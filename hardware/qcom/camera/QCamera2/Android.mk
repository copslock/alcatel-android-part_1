# Enable SDLLVM compiler option for build flavour >= N flavour
PLATFORM_SDK_NPDK = 24
ENABLE_CAM_SDLLVM  := $(shell if [ $(PLATFORM_SDK_VERSION) -ge $(PLATFORM_SDK_NPDK) ] ; then echo true ; else echo false ; fi)
ifeq ($(ENABLE_CAM_SDLLVM),true)
SDCLANGSAVE := $(SDCLANG)
SDCLANG := true
endif

ifneq (,$(filter $(TARGET_ARCH), arm arm64))

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_COPY_HEADERS_TO := qcom/camera
LOCAL_COPY_HEADERS := QCameraFormat.h

#[FEATURE]Add-BEGIN by TCTSH.chen.cao 2016/1/28 for camera tsHDR feature support
TS_PATH := HAL/ts
include $(CLEAR_VARS)
LOCAL_MODULE        := libtsvideoprocess
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB      := 32
LOCAL_MODULE_TAGS   := optional
LOCAL_SRC_FILES     := $(TS_PATH)/libtsvideoprocess.so
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/lib
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)
#[FEATURE]Add-END by TCTSH.chen.cao

# MODIFIED-BEGIN by hongzhang, 2016-10-21,BUG-3199086
include $(CLEAR_VARS)
EIS_PATH := DxOEIS_n8996
LOCAL_MODULE        := DxOEIS
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_SRC_FILES     := $(EIS_PATH)/DxOEIS.a
LOCAL_MULTILIB      := 32
LOCAL_MODULE_SUFFIX := .a
LOCAL_MODULE_TAGS   := optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_2ND_ARCH_VAR_PREFIX := arm
include $(BUILD_PREBUILT)
# MODIFIED-END by hongzhang,BUG-3199086

# MODIFIED-BEGIN by hongzhang, 2016-10-17,BUG-3124009
VISIDON_PATH := Visidon
include $(CLEAR_VARS)
LOCAL_MODULE        := libVDSuperPhotoAPI
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB      := 32
LOCAL_MODULE_TAGS   := optional
LOCAL_SRC_FILES     := $(VISIDON_PATH)/libVDSuperPhotoAPI.so
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/lib
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libVDLowLightAPI
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB      := 32
LOCAL_MODULE_TAGS   := optional
LOCAL_SRC_FILES     := $(VISIDON_PATH)/libVDLowLightAPI.so
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/lib
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libVDHDRAPI
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB      := 32
LOCAL_MODULE_TAGS   := optional
LOCAL_SRC_FILES     := $(VISIDON_PATH)/libVDHDRAPI.so
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/lib
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libVDBeautyShotAPI
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB      := 32
LOCAL_MODULE_TAGS   := optional
LOCAL_SRC_FILES     := $(VISIDON_PATH)/libVDBeautyShotAPI.so
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/lib
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)
include $(CLEAR_VARS)
# MODIFIED-END by hongzhang,BUG-3124009
LOCAL_SRC_FILES := \
        util/QCameraBufferMaps.cpp \
        util/QCameraCmdThread.cpp \
        util/QCameraFlash.cpp \
        util/QCameraPerf.cpp \
        util/QCameraQueue.cpp \
        util/QCameraCommon.cpp \
        QCamera2Hal.cpp \
        QCamera2Factory.cpp

#HAL 3.0 source
LOCAL_SRC_FILES += \
        HAL3/QCamera3HWI.cpp \
        HAL3/QCamera3Mem.cpp \
        HAL3/QCamera3Stream.cpp \
        HAL3/QCamera3Channel.cpp \
        HAL3/QCamera3VendorTags.cpp \
        HAL3/QCamera3PostProc.cpp \
        HAL3/QCamera3CropRegionMapper.cpp \
        HAL3/QCamera3StreamMem.cpp

LOCAL_CFLAGS := -Wall -Wextra -Werror

#HAL 1.0 source

ifeq ($(TARGET_SUPPORT_HAL1),false)
LOCAL_CFLAGS += -DQCAMERA_HAL3_SUPPORT
else
LOCAL_CFLAGS += -DQCAMERA_HAL1_SUPPORT
LOCAL_SRC_FILES += \
        HAL/QCamera2HWI.cpp \
        HAL/QCameraMuxer.cpp \
        HAL/QCameraMem.cpp \
        HAL/QCameraStateMachine.cpp \
        util/QCameraDisplay.cpp \
        HAL/QCameraChannel.cpp \
        HAL/QCameraStream.cpp \
        HAL/QCameraPostProc.cpp \
        HAL/QCamera2HWICallbacks.cpp \
        HAL/QCameraParameters.cpp \
        HAL/QCameraParametersIntf.cpp \
        HAL/QCameraThermalAdapter.cpp
endif

# System header file path prefix
LOCAL_CFLAGS += -DSYSTEM_HEADER_PREFIX=sys

LOCAL_CFLAGS += -DHAS_MULTIMEDIA_HINTS -D_ANDROID

ifeq ($(TARGET_USES_AOSP),true)
LOCAL_CFLAGS += -DVANILLA_HAL
endif

ifeq (1,$(filter 1,$(shell echo "$$(( $(PLATFORM_SDK_VERSION) <= 23 ))" )))
LOCAL_CFLAGS += -DUSE_HAL_3_3
endif

#use media extension
ifeq ($(TARGET_USES_MEDIA_EXTENSIONS), true)
LOCAL_CFLAGS += -DUSE_MEDIA_EXTENSIONS
endif

LOCAL_CFLAGS += -std=c++11 -std=gnu++0x
#HAL 1.0 Flags
LOCAL_CFLAGS += -DDEFAULT_DENOISE_MODE_ON -DHAL3 -DQCAMERA_REDEFINE_LOG

LOCAL_C_INCLUDES := \
        $(LOCAL_PATH)/../mm-image-codec/qexif \
        $(LOCAL_PATH)/../mm-image-codec/qomx_core \
        $(LOCAL_PATH)/include \
        $(LOCAL_PATH)/stack/common \
        $(LOCAL_PATH)/stack/mm-camera-interface/inc \
        $(LOCAL_PATH)/util \
        $(LOCAL_PATH)/HAL3 \
        hardware/libhardware/include/hardware \
        hardware/qcom/media/libstagefrighthw \
        hardware/qcom/media/mm-core/inc \
        system/core/include/cutils \
        system/core/include/system \
        system/media/camera/include/system

#HAL 1.0 Include paths
LOCAL_C_INCLUDES += \
        frameworks/native/include/media/hardware \
        hardware/qcom/camera/QCamera2/HAL

ifeq ($(TARGET_COMPILE_WITH_MSM_KERNEL),true)
LOCAL_C_INCLUDES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include
LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr
endif

ifneq (,$(filter msm8974 msm8916 msm8226 msm8610 msm8916 apq8084 msm8084 msm8994 msm8992 msm8952 msm8937 msm8953 msm8996 msmcobalt msmfalcon, $(TARGET_BOARD_PLATFORM)))
    LOCAL_CFLAGS += -DVENUS_PRESENT
endif

ifneq (,$(filter msm8996 msmcobalt msmfalcon,$(TARGET_BOARD_PLATFORM)))
    LOCAL_CFLAGS += -DUBWC_PRESENT
endif

#LOCAL_STATIC_LIBRARIES := libqcamera2_util
LOCAL_C_INCLUDES += \
        $(TARGET_OUT_HEADERS)/qcom/display
LOCAL_C_INCLUDES += \
        hardware/qcom/display/libqservice
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(EIS_PATH)
LOCAL_SHARED_LIBRARIES := libcamera_client liblog libhardware libutils libcutils libdl libsync libgui
LOCAL_SHARED_LIBRARIES += libmmcamera_interface libmmjpeg_interface libui libcamera_metadata
# MODIFIED-BEGIN by hongzhang, 2016-10-17,BUG-3124009
LOCAL_SHARED_LIBRARIES += libVDSuperPhotoAPI libVDHDRAPI libVDLowLightAPI libVDBeautyShotAPI
LOCAL_CFLAGS += -DTCT_VISIDON_FEATURE
# MODIFIED-END by hongzhang,BUG-3124009

#[FEATURE]Add-BEGIN by TCTSH.chen.cao 2016/1/28 for camera tsHDR feature support
LOCAL_C_INCLUDES += $(LOCAL_PATH)/HAL/ts
LOCAL_SHARED_LIBRARIES += libtsvideoprocess
LOCAL_CFLAGS += -DTCT_TSHDR_FEATURE
#[FEATURE]Add-END by TCTSH.chen.cao 2016/1/28

LOCAL_SHARED_LIBRARIES += libqdMetaData libqservice libbinder
LOCAL_SHARED_LIBRARIES += libcutils libdl

# MODIFIED-BEGIN by hongzhang, 2016-10-21,BUG-3199086
LOCAL_SHARED_LIBRARIES += libandroid
LOCAL_STATIC_LIBRARIES += DxOEIS
LOCAL_CFLAGS += -DTCT_TARGET_EIS_DXO_ENABLE
# MODIFIED-END by hongzhang,BUG-3199086

LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE := camera.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_TAGS := optional

LOCAL_32_BIT_ONLY := $(BOARD_QTI_CAMERA_32BIT_ONLY)
include $(BUILD_SHARED_LIBRARY)

include $(call first-makefiles-under,$(LOCAL_PATH))
endif
ifeq ($(ENABLE_CAM_SDLLVM),true)
SDCLANG := $(SDCLANGSAVE)
endif

