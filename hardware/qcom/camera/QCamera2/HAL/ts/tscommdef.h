/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef __TS_COMM_DEF_H__
#define __TS_COMM_DEF_H__
typedef enum enumVideoType
{
    VIDEO_PROCSS_NONE = -1,
    VIDEO_PROCESS_VIDEO_DENOISER = 0,
    VIDEO_PROCESS_MOVIE_SOLID=1,
    VIDEO_PROCESS_MOVIE_SOLID_TS=2,
    VIDEO_PROCESS_VIDEO_WDR=3,
    VIDEO_PROCESS_HDR_CHECKER=4,
    VIDEO_PROCSS_END,
}VideoType;
typedef enum enumPhotoType
{
    PHOTO_PROCESS_NONE = -1,
    PHOTO_PROCESS_NIGHTPORTRAIT = 0,
    PHOTO_PROCESS_HDR=1,
    PHOTO_PROCESS_PHOTOSOLID=2,
    PHOTO_PROCESS_DENOISER=3,
    PHOTO_PROCESS_SUPERRESOLUTION=4,
    PHOTO_PROCESS_SUPERZOOM=5,
    PHOTO_PROCESS_SUPERRESOLUTIONFAST=6,
    PHOTO_PROCESS_ULTRANR=7,
    PHOTO_PROCESS_SMARTSELECT=8,
    PHOTO_PROCESS_END,
}PhotoType;
typedef void (*ALLOCATECALLBACK)(void **buffer, int bufferSize, void *caller);
typedef void (*DEALLOCATECALLBACK)(void *caller);

// param hdr key
#define TS_MORPHO_HDR_GHOST_DETECTION_SENSITIVITY_LEVEL   "ts_morpho_hdr_GhostDetectionSensitivityLevel"
#define TS_MORPHO_HDR_GHOST_REMOVAL_STRENGTH_LEVEL        "ts_morpho_hdr_GhostRemovalStrengthLevel"
#define TS_MORPHO_HDR_RELIABLE_RECT_RATE_THRESHOLD        "ts_morpho_hdr_ReliableRectRateThreshold"
#define TS_MORPHO_HDR_GHOST_RATE_THREASHOLD               "ts_morpho_hdr_GhostRateThreshold"
#define TS_MORPHO_HDR_ZOOM                                "ts_morpho_hdr_Zoom"
#define TS_MORPHO_HDR_FAIL_SOFTMERGING_STATU              "ts_morpho_hdr_FailSoftMergingStatus"
#define TS_MORPHO_HDR_FMCOLOR_CORRECTION_CONTRAST_LEVEL   "ts_morpho_hdr_FMColorCorrectionContrastLevels"
#define TS_MORPHO_HDR_FMCOLOR_CORRECTION_SATURATION_LEVEL "ts_morpho_hdr_FMColorCorrectionSaturationLevels"
#define TS_MORPHO_HDR_NORMAL_EV_INDEX                     "ts_morpho_hdr_normal_ev_index"
#define TS_MORPHO_HDR_LOWER_EV_INDEX                      "ts_morpho_hdr_lower_ev_index"
#define TS_MORPHO_HDR_HIGH_EV_INDEX                       "ts_morpho_hdr_high_ev_index"
// end

// end
#endif // __TS_COMM_DEF_H__
