/*
#============================================================================
#  Name:                                                                     
#    qc_version.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 02:43:39 2016 
#============================================================================
*/
const char QC_IMAGE_VERSION_STRING_AUTO_UPDATED[]="QC_IMAGE_VERSION_STRING=BOOT.BF.3.3-00177";
