#!/bin/bash

cd `dirname $0`
#First build to ensure no error

if [ $1 = "8952" ]
then
       echo  " Building for Target 8952 "
       sh build.sh TARGET_FAMILY=8952  --prod

       sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK

       # Remove unship files
       sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952 BUILD_ID=SAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC

       #recommpile
       sh build.sh TARGET_FAMILY=8952 --prod 
              
fi

if [ $1 = "8976" ]
then
       echo  " Building for Target 8976 "
       sh build.sh TARGET_FAMILY=8976  --prod

       sh build.sh TARGET_FAMILY=8976 BUILD_ID=EAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build.sh TARGET_FAMILY=8976 BUILD_ID=EAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build.sh TARGET_FAMILY=8976 BUILD_ID=EAAAANAZ 
              
fi

if [ $1 = "8952_8976" ]
then
	   sh build.sh TARGET_FAMILY=8952_8976  --prod
       echo  " Building for Target 8976 "
       #sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   #sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   # Remove unship files
       #sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=SAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
	   
	   #sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=EAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
       
	   sh build.sh TARGET_FAMILY=8952_8976 --prod
fi


if [ $1 = "8953" ]
then
       echo  " Building for Target 8953 "
       sh build.sh TARGET_FAMILY=8953  --prod

       sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK

       # Remove unship files
       sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8953 BUILD_ID=JAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC

       #recommpile
       sh build.sh TARGET_FAMILY=8953 --prod 
              
fi

if [ $1 = "8937" ]
then
       echo  " Building for Target 8937 "
       sh build.sh TARGET_FAMILY=8937  --prod

       sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK

       # Remove unship files
       sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC

       #recommpile
       sh build.sh TARGET_FAMILY=8937 --prod 
              
fi

if [ $1 = "8917" ]
then
       echo  " Building for Target 8917 "
       sh build.sh TARGET_FAMILY=8917  --prod

       sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK

       # Remove unship files
       sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8917 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC

       #recommpile
       sh build.sh TARGET_FAMILY=8917 --prod 
              
fi
if [ $1 = "8937_8917" ]
then
	   sh build.sh TARGET_FAMILY=8937_8917  --prod
       echo  " Building for Target 8937 "
       #sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   #sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=LAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=LAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   # Remove unship files
       #sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
	   
	   #sh build.sh TARGET_FAMILY=8952_8976 BUILD_ID=LAAAANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=LAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
       
	   sh build.sh TARGET_FAMILY=8937_8917 --prod
fi

if [ $1 = "8937_8917_8953" ]
then
	   sh build.sh TARGET_FAMILY=8937_8917_8953 --prod
       sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=FAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=LAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=JAASANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=JAADANAZ -c --implicit-deps-unchanged USES_FLAGS=USES_CLEAN_PACK
	   
	   # Remove unship files
       sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=FAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=FAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
	   
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=LAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=LAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC
       
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=JAASANAZ  -c --implicit-deps-unchanged --cleanpack
	   sh build.sh TARGET_FAMILY=8937_8917_8953 BUILD_ID=JAADANAZ -c --implicit-deps-unchanged --cleanpack USES_FLAGS=USES_DEVPRO_DDR,USES_DEVPRO_DDR_SEC,USES_DEVPRO_LITE,USES_DEVPRO_LITE_SEC	
		
	   sh build.sh TARGET_FAMILY=8937_8917_8953 --prod
fi