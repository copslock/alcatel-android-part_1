// *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                     SCATTER LOADING DESCRIPTION FILE TEMPLATE
//  
//
//  GENERAL DESCRIPTION
//
//  Memory Map for E-HOSTDL.
//
//  This file is a template which gets run through the C pre-processor to
//  generate the actual scatter load file which will be used.  This allows
//  this file to be used by all targets and be relocated on the fly.
//
//  Copyright (c) 2010 Qualcomm Incorporated. 
//  All Rights Reserved.
//  Qualcomm Confidential and Proprietary
//
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

/* ===========================================================================

                           EDIT HISTORY FOR FILE
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.3/boot_images/core/storage/flash/tools/src/hostdl/ehostdl_in.scl#1 $  $DateTime: 2015/07/02 04:11:47 $ $Author: pwbldsvc $
   
 when       who     what, where, why
 --------   ---     -------------------------------------------------------
 08/18/10   bb      Initial Version
==========================================================================*/

#include "ddr_ehostdl.h"
#include "flash_ehostdl.h"

RAM CODE_START_ADDR
{
   CODE CODE_START_ADDR 
   {
     hostdl_startup.o (BootRom, +FIRST)
     * (+RO)
   }
   ;DBL code is needed for system initialization (which includes main memory)
   ;Once we have the main memory, we donot use DBL code for core host 
   ;download functionality. The solution is to move the DBL code out of 
   ;relocation sections.
   DBL_RAM_RW +0x0
   {
     DDR_EHOSTDL_RW_MODULES
     FLASH_EHOSTDL_RW_MODULES
     
     DDR_EHOSTDL_CONST_MODULES
     FLASH_EHOSTDL_CONST_MODULES
   }

   APP_RAM DATA_ADDR
   {
     * (.constdata)   ; Relocate all constdata to RAM since accessing
                       ; string literals and other unaligned
                       ; data in ADSP IRAM could cause issues
     * (+RW)
   }
}

;DBL ZI data initialization zero's out the range of addresses.
;In order to avoid the harm of accidental overwriting of the APP_RAM data
;(which needs to be relocated to it its execution view in RAM), DBL ZI is
;assigned separate region at the end.
DBL_ZI_REGION +0x0
{
   DBL_RAM_ZI +0x0
   {
     DDR_EHOSTDL_ZI_MODULES
     FLASH_EHOSTDL_ZI_MODULES
   }
}

; ZI region for code in DDR. The execution region address, 
; is coming from the Scons/make files. 
APP_RAM_ZI_REGION +0x0
{
   APP_RAM_ZI APP_RAM_ZI_REGION_OFFSET
   {
     * (+ZI)
   }
}

