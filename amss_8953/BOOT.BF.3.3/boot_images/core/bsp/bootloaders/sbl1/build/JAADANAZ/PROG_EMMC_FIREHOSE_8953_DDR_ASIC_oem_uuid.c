/*
#============================================================================
#  Name:                                                                     
#    PROG_EMMC_FIREHOSE_8953_DDR_ASIC_oem_uuid.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 03:00:31 2016 
#============================================================================
*/
const char OEM_IMAGE_UUID_STRING_AUTO_UPDATED[]="OEM_IMAGE_UUID_STRING=Q_SENTINEL_{8F31F0F0-3619-4F18-B5B4-8F4EB1FE389D}_20160818_0300";
