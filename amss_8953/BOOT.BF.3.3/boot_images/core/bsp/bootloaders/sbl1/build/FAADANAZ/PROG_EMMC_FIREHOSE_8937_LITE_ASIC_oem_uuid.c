/*
#============================================================================
#  Name:                                                                     
#    PROG_EMMC_FIREHOSE_8937_LITE_ASIC_oem_uuid.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2016 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Thu Aug 18 02:54:59 2016 
#============================================================================
*/
const char OEM_IMAGE_UUID_STRING_AUTO_UPDATED[]="OEM_IMAGE_UUID_STRING=Q_SENTINEL_{8EE6ECD1-2785-47BA-867C-53266B7B0F07}_20160818_0254";
