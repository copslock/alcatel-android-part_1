/*==============================================================================

FILE:      icbcfg_data.c

DESCRIPTION: This file implements the ICB Configuration driver.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

$Header: //components/rel/boot.bf/3.3/boot_images/core/buses/icb/src/8976/icbcfg_data.c#5 $ 
$DateTime: 2016/01/06 23:32:55 $
$Author: pwbldsvc $
$Change: 9681911 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2015/05/27  rc     Update BKE context allocation and recommended settings 
                   for DSAT
2015/10/06  sds    Force all accessses to SNOC to DEVICE to deal with
                   Gladiator forcing all accesses to SO.
2015/09/10  sds    Force AOOO for all accesses from modem Q6.
2015/05/27  rc     To map the SNOC to the 512MB region 
2014/11/07  rc     Branched for 8952
2014/09/12  sds    Update danger context allocation
2014/06/19  sds    Update to v0.9 8936 spreadsheet
2014/05/20  sds    Change the hardware base address to a uint8_t*.
2014/05/20  sds    Allow 3GB memory maps
2014/01/27  sds    Branched for 8936
2013/12/11  sds    First pass at DDR settings spreadsheet
2013/10/09  sds    Branched for 8916
2013/02/26  sds    Changed how available DDR regions are handled.
2013/02/04  jc     Updated masters and slaves to match TO
2012/12/06  jc     8x10 implementation
2012/03/26  av     Created
 
        Copyright (c) 2012-2015 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "icbcfg.h"
#include "icbcfgi.h"
#include "icbcfg_hwio.h"
#include "HALbimc.h"
#include "HALbimcHwioGeneric.h"

/*---------------------------------------------------------------------------*/
/*          Macro and constant definitions                                   */
/*---------------------------------------------------------------------------*/
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))

/* BIMC register value macros */
#define SLAVE_SEGMENT(slave,index,addr_base,addr_mask)  \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_BASEm_LOWER_ADDR((uint8_t *)BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_BASEm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_BASEm_LOWER_RMSK, \
   (addr_base) }, \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_MASKm_LOWER_ADDR((uint8_t *)BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   (addr_mask) }
#define SLAVE_SEGMENT_MASK(slave,index,addr_mask)  \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_SEGMENTn_ADDR_MASKm_LOWER_ADDR(BIMC_BASE,slave,index), \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   BIMC_SEGMENTn_ADDR_MASKm_LOWER_RMSK, \
   (addr_mask) }

#define BIMC_DEFAULT(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_DEFAULT_SEGMENT_ADDR(BIMC_BASE), \
   BIMC_DEFAULT_SEGMENT_RMSK, \
   BIMC_DEFAULT_SEGMENT_RMSK, \
   (value) }

#define ARB_MODE(slave,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_S_ARB_MODE_ADDR((uint8_t *)BIMC_BASE,slave), \
   BIMC_S_ARB_MODE_RMSK, \
   BIMC_S_ARB_MODE_RMSK, \
   (mode) }

#define SWAY_GATHERING(slave,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_S_SWAY_GATHERING_ADDR(BIMC_BASE,slave), \
   BIMC_S_SWAY_GATHERING_RMSK, \
   BIMC_S_SWAY_GATHERING_RMSK, \
   (mode) }

/* Slave indexes */
#define SLAVE_DDR_CH0 0
#define SLAVE_DDR_CH1 1
#define SLAVE_SNOC    2

#define MPORT_MODE(master,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_MODE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_MODE_RMSK, \
   BIMC_M_MODE_RMSK, \
   (mode) }

#define MPORT_PIPE0_GATHERING(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_PIPE0_GATHERING_ADDR(BIMC_BASE,master), \
   BIMC_M_PIPE0_GATHERING_RMSK, \
   BIMC_M_PIPE0_GATHERING_RMSK, \
   (value) }

#define MPORT_PIPE1_GATHERING(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_PIPE1_GATHERING_ADDR(BIMC_BASE,master), \
   BIMC_M_PIPE1_GATHERING_RMSK, \
   BIMC_M_PIPE1_GATHERING_RMSK, \
   (value) }

#define MPORT_RD_TRACKING_INDEX(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_RD_TRACKING_INDEX_ADDR(BIMC_BASE,master), \
   BIMC_M_RD_TRACKING_INDEX_RMSK, \
   BIMC_M_RD_TRACKING_INDEX_RMSK, \
   (value) }

#define MPORT_WR_TRACKING_INDEX(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_WR_TRACKING_INDEX_ADDR(BIMC_BASE,master), \
   BIMC_M_WR_TRACKING_INDEX_RMSK, \
   BIMC_M_WR_TRACKING_INDEX_RMSK, \
   (value) }

#define MPORT_BKE_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE_ENABLE_RMSK, \
   BIMC_M_BKE_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE1_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE1_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE1_ENABLE_RMSK, \
   BIMC_M_BKE1_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE2_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE2_ENABLE_ADDR(BIMC_BASE,master), \
   BIMC_M_BKE2_ENABLE_RMSK, \
   BIMC_M_BKE2_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_ENABLE_RMSK, \
   BIMC_M_BKE3_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_PERIOD(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_PERIOD_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_COUNT(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_COUNT_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_MEDIUM(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_MEDIUM_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_LOW(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_LOW_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   (value) }

#define MPORT_BKE3_HEALTH_0(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_HEALTH_0_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   (value) }

#define MPORT_READ_COMMAND_OVERRIDE(master, value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_READ_COMMAND_OVERRIDE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_READ_COMMAND_OVERRIDE_RMSK, \
   BIMC_M_READ_COMMAND_OVERRIDE_RMSK, \
   (value) }

#define MPORT_WRITE_COMMAND_OVERRIDE(master, value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_WRITE_COMMAND_OVERRIDE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_WRITE_COMMAND_OVERRIDE_RMSK, \
   BIMC_M_WRITE_COMMAND_OVERRIDE_RMSK, \
   (value) }

/* Master indexes */
#define MASTER_APP   0
#define MASTER_DSP   1
#define MASTER_MMSS  2
#define MASTER_SYS   3
#define MASTER_TCU   4

#define QOS_CTRL(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_CTRL_ADDR(BIMC_BASE), \
   BIMC_QOS_CTRL_RMSK, \
   BIMC_QOS_CTRL_RMSK, \
   (value) }

#define QOS_CFG(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_CFG_ADDR(BIMC_BASE), \
   BIMC_QOS_CFG_RMSK, \
   BIMC_QOS_CFG_RMSK, \
   (value) }

#define QOS_TIMEOUT_CNT_LOW_URGENCY(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_ADDR(BIMC_BASE), \
   BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_RMSK, \
   BIMC_QOS_TIMEOUT_CNT_LOW_URGENCY_RMSK, \
   (value) }

#define QOS_TIMEOUT_CNT_HIGH_URGENCY(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_ADDR(BIMC_BASE), \
   BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_RMSK, \
   BIMC_QOS_TIMEOUT_CNT_HIGH_URGENCY_RMSK, \
   (value) }

#define QOS_FSSH_URGENCY_SEL(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_QOS_FSSH_URGENCY_SEL_ADDR(BIMC_BASE), \
   BIMC_QOS_FSSH_URGENCY_SEL_RMSK, \
   BIMC_QOS_FSSH_URGENCY_SEL_RMSK, \
   (value) }

#define SYS_SWAY_READ_COMMAND_OVERRIDE  0x00458250
#define SYS_SWAY_WRITE_COMMAND_OVERRIDE 0x00458260
/* Throttle Master indexes */
#define MASTER_GPU_0   0
#define MASTER_GPU_1   1

#define THROTTLE_CNTRL(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_CNTRL_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_CNTRL_RMSK, \
   THROTTLE_CNTRL_RMSK, \
   (value) }

#define THROTTLE_CGC_CNTRL(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_CGC_CNTRL_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_CGC_CNTRL_RMSK, \
   THROTTLE_CGC_CNTRL_RMSK, \
   (value) }

#define THROTTLE_GRANT_PERIOD(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_GRANT_PERIOD_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_GRANT_PERIOD_RMSK, \
   THROTTLE_GRANT_PERIOD_RMSK, \
   (value) }

#define THROTTLE_THRESHOLD_00(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_THRESHOLD_00_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_THRESHOLD_00_RMSK, \
   THROTTLE_THRESHOLD_00_RMSK, \
   (value) }

#define THROTTLE_THRESHOLD_01(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_THRESHOLD_01_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_THRESHOLD_01_RMSK, \
   THROTTLE_THRESHOLD_01_RMSK, \
   (value) }

#define THROTTLE_THRESHOLD_02(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_THRESHOLD_02_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_THRESHOLD_02_RMSK, \
   THROTTLE_THRESHOLD_02_RMSK, \
   (value) }

#define THROTTLE_THRESHOLD_03(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_THRESHOLD_03_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_THRESHOLD_03_RMSK, \
   THROTTLE_THRESHOLD_03_RMSK, \
   (value) }

#define THROTTLE_PEAK_ACCUM_CREDIT(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)THROTTLE_PEAK_ACCUM_CREDIT_ADDR((uint8_t *)THROTTLE_WRAPPER_2_THROTTLE_WRAPPER_2_BASE,master), \
   THROTTLE_PEAK_ACCUM_CREDIT_RMSK, \
   THROTTLE_PEAK_ACCUM_CREDIT_RMSK, \
   (value) }                                   
/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/*---------------------------------------------------------------------------*/
/*          Properties data for device ID  = "icbcfg/boot"                   */
/*---------------------------------------------------------------------------*/

/* ICBcfg Boot Configuration Data*/

icbcfg_data_type icbcfg_boot_data[] = 
{
    /* Add configuration data using
      ICBCFG_HWIO_*() or
      ICBCFG_RAW_*() macros below
      .
      .                          
      .                          */

    BIMC_DEFAULT(0x00030003),	
    SLAVE_SEGMENT_MASK(SLAVE_SNOC, 0, 0xF0000000),
    ARB_MODE(SLAVE_DDR_CH0, 0x10000001),
    ARB_MODE(SLAVE_DDR_CH1, 0x10000001),


    MPORT_MODE(MASTER_APP,   0x10),
    MPORT_MODE(MASTER_DSP,   0x12),
    MPORT_MODE(MASTER_MMSS,  0x12),
    MPORT_MODE(MASTER_SYS,   0x12),
    MPORT_MODE(MASTER_TCU,   0x2012),

    /* Force A000 for Modem Q6 */
    MPORT_READ_COMMAND_OVERRIDE(MASTER_DSP, 0x202),
    MPORT_WRITE_COMMAND_OVERRIDE(MASTER_DSP, 0x202),

    /* Force DeviceType for all traffic to sys slaveway */
    ICBCFG_RAW_DW(SYS_SWAY_READ_COMMAND_OVERRIDE, 0x00010040),
    ICBCFG_RAW_DW(SYS_SWAY_WRITE_COMMAND_OVERRIDE, 0x00010040),

    MPORT_PIPE0_GATHERING(MASTER_SYS, 0x50000),
    MPORT_PIPE0_GATHERING(MASTER_TCU, 0x30000),
    MPORT_PIPE1_GATHERING(MASTER_SYS, 0x50000),
    MPORT_PIPE1_GATHERING(MASTER_TCU, 0x30000),

    THROTTLE_CGC_CNTRL(MASTER_GPU_0,  0x1),
    THROTTLE_GRANT_PERIOD(MASTER_GPU_0,  0x3E8),
    THROTTLE_THRESHOLD_00(MASTER_GPU_0,  0x320032),
    THROTTLE_THRESHOLD_01(MASTER_GPU_0,  0x960064),
    THROTTLE_THRESHOLD_02(MASTER_GPU_0,  0x12C00E1),
    THROTTLE_THRESHOLD_03(MASTER_GPU_0,  0xFFFF),
    THROTTLE_PEAK_ACCUM_CREDIT(MASTER_GPU_0,  0x100),


    THROTTLE_CGC_CNTRL(MASTER_GPU_1,  0x1),
    THROTTLE_GRANT_PERIOD(MASTER_GPU_1,  0x3E8),
    THROTTLE_THRESHOLD_00(MASTER_GPU_1,  0x320032),
    THROTTLE_THRESHOLD_01(MASTER_GPU_1,  0x960064),
    THROTTLE_THRESHOLD_02(MASTER_GPU_1,  0x12C00E1),
    THROTTLE_THRESHOLD_03(MASTER_GPU_1,  0xFFFF),
    THROTTLE_PEAK_ACCUM_CREDIT(MASTER_GPU_1,  0x100),

    ICBCFG_HWIO_DW(DSA_DSA_CORE_CLK_CGC_CNTRL, 0x1),
    ICBCFG_HWIO_DW(DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0, 0x60),
    ICBCFG_HWIO_DW(DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0, 0x60),
    ICBCFG_HWIO_DW(DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3, 0x27),
    ICBCFG_HWIO_DW(DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL, 0x1),

    ICBCFG_HWIO_DW(DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT, 0x0),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL, 8, 0x3),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL, 8, 0x1),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL, 9, 0x3),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL, 9, 0x1),
    ICBCFG_HWIO_DW(DSA_DSA_DSP_PRIORITY_MODE_SEL, 0x1),
    ICBCFG_HWIO_DW(DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL, 0x0),

    ICBCFG_HWIOI_DW(DSA_QOS_FREQ_BAND_BNDRY_n, 0, 0x17EF),
    ICBCFG_HWIOI_DW(DSA_QOS_FREQ_BAND_BNDRY_n, 1, 0xE06),
    ICBCFG_HWIOI_DW(DSA_QOS_FREQ_BAND_BNDRY_n, 2, 0xA2C),

    ICBCFG_HWIOF_READ_DW(BIMC_DDR_CH0_CLK_PERIOD,PERIOD),
    ICBCFG_HWIOF_WRITE_DW(DSA_DSA_PERIOD_BUS_CFG,PERIOD_BUS_SW),
    ICBCFG_HWIOF_DW(DSA_DSA_PERIOD_BUS_CFG, PERIOD_BUS_LOAD_SW, 0x1),
    ICBCFG_HWIOF_DW(DSA_DSA_PERIOD_BUS_CFG, PERIOD_BUS_SW_OVERRIDE, 0x0),

    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 1, 0x7C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 2, 0x81C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 3, 0x80C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 4, 0x82C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 5, 0x81C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 6, 0x80C8),
    ICBCFG_HWIOI_DW(DSA_DSA_THROTTLE_LEVEL_QOS_n, 7, 0x7C8),

    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1, 8,  0x0),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2, 8,  0xEA000081),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3, 8,  0xEA000081),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4, 8,  0xEA000082),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5, 8,  0xEA000082),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6, 8,  0xEA000082),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7, 8,  0xEE000003),

    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1, 9,  0xEE000001),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2, 9,  0xEE000002),
    ICBCFG_HWIOI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3, 9,  0xEE000003),
    ICBCFG_HWIOFI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 8, THROTTLE_LEVEL_EN, 0x1),
    ICBCFG_HWIOFI_DW(DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 9, THROTTLE_LEVEL_EN, 0x1),
    ICBCFG_HWIO_DW(DSA_DSA_DANGER_SAFE_CNTRL,0x1),
    

    THROTTLE_CNTRL(MASTER_GPU_0,  0x1),
    THROTTLE_CNTRL(MASTER_GPU_1,  0x1),

    MPORT_BKE3_GRANT_PERIOD(MASTER_APP, 0x14),
    MPORT_BKE3_GRANT_COUNT(MASTER_APP, 0x64),
    MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_APP, 0xFFCE),
    MPORT_BKE3_THRESHOLD_LOW(MASTER_APP, 0xFF9C),
    MPORT_BKE3_HEALTH_0(MASTER_APP, 0x80000000),

    MPORT_BKE_ENABLE(MASTER_APP,  0x01030000),
    MPORT_BKE1_ENABLE(MASTER_APP, 0x02000000),
    MPORT_BKE2_ENABLE(MASTER_APP, 0x04000000),
    MPORT_BKE3_ENABLE(MASTER_APP, 0x38000001),

    QOS_FSSH_URGENCY_SEL(0x00000010),
    QOS_CTRL(0x00000001),
};

icbcfg_prop_type icbcfg_boot_prop = 
{
    /* Length of the config  data array */
    ARRAY_SIZE(icbcfg_boot_data),
    /* Pointer to config data array */ 
    icbcfg_boot_data                                    
};

/* DDR map information. */
uint32 map_ddr_region_count = 1; 
icbcfg_mem_region_type map_ddr_regions[1] =
{
  { 0x10000000ULL, 0x100000000ULL },
};

uint32 channel_map[2] = { SLAVE_DDR_CH0, SLAVE_DDR_CH1 };

HAL_bimc_InfoType bimc_hal_info =
{
  (uint8_t *)BIMC_BASE, /* Base address */
  19200,     /* QoS frequency */
  {
    0,
    0,
    0,
    0,
    0,
    0,
    3, /**< Number of segments for address decode. */
  }
};

/* Make sure the config region is always prohibited when "resetting" */
HAL_bimc_SlaveSegmentType safe_reset_seg =
{
  true,
  0x00000000ULL,                 /* start of config region */
  0x10000000ULL,                 /* 256 MB */
  BIMC_SEGMENT_TYPE_SUBTRACTIVE,
  BIMC_INTERLEAVE_NONE,
};

/*---------------------------------------------------------------------------*/
/* MSM8976 - ALL */
icbcfg_device_config_type msm8976_all =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8976,  /**< Chip family */
  false,                       /**< Exact match for version? */
  0,                           /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop,           /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  3,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  NULL,                        /**< L2 TCM unmap configuration */
};

/* Definitions list */
icbcfg_device_config_type *configs[] =
{
  &msm8976_all,
};

/* Exported target definitions */
icbcfg_info_type icbcfg_info =
{
  ARRAY_SIZE(configs),
  configs,
};
