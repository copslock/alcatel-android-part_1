#ifndef __ICBCFG_HWIO_H__
#define __ICBCFG_HWIO_H__
/*
===========================================================================
*/
/**
  @file icbcfg_hwio.h
  @brief Auto-generated HWIO interface include file.

*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/icb/src/8976/icbcfg_hwio.h#2 $
  $DateTime: 2015/11/19 23:23:16 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/* ICBCFG HWIO File for 8952 target */

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: DSA_DSA
 *--------------------------------------------------------------------------*/

#define DSA_DSA_REG_BASE                                                                                       (DSA_DSA_BASE      + 0x00000000)

#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_ADDR                                                                     (DSA_DSA_REG_BASE      + 0x00000000)
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_RMSK                                                                            0x1
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_ACK_BYPASS_EN_ADDR, HWIO_DSA_DSA_DT_ACK_BYPASS_EN_RMSK)
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_ACK_BYPASS_EN_ADDR, m)
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_ACK_BYPASS_EN_ADDR,v)
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_ACK_BYPASS_EN_ADDR,m,v,HWIO_DSA_DSA_DT_ACK_BYPASS_EN_IN)
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_DT_ACK_BYPASS_EN_BMSK                                                           0x1
#define HWIO_DSA_DSA_DT_ACK_BYPASS_EN_DT_ACK_BYPASS_EN_SHFT                                                           0x0

#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_ADDR                                                                    (DSA_DSA_REG_BASE      + 0x00000004)
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_RMSK                                                                           0x1
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_DANGER_SAFE_CNTRL_ADDR, HWIO_DSA_DSA_DANGER_SAFE_CNTRL_RMSK)
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DANGER_SAFE_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DANGER_SAFE_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DANGER_SAFE_CNTRL_ADDR,m,v,HWIO_DSA_DSA_DANGER_SAFE_CNTRL_IN)
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_DANGER_SAFE_EN_BMSK                                                            0x1
#define HWIO_DSA_DSA_DANGER_SAFE_CNTRL_DANGER_SAFE_EN_SHFT                                                            0x0

#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_ADDR                                                                  (DSA_DSA_REG_BASE      + 0x00000008)
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_RMSK                                                                         0x3
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_ADDR, HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_RMSK)
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_ADDR,m,v,HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_IN)
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_DSP_SAFE_MASK_VALUE_BMSK                                                     0x2
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_DSP_SAFE_MASK_VALUE_SHFT                                                     0x1
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_DSP_SAFE_MASK_EN_BMSK                                                        0x1
#define HWIO_DSA_DSA_DSP_SAFE_MASK_CNTRL_DSP_SAFE_MASK_EN_SHFT                                                        0x0

#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_ADDR                                                             (DSA_DSA_REG_BASE      + 0x0000000c)
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_RMSK                                                                  0x101
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_ADDR, HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_RMSK)
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_ADDR,m,v,HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_IN)
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_DSA_AGGR_SAFE_OVERRIDE_VALUE_BMSK                                     0x100
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_DSA_AGGR_SAFE_OVERRIDE_VALUE_SHFT                                       0x8
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_DSA_AGGR_SAFE_OVERRIDE_SEL_BMSK                                         0x1
#define HWIO_DSA_DSA_AGGR_SAFE_OVERRIDE_CNTRL_DSA_AGGR_SAFE_OVERRIDE_SEL_SHFT                                         0x0

#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00000020)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_RMSK                                                             0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_IN          \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_ADDR, HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_RMSK)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_ADDR, m)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_OUT(v)      \
        out_dword(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_ADDR,v)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_ADDR,m,v,HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_IN)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_PRE_STALL_TIMEOUT_CNT_URG_0_BMSK                                 0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_0_PRE_STALL_TIMEOUT_CNT_URG_0_SHFT                                     0x0

#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00000024)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_RMSK                                                             0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_IN          \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_ADDR, HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_RMSK)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_ADDR, m)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_OUT(v)      \
        out_dword(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_ADDR,v)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_ADDR,m,v,HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_IN)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_PRE_STALL_TIMEOUT_CNT_URG_1_BMSK                                 0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_1_PRE_STALL_TIMEOUT_CNT_URG_1_SHFT                                     0x0

#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00000028)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_RMSK                                                             0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_IN          \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_ADDR, HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_RMSK)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_ADDR, m)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_OUT(v)      \
        out_dword(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_ADDR,v)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_ADDR,m,v,HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_IN)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_PRE_STALL_TIMEOUT_CNT_URG_2_BMSK                                 0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_2_PRE_STALL_TIMEOUT_CNT_URG_2_SHFT                                     0x0

#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_ADDR                                                          (DSA_DSA_REG_BASE      + 0x0000002c)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_RMSK                                                             0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_IN          \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_ADDR, HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_RMSK)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_ADDR, m)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_OUT(v)      \
        out_dword(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_ADDR,v)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_ADDR,m,v,HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_IN)
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_PRE_STALL_TIMEOUT_CNT_URG_3_BMSK                                 0xfffff
#define HWIO_DSA_DSA_PRE_STALL_TIMEOUT_CNT_URG_3_PRE_STALL_TIMEOUT_CNT_URG_3_SHFT                                     0x0

#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_ADDR                                                         (DSA_DSA_REG_BASE      + 0x00000030)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_RMSK                                                            0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_IN          \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_ADDR, HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_RMSK)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_ADDR, m)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_OUT(v)      \
        out_dword(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_ADDR,v)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_ADDR,m,v,HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_IN)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_POST_STALL_TIMEOUT_CNT_URG_0_BMSK                               0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_0_POST_STALL_TIMEOUT_CNT_URG_0_SHFT                                   0x0

#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_ADDR                                                         (DSA_DSA_REG_BASE      + 0x00000034)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_RMSK                                                            0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_IN          \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_ADDR, HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_RMSK)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_ADDR, m)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_OUT(v)      \
        out_dword(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_ADDR,v)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_ADDR,m,v,HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_IN)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_POST_STALL_TIMEOUT_CNT_URG_1_BMSK                               0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_1_POST_STALL_TIMEOUT_CNT_URG_1_SHFT                                   0x0

#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_ADDR                                                         (DSA_DSA_REG_BASE      + 0x00000038)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_RMSK                                                            0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_IN          \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_ADDR, HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_RMSK)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_ADDR, m)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_OUT(v)      \
        out_dword(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_ADDR,v)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_ADDR,m,v,HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_IN)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_POST_STALL_TIMEOUT_CNT_URG_2_BMSK                               0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_2_POST_STALL_TIMEOUT_CNT_URG_2_SHFT                                   0x0

#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_ADDR                                                         (DSA_DSA_REG_BASE      + 0x0000003c)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_RMSK                                                            0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_IN          \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_ADDR, HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_RMSK)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_ADDR, m)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_OUT(v)      \
        out_dword(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_ADDR,v)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_ADDR,m,v,HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_IN)
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_POST_STALL_TIMEOUT_CNT_URG_3_BMSK                               0xfffff
#define HWIO_DSA_DSA_POST_STALL_TIMEOUT_CNT_URG_3_POST_STALL_TIMEOUT_CNT_URG_3_SHFT                                   0x0

#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00000050)
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_RMSK                                                              0x1103
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_IN          \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_ADDR, HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_RMSK)
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_ADDR, m)
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_ADDR,v)
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_ADDR,m,v,HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_IN)
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_WDW_TERM_BY_SAFE_BMSK                                        0x1000
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_WDW_TERM_BY_SAFE_SHFT                                           0xc
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_PRE_WDW_OVERLAP_PRIORITY_BMSK                                 0x100
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_PRE_WDW_OVERLAP_PRIORITY_SHFT                                   0x8
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_POST_WDW_OVERLAP_PRIORITY_BMSK                                  0x3
#define HWIO_DSA_DSA_POST_STALL_WDW_OVERLAP_CNTL_POST_POST_WDW_OVERLAP_PRIORITY_SHFT                                  0x0

#define HWIO_DSA_DSAT_REVISION_ID_ADDR                                                                         (DSA_DSA_REG_BASE      + 0x00000060)
#define HWIO_DSA_DSAT_REVISION_ID_RMSK                                                                         0xffffffff
#define HWIO_DSA_DSAT_REVISION_ID_IN          \
        in_dword_masked(HWIO_DSA_DSAT_REVISION_ID_ADDR, HWIO_DSA_DSAT_REVISION_ID_RMSK)
#define HWIO_DSA_DSAT_REVISION_ID_INM(m)      \
        in_dword_masked(HWIO_DSA_DSAT_REVISION_ID_ADDR, m)
#define HWIO_DSA_DSAT_REVISION_ID_MAJOR_BMSK                                                                   0xf0000000
#define HWIO_DSA_DSAT_REVISION_ID_MAJOR_SHFT                                                                         0x1c
#define HWIO_DSA_DSAT_REVISION_ID_MINOR_BMSK                                                                    0xfff0000
#define HWIO_DSA_DSAT_REVISION_ID_MINOR_SHFT                                                                         0x10
#define HWIO_DSA_DSAT_REVISION_ID_STEP_BMSK                                                                        0xffff
#define HWIO_DSA_DSAT_REVISION_ID_STEP_SHFT                                                                           0x0

#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_ADDR                                                                 (DSA_DSA_REG_BASE      + 0x00000064)
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_RMSK                                                                 0xffffffff
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_IN          \
        in_dword_masked(HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_ADDR, HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_RMSK)
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_ADDR, m)
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_ADDR,v)
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_ADDR,m,v,HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_IN)
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_31_BMSK                                                0x80000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_31_SHFT                                                      0x1f
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_30_BMSK                                                0x40000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_30_SHFT                                                      0x1e
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_29_BMSK                                                0x20000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_29_SHFT                                                      0x1d
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_28_BMSK                                                0x10000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_28_SHFT                                                      0x1c
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_27_BMSK                                                 0x8000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_27_SHFT                                                      0x1b
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_26_BMSK                                                 0x4000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_26_SHFT                                                      0x1a
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_25_BMSK                                                 0x2000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_25_SHFT                                                      0x19
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_24_BMSK                                                 0x1000000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_24_SHFT                                                      0x18
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_23_BMSK                                                  0x800000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_23_SHFT                                                      0x17
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_22_BMSK                                                  0x400000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_22_SHFT                                                      0x16
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_21_BMSK                                                  0x200000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_21_SHFT                                                      0x15
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_20_BMSK                                                  0x100000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_20_SHFT                                                      0x14
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_19_BMSK                                                   0x80000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_19_SHFT                                                      0x13
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_18_BMSK                                                   0x40000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_18_SHFT                                                      0x12
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_17_BMSK                                                   0x20000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_17_SHFT                                                      0x11
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_16_BMSK                                                   0x10000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_16_SHFT                                                      0x10
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_15_BMSK                                                    0x8000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_15_SHFT                                                       0xf
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_14_BMSK                                                    0x4000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_14_SHFT                                                       0xe
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_13_BMSK                                                    0x2000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_13_SHFT                                                       0xd
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_12_BMSK                                                    0x1000
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_12_SHFT                                                       0xc
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_11_BMSK                                                     0x800
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_11_SHFT                                                       0xb
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_10_BMSK                                                     0x400
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_10_SHFT                                                       0xa
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_9_BMSK                                                      0x200
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_9_SHFT                                                        0x9
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_8_BMSK                                                      0x100
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_8_SHFT                                                        0x8
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_7_BMSK                                                       0x80
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_7_SHFT                                                        0x7
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_6_BMSK                                                       0x40
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_6_SHFT                                                        0x6
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_5_BMSK                                                       0x20
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_5_SHFT                                                        0x5
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_4_BMSK                                                       0x10
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_4_SHFT                                                        0x4
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_3_BMSK                                                        0x8
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_3_SHFT                                                        0x3
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_2_BMSK                                                        0x4
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_2_SHFT                                                        0x2
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_1_BMSK                                                        0x2
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_1_SHFT                                                        0x1
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_0_BMSK                                                        0x1
#define HWIO_DSA_DSA_SECURE_SPARE_REGS_V2_SPARE_REG_BIT_0_SHFT                                                        0x0

#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_ADDR                                                                   (DSA_DSA_REG_BASE      + 0x00001000)
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_RMSK                                                                         0x1f
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_ADDR, HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_RMSK)
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_ADDR,m,v,HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_IN)
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DSA_DEBUG_CGC_EN_BMSK                                                        0x10
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DSA_DEBUG_CGC_EN_SHFT                                                         0x4
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_PERF_CNTR_CGC_EN_BMSK                                                         0x8
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_PERF_CNTR_CGC_EN_SHFT                                                         0x3
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DSA_OUTPUT_CGC_BYPASS_BMSK                                                    0x4
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DSA_OUTPUT_CGC_BYPASS_SHFT                                                    0x2
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DT_CGC_BYPASS_BMSK                                                            0x2
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DT_CGC_BYPASS_SHFT                                                            0x1
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DANGER_SAFE_CGC_BYPASS_BMSK                                                   0x1
#define HWIO_DSA_DSA_CORE_CLK_CGC_CNTRL_DANGER_SAFE_CGC_BYPASS_SHFT                                                   0x0

#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_ADDR                                                               (DSA_DSA_REG_BASE      + 0x00001004)
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_RMSK                                                                      0x7
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_ADDR, HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_RMSK)
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_ADDR, m)
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_ADDR,v)
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_ADDR,m,v,HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_IN)
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_DT_AGGR_MAX_SKEW_BMSK                                                     0x7
#define HWIO_DSA_DSA_DT_AGGR_INPUT_MAX_SKEW_DT_AGGR_MAX_SKEW_SHFT                                                     0x0

#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_ADDR                                                              (DSA_DSA_REG_BASE      + 0x00001008)
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_RMSK                                                              0x70070707
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_IN          \
        in_dword_masked(HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_ADDR, HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_RMSK)
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_ADDR, m)
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_OUT(v)      \
        out_dword(HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_ADDR,v)
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_ADDR,m,v,HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_IN)
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_DSP_MAX_SKEW_BMSK                                                 0x70000000
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_DSP_MAX_SKEW_SHFT                                                       0x1c
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_VENUS_DANGER_MAX_SKEW_BMSK                                           0x70000
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_VENUS_DANGER_MAX_SKEW_SHFT                                              0x10
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_MDP_DANGER_MAX_SKEW_BMSK                                               0x700
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_MDP_DANGER_MAX_SKEW_SHFT                                                 0x8
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_VFE_DANGER_MAX_SKEW_BMSK                                                 0x7
#define HWIO_DSA_DSA_RT_CLNT_DANGER_MAX_SKEW_VFE_DANGER_MAX_SKEW_SHFT                                                 0x0

#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_ADDR(n)                                                          (DSA_DSA_REG_BASE      + 0x00001020 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_RMSK                                                                  0x117
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_MAXn                                                                      9
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_ADDR(n), HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_RMSK)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_INI(n))
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_DANGER_AGGR_ALGO_SEL_BMSK                                             0x100
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_DANGER_AGGR_ALGO_SEL_SHFT                                               0x8
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_DSP_EN_BMSK                                                            0x10
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_DSP_EN_SHFT                                                             0x4
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_VENUS_EN_BMSK                                                           0x4
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_VENUS_EN_SHFT                                                           0x2
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_MDP_EN_BMSK                                                             0x2
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_MDP_EN_SHFT                                                             0x1
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_VFE_EN_BMSK                                                             0x1
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_CNTRL_VFE_EN_SHFT                                                             0x0

#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00001070)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_RMSK                                                                     0x1f7
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_ADDR, HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_RMSK)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_ADDR,m,v,HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_IN)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DANGER_AGGR_ALGO_SEL_BMSK                                                0x100
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DANGER_AGGR_ALGO_SEL_SHFT                                                  0x8
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_3_EN_BMSK                                                             0x80
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_3_EN_SHFT                                                              0x7
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_2_EN_BMSK                                                             0x40
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_2_EN_SHFT                                                              0x6
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_1_EN_BMSK                                                             0x20
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_1_EN_SHFT                                                              0x5
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_0_EN_BMSK                                                             0x10
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_DSP_0_EN_SHFT                                                              0x4
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_VENUS_EN_BMSK                                                              0x4
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_VENUS_EN_SHFT                                                              0x2
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_MDP_EN_BMSK                                                                0x2
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_MDP_EN_SHFT                                                                0x1
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_VFE_EN_BMSK                                                                0x1
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_CNTRL_VFE_EN_SHFT                                                                0x0

#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_ADDR                                                  (DSA_DSA_REG_BASE      + 0x00001080)
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_RMSK                                                         0x1
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_ADDR, HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_RMSK)
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_ADDR, m)
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_ADDR,v)
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_ADDR,m,v,HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_IN)
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_BMSK                     0x1
#define HWIO_DSA_DSA_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_DSP_HPM_RT_CLNT_CONTRIBUTE_OVERRIDE_SHFT                     0x0

#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00001090)
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_RMSK                                                                       0x1
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_IN          \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_CNTR_RESET_ADDR, HWIO_DSA_DSA_STATISTICS_CNTR_RESET_RMSK)
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_CNTR_RESET_ADDR, m)
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_OUT(v)      \
        out_dword(HWIO_DSA_DSA_STATISTICS_CNTR_RESET_ADDR,v)
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_STATISTICS_CNTR_RESET_ADDR,m,v,HWIO_DSA_DSA_STATISTICS_CNTR_RESET_IN)
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_STATISTICS_CNTR_RESET_BMSK                                                 0x1
#define HWIO_DSA_DSA_STATISTICS_CNTR_RESET_STATISTICS_CNTR_RESET_SHFT                                                 0x0

#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_ADDR                                                         (DSA_DSA_REG_BASE      + 0x00001094)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_RMSK                                                         0xffffffff
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_IN          \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_ADDR, HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_RMSK)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_ADDR, m)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_OUT(v)      \
        out_dword(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_ADDR,v)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_ADDR,m,v,HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_IN)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_STATISTICS_DURATION_CNTR_THR_BMSK                            0xffffffff
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_THR_STATISTICS_DURATION_CNTR_THR_SHFT                                   0x0

#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_ADDR                                                    (DSA_DSA_REG_BASE      + 0x00001098)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_RMSK                                                    0xffffffff
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_ADDR, HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_STATISTICS_DURATION_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_STATISTICS_DURATION_CNTR_READBACK_STATISTICS_DURATION_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n)                                                  (DSA_DSA_REG_BASE      + 0x000010a0 + 0x4 * (n))
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_RMSK                                                     0xffffffff
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_MAXn                                                              3
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), mask)
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_VFE_DANGER_LEVEL_CNTR_READBACK_BMSK                      0xffffffff
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_n_CNTR_READBACK_VFE_DANGER_LEVEL_CNTR_READBACK_SHFT                             0x0

#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR                                                  (DSA_DSA_REG_BASE      + 0x000010b0)
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK                                                  0xffffffff
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_BMSK              0xffffffff
#define HWIO_DSA_DSA_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_VFE_DANGER_LEVEL_GT_0_CNTR_READBACK_SHFT                     0x0

#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR                                                    (DSA_DSA_REG_BASE      + 0x000010b4)
#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK                                                    0xffffffff
#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_VFE_SAFE_LEVEL_LT_1_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n)                                                  (DSA_DSA_REG_BASE      + 0x000010c0 + 0x4 * (n))
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_RMSK                                                     0xffffffff
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_MAXn                                                              3
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), mask)
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_MDP_DANGER_LEVEL_CNTR_READBACK_BMSK                      0xffffffff
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_n_CNTR_READBACK_MDP_DANGER_LEVEL_CNTR_READBACK_SHFT                             0x0

#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR                                                  (DSA_DSA_REG_BASE      + 0x000010d0)
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK                                                  0xffffffff
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_BMSK              0xffffffff
#define HWIO_DSA_DSA_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_MDP_DANGER_LEVEL_GT_0_CNTR_READBACK_SHFT                     0x0

#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR                                                    (DSA_DSA_REG_BASE      + 0x000010d4)
#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK                                                    0xffffffff
#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_MDP_SAFE_LEVEL_LT_1_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n)                                                (DSA_DSA_REG_BASE      + 0x000010e0 + 0x4 * (n))
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_RMSK                                                   0xffffffff
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_MAXn                                                            3
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), mask)
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_VENUS_DANGER_LEVEL_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_n_CNTR_READBACK_VENUS_DANGER_LEVEL_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR                                                (DSA_DSA_REG_BASE      + 0x000010f0)
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK                                                0xffffffff
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_BMSK          0xffffffff
#define HWIO_DSA_DSA_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_VENUS_DANGER_LEVEL_GT_0_CNTR_READBACK_SHFT                 0x0

#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR                                                  (DSA_DSA_REG_BASE      + 0x000010f4)
#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK                                                  0xffffffff
#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_BMSK              0xffffffff
#define HWIO_DSA_DSA_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_VENUS_SAFE_LEVEL_LT_1_CNTR_READBACK_SHFT                     0x0

#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n)                                                (DSA_DSA_REG_BASE      + 0x00001100 + 0x4 * (n))
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_RMSK                                                   0xffffffff
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_MAXn                                                            3
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), mask)
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_MODEM_DANGER_LEVEL_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_n_CNTR_READBACK_MODEM_DANGER_LEVEL_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR                                                (DSA_DSA_REG_BASE      + 0x00001110)
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK                                                0xffffffff
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_BMSK          0xffffffff
#define HWIO_DSA_DSA_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_MODEM_DANGER_LEVEL_GT_0_CNTR_READBACK_SHFT                 0x0

#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR                                                  (DSA_DSA_REG_BASE      + 0x00001114)
#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK                                                  0xffffffff
#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_BMSK              0xffffffff
#define HWIO_DSA_DSA_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_MODEM_SAFE_LEVEL_LT_1_CNTR_READBACK_SHFT                     0x0

#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n)                                                  (DSA_DSA_REG_BASE      + 0x00001120 + 0x4 * (n))
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_RMSK                                                     0xffffffff
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_MAXn                                                              3
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_ADDR(n), mask)
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_DSP_DANGER_LEVEL_CNTR_READBACK_BMSK                      0xffffffff
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_n_CNTR_READBACK_DSP_DANGER_LEVEL_CNTR_READBACK_SHFT                             0x0

#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR                                                  (DSA_DSA_REG_BASE      + 0x00001130)
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK                                                  0xffffffff
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_BMSK              0xffffffff
#define HWIO_DSA_DSA_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_DSP_DANGER_LEVEL_GT_0_CNTR_READBACK_SHFT                     0x0

#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR                                                    (DSA_DSA_REG_BASE      + 0x00001134)
#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK                                                    0xffffffff
#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_BMSK                  0xffffffff
#define HWIO_DSA_DSA_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_DSP_SAFE_LEVEL_LT_1_CNTR_READBACK_SHFT                         0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_ADDR(n)                                                   (DSA_DSA_REG_BASE      + 0x00001150 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_RMSK                                                        0xffffff
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_MAXn                                                              10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_7_BMSK                                0xe00000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_7_SHFT                                    0x15
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_6_BMSK                                0x1c0000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_6_SHFT                                    0x12
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_5_BMSK                                 0x38000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_5_SHFT                                     0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_4_BMSK                                  0x7000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_4_SHFT                                     0xc
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_3_BMSK                                   0xe00
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_3_SHFT                                     0x9
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_2_BMSK                                   0x1c0
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_2_SHFT                                     0x6
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_1_BMSK                                    0x38
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_1_SHFT                                     0x3
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_0_BMSK                                     0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00_THROTTLE_LEVEL_OUTPUT_0_SHFT                                     0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_ADDR(n)                                                   (DSA_DSA_REG_BASE      + 0x000011a0 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_RMSK                                                           0xfff
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_MAXn                                                              10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_11_BMSK                                  0xe00
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_11_SHFT                                    0x9
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_10_BMSK                                  0x1c0
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_10_SHFT                                    0x6
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_9_BMSK                                    0x38
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_9_SHFT                                     0x3
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_8_BMSK                                     0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01_THROTTLE_LEVEL_OUTPUT_8_SHFT                                     0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_ADDR(n)                                                   (DSA_DSA_REG_BASE      + 0x000011e0 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_RMSK                                                             0x1
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_MAXn                                                              10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_THROTTLE_LEVEL_EN_BMSK                                           0x1
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN_THROTTLE_LEVEL_EN_SHFT                                           0x0

#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_ADDR                                                        (DSA_DSA_REG_BASE      + 0x00001210)
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_RMSK                                                               0x7
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_ADDR, HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_RMSK)
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_ADDR, m)
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_ADDR,v)
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_ADDR,m,v,HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_IN)
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_DSP_HPM_THROTTLE_LEVEL_OUTPUT_BMSK                                 0x7
#define HWIO_DSA_DSA_DSP_HPM_THROTTLE_LEVEL_OUTPUT_DSP_HPM_THROTTLE_LEVEL_OUTPUT_SHFT                                 0x0

#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_ADDR                                                             (DSA_DSA_REG_BASE      + 0x00001214)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_RMSK                                                               0xffffff
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_ADDR, HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_RMSK)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_ADDR, m)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_ADDR,v)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_ADDR,m,v,HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_IN)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_11_BMSK                                       0xc00000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_11_SHFT                                           0x16
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_10_BMSK                                       0x300000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_10_SHFT                                           0x14
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_9_BMSK                                         0xc0000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_9_SHFT                                            0x12
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_8_BMSK                                         0x30000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_8_SHFT                                            0x10
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_7_BMSK                                          0xc000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_7_SHFT                                             0xe
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_6_BMSK                                          0x3000
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_6_SHFT                                             0xc
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_5_BMSK                                           0xc00
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_5_SHFT                                             0xa
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_4_BMSK                                           0x300
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_4_SHFT                                             0x8
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_3_BMSK                                            0xc0
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_3_SHFT                                             0x6
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_2_BMSK                                            0x30
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_2_SHFT                                             0x4
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_1_BMSK                                             0xc
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_1_SHFT                                             0x2
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_0_BMSK                                             0x3
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_SYSTEM_DANGER_OUTPUT_0_SHFT                                             0x0

#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00001218)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_RMSK                                                                 0x1
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_ADDR, HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_RMSK)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_ADDR, m)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_ADDR,v)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_ADDR,m,v,HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_IN)
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_SYSTEM_DANGER_OUTPUT_EN_BMSK                                         0x1
#define HWIO_DSA_DSA_DSP_SYSTEM_DANGER_OUTPUT_EN_SYSTEM_DANGER_OUTPUT_EN_SHFT                                         0x0

#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_ADDR                                                                (DSA_DSA_REG_BASE      + 0x0000121c)
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_RMSK                                                                       0x7
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_ADDR, HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_RMSK)
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_ADDR, m)
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_ADDR,v)
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_ADDR,m,v,HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_IN)
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_DSP_PRIORITY_MODE_FLIP_BMSK                                                0x4
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_DSP_PRIORITY_MODE_FLIP_SHFT                                                0x2
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_DSP_PRIORITY_MODE_SEL_BMSK                                                 0x3
#define HWIO_DSA_DSA_DSP_PRIORITY_MODE_SEL_DSP_PRIORITY_MODE_SEL_SHFT                                                 0x0

#define HWIO_DSA_DSA_INTERRUPT_MASK_ADDR                                                                       (DSA_DSA_REG_BASE      + 0x00001220)
#define HWIO_DSA_DSA_INTERRUPT_MASK_RMSK                                                                              0xf
#define HWIO_DSA_DSA_INTERRUPT_MASK_IN          \
        in_dword_masked(HWIO_DSA_DSA_INTERRUPT_MASK_ADDR, HWIO_DSA_DSA_INTERRUPT_MASK_RMSK)
#define HWIO_DSA_DSA_INTERRUPT_MASK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_INTERRUPT_MASK_ADDR, m)
#define HWIO_DSA_DSA_INTERRUPT_MASK_OUT(v)      \
        out_dword(HWIO_DSA_DSA_INTERRUPT_MASK_ADDR,v)
#define HWIO_DSA_DSA_INTERRUPT_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_INTERRUPT_MASK_ADDR,m,v,HWIO_DSA_DSA_INTERRUPT_MASK_IN)
#define HWIO_DSA_DSA_INTERRUPT_MASK_TPDM_BC_OFSAT_INT_MASK_BMSK                                                       0x8
#define HWIO_DSA_DSA_INTERRUPT_MASK_TPDM_BC_OFSAT_INT_MASK_SHFT                                                       0x3
#define HWIO_DSA_DSA_INTERRUPT_MASK_POST_STALL_TIMEOUT_INT_MASK_BMSK                                                  0x4
#define HWIO_DSA_DSA_INTERRUPT_MASK_POST_STALL_TIMEOUT_INT_MASK_SHFT                                                  0x2
#define HWIO_DSA_DSA_INTERRUPT_MASK_PRE_STALL_TIMEOUT_INT_MASK_BMSK                                                   0x2
#define HWIO_DSA_DSA_INTERRUPT_MASK_PRE_STALL_TIMEOUT_INT_MASK_SHFT                                                   0x1
#define HWIO_DSA_DSA_INTERRUPT_MASK_STATISTICS_DURATION_CNTR_SATURATE_INT_MASK_BMSK                                   0x1
#define HWIO_DSA_DSA_INTERRUPT_MASK_STATISTICS_DURATION_CNTR_SATURATE_INT_MASK_SHFT                                   0x0

#define HWIO_DSA_DSA_INTERRUPT_CLR_ADDR                                                                        (DSA_DSA_REG_BASE      + 0x00001224)
#define HWIO_DSA_DSA_INTERRUPT_CLR_RMSK                                                                               0x7
#define HWIO_DSA_DSA_INTERRUPT_CLR_OUT(v)      \
        out_dword(HWIO_DSA_DSA_INTERRUPT_CLR_ADDR,v)
#define HWIO_DSA_DSA_INTERRUPT_CLR_POST_STALL_TIMEOUT_INT_CLR_BMSK                                                    0x4
#define HWIO_DSA_DSA_INTERRUPT_CLR_POST_STALL_TIMEOUT_INT_CLR_SHFT                                                    0x2
#define HWIO_DSA_DSA_INTERRUPT_CLR_PRE_STALL_TIMEOUT_INT_CLR_BMSK                                                     0x2
#define HWIO_DSA_DSA_INTERRUPT_CLR_PRE_STALL_TIMEOUT_INT_CLR_SHFT                                                     0x1
#define HWIO_DSA_DSA_INTERRUPT_CLR_STATISTICS_DURATION_CNTR_SATURATE_INT_CLR_BMSK                                     0x1
#define HWIO_DSA_DSA_INTERRUPT_CLR_STATISTICS_DURATION_CNTR_SATURATE_INT_CLR_SHFT                                     0x0

#define HWIO_DSA_DSA_INTERRUPT_STATUS_ADDR                                                                     (DSA_DSA_REG_BASE      + 0x00001228)
#define HWIO_DSA_DSA_INTERRUPT_STATUS_RMSK                                                                            0xf
#define HWIO_DSA_DSA_INTERRUPT_STATUS_IN          \
        in_dword_masked(HWIO_DSA_DSA_INTERRUPT_STATUS_ADDR, HWIO_DSA_DSA_INTERRUPT_STATUS_RMSK)
#define HWIO_DSA_DSA_INTERRUPT_STATUS_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_INTERRUPT_STATUS_ADDR, m)
#define HWIO_DSA_DSA_INTERRUPT_STATUS_TPDM_BC_OFSAT_INT_STATUS_BMSK                                                   0x8
#define HWIO_DSA_DSA_INTERRUPT_STATUS_TPDM_BC_OFSAT_INT_STATUS_SHFT                                                   0x3
#define HWIO_DSA_DSA_INTERRUPT_STATUS_POST_STALL_TIMEOUT_INT_STATUS_BMSK                                              0x4
#define HWIO_DSA_DSA_INTERRUPT_STATUS_POST_STALL_TIMEOUT_INT_STATUS_SHFT                                              0x2
#define HWIO_DSA_DSA_INTERRUPT_STATUS_PRE_STALL_TIMEOUT_INT_STATUS_BMSK                                               0x2
#define HWIO_DSA_DSA_INTERRUPT_STATUS_PRE_STALL_TIMEOUT_INT_STATUS_SHFT                                               0x1
#define HWIO_DSA_DSA_INTERRUPT_STATUS_STATISTICS_DURATION_CNTR_SATURATE_INT_STATUS_BMSK                               0x1
#define HWIO_DSA_DSA_INTERRUPT_STATUS_STATISTICS_DURATION_CNTR_SATURATE_INT_STATUS_SHFT                               0x0

#define HWIO_DSA_DSA_DT_STATUS_ADDR                                                                            (DSA_DSA_REG_BASE      + 0x00001230)
#define HWIO_DSA_DSA_DT_STATUS_RMSK                                                                                   0x7
#define HWIO_DSA_DSA_DT_STATUS_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_STATUS_ADDR, HWIO_DSA_DSA_DT_STATUS_RMSK)
#define HWIO_DSA_DSA_DT_STATUS_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_STATUS_ADDR, m)
#define HWIO_DSA_DSA_DT_STATUS_DT_POST_STALL_ACTIVE_READBACK_BMSK                                                     0x4
#define HWIO_DSA_DSA_DT_STATUS_DT_POST_STALL_ACTIVE_READBACK_SHFT                                                     0x2
#define HWIO_DSA_DSA_DT_STATUS_DT_PRE_STALL_ACTIVE_READBACK_BMSK                                                      0x2
#define HWIO_DSA_DSA_DT_STATUS_DT_PRE_STALL_ACTIVE_READBACK_SHFT                                                      0x1
#define HWIO_DSA_DSA_DT_STATUS_DT_ACK_BYPASS_EN_READBACK_BMSK                                                         0x1
#define HWIO_DSA_DSA_DT_STATUS_DT_ACK_BYPASS_EN_READBACK_SHFT                                                         0x0

#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_ADDR                                                           (DSA_DSA_REG_BASE      + 0x00001234)
#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_RMSK                                                              0xfffff
#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_ADDR, HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_DT_PRE_STALL_CNTR_READBACK_BMSK                                   0xfffff
#define HWIO_DSA_DSA_DT_PRE_STALL_CNTR_READBACK_DT_PRE_STALL_CNTR_READBACK_SHFT                                       0x0

#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00001238)
#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_RMSK                                                             0xfffff
#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_ADDR, HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_RMSK)
#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_ADDR, m)
#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_DT_POST_STALL_CNTR_READBACK_BMSK                                 0xfffff
#define HWIO_DSA_DSA_DT_POST_STALL_CNTR_READBACK_DT_POST_STALL_CNTR_READBACK_SHFT                                     0x0

#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_ADDR                                                                  (DSA_DSA_REG_BASE      + 0x0000123c)
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_RMSK                                                                        0x1f
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_IN          \
        in_dword_masked(HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_ADDR, HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_RMSK)
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_ADDR, m)
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DSA_DEBUG_CORE_CLK_CGC_EN_READBACK_BMSK                                     0x10
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DSA_DEBUG_CORE_CLK_CGC_EN_READBACK_SHFT                                      0x4
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_PERF_CNTR_CORE_CLK_CGC_EN_READBACK_BMSK                                      0x8
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_PERF_CNTR_CORE_CLK_CGC_EN_READBACK_SHFT                                      0x3
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DSA_OUTPUT_CORE_CLK_CGC_EN_READBACK_BMSK                                     0x4
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DSA_OUTPUT_CORE_CLK_CGC_EN_READBACK_SHFT                                     0x2
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DT_CORE_CLK_CGC_EN_READBACK_BMSK                                             0x2
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DT_CORE_CLK_CGC_EN_READBACK_SHFT                                             0x1
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DANGER_SAFE_CORE_CLK_CGC_EN_READBACK_BMSK                                    0x1
#define HWIO_DSA_DSA_CORE_CLK_CGC_STATUS_DANGER_SAFE_CORE_CLK_CGC_EN_READBACK_SHFT                                    0x0

#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_ADDR                                                                       (DSA_DSA_REG_BASE      + 0x00001240)
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_RMSK                                                                       0x80000f01
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_IN          \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_CTLR_ADDR, HWIO_DSA_DSA_DEBUG_BUS_CTLR_RMSK)
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_CTLR_ADDR, m)
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DEBUG_BUS_CTLR_ADDR,v)
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DEBUG_BUS_CTLR_ADDR,m,v,HWIO_DSA_DSA_DEBUG_BUS_CTLR_IN)
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DEBUG_BUS_SIGNATURE_READBACK_BMSK                                          0x80000000
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DEBUG_BUS_SIGNATURE_READBACK_SHFT                                                0x1f
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DSA_DEBUG_BUS_SEL_BMSK                                                          0xf00
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DSA_DEBUG_BUS_SEL_SHFT                                                            0x8
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DSA_DEBUG_BUS_EN_BMSK                                                             0x1
#define HWIO_DSA_DSA_DEBUG_BUS_CTLR_DSA_DEBUG_BUS_EN_SHFT                                                             0x0

#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_ADDR                                                                  (DSA_DSA_REG_BASE      + 0x00001248)
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_RMSK                                                                      0xffff
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_IN          \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_ADDR, HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_RMSK)
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_ADDR, m)
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_ADDR,v)
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_ADDR,m,v,HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_IN)
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_DEBUG_BUS_SIGNATURE_BMSK                                                  0xffff
#define HWIO_DSA_DSA_DEBUG_BUS_SIGNATURE_DEBUG_BUS_SIGNATURE_SHFT                                                     0x0

#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_ADDR                                                                   (DSA_DSA_REG_BASE      + 0x0000124c)
#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_RMSK                                                                   0xffffffff
#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_READBACK_ADDR, HWIO_DSA_DSA_DEBUG_BUS_READBACK_RMSK)
#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DEBUG_BUS_READBACK_ADDR, m)
#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_DSA_DEBUG_BUS_READBACK_BMSK                                            0xffffffff
#define HWIO_DSA_DSA_DEBUG_BUS_READBACK_DSA_DEBUG_BUS_READBACK_SHFT                                                   0x0

#define HWIO_DSA_DSA_HW_EVENTS_READBACK_ADDR                                                                   (DSA_DSA_REG_BASE      + 0x0000125c)
#define HWIO_DSA_DSA_HW_EVENTS_READBACK_RMSK                                                                   0xffffffff
#define HWIO_DSA_DSA_HW_EVENTS_READBACK_IN          \
        in_dword_masked(HWIO_DSA_DSA_HW_EVENTS_READBACK_ADDR, HWIO_DSA_DSA_HW_EVENTS_READBACK_RMSK)
#define HWIO_DSA_DSA_HW_EVENTS_READBACK_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_HW_EVENTS_READBACK_ADDR, m)
#define HWIO_DSA_DSA_HW_EVENTS_READBACK_DSA_HW_EVENTS_READBACK_BMSK                                            0xffffffff
#define HWIO_DSA_DSA_HW_EVENTS_READBACK_DSA_HW_EVENTS_READBACK_SHFT                                                   0x0

#define HWIO_DSA_DSA_STM_MUX_SEL_n_ADDR(n)                                                                     (DSA_DSA_REG_BASE      + 0x00001260 + 0x4 * (n))
#define HWIO_DSA_DSA_STM_MUX_SEL_n_RMSK                                                                        0x80000007
#define HWIO_DSA_DSA_STM_MUX_SEL_n_MAXn                                                                                31
#define HWIO_DSA_DSA_STM_MUX_SEL_n_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_STM_MUX_SEL_n_ADDR(n), HWIO_DSA_DSA_STM_MUX_SEL_n_RMSK)
#define HWIO_DSA_DSA_STM_MUX_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_STM_MUX_SEL_n_ADDR(n), mask)
#define HWIO_DSA_DSA_STM_MUX_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_STM_MUX_SEL_n_ADDR(n),val)
#define HWIO_DSA_DSA_STM_MUX_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_STM_MUX_SEL_n_ADDR(n),mask,val,HWIO_DSA_DSA_STM_MUX_SEL_n_INI(n))
#define HWIO_DSA_DSA_STM_MUX_SEL_n_EN_BMSK                                                                     0x80000000
#define HWIO_DSA_DSA_STM_MUX_SEL_n_EN_SHFT                                                                           0x1f
#define HWIO_DSA_DSA_STM_MUX_SEL_n_SEL_BMSK                                                                           0x7
#define HWIO_DSA_DSA_STM_MUX_SEL_n_SEL_SHFT                                                                           0x0

#define HWIO_DSA_DSA_SPARE_REGS_ADDR                                                                           (DSA_DSA_REG_BASE      + 0x00001300)
#define HWIO_DSA_DSA_SPARE_REGS_RMSK                                                                           0xffffffff
#define HWIO_DSA_DSA_SPARE_REGS_IN          \
        in_dword_masked(HWIO_DSA_DSA_SPARE_REGS_ADDR, HWIO_DSA_DSA_SPARE_REGS_RMSK)
#define HWIO_DSA_DSA_SPARE_REGS_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SPARE_REGS_ADDR, m)
#define HWIO_DSA_DSA_SPARE_REGS_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SPARE_REGS_ADDR,v)
#define HWIO_DSA_DSA_SPARE_REGS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SPARE_REGS_ADDR,m,v,HWIO_DSA_DSA_SPARE_REGS_IN)
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_31_BMSK                                                          0x80000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_31_SHFT                                                                0x1f
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_30_BMSK                                                          0x40000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_30_SHFT                                                                0x1e
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_29_BMSK                                                          0x20000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_29_SHFT                                                                0x1d
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_28_BMSK                                                          0x10000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_28_SHFT                                                                0x1c
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_27_BMSK                                                           0x8000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_27_SHFT                                                                0x1b
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_26_BMSK                                                           0x4000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_26_SHFT                                                                0x1a
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_25_BMSK                                                           0x2000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_25_SHFT                                                                0x19
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_24_BMSK                                                           0x1000000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_24_SHFT                                                                0x18
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_23_BMSK                                                            0x800000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_23_SHFT                                                                0x17
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_22_BMSK                                                            0x400000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_22_SHFT                                                                0x16
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_21_BMSK                                                            0x200000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_21_SHFT                                                                0x15
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_20_BMSK                                                            0x100000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_20_SHFT                                                                0x14
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_19_BMSK                                                             0x80000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_19_SHFT                                                                0x13
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_18_BMSK                                                             0x40000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_18_SHFT                                                                0x12
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_17_BMSK                                                             0x20000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_17_SHFT                                                                0x11
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_16_BMSK                                                             0x10000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_16_SHFT                                                                0x10
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_15_BMSK                                                              0x8000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_15_SHFT                                                                 0xf
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_14_BMSK                                                              0x4000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_14_SHFT                                                                 0xe
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_13_BMSK                                                              0x2000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_13_SHFT                                                                 0xd
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_12_BMSK                                                              0x1000
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_12_SHFT                                                                 0xc
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_11_BMSK                                                               0x800
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_11_SHFT                                                                 0xb
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_10_BMSK                                                               0x400
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_10_SHFT                                                                 0xa
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_9_BMSK                                                                0x200
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_9_SHFT                                                                  0x9
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_8_BMSK                                                                0x100
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_8_SHFT                                                                  0x8
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_7_BMSK                                                                 0x80
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_7_SHFT                                                                  0x7
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_6_BMSK                                                                 0x40
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_6_SHFT                                                                  0x6
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_5_BMSK                                                                 0x20
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_5_SHFT                                                                  0x5
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_4_BMSK                                                                 0x10
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_4_SHFT                                                                  0x4
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_3_BMSK                                                                  0x8
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_3_SHFT                                                                  0x3
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_2_BMSK                                                                  0x4
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_2_SHFT                                                                  0x2
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_1_BMSK                                                                  0x2
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_1_SHFT                                                                  0x1
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_0_BMSK                                                                  0x1
#define HWIO_DSA_DSA_SPARE_REGS_SPARE_REG_BIT_0_SHFT                                                                  0x0

#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00001320)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_RMSK                                                          0x3fffffff
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_IN          \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_ADDR, HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_RMSK)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_ADDR, m)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_OUT(v)      \
        out_dword(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_ADDR,v)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_ADDR,m,v,HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_IN)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_9_THROTTLE_LEVEL_THR_BMSK                                0x38000000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_9_THROTTLE_LEVEL_THR_SHFT                                      0x1b
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_8_THROTTLE_LEVEL_THR_BMSK                                 0x7000000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_8_THROTTLE_LEVEL_THR_SHFT                                      0x18
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_7_THROTTLE_LEVEL_THR_BMSK                                  0xe00000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_7_THROTTLE_LEVEL_THR_SHFT                                      0x15
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_6_THROTTLE_LEVEL_THR_BMSK                                  0x1c0000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_6_THROTTLE_LEVEL_THR_SHFT                                      0x12
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_5_THROTTLE_LEVEL_THR_BMSK                                   0x38000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_5_THROTTLE_LEVEL_THR_SHFT                                       0xf
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_4_THROTTLE_LEVEL_THR_BMSK                                    0x7000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_4_THROTTLE_LEVEL_THR_SHFT                                       0xc
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_3_THROTTLE_LEVEL_THR_BMSK                                     0xe00
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_3_THROTTLE_LEVEL_THR_SHFT                                       0x9
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_2_THROTTLE_LEVEL_THR_BMSK                                     0x1c0
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_2_THROTTLE_LEVEL_THR_SHFT                                       0x6
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_1_THROTTLE_LEVEL_THR_BMSK                                      0x38
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_1_THROTTLE_LEVEL_THR_SHFT                                       0x3
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_0_THROTTLE_LEVEL_THR_BMSK                                       0x7
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_00_CLNT_0_THROTTLE_LEVEL_THR_SHFT                                       0x0

#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00001324)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_RMSK                                                                 0x7
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_IN          \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_ADDR, HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_RMSK)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_ADDR, m)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_OUT(v)      \
        out_dword(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_ADDR,v)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_ADDR,m,v,HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_IN)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_CLNT_10_THROTTLE_LEVEL_THR_BMSK                                      0x7
#define HWIO_DSA_DSA_THROTTLE_LEVEL_THRESHOLD_01_CLNT_10_THROTTLE_LEVEL_THR_SHFT                                      0x0

#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00002000)
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_RMSK                                                                       0x7
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_IN          \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_ADDR, HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_RMSK)
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_ADDR, m)
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_OUT(v)      \
        out_dword(HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_ADDR,v)
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_ADDR,m,v,HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_IN)
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_MODEM_DANGER_MAX_SKEW_BMSK                                                 0x7
#define HWIO_DSA_DSA_MODEM_DANGER_MAX_SKEW_MODEM_DANGER_MAX_SKEW_SHFT                                                 0x0

#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_ADDR(n)                                                    (DSA_DSA_REG_BASE      + 0x00002020 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_RMSK                                                              0x1
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_MAXn                                                                9
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_ADDR(n), HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_RMSK)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_INI(n))
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_MODEM_EN_BMSK                                                     0x1
#define HWIO_DSA_DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL_MODEM_EN_SHFT                                                     0x0

#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_ADDR                                                          (DSA_DSA_REG_BASE      + 0x00002070)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_RMSK                                                                 0x1
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_ADDR, HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_RMSK)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_ADDR,m,v,HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_IN)
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_MODEM_EN_BMSK                                                        0x1
#define HWIO_DSA_DSA_DSP_DANGER_AGGR_MODEM_CNTRL_MODEM_EN_SHFT                                                        0x0

#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_ADDR                                                                  (DSA_DSA_REG_BASE      + 0x00003000)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_RMSK                                                                  0x8000001f
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_IN          \
        in_dword_masked(HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_ADDR, HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_RMSK)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_ADDR, m)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_ADDR,v)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_ADDR,m,v,HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_IN)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_SW_SAFE_OVERRIDE_VALUE_BMSK                                           0x80000000
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_SW_SAFE_OVERRIDE_VALUE_SHFT                                                 0x1f
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_MODEM_SAFE_OVERRIDE_VALUE_BMSK                                              0x10
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_MODEM_SAFE_OVERRIDE_VALUE_SHFT                                               0x4
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VENUS_SAFE_OVERRIDE_VALUE_BMSK                                               0x8
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VENUS_SAFE_OVERRIDE_VALUE_SHFT                                               0x3
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_MDP_SAFE_OVERRIDE_VALUE_BMSK                                                 0x4
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_MDP_SAFE_OVERRIDE_VALUE_SHFT                                                 0x2
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VFE1_SAFE_OVERRIDE_VALUE_BMSK                                                0x2
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VFE1_SAFE_OVERRIDE_VALUE_SHFT                                                0x1
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VFE0_SAFE_OVERRIDE_VALUE_BMSK                                                0x1
#define HWIO_DSA_DSA_SAFE_OVERRIDE_VALUE_VFE0_SAFE_OVERRIDE_VALUE_SHFT                                                0x0

#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_ADDR                                                                     (DSA_DSA_REG_BASE      + 0x00003004)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_RMSK                                                                     0x8000001f
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_SAFE_OVERRIDE_EN_ADDR, HWIO_DSA_DSA_SAFE_OVERRIDE_EN_RMSK)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SAFE_OVERRIDE_EN_ADDR, m)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SAFE_OVERRIDE_EN_ADDR,v)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SAFE_OVERRIDE_EN_ADDR,m,v,HWIO_DSA_DSA_SAFE_OVERRIDE_EN_IN)
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_SW_SAFE_OVERRIDE_EN_BMSK                                                 0x80000000
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_SW_SAFE_OVERRIDE_EN_SHFT                                                       0x1f
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_MODEM_SAFE_OVERRIDE_EN_BMSK                                                    0x10
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_MODEM_SAFE_OVERRIDE_EN_SHFT                                                     0x4
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VENUS_SAFE_OVERRIDE_EN_BMSK                                                     0x8
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VENUS_SAFE_OVERRIDE_EN_SHFT                                                     0x3
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_MDP_SAFE_OVERRIDE_EN_BMSK                                                       0x4
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_MDP_SAFE_OVERRIDE_EN_SHFT                                                       0x2
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VFE1_SAFE_OVERRIDE_EN_BMSK                                                      0x2
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VFE1_SAFE_OVERRIDE_EN_SHFT                                                      0x1
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VFE0_SAFE_OVERRIDE_EN_BMSK                                                      0x1
#define HWIO_DSA_DSA_SAFE_OVERRIDE_EN_VFE0_SAFE_OVERRIDE_EN_SHFT                                                      0x0

#define HWIO_DSA_DSA_SAFE_SW_ADDR                                                                              (DSA_DSA_REG_BASE      + 0x00003008)
#define HWIO_DSA_DSA_SAFE_SW_RMSK                                                                                     0x1
#define HWIO_DSA_DSA_SAFE_SW_IN          \
        in_dword_masked(HWIO_DSA_DSA_SAFE_SW_ADDR, HWIO_DSA_DSA_SAFE_SW_RMSK)
#define HWIO_DSA_DSA_SAFE_SW_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SAFE_SW_ADDR, m)
#define HWIO_DSA_DSA_SAFE_SW_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SAFE_SW_ADDR,v)
#define HWIO_DSA_DSA_SAFE_SW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SAFE_SW_ADDR,m,v,HWIO_DSA_DSA_SAFE_SW_IN)
#define HWIO_DSA_DSA_SAFE_SW_SW_SAFE_BMSK                                                                             0x1
#define HWIO_DSA_DSA_SAFE_SW_SW_SAFE_SHFT                                                                             0x0

#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_ADDR                                                          (DSA_DSA_REG_BASE      + 0x0000300c)
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_RMSK                                                               0x101
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_ADDR, HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_RMSK)
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_ADDR,m,v,HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_IN)
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_DSA_AGGR_SAFE_DT_OVERRIDE_VALUE_BMSK                               0x100
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_DSA_AGGR_SAFE_DT_OVERRIDE_VALUE_SHFT                                 0x8
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_DSA_AGGR_SAFE_DT_OVERRIDE_EN_BMSK                                    0x1
#define HWIO_DSA_DSA_AGGR_SAFE_DT_OVERRIDE_CNTRL_DSA_AGGR_SAFE_DT_OVERRIDE_EN_SHFT                                    0x0

#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_ADDR                                                       (DSA_DSA_REG_BASE      + 0x00003010)
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_RMSK                                                            0x101
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_ADDR, HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_RMSK)
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_ADDR,m,v,HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_IN)
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_DSA_AGGR_SAFE_TL_V2_OVERRIDE_VALUE_BMSK                         0x100
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_DSA_AGGR_SAFE_TL_V2_OVERRIDE_VALUE_SHFT                           0x8
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_DSA_AGGR_SAFE_TL_V2_OVERRIDE_EN_BMSK                              0x1
#define HWIO_DSA_DSA_AGGR_SAFE_TL_V2_OVERRIDE_CNTRL_DSA_AGGR_SAFE_TL_V2_OVERRIDE_EN_SHFT                              0x0

#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00003020)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_RMSK                                                                 0x3ffffff
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_IN          \
        in_dword_masked(HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_ADDR, HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_RMSK)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_ADDR, m)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_ADDR,v)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_ADDR,m,v,HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_IN)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_3_OTHER_DANGER_OVERRIDE_VALUE_BMSK                               0x3000000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_3_OTHER_DANGER_OVERRIDE_VALUE_SHFT                                    0x18
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_2_OTHER_DANGER_OVERRIDE_VALUE_BMSK                                0xc00000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_2_OTHER_DANGER_OVERRIDE_VALUE_SHFT                                    0x16
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_1_OTHER_DANGER_OVERRIDE_VALUE_BMSK                                0x300000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_1_OTHER_DANGER_OVERRIDE_VALUE_SHFT                                    0x14
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_0_OTHER_DANGER_OVERRIDE_VALUE_BMSK                                 0xc0000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_0_OTHER_DANGER_OVERRIDE_VALUE_SHFT                                    0x12
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_3_DANGER_OVERRIDE_VALUE_BMSK                                       0x30000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_3_DANGER_OVERRIDE_VALUE_SHFT                                          0x10
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_2_DANGER_OVERRIDE_VALUE_BMSK                                        0xc000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_2_DANGER_OVERRIDE_VALUE_SHFT                                           0xe
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_1_DANGER_OVERRIDE_VALUE_BMSK                                        0x3000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_1_DANGER_OVERRIDE_VALUE_SHFT                                           0xc
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_0_DANGER_OVERRIDE_VALUE_BMSK                                         0xc00
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_DSP_0_DANGER_OVERRIDE_VALUE_SHFT                                           0xa
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_MODEM_DANGER_OVERRIDE_VALUE_BMSK                                         0x300
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_MODEM_DANGER_OVERRIDE_VALUE_SHFT                                           0x8
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VENUS_DANGER_OVERRIDE_VALUE_BMSK                                          0xc0
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VENUS_DANGER_OVERRIDE_VALUE_SHFT                                           0x6
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_MDP_DANGER_OVERRIDE_VALUE_BMSK                                            0x30
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_MDP_DANGER_OVERRIDE_VALUE_SHFT                                             0x4
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VFE1_DANGER_OVERRIDE_VALUE_BMSK                                            0xc
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VFE1_DANGER_OVERRIDE_VALUE_SHFT                                            0x2
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VFE0_DANGER_OVERRIDE_VALUE_BMSK                                            0x3
#define HWIO_DSA_DSA_DANGER_OVERRIDE_VALUE_VFE0_DANGER_OVERRIDE_VALUE_SHFT                                            0x0

#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_ADDR                                                                   (DSA_DSA_REG_BASE      + 0x00003024)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_RMSK                                                                       0x1fff
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_DANGER_OVERRIDE_EN_ADDR, HWIO_DSA_DSA_DANGER_OVERRIDE_EN_RMSK)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DANGER_OVERRIDE_EN_ADDR, m)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DANGER_OVERRIDE_EN_ADDR,v)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DANGER_OVERRIDE_EN_ADDR,m,v,HWIO_DSA_DSA_DANGER_OVERRIDE_EN_IN)
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_3_OTHER_DANGER_OVERRIDE_EN_BMSK                                        0x1000
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_3_OTHER_DANGER_OVERRIDE_EN_SHFT                                           0xc
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_2_OTHER_DANGER_OVERRIDE_EN_BMSK                                         0x800
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_2_OTHER_DANGER_OVERRIDE_EN_SHFT                                           0xb
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_1_OTHER_DANGER_OVERRIDE_EN_BMSK                                         0x400
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_1_OTHER_DANGER_OVERRIDE_EN_SHFT                                           0xa
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_0_OTHER_DANGER_OVERRIDE_EN_BMSK                                         0x200
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_0_OTHER_DANGER_OVERRIDE_EN_SHFT                                           0x9
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_3_DANGER_OVERRIDE_EN_BMSK                                               0x100
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_3_DANGER_OVERRIDE_EN_SHFT                                                 0x8
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_2_DANGER_OVERRIDE_EN_BMSK                                                0x80
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_2_DANGER_OVERRIDE_EN_SHFT                                                 0x7
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_1_DANGER_OVERRIDE_EN_BMSK                                                0x40
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_1_DANGER_OVERRIDE_EN_SHFT                                                 0x6
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_0_DANGER_OVERRIDE_EN_BMSK                                                0x20
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_DSP_0_DANGER_OVERRIDE_EN_SHFT                                                 0x5
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_MODEM_DANGER_OVERRIDE_EN_BMSK                                                0x10
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_MODEM_DANGER_OVERRIDE_EN_SHFT                                                 0x4
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VENUS_DANGER_OVERRIDE_EN_BMSK                                                 0x8
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VENUS_DANGER_OVERRIDE_EN_SHFT                                                 0x3
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_MDP_DANGER_OVERRIDE_EN_BMSK                                                   0x4
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_MDP_DANGER_OVERRIDE_EN_SHFT                                                   0x2
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VFE1_DANGER_OVERRIDE_EN_BMSK                                                  0x2
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VFE1_DANGER_OVERRIDE_EN_SHFT                                                  0x1
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VFE0_DANGER_OVERRIDE_EN_BMSK                                                  0x1
#define HWIO_DSA_DSA_DANGER_OVERRIDE_EN_VFE0_DANGER_OVERRIDE_EN_SHFT                                                  0x0

#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_ADDR                                                             (DSA_DSA_REG_BASE      + 0x00003028)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_RMSK                                                               0x3fffff
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_IN          \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_ADDR, HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_RMSK)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_ADDR, m)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_ADDR,v)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_ADDR,m,v,HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_IN)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_10_SW_DANGER_OVERRIDE_VALUE_BMSK                              0x300000
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_10_SW_DANGER_OVERRIDE_VALUE_SHFT                                  0x14
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_9_SW_DANGER_OVERRIDE_VALUE_BMSK                                0xc0000
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_9_SW_DANGER_OVERRIDE_VALUE_SHFT                                   0x12
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_8_SW_DANGER_OVERRIDE_VALUE_BMSK                                0x30000
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_8_SW_DANGER_OVERRIDE_VALUE_SHFT                                   0x10
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_7_SW_DANGER_OVERRIDE_VALUE_BMSK                                 0xc000
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_7_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0xe
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_6_SW_DANGER_OVERRIDE_VALUE_BMSK                                 0x3000
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_6_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0xc
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_5_SW_DANGER_OVERRIDE_VALUE_BMSK                                  0xc00
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_5_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0xa
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_4_SW_DANGER_OVERRIDE_VALUE_BMSK                                  0x300
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_4_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0x8
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_3_SW_DANGER_OVERRIDE_VALUE_BMSK                                   0xc0
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_3_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0x6
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_2_SW_DANGER_OVERRIDE_VALUE_BMSK                                   0x30
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_2_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0x4
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_1_SW_DANGER_OVERRIDE_VALUE_BMSK                                    0xc
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_1_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0x2
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_0_SW_DANGER_OVERRIDE_VALUE_BMSK                                    0x3
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_VALUE_CLNT_0_SW_DANGER_OVERRIDE_VALUE_SHFT                                    0x0

#define HWIO_DSA_DSA_SW_DANGER_ADDR                                                                            (DSA_DSA_REG_BASE      + 0x0000302c)
#define HWIO_DSA_DSA_SW_DANGER_RMSK                                                                              0x3fffff
#define HWIO_DSA_DSA_SW_DANGER_IN          \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_ADDR, HWIO_DSA_DSA_SW_DANGER_RMSK)
#define HWIO_DSA_DSA_SW_DANGER_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_ADDR, m)
#define HWIO_DSA_DSA_SW_DANGER_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SW_DANGER_ADDR,v)
#define HWIO_DSA_DSA_SW_DANGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SW_DANGER_ADDR,m,v,HWIO_DSA_DSA_SW_DANGER_IN)
#define HWIO_DSA_DSA_SW_DANGER_CLNT_10_SW_DANGER_BMSK                                                            0x300000
#define HWIO_DSA_DSA_SW_DANGER_CLNT_10_SW_DANGER_SHFT                                                                0x14
#define HWIO_DSA_DSA_SW_DANGER_CLNT_9_SW_DANGER_BMSK                                                              0xc0000
#define HWIO_DSA_DSA_SW_DANGER_CLNT_9_SW_DANGER_SHFT                                                                 0x12
#define HWIO_DSA_DSA_SW_DANGER_CLNT_8_SW_DANGER_BMSK                                                              0x30000
#define HWIO_DSA_DSA_SW_DANGER_CLNT_8_SW_DANGER_SHFT                                                                 0x10
#define HWIO_DSA_DSA_SW_DANGER_CLNT_7_SW_DANGER_BMSK                                                               0xc000
#define HWIO_DSA_DSA_SW_DANGER_CLNT_7_SW_DANGER_SHFT                                                                  0xe
#define HWIO_DSA_DSA_SW_DANGER_CLNT_6_SW_DANGER_BMSK                                                               0x3000
#define HWIO_DSA_DSA_SW_DANGER_CLNT_6_SW_DANGER_SHFT                                                                  0xc
#define HWIO_DSA_DSA_SW_DANGER_CLNT_5_SW_DANGER_BMSK                                                                0xc00
#define HWIO_DSA_DSA_SW_DANGER_CLNT_5_SW_DANGER_SHFT                                                                  0xa
#define HWIO_DSA_DSA_SW_DANGER_CLNT_4_SW_DANGER_BMSK                                                                0x300
#define HWIO_DSA_DSA_SW_DANGER_CLNT_4_SW_DANGER_SHFT                                                                  0x8
#define HWIO_DSA_DSA_SW_DANGER_CLNT_3_SW_DANGER_BMSK                                                                 0xc0
#define HWIO_DSA_DSA_SW_DANGER_CLNT_3_SW_DANGER_SHFT                                                                  0x6
#define HWIO_DSA_DSA_SW_DANGER_CLNT_2_SW_DANGER_BMSK                                                                 0x30
#define HWIO_DSA_DSA_SW_DANGER_CLNT_2_SW_DANGER_SHFT                                                                  0x4
#define HWIO_DSA_DSA_SW_DANGER_CLNT_1_SW_DANGER_BMSK                                                                  0xc
#define HWIO_DSA_DSA_SW_DANGER_CLNT_1_SW_DANGER_SHFT                                                                  0x2
#define HWIO_DSA_DSA_SW_DANGER_CLNT_0_SW_DANGER_BMSK                                                                  0x3
#define HWIO_DSA_DSA_SW_DANGER_CLNT_0_SW_DANGER_SHFT                                                                  0x0

#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00003030)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_RMSK                                                                     0x7ff
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_ADDR, HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_RMSK)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_ADDR, m)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_ADDR,v)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_ADDR,m,v,HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_IN)
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_10_SW_DANGER_OVERRIDE_EN_BMSK                                       0x400
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_10_SW_DANGER_OVERRIDE_EN_SHFT                                         0xa
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_9_SW_DANGER_OVERRIDE_EN_BMSK                                        0x200
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_9_SW_DANGER_OVERRIDE_EN_SHFT                                          0x9
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_8_SW_DANGER_OVERRIDE_EN_BMSK                                        0x100
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_8_SW_DANGER_OVERRIDE_EN_SHFT                                          0x8
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_7_SW_DANGER_OVERRIDE_EN_BMSK                                         0x80
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_7_SW_DANGER_OVERRIDE_EN_SHFT                                          0x7
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_6_SW_DANGER_OVERRIDE_EN_BMSK                                         0x40
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_6_SW_DANGER_OVERRIDE_EN_SHFT                                          0x6
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_5_SW_DANGER_OVERRIDE_EN_BMSK                                         0x20
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_5_SW_DANGER_OVERRIDE_EN_SHFT                                          0x5
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_4_SW_DANGER_OVERRIDE_EN_BMSK                                         0x10
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_4_SW_DANGER_OVERRIDE_EN_SHFT                                          0x4
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_3_SW_DANGER_OVERRIDE_EN_BMSK                                          0x8
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_3_SW_DANGER_OVERRIDE_EN_SHFT                                          0x3
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_2_SW_DANGER_OVERRIDE_EN_BMSK                                          0x4
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_2_SW_DANGER_OVERRIDE_EN_SHFT                                          0x2
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_1_SW_DANGER_OVERRIDE_EN_BMSK                                          0x2
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_1_SW_DANGER_OVERRIDE_EN_SHFT                                          0x1
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_0_SW_DANGER_OVERRIDE_EN_BMSK                                          0x1
#define HWIO_DSA_DSA_SW_DANGER_OVERRIDE_EN_CLNT_0_SW_DANGER_OVERRIDE_EN_SHFT                                          0x0

#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_ADDR                                                                    (DSA_DSA_REG_BASE      + 0x00003040)
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_RMSK                                                                          0x1f
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_OVERRIDE_VALUE_ADDR, HWIO_DSA_DSA_DT_OVERRIDE_VALUE_RMSK)
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_OVERRIDE_VALUE_ADDR, m)
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_OVERRIDE_VALUE_ADDR,v)
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_OVERRIDE_VALUE_ADDR,m,v,HWIO_DSA_DSA_DT_OVERRIDE_VALUE_IN)
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_ACK_OVERRIDE_VALUE_BMSK                                                    0x10
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_ACK_OVERRIDE_VALUE_SHFT                                                     0x4
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_URG_OVERRIDE_VALUE_BMSK                                                     0xe
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_URG_OVERRIDE_VALUE_SHFT                                                     0x1
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_REQ_OVERRIDE_VALUE_BMSK                                                     0x1
#define HWIO_DSA_DSA_DT_OVERRIDE_VALUE_DT_REQ_OVERRIDE_VALUE_SHFT                                                     0x0

#define HWIO_DSA_DSA_DT_OVERRIDE_EN_ADDR                                                                       (DSA_DSA_REG_BASE      + 0x00003044)
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_RMSK                                                                              0x7
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_OVERRIDE_EN_ADDR, HWIO_DSA_DSA_DT_OVERRIDE_EN_RMSK)
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_OVERRIDE_EN_ADDR, m)
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_OVERRIDE_EN_ADDR,v)
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_OVERRIDE_EN_ADDR,m,v,HWIO_DSA_DSA_DT_OVERRIDE_EN_IN)
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_ACK_OVERRIDE_EN_BMSK                                                           0x4
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_ACK_OVERRIDE_EN_SHFT                                                           0x2
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_URG_OVERRIDE_EN_BMSK                                                           0x2
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_URG_OVERRIDE_EN_SHFT                                                           0x1
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_REQ_OVERRIDE_EN_BMSK                                                           0x1
#define HWIO_DSA_DSA_DT_OVERRIDE_EN_DT_REQ_OVERRIDE_EN_SHFT                                                           0x0

#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00003048)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_RMSK                                                                       0xf
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_ADDR, HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_RMSK)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_ADDR, m)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_ADDR,v)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_ADDR,m,v,HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_IN)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_DT_URG_EXT_OVERRIDE_VALUE_BMSK                                             0xe
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_DT_URG_EXT_OVERRIDE_VALUE_SHFT                                             0x1
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_DT_REQ_EXT_OVERRIDE_VALUE_BMSK                                             0x1
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_VALUE_DT_REQ_EXT_OVERRIDE_VALUE_SHFT                                             0x0

#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_ADDR                                                                   (DSA_DSA_REG_BASE      + 0x0000304c)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_RMSK                                                                          0x3
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_IN          \
        in_dword_masked(HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_ADDR, HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_RMSK)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_ADDR, m)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_OUT(v)      \
        out_dword(HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_ADDR,v)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_ADDR,m,v,HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_IN)
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_DT_URG_EXT_OVERRIDE_EN_BMSK                                                   0x2
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_DT_URG_EXT_OVERRIDE_EN_SHFT                                                   0x1
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_DT_REQ_EXT_OVERRIDE_EN_BMSK                                                   0x1
#define HWIO_DSA_DSA_DT_EXT_OVERRIDE_EN_DT_REQ_EXT_OVERRIDE_EN_SHFT                                                   0x0

#define HWIO_DSA_DSA_BIMC_QOS_CFG_ADDR                                                                         (DSA_DSA_REG_BASE      + 0x00003050)
#define HWIO_DSA_DSA_BIMC_QOS_CFG_RMSK                                                                                0xf
#define HWIO_DSA_DSA_BIMC_QOS_CFG_IN          \
        in_dword_masked(HWIO_DSA_DSA_BIMC_QOS_CFG_ADDR, HWIO_DSA_DSA_BIMC_QOS_CFG_RMSK)
#define HWIO_DSA_DSA_BIMC_QOS_CFG_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_BIMC_QOS_CFG_ADDR, m)
#define HWIO_DSA_DSA_BIMC_QOS_CFG_OUT(v)      \
        out_dword(HWIO_DSA_DSA_BIMC_QOS_CFG_ADDR,v)
#define HWIO_DSA_DSA_BIMC_QOS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_BIMC_QOS_CFG_ADDR,m,v,HWIO_DSA_DSA_BIMC_QOS_CFG_IN)
#define HWIO_DSA_DSA_BIMC_QOS_CFG_PIMEM_QOS_SEL_BMSK                                                                  0x8
#define HWIO_DSA_DSA_BIMC_QOS_CFG_PIMEM_QOS_SEL_SHFT                                                                  0x3
#define HWIO_DSA_DSA_BIMC_QOS_CFG_MDSP_QOS_SEL_BMSK                                                                   0x4
#define HWIO_DSA_DSA_BIMC_QOS_CFG_MDSP_QOS_SEL_SHFT                                                                   0x2
#define HWIO_DSA_DSA_BIMC_QOS_CFG_MNOC_QOS_SEL_BMSK                                                                   0x2
#define HWIO_DSA_DSA_BIMC_QOS_CFG_MNOC_QOS_SEL_SHFT                                                                   0x1
#define HWIO_DSA_DSA_BIMC_QOS_CFG_SNOC_QOS_SEL_BMSK                                                                   0x1
#define HWIO_DSA_DSA_BIMC_QOS_CFG_SNOC_QOS_SEL_SHFT                                                                   0x0

#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ADDR                                                                    (DSA_DSA_REG_BASE      + 0x00003060)
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_RMSK                                                                           0x1
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_IN          \
        in_dword_masked(HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ADDR, HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_RMSK)
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ADDR, m)
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_OUT(v)      \
        out_dword(HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ADDR,v)
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ADDR,m,v,HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_IN)
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ATB_EN_BMSK                                                                    0x1
#define HWIO_DSA_DSA_OBS_DBG_ATB_CNTRL_ATB_EN_SHFT                                                                    0x0

#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_ADDR                                                         (DSA_DSA_REG_BASE      + 0x00001740)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_RMSK                                                                0x1
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_IN          \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_ADDR, HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_RMSK)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_ADDR, m)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_OUT(v)      \
        out_dword(HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_ADDR,v)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_ADDR,m,v,HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_IN)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_THROTTLE_LEVEL_LEGACY_SELECT_BMSK                                   0x1
#define HWIO_DSA_DSA_THROTTLE_LEVEL_LEGACY_SELECT_THROTTLE_LEVEL_LEGACY_SELECT_SHFT                                   0x0

#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_ADDR                                                                (DSA_DSA_REG_BASE      + 0x00001744)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_RMSK                                                                      0x7f
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_IN          \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_ADDR, HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_RMSK)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_ADDR, m)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_OUT(v)      \
        out_dword(HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_ADDR,v)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_ADDR,m,v,HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_IN)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_7_BMSK                                                0x40
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_7_SHFT                                                 0x6
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_6_BMSK                                                0x20
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_6_SHFT                                                 0x5
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_5_BMSK                                                0x10
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_5_SHFT                                                 0x4
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_4_BMSK                                                 0x8
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_4_SHFT                                                 0x3
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_3_BMSK                                                 0x4
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_3_SHFT                                                 0x2
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_2_BMSK                                                 0x2
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_2_SHFT                                                 0x1
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_1_BMSK                                                 0x1
#define HWIO_DSA_DSA_THROTTLE_LEVEL_SU_CFG_QOS_ASSIST_REQ_MODE_1_SHFT                                                 0x0

#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_ADDR(n)                                                                 (DSA_DSA_REG_BASE      + 0x00001748 + 0x4 * (n))
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_RMSK                                                                       0x1ffff
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_MAXn                                                                             2
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_INI(n)        \
        in_dword_masked(HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_ADDR(n), HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_RMSK)
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_ADDR(n), mask)
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_OUTI(n,val)    \
        out_dword(HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_ADDR(n),val)
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_ADDR(n),mask,val,HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_INI(n))
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_FREQ_BAND_BNDRY_BMSK                                                       0x1ffff
#define HWIO_DSA_QOS_FREQ_BAND_BNDRY_n_FREQ_BAND_BNDRY_SHFT                                                           0x0

#define HWIO_DSA_DSA_PERIOD_BUS_CFG_ADDR                                                                       (DSA_DSA_REG_BASE      + 0x00001754)
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_RMSK                                                                       0xc001ffff
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_IN          \
        in_dword_masked(HWIO_DSA_DSA_PERIOD_BUS_CFG_ADDR, HWIO_DSA_DSA_PERIOD_BUS_CFG_RMSK)
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PERIOD_BUS_CFG_ADDR, m)
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_OUT(v)      \
        out_dword(HWIO_DSA_DSA_PERIOD_BUS_CFG_ADDR,v)
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_DSA_DSA_PERIOD_BUS_CFG_ADDR,m,v,HWIO_DSA_DSA_PERIOD_BUS_CFG_IN)
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_SW_OVERRIDE_BMSK                                                0x80000000
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_SW_OVERRIDE_SHFT                                                      0x1f
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_LOAD_SW_BMSK                                                    0x40000000
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_LOAD_SW_SHFT                                                          0x1e
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_SW_BMSK                                                            0x1ffff
#define HWIO_DSA_DSA_PERIOD_BUS_CFG_PERIOD_BUS_SW_SHFT                                                                0x0

#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_ADDR                                                                    (DSA_DSA_REG_BASE      + 0x00001758)
#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_RMSK                                                                       0x1ffff
#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_IN          \
        in_dword_masked(HWIO_DSA_DSA_PERIOD_BUS_STATUS_ADDR, HWIO_DSA_DSA_PERIOD_BUS_STATUS_RMSK)
#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_INM(m)      \
        in_dword_masked(HWIO_DSA_DSA_PERIOD_BUS_STATUS_ADDR, m)
#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_PERIOD_BUS_SAMPLE_REG_BMSK                                                 0x1ffff
#define HWIO_DSA_DSA_PERIOD_BUS_STATUS_PERIOD_BUS_SAMPLE_REG_SHFT                                                     0x0

#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_ADDR(n)                                                              (DSA_DSA_REG_BASE      + 0x0000175c + 0x4 * (n))
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_RMSK                                                                     0x87ff
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_MAXn                                                                          7
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_ADDR(n), HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_RMSK)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_ADDR(n), mask)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_ADDR(n),val)
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_ADDR(n),mask,val,HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_INI(n))
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_FREQ_EQ_BMSK                                                       0x8000
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_FREQ_EQ_SHFT                                                          0xf
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_FREQ_BMSK                                                           0x700
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_FREQ_SHFT                                                             0x8
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_SU_H_BMSK                                                            0xf0
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_SU_H_SHFT                                                             0x4
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_SU_L_BMSK                                                             0xf
#define HWIO_DSA_DSA_THROTTLE_LEVEL_QOS_n_THRES_SU_L_SHFT                                                             0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x0000178c + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_1_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x000017b8 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_2_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x000017e4 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_3_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x00001810 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_4_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x0000183c + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_5_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x00001868 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_6_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_ADDR(n)                                                       (DSA_DSA_REG_BASE      + 0x00001894 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_RMSK                                                          0xff00008f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_MAXn                                                                  10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_AGG_BOOLEAN_FUNCTION_BMSK                                     0xff000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_AGG_BOOLEAN_FUNCTION_SHFT                                           0x18
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_THRES_DANGER_EQ_BMSK                                                0x80
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_THRES_DANGER_EQ_SHFT                                                 0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_THRES_DANGER_BMSK                                                    0xf
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_QOS_7_THRES_DANGER_SHFT                                                    0x0

#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_ADDR(n)                                                     (DSA_DSA_REG_BASE      + 0x000018c0 + 0x4 * (n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_RMSK                                                        0x80000007
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_MAXn                                                                10
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_INI(n)        \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_ADDR(n), HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_RMSK)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_INMI(n,mask)    \
        in_dword_masked(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_ADDR(n), mask)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_OUTI(n,val)    \
        out_dword(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_ADDR(n),val)
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_ADDR(n),mask,val,HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_INI(n))
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_THROTTLE_LEVEL_SW_OVERRD_BMSK                               0x80000000
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_THROTTLE_LEVEL_SW_OVERRD_SHFT                                     0x1f
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_THROTTLE_LEVEL_SW_BMSK                                             0x7
#define HWIO_DSA_DSA_CLNT_n_THROTTLE_LEVEL_MUX_CFG_THROTTLE_LEVEL_SW_SHFT                                             0x0


#define GCC_CLK_CTL_REG_REG_BASE                                                                (CLK_CTL_BASE      + 0x00000000)

#define HWIO_GCC_DSA_CORE_CMD_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e00c)
#define HWIO_GCC_DSA_CORE_CMD_RCGR_RMSK                                                         0x80000013
#define HWIO_GCC_DSA_CORE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_DSA_CORE_CMD_RCGR_ADDR, HWIO_GCC_DSA_CORE_CMD_RCGR_RMSK)
#define HWIO_GCC_DSA_CORE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_DSA_CORE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_DSA_CORE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_DSA_CORE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_DSA_CORE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DSA_CORE_CMD_RCGR_ADDR,m,v,HWIO_GCC_DSA_CORE_CMD_RCGR_IN)
#define HWIO_GCC_DSA_CORE_CMD_RCGR_ROOT_OFF_BMSK                                                0x80000000
#define HWIO_GCC_DSA_CORE_CMD_RCGR_ROOT_OFF_SHFT                                                      0x1f
#define HWIO_GCC_DSA_CORE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                0x10
#define HWIO_GCC_DSA_CORE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                 0x4
#define HWIO_GCC_DSA_CORE_CMD_RCGR_ROOT_EN_BMSK                                                        0x2
#define HWIO_GCC_DSA_CORE_CMD_RCGR_ROOT_EN_SHFT                                                        0x1
#define HWIO_GCC_DSA_CORE_CMD_RCGR_UPDATE_BMSK                                                         0x1
#define HWIO_GCC_DSA_CORE_CMD_RCGR_UPDATE_SHFT                                                         0x0

#define HWIO_GCC_DSA_CORE_CFG_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e010)
#define HWIO_GCC_DSA_CORE_CFG_RCGR_RMSK                                                              0x71f
#define HWIO_GCC_DSA_CORE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_DSA_CORE_CFG_RCGR_ADDR, HWIO_GCC_DSA_CORE_CFG_RCGR_RMSK)
#define HWIO_GCC_DSA_CORE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_DSA_CORE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_DSA_CORE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_DSA_CORE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_DSA_CORE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DSA_CORE_CFG_RCGR_ADDR,m,v,HWIO_GCC_DSA_CORE_CFG_RCGR_IN)
#define HWIO_GCC_DSA_CORE_CFG_RCGR_SRC_SEL_BMSK                                                      0x700
#define HWIO_GCC_DSA_CORE_CFG_RCGR_SRC_SEL_SHFT                                                        0x8
#define HWIO_GCC_DSA_CORE_CFG_RCGR_SRC_DIV_BMSK                                                       0x1f
#define HWIO_GCC_DSA_CORE_CFG_RCGR_SRC_DIV_SHFT                                                        0x0

#define HWIO_GCC_DSA_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e000)
#define HWIO_GCC_DSA_BCR_RMSK                                                                          0x1
#define HWIO_GCC_DSA_BCR_IN          \
        in_dword_masked(HWIO_GCC_DSA_BCR_ADDR, HWIO_GCC_DSA_BCR_RMSK)
#define HWIO_GCC_DSA_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DSA_BCR_ADDR, m)
#define HWIO_GCC_DSA_BCR_OUT(v)      \
        out_dword(HWIO_GCC_DSA_BCR_ADDR,v)
#define HWIO_GCC_DSA_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DSA_BCR_ADDR,m,v,HWIO_GCC_DSA_BCR_IN)
#define HWIO_GCC_DSA_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_DSA_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_DSA_CORE_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e004)
#define HWIO_GCC_DSA_CORE_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_DSA_CORE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DSA_CORE_CBCR_ADDR, HWIO_GCC_DSA_CORE_CBCR_RMSK)
#define HWIO_GCC_DSA_CORE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DSA_CORE_CBCR_ADDR, m)
#define HWIO_GCC_DSA_CORE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DSA_CORE_CBCR_ADDR,v)
#define HWIO_GCC_DSA_CORE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DSA_CORE_CBCR_ADDR,m,v,HWIO_GCC_DSA_CORE_CBCR_IN)
#define HWIO_GCC_DSA_CORE_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_DSA_CORE_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_DSA_CORE_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_DSA_CORE_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e008)
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_RMSK                                                      0xf0008003
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_ADDR, HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_IN)
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                             0x70000000
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                   0x1c
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                     0x8000
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                        0xf
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_RESERVE_BIT1_BMSK                                                0x2
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_RESERVE_BIT1_SHFT                                                0x1
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_DSA_NOC_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0


#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_ADDR                                                      (BIMC_BASE      + 0x00000028)
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_RMSK                                                         0x1001ffff
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_MAXn                                                                  1
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_INI        \
        in_dword_masked(HWIO_BIMC_DDR_CH0_CLK_PERIOD_ADDR, HWIO_BIMC_DDR_CHn_CLK_PERIOD_RMSK)
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_INMI(mask)    \
        in_dword_masked(HWIO_BIMC_DDR_CH0_CLK_PERIOD_ADDR, mask)
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_OUTI(val)    \
        out_dword(HWIO_BIMC_DDR_CH0_CLK_PERIOD_ADDR,val)
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_OUTMI(mask,val) \
        out_dword_masked_ns(HWIO_BIMC_DDR_CHn_CLK_PERIOD_ADDR,mask,val,HWIO_BIMC_DDR_CHn_CLK_PERIOD_INI)
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_CLK_PERIOD_RESOLUTION_BMSK                                   0x10000000
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_CLK_PERIOD_RESOLUTION_SHFT                                         0x1c
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_PERIOD_BMSK                                                     0x1ffff
#define HWIO_BIMC_DDR_CH0_CLK_PERIOD_PERIOD_SHFT                                                         0x0

/*----------------------------------------------------------------------------
 * MODULE: THROTTLE
 *--------------------------------------------------------------------------*/
#define THROTTLE_REG_BASE(b)                                       ((uint8_t *)(b))

#define THROTTLE_CNTRL_ADDR(b,n)                                   (THROTTLE_REG_BASE(b)   + 0x00000000 + 0x200 * (n))
#define THROTTLE_CNTRL_RMSK                                               0x1
#define THROTTLE_CNTRL_INI(b,n)     \
        in_dword_masked(THROTTLE_CNTRL_ADDR(b,n), THROTTLE_CNTRL_RMSK)
#define THROTTLE_CNTRL_INMI(b,n,m) \
        in_dword_masked(THROTTLE_CNTRL_ADDR(b,n), m)
#define THROTTLE_CNTRL_OUTI(b,n,v) \
        out_dword(THROTTLE_CNTRL_ADDR(b,n),v)
#define THROTTLE_CNTRL_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_CNTRL_ADDR(b,n),m,v,THROTTLE_CNTRL_INI(b,n))
#define THROTTLE_CNTRL_THROTTLE_EN_BMSK                                   0x1
#define THROTTLE_CNTRL_THROTTLE_EN_SHFT                                   0x0

#define THROTTLE_CGC_CNTRL_ADDR(b,n)                               (THROTTLE_REG_BASE(b)   + 0x00000004 + 0x200 * (n))
#define THROTTLE_CGC_CNTRL_RMSK                                           0x1
#define THROTTLE_CGC_CNTRL_INI(b,n)     \
        in_dword_masked(THROTTLE_CGC_CNTRL_ADDR(b,n), THROTTLE_CGC_CNTRL_RMSK)
#define THROTTLE_CGC_CNTRL_INMI(b,n,m) \
        in_dword_masked(THROTTLE_CGC_CNTRL_ADDR(b,n), m)
#define THROTTLE_CGC_CNTRL_OUTI(b,n,v) \
        out_dword(THROTTLE_CGC_CNTRL_ADDR(b,n),v)
#define THROTTLE_CGC_CNTRL_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_CGC_CNTRL_ADDR(b,n),m,v,THROTTLE_CGC_CNTRL_INI(b,n))
#define THROTTLE_CGC_CNTRL_THROTTLE_CGC_EN_BMSK                           0x1
#define THROTTLE_CGC_CNTRL_THROTTLE_CGC_EN_SHFT                           0x0

#define THROTTLE_RESET_CNTRL_ADDR(b,n)                             (THROTTLE_REG_BASE(b)   + 0x00000008 + 0x200 * (n))
#define THROTTLE_RESET_CNTRL_RMSK                                         0x1
#define THROTTLE_RESET_CNTRL_INI(b,n)     \
        in_dword_masked(THROTTLE_RESET_CNTRL_ADDR(b,n), THROTTLE_RESET_CNTRL_RMSK)
#define THROTTLE_RESET_CNTRL_INMI(b,n,m) \
        in_dword_masked(THROTTLE_RESET_CNTRL_ADDR(b,n), m)
#define THROTTLE_RESET_CNTRL_OUTI(b,n,v) \
        out_dword(THROTTLE_RESET_CNTRL_ADDR(b,n),v)
#define THROTTLE_RESET_CNTRL_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_RESET_CNTRL_ADDR(b,n),m,v,THROTTLE_RESET_CNTRL_INI(b,n))
#define THROTTLE_RESET_CNTRL_CORE_SW_RESET_BMSK                           0x1
#define THROTTLE_RESET_CNTRL_CORE_SW_RESET_SHFT                           0x0

#define THROTTLE_CONFIG_ADDR(b,n)                                  (THROTTLE_REG_BASE(b)   + 0x0000000c + 0x200 * (n))
#define THROTTLE_CONFIG_RMSK                                              0x7
#define THROTTLE_CONFIG_INI(b,n)     \
        in_dword_masked(THROTTLE_CONFIG_ADDR(b,n), THROTTLE_CONFIG_RMSK)
#define THROTTLE_CONFIG_INMI(b,n,m) \
        in_dword_masked(THROTTLE_CONFIG_ADDR(b,n), m)
#define THROTTLE_CONFIG_OUTI(b,n,v) \
        out_dword(THROTTLE_CONFIG_ADDR(b,n),v)
#define THROTTLE_CONFIG_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_CONFIG_ADDR(b,n),m,v,THROTTLE_CONFIG_INI(b,n))
#define THROTTLE_CONFIG_THROTTLE_CNTRL_MAX_SKEW_BMSK                      0x7
#define THROTTLE_CONFIG_THROTTLE_CNTRL_MAX_SKEW_SHFT                      0x0

#define THROTTLE_GRANT_PERIOD_ADDR(b,n)                            (THROTTLE_REG_BASE(b)   + 0x00000010 + 0x200 * (n))
#define THROTTLE_GRANT_PERIOD_RMSK                                     0x3fff
#define THROTTLE_GRANT_PERIOD_INI(b,n)     \
        in_dword_masked(THROTTLE_GRANT_PERIOD_ADDR(b,n), THROTTLE_GRANT_PERIOD_RMSK)
#define THROTTLE_GRANT_PERIOD_INMI(b,n,m) \
        in_dword_masked(THROTTLE_GRANT_PERIOD_ADDR(b,n), m)
#define THROTTLE_GRANT_PERIOD_OUTI(b,n,v) \
        out_dword(THROTTLE_GRANT_PERIOD_ADDR(b,n),v)
#define THROTTLE_GRANT_PERIOD_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_GRANT_PERIOD_ADDR(b,n),m,v,THROTTLE_GRANT_PERIOD_INI(b,n))
#define THROTTLE_GRANT_PERIOD_GRANT_PERIOD_BMSK                        0x3fff
#define THROTTLE_GRANT_PERIOD_GRANT_PERIOD_SHFT                           0x0

#define THROTTLE_GRANT_COUNT_ADDR(b,n)                             (THROTTLE_REG_BASE(b)   + 0x00000014 + 0x200 * (n))
#define THROTTLE_GRANT_COUNT_RMSK                                      0xffff
#define THROTTLE_GRANT_COUNT_INI(b,n)     \
        in_dword_masked(THROTTLE_GRANT_COUNT_ADDR(b,n), THROTTLE_GRANT_COUNT_RMSK)
#define THROTTLE_GRANT_COUNT_INMI(b,n,m) \
        in_dword_masked(THROTTLE_GRANT_COUNT_ADDR(b,n), m)
#define THROTTLE_GRANT_COUNT_OUTI(b,n,v) \
        out_dword(THROTTLE_GRANT_COUNT_ADDR(b,n),v)
#define THROTTLE_GRANT_COUNT_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_GRANT_COUNT_ADDR(b,n),m,v,THROTTLE_GRANT_COUNT_INI(b,n))
#define THROTTLE_GRANT_COUNT_GRANT_COUNT_BMSK                          0xffff
#define THROTTLE_GRANT_COUNT_GRANT_COUNT_SHFT                             0x0

#define THROTTLE_THRESHOLD_i_ADDR(b,n,i)                          (THROTTLE_REG_BASE(b)   + 0x00000018 + 0x200 * (n) + 0x4 * (i))
#define THROTTLE_THRESHOLD_i_RMSK                                 0xffffffff
#define THROTTLE_THRESHOLD_i_INI2(b,n,i)   \
        in_dword_masked(THROTTLE_THRESHOLD_i_ADDR(b,n,i), THROTTLE_THRESHOLD_i_RMSK)
#define THROTTLE_THRESHOLD_i_INMI2(b,n,i,m) \
        in_dword_masked(THROTTLE_THRESHOLD_i_ADDR(b,n,i), m)
#define THROTTLE_THRESHOLD_i_OUTI2(b,n,i,v) \
        out_dword(THROTTLE_THRESHOLD_i_ADDR(b,n,i),v)
#define THROTTLE_THRESHOLD_i_OUTMI2(b,n,i,m,v) \
        out_dword_masked_ns(THROTTLE_THRESHOLD_i_ADDR(b,n,i),m,v,THROTTLE_THRESHOLD_i_INI2(b,n,i))
#define THROTTLE_THRESHOLD_i_THRESHOLD_LEVEL_0_BMSK               0xffff0000
#define THROTTLE_THRESHOLD_i_THRESHOLD_LEVEL_0_SHFT                     0x10
#define THROTTLE_THRESHOLD_i_THRESHOLD_LEVEL_1_BMSK                   0xffff
#define THROTTLE_THRESHOLD_i_THRESHOLD_LEVEL_1_SHFT                      0x0

#define THROTTLE_THRESHOLD_00_ADDR(b,n)                            (THROTTLE_REG_BASE(b)   + 0x00000018 + 0x200 * (n))
#define THROTTLE_THRESHOLD_00_RMSK                                 0xffffffff
#define THROTTLE_THRESHOLD_00_INI(b,n)     \
        in_dword_masked(THROTTLE_THRESHOLD_00_ADDR(b,n), THROTTLE_THRESHOLD_00_RMSK)
#define THROTTLE_THRESHOLD_00_INMI(b,n,m) \
        in_dword_masked(THROTTLE_THRESHOLD_00_ADDR(b,n), m)
#define THROTTLE_THRESHOLD_00_OUTI(b,n,v) \
        out_dword(THROTTLE_THRESHOLD_00_ADDR(b,n),v)
#define THROTTLE_THRESHOLD_00_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_THRESHOLD_00_ADDR(b,n),m,v,THROTTLE_THRESHOLD_00_INI(b,n))
#define THROTTLE_THRESHOLD_00_THRESHOLD_LEVEL_6_BMSK               0xffff0000
#define THROTTLE_THRESHOLD_00_THRESHOLD_LEVEL_6_SHFT                     0x10
#define THROTTLE_THRESHOLD_00_THRESHOLD_LEVEL_7_BMSK                   0xffff
#define THROTTLE_THRESHOLD_00_THRESHOLD_LEVEL_7_SHFT                      0x0

#define THROTTLE_THRESHOLD_01_ADDR(b,n)                            (THROTTLE_REG_BASE(b)   + 0x0000001c + 0x200 * (n))
#define THROTTLE_THRESHOLD_01_RMSK                                 0xffffffff
#define THROTTLE_THRESHOLD_01_INI(b,n)     \
        in_dword_masked(THROTTLE_THRESHOLD_01_ADDR(b,n), THROTTLE_THRESHOLD_01_RMSK)
#define THROTTLE_THRESHOLD_01_INMI(b,n,m) \
        in_dword_masked(THROTTLE_THRESHOLD_01_ADDR(b,n), m)
#define THROTTLE_THRESHOLD_01_OUTI(b,n,v) \
        out_dword(THROTTLE_THRESHOLD_01_ADDR(b,n),v)
#define THROTTLE_THRESHOLD_01_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_THRESHOLD_01_ADDR(b,n),m,v,THROTTLE_THRESHOLD_01_INI(b,n))
#define THROTTLE_THRESHOLD_01_THRESHOLD_LEVEL_4_BMSK               0xffff0000
#define THROTTLE_THRESHOLD_01_THRESHOLD_LEVEL_4_SHFT                     0x10
#define THROTTLE_THRESHOLD_01_THRESHOLD_LEVEL_5_BMSK                   0xffff
#define THROTTLE_THRESHOLD_01_THRESHOLD_LEVEL_5_SHFT                      0x0

#define THROTTLE_THRESHOLD_02_ADDR(b,n)                            (THROTTLE_REG_BASE(b)   + 0x00000020 + 0x200 * (n))
#define THROTTLE_THRESHOLD_02_RMSK                                 0xffffffff
#define THROTTLE_THRESHOLD_02_INI(b,n)     \
        in_dword_masked(THROTTLE_THRESHOLD_02_ADDR(b,n), THROTTLE_THRESHOLD_02_RMSK)
#define THROTTLE_THRESHOLD_02_INMI(b,n,m) \
        in_dword_masked(THROTTLE_THRESHOLD_02_ADDR(b,n), m)
#define THROTTLE_THRESHOLD_02_OUTI(b,n,v) \
        out_dword(THROTTLE_THRESHOLD_02_ADDR(b,n),v)
#define THROTTLE_THRESHOLD_02_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_THRESHOLD_02_ADDR(b,n),m,v,THROTTLE_THRESHOLD_02_INI(b,n))
#define THROTTLE_THRESHOLD_02_THRESHOLD_LEVEL_2_BMSK               0xffff0000
#define THROTTLE_THRESHOLD_02_THRESHOLD_LEVEL_2_SHFT                     0x10
#define THROTTLE_THRESHOLD_02_THRESHOLD_LEVEL_3_BMSK                   0xffff
#define THROTTLE_THRESHOLD_02_THRESHOLD_LEVEL_3_SHFT                      0x0

#define THROTTLE_THRESHOLD_03_ADDR(b,n)                            (THROTTLE_REG_BASE(b)   + 0x00000024 + 0x200 * (n))
#define THROTTLE_THRESHOLD_03_RMSK                                     0xffff
#define THROTTLE_THRESHOLD_03_INI(b,n)     \
        in_dword_masked(THROTTLE_THRESHOLD_03_ADDR(b,n), THROTTLE_THRESHOLD_03_RMSK)
#define THROTTLE_THRESHOLD_03_INMI(b,n,m) \
        in_dword_masked(THROTTLE_THRESHOLD_03_ADDR(b,n), m)
#define THROTTLE_THRESHOLD_03_OUTI(b,n,v) \
        out_dword(THROTTLE_THRESHOLD_03_ADDR(b,n),v)
#define THROTTLE_THRESHOLD_03_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_THRESHOLD_03_ADDR(b,n),m,v,THROTTLE_THRESHOLD_03_INI(b,n))
#define THROTTLE_THRESHOLD_03_THRESHOLD_LEVEL_1_BMSK                   0xffff
#define THROTTLE_THRESHOLD_03_THRESHOLD_LEVEL_1_SHFT                      0x0

#define THROTTLE_PEAK_ACCUM_CREDIT_ADDR(b,n)                       (THROTTLE_REG_BASE(b)   + 0x00000028 + 0x200 * (n))
#define THROTTLE_PEAK_ACCUM_CREDIT_RMSK                               0xfffff
#define THROTTLE_PEAK_ACCUM_CREDIT_INI(b,n)     \
        in_dword_masked(THROTTLE_PEAK_ACCUM_CREDIT_ADDR(b,n), THROTTLE_PEAK_ACCUM_CREDIT_RMSK)
#define THROTTLE_PEAK_ACCUM_CREDIT_INMI(b,n,m) \
        in_dword_masked(THROTTLE_PEAK_ACCUM_CREDIT_ADDR(b,n), m)
#define THROTTLE_PEAK_ACCUM_CREDIT_OUTI(b,n,v) \
        out_dword(THROTTLE_PEAK_ACCUM_CREDIT_ADDR(b,n),v)
#define THROTTLE_PEAK_ACCUM_CREDIT_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_PEAK_ACCUM_CREDIT_ADDR(b,n),m,v,THROTTLE_PEAK_ACCUM_CREDIT_INI(b,n))
#define THROTTLE_PEAK_ACCUM_CREDIT_PEAK_ACCUM_CREDIT_BMSK             0xfffff
#define THROTTLE_PEAK_ACCUM_CREDIT_PEAK_ACCUM_CREDIT_SHFT                 0x0

#define THROTTLE_DEBUG_CNTL_ADDR(b,n)                              (THROTTLE_REG_BASE(b)   + 0x00000030 + 0x200 * (n))
#define THROTTLE_DEBUG_CNTL_RMSK                                        0xf01
#define THROTTLE_DEBUG_CNTL_INI(b,n)     \
        in_dword_masked(THROTTLE_DEBUG_CNTL_ADDR(b,n), THROTTLE_DEBUG_CNTL_RMSK)
#define THROTTLE_DEBUG_CNTL_INMI(b,n,m) \
        in_dword_masked(THROTTLE_DEBUG_CNTL_ADDR(b,n), m)
#define THROTTLE_DEBUG_CNTL_OUTI(b,n,v) \
        out_dword(THROTTLE_DEBUG_CNTL_ADDR(b,n),v)
#define THROTTLE_DEBUG_CNTL_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_DEBUG_CNTL_ADDR(b,n),m,v,THROTTLE_DEBUG_CNTL_INI(b,n))
#define THROTTLE_DEBUG_CNTL_DEBUG_SEL_BMSK                              0xf00
#define THROTTLE_DEBUG_CNTL_DEBUG_SEL_SHFT                                0x8
#define THROTTLE_DEBUG_CNTL_DEBUG_EN_BMSK                                 0x1
#define THROTTLE_DEBUG_CNTL_DEBUG_EN_SHFT                                 0x0

#define THROTTLE_DEBUG_READBACK_ADDR(b,n)                          (THROTTLE_REG_BASE(b)   + 0x00000034 + 0x200 * (n))
#define THROTTLE_DEBUG_READBACK_RMSK                               0xffffffff
#define THROTTLE_DEBUG_READBACK_INI(b,n)     \
        in_dword_masked(THROTTLE_DEBUG_READBACK_ADDR(b,n), THROTTLE_DEBUG_READBACK_RMSK)
#define THROTTLE_DEBUG_READBACK_INMI(b,n,m) \
        in_dword_masked(THROTTLE_DEBUG_READBACK_ADDR(b,n), m)
#define THROTTLE_DEBUG_READBACK_DEBUG_BUS_READBACK_BMSK            0xffffffff
#define THROTTLE_DEBUG_READBACK_DEBUG_BUS_READBACK_SHFT                   0x0

#define THROTTLE_SPARE_REGS_ADDR(b,n)                              (THROTTLE_REG_BASE(b)   + 0x00000040 + 0x200 * (n))
#define THROTTLE_SPARE_REGS_RMSK                                   0xffffffff
#define THROTTLE_SPARE_REGS_INI(b,n)     \
        in_dword_masked(THROTTLE_SPARE_REGS_ADDR(b,n), THROTTLE_SPARE_REGS_RMSK)
#define THROTTLE_SPARE_REGS_INMI(b,n,m) \
        in_dword_masked(THROTTLE_SPARE_REGS_ADDR(b,n), m)
#define THROTTLE_SPARE_REGS_OUTI(b,n,v) \
        out_dword(THROTTLE_SPARE_REGS_ADDR(b,n),v)
#define THROTTLE_SPARE_REGS_OUTMI(b,n,m,v) \
        out_dword_masked_ns(THROTTLE_SPARE_REGS_ADDR(b,n),m,v,THROTTLE_SPARE_REGS_INI(b,n))
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_31_BMSK                  0x80000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_31_SHFT                        0x1f
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_30_BMSK                  0x40000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_30_SHFT                        0x1e
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_29_BMSK                  0x20000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_29_SHFT                        0x1d
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_28_BMSK                  0x10000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_28_SHFT                        0x1c
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_27_BMSK                   0x8000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_27_SHFT                        0x1b
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_26_BMSK                   0x4000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_26_SHFT                        0x1a
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_25_BMSK                   0x2000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_25_SHFT                        0x19
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_24_BMSK                   0x1000000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_24_SHFT                        0x18
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_23_BMSK                    0x800000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_23_SHFT                        0x17
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_22_BMSK                    0x400000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_22_SHFT                        0x16
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_21_BMSK                    0x200000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_21_SHFT                        0x15
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_20_BMSK                    0x100000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_20_SHFT                        0x14
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_19_BMSK                     0x80000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_19_SHFT                        0x13
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_18_BMSK                     0x40000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_18_SHFT                        0x12
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_17_BMSK                     0x20000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_17_SHFT                        0x11
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_16_BMSK                     0x10000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_16_SHFT                        0x10
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_15_BMSK                      0x8000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_15_SHFT                         0xf
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_14_BMSK                      0x4000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_14_SHFT                         0xe
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_13_BMSK                      0x2000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_13_SHFT                         0xd
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_12_BMSK                      0x1000
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_12_SHFT                         0xc
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_11_BMSK                       0x800
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_11_SHFT                         0xb
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_10_BMSK                       0x400
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_10_SHFT                         0xa
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_9_BMSK                        0x200
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_9_SHFT                          0x9
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_8_BMSK                        0x100
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_8_SHFT                          0x8
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_7_BMSK                         0x80
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_7_SHFT                          0x7
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_6_BMSK                         0x40
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_6_SHFT                          0x6
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_5_BMSK                         0x20
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_5_SHFT                          0x5
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_4_BMSK                         0x10
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_4_SHFT                          0x4
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_3_BMSK                          0x8
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_3_SHFT                          0x3
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_2_BMSK                          0x4
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_2_SHFT                          0x2
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_1_BMSK                          0x2
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_1_SHFT                          0x1
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_0_BMSK                          0x1
#define THROTTLE_SPARE_REGS_SPARE_REG_BIT_0_SHFT                          0x0

#endif /* __ICBCFG_HWIO_H__ */
