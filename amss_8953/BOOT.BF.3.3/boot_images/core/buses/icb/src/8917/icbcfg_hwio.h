#ifndef __ICBCFG_HWIO_H__
#define __ICBCFG_HWIO_H__
/*
===========================================================================
*/
/**
  @file icbcfg_hwio.h
  @brief Auto-generated HWIO interface include file.

*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/icb/src/8917/icbcfg_hwio.h#4 $
  $DateTime: 2015/12/21 03:35:11 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/* ICBCFG HWIO File for 8917 target */

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: OCIMEM_CSR
 *--------------------------------------------------------------------------*/

#define OCIMEM_CSR_REG_BASE                                         (OCIMEM_WRAPPER_CSR_BASE      + 0x00000000)

#define HWIO_OCIMEM_HW_VERSION_ADDR                                 (OCIMEM_CSR_REG_BASE      + 0x00000000)
#define HWIO_OCIMEM_HW_VERSION_RMSK                                 0xffffffff
#define HWIO_OCIMEM_HW_VERSION_IN          \
        in_dword_masked(HWIO_OCIMEM_HW_VERSION_ADDR, HWIO_OCIMEM_HW_VERSION_RMSK)
#define HWIO_OCIMEM_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_HW_VERSION_ADDR, m)
#define HWIO_OCIMEM_HW_VERSION_MAJOR_BMSK                           0xf0000000
#define HWIO_OCIMEM_HW_VERSION_MAJOR_SHFT                                 0x1c
#define HWIO_OCIMEM_HW_VERSION_MINOR_BMSK                            0xfff0000
#define HWIO_OCIMEM_HW_VERSION_MINOR_SHFT                                 0x10
#define HWIO_OCIMEM_HW_VERSION_STEP_BMSK                                0xffff
#define HWIO_OCIMEM_HW_VERSION_STEP_SHFT                                   0x0

#define HWIO_OCIMEM_HW_PROFILE_ADDR                                 (OCIMEM_CSR_REG_BASE      + 0x00000004)
#define HWIO_OCIMEM_HW_PROFILE_RMSK                                    0x1f1ff
#define HWIO_OCIMEM_HW_PROFILE_IN          \
        in_dword_masked(HWIO_OCIMEM_HW_PROFILE_ADDR, HWIO_OCIMEM_HW_PROFILE_RMSK)
#define HWIO_OCIMEM_HW_PROFILE_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_HW_PROFILE_ADDR, m)
#define HWIO_OCIMEM_HW_PROFILE_NUM_BANKS_BMSK                          0x1f000
#define HWIO_OCIMEM_HW_PROFILE_NUM_BANKS_SHFT                              0xc
#define HWIO_OCIMEM_HW_PROFILE_NUM_EXCL_MON_BMSK                         0x1ff
#define HWIO_OCIMEM_HW_PROFILE_NUM_EXCL_MON_SHFT                           0x0

#define HWIO_OCIMEM_GEN_CTL_ADDR                                    (OCIMEM_CSR_REG_BASE      + 0x00000008)
#define HWIO_OCIMEM_GEN_CTL_RMSK                                      0x1f000f
#define HWIO_OCIMEM_GEN_CTL_IN          \
        in_dword_masked(HWIO_OCIMEM_GEN_CTL_ADDR, HWIO_OCIMEM_GEN_CTL_RMSK)
#define HWIO_OCIMEM_GEN_CTL_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_GEN_CTL_ADDR, m)
#define HWIO_OCIMEM_GEN_CTL_OUT(v)      \
        out_dword(HWIO_OCIMEM_GEN_CTL_ADDR,v)
#define HWIO_OCIMEM_GEN_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_GEN_CTL_ADDR,m,v,HWIO_OCIMEM_GEN_CTL_IN)
#define HWIO_OCIMEM_GEN_CTL_EXMON_GRAN_MASK_BMSK                      0x1f0000
#define HWIO_OCIMEM_GEN_CTL_EXMON_GRAN_MASK_SHFT                          0x10
#define HWIO_OCIMEM_GEN_CTL_CLREXMONREQ_PULSE_EN_BMSK                      0x8
#define HWIO_OCIMEM_GEN_CTL_CLREXMONREQ_PULSE_EN_SHFT                      0x3
#define HWIO_OCIMEM_GEN_CTL_EXMON_GRAN_128B_EN_BMSK                        0x4
#define HWIO_OCIMEM_GEN_CTL_EXMON_GRAN_128B_EN_SHFT                        0x2
#define HWIO_OCIMEM_GEN_CTL_CSR_AHB_BRIDGE_POSTWR_DIS_BMSK                 0x2
#define HWIO_OCIMEM_GEN_CTL_CSR_AHB_BRIDGE_POSTWR_DIS_SHFT                 0x1
#define HWIO_OCIMEM_GEN_CTL_SW_STM_FLAG_BMSK                               0x1
#define HWIO_OCIMEM_GEN_CTL_SW_STM_FLAG_SHFT                               0x0

#define HWIO_OCIMEM_GEN_STAT_ADDR                                   (OCIMEM_CSR_REG_BASE      + 0x0000000c)
#define HWIO_OCIMEM_GEN_STAT_RMSK                                          0x3
#define HWIO_OCIMEM_GEN_STAT_IN          \
        in_dword_masked(HWIO_OCIMEM_GEN_STAT_ADDR, HWIO_OCIMEM_GEN_STAT_RMSK)
#define HWIO_OCIMEM_GEN_STAT_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_GEN_STAT_ADDR, m)
#define HWIO_OCIMEM_GEN_STAT_AXI_ERR_SLEEP_BMSK                            0x2
#define HWIO_OCIMEM_GEN_STAT_AXI_ERR_SLEEP_SHFT                            0x1
#define HWIO_OCIMEM_GEN_STAT_AXI_ERR_DECODE_BMSK                           0x1
#define HWIO_OCIMEM_GEN_STAT_AXI_ERR_DECODE_SHFT                           0x0

#define HWIO_OCIMEM_INTC_CLR_ADDR                                   (OCIMEM_CSR_REG_BASE      + 0x00000010)
#define HWIO_OCIMEM_INTC_CLR_RMSK                                          0x1
#define HWIO_OCIMEM_INTC_CLR_IN          \
        in_dword_masked(HWIO_OCIMEM_INTC_CLR_ADDR, HWIO_OCIMEM_INTC_CLR_RMSK)
#define HWIO_OCIMEM_INTC_CLR_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_INTC_CLR_ADDR, m)
#define HWIO_OCIMEM_INTC_CLR_OUT(v)      \
        out_dword(HWIO_OCIMEM_INTC_CLR_ADDR,v)
#define HWIO_OCIMEM_INTC_CLR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_INTC_CLR_ADDR,m,v,HWIO_OCIMEM_INTC_CLR_IN)
#define HWIO_OCIMEM_INTC_CLR_AXI_ERR_INT_CLR_BMSK                          0x1
#define HWIO_OCIMEM_INTC_CLR_AXI_ERR_INT_CLR_SHFT                          0x0

#define HWIO_OCIMEM_INTC_MASK_ADDR                                  (OCIMEM_CSR_REG_BASE      + 0x00000014)
#define HWIO_OCIMEM_INTC_MASK_RMSK                                         0x1
#define HWIO_OCIMEM_INTC_MASK_IN          \
        in_dword_masked(HWIO_OCIMEM_INTC_MASK_ADDR, HWIO_OCIMEM_INTC_MASK_RMSK)
#define HWIO_OCIMEM_INTC_MASK_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_INTC_MASK_ADDR, m)
#define HWIO_OCIMEM_INTC_MASK_OUT(v)      \
        out_dword(HWIO_OCIMEM_INTC_MASK_ADDR,v)
#define HWIO_OCIMEM_INTC_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_INTC_MASK_ADDR,m,v,HWIO_OCIMEM_INTC_MASK_IN)
#define HWIO_OCIMEM_INTC_MASK_AXI_ERR_INT_MSK_BMSK                         0x1
#define HWIO_OCIMEM_INTC_MASK_AXI_ERR_INT_MSK_SHFT                         0x0

#define HWIO_OCIMEM_INTC_STAT_ADDR                                  (OCIMEM_CSR_REG_BASE      + 0x00000018)
#define HWIO_OCIMEM_INTC_STAT_RMSK                                         0x1
#define HWIO_OCIMEM_INTC_STAT_IN          \
        in_dword_masked(HWIO_OCIMEM_INTC_STAT_ADDR, HWIO_OCIMEM_INTC_STAT_RMSK)
#define HWIO_OCIMEM_INTC_STAT_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_INTC_STAT_ADDR, m)
#define HWIO_OCIMEM_INTC_STAT_AXI_ERR_INT_STAT_BMSK                        0x1
#define HWIO_OCIMEM_INTC_STAT_AXI_ERR_INT_STAT_SHFT                        0x0

#define HWIO_OCIMEM_OSW_STATUS_ADDR                                 (OCIMEM_CSR_REG_BASE      + 0x0000001c)
#define HWIO_OCIMEM_OSW_STATUS_RMSK                                       0x3f
#define HWIO_OCIMEM_OSW_STATUS_IN          \
        in_dword_masked(HWIO_OCIMEM_OSW_STATUS_ADDR, HWIO_OCIMEM_OSW_STATUS_RMSK)
#define HWIO_OCIMEM_OSW_STATUS_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_OSW_STATUS_ADDR, m)
#define HWIO_OCIMEM_OSW_STATUS_OSW_RRESP_FIFO_FULL_BMSK                   0x20
#define HWIO_OCIMEM_OSW_STATUS_OSW_RRESP_FIFO_FULL_SHFT                    0x5
#define HWIO_OCIMEM_OSW_STATUS_OSW_WRESP_FIFO_FULL_BMSK                   0x10
#define HWIO_OCIMEM_OSW_STATUS_OSW_WRESP_FIFO_FULL_SHFT                    0x4
#define HWIO_OCIMEM_OSW_STATUS_OSW_RDDATA_FIFO_FULL_BMSK                   0x8
#define HWIO_OCIMEM_OSW_STATUS_OSW_RDDATA_FIFO_FULL_SHFT                   0x3
#define HWIO_OCIMEM_OSW_STATUS_OSW_WRDATA_FIFO_FULL_BMSK                   0x4
#define HWIO_OCIMEM_OSW_STATUS_OSW_WRDATA_FIFO_FULL_SHFT                   0x2
#define HWIO_OCIMEM_OSW_STATUS_OSW_CMDFIFO_FULL_BMSK                       0x2
#define HWIO_OCIMEM_OSW_STATUS_OSW_CMDFIFO_FULL_SHFT                       0x1
#define HWIO_OCIMEM_OSW_STATUS_OSW_STAT_IDLE_BMSK                          0x1
#define HWIO_OCIMEM_OSW_STATUS_OSW_STAT_IDLE_SHFT                          0x0

#define HWIO_OCIMEM_PSCGC_TIMERS_ADDR                               (OCIMEM_CSR_REG_BASE      + 0x00000034)
#define HWIO_OCIMEM_PSCGC_TIMERS_RMSK                                    0xf0f
#define HWIO_OCIMEM_PSCGC_TIMERS_IN          \
        in_dword_masked(HWIO_OCIMEM_PSCGC_TIMERS_ADDR, HWIO_OCIMEM_PSCGC_TIMERS_RMSK)
#define HWIO_OCIMEM_PSCGC_TIMERS_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_PSCGC_TIMERS_ADDR, m)
#define HWIO_OCIMEM_PSCGC_TIMERS_OUT(v)      \
        out_dword(HWIO_OCIMEM_PSCGC_TIMERS_ADDR,v)
#define HWIO_OCIMEM_PSCGC_TIMERS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_PSCGC_TIMERS_ADDR,m,v,HWIO_OCIMEM_PSCGC_TIMERS_IN)
#define HWIO_OCIMEM_PSCGC_TIMERS_TO_SLEEP_COUNTER_BMSK                   0xf00
#define HWIO_OCIMEM_PSCGC_TIMERS_TO_SLEEP_COUNTER_SHFT                     0x8
#define HWIO_OCIMEM_PSCGC_TIMERS_WAKEUP_COUNTER_BMSK                       0xf
#define HWIO_OCIMEM_PSCGC_TIMERS_WAKEUP_COUNTER_SHFT                       0x0

#define HWIO_OCIMEM_PSCGC_STAT_ADDR                                 (OCIMEM_CSR_REG_BASE      + 0x00000038)
#define HWIO_OCIMEM_PSCGC_STAT_RMSK                                        0x1
#define HWIO_OCIMEM_PSCGC_STAT_IN          \
        in_dword_masked(HWIO_OCIMEM_PSCGC_STAT_ADDR, HWIO_OCIMEM_PSCGC_STAT_RMSK)
#define HWIO_OCIMEM_PSCGC_STAT_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_PSCGC_STAT_ADDR, m)
#define HWIO_OCIMEM_PSCGC_STAT_M0_PSCGC_CLKOFF_BMSK                        0x1
#define HWIO_OCIMEM_PSCGC_STAT_M0_PSCGC_CLKOFF_SHFT                        0x0

#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_ADDR                            (OCIMEM_CSR_REG_BASE      + 0x0000003c)
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_RMSK                                   0x7
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_IN          \
        in_dword_masked(HWIO_OCIMEM_PSCGC_M0_M7_CTL_ADDR, HWIO_OCIMEM_PSCGC_M0_M7_CTL_RMSK)
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_PSCGC_M0_M7_CTL_ADDR, m)
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_OUT(v)      \
        out_dword(HWIO_OCIMEM_PSCGC_M0_M7_CTL_ADDR,v)
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_PSCGC_M0_M7_CTL_ADDR,m,v,HWIO_OCIMEM_PSCGC_M0_M7_CTL_IN)
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_M0_PSCGC_CTL_BMSK                      0x7
#define HWIO_OCIMEM_PSCGC_M0_M7_CTL_M0_PSCGC_CTL_SHFT                      0x0

#define HWIO_OCIMEM_ERR_ADDRESS_ADDR                                (OCIMEM_CSR_REG_BASE      + 0x00000060)
#define HWIO_OCIMEM_ERR_ADDRESS_RMSK                                0xffffffff
#define HWIO_OCIMEM_ERR_ADDRESS_IN          \
        in_dword_masked(HWIO_OCIMEM_ERR_ADDRESS_ADDR, HWIO_OCIMEM_ERR_ADDRESS_RMSK)
#define HWIO_OCIMEM_ERR_ADDRESS_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_ERR_ADDRESS_ADDR, m)
#define HWIO_OCIMEM_ERR_ADDRESS_ERROR_ADDRESS_BMSK                  0xffffffff
#define HWIO_OCIMEM_ERR_ADDRESS_ERROR_ADDRESS_SHFT                         0x0

#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ADDR                           (OCIMEM_CSR_REG_BASE      + 0x00000064)
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_RMSK                           0xffff7f3f
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_IN          \
        in_dword_masked(HWIO_OCIMEM_AXI_ERR_SYNDROME_ADDR, HWIO_OCIMEM_AXI_ERR_SYNDROME_RMSK)
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_AXI_ERR_SYNDROME_ADDR, m)
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ABID_BMSK                0xe0000000
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ABID_SHFT                      0x1d
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_APID_BMSK                0x1f000000
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_APID_SHFT                      0x18
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_AMID_BMSK                  0xff0000
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_AMID_SHFT                      0x10
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ATID_BMSK                    0x7f00
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ATID_SHFT                       0x8
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ABURST_BMSK                    0x20
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ABURST_SHFT                     0x5
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_AWRITE_BMSK                    0x10
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_AWRITE_SHFT                     0x4
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ALEN_BMSK                       0xf
#define HWIO_OCIMEM_AXI_ERR_SYNDROME_ERROR_ALEN_SHFT                       0x0

#define HWIO_OCIMEM_DEBUG_CTL_ADDR                                  (OCIMEM_CSR_REG_BASE      + 0x00000068)
#define HWIO_OCIMEM_DEBUG_CTL_RMSK                                       0x10f
#define HWIO_OCIMEM_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_OCIMEM_DEBUG_CTL_ADDR, HWIO_OCIMEM_DEBUG_CTL_RMSK)
#define HWIO_OCIMEM_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_OCIMEM_DEBUG_CTL_ADDR, m)
#define HWIO_OCIMEM_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_OCIMEM_DEBUG_CTL_ADDR,v)
#define HWIO_OCIMEM_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OCIMEM_DEBUG_CTL_ADDR,m,v,HWIO_OCIMEM_DEBUG_CTL_IN)
#define HWIO_OCIMEM_DEBUG_CTL_DEBUG_BUS_EN_BMSK                          0x100
#define HWIO_OCIMEM_DEBUG_CTL_DEBUG_BUS_EN_SHFT                            0x8
#define HWIO_OCIMEM_DEBUG_CTL_DEBUG_BUS_SEL_BMSK                           0xf
#define HWIO_OCIMEM_DEBUG_CTL_DEBUG_BUS_SEL_SHFT                           0x0

#endif /* __ICBCFG_HWIO_H__ */
