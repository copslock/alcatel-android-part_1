/**
 * @file:  SpmiMaster.h
 * @brief: Configures the Spmi Master Controller
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/core/hal/bear/SpmiMaster.h#1 $
 * $Change: 8504943 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */
#ifndef SPMIMASTER_H
#define	SPMIMASTER_H

#include "SpmiBusCfg.h"
#include "SpmiUtils.h"

Spmi_Result SpmiMaster_ConfigHW(uint8 bid, uint8 masterId, SpmiBusCfg_ConfigType cfgType);
void SpmiMaster_LoadFirmware(uint8 bid);
void SpmiMaster_LoadRadixTree(uint8 bid, SpmiBusCfg_ChannelCfg* entries, uint32 numEntries);

#endif

