/**
 * @file:  SpmiCfg.c
 * 
 * Copyright (c) 2013-2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/core/hal/bear/SpmiBusCfg.c#1 $
 * $Change: 8504943 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 04/6/15  Multiple bus support
 * 11/3/14  Automatic channel assignment
 * 9/2/14   Enabled security features when channel info is set
 * 10/1/13  Initial Version
*/

#include <stdlib.h>
#include "SpmiBusCfg.h"
#include "SpmiCfgInternal.h"
#include "SpmiUtils.h"
#include "SpmiLogs.h"
#include "SpmiHal.h"
#include "SpmiMaster.h"
#include "PmicArb.h"
#include "SpmiOs.h"

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

#define DEFAULT_MASTER_ID 0
#define INIT_CHECK() do { if(!initialized) { return SPMI_FAILURE_NOT_INITIALIZED; } } while(FALSE)

//******************************************************************************
// Global Data
//******************************************************************************

static boolean initialized = FALSE;
static uint16 nextChanIdx[SWIO_MAX_BUSES_SUPPORTED] = {0};
static boolean dynamicChannelMode[SWIO_MAX_BUSES_SUPPORTED] = {FALSE};
static SpmiOs_ReservedChannels* reservedChans[SWIO_MAX_BUSES_SUPPORTED] = {NULL};

//******************************************************************************
// Local Helper Functions
//******************************************************************************

typedef boolean (*CmprFunc)(const SpmiBusCfg_ChannelCfg*, const SpmiBusCfg_ChannelCfg*);

static boolean ppidCmpr(const SpmiBusCfg_ChannelCfg* c1, const SpmiBusCfg_ChannelCfg* c2)
{
    if(c2->slaveId == c1->slaveId) {
        return c2->periphId > c1->periphId;
    }
    
    return c2->slaveId > c1->slaveId;
}

static boolean ownerCmpr(const SpmiBusCfg_ChannelCfg* c1, const SpmiBusCfg_ChannelCfg* c2)
{    
    if(c1->periphOwner == c2->periphOwner) {
        return c1->intOwner > c2->intOwner;
    }
    
    return c1->periphOwner > c2->periphOwner;
}

static void shellSort(SpmiBusCfg_ChannelCfg* entries, uint32 numEntries, CmprFunc cmpr)
{
    // https://en.wikipedia.org/wiki/Shellsort
    static const uint8 gaps[] = {57, 23, 10, 4, 1};
    uint32 i, k;
    
    for(i = 0; i < sizeof(gaps) / sizeof(uint8); i++) 
    {
        uint8 gap = gaps[i];
        
        for(k = gap; k < numEntries; k++)
        {
            SpmiBusCfg_ChannelCfg tmp = entries[k];
            uint32 j = k;
            
            while(j >= gap && cmpr( &entries[j - gap], &tmp ))
            {
                entries[j] = entries[j-gap];
                j -= gap;
            }
            
            entries[j] = tmp;
        }
    }
}

static Spmi_Result assignChannel(uint8 busId, SpmiBusCfg_ChannelCfg* entry, boolean autoAssignChan)
{
    boolean chanReserved;
    SpmiOs_ReservedChannels* reserved = reservedChans[busId];
    
    do
    {
        chanReserved = FALSE;
        
        if(autoAssignChan) {
            entry->channel = nextChanIdx[busId];
        }

        if(entry->channel > SWIO_PMIC_ARB_REG_CHNLn_MAXn( busId ))
        {
            if(autoAssignChan && dynamicChannelMode[busId])
            {
                SPMI_LOG_VERBOSE( "Exhausted channels. Rolling over." );
                entry->channel = 0;
            }
            else {
                return SPMI_FAILURE_INVALID_CHAN_NUM;
            }
        }
     
        nextChanIdx[busId] = entry->channel + 1;
        
        if(reserved != NULL) 
        {
            uint32 i;
            
            for(i = 0; i < reserved->numReserved; i++) 
            {
                if(entry->channel == reserved->reserved[i]) {
                    chanReserved = TRUE;
                    break;
                }
            }
        }
        
        if(!autoAssignChan && chanReserved) {
            return SPMI_FAILURE_CHANNEL_RESERVED;
        }
        
    } while(autoAssignChan && chanReserved);
    
    return SPMI_SUCCESS;
}

static Spmi_Result configChannels(uint8 busId, SpmiBusCfg_ChannelCfg* entries, uint32 numEntries, 
                                  SpmiBusCfg_RGConfig* rgCfg, uint32* rgCount, 
                                  boolean autoAssignChan, boolean configHW, boolean legacySorted)
{
    uint32 i;
    uint32 rgIdx = 0;
    Spmi_Result rslt;
    SpmiBusCfg_RGConfig channelRG;
    SpmiBusCfg_RGConfig interruptRG;
    SpmiBusCfg_ChannelCfg* entry = NULL;
    
    INIT_CHECK();
    
    if(entries == NULL || numEntries == 0 || (rgCfg != NULL && rgCount == NULL)) {
        return SPMI_FAILURE_INVALID_PARAMETER;
    }
    
    if(numEntries > SWIO_MAX_CHANNELS_SUPPORTED) {
        return SPMI_FAILURE_TOO_MANY_ENTRIES;
    }
    
    if(autoAssignChan) {
        nextChanIdx[busId] = 0;
    }
    
    if(configHW)
    {
        // Zero table
        for (i = 0; i <= SWIO_PMIC_ARB_REG_CHNLn_MAXn(busId); i++) {
            SPMI_HWIO_OUT( SWIO_PMIC_ARB_REG_CHNLn_ADDR( busId, i ), 0 );
        }
    }
    
    if(rgCfg != NULL)
    {
        shellSort(entries, numEntries, ownerCmpr);

        channelRG.owner = entries[0].periphOwner;
        channelRG.startIdx = 0;
        channelRG.startAddr = (uint32) SWIO_PMIC_ARBq_CHNLn_CMD_ADDR( busId, 0, 0 );

        interruptRG.owner = entries[0].intOwner;
        interruptRG.startIdx = 0;
        interruptRG.startAddr = (uint32) SWIO_SPMI_PIC_ACC_ENABLEn_ADDR( busId, 0 );
    }
    
    for (i = 0; i < numEntries; i++)
    {
        entry = &entries[i];

        if(entry->slaveId > SPMI_MAX_SLAVE_ID) {
            SPMI_LOG_ERROR( "Invalid slaveId: %d", entry->slaveId );
            return SPMI_FAILURE_INVALID_SLAVE_ID;
        }
        
        if(entry->intOwner > SWIO_PMIC_ARBq_CHNLn_CMD_MAXq( busId )) {
            SPMI_LOG_ERROR( "Invalid intOwner: %d", entry->intOwner );
            return SPMI_FAILURE_INVALID_OWNER;
        }
        
        if(rgCfg != NULL &&
           entry->periphOwner > SWIO_PMIC_ARBq_CHNLn_CMD_MAXq( busId ) && 
           entry->periphOwner != SPMI_OPEN_OWNER) 
        {
            SPMI_LOG_ERROR( "Invalid periphOwner: %d", entry->periphOwner );
            return SPMI_FAILURE_INVALID_OWNER;
        }
        
        if(configHW) {
            rslt = SpmiBusCfg_ConfigureChannel( busId, entry, autoAssignChan );
        }
        else {
            rslt = assignChannel( busId, entry, autoAssignChan );
        }
        
        if(rslt != SPMI_SUCCESS) {
            return rslt;
        }
        
        if(rgCfg != NULL)
        {
            if(entry->periphOwner != channelRG.owner)
            {
                if(rgIdx == *rgCount) {
                    return SPMI_FAILURE_NOT_ENOUGH_RGS;
                }
                
                channelRG.endIdx = entry->channel - 1;
                channelRG.size = (uint32) SWIO_PMIC_ARBq_CHNLn_CMD_ADDR( busId, 0, entry->channel ) - channelRG.startAddr;
                rgCfg[rgIdx++] = channelRG;

                channelRG.owner = entry->periphOwner;
                channelRG.startIdx = entry->channel;
                channelRG.startAddr = (uint32) SWIO_PMIC_ARBq_CHNLn_CMD_ADDR( busId, 0, entry->channel );
            }

            if(entry->intOwner != interruptRG.owner)
            {
                if(rgIdx == *rgCount) {
                    return SPMI_FAILURE_NOT_ENOUGH_RGS;
                }
                
                interruptRG.endIdx = entry->channel - 1;
                interruptRG.size = (uint32) SWIO_SPMI_PIC_ACC_ENABLEn_ADDR( busId, entry->channel ) - interruptRG.startAddr;
                rgCfg[rgIdx++] = interruptRG;

                interruptRG.owner = entry->intOwner;
                interruptRG.startIdx = entry->channel;
                interruptRG.startAddr = (uint32) SWIO_SPMI_PIC_ACC_ENABLEn_ADDR( busId, entry->channel );
            }
        }
    }

    if(rgCfg != NULL)
    {
        channelRG.endIdx = entry->channel;
        channelRG.size = (uint32) SWIO_PMIC_ARBq_CHNLn_CMD_ADDR( busId, 0, entry->channel + 1 ) - channelRG.startAddr;

        if(channelRG.owner != SPMI_OPEN_OWNER)
        {
            if(rgIdx == *rgCount) {
                return SPMI_FAILURE_NOT_ENOUGH_RGS;
            }
            
            rgCfg[rgIdx++] = channelRG;
        }

        if(rgIdx == *rgCount) {
            return SPMI_FAILURE_NOT_ENOUGH_RGS;
        }

        interruptRG.endIdx = entry->channel;
        interruptRG.size = (uint32) SWIO_SPMI_PIC_ACC_ENABLEn_ADDR( busId, entry->channel + 1 ) - interruptRG.startAddr;
        rgCfg[rgIdx++] = interruptRG;
    }
    
    if(rgCount != NULL) {
        *rgCount = rgIdx;
    }
    
    if(configHW && !dynamicChannelMode[busId])
    {
        // Load the SPMI radix tree
        if(!legacySorted) {
            shellSort(entries, numEntries, ppidCmpr);
        }
        SpmiMaster_LoadRadixTree( busId, entries, numEntries );
    
        // Enable security now that the tables are configured
        SPMI_HWIO_OUT_SET( SWIO_SPMI_CMPR_EN_REG_ADDR(busId), SWIO_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK(busId) );
        SPMI_HWIO_OUT_CLEAR( SWIO_SPMI_SEC_DISABLE_REG_ADDR(busId), SWIO_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK(busId) );

        // Enable interrupt processing
        SPMI_HWIO_OUT_SET( SWIO_SPMI_MWB_ENABLE_REG_ADDR(busId), SWIO_SPMI_MWB_ENABLE_REG_MWB_ENABLE_BMSK(busId) );
    }
    
    return SPMI_SUCCESS;
}

//******************************************************************************
// Public API Functions
//******************************************************************************

Spmi_Result SpmiBusCfg_Init(boolean initHw)
{
    uint32 i;
    Spmi_Result rslt;
    
    if (!initialized) 
    {
        SPMI_LOG_INIT();

        if ((rslt = SpmiHal_Init()) != SPMI_SUCCESS) {
            return rslt;
        }
        
        initialized = TRUE;
    }
    
    for(i = 0; i < SWIO_MAX_BUSES_SUPPORTED; i++) 
    {
        reservedChans[i] = SpmiOs_GetReservedChannels( i );
        
        if(initHw) 
        {
            if((rslt = SpmiMaster_ConfigHW( i, DEFAULT_MASTER_ID, SPMI_CFG_FULL )) != SPMI_SUCCESS) {
                return rslt;
            }
        }
    }

    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_DeInit()
{
    initialized = FALSE;
    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_GetChannelInfo(uint8 busId, uint16 channel, uint8* slaveId, uint8* periphId, uint8* owner)
{
    INIT_CHECK();
    
    if(channel > SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_MAXm( busId )) {
        return SPMI_FAILURE_INVALID_CHAN_NUM;
    }
    
    *slaveId = SPMI_SWIO_IN_FIELD( busId,
                                   SWIO_PMIC_ARB_REG_CHNLn_ADDR( busId, channel ),
                                   SWIO_PMIC_ARB_REG_CHNLn_SID );
    
    *periphId = SPMI_SWIO_IN_FIELD( busId,
                                    SWIO_PMIC_ARB_REG_CHNLn_ADDR( busId, channel ),
                                    SWIO_PMIC_ARB_REG_CHNLn_ADDRESS );
    
    *owner = SPMI_SWIO_IN_FIELD( busId,
                                 SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR( busId, channel ),
                                 SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER );
    
    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_GetPeripherialInfo(uint8 busId, uint8 slaveId, uint8 periphId, uint16* channel, uint8* owner)
{
    uint32 i;    
    INIT_CHECK();
    
    if(slaveId > SPMI_MAX_SLAVE_ID) {
        return SPMI_FAILURE_INVALID_SLAVE_ID;
    }
    
    uint16 ppid = SLAVE_PERIPH_TO_PPID( slaveId, periphId );

    for(i = 0; i <= SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_MAXm( busId ); i++)
    {
        if(ppid == SPMI_SWIO_IN_FIELD( busId,
                                       SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR( busId, i ), 
                                       SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID ))
        {
            *channel = i;
            *owner = SPMI_SWIO_IN_FIELD( busId,
                                         SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR( busId, i ),
                                         SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER );
            return SPMI_SUCCESS;
        }   
    }

    return SPMI_FAILURE_CHANNEL_NOT_FOUND;
}

boolean SpmiBusCfg_InDynamicChannelMode(uint8 busId)
{
    return dynamicChannelMode[busId];
}

Spmi_Result SpmiBusCfg_SetDynamicChannelMode(uint8 busId, boolean enabled)
{
    // If we are changing the channel -> ppid mapping, the security 
    // and interrupt tables wont be in sync (assuming they were populated)
    // and we dont have the information to update them; so interrupts and security aren't supported.
    if(enabled) 
    {
        SPMI_LOG_WARNING( "Disabling security and interrupts" );
        SPMI_HWIO_OUT_CLEAR( SWIO_SPMI_CMPR_EN_REG_ADDR( busId ), SWIO_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK( busId ) );
        SPMI_HWIO_OUT_SET( SWIO_SPMI_SEC_DISABLE_REG_ADDR( busId ), SWIO_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK( busId ) );
        SPMI_HWIO_OUT_CLEAR( SWIO_SPMI_MWB_ENABLE_REG_ADDR( busId ), SWIO_SPMI_MWB_ENABLE_REG_MWB_ENABLE_BMSK( busId ) );
    }
    
    dynamicChannelMode[busId] = enabled;
    
    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_ConfigureChannel(uint8 busId, SpmiBusCfg_ChannelCfg* entry, boolean autoAssignChan)
{
    Spmi_Result rslt;
    uint32 reg;
    
    if((rslt = assignChannel( busId, entry, autoAssignChan )) != SPMI_SUCCESS) {
        return rslt;
    }
    
    // Update the channel address translation table - used by owner and observer channels
    reg = SPMI_SWIO_SET_FIELD_VALUE( busId, entry->slaveId, SWIO_PMIC_ARB_REG_CHNLn_SID );
    reg |= SPMI_SWIO_SET_FIELD_VALUE( busId, entry->periphId, SWIO_PMIC_ARB_REG_CHNLn_ADDRESS );
    
    SPMI_HWIO_OUT( SWIO_PMIC_ARB_REG_CHNLn_ADDR( busId, entry->channel ), reg );

    if(!dynamicChannelMode[busId])
    {
        // Update peripheral interrupt ownership table
        reg = SPMI_SWIO_SET_FIELD_VALUE( busId, SLAVE_PERIPH_TO_PPID( entry->slaveId, entry->periphId ),
                                         SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID );

        reg |= SPMI_SWIO_SET_FIELD_VALUE( busId, entry->intOwner,
                                          SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER );

        SPMI_HWIO_OUT( SWIO_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR( busId, entry->channel ), reg );
    }
    
    SPMI_LOG_VERBOSE( "%sBus %d, PPID %01X %02X (intOwner %d, chanOwner %d) @ chan %d",
                      dynamicChannelMode[busId] ? "*" : "", busId,
                      entry->slaveId, entry->periphId, entry->intOwner, 
                      entry->periphOwner, entry->channel );
    
    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_ConfigureChannels(SpmiBusCfg_ChannelCfg* entries, uint32 numEntries)
{
    return configChannels( 0, entries, numEntries, NULL, NULL, FALSE, TRUE, TRUE );
}

Spmi_Result SpmiBusCfg_ConfigurePeripherals(uint8 busId, SpmiBusCfg_ChannelCfg* entries, uint32 numEntries, 
                                            SpmiBusCfg_RGConfig* rgCfg, uint32* rgCount)
{
    return configChannels( busId, entries, numEntries, rgCfg, rgCount, TRUE, TRUE, FALSE );
}

Spmi_Result SpmiBusCfg_CalculateRGConfig(uint8 busId, SpmiBusCfg_ChannelCfg* entries, uint32 numEntries, 
                                         SpmiBusCfg_RGConfig* rgCfg, uint32* rgCount)
{
    return configChannels( busId, entries, numEntries, rgCfg, rgCount, TRUE, FALSE, FALSE );
}

Spmi_Result SpmiBusCfg_ConfigurePvcPort(uint8 busId, const SpmiBusCfg_PvcPortCfg* portCfg)
{
    uint32 i;
    INIT_CHECK();

    if(portCfg->pvcPortId > SWIO_PMIC_ARB_PVC_PORTn_CTL_MAXn( busId ) || 
       portCfg->pvcPortId > SWIO_PMIC_ARB_PVCn_ADDRm_MAXn( busId )) 
    {
        return SPMI_FAILURE_INVALID_PORT_ID;
    }
    
    SPMI_SWIO_OUT_FIELD( busId,
                         SWIO_PMIC_ARB_PVC_PORTn_CTL_ADDR( busId, portCfg->pvcPortId ), 
                         SWIO_PMIC_ARB_PVC_PORTn_CTL_SPMI_PRIORITY,
                         portCfg->priority );

    if(portCfg->numPpids > SWIO_PMIC_ARB_PVCn_ADDRm_MAXm( busId ) + 1) {
        return SPMI_FAILURE_TOO_MANY_ENTRIES;
    }
    
    for(i = 0; i < portCfg->numPpids; i++) 
    {
        if(portCfg->ppids[i].slaveId > SPMI_MAX_SLAVE_ID) {
            return SPMI_FAILURE_INVALID_SLAVE_ID;
        }

        SPMI_SWIO_OUT_FIELD( busId,
                             SWIO_PMIC_ARB_PVCn_ADDRm_ADDR( busId, portCfg->pvcPortId, i ), 
                             SWIO_PMIC_ARB_PVCn_ADDRm_SID, 
                             portCfg->ppids[i].slaveId );
        
        SPMI_SWIO_OUT_FIELD( busId,
                             SWIO_PMIC_ARB_PVCn_ADDRm_ADDR( busId, portCfg->pvcPortId, i ), 
                             SWIO_PMIC_ARB_PVCn_ADDRm_ADDRESS, 
                             portCfg->ppids[i].address );
    }

    // Enable the individual port
    SPMI_HWIO_OUT_SET( SWIO_PMIC_ARB_PVC_PORTn_CTL_ADDR( busId, portCfg->pvcPortId ),
                       SWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_BMSK( busId ) );

    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_SetPortPriorities(uint8 busId, uint8* ports, uint32 numPorts)
{
    uint32 i;
    
    INIT_CHECK();
    
    if(numPorts > SWIO_PMIC_ARB_PRIORITIESn_MAXn( busId ) + 1) {
        return SPMI_FAILURE_TOO_MANY_ENTRIES;
    }
    
    for(i = 0; i < numPorts; i++) 
    {
        if(ports[i] > (SWIO_PMIC_ARB_PRIORITIESn_PORT_BMSK( busId ) >> SWIO_PMIC_ARB_PRIORITIESn_PORT_SHFT( busId ))) {
            return SPMI_FAILURE_INVALID_PORT_ID;
        }

        SPMI_SWIO_OUT_FIELD( busId, 
                             SWIO_PMIC_ARB_PRIORITIESn_ADDR( busId, i ), 
                             SWIO_PMIC_ARB_PRIORITIESn_PORT,
                             ports[i] );
    }
    
    return SPMI_SUCCESS;
}

Spmi_Result SpmiBusCfg_SetPVCPortsEnabled(uint8 busId, boolean enable)
{
    INIT_CHECK();
    
    SPMI_SWIO_OUT_FIELD( busId, 
                         SWIO_PMIC_ARB_PVC_INTF_CTL_ADDR( busId ), 
                         SWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN, 
                         enable );
    return SPMI_SUCCESS;
}
