/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/07/02 04:11:47 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/buses/spmi/src/platform/os/sbl/SpmiOsLogs.c#1 $
 * $Change: 8504943 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

char spmiLogBuf[SPMI_MAX_LOG_LEN];
