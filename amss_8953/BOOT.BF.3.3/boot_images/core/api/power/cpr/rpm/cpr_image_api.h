/**
 * @file:  cpr_image_api.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 01:39:35 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/api/power/cpr/rpm/cpr_image_api.h#2 $
 * $Change: 9714790 $
 */
#ifndef CPR_IMAGE_API_H
#define CPR_IMAGE_API_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "DALStdDef.h"

//This function returns the hash of the CPR settings that are related to Cx/Mx.
//On targets with fixed Mx voltages, this hash will factor in the static table of
//Mx voltages as well as the Cx CPR settings.
//On targets with CPR-managed Mx voltages, this hash will factor in the
//Mx CPR settings.
uint32 cpr_cx_mx_settings_checksum(void);

//Function to be called after SMEM is initialized to push out the CPR settings
//to SMEM. These settings are to be picked up by the RPM CPR driver during boot.
//Must be done before the RPM FW execution begins.
void cpr_externalize_state(void);

#ifdef __cplusplus
};
#endif // __cplusplus

#endif // CPR_IMAGE_API_H
