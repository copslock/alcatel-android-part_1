#ifndef __PM_FG_SOC_MEMIF_H__
#define __PM_FG_SOC_MEMIF_H__


/*! \file pm_fg_memif.h 
 *  \n
 *  \brief  PMIC-FG MODULE RELATED DECLARATION
 *  \details  This file contains functions and variable declarations to support 
 *   the PMIC FG Fule Gauge memory interface module.
 *
 *  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
 *  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/api/systemdrivers/pmic/pm_fg_memif.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/25/14   va      Driver update, added protected call
04/14/14   va      Initial Release 
===========================================================================*/

#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"

/*===========================================================================
                        TYPE DEFINITIONS 
===========================================================================*/

/* Common Interrupt Prototype */
typedef enum
{
  /* 1=indicates that s/w allowed to update FG ram contents to assist in boot
      0x0 : DATA_RCVRY_SUG_FALSE
      0x1 : DATA_RCVRY_SUG_TRUE  */
  PM_FG_MEMIF_FG_MEM_AVAIL_RT_STS=0, 
  /*1= S/W allowed to access FG memory
    0x0 : FG_MEM_AVAIL_FALSE
    0x1 : FG_MEM_AVAIL_TRUE */
  PM_FG_MEMIF_DATA_RCVRY_SUG_RT_STS,
  PM_FG_IRQ_MEMIF_TYPE_INVALID
} pm_fg_memif_irq_type;

typedef enum{
  /* Selects the MID that will receive the interrupt */
  PM_FG_MEMIF_INT_MID_SEL_MID0,
  PM_FG_MEMIF_INT_MID_SEL_MID1,
  PM_FG_MEMIF_INT_MID_SEL_MID2,
  PM_FG_MEMIF_INT_MID_SEL_MID3,
  PM_FG_MEMIF_INT_MID_SEL_INVALID
}pm_fg_memif_int_mid_sel;

/* End of Common Interrupt */

typedef enum
{
  /* 1: RIF is granted access to FG memory any time during long CC conversion period rather than at the beginning of this period.
      Helps in reducing latency for access to FG Memory; Has potential to extend FG cycle and upset SoC estimation. To be used sparingly, preferably at boot only
      0x0 : LOW_LATENCY_ACS_EN_FALSE
      0x1 : LOW_LATENCY_ACS_EN_TRUE */
  PM_FG_MEMIF_MEM_INTF_CFG_LOW_LATENCY_ACS_EN=0x06, 
  /* Enables RIF memory interface and the RIF Memory Access Mode.  1 = Mem access requested  0 = Mem not needed.
      0x0 : RIF_MEM_ACCESS_REQ_FALSE
      0x1 : RIF_MEM_ACCESS_REQ_TRUE */
  PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ,
  PM_FG_MEMIF_MEM_INTF_CFG_INVALID
} pm_fg_memif_mem_intf_cfg;


typedef enum
{
  /*Defines Write/Read Access.0 = Read Access.1 = Write Access.
      0x0 : READ_ACCESS
      0x1 : WRITE_ACCESS */
  PM_FG_MEMIF_MEM_INTF_CTL_WR_EN=0x06, 
  /* Defines Single/Burst Access. 0 = Single Mode enabled.1 = Burst Mode enabled.
      0x0 : MEM_ACS_SINGLE
      0x1 : MEM_ACS_BURST */
  PM_FG_MEMIF_MEM_INTF_CTL_WR_BURST,
  PM_FG_MEMIF_MEM_INTF_CTL_INVALID
} pm_fg_memif_mem_intf_ctl;

typedef struct
{
  /* Controls OTP test mode enabling pins.
      0x0 : OTP_TM0
      0x1 : OTP_TM1
      0x2 : OTP_TM2
      0x3 : OTP_TM3 */
  uint8 pm_fg_memif_otp_cfg_ptm;
  /* Controls OTP memory program mode enable pin (PPROG)  0 = OTP programming disabled.  1 = OTP programmin enabled.
      0x0 : OTP_PROGRAM_DISABLE
      0x1 : OTP_PROGRAM_ENABLE  */
  boolean pm_fg_memif_otp_cfg_pprog;
  /* Controls the analog switch that selects the power supply feeding the OTP memory VPP pin.  0 = dVdd  1 = VPPext
      0x0 : VPP_SEL_DVDD
      0x1 : VPP_SEL_VPP  */
  boolean pm_fg_memif_otp_cfg_vpp_sel;
  /* Defines wait states for margin reads */
  uint8 pm_fg_memif_otp_cfg_margin_rd_ws;
} pm_fg_memif_otp_cfg;



/*===========================================================================
                        EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/**
* @brief This function enables irq
* 
* @details
* This function enables irq
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] pm_fg_memif_irq_type               irq type
* @param[in] boolean        enable/disable value
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_irq_enable(uint32 device_index, pm_fg_memif_irq_type irq, boolean enable);

/**
* @brief This function clears irq
* 
* @details
*  This function clears irq
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] pm_fg_memif_irq_type               irq type
*
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
  pm_err_flag_type pm_fg_memif_irq_clear(uint32  device_index, pm_fg_memif_irq_type irq);
  
/**
* @brief This function sets interrupt triggers
* 
* @details
*  This function sets interrupt triggers
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] pm_fg_memif_irq_type               irq type
* @param[in] pm_irq_trigger_type               trigger
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_irq_set_trigger(uint32 device_index, pm_fg_memif_irq_type irq, pm_irq_trigger_type trigger);

/**
* @brief This function returns irq status
* 
* @details
*  This function returns irq status
* 
* @param[in] pmic_device_index.  Primary: 0 Secondary: 1
* @param[in] pm_fg_memif_irq_type        irq type
* @param[in] pm_irq_status_type        irq status type
* @param[in] boolean        irq TRUE/FALSE status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_irq_status(uint32 device_index, pm_fg_memif_irq_type irq, pm_irq_status_type type, boolean *status);

/**
 * @brief This function returns Fule Gauge Memory Interface Configuration * 
 * @details
 *  This function returns Fule Gauge Memory Interface Configuration 
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]pm_fg_memif_mem_intf_cfg memory Interface configuration
 * @param[out]enable TRUE/FALSE value of passed memory interface cfg
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_get_mem_intf_cfg(uint32 device_index, pm_fg_memif_mem_intf_cfg mem_intf_cfg, boolean *enable);

/**
 * @brief This function sets Fule Gauge Memory Interface Configuration * 
 * @details
 *  This function sets Fule Gauge Memory Interface Configuration 
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]pm_fg_memif_mem_intf_cfg memory Interface configuration
 * @param[in]enable TRUE/FALSE value of passed memory interface cfg
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_set_mem_intf_cfg(uint32 device_index, pm_fg_memif_mem_intf_cfg mem_intf_cfg, boolean enable);


/**
 * @brief This function returns Fule Gauge Memory Interface Configuration Control * 
 * @details
 *  This function returns Fule Gauge Memory Interface Configuration Control 
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]fg_memif_addr mem intf address 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_get_mem_intf_ctl(uint32 device_index, pm_fg_memif_mem_intf_ctl mem_intf_ctl, boolean *enable);

/**
 * @brief This function sets Fule Gauge Memory Interface Configuration Control * 
 * @details
 *  This function sets Fule Gauge Memory Interface Configuration Control
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]pm_fg_memif_mem_intf_ctl memory Interface Configuration Control
 * @param[in]enable TRUE/FALSE value of passed memory interface ctl
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_set_mem_intf_ctl(uint32 device_index, pm_fg_memif_mem_intf_ctl mem_intf_ctl, boolean enable);

/**
 * @brief  This function Writes Fule Gauge Memory Interface Address Regsiter * 
 * @details
 *  This function Writes Fule Gauge Memory Interface Address Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]pm_fg_memif_mem_intf_ctl memory Interface configuration Control
 * @param[out]enable TRUE/FALSE value of passed memory interface ctl
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_write_addr(uint32 device_index, uint16 fg_memif_addr);

/**
 * @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
 * @details
 *  This function Reads Fule Gauge Memory Interface Address Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]fg_memif_addr mem intf address 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_read_addr_reg(uint32 device_index, uint16 *fg_memif_addr);


/**
 * @brief  This function Writes Fule Gauge Memory Interface Data Regsiter * 
 * @details
 *  This function Writes Fule Gauge Memory Interface Data Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]fg_memif_data mem intf data 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_write_data(uint32 device_index, uint32 fg_memif_data);

/**
 * @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
 * @details
 *  This function Reads Fule Gauge Memory Interface Address Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]fg_memif_data mem intf data 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_read_data_reg(uint32 device_index, uint32 *fg_memif_data);

/*FG MEMIF SEC ACCESS */


/**
 * @brief  This function Writes Fule Gauge Memory Interface Data Regsiter * 
 * @details
 *  This function Writes Fule Gauge Memory Interface Data Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]pm_fg_memif_otp_cfg mem intf data 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_set_otp_cfg(uint32 device_index, pm_fg_memif_otp_cfg fg_memif_cfg);

/**
 * @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
 * @details
 *  This function Reads Fule Gauge Memory Interface Address Regsiter
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[out]pm_fg_memif_otp_cfg  mem intf data 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_get_otp_cfg(uint32 device_index, pm_fg_memif_otp_cfg *fg_memif_otp_cfg);



/**
 * @brief  This function Enable/Disbale Fule Gauge Memory Interface OTP Programming * 
 * @details
 *  This function Enable/Disbale Fule Gauge Memory Interface OTP Programming 
 *  Read status of this setting by calling 'pm_fg_memif_get_ot_cfg function'
 * 
 * @param[in] pmic_device_index. Primary: 0 Secondary: 1
 * @param[in]enable Enable/Disable 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_fg_memif_ctl_otp_pprog(uint32 device_index, boolean enable);


#endif /* __PM_FG_SOC_MEMIF_H__ */
