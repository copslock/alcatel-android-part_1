#ifndef VSENSE_H
#define VSENSE_H
/*
===========================================================================
*/
/**
  @file vsense.h

  Common functions for vsense init and start calibration routine
*/
/*
  ====================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================


  when        who     what, where, why
  --------    ---     -------------------------------------------------
  2014/10/14  aks      Initial revision.
  ====================================================================
*/

#include "com_dtypes.h"

/*----------------------------------------------------------------------------
 * Function : vsense_init
 * -------------------------------------------------------------------------*/
/*!
    Description: Initialize the vsense driver :Allocate Shared Memory
    @param
      void
    @return
    TRUE/FALSE
    
-------------------------------------------------------------------------*/
boolean vsense_init(void);


/*----------------------------------------------------------------------------
 * Function : vsense_start_calibration
 * -------------------------------------------------------------------------*/
/*!
    Description: Calibrate all the supported rails 
    @param
      void
    @return
    TRUE/FALSE
    
-------------------------------------------------------------------------*/

boolean vsense_start_calibration(void);

/*----------------------------------------------------------------------------
 * Function : vsense_copy_to_smem
 * -------------------------------------------------------------------------*/
/*!
    Description: Store the calibration information into shared memory
    @param
      void
    @return
    TRUE/FALSE
    
-------------------------------------------------------------------------*/
boolean  vsense_copy_to_smem(void);

#endif
