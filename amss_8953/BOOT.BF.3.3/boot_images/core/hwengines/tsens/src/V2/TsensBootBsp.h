#ifndef TSENS_BOOT_BSP_H
#define TSENS_BOOT_BSP_H
/*============================================================================
  @file TsensBootBsp.h

  Tsens boot BSP file.

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/* $Header: //components/rel/boot.bf/3.3/boot_images/core/hwengines/tsens/src/V2/TsensBootBsp.h#1 $ */

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/
#define TSENS_THRESHOLD_DISABLED 0x7fffffff
#define TSENS_MAX_NUM_SENSORS 16
#define TSENS_MAX_NUM_CPU_SENSORS 8

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "DALStdDef.h"

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
typedef enum
{
   TSENS_BSP_SENSOR_CAL_NORMAL = 0,         /* Normal method, i.e. use QFPROM if
                                             * avail else use default char data */
   TSENS_BSP_SENSOR_CAL_IGNORE_DEVICE_CAL   /* Force using default char data */
} TsensBspSensorCalType;

typedef struct
{
   int32 nCpuHighThreshold[TSENS_MAX_NUM_CPU_SENSORS];
   int32 nCpuLowThreshold[TSENS_MAX_NUM_CPU_SENSORS];
   uint32 uCpuIndexes;
   DALBOOL bCpuThresholdsEnabled;
} CpuThresholdsConfigType;

typedef enum
{
   TSENS_BSP_MTC_SYS_PERF_100 = 0x10,
   TSENS_BSP_MTC_SYS_PERF_75  = 0x11,
   TSENS_BSP_MTC_SYS_PERF_67  = 0x12,
   TSENS_BSP_MTC_SYS_PERF_50  = 0x13,
   TSENS_BSP_MTC_SYS_PERF_33  = 0x14,
   TSENS_BSP_MTC_SYS_PERF_25  = 0x15,
} TsensBspMTCSysPerfType;

typedef struct
{
   uint32 uTsensConfig;          /* Config value for the sensor */
   TsensBspSensorCalType eCal;   /* Which cal type to use */
   int32 nCalPoint1CodeDefault;  /* Default TSENS code at calibration point nCalPoint1DeciDegC */
   int32 nCalPoint2CodeDefault;  /* Default TSENS code at calibration point nCalPoint2DeciDegC */
   int32 nCriticalMin;           /* Minimum temperature threshold for critical shutdown
                                  * or use TSENS_THRESHOLD_DISABLED to disable */
   int32 nCriticalMax;           /* Maximum temperature threshold for critical shutdown
                                  * or use TSENS_THRESHOLD_DISABLED to disable */
   uint32 uMTCThreshold1Temp;    /* Multi-Zone Temperature Control Threshold1 Temperature
                                    where crossover occurs between cool and yellow zones */
   uint32 uMTCThreshold2Temp;    /* Multi-Zone Temperature Control Threshold2 Temperature
                                    where crossover occurs between yellow and red zones */
} TsensBootSensorType;

typedef struct
{
   uint32 bIsZoneEnabled;
   TsensBspMTCSysPerfType uPSCommandTh2Viol;
   TsensBspMTCSysPerfType uPSCommandTh1Viol;
   TsensBspMTCSysPerfType uPSCommandCool;
   uint32 uSensorMask;
   DALBOOL bIsTH1Enabled;
   DALBOOL bIsTH2Enabled;
} TsensBootMTCConfigType;

typedef struct
{
   const TsensBootSensorType *paSensors;
   uint32 uNumSensors;
   uint32 uPeriod;
   uint32 uPeriodSleep;
   DALBOOL bAutoAdjustPeriod; 
   uint32 uSensorConvTime_us;
   uint32 uNumGetTempRetries;
   uint32 uGlobalConfig;
   int32 nCalPoint1DeciDegC;  /* Calibration point 1 in deci deg C */
   int32 nCalPoint2DeciDegC;  /* Calibration point 2 in deci deg C */
   uint32 uShift;             /* Shift value */
   uint32 bIsMTCSupported;
   const TsensBootMTCConfigType *paMTCConfig;
   uint32 uNumMTCZones;
   uint32 uMTCThreshold1Margin;
   uint32 uMTCThreshold2Margin;
   const CpuThresholdsConfigType *pCpuThresholdsCfg;
   DALBOOL bPSHoldResetEn;     /* Whether to enable PS_HOLD reset */
   DALBOOL bPWMEn;             /* Whether to enable PWM */
   DALBOOL bStandAlone;        /* Whether this is a stand alone controller */
   uint32 uSidebandEnMask;     /* Mask of sideband sensors to enable */
} TsensBootBspType;

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/

#endif /* #ifndef TSENS_BOOT_BSP_H */

