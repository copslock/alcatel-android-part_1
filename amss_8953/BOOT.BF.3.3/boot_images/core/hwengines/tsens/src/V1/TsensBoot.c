/*============================================================================
  FILE:         TsensBoot.c

  OVERVIEW:     Implementation of TSENS in the boot

  DEPENDENCIES: None

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  $Header: //components/rel/boot.bf/3.3/boot_images/core/hwengines/tsens/src/V1/TsensBoot.c#3 $$DateTime: 2015/12/02 08:08:41 $$Author: pwbldsvc $


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-11-24  SA   Add 8917 support.
  2015-02-24  SA   Added MTC support.
  2014-07-09  jjo  Add support for 20 nm TSENS.
  2013-01-15  jjo  Fixed timeout to account for all sensors being enabled.
  2012-11-14  jjo  Added critical thresholds.
  2012-09-13  jjo  Only check the ready bit on the first read.
  2012-07-30  jjo  Now using a C file for properties.
  2012-05-23  jjo  Added DAL properties.
  2012-05-20  jjo  Ported to 8974.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBoot.h"
#include "HALtsens.h"
#include "busywait.h"
#include "DALSys.h"
#include "DALStdDef.h"
#include "TsensiConversion.h"
#include "TsensBootBsp.h"
#include "DDIChipInfo.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
int32 gnTsensLastTemp = 0x7FFFFFFF;
uint32 guTsensLastSensor = 0;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const TsensBootBspType *pTsensBootBsp = NULL;
static boolean bTsensInitialized = FALSE;   /* Flag to determine if Tsens is initialized */
static boolean bFirstReadDone = FALSE;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/* ============================================================================
**
**  Tsens_MTC_Init
**
**  Description:
**    Initializes TSENS MTC in the boot.
**
**  Parameters:
**    None
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    None
**
** ========================================================================= */
static TsensResultType Tsens_MTC_Init()
{
   uint32 i, uTh1Code = 0, uTh2Code = 0;
   uint32 uTh1Margin, uTh2Margin;
   uint32 uTh1MaxMargin = 0, uTh2MaxMargin = 0;
   uint32 uSens, uSensMask;
   const TsensBootMTCConfigType *pMTCConfig;
   const TsensBootSensorType *pSensor;
   TsensResultType result;

   // Set the threshold temperature for all sensors
   for (i = 0; i < pTsensBootBsp->uNumSensors; ++i)
   {
      pSensor = &pTsensBootBsp->paSensors[i];
      result = Tsensi_ConvertFromDegC(i, pSensor->uMTCThreshold1Temp, &uTh1Code);
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
      result = Tsensi_ConvertFromDegC(i, pSensor->uMTCThreshold2Temp, &uTh2Code);
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
      HAL_tsens_MTC_SetThresholds(i, uTh1Code, uTh2Code);
   }

   //
   // Calculate highest code among all the sensors for the threshold margins...this is to avoid
   // cases where some senors may have low code value for the threshold margin and so could
   // result in applying the zone change (from red to yellow or yellow to cool) action too soon.
   //

   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      uSensMask = pMTCConfig->uSensorMask;
      uSens = 0;

      while (uSensMask)
      {
         if (uSensMask & 0x1)
         {
            pSensor = &pTsensBootBsp->paSensors[uSens];
            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold1Temp-pTsensBootBsp->uMTCThreshold1Margin, &uTh1Margin);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold1Temp, &uTh1Code);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            if (uTh1MaxMargin < uTh1Code-uTh1Margin)
            {
               uTh1MaxMargin = uTh1Code-uTh1Margin;
            }

            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold2Temp-pTsensBootBsp->uMTCThreshold2Margin, &uTh2Margin);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold2Temp, &uTh2Code);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            if (uTh2MaxMargin < uTh2Code-uTh2Margin)
            {
               uTh2MaxMargin = uTh2Code-uTh2Margin;
            }
         }
         uSens++;
         uSensMask = (uSensMask >> 1);
      }
   }
   uTh2Code=0;
   uTh1Code=0;
   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      uSensMask = pMTCConfig->uSensorMask;
      uSens = 0;

      while (uSensMask)
      {
         if (uSensMask & 0x1)
         {
            pSensor = &pTsensBootBsp->paSensors[uSens];

            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold1Temp, &uTh1Code);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            result = Tsensi_ConvertFromDegC(uSens, pSensor->uMTCThreshold2Temp, &uTh2Code);
            if (result != TSENS_SUCCESS)
            {
               return result;
            }
            if (uTh2Code-uTh2MaxMargin <= uTh1Code-uTh1MaxMargin)
            {
               uTh1MaxMargin = 0;
               uTh2MaxMargin = 0;
               goto SET_MARGIN;
            }
         }
         uSens++;
         uSensMask = (uSensMask >> 1);
      }
   }

SET_MARGIN:
   // Set margins
   HAL_tsens_MTC_SetMargins(uTh1MaxMargin, uTh2MaxMargin);

   // Configure and enable MTC zones
   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      HAL_tsens_MTC_ConfigZone(i, pMTCConfig->uPSCommandTh2Viol, pMTCConfig->uPSCommandTh1Viol, pMTCConfig->uPSCommandCool, pMTCConfig->uSensorMask);
   }

   HAL_tsens_SetMTCState(HAL_TSENS_ENABLE_MTC);
   for (i = 0; i < pTsensBootBsp->uNumMTCZones; ++i)
   {
      pMTCConfig = &pTsensBootBsp->paMTCConfig[i];
      HAL_tsens_MTC_ConfigZoneSwMask(i, pMTCConfig->bIsTH1Enabled, pMTCConfig->bIsTH2Enabled);	  
   }

   return TSENS_SUCCESS;
}

static TsensResultType Tsens_Tsens_SetCpuThresholdsInternal(uint32 uCpuIndex, int32 nCpuLowDegC, int32 nCpuHighDegC)
{
   TsensResultType result;
   boolean bCpuHighEn;
   boolean bCpuLowEn;
   uint32 uCodeHigh;
   uint32 uCodeLow;
   uint32 uSensor;

   if (uCpuIndex >= TSENS_MAX_NUM_CPU_SENSORS)
   {
      return TSENS_ERROR;
   }

   uSensor = (pTsensBootBsp->pCpuThresholdsCfg->uCpuIndexes >> (uCpuIndex * 4)) & 0xf;
   if (uSensor >= TSENS_MAX_NUM_SENSORS)
   {
      return TSENS_ERROR;
   }

   if (nCpuHighDegC != TSENS_THRESHOLD_DISABLED)
   {
      bCpuHighEn = TRUE;
      result = Tsensi_ConvertFromDegC(uSensor, nCpuHighDegC, &uCodeHigh);
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
   }
   else
   {
      bCpuHighEn = FALSE;
      uCodeHigh = HAL_tsens_GetMaxCode();;
   }

   if (nCpuLowDegC != TSENS_THRESHOLD_DISABLED)
   {
      bCpuLowEn = TRUE;
      result = Tsensi_ConvertFromDegC(uSensor, nCpuLowDegC, &uCodeLow);
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
   }
   else
   {
      bCpuLowEn = FALSE;
      uCodeLow = 0;
   }

   HAL_tsens_SetCpuHighLowThreshold(uCpuIndex, uCodeHigh, uCodeLow, bCpuHighEn, bCpuLowEn);

   return TSENS_SUCCESS;
}


/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  Tsens_Init
**
**  Description:
**    Initializes TSENS in the boot.
**
**  Parameters:
**    None
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    None
**
** ========================================================================= */
TsensResultType Tsens_Init(void)
{
   DALSYS_PROPERTY_HANDLE_DECLARE(hTsensBootProperties);
   DALSYSPropertyVar propertyVar;
   DALResult status;
   TsensResultType result;
   const TsensBootSensorType *pSensor;
   uint32 uSensor;
   uint32 uCode;
   uint32 uSensorEnableMask;
   uint32 uDisabledCPUsMask;
   uint32 uCpuIndex;
   const CpuThresholdsConfigType *pCpuThresholdsCfg;
   DalChipInfoFamilyType eChipInfoFamilyType;
   const char *pszTsensBspPropName = "TSENS_BOOT_BSP";

   if (bTsensInitialized == TRUE)
   {
      return TSENS_SUCCESS;
   }

   /* Get the properties */
   status = DALSYS_GetDALPropertyHandleStr("QTsens", hTsensBootProperties);
   if (status != DAL_SUCCESS)
   {
      return TSENS_ERROR;
   }

   /* Get the chip family */
   eChipInfoFamilyType = DalChipInfo_ChipFamily();
   if (eChipInfoFamilyType == DALCHIPINFO_FAMILY_MSM8917)
   {
      pszTsensBspPropName = "TSENS_BOOT_BSP_8917";
   }

   status = DALSYS_GetPropertyValue(hTsensBootProperties,
                                    pszTsensBspPropName,
                                    0,
                                    &propertyVar);
   if (status != DAL_SUCCESS)
   {
      return TSENS_ERROR;
   }

   pTsensBootBsp = (TsensBootBspType *)propertyVar.Val.pStruct;

   uSensorEnableMask = 0;
   for (uSensor = 0; uSensor < pTsensBootBsp->uNumSensors; uSensor++)
   {
      uSensorEnableMask |= 1 << uSensor;
   }

   /* Initialize calibration */
   Tsensi_CalibrationInit(pTsensBootBsp->paSensors,
                          pTsensBootBsp->uNumSensors,
                          pTsensBootBsp->nY1,
                          pTsensBootBsp->nY2);

   /* Configure & start Tsens */
   HAL_tsens_SetState(HAL_TSENS_DISABLE);
   HAL_tsens_Reset();
   HAL_tsens_Init(pTsensBootBsp->uGlobalConfig);

   /* Configure the sensor & set the critical thresholds */
   for (uSensor = 0; uSensor < pTsensBootBsp->uNumSensors; uSensor++)
   {
      pSensor = &pTsensBootBsp->paSensors[uSensor];

      /* Sensor configuration */
      HAL_tsens_ConfigSensor(uSensor, pSensor->uTsensConfig);

      /* Critical threshold configuration */
      if (pSensor->nCriticalMin != TSENS_THRESHOLD_DISABLED)
      {
         result = Tsensi_ConvertFromDegC(uSensor, pSensor->nCriticalMin, &uCode);
         if (result != TSENS_SUCCESS)
         {
            return result;
         }

         HAL_tsens_SetThreshold(HAL_TSENS_MIN_LIMIT_TH, uSensor, uCode);
         HAL_tsens_EnableInterrupt(HAL_TSENS_MIN_LIMIT_TH, uSensor);
      }
      else
      {
         HAL_tsens_DisableInterrupt(HAL_TSENS_MIN_LIMIT_TH, uSensor);
      }

      if (pSensor->nCriticalMax != TSENS_THRESHOLD_DISABLED)
      {
         result = Tsensi_ConvertFromDegC(uSensor, pSensor->nCriticalMax, &uCode);
         if (result != TSENS_SUCCESS)
         {
            return result;
         }

         HAL_tsens_SetThreshold(HAL_TSENS_MAX_LIMIT_TH, uSensor, uCode);
         HAL_tsens_EnableInterrupt(HAL_TSENS_MAX_LIMIT_TH, uSensor);
      }
      else
      {
         HAL_tsens_DisableInterrupt(HAL_TSENS_MAX_LIMIT_TH, uSensor);
      }
   }

   HAL_tsens_SetPeriod(pTsensBootBsp->uPeriod);
   HAL_tsens_SetPeriodSleep(pTsensBootBsp->uPeriodSleep);
   HAL_tsens_SetAutoAdjustPeriod(pTsensBootBsp->bAutoAdjustPeriod);
   HAL_tsens_SelectADCClkSrc(HAL_TSENS_INTERNAL);
   HAL_tsens_SetSensorsEnabled(uSensorEnableMask);

   if (pTsensBootBsp->bIsMTCSupported)
   {
      result = Tsens_MTC_Init();
      if (result != TSENS_SUCCESS)
      {
         return result;
      }
   }

   /* LMh-Lite configuration */
   pCpuThresholdsCfg = pTsensBootBsp->pCpuThresholdsCfg;
   if (pCpuThresholdsCfg != NULL)
   {
      // Get disabled CPUs mask
      uDisabledCPUsMask = HAL_tsens_GetDisabledCPUsMask();

      for (uCpuIndex = 0; uCpuIndex < TSENS_MAX_NUM_CPU_SENSORS; uCpuIndex++)
      {
         if (uDisabledCPUsMask & (1 << uCpuIndex))
         {
            // This CPU is disabled
            continue;
         }
         result = Tsens_Tsens_SetCpuThresholdsInternal(uCpuIndex,
                                                       pCpuThresholdsCfg->nCpuLowThreshold[uCpuIndex],
                                                       pCpuThresholdsCfg->nCpuHighThreshold[uCpuIndex]);
         if (result != TSENS_SUCCESS)
         {
            return result;
         }
      }

      HAL_tsens_SetCpuIndexes(pCpuThresholdsCfg->uCpuIndexes);
      HAL_tsens_SetCpuHighLowEnable(pCpuThresholdsCfg->bCpuThresholdsEnabled);
   }

   HAL_tsens_SetState(HAL_TSENS_ENABLE);

   bTsensInitialized = TRUE;
   bFirstReadDone = FALSE;

   return TSENS_SUCCESS;
}

/* ============================================================================
**
**  Tsens_GetTemp
**
**  Description:
**    Gets the current temperature of the sensor.
**
**  Parameters:
**    uSensor [in]: sensor index
**    pnDegC [out]: sensor temperature in degrees C
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    Tsens_Init must be called once prior to calling this function.
**
** ========================================================================= */
TsensResultType Tsens_GetTemp(uint32 uSensor, int32 *pnDegC)
{
   DALBOOL bMeasComplete = FALSE;
   uint32 uTimerCnt;
   uint32 uAdcCode = 0;
   TsensResultType result;
   uint32 uWaitTime_us;
   uint32 uAdcCodeTry1;
   uint32 uAdcCodeTry2;
   uint32 uAdcCodeTry3;
   boolean bValid;

   if (pnDegC == NULL)
   {
      return TSENS_ERROR;
   }

   if (bTsensInitialized != TRUE)
   {
      return TSENS_ERROR_NOT_INITIALIZED;
   }

   if (uSensor >= pTsensBootBsp->uNumSensors)
   {
      return TSENS_ERROR;
   }

   /* for continuous mode(measure period=0), TRDY stays low and hence wait for all sensors to convert once and relay on VALID bit alone*/
   if((!pTsensBootBsp->uPeriod) && (bFirstReadDone == FALSE))
   {
      DALSYS_BusyWait(pTsensBootBsp->uSensorConvTime_us * pTsensBootBsp->uNumSensors);
      bFirstReadDone=TRUE;
   }
   
   /* Need to delay until TSENS performs the first reads */
   if (bFirstReadDone == FALSE)
   {
      uWaitTime_us = pTsensBootBsp->uSensorConvTime_us * pTsensBootBsp->uNumSensors;

      for (uTimerCnt = 0; uTimerCnt < pTsensBootBsp->uNumGetTempRetries; uTimerCnt++)
      {
         if (HAL_tsens_TempMeasurementIsComplete() == TRUE)
         {
            bMeasComplete = TRUE;
            break;
         }
         DALSYS_BusyWait(uWaitTime_us);
      }

      if (bMeasComplete == FALSE)
      {
         if (HAL_tsens_TempMeasurementIsComplete() == FALSE)
         {
            return TSENS_ERROR_TIMEOUT;
         }
      }

      bFirstReadDone = TRUE;
   }

   /* Read the ADC code */
   bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &uAdcCodeTry1);
   if (bValid == TRUE)
   {
      uAdcCode = uAdcCodeTry1;
   }
   else
   {
      bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &uAdcCodeTry2);
      if (bValid == TRUE)
      {
         uAdcCode = uAdcCodeTry2;
      }
      else
      {
         bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &uAdcCodeTry3);
         if (bValid == TRUE)
         {
            uAdcCode = uAdcCodeTry3;
         }
         else if (uAdcCodeTry1 == uAdcCodeTry2)
         {
            uAdcCode = uAdcCodeTry1;
         }
         else if (uAdcCodeTry2 == uAdcCodeTry3)
         {
            uAdcCode = uAdcCodeTry2;
         }
         else
         {
            uAdcCode = uAdcCodeTry1;
         }
      }
   }

   /* Scale the ADC code to physical units */
   result = Tsensi_ConvertToDegC(uSensor, uAdcCode, pnDegC);

   if (result == TSENS_SUCCESS)
   {
      gnTsensLastTemp = *pnDegC;
      guTsensLastSensor = uSensor;
   }

   return result;
}

/* ============================================================================
**
**  Tsens_GetNumSensors
**
**  Description:
**    Gets the number of sensors. Note that the sensor index is zero-based, i.e.
**    uSensor = {0 to *puNumSensors - 1}
**
**  Parameters:
**    puNumSensors [out]: number of sensors
**
**  Return:
**    TsensResultType
**
**  Dependencies:
**    Tsens_Init must be called once prior to calling this function.
**
** ========================================================================= */
TsensResultType Tsens_GetNumSensors(uint32 *puNumSensors)
{
   if (puNumSensors == NULL)
   {
      return TSENS_ERROR;
   }

   if (bTsensInitialized != TRUE)
   {
      return TSENS_ERROR_NOT_INITIALIZED;
   }

   *puNumSensors = pTsensBootBsp->uNumSensors;

   return TSENS_SUCCESS;
}

