/*============================================================================
  FILE:         TsensBootBsp.c

  OVERVIEW:     8953 BSP for Tsens boot.

  DEPENDENCIES: None

                Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2016-09-03  SA   Disable min and max threshold for Tsens sensor 0 (CR 977820).
  2016-25-02  SA   Update MTC thresholds with initial recommended values from SPT team.
  2015-06-08  PR   updated MTC configuration with SPT recommended tuning values.
  2015-02-11  SA   Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBootBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

/* Thresholds */
#define TSENS_THRESHOLD_MIN       -350
#define TSENS_THRESHOLD_LOWER     TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_UPPER     TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_CRITICAL  TSENS_THRESHOLD_DISABLED
#define TSENS_THRESHOLD_MAX       1200

/*configurations*/
#define TSENS_PERIOD                         5   // 9.75ms
#define TSENS_PERIOD_SLEEP                0xFE   // 870ms
#define TSENS_SENSOR_CONV_TIME_US          170
#define TSENS_NUM_GET_TEMP_RETRIES           5
#define TSENS_TS_CONTROL            0x01803020
#define TSENS_TS_CONFIG                   0x4B


/*DeciDegrees*/
#define TSENS_MTC_THRESHOLD1_TEMP         1000
#define TSENS_MTC_THRESHOLD2_TEMP         1100
#define TSENS_MTC_THRESHOLD1_MARGIN         50
#define TSENS_MTC_THRESHOLD2_MARGIN         50

/* Calibration points */
#define POINT_1_DECI_DEG_C 300
#define POINT_2_DECI_DEG_C 1200

/* Default calibration values */
#define INTERPOLATE_FOR_Y(m,x,x1,y1) (((m) * ((x) - (x1))) + (y1))
#define CODE_PER_DECI_DEG_C 0.312
#define POINT_1_CODE 456
#define POINT_2_CODE (INTERPOLATE_FOR_Y(CODE_PER_DECI_DEG_C, POINT_2_DECI_DEG_C, POINT_1_DECI_DEG_C, POINT_1_CODE))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const TsensBootSensorType aSensors[] =
{
   /* Sensor 0 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_DISABLED,
      /* .nCriticalMax         */ TSENS_THRESHOLD_DISABLED,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 1 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 2 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 3 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 4 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 5 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 6 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 7 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 8 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 9 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },

   /* Sensor 10 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
   /* Sensor 11 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
   /* Sensor 12 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
   /* Sensor 13 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
   /* Sensor 14 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
   /* Sensor 15 */
   {
      /* .uTsensConfig         */ TSENS_TS_CONFIG,
      /* .eCal                 */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nCalPoint1CodeDefault*/ POINT_1_CODE,
      /* .nCalPoint2CodeDefault*/ POINT_2_CODE,
      /* .nCriticalMin         */ TSENS_THRESHOLD_MIN,
      /* .nCriticalMax         */ TSENS_THRESHOLD_MAX,
      /* .uMTCThreshold1Temp   */ TSENS_MTC_THRESHOLD1_TEMP,
      /* .uMTCThreshold2Temp   */ TSENS_MTC_THRESHOLD2_TEMP,
   },
};

static const TsensBootMTCConfigType aMTCConfig[] =
{
   /* Zone 0 */
   {
      /* .bIsZoneEnabled    */ TRUE,
      /* .uPSCommandTh2Viol */ TSENS_BSP_MTC_SYS_PERF_25,
      /* .uPSCommandTh1Viol */ TSENS_BSP_MTC_SYS_PERF_50,
      /* .uPSCommandCool    */ TSENS_BSP_MTC_SYS_PERF_100,
      /* .uSensorMask       */ 0x1E00, // S9, S10, S11 and S12
      /* .bIsTH1Enabled     */ TRUE,
      /* .bIsTH2Enabled     */ TRUE,
   },
   /* Zone 1 */
   {
      /* .bIsZoneEnabled    */ TRUE,
      /* .uPSCommandTh2Viol */ TSENS_BSP_MTC_SYS_PERF_25,
      /* .uPSCommandTh1Viol */ TSENS_BSP_MTC_SYS_PERF_50,
      /* .uPSCommandCool    */ TSENS_BSP_MTC_SYS_PERF_100,
      /* .uSensorMask       */ 0xF0, // S4, S5, S6, and S7
      /* .bIsTH1Enabled     */ TRUE,
      /* .bIsTH2Enabled     */ TRUE,
   }
};

const TsensBootBspType TsensBootBsp[] =
{
   {
      /* .paSensors            */ aSensors,
      /* .uNumSensors          */ ARRAY_LENGTH(aSensors),
      /* .uPeriod              */ TSENS_PERIOD,
      /* .uPeriodSleep         */ TSENS_PERIOD_SLEEP,
      /* .bAutoAdjustPeriod    */ TRUE,
      /* .uSensorConvTime_us   */ TSENS_SENSOR_CONV_TIME_US,
      /* .uNumGetTempRetries   */ TSENS_NUM_GET_TEMP_RETRIES,
      /* .uGlobalConfig        */ TSENS_TS_CONTROL,
      /* .nCalPoint1DeciDegC   */ POINT_1_DECI_DEG_C,
      /* .nCalPoint2DeciDegC   */ POINT_2_DECI_DEG_C,
      /* .uShift               */ 10,
      /* .bIsMTCSupported      */ TRUE,
      /* .paMTCConfig          */ aMTCConfig,
      /* .uNumMTCZones         */ ARRAY_LENGTH(aMTCConfig),
      /* .uMTCThreshold1Margin */ TSENS_MTC_THRESHOLD1_MARGIN,
      /* .uMTCThreshold2Margin */ TSENS_MTC_THRESHOLD2_MARGIN,
      /* .pCpuThresholdsCfg    */ NULL,
      /* .bPSHoldResetEn       */ TRUE,
      /* .bPWMEn               */ FALSE,
      /* .bStandAlone          */ FALSE,
      /* .uSidebandEnMask      */ 0xFFFF
   }
};

