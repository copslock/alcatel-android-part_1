<driver name="VAdc">
  <device id="DALDEVICEID_VADC_8937_PM8950">
    <props name="VADC_BSP" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      VAdcBsp_8937_PM8950
    </props>
  </device>
  <device id="DALDEVICEID_VADC_8937_PMI8950">
    <props name="VADC_BSP" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      VAdcBsp_8937_PMI8950
    </props>
  </device>
</driver>
<driver name="NULL">
  <device id="QAdc">
    <props name="ADC_BOOT_BSP" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      AdcBootBsp_8937_PM8950
    </props>
    <props name="ADC_BOOT_BSP_8937_PM8950_PMI8950" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      AdcBootBsp_8937_PM8950_PMI8950
    </props>
  </device>
</driver>
