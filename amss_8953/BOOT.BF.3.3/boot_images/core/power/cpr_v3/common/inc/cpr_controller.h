/**
 * @file:  cpr_controller.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/15 23:28:14 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/common/inc/cpr_controller.h#1 $
 * $Change: 9594144 $
 */
#ifndef CPR_CONTROLLER_H
#define	CPR_CONTROLLER_H

#include "cpr_cfg.h"

void cpr_controller_init(cpr_controller* controller);

#endif

