/**
 * @file:  cpr_image_logs.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 01:39:35 $
 * $Header: //components/rel/boot.bf/3.3/boot_images/core/power/cpr_v3/image/sbl/src/cpr_image_logs.c#2 $
 * $Change: 9714790 $
 */

#include "cpr_image_logs.h"
#include "cpr_data.h"
#include "cpr_cfg.h"

char cpr_image_log_buf[CPR_LOG_SIZE];


#if 0
void cpr_image_static_log_update_all(void)
{
    cpr_rail *rail = cpr_get_rail(CPR_RAIL_MSS);

    CPR_STATIC_LOG_CPR_INFO(&cpr_info);

    CPR_STATIC_LOG_RAIL_INFO(&cpr_info.railStates[rail->railIdx]);

    for(int modeIdx = 0; modeIdx < rail->vp->modesCount; modeIdx++)
    {
        cpr_mode_settings *modeSetting = &cpr_info.railStates[rail->railIdx].modeSettings[modeIdx];

        for(int submodeIdx = 0; submodeIdx < modeSetting->subModesCount; submodeIdx++)
        {
            cpr_submode_settings* submode = &modeSetting->subModes[submodeIdx];

            CPR_STATIC_LOG_MODE_INFO(&cpr_info.railStates[rail->railIdx], modeSetting, submode);
            CPR_STATIC_LOG_ISR_INFO(&cpr_info.railStates[rail->railIdx], modeSetting, submode);
        }
    }
}
#endif

//ULogHandle cprLogHandle;
