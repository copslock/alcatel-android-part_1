#ifndef CPR_IMAGE_TARGET_H
#define CPR_IMAGE_TARGET_H

#include "cpr_defs.h"
#include "railway.h"


railway_corner cpr_image_target_cpr_to_railway(cpr_voltage_mode mode);
cpr_voltage_mode cpr_image_target_railway_to_cpr(railway_corner corner);


#endif /*CPR_IMAGE_TARGET_H*/
