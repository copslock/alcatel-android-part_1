#ifndef CPR_ENABLEMENT_H
#define CPR_ENABLEMENT_H

#include "cpr_cfg.h"

extern cpr_misc_cfg cpr_misc;
#define CPR_NUM_ENABLEMENTS 2
#define CPR_NUM_CONTROLLERS 1
#define CPR_NUM_RAILS 2

cpr_rail* cpr_get_rail(cpr_domain_id railId);
uint32 cpr_get_rail_idx(cpr_domain_id railId);
extern cpr_rail* cpr_rails[];
extern cpr_controller* cpr_controllers[];
extern cpr_enablement* cpr_enablements[];

#endif
