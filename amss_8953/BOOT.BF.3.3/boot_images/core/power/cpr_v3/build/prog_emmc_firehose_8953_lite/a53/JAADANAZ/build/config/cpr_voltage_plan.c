#include "cpr_voltage_plan.h"
#include "cpr_logs.h"
#include "cpr_cfg.h"
#include "cpr_fuses_hwio.h"

//hash value of Voltage Plan file (extracted all cell values)
const char cpr_voltage_plan_hash_value[] = "4A46EB9D5BD546ED275B45A20544DF07";


cpr_margin_cfg margins_0 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 0, .openLoop = 0, .closedLoop = 0, .maxFloorToCeil = 0 } }
};


cpr_quotient_cfg quotients_5671414031 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =    0 },
                                { .ro =  7, .quotient =    0 },
                                { .ro =  8, .quotient =    0 },
                                { .ro =  9, .quotient =    0 },
                                { .ro = 10, .quotient =    0 },
                                { .ro = 11, .quotient =    0 } },
    .kvs = (uint32[]) { 186, 176, 174, 198, 204, 204 },
};


cpr_quotient_cfg quotients_18314889212 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  606 },
                                { .ro =  7, .quotient =  568 },
                                { .ro =  8, .quotient =  803 },
                                { .ro =  9, .quotient =  784 },
                                { .ro = 10, .quotient =  843 },
                                { .ro = 11, .quotient =  823 } },
    .kvs = (uint32[]) { 186, 176, 174, 198, 204, 204 },
};


cpr_fuse_data fuses_4205407389 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_LSB, CPR1_TARG_VOLT_NOM) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = NULL,
};


cpr_quotient_cfg quotients_13976616038 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  606 },
                                { .ro =  7, .quotient =  568 },
                                { .ro =  8, .quotient =  818 },
                                { .ro =  9, .quotient =  786 },
                                { .ro = 10, .quotient =  848 },
                                { .ro = 11, .quotient =  826 } },
    .kvs = (uint32[]) { 186, 176, 174, 198, 204, 204 },
};


cpr_quotient_cfg quotients_13391412223 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  699 },
                                { .ro =  7, .quotient =  656 },
                                { .ro =  8, .quotient =  905 },
                                { .ro =  9, .quotient =  885 },
                                { .ro = 10, .quotient =  950 },
                                { .ro = 11, .quotient =  928 } },
    .kvs = (uint32[]) { 186, 176, 174, 198, 204, 204 },
};


cpr_fuse_data fuses_1642554776 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW5_LSB, CPR1_TARG_VOLT_TUR) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = NULL,
};


uint32 cpr_get_mode_idx_0(cpr_voltage_mode val)
{
    switch(val) {
        case CPR_VOLTAGE_MODE_LOW_SVS: return 0;
        case CPR_VOLTAGE_MODE_SVS: return 1;
        case CPR_VOLTAGE_MODE_SVS_L1: return 2;
        case CPR_VOLTAGE_MODE_NOMINAL: return 3;
        case CPR_VOLTAGE_MODE_NOMINAL_L1: return 4;
        case CPR_VOLTAGE_MODE_SUPER_TURBO: return 5;
        default:
            CPR_LOG_FATAL("Unsupported voltage mode: 0x%x", val);
            break;
    }

    return 0;
}


cpr_voltage_plan voltage_plan_11762603058 =
{
    .modesCount = 6,
    .idxLookupFunc = cpr_get_mode_idx_0,
    .tempAdjControls = (cpr_temp_adjust_controls[]) {  
        {
        .tempAdjTempPoints = (cpr_temp_adjust_temp_points[]) { { .cprMarginTempPoint0 = 0, .cprMarginTempPoint1 = 0, .cprMarginTempPoint2 = 0 } },
        .tempAdjMiscReg = (cpr_temp_adjust_misc_reg[]) { { .tempSensorStartId = 0, .tempSensorEndId = 0 } },
        .tempAdjCtl = (cpr_temp_adjust_margin_adj_ctl[]) { { .singleStepQuot = 0, .perRoKvMarginEn = 0 } },
        } },
    .supportedModes = (cpr_voltage_mode[]) { CPR_VOLTAGE_MODE_LOW_SVS,
                                             CPR_VOLTAGE_MODE_SVS,
                                             CPR_VOLTAGE_MODE_SVS_L1,
                                             CPR_VOLTAGE_MODE_NOMINAL,
                                             CPR_VOLTAGE_MODE_NOMINAL_L1,
                                             CPR_VOLTAGE_MODE_SUPER_TURBO },
    .minMode = NULL,
    .modes = (struct cpr_voltage_data[]) {
        // LOW_SVS
        {.fref = 848000, .freqDelta = 0, .fuses = NULL, .quotients = &quotients_5671414031, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  848000, .floor =  848000, .freq =       0} } },

        // SVS
        {.fref = 848000, .freqDelta = 0, .fuses = NULL, .quotients = &quotients_5671414031, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  848000, .floor =  848000, .freq =       0} } },

        // SVS_L1
        {.fref = 848000, .freqDelta = 0, .fuses = NULL, .quotients = &quotients_5671414031, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  848000, .floor =  848000, .freq =       0} } },

        // NOMINAL
        {.fref = 952000, .freqDelta = 0, .fuses = &fuses_4205407389, .quotients = &quotients_18314889212, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  944000, .floor =  848000, .freq =       0} } },

        // NOMINAL_L1
        {.fref = 952000, .freqDelta = 0, .fuses = &fuses_4205407389, .quotients = &quotients_13976616038, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  952000, .floor =  848000, .freq =       0} } },

        // SUPER_TURBO
        {.fref = 1024000, .freqDelta = 0, .fuses = &fuses_1642554776, .quotients = &quotients_13391412223, .margins = &margins_0,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling = 1024000, .floor =  848000, .freq =       0} } }},
};


cpr_margin_cfg margins_870 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = 60, .maxFloorToCeil = 60 } }
};


cpr_quotient_cfg quotients_15543540899 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  202 },
                                { .ro =  7, .quotient =  193 },
                                { .ro =  8, .quotient =  331 },
                                { .ro =  9, .quotient =  326 },
                                { .ro = 10, .quotient =  337 },
                                { .ro = 11, .quotient =  345 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_6235187012 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_LOWSVS) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_OFFSET_LOWSVS) } } },
};


cpr_margin_cfg margins_899 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = 65, .maxFloorToCeil = 64 } }
};


cpr_quotient_cfg quotients_15344839890 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  317 },
                                { .ro =  7, .quotient =  300 },
                                { .ro =  8, .quotient =  476 },
                                { .ro =  9, .quotient =  463 },
                                { .ro = 10, .quotient =  489 },
                                { .ro = 11, .quotient =  489 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_7562370877 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_SVS) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_MSB, CPR0_TARG_VOLT_OFFSET_LOWSVS) } } },
};


cpr_margin_cfg margins_733 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = 25, .maxFloorToCeil = 98 } }
};


cpr_quotient_cfg quotients_11451110573 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  411 },
                                { .ro =  7, .quotient =  387 },
                                { .ro =  8, .quotient =  595 },
                                { .ro =  9, .quotient =  572 },
                                { .ro = 10, .quotient =  611 },
                                { .ro = 11, .quotient =  602 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_4350869267 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_SVS) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_OFFSET_SVS) } } },
};


cpr_margin_cfg margins_652 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = 10, .maxFloorToCeil = 92 } }
};


cpr_quotient_cfg quotients_14213015954 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  522 },
                                { .ro =  7, .quotient =  489 },
                                { .ro =  8, .quotient =  727 },
                                { .ro =  9, .quotient =  696 },
                                { .ro = 10, .quotient =  748 },
                                { .ro = 11, .quotient =  732 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_6767873943 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_NOM) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_OFFSET_NOM) } } },
};


cpr_margin_cfg margins_656 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = 0, .maxFloorToCeil = 146 } }
};


cpr_quotient_cfg quotients_18578366758 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  606 },
                                { .ro =  7, .quotient =  568 },
                                { .ro =  8, .quotient =  818 },
                                { .ro =  9, .quotient =  786 },
                                { .ro = 10, .quotient =  848 },
                                { .ro = 11, .quotient =  826 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_3329597576 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_NOM) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_OFFSET_TUR) } } },
};


cpr_margin_cfg margins_560 = {
    .count = 1,
    .data = (cpr_margin_data[]) { { .cprRevMin = 0, .cprRevMax = 255, .openLoop = 0, .closedLoop = -20, .maxFloorToCeil = 150 } }
};


cpr_quotient_cfg quotients_9854876841 =
{
    .count = 6,
    .quots = (cpr_quotient[]) { { .ro =  6, .quotient =  699 },
                                { .ro =  7, .quotient =  656 },
                                { .ro =  8, .quotient =  905 },
                                { .ro =  9, .quotient =  885 },
                                { .ro = 10, .quotient =  950 },
                                { .ro = 11, .quotient =  928 } },
    .kvs = (uint32[]) { 174, 161, 203, 195, 215, 204 },
};


cpr_fuse_data fuses_2417127291 =
{
    .volt       = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_TUR) } } },
    .quot       = NULL,
    .rosel      = NULL,
    .quotOffset = NULL,
    .voltOffset = (cpr_fuse[]) { {.count = 1, .data = (struct raw_fuse_data[]) { CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW4_LSB, CPR0_TARG_VOLT_OFFSET_TUR) } } },
};


cpr_voltage_plan voltage_plan_16453213830 =
{
    .modesCount = 6,
    .idxLookupFunc = cpr_get_mode_idx_0,
    .tempAdjControls = (cpr_temp_adjust_controls[]) {  
        {
        .tempAdjTempPoints = (cpr_temp_adjust_temp_points[]) { { .cprMarginTempPoint0 = 5, .cprMarginTempPoint1 = 25, .cprMarginTempPoint2 = 85 } },
        .tempAdjMiscReg = (cpr_temp_adjust_misc_reg[]) { { .tempSensorStartId = 14, .tempSensorEndId = 3 } },
        .tempAdjCtl = (cpr_temp_adjust_margin_adj_ctl[]) { { .singleStepQuot = 15, .perRoKvMarginEn = 0 } },
        } },
    .supportedModes = (cpr_voltage_mode[]) { CPR_VOLTAGE_MODE_LOW_SVS,
                                             CPR_VOLTAGE_MODE_SVS,
                                             CPR_VOLTAGE_MODE_SVS_L1,
                                             CPR_VOLTAGE_MODE_NOMINAL,
                                             CPR_VOLTAGE_MODE_NOMINAL_L1,
                                             CPR_VOLTAGE_MODE_SUPER_TURBO },
    .minMode = NULL,
    .modes = (struct cpr_voltage_data[]) {
        // LOW_SVS
        {.fref = 648000, .freqDelta = 0, .fuses = &fuses_6235187012, .quotients = &quotients_15543540899, .margins = &margins_870,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  648000, .floor =  504000, .freq =       0} } },

        // SVS
        {.fref = 720000, .freqDelta = 0, .fuses = &fuses_7562370877, .quotients = &quotients_15344839890, .margins = &margins_899,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  720000, .floor =  592000, .freq =       0} } },

        // SVS_L1
        {.fref = 792000, .freqDelta = 0, .fuses = &fuses_4350869267, .quotients = &quotients_11451110573, .margins = &margins_733,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  792000, .floor =  648000, .freq =       0} } },

        // NOMINAL
        {.fref = 864000, .freqDelta = 0, .fuses = &fuses_6767873943, .quotients = &quotients_14213015954, .margins = &margins_652,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  864000, .floor =  712000, .freq =       0} } },

        // NOMINAL_L1
        {.fref = 920000, .freqDelta = 0, .fuses = &fuses_3329597576, .quotients = &quotients_18578366758, .margins = &margins_656,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  920000, .floor =  744000, .freq =       0} } },

        // SUPER_TURBO
        {.fref = 992000, .freqDelta = 0, .fuses = &fuses_2417127291, .quotients = &quotients_9854876841, .margins = &margins_560,
         .marginModeTableSize = 1,
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) { { .tempBand0 = 0, .tempBand1 = 0, .tempBand2 = 0, .tempBand3 = 0 } },
         .subModesCount = 1,
         .subModes = (struct cpr_freq_data[]) { {.ceiling =  992000, .floor =  784000, .freq =       0} } }},
};


cpr_sw_settings sw_settings =
{
    .stepquotmax          = 17,
    .stepquotmin          = 14,
    .autocontinterval     = 96000,
    .upthresh             = 0,
    .dnthresh             = 2,
    .consecup             = 0,
    .consecdn             = 2,
    .clamptimerinterval   = 0,
    .sensorbypass         = 0,
    .sensormask           = 0,
    .countrepeat          = 1,
    .countmode            = 0,
    .gcnts                = { (uint32[]){ 0,0,0,0,0,0,19,19,19,19,19,19,0,0,0,0,0 } },
};


static cpr_versioned_voltage_plan cpr_rail_cx_vvp = {
    .rail    = CPR_RAIL_CX,
    .list = (struct voltage_plan_list[]) {
        {
            .version = { CPR_FOUNDRY_ANY, CPR_CHIPINFO_VERSION(1,0), CPR_CHIPINFO_VERSION(1,255) },
            .cfg  = &voltage_plan_16453213830
        },
    },
    .count = 1,
    .agingCfg = NULL
};


static cpr_versioned_voltage_plan cpr_rail_mx_vvp = {
    .rail    = CPR_RAIL_MX,
    .list = (struct voltage_plan_list[]) {
        {
            .version = { CPR_FOUNDRY_ANY, CPR_CHIPINFO_VERSION(1,0), CPR_CHIPINFO_VERSION(1,255) },
            .cfg  = &voltage_plan_11762603058
        },
    },
    .count = 1,
    .agingCfg = NULL
};


const cpr_versioned_voltage_plan* cpr_get_versioned_voltage_plan(cpr_domain_id railId)
{
    switch(railId)
    {
        case CPR_RAIL_CX: return &cpr_rail_cx_vvp;
        case CPR_RAIL_MX: return &cpr_rail_mx_vvp;
        default:
            CPR_LOG_FATAL("Unsupported railId: 0x%x", railId);
            break;
    }

    return NULL;
}

