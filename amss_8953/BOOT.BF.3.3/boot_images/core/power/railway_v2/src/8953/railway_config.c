/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/railway_v2/src/8953/railway_config.c#1 $
$Date: 2015/12/15 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2012 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "railway_config.h"
#include "pm_ldo.h"
#include "pm_smps.h"

extern uint32 railway_get_corner_voltage(int rail, railway_corner corner);
/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

//
// BEGIN config data; should migrate to the system enumeration data method
//
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .rail_type = RAILWAY_RAIL_TYPE_MX,
            .vreg_name = "vddmx",

            //SMPS7A
            .pmic_rail_type = RAILWAY_SMPS_TYPE,
            .pmic_chip_id   = 0,
            .pmic_peripheral_index = PM_SMPS_7,

            .pmic_step_size = 8000,

            .initial_corner = RAILWAY_NOMINAL,

            .cpr_rail_id = CPR_RAIL_MX,
        },

        // VDDCX
        {
            .rail_type = RAILWAY_RAIL_TYPE_LOGIC,
            .vreg_name = "vddcx",

            // SMPS2A
            .pmic_rail_type = RAILWAY_SMPS_TYPE,
            .pmic_chip_id   = 0,
            .pmic_peripheral_index = PM_SMPS_2,

            .pmic_step_size = 8000,

            .initial_corner = RAILWAY_NOMINAL,

            .cpr_rail_id = CPR_RAIL_CX,
        },
    },

    .num_rails = 2,
};
//
// END config data
//

const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

void railway_target_init(void)
{
}

