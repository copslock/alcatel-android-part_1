/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_target_quotients.h"
#include "cpr_device_hw_version.h"

static const cpr_target_quotient_versioned_config_t tsmc_cx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  725,    707,    934,    859,    638,   625,     806,    755},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  646,    632,    846,    781,    569,   558,     728,    687},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   557,    546,    760,    711,    487,   494,     653,    625},
        {CPR_VOLTAGE_MODE_NOMINAL,      474,    466,    673,    644,    414,   423,     575,    565},
        {CPR_VOLTAGE_MODE_SVS_L1,       389,    383,    579,    564,    337,   347,     492,    497},
        {CPR_VOLTAGE_MODE_SVS,          275,    272,    436,    424,    235,   243,     365,    371},
    },
    .ro_kv_x_100 = {                    158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t umc_cx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   1327,   1254,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   1206,   1144,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,        970,    933,   1098,   1042,    550,   596,     401,    452},
        {CPR_VOLTAGE_MODE_SVS,           807,    778,    930,    881,    432,   479,     299,    348},
    },
    .ro_kv_x_100 = {                     250,    220,    250,    220,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t gf_cx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  681,   699,   983,   941,    641,   627,     879,    844},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  607,   627,   893,   856,    575,   559,     798,    769},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   531,   556,   808,   779,    512,   489,     722,    699},
        {CPR_VOLTAGE_MODE_NOMINAL,      470,   498,   739,   714,    460,   434,     660,    642},
        {CPR_VOLTAGE_MODE_SVS_L1,       382,   413,   634,   616,    383,   352,     565,    555},
        {CPR_VOLTAGE_MODE_SVS,          271,   302,   493,   484,    282,   251,     439,    437},
    },
    .ro_kv_x_100 = {                     149,    145,    180,    169,    131,   137,     162,    151},
};
static const cpr_target_quotient_rail_config_t cx_quotient_config_8937 =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_cx_quotients_8937,
		&umc_cx_quotients_8937,
		&gf_cx_quotients_8937,
    },
    .versioned_target_quotient_config_count = 3,
};

static const cpr_target_quotient_versioned_config_t tsmc_mx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   604,   591,   812,   758,    529,   534,     701,    666},
        {CPR_VOLTAGE_MODE_NOMINAL,      522,   511,   726,   691,    455,   463,     623,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       420,    413,   614,   596,    364,   373,     523,    524},
        {CPR_VOLTAGE_MODE_SVS,          420,    413,   614,   596,    364,   373,     523,    524},
    },
    .ro_kv_x_100 = {                     158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t umc_mx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  1329,   1260,   0,      0,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   0,      0,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   0,      0,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   0,      0,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,       1010,    971,   0,      0,    579,   626,     428,    479},
        {CPR_VOLTAGE_MODE_SVS,          1010,    971,   0,      0,    579,   626,     428,    479},
    },
    .ro_kv_x_100 = {                     250,    220,   0,      0,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t gf_mx_quotients_8937 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  699,   712,   995,   949,    652,   642,     903,    851},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  699,   712,   995,   949,    652,   642,     903,    851},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   584,   607,   870,   835,    557,   538,     789,    749},
        {CPR_VOLTAGE_MODE_NOMINAL,      523,   549,   801,   772,    505,   482,     727,    693},
        {CPR_VOLTAGE_MODE_SVS_L1,       418,   448,   678,   658,    415,   385,     614,    591},
		{CPR_VOLTAGE_MODE_SVS,       	418,   448,   678,   658,    415,   385,     614,    591},
    },
    .ro_kv_x_100 = {                     157,    147,    177,    163,    132,   143,     161,    145},
};
static const cpr_target_quotient_rail_config_t mx_quotient_config_8937 =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_mx_quotients_8937,
		&umc_mx_quotients_8937,
		&gf_mx_quotients_8937,
    },
    .versioned_target_quotient_config_count = 3,
};

static cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config_8937 = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &cx_quotient_config_8937,
        &mx_quotient_config_8937,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8937,
    .rail_config_count = 2,
};

//For 8940 Target

static const cpr_target_quotient_versioned_config_t tsmc_cx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev    s                  CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  725,    707,    934,    859,    638,   625,     806,    755},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  646,    632,    846,    781,    569,   558,     728,    687},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   557,    546,    760,    711,    487,   494,     653,    625},
        {CPR_VOLTAGE_MODE_NOMINAL,      474,    466,    673,    644,    414,   423,     575,    565},
        {CPR_VOLTAGE_MODE_SVS_L1,       389,    383,    579,    564,    337,   347,     492,    497},
        {CPR_VOLTAGE_MODE_SVS,          275,    272,    436,    424,    235,   243,     365,    371},
    },
    .ro_kv_x_100 = {                    158,    150,    175,    158,    138,   133,     158,    138},
};

static const cpr_target_quotient_versioned_config_t gf_cx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  644,   671,   977,   930,    612,   590,     882,    830},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  569,   598,   887,   845,    547,   521,     800,    754},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   495,   529,   804,   767,    484,   454,     724,    685},
        {CPR_VOLTAGE_MODE_NOMINAL,      437,   472,   735,   703,    434,   401,     662,    628},
        {CPR_VOLTAGE_MODE_SVS_L1,       351,   389,   632,   605,    358,   323,     568,    541},
        {CPR_VOLTAGE_MODE_SVS,          246,   282,   493,   473,    261,   226,     442,    423},
    },
    .ro_kv_x_100 = {                     150,    146,    180,    170,    131,   137,     164,    151},
};

static const cpr_target_quotient_versioned_config_t smic_cx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  713,   691,   908,   824,    619,   639,     797,    731},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  633,   619,   821,   746,    554,   568,     721,    661},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   556,   551,   740,   674,    493,   501,     651,    597},
        {CPR_VOLTAGE_MODE_NOMINAL,      496,   496,   674,   615,    444,   447,     593,    544},
        {CPR_VOLTAGE_MODE_SVS_L1,       405,   414,   574,   525,    370,   367,     506,    463},
        {CPR_VOLTAGE_MODE_SVS,          291,   307,   443,   406,    274,   266,     390,    356},
    },
    .ro_kv_x_100 = {                     160,    144,    173,    155,    130,   142,     151,    139},
};

static const cpr_target_quotient_rail_config_t cx_quotient_config_8940 =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_cx_quotients_8940,
		&gf_cx_quotients_8940,
		&smic_cx_quotients_8940,
    },
    .versioned_target_quotient_config_count = 3,
};

static const cpr_target_quotient_versioned_config_t tsmc_mx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   604,   591,   812,   758,    529,   534,     701,    666},
        {CPR_VOLTAGE_MODE_NOMINAL,      522,   511,   726,   691,    455,   463,     623,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       420,    413,   614,   596,    364,   373,     523,    524},
        {CPR_VOLTAGE_MODE_SVS,          420,    413,   614,   596,    364,   373,     523,    524},
    },
    .ro_kv_x_100 = {                     158,    150,    175,    158,    138,   133,     158,    138},
};

static const cpr_target_quotient_versioned_config_t gf_mx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  739,   758,   1079,   1024,    690,   674,     973,    913},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  660,   683,   989,   940,    623,   603,     892,    839},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   548,   579,   866,   826,    530,   501,     779,    736},
        {CPR_VOLTAGE_MODE_NOMINAL,      489,   522,   798,   762,    479,   446,     718,    680},
        {CPR_VOLTAGE_MODE_SVS_L1,       387,   423,   677,   648,    390,   353,     607,    578},
		{CPR_VOLTAGE_MODE_SVS,       	387,   423,   677,   648,    390,   353,     607,    578},
    },
    .ro_kv_x_100 = {                     156,    148,    178,    167,    133,   142,     162,    148},
};

static const cpr_target_quotient_versioned_config_t smic_mx_quotients_8940 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 6,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  803,   772,   1005,   914,    695,   719,     881,    808},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  723,   700,   918,   836,    630,   648,     805,    739},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   608,   599,   798,   729,    539,   547,     700,    644},
        {CPR_VOLTAGE_MODE_NOMINAL,      547,   544,   732,   671,    489,   493,     643,    591},
        {CPR_VOLTAGE_MODE_SVS_L1,       440,   447,   615,   566,    402,   399,     541,    497},
		{CPR_VOLTAGE_MODE_SVS,       	440,   447,   615,   566,    402,   399,     541,    497},
    },
    .ro_kv_x_100 = {                     160,    144,    173,    155,    130,   142,     151,    139},
};

static const cpr_target_quotient_rail_config_t mx_quotient_config_8940 =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_mx_quotients_8940,
		&gf_mx_quotients_8940,
		&smic_mx_quotients_8940,
    },
    .versioned_target_quotient_config_count = 3,
};

static cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config_8940 = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &cx_quotient_config_8940,
        &mx_quotient_config_8940,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8940,
    .rail_config_count = 2,
};

const cpr_bsp_hw_target_quotient_type_t cpr_bsp_hw_target_quotient =
{
	.cpr_target_quotient = (cpr_target_quotient_global_config_t*[])
	{
		&cpr_bsp_target_quotient_config_8937,
		&cpr_bsp_target_quotient_config_8940,	
	},
	.cpr_bsp_target_quotient_type_count = 2,
};

