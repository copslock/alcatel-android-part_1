/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_voltage_ranges.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"

// Mx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8917_mx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling, Floor,   Fuse-Ref,  max_floor_to_ceiling, OpenLoop_adjust, Corr-factor, volt-Fuse-Type			offset-Fuse-Type
        {CPR_VOLTAGE_MODE_SVS,         1050000, 1050000, 1050000,		0,					0,					0,           CPR_FUSE_NO_FUSE, 		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 1050000, 1162500,   	0,					0,					0,           CPR_FUSE_SVS_PLUS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1062500, 1225000,   	0,					0,					0,           CPR_FUSE_NOMINAL,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1137500, 1287500,   	0,					0,					0,           CPR_FUSE_NOMINAL_PLUS,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1187500, 1350000,   	0,					0,					0,           CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE}, 
    },

    .voltage_level_count = 5,
};

static cpr_config_rail_voltage_ranges_t mx_voltage_ranges_8917 =
{
    .rail_id = CPR_RAIL_MX,
    .disable_fused_floor = true,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8917_mx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// Cx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8917_cx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,   Floor, Fuse-Ref,  max_floor_to_ceiling,	OpenLoop_adjust,   Corr-factor, Volt-Fuse-Type,	   offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,         1050000, 900000, 1050000, 			0,				0,					0, 		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 975000, 1162500, 			0,				0,					0,		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1037500,1225000, 			0,				0,					0, 		CPR_FUSE_NOMINAL,	CPR_FUSE_NOMINAL_OFFSET},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1075000,1287500, 			0,				0,					0, 		CPR_FUSE_NOMINAL,	CPR_FUSE_TURBO_OFFSET},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1125000,1350000, 			0,				0,					0, 		CPR_FUSE_TURBO,		CPR_FUSE_TURBO_OFFSET},
    },

    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t cx_voltage_ranges_8917 =
{
    .rail_id = CPR_RAIL_CX,
	.disable_fused_floor = false,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8917_cx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// MSS Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8917_mss_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,   Floor, Fuse-Ref, max_floor_to_ceiling,  OpenLoop_adjust,   Corr-factor,   Volt-Fuse-Type, 	    Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,         1050000, 900000, 1050000, 		0, 					0,					0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 975000, 1162500, 		0, 					0,					0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1037500,1225000, 		0, 					0,					0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1075000,1287500, 		0, 					0,					0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1125000,1350000, 		0, 					0,					0,			CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t mss_voltage_ranges_8917 =
{
    .rail_id = CPR_RAIL_MSS,
	.disable_fused_floor = false,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8917_mss_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};


cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config_8917 =
{
    .rail_voltage_ranges = (cpr_config_rail_voltage_ranges_t*[])
    {
		&mx_voltage_ranges_8917,
		&cx_voltage_ranges_8917,
        &mss_voltage_ranges_8917,

    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8917,
    .rail_voltage_ranges_count = 3,
};

// 8920 Target Voltages

static const cpr_config_versioned_voltage_ranges_t TSMC_8920_mx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                        Ceiling, Floor,   Fuse-Ref,  max_floor_to_ceiling, OpenLoop_adjust, Corr-factor, volt-Fuse-Type			offset-Fuse-Type
        {CPR_VOLTAGE_MODE_SVS,         1050000, 1050000, 1050000,		0,					0,					0,           CPR_FUSE_NO_FUSE, 		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 1050000, 1162500,   	0,					0,					0,           CPR_FUSE_SVS_PLUS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1062500, 1225000,   	0,					0,					0,           CPR_FUSE_NOMINAL,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1137500, 1287500,   	0,					0,					0,           CPR_FUSE_NOMINAL_PLUS,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1187500, 1350000,   	0,					0,					0,           CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE}, 
    },

    .voltage_level_count = 5,
};

static cpr_config_rail_voltage_ranges_t mx_voltage_ranges_8920 =
{
    .rail_id = CPR_RAIL_MX,
    .disable_fused_floor = true,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8920_mx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// Cx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8920_cx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,   Floor, Fuse-Ref,  max_floor_to_ceiling,	OpenLoop_adjust,   Corr-factor, Volt-Fuse-Type,	   offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,         1050000, 900000, 1050000, 			0,				0,					0, 		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 975000, 1162500, 			0,				0,					0,		CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1037500,1225000, 			0,				0,					0, 		CPR_FUSE_NOMINAL,	CPR_FUSE_NOMINAL_OFFSET},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1075000,1287500, 			0,				0,					0, 		CPR_FUSE_NOMINAL,	CPR_FUSE_TURBO_OFFSET},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1125000,1350000, 			0,				0,					0, 		CPR_FUSE_TURBO,		CPR_FUSE_TURBO_OFFSET},
    },

    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t cx_voltage_ranges_8920 =
{
    .rail_id = CPR_RAIL_CX,
	.disable_fused_floor = false,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8920_cx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

// MSS Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t TSMC_8920_mss_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                      Ceiling,   Floor, Fuse-Ref, max_floor_to_ceiling,  OpenLoop_adjust,   Corr-factor,   Volt-Fuse-Type, 	    Offset-Fuse-Type
		{CPR_VOLTAGE_MODE_SVS,         1050000, 900000, 1050000, 		0, 					0,					0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SVS_L1,      1162500, 975000, 1162500, 		0, 					0,					0,			CPR_FUSE_SVS,		CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL,     1225000, 1037500,1225000, 		0, 					0,					0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,  1287500, 1075000,1287500, 		0, 					0,					0,			CPR_FUSE_NOMINAL,	CPR_FUSE_NO_FUSE},
		{CPR_VOLTAGE_MODE_SUPER_TURBO, 1350000, 1125000,1350000, 		0, 					0,					0,			CPR_FUSE_TURBO,		CPR_FUSE_NO_FUSE},
    },
    .voltage_level_count = 5,
};

cpr_config_rail_voltage_ranges_t mss_voltage_ranges_8920 =
{
    .rail_id = CPR_RAIL_MSS,
	.disable_fused_floor = false,
	.openloop_adjustment = false,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
		&TSMC_8920_mss_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};


cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config_8920 =
{
    .rail_voltage_ranges = (cpr_config_rail_voltage_ranges_t*[])
    {
		&mx_voltage_ranges_8920,
		&cx_voltage_ranges_8920,
        &mss_voltage_ranges_8920,

    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8920,
    .rail_voltage_ranges_count = 3,
};


const cpr_config_hw_voltage_ranges_t cpr_bsp_hw_voltage_ranges =
{
	.cpr_config_global_voltage_ranges = (cpr_config_global_voltage_ranges_t*[])
	{
		&cpr_bsp_voltage_ranges_config_8917,
		&cpr_bsp_voltage_ranges_config_8920,
	},
	.cpr_bsp_hw_voltage_ranges_count = 2,
};
// Rail PMIC step-sizes

const cpr_config_misc_info_t cpr_bsp_misc_info_config =
{
    .cpr_rev_fuse = {HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, 4, (1<<6) | (1<<5) | (1<<4)}, //0xA422C 6:4 CPR_GLOBAL_RC
    .rail_info = (const cpr_config_misc_rail_info_t[])
    {
        //Rail,             PMIC Step-Size
        {CPR_RAIL_MX,                12500},
        {CPR_RAIL_CX,                12500},
		{CPR_RAIL_MSS,               12500},
    },
    .rail_info_count = 3,
};
