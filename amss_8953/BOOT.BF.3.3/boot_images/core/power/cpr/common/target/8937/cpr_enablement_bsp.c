/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_enablement.h"
#include "cpr_device_hw_version.h"
#include "cpr_voltage_ranges.h"
#include "cpr_qfprom.h"
#include "HALhwio.h"
#include "CoreVerify.h"

////////////////////////////////////////////////
// Mx config
////////////////////////////////////////////////
/*#define EFUSE_VOLTAGE_MULTIPLIER 10000  //All rail fuses use a 10mV multiplier
#define MSS_PMIC_STEP_SIZE 12500        //MSS pmic step size

#define MSS_TURBO_GLOBAL_CEILING 1112500      // defined in go/voltageplan
#define MSS_TURBO_GLOBAL_FLOOR    937500 */

/*===========================================================================
FUNCTION: calc_mss_turbo_open_loop_voltage

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
/*static uint32 calc_mss_turbo_open_loop_voltage(cpr_rail_id_t rail_id, cpr_fuse_type fuse_type)
{
    const int32 voltage_offset_steps =  cpr_fuses_get_fuse_value(rail_id, fuse_type);
    const int32 voltage_offset = EFUSE_VOLTAGE_MULTIPLIER * voltage_offset_steps;
    
    uint32 open_loop_voltage = voltage_offset + MSS_TURBO_GLOBAL_CEILING;

    //Now clip the Open-Loop voltage to the floor/ceiling limits.
    open_loop_voltage = MIN(MSS_TURBO_GLOBAL_CEILING, MAX(open_loop_voltage, MSS_TURBO_GLOBAL_FLOOR));

    //Now round up to PMIC step size.
    if(open_loop_voltage%MSS_PMIC_STEP_SIZE)
    {
        open_loop_voltage += (MSS_PMIC_STEP_SIZE - (open_loop_voltage%MSS_PMIC_STEP_SIZE));
    }
    CORE_VERIFY(open_loop_voltage<=MSS_TURBO_GLOBAL_CEILING);
    
    return open_loop_voltage;
}*/


/*===========================================================================
FUNCTION: mx_turbo_global_max_open_loop

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static uint32 mx_turbo_global_max_open_loop(cpr_enablement_custom_voltage_calculation_type_t type, cpr_voltage_mode_t mode)
{
    // all modes other than SVS (its already pinned)
    if(mode != CPR_VOLTAGE_MODE_SVS) 
    {
        uint32 mx_open_loop = cpr_config_open_loop_voltage(CPR_RAIL_MX, mode);
        uint32 cx_open_loop = cpr_config_open_loop_voltage(CPR_RAIL_CX, mode);
		if(mode == CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH)
		{
			mx_open_loop = MAX(mx_open_loop, cx_open_loop);
		}
		else
		{
			uint32 mss_open_loop = cpr_config_open_loop_voltage(CPR_RAIL_MSS, mode);
			mx_open_loop = MAX(mx_open_loop, MAX(cx_open_loop, mss_open_loop));
		}
		uint32 mx_open_loop_voltage = cpr_config_round_up_to_pmic_step(CPR_RAIL_MX, mx_open_loop);
        return mx_open_loop_voltage;
    }
    else if( type == CPR_OPEN_LOOP_VOLTAGE)
    {
        return cpr_config_open_loop_voltage(CPR_RAIL_MX, mode);
    }
    else if( type == CPR_CLOSED_LOOP_FLOOR_VOLTAGE )
    {
        return cpr_config_closed_loop_floor_voltage(CPR_RAIL_MX, mode);
    }
    else
    {
        CORE_VERIFY(0); //should not get here.
    }
}

// FOR 8937 TARGET

static cpr_enablement_versioned_rail_config_t TSMC_8937_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
	.custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,            100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SVS_L1,         100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,        100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,     100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,    100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 100,                NULL,                   1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};
static cpr_enablement_versioned_rail_config_t UMC_8937_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),  0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,       0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  0,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 0,               NULL,                 1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t GF_8937_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,       0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  0,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 0,               NULL,                  1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static const cpr_enablement_rail_config_t mx_8937_cpr_enablement =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
		&TSMC_8937_mx_versioned_cpr_enablement,
		&UMC_8937_mx_versioned_cpr_enablement,
		&GF_8937_mx_versioned_cpr_enablement,
    },
    .versioned_rail_config_count = 3,
};

////////////////////////////////////////////////
// Cx config
////////////////////////////////////////////////

static cpr_enablement_versioned_rail_config_t TSMC_8937_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          25,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SVS_L1,       50,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      50,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   25,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  63,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 3,                NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};
static cpr_enablement_versioned_rail_config_t UMC_8937_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 100,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t GF_8937_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         38,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      50,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     25,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  25,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 33,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 28,             NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static const cpr_enablement_rail_config_t cx_8937_cpr_enablement =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &TSMC_8937_versioned_cpr_enablement,
		&UMC_8937_versioned_cpr_enablement,
		&GF_8937_versioned_cpr_enablement,
    },
    .versioned_rail_config_count = 3,
};

static cpr_enablement_config_t cpr_bsp_enablement_config_8937 =
{
    .rail_enablement_config = (const cpr_enablement_rail_config_t*[])
    {
		&mx_8937_cpr_enablement,
	    &cx_8937_cpr_enablement,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8937,
    .rail_enablement_config_count = 2,
};

// FOR 8940 TARGET
static cpr_enablement_versioned_rail_config_t TSMC_8940_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
	.custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SVS_L1,       100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_NOMINAL_L1,   100,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  100,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  100,              NULL,                  1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t GF_8940_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,       0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  0,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  0,              NULL,                  1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t SMIC_8940_mx_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .custom_voltage_fn = mx_turbo_global_max_open_loop,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,       0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  0,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  0,              NULL,                  1},
    },
    .supported_level_count = 6,
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static const cpr_enablement_rail_config_t mx_8940_cpr_enablement =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
		&TSMC_8940_mx_versioned_cpr_enablement,
		&GF_8940_mx_versioned_cpr_enablement,
		&SMIC_8940_mx_versioned_cpr_enablement,
    },
    .versioned_rail_config_count = 3,
};

////////////////////////////////////////////////
// Cx config
////////////////////////////////////////////////

static cpr_enablement_versioned_rail_config_t TSMC_8940_versioned_cpr_enablement_v1_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0x2},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          58,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SVS_L1,       51,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      70,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   53,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  61,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  78,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t TSMC_8940_versioned_cpr_enablement_v1_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0x2,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,          45,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SVS_L1,       53,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,      31,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   34,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  44,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH,  78,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t GF_8940_versioned_cpr_enablement_v1_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           2},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         40,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      41,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     72,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  61,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 85,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 100,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t GF_8940_versioned_cpr_enablement_v1_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 2,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         80,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      90,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     54,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  63,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 28,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 5,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t SMIC_8940_versioned_cpr_enablement_v1_1 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0x2},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         0,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      13,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     47,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  70,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 53,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 62,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static cpr_enablement_versioned_rail_config_t SMIC_8940_versioned_cpr_enablement_v1_2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry       Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0x2,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {
        //Mode                 Static-Margin (mV) Custom static margin function    aging_scaling_factor
        {CPR_VOLTAGE_MODE_SVS,         75,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SVS_L1,      89,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL,     32,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,  42,                   NULL,                  1},
        {CPR_VOLTAGE_MODE_SUPER_TURBO, 41,                   NULL,                  1},
		{CPR_VOLTAGE_MODE_SUPER_TURBO_HIGH, 0,              NULL,                  1},
    },
    .supported_level_count = 6,  
    .mode_to_settle = (const cpr_voltage_mode_t[])
    {
        CPR_VOLTAGE_MODE_SUPER_TURBO
    },
    .mode_to_settle_count = 1,
    .aging_static_margin_limit = 15, //mV
};

static const cpr_enablement_rail_config_t cx_8940_cpr_enablement =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &TSMC_8940_versioned_cpr_enablement_v1_1,
		&TSMC_8940_versioned_cpr_enablement_v1_2,
		&GF_8940_versioned_cpr_enablement_v1_1,
		&GF_8940_versioned_cpr_enablement_v1_2,
		&SMIC_8940_versioned_cpr_enablement_v1_1,
		&SMIC_8940_versioned_cpr_enablement_v1_2,
    },
    .versioned_rail_config_count = 6,
};

static cpr_enablement_config_t cpr_bsp_enablement_config_8940 =
{
    .rail_enablement_config = (const cpr_enablement_rail_config_t*[])
    {
		&mx_8940_cpr_enablement,
	    &cx_8940_cpr_enablement,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8940,
    .rail_enablement_config_count = 2,
};

const cpr_bsp_hw_enablement_type_t cpr_bsp_hw_enablement_type =
{
	.cpr_enablement_config = (cpr_enablement_config_t*[])
	{
		&cpr_bsp_enablement_config_8937,
		&cpr_bsp_enablement_config_8940,	
	},
	.cpr_bsp_hw_enablement_type_count = 2,
};

