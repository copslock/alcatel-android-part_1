/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_target_quotients.h"
#include "cpr_device_hw_version.h"

static const cpr_target_quotient_versioned_config_t tsmc_cx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  646,    632,    846,    781,    569,   558,     728,    687},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   557,    546,    760,    711,    487,   494,     653,    625},
        {CPR_VOLTAGE_MODE_NOMINAL,      474,    466,    673,    644,    414,   423,     575,    565},
        {CPR_VOLTAGE_MODE_SVS_L1,       389,    383,    579,    564,    337,   347,     492,    497},
        {CPR_VOLTAGE_MODE_SVS,          275,    272,    436,    424,    235,   243,     365,    371},
    },
    .ro_kv_x_100 = {                    158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t umc_cx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   1457,   1373,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   1327,   1254,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   1206,   1144,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,        970,    933,   1098,   1042,    550,   596,     401,    452},
        {CPR_VOLTAGE_MODE_SVS,           807,    778,    930,    881,    432,   479,     299,    348},
    },
    .ro_kv_x_100 = {                     250,    220,    250,    220,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t smic_cx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                       CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  639,    595,    875,    768,    528,    567,     757,    671},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   565,    530,    796,    697,    470,    503,     689,    608},
        {CPR_VOLTAGE_MODE_NOMINAL,      506,    477,    731,    639,    423,    451,     632,    557},
        {CPR_VOLTAGE_MODE_SVS_L1,       422,    401,    635,    553,    356,    378,     550,    482},
        {CPR_VOLTAGE_MODE_SVS,          309,    297,    501,    433,    262,    278,     434,    375},
    },
    .ro_kv_x_100 = {                     153,    141,    174,    158,    126,   134,     151,    140},
};
static const cpr_target_quotient_rail_config_t cx_quotient_config_8917 =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_cx_quotients_8917,
		&umc_cx_quotients_8917,
		&smic_cx_quotients_8917,
    },
    .versioned_target_quotient_config_count = 3,
};

static const cpr_target_quotient_versioned_config_t tsmc_mx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   604,   591,   812,   758,    529,   534,     701,    666},
        {CPR_VOLTAGE_MODE_NOMINAL,      522,   511,   726,   691,    455,   463,     623,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       420,    413,   614,   596,    364,   373,     523,    524},
        {CPR_VOLTAGE_MODE_SVS,          420,    413,   614,   596,    364,   373,     523,    524},
    },
    .ro_kv_x_100 = {                     158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t umc_mx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_UMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},     
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  1329,   1260,   0,      0,    823,   855,     653,    697},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   1198,   1143,   0,      0,    721,   760,     556,    605},
        {CPR_VOLTAGE_MODE_NOMINAL,      1078,   1033,   0,      0,    629,   674,     473,    524},
        {CPR_VOLTAGE_MODE_SVS_L1,       1010,    971,   0,      0,    579,   626,     428,    479},
        {CPR_VOLTAGE_MODE_SVS,          1010,    971,   0,      0,    579,   626,     428,    479},
    },
    .ro_kv_x_100 = {                     250,    220,   0,      0,    160,   180,     120,    140},
};
static const cpr_target_quotient_versioned_config_t smic_mx_quotients_8917 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  727,    678,   972,    858,   602,   644,     840,    749},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   616,    580,   854,    753,   515,   548,     739,    657},
        {CPR_VOLTAGE_MODE_NOMINAL,      556,    527,   790,    695,   467,   496,     683,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       456,    436,   677,    595,   387,   409,     586,    517},
        {CPR_VOLTAGE_MODE_SVS,          456,    436,   677,    595,   387,   409,     586,    517},
    },
    .ro_kv_x_100 = {                     157,    147,   177,   163,   132,   143,     161,    145},
};
static const cpr_target_quotient_rail_config_t mx_quotient_config_8917 =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_mx_quotients_8917,
		&umc_mx_quotients_8917,
		&smic_mx_quotients_8917,
    },
    .versioned_target_quotient_config_count = 3,
};

static cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config_8917 = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &cx_quotient_config_8917,
        &mx_quotient_config_8917,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8917,
    .rail_config_count = 2,
};

//8920 Target 

static const cpr_target_quotient_versioned_config_t tsmc_cx_quotients_8920 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  646,    632,    846,    781,    569,   558,     728,    687},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   557,    546,    760,    711,    487,   494,     653,    625},
        {CPR_VOLTAGE_MODE_NOMINAL,      474,    466,    673,    644,    414,   423,     575,    565},
        {CPR_VOLTAGE_MODE_SVS_L1,       389,    383,    579,    564,    337,   347,     492,    497},
        {CPR_VOLTAGE_MODE_SVS,          275,    272,    436,    424,    235,   243,     365,    371},
    },
    .ro_kv_x_100 = {                    158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t smic_cx_quotients_8920 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry        Chip Min Rev               Chip Max Rev                       CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  639,    595,    875,    768,    528,    567,     757,    671},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   565,    530,    796,    697,    470,    503,     689,    608},
        {CPR_VOLTAGE_MODE_NOMINAL,      506,    477,    731,    639,    423,    451,     632,    557},
        {CPR_VOLTAGE_MODE_SVS_L1,       422,    401,    635,    553,    356,    378,     550,    482},
        {CPR_VOLTAGE_MODE_SVS,          309,    297,    501,    433,    262,    278,     434,    375},
    },
    .ro_kv_x_100 = {                     153,    141,    174,    158,    126,   134,     151,    140},
};
static const cpr_target_quotient_rail_config_t cx_quotient_config_8920 =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_cx_quotients_8920,
		&smic_cx_quotients_8920,
    },
    .versioned_target_quotient_config_count = 2,
};

static const cpr_target_quotient_versioned_config_t tsmc_mx_quotients_8920 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  733,   715,   943,   867,    645,   631,     814,    762},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   604,   591,   812,   758,    529,   534,     701,    666},
        {CPR_VOLTAGE_MODE_NOMINAL,      522,   511,   726,   691,    455,   463,     623,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       420,    413,   614,   596,    364,   373,     523,    524},
        {CPR_VOLTAGE_MODE_SVS,          420,    413,   614,   596,    364,   373,     523,    524},
    },
    .ro_kv_x_100 = {                     158,    150,    175,    158,    138,   133,     158,    138},
};
static const cpr_target_quotient_versioned_config_t smic_mx_quotients_8920 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            // Foundry         Chip Min Rev               Chip Max Rev                      CPR Min Rev  CPR Max Rev
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0,           0xFF},
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level_count = 5,
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                         RO[0],  RO[1],  RO[2],  RO[3],  RO[4],  RO[5],  RO[6],  RO[7]
        {CPR_VOLTAGE_MODE_SUPER_TURBO,  727,    678,   972,    858,   602,   644,     840,    749},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,   616,    580,   854,    753,   515,   548,     739,    657},
        {CPR_VOLTAGE_MODE_NOMINAL,      556,    527,   790,    695,   467,   496,     683,    606},
        {CPR_VOLTAGE_MODE_SVS_L1,       456,    436,   677,    595,   387,   409,     586,    517},
        {CPR_VOLTAGE_MODE_SVS,          456,    436,   677,    595,   387,   409,     586,    517},
    },
    .ro_kv_x_100 = {                     157,    147,   177,   163,   132,   143,     161,    145},
};
static const cpr_target_quotient_rail_config_t mx_quotient_config_8920 =
{
    .rail_id = CPR_RAIL_MX,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &tsmc_mx_quotients_8920,
		&smic_mx_quotients_8920,
    },
    .versioned_target_quotient_config_count = 2,
};

static cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config_8920 = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &cx_quotient_config_8920,
        &mx_quotient_config_8920,
    },
	.supported_chipset = DALCHIPINFO_FAMILY_MSM8920,
    .rail_config_count = 2,
};

const cpr_bsp_hw_target_quotient_type_t cpr_bsp_hw_target_quotient =
{
	.cpr_target_quotient = (cpr_target_quotient_global_config_t*[])
	{
		&cpr_bsp_target_quotient_config_8917,
		&cpr_bsp_target_quotient_config_8920,
	},
	.cpr_bsp_target_quotient_type_count = 2,
};
