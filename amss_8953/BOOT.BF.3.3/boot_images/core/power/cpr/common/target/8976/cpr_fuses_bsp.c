/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_fuses.h"
#include "cpr_device_hw_version.h"
#include "cpr_qfprom.h"
#include "HALhwio.h"

// -----------------------------------------------------------------
// 8976 Fuses 
// -----------------------------------------------------------------
const cpr_bsp_fuse_versioned_config_t cpr_bsp_fuse_config_8976 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x1, 0xFF),
        },
        .foundry_range_count = 1,
    },
    .rail_fuse_config = (cpr_bsp_fuse_rail_config_t[])
    {
        {   //Vdd_Mx
            .rail_id = CPR_RAIL_MX,
            .number_of_fuses = 6,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS2,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW8_LSB , CPR3_SVS2_TARGET)
						},
                },
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_MSB , CPR3_SVS_TARGET)
                        },
                },
				{
                    .fuse_type = CPR_FUSE_SVS_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    //CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW8_LSB , CPR3_SVS2_TARGET)
							{
							   .fuse_address   = HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, 
                               .offset         = 0xD, 
                               .mask           = 0x3E000, 
							}
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_MSB  , CPR3_NOM_TARGET)
                        },
                },
				{
                    .fuse_type = CPR_FUSE_NOMINAL_PLUS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						   // CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_MSB  , CPR3_NOM_TARGET)
						   {
						    .fuse_address   = HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, 
                            .offset         = 0x12, 
                            .mask           = 0x7C0000, 
							}
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_MSB , CPR3_TURBO_TARGET)
                        },
                },
            },
        },
        {   //Vdd_Cx
            .rail_id = CPR_RAIL_CX,
            .number_of_fuses = 4, 
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB, CPR0_SVS_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB, CPR0_NOMINAL_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB, CPR0_TURBO_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_AGING,
                    .number_of_partial_fuse_configs = 2,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
							CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW6_MSB , CPR0_AGING_SENSOR_7_6),
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_LSB , CPR0_AGING_SENSOR_5_0)
                        },
                },
            }, 
        },
		{   //Vdd_Mss
            .rail_id = CPR_RAIL_MSS,
            .number_of_fuses = 4,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS2,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB12_ROW0_MSB , CPR1_SVS2_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB , CPR1_SVS_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB , CPR1_NOMINAL_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_LSB , CPR1_TURBO_TARGET)
                        },
                },
            },
        },
		{   //Vdd_Gfx
            .rail_id = CPR_RAIL_GFX,
            .number_of_fuses = 4,
            .individual_fuse_configs = (cpr_bsp_individual_fuse_config_t[])
            {
                {
                    .fuse_type = CPR_FUSE_SVS2,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_MSB , CPR2_SVS2_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_SVS,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_MSB , CPR2_SVS_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_NOMINAL,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_MSB , CPR2_NOMINAL_TARGET)
                        },
                },
                {
                    .fuse_type = CPR_FUSE_TURBO,
                    .number_of_partial_fuse_configs = 1,
                    .partial_individual_fuse_configs = (const cpr_bsp_part_individual_fuse_config_t[])
                        {
						    CPR_FUSE_MAPPING(QFPROM_CORR_CALIB_ROW7_MSB , CPR2_TURBO_TARGET)
                        },
                },
            },
        },
    },
    .number_of_fused_rails = 4,
};

const cpr_config_bsp_fuse_config_t cpr_bsp_fuse_config =
{
    .versioned_fuses = (const cpr_bsp_fuse_versioned_config_t*[])
    {
		&cpr_bsp_fuse_config_8976,
    },
    .versioned_fuses_count = 1,
};
