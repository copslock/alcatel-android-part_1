/*===========================================================================

                    LIMITS STUBS DEFINITIONS

DESCRIPTION
  Contains limits drivers stubs for images that do not require them.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/power/limits/src/limits_stub.c#1 $
$DateTime: 2015/07/02 04:11:47 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/16/14   clm     Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "lmh_limits.h"


/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================

**  Function :  limits_early_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver init in secure execution environment
*
* @return
*  None.
*
* @par Dependencies
*  None
*
*
*/

void limits_early_init(uint32 flag){}

/*===========================================================================

**  Function :  limits_late_init

** ==========================================================================
*/
/*!
*
*    Performs limits driver late init
*
* @return
*  None.
*
* @par Dependencies
*    Function to be called post qsee init
*
*/

void limits_late_init(void){}

