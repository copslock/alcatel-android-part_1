/*===========================================================================
                Unified Image Encryption (UIE) L1 Fuse Key

GENERAL DESCRIPTION
  Chipset-specific key source retrieval

Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                                 INCLUDE FILES
===========================================================================*/
#include <stddef.h>
#include <string.h>
#include "uie.h"
#include "uie_priv.h"
#include "uie_env.h"
#include "uiehwioreg.h"
#include "HALhwio.h"

#define SECURITY_CONTROL_CORE_REG_FEC_OFFSET  0x2000
#define SECURITY_CONTROL_CORE_REG_CORR_OFFSET 0x4000
#define SECURITY_CONTROL_CORE_REG_SW_BASE_OFFSET 0x6000

#define BREAKIF_SETSTATUS(cond, status_code) \
  if (cond) { status = status_code; break; }

/*
 * The 128-bit L1 fuse key is represented via three corrected fuse rows.
 * Each corrected row consists of 4 least significant bytes
 * and 3 most significant bytes (1 byte was used for forward error correction).
 * Of the third row, only two least significant bytes are used.
 */
typedef struct __attribute__((__packed__))
{
  uint8 row0_lsb[4];
  uint8 row0_msb[3];
  uint8 row1_lsb[4];
  uint8 row1_msb[3];
  uint8 row2_partial_lsb[2];
} uie_fuse_key_t;

bool uie_is_image_encryption_fuse_enabled(void)
{
  return false;
}

int uie_get_l1_fuse_key(uie_key_src_t key_source, uint8 *key, uint32 keylen)
{
  return UIE_STATUS_SUCCESS;
}

uint16 uie_get_fec()
{
  return 1;
}

void uie_clear_fec()
{
  //Do nothing
}
