/*===========================================================================
              Unified Image Encryption (UIE) Environment Interface

GENERAL DESCRIPTION
  Definitions and types for UIE (document 80-NP914-1 Rev A) on the SBL.

Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
===========================================================================*/
#ifndef _UIE_ENV_H
#define _UIE_ENV_H

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "boot_extern_dal_interface.h" // DALSYS_Malloc, DALSYS_Free
#include <stringl/stringl.h>           // memscpy

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
#define UIE_MALLOC  uie_sbl_malloc
#define UIE_FREE    DALSYS_Free
#define UIE_MEMSCPY memscpy

#define UIE_LOG_MSG(fmt, ...)
#define UIE_LOG_HEX(label, buf, buflen)

static inline void *uie_sbl_malloc(uint32 num_bytes)
{
  void *address = NULL;
  DALSYS_Malloc(num_bytes, &address);
  return address;
}

/**
 * @brief Map the device memory into the MMU for access
 *
 * @return - 0 on success; otherwise error
 */
static inline int uie_map_fuse_area(uint32 addr, uint32 len)
{
  return 0;
}

/**
 * @brief Unmap the device memory into the MMU for access
 *
 * @return - 0 on success; otherwise error
 */
static inline void uie_unmap_fuse_area(uint32 addr, uint32 len)
{
  return;
}

#endif
