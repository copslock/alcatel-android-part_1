#===============================================================================
#
# SECUREMSM Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2015 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/boot.bf/3.3/boot_images/core/securemsm/cryptodrivers/prng/chipset/build/msm8917.SConscript#6 $
#  $DateTime: 2016/03/24 05:41:35 $
#  $Author: pwbldsvc $
#  $Change: 10133974 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 09/26/11   nk      Intial checkin
#===============================================================================
import os
Import('env')
env = env.Clone()

# 8917 interface redirection
CHIPSETREDIRECTION = 'msm8917'

script_sconfname = None
if os.path.exists('../' + CHIPSETREDIRECTION + '/build/SConscript') :
   script_sconfname = '../' + CHIPSETREDIRECTION + '/build/SConscript'
else :
   print "Directory: [%s] doesnot exist"%script_sconfname
   raise RuntimeError, "msm8917  [%s] does not exist"%script_sconfname

DEL_FILES = [
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/apq8084.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8x10.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8x26.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/mdm9x25.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8x62.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8974.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/mpq8092.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8916.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8936.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/mdm9x35.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/mdm9x45.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8909.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8952.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/build/msm8976.SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/mpq8092/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8x26/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/apq8084/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8x10/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/mdm9x25/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8x62/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8974/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8916/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8936/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/mdm9x35/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/mdm9x45/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8909/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8976/build/SConscript',
    '${BUILD_ROOT}/core/securemsm/cryptodrivers/prng/chipset/msm8952/build/SConscript',
	
]

env.CleanPack(['SBL1_BOOT'], DEL_FILES)

#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------

# Remove target files
ALL_FILES = env.FindFiles(
  ['*'],
  '../')
BUILD_FILES = env.FindFiles(
  ['*'],
  '../build/')
TARGET_FILES = env.FindFiles(
  ['*'],
  '../${CHIPSET}')
TARGET_FILES_8917 = env.FindFiles(
  ['*'],
  '../msm8917')
TARGET_FILES_8937 = env.FindFiles(
  ['*'],
  '../msm8937')
TARGET_FILES_8953 = env.FindFiles(
  ['*'],
  '../msm8953')

if env['MSM_ID'] in ['8917', '8937', '8953']:
   PACK_FILES = list(set(ALL_FILES)- set(BUILD_FILES) - set(TARGET_FILES_8917) - set(TARGET_FILES_8937) - set(TARGET_FILES_8953))
else:
   PACK_FILES = list(set(ALL_FILES) - set(BUILD_FILES) - set(TARGET_FILES))
env.CleanPack(['SBL1_BOOT'], PACK_FILES)

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
script_scon = None
script_scon = '../' + CHIPSETREDIRECTION + '/build'

env.SConscript(dirs=[script_scon], exports='env')
