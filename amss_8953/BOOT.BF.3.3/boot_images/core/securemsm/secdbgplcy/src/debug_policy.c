/*===========================================================================

                     S E C U R I T Y    S E R V I C E S

                S E C U R E  B O O T   X.5 0 9  P A R S E R

                               M O D U L E

FILE:  debug_policy.c

DESCRIPTION:
  Debug Policy functionality

EXTERNALIZED FUNCTIONS


Copyright (c) 2013-2014 by Qualcomm Technologies, Inc. All Rights Reserved.
===========================================================================*/

/*=========================================================================

                          EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.3/boot_images/core/securemsm/secdbgplcy/src/debug_policy.c#5 $
$DateTime: 2016/06/27 01:02:21 $
$Author: pwbldsvc $

when         who                what, where, why
--------   ----               ------------------------------------------- 
11/06/13   hw                  Init version.
===========================================================================*/


/*==========================================================================

           Include Files for Module

==========================================================================*/
#include "secboot_hw.h"
#include "secboot_x509.h"
#include "HALhwio.h"
#include "msmhwiobase.h"
#include "msmhwioreg.h"
#include "secboot_debug_policy.h"
#include "debug_policy_private.h"
#include "stringl.h"

extern boolean is_debug_policy_disable_fuse_bit_set(void);
extern void debug_policy_override_jtag(void);

static dbg_policy_t dbg_policy_local = { 0 };

// The region of memory where the debug policy is loaded is
// not immediately available for access. We must therefore
// wait for a point in the boot process where we know it's
// ready for use. This flag captures this state. In addition,
// this flag is only set to TRUE after the policy has been
// validated for this device.
static boolean debug_policy_is_valid = FALSE;

// "debug policy override" is a bit read from a separate partition by SBL
// If set, this value must prevent the debug policy from ever taking effect
static boolean dpo_value = FALSE;

/**
 * @brief See documentation in the header
 *
 */
dbg_policy_t* get_dbg_policy()
{
    return debug_policy_is_valid ? &dbg_policy_local : NULL;
}

/*!
* 
* @brief
*    This function checks if debug policy ELF file is valid for enabling debug policy
*
* @retval
* @param[in/out]  TRUE   The debug policy is valid and is OK to enable it.
*                 FALSE  The debug policy contains bogus information. Disable the policy.
* 
*/
boolean is_dbg_policy_bound(dbg_policy_t *dbgp_ptr)
{
  secboot_hw_etype   status = E_SECBOOT_HW_FAILURE;
  secboot_fuse_info_type fuse_info;
  
  if (dpo_value)
  {
   // Override bit set - ignore the debug policy
   // (we should never get here if sbl is working as intended)
   return(FALSE);  
  }  

  //TRUE = fuse bit = 1 means to bypass DP
  if (is_debug_policy_disable_fuse_bit_set())
  {
   return(FALSE);  
  }  
  
  MEMSET(&fuse_info, 0, sizeof(fuse_info));
  status = secboot_hw_get_serial_num(&fuse_info.serial_num);

  if (E_SECBOOT_HW_SUCCESS != status)
  {
   return (FALSE);
  }
  
  /* Serial Number should match one in the array */
  if(!is_sec_dbg_skip_serial_number())
  { 
    if (((fuse_info.serial_num) <= (dbgp_ptr->serial_num_end)) &&
        ((fuse_info.serial_num) >= (dbgp_ptr->serial_num_start)))
    {
     return (TRUE);  
    }  
  }
  else
  { 
    return TRUE;
  }

  return (FALSE);
}

/*!
* 
* @brief
*    This function checks if debug policy ELF file passes basic sanity checks
*
* @retval
* @param[in/out]  TRUE   The debug policy is valid and is OK to enable it.
*                 FALSE  The debug policy contains bogus information. Disable the policy.
 *
 */
boolean is_dbg_policy_valid(dbg_policy_t *dbgp_ptr)
{
  //uint64 flags;

  if(debug_policy_is_valid)
  {
      return TRUE;
  }

  if (!( ('D'==dbgp_ptr->magic[0])&&
         ('B'==dbgp_ptr->magic[1])&&
         ('G'==dbgp_ptr->magic[2])&&
         ('P'==dbgp_ptr->magic[3])) )
  {
   return(FALSE);
  }

#if 0
  // Officially, the size of the policy is variable
  // In practice, it's fixed. Ignore for now.
  if (dbgp_ptr->size != sizeof(dbg_policy_t))
  {
    return(FALSE);
  }
#endif

  if (dbgp_ptr->revision != DBG_POLICY_REVISION_NUMBER)
  {
    return(FALSE);
  }

  if (DBG_POLICY_SERIAL_NUM_RANGE_LIMIT < ((uint32)dbgp_ptr->serial_num_end - (uint32)dbgp_ptr->serial_num_start))
  {
    return(FALSE);  
  }

  if (dbgp_ptr->reserved != 0)
  {
    return(FALSE);
  }

  if (dbgp_ptr->flags.reserved_bits != 0)
  {
    // must be 0
    return(FALSE);
  }

  if (dbgp_ptr->image_id_count > DBG_POLICY_ID_ARRAY_SIZE)
  {
    // out of bounds
    return(FALSE);
  }

  if (dbgp_ptr->root_cert_hash_count > DBG_POLICY_CERT_ARRAY_SIZE)
  {
    // out of bounds
    return(FALSE);
  }

  if (0 == dbgp_ptr->root_cert_hash_count && 0 != dbgp_ptr->image_id_count)
  {
      // doesn't make sense
      return(FALSE);
  }

  return is_dbg_policy_bound(dbgp_ptr);
}

/**
 * @brief See documentation in the header
 *
 */
boolean copy_debug_policy(dbg_policy_t* dp)
{
    debug_policy_is_valid = is_dbg_policy_valid(dp);	
    if(debug_policy_is_valid)
    {
        memscpy(&dbg_policy_local, sizeof(*dp), dp, sizeof(*dp));
    }
    return debug_policy_is_valid;
}

/**
 * @brief 
 *        Check whether a feature flag is set in the debug policy.
 *
 * @param[in] flag The feature flag in question
 * @retval    TRUE if the flag is set, FALSE otherwise
 *
 */
boolean is_debug_policy_flag_set ( uint32 flag )
{
  boolean enable_flag = FALSE;

  if(!debug_policy_is_valid)
  {
      return FALSE;
  }

  switch( flag )
  {
   case DBG_POLICY_ENABLE_ONLINE_CRASH_DUMPS:   
        enable_flag = dbg_policy_local.flags.enable_online_crash_dumps;   
        break;   
   case DBG_POLICY_ENABLE_OFFLINE_CRASH_DUMPS:   
        enable_flag = dbg_policy_local.flags.enable_offline_crash_dumps;   
        break;   
   case DBG_POLICY_DISABLE_AUTHENTICATION:   
        enable_flag = dbg_policy_local.flags.disable_authentication;   
        break;   
   case DBG_POLICY_ENABLE_JTAG:   
        enable_flag = dbg_policy_local.flags.enable_jtag;   
        break;   
   case DBG_POLICY_ENABLE_LOGGING:   
        enable_flag = dbg_policy_local.flags.enable_logs;   
        break;   
   default: break;
  }

  return( enable_flag );
}

/**
 * @brief 
 *        Check the current image ID is in the image_id_array
 *        in the debug policy.
 *
 * @param[in] sw_type is the current image ID being authenticated.
 *
 * @retval    TRUE if the id is present id the debug policy, FALSE otherwise
 *
 */
static boolean dbg_policy_check_image_id( uint32 sw_type )
{
    int32 index;
	
    for ( index = 0; index < dbg_policy_local.image_id_count; index++ )
    {
     if ( dbg_policy_local.image_id_array[index] == sw_type )
	 {
	  return( TRUE );
     }	
    }
    return( FALSE );	
}

/**
 * @brief See documentation in the header
 *
 */
boolean is_dbg_policy_rot_for_image( uint32 sw_type )
{
    if (!debug_policy_is_valid)
    {
        return FALSE;
    }

    if (0 == dbg_policy_local.root_cert_hash_count ) // no certs in policy
    {
      return ( FALSE ); 
    }

    if ( 0 == dbg_policy_local.image_id_count )  // authenticate all images against policy
    {
      return( TRUE );	
    }
    
    return dbg_policy_check_image_id( sw_type );
}

/**
 * @brief 
 *        check if the debug policy override flag partition stored in eMMC
 *        to see if it needs to be disabled or not.
 *
 * @param[in/out] return TRUE means disable the policy
 *                return FALSE means to load the debug policy ELF.
 *
 */
boolean sec_is_dp_disable( uint8* dpo_mem_ptr )
{
    dbg_policy_override dpo_mem;
	
    memscpy( &dpo_mem, sizeof(dbg_policy_override), dpo_mem_ptr, sizeof(dbg_policy_override) );
    if ((strcmp(dpo_mem.magic, "DBGO")) && (0 == dpo_mem.revision))
    {
      dpo_value = 1 == dpo_mem.override;
    }
	
    return( dpo_value );	
}


/**
 * @brief See documentation in the header
* 
*/
void apply_dbg_policy()
{
   if (is_debug_policy_flag_set(DBG_POLICY_ENABLE_JTAG))
   {
    debug_policy_override_jtag();   
   }
}
/*!
* 
* @brief
*    This function override jtag register if debug policy requested it.
*
* @param[in]                
*
* @retval
* @param[in/out]  
*                 
* 
*/

void debug_policy_override_jtag()
{
  HWIO_OUT(OVERRIDE_2, HWIO_OVERRIDE_2_RMSK);
  HWIO_OUT(OVERRIDE_3, HWIO_OVERRIDE_3_RMSK);
}

/*!
* 
* @brief
*    This function checks if debug policy disable fuse bit is blown or not
*    The DEBUG_POLICY_DISABLE fuse bit has been difined for this purpose on 8996
*
* @param[in]  dbgp_ptr              
*
* @retval
* @param[in/out]  TRUE   means the bit is 1 and debug policy should be by passed.
*                 FALSE  means the bit is 0 and debug policy shall be loaded.
* 
*/

boolean is_debug_policy_disable_fuse_bit_set()
{
	return (0 != HWIO_INM(QFPROM_RAW_OEM_CONFIG_ROW0_LSB, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_BMSK));
}
