#ifndef CLOCKSBL_H
#define CLOCKSBL_H

/*
===========================================================================
*/
/**
  @file ClockSBL.h

  Internal header file for the clock device driver for the Secondary Boot Loader.
*/
/*
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/clock/hw/msm8937/src/ClockSBL.h#1 $
  $DateTime: 2015/07/06 06:13:34 $
  $Author: pwbldsvc $

  when        who     what, where, why
  --------    ---     -------------------------------------------------
  2012/03/13  bc      Initial revision.
  ====================================================================
*/

#include "ClockSBLConfig.h"
#include "ClockSBLCommon.h"

#endif

