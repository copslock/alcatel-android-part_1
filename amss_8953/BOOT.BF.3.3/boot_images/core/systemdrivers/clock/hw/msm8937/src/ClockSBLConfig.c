/** @file ClockSBLConfig.c

  Definitions of the support clock perf level for the 8974.

  Copyright (c) 2012, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/clock/hw/msm8937/src/ClockSBLConfig.c#10 $
  $DateTime: 2016/04/21 00:58:02 $
  $Author: pwbldsvc $

 when           who     what, where, why
 --------    ---     -----------------------------------------------------------
09/05/12     vph     Set GPLL3 to 200MHz for configure BIMC @ 200MHz with Div-1
08/06/11     vtw     Added SDC clock configurations.
07/15/11     vtw     Created.

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockSBLConfig.h"
#include "ClockSBLCommon.h"
#include "ClockHWIO.h"

/*=========================================================================
      Prototypes
==========================================================================*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToAPCS(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToCCI(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToBIMC(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToIPA(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToBIMCGPU(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToSDCC(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_GCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_APCSMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_CCIMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_BIMCMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_IPAMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_BIMCGPUMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_SDCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource);



/*=========================================================================
      Data
==========================================================================*/
static Clock_SBLRailwayType ClockRailway =
{
  "vddcx",
  0,
  0
};
 
ClockSourceConfigType BIMCPLLConfig[] = 
{
  {
    /* Index 0 : BIMC_PLL/GPLL5 @ 403.2 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, 
      .nVoteAddr     =  0, 
      .nVoteMask     =  0,
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  42, 
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  },
  {
    /* Index 1 : BIMC_PLL/GPLL5 @ 422.4 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, 
      .nVoteAddr     =  0, 
      .nVoteMask     =  0,
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  44, 
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  },

  {
    /* Index 2 : BIMC_PLL/GPLL5 @ 278.4 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  29, 
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765  
    },
  },

  {
    /* Index 3 : BIMC_PLL/GPLL5 @ 384 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  40, 
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765  
    },
  },
  {
    /* Index 4 : BIMC_PLL/GPLL5 @ 556.8 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  58, 
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765  
    },
  },
  {
    /* Index 5 : GPLL5/BIMC_PLL @ 662.4 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  69,   
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  },
  {
    /* Index 6 : GPLL5/BIMC_PLL @ 748.8 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  78,   
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  },
        
  {
    /* Index 7 : GPLL5/BIMC_PLL @ 806.4 MHz  */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  2, 
      .nL            =  84,   
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  },

  {
    /* Index 8 : GPLL5/BIMC_PLL @ 921.6MHZ */
    .ePLL = SRC_RAW,
    .PLLCfg =
    {
      .nPLLModeAddr  =  0, // Will be replaced when PLL is chosen.
      .nVoteAddr     =  0,
      .nVoteMask     =  0, 
      .nVCO          =  1, 
      .nPreDiv       =  1, 
      .nPostDiv      =  1, 
      .nL            =  48,
      .nM            =  0,
      .nN            =  1,
      .nConfigCtl    =  0x4C015765
    },
  }

};

Clock_SBLConfigType Clock_SBLConfigData =
{

  /*GPLL0 @ 800MHZ*/
  .PLL0_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL0_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0),
    .nVCO          =  1, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  41, 
    .nM            =  0,
    .nN            =  1,
    .nAlpha        =  0xAAAA000000,
    .nConfigCtl    =  0x4001055B,
    .ePLL          =  CLK_SOURCE_PLL_STROMER
  },
  /*GPLL4 @ 1152MHZ*/
  .PLL4_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL4_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL4),
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  60, 
    .nM            =  0,
    .nN            =  1,
    .nAlpha        =  0x0,
    .nConfigCtl    =  0x4001055B, 
    .ePLL          =  CLK_SOURCE_PLL_STROMER
  },

   /* GPLL5 @ 384MHz */
  .PLL5_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL5_MODE), 
    .nVoteAddr     =  0, 
    .nVoteMask     =  0,
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  2, 
    .nL            =  40, 
    .nM            =  0,
    .nN            =  1,
    .nConfigCtl    =  0x4C015765
  },

  /* BIMCPLL @ 921.6MHz. */
  .BIMCPLL_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_BIMC_PLL_MODE), 
    .nVoteAddr     =  0, 
    .nVoteMask     =  0,
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  48, 
    .nM            =  0,
    .nN            =  1,
    .nConfigCtl    =  0x4C015765
  },

   /* GPLL6 @ 1080MHz */
  .PLL6_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL6_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL6),
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  56, 
    .nM            =  1,
    .nN            =  4,
    .nConfigCtl    =  0x00031000
  },
 #if 0
   /* A53PWRPLL @ 998.4MHZ*/
  .A53PWRPLL_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(APCS_C0_PLL_MODE), 
    .nVoteAddr     =  0,
    .nVoteMask     =  0,
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  52,
    .nM            =  0,
    .nN            =  1,
    .nConfigCtl    =  0x4C015765,
  },
 #endif
  /* A53PERFPLL @ 1036.8MHZ*/
  .A53PERFPLL_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(APCS_C1_PLL_MODE), 
    .nVoteAddr     =  0,
    .nVoteMask     =  0,
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  54,
    .nM            =  0,
    .nN            =  1,
    .nConfigCtl    =  0x7805C765,
  },
 
  .CPU_Cfg = 
  {
    {0, {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0}},                                    /* PERF NONE */
    {19200, {HWIO_ADDR(APCS_ALIAS1_CMD_RCGR), MUX_APCS, SRC_CXO,    2, 0, 0, 0}},  /* MIN - 19.2 MHz*/
    {400000,{HWIO_ADDR(APCS_ALIAS1_CMD_RCGR), MUX_APCS, SRC_GPLL0,  4, 0, 0, 0}},  /* NOM - 400 MHz*/
    {800000, {HWIO_ADDR(APCS_ALIAS1_CMD_RCGR), MUX_APCS, SRC_GPLL0, 2, 0, 0, 0}},  /* MAX - 800 MHz*/
    {800000, {HWIO_ADDR(APCS_ALIAS1_CMD_RCGR), MUX_APCS, SRC_GPLL0, 2, 0, 0, 0}}   /* DEFAULT - 800 MHz */
  },

  .CCI_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                            /* PERF NONE */
    {HWIO_ADDR(APCS_COMMON_CCI_CMD_RCGR), MUX_CCI,  SRC_CXO,        2, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(APCS_COMMON_CCI_CMD_RCGR), MUX_CCI,  SRC_GPLL0,      8, 0, 0, 0},  /* NOM - 200 MHz*/
    {HWIO_ADDR(APCS_COMMON_CCI_CMD_RCGR), MUX_CCI,  SRC_GPLL0,      4, 0, 0, 0},  /* MAX - 400 MHz*/
    {HWIO_ADDR(APCS_COMMON_CCI_CMD_RCGR), MUX_CCI,  SRC_GPLL0,      4, 0, 0, 0}   /* DEFAULT - 400 MHz */
  },


  .SNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_CXO,  1, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0},  /* NOM - 100 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0},  /* MAX - 200 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}   /* DEFAULT - SAME AS NOMINAL */
  },

  .SYSMMNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_SYSTEM_MM_NOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_CXO,  1, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_MM_NOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 10, 0, 0, 0},  /* NOM - 160 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_MM_NOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 5, 0, 0, 0},  /* MAX - 320 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_MM_NOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 5, 0, 0, 0}   /* DEFAULT - SAME AS MAX */
  },
  .PCNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_CXO,  1, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 32, 0, 0, 0},  /* NOM - 50 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0},  /* MAX - 100 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR ), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}   /* DEFAULT - 100 MHz (Max Nom) */
  },

  /* SDC configuration : for backwards compatiblity to the old API */
  .SDC_Cfg =
  {
    0,       /* PERF NONE */
    400,     /* MIN - 400KHz */
    25000,   /* NOMINAL - 25MHz */
    50000,   /* MAX - 50MHz */
    25000    /* DEFAULT - SAME AS NOMINAL */
  }, /* END SDC config */
  /*  SDC extended configurations */
  .SDC1_Ext_Cfg =
  {
    {   400,  {0, MUX_SDCC, SRC_CXO,   24, 1, 4, 4}},
    { 25000,  {0, MUX_SDCC, SRC_GPLL0, 32, 1, 2, 2}},
    { 50000,  {0, MUX_SDCC, SRC_GPLL0, 32, 0, 0, 0}},
    {100000,  {0, MUX_SDCC, SRC_GPLL0, 16, 0, 0, 0}},
    {192000,  {0, MUX_SDCC, SRC_GPLL4, 12, 0, 0, 0}},
    { 0 }
  }, /* END SDC1_Ext_Cfg */
  .SDC2_Ext_Cfg =
  {
    {   400,  {0, MUX_GCC, SRC_CXO,   24, 1, 4, 4}},
    { 25000,  {0, MUX_GCC, SRC_GPLL0, 32, 1, 2, 2}},
    { 50000,  {0, MUX_GCC, SRC_GPLL0, 32, 0, 0, 0}},
    {100000,  {0, MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}},
    {200000,  {0, MUX_GCC, SRC_GPLL0, 8, 0, 0, 0}},
    { 0 }
  }, /* END SDC2_Ext_Cfg */

  /* Crypto configuration CE_Cfg : 160 MHz */
  .CE_Cfg = 
  {
    HWIO_ADDR(GCC_CRYPTO_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    10, /* nDiv2x */
    0,0,0 /* M/N:D */
  },

  /* USBHS configuration USBHS_Cfg : 133.33 MHz */
  .USBHS_Cfg =
  {
    HWIO_ADDR(GCC_USB_HS_SYSTEM_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    12, /* nDiv2x */
    0,0,0 /* M/N:D */
  },

  /*UART configurations UART_cfg : 3.6864 MHz for UART1 to UART6 clocks*/
  .UART_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                                   /* PERF-NONE */
    {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 72, 15625, 15625}, /*MIN - 3.6864 MHz */
    {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 72, 15625, 15625}, /* NOMINAL - 3.6864 MHz MHz */
    {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 72, 15625, 15625}, /* MAX - 3.6864 MHz MHz */
    {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 72, 15625, 15625}, /* DEFAULT - SAME AS NOMINAL */    
  },
  
  /* RPM configuration : 177.78 MHz */
  .RPM_Cfg = 
  {
    HWIO_ADDR(GCC_RPM_CMD_RCGR), /*CMD RCGR register*/
    MUX_GCC, SRC_GPLL0,  /* eSource */
    9, /* nDiv2x */
    0,0,0 /* M/N:D */
  },
  
  /*I2C configuration support. Max at 50MHZ.*/
  .I2C_Cfg = 
  {
     {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                              /* PERF-NONE */
     {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* MIN - 19.2 MHz */
     {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* NOMINAL - 19.2 MHz */
     {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 32, 0, 0, 0}, /* MAX - 50 MHz */
     {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* DEFAULT - 19.2 MHz */
   },
  /* Q6_TBU configuration : 400 MHz */
  .Q6TBU_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                 /* PERF-NONE */
    {HWIO_ADDR(GCC_Q6_TBU_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}, /* MIN - 100 MHz */
    {HWIO_ADDR(GCC_Q6_TBU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0}, /* NOMINAL - 200 MHz */
    {HWIO_ADDR(GCC_Q6_TBU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* MAX - 400 MHz */
    {HWIO_ADDR(GCC_Q6_TBU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* DEFAULT - SAME AS MAX */    
  },  

  /* APSS_AXI configuration : 400 MHz */
  .APSSAXI_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                 /* PERF-NONE */
    {HWIO_ADDR(GCC_APSS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}, /* MIN - 100 MHz */
    {HWIO_ADDR(GCC_APSS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0}, /* NOMINAL - 200 MHz */
    {HWIO_ADDR(GCC_APSS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* MAX - 400 MHz */
    {HWIO_ADDR(GCC_APSS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* DEFAULT - SAME AS MAX */ 
  }, 
 
  /* APSS_TCU configuration : 400 MHz */
  .APSSTCU_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                 /* PERF-NONE */
    {HWIO_ADDR(GCC_APSS_TCU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0}, /* MIN - 200 MHz */
    {HWIO_ADDR(GCC_APSS_TCU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0}, /* NOMINAL - 200 MHz */
    {HWIO_ADDR(GCC_APSS_TCU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* MAX - 400 MHz */
    {HWIO_ADDR(GCC_APSS_TCU_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}, /* DEFAULT - SAME AS MAX */ 
  }, 
 
 /* BIMC configuration BIMC_Cfg */
  .BIMC_Cfg_Feero =
  {
    {  /* BIMC configuration BIMC_Cfg : at 19.2 Mhz */
      19200000, RAIL_VOLTAGE_LEVEL_NOMINAL, NULL,
       { /* Separate clock root for BIMC and DDR.  APPS_AXI scaled along with BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC, SRC_CXO,      2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 19.2MHz.6MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_CXO,   4, 0, 0, 0}  /* BIMC_GPU_clk 9.6MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 100.8 Mhz */
      100800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[0],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   8, 1, 0, 0}, /*  GCC DDR Mux, ddr_clk @ 100.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0,  8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
      }
    },
    {  /* BIMC configuration BIMC_Cfg : at 211.2 Mhz */
       211200000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[1],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,    4, 1, 0, 0}, /*  GCC DDR Mux, ddr_clk @ 211.2MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0,  8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 278.4 Mhz */
       278400000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[2],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 278.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 384 Mhz */
       384000000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[3],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),        MUX_BIMC,    SRC_RAW,   2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 384MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),   MUX_BIMCGPU, SRC_GPLL0, 8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 556.8 Mhz */
       556800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[4],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),      MUX_BIMC,    SRC_RAW,     2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 556.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR), MUX_BIMCGPU, SRC_GPLL6,   8, 0, 0, 0}  /*  GCC BIMC GPU Mux, BIMC_GPU_clk at 270MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 662.4 Mhz */
       662400000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[5],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,    2, 0, 16, 0},/*  GCC DDR Mux, ddr_clk @ 662.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL6,  8, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 270MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 748.8 Mhz */
       748800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[6],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 16, 0},/*  GCC DDR Mux, ddr_clk @ 748.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 5, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 320MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 806.4 Mhz */
       806400000, RAIL_VOLTAGE_LEVEL_NOMINAL_HIGH, &BIMCPLLConfig[7],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 18, 0},/*  GCC DDR Mux, ddr_clk @ 806.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL6, 6, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 360MHZ */
       }
    },
#if 0
    {  /* BIMC configuration BIMC_Cfg : at 921.6 Mhz */
       921600000, RAIL_VOLTAGE_LEVEL_TURBO, &BIMCPLLConfig[8],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 20, 0},/* GCC DDR Mux, ddr_clk @ 921.6MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 4, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 400MHZ */
       }
    },
#endif
    {0},
  },

   /* BIMC configuration Feero pro and FeeroCAT6 BIMC_Cfg_921p6MHz */
  .BIMC_Cfg_921p6MHz =
  {
    {  /* BIMC configuration BIMC_Cfg : at 19.2 Mhz */
      19200000, RAIL_VOLTAGE_LEVEL_NOMINAL, NULL,
       { /* Separate clock root for BIMC and DDR.  APPS_AXI scaled along with BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC, SRC_CXO,      2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 19.2MHz.6MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_CXO,   4, 0, 0, 0}  /* BIMC_GPU_clk 9.6MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 100.8 Mhz */
      100800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[0],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   8, 1, 0, 0}, /*  GCC DDR Mux, ddr_clk @ 100.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0,  8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
      }
    },
    {  /* BIMC configuration BIMC_Cfg : at 211.2 Mhz */
       211200000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[1],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,    4, 1, 0, 0}, /*  GCC DDR Mux, ddr_clk @ 211.2MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0,  8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 278.4 Mhz */
       278400000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[2],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 278.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 384 Mhz */
       384000000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[3],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),        MUX_BIMC,    SRC_RAW,   2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 384MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),   MUX_BIMCGPU, SRC_GPLL0, 8, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 200MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 556.8 Mhz */
       556800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[4],
       { /* Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),      MUX_BIMC,    SRC_RAW,     2, 1, 0, 0}, /* GCC DDR Mux, ddr_clk @ 556.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR), MUX_BIMCGPU, SRC_GPLL6,   8, 0, 0, 0}  /*  GCC BIMC GPU Mux, BIMC_GPU_clk at 270MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 662.4 Mhz */
       662400000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[5],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,    2, 0, 16, 0},/*  GCC DDR Mux, ddr_clk @ 662.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL6,  8, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 270MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 748.8 Mhz */
       748800000, RAIL_VOLTAGE_LEVEL_NOMINAL, &BIMCPLLConfig[6],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 16, 0},/*  GCC DDR Mux, ddr_clk @ 748.8MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 5, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 320MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 806.4 Mhz */
       806400000, RAIL_VOLTAGE_LEVEL_NOMINAL_HIGH, &BIMCPLLConfig[7],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 18, 0},/*  GCC DDR Mux, ddr_clk @ 806.4MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL6, 6, 0, 0, 0} /* GCC BIMC GPU Mux, BIMC_GPU_clk at 360MHZ */
       }
    },
    {  /* BIMC configuration BIMC_Cfg : at 921.6 Mhz */
       921600000, RAIL_VOLTAGE_LEVEL_TURBO, &BIMCPLLConfig[8],
       { /*  Single clock root for BIMC and DDR.  BIMC_GPU must be atleast half of BIMC */
         {0},
         { HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC,    SRC_RAW,   2, 0, 20, 0},/* GCC DDR Mux, ddr_clk @ 921.6MHZ */
         { HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_BIMCGPU, SRC_GPLL0, 4, 0, 0, 0}  /* GCC BIMC GPU Mux, BIMC_GPU_clk at 400MHZ */
       }
    },
    {0},
  },
};


boolean (*Clock_SBL_MuxMap[NUM_MUX_TYPES])(ClockSourceType, uint32 *) =
{
  Clock_SourceMapToGCC, 
  Clock_SourceMapToAPCS,
  Clock_SourceMapToGCC,
  Clock_SourceMapToBIMC,
  Clock_SourceMapToCCI,
  NULL,
  Clock_SourceMapToIPA,
  NULL,
  Clock_SourceMapToSDCC,
  Clock_SourceMapToBIMCGPU
};

boolean (*Clock_SBL_SourceMap[NUM_MUX_TYPES])(uint32,ClockSourceType *) =
{
  Clock_GCCMuxMapToSource, 
  Clock_APCSMuxMapToSource,
  Clock_GCCMuxMapToSource,
  Clock_BIMCMuxMapToSource,
  Clock_CCIMuxMapToSource,
  NULL,
  Clock_IPAMuxMapToSource,
  NULL,
  Clock_SDCCMuxMapToSource,
  Clock_BIMCGPUMuxMapToSource
};


/*=========================================================================
      Functions
==========================================================================*/

/* ============================================================================
**  Function : Clock_RailwayCfg
** ============================================================================
*/
/*!
    Return a pointer to the Railway configuration data.

   @param  None

   @retval a pointer to the Railway configuration data

*/
Clock_SBLRailwayType *Clock_RailwayConfig( void )
{
  return &ClockRailway;
}

/* ============================================================================
**  Function : Clock_SBLConfig
** ============================================================================
*/
/*!
    Return a pointer to the SBL configuration data.

   @param  None

   @retval a pointer to the SBL configuration data

*/
Clock_SBLConfigType *Clock_SBLConfig( void )
{
  return &Clock_SBLConfigData;
}

/* ============================================================================
**  Function : Clock_SourceMapToMux
** ============================================================================
*/
boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
)
{
  if( (pConfig == NULL) ||
      (nMuxValue == NULL) ||
      pConfig->eMux >= NUM_MUX_TYPES )
  {
    return FALSE;
  }

  return Clock_SBL_MuxMap[pConfig->eMux](pConfig->eSource, nMuxValue);
}


/* ============================================================================
**  Function : Clock_MuxMapToSource
** ============================================================================
*/
boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
)
{
  if( (pConfig == NULL) ||
      pConfig->eMux >= NUM_MUX_TYPES )
  {
    return FALSE;
  }

  return Clock_SBL_SourceMap[pConfig->eMux](nSource,&(pConfig->eSource));
}


/* ============================================================================
**  Function : Clock_SourceMapToGCC
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
      break;
    case SRC_GPLL6:
      *nMuxValue = 2;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

 /*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_GCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL0;
      break;
    case 2:
      *pSource = SRC_GPLL6;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}


/* ============================================================================
**  Function : Clock_SourceMapToBIMCGPU
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for BIMC GPU.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToBIMCGPU(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
      break;
    case SRC_GPLL6:
      *nMuxValue = 6;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

 /*!
   Map the source enumeration to a physical mux setting for BIMC GPU.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_BIMCGPUMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL0;
      break;
    case 6:
      *pSource = SRC_GPLL6;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}


/* ============================================================================
**  Function : Clock_SourceMapToAPCS
** ============================================================================
*/
/*!
   Map a ClockSourceType into a physical mux setting for the APCS muxes.

   @param  None

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToAPCS(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 4;
      break;
    case SRC_A53PERFPLL:
      *nMuxValue = 5;
      break; 
    default:
      return FALSE;
  }
  return TRUE;
}

/*!
   Map the source enumeration to a physical mux setting for APCS.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_APCSMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 4:
      *pSource = SRC_GPLL0;
      break;
    case 5:
      *pSource = SRC_A53PERFPLL;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}


/* ============================================================================
**  Function : Clock_SourceMapToCCI
** ============================================================================
*/
/*!
   Map a ClockSourceType into a physical mux setting for the CCI mux.

   @param  None

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToCCI(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 4;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/*!
   Map the source enumeration to a physical mux setting for CCI.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_CCIMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 4:
      *pSource = SRC_GPLL0;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_SourceMapToIPA
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for IPA

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToIPA(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL6:
      *nMuxValue = 1;
       break;
    case SRC_GPLL0:
      *nMuxValue = 2;
      break;
    case SRC_GPLL4:
      *nMuxValue = 3;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/*!
   Map the source enumeration to a physical mux setting for IPA.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_IPAMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL6;
      break;
    case 2:
      *pSource = SRC_GPLL0;
      break; 
    case 3:
      *pSource = SRC_GPLL4;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_SourceMapToSDCC
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for SDCC

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToSDCC(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
       break;
    case SRC_GPLL4:
      *nMuxValue = 2;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/*!
   Map the source enumeration to a physical mux setting for SDCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SDCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL0;
      break; 
    case 2:
      *pSource = SRC_GPLL4;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_SourceMapToBIMC
** ============================================================================
*/
/*!
   Map a ClockSourceType into a physical mux setting for the BIMC_DDR and DDR mux.

   @param  None

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToBIMC(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
      break;
    case SRC_BIMCPLL:
      *nMuxValue = 2;
      break;
    case SRC_GPLL5:
      *nMuxValue = 5;
      break;
    default:
      return FALSE;
  }
  return TRUE;
} 

/*!
   Map the source enumeration to a physical mux setting for BIMC/DDR.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_BIMCMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL0;
      break;
    case 2:
      *pSource = SRC_BIMCPLL;
      break; 
    case 5:
      *pSource = SRC_GPLL5;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}


/* ============================================================================
**  Function : Clock_EnableSource
** ============================================================================
*/

boolean Clock_EnableSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;

    case SRC_GPLL0:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL0_Cfg);
      /*Enabling Aux output for CCI, QDSS and USB_FS_IC*/
      HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1);
      
      break;

    case SRC_GPLL4:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL4_Cfg);
      /* AUX OUTPUT enable for GCC muxes */
      HWIO_OUTF(GCC_GPLL4_USER_CTL, PLLOUT_LV_AUX, 1);
      HWIO_OUT(GCC_GPLL4_TEST_CTL_U,0x400000);
      break;

    case SRC_GPLL5:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL5_Cfg);
      break; 

    case SRC_BIMCPLL:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.BIMCPLL_Cfg);
      break;
   
    case SRC_GPLL6:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL6_Cfg);
       /* AUX OUTPUT enable for GCC muxes */
      HWIO_OUTF(GCC_GPLL6_USER_CTL, PLLOUT_LV_AUX, 1);
      
      break;
    
    #if 0
    case SRC_A53PWRPLL:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.A53PWRPLL_Cfg);
      HWIO_OUTF(APCS_C0_PLL_USER_CTL, PLLOUT_LV_EARLY, 1); 
      break;
    #endif
    case SRC_A53PERFPLL:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.A53PERFPLL_Cfg);
      HWIO_OUTF(APCS_C1_PLL_USER_CTL, LVEARLY_EN, 1);
      break; 

    default:
      break;
  }
  return rtrn;
}

/* ============================================================================
**  Function : Clock_ConfigureSource
** ============================================================================
*/
boolean Clock_ConfigureSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;
    case SRC_GPLL0:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL0_Cfg);
      /*Enabling Aux output for CCI, QDSS and USB_FS_IC*/
      HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1);
      
      break;
  
    case SRC_GPLL4:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL4_Cfg);
      /* AUX OUTPUT enable for GCC muxes */
      HWIO_OUTF(GCC_GPLL4_USER_CTL, PLLOUT_LV_AUX, 1);
      HWIO_OUT(GCC_GPLL4_TEST_CTL_U,0x400000);
     break;

    case SRC_GPLL5:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL5_Cfg);
      break; 

    case SRC_BIMCPLL:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.BIMCPLL_Cfg);
      break;

    case SRC_GPLL6:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL6_Cfg);
     /* AUX OUTPUT enable for GCC muxes */
      HWIO_OUTF(GCC_GPLL6_USER_CTL, PLLOUT_LV_AUX, 1);
      
      break;
   
    #if 0
    case SRC_A53PWRPLL:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.A53PWRPLL_Cfg);
      HWIO_OUTF(APCS_C0_PLL_USER_CTL, PLLOUT_LV_EARLY, 1);
      break;
    #endif 
    case SRC_A53PERFPLL:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.A53PERFPLL_Cfg);
      HWIO_OUTF(APCS_C1_PLL_USER_CTL, LVEARLY_EN, 1);

    default:
      break;
  }
  return rtrn;
}
