/*
===========================================================================
  @file ClockSBLCPU.c

  This file provides clock initialization for the Apps SBL.
===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/clock/hw/msm8953/src/ClockSBLCPU.c#2 $
  $DateTime: 2015/10/05 23:23:20 $
  $Author: pwbldsvc $

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"
#include "busywait.h"

/*=========================================================================
      Macro Definitions
==========================================================================*/

/*=========================================================================
     Externs
==========================================================================*/

/*=========================================================================
      Function Prototypes
==========================================================================*/

/*=========================================================================
      Function Definitions
==========================================================================*/

/*=========================================================================
      Data
==========================================================================*/
/* variable that holds the configured apps clock freq */
uint32 clock_apps_speed_khz = 0;

/*=========================================================================
      Function Definitions
=========================================================================*/
boolean Clock_SetCCIPerfLevel(ClockBootPerfLevelType eCCIPerfLevel);
/* ============================================================================
**  Function : Clock_SetCPUPerfLevel
** ============================================================================
*/
/**
  Configure Krait CPU to a specific perf level.

  @param eCPUPerfLevel [in] - CPU performance level.

  @return
  TRUE -- CPU was configured to perf level successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetCPUPerfLevel(ClockBootPerfLevelType eCPUPerfLevel)
{
  Clock_SBLConfigType *cfg = Clock_SBLConfig();
  const ClockConfigMuxType *CPU_mux, *CXO_cfg;

  if( eCPUPerfLevel >= CLOCK_BOOT_PERF_NUM ) return FALSE;

  CPU_mux = &cfg->CPU_Cfg[eCPUPerfLevel].ApcsMux;

  /* Check to see if the switch is from A53PLL to A53PLL */
  if( CPU_mux->eSource == SRC_A53PERFPLL )
  {
    if(HWIO_INF(APCS_C1_PLL_MODE, OUTCTRL))
    {
      /* Switch away from A53PERF PLL so that it can be re-tuned */
      CXO_cfg = &cfg->CPU_Cfg[CLOCK_BOOT_PERF_MIN].ApcsMux;
      if( ! Clock_EnableSource( CXO_cfg->eSource )) return FALSE;
      if( ! Clock_ConfigMux(CXO_cfg)) return FALSE;

      /* Disable the A53PLL */
      HWIO_OUTF(APCS_C1_PLL_MODE, OUTCTRL, 0x0);
    }
  }

  /*Switch CCI*/
  if(! Clock_SetCCIPerfLevel(eCPUPerfLevel)) return FALSE;
  /* Now switch */
  if( ! Clock_EnableSource( CPU_mux->eSource )) return FALSE;
  if( ! Clock_ConfigMux(CPU_mux)) return FALSE;

  clock_apps_speed_khz = cfg->CPU_Cfg[eCPUPerfLevel].nFrequency;

  return TRUE;
}

/* ============================================================================
**  Function : Clock_SetL2PerfLevel
** ============================================================================
*/
/*!
    Configure L2 cache to a specific perf level.

    @param eL2PerfLevel   -  [IN] CPU performance level

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetL2PerfLevel(ClockBootPerfLevelType eL2PerfLevel)
{
  return FALSE;
}

/* ============================================================================
**  Function : Clock_SetCCIPerfLevel
** ============================================================================
*/
/*!
    Configure CCI to a specific perf level.

    @param eCCIPerfLevel   -  [IN] CPU performance level

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetCCIPerfLevel(ClockBootPerfLevelType eCCIPerfLevel)
{
   Clock_SBLConfigType *cfg = Clock_SBLConfig();
   const ClockConfigMuxType *CCI_cfg;

   if( eCCIPerfLevel >= CLOCK_BOOT_PERF_NUM ) return FALSE;

   CCI_cfg = &cfg->CCI_Cfg[eCCIPerfLevel];

   /* Configure APCS_COMMON_CCI_RCGR_MUX for CCI scaling */
   //HWIO_OUTF(APCS_COMMON_CCI_RCGR_MUX, MUX_SEL, 0x1);

   /* Now switch */
   if( ! Clock_EnableSource( CCI_cfg->eSource )) return FALSE;
   if( ! Clock_ConfigMux(CCI_cfg)) return FALSE;

 

   return TRUE;

}


