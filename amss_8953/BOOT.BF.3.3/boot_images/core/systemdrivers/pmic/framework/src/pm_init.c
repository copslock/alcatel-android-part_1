/*! \file pm_init.c
*   \n
*   \brief This file contains PMIC initialization function which initializes the PMIC Comm
*   \n layer, PMIC drivers and PMIC applications.
*   \n
*   \n &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/framework/src/pm_init.c#2 $  

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/09/15   mr      Change after BOOT VIRTIO Validation + Added STUB implementation (CR-803648)
01/15/13   aab     Fixed KW error
05/10/11   jtn     Fix RPM init bug for 8960
07/01/10   umr     Created.
===========================================================================*/

/*===========================================================================

                    INCLUDE FILES

===========================================================================*/
#include "pm_resource_manager.h"
#include "device_info.h"
#include "pm_sbl_boot.h"
#include "pm_comm.h"


/*===========================================================================

                    STATIC VARIABLES

===========================================================================*/
static uint8 g_pm_stub_pmic_type = 0;    /* This flag to be set with PMIC Type from <VIRTIO/RUMI_load>.cmm Script */

/*===========================================================================

                    FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_driver_init( void )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if(TRUE == pm_is_stub_enabled ())
    {
        return err_flag;
    }

    err_flag |= pm_driver_pre_init();

    pm_resource_manager_init();

    err_flag |= pm_driver_post_init ();

    return err_flag;
}

boolean pm_is_stub_enabled ( void )
{
    if (0 != g_pm_stub_pmic_type)
    {
      return TRUE;
    }

    return FALSE;
}
