/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM CLK DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
/* For TEST/DEBUG purpose */  /* Same varialbes are used for SMPS Driver */
pm_lpg_status_type g_lpg_status;
int g_pwm_value;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
 /**
 * @name pm_test_driver_lpg_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_lpg_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 i;

    if (0 == g_pm_test_lpg_num)
    {
        return err_flag;
    }

    for(i = 0; (pm_lpg_chan_type)i < PM_LPG_CHAN_INVALID; i++)
    {
        err_flag = pm_lpg_pwm_enable (pmic_index, (pm_lpg_chan_type)i, TRUE);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_ENABLE, i, err_flag);
        }

        err_flag = pm_lpg_pwm_output_enable (pmic_index, (pm_lpg_chan_type)i, TRUE);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_OUTPUT_ENABLE, i, err_flag);
        }

        err_flag = pm_lpg_pwm_set_pwm_value (pmic_index, (pm_lpg_chan_type)i, 50);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_SET_PWM_VALUE, i, err_flag);
        }

        err_flag = pm_lpg_pwm_set_pre_divide (pmic_index, (pm_lpg_chan_type)i, PM_LPG_PWM_PRE_DIV_5, PM_LPG_PWM_FREQ_EXPO_4);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_SET_PRE_DIVIDE, i, err_flag);
        }

        err_flag = pm_lpg_pwm_clock_sel (pmic_index, (pm_lpg_chan_type)i, PM_LPG_PWM_19_2_MHZ);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_CLOCK_SEL, i, err_flag);
        }

        err_flag = pm_lpg_set_pwm_bit_size (pmic_index, (pm_lpg_chan_type)i, PM_LPG_PWM_8BIT);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_SET_PWM_BIT_SIZE, i, err_flag);
        }

        err_flag = pm_lpg_pwm_src_select (pmic_index, (pm_lpg_chan_type)i, PM_LPG_PWM_SRC_PWM_REGISTER);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_SRC_SELECT, i, err_flag);
        }

        err_flag = pm_lpg_config_pwm_type (pmic_index, (pm_lpg_chan_type)i, 1, 1, 1, PM_LPG_PHASE_STAG_SHIFT_180_DEG);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_CONFIG_PWM_TYPE, i, err_flag);
        }

        err_flag = pm_lpg_pattern_config (pmic_index, (pm_lpg_chan_type)i, 1, 1, 1, 1, 1);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PATTERN_CONFIG, i, err_flag);
        }

        err_flag = pm_lpg_pwm_ramp_generator_start (pmic_index, (pm_lpg_chan_type)i);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_RAMP_GENERATOR_START, i, err_flag);
        }

        err_flag = pm_lpg_pwm_lut_index_set (pmic_index, (pm_lpg_chan_type)i, 20, 60);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_PWM_LUT_INDEX_SET, i, err_flag);
        }

        err_flag = pm_lpg_config_pause_time (pmic_index, (pm_lpg_chan_type)i, 25, 50, 40);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_CONFIG_PAUSE_TIME, i, err_flag);
        }

        err_flag = pm_lpg_get_status (pmic_index, (pm_lpg_chan_type)i, &g_lpg_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_LPG_GET_STATUS, i, err_flag);
        }
    }

    err_flag = pm_lpg_lut_config_set (pmic_index, 50, 100);    //2nd param (0-64), 3rd PWM value
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_LPG_LUT_CONFIG_SET, i, err_flag);
    }

    err_flag = pm_lpg_lut_config_get (pmic_index, 50, &g_pwm_value);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_LPG_LUT_CONFIG_GET, i, err_flag);
    }

    err_flag = pm_lpg_lut_config_set_array (pmic_index, 50, &g_pwm_value, 4);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_LPG_LUT_CONFIG_SET_ARRAY, i, err_flag);
    }

    err_flag = pm_lpg_lut_config_get_array (pmic_index, 50, &g_pwm_value, 4);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_LPG_LUT_CONFIG_GET_ARRAY, i, err_flag);
    }

    return err_flag;
}

