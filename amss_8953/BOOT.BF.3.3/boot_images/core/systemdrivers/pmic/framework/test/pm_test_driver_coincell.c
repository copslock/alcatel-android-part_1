/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM SMPS DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
boolean g_coincell_status = 0;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_test_driver_coincell_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_coincell_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    err_flag = pm_coincell_enable(pmic_index, PM_ON);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_COINCELL_ENABLE, 1, err_flag);
    }

    err_flag = pm_coincell_get_status(pmic_index, &g_coincell_status);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_COINCELL_GET_STATUS, 1, err_flag);
    }

    err_flag = pm_coincell_set_cc_current_limit_resistor(pmic_index, PM_COINCELL_RSET_2K1);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_COINCELL_SET_CC_CURRENT_LIMIT_RESISTOR, 1, err_flag);
    }

    err_flag = pm_coincell_set_cc_charging_voltage(pmic_index, PM_COINCELL_VSET_3V2);
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_COINCELL_SET_CC_CHARGING_VOLTAGE, 1, err_flag);
    }

    return err_flag;
}

#if 0
/**
 * @name pm_test_driver_coincell_level_1
 *
 * @brief This function tests for SET-GET APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_coincell_level_1 (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}
#endif
