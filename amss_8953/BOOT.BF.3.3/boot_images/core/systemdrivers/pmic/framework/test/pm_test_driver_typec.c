/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM CLK DRIVER TEST

GENERAL DESCRIPTION
  This file contains TypeC Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/23/15   mr      Added TypeC Module Unit tests in PMIC Test Framework (CR-954241)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_typec_driver.h"
#include "pm_test_framework.h"


/*===========================================================================

                    GLOBAL VARIABLE DEFINITIONS

===========================================================================*/
uint8 typec_en_status = 0;          /* = 0xBF46 */
uint8 typec_dis_cmd_status = 0;     /* = 0xBF52 */
uint8 typec_port_role_status = 0;   /* = 0xBF52 */
uint8 typec_vconn_en_status = 0;    /* = 0xBF52 */


/*===========================================================================

                    FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_typec_read_periph_reg
 *
 * @brief To read TypeC periph reg value. To be used after SET APIs
 *
 */
pm_err_flag_type pm_typec_read_periph_reg(uint16 reg_addr, uint8 *reg_val);

 /**
 * @name pm_test_driver_typec_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] pmic_index
 *
 * @param [out] none
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_driver_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_typec_level_0 (uint8 pmic_index)
{
    uint8 i = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    boolean vbus_valid = FALSE, typec_enable = TRUE, typec_cmd_sw = TRUE;
    pm_typec_plug_orientation_type cc_out;
    pm_typec_ufp_type_type ufp_type;
    pm_typec_current_advertise_type current_adv;
    pm_typec_port_role_type port_role = PM_ROLE_UFP;
    pm_typec_vconn_enable_type vconn_enable = PM_VCONN_CONTROL_BY_HW;

    err_flag  = pm_typec_read_periph_reg(0xBF46, &typec_en_status);
    err_flag |= pm_typec_read_periph_reg(0xBF52, &typec_dis_cmd_status);
    typec_port_role_status = typec_vconn_en_status = typec_dis_cmd_status;
    if(PM_ERR_FLAG__SUCCESS != err_flag)
    {
        pm_test_handle_error(pmic_index, PM_TYPEC_READ_PERIPH_REG, i, err_flag);
    }

    for(i = 0; i < g_pm_test_typec_num; i++)
    {
        err_flag = pm_typec_is_vbus_valid(&vbus_valid);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_IS_VBUS_VALID, i, err_flag);
        }

        err_flag = pm_typec_get_plug_orientation_status(&cc_out);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_PLUG_ORIENTATION_STATUS, i, err_flag);
        }

        err_flag = pm_typec_get_ufp_type(&ufp_type);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
        }

        err_flag = pm_typec_get_dfp_curr_advertise(&current_adv);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_DFP_CURR_ADVERTISE, i, err_flag);
        }

        err_flag = pm_typec_enable(typec_enable);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_ENABLE, i, err_flag);
        }

        err_flag = pm_typec_disable_typec_command_sw(typec_cmd_sw);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_DISABLE_TYPEC_COMMAND_SW, i, err_flag);
        }

        err_flag = pm_typec_set_port_role(port_role);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_SET_PORT_ROLE, i, err_flag);
        }

        err_flag = pm_typec_vconn_enable(vconn_enable);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_VCONN_ENABLE, i, err_flag);
        }
    }

    return err_flag;
}

pm_err_flag_type pm_test_driver_typec_level_1 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 i = 0, reg_read_val = 0;
    boolean vbus_valid = FALSE;
    pm_typec_plug_orientation_type cc_out;
    pm_typec_ufp_type_type ufp_type;
    pm_typec_current_advertise_type current_adv;

    for(i = 0; i < g_pm_test_typec_num; i++)
    {
        /* Test case for pm_typec_is_vbus_valid() - Cross verify raw status and status returned from API */
        reg_read_val = 0;
        err_flag  = pm_typec_read_periph_reg(0xBF08 /*STATUS1*/, &reg_read_val);
        err_flag |= pm_typec_is_vbus_valid(&vbus_valid);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_IS_VBUS_VALID, i, err_flag);
        }
        else
        {
            if(TRUE == vbus_valid)
            {
                if(!(reg_read_val & PM_BIT(5)))    /* Expected STATUS1 bit<5> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_IS_VBUS_VALID, i, err_flag);
                }
            }
            else
            {
                if(reg_read_val & PM_BIT(5))    /* Expected STATUS1 bit<5> = 0 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_IS_VBUS_VALID, i, err_flag);
                }
            }
        }

        /* Test case for pm_typec_get_plug_orientation_status() - Cross verify raw status and status returned from API */
        reg_read_val = 0;
        err_flag  = pm_typec_read_periph_reg(0xBF08 /*STATUS1*/, &reg_read_val);
        err_flag |= pm_typec_get_plug_orientation_status(&cc_out);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_PLUG_ORIENTATION_STATUS, i, err_flag);
        }
        else
        {
            if(PM_PLUG_UNFLIP /*CC1*/ == cc_out)
            {
                if(reg_read_val & PM_BITS(7,6))    /* Expected STATUS1 bit<7:6> = 0 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_PLUG_ORIENTATION_STATUS, i, err_flag);
                }
            }
            else if(PM_PLUG_FLIP /*CC2*/ == cc_out)
            {
                if(!(reg_read_val & PM_BIT(7)))    /* Expected STATUS1 bit<7> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_PLUG_ORIENTATION_STATUS, i, err_flag);
                }
            }
            else if(PM_PLUG_OPEN /*OPEN*/ == cc_out)
            {
                if(!(reg_read_val & PM_BIT(6)))    /* Expected STATUS1 bit<6> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_PLUG_ORIENTATION_STATUS, i, err_flag);
                }
            }
        }

        /* Test case for pm_typec_get_ufp_type() - Cross verify raw status and status returned from API */
        reg_read_val = 0;
        err_flag  = pm_typec_read_periph_reg(0xBF09 /*STATUS2*/, &reg_read_val);
        err_flag |= pm_typec_get_ufp_type(&ufp_type);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
        }
        else
        {
            if(PM_UFP_UNATTACHED == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(2)))    /* Expected STATUS2 bit<2> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
            else if(PM_UFP_AUDIO_ADAPTER == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(3)))    /* Expected STATUS2 bit<3> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
            else if(PM_UFP_DEBUG_ACCESSORY == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(4)))    /* Expected STATUS2 bit<4> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
            else if(PM_UFP_ATTACHED_POWER_CABLE == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(5)))    /* Expected STATUS2 bit<5> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
            else if(PM_UFP_ATTACHED == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(6)))    /* Expected STATUS2 bit<6> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
            else if(PM_UFP_NOT_ATTACHED_POWER_CABLE == ufp_type)
            {
                if(!(reg_read_val & PM_BIT(7)))    /* Expected STATUS2 bit<7> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_UFP_TYPE, i, err_flag);
                }
            }
        }

        /* Test case for pm_typec_get_dfp_curr_advertise() - Cross verify raw status and status returned from API */
        reg_read_val = 0;
        err_flag  = pm_typec_read_periph_reg(0xBF08 /*STATUS1*/, &reg_read_val);
        err_flag |= pm_typec_get_dfp_curr_advertise(&current_adv);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_GET_DFP_CURR_ADVERTISE, i, err_flag);
        }
        else
        {
            if(PM_CURR_ADV_3A == current_adv)
            {
                if(!(reg_read_val & PM_BIT(0)))    /* Expected STATUS1 bit<0> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_DFP_CURR_ADVERTISE, i, err_flag);
                }
            }
            else if(PM_CURR_ADV_1P5A == current_adv)
            {
                if(!(reg_read_val & PM_BIT(1)))    /* Expected STATUS1 bit<1> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_DFP_CURR_ADVERTISE, i, err_flag);
                }
            }
            else if(PM_CURR_ADV_STANDARD == current_adv)
            {
                if(!(reg_read_val & PM_BIT(2)))    /* Expected STATUS1 bit<2> = 1 */
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_GET_DFP_CURR_ADVERTISE, i, err_flag);
                }
            }
        }

        /* Test case for pm_typec_enable() - store current state, set and validate, restore state */
        reg_read_val = 0;
        err_flag = pm_typec_read_periph_reg(0xBF46 /*EN_CTL1*/, &reg_read_val);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_ENABLE, i, err_flag);
        }
        else
        {
            boolean typec_en_sts = FALSE;    /* TypeC periph previous state */
            if (reg_read_val & PM_BIT(7))
            {
                typec_en_sts = TRUE;
                err_flag = pm_typec_enable(FALSE);
            }
            else
            {
                typec_en_sts = FALSE;
                err_flag = pm_typec_enable(TRUE);
            }

            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_TYPEC_ENABLE, i, err_flag);
            }
            else
            {
                reg_read_val = 0;
                err_flag = pm_typec_read_periph_reg(0xBF46 /*EN_CTL1*/, &reg_read_val);
                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_ENABLE, i, err_flag);
                }
                else
                {
                    if((TRUE == typec_en_sts) && (!(reg_read_val & PM_BIT(7))))  /* Set Op = FALSE and Expected bit<7> = 0 */
                    {
                        err_flag = pm_typec_enable(typec_en_sts);
                    }
                    else if((FALSE == typec_en_sts) && (reg_read_val & PM_BIT(7)))  /* Set Op = TRUE and Expected bit<7> = 1 */
                    {
                        err_flag = pm_typec_enable(typec_en_sts);
                    }

                    if(PM_ERR_FLAG__SUCCESS != err_flag)
                    {
                        pm_test_handle_error(pmic_index, PM_TYPEC_ENABLE, i, err_flag);
                    }
                }
            }
        }

        /* Test case for pm_typec_disable_typec_command_sw() - store current state, set and validate, restore state */
        reg_read_val = 0;
        err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_DISABLE_TYPEC_COMMAND_SW, i, err_flag);
        }
        else
        {
            boolean typec_dis_cmd_sts = FALSE;    /* TypeC SW DISABLE CMD previous state */
            if (reg_read_val & PM_BIT(3))
            {
                typec_dis_cmd_sts = TRUE;
                err_flag = pm_typec_disable_typec_command_sw(FALSE);
            }
            else
            {
                typec_dis_cmd_sts = FALSE;
                err_flag = pm_typec_disable_typec_command_sw(TRUE);
            }

            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_TYPEC_DISABLE_TYPEC_COMMAND_SW, i, err_flag);
            }
            else
            {
                err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_DISABLE_TYPEC_COMMAND_SW, i, err_flag);
                }
                else
                {
                    if((TRUE == typec_dis_cmd_sts) && (!(reg_read_val & PM_BIT(3))))  /* Set Op = FALSE and Expected bit<3> = 0 */
                    {
                        err_flag = pm_typec_disable_typec_command_sw(typec_dis_cmd_sts);
                    }
                    else if((FALSE == typec_dis_cmd_sts) && (reg_read_val & PM_BIT(3)))  /* Set Op = TRUE and Expected bit<3> = 1 */
                    {
                        err_flag = pm_typec_disable_typec_command_sw(typec_dis_cmd_sts);
                    }

                    if(PM_ERR_FLAG__SUCCESS != err_flag)
                    {
                        pm_test_handle_error(pmic_index, PM_TYPEC_DISABLE_TYPEC_COMMAND_SW, i, err_flag);
                    }
                }
            }
        }

        /* Test case for pm_typec_set_port_role() - store current role, set and validate, restore state */
        reg_read_val = 0;
        err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_SET_PORT_ROLE, i, err_flag);
        }
        else
        {
            pm_typec_port_role_type typec_role_sts;

            if(reg_read_val & PM_BIT(4))
            {
                typec_role_sts = PM_ROLE_DFP;
                err_flag = pm_typec_set_port_role(PM_ROLE_DRP);
            }
            else if(reg_read_val & PM_BIT(5))
            {
                typec_role_sts = PM_ROLE_UFP;
                err_flag = pm_typec_set_port_role(PM_ROLE_DRP);
            }
            else
            {
                typec_role_sts = PM_ROLE_DRP;
                err_flag = pm_typec_set_port_role(PM_ROLE_UFP);
            }

            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_TYPEC_SET_PORT_ROLE, i, err_flag);
            }
            else
            {
                reg_read_val = 0;
                err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_SET_PORT_ROLE, i, err_flag);
                }
                else
                {
                    if((PM_ROLE_DFP == typec_role_sts || PM_ROLE_UFP == typec_role_sts) &&    /* Set Op = ROLE_DRP and Expected bit<5:4> = 0 */
                       (!(reg_read_val & PM_BIT(4) && reg_read_val & PM_BIT(5))))
                    {
                        err_flag = pm_typec_set_port_role(typec_role_sts);
                    }
                    else if((PM_ROLE_DRP == typec_role_sts) && (reg_read_val & PM_BIT(5)))  /* Set Op = ROLE_UFP and Expected bit<5> = 1 */
                    {
                        err_flag = pm_typec_set_port_role(typec_role_sts);
                    }

                    if(PM_ERR_FLAG__SUCCESS != err_flag)
                    {
                        pm_test_handle_error(pmic_index, PM_TYPEC_SET_PORT_ROLE, i, err_flag);
                    }
                }
            }
        }

        /* Test case for pm_typec_vconn_enable() - store current state, set and validate, restore state */
        reg_read_val = 0;
        err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_TYPEC_VCONN_ENABLE, i, err_flag);
        }
        else
        {
            pm_typec_vconn_enable_type typec_vconn_sts;

            if(reg_read_val & PM_BIT(7))    /* VCONN control by SW enabled */
            {
                typec_vconn_sts = (reg_read_val & PM_BIT(6)) ? PM_VCONN_ENABLE_BY_SW : PM_VCONN_DISABLE_BY_SW;
                err_flag = pm_typec_vconn_enable(PM_VCONN_CONTROL_BY_HW);
            }
            else
            {
                typec_vconn_sts = PM_VCONN_CONTROL_BY_HW;
                err_flag = pm_typec_vconn_enable(PM_VCONN_ENABLE_BY_SW);
            }

            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_TYPEC_VCONN_ENABLE, i, err_flag);
            }
            else
            {
                reg_read_val = 0;
                err_flag = pm_typec_read_periph_reg(0xBF52 /*SW_CTL*/, &reg_read_val);
                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    pm_test_handle_error(pmic_index, PM_TYPEC_VCONN_ENABLE, i, err_flag);
                }
                else
                {
                    if((PM_VCONN_ENABLE_BY_SW == typec_vconn_sts || PM_VCONN_DISABLE_BY_SW == typec_vconn_sts) &&
                       (!(reg_read_val & PM_BIT(7))))                             /* Set Op = HW_CTRL and Expected bit<7> = 0 */
                    {
                        err_flag = pm_typec_vconn_enable(typec_vconn_sts);
                    }
                    else if((PM_VCONN_CONTROL_BY_HW == typec_vconn_sts) &&
                            (PM_BITS(7, 6) == (reg_read_val & PM_BITS(7, 6))))    /* Set Op = SW_CTL_EN and Expected bit<7:6> = b11 */
                    {
                        err_flag = pm_typec_vconn_enable(typec_vconn_sts);
                    }

                    if(PM_ERR_FLAG__SUCCESS != err_flag)
                    {
                        pm_test_handle_error(pmic_index, PM_TYPEC_VCONN_ENABLE, i, err_flag);
                    }
                }
            }
        }
    }

    return err_flag;
}


/** To be used only for TypeC driver validation from Unit Test Framework */
pm_err_flag_type pm_typec_read_periph_reg(uint16 reg_addr, uint8 *reg_val)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    pm_typec_data_type* typec = pm_typec_get_data(pm_typec_get_pmic_index());

    if(NULL == reg_val)
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    *reg_val = 0;
    err = pm_comm_read_byte(typec->comm_ptr->slave_id, reg_addr, reg_val, 0);

    return err;
}
