/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM SMPS DRIVER TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
pm_rgb_status_type g_rgb_status;

/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_test_driver_rgb_level_0
 *
 * @brief This function tests for APIs' successful return.
 *
 * @param [in] void
 *
 * @param [out] void
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 * @dependencies Should be called after pm_device_init().
 *
 * @sideeffect
 *
 * @note
 *
 */
pm_err_flag_type pm_test_driver_rgb_level_0 (uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 i = 0, j = 0;

    if (0 == g_pm_test_rgb_num)
    {
        return err_flag;
    }

    for(i = 0; i < g_pm_test_rgb_num; i++)
    {
        for(j = 0; (pm_rgb_voltage_source_type)j < PM_RGB_VOLTAGE_SOURCE_INVALID; j++)
        {
            err_flag = pm_rgb_set_voltage_source (pmic_index, (pm_rgb_which_type)i, (pm_rgb_voltage_source_type)j);
            if(PM_ERR_FLAG__SUCCESS != err_flag)
            {
                pm_test_handle_error(pmic_index, PM_RGB_SET_VOLTAGE_SOURCE, i, err_flag);
            }
        }

        err_flag = pm_rgb_enable (pmic_index, (pm_rgb_which_type)i, 0xFF, 1, 1);//third param is mask
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_RGB_ENABLE, i, err_flag);
        }

        err_flag = pm_rgb_get_status (pmic_index, (pm_rgb_which_type)i, &g_rgb_status);
        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            pm_test_handle_error(pmic_index, PM_RGB_GET_STATUS, i, err_flag);
        }
    }

    return err_flag;
}

