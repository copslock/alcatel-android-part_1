#ifndef PM_UTILS_H
#define PM_UTILS_H

/*! \file
 *
 *  \brief  pm_utils.h ----This file contain PMIC wrapper function of DALSYS_Malloc()
 *  \details his file contain PMIC wrapper function of DALSYS_Malloc()
 *
 *    &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/framework/inc/pm_utils.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/23/15   mr      Added Unit Tests for TypeC module. (CR-954241)
03/20/14   aab     Added pm_boot_adc_get_mv()
06/11/13   hs      Adding settling time for regulators.
06/20/12   hs      Created
===========================================================================*/

/*===========================================================================

                    INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "AdcBoot.h"
#include "pm_err_flags.h"


/*===========================================================================

                    TYPEDEF ANS MACRO DECLARATIONS

===========================================================================*/
/* To Set/Mask Bit at 'nth' position */
#define PM_BIT(n) (1 << (n))

/* To Set/Mask Bits between high-low (including) position */
#define PM_BITS(high, low) (PM_BIT((high) - (low) + 1) - 1) << (low)


/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
extern void pm_malloc(uint32 dwSize, void **ppMem);

extern uint64 pm_convert_time_to_timetick(uint64 time_us);

extern uint64 pm_convert_timetick_to_time(uint64 time_tick);


pm_err_flag_type
pm_boot_adc_get_mv(const char *pszInputName, uint32 *battery_voltage);

pm_err_flag_type
pm_clk_busy_wait ( uint32 uS );

#endif    /* PM_UTILS_H */
