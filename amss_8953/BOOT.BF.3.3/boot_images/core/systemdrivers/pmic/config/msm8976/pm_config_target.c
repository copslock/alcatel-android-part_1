/*! \file
 *
 *  \brief
 *   pm_config_registers.c
 *  \details
 *   This file contains customizable target specific
 *   driver settings & PMIC registers. This file is generated from database functional
 *   configuration information that is maintained for each of the targets.
 *
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *
 *  &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/config/msm8976/pm_config_target.c#7 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/13/16   pxm     Added parameter fpr dbc high current mode (CR922874)
09/28/15   pxm     Add device index support, Updated led control support
09/16/15   pxm     Change Float Voltage from 4.2vto 4.35v
05/06/15   mr      Added support for SMBCHGL Charger Driver (CR-833504)
04/09/15   mr      Change after BOOT VIRTIO Validation (CR-803648)
01/04/15   mr      Added support for 8956 target (CR-790476)
05/17/14   rk      Modify XPU permission and Int Ownership for PMIC Peripherals (CR-666063)
12/19/13   rk      Added spmi channel config setings
10/08/13   rk      Added target specific configurations
07/15/13   aab     Added support for SMBB driver
05/09/13   aab     Updated MPP driver support
03/26/13   aab     Updated GPIO driver support
03/07/13   aab     Added VIB driver support
01/18/13   aab     Cleanup
01/09/13   aab     Added support for sleep clk
12/06/12   hw      Rearchitecturing module driver to peripheral driver
08/07/12   hs      Added support for 5V boost.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "DALDeviceId.h"
#include "SpmiCfg.h"

#include "pm_pbs_info.h"
#include"pm_gpio_driver.h"
#include"pm_mpp_driver.h"
#include"pm_clk_driver.h"
#include"pm_vib_driver.h"
#include "pm_led.h"
#include "pm_smbchg_bat_if.h"
#include "pm_fg_sram.h"
#include "pm_smbchg_driver.h"
#include "pm_app_smbchg.h"
#include "pm_sbl_boot.h"

/* 2 slave IDs per PMIC device. */
#define PM_MAX_SLAVE_ID          2

 /************************************************ DRIVER DATA *********************************/




//===== LUT ====================================================
pm_led_config_type led_array[] =
{   
    //device_index,   pm_led_type,   pm_led_channel,  level 
        {1, PM_LED_CHG, PM_LED_CH_INVALID, 0},
    // {2, PM_LED_KPD, PM_LED_CH_MPP2, PM_MPP__I_SINK__LEVEL_40mA},
    // {2, PM_LED_RED, PM_LED_CH_RED, PM_RGB_DIM_LEVEL_MID},
        {2, PM_LED_GREEN, PM_LED_CH_MPP2, PM_MPP__I_SINK__LEVEL_40mA},
    // {2, PM_LED_BLUE, PM_LED_CH_BLUE, PM_RGB_DIM_LEVEL_MID},
};

const pm_led_type default_led = PM_LED_CHG;


// This array is used for tri-color LED connected to RGB module only.
// For tri-color LED which is connect to led_array, search in led_array
// Do NOT suppose this array size must be 3. Customers may use dual color LED but connect to RGB module
pm_led_rgb_config_type rgb_configs[] =
{
    /* RGB Mask bit,     LPG channel,   Power source */
    {  PM_RGB_SEGMENT_R, PM_LPG_CHAN_7, PM_RGB_VOLTAGE_SOURCE_VPH},
    {  PM_RGB_SEGMENT_G, PM_LPG_CHAN_6, PM_RGB_VOLTAGE_SOURCE_VPH},
    {  PM_RGB_SEGMENT_B, PM_LPG_CHAN_5, PM_RGB_VOLTAGE_SOURCE_VPH},
};

pm_led_info_type led_info =
{
    led_array,
    rgb_configs,
    default_led
};

pm_smbchg_specific_data_type
smbchg_specific_data[1] =
{
   {
      /* Configuration Value,                              Enable config */
      {  PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_3P25,         PM_DISABLE_CONFIG },    //Vlowbatt Threshold
      {  PM_SMBCHG_USBCHGPTH_INPUT_PRIORITY_USBIN,         PM_DISABLE_CONFIG },    //Charger Path Input Priority
      {  PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_BMD_PIN,     PM_DISABLE_CONFIG },    //Battery Missing Detection Source
      {  PM_SMBCHG_MISC_WD_TMOUT_18S,                      PM_DISABLE_CONFIG },    //WDOG Timeout
      {  FALSE,                                            PM_DISABLE_CONFIG },    //Enable WDOG
      {  1000,                                             PM_DISABLE_CONFIG },    //FAST Charging Current
      {  250,                                              PM_DISABLE_CONFIG },    //PRE Charge Current
      {  3000,                                             PM_DISABLE_CONFIG },    //PRE to Fast Charge Current
      {  4350,                                             PM_ENABLE_CONFIG },    //Float Voltage
      {  2100,                                             PM_DISABLE_CONFIG },    //USBIN Input Current Limit
      // {  1000,                                             PM_DISABLE_CONFIG },    //DCIN Input Current Limit
	  {  200,                                              PM_DISABLE_CONFIG },  //apsd_reset_theshold_mv
      3500,                                                                      //bootup_battery_theshold_mv
      3800,                                                                      //wipowr bootup battery thesholdmv
      FALSE,                                                                     //Enable/Disable JEITA Hard Temp Limit Check in SBL
	  FALSE,                                                                   // dbc_high_current_mode
   }
};

chg_range_data_type chg_range_data[1] =
{
   {
      // batt_missing_detect_time_range
      { 80, 160, 320, 640 },
      // DCIN_range
      { 300, 400, 450, 475, 500, 550, 600, 650, 700, 900, 950, 1000, 1100, 1200, 1400, 1450, 1500, 1600, 1800, 1850, 1880, 1910, 1930, 1950, 1970, 2000 },
      // USBIN_range
      { 300, 400, 450, 475, 500, 550, 600, 650, 700, 900, 950, 1000, 1100, 1200, 1400, 1450, 1500, 1600, 1800, 1850, 1880, 1910, 1930, 1950, 1970, 2000, 2050, 2100, 2300, 2400, 2500, 3000 }
   }
};

/* By default, Configuration of the FG params is NOT enabled.
 * Since the last parameter(EnableConfig) = 0 , To enable configuration, set EnableConfig = 1
 */
FgSramAddrDataEx_type
fg_sram_data[SBL_SRAM_CONFIG_SIZE] =
{
    /* SramAddr,   SramData, DataOffset, DataSize, EnableConfig */
    // JEITA Thresholds:
    {  0x0454,     0x23,     0,          1,        PM_DISABLE_CONFIG }, //JEITA Soft Cold Threshold:  default = 0x23
    {  0x0454,     0x46,     1,          1,        PM_DISABLE_CONFIG }, //JEITA Soft Hot  Threshold:  default = 0x46
    {  0x0454,     0x1E,     2,          1,        PM_DISABLE_CONFIG }, //JEITA Hard Cold Threshold:  default = 0x1E
    {  0x0454,     0x48,     3,          1,        PM_DISABLE_CONFIG }, //JEITA hard Hot  Threshold:  default = 0x48
    // Thermistor Beta Co-efficient:
    {  0x0444,     0x86D8,   2,          2,        PM_DISABLE_CONFIG }, //thremistor_c1_coeff:  default = 0x86D8;
    {  0x0448,     0x50F1,   0,          2,        PM_DISABLE_CONFIG }, //thremistor_c2_coeff:  default = 0x50F1;
    {  0x0448,     0x3C11,   2,          2,        PM_DISABLE_CONFIG }  //thremistor_c3_coeff:  default = 0x3C11;
};

unsigned smps_phase_lut_a[ ]= {0,0,0,0};

pm_mpp_specific_info_type
mpp_specific[1] =
{
    {0x9E0, 4}
};

pm_vib_specific_data_type
vib_specific[1] =
{
    {1200, 3100}
};

pm_sbl_specific_data_type
sbl_specific_data[1] = {
   {
     FALSE,                     //Used to enable/disable verbose UART logging
   }
};

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/


const uint8 pm_periph_bitmap[][2][32] =
{
    /* PM8950*/
    {
        {
         0x72, 0x07, 0x00, 0x10, 
         0x10, 0x11, 0x36, 0x00, 
         0x00, 0x00, 0x37, 0x3f, 
         0x07, 0x00, 0xff, 0x01, 
         0x00, 0x00, 0x00, 0x00, 
         0x0f, 0x00, 0x00, 0x00, 
         0xff, 0x00, 0x00, 0x00, 
         0x00, 0x00, 0x00, 0x40, 
        },
        {
          0x00, 0x00, 0xf1, 0xff, 
          0x3f, 0x00, 0x00, 0x00, 
          0xff, 0xff, 0x7f, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x10, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x0f, 0x00, 
        },
    },
    /* PMI8950*/
    {
        {
          0x72, 0x03, 0x5f, 0x18, 
          0x00, 0x10, 0x02, 0x00, 
          0x1f, 0x00, 0x00, 0x02, 
          0x00, 0x00, 0xff, 0x03, 
          0x00, 0x00, 0x00, 0x00, 
          0x0f, 0x00, 0x00, 0x00, 
          0x03, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x40, 
        },
        {
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x01, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x01, 0x00, 
          0x01, 0x00, 0x08, 0x53, 
          0x00, 0x00, 0x00, 0x00, 
        }
    },
    /* PM8004*/
    {
        {
          0x72, 0x03, 0x00, 0x00, 
          0x10, 0x10, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x02, 
          0x00, 0x00, 0xff, 0x01, 
          0x00, 0x00, 0x00, 0x00, 
          0x0f, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x40,
        },
        {
          0x00, 0x00, 0xf1, 0xe3, 
          0x07, 0x00, 0x00, 0x00, 
          0x01, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00, 
          0x00, 0x00, 0x00, 0x00,
        }
    }
};


pm_pbs_info_data_type pm_pbs_info_data_a =
{
    128,        /* PBS MEMORY Size */
    PM_PBS_INFO_IN_MISC,     /* Place where MFG Info stored */
    0x0100,     /* MISC Periph BASE_ADDRESS */
    0xF0,       /* TRIM_NUM  */
    0xF1,       /* TP_REV    */
    0xF2,       /* FAB_ID    */
    0xF3,       /* WAFER_ID  */
    0xF4,       /* X_CO_ORDI */
    0xF5,       /* Y_CO_ORDI */
    0xF6,       /* LOT_ID    */
    0xFF,       /* MFG_ID    */
};

pm_pbs_info_data_type pm_pbs_info_data_b =
{
    128,        /* PBS MEMORY Size */
    PM_PBS_INFO_IN_MISC,     /* Place where MFG Info stored */
    0x0100,     /* MISC Periph BASE_ADDRESS */
    0xF0,       /* TRIM_NUM  */
    0xF1,       /* TP_REV    */
    0xF2,       /* FAB_ID    */
    0xF3,       /* WAFER_ID  */
    0xF4,       /* X_CO_ORDI */
    0xF5,       /* Y_CO_ORDI */
    0xF6,       /* LOT_ID    */
    0xFF,       /* MFG_ID    */
};

pm_pbs_info_data_type pm_pbs_info_data_c =
{
    256,        /* PBS MEMORY Size */
    PM_PBS_INFO_INVALID,     /* Place where MFG Info stored */
    0,          /* MISC Periph BASE_ADDRESS */
    0,          /* TRIM_NUM  */
    0,          /* TP_REV    */
    0,          /* FAB_ID    */
    0,          /* WAFER_ID  */
    0,          /* X_CO_ORDI */
    0,          /* Y_CO_ORDI */
    0,          /* LOT_ID    */
    0,          /* MFG_ID    */
};

pm_pbs_info_data_type *pm_pbs_info_data[]=
{
    {&pm_pbs_info_data_a}, {&pm_pbs_info_data_b}, {&pm_pbs_info_data_c}
};


