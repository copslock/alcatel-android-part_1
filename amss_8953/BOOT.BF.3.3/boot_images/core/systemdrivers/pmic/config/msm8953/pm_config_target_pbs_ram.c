/*! \file  pm_config_target_pbs_ram.c
 *  
 *  \brief  File Contains the PMIC Set Mode Driver Implementation
 *  \details Set Mode Driver implementation is responsible for setting and getting 
 *  all mode settings such as Register values, memory values, etc.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: PMx8953-x.x-OTP-x.x-MSM8953-20160518_b0_v014 - Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2016 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/pmic/config/msm8953/pm_config_target_pbs_ram.c#12 $ 
$DateTime: 2016/05/26 03:54:29 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_target_information.h"
#include "pm_config_sbl.h"
#include "pm_pbs_driver.h"

/*========================== PBS RAM LUT =============================*/

// To handle multiple PBS RAM configuration for different rev of the same PMIC or for multiple PMICs,
// a double dimension array of PBS RAM data is used. The data field of the specific command (in pm_config_target_sbl_sequence.c)
// to program PBS RAM will hold the index to the PBS RAM that needs to be used. during programming.
// Example:
// 1.sid; 2.data; 3.base_address; 4.offset;  5.reg_operation; 6.rev_id_operation; 7.rev_id;
// {0, 0x00, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_2_0},  //data = 0:  Use the 1st set of PBS RAM data if PMIC Rev ID = REV_ID_2_0
// {0, 0x01, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_1_0},  //data = 1:  Use the 2nd set of PBS RAM data if PMIC Rev ID = REV_ID_1_0

pm_pbs_ram_data_type
pm_pbs_seq [ ][PBS_RAM_DATA_SIZE] =
{
   // PBS_RAM_MSM8953.PMIC.HW.PM8953_2p0_1_0_8
   {
      //data  offset  base_addr  sid
      { 0x28,	0x05,	0x00,	0x80},	// W#0	-	 Spare (A) DTEST {1-4} PBS-RAM Trigger. Used by DTEST1 for XO_TRIM_CORRECTION (on SLEEP_B)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#1	-	
      { 0x88,	0x05,	0x00,	0x80},	// W#2	-	 POFF PBS-RAM Trigger (Used as Mainline, if SBL was loaded)
      { 0x48,	0x04,	0x00,	0x80},	// W#3	-	 BUA PBS-RAM Trigger
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#4	-	
      { 0x90,	0x04,	0x00,	0x80},	// W#5	-	 Sleep_Wake PBS-RAM Trigger (PBS_CLIENT5 [PRE-PON] repurposed as New Sleep-Wake trigger in SBL, to support MPM SPMI-Broadcast feature on a common PBS_CLIENT5 across all PMICs in Chipset.
      { 0x58,	0x05,	0x00,	0x80},	// W#6	-	 WARM_RESET PBS-RAM Trigger (Used as Mainline, if SBL was loaded)
      { 0x20,	0x04,	0x00,	0x80},	// W#7	-	 Spare (B) DTEST {1-4} PBS-RAM Trigger. Used by DTEST2 for uSD-DragonFly-Debug Support
      { 0x00,	0x00,	0x01,	0x00},	// W#8	-	 DUMMY-01 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x10,	0x08,	0x40},	// W#9	-	 PON.INT_RT_STS ==> Read value & Store in Local Buffer
      { 0x02,	0x01,	0x02,	0x90},	// W#10	-	 IF RESIN_ON = 1 (i.e. Vol Down Button pressed) then SKIP Next Line
      { 0x44,	0x04,	0x00,	0x80},	// W#11	-	 VOL- Key Not pressed ==> No DragonFly. End this SEQ immediately
      { 0x60,	0x41,	0x4B,	0x01},	// W#12	-	 LDO12.VOLTAGE_CTL2 = 0x60 ==> Set to 2.95V
      { 0x80,	0x46,	0x4B,	0x01},	// W#13	-	 LDO12: Enable (SD VDDPX_2)
      { 0x60,	0x41,	0x4A,	0x01},	// W#14	-	 LDO11.VOLTAGE_CTL2 = 0x60 ==> Set to 2.95V
      { 0x80,	0x46,	0x4A,	0x01},	// W#15	-	 LDO11: Enable (MEM_SD_MC_VDD)
      { 0x00,	0x00,	0x01,	0x00},	// W#16	-	 DUMMY-02 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#17	-	
      { 0x00,	0x00,	0x01,	0x00},	// W#18	-	 DUMMY-03 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x18,	0x1C,	0x40},	// W#19	-	 BUA.INT_LATCHED_STS --> Read Value & Store in Local Buffer
      { 0x01,	0x01,	0x01,	0x90},	// W#20	-	 IF BUA.BATT_ALARM_LATCHED_STS = 1; Skip Next Line in order to execute BUA_POST_BATT_REM_SEQ
      { 0x68,	0x04,	0x00,	0x80},	// W#21	-	 Jump to BUA_POST_UICC_REM_SEQ  (since BUA Trigger wasn't asserted due to BAT-Removal)
      { 0x00,	0x00,	0x01,	0x00},	// W#22	-	 DUMMY-04 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x84,	0x04,	0x4D,	0x89},	// W#23	-	 LDO14 (UIM1 LDO): Assert Local Soft-Reset (effectively Disables it)
      { 0x84,	0x04,	0x4E,	0x89},	// W#24	-	 LDO15 (UIM2 LDO): Assert Local Soft-Reset (effectively Disables it)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#25	-	 End of BUA_POST_BATT_REM_SEQ
      { 0x00,	0x00,	0x01,	0x00},	// W#26	-	 DUMMY-05 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x09,	0x1C,	0x40},	// W#27	-	 BUA.STATUS2 --> Read Value & Store in Local Buffer (for UICCx_ALARM detection)
      { 0x01,	0x00,	0x01,	0x90},	// W#28	-	 IF UICC1_ALARM NOT Detected - Skip Next Line
      { 0x84,	0x04,	0x4D,	0x89},	// W#29	-	 LDO14 (UIM1 LDO): Assert Local Soft-Reset (effectively Disables it)
      { 0x02,	0x00,	0x02,	0x90},	// W#30	-	 IF UICC2_ALARM NOT Detected - Skip Next Line
      { 0x84,	0x04,	0x4E,	0x89},	// W#31	-	 LDO15 (UIM2 LDO): Assert Local Soft-Reset (effectively Disables it)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#32	-	 End of BUA_POST_UICC_REM_SEQ
      { 0xA5,	0xD0,	0x00,	0x04},	// W#33	-	 Unlock the peripheral (module) for Secure-Access
      { 0x01,	0xDB,	0x00,	0x04},	// W#34	-	 PERPH_RESET_CTL4 = 0x01 ==> Assert Local Soft-Reset on the peripheral
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#35	-	 Go back to next line of the calling function
      { 0x00,	0x00,	0x01,	0x00},	// W#36	-	 DUMMY-06 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x8A,	0x08,	0x40},	// W#37	-	 PON.AVDD_VPH Read Register & store value in Buffer (Used to decide whether we are entering into or exiting from sleep)
      { 0x00,	0x00,	0x60,	0x90},	// W#38	-	 IF AVDD_MODE != 0 (LPM) => AVDD is in some HPM Skip next line to Enter sleep
      { 0xE4,	0x04,	0x00,	0x80},	// W#39	-	 Jump to WAKE_SEQ_BEGIN --> IF the PMIC was detected to be already in Sleep
      { 0x00,	0x00,	0x01,	0x00},	// W#40	-	 DUMMY-07 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x10,	0x40,	0xC3,	0x00},	// W#41	-	 GPIO4: Write MODE_CTL = 0x10 => DIGITAL_OUTPUT & LOW STATE (This will Force the external BBYP into Bypass Mode leading to possibly lowered headrooms on HV LDOs & some power-savings)
      { 0x06,	0x7E,	0x18,	0x01},	// W#42	-	 S2_PS: Set PFM_CURRENT_LIMIT_THRESH_HALF (Retention-setting)
      { 0xD8,	0x04,	0x17,	0x89},	// W#43	-	 S2_CTRL: Configure Buck-Settings for Retention
      { 0x06,	0x7E,	0x1B,	0x01},	// W#44	-	 S3_PS: Set PFM_CURRENT_LIMIT_THRESH_HALF (Retention-setting)
      { 0xD8,	0x04,	0x1A,	0x89},	// W#45	-	 S3_CTRL: Configure Buck-Settings for Retention
      { 0x05,	0x45,	0x1D,	0x01},	// W#46	-	 CR-0000173801: S4_CTRL.MODE_CTL = FORCED_LPM (PFM) [0x05]
      { 0x06,	0x7E,	0x27,	0x01},	// W#47	-	 S7_PS: Set PFM_CURRENT_LIMIT_THRESH_HALF (Retention-setting)
      { 0xD8,	0x04,	0x26,	0x89},	// W#48	-	 S7_CTRL: Configure Buck-Settings for Retention
      { 0x11,	0x40,	0xA0,	0x00},	// W#49	-	 Change MPP1 Mode from 1.250V Analog Output to 1.225V Digital Output Mode (for low power consumption) [MPP1.DIG_VIN already configured for VIN2 in SBL]
      { 0x00,	0x8A,	0x08,	0x00},	// W#50	-	 Put AVDD Regulator into LPM
      { 0x01,	0x46,	0x51,	0x00},	// W#51	-	 Configure BBCLK1 to (only) follow BBCLK1_EN (CXO_EN) Pin-Control
      { 0x00,	0x00,	0x01,	0x00},	// W#52	-	 DUMMY-08 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#53	-	 EOS (End of SLEEP_SEQ)
      { 0x47,	0x44,	0x00,	0x04},	// W#54	-	 Sx_CTRL.PFM_CTL: Change the PFM_IPLIM_DLY to 600NS
      { 0x04,	0x45,	0x00,	0x04},	// W#55	-	 Sx_CTRL.MODE_CTL: Force Retention-Mode
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#56	-	 RETURN: Go back to next line of the calling function
      { 0x00,	0x00,	0x01,	0x00},	// W#57	-	 DUMMY-09 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x81,	0x46,	0x51,	0x00},	// W#58	-	 BBCLK1: Explicitly Enable again [on top of HW-EN through BBCLK1_EN / CXO_EN Pin that got asserted-high before RAM-Wake-Up-Seq got triggered)
      { 0x60,	0x8A,	0x08,	0x00},	// W#59	-	 AVDD_MODE = HPM2 & Enable Auto-selection to Master BandGap
      { 0x51,	0x40,	0xA0,	0x00},	// W#60	-	 Revert MPP1 Mode from 1.225V Digital Output to 1.250V Analog utput Mode
      { 0x1C,	0x05,	0x26,	0x89},	// W#61	-	 S7_CTRL: Configure Buck-Settings for Auto
      { 0x02,	0x7E,	0x27,	0x01},	// W#62	-	 S7_PS: Set PFM_CURRENT_LIMIT_THRESH_NORMAL and AUTO_MODE_BW_SEL_QUARTER (SBL Settings)
      { 0x06,	0x45,	0x1D,	0x01},	// W#63	-	 CR-0000173801: S4_CTRL.MODE_CTL = AUTO_MODE [0x06]
      { 0x1C,	0x05,	0x1A,	0x89},	// W#64	-	 S3_CTRL: Configure Buck-Settings for Auto
      { 0x02,	0x7E,	0x1B,	0x01},	// W#65	-	 S3_PS: Set PFM_CURRENT_LIMIT_THRESH_NORMAL and AUTO_MODE_BW_SEL_QUARTER (SBL Settings)
      { 0x1C,	0x05,	0x17,	0x89},	// W#66	-	 S2_CTRL: Configure Buck-Settings for Auto
      { 0x02,	0x7E,	0x18,	0x01},	// W#67	-	 S2_PS: Set PFM_CURRENT_LIMIT_THRESH_NORMAL and AUTO_MODE_BW_SEL_QUARTER (SBL Settings)
      { 0x11,	0x40,	0xC3,	0x00},	// W#68	-	 GPIO4: Write MODE_CTL = 0x11 => DIGITAL_OUTPUT & HIGH STATE (This will configure the external BBYP back to Auto Mode as normally required)
      { 0x00,	0x00,	0x01,	0x00},	// W#69	-	 DUMMY-10 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#70	-	 EOS (End of WAKE_SEQ)
      { 0x06,	0x45,	0x00,	0x04},	// W#71	-	 CR-0000174057: Sx_CTRL.MODE_CTL: AUTO_MODE
      { 0x45,	0x44,	0x00,	0x04},	// W#72	-	 Sx_CTRL.PFM_CTL: Revert the PFM_IPLIM_DLY to 150NS
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#73	-	 RETURN: Go back to next line of the calling function
      { 0x00,	0x00,	0x01,	0x00},	// W#74	-	 DUMMY-11 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x08,	0x50,	0x40},	// W#75	-	 CR-0000174502: Read XO.STATUS1 & Store value in Local Buffer (Used to decide whether PMIC is in SLEEP or AWAKE)
      { 0x00,	0x01,	0x40,	0x90},	// W#76	-	 IF BIT<6> SLEEP_B == 0 => PMIC has been put to SLEEP, so SKIP next line.(Will effectively execute XO_CORRECT_POST_SLEEP seq)
      { 0x48,	0x05,	0x00,	0x80},	// W#77	-	 Goto XO_CORRECT_POST_WAKE, since SLEEP_B == 1 => PMIC has been awoken
      { 0x00,	0x00,	0x01,	0x00},	// W#78	-	 DUMMY-12 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x52,	0x59,	0x40},	// W#79	-	 CR-0000167854 (Step-1): Read CLK_DIST_SPARE2 (=XO_ADJ_FINE NOM++)
      { 0x00,	0x5C,	0x50,	0x08},	// W#80	-	 CR-0000167854 (Step-2): Write (Copy) the local-buffer (SPARE2) value to XO_ADJ_FINE
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#81	-	 End of XO_TRIM_POST_SLEEP sequence
      { 0x00,	0x00,	0x01,	0x00},	// W#82	-	 DUMMY-13 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x00,	0x51,	0x59,	0x40},	// W#83	-	 CR-0000167854 (Step-3): Read CLK_DIST_SPARE1 (=XO_ADJ_FINE NOM)
      { 0x00,	0x5C,	0x50,	0x08},	// W#84	-	 CR-0000167854 (Step-4): Write (Copy) the local-buffer (SPARE1) value to XO_ADJ_FINE
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#85	-	 End of XO_TRIM_POST_WAKE sequence
      { 0x00,	0x00,	0x01,	0x00},	// W#86	-	 DUMMY-14 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x21,	0x00,	0x01,	0xC0},	// W#87	-	 Wait 1 ms (= 33 cycles of 32 kHz clock)Addressing IMS/CR-0000172869 - to let all modules (esp. PON) finish their perph_rb before the start of PBS-Warm-Reset Sequence
      { 0x60,	0x8A,	0x08,	0x00},	// W#88	-	 PON: AVDD_VPH = 0x60 (After Sleep, Put AVDD back to HPM2 & Enable Auto Selection to Master-Bandgap, since PON is configured to Ignore warm_rb in SBL)
      { 0x00,	0x40,	0x50,	0x00},	// W#89	-	 CR-0000169566: XO.MUX_CTL = 0x00 (Configure VMUX to Auto, so that it has XVDD to rely upon & will also enable LDO_XO automatically, esp. after Sleep [since XO_CORE Ignores warm_rb])
      { 0x11,	0x40,	0xC3,	0x00},	// W#90	-	 GPIO4: Write MODE_CTL = 0x11 => DIGITAL_OUTPUT & HIGH STATE (PON_RESET_N was already asserted Low, followed by warm_rb [internal] assertion; before the start of this WARM_RESET_SEQ)
      { 0x80,	0x46,	0xC3,	0x00},	// W#91	-	 GPIO4: Enable (Required, in case it was disabled by SW before the start of Warm-Reset [Cfg to Ignore warm_rb in SBL])
      { 0x51,	0x40,	0xA0,	0x00},	// W#92	-	 MPP1: Restore to ANALOG OUTPUT = 1.25V (esp. after Sleep, since MPP1 is configured to Ignore warm_rb, in SBL)
      { 0xFF,	0x16,	0x08,	0x00},	// W#93	-	 Prism-CR774764: PON.INT_EN_CLR = 0xFF (Will Clear off all pending PON Interrupts, freeing SPMI bandwidth)
      { 0x34,	0x03,	0x01,	0xC0},	// W#94	-	 Wait 25 ms (= 820 cycles of 32 kHz clock) This was reduced from previous 26 ms here, since 1ms has already been covered at the beginning of this Warm-Reset sequence
      { 0x00,	0x00,	0x01,	0x00},	// W#95	-	 DUMMY-15 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0x40,	0x91,	0x08,	0x00},	// W#96	-	 Write Sequence Complete ACK to PON
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#97	-	 EOS (Finish Warm-Reset-Sequence) PON_RESET_N now gets de-asserted to High & PMIC starts the 200 ms timeout for PS_HOLD ack from MSM to get detected as High.
      { 0x00,	0x00,	0x01,	0x00},	// W#98	-	 DUMMY-16 (CR-0000173897) Debug-option for SW-Team, to replace with Any-Signal Enable-command
      { 0xA5,	0xD0,	0x44,	0x01},	// W#99	-	 LDO5: Unlock Secure Access
      { 0x00,	0xE0,	0x44,	0x01},	// W#100	-	 IMS/CR-0000175308 LDO5: Disable INT_TEST_MODE so that VREG_OK now comes from internal comparator
      { 0x08,	0x00,	0x00,	0x80},	// W#101	-	Goto OTP-POFF_SEQ and End there
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#102	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#103	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#104	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#105	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#106	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#107	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#108	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#109	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#110	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#111	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#112	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#113	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#114	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#115	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#116	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#117	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#118	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#119	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#120	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#121	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#122	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#123	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#124	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#125	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#126	-	
      { 0x00,	0x08,	0xFF,	0xFF},	// W#127	-	
   },
   // PBS_RAM_MSM8953.PM.HW.PMi8950_2p0_2_0_4
   {
      //data  offset  base_addr  sid
      { 0x2C,	0x04,	0x00,	0x80},	// W#0	-	
      { 0xB4,	0x04,	0x00,	0x80},	// W#1	-	
      { 0xCC,	0x04,	0x00,	0x80},	// W#2	-	
      { 0xE4,	0x04,	0x00,	0x80},	// W#3	-	
      { 0x2C,	0x05,	0x00,	0x80},	// W#4	-	
      { 0x58,	0x05,	0x00,	0x80},	// W#5	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#6	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#7	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#8	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#9	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#10	-	
      { 0x00,	0x8A,	0x08,	0x40},	// W#11	-	Read AVDD Regulator to determine if we are entering or exiting sleep
      { 0x20,	0x01,	0x20,	0x90},	// W#12	-	If AVDD_HPM_EN (bit 5) == 1 then skip over to sleep sequence
      { 0x74,	0x04,	0x00,	0x80},	// W#13	-	Else AVDD_HPM_EN == 0 sp we are waking, goto wake sequence
      { 0x00,	0x4A,	0x59,	0x00},	// W#14	-	Force SLEEP_B low
      { 0x10,	0x8A,	0x08,	0x00},	// W#15	-	AVDD LDO in LPM
      { 0x00,	0x08,	0x13,	0x40},	// W#16	-	Read PMIC power path status
      { 0xA5,	0xD0,	0x41,	0x00},	// W#17	-	Unlock Secure Access
      { 0x01,	0x00,	0x03,	0x90},	// W#18	-	Skip if not battery powered
      { 0x00,	0xF5,	0x41,	0x00},	// W#19	-	Disable ESR Estimation in SLEEP
      { 0x01,	0x01,	0x03,	0x90},	// W#20	-	Skip if battery powered
      { 0x01,	0xF5,	0x41,	0x00},	// W#21	-	Enable ESR Estimation in SLEEP
      { 0x01,	0x46,	0x59,	0x00},	// W#22	-	CR843858: Put RC Osc in HW CTL during sleep
      { 0x80,	0x44,	0x2C,	0x00},	// W#23	-	MBG in NPM
      { 0x00,	0x8C,	0x08,	0x40},	// W#24	-	Read PERPH_RB_SPARE, to check if SW wants MBG in LPM
      { 0x02,	0x01,	0x02,	0x90},	// W#25	-	Skip next line if, bit is set, to end sleep seq
      { 0x10,	0x44,	0x2C,	0x00},	// W#26	-	MBG in LPM
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#27	-	End of Sleep seq
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#28	-	Blank space for SW to try experimental changes
      { 0x80,	0x44,	0x2C,	0x00},	// W#29	-	MBG in NPM
      { 0x70,	0x8A,	0x08,	0x00},	// W#30	-	 AVDD LDO in HPM
      { 0x01,	0x4A,	0x59,	0x00},	// W#31	-	SLEEP_B to FOLLOW HW (connected HIGH)
      { 0xA5,	0xD0,	0x41,	0x00},	// W#32	-	Unlock Secure Access
      { 0x01,	0xF5,	0x41,	0x00},	// W#33	-	Enable ESR Estimation coming out of SLEEP
      { 0x80,	0x46,	0x59,	0x00},	// W#34	-	CR165419: Force RC Osc on when awake
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#35	-	End wake sequence
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#36	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#37	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#38	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#39	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#40	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#41	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#42	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#43	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#44	-	
      { 0x54,	0x03,	0x01,	0xC0},	// W#45	-	Wait 26ms
      { 0x40,	0x91,	0x08,	0x00},	// W#46	-	ACK to PON module
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#47	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#48	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#49	-	Blank space for SW to try experimental changes
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#50	-	Blank space for SW to try experimental changes
      { 0x00,	0x46,	0xDC,	0x01},	// W#51	-	(CR-0000170085) PBS for IBB/LAB short circuit
      { 0x00,	0x46,	0xDE,	0x01},	// W#52	-	(CR-0000170085) PBS for IBB/LAB short circuit
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#53	-	End of LAB/IBB SCP sequence
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#54	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#55	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#56	-	
      { 0x00,	0x8D,	0x08,	0x40},	// W#57	-	Read the dvdd_rb_spare register to determin WP vs nWP config
      { 0x01,	0x01,	0x01,	0x90},	// W#58	-	Skip next line if WP config is detected
      { 0xFC,	0x04,	0x00,	0x80},	// W#59	-	nWP config detected, so load non-Wipower settings
      { 0xA5,	0xD0,	0x12,	0x00},	// W#60	-	
      { 0x24,	0xF2,	0x12,	0x00},	// W#61	-	Set AICL threshold to 6.25V/6.75V
      { 0x1C,	0x05,	0x00,	0x80},	// W#62	-	Done loading WiPower Settings
      { 0xA5,	0xD0,	0x13,	0x00},	// W#63	-	
      { 0x00,	0xFF,	0x13,	0x00},	// W#64	-	Disable all bits in WI_PWR_OPTIONS register
      { 0xA5,	0xD0,	0x14,	0x00},	// W#65	-	
      { 0x04,	0xF3,	0x14,	0x00},	// W#66	-	Set DCIN input collapse glitch filter to 5ms falling
      { 0xA5,	0xD0,	0x14,	0x00},	// W#67	-	
      { 0x00,	0xF4,	0x14,	0x00},	// W#68	-	Set DCIN AICL debounce time to 20ms rising and 20ms falling
      { 0xA5,	0xD0,	0x16,	0x00},	// W#69	-	
      { 0x84,	0xF2,	0x16,	0x00},	// W#70	-	Disable UVLO dependent on CHG_OK and disable Charge OK function
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#71	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#72	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#73	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#74	-	
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#75	-	
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#76	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#77	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#78	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#79	-	
      { 0x01,	0x00,	0x01,	0xC0},	// W#80	-	Wait ~30us
      { 0x00,	0x54,	0x44,	0x40},	// W#81	-	 Read IMA_OPERATION_STS
      { 0x02,	0x01,	0x02,	0x90},	// W#82	-	If IACS_RDY=1 (bit1), then access is ready, skip the next line
      { 0xF4,	0xFF,	0x00,	0x84},	// W#83	-	Access not ready. Poll again.
      { 0xFF,	0xFF,	0xFF,	0x8C},	// W#84	-	IACS_READY=1
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#85	-	
      { 0x00,	0x44,	0xD8,	0x41},	// W#86	-	Read EN_AMOLED register
      { 0x80,	0x01,	0xFF,	0x90},	// W#87	-	Skip next line if AMOLED mode is enabled (0x80)
      { 0x8C,	0x05,	0x00,	0x80},	// W#88	-	LCD mode selected. Go to the LCD cap buzzing WA.
      { 0x10,	0x40,	0xA2,	0x00},	// W#89	-	Enable PFET (drive MPP3 low)
      { 0x0A,	0x00,	0x00,	0xC0},	// W#90	-	Delay 500ns
      { 0x11,	0x40,	0xA2,	0x00},	// W#91	-	Diable PFET (drive MPP3 high)
      { 0x28,	0x00,	0x00,	0xC0},	// W#92	-	delay 2us
      { 0x00,	0x09,	0x09,	0x40},	// W#93	-	Read DTEST status
      { 0x00,	0x01,	0x01,	0x90},	// W#94	-	Skip next line if DTEST1 is low (indicating precharge timer expired)
      { 0x64,	0x05,	0x00,	0x80},	// W#95	-	DTEST1 is still high. Keep pulsing.
      { 0x1A,	0x40,	0xA2,	0x00},	// W#96	-	Configure MPP3 to follow DTEST2
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#97	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#98	-	
      { 0x00,	0x09,	0x09,	0x40},	// W#99	-	Read DTEST Status. CR-0000175317 
      { 0x00,	0x01,	0x01,	0x90},	// W#100	-	Skip next line if DTEST1=0
      { 0xA4,	0x05,	0x00,	0x80},	// W#101	-	DTEST1=1 --> GoTo Enable PFM.
      { 0x05,	0x5D,	0xD8,	0x01},	// W#102	-	DTEST=0 --> Disable PFM
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#103	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#104	-	
      { 0x85,	0x5D,	0xD8,	0x01},	// W#105	-	DTEST1=1 --> Enable PFM.
      { 0xFF,	0xFF,	0xFF,	0xFC},	// W#106	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#107	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#108	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#109	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#110	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#111	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#112	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#113	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#114	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#115	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#116	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#117	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#118	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#119	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#120	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#121	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#122	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#123	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#124	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#125	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#126	-	
      { 0x00,	0x04,	0xFF,	0xF8},	// W#127	-	
   },
};
