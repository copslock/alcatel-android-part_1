#include "pm_boot.h"
#include "pm_pon.h"
#include "pm_version.h"
#include "device_info.h"
#include "pm_comm.h"

#define PRIMARY_PMIC_INDEX 0

pm_err_flag_type
pm_device_pre_init(void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}

pm_err_flag_type
pm_device_post_init(void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pbs_info_type pbs_info;
    uint8              pmic_index = 0;

	if(PM_ERR_FLAG__SUCCESS == pm_is_pmic_supported(PMIC_IS_PMI8950, &pmic_index))
    {
        err_flag |= pm_comm_write_byte(2*PRIMARY_PMIC_INDEX, 0x853, 0x80, 1);   /* GP1 Trigger Enable */ 
    }
    /* PON KPDPWR configuration:  s1_timer=10256ms, S2_timer=100ms,  Warm Reset */
    err_flag |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_WARM_RESET);

    /* PON RESIN_AND_KPDPWR configuration:  s1_timer=6720ms, S2_timer=100ms,  Hard Reset */
    err_flag |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_HARD_RESET);

    for (pmic_index = 0; pmic_index < PM_MAX_NUM_PMICS; pmic_index++)
    {
        /* Write PBS ROM ID in REV_ID.PBS_OTP_ID (0x0154) */
        if (PM_ERR_FLAG__SUCCESS == pm_get_pbs_info(pmic_index, &pbs_info))
        {
            uint8 rom_id = (uint8)(((pbs_info.rom_version & 0x00001F00) >> 8) | ((pbs_info.rom_version & 0x00010000) >> 11) | ((pbs_info.rom_version & 0x00000003) << 6));
            err_flag |= pm_comm_write_byte(2*pmic_index, 0x0154, rom_id, 1);   /* Update REV_ID.PBS_OTP_ID */
        }
    }

    return err_flag;
}


pm_err_flag_type
pm_driver_pre_init (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}

pm_err_flag_type
pm_driver_post_init (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}

pm_err_flag_type
pm_sbl_chg_pre_init (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}


pm_err_flag_type
pm_sbl_chg_post_init (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    return err_flag;
}


