

#include "pm_target_information.h"
#include "CoreVerify.h"
#include "pm_smbchg_driver.h"
#include "pm_smbchg_chgr.h"

/* Static global variable to store the SMBCHG driver data */
static pm_smbchg_data_type *pm_smbchg_data_arr[PM_MAX_NUM_PMICS];
static uint8 charger_pmic_index = 0xFF;
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void
pm_smbchg_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_smbchg_data_type *smbchg_ptr = NULL;
    uint32 prop_id_arr[] = {PM_PROP_SMBCHGA_NUM, PM_PROP_SMBCHGB_NUM, PM_PROP_SMBCHGC_NUM}; 

    smbchg_ptr = pm_smbchg_data_arr[pmic_index];

    if (smbchg_ptr == NULL)
    {
        pm_malloc( sizeof(pm_smbchg_data_type), (void**)&smbchg_ptr);                                                    
        /* Assign Comm ptr */
        smbchg_ptr->comm_ptr = comm_ptr;

        /* smbchg Register Info - Obtaining Data through dal config */
        smbchg_ptr->smbchg_register = (smbchg_register_ds*)pm_target_information_get_common_info((uint32)PM_PROP_SMBCHG_REG);
        CORE_VERIFY_PTR(smbchg_ptr->smbchg_register);

        /* SMBCHG Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));

	    smbchg_ptr->num_of_peripherals = pm_target_information_get_periph_count_info(prop_id_arr[pmic_index], pmic_index);
        CORE_VERIFY(smbchg_ptr->num_of_peripherals != 0);

        smbchg_ptr->chg_range_data = (chg_range_data_type*)pm_target_information_get_specific_info((uint32)PM_PROP_SMBCHG_DATA);
        CORE_VERIFY_PTR(smbchg_ptr->chg_range_data);

        pm_smbchg_data_arr[pmic_index] = smbchg_ptr;
        charger_pmic_index = pmic_index;
    }
}

pm_smbchg_data_type* pm_smbchg_get_data(uint8 pmic_index)
{
  if(pmic_index < PM_MAX_NUM_PMICS)
  {
      return pm_smbchg_data_arr[pmic_index];
  }

  return NULL;
}
uint8 pm_smbchg_get_index()
{
  return charger_pmic_index;
}

