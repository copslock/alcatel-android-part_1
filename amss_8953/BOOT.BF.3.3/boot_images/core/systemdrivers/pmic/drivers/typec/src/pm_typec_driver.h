#ifndef PM_TYEPC_DRIVER__H
#define PM_TYEPC_DRIVER__H

/*! \file pm_typec _driver.h
 *  \n
 *  \brief This file contains functions prototypes and variable/type/constant
*          declarations for supporting type-c peripheral  
 *  \n  
 *  \n &copy;
 *  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when         who      what, where, why
--------   ---      ----------------------------------------------------------
12/08/15   pxm      Created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_target_information.h"
#include "pm_comm.h"

/*===========================================================================

                     STRUCTURE TYPE AND ENUM

===========================================================================*/


/************************************************************************/
/* register definitions                                                                     */
/************************************************************************/
typedef struct 
{
    uint32                   base_address;          //0xBF00

    pm_register_address_type status1;               // 0xBF08
    pm_register_address_type status2;               // 0xBF09
    pm_register_address_type en_ctl1;               // 0xBF46
    pm_register_address_type sw_ctl;                // 0xBF52
    pm_register_address_type sec_access;            // 0xBFD0
} typec_register_ds;


typedef struct
{
    pm_comm_info_type            *comm_ptr;
    typec_register_ds            *typec_register;
    uint8                        num_of_peripherals;
} pm_typec_data_type;


/*===========================================================================

                     FUNCTION DECLARATION 

===========================================================================*/
void pm_typec_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index);

pm_typec_data_type* pm_typec_get_data(uint8 pmic_index);

#endif // PM_TYEPC_DRIVER__H
