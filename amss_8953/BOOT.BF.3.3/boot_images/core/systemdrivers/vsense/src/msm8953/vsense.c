/*
===========================================================================
  @file vsense.c

  This file provides functions to calibrate the voltage sensor data and store in shared memory.
===========================================================================

  Copyright (c) 2012 - 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================



  =========================================================================
*/

#include "HALhwio.h"
#include "tcsr_hwio.h"
#include "apcs_hwio.h"
#include "DALSys.h"
#include "smem_type.h"
#include "vsense.h"
#include "smem.h"
#include "railway.h"
#include "vsense_internal.h"
#include "pm_smps.h"
#include "pm_ldo.h"
#include "DALSys.h"
#include "CoreVerify.h"
#include "ClockBoot.h"
#include "boot_util.h"
//#include "apcs_alias1_apss_glb_hwio.h"
#include "DALFramework.h"
//#include "apcs_common_apss_vs.h"

#include "boot_logger.h"



uint8 vsense_calibrate_rail(vsense_type_rail rail);
void vsense_read_fifo(vsense_type_rail rail,  uint32* fifo_sum);
uint32 vsense_set_voltage( vsense_type_rail rail, 
                   railway_corner corner, uint32 fuse_voltage );
void vsense_apc1_configure(boolean enable);
void vsense_get_rail_status( vsense_type_rail rail, uint32* voltage, 
                                 uint32* sw_enable, railway_settings* settings );
void vsense_enable_rail(vsense_type_rail rail, uint32 sw_enable);
void vsense_power_down(vsense_type_rail rail);
void vsense_unreset_and_cgc_clk_enable(vsense_type_rail rail);


#define VSENSE_FIFO_ARRAY_SIZE  64
#define VSENSE_FIFO_READ_COUNT  10
#define VSENSE_FIFO_READ_DELAY  1 //10 XO clk cycles .. 10 * (1000000/19200000)
static  void *vsense_smem_ptr = NULL;
#define VSENSE_MIN_FUSE_VOLTAGE_DIFF 100*1000 //100 mV



#define LOG_BUF_SIZE 128
static char s_buf[LOG_BUF_SIZE] = {0};
static boolean s_log_on = TRUE;

// LOGD: debug logging. Enable by setting s_log_on TRUE, disable by setting s_log_on FALSE.
// We should by default disable logging it since it would increase boot time
#define LOGD(x) \
    do { \
        if(s_log_on) { \
            boot_log_message(x); \
        } \
    } while(0)

// leave here so we can use it directly when we need to debug in this file.
#define LOGD_STUB(x) \
    do { \
        if(s_log_on) { \
            snprintf(s_buf, LOG_BUF_SIZE, "%s. Func: %s, L%d", x, __FUNCTION__, __LINE__);\
            boot_log_message(s_buf); \
        } \
    } while(0)


static vsense_type_rail_and_fuse_info vsense_rail_and_fuse_info =
{
  .rail_info = 
  {
    {
      .rail  = VSENSE_TYPE_RAIL_MX,
      .is_supported = TRUE,
      .peripheral_index = 6, //S7A is MX
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .fuse1_corner = RAILWAY_SVS_SOC,
      .fuse2_corner = RAILWAY_SUPER_TURBO_NO_CPR,
      .config_0_address =  HWIO_ADDR(TCSR_VSENS1_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(TCSR_VSENS1_CONFIG_1),
      .status_address   =  HWIO_ADDR(TCSR_VSENS1_STATUS)
    },                
    {               
      .rail = VSENSE_TYPE_RAIL_MSS,
      .is_supported = TRUE,
      .peripheral_index = 0, //S1A is MSS
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .fuse1_corner = RAILWAY_SVS_SOC,
      .fuse2_corner = RAILWAY_SUPER_TURBO_NO_CPR,
      .config_0_address =  HWIO_ADDR(TCSR_VSENS2_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(TCSR_VSENS2_CONFIG_1),
      .status_address   =  HWIO_ADDR(TCSR_VSENS2_STATUS)
    },
    {               
      .rail = VSENSE_TYPE_RAIL_CX,
      .is_supported = TRUE,
      .peripheral_index = 1, //S2A is CX
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .fuse1_corner = RAILWAY_NOMINAL,
      .fuse2_corner = RAILWAY_SUPER_TURBO_NO_CPR,
      .config_0_address =  HWIO_ADDR(TCSR_VSENS0_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(TCSR_VSENS0_CONFIG_1),
      .status_address   =  HWIO_ADDR(TCSR_VSENS0_STATUS)
    },

   {               
      .rail = VSENSE_TYPE_RAIL_APC0,
      .is_supported = TRUE, 
      .peripheral_index = 4, //S5A is APC rail
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .fuse1_corner = RAILWAY_NOMINAL,
      .fuse2_corner = RAILWAY_SUPER_TURBO,
      .config_0_address =  HWIO_ADDR(APCS_COMMON_APC0_VSENS_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(APCS_COMMON_APC0_VSENS_CONFIG_1),
      .status_address   =  HWIO_ADDR(APCS_COMMON_APC0_VSENS_STATUS)
    },
    {               
      .rail = VSENSE_TYPE_RAIL_APC1,
      .is_supported = TRUE, 
      .peripheral_index = 4, //S5A is APC rail
      .pmic_index = 0,
      .pmic_resource_type = VSENSE_TYPE_PMIC_RESOURCE_SMPS,
      .fuse1_corner = RAILWAY_NOMINAL,
      .fuse2_corner = RAILWAY_SUPER_TURBO,
      .config_0_address =  HWIO_ADDR(APCS_COMMON_APC1_VSENS_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(APCS_COMMON_APC1_VSENS_CONFIG_1),
      .status_address   =  HWIO_ADDR(APCS_COMMON_APC1_VSENS_STATUS)
    },
  },
  
  .smem_info =
  {  
    .version = 1,
    .num_of_vsense_rails = VSENSE_TYPE_RAIL_MAX,
    .rails_fuse_info = 
    {
     {
        .rail  = VSENSE_TYPE_RAIL_MX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,	      
      },                
      {               
        .rail = VSENSE_TYPE_RAIL_MSS,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv =  1000000,
     
      },
      {               
        .rail  = VSENSE_TYPE_RAIL_CX,
        .fuse1_voltage_uv =  900000, 
        .fuse2_voltage_uv = 1000000,	
        
      },

     {               
        .rail = VSENSE_TYPE_RAIL_APC0,
        .fuse1_voltage_uv =   900000, 
        .fuse2_voltage_uv =  1000000,
    
      },
      {               
        .rail = VSENSE_TYPE_RAIL_APC1,
        .fuse1_voltage_uv =   900000, 
        .fuse2_voltage_uv =  1000000,
      },
    }
  }
  
};

const vsense_type_rail_and_fuse_info* const vsense_rail_ref = &vsense_rail_and_fuse_info;



//configure common settings for any voltage sensor before
//it can be used
void vsense_config_common(vsense_type_rail rail)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;
  //need to write multiple times to the same register since sw
  //needs to maintain the state machine for Elessar
  //DO NOT OPTMIZE THIS CODE TO A SINGLE REGISTER WRITE
  val_config_1 = HWIO_FVAL(TCSR_VSENS0_CONFIG_1, VS_CSR_QSS_MUX_SEL, 1); 
  outpdw(addr_config_1, val_config_1);
  val_config_0 = HWIO_FVAL(TCSR_VSENS0_CONFIG_1, POWER_EN, 1);
  outpdw(addr_config_0, val_config_0);
  
  //val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CLAMP_DIS, 1) ;
  //outpdw(addr_config_1, val_config_1);

  //val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, RESET_FUNC, 1);
  //outpdw(addr_config_0, val_config_0);
  
  //val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE, 1);
  //outpdw(addr_config_1, val_config_1);

}


//allocate shared memory for vsense driver

volatile int vs_enable = 0 ;

boolean vsense_init()
{
  uint32 rail_count;
  int32  railway_id;

  if (0 == vs_enable)
  {
	  return TRUE;
  }
  LOGD("START vsense_init");

  if(Clock_InitVSense() == FALSE)
  {
     return FALSE;
  }
  
  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_MAX; rail_count++ )
  {
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
    { 
      switch(vsense_rail_and_fuse_info.rail_info[rail_count].rail)
      {
        case VSENSE_TYPE_RAIL_MX:
        case VSENSE_TYPE_RAIL_MSS:
          railway_id = rail_id("vddmx");

          break;
        case VSENSE_TYPE_RAIL_CX:
          railway_id = rail_id("vddcx");
          break;

        default: 
          railway_id = RAIL_NOT_SUPPORTED_BY_RAILWAY;
      }
      vsense_rail_and_fuse_info.rail_info[rail_count].railway_id = railway_id;
    } 
  }
  vsense_start_calibration();
  

  {
      snprintf(s_buf, LOG_BUF_SIZE, "VSENSE MX: Vol1=%d,Code1=%d, Vol2=%d,Code2=%d", 
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MX].fuse1_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MX].fuse1_voltage_code,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MX].fuse2_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MX].fuse2_voltage_code);
    LOGD(s_buf);
      snprintf(s_buf, LOG_BUF_SIZE, "VSENSE MSS: Vol1=%d,Code1=%d, Vol2=%d,Code2=%d", 
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MSS].fuse1_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MSS].fuse1_voltage_code,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MSS].fuse2_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_MSS].fuse2_voltage_code);
    LOGD(s_buf);
    snprintf(s_buf, LOG_BUF_SIZE, "VSENSE CX: Vol1=%d,Code1=%d, Vol2=%d,Code2=%d", 
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_CX].fuse1_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_CX].fuse1_voltage_code,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_CX].fuse2_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_CX].fuse2_voltage_code);
    LOGD(s_buf);

    snprintf(s_buf, LOG_BUF_SIZE, "VSENSE APC0: Vol1=%d,Code1=%d, Vol2=%d,Code2=%d", 
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC0].fuse1_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC0].fuse1_voltage_code,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC0].fuse2_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC0].fuse2_voltage_code);
    LOGD(s_buf);
    snprintf(s_buf, LOG_BUF_SIZE, "VSENSE APC1: Vol1=%d,Code1=%d, Vol2=%d,Code2=%d", 
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC1].fuse1_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC1].fuse1_voltage_code,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC1].fuse2_voltage_uv,
          vsense_rail_and_fuse_info.smem_info.rails_fuse_info[VSENSE_TYPE_RAIL_APC1].fuse2_voltage_code);
    LOGD(s_buf);


  }
  return Clock_ShutdownVSense();
}


//store the calibrated data into the shared memory 
boolean vsense_copy_to_smem()
{

vsense_smem_ptr = (void *)smem_alloc(SMEM_VSENSE_DATA, 
                          sizeof(vsense_rail_and_fuse_info.smem_info));
  if(vsense_smem_ptr == NULL) 
  {  
    return FALSE;
  }
  qmemscpy(vsense_smem_ptr, sizeof(vsense_rail_and_fuse_info.smem_info),
              &vsense_rail_and_fuse_info.smem_info, 
              sizeof(vsense_rail_and_fuse_info.smem_info));
  
  LOGD("VSENSE codes wrote into SMEM");
  return TRUE;

}

//calibrate the vsense for the supported rails
boolean vsense_start_calibration()
{
  uint32 rail_count;
  uint32 voltage = 0;
  uint32 sw_enable = 0;
  railway_settings settings = {RAILWAY_NOMINAL, RAIL_VOLTAGE_LEVEL_NOMINAL, 0};

  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_MAX; rail_count++ )
  {
    voltage = 0;
    sw_enable = 0;
    settings.mode = RAILWAY_NOMINAL;
    settings.microvolts = 0;
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
    {
      vsense_get_rail_status( vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                             &voltage, &sw_enable, &settings);

      if(vsense_rail_and_fuse_info.rail_info[rail_count].rail == VSENSE_TYPE_RAIL_APC1)
      {
        vsense_apc1_configure(TRUE); 
      }

      //set the fuse 1 voltage
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_uv = 
                    vsense_set_voltage( vsense_rail_and_fuse_info.rail_info[rail_count].rail , 
                    vsense_rail_and_fuse_info.rail_info[rail_count].fuse1_corner, 
                    vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_uv);

      if(vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_uv == 0)
      {
        vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].not_calibrated =
        	 TRUE;
        continue;
      }

      // turn rail on for calibration
      if(sw_enable == PM_OFF)
      {
        vsense_enable_rail(vsense_rail_and_fuse_info.rail_info[rail_count].rail, 
                          PM_ON);
        DALSYS_BusyWait(500); //need for vsense   
      }
      
      //calibrate the rail
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_code = 
                  vsense_calibrate_rail(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
  
      //set fuse 2 voltage
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse2_voltage_uv = 
                    vsense_set_voltage( vsense_rail_and_fuse_info.rail_info[rail_count].rail , 
                    vsense_rail_and_fuse_info.rail_info[rail_count].fuse2_corner, 
                    vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse2_voltage_uv);


      if(vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_uv == 0)
      {
        vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].not_calibrated =
        	 TRUE;
        continue;
      }
      
      //ensure 100mV gap between fuse1 and fuse2 voltage
      CORE_VERIFY(vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse2_voltage_uv -
      	   vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_uv >= 
            VSENSE_MIN_FUSE_VOLTAGE_DIFF);
      
      //calibrate the rail
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse2_voltage_code = 
                  vsense_calibrate_rail(vsense_rail_and_fuse_info.rail_info[rail_count].rail);

      //fuse 1 code (lower voltage) cannot be greater than/equal to fuse 2 code (higher voltage)
      if(vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse1_voltage_code >= 
      	  vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].fuse2_voltage_code )
      {
        vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].not_calibrated =
       	   TRUE;
         
      }
      //turn off the rail if it was off previously
      if(sw_enable == PM_OFF)
      {
        vsense_enable_rail(vsense_rail_and_fuse_info.rail_info[rail_count].rail, PM_OFF);
      }
      
      if(vsense_rail_and_fuse_info.rail_info[rail_count].rail == VSENSE_TYPE_RAIL_APC1)
      {
        vsense_apc1_configure(FALSE); 
      }

      //restore the rail voltage to before calibration state
      vsense_set_voltage( vsense_rail_and_fuse_info.rail_info[rail_count].rail ,settings.mode, 
                          voltage);
       vsense_power_down(vsense_rail_and_fuse_info.rail_info[rail_count].rail);   
    }
   
 }

  return TRUE;
}

void vsense_enable_rail(vsense_type_rail rail, uint32 sw_enable)
{
  /*if(rail == VSENSE_TYPE_RAIL_APC1)
  {
    vsense_apc1_configure(sw_enable); 
  }*/
  switch( vsense_rail_and_fuse_info.rail_info[rail].pmic_resource_type)
  {
    case VSENSE_TYPE_PMIC_RESOURCE_LDO:
       
      pm_ldo_sw_enable(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                        vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                        (pm_on_off_type)sw_enable);
      break;
    case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
      pm_smps_sw_enable(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                        vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                        (pm_on_off_type)sw_enable);
      break;
  }

}
//set the fuse voltage based on corner -for railway
//else set the default fuse voltage
uint32 vsense_set_voltage( vsense_type_rail rail, railway_corner corner, uint32 fuse_voltage )
{   
  uint32 uv = fuse_voltage;
  //railway_voter_t rail_voter;
  //railway_settings mx_settings;
  uint32 mx_voltage = 0;

  pm_smps_volt_level_status(vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].pmic_index,
                                    vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].peripheral_index ,
                                    (pm_volt_level_type*) &mx_voltage);


  //railway_get_current_settings( vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].railway_id, 
  //  	                             &mx_settings);
  

  /*if( rail == VSENSE_TYPE_RAIL_CX )
  {
    uv = railway_get_corner_voltage( vsense_rail_and_fuse_info.rail_info[rail].railway_id, corner);
    rail_voter = railway_create_voter(vsense_rail_and_fuse_info.rail_info[rail].railway_id, 
                                     RAILWAY_VSENSE_VOTER_ID);
    //check if voltage is <= Memory domain 
    if(uv  > mx_voltage)
    {
      //return 0; //TODO ARUN - revisit this logic, today it will fail all other rails, as the MX is at .9V while other rails will be higher       
    }
       
    railway_corner_vote(rail_voter, corner);
    railway_transition_rails();
  }
  else*/
 {
    //check if voltage is <= Memory domain 
    if((rail != VSENSE_TYPE_RAIL_MX) && (rail != VSENSE_TYPE_RAIL_MSS) && (fuse_voltage > mx_voltage))
    {
      //return 0; //TODO ARUN - revisit this logic, today it will fail all other rails, as the MX is at .9V while other rails will be higher              
    }
    
    switch( vsense_rail_and_fuse_info.rail_info[rail].pmic_resource_type)
    {
      case VSENSE_TYPE_PMIC_RESOURCE_LDO:
        pm_ldo_volt_level(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                            vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                            fuse_voltage);
        break;
        case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
        pm_smps_volt_level( vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                            vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                            fuse_voltage);
        break;
    }
    DALSYS_BusyWait(500);  
  }

  return uv;
}

void vsense_get_rail_status( vsense_type_rail rail, uint32* voltage, 
                                 uint32* sw_enable, railway_settings* settings )
{   
  
  /*if(rail == VSENSE_TYPE_RAIL_CX )
  {
    railway_get_current_settings( vsense_rail_and_fuse_info.rail_info[rail].railway_id, 
                                 settings);
  }*/
  switch( vsense_rail_and_fuse_info.rail_info[rail].pmic_resource_type)
  {
    case VSENSE_TYPE_PMIC_RESOURCE_LDO:
      pm_ldo_volt_level_status( vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                                vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                                voltage);
      pm_ldo_sw_enable_status(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                                vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                                (pm_on_off_type *)sw_enable);
      break;
    case VSENSE_TYPE_PMIC_RESOURCE_SMPS:
      pm_smps_volt_level_status(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                                vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                                voltage);
      pm_smps_sw_enable_status(vsense_rail_and_fuse_info.rail_info[rail].pmic_index,
                                vsense_rail_and_fuse_info.rail_info[rail].peripheral_index , 
                                (pm_on_off_type *)sw_enable);
     break;
  }
  
}

void vsense_en_func(vsense_type_rail rail,boolean en_func)
{
  uint32 val_config_1;
  uint32 addr_config_1; 

  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  val_config_1 = inpdw(addr_config_1);
   
  if(en_func == TRUE)
  {
    val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1);
  }
  else
  {
    val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN);
  }
  outpdw(addr_config_1, val_config_1);

}

//enable and set the start capture 
void vsense_start_capture(vsense_type_rail rail, boolean en_capture)
{
  uint32 val_config_0;
  uint32 addr_config_0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;
  val_config_0 = inpdw(addr_config_0);
 
  //val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, EN_ALARM);
  // outpdw(addr_config_1, val_config_1);
 

  //DALSYS_BusyWait(1); //let the state machine settle
  if(en_capture == TRUE)
  {
    val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, SW_CAPTURE, 1);
  }
  else
  {
    val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, SW_CAPTURE);
  }

  outpdw(addr_config_0, val_config_0);
  
}

void vsense_reset_and_cgc_clk_disable(vsense_type_rail rail)
{
  //uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  //uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  //addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  //val_config_0 = inpdw(addr_config_0);
  val_config_1 = inpdw(addr_config_1);


  //val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE); - NO clock enable/disable in Jacala
  //outpdw(addr_config_1, val_config_1);

  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN);  ///Set FUNC_EN to 0
  outpdw(addr_config_1, val_config_1);

  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN); 
  outpdw(addr_config_1, val_config_1);

  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN); 
  outpdw(addr_config_1, val_config_1);
  //val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, RESET_FUNC);
  //outpdw(addr_config_0, val_config_0);

}

//disable and reset the vsense
void vsense_power_down(vsense_type_rail rail)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  val_config_0 = 0x00FF0000;
  val_config_1 = 0x00001FE0;
  
  //Do not change the order and do not try to optmize it to a 
  //single write !!!!!
 /* val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, EN_FUNC);
  outpdw(addr_config_1, val_config_1);

  val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, START_CAPTURE); 
  outpdw(addr_config_0, val_config_0);

  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE);
 outpdw(addr_config_1, val_config_1);
 
  val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, RESET_FUNC);
  outpdw(addr_config_0, val_config_0);
  */

  outpdw(addr_config_0, val_config_0);
  outpdw(addr_config_1, val_config_1); 
  
}

void vsense_unreset_and_cgc_clk_enable(vsense_type_rail rail)
{
  //uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  //uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  //addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  //val_config_0 = inpdw(addr_config_0);
  val_config_1 = inpdw(addr_config_1);


  val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1);
  outpdw(addr_config_1, val_config_1);

}

void vsense_disable_alarms(vsense_type_rail rail)
{
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
 

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
 
  val_config_1 = inpdw(addr_config_1);

  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN); // TODO review
  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN);
  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_SLOPE_POS_EN);

  outpdw(addr_config_1, val_config_1);
  
}

//start the capture, read fifio and return the fifo average -> voltage code
uint8 vsense_calibrate_rail(vsense_type_rail rail)
{
  //uint32 val_config_0 ;
  //uint32 val_config_1 ;

  uint32 fifo_sum=0;
  uint32 fifo_cumulative_sum = 0;
  uint8 read_fifo_count ;
  
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return 0;
  }
  
  
  //configure the common settings
  vsense_config_common(rail);
  vsense_disable_alarms(rail);
  for(read_fifo_count = 0; read_fifo_count < VSENSE_FIFO_READ_COUNT; read_fifo_count++)
  {
    vsense_en_func(rail, TRUE); 
    //set the en and start capture 
    vsense_start_capture(rail, TRUE);
   //read the fifo buffer
    vsense_read_fifo(rail, &fifo_sum);
    //sum the fifo read
    fifo_cumulative_sum += fifo_sum;
    //disable the vsense and reset
   // vsense_power_down(rail);
   // vsense_unreset_and_cgc_clk_enable(rail);
    vsense_en_func(rail,FALSE);
    vsense_start_capture(rail, FALSE);
    vsense_reset_and_cgc_clk_disable(rail);
    vsense_unreset_and_cgc_clk_enable(rail);
  }
  vsense_power_down(rail);
  vsense_disable_alarms(rail);
  
  return   (fifo_cumulative_sum + 
                (VSENSE_FIFO_ARRAY_SIZE * VSENSE_FIFO_READ_COUNT)/2 ) /
                (VSENSE_FIFO_ARRAY_SIZE * VSENSE_FIFO_READ_COUNT) ;
  
}

//read the 64 bytes fifo from the status register
void vsense_read_fifo(vsense_type_rail rail, uint32* fifo_sum)
{
  uint32 addr_config_1 = 0;
  uint32 addr_status  = 0 ;
  uint32 val_config_1 = 0;
  uint32 val_status_reg = 0; 
  uint32 fifo_array_sum = 0;
  int8   fifo_reg_offset  = 0x3F;
  uint8  read_byte_count = 0;
  static uint8  fifo_data[VSENSE_FIFO_ARRAY_SIZE] = {0}; //ToDo: remove the static later 
   
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_status = vsense_rail_and_fuse_info.rail_info[rail].status_address;
  val_status_reg = inpdw(addr_status);
 
  DALSYS_memset(fifo_data,VSENSE_FIFO_ARRAY_SIZE,0);
 
  if(val_status_reg & HWIO_FVAL(TCSR_VSENS0_STATUS, FIFO_COMPLETE, 1))
  {
    do
    {
      val_config_1 = inpdw(addr_config_1);
      val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FIFO_RD_ADDRESS);
      val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FIFO_RD_ADDRESS, fifo_reg_offset);
      outpdw(addr_config_1, val_config_1);
      DALSYS_BusyWait(VSENSE_FIFO_READ_DELAY); //10 XO clk  cycles
      val_status_reg = inpdw(addr_status);
      fifo_data[read_byte_count] = 
      (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, FIFO_DATA)) >>
      HWIO_SHFT(TCSR_VSENS0_STATUS, FIFO_DATA);
      fifo_array_sum +=  fifo_data[read_byte_count++];
      fifo_reg_offset--;
    }while (fifo_reg_offset >= 0);
    *fifo_sum = fifo_array_sum;
  }
}
   
//Turn on the L2 to power the voltage sensor
void vsense_apc1_configure( boolean enable)
{
/*
  if(enable == TRUE)
  {
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x00029716); 
    memory_barrier();
    DALSYS_BusyWait(8);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x00023716);
    memory_barrier();
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002371E);
    memory_barrier();
    DALSYS_BusyWait(8);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002371C);
    memory_barrier();
    DALSYS_BusyWait(4);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002361C);
    memory_barrier();
    DALSYS_BusyWait(2);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x1002361C);
    memory_barrier();

  }
  else
  {    
    //turn off L2 by magic numbers
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002361C);  
    memory_barrier(); 
    DALSYS_BusyWait(4);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002371C);
    memory_barrier(); 
    DALSYS_BusyWait(4);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x0002371E);
    memory_barrier(); 
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x00023716);
    memory_barrier(); 
    DALSYS_BusyWait(8);
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x00029716);
    memory_barrier(); 
    HWIO_OUT(APCS_ALIAS1_L2_PWR_CTL,0x00109506);
    memory_barrier();
  }
*/
}


