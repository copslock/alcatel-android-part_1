#ifndef VSENSE_INTERNAL_H
#define VSENSE_INTERNAL_H
/*
===========================================================================
*/
/**
  @file vsense_internal.h

  Common internal data structures for vsense init and start calibration routine
*/
/*
  ====================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================


  when        who     what, where, why
  --------    ---     -------------------------------------------------
  2014/10/14  aks      Initial revision.
  ====================================================================
*/

#include "com_dtypes.h"

typedef enum
{
  VSENSE_TYPE_RAIL_MX      ,
  VSENSE_TYPE_RAIL_MSS     , 
  VSENSE_TYPE_RAIL_CX      ,
  VSENSE_TYPE_RAIL_APC0    , 
  VSENSE_TYPE_RAIL_APC1    , 
  VSENSE_TYPE_RAIL_MAX     , 
}vsense_type_rail;

typedef enum
{
  VSENSE_TYPE_PMIC_RESOURCE_LDO,
  VSENSE_TYPE_PMIC_RESOURCE_SMPS
}vsense_type_pmic_resource;

typedef struct
{
  vsense_type_rail           rail;
  boolean                    is_supported;
  uint8                      peripheral_index;  
  uint8                      pmic_index;
  int32                      railway_id;
  vsense_type_pmic_resource  pmic_resource_type;
  railway_corner             fuse1_corner;
  railway_corner             fuse2_corner;
  uint32                     config_0_address;
  uint32                     config_1_address;
  uint32                     status_address;
}vsense_type_rail_info;


typedef struct
{
  vsense_type_rail          rail;
  uint32                    not_calibrated;
  uint32                    fuse1_voltage_uv;
  uint32                    fuse2_voltage_uv;
  uint32                    fuse1_voltage_code;
  uint32                    fuse2_voltage_code;
}vsense_type_fuse_info;

typedef struct
{
  uint32                    version;
  uint32                    num_of_vsense_rails; //no. of rails vsense is monitoring
  vsense_type_fuse_info     rails_fuse_info[VSENSE_TYPE_RAIL_MAX]; //array size is num_of_rails_supported value 
}vsense_type_smem_info;



typedef struct 
{ 
  vsense_type_smem_info      smem_info;
  vsense_type_rail_info      rail_info[VSENSE_TYPE_RAIL_MAX];         
}vsense_type_rail_and_fuse_info;


#endif
