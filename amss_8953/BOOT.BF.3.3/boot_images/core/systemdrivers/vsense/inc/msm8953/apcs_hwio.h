#ifndef __APCS_HWIO_H__
#define __APCS_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8953 (Jacala) [jacala_v1.0_p3q3r17.3_MTO]
 
  This file contains HWIO register definitions for the following modules:
    APCS_COMMON_APSS_VS

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8953/apcs_hwio.h#1 $
  $DateTime: 2015/12/21 23:11:20 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: APCS_COMMON_APSS_VS
 *--------------------------------------------------------------------------*/

#include "msmhwiobase.h"

#define APCS_COMMON_APSS_VS_REG_BASE                                          (A53SS_BASE      + 0x001d6000)

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000000)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_SLOPE_BMSK             0xff000000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_SLOPE_SHFT                   0x18
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                 0xff0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                     0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                   0xff00
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                      0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                     0xf0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                      0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_POS_BMSK                           0xc
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_TRIG_POS_SHFT                           0x2
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RSVD_BITS1_0_BMSK                       0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_0_RSVD_BITS1_0_SHFT                       0x0

#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000004)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BITS31_18_BMSK              0xfffc0000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BITS31_18_SHFT                    0x12
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_NEG_EN_BMSK             0x20000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_NEG_EN_SHFT                0x11
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_POS_EN_BMSK             0x10000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_SLOPE_POS_EN_SHFT                0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_MIN_EN_BMSK                    0x8000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_MIN_EN_SHFT                       0xf
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_MAX_EN_BMSK                    0x4000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_ALARM_MAX_EN_SHFT                       0xe
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_POWER_EN_BMSK                        0x2000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_POWER_EN_SHFT                           0xd
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FUNC_EN_BMSK                         0x1000
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_FUNC_EN_SHFT                            0xc
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLEAR_BMSK                            0x800
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_CLEAR_SHFT                              0xb
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BIT10_BMSK                       0x400
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BIT10_SHFT                         0xa
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_HW_CAPTURE_EN_BMSK                    0x200
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_HW_CAPTURE_EN_SHFT                      0x9
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SW_CAPTURE_BMSK                       0x100
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SW_CAPTURE_SHFT                         0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_TRIG_SEL_BMSK                          0xe0
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_TRIG_SEL_SHFT                           0x5
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_MODE_SEL_BMSK                          0x10
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_MODE_SEL_SHFT                           0x4
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BIT3_BMSK                          0x8
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_RSVD_BIT3_SHFT                          0x3
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_DELTA_CYC_BMSK                    0x7
#define HWIO_APCS_COMMON_APC0_VSENS_CONFIG_1_SLOPE_DELTA_CYC_SHFT                    0x0

#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR                               (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000008)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK                                  0x3ffff
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC0_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC0_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_BMSK                        0x3fc00
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_DATA_SHFT                            0xa
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_BMSK                          0x3c0
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_VS_FSM_ST_SHFT                            0x6
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                   0x20
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                    0x5
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                     0x10
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                      0x4
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_BMSK                            0x8
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MAX_SHFT                            0x3
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_BMSK                            0x4
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_ALARM_MIN_SHFT                            0x2
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_NEG_BMSK                            0x2
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_NEG_SHFT                            0x1
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_POS_BMSK                            0x1
#define HWIO_APCS_COMMON_APC0_VSENS_STATUS_SLOPE_POS_SHFT                            0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_SLOPE_BMSK             0xff000000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_SLOPE_SHFT                   0x18
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_BMSK                 0xff0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MAX_SHFT                     0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_BMSK                   0xff00
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_THRESHOLD_MIN_SHFT                      0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_BMSK                     0xf0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_CAPTURE_DELAY_SHFT                      0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_POS_BMSK                           0xc
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_TRIG_POS_SHFT                           0x2
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RSVD_BITS1_0_BMSK                       0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_0_RSVD_BITS1_0_SHFT                       0x0

#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR                             (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000010)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK                             0xffffffff
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUT(v)      \
        out_dword(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,v)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ADDR,m,v,HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_IN)
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BITS31_18_BMSK              0xfffc0000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BITS31_18_SHFT                    0x12
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_NEG_EN_BMSK             0x20000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_NEG_EN_SHFT                0x11
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_POS_EN_BMSK             0x10000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_SLOPE_POS_EN_SHFT                0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_MIN_EN_BMSK                    0x8000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_MIN_EN_SHFT                       0xf
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_MAX_EN_BMSK                    0x4000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_ALARM_MAX_EN_SHFT                       0xe
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_POWER_EN_BMSK                        0x2000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_POWER_EN_SHFT                           0xd
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FUNC_EN_BMSK                         0x1000
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_FUNC_EN_SHFT                            0xc
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLEAR_BMSK                            0x800
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_CLEAR_SHFT                              0xb
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BIT10_BMSK                       0x400
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BIT10_SHFT                         0xa
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_HW_CAPTURE_EN_BMSK                    0x200
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_HW_CAPTURE_EN_SHFT                      0x9
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SW_CAPTURE_BMSK                       0x100
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SW_CAPTURE_SHFT                         0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_TRIG_SEL_BMSK                          0xe0
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_TRIG_SEL_SHFT                           0x5
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_MODE_SEL_BMSK                          0x10
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_MODE_SEL_SHFT                           0x4
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BIT3_BMSK                          0x8
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_RSVD_BIT3_SHFT                          0x3
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_DELTA_CYC_BMSK                    0x7
#define HWIO_APCS_COMMON_APC1_VSENS_CONFIG_1_SLOPE_DELTA_CYC_SHFT                    0x0

#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR                               (APCS_COMMON_APSS_VS_REG_BASE      + 0x00000014)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK                                  0x3ffff
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_IN          \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, HWIO_APCS_COMMON_APC1_VSENS_STATUS_RMSK)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_COMMON_APC1_VSENS_STATUS_ADDR, m)
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_BMSK                        0x3fc00
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_DATA_SHFT                            0xa
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_BMSK                          0x3c0
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_VS_FSM_ST_SHFT                            0x6
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_BMSK                   0x20
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_COMPLETE_STS_SHFT                    0x5
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACTIVE_STS_BMSK                     0x10
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_FIFO_ACTIVE_STS_SHFT                      0x4
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_BMSK                            0x8
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MAX_SHFT                            0x3
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_BMSK                            0x4
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_ALARM_MIN_SHFT                            0x2
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_NEG_BMSK                            0x2
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_NEG_SHFT                            0x1
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_POS_BMSK                            0x1
#define HWIO_APCS_COMMON_APC1_VSENS_STATUS_SLOPE_POS_SHFT                            0x0


#endif /* __APCS_HWIO_H__ */
