#ifndef __APCS_ALIAS1_APSS_GLB_HWIO_H__
#define __APCS_ALIAS1_APSS_GLB_HWIO_H__
/*
===========================================================================
*/
/**
  @file apcs_alias1_apss_glb_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    APCS_ALIAS1_APSS_GLB

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.3/boot_images/core/systemdrivers/vsense/inc/msm8994/apcs_alias1_apss_glb_hwio.h#1 $
  $DateTime: 2015/12/02 08:25:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS1_APSS_GLB
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS1_APSS_GLB_REG_BASE                                                              (TERMINATOR_BASE      + 0x0000f000)

#define HWIO_APCS_ALIAS1_GLB_SECURE_ADDR                                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS1_GLB_SECURE_RMSK                                                                 0x3f
#define HWIO_APCS_ALIAS1_GLB_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR, HWIO_APCS_ALIAS1_GLB_SECURE_RMSK)
#define HWIO_APCS_ALIAS1_GLB_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_SECURE_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_SECURE_IN)
#define HWIO_APCS_ALIAS1_GLB_SECURE_CFG_BMSK                                                             0x20
#define HWIO_APCS_ALIAS1_GLB_SECURE_CFG_SHFT                                                              0x5
#define HWIO_APCS_ALIAS1_GLB_SECURE_VOTE_BMSK                                                            0x10
#define HWIO_APCS_ALIAS1_GLB_SECURE_VOTE_SHFT                                                             0x4
#define HWIO_APCS_ALIAS1_GLB_SECURE_ACP_BMSK                                                              0x8
#define HWIO_APCS_ALIAS1_GLB_SECURE_ACP_SHFT                                                              0x3
#define HWIO_APCS_ALIAS1_GLB_SECURE_TST_BMSK                                                              0x4
#define HWIO_APCS_ALIAS1_GLB_SECURE_TST_SHFT                                                              0x2
#define HWIO_APCS_ALIAS1_GLB_SECURE_PWR_BMSK                                                              0x2
#define HWIO_APCS_ALIAS1_GLB_SECURE_PWR_SHFT                                                              0x1
#define HWIO_APCS_ALIAS1_GLB_SECURE_STS_BMSK                                                              0x1
#define HWIO_APCS_ALIAS1_GLB_SECURE_STS_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RMSK                                                       0xf00007
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_ADDR,v)
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_TZ_SPARE_IPC_BMSK                                          0xf00000
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_TZ_SPARE_IPC_SHFT                                              0x14
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RPM_TZ_IPC_BMSK                                                 0x7
#define HWIO_APCS_ALIAS1_TZ_IPC_INTERRUPT_RPM_TZ_IPC_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RMSK                                                          0xffff07
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADDR,v)
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_SPARE_IPC_BMSK                                                0xf00000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_SPARE_IPC_SHFT                                                    0x14
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_WCN_IPC_BMSK                                                   0xf0000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_WCN_IPC_SHFT                                                      0x10
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_MSS_IPC_BMSK                                                    0xf000
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_MSS_IPC_SHFT                                                       0xc
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADSP_IPC_BMSK                                                    0xf00
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_ADSP_IPC_SHFT                                                      0x8
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RPM_HLOS_IPC_BMSK                                                  0x7
#define HWIO_APCS_ALIAS1_IPC_INTERRUPT_RPM_HLOS_IPC_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK                                                       0x7d001f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_IN)
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_BMSK                                             0x400000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_PRESETDBG_SHFT                                                 0x16
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L1_RAM_COLLAPSE_DIS_BMSK                                   0x3c0000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L1_RAM_COLLAPSE_DIS_SHFT                                       0x12
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_BMSK                                         0x10000
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_L2_RET_SLP_DIS_SHFT                                            0x10
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_BMSK                                                    0x1f
#define HWIO_APCS_ALIAS1_PWR_CTL_OVERRIDE_CLK_EN_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR                                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_RMSK                                                          0xf001f
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR, HWIO_APCS_ALIAS1_GLB_CLK_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_QGIC_WAKEUP_BMSK                                              0xf0000
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_QGIC_WAKEUP_SHFT                                                 0x10
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_STS_BMSK                                                         0x1f
#define HWIO_APCS_ALIAS1_GLB_CLK_STATUS_STS_SHFT                                                          0x0

#define HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR                                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK                                                           0x93ffbffe
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_L2_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_CLK_ON_BMSK                                                    0x80000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_CLK_ON_SHFT                                                          0x1f
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_BMSK                                               0x10000000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_PMIC_APC_ON_SHFT                                                     0x1c
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_BMSK                                                  0x3ff0000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CNT_SHFT                                                       0x10
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_BMSK                                             0x8000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_ARRAY_HS_CLAMP_SHFT                                                0xf
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_BMSK                                                 0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_REST_SHFT                                                    0xd
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_BMSK                                                     0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_SYS_RESET_SHFT                                                        0xc
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_BMSK                                                 0x800
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLEEP_STATE_SHFT                                                   0xb
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_BMSK                                                   0x200
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_EN_FEW_SHFT                                                     0x9
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_BMSK                                                    0x100
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_HS_CLAMP_SHFT                                                      0x8
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_WL_EN_CLK_BMSK                                                    0x80
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_WL_EN_CLK_SHFT                                                     0x7
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_WORDLINE_EN_BMSK                                                  0x40
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_WORDLINE_EN_SHFT                                                   0x6
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_CLK_EN_BMSK                                                       0x20
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_CLK_EN_SHFT                                                        0x5
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLP_NRET_N_BMSK                                                   0x10
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLP_NRET_N_SHFT                                                    0x4
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLP_RET_N_BMSK                                                     0x8
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_SLP_RET_N_SHFT                                                     0x3
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_BMSK                                                       0x4
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_L2_RST_DIS_SHFT                                                       0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_GATE_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS1_L2_PWR_CTL_GATE_CLK_SHFT                                                         0x1

#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000018)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK                                                          0x3e3c62
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, HWIO_APCS_ALIAS1_L2_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_BMSK                                            0x200000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_APC_SUPPLY_ON_SHFT                                                0x15
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_BMSK                                              0x100000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_SAW_SLP_ACK_SHFT                                                  0x14
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_BMSK                                             0x80000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_PWRUP_REQ_SHFT                                                0x13
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_BMSK                                               0x40000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLK_IDLE_SHFT                                                  0x12
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_BMSK                                               0x20000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_SLV_IDLE_SHFT                                                  0x11
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_DORMANT_C1_BMSK                                  0x2000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_DORMANT_C1_SHFT                                     0xd
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_RET_C1_BMSK                                      0x1000
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_RET_C1_SHFT                                         0xc
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_IDLE_C1_BMSK                                      0x800
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_IDLE_C1_SHFT                                        0xb
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_ON_C1_BMSK                                        0x400
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HW_EVENT_L2_ON_C1_SHFT                                          0xa
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_BMSK                                                  0x40
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_DBG_RST_STS_SHFT                                                   0x6
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_BMSK                                                0x20
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_HS_RST_STS_SHFT                                                 0x5
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_L2_PWR_STATUS_L2_CLMP_STS_SHFT                                                   0x1

#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000001c)
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_RMSK                                                         0xd0037335
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_DIAG_ADDR, HWIO_APCS_ALIAS1_GLB_CLK_DIAG_RMSK)
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_CLK_DIAG_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_CLK_DIAG_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_CLK_DIAG_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_CLK_DIAG_IN)
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_CDIV_BMSK                                                    0xc0000000
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_CDIV_SHFT                                                          0x1e
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DROOP_CLK_SEL_BMSK                                           0x10000000
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DROOP_CLK_SEL_SHFT                                                 0x1c
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL3_SEL_BMSK                                           0x30000
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL3_SEL_SHFT                                              0x10
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL2_SEL_BMSK                                            0x7000
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL2_SEL_SHFT                                               0xc
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL1_SEL_BMSK                                             0x300
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_DBG_CLK_LVL1_SEL_SHFT                                               0x8
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL3_SEL_BMSK                                             0x30
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL3_SEL_SHFT                                              0x4
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL2_SEL_BMSK                                              0x4
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL2_SEL_SHFT                                              0x2
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL1_SEL_BMSK                                              0x1
#define HWIO_APCS_ALIAS1_GLB_CLK_DIAG_PLL_TEST_LVL1_SEL_SHFT                                              0x0

#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000020)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_RMSK                                                       0xff003f
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_A53_TEST_BUS_SEL_BMSK                                      0xff0000
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_A53_TEST_BUS_SEL_SHFT                                          0x10
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_EN_BMSK                                                        0x20
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_EN_SHFT                                                         0x5
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_SEL_BMSK                                                       0x1f
#define HWIO_APCS_ALIAS1_GLB_TEST_BUS_SEL_SEL_SHFT                                                        0x0

#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR                                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000024)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_RMSK                                                                 0x1
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_DBG_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_DBG_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_DBG_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_REQ_BMSK                                                             0x1
#define HWIO_APCS_ALIAS1_DBG_PWR_CTL_REQ_SHFT                                                             0x0

#define HWIO_APCS_ALIAS1_GLB_SPARE_ADDR                                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000028)
#define HWIO_APCS_ALIAS1_GLB_SPARE_RMSK                                                            0xc00000ff
#define HWIO_APCS_ALIAS1_GLB_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR, HWIO_APCS_ALIAS1_GLB_SPARE_RMSK)
#define HWIO_APCS_ALIAS1_GLB_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS1_GLB_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS1_GLB_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GLB_SPARE_ADDR,m,v,HWIO_APCS_ALIAS1_GLB_SPARE_IN)
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_SO_IMPL_AXI10_CTRL_BMSK                                 0x80000000
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_SO_IMPL_AXI10_CTRL_SHFT                                       0x1f
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_CLK_GATE_FIX_BMSK                                       0x40000000
#define HWIO_APCS_ALIAS1_GLB_SPARE_DISABLE_CLK_GATE_FIX_SHFT                                             0x1e
#define HWIO_APCS_ALIAS1_GLB_SPARE_BITS_BMSK                                                             0xff
#define HWIO_APCS_ALIAS1_GLB_SPARE_BITS_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000002c)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_RMSK                                                               0xff
#define HWIO_APCS_ALIAS1_SPARE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR, HWIO_APCS_ALIAS1_SPARE_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPARE_STATUS_BITS_BMSK                                                          0xff
#define HWIO_APCS_ALIAS1_SPARE_STATUS_BITS_SHFT                                                           0x0

#define HWIO_APCS_ALIAS1_IDR_ADDR                                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS1_IDR_RMSK                                                                   0xdffffff
#define HWIO_APCS_ALIAS1_IDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_IDR_ADDR, HWIO_APCS_ALIAS1_IDR_RMSK)
#define HWIO_APCS_ALIAS1_IDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_IDR_ADDR, m)
#define HWIO_APCS_ALIAS1_IDR_CFG_APCPLL_BMSK                                                        0xc000000
#define HWIO_APCS_ALIAS1_IDR_CFG_APCPLL_SHFT                                                             0x1a
#define HWIO_APCS_ALIAS1_IDR_QDSSGEN_BMSK                                                           0x1000000
#define HWIO_APCS_ALIAS1_IDR_QDSSGEN_SHFT                                                                0x18
#define HWIO_APCS_ALIAS1_IDR_CFG_HBID_BMSK                                                           0xe00000
#define HWIO_APCS_ALIAS1_IDR_CFG_HBID_SHFT                                                               0x15
#define HWIO_APCS_ALIAS1_IDR_CFG_HPID_BMSK                                                           0x1f0000
#define HWIO_APCS_ALIAS1_IDR_CFG_HPID_SHFT                                                               0x10
#define HWIO_APCS_ALIAS1_IDR_L2_SIZE_BMSK                                                              0xf000
#define HWIO_APCS_ALIAS1_IDR_L2_SIZE_SHFT                                                                 0xc
#define HWIO_APCS_ALIAS1_IDR_NUM_FRAME_BMSK                                                             0xf00
#define HWIO_APCS_ALIAS1_IDR_NUM_FRAME_SHFT                                                               0x8
#define HWIO_APCS_ALIAS1_IDR_VARIANT_BMSK                                                                0xf0
#define HWIO_APCS_ALIAS1_IDR_VARIANT_SHFT                                                                 0x4
#define HWIO_APCS_ALIAS1_IDR_NUM_CPU_BMSK                                                                 0xf
#define HWIO_APCS_ALIAS1_IDR_NUM_CPU_SHFT                                                                 0x0

#define HWIO_APCS_ALIAS1_XO_CBCR_ADDR                                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS1_XO_CBCR_RMSK                                                              0x80000003
#define HWIO_APCS_ALIAS1_XO_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_XO_CBCR_ADDR, HWIO_APCS_ALIAS1_XO_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_XO_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_XO_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_XO_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_XO_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_XO_CBCR_IN)
#define HWIO_APCS_ALIAS1_XO_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_APCS_ALIAS1_XO_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_APCS_ALIAS1_XO_CBCR_HW_CTL_BMSK                                                              0x2
#define HWIO_APCS_ALIAS1_XO_CBCR_HW_CTL_SHFT                                                              0x1
#define HWIO_APCS_ALIAS1_XO_CBCR_SW_CTL_BMSK                                                              0x1
#define HWIO_APCS_ALIAS1_XO_CBCR_SW_CTL_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR                                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000038)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_RMSK                                                           0x80000003
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR, HWIO_APCS_ALIAS1_SLEEP_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SLEEP_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_SLEEP_CBCR_IN)
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_HW_CTL_BMSK                                                           0x2
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_HW_CTL_SHFT                                                           0x1
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_SW_CTL_BMSK                                                           0x1
#define HWIO_APCS_ALIAS1_SLEEP_CBCR_SW_CTL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS1_CFG_GFM_ADDR                                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS1_CFG_GFM_RMSK                                                              0x8000003f
#define HWIO_APCS_ALIAS1_CFG_GFM_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CFG_GFM_ADDR, HWIO_APCS_ALIAS1_CFG_GFM_RMSK)
#define HWIO_APCS_ALIAS1_CFG_GFM_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CFG_GFM_ADDR, m)
#define HWIO_APCS_ALIAS1_CFG_GFM_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CFG_GFM_ADDR,v)
#define HWIO_APCS_ALIAS1_CFG_GFM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CFG_GFM_ADDR,m,v,HWIO_APCS_ALIAS1_CFG_GFM_IN)
#define HWIO_APCS_ALIAS1_CFG_GFM_CLUST_CLK_ON_BMSK                                                 0x80000000
#define HWIO_APCS_ALIAS1_CFG_GFM_CLUST_CLK_ON_SHFT                                                       0x1f
#define HWIO_APCS_ALIAS1_CFG_GFM_PRI_PLL_MAIN_DIV_BMSK                                                   0x20
#define HWIO_APCS_ALIAS1_CFG_GFM_PRI_PLL_MAIN_DIV_SHFT                                                    0x5
#define HWIO_APCS_ALIAS1_CFG_GFM_GFMUX_SRC_SEL_HF_BMSK                                                   0x18
#define HWIO_APCS_ALIAS1_CFG_GFM_GFMUX_SRC_SEL_HF_SHFT                                                    0x3
#define HWIO_APCS_ALIAS1_CFG_GFM_GFMUX_SRC_SEL_LF_BMSK                                                    0x6
#define HWIO_APCS_ALIAS1_CFG_GFM_GFMUX_SRC_SEL_LF_SHFT                                                    0x1
#define HWIO_APCS_ALIAS1_CFG_GFM_CLK_GATE_BMSK                                                            0x1
#define HWIO_APCS_ALIAS1_CFG_GFM_CLK_GATE_SHFT                                                            0x0

#define HWIO_APCS_ALIAS1_CORE_CBCR_ADDR                                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS1_CORE_CBCR_RMSK                                                                   0x3
#define HWIO_APCS_ALIAS1_CORE_CBCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, HWIO_APCS_ALIAS1_CORE_CBCR_RMSK)
#define HWIO_APCS_ALIAS1_CORE_CBCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,v)
#define HWIO_APCS_ALIAS1_CORE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CORE_CBCR_ADDR,m,v,HWIO_APCS_ALIAS1_CORE_CBCR_IN)
#define HWIO_APCS_ALIAS1_CORE_CBCR_LEAF_CLK_BYPASS_BMSK                                                   0x2
#define HWIO_APCS_ALIAS1_CORE_CBCR_LEAF_CLK_BYPASS_SHFT                                                   0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_LEAF_CLK_EN_BMSK                                                       0x1
#define HWIO_APCS_ALIAS1_CORE_CBCR_LEAF_CLK_EN_SHFT                                                       0x0

#define HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR                                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS1_RBCPR_BCR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR, HWIO_APCS_ALIAS1_RBCPR_BCR_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_BCR_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_BCR_IN)
#define HWIO_APCS_ALIAS1_RBCPR_BCR_BLK_ARES_BMSK                                                          0x1
#define HWIO_APCS_ALIAS1_RBCPR_BCR_BLK_ARES_SHFT                                                          0x0

#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RMSK                                                        0x8000001f
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_ADDR, HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_IN)
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_ON_BMSK                                      0x80000000
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_ON_SHFT                                            0x1f
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_SECONDARY_DIV_BMSK                                 0x18
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_SECONDARY_DIV_SHFT                                  0x3
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_PRIMARY_DIV_BMSK                                    0x6
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_RBCPR_FAST_CLK_PRIMARY_DIV_SHFT                                    0x1
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_CLK_SEL_BMSK                                                       0x1
#define HWIO_APCS_ALIAS1_RBCPR_CLK_SEL_CLK_SEL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR                                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000068)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_RMSK                                                                  0x3
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR, HWIO_APCS_ALIAS1_RBCPR_PMIC_RMSK)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR, m)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR,v)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RBCPR_PMIC_ADDR,m,v,HWIO_APCS_ALIAS1_RBCPR_PMIC_IN)
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_ADDRIDX_BMSK                                                     0x2
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_ADDRIDX_SHFT                                                     0x1
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_SIZE_BMSK                                                        0x1
#define HWIO_APCS_ALIAS1_RBCPR_PMIC_PMIC_SIZE_SHFT                                                        0x0

#define HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR                                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000006c)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_RMSK                                                          0xffff0000
#define HWIO_APCS_ALIAS1_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS1_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR,v)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_A53_CFG_STS_ADDR,m,v,HWIO_APCS_ALIAS1_A53_CFG_STS_IN)
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF1_BMSK                                            0xff000000
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF1_SHFT                                                  0x18
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF2_BMSK                                              0xff0000
#define HWIO_APCS_ALIAS1_A53_CFG_STS_CLUSTERIDAFF2_SHFT                                                  0x10

#define HWIO_APCS_ALIAS1_QSB_LP_ADDR                                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS1_QSB_LP_RMSK                                                                     0x7f
#define HWIO_APCS_ALIAS1_QSB_LP_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QSB_LP_ADDR, HWIO_APCS_ALIAS1_QSB_LP_RMSK)
#define HWIO_APCS_ALIAS1_QSB_LP_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QSB_LP_ADDR, m)
#define HWIO_APCS_ALIAS1_QSB_LP_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QSB_LP_ADDR,v)
#define HWIO_APCS_ALIAS1_QSB_LP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QSB_LP_ADDR,m,v,HWIO_APCS_ALIAS1_QSB_LP_IN)
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_CNTR_BMSK                                                         0x7e
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_CNTR_SHFT                                                          0x1
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_OVERRIDE_BIT_BMSK                                                  0x1
#define HWIO_APCS_ALIAS1_QSB_LP_QSB_LP_OVERRIDE_BIT_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_RVBARADDR_LO_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_RMSK                                                         0xfffffffc
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RVBARADDR_LO_ADDR, HWIO_APCS_ALIAS1_RVBARADDR_LO_RMSK)
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RVBARADDR_LO_ADDR, m)
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RVBARADDR_LO_ADDR,v)
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RVBARADDR_LO_ADDR,m,v,HWIO_APCS_ALIAS1_RVBARADDR_LO_IN)
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_RVBARADDR_LOWER_BITS_BMSK                                    0xfffffffc
#define HWIO_APCS_ALIAS1_RVBARADDR_LO_RVBARADDR_LOWER_BITS_SHFT                                           0x2

#define HWIO_APCS_ALIAS1_RVBARADDR_HI_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_RMSK                                                              0xfff
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_RVBARADDR_HI_ADDR, HWIO_APCS_ALIAS1_RVBARADDR_HI_RMSK)
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_RVBARADDR_HI_ADDR, m)
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_RVBARADDR_HI_ADDR,v)
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_RVBARADDR_HI_ADDR,m,v,HWIO_APCS_ALIAS1_RVBARADDR_HI_IN)
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_RVBARADDR_UPPER_BITS_BMSK                                         0xfff
#define HWIO_APCS_ALIAS1_RVBARADDR_HI_RVBARADDR_UPPER_BITS_SHFT                                           0x0

#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_RMSK                                                              0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_ADDR, HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_RMSK)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_ADDR, m)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_ADDR,v)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_ADDR,m,v,HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_IN)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_MEM_ACC_TYPE1_BITS_BMSK                                           0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE1_MEM_ACC_TYPE1_BITS_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_RMSK                                                              0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_ADDR, HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_RMSK)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_ADDR, m)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_ADDR,v)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_ADDR,m,v,HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_IN)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_MEM_ACC_TYPE2_BITS_BMSK                                           0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE2_MEM_ACC_TYPE2_BITS_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_RMSK                                                              0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_ADDR, HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_RMSK)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_ADDR, m)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_ADDR,v)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_ADDR,m,v,HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_IN)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_MEM_ACC_TYPE3_BITS_BMSK                                           0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE3_MEM_ACC_TYPE3_BITS_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_RMSK                                                              0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_ADDR, HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_RMSK)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_ADDR, m)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_ADDR,v)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_ADDR,m,v,HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_IN)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_MEM_ACC_TYPE4_BITS_BMSK                                           0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE4_MEM_ACC_TYPE4_BITS_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_RMSK                                                              0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_ADDR, HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_RMSK)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_ADDR, m)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_ADDR,v)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_ADDR,m,v,HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_IN)
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_MEM_ACC_TYPE5_BITS_BMSK                                           0xff
#define HWIO_APCS_ALIAS1_MEM_ACC_TYPE5_MEM_ACC_TYPE5_BITS_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_ADDR                                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000270)
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_RMSK                                                           0x1
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_ADDR, HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_RMSK)
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_IN)
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_SYSBARDISABLE_BMSK                                             0x1
#define HWIO_APCS_ALIAS1_SYSBARDISABLE_CFG_SYSBARDISABLE_SHFT                                             0x0

#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000274)
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_RMSK                                                          0x1
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_ADDR, HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_IN)
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_BROADCASTINNER_BMSK                                           0x1
#define HWIO_APCS_ALIAS1_BROADCASTINNER_CFG_BROADCASTINNER_SHFT                                           0x0

#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000278)
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_RMSK                                                          0x1
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_ADDR, HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_IN)
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_BROADCASTOUTER_BMSK                                           0x1
#define HWIO_APCS_ALIAS1_BROADCASTOUTER_CFG_BROADCASTOUTER_SHFT                                           0x0

#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000104)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR, HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_BOOTFSM_STATUS_BMSK                                   0xffffffff
#define HWIO_APCS_ALIAS1_APPS_BOOTFSM_STATUS_BOOTFSM_STATUS_SHFT                                          0x0

#define HWIO_APCS_ALIAS1_VERSION_ADDR                                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000fd0)
#define HWIO_APCS_ALIAS1_VERSION_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS1_VERSION_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_VERSION_ADDR, HWIO_APCS_ALIAS1_VERSION_RMSK)
#define HWIO_APCS_ALIAS1_VERSION_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_VERSION_ADDR, m)
#define HWIO_APCS_ALIAS1_VERSION_MAJOR_BMSK                                                        0xf0000000
#define HWIO_APCS_ALIAS1_VERSION_MAJOR_SHFT                                                              0x1c
#define HWIO_APCS_ALIAS1_VERSION_MINOR_BMSK                                                         0xfff0000
#define HWIO_APCS_ALIAS1_VERSION_MINOR_SHFT                                                              0x10
#define HWIO_APCS_ALIAS1_VERSION_STEP_BMSK                                                             0xffff
#define HWIO_APCS_ALIAS1_VERSION_STEP_SHFT                                                                0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR                                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000200)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RMSK                                                       0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                     0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                           0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                      0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                            0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                              0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000204)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RMSK                                                     0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                   0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                         0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                    0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                          0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                            0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR                                               (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000208)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RMSK                                               0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                     0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                           0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                      0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                            0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                              0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                   0x0

#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000020c)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RMSK                                                0xc0ffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                       0x80000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                             0x1f
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                        0x40000000
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                              0x1e
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                     0x0

#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_ADDR                                                       (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000214)
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_RMSK                                                              0x3
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_ADDR, HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_RMSK)
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_ADDR, m)
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_ADDR,v)
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_ADDR,m,v,HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_IN)
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_SEL_BMSK                                                          0x3
#define HWIO_APCS_ALIAS1_CPUSPM_RPU_MUX_SEL_SHFT                                                          0x0

#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000218)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_RMSK                                                               0x7
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ADDR, HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_RMSK)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_IN)
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_L2FLUSHREQ_BMSK                                                    0x4
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_L2FLUSHREQ_SHFT                                                    0x2
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_AINACTS_BMSK                                                       0x2
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_AINACTS_SHFT                                                       0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ACINACTM_BMSK                                                      0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_CTRL_ACINACTM_SHFT                                                      0x0

#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000234)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_RMSK                                                                0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR, HWIO_APCS_ALIAS1_L2_FLUSH_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_FLUSH_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_L2FLUSHDONE_BMSK                                                    0x1
#define HWIO_APCS_ALIAS1_L2_FLUSH_STS_L2FLUSHDONE_SHFT                                                    0x0

#define HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR                                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000220)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_RMSK                                                          0x80ffffff
#define HWIO_APCS_ALIAS1_AINACTS_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR, HWIO_APCS_ALIAS1_AINACTS_CNT_RMSK)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR, m)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR,v)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_AINACTS_CNT_ADDR,m,v,HWIO_APCS_ALIAS1_AINACTS_CNT_IN)
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_WIP_BMSK                                          0x80000000
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_WIP_SHFT                                                0x1f
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_VAL_BMSK                                            0xffffff
#define HWIO_APCS_ALIAS1_AINACTS_CNT_AINACTS_CNT_VAL_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_ACP_DBG_ADDR                                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000224)
#define HWIO_APCS_ALIAS1_ACP_DBG_RMSK                                                                    0xf3
#define HWIO_APCS_ALIAS1_ACP_DBG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_DBG_ADDR, HWIO_APCS_ALIAS1_ACP_DBG_RMSK)
#define HWIO_APCS_ALIAS1_ACP_DBG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_DBG_ADDR, m)
#define HWIO_APCS_ALIAS1_ACP_DBG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_ACP_DBG_ADDR,v)
#define HWIO_APCS_ALIAS1_ACP_DBG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_ACP_DBG_ADDR,m,v,HWIO_APCS_ALIAS1_ACP_DBG_IN)
#define HWIO_APCS_ALIAS1_ACP_DBG_AWUSERS_OVERRIDE_BMSK                                                   0xc0
#define HWIO_APCS_ALIAS1_ACP_DBG_AWUSERS_OVERRIDE_SHFT                                                    0x6
#define HWIO_APCS_ALIAS1_ACP_DBG_ARUSERS_OVERRIDE_BMSK                                                   0x30
#define HWIO_APCS_ALIAS1_ACP_DBG_ARUSERS_OVERRIDE_SHFT                                                    0x4
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_VAL_BMSK                                                         0x2
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_VAL_SHFT                                                         0x1
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_OVERRIDE_BMSK                                                    0x1
#define HWIO_APCS_ALIAS1_ACP_DBG_AINACTS_OVERRIDE_SHFT                                                    0x0

#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000228)
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ADDR, HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_QSB_CLK_ON_REQ_BMSK                                      0x80000000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_QSB_CLK_ON_REQ_SHFT                                            0x1f
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_WSTARTS_BMSK                                             0x40000000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_WSTARTS_SHFT                                                   0x1e
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_WR_REQ_CNT_BMSK                                          0x3f000000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_WR_REQ_CNT_SHFT                                                0x18
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ALL_CORE_IN_STBL_LPM_BMSK                                  0x800000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ALL_CORE_IN_STBL_LPM_SHFT                                      0x17
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_WAIT_REQ_BMSK                                        0x400000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_WAIT_REQ_SHFT                                            0x16
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_TIMER_TRIG_ACK_BMSK                                  0x200000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_TIMER_TRIG_ACK_SHFT                                      0x15
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_TIMER_TRIG_BMSK                                      0x100000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_TIMER_TRIG_SHFT                                          0x14
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_CBC_EN_OVERRIDE_BMSK                                        0x80000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_CBC_EN_OVERRIDE_SHFT                                           0x13
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_MASK_ACP_XFERS_BMSK                                         0x40000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_MASK_ACP_XFERS_SHFT                                            0x12
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_STATE_BMSK                                                  0x30000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_STATE_SHFT                                                     0x10
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_PC_MODE_QSB_BMSK                                       0x8000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2SPM_PC_MODE_QSB_SHFT                                          0xf
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_AINACTS_CNT_WR_ACK_BMSK                                      0x4000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_AINACTS_CNT_WR_ACK_SHFT                                         0xe
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ACP_STALL_BMSK                                               0x2000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_ACP_STALL_SHFT                                                  0xd
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2FLUSHDONE_BMSK                                             0x1000
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2FLUSHDONE_SHFT                                                0xc
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2FLUSHREQ_BMSK                                               0x800
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_L2FLUSHREQ_SHFT                                                 0xb
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_EXP_BMSK                                                0x400
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_EXP_SHFT                                                  0xa
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_EN_BMSK                                                 0x200
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_EN_SHFT                                                   0x9
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_TRIG_ACK_QSB_BMSK                                       0x100
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_TRIG_ACK_QSB_SHFT                                         0x8
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_TRIG_BMSK                                                0x80
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_TIMER_TRIG_SHFT                                                 0x7
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_AINACTS_BMSK                                                   0x40
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_AINACTS_SHFT                                                    0x6
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_XFER_CNT_BMSK                                                  0x3f
#define HWIO_APCS_ALIAS1_ACP_L2_PM_STATUS_XFER_CNT_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR                                                    (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000244)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_RMSK                                                    0xffffffff
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR,v)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_ADDR,m,v,HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_IN)
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACTIVE_BMSK                                        0xf0000000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACTIVE_SHFT                                              0x1c
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQDENY_BMSK                                           0xf000000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQDENY_SHFT                                                0x18
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACCEPTN_BMSK                                         0xf00000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQACCEPTN_SHFT                                             0x14
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQREQN_BMSK                                             0xf0000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_NEONQREQN_SHFT                                                0x10
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACTIVE_BMSK                                             0xf000
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACTIVE_SHFT                                                0xc
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQDENY_BMSK                                                0xf00
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQDENY_SHFT                                                  0x8
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACCEPTN_BMSK                                              0xf0
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQACCEPTN_SHFT                                               0x4
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQREQN_BMSK                                                  0xf
#define HWIO_APCS_ALIAS1_QCHANNEL_CPU_NEON_CPUQREQN_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR                                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000248)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_RMSK                                                                 0xf
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_L2_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR,v)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QCHANNEL_L2_ADDR,m,v,HWIO_APCS_ALIAS1_QCHANNEL_L2_IN)
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACTIVE_BMSK                                                       0x8
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACTIVE_SHFT                                                       0x3
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QDENY_BMSK                                                         0x4
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QDENY_SHFT                                                         0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACCEPTN_BMSK                                                      0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QACCEPTN_SHFT                                                      0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QREQN_BMSK                                                         0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_L2_L2QREQN_SHFT                                                         0x0

#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000022c)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_RMSK                                                              0x110
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_MISC_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MISC_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_MISC_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_CLR_EX_MON_REQ_BMSK                                               0x100
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_CLR_EX_MON_REQ_SHFT                                                 0x8
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ACP_STALL_REQ_BMSK                                                 0x10
#define HWIO_APCS_ALIAS1_MISC_PWR_CTL_ACP_STALL_REQ_SHFT                                                  0x4

#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000230)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_RMSK                                                               0x10
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR, HWIO_APCS_ALIAS1_MISC_PWR_STS_RMSK)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ACP_STALL_ACK_BMSK                                                 0x10
#define HWIO_APCS_ALIAS1_MISC_PWR_STS_ACP_STALL_ACK_SHFT                                                  0x4

#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_ADDR                                                         (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000027c)
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_RMSK                                                                0x1
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_ACK_ADDR, HWIO_APCS_ALIAS1_MISC_PWR_ACK_RMSK)
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MISC_PWR_ACK_ADDR, m)
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MISC_PWR_ACK_ADDR,v)
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MISC_PWR_ACK_ADDR,m,v,HWIO_APCS_ALIAS1_MISC_PWR_ACK_IN)
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_CLR_EX_MON_ACK_BMSK                                                 0x1
#define HWIO_APCS_ALIAS1_MISC_PWR_ACK_CLR_EX_MON_ACK_SHFT                                                 0x0

#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_ADDR                                            (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000280)
#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_RMSK                                                   0x1
#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_ADDR, HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_CLREXMONACK_ACTIVE_STATUS_BIT_BMSK                     0x1
#define HWIO_APCS_ALIAS1_CLREXMONACK_ACTIVE_STATUS_CLREXMONACK_ACTIVE_STATUS_BIT_SHFT                     0x0

#define HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR                                                        (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000240)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_RMSK                                                               0xf
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR, HWIO_APCS_ALIAS1_CORE_HS_STATE_RMSK)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HS_STATE_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_HS_STATE_BMSK                                                      0xf
#define HWIO_APCS_ALIAS1_CORE_HS_STATE_HS_STATE_SHFT                                                      0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000250)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_RMSK                                                      0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_EN_BMSK                                                   0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EN_EN_SHFT                                                    0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000254)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_RMSK                                                         0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                               0xff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR                                              (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000025c)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_RMSK                                                0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                             0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR                                                 (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000258)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_RMSK                                                   0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                       0xffffff
#define HWIO_APCS_ALIAS1_L2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                            0x0

#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR                                                     (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000260)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_RMSK                                                           0xff
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_EVENT_VAL_BMSK                                                 0xff
#define HWIO_APCS_ALIAS1_L2_SPM_EVENT_STS_EVENT_VAL_SHFT                                                  0x0

#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000264)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_RMSK                                                       0x7fff
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS1_L2_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                              0x0

#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR                                                  (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000210)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_RMSK                                                         0x7
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                         0x4
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                         0x2
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                  0x2
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                  0x1
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                              0x1
#define HWIO_APCS_ALIAS1_L2_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                              0x0

#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_ADDR                                                (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000268)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RMSK                                                0xffffffff
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_L2_SPM_WAKEUP_COUNTER_VALUE_BMSK                    0xffffffff
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_L2_SPM_WAKEUP_COUNTER_VALUE_SHFT                           0x0

#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_ADDR                                          (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000026c)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_RMSK                                                 0x1
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_ADDR, HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_RMSK)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_IN)
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_L2_SPM_WAKEUP_COUNTER_RESET_BIT_BMSK                 0x1
#define HWIO_APCS_ALIAS1_L2_SPM_WAKEUP_COUNTER_RESET_L2_SPM_WAKEUP_COUNTER_RESET_BIT_SHFT                 0x0

#define HWIO_APCS_ALIAS1_L1_RST_DIS_ADDR                                                           (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000284)
#define HWIO_APCS_ALIAS1_L1_RST_DIS_RMSK                                                                  0x1
#define HWIO_APCS_ALIAS1_L1_RST_DIS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_RST_DIS_ADDR, HWIO_APCS_ALIAS1_L1_RST_DIS_RMSK)
#define HWIO_APCS_ALIAS1_L1_RST_DIS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_RST_DIS_ADDR, m)
#define HWIO_APCS_ALIAS1_L1_RST_DIS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L1_RST_DIS_ADDR,v)
#define HWIO_APCS_ALIAS1_L1_RST_DIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L1_RST_DIS_ADDR,m,v,HWIO_APCS_ALIAS1_L1_RST_DIS_IN)
#define HWIO_APCS_ALIAS1_L1_RST_DIS_L1_RST_DIS_CSR_BMSK                                                   0x1
#define HWIO_APCS_ALIAS1_L1_RST_DIS_L1_RST_DIS_CSR_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x00000288)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_RMSK                                                         0x3f
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR, HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_IN)
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_DIV_BMSK                                                 0x3c
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_DIV_SHFT                                                  0x2
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_SEL_BMSK                                                  0x2
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_SEL_SHFT                                                  0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_EN_BMSK                                                   0x1
#define HWIO_APCS_ALIAS1_L1_SLP_SEQ_CLK_CTL_CLK_EN_SHFT                                                   0x0

#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR                                                   (APCS_ALIAS1_APSS_GLB_REG_BASE      + 0x0000028c)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_RMSK                                                         0x3f
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR, HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_RMSK)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_IN)
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_DIV_BMSK                                                 0x3c
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_DIV_SHFT                                                  0x2
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_SEL_BMSK                                                  0x2
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_SEL_SHFT                                                  0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_EN_BMSK                                                   0x1
#define HWIO_APCS_ALIAS1_L2_SLP_SEQ_CLK_CTL_CLK_EN_SHFT                                                   0x0


#endif /* __APCS_ALIAS1_APSS_GLB_HWIO_H__ */
