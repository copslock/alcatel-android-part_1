


ARM Macro Assembler    Page 1 


    1 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
    2 00000000         ;
    3 00000000         ;                              SBL1 DDR Debug
    4 00000000         ;
    5 00000000         ; GENERAL DESCRIPTION
    6 00000000         ;   This file bootstraps the processor. The Start-up
    7 00000000         ;   (SBL1_ddr_debug) performs the following functions:
    8 00000000         ;
    9 00000000         ;      - Set up the hardware to continue boot process.
   10 00000000         ;      - Initialize DDR memory
   11 00000000         ;      - Copies SBL1 to CODERAM
   12 00000000         ;      - Sets up stack in WDOG reset path .
   13 00000000         ;      - Jumps to OCIMEM to execute WDOG reset path
   14 00000000         ;
   15 00000000         ;   The SBL1_ddr_debug is written to perform the above f
                       unctions with optimal speed.
   16 00000000         ;   It also attempts to minimize the execution time and 
                       hence reduce boot time.
   17 00000000         ;
   18 00000000         ; Copyright 2013 by Qualcomm Technologies, Incorporated.
                       All Rights Reserved.
   19 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   20 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   21 00000000         ;
   22 00000000         ;                           EDIT HISTORY FOR FILE
   23 00000000         ;
   24 00000000         ; This section contains comments describing changes made
                        to the module.
   25 00000000         ; Notice that changes are listed in reverse chronologica
                       l order.
   26 00000000         ;
   27 00000000         ; $Header: 
   28 00000000         ;
   29 00000000         ; when       who     what, where, why
   30 00000000         ; --------   ---     -----------------------------------
                       ---------------------
   31 00000000         ; 07/20/15   yps     Porting code to 8952 platform.
   32 00000000         ; 07/14/14   yps     Added sbl1_external_abort_enable fu
                       ntion for DDI build
   33 00000000         ; 05/14/14   yps     Porting code to 8916 platform.
   34 00000000         ; 09/13/13   sl      Cleaned up exception handlers.
   35 00000000         ; 07/17/13   sr      Initial Version.
   36 00000000         ;*====*====*====*====*====*====*====*====*====*====*====
                       *====*====*====*====*
   37 00000000         ;=======================================================
                       =====================
   38 00000000         ;
   39 00000000         ;                            MODULE INCLUDES
   40 00000000         ;
   41 00000000         ;=======================================================
                       =====================
   42 00000000         ;=======================================================
                       =====================
   43 00000000         ;
   44 00000000         ;                             MODULE DEFINES
   45 00000000         ;



ARM Macro Assembler    Page 2 


   46 00000000         ;=======================================================
                       =====================
   47 00000000         ;
   48 00000000 00000013 
                       Mode_SVC
                               EQU              0x13
   49 00000000 00000017 
                       Mode_ABT
                               EQU              0x17
   50 00000000 0000001B 
                       Mode_UND
                               EQU              0x1b
   51 00000000 00000010 
                       Mode_USR
                               EQU              0x10
   52 00000000 00000011 
                       Mode_FIQ
                               EQU              0x11
   53 00000000 00000012 
                       Mode_IRQ
                               EQU              0x12
   54 00000000 0000001F 
                       Mode_SYS
                               EQU              0x1F
   55 00000000 00000080 
                       I_Bit   EQU              0x80
   56 00000000 00000040 
                       F_Bit   EQU              0x40
   57 00000000         ;=======================================================
                       =====================
   58 00000000         ; MACRO mdsb
   59 00000000         ;
   60 00000000         ; ARGS
   61 00000000         ;   NONE
   62 00000000         ;
   63 00000000         ; DESCRIPTION
   64 00000000         ;   Performs a data synchronization barrier, either usin
                       g the ARMv7 instruction
   65 00000000         ;   or the legacy coprocessor instruction.
   66 00000000         ;
   67 00000000         ; NOTES
   68 00000000         ;   For reference see ARM DDI 0406A-03 section A3.8.3.
   69 00000000         ;=======================================================
                       =====================
   70 00000000                 MACRO
   71 00000000                 mdsb
   72 00000000                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   73 00000000                 dsb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi
                                                            ons
   74 00000000                 ELSE
   75 00000000                 IF               {CONFIG} = 32
   76 00000000                 DCI              0xF57FF040 :OR: SY 
                                                            ; ARMv7 A1 Opcode
   77 00000000                 ELSE
   78 00000000                 DCI              0xF3BF8F40 :OR: SY 
                                                            ; ARMv7 T1 Opcode
   79 00000000                 ENDIF



ARM Macro Assembler    Page 3 


   80 00000000         ;mcr    p15, 0, r0, c7, c10, 4  ; Legacy Data Write Barr
                       ier
   81 00000000                 ENDIF
   82 00000000                 MEND
   83 00000000         ;=======================================================
                       =====================
   84 00000000         ; MACRO misb
   85 00000000         ;
   86 00000000         ; ARGS
   87 00000000         ;   NONE
   88 00000000         ;
   89 00000000         ; DESCRIPTION
   90 00000000         ;   Performs an instruction synchronization barrier, eit
                       her using the ARMv7
   91 00000000         ;   instruction or the legacy coprocessor instruction.
   92 00000000         ;
   93 00000000         ; NOTES
   94 00000000         ;   For reference see ARM DDI 0406A-03 section A3.8.3.
   95 00000000         ;=======================================================
                       =====================
   96 00000000                 MACRO
   97 00000000                 misb
   98 00000000                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   99 00000000                 isb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi
                                                            ons
  100 00000000                 ELSE
  101 00000000                 IF               {CONFIG} = 32
  102 00000000                 DCI              0xF57FF060 :OR: SY 
                                                            ; ARMv7 A1 Opcode
  103 00000000                 ELSE
  104 00000000                 DCI              0xF3BF8F60 :OR: SY 
                                                            ; ARMv7 T1 Opcode
  105 00000000                 ENDIF
  106 00000000         ;mcr    p15, 0, r0, c7, c5, 4   ; Legacy Pre-Fetch Flush
                       
  107 00000000                 ENDIF
  108 00000000                 MEND
  109 00000000         ;=======================================================
                       =====================
  110 00000000         ;
  111 00000000         ;                             MODULE IMPORTS
  112 00000000         ;
  113 00000000         ;=======================================================
                       =====================
  114 00000000         ; Import the external symbols that are referenced in thi
                       s module.
  115 00000000                 IMPORT           ddr_debug_init
  116 00000000                 IMPORT           sbl1_wdogpath_main_ctl
  117 00000000                 IMPORT           sbl1_ddr_debug_main_ctl
  118 00000000         ;=======================================================
                       =====================
  119 00000000         ;
  120 00000000         ;                             MODULE EXPORTS
  121 00000000         ;
  122 00000000         ;=======================================================
                       =====================
  123 00000000         ; Export the external symbols that are referenced in thi



ARM Macro Assembler    Page 4 


                       s module.
  124 00000000                 EXPORT           sbl_loop_here
  125 00000000                 EXPORT           ddr_debug_invalidate_tlb
  126 00000000         ; Export the symbols __main and _main to prevent the lin
                       ker from
  127 00000000         ; including the standard runtime library and startup rou
                       tine.
  128 00000000                 EXPORT           __main
  129 00000000                 EXPORT           _main
  130 00000000                 EXPORT           sbl_save_regs
  131 00000000                 EXPORT           sbl1_external_abort_enable
  132 00000000         ;=======================================================
                       =====================
  133 00000000         ;
  134 00000000         ;                             MODULE DATA AREA
  135 00000000         ;
  136 00000000         ;=======================================================
                       =====================
  137 00000000                 PRESERVE8
  138 00000000                 AREA             SBL1_INDIRECT_VECTOR_TABLE, COD
E, READONLY, ALIGN=4
  139 00000000                 CODE32
  140 00000000         unused_reset_vector
  141 00000000 EAFFFFFE        B                0x00000000
  142 00000004         undefined_instruction_vector
  143 00000004 EAFFFFFE        B                loophere
  144 00000008         swi_vector
  145 00000008 EAFFFFFE        B                loophere
  146 0000000C         prefetch_abort_vector
  147 0000000C EAFFFFFE        B                loophere
  148 00000010         data_abort_vector
  149 00000010 EAFFFFFE        B                loophere
  150 00000014         reserved_vector
  151 00000014 EAFFFFFE        B                loophere
  152 00000018         irq_vector
  153 00000018 EAFFFFFE        B                loophere
  154 0000001C         fiq_vector
  155 0000001C EAFFFFFE        B                loophere
  156 00000020         ;=======================================================
                       =====================
  157 00000020         ; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
  158 00000020         ;=======================================================
                       =====================
  159 00000020                 AREA             SBL1_ENTRY, CODE, READONLY, ALI
GN=4
  160 00000000                 CODE32
  161 00000000                 ENTRY
  162 00000000         __main
  163 00000000         _main
  164 00000000         sbl1_entry
  165 00000000         ;Change to Supervisor Mode
  166 00000000 E321F0D3        msr              CPSR_c, #Mode_SVC:OR:I_Bit:OR:F
_Bit
  167 00000004         ; Save the passing parameter from PBL
  168 00000004 E1A07000        mov              r7, r0
  169 00000008         ; Set VBAR (Vector Base Address Register) to SBL vector 
                       table
  170 00000008 E59F0090        ldr              r0, =0x08006000
  171 0000000C EE0C0F10        MCR              p15, 0, r0, c12, c0, 0



ARM Macro Assembler    Page 5 


  172 00000010         ; Setup the supervisor mode stack
  173 00000010 E59F008C        ldr              r0, =(0x08006000 + 0x0055000)
  174 00000014 E1A0D000        mov              r13, r0
  175 00000018         ; Switch to undefined mode and setup the undefined mode 
                       stack
  176 00000018 E321F0DB        msr              CPSR_c, #Mode_UND:OR:I_Bit:OR:F
_Bit
  177 0000001C E1A0D000        mov              r13, r0
  178 00000020         ; Switch to abort mode and setup the abort mode stack
  179 00000020 E321F0D7        msr              CPSR_c, #Mode_ABT:OR:I_Bit:OR:F
_Bit
  180 00000024 E1A0D000        mov              r13, r0
  181 00000028         ; Return to supervisor mode
  182 00000028 E321F0D3        msr              CPSR_c, #Mode_SVC:OR:I_Bit:OR:F
_Bit
  183 0000002C         ; Restore the passing parameter
  184 0000002C E1A00007        mov              r0, r7
  185 00000030 E59F5070        ldr              r5, =ddr_debug_init
  186 00000034 E12FFF35        blx              r5
  187 00000038 E59F506C        ldr              r5,=sbl1_ddr_debug_main_ctl
  188 0000003C E12FFF35        blx              r5
  189 00000040 EA000015        b                loophere
  190 00000044         ;=======================================================
                       =====================
  191 00000044         ; ddr_debug_invalidate_tlb
  192 00000044         ;
  193 00000044         ; ARGS
  194 00000044         ;   NONE
  195 00000044         ;
  196 00000044         ; DESCRIPTION
  197 00000044         ;   Invalidates the entire Translation Look-aside Buffer
                        (TLB) as a unified
  198 00000044         ;   operation (Data and Instruction). Invalidates all un
                       locked entries in the
  199 00000044         ;   TLB. Causes the prefetch buffer to be flushed. All f
                       ollowing instructions
  200 00000044         ;   are fetched after the TLB invalidation.
  201 00000044         ;   We should do this before we enable to MMU.
  202 00000044         ;=======================================================
                       =====================
  203 00000044         ddr_debug_invalidate_tlb
  204 00000044         ; Call memory barrier operations to ensure completion of
                        all cache
  205 00000044         ; maintenance & branch predictor operations appearing in
                        program
  206 00000044         ; order, before these instructions
  207 00000044                 mdsb
   72 00000044                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   73 00000044 F57FF04F        dsb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi
                                                            ons
   74 00000048                 ELSE
   81                          ENDIF
  208 00000048                 misb
   98 00000048                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   99 00000048 F57FF06F        isb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi



ARM Macro Assembler    Page 6 


                                                            ons
  100 0000004C                 ELSE
  107                          ENDIF
  209 0000004C E3A00000        mov              r0 , #0
  210 00000050 EE080F17        mcr              p15, 0, r0, c8, c7, 0
  211 00000054                 mdsb
   72 00000054                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   73 00000054 F57FF04F        dsb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi
                                                            ons
   74 00000058                 ELSE
   81                          ENDIF
  212 00000058                 misb
   98 00000058                 IF               {ARCHITECTURE} = "7-A" :LOR: {A
RCHITECTURE} = "7-M" :LOR: {ARCHITECTURE} == "7-A.security"
   99 00000058 F57FF06F        isb                          ; RVDS >= 3.0 suppo
                                                            rts ARMv7 instructi
                                                            ons
  100 0000005C                 ELSE
  107                          ENDIF
  213 0000005C E12FFF1E        bx               lr
  214 00000060         ;=======================================================
                       =====================
  215 00000060         ; sbl_save_regs
  216 00000060         ;
  217 00000060         ; PROTOTYPE
  218 00000060         ;   void sbl_save_regs();
  219 00000060         ;
  220 00000060         ; ARGS
  221 00000060         ;   None
  222 00000060         ;
  223 00000060         ; DESCRIPTION
  224 00000060         ;   Configure VBAR, vector table base register.
  225 00000060         ;   
  226 00000060         ;=======================================================
                       =====================    
  227 00000060         sbl_save_regs
  228 00000060         ; Finished so return    
  229 00000060 E12FFF1E        bx               lr
  230 00000064         ; void sbl1_external_abort_enable(uint32 flags)
  231 00000064         sbl1_external_abort_enable
                               FUNCTION
  232 00000064 E12FFF1E        bx               lr
  233 00000068                 ENDFUNC
  234 00000068         ;=======================================================
                       ===============
  235 00000068         ; Called by sbl1_error_handler only. We clean up the reg
                       isters and loop
  236 00000068         ; here until JTAG is connected.
  237 00000068         ;=======================================================
                       ===============
  238 00000068         sbl_loop_here
  239 00000068 E3A00000        mov              r0,#0
  240 0000006C E3A01000        mov              r1,#0
  241 00000070 E3A02000        mov              r2,#0
  242 00000074 E3A03000        mov              r3,#0
  243 00000078 E3A04000        mov              r4,#0
  244 0000007C E3A05000        mov              r5,#0



ARM Macro Assembler    Page 7 


  245 00000080 E3A06000        mov              r6,#0
  246 00000084 E3A07000        mov              r7,#0
  247 00000088 E3A08000        mov              r8,#0
  248 0000008C E3A09000        mov              r9,#0
  249 00000090 E3A0A000        mov              r10,#0
  250 00000094 E3A0B000        mov              r11,#0
  251 00000098 E3A0C000        mov              r12,#0
  252 0000009C         loophere
  253 0000009C EAFFFFFE        b                loophere
  254 000000A0                 END
              08006000 
              0805B000 
              00000000 
              00000000 
Command Line: --cpu=7-A.security --apcs=/noswst/interwork --no_unaligned_access
 -o/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0
22933/b/boot_images/core/boot/secboot3/hw/build/ddr_debug_8937/a53/FAADANAZ/msm
8952/sbl1/sbl1_ddr_debug.o -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M
8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/build -I/lo
cal/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/
b/boot_images/build/cust -I. -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177
-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/boot -I/local/mnt/work
space/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_image
s/core/api/boot/qfprom -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917
LAAAANAZB-1_20160818_022933/b/boot_images/core/buses/api/spmi -I/local/mnt/work
space/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_image
s/core/buses/api/icb -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LA
AAANAZB-1_20160818_022933/b/boot_images/core/buses/api/uart -I/local/mnt/worksp
ace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/
core/buses/api/i2c -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAA
ANAZB-1_20160818_022933/b/boot_images/core/api/dal -I/local/mnt/workspace/CRMBu
ilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/
services -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20
160818_022933/b/boot_images/core/api/storage -I/local/mnt/workspace/CRMBuilds/B
OOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/system
drivers -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_201
60818_022933/b/boot_images/core/api/systemdrivers/pmic -I/local/mnt/workspace/C
RMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/
api/systemdrivers/hwio/msm8937 -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-001
77-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/wiredconnectivity -I
/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_0229
33/b/boot_images/core/api/securemsm -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.
3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/secboot/a
pi -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818
_022933/b/boot_images/core/api/kernel/libstd/stringl -I/local/mnt/workspace/CRM
Builds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/ap
i/securemsm/secimgauth -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917
LAAAANAZB-1_20160818_022933/b/boot_images/core/api/hwengines -I/local/mnt/works
pace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images
/core/api/power -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANA
ZB-1_20160818_022933/b/boot_images/core/power/utils/inc -I/local/mnt/workspace/
CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core
/api/power/cpr/rpm -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAA
ANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/src -I/local/mnt/works
pace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images
/core/boot/ddr/common -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917L
AAAANAZB-1_20160818_022933/b/boot_images/core/boot/ddr/hw/msm8937 -I/local/mnt/
workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_i
mages/core/boot/ddr/hw/phy -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M



ARM Macro Assembler    Page 8 


8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/ddr/hw/controller -I/lo
cal/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/
b/boot_images/core/boot/secboot3/hw/msm8937 -I/local/mnt/workspace/CRMBuilds/BO
OT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/dal/src -I/
local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_02293
3/b/boot_images/core/dal/config -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00
177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/services/utils/src -I/l
ocal/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933
/b/boot_images/core/storage/flash/src/dal -I/local/mnt/workspace/CRMBuilds/BOOT
.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/flash
/src/hal -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20
160818_022933/b/boot_images/core/storage/flash/tools/inc -I/local/mnt/workspace
/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/cor
e/storage/flash/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAA
AANAZB-1_20160818_022933/b/boot_images/core/storage/sdcc/src -I/local/mnt/works
pace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images
/core/storage/sdcc/src/hal -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M
8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/sdcc/src/bsp -I/loca
l/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/
boot_images/core/storage/efs/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-0
0177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/hfat/inc -I/lo
cal/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/
b/boot_images/core/storage/hotplug/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF
.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/storage/tools/de
viceprogrammer_ddr/src/firehose -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00
177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/systemdrivers/tlmm/inc 
-I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_02
2933/b/boot_images/core/systemdrivers/tlmm/src -I/local/mnt/workspace/CRMBuilds
/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/wiredcon
nectivity/qhsusb/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LA
AAANAZB-1_20160818_022933/b/boot_images/core/wiredconnectivity/qusb/inc -I/loca
l/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/
boot_images/core/securemsm/cryptodrivers/ce/shared/inc -I/local/mnt/workspace/C
RMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/
securemsm/cryptodrivers/ce/test/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.
3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/cryptodri
vers/prng/shared/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LA
AAANAZB-1_20160818_022933/b/boot_images/core/securemsm/cryptodrivers/prng/test/
inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_2016081
8_022933/b/boot_images/core/securemsm/secdbgplcy/api -I/local/mnt/workspace/CRM
Builds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/se
curemsm/cryptodrivers/prng/shared/src -I/local/mnt/workspace/CRMBuilds/BOOT.BF.
3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/api/securemsm/sec
boot -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_201608
18_022933/b/boot_images/core/api/securemsm/seccfg -I/local/mnt/workspace/CRMBui
lds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/secur
emsm/secmath/shared/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M891
7LAAAANAZB-1_20160818_022933/b/boot_images/core/securemsm/fuseprov/chipset/msm8
937/inc -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_201
60818_022933/b/boot_images/core/hwengines/mhi -I/local/mnt/workspace/CRMBuilds/
BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/hwengines
/pcie -I/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160
818_022933/b/boot_images/core/boot/secboot3/hw/msm8937/sbl1 --list=/local/mnt/w
orkspace/CRMBuilds/BOOT.BF.3.3-00177-M8917LAAAANAZB-1_20160818_022933/b/boot_im
ages/core/boot/secboot3/hw/build/ddr_debug_8937/a53/FAADANAZ/msm8952/sbl1/sbl1_
ddr_debug.o.lst --sitelicense /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00177-
M8917LAAAANAZB-1_20160818_022933/b/boot_images/core/boot/secboot3/hw/build/ddr_
debug_8937/a53/FAADANAZ/msm8952/sbl1/sbl1_ddr_debug.o.i
