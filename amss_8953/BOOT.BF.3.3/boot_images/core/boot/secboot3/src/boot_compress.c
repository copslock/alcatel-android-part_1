/*===========================================================================

                         Boot Compression Feature File

GENERAL DESCRIPTION
  This feature file contains definitions  for  functions for compressing
  memory dumps, such as dumps to SD card.

Copyright 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who             what, where, why
--------   ---         --------------------------------------------------
06/23/15   ak          Initial version

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_compress.h"
#include "boot_cache_mmu.h"
#include "boot_error_if.h"                               
#include "boot_util.h"



#define NUM_ANCHORS 4       /* Must be a power of 2 */
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_ANCHOR_BITS 2
#define NUM_CODE_BITS 2
#define NUM_PARTIAL_BITS 10                               
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) /* 32 */
#define COMP_CODE_ZERO 0
#define COMP_CODE_EXACT 1
#define COMP_CODE_PARTIAL 2                              
#define COMP_CODE_NEW_ANCHOR 3

 
/* Add some amount of bits to the output stream. */
#define push_compressed_bits(bits,num_bits,compressed_word_stream,num_compressed_words,compressed_partial_bits,compressed_partial_word) \
  {                                                                                                         \
    uint64 temp = p_shl_64((uint32) bits, compressed_partial_bits);                                         \
    compressed_partial_bits += num_bits;                                                                    \
    compressed_partial_word = compressed_partial_word | temp;                                               \
    if(compressed_partial_bits >= WORD_SIZE_IN_BITS)                                                        \
    {                                                                                                       \
        compressed_word_stream[num_compressed_words++] = compressed_partial_word;                           \
        compressed_partial_word = compressed_partial_word >> WORD_SIZE_IN_BITS;                             \
        compressed_partial_bits -= WORD_SIZE_IN_BITS;                                                       \
    }                                                                                                       \
  }

/* Add the last compressed partial word to the stream. */
#define finalize_compressed_bits(compressed_word_stream,num_compressed_words,compressed_partial_bits,compressed_partial_word) \
  if(compressed_partial_bits > 0)                                                                           \
  {                                                                                                         \
    compressed_word_stream[num_compressed_words++] = compressed_partial_word;                               \
  } 

/* Check for exact or partial match, whichever is first in the anchor list
 * Code for exact match is 01 and for partial match is 10
 */
#define CHECK_ANCHOR(anchor) \
        anchor_val = anchors[anchor];                                                                       \
        if (anchor_val == val)                                                                              \
        {                                                                                                   \
          push_compressed_bits((anchor << NUM_CODE_BITS) + COMP_CODE_EXACT,                                 \
                               NUM_ANCHOR_BITS + NUM_CODE_BITS,                                             \
                               compressed,                                                                  \
                               num_compressed_words,                                                        \
                               num_compressed_partial_bits,                                                 \
                               compressed_partial_word);                                                    \
          continue;                                                                                         \
        }                                                                                                   \
        if ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00))                                                \
        {                                                                                                   \
          push_compressed_bits(((val & 0x3FF) << (NUM_ANCHOR_BITS+NUM_CODE_BITS)) + ((anchor<<NUM_CODE_BITS)\
                                + COMP_CODE_PARTIAL),                                                       \
                               NUM_ANCHOR_BITS + NUM_CODE_BITS + NUM_PARTIAL_BITS,                          \
                               compressed,                                                                  \
                               num_compressed_words,                                                        \
                               num_compressed_partial_bits,                                                 \
                               compressed_partial_word);                                                    \
          anchors[anchor] = val;                                                                            \
          continue;                                                                                         \
        }                                                                                                   \
        anchor = (anchor + (NUM_ANCHORS - 1)) & (NUM_ANCHORS - 1);                                          \

typedef unsigned int (*compress_type)(uint32 * uncomp, uint32 * comp, uint32 in_len);
typedef uint64 (*shl_64_type)(uint64 value, uint32 bits);        

/*=========================================================================
                            
                     LOCAL FUNCTION DECLARATIONS

=========================================================================*/
static uint32 boot_compress_buffer(uint32 * uncompressed, uint32 * compressed, uint32 in_len);

static uint64 shl_64(uint64 value, uint32 bits)
{
  return value << bits;
}

static shl_64_type p_shl_64 = shl_64;

/*=========================================================================
                            
                       FUNCTION DEFINITIONS

=========================================================================*/


/*===========================================================================

**  Function :  boot_compress_get_header

** ==========================================================================
*/
/*!
* 
* @brief  
*   This routine modifies a 16-byte header that marks the start of a compressed
*   file. This should be called and transmitted before writing any compressed
*   output.
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void boot_compress_get_header(uint32 block_size, uint32 header[DC_HEADER_SIZE])
{
  uint8 * header_idx_2 = (uint8 *)(&(header[2]));
  
  header[0] = 0x00435752;  /* "RWC" - Magic Number */
  header[1] = block_size;  /* Size of input blocks */

  header_idx_2[0] = 0x01;  /* System version       */
  header_idx_2[1] = 0x00;
  header_idx_2[2] = 0x02;  /* Algorithm version    */
  header_idx_2[3] = 0x00;
  header[3] = 0;           /* Offset to skip one contiguous PAGES_PER_CHUNK * PAGE_SIZE aligned block */
  header[4] = 0;		 	/*Size of Block	*/
}


/*===========================================================================

**  Function :  boot_compress_input

** ==========================================================================
*/
/*!
* 
* @brief  
*   This routine compresses a word-aligned uncompressed buffer of in_len 
*   words into a word-aligned output buffer. This function runs from DDR.
*
* @par Dependencies
*   boot_compress_init needs to be called first.
*
* @retval
*   uint32 size of the compressed output in words
* 
* @par Side Effects
*   None
* 
*/
uint32 boot_compress_input(uint32 * uncompressed, uint32 * compressed, uint32 in_len)
{
  uint32 comp_sz;

  BL_VERIFY((uncompressed != NULL) && (compressed != NULL), 
            BL_ERR_NULL_PTR_PASSED); 

  comp_sz = boot_compress_buffer(uncompressed, &(compressed[1]), in_len);
  compressed[0] = comp_sz;

  return comp_sz + 1;

}


/*===========================================================================

**  Function :  boot_compress_buffer

** ==========================================================================
*/
/*!
* 
* @brief  
*   This routine compresses a word-aligned uncompressed buffer of in_len 
*   words into a word-aligned output buffer.
*
* @par Dependencies
*   boot_compress_init needs to be called first.
*
* @retval
*   uint32 size of the compressed output in words
* 
* @par Side Effects
*   None
* 
*/
static uint32 boot_compress_buffer(uint32 * uncompressed, uint32 * compressed, uint32 in_len)
{
  uint32 i, val;
  uint32 anchors[NUM_ANCHORS];
  uint32 anchor, anchor_val;
  uint32 anchor_index = NUM_ANCHORS - 1;
  uint32 num_compressed_partial_bits = 0, num_compressed_words = 0;
  uint64 compressed_partial_word = 0;

  BL_VERIFY((uncompressed != NULL) && (compressed != NULL), 
            BL_ERR_NULL_PTR_PASSED); 

  for(i = 0; i < NUM_ANCHORS; i++)
  {
    anchors[i] = 0;
  }


  /* Loop over the input. */ 
  for (i = 0; i < in_len; i++)
  {
    val = uncompressed[i];
    if (val == 0) 
    {
      /* 0 is a special case, so there is no need to add it as an anchor. */
      /* Code: 00 */
      push_compressed_bits(COMP_CODE_ZERO,
                           NUM_CODE_BITS,
                           compressed,
                           num_compressed_words,
                           num_compressed_partial_bits,
                           compressed_partial_word);
    }
    else 
    {
      /* For non-zero values, check for exact or partial anchor matches.
       * Traverse the anchors such that the most recently seen ones come first.
       */
      anchor = anchor_index;
      CHECK_ANCHOR(anchor);
      CHECK_ANCHOR(anchor);
      CHECK_ANCHOR(anchor);
      CHECK_ANCHOR(anchor);

      /* If there is a full or partial match above, control would have hit a
       * continue statement. If there is no full or partial match, leave the
       * word uncompressed and add it as an anchor.
       */
      anchor_index = (anchor_index + 1) & (NUM_ANCHORS - 1);
      anchors[anchor_index] = val; 
      /* Code: 11 */
      push_compressed_bits(COMP_CODE_NEW_ANCHOR,
                           NUM_CODE_BITS,
                           compressed,
                           num_compressed_words,
                           num_compressed_partial_bits,
                           compressed_partial_word); 

      /* Push the word. */
      push_compressed_bits(val,
                           WORD_SIZE_IN_BITS,
                           compressed,
                           num_compressed_words,
                           num_compressed_partial_bits,
                           compressed_partial_word);
    } /* val != 0 */
  } 
  /* Take care of the remaining bits. */
  finalize_compressed_bits(compressed,
                           num_compressed_words, 
                           num_compressed_partial_bits, 
                           compressed_partial_word);

  return num_compressed_words;
}



