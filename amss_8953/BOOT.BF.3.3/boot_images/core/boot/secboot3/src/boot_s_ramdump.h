#ifndef BOOT_S_RAMDUMP_H
#define BOOT_S_RAMDUMP_H


#include "boot_comdef.h"

#include "boot_logger.h"
#include "boot_logger_timer.h"

extern char log_message[];

__packed struct boot_s_ramdump_config
{
  uint16 magic;     /* 0xAAAA or 0x5555 */
  uint8  timeout;   /* time out minutes waiting for SD card */
  uint8  location;  /* dump device location type */
};

__packed struct boot_s_ramdump_restart_reason
{
  uint32 reason;    /* restart reason */
  uint32 dload;     /* 0x504d5544, string "DUMP" */
};

extern struct boot_s_ramdump_config s_ramdump_config;

enum dump_device_location_type
{
  LOCATION_INTERNAL_ONLY    = 0,           /* try ramdump partition -> QPST */
  LOCATION_EXTERNAL_ONLY,                  /* try SD card -> QPST */
  LOCATION_INTERNAL_FORCE,                 /* force ramdump partition -> QPST */
  LOCATION_EXTERNAL_FORCE,                 /* force SD card -> QPST */
  LOCATION_INTERNAL_PREFER,                /* try ramdump partition -> try SD card -> QPST */
  LOCATION_EXTERNAL_PREFER,                /* try SD card -> try ramdump partition -> QPST */
  LOCATION_DEVICE_AUTO,                    /* SD card & ramdump partition both failed -> QPST */
  LOCATION_DEVICE_DEFAULT   = LOCATION_EXTERNAL_ONLY,
  LOCATION_DISABLED         = 0xFF,        /* QPST */
};

enum restart_reason_type
{ /* reference to kernel/drivers/power/reset/msm-poweroff.c */
  REASON_IGNORE             = -1,         /* ignore this input */
  REASON_BOOTLOADER         = 0x77665500,
  REASON_RECOVERY           = 0x77665502,
  REASON_RTC                = 0x77665503,
  REASON_DMVERITY_CORRUPTED = 0x77665508,
  REASON_DMVERITY_ENFORCE   = 0x77665509,
  REASON_KEYS_CLEAR         = 0x7766550a,
  REASON_OEM_PREFIX         = 0x6f656d00,
};

#define FULL_RAMDUMP_SIZE 4328521728UL    /* (4 * 1024 + 32) MB, 4136+MB in fact*/

extern uint8 traceability_partition_id[];


#define F2BS_MAGIC          0x53463242  /* string "F2BS" */
#define F2BS_FLAG_VALID     0x00000001

__packed struct boot_f2bs_header
{
  uint32 magic;     /* magic number */
  uint32 flag;      /* f2bs flags */
  uint32 blocks;    /* total blocks */
  uint32 blksize;   /* block size */
  uint16 ranges;    /* total ranges */
  uint16 crc16;     /* ranges crc16 value */
  uint32 reserved;  /* reserved for future */
};

__packed struct boot_f2bs_range
{
  uint32 range[2];  /* half open range, [range[0], range[1]) */
};

#define F2BS_MAX_RANGES     128

#define F2BS_HEADER_SIZE    (sizeof(struct boot_f2bs_header))
#define F2BS_RANGE_SIZE     (sizeof(struct boot_f2bs_range))

struct boot_f2bs_range_info
{
  uint64 relative_offset;
  uint64 absolute_offset;
  uint64 size;
};

#define S_RAMDUMP_RESTART_REASON_OFFSET 0x1200
#define F2BS_OFFSET (S_RAMDUMP_RESTART_REASON_OFFSET + 0x8)
#define S_RAMDUMP_CONFIG_OFFSET 0xE7000

extern boot_boolean boot_s_ramdump_prepare( void );


#endif /* BOOT_S_RAMDUMP_H */
