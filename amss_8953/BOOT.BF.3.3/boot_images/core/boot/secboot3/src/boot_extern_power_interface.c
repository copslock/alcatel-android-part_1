/*===========================================================================

                    BOOT EXTERN POWER DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external power drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/secboot3/src/boot_extern_power_interface.c#2 $
$DateTime: 2015/10/29 03:17:02 $
$Author: pwbldsvc $
    
when         who    what, where, why
--------   ---     ----------------------------------------------------------
09/04/13   jz       Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "railway.h"
#include "cpr.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/*===========================================================================

  FUNCTION RAILWAY_INIT

  DESCRIPTION
        This function initialize the railway driver and all of its associated rail control.

  PARAMETERS
    None.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None

===========================================================================*/
void boot_railway_init()
{
  railway_init();
}

/*===========================================================================

  FUNCTION CPR_INIT

  DESCRIPTION
        This function initializes the CPR driver.

  PARAMETERS
    None.

  DEPENDENCIES
    Needs to call railway_init first.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None

===========================================================================*/
void boot_cpr_init()
{
	rbcpr_init();
}

/*===========================================================================

**  Function :  boot_populate_cpr_settings

** ==========================================================================
*/
/*!
* 
* @brief
*   This function calls cpr_externalize_state to populate the CPR settings and voltages in smem   
* 
* @param
*   None
*
* @par Dependencies
*   smem must have been initialized.
*   
* @retval
*   None                                                             
* 
* @par Side Effects
*   None
* 
*/
void boot_populate_cpr_settings(void)
{
	rbcpr_externalize_state();
}

/*===========================================================================

**  Function :  boot_rbcpr_set_cx_mx_to_safe_turbo_voltage

** ==========================================================================
*/
/*!
*
* @brief
*   This function calls rbcpr_set_cx_mx_to_safe_turbo_voltage to set cx & mx turbo voltage
*
* @param
*   None
*
* @par Dependencies
*   smem must have been initialized.
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void boot_rbcpr_set_cx_mx_to_safe_turbo_voltage(void)
{

}

/*===========================================================================

**  Function :  boot_railway_setting_checksum

** ==========================================================================
*/
/*!
*
* @brief
*   Returns a 16-bit checksum of the specified rail setting.
*
* @param
*   None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
uint16 boot_railway_setting_checksum(uint32 rail_id)
{
    return railway_setting_checksum(rail_id);
}

/*===========================================================================

**  Function :  boot_rail_id

** ==========================================================================
*/
/*!
*
* @brief
*   Return the ID for the named rail on this target.
*
* @param
*   None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
int boot_rail_id(const char* rail)
{
    return rail_id(rail);
}

/*===========================================================================

**  Function :  boot_rbcpr_cx_mx_settings_checksum

** ==========================================================================
*/
/*!
*
* @brief
*   Return the hash of the CPR settings that are related to Cx/Mx.
*
* @param
*   None
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
uint32 boot_rbcpr_cx_mx_settings_checksum(void)
{
    return rbcpr_cx_mx_settings_checksum();
}
