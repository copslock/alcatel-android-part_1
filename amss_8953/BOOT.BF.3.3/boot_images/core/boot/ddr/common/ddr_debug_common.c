/**
 * @file boot_ddr_debug.c
 * @brief
 * Implementation for DDR Debug Mode.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/common/ddr_debug_common.c#5 $
$DateTime: 2016/04/21 08:39:38 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
11/18/15   yps     Porting code to 8953 platform
11/26/14   yps     Porting code to 8909 platform
05/11/14   yps     Porting code to 8916 platform
09/17/13   sl      Added more DDR tuning options.
09/13/13   sl      Fixed pslew/nslew order.
08/29/13   sl      Use USB APIs instead of Boot APIs.
08/20/13   sl      Added DDR tuning.
08/07/13   sr      Added watchdog reset support.
06/18/13   sl      Initial version.
================================================================================
                     Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "stdio.h"
#include "string.h"
#include "qhsusb_al_bulk.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_test.h"
#include "ddr_debug_common.h"
#include "boot_extern_clk_interface.h"
#include "boot_page_table_armv7.h"
#include "boot_error_if.h"
#include "boot_cache_mmu.h"

/*==============================================================================
                                  MACROS
==============================================================================*/

extern void logMessage(const char* format, ...);

/*==============================================================================
                                  DATA
==============================================================================*/
/* Echo and message buffers: must be addressable by USB and 4-byte aligned */
__attribute__((aligned(4)))
char rx_buf[BUFFER_LENGTH], tx_buf[BUFFER_LENGTH+1], msg_buf[BUFFER_LENGTH+1] = {0};

static volatile uint32 min_base = 0;

char str[BUFFER_LENGTH];
char str_error_log[BUFFER_LENGTH]={0};


/* DDR tuning pattern */
static const uint32 ddr_tuning_pattern[] =
{
  0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE,
  0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D,

  0xA5A5A5A5, 0x5A5A5A5A, 0xA5A5A5A5, 0x5A5A5A5A,
  0x0F0F0F0F, 0xF0F0F0F0, 0x00000000, 0xFFFFFFFF,

  0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF,
  0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000,
};

/**
 * @brief function defect test
 *        name function test name
 *        fp   test function pointer
 */
typedef struct  {
    char *name;
    boolean (*fp)(const uint32, const uint32);
}funcDefectTest_s;

char pattern_name[][20] =
 {
   {"DEFAULT"},
   {"Bit Flip"},
   {"Checkerboard"},
   {"Bit Spread"},
   {"Solid bits"},
   {"Walking ones"},
   {"Walking zeros"},
   {"Seq incr"},
   {"Customer"},
 };
/* DDR tuning cookies */
__attribute__((section("DDR_DEBUG_TUNING_COOKIE")))
static void (*volatile reset_cb_func)(boolean) = NULL;
DDR_TEST_PATTERN PATTERN;

uint32 start_address;
uint32 test_size;
/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
#ifdef SEC_WDOG_ENABLE
void ddr_debug_force_reset( void )
{
  ddr_debug_dog_init(FALSE);
  ddr_debug_dog_enable(TRUE);

  /* Wait for watchdog reset */
  for (; ;) {}
}
#endif

/* =============================================================================
**  Function : ddr_debug_rx_cb
** =============================================================================
*/
/**
*   @brief
*   Callback for transportation layer receiving.
*
*   @param[in]  bytes_read  Number of bytes read
*   @param[in]  err_code    Error code returned
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/










/* =============================================================================
**  Function : ddr_debug_tx_cb
** =============================================================================
*/
/**
*   @brief
*   Callback for transportation layer transmitting.
*
*   @param[in]  bytes_written  Number of bytes written
*   @param[in]  err_code       Error code returned
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/

/* =============================================================================
**  Function : ddr_debug_input
** =============================================================================
*/
/**
*   @brief
*   Get input command for DDR Debug Mode.
*
*   @param  None
*
*   @retval  Input command
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/


    /* Receive user's inputs */


    /* Echo user's inputs */



/* =============================================================================
**  Function : ddr_debug_output
** =============================================================================
*/
/**
*   @brief
*   Output message for DDR Debug Mode.
*
*   @param[in]  msg  Message to be output
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/




void ddr_debug_test(uint32 *ddr_base, uint32 failures[4])
{
  uint32 idx;
  uint32 result;

  result = 0;

  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    ddr_base[idx] = 0;
  }
  
  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    ddr_base[idx] = ddr_tuning_pattern[idx];
  }

  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    result |= ddr_base[idx] ^ ddr_tuning_pattern[idx];
  }

  for (idx = 0; idx < 4; idx++)
  {
    if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
  }

} /* ddr_debug_test */

#if 1

/* =============================================================================
**  Function : ddr_test_data_lines
** =============================================================================
*/
/**
*   @brief
*   Test by walking-ones if none of data lines is open, or is shorted high, low
*   or to another line.
*
*   This function writes (0x1 << n) to the first word of memory and then reads
*   back to check if the same value is fetched, where n is from 0 to
*   (data width - 1). This is for testing if each data line can be set and unset
*   individually. ~(0x1 << n) is written to the last word of memory each time
*   after (0x1 << n) is written, in order to clear data lines in case some line
*   is open.
*
*   @param[in]  base     Base address of memory
*   @param[in]  limit    Maximum offset (in bytes) of memory
*
*   @retval  TRUE     Test passed
*   @retval  FALSE    Test failed
*
*   @dependencies
*   Memory size in words must be power of 2, and base address must be aligned
*   with size.
*
*   @sideeffects
*   Memory is corrupted after this function is called. Error is logged if any.
*
*   @sa
*   None
*/

static boolean ddr_test_data_lines
(
  uint32 base,
  uint32 limit
)
{
  volatile uint32 *base_addr;
  uint32 pattern;
  uint32 data0, data1;
  uint8 result = TRUE;
    
  /* convert base address and limit for accessing memory by word */
  base_addr = (uint32 *)base;
  limit >>= 2;

  /* save the data before writing to it for ram dump case */
  data0 = base_addr[0];
  data1 = base_addr[limit];

  /* test data lines by walking-ones */
  for (pattern = 0x1; pattern != 0x0; pattern <<= 1)
  {
    /* write (0x1 << n) to first word */
    base_addr[0] = pattern;

    /* write ~(0x1 << n) to last word to clear data lines */
    base_addr[limit] = ~pattern;

    /* check if same value is read back */
    if (base_addr[0] != pattern)
    {
      logMessage("Failure happened on data line:%d",pattern);
      result = FALSE;
    }
  }

	
	/* restore the data for ram dump*/
  base_addr[0] = data0;
  base_addr[limit]= data1;

  return result;
}

/* =============================================================================
**  Function : ddr_test_addr_lines
** =============================================================================
*/
/**
*   @brief
*   Test if none of address lines is open, or is shorted high, low or to another
*   line.
*
*   This function first writes a unique value to each word at the address of
*   (0x1 << n), as well as to the word at 0x0, where n is from 0 to
*   (address width - 1). If address lines are not wired properly, then two
*   writes will go to the same location, which can be detected by checking if a
*   different value is read back from one of these words.
*
*   @param[in]  base     Base address of memory
*   @param[in]  limit    Maximum offset (in bytes) of memory
*
*   @retval  TRUE     Test passed
*   @retval  FALSE    Test failed
*
*   @dependencies
*   Memory size in words must be power of 2, and base address must be aligned
*   with size. Also assume data lines are wired properly.
*
*   @sideeffects
*   Memory is corrupted after this function is called. Error is logged if any.
*
*   @sa
*   None
*/

static boolean ddr_test_addr_lines
(
  uint32 base,
  uint32 limit
)
{
  volatile uint32 *base_addr;
  uint32 addr_line;
  uint32 data[32] = {0};
  uint32 i;
  uint8 result = TRUE;


  /* convert base address and limit for accessing memory by word */
  base_addr = (uint32 *)base;
  limit >>= 2;

  /* write negation of address to each word at (0x1 << n) */
  for (addr_line = 0x1, i = 1; addr_line <= limit; addr_line <<= 1, i++)
  {
    /* save the address prior to overwriting it */
    data[i] = base_addr[addr_line];
    base_addr[addr_line] = ~addr_line;
  }

  /*
  save content at base */
  data[0] = base_addr[0x0];

  /* write negation of address to word at 0x0 */
  base_addr[0x0] = ~0x0;

  /* check if same value is read back from each word */
  for (addr_line = 0x1, i = 1; addr_line <= limit; addr_line <<= 1, i++)
  {
    if (base_addr[addr_line] != ~addr_line)
    {
      logMessage("Failure happened on address line:%d",addr_line+1);
      result = FALSE;
    }
    else
    {
      /* restore the address line's original content */
      base_addr[addr_line] = data[i];
    }
  }

  /* restore base content */
  base_addr[0x0] = data[0];
  return result;
}

/* =============================================================================
**  Function :  ddr_test_own_addr
** =============================================================================
*/
/**
*   @brief
*   Test memory integrity by own-address algorithm.
*
*   This function writes each word of memory with its own address, and reads
*   back to check if the same value is fetched. It then runs a second write/read
*   pass using the negation of address, in order to test each bit with both "0"
*   and "1".
*
*   @param[in]  base     Base address of memory
*   @param[in]  limit    Maximum offset (in bytes) of memory
*
*   @retval  TRUE     Test passed
*   @retval  FALSE    Test failed
*
*   @dependencies
*   Memory size in words must be power of 2, and base address must be aligned
*   with size. Also assume data/address lines are wired properly.
*
*   @sideeffects
*   Memory is corrupted after this function is called. Error is logged if any.
*
*   @sa
*   None
*/

static boolean ddr_test_own_addr
(
  uint32 base,
  uint32 limit
)
{
  volatile uint32 *base_addr;
  uint32 offset;

  
  /* convert base address and limit for accessing memory by word */
  base_addr = (uint32 *)base;
  limit >>= 2;

  /* write each word with its own address */
  for (offset = 0; offset <= limit; offset++)
  {
    base_addr[offset] = offset;
  }
  /* check if same value is read back from each word */
  for (offset = 0; offset <= limit; offset++)
  {
    if (base_addr[offset] != offset)
    {
      logMessage("Failed to Write address 0x%0x, date 0x%0x\n",&base_addr[offset], offset);
      return FALSE;
    }
  }

  /* write each word with negation of address */
  for (offset = 0; offset <= limit; offset++)
  {
    base_addr[offset] = ~offset;
  }
  /* check if same value is read back from each word */
  for (offset = 0; offset <= limit; offset++)
  {
    if (base_addr[offset] != ~offset)
    {
      logMessage("Failed to Write address 0x%0x, date 0x%0x\n",&base_addr[offset], ~offset);
      return FALSE;
    }
  }

  return TRUE;
}

#endif
boolean ddr_function_defect_test_bit_flip_tuning(const uint32 base, const uint32 size, boolean rdcdc_scan,uint8 coarse_sweep,uint8 fine_sweep, uint32 failures[4])
{
  const uint8 UINT32_LEN = 32;
  const uint8 LOOP_CYCLE = 2;
  const uint32 ONE = 0x1;
  uint8 k = 0, j = 0;
  PATTERN = BITFLIP_PATTERN;

  for (j = 0; j < UINT32_LEN * 2; ++j)
  {
    uint32 val = ONE << j;	
   
    for (k = 0; k < LOOP_CYCLE; ++k)
    {
        val = ~val;
	 ddr_write_pattern(base,size,&val);
  		
        /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
  				
  	   
  					
              return FALSE;
    }
  }
	
  return TRUE;
}

#if 0
void ddr_debug_tune_cdc(boolean in_progress)
{
  /* Install reset callback function */
  reset_cb_func = &ddr_debug_tune_cdc;

  /* Do tuning */
  ddr_debug_do_tuning(in_progress, FALSE,FALSE);

  /* Uninstall reset callback function */
  reset_cb_func = NULL;
#ifdef SEC_WDOG_ENABLE
  /* Force watchdog reset */
  ddr_debug_force_reset();
#endif
} /* ddr_debug_tune_cdc */

void ddr_debug_tune_all(boolean in_progress)
{
  /* Install reset callback function */
  reset_cb_func = &ddr_debug_tune_all;

  /* Do tuning */
  ddr_debug_do_tuning(in_progress, TRUE,FALSE);

  /* Uninstall reset callback function */
  reset_cb_func = NULL;
#ifdef SEC_WDOG_ENABLE
  /* Force watchdog reset */
  ddr_debug_force_reset();
#endif
} /* ddr_debug_tune_all */


void ddr_debug_dq_training(boolean in_progress)
{
  /* Install reset callback function */
  reset_cb_func = &ddr_debug_tune_cdc;

  /* Do tuning */
  ddr_debug_do_tuning(in_progress, FALSE,TRUE);

  /* Uninstall reset callback function */
  reset_cb_func = NULL;
#ifdef SEC_WDOG_ENABLE
  /* Force watchdog reset */
  //ddr_debug_force_reset();
#endif
} /* ddr_debug_tune_cdc */
#endif
/* =============================================================================
**  Function : ddr_function_defect_test_solid_bits
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        Write the data 0x00000000 and 0x11111111 alternately to the tested 
 *  memory area, and read back to check if the same value is fetched.
 *
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_solid_bits(uint32 base, uint32 size)
{
  const uint32 UINT32_ZERO = 0x0;
  const uint8 LOOP_CYCLES = 64;
  uint8  j = 0;
  PATTERN = SOLID_BITS_PATTERN;
  
  for (j = 0; j < LOOP_CYCLES; ++j)
  {
    uint32 val = (j % 2) == 0 ? (~UINT32_ZERO) : UINT32_ZERO;
  	 
	 ddr_write_pattern(base,size,&val);
  
    /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
        return FALSE;
    
  }
  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_check_board
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        CheckBoard test on the memory test size given
 *        with partten (0x55555555, 0xAAAAAAAA)
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_check_board(uint32 base, uint32 size)
{
  const uint32 CHECK_BOARD = 0x55555555;
  const uint8 LOOP_CYCLES = 64;
  uint8  j = 0;
  PATTERN = CHECK_BOARD_PATTERN;
  
  for (j = 0; j < LOOP_CYCLES; ++j)
  {
    uint32 val = (j % 2) == 0 ? (~CHECK_BOARD) : CHECK_BOARD;
	ddr_write_pattern(base,size,&val);
  	
    
      /* verify */
	if(ddr_read_pattern(base,size,&val)==FALSE)
       return FALSE;

  }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_bit_spread
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        BitSpread test on the memory test size given
 *        with address:   00000101b -> 00001010b -> 00010100b  -> ...
 *             address++: 11111010b -> 11110101b -> 11101011b -> ...
 *             ...      :
 *        shmoo
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 * @sideeffects
 * Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_bit_spread(const uint32 base, const uint32 size)
{
  const uint8 UINT32_LEN = 32;
  const uint32 ONE = 0x1;
  uint8  j = 0;
  PATTERN = BITSPREAD_PATTERN;
  
  for (j = 0; j < UINT32_LEN * 2; ++j)
  {
    uint32 val = 0;
    val = (ONE << j) | (ONE << (j + 2));
  
    ddr_write_pattern(base,size,&val);
  
  		 
    /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
        return FALSE;
  }

   return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_bit_flip
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        BitFlip test on the memory test size given
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_bit_flip(const uint32 base, const uint32 size)
{
  const uint8 UINT32_LEN = 32;
  const uint32 ONE = 0x1;
  uint8 j = 0;
  PATTERN = BITFLIP_PATTERN;
  
  for (j = 0; j < UINT32_LEN * 2; ++j)
  {
    uint32 val = ONE << j;
  	
    ddr_write_pattern(base,size,&val);
   
  
      /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
          return FALSE;
        }
  
  
  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_walking_one
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        WalkingOne test on the memory test size given
 *        with address:   0000001b -> 0000010b
 *                     -> 0000100b -> 0001000b
 *             ...      :
 *        shmoo
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_walking_one(const uint32 base, const uint32 size)
{
  const uint8 UINT32_LEN = 32;
  const uint32 ONE = 0x1;
  uint8  j = 0;
  PATTERN = WORKING_ONE_PATTERN;
  
  for (j = 0; j < UINT32_LEN * 2; ++j)
  {
    uint32 val = 0;
    if (j < UINT32_LEN)
      val = (ONE << j) ;
    else
      val = (ONE << (UINT32_LEN * 2 - 1 - j));
  
    ddr_write_pattern(base,size,&val);
  
    
    /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
        return FALSE;
  }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_walking_zero
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        WalkingZero test on the memory test size given
 *        with address:   1111110b -> 1111101b
 *                     -> 1111011b -> 1110111b
 *             ...      :
 *        shmoo
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_walking_zero(const uint32 base, const uint32 size)
{
  const uint8 UINT32_LEN = 32;
  const uint32 ONE = 0x1;
  uint8  j = 0;
  PATTERN = WORKING_ZERO_PATTERN;
  
  for (j = 0; j < UINT32_LEN * 2; ++j)
  {
    uint32 val = 0;
    if (j < UINT32_LEN)
      val = ~(ONE << j) ;
    else
      val = ~(ONE << (UINT32_LEN * 2 - 1 - j));
    ddr_write_pattern(base,size,&val);
  
    
    /* verify */
     if(ddr_read_pattern(base,size,&val)==FALSE)
        return FALSE;
  }

  return TRUE;
}
/* =============================================================================
**  Function : pseudo_rand_value_generate
** =============================================================================
*/
/**
 * @brief pseudo_rand_value_generate
 *        use a pseudo method to generate the random data from the seed
 *
 * @param[in] seed
 *
 * @return the random value
 */
uint32 pseudo_rand_value_generate(const uint32 seed)
{
  uint32 randVal = (seed * 123 + 59) % 0xFFFFFFFF;
  return randVal;
}

/* =============================================================================
**  Function : ddr_function_defect_test_rand_val
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        ddr_function_defect_test_rand_val will fill the data test area with random 
 *        test data,the 1st data will come from data 0, the remaining data will be 
 *        generated from a pesudo random generation function
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 * @sideeffects
 * Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_rand_val(const uint32 base, const uint32 size)
{
  volatile uint32 * base_addr = (uint32 *)base;
  uint32 j = 0;
  uint32 randVal = 0;
  
  for (j = 0; j <= size; ++j)
  {
    base_addr[j] = randVal;
    randVal = pseudo_rand_value_generate(randVal);
  }
  
  ddr_debug_output("."); 
  
  /* verify */
  randVal = 0;
  for (j = 0; j <= size; ++j)
  {
    if (base_addr[j] != randVal)
    {
      snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
  	   	                                               base_addr + j, base_addr[j], randVal);
      ddr_debug_output(str); 
      return FALSE;
    }
    randVal = pseudo_rand_value_generate(randVal);
  }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_seq_incr
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *  The value wrote to the address is increased 
 *  continuously with address increased
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
boolean ddr_function_defect_test_seq_incr(const uint32 base, const uint32 size)
{
  uint32 val = 0x0;
  PATTERN = SEQ_INR_PARTTERN;
  ddr_write_pattern(base,size,&val);
  
  
  /* verify */	
  if(ddr_read_pattern(base,size,&val)==FALSE)
      return FALSE;

  return TRUE;
}

/* =============================================================================
**  Function : ddr_defect_interval_test_mem_map
** =============================================================================
*/
/**
*   @brief
*   Test if memory size in words is power of 2, and base address is aligned with
*   size.
*
*   Memory size in words is power of 2 if and only if size in bytes is power of
*   2 (i.e., (size & (size-1)) == 0x0) and is no smaller than one word. Base
*   address is aligned with size if and only if (base & (size-1)) == 0x0.
*
*   Notice this function takes memory limit as input, where limit = (size-1).
*
*   @param[in]  base     Base address of memory
*   @param[in]  limit    Maximum offset (in bytes) of memory
*
*   @return TRUE test pass, FALSE test fail
*/

boolean ddr_defect_interval_test_mem_map
(
 uint32 base,
 uint32 limit
)
{
  return ( ((limit + 1) & limit) == 0x0 &&
           limit >= sizeof(uint32) - 1 &&
           (base & limit) == 0x0 );
}

boolean ddr_function_defect_test(const uint32  ddr_base, const uint32 size)
{
  static funcDefectTest_s function_defect_test[] =
  {
    {"Solid bits", ddr_function_defect_test_solid_bits},
    {"Checkerboard", ddr_function_defect_test_check_board},
    {"Bit Spread", ddr_function_defect_test_bit_spread},
    {"Bit Flip", ddr_function_defect_test_bit_flip},
   // {"Walking ones", ddr_function_defect_test_walking_one},
  //  {"Walking zeros", ddr_function_defect_test_walking_zero},
   // {"Random value", ddr_function_defect_test_rand_val},
   // {"Seq incr", ddr_function_defect_test_seq_incr},
  };
	
  uint8 ddr_function_defect_test_num = sizeof(function_defect_test) / sizeof(*function_defect_test);
  uint8 i= 0;
  
  for(i=0; i < ddr_function_defect_test_num; i++)
  {    
    if(function_defect_test[i].name && function_defect_test[i].fp)
    {
      ddr_debug_output(function_defect_test[i].name);
      ddr_debug_output(":\r\n"); 
      if(!(function_defect_test[i].fp(ddr_base, size >> 2)))
      {
      	return 	FALSE;
      }
      else
        ddr_debug_output("ok\r\n"); 	
    }
   }
	
  return TRUE;
}

/**
 * @brief DDR memory test by step
 *        test method:
 *        Step 0: write/read every double word until whole space looped
 *        Step 1: write/read every second double word until whole space looped
 *                totally 2 cycles.
 *        Step 2: write/read every four double word until whole space looped
 *                totally 4 cycles.
 *        ......
 *        Step n: write/read every 2^n double word until whole space looped
 *                totally 2^n cycles.
 *        NOTE: request at least 40MiB for test, and the memory space size
 *              should be n * 64KiByte
 *
 * @param base test memory space start pointer
 * @param length test memory space length in byte
 * @param stepPower the power of the step number
 * @param sampleSize the repeat times of this step test*
 * @return True test pass, FALSE test failure at read verification
 */
boolean ddr_interval_stress_test_per_step(uint32  const base, const uint32 length,
        const uint32 stepPower, const uint32 sampleSize)
{
  uint32 sampleCycle   = 0;
  uint32 stepStart     = 0;
  uint32 stepCycle     = 0;
  uint32 stepLength    = 0;
  uint32 dwordLength   = 0;
  volatile uint32 *base_addr;
  
  /* init function var*/
  stepLength = 1 << stepPower;
  dwordLength = length >> 2;
  base_addr = (uint32 *) base;
  	 
  /* write speed test by step defined*/
  for (sampleCycle = 0; sampleCycle != sampleSize; ++sampleCycle)
  {
    for (stepStart = 0; stepStart != stepLength; ++stepStart)
    {
      uint32 stepEnd = dwordLength - stepStart;
      for (stepCycle = 0; stepCycle < stepEnd;
          stepCycle += stepLength)
      {
        base_addr[stepStart + stepCycle] = stepStart + stepCycle;
      }
    }
  }	
  
  /* read verification for ram data retention */
  for (stepStart = 0; stepStart != stepLength; ++stepStart)
  {
    uint32 stepEnd = dwordLength - stepStart;
    for (stepCycle = 0; stepCycle < stepEnd;
        stepCycle += stepLength)
    {
      if (base_addr[stepStart + stepCycle] != stepStart + stepCycle)
      {
        snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
  	   	                                      base_addr + stepStart + stepCycle, base_addr[stepStart + stepCycle],stepStart + stepCycle);
        ddr_debug_output(str); 
        return FALSE;
      }
    }
  }
  return TRUE;
}

boolean ddr_interval_stress_test(uint32 const base, const uint32 size)
{
  uint32 i = 0;
  uint32 stepShmooBeg = 0;
  uint32 stepShmooEnd = MAX_STEP_POWER;
  
  /* print out related test information */
  snprintf(str,BUFFER_LENGTH, "\r\nMemory Test Space: 0x%x++(0x%x) %%BYTE\r\n"
        "\rInterval step ran ge: 0x%x~0x%x \r\n",
        base, size, stepShmooBeg, stepShmooEnd);
  ddr_debug_output(str);	
  ddr_debug_output("\rstep\t\tResult\r\n"); 
  
  for (i = stepShmooBeg; i != stepShmooEnd; i++)
  {
    if (!(ddr_interval_stress_test_per_step(base, size, i, DEFAULT_SAMPLE_SIZE)))
    {
      return FALSE;
    }
    else
    {
      snprintf(str,BUFFER_LENGTH, "\r\n%d\t\tok\r\n", 1 << i);
      ddr_debug_output(str); 	   	
    }
  }
  return TRUE;
}


boolean ddr_defect_interval_test
(  
  uint32            test_select,
  SDRAM_INTERFACE   interface,
  SDRAM_CHIPSELECT  chip_select
)
{   
  uint32 ddr_base = 0, ddr_size = 0;
  ddr_size_info ddr_info = ddr_get_size();
  
  /* get base address and size */
  if (interface == SDRAM_INTERFACE_0)
  {
    if (chip_select == SDRAM_CS0)
    {
      ddr_base = ddr_info.sdram0_cs0_addr;
      ddr_size = ddr_info.sdram0_cs0;
    }
    else if (chip_select == SDRAM_CS1)
    {
      ddr_base = ddr_info.sdram0_cs1_addr;
      ddr_size = ddr_info.sdram0_cs1;
    }
    else if (chip_select == SDRAM_BOTH)
    {
      ddr_base = (ddr_info.sdram0_cs0_addr < ddr_info.sdram0_cs1_addr) ?
                  ddr_info.sdram0_cs0_addr : ddr_info.sdram0_cs1_addr;
      ddr_size = ddr_info.sdram0_cs0 + ddr_info.sdram0_cs1;
    }
  }
  else if (interface == SDRAM_INTERFACE_1)
  {
    if (chip_select == SDRAM_CS0)
    {
      ddr_base = ddr_info.sdram1_cs0_addr;
      ddr_size = ddr_info.sdram1_cs0;
    }
    else if (chip_select == SDRAM_CS1)
    {
      ddr_base = ddr_info.sdram1_cs1_addr;
      ddr_size = ddr_info.sdram1_cs1;
    }
    else if (chip_select == SDRAM_BOTH)
    {
      ddr_base = (ddr_info.sdram1_cs0_addr < ddr_info.sdram1_cs1_addr) ?
                  ddr_info.sdram1_cs0_addr : ddr_info.sdram1_cs1_addr;
      ddr_size = ddr_info.sdram1_cs0 + ddr_info.sdram1_cs1;
    }
  }
  
  /* convert size from megabyte to byte */
  ddr_size <<= 20;
  
  if (ddr_size == 0)
  {
    return FALSE;
  }
  else if (!ddr_defect_interval_test_mem_map(ddr_base, ddr_size-1))
  {
    ddr_debug_output("\r\nThe memory size in words is not power of 2\r\n");
    return FALSE;
  }
  else if (ddr_base + ddr_size > min_base)
  {
    /* adjust base address and size based on minimum base address for test */
    if (ddr_base < min_base)
    {
      ddr_size -= (min_base - ddr_base);
      ddr_base = min_base;
    }	 
  
    ddr_size >>= 2;
  		
  	if(test_select == 0)
  	{
  	  if(!(ddr_function_defect_test(ddr_base, ddr_size - 1)))	
  	    return FALSE;
  	}
  	if (test_select == 1)
  	{
  	  if(!(ddr_interval_stress_test(ddr_base, ddr_size - 1)))
  	    return FALSE;
  	}		
  }
  
  return TRUE;
}

boolean ddr_basically_test(ddr_test_suite	 test_suite,uint32			 test_level ,SDRAM_CHIPSELECT  chip_select)
{
 
 /* These variables are modified by Trace32 */
   static volatile boolean			 is_dcache_on = FALSE;
   static volatile boolean			 is_test_all = TRUE;
   static volatile SDRAM_INTERFACE	 interface = SDRAM_INTERFACE_0;
   uint32 size = 0;
   boolean result = TRUE;
   uint32 base = 0;
 	
	 if (!is_dcache_on)
	 {
	   struct mem_block sbl_ddr_mem_block =
	   {
		 0, 0, 0,
		 MMU_L1_SECTION_MAPPING, MMU_L1_PAGETABLE_MEM_READ_WRITE,
		 MMU_L1_PAGETABLE_MEM_NON_CACHEABLE, MMU_L1_PAGETABLE_NON_EXECUTABLE_REGION,
		 MMU_PAGETABLE_DEF_DOMAIN
	   };
  
	   ddr_size_info ddr_size = ddr_get_size();

	   
	   if(ddr_size.sdram0_cs1 != 0)
		 base = (ddr_size.sdram0_cs0_addr < ddr_size.sdram0_cs1_addr) ?
					 ddr_size.sdram0_cs0_addr : ddr_size.sdram0_cs1_addr;
	   else
		 base = ddr_size.sdram0_cs0_addr;
  
	   size = (ddr_size.sdram0_cs0 + ddr_size.sdram0_cs1 +
					  ddr_size.sdram1_cs0 + ddr_size.sdram1_cs1) << 20;
  
	   //size -= (SCL_SBL1_DDR_DATA_BASE + SCL_SBL1_DDR_DATA_SIZE) - base;
  
	   //base = (SCL_SBL1_DDR_DATA_BASE + SCL_SBL1_DDR_DATA_SIZE);
  
	   sbl_ddr_mem_block.p_base = sbl_ddr_mem_block.v_base = base;
	   sbl_ddr_mem_block.size_in_kbytes = size >> 10;
	   
	   BL_VERIFY(boot_mmu_page_table_map_single_mem_block(mmu_get_page_table_base(), &sbl_ddr_mem_block),
                BL_ERR_MMU_PGTBL_MAPPING_FAIL|BL_ERROR_GROUP_BOOT);
	 }
if(test_level == 0 )
{
		result = ddr_test_addr_lines(base,size-1);
		result = ddr_test_data_lines(base,size-1);
}
if(test_level == 1)
{
	result = ddr_test_own_addr(base,size-1);
}
		
	 return result;
}
