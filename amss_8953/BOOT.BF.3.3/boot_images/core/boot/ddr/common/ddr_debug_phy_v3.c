/**
 * @file boot_ddr_debug.c
 * @brief
 * Implementation for DDR Debug Mode.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/common/ddr_debug_phy_v3.c#6 $
$DateTime: 2016/04/25 05:13:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
03/10/15   yps    Enable DQ and CA cdc sweep
10/13/14   yps    Mask watch dog reset funciton on 8936 platform
09/23/14   yps    Porting code from 8916 to 8936 platform
09/17/13   sl      Added more DDR tuning options.
09/13/13   sl      Fixed pslew/nslew order.
08/29/13   sl      Use USB APIs instead of Boot APIs.
08/20/13   sl      Added DDR tuning.
08/07/13   sr      Added watchdog reset support.
06/18/13   sl      Initial version.
================================================================================
                     Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include <locale.h>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "HAL_SEQ_DDR.h"
#include "ddr_test.h"
#include "msmhwiobase.h"
#include "boot_extern_clk_interface.h"
#include "HALhwio.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_power_interface.h"
#include "ddr_debug_common.h"
#include "ddi_firehose.h"
#include "ddr_phy_config.h"
#include "ddr_params.h"
#include "DALSys.h"
#include "ddrss_training.h"
#include "ddr_phy_technology.h"
#include "ddr_target.h"
#include "boot_extern_ddr_interface.h"
/*==============================================================================
                                  MACROS
==============================================================================*/
/* Length of transportation buffer in characters */
//#define BUFFER_LENGTH   512


/* Watchdog bark and bite time */
#define WATCHDOG_BARK_TIME_SCLK  0x7FFF
#define WATCHDOG_BITE_TIME_SCLK  0xFFFF

/* DDR tuning parameters */
#define DDR_TUNING_LOOP_COUNT  128
#define DQ_ROUT_MAX            0x7
#define DQ_PSLEW_MAX           0x3
#define DQ_NSLEW_MAX           0x3

#define RESET_DEBUG_SW_ENTRY_ENABLE  0x1
#define BIT_ERROR_STATISTICS
//#define SWEEP_WRITE_CDC_ONLY
uint32 BITFLIP_TEST_SIZE=1024*1024;
uint32 failures[4];
uint32 error_statistics =0;
const char *cmd;
clock_info_t g_clkInfo = {0, NULL};

uint32 ddrphy_dq_dqs_rout_cfg[][2] = 
{
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x6BA85B6A},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG), 0x6BA85B6C},		  
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{0x0, 0x0}
};

uint32 (*ddrphy_dq_dqs_rout_cfg_ptr)[2] = ddrphy_dq_dqs_rout_cfg;

uint32 ddrphy_ca0_rout_cfg[][2] = 
{
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x6BA85B6A},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG), 0x6BA85B6C},		  
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG), 0x6BA85B6C}, 
	{0x0, 0x0}
};

uint32 (*ddrphy_ca0_rout_cfg_ptr)[2] = ddrphy_ca0_rout_cfg;

uint32 ddrphy_ca1_rout_cfg[][2] = 
{
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x6BA85B6A},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG), 0x6BA85B6C},		  
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG), 0x6BA85B6C},	
	{HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG), 0x6BA85B6C}, 
	{0x0, 0x0}
};

uint32 (*ddrphy_ca1_rout_cfg_ptr)[2] = ddrphy_ca1_rout_cfg;



extern char pattern_name[][20];
/*==============================================================================
                                  DATA
==============================================================================*/
/* Echo and message buffers: must be addressable by USB and 4-byte aligned */


/* DDR tuning cookies */
extern void (*volatile reset_cb_func)(boolean);

__attribute__((section("DDR_DEBUG_TUNING_COOKIE")))
uint32 dq_rout, dq_pslew, dq_nslew, read_cdc_delay, write_cdc_delay,ca_rout;

int32 clock_index = -1;

boolean rdcdc_scan = FALSE;
uint8 coarse_sweep,fine_sweep;
uint32 vref = 0;
extern uint32 mc_fmax;
extern uint32 ddr_fmax;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
extern void logMessage(const char* format, ...);
pm_err_flag_type switch_cx_mx_to_turbo_mode(void)
{
	pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;


	err_flag = pm_smps_volt_level(0,PM_SMPS_1,1287500);
	if(err_flag != PM_ERR_FLAG__SUCCESS)
		return err_flag;
	

    err_flag = pm_ldo_volt_level(0,PM_LDO_3,1287500);
	return err_flag;
}
// LPDDR2 VDDQ ---> LDO2
pm_err_flag_type pm_comm_write_byte(uint32 slave_id, uint16 addr, uint8 data, uint8 priority);

pm_err_flag_type sbl1_ddr_debug_set_vddq(uint32 vol_uv)
{
	pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
	uint8 val = 0x42; // default value

	if(vol_uv > 1537500 || vol_uv < 375000)
	{
		err_flag =	PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
	}
	else
	{
		val = (vol_uv - 375000) / 12500;
	   err_flag = pm_comm_write_byte(1,0x4141, val, 1);
	}

	return err_flag;
}

/* =============================================================================
**  Function : ddr_debug_output
** =============================================================================
*/
/**
*   @brief
*   Output message for DDR Debug Mode.
*
*   @param[in]  msg  Message to be output
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_debug_output(const char *msg)
{


} /* ddr_debug_output */


extern pm_err_flag_type  pm_comm_write_byte(uint32 uSlaveId , 
                                          uint16 addr, 
                                          uint8 data, 
                                          uint8 priority);

void ddr_debug_dog_enable(boolean enable)
{
}



void ddr_debug_dog_kick( void )
{

}

void ddr_tuning_log_out(uint32 failures[4],uint32 tempresult[4],uint16 CDC_DELAY_START,uint16 WRITE_CDC_DELAY_MAX)
{
	/*if( ((failures[0]!=tempresult[0]) 
	  || (failures[1]!=tempresult[1]) 
	  || (failures[2]!=tempresult[2]) 
	  || (failures[3]!=tempresult[3]))
	  || failures[0]
	  || failures[1]
	  || failures[2]
	  || failures[3]
	  || (write_cdc_delay == CDC_DELAY_START)
	  || (write_cdc_delay == WRITE_CDC_DELAY_MAX))*/
	
	{
	
	  memset(str,0,BUFFER_LENGTH);
	  if( failures[0]==0
	   && failures[1]==0
	   && failures[2]==0
	   && failures[3]==0
		 )
	   {
		 snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
								"%3u,%3u,%3u,%3u.[log]%s\r\n",
								dq_rout, read_cdc_delay, write_cdc_delay,
								failures[0], failures[1], failures[2], failures[3],"[stress test]PASS");
	   }
	  else
	   {
		 snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
								"%3u,%3u,%3u,%3u.[log]%s\r\n",
								dq_rout, read_cdc_delay, write_cdc_delay,
								failures[0], failures[1], failures[2], failures[3],str_error_log);
	   }
	   logMessage(str);
	   memset(str_error_log,0,BUFFER_LENGTH);
	   tempresult[0]= failures[0];
	   tempresult[1]= failures[1];
	   tempresult[2]= failures[2];
	   tempresult[3]= failures[3];
	}

}



void ddr_tuning_init(uint16 *CDC_DELAY_START,uint16 *READ_CDC_DELAY_MAX,uint16 *WRITE_CDC_DELAY_MAX,uint8 * CDC_DELAY_INC)
{
 		//ddr_pre_clock_switch(0, 800000, SDRAM_INTERFACE_0);
		//Clock_SetBIMCSpeed(400000);
		//ddr_post_clock_switch(0, 800000, SDRAM_INTERFACE_0);   
		 *CDC_DELAY_START = 100;
   		 *READ_CDC_DELAY_MAX = 600;
         *WRITE_CDC_DELAY_MAX = 600 ;
         *CDC_DELAY_INC = 15;
		//ddr_debug_output("\r\n DDR test frequency on 800MHz\r\n");  
		//ddr_debug_output("\r\n Scan range[100,15,600]\r\n");
		//ddr_debug_output("\r\n Pass range[150,570]\r\n");
			
}


void ddr_debug_vddq_tuning(void)
{
}
void ddr_debug_param_init(void)
{
    uint32 reg;
	uint8 i;
	uint32   reg_ddrss_base = 0;
	reg_ddrss_base = ddrsns_share_data->base_addr.ddrss_base_addr;
	for(reg = 0; ddr_phy_dq_config_base[reg][0]!= 0; reg++)
	{
		for(i = 0; ddrphy_dq_dqs_rout_cfg_ptr[i][0]!=0 ; i++)
			{
			  if(ddrphy_dq_dqs_rout_cfg_ptr[i][0] == ddr_phy_dq_config_base[reg][0])
			  	ddrphy_dq_dqs_rout_cfg_ptr[i][1] = ddr_phy_dq_config_base[reg][1];
			}
	}
	for(reg = 0; ddr_phy_ca0_config_base[reg][0]!= 0; reg++)
	{
		for(i = 0; ddrphy_ca0_rout_cfg_ptr[i][0]!=0 ; i++)
			{
			  if(ddrphy_ca0_rout_cfg_ptr[i][0] == ddr_phy_ca0_config_base[reg][0])
			  	ddrphy_ca0_rout_cfg_ptr[i][1] = ddr_phy_ca0_config_base[reg][1];
			}
	}
	for(reg = 0; ddr_phy_ca1_config_base[reg][0]!= 0; reg++)
	{
		for(i = 0; ddrphy_ca1_rout_cfg_ptr[i][0]!=0 ; i++)
			{
			  if(ddrphy_ca1_rout_cfg_ptr[i][0] == ddr_phy_ca1_config_base[reg][0])
			  	ddrphy_ca1_rout_cfg_ptr[i][1] = ddr_phy_ca1_config_base[reg][1];
			}
	}

	 // Enable broadcast mode for all DQ PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x1E3C);
   ddr_phy_dq_set_config(ddrsns_share_data, (reg_ddrss_base + BROADCAST_BASE), ddrphy_dq_dqs_rout_cfg_ptr, NULL);
   
   // Enable broadcast mode for all CA PHYs on both channels
   // Enable broadcast mode CA0 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA0);
   ddr_phy_ca0_set_config(ddrsns_share_data,(reg_ddrss_base + BROADCAST_BASE), ddrphy_ca0_rout_cfg_ptr, NULL);

   // Enable broadcast mode CA1 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA1);
   ddr_phy_ca1_set_config(ddrsns_share_data,(reg_ddrss_base + BROADCAST_BASE), ddrphy_ca1_rout_cfg_ptr, NULL);	
      // Disable broadcast mode
      // Disable broadcast mode
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);
	
}
void DDI_DQ_Rout_setting(uint32 rout)
{
  uint32 i = 0;
  uint32   reg_ddrss_base = 0;
   for(i=0; ddrphy_dq_dqs_rout_cfg_ptr[i][0]!=0;i++)
  	{
  		ddrphy_dq_dqs_rout_cfg_ptr[i][1] = (ddrphy_dq_dqs_rout_cfg_ptr[i][1]& ~(0x7<<3)&~(0x7<<9))|(rout<<3)|(rout<<9);
  	}
   reg_ddrss_base = ddrsns_share_data->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all DQ PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x1E3C);
   ddr_phy_dq_set_config(ddrsns_share_data, (reg_ddrss_base + BROADCAST_BASE), ddrphy_dq_dqs_rout_cfg_ptr, NULL);	
      // Disable broadcast mode
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);
}

void DDI_DQs_Rout_setting(uint32 rout)
{
  uint32 i = 0;
  uint32   reg_ddrss_base = 0;
  for(i=0; ddrphy_dq_dqs_rout_cfg[i][0]!=0;i++)
  	{
  		ddrphy_dq_dqs_rout_cfg[i][1] = (ddrphy_dq_dqs_rout_cfg[i][1]& ~(0x7<<6)&~(0x7<<12))|(rout<<6)|(rout<<12);
  	}
   reg_ddrss_base = ddrsns_share_data->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all DQ PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x1E3C);
   ddr_phy_dq_set_config(ddrsns_share_data, (reg_ddrss_base + BROADCAST_BASE), ddrphy_dq_dqs_rout_cfg_ptr, NULL);	
}

void DDI_CA_Rout_setting(uint32 rout)
{
  uint32 i = 0;
  uint32   reg_ddrss_base = 0;
  //CA0 ROUT setting
   for(i=0; ddrphy_ca0_rout_cfg_ptr[i][0]!=0;i++)
  	{
  		ddrphy_ca0_rout_cfg_ptr[i][1] = (ddrphy_ca0_rout_cfg_ptr[i][1]& ~(0x7<<3)&~(0x7<<9))|(rout<<3)|(rout<<9);
  	}
   for(i=0; ddrphy_ca1_rout_cfg_ptr[i][0]!=0;i++)
  	{
  		ddrphy_ca1_rout_cfg_ptr[i][1] = (ddrphy_ca1_rout_cfg_ptr[i][1]& ~(0x7<<3)&~(0x7<<9))|(rout<<3)|(rout<<9);
  	}
   reg_ddrss_base = ddrsns_share_data->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all CA PHYs on both channels
   // Enable broadcast mode CA0 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA0);
   ddr_phy_ca0_set_config(ddrsns_share_data,(reg_ddrss_base + BROADCAST_BASE), ddrphy_ca0_rout_cfg_ptr, NULL);

   // Enable broadcast mode CA1 PHYs on both channels
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CA1);
   ddr_phy_ca1_set_config(ddrsns_share_data,(reg_ddrss_base + BROADCAST_BASE), ddrphy_ca1_rout_cfg_ptr, NULL);	
      // Disable broadcast mode
   HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);
}

void  ddr_write_pattern(const uint32 base,uint32 test_size,uint32 *val)
{

    volatile uint32 * base_addr = (uint32 *)base;
    uint32 i = 0;
	uint32 value;
	uint32 training_freq_table[TRAINING_NUM_FREQ_LPDDR3];
	DDR_STRUCT *ddr = ddrsns_share_data;
	DDR_CHANNEL channel = DDR_CH_BOTH;
	DDR_CHIPSELECT chip_select = DDR_CS_BOTH;
	uint8    training_ddr_freq_indx = 0;
	
    uint32 boot_ddr_freq;        //384000;
    uint32 dq0_ddr_phy_base = 0;
	uint8                  byte_lane = 0;
	 //ddr_interface_state ddr_status;
	 value = *val;
    if((coarse_sweep != 0xFF)&&(fine_sweep!=0xff))
    {  

	DDRSS_Get_Training_Address(ddr);
    // Get ddr boot frequency
    boot_ddr_freq = ddr->misc.ddr_boot_freq;
	// Set DQ0 base for addressing
	dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(0) + DQ0_DDR_PHY_OFFSET;
    training_freq_table[0] = ddr->misc.clock_plan[clock_index].clk_freq_in_khz;  //768000 
	training_ddr_freq_indx = 0;

     /* Clock API to scale to low speed */
	 if(rdcdc_scan == TRUE)
	 {
	   // Switch to boot freq to do a low speed memory write to be used for read back during training
	     ddr_external_set_clk_speed (boot_ddr_freq);
		 
	   // Enable refresh so that memory contents are retained after low speed write
	     BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
	 }
	 else
	 {
		 
	   // Switch to max freq to do dq-dqs training
	     ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
	   
	   // Enable refresh so that memory contents are retained after low speed write
	     BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
	   // Initialize perbit and fine settings before WR training start.
			   for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
			   {
			   	 DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
											  coarse_sweep, 
											  1, 
											  1, 
											  0);
				 DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
											  fine_sweep, 
											  0, 
											  1, 
											  0);
				 DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
											  coarse_sweep, 
											  1, 
											  1, 
											  1);
				 DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
											  fine_sweep, 
											  0, 
											  1, 
											  1);
				  }
	 }
     }
	 switch(PATTERN)
	 {
	  case BITFLIP_PATTERN:
	  case CHECK_BOARD_PATTERN:
	  case BITSPREAD_PATTERN:
	  case SOLID_BITS_PATTERN:	
		 for (i = 0; i <= test_size; i++)
	      {
	          base_addr[i] = (i % 2) == 0 ? value : ~value;
	      }
	     break;
	  case WORKING_ONE_PATTERN:
	  case WORKING_ZERO_PATTERN:
	  	for (i = 0; i <= test_size; i++)
	    {
	      base_addr[i] = value;
	    }
		break;
	  case SEQ_INR_PARTTERN:
    	for (i = 0; i <= test_size; ++i)
  	    {
  	      base_addr[i] = value;
  	      ++value;
  	    }
		break;
	  case CUSTOMER_PARTTERN:
	  	break;
	 
	 }
}

boolean ddr_read_pattern(const uint32 base,uint32 test_size,uint32 *val)
{
#if 1
    volatile uint32 * base_addr = (uint32 *)base;
	uint32 i = 0;
	uint32 value;
	uint32 temp_value = 0;
	uint32 expect_value = 0;
	uint32 result = 0;
	uint32 idx = 0;
	uint32 training_freq_table[TRAINING_NUM_FREQ_LPDDR3];
	DDR_STRUCT *ddr = ddrsns_share_data;
	DDR_CHANNEL channel = DDR_CH_BOTH;
	DDR_CHIPSELECT chip_select = DDR_CS_BOTH;
	uint8    training_ddr_freq_indx = 0;
	
    uint32 boot_ddr_freq;        //384000;
    uint32 dq0_ddr_phy_base = 0;
	uint8                  byte_lane = 0;
	//ddr_interface_state ddr_status;
	 
	
	//ddr_status = ddr_get_status();
	//char pattern_name[20];
	value = *val;
    
    if((coarse_sweep != 0xFF)&&(fine_sweep!=0xff))
    {  
    DDRSS_Get_Training_Address(ddr);
	    // Get ddr boot frequency
    boot_ddr_freq = ddr->misc.ddr_boot_freq;
	 // Set DQ0 base for addressing
	 dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(0) + DQ0_DDR_PHY_OFFSET;
	 
	 // Populate training table with the training frequency in khz
	// training_freq_table[1] = ddr->misc.clock_plan[ddr->misc.ddr_num_clock_levels - 1].clk_freq_in_khz;  //931200 
	 training_freq_table[0] = ddr->misc.clock_plan[clock_index].clk_freq_in_khz;  //768000 
	 
	 training_ddr_freq_indx = 0;
	  /* Clock API to scale to low speed */
	  if(rdcdc_scan == TRUE)
	  {
		  // Switch to max freq to do dq-dqs training
			ddr_external_set_clk_speed (training_freq_table[training_ddr_freq_indx]);
		 
		 // Enable refresh so that memory contents are retained after low speed write
		   BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
		 // Initialize perbit and fine settings before WR training start.
		 for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
		 {
		   DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
										coarse_sweep, 
										1, 
										1, 
										0);
		   DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
										fine_sweep, 
										0, 
										1, 
										0);
		   DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
										coarse_sweep, 
										1, 
										1, 
										1);
		   DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
										fine_sweep, 
										0, 
										1, 
										1);
		 	}

	  }
	  else
	  {
		// Switch to boot freq to do a low speed memory write to be used for read back during training
		  ddr_external_set_clk_speed (boot_ddr_freq);
		  
		// Enable refresh so that memory contents are retained after low speed write
		  BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_ENABLE/*1 for enable*/);		  

	  }
     }
	 switch(PATTERN)
	 {
	  case BITFLIP_PATTERN:
	  case CHECK_BOARD_PATTERN:
	  case BITSPREAD_PATTERN:
	  case SOLID_BITS_PATTERN:	
		  for (i = 0; i <= test_size; i++)
	      {
	        temp_value = base_addr[i];
	  	    expect_value = (i % 2) == 0 ? value : ~value;
	        if (temp_value != expect_value)
	        {
	          memset(str_error_log,0,BUFFER_LENGTH);
	          if((temp_value!=expect_value))
	          {
	            if((coarse_sweep != 0xFF)&&(fine_sweep!=0xff))
                {  
	              result = temp_value ^ expect_value;
	            #ifdef BIT_ERROR_STATISTICS
	              error_statistics|=result;
			      if(rdcdc_scan == TRUE)
			 	    snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                              pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			      else
			   	    snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                              pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			    #else
	  		      if(rdcdc_scan == TRUE)
			 	    snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error data:0x%08x, expect data:0x%08x", 
	  	   	                                              pattern_name[PATTERN],(base_addr+i), temp_value, expect_value);
			      else
			   	    snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error data:0x%08x, expect data:0x%08x", 
	  	   	                                              pattern_name[PATTERN],(base_addr+i), temp_value, expect_value);
			     #endif
                 }
                 else
                 {
                   if(temp_value != base_addr[i])
                      snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), temp_value, expect_value); 
                   else
                      snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), temp_value, expect_value);
                 }
	  
	  		  }

	  	   
	          for (idx = 0; idx < 4; idx++)
	          {
	              	if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
	          }
	  					
	         // return FALSE;
	        }
	      }
	     break;
	  case WORKING_ONE_PATTERN:
	  case WORKING_ZERO_PATTERN:
	  	  /* verify */
		  for (i = 0; i <= test_size; i++)
		  {

		    temp_value = base_addr[i]; 
		    if (temp_value != value)
		    {
		    	
              if((coarse_sweep != 0xFF)&&(fine_sweep!=0xff))
              {  
                result = temp_value ^ value;
		      #ifdef BIT_ERROR_STATISTICS
	           error_statistics |= result;
			  
			   if(rdcdc_scan == TRUE)
			 	 snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			   else
			   	 snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			  #else
		      if(rdcdc_scan == TRUE)
		        snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		  	   	                             pattern_name[PATTERN],base_addr + i, base_addr[i], ((i % 2) == 0 ? value : ~value));
			  else
			  	
		        snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		  	   	                             pattern_name[PATTERN],base_addr + i, base_addr[i], ((i % 2) == 0 ? value : ~value));
			  #endif
              }
              else
              {
                if(temp_value != base_addr[i])
                  snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), temp_value, value); 
                else
                  snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), temp_value, value); 
              }
	  	   
	          for (idx = 0; idx < 4; idx++)
	          {
	              	if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
	          }
		       // return FALSE;
		    }
			 
		  }
		break;
	  case SEQ_INR_PARTTERN:
	  	for (i = 0; i <= test_size; ++i)
		  {
		  	temp_value = base_addr[i]; 
		    if (temp_value != value)
		    {
		     result = temp_value ^ value;
		     #ifdef BIT_ERROR_STATISTICS
	           error_statistics|=result;
			   if(rdcdc_scan == TRUE)
			 	 snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			   else
			   	 snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:0x%x, error result:0x%08x, expect result:0x%08x", 
	  	   	                                            pattern_name[PATTERN],(base_addr+i), error_statistics, 0x0);
			  #else
		     if(rdcdc_scan == TRUE)
		      snprintf(str_error_log,BUFFER_LENGTH, "[%s]read failed at address:address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		  	   	                                       pattern_name[PATTERN], base_addr + i, base_addr[i], value);
			 else
			 	
 			  snprintf(str_error_log,BUFFER_LENGTH, "[%s]write failed at address:address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		  	   	                                       pattern_name[PATTERN],  base_addr + i, base_addr[i], value);
			#endif
			 
            for (idx = 0; idx < 4; idx++)
	          {
	              	if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
	          }
		      //return FALSE;
		    }
		    ++value;
		  }
		break;
		
	 }
	 if(failures[0]||failures[1]||failures[2]||failures[3])
	 	return FALSE;
        /* verify */
#endif
     return TRUE;
}

void ddr_debug_tuning_init()
{
	//HAL_DDR_Boot_Training(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH, DDR_TRAINING_MODE_INIT);
    boot_ddr_do_ddr_training();

}
boolean ddr_debug_dq_cdc_shmoo(void* inArgs)
{
	    uint8 loop_count=0;
	    uint16 training_period_1ps=0;
		static uint32 tempresult[4]={0xFF};
		// Training params structure
        static training_params_t training_params;
        training_params_t *training_params_ptr;
		DDR_STRUCT *ddr = ddrsns_share_data;
		uint32 training_freq_table[TRAINING_NUM_FREQ_LPDDR3];
		uint8 max_coarse=0,max_fine=0;
        
        arg_dqshmoo_p tmpDQArgs;
        uint8 rwType;


        if(NULL == inArgs)
        {
          return FALSE;
        }
        
        tmpDQArgs = (arg_dqshmoo_p)inArgs;

        // not good for following codes, may change in future
        clock_index = tmpDQArgs->cur_clocklevel;
        vref = tmpDQArgs->cur_vref;
        dq_rout = tmpDQArgs->cur_rout;
        start_address = tmpDQArgs->cur_startaddr;
        test_size = tmpDQArgs->cur_testsize;
        rwType = tmpDQArgs->cur_rwtype;

        
		// Training params structure pointer
        training_params_ptr = &training_params;

    
        ddr_debug_tuning_init();
    //================================================================================================//
    // Pre training setup
    //================================================================================================//
    // Set all training parameters to recommended values
        DDRSS_set_training_params(training_params_ptr);
	    training_freq_table[0]= ddr->misc.clock_plan[clock_index].clk_freq_in_khz;



		training_period_1ps = 1000000000/training_freq_table[0];
		
		max_coarse = ((training_period_1ps / 2) / COARSE_STEP_IN_PS);
		max_fine = COARSE_STEP_IN_PS/FINE_STEP_IN_PS;

	//===============================================================================================//
	//Print Common information 
	//===============================================================================================//
    	memset(str,0,BUFFER_LENGTH);
    	snprintf(str, BUFFER_LENGTH, "$Clock(%4u, 0, %4u)\r\n", training_freq_table[0],training_freq_table[0]);
    	logMessage(str);

	    if(vref!=0)
	    {
        snprintf(str, BUFFER_LENGTH, "$Vref(%4u, 0, %4u)\r\n",
								vref,vref);
		logMessage(str);
	    }
        
        memset(str,0,BUFFER_LENGTH);
        snprintf(str, BUFFER_LENGTH, "$Rout(%4u, 0, %4u)\r\n", dq_rout,dq_rout);
        logMessage(str);
        

        ddr_debug_param_init();
		DDI_DQ_Rout_setting(dq_rout);
	if(0 == rwType || 1 == rwType){
		rdcdc_scan = TRUE;
		for(coarse_sweep = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; coarse_sweep <= max_coarse; coarse_sweep+=training_params_ptr->rd_dqdqs.coarse_cdc_step)
		{
		 
		  for(fine_sweep = training_params_ptr->rd_dqdqs.fine_cdc_start_value; fine_sweep <= max_fine; fine_sweep+= training_params_ptr->rd_dqdqs.fine_cdc_step)
		  {
			
			for(loop_count = 0; loop_count < 1; loop_count++)
			{
			 failures[0] = failures[1] = failures[2] = failures[3] = 0;
			 error_statistics = 0;
			 ddr_function_defect_test((uint32)start_address, test_size);
			 read_cdc_delay = coarse_sweep*COARSE_STEP_IN_PS+fine_sweep*FINE_STEP_IN_PS; 
			 write_cdc_delay = 0xffff;
			 
			 ddr_tuning_log_out(failures,tempresult,0,0);
			} // loop_count
		  } // fine_sweep
		} // coarse_sweep
		}
		if(0 == rwType || 2 == rwType){
		rdcdc_scan = FALSE;
		for(coarse_sweep = training_params_ptr->wr_dqdqs.coarse_cdc_start_value; coarse_sweep <= max_coarse; coarse_sweep+=training_params_ptr->wr_dqdqs.coarse_cdc_step)
		{
		 
		  for(fine_sweep = training_params_ptr->wr_dqdqs.fine_cdc_start_value; fine_sweep <= max_fine; fine_sweep+=training_params_ptr->wr_dqdqs.fine_cdc_step)
		  {
			
			for(loop_count = 0; loop_count < 1; loop_count++)
			{
			 failures[0] = failures[1] = failures[2] = failures[3] = 0;
			 error_statistics = 0;
			 ddr_function_defect_test((uint32)start_address, test_size);
			 read_cdc_delay = 0xffff;
			 write_cdc_delay = coarse_sweep*COARSE_STEP_IN_PS+fine_sweep*FINE_STEP_IN_PS;
			 ddr_tuning_log_out(failures,tempresult,0,0);
			} // loop_count
		  } // fine_sweep
		} // coarse_sweep
		}
		vref = 0;
        return TRUE;
}

#ifdef DDI_CA_SCAN_ENABLE
boolean ddr_debug_ca_cdc_sweep(void *inArgs)

{
    uint8  ch                = 0;
    uint32 reg_offset_dpe    = 0;

#if   DSF_CA_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_WRLVL_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_WR_DQDQS_TRAINING_EN
    uint8  cs                = 0;
#endif

//    DDR_CHIPSELECT qualified_cs = (DDR_CHIPSELECT)(chip_select & ddr->cdt_params[0].common.populated_chipselect);
    
#if DSF_CA_TRAINING_EN
    uint32 boot_ddr_freq;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint32 boot_ddr_freq;        //384000;
#endif

    int8   training_freq_indx         = 0;    
    //uint8  training_freq_indx         = 0; 
    int8   training_indx              = 0;
    uint8  max_training_ddr_freq_indx = TRAINING_NUM_FREQ_LPDDR3 - 1;  //Set the max number of training frequecies  
    uint8  prfs_index                 = TRAINING_START_PRFS; 
	DDR_STRUCT *ddr = ddrsns_share_data;
    uint8  max_clock_index            = ddr->misc.ddr_num_clock_levels - 1;
    uint32 curr_training_freq_khz     = 0;
    // Training params structure
    static training_params_t training_params;
    training_params_t *training_params_ptr;
 
    // Set aside an area to be used by training algorithm for computation local_vars area.
    // This area is declared as a static local array so as to get it into the ZI data section
    // of the executable. Training algorithms will receive a pointer to this area, and can
    // use it as a storage for what would otherwise have been local variables, consuming
    // huge amounts of stack space.
    static   ddrss_ca_vref_local_vars *local_vars;
    uint32 local_var_size = LOCAL_VARS_AREA_SIZE;

	DDR_CHANNEL channel = DDR_CH_BOTH;
	DDR_CHIPSELECT chip_select = DDR_CS_BOTH;
	uint8					   bit = 0;
	uint8	   nobe = 0;
	uint32 debug_msg_cnt=0;
	uint8 fail_byte[4]={0};
	char str_detail_log[128];
	boolean bResult = TRUE;
    arg_cashmoo_p tmpCAArgs;


    
    tmpCAArgs = (arg_cashmoo_p)inArgs;

    vref = tmpCAArgs->cur_vref;
    ca_rout = tmpCAArgs->cur_rout;
    bResult = ddr_external_get_buffer((void **)&local_vars, &local_var_size);
    if (bResult != TRUE)
    {
         return FALSE;
    }


   if(vref!=0){
   	memset(str,0,BUFFER_LENGTH);
    snprintf(str, BUFFER_LENGTH, "$Vref(%4u, 0, %4u)\r\n", vref,vref);
	logMessage(str);
   }
   	memset(str,0,BUFFER_LENGTH);
    snprintf(str, BUFFER_LENGTH, "$CA_Rout(%4u, 0, %4u)\r\n", ca_rout,ca_rout);
	logMessage(str);

    


   /* training data corrupted, memclr training data to force a retraining */
   memset(&ddrsns_share_data->flash_params, 0x0, sizeof(struct ddr_params_partition));
  
	  // Get ddr boot frequency
	  boot_ddr_freq = ddr->misc.ddr_boot_freq;
  
	  // Training params structure pointer
	  training_params_ptr = &training_params;
  
	  
  //================================================================================================//
  // Pre training setup
  //================================================================================================//
	  // Set all training parameters to recommended values
	  DDRSS_set_training_params(training_params_ptr);
	  DDRSS_Get_Training_Address(ddr);
  
  // Disable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
  BIMC_All_Periodic_Ctrl (ddr, channel, DDR_CS_BOTH, FEATURE_DISABLE/*0 for disable*/);
  
  for (ch = 0; ch < NUM_CH; ch++)
  {
	  reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
	  if ((channel >> ch) & 0x1)
	  {
		// Disable Power Down
		HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x0, 0x0);
	  }
  }

 //================================================================================================//
 // Training frequency loop
 //================================================================================================//
	 // Set the maximum training frequency index
	 //max_training_ddr_freq_indx = TRAINING_NUM_FREQ_LPDDR3 - 1;
  
	 // Train at selected number of training frequecies 
	 for (training_indx = max_training_ddr_freq_indx; training_indx >= 0; training_indx--) // loop for number of times training is to be done
	 {
		 for(training_freq_indx = max_clock_index; training_freq_indx > 0; training_freq_indx--)// loop for clock frequency table
		 {
			 if((ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] ) && (prfs_index > 0))
			 {
				 while( (prfs_index > 0) && (ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz < freq_range[prfs_index -1]) )
				 {
					 prfs_index--;
				 }
				 break;
			 }
			 else if (prfs_index ==0)
			 {
				 if(ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] )
				 {
				  break;
				 }
			 }
		 }
 
		curr_training_freq_khz = ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz;
		max_clock_index		= training_freq_indx--;
		 
	   ddr_printf(DDR_NORMAL, "\n========================================================\n");
	   ddr_printf(DDR_NORMAL, "START: DDR training at freq: %u, prfs level at: %d .\n", curr_training_freq_khz,prfs_index);
	   
	   ddr_printf(DDR_NORMAL,"========================================================\n\n");
	   memset(str,0,BUFFER_LENGTH);
       snprintf(str, BUFFER_LENGTH, "$Clock(%4u, 0, %4u)\r\n", curr_training_freq_khz,curr_training_freq_khz);
	   logMessage(str); 
	   memset(str,0,BUFFER_LENGTH);
       snprintf(str, BUFFER_LENGTH, "$prfs_index(%4u, 0, %4u)\r\n", prfs_index,prfs_index);
	   logMessage(str); 
	   // Switch to the training frequency
	   ddr_external_set_clk_speed (curr_training_freq_khz);
	   //================================================================================================//
	   // DCC
	   //================================================================================================//
#if DSF_PASS_1_DCC_TRAINING_EN 
		  
			   // Train DCC at the higest frequency( highest prfs band)
			   if (training_indx == max_training_ddr_freq_indx)
			   {
				 // DCC Training for WRLVL CDC, WR90 CDC and IO DCC at training freq
				 ddr_printf(DDR_NORMAL, "\n  START: DCC training at freq: %u\n", curr_training_freq_khz);
				 DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, curr_training_freq_khz );  
				 ddr_printf(DDR_NORMAL, "\n  END: DCC training at freq: %u\n\n", curr_training_freq_khz);
	   
#ifdef TARGET_8992_COMPATIBLE
			   // Switch to the boot frequency
				 ddr_external_set_clk_speed (boot_ddr_freq);
	   
			   // Switch to the training frequency
				 ddr_external_set_clk_speed (curr_training_freq_khz);
#endif
			   }
	   
#endif // DSF_PASS_1_DCC_TRAINING_EN

//================================================================================================//
// CA-Vref
//================================================================================================//
#if DSF_CA_TRAINING_EN


        // Command Bus Training - CBT (CA, CS and Vref)
        for (ch = 0; ch < NUM_CH; ch++)
        {
            
            if ((channel >> ch) & 0x1)
            {
                
                for (cs = 0; cs < NUM_TRAINED_CS; cs++)
                {
                    if ((chip_select >> cs) & 0x1)
                    {         
                       ddr_printf(DDR_NORMAL, "\n  START: CA training on Ch: %u Rank: %u\n", ch, cs);

                       // CA Training call
                       DDRSS_ca_vref_lpddr3 (ddr,                                          
                                   (ddrss_ca_vref_local_vars *)local_vars,                                                     
                                             ch,
                                             cs,
                                             training_params_ptr,
                                             curr_training_freq_khz,
                                             training_indx,
                                             prfs_index
                                            );
                       
                       // Store local vars in flash and zero-initialize for use by the next training function.
                       //ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);
                       

                       ddr_printf(DDR_NORMAL, "\n  END: CA training on Ch: %u Rank: %u\n\n", ch, cs);
                    }            
                } // End of cs loop
                
              
            }
        }  // End of ch loop    
        
#endif  // DSF_CA_VREF_TRAINING_EN

	// Switch to the boot frequency
	ddr_external_set_clk_speed (boot_ddr_freq);

 
	// Disable Auto refresh at the beginning of training of each frequency
	BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_DISABLE/*0 for disable*/);		 

	// Put back the passed in chip_select saved at the beginning	
	//chip_select = qualified_cs;	
	ddr_printf(DDR_NORMAL,"========================================================\n");
	ddr_printf(DDR_NORMAL, "END: DDR training at freq: %u\n", curr_training_freq_khz);
	ddr_printf(DDR_NORMAL,"========================================================\n\n");

	
} // end of freq loop.


   for (ch = 0; ch < NUM_CH; ch++)
    {
        reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
        if ((channel >> ch) & 0x1)
        {
            // Enable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
            BIMC_All_Periodic_Ctrl (ddr, CH_1HOT(ch), 
                                   (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect, 
                                    FEATURE_ENABLE/*1 for enable*/);

            // Enable Power Down
            HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x1, 0x1);
        }
    }

   for(nobe = 0; nobe < HISTOGRAM_SIZE; nobe ++)
	{
		
		 for (bit = 0; bit < 10; bit ++)
		 {
		 	fail_byte[bit/8]|= (local_vars->failcount_histogram[2][0][bit][nobe]?1:0)<<(bit/8);
		 }

		
		if(0 == fail_byte[0] && 0 == fail_byte[1] && 0 == fail_byte[2] && 0 == fail_byte[3])
		{// pass
		  snprintf(str_detail_log,BUFFER_LENGTH, "[msg%u][CA TEST]pass",debug_msg_cnt);
		  snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
							           "%3u,%3u,%3u,%3u.[log]%s\r\n",
							           0xffff, ((nobe/FINE_STEPS_PER_COARSE)*COARSE_STEP_IN_PS+(nobe%FINE_STEPS_PER_COARSE)*FINE_STEP_IN_PS), 0xffff,
							           fail_byte[0], fail_byte[1], fail_byte[2], fail_byte[3], str_detail_log);

		}
		else
		{
		  snprintf(str_detail_log,BUFFER_LENGTH, "[msg%u][CA TEST]failed, error result:0x%08x expect result:0x%08x", debug_msg_cnt, (fail_byte[1]<<8)|fail_byte[0],0);
		  snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
							           "%3u,%3u,%3u,%3u.[log]%s\r\n",
							           0xffff, ((nobe/FINE_STEPS_PER_COARSE)*COARSE_STEP_IN_PS+(nobe%FINE_STEPS_PER_COARSE)*FINE_STEP_IN_PS), 0xffff,
							           fail_byte[0], fail_byte[1], fail_byte[2], fail_byte[3], str_detail_log);
		}
		logMessage(str);
		memset(str,0,128);
		memset(str_detail_log,0,128);
		fail_byte[0]=0; fail_byte[1]=0; fail_byte[2]=0; fail_byte[3]=0;
		debug_msg_cnt++;
   	}
									
    vref = 0;
    return TRUE;
}

#endif

extern void boot_busywait(uint32 pause_time_us);

boolean ddr_debug_get_clock_plan(char *string,uint32 length)
{
  uint32		  nNumLevel=0;
  ClockPlanType   *pClockPlan;
  uint8 i;

  if(string !=NULL)
  {
  /* Call BIMCQuery to get the Num Perf Levels */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);

  if(pClockPlan != NULL)
  	{
	  /* Call BIMCQuery to get the Clock Plan */
	  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);
	  snprintf(string,length,"[Clocklevel:Frequency] ");
	  for(i=0;i< nNumLevel;i++)
	  	{
	  	  snprintf(string+strlen(string),length,"%d,%d; ",i,pClockPlan[i].nFreqKHz);
	  	}
	  DALSYS_Free((void **)&pClockPlan);
	  return TRUE;
  	}
   
  }
  return FALSE;
  
}

void* ddr_debug_get_clock_info(void)
{
  uint32 nNumLevel = 0, ii = 0;
  ClockPlanType *pClockPlan;

  if(0 != g_clkInfo.ClockLevels && NULL != g_clkInfo.ClockLevels) return &g_clkInfo;

  /* Call BIMCQuery to get the Num Perf Levels */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);
  if(NULL == pClockPlan) return NULL;

  /* Call BIMCQuery to get the Clock Plan */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);

  g_clkInfo.ClockLevels = nNumLevel;

  g_clkInfo.ClockFreqKHz = (uint32*)malloc(nNumLevel * sizeof(uint32));
  if(NULL == g_clkInfo.ClockFreqKHz) return NULL;

  for(ii = 0; ii < nNumLevel; ii++){
    g_clkInfo.ClockFreqKHz[ii] = pClockPlan[ii].nFreqKHz;
  }

  g_clkInfo.mc_fmax = g_clkInfo.ClockFreqKHz[g_clkInfo.ClockLevels-1];
  g_clkInfo.ddr_fmax = g_clkInfo.mc_fmax * DDR_CLOCK_RATIO;

  // invalid clock_index input, reset to -1
  if(clock_index > -1 && clock_index < g_clkInfo.ClockLevels){
    clock_index = -1;
  }
  if(NULL != pClockPlan) DALSYS_Free(pClockPlan);//need free pClockPlan
  return &g_clkInfo;
}
boolean ddr_debug_stress_test(void* inArgs)
{
  arg_stress_p tmpStressArgs;
  DDR_STRUCT *ddr = ddrsns_share_data;
  uint32 loop_count;
  uint32 stress_freq;
  if(NULL == inArgs)
  {
    return FALSE;
  }
  tmpStressArgs = (arg_stress_p)inArgs;
  stress_freq = ddr->misc.clock_plan[tmpStressArgs->cur_clocklevel].clk_freq_in_khz;  
  coarse_sweep = 0xff;
  fine_sweep = 0xff;
  boot_ddr_do_ddr_training();
  ddr_external_set_clk_speed (stress_freq);
  for(loop_count = 0; loop_count < tmpStressArgs->cur_looptime; loop_count++)
  {
    ddr_function_defect_test((uint32)tmpStressArgs->cur_startaddr, tmpStressArgs->cur_testsize);
    memset(str,0,BUFFER_LENGTH);
    snprintf(str, BUFFER_LENGTH, "loop %d finished!\r\n",(loop_count+1));
    logMessage(str);
  }
  return TRUE;
}

