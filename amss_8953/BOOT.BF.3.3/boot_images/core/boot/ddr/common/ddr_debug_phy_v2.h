#ifndef __DDR_DEBUG_PHY_V2_H__
#define __DDR_DEBUG_PHY_V2_H__

/*==============================================================================
                   Copyright 2012-2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"

//extern uint32 dq_rout;
//extern int32 clock_index;

/*==============================================================================
                                  Macro
==============================================================================*/
typedef struct {
  uint32 ClockLevels;
  uint32 *ClockFreqKHz;
  uint32 mc_fmax;
  uint32 ddr_fmax;
}clock_info_t, *clock_info_p;

typedef struct{
  uint32 cur_clocklevel;
  uint32 cur_vref;
  uint32 cur_rout;
  uint32 cur_rwtype;
  uint32 cur_startaddr;
  uint32 cur_testsize;
}arg_dqshmoo, *arg_dqshmoo_p;

typedef struct{
  uint32 cur_clocklevel;
  uint32 cur_vref;
}arg_cashmoo, *arg_cashmoo_p;

/*==============================================================================
                                  Function
==============================================================================*/
void ddr_debug_output(const char *msg);
void  ddr_write_pattern(const uint32 base,uint32 test_size,uint32 *val);
boolean ddr_read_pattern(const uint32 base,uint32 test_size,uint32 *val);

void ddr_debug_do_tuning(int lineType, int rwType, boolean is_training);

void* ddr_debug_get_clock_info(void); // return clock_info_p
boolean ddr_debug_dq_cdc_sweep(void* inArgs); // input arg_dqshmoo_p
boolean ddr_debug_ca_cdc_sweep(void* inArgs); //input arg_cashmoo_p


#endif

