#ifndef __DDR_DEBUG_PHY_V3_H__
#define __DDR_DEBUG_PHY_V3_H__


/*==============================================================================
                      Warning: This file is auto-generated
================================================================================
                   Copyright 2012-2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
/*==============================================================================
                                  Macro
==============================================================================*/
#define DDI_CA_SCAN_ENABLE
//#define DDI_VDDQ_TUNING_ENABLE



extern uint32 dq_rout;
extern uint32 ca_rout;
extern int32 clock_index;


typedef struct {
  uint32 ClockLevels;
  uint32 *ClockFreqKHz;
  uint32 mc_fmax;
  uint32 ddr_fmax;
}clock_info_t, *clock_info_p;

typedef struct{
  uint32 cur_clocklevel;
  uint32 cur_vref;
  uint32 cur_rout;
  uint32 cur_rwtype;
  uint32 cur_startaddr;
  uint32 cur_testsize;
}arg_dqshmoo, *arg_dqshmoo_p;

typedef struct{
  uint32 cur_rout;
  uint32 cur_vref;
}arg_cashmoo, *arg_cashmoo_p;

typedef struct{
  uint32 cur_clocklevel;
  uint32 cur_startaddr;
  uint32 cur_testsize;
  uint32 cur_looptime;
}arg_stress, *arg_stress_p;


/*==============================================================================
                                  Function
==============================================================================*/


/**
*   @brief
*   Output message for DDR Debug Mode.
*
*   @param[in]  msg  Message to be output
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_debug_output(const char *msg);
void ddr_debug_do_tuning(int lineType, int rwType, boolean is_training);
boolean ddr_debug_dq_cdc_shmoo(void* inArgs);
boolean ddr_debug_ca_cdc_sweep(void *inArgs);

void  ddr_write_pattern(const uint32 base,uint32 test_size,uint32 *val);
boolean ddr_read_pattern(const uint32 base,uint32 test_size,uint32 *val);
void ddr_debug_ca_scan(void);
void DDI_CA_Rout_setting(uint32 rout);
boolean ddr_debug_get_clock_plan(char *string,uint32 length);
void ddr_debug_param_init(void);
void* ddr_debug_get_clock_info(void);
boolean ddr_debug_stress_test(void* inArgs);

#endif

