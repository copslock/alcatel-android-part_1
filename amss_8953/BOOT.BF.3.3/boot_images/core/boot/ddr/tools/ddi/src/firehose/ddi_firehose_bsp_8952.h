/*==================================================================
 *
 * FILE:        deviceprogrammer_firehose.h
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2013 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose_bsp_8952.h#2 $ 
 *   $DateTime: 2016/04/21 08:39:38 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2016-03-10   qbz      Move to new framework
 * 2016-03-10   yps      Initial checkin
 */

#ifndef DDI_FIREHOSE_BSP_8952_H
#define DDI_FIREHOSE_BSP_8952_H

#include "com_dtypes.h"

#define DDI_PLATFORM      "MSM"
#define DDI_VERSION       9

#endif
