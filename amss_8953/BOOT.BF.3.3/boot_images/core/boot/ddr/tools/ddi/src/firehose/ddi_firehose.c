/*==================================================================
 *
 * FILE:        ddi_firehose.c
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2014 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose.c#7 $ 
 *   $DateTime: 2016/04/25 05:13:37 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2016-03-18   qbz     Re-contruct DDI framework, easier to maintain
 * 2014-10-15   sb      Updated for 8909
 * 2014-03-03   dks     Added MMC FFU Support
 * 2013-06-03   ah      Added legal header
 * 2013-05-31   ab      Initial checkin
 */

#include "string.h"
#include "stringl.h"
#include "ddi_firehose.h"
#include "deviceprogrammer_utils.h"
#include "deviceprogrammer_xml_parser.h"
#include "deviceprogrammer_security.h"
#include "qusb_common.h"
#include "qusb_al_bulk.h"
#include "qhsusb_al_bulk.h"
#include "boot_pbl_v1.h"
#include "boot_util.h"

#define FIREHOSE_XML_BUFFER_SIZE              4096
#define FIREHOSE_LOG_BUFFER_SIZE              4096
#define FIREHOSE_TEMP_BUFFER_SIZE             512
#define FIREHOSE_TEMP_LOG_BUFFER_SIZE         256
#define FIREHOSE_TEMP_ATTRIBUTE_BUFFER_SIZE   256
#define FIREHOSE_DEBUG_LOG_MESSAGE_SIZE       32
#define FIREHOSE_NUM_DEBUG_LOG_MESSAGES       32
#define FIREHOSE_SIGNED_HASH_PACKET_SIZE      8192
#define FIREHOSE_NUM_HASHES_IN_HASH_TABLE     256
#define FIREHOSE_TX_BUFFER_SIZE               4096

typedef enum response_t {
  NAK,
  ACK
} response_t;

typedef enum response_ddi_info_t {
  CMD,
  PAR
} response_ddi_info_t;

typedef enum firehose_error_t {
  FIREHOSE_SUCCESS,
  FIREHOSE_ERROR,
  FIREHOSE_TRANSPORT_ERROR,
  FIREHOSE_STORAGE_ERROR,
  FIREHOSE_VALIDATION_ERROR
} firehose_error_t;

typedef struct
{
  // Buffer which is passed to USB
  // Using a constant value instead of a #define since that would
  // give the impression that this value is flexible. It is currently not.
  byte* channel_buffer[2];

  // Main buffer which holds the XML file to be parsed
  byte xml_buffer[FIREHOSE_XML_BUFFER_SIZE];

  // Transmit buffer for any outgoing XML or data
  byte* tx_buffer;

  // XML reader
  xml_reader_t xml_reader;

  // XML writer
  xml_writer_t xml_writer;

  // Channel variables
  uint32 channel_error_code;
  SIZE_T channel_bytes_read;
  boolean channel_read_completed;
  boolean channel_write_completed;
  SIZE_T channel_bytes_written;
  SIZE_T channel_total_bytes_written;
  SIZE_T channel_total_bytes_read;
  SIZE_T channel_buffer_capacity;
  SIZE_T channel_num_errors;
  SIZE_T num_packets_received;
  boolean channel_buffer_queued;
  int channel_buffer_to_use;

  // Variables for validation mode
  boolean validation_enabled;
  boolean validation_signed_hash_received;
  byte validation_hash_buffer[FIREHOSE_NUM_HASHES_IN_HASH_TABLE][32];
  SIZE_T validation_hash_buffer_num_hashes;
  SIZE_T validation_hash_buffer_index;
  SIZE_T validation_hash_buffer_capacity;
  SIZE_T validation_hash_buffer_new_capacity;
  SIZE_T validation_table_count;
  uint8 validation_hash_value[32];

  // On target debug log buffer
  byte debug_log_buffer[FIREHOSE_NUM_DEBUG_LOG_MESSAGES][FIREHOSE_DEBUG_LOG_MESSAGE_SIZE];
  SIZE_T debug_log_position;

  // Performance measurement
  boolean skip_storage_writes;
  boolean hash_in_non_secure;
  boolean verbose;
  unsigned int ack_raw_data_every;
  long start_time;
  long end_time;
} firehose_protocol_t;

// These buffers are being allocated separately so as to make them easy to relocate
// because these are currently accessed directly by the hardware
byte main_channel_buffer_A[FIREHOSE_CHANNEL_BUFFER_SIZE]  __attribute__ ((section ("CHANNEL_BUFFER_A_SECTION"), zero_init));
byte main_channel_buffer_B[FIREHOSE_CHANNEL_BUFFER_SIZE]  __attribute__ ((section ("CHANNEL_BUFFER_B_SECTION"), zero_init));
byte transmit_buffer[FIREHOSE_TX_BUFFER_SIZE]  __attribute__ ((section ("TRANSMIT_BUFFER_SECTION"), zero_init));

static const char xml_header[] = "<\?xml version=\"1.0\" encoding=\"UTF-8\" \?>";

const char ddi_firehose_bool_name[2][DDI_FIREHOSE_SIZE_BOOLLEN]=
{
  "false",
  "true"
};

const char ddi_firehose_type_name[DDI_FIREHOSE_ARRAY_UINT32+1][DDI_FIREHOSE_SIZE_TYPELEN]=
{
  "",
  "int8",
  "uint8",
  "int16",
  "uint16",
  "int32",
  "uint32",
  "string",
  "bool",
  "array_uint32"
};

boot_pbl_shared_data_type *pbl_shared_global; // not used

firehose_protocol_t fh;

boolean quitDDI = FALSE; // flag to indicate if quit DDI is required

ddi_firehose_item_set_p ddi_firehose_package = NULL;

ddi_firehose_info_p *received_pars = NULL;

// following vals is used for processddicommand

int common_decimal_char_to_int(const char* inCharArr, boolean* convertRes)
{
  char *tmpChar;
  boolean isHead = TRUE;
  boolean isNegtive = FALSE;
  uint32 validBareResult = 0;
  uint8 targetNumEach[16];
  uint8 tagetNumCnt = 0;
  uint32 bitWeight = 0;
  uint8 ii = 0, jj = 0;

  if (NULL == inCharArr) {
    *convertRes = FALSE;
    return 0;
  }

  tmpChar = (char*)inCharArr;
  while (*tmpChar != '\0') {
    if (*tmpChar >= 0x30 && *tmpChar <= 0x39) {
      // ignore zeros in head
      if (0x30 == *tmpChar && TRUE == isHead) {
        tmpChar++;
        continue;
      }

      targetNumEach[tagetNumCnt] = *tmpChar - 0x30;
      tagetNumCnt++;
    }
    else if ('-' == *tmpChar) {
      if (TRUE == isHead) {//"-" can only appear in the head
        if (FALSE == isNegtive) {
          isNegtive = TRUE;
          tmpChar++;
          continue;
        }
        else {// more than one "-" in head
          *convertRes = FALSE;
          return 0;
        }
      }
      else {
        *convertRes = FALSE;
        return 0;
      }
    }
    else {
      *convertRes = FALSE;
      return 0;
    }

    isHead = FALSE;
    tmpChar++;
  }

  validBareResult = 0;
  for (ii = 0; ii<tagetNumCnt; ii++) {
    bitWeight = 1;
    for (jj = 0; jj < tagetNumCnt - ii - 1; jj++) {
      bitWeight *= 10;
    }
    validBareResult += bitWeight * targetNumEach[ii];
  }

  if (TRUE == isNegtive) {
    if (validBareResult > 0x80000000) {
      *convertRes = FALSE;
      return 0;
    }
    return ((int)validBareResult * -1);
  }
  else {
    if (validBareResult > 0x7ffffff) {
      *convertRes = FALSE;
      return 0;
    }
    return (int)validBareResult;
  }
}


firehose_error_t handleNop(void) {
  return FIREHOSE_SUCCESS;
}

void firehoseResetValidationVariables() {
  fh.num_packets_received = 0;

  fh.validation_hash_buffer_num_hashes = 0;
  fh.validation_hash_buffer_capacity = sizeof(fh.validation_hash_buffer) / sizeof(fh.validation_hash_value);
  fh.validation_hash_buffer_new_capacity = fh.validation_hash_buffer_capacity;
  fh.validation_hash_buffer_index = 0;
  fh.validation_table_count = 0;
  fh.validation_signed_hash_received = FALSE;
}

void initFirehoseProtocol(void)
{
#ifdef FEATURE_FIREHOSE_VALIDATION_ENABLED
  fh.validation_enabled = TRUE;
#else
  fh.validation_enabled = FALSE;
#endif
  fh.tx_buffer = transmit_buffer;
  // Channel variables
  fh.channel_buffer[0] = main_channel_buffer_A;
  fh.channel_buffer[1] = main_channel_buffer_B;
  fh.channel_error_code = 0;
  fh.channel_bytes_read = 0;
  fh.channel_read_completed = FALSE;
  fh.channel_total_bytes_read = 0;
  fh.channel_write_completed = FALSE;
  fh.channel_bytes_written = 0;
  fh.channel_total_bytes_written = 0;
  fh.channel_buffer_capacity = FIREHOSE_CHANNEL_BUFFER_SIZE;
  fh.channel_num_errors = 0;
  fh.channel_buffer_queued = FALSE;
  fh.channel_buffer_to_use = 0;

  fh.debug_log_position = 0;

  xmlInitReader(&(fh.xml_reader), NULL, 0);
  xmlInitWriter(&(fh.xml_writer),
                fh.tx_buffer,
                FIREHOSE_TX_BUFFER_SIZE);

  fh.skip_storage_writes = FALSE;
  fh.hash_in_non_secure = FALSE;
  fh.verbose = FALSE;
  fh.ack_raw_data_every = 0;

  firehoseResetValidationVariables();
}


void debugMessage(const char* format, ...) {
  char *temp_log_buffer = (char *) fh.debug_log_buffer[fh.debug_log_position];
  SIZE_T buffer_space = FIREHOSE_DEBUG_LOG_MESSAGE_SIZE;

  va_list args;
  va_start (args, format);

  vsnprintf(temp_log_buffer,
            buffer_space,
            format,
            args);
  va_end (args);
  fh.debug_log_position = (fh.debug_log_position + 1) % FIREHOSE_NUM_DEBUG_LOG_MESSAGES;
}

void packet_handle_incoming_buf (uint32 bytes_read, uint32 err_code) {
  debugMessage("cback %d err %d", bytes_read, err_code);
  fh.channel_error_code = err_code;
  if (err_code != BULK_SUCCESS) {
    fh.channel_bytes_read = 0;
    fh.channel_num_errors += 1;
    //qhsusb_al_bulk_cancel_rx();
  }
  else {
    fh.channel_bytes_read = bytes_read;
    fh.num_packets_received += 1;
  }

  fh.channel_total_bytes_read += bytes_read;
  fh.channel_read_completed = TRUE;
  return;
}

firehose_error_t readChannel(byte** packet_ptr, SIZE_T bytes_to_read, SIZE_T *bytes_read, boolean queue_only) {
  // Since the 'channel' variables are directly manipulated here, they must not be used by clients such
  // as handleProgram directly. Only the copies made by this function (packet_ptr, bytes_read) must
  // be used by those client functions

  if (fh.channel_buffer_queued == FALSE) {
    debugMessage("queue %d", bytes_to_read);
#ifndef FEATURE_FIREHOSE_QUSB
    qhsusb_al_bulk_receive(fh.channel_buffer[fh.channel_buffer_to_use], bytes_to_read, packet_handle_incoming_buf, &fh.channel_error_code);
#else
    qusb_al_bulk_receive(fh.channel_buffer[fh.channel_buffer_to_use], bytes_to_read, packet_handle_incoming_buf, &fh.channel_error_code);
#endif
    if (fh.channel_error_code != BULK_SUCCESS) return FIREHOSE_TRANSPORT_ERROR;

    fh.channel_read_completed = FALSE;
    fh.channel_bytes_read = 0;
    fh.channel_buffer_queued = TRUE;
  }

  if (queue_only == TRUE) return FIREHOSE_SUCCESS;

  while (fh.channel_read_completed == FALSE) {
#ifndef FEATURE_FIREHOSE_QUSB
    qhsusb_al_bulk_poll();
#else
    qusb_al_bulk_poll();
#endif
  }

  if (fh.channel_bytes_read > fh.channel_buffer_capacity) {
    debugMessage("read %i bufsize %i", fh.channel_bytes_read, fh.channel_buffer_capacity);
    return FIREHOSE_TRANSPORT_ERROR;
  }

  *bytes_read = fh.channel_bytes_read;
  *packet_ptr = fh.channel_buffer[fh.channel_buffer_to_use];
  fh.channel_buffer_queued = FALSE;
  fh.channel_buffer_to_use = 1 - fh.channel_buffer_to_use;

  if (fh.channel_error_code != BULK_SUCCESS) {
    return FIREHOSE_TRANSPORT_ERROR;
  }

  return FIREHOSE_SUCCESS;
}

void packet_handle_outgoing_buf(uint32 bytes_written, uint32 err_code) {
  fh.channel_bytes_written = bytes_written;
  fh.channel_total_bytes_written += bytes_written;
  fh.channel_error_code = err_code;
  fh.channel_write_completed = TRUE;
  return;
}

firehose_error_t writeChannel(byte* buffer, SIZE_T length) {
  fh.channel_write_completed = FALSE;
#ifndef FEATURE_FIREHOSE_QUSB
  qhsusb_al_bulk_transmit(buffer, length, packet_handle_outgoing_buf, &fh.channel_error_code);
#else
  qusb_al_bulk_transmit(buffer, length, packet_handle_outgoing_buf, &fh.channel_error_code);
#endif
  while (fh.channel_write_completed == FALSE) {
#ifndef FEATURE_FIREHOSE_QUSB
    qhsusb_al_bulk_poll();
#else
    qusb_al_bulk_poll();
#endif
  }
  return FIREHOSE_SUCCESS;
}

firehose_error_t sendXMLBuffer() {
  firehose_error_t retval = writeChannel(fh.xml_writer.buffer, fh.xml_writer.write_position);
  xmlWriterReset(&fh.xml_writer);
  return retval;
}

void logMessage(const char* format, ...) {
  int retval;
  int written;
  char temp_log_buffer[FIREHOSE_TEMP_LOG_BUFFER_SIZE] = {0};
  SIZE_T buffer_space = sizeof(temp_log_buffer);

  va_list args;
  va_start (args, format);

  retval = vsnprintf(temp_log_buffer,
                     buffer_space,
                     format,
                     args);
  va_end (args);
  written = MIN(retval, buffer_space);

  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                              (byte *) xml_header,
                              strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");

  xmlWriterRawStartTag(&fh.xml_writer, "log");
  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            "value",
                                            (byte *) temp_log_buffer,
                                            written);

  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");

  sendXMLBuffer();
}

void logDigest(char* prefix, byte* digest, SIZE_T length) {
  int i = 0;
  int written;
  char log_string[FIREHOSE_TEMP_BUFFER_SIZE] = { 0 };

  if (sizeof(log_string) < ((length * 2) + strlen(prefix) + 1)) {
    return;
  }

  written = snprintf(log_string,
                     sizeof(log_string),
                     "%s",
                     prefix);

  while (written < sizeof(log_string) && i < length) {
    written += snprintf(log_string + written,
                        sizeof(log_string) - written,
                        "%02X",
                        digest[i]);
    i++;
  }

  ddi_firehose_print_debug(log_string);
}

firehose_error_t sendResponseFields(response_t response, int n_args, ...) {
  va_list args;
  int i;

  char format;
  int int_arg;
  SIZE_T size_arg;
  char char_arg;
  char* string_arg;
  char *attribute_name;
  char temp_attribute_buffer[FIREHOSE_TEMP_ATTRIBUTE_BUFFER_SIZE] = {0};
  int written;

  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                              (byte *) xml_header,
                              strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");

  xmlWriterRawStartTag(&fh.xml_writer, "response");
  xmlWriterRawWriteAttribute(&fh.xml_writer,
                             "value",
                             (response == ACK) ? "ACK" : "NAK");

  va_start (args, n_args);
  for (i = 0; i < n_args; i++) {
    attribute_name = va_arg(args, char *);
    format = (char) va_arg(args, int);
    switch(format) {
      case 's':
        string_arg = va_arg(args, char *);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%s",
                           string_arg);
        break;
      case 'd':
        int_arg = va_arg(args, int);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%d",
                           int_arg);
        break;
      case 't':
        size_arg = va_arg(args, SIZE_T);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           SIZE_T_FORMAT,
                           size_arg);
        break;
      case 'c':
        char_arg = (char) va_arg(args, int);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%c",
                           char_arg);
        break;
      default:
        va_end (args);
        return FIREHOSE_ERROR;
    }

    if (written >= sizeof(temp_attribute_buffer)) {
      va_end (args);
      return FIREHOSE_ERROR;
    }
    xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                              attribute_name,
                                              (byte *) temp_attribute_buffer,
                                              written);
  }
  va_end (args);
  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");

  sendXMLBuffer();
  return FIREHOSE_SUCCESS;
}

firehose_error_t sendDDIResponseFields(const char *tag_name, int n_args, ...) {
  va_list args;
  int i;

  char format;
  int int_arg;
  int32 uint_arg;
  SIZE_T size_arg;
  char char_arg;
  char* string_arg;
  char *attribute_name;
  char temp_attribute_buffer[FIREHOSE_TEMP_ATTRIBUTE_BUFFER_SIZE] = {0};
  int written;

  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                              (byte *) xml_header,
                              strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");
  xmlWriterRawStartTag(&fh.xml_writer, tag_name);

  va_start (args, n_args);
  for (i = 0; i < n_args; i++) {
    attribute_name = va_arg(args, char *);
    format = (char) va_arg(args, int);
    switch(format) {
      case 's':
        string_arg = va_arg(args, char *);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%s",
                           string_arg);
        break;
      case 'd':
        int_arg = va_arg(args, int);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%d",
                           int_arg);
        break;
      case 'u':
        uint_arg = va_arg(args, uint32);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%u",
                           uint_arg);
        break;
      case 't':
        size_arg = va_arg(args, SIZE_T);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           SIZE_T_FORMAT,
                           size_arg);
        break;
      case 'c':
        char_arg = (char) va_arg(args, int);
        written = snprintf(temp_attribute_buffer,
                           sizeof(temp_attribute_buffer),
                           "%c",
                           char_arg);
        break;
      default:
        va_end (args);
        return FIREHOSE_ERROR;
    }

    if (written >= sizeof(temp_attribute_buffer)) {
      va_end (args);
      return FIREHOSE_ERROR;
    }
    xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                              attribute_name,
                                              (byte *) temp_attribute_buffer,
                                              written);
  }

  va_end (args);
  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");

  sendXMLBuffer();
  return FIREHOSE_SUCCESS;
}

firehose_error_t sendResponse(response_t response)
{
  return sendResponseFields(response, 0);
}

void firehoseResetValidation(void)
{
  firehoseResetValidationVariables();
  ddi_firehose_print_debug("Reset validation state. Expecting Digital signature");
}

firehose_error_t report_ddinfo(void)
{
  char temp_attribute_buffer[FIREHOSE_TEMP_ATTRIBUTE_BUFFER_SIZE] = {0};
  ddi_firehose_info_p tmpInfo;
  ddi_firehose_item_p tmpItems;
  ddi_firehose_params_p tmpItemParams;
  ddi_firehose_rspons_p tmpItemRspons;
  uint32 itemNum = 0;
  uint32 itemParamNum = 0;
  uint32 itemRsponNum = 0;
  int written;

  if(NULL == ddi_firehose_package) {
    ddi_firehose_print_debug("DDI item package not defined!");
    return FIREHOSE_ERROR;
  }

  if(NULL == ddi_firehose_package->item_arr){
    ddi_firehose_print_debug("No valid item in DDI package, NULL array!");
    return FIREHOSE_ERROR;
  }

  itemNum = 0;
  tmpItems = ddi_firehose_package->item_arr;
  while(NULL != tmpItems->item_name){
    tmpItems++;
    itemNum++;
  }
  if(0 == itemNum){
    ddi_firehose_print_debug("No valid item in DDI package, 0 item!");
    return FIREHOSE_ERROR;
  }
  xmlWriterReset(&fh.xml_writer);
  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                              (byte *) xml_header,
                              strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");
  xmlWriterRawStartTag(&fh.xml_writer, "ddidefinition");
  // ddi version
  written = snprintf(temp_attribute_buffer,
                     sizeof(temp_attribute_buffer),
                     "%d",
                     ddi_firehose_package->version);
  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            "version",
                                            (byte *) temp_attribute_buffer,
                                            written);
  // ddi platform
  written = snprintf(temp_attribute_buffer,
                     sizeof(temp_attribute_buffer),
                     "%s",
                     ddi_firehose_package->platform);
  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            "platform",
                                            (byte *) temp_attribute_buffer,
                                            written);
  // item num
  written = snprintf(temp_attribute_buffer,
                     sizeof(temp_attribute_buffer),
                     "%u",
                     itemNum);
  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            "commandnum",
                                            (byte *) temp_attribute_buffer,
                                            written);
  // ddi extra infomation
  if(NULL != ddi_firehose_package->info_arr){
    tmpInfo = ddi_firehose_package->info_arr;
    while(NULL != tmpInfo->info_key){
      written = snprintf(temp_attribute_buffer,
                         sizeof(temp_attribute_buffer),
                         "%s",
                         tmpInfo->info_val);
      xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                                tmpInfo->info_key,
                                                (byte *) temp_attribute_buffer,
                                                written);
      tmpInfo++;
    }
  }
  
  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");
  sendXMLBuffer();

  tmpItems = ddi_firehose_package->item_arr;
  while(NULL != tmpItems->item_name){
    // loop for param num
    itemParamNum = 0;
    if(NULL != tmpItems->item_params_arr){
      tmpItemParams = tmpItems->item_params_arr;
      while(NULL != tmpItemParams->param_name){
        itemParamNum++;
        tmpItemParams++;
      }
    }

    // loop for response num
    itemRsponNum = 0;
    if(NULL != tmpItems->item_rspons_arr){
      tmpItemRspons = tmpItems->item_rspons_arr;
      while(NULL != tmpItemRspons->rspons_name){
        itemRsponNum++;
        tmpItemRspons++;
      }
    }
    
    // send command defination
    sendDDIResponseFields(
      "commandefinition",
      4,
      "name",          's', tmpItems->item_name,
      "version",       'u', tmpItems->item_ver,
      "parameternum",  'u', itemParamNum,
      "responsenum",   'u', itemRsponNum
    );

    // send parameter defination
    if(NULL != tmpItems->item_params_arr){
      tmpItemParams = tmpItems->item_params_arr;
      while(NULL != tmpItemParams->param_name){
        if(DDI_FIREHOSE_UINT8 == tmpItemParams->param_type ||
           DDI_FIREHOSE_UINT16 == tmpItemParams->param_type ||
           DDI_FIREHOSE_UINT32 == tmpItemParams->param_type){
          sendDDIResponseFields(
            "parameterdefiniton",
            5,
            "name",          's', tmpItemParams->param_name,
            "type",          's', ddi_firehose_type_name[tmpItemParams->param_type],
            "min",           'u', tmpItemParams->param_min,
            "max",           'u', tmpItemParams->param_max,
            "default",       'u', tmpItemParams->param_default
          );
        }
        else if(DDI_FIREHOSE_INT8 == tmpItemParams->param_type ||
                DDI_FIREHOSE_INT16 == tmpItemParams->param_type ||
                DDI_FIREHOSE_INT32 == tmpItemParams->param_type){
          sendDDIResponseFields(
            "parameterdefiniton",
            5,
            "name",          's', tmpItemParams->param_name,
            "type",          's', ddi_firehose_type_name[tmpItemParams->param_type],
            "min",           'd', (int)tmpItemParams->param_min,
            "max",           'd', (int)tmpItemParams->param_max,
            "default",       'd', (int)tmpItemParams->param_default
          );
        }
        else{
          sendDDIResponseFields(
            "parameterdefiniton",
            3,
            "name",          's', tmpItemParams->param_name,
            "type",          's', ddi_firehose_type_name[tmpItemParams->param_type],
            "default",       's', (char*)(int)tmpItemParams->param_default
          );
        }
        tmpItemParams++;
      }
    }

    // send response defination
    if(NULL != tmpItems->item_rspons_arr){
      tmpItemRspons = tmpItems->item_rspons_arr;
      while(NULL != tmpItemRspons->rspons_name){
        sendDDIResponseFields(
          "responsedefinition",
          2,
          "name",          's', tmpItemRspons->rspons_name,
          "type",          's', ddi_firehose_type_name[tmpItemRspons->rspons_type]
        );
        tmpItemRspons++;
      }
    }

    tmpItems++;
  }
#ifdef FREE_DDI_INFOMATION
  if(NULL != ddi_firehose_package->info_arr){
    tmpInfo = ddi_firehose_package->info_arr;
    while(NULL != tmpInfo->info_key){
      free(tmpInfo->info_key);
      tmpInfo->info_key = NULL;
      
      free(tmpInfo->info_val);
      tmpInfo->info_val = NULL;
      
      tmpInfo++;
    }
  }
#endif
  return FIREHOSE_SUCCESS;
}

firehose_error_t processddicommand(void)
{
  byte temp_buffer[FIREHOSE_TEMP_BUFFER_SIZE] = {0};
  ddi_firehose_item_p tmpItems, currItem = NULL;
  ddi_firehose_params_p tmpItemParams;
  uint32 currParamNum = 0;
  uint32 paramCnt = 0, initCnt = 0;
  boolean *isGotPar = NULL;
  boolean *isEntityMalloc = NULL;
  void **tmpOutArgPre = NULL, **tmpOutArgTest = NULL;
  uint32 tmpCvntUint = 0;
  int32 tmpCvntInt = 0;
  boolean cvntRes = FALSE;
  firehose_error_t retReslut = FIREHOSE_SUCCESS;
  
  if(NULL == ddi_firehose_package){
    ddi_firehose_print_debug("DDI item package not defined!");
    retReslut = FIREHOSE_ERROR;
    goto FINSIHED_DDI_CMD_PROCESS;
  }

  if(NULL == ddi_firehose_package->item_arr){
    ddi_firehose_print_debug("No valid item in DDI package!");
    retReslut = FIREHOSE_ERROR;
    goto FINSIHED_DDI_CMD_PROCESS;
  }

  // to match item
  currItem = NULL;
  tmpItems = ddi_firehose_package->item_arr;
  while(NULL != tmpItems->item_name){
    if(xmlIsTag(&fh.xml_reader,tmpItems->item_name)){
      currItem = tmpItems;
      break;
    }
    tmpItems++;
  }
  if(NULL == currItem){
    ddi_firehose_print_debug("Command not found in DDI package!");
    retReslut = FIREHOSE_VALIDATION_ERROR;
    goto FINSIHED_DDI_CMD_PROCESS;
  }

  if(NULL != currItem->item_params_arr){
    // loop param num
    currParamNum=0;
    tmpItemParams = currItem->item_params_arr;
    while(NULL != tmpItemParams->param_name){
      currParamNum++;
      tmpItemParams++;
    }
    
    if(0 != currParamNum){//has param

      // init judge array
      isGotPar = (boolean*)malloc(currParamNum * sizeof(boolean));
      if(NULL == isGotPar){
        ddi_firehose_print_debug("Malloc for par judge array failed!");
        retReslut = FIREHOSE_ERROR;
        goto FINSIHED_DDI_CMD_PROCESS;
      }
      for(initCnt = 0; initCnt < currParamNum; initCnt++){
        isGotPar[initCnt] = FALSE;
      }

      // init entity malloc flag array
      isEntityMalloc = (boolean*)malloc(currParamNum * sizeof(boolean));
      if(NULL == isEntityMalloc){
        ddi_firehose_print_debug("Malloc for entity malloc flag array failed!");
        retReslut = FIREHOSE_ERROR;
        goto FINSIHED_DDI_CMD_PROCESS;
      }
      for(initCnt = 0; initCnt < currParamNum; initCnt++){
        isEntityMalloc[initCnt] = FALSE;
      }
      
      // Let's first clear the variables that the <configure> can
      // set, by assigning default values
      fh.skip_storage_writes = FALSE;
      fh.ack_raw_data_every = 0;
      fh.hash_in_non_secure = FALSE;
      fh.verbose = FALSE;
      while (xmlGetToken(&fh.xml_reader) == XML_TOKEN_ATTRIBUTE){// loop attributes
        paramCnt = 0;
        tmpItemParams = currItem->item_params_arr;
        while(NULL != tmpItemParams->param_name){//loop params
          if (xmlIsAttribute(&fh.xml_reader, tmpItemParams->param_name)){
            isGotPar[paramCnt] = TRUE;
            xmlGetAttributeValue(&fh.xml_reader, temp_buffer, sizeof(temp_buffer));
            if(DDI_FIREHOSE_UINT8 == tmpItemParams->param_type ||
               DDI_FIREHOSE_UINT16 == tmpItemParams->param_type ||
               DDI_FIREHOSE_UINT32 == tmpItemParams->param_type){
              // check uint
              tmpCvntUint = stringToNumber((const char *) temp_buffer, &cvntRes);
              if(FALSE == cvntRes){// convert not OK
                ddi_firehose_print_debug("Convert type(%s=%s) failed!",
                                         ddi_firehose_type_name[tmpItemParams->param_type],
                                         temp_buffer
                                         );
                retReslut = FIREHOSE_ERROR;
                goto FINSIHED_DDI_CMD_PROCESS;
              }
              
              if(tmpCvntUint < tmpItemParams->param_min || tmpCvntUint > tmpItemParams->param_max){
                ddi_firehose_print_debug("<%s> out of range(%d~%d)",
                                         tmpItemParams->param_name,
                                         tmpItemParams->param_min,
                                         tmpItemParams->param_max
                                         );
                retReslut = FIREHOSE_ERROR;
                goto FINSIHED_DDI_CMD_PROCESS;
              }
              
              // asign value to entity
              if(DDI_FIREHOSE_UINT8 == tmpItemParams->param_type){
                if(tmpCvntUint > 255){
                  ddi_firehose_print_debug("Type(%s=%d) overflow",
                                           ddi_firehose_type_name[tmpItemParams->param_type],
                                           tmpCvntUint
                                           );
                  retReslut = FIREHOSE_ERROR;
                  goto FINSIHED_DDI_CMD_PROCESS;
                }
                    
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (uint8*)malloc(sizeof(uint8));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((uint8*)tmpItemParams->param_entity) = (uint8)tmpCvntUint;
              }
              else if(DDI_FIREHOSE_UINT16 == tmpItemParams->param_type){
                if(tmpCvntUint > 65535){
                  ddi_firehose_print_debug("Type(%s=%d) overflow",
                                           ddi_firehose_type_name[tmpItemParams->param_type],
                                           tmpCvntUint
                                           );
                  retReslut = FIREHOSE_ERROR;
                  goto FINSIHED_DDI_CMD_PROCESS;
                }
                
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (uint16*)malloc(sizeof(uint16));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((uint16*)tmpItemParams->param_entity) = (uint16)tmpCvntUint;
              }
              else if(DDI_FIREHOSE_UINT32 == tmpItemParams->param_type){
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (uint32*)malloc(sizeof(uint32));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((uint32*)tmpItemParams->param_entity) = tmpCvntUint;
              }
              else{
              }
            }
            else if(DDI_FIREHOSE_INT8 == tmpItemParams->param_type ||
                    DDI_FIREHOSE_INT16 == tmpItemParams->param_type ||
                    DDI_FIREHOSE_INT32 == tmpItemParams->param_type){
              //check int
              tmpCvntInt = common_decimal_char_to_int((const char *) temp_buffer, &cvntRes);
              if(FALSE == cvntRes){
                ddi_firehose_print_debug("Convert type(%s=%s) failed!",
                                         ddi_firehose_type_name[tmpItemParams->param_type],
                                         temp_buffer
                                         );
                retReslut = FIREHOSE_ERROR;
                goto FINSIHED_DDI_CMD_PROCESS;
              }

              if(tmpCvntInt < (int)tmpItemParams->param_min || tmpCvntInt > (int)tmpItemParams->param_max){
                ddi_firehose_print_debug("<%s> out of range(%d~%d)",
                                         tmpItemParams->param_name,
                                         (int)tmpItemParams->param_min,
                                         (int)tmpItemParams->param_max
                                         );
                retReslut = FIREHOSE_ERROR;
                goto FINSIHED_DDI_CMD_PROCESS;
              }

              // asign value to entity
              if(DDI_FIREHOSE_INT8 == tmpItemParams->param_type){
                if(tmpCvntInt > 127 || tmpCvntInt <-128){
                  ddi_firehose_print_debug("Type(%s=%d) overflow",
                                           ddi_firehose_type_name[tmpItemParams->param_type],
                                           tmpCvntInt
                                           );
                  retReslut = FIREHOSE_ERROR;
                  goto FINSIHED_DDI_CMD_PROCESS;
                }
                
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (int8*)malloc(sizeof(int8));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((int8*)tmpItemParams->param_entity) = (int8)tmpCvntInt;
              }
              else if(DDI_FIREHOSE_INT16 == tmpItemParams->param_type){
                if(tmpCvntInt > 32767 || tmpCvntInt <-32768){
                  ddi_firehose_print_debug("Type(%s=%d) overflow",
                                           ddi_firehose_type_name[tmpItemParams->param_type],
                                           tmpCvntInt
                                           );
                  retReslut = FIREHOSE_ERROR;
                  goto FINSIHED_DDI_CMD_PROCESS;
                }
                
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (int16*)malloc(sizeof(int16));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((int16*)tmpItemParams->param_entity) = (int16)tmpCvntInt;
              }
              else if(DDI_FIREHOSE_INT32 == tmpItemParams->param_type){
                if(NULL == tmpItemParams->param_entity){
                  tmpItemParams->param_entity = (int32*)malloc(sizeof(int32));
                  if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                  isEntityMalloc[paramCnt] = TRUE;
                }
                if(NULL != tmpItemParams->param_entity) *((int32*)tmpItemParams->param_entity) = (int32)tmpCvntInt;
              }
              else{
              }
            }
            else if(DDI_FIREHOSE_STRING == tmpItemParams->param_type){
              //check string
              if(NULL == tmpItemParams->param_entity){
                tmpItemParams->param_entity = (char*)malloc(DDI_FIREHOSE_SIZE_INFOVAL);
                if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                isEntityMalloc[paramCnt] = TRUE;
              }
              if(NULL != tmpItemParams->param_entity) {
                qmemcpy((char*)tmpItemParams->param_entity, (const char*)temp_buffer, DDI_FIREHOSE_SIZE_INFOVAL-1);
                ((char*)tmpItemParams->param_entity)[DDI_FIREHOSE_SIZE_INFOVAL-1] = '\0';
              }
            }
            else if(DDI_FIREHOSE_BOOLEAN == tmpItemParams->param_type){
              //check boolean
              if(NULL == tmpItemParams->param_entity){
                tmpItemParams->param_entity = (boolean*)malloc(sizeof(boolean));
                if(NULL == tmpItemParams->param_entity) goto FINSIHED_DDI_CMD_PROCESS;
                isEntityMalloc[paramCnt] = TRUE;
              }
              if('t' == temp_buffer[0] && 
                 'r' == temp_buffer[1] && 
                 'u' == temp_buffer[2] && 
                 'e' == temp_buffer[3]){
                if(NULL != tmpItemParams->param_entity) *((boolean*)tmpItemParams->param_entity) = TRUE;
              }
              else{
                if(NULL != tmpItemParams->param_entity) *((boolean*)tmpItemParams->param_entity) = FALSE;
              }
            }
            else if(DDI_FIREHOSE_ARRAY_UINT32 == tmpItemParams->param_type){
              //check array, not used, do nothing here, will define in future
            }
            else{
              ddi_firehose_print_debug("Type(%d) not supported!", tmpItemParams->param_type);
              retReslut = FIREHOSE_ERROR;
              goto FINSIHED_DDI_CMD_PROCESS;
            }
            break;
          }
          paramCnt++;
          tmpItemParams++;
        }
      }
      
      //to check if all parameters well init
      for(paramCnt = 0; paramCnt < currParamNum; paramCnt++){
        if(FALSE == isGotPar[paramCnt]){// quit or not??
          ddi_firehose_print_debug("Missing parameter(%s)!", (currItem->item_params_arr)[paramCnt].param_name);
          retReslut = FIREHOSE_ERROR;
          goto FINSIHED_DDI_CMD_PROCESS;
        }
      }

      // log out all the input parameters
      for(paramCnt = 0; paramCnt < currParamNum; paramCnt++){
        switch((currItem->item_params_arr)[paramCnt].param_type){
          case DDI_FIREHOSE_INT8:
            ddi_firehose_print_log("   [%s=%d]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((int8*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_UINT8:
            ddi_firehose_print_log("   [%s=%u]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((uint8*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_INT16:
            ddi_firehose_print_log("   [%s=%d]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((int16*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_UINT16:
            ddi_firehose_print_log("   [%s=%u]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((uint16*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_INT32:
            ddi_firehose_print_log("   [%s=%d]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((int32*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_UINT32:
            ddi_firehose_print_log("   [%s=%u]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   *((uint32*)((currItem->item_params_arr)[paramCnt].param_entity)));
            break;
          case DDI_FIREHOSE_STRING:
            ddi_firehose_print_log("   [%s=\"%s\"]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   (char*)((currItem->item_params_arr)[paramCnt].param_entity));
            break;
          case DDI_FIREHOSE_BOOLEAN:
            ddi_firehose_print_log("   [%s=\"%s\"]",
                                   (currItem->item_params_arr)[paramCnt].param_name,
                                   TRUE == *((boolean*)((currItem->item_params_arr)[paramCnt].param_entity)) ? "true" : "false");
            break;
          case DDI_FIREHOSE_ARRAY_UINT32:
            // in future
            break;
          default:
            // not supported
            break;
        }
      }
      ddi_firehose_print_log("");
    }
  }

  //init pretest par pointer
  tmpOutArgPre = (void**)malloc(sizeof(void*));
  if(NULL == tmpOutArgPre){
    retReslut = FIREHOSE_ERROR;
    goto FINSIHED_DDI_CMD_PROCESS;
  }
  *tmpOutArgPre = NULL;

  //init test par pointer
  tmpOutArgTest = (void**)malloc(sizeof(void*));
  if(NULL == tmpOutArgTest){
    retReslut = FIREHOSE_ERROR;
    goto FINSIHED_DDI_CMD_PROCESS;
  }
  *tmpOutArgTest = NULL;
  
  if(NULL != currItem->item_exec_pretest){// run pretest
    if(FALSE == (*currItem->item_exec_pretest)(currItem, tmpOutArgPre)){
      ddi_firehose_print_debug("Run pre-test failed!");
      retReslut = FIREHOSE_ERROR;
      goto FINSIHED_DDI_CMD_PROCESS;
    }
  }
    
  if(NULL != currItem->item_exec_dotest && NULL != tmpOutArgPre){// run test
    if(FALSE == (*currItem->item_exec_dotest)(currItem, *tmpOutArgPre, tmpOutArgTest)){
      ddi_firehose_print_debug("Run testing failed!");
      retReslut = FIREHOSE_ERROR;
      goto FINSIHED_DDI_CMD_PROCESS;
    }
  }

  if(NULL != currItem->item_exec_postest && NULL != tmpOutArgTest){// run posttest
    if(FALSE == (*currItem->item_exec_postest)(currItem, *tmpOutArgTest)){
      ddi_firehose_print_debug("Run post-test failed!");
      retReslut = FIREHOSE_ERROR;
      goto FINSIHED_DDI_CMD_PROCESS;
    }
  }

FINSIHED_DDI_CMD_PROCESS:
  //free all resources

  if(NULL != isEntityMalloc){
    for(paramCnt = 0; paramCnt < currParamNum; paramCnt++){
      if(TRUE == isEntityMalloc[paramCnt] && NULL != (currItem->item_params_arr)[paramCnt].param_entity){
        free((currItem->item_params_arr)[paramCnt].param_entity);
        (currItem->item_params_arr)[paramCnt].param_entity = NULL;
      }
    }
    free(isEntityMalloc);
    isEntityMalloc = NULL;
  }

  if(NULL != isGotPar){
    free(isGotPar);
    isGotPar = NULL;
  }

  if(NULL != tmpOutArgPre){
    free(tmpOutArgPre);
    tmpOutArgPre = NULL;
  }

  if(NULL != tmpOutArgTest){
    free(tmpOutArgTest);
    tmpOutArgTest = NULL;
  }
  
  return retReslut;
}

firehose_error_t processNextXMLTag(void)
{
  xml_token_t token = xmlGetToken(&fh.xml_reader);
  byte temp_buffer[FIREHOSE_TEMP_BUFFER_SIZE] = {0};
  firehose_error_t res;

  if (token == XML_TOKEN_NONE) {
    // If XML commands have been exhausted, get out of RAW data mode
    return FIREHOSE_ERROR;
  }
  else if (token == XML_TOKEN_TAG_ENDING) {
    // Check if </data> has been reached
    // Remove this check to be able to handle multiple tags/commands in a
    // single XML file
    xmlGetTag(&fh.xml_reader, temp_buffer, sizeof(temp_buffer));
    if (xmlIsTag(&fh.xml_reader, "data")) return FIREHOSE_ERROR;
  }
  else if (token == XML_TOKEN_TAG) {
    // found some valid tag
    if (xmlIsTag(&fh.xml_reader, "nop")) {
      return handleNop();
    }
    else if (xmlIsTag(&fh.xml_reader, "?xml") ||
             xmlIsTag(&fh.xml_reader, "data") ||
             xmlIsTag(&fh.xml_reader, "patches")) {
      // found start of XML file, i.e. <?xml version="1.0"?>, so just ignore
      // found next line of xml file we expect, <data> so ignore
    }
    else if(xmlIsTag(&fh.xml_reader, "ddinfo")) {
      res = report_ddinfo();
      if(FIREHOSE_SUCCESS == res){
      }
      else{
        sendResponse(NAK);
      }
    }
    else {
      res = processddicommand();
      if(FIREHOSE_SUCCESS == res) {
      }
      else if(FIREHOSE_VALIDATION_ERROR == res){
        if (FALSE == xmlGetTag(&fh.xml_reader, temp_buffer, sizeof(temp_buffer))) {
          ddi_firehose_print_debug("WARNING: Ignoring unrecognized tag too long to display. Continuing.");
        }
        else {
          ddi_firehose_print_debug("WARNING: Ignoring unrecognized tag '%s'. Continuing.", temp_buffer);
        }
        return sendResponse(ACK);
      }
      else{
        sendResponse(NAK);
      }
    }
  }
  return FIREHOSE_SUCCESS;
}

firehose_error_t validateXMLBuffer(void)
{
  xml_token_t token;
  byte opening_tag[FIREHOSE_TEMP_BUFFER_SIZE] = {0};
  byte closing_tag[FIREHOSE_TEMP_BUFFER_SIZE] = {0};

  do {
    token = xmlGetToken(&fh.xml_reader);
  } while (token != XML_TOKEN_NONE && token != XML_TOKEN_TAG);

  if (token == XML_TOKEN_NONE) return FIREHOSE_ERROR;

  if (!xmlIsTag(&fh.xml_reader, "?xml")) return FIREHOSE_ERROR;

  do {
    token = xmlGetToken(&fh.xml_reader);
  } while (token != XML_TOKEN_NONE && token != XML_TOKEN_TAG);

  if (token != XML_TOKEN_TAG) return FIREHOSE_ERROR;
  
  xmlGetTag(&fh.xml_reader, opening_tag, sizeof(opening_tag));

  while ((token = xmlGetToken(&fh.xml_reader)) != XML_TOKEN_NONE) {
    if (token == XML_TOKEN_TAG_ENDING) {
      xmlGetTag(&fh.xml_reader, closing_tag, sizeof(closing_tag));
      if (strncasecmp((const char *) opening_tag,
                       (const char *) closing_tag,
                       sizeof(opening_tag)) == 0) return FIREHOSE_SUCCESS;
    }
    else if (token == XML_TOKEN_TEXT) {
      return FIREHOSE_ERROR;
    }
  }

  return FIREHOSE_ERROR;
}

firehose_error_t recvNextPacket(byte** packet_ptr, SIZE_T bytes_to_read, SIZE_T *bytes_read, boolean queue_only) {
  boolean packet_validated = FALSE;
  byte* hash_location;
  SIZE_T signed_payload_size;
  SIZE_T len_copied;
  firehose_error_t read_retval = FIREHOSE_SUCCESS;

  if (fh.validation_enabled == TRUE) {
    // This code section handles Validated programming
    // if a failure occurs call the function to reset
    // all validation related variables to their initial
    // states

    while(packet_validated == FALSE) {
      if (fh.validation_signed_hash_received == FALSE) {

        read_retval = readChannel(packet_ptr, FIREHOSE_SIGNED_HASH_PACKET_SIZE, bytes_read, queue_only);
        if (read_retval != FIREHOSE_SUCCESS) {
          firehoseResetValidation();
          return read_retval;
        }
        if (queue_only == TRUE) return FIREHOSE_SUCCESS;
        ddi_firehose_print_debug("Validation is enabled.");
        // Verify the data in the receive buffer, then copy the data
        // into validation_hash_buffer

        if (authenticateSignedHash(*packet_ptr,
                                   *bytes_read,
                                   &hash_location,
                                   &signed_payload_size) == FALSE) {
          // Failed to authenticate the signed hash
          ddi_firehose_print_debug("ERROR: Failed to authenticate Digital Signature");
          ddi_firehose_print_debug("Resetting validation state");
          sendResponse(NAK);
          firehoseResetValidation();
          return FIREHOSE_VALIDATION_ERROR;
        }

        fh.validation_signed_hash_received = TRUE;

        // Resetting num_pacets so as to match zeno when
        // debugging validated image programming
        fh.num_packets_received = 0;

        fh.validation_hash_buffer_num_hashes = signed_payload_size / sizeof(fh.validation_hash_value);
        memscpy(
          &fh.validation_hash_buffer[fh.validation_hash_buffer_capacity - fh.validation_hash_buffer_num_hashes],
          sizeof(fh.validation_hash_buffer),
          hash_location,
          signed_payload_size);
        fh.validation_hash_buffer_index = fh.validation_hash_buffer_capacity - fh.validation_hash_buffer_num_hashes;
        sendResponse(ACK);
        continue;
      }
      else {
        if (fh.validation_hash_buffer_index == fh.validation_hash_buffer_capacity - 1) {
          read_retval = readChannel(packet_ptr,
                                    fh.validation_hash_buffer_new_capacity * sizeof(fh.validation_hash_value),
                                    bytes_read,
                                    queue_only);
        }
        else {
          read_retval = readChannel(packet_ptr, bytes_to_read, bytes_read, queue_only);
        }

        if (read_retval != FIREHOSE_SUCCESS) {
          firehoseResetValidation();
          return read_retval;
        }

        if (queue_only == TRUE) return FIREHOSE_SUCCESS;

        if (fh.validation_hash_buffer_num_hashes == 0) {
          ddi_firehose_print_debug("No more hashes to validate against");
          sendResponse(NAK);
          firehoseResetValidation();
          return FIREHOSE_VALIDATION_ERROR;
        }

        PerformSHA256(*packet_ptr,
                      *bytes_read,
                      fh.validation_hash_value);

        if (memcmp(
                    fh.validation_hash_value,
                    fh.validation_hash_buffer[fh.validation_hash_buffer_index],
                    sizeof(fh.validation_hash_value)) != 0) {
          // Invalid Hash value
          ddi_firehose_print_debug("ERROR: Hash value mismatch. Rejecting this packet.");
          logDigest("Expected:      ", fh.validation_hash_buffer[fh.validation_hash_buffer_index], sizeof(fh.validation_hash_value));
          logDigest("Found instead: ", fh.validation_hash_value, sizeof(fh.validation_hash_value));
          ddi_firehose_print_debug("(P%04d) (H%04d) Table %d", fh.num_packets_received - 1, fh.validation_hash_buffer_index, fh.validation_table_count);
          ddi_firehose_print_debug("Resetting validation state.");

          sendResponse(NAK);
          firehoseResetValidation();
          return FIREHOSE_VALIDATION_ERROR;
        }
        fh.validation_hash_buffer_num_hashes--;

        if (fh.validation_hash_buffer_index == fh.validation_hash_buffer_capacity - 1) {
          len_copied = MIN(
                            sizeof(fh.validation_hash_buffer),
                            *bytes_read);
          memscpy(fh.validation_hash_buffer,
                  sizeof(fh.validation_hash_buffer),
                  *packet_ptr,
                  len_copied);
          fh.validation_hash_buffer_index = 0;
          fh.validation_hash_buffer_num_hashes = len_copied / sizeof(fh.validation_hash_value);
          if (fh.validation_hash_buffer_capacity != fh.validation_hash_buffer_new_capacity) {
            fh.validation_hash_buffer_capacity = fh.validation_hash_buffer_new_capacity;
          }

          if (fh.validation_hash_buffer_num_hashes < 2) {
            debugMessage("read hashes %d", fh.validation_hash_buffer_num_hashes);
          }

          fh.validation_table_count++;
          sendResponse(ACK);
          continue;
        }
        else {
          fh.validation_hash_buffer_index++;
          packet_validated = TRUE;
        }
      }
    }
  }
  else {
    read_retval = readChannel(packet_ptr, bytes_to_read, bytes_read, queue_only);
    if (read_retval != FIREHOSE_SUCCESS) {
      return read_retval;
    }
    if (queue_only == TRUE) return FIREHOSE_SUCCESS;
    if (fh.hash_in_non_secure == TRUE) {
      PerformSHA256(*packet_ptr,
                    *bytes_read,
                    fh.validation_hash_value);
    }
  }
  return FIREHOSE_SUCCESS;
}

firehose_error_t getNextPacket(byte** packet_ptr, SIZE_T bytes_to_read, SIZE_T *bytes_read) {
  return recvNextPacket(packet_ptr, bytes_to_read, bytes_read, FALSE);
}

void readAndProcessXML() {
  SIZE_T bytes_read = 0;
  byte* packet;
  firehose_error_t read_retval;

  while (bytes_read == 0) {
    read_retval = getNextPacket(&packet, MIN(sizeof(fh.xml_buffer), fh.channel_buffer_capacity), &bytes_read);
  }

  if (FIREHOSE_SUCCESS != read_retval) return;

  xmlInitReader(&fh.xml_reader, packet, bytes_read);

  if (FIREHOSE_ERROR == validateXMLBuffer()) {
    ddi_firehose_print_debug("XML (%d bytes) not validated", bytes_read);
    sendResponse(NAK);
    xmlInitReader(&fh.xml_reader, NULL, 0);
    return;
  }
  
  if (sizeof(fh.xml_buffer) < bytes_read) {
    ddi_firehose_print_debug("XML file cannot fit in buffer");
    xmlInitReader(&fh.xml_reader, NULL, 0);
    return;
  }

  memscpy(fh.xml_buffer, sizeof(fh.xml_buffer), packet, bytes_read);
  xmlInitReader(&fh.xml_reader, fh.xml_buffer, bytes_read);

  while (processNextXMLTag() != FIREHOSE_ERROR) {
  }

  return;
}

void ddi_firehose_print_debug(const char* format, ...)
{
  char temp_log_buffer[FIREHOSE_TEMP_LOG_BUFFER_SIZE] = {0};
  SIZE_T buffer_space = sizeof(temp_log_buffer);

  va_list args;
  va_start (args, format);

  vsnprintf(temp_log_buffer,
            buffer_space,
            format,
            args);

  boot_log_message(temp_log_buffer);
}

void ddi_firehose_print_log(const char* format, ...)
{
  int retval;
  int written;
  char temp_log_buffer[FIREHOSE_TEMP_LOG_BUFFER_SIZE] = {0};
  SIZE_T buffer_space = sizeof(temp_log_buffer);
  
  va_list args;
  va_start (args, format);
  
  retval = vsnprintf(temp_log_buffer,
                     buffer_space,
                     format,
                     args);
  va_end (args);
  written = MIN(retval, buffer_space);
  
  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                              (byte *) xml_header,
                              strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");
  
  xmlWriterRawStartTag(&fh.xml_writer, "log");

  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            "value",
                                            (byte *) temp_log_buffer,
                                            written);
  
  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");
  
  sendXMLBuffer();
}

void ddi_firehose_print_response(const char* item_name, const char* rsp_name, const char* rsp_val)
{
  char temp_buffer[FIREHOSE_TEMP_ATTRIBUTE_BUFFER_SIZE] = {0};
  int written;

  xmlWriterRawWriteFromBuffer(&fh.xml_writer,
                                (byte *) xml_header,
                                strlen(xml_header));
  xmlWriterRawStartTagNoAttributes(&fh.xml_writer, "data");
  
  snprintf(temp_buffer,
           sizeof(temp_buffer),
           "%s%s",
           "response",item_name);
  
  xmlWriterRawStartTag(&fh.xml_writer, temp_buffer);

  written = snprintf(temp_buffer,
                     sizeof(temp_buffer),
                     "%s",
                     rsp_val);
  xmlWriterRawWriteAttributeValueFromBuffer(&fh.xml_writer,
                                            rsp_name,
                                            (byte *) temp_buffer,
                                            written);

  xmlWriterRawCloseTag(&fh.xml_writer);
  xmlWriterRawCloseTagName(&fh.xml_writer, "data");
  
  sendXMLBuffer();
}

boolean ddi_firehose_add_ddi_infomation(ddi_firehose_info **modInfo, const char *key, const char type, void *val)
{
  uint32 infoCnt = 0;
  ddi_firehose_info_p tmpDDInfo;

  if(NULL == modInfo) return FALSE;

  infoCnt = 0;
  tmpDDInfo = *modInfo;
  while(NULL != tmpDDInfo->info_key){
    if(infoCnt >= DDI_FIREHOSE_SIZE_INFO_MAX){//protect
      tmpDDInfo->info_key = NULL;
      tmpDDInfo->info_val = NULL;
      return FALSE;
    }
    tmpDDInfo++;
    infoCnt++;
  }

  tmpDDInfo->info_key = (char*)malloc(strlen(key) + 1);
  if(NULL == tmpDDInfo->info_key){
    // make sure info well ended
    tmpDDInfo->info_key = NULL;
    tmpDDInfo->info_val = NULL;
    return FALSE;
  }
  
  switch(type){
    case 'u':
      tmpDDInfo->info_val = (char*)malloc(DDI_FIREHOSE_SIZE_STR_INT32);
      if(NULL == tmpDDInfo->info_val){
        if(NULL != tmpDDInfo->info_key) free(tmpDDInfo->info_key);
        tmpDDInfo->info_key = NULL;
        tmpDDInfo->info_val = NULL;
        return FALSE;
      }
      snprintf(tmpDDInfo->info_val,
               DDI_FIREHOSE_SIZE_STR_INT32,
               "%u",
               *(uint32*)val);
      break;
    case 'd':
      tmpDDInfo->info_val = (char*)malloc(DDI_FIREHOSE_SIZE_STR_INT32);
      if(NULL == tmpDDInfo->info_val){
        if(NULL != tmpDDInfo->info_key) free(tmpDDInfo->info_key);
        tmpDDInfo->info_key = NULL;
        tmpDDInfo->info_val = NULL;
        return FALSE;
      }
      snprintf(tmpDDInfo->info_val,
               DDI_FIREHOSE_SIZE_STR_INT32,
               "%d",
               *(int32*)val);
      break;
    case 's':
      tmpDDInfo->info_val = (char*)malloc(strlen((char*)val) + 1);
      if(NULL == tmpDDInfo->info_val){
        if(NULL != tmpDDInfo->info_key) free(tmpDDInfo->info_key);
        tmpDDInfo->info_key = NULL;
        tmpDDInfo->info_val = NULL;
        return FALSE;
      }
      snprintf(tmpDDInfo->info_val,
               strlen((const char*)val) + 1,
               "%s",
               (const char*)val);
      break;
    case 'x':
      tmpDDInfo->info_val = (char*)malloc(DDI_FIREHOSE_SIZE_STR_INT32);
      if(NULL == tmpDDInfo->info_val){
        if(NULL != tmpDDInfo->info_key) free(tmpDDInfo->info_key);
        tmpDDInfo->info_key = NULL;
        tmpDDInfo->info_val = NULL;
        return FALSE;
      }
      snprintf(tmpDDInfo->info_val,
               DDI_FIREHOSE_SIZE_STR_INT32,
               "0x%x",
               *(uint32*)val);
      break;
    default:
      if(NULL != tmpDDInfo->info_key) free(tmpDDInfo->info_key);
      tmpDDInfo->info_key = NULL;
      tmpDDInfo->info_val = NULL;
      return FALSE;
  }
  snprintf(tmpDDInfo->info_key,
           DDI_FIREHOSE_SIZE_NAME,
           "%s",
           key);

  return TRUE;
}

void* ddi_firehose_fetch_entiy(const char* itemName, const char* parName, boolean* fetchRes)
{
  ddi_firehose_item_p tmpItems, currItem = NULL;
  ddi_firehose_params_p tmpItemParams;

  if(NULL == ddi_firehose_package){
    ddi_firehose_print_debug("DDI item package not defined!");
    if(NULL != fetchRes) *fetchRes = FALSE;
    return NULL;
  }

  if(NULL == ddi_firehose_package->item_arr){
    ddi_firehose_print_debug("No valid item in DDI package!");
    if(NULL != fetchRes) *fetchRes = FALSE;
    return NULL;
  }

  tmpItems = ddi_firehose_package->item_arr;
  while(NULL != tmpItems->item_name){
    if(0 == strncmp(tmpItems->item_name, itemName, DDI_FIREHOSE_SIZE_NAME)){
      currItem = tmpItems;
      break;
    }
    tmpItems++;
  }
  if(NULL == currItem){
    ddi_firehose_print_debug("Not found item(%s) in DDI package!", itemName);
    if(NULL != fetchRes) *fetchRes = FALSE;
    return NULL;
  }
  if(NULL == currItem->item_params_arr){
    ddi_firehose_print_debug("DDI item(%s) has no parameters!", itemName);
    if(NULL != fetchRes) *fetchRes = FALSE;
    return NULL;
  }

  tmpItemParams = currItem->item_params_arr;
  while(NULL != tmpItemParams->param_name){
    if(0 == strncmp(tmpItemParams->param_name, parName, DDI_FIREHOSE_SIZE_NAME)){
      if(NULL != fetchRes) *fetchRes = TRUE;
      return tmpItemParams->param_entity;
    }
    tmpItemParams++;
  }

  if(NULL != fetchRes) *fetchRes = FALSE;
  return NULL;
}

void ddi_firehose_update_item_set(ddi_firehose_item_set_p ddiItemSet)
{
  if(NULL != ddiItemSet) ddi_firehose_package = ddiItemSet;
}

void initFirehoseTransport(void)
{
#ifndef FEATURE_FIREHOSE_QUSB
  qhsusb_al_bulk_init(0x0, QHSUSB_MODE__SER_ONLY);
#else
  qusb_al_bulk_init(QUSB_MAX_SPEED_HIGH);
#endif
}

void ddi_firehose_main(void)
{
  boolean one_time_log = FALSE;
  ddi_firehose_item_p tmpItems;

  initFirehoseProtocol();
  initFirehoseTransport();

  if(NULL == ddi_firehose_package){
    ddi_firehose_print_log("Not found DDI command/item package defination!");
    return;
  }

  if(NULL == ddi_firehose_package->item_arr){
    ddi_firehose_print_debug("No valid item in DDI package!");
    return;
  }
  
  if(NULL != ddi_firehose_package->worker_exec_pre){
    if(FALSE == (*ddi_firehose_package->worker_exec_pre)(ddi_firehose_package)){
      ddi_firehose_print_log("Failed to run DDI prework!");
      return;
    }
  }

  tmpItems = ddi_firehose_package->item_arr;
  while(NULL != tmpItems->item_name){
    if(NULL != tmpItems->item_exec_init){
      if(FALSE == (*tmpItems->item_exec_init)(tmpItems)){
        ddi_firehose_print_log("Init item(%s) failed!", tmpItems->item_name);
      }
    }
    tmpItems++;
  }

  ddi_firehose_print_debug("Start DDI using firehose");
  while (FALSE == quitDDI){
    readAndProcessXML();
    if (one_time_log == FALSE) {
      one_time_log = TRUE;
      ddi_firehose_print_debug("Filehose logbuf@0x%08X fh@0x%08X", fh.debug_log_buffer, &fh);
    }
  }
  
  if(NULL != ddi_firehose_package->worker_exec_post){
    if(FALSE == (*ddi_firehose_package->worker_exec_post)(ddi_firehose_package)){
      ddi_firehose_print_log("Failed to run DDI postwork!");
      return;
    }
  }
}

