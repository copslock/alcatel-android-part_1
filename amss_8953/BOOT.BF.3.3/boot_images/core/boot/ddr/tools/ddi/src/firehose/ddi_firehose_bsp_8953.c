/*==================================================================
 *
 * FILE:        ddi_firehose_bsp_8917.c
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2013 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose_bsp_8953.c#2 $ 
 *   $DateTime: 2016/04/21 08:39:38 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ---------------------------------------------- 
 * 2016-03-14   qbz      Create for 8917 DDI
 */
#include DDI_FIREHOSE_BSP_H
#include DDR_DEBUG_PHY_H
#include "locale.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "ddi_firehose.h"
#include "boot_extern_clk_interface.h"
#include "boot_extern_ddr_interface.h"
#include "ddr_debug_common.h"
#include "DDIChipInfo.h"

#define DEBUG_DDI_8917

#define NAME_STR_RESULT    "Result"
#define NAME_STR_CLKLVL    "ClockLevel"
#define NAME_STR_VREF      "Vref"
#define NAME_STR_ROUT      "Rout"
#define NAME_STR_CA_ROUT   "CARout"
#define NAME_STR_BYTE      "Byte"
#define NAME_STR_RWTYPE    "RWType"
#define NAME_STR_STRTADDR  "Start_Address"
#define NAME_STR_TST_SIZE  "Test_Size"
#define NAME_STR_LOOP_TIME "Loop_time"
#define NAME_STR_ITEM_A    "TestingDDRSignalLines"
#define NAME_STR_ITEM_B    "DDRBasicllyTesting"
#define NAME_STR_ITEM_C    "DQLineShmoo"
#define NAME_STR_ITEM_D    "CALineShmoo"
#define NAME_STR_ITEM_E    "StressTest"

#define LEN_INT32_STR   12

boolean signalines_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs);
boolean signalines_postest(void* this_p, void *doneOutArgsObj);

boolean basic_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs);
boolean basic_postest(void* this_p, void *doneOutArgsObj);

boolean dqlineshmoo_init(void *this_p);
boolean dqlineshmoo_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs);
boolean dqlineshmoo_postest(void* this_p, void *doneOutArgsObj);

//boolean calineshmoo_init(void *this_p);
boolean calineshmoo_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs);
boolean calineshmoo_postest(void* this_p, void *doneOutArgsObj);

boolean stress_init(void* this_p);
boolean stress_dotest(void *this_p, void *preOutArgsObj, void **doneOutArgs);
boolean stress_postest(void *this_p, void *doneOutArgsObj);




boolean ddi_firehose_pre_work(void* this_p);
boolean ddi_firehose_post_work(void* this_p);


ddi_firehose_rspons item_result_rsp[] = {
  {NAME_STR_RESULT,    DDI_FIREHOSE_BOOLEAN},
  {NULL,               (ddi_firehose_type)NULL}
};

ddi_firehose_params item_dqlineshmoo_arg[] = {
  {NAME_STR_CLKLVL,   DDI_FIREHOSE_UINT32,          0,       0,              0,             NULL}, // max, default value will be updated in pre-work
  {NAME_STR_VREF,     DDI_FIREHOSE_UINT32,          0,       1200,           600,           NULL},
  {NAME_STR_ROUT,     DDI_FIREHOSE_UINT32,          0,       7,              5,             NULL},
//  {NAME_STR_BYTE,     DDI_FIREHOSE_UINT32,          0,       3,              0,             NULL},
  {NAME_STR_RWTYPE,   DDI_FIREHOSE_UINT32,          0,       2,              0,             NULL},
  {NAME_STR_STRTADDR, DDI_FIREHOSE_UINT32,          0,       0xffffffff,     0x80000000,    NULL},
  {NAME_STR_TST_SIZE, DDI_FIREHOSE_UINT32,          0,       0xffffffff,     1024 * 1024,   NULL},
  {NULL,              DDI_FIREHOSE_NULL,            NULL ,   NULL,           NULL,          NULL}
};

ddi_firehose_params item_calineshmoo_arg[] = {
  {NAME_STR_CA_ROUT,  DDI_FIREHOSE_UINT32,        0,        7,          5,        NULL}, // max, default value will be updated in pre-work
  {NAME_STR_VREF,     DDI_FIREHOSE_UINT32,        0,        1200,       600,      NULL},
  {NULL,              DDI_FIREHOSE_NULL,          NULL,     NULL,       NULL,     NULL}
};

ddi_firehose_params item_stress_arg[] = {
  {NAME_STR_CLKLVL,       DDI_FIREHOSE_UINT32,        0,        7,                7,               NULL}, // max, default value will be updated in pre-work
  {NAME_STR_STRTADDR,     DDI_FIREHOSE_UINT32,        0,        0xFFFFFFFF,       0x80000000,      NULL},  
  {NAME_STR_TST_SIZE,     DDI_FIREHOSE_UINT32,        0,        0xFFFFFFFF,       0x0,             NULL},
  {NAME_STR_LOOP_TIME,    DDI_FIREHOSE_UINT32,        0,        0xFFFFFFFF,       0x1,             NULL},
  {NULL,                  DDI_FIREHOSE_NULL,          NULL,     NULL,             NULL,            NULL}
};



ddi_firehose_item ddi_firehose_pack_items[] = {
  {NAME_STR_ITEM_A,    1,       NULL,                       item_result_rsp,   NULL,              NULL,  &signalines_dotest,   &signalines_postest},
  {NAME_STR_ITEM_B,    1,       NULL,                       item_result_rsp,   NULL,              NULL,  &basic_dotest,        &basic_postest},
  {NAME_STR_ITEM_C,    1,       item_dqlineshmoo_arg,       item_result_rsp,   &dqlineshmoo_init, NULL,  &dqlineshmoo_dotest,  &dqlineshmoo_postest},
  {NAME_STR_ITEM_D,    1,       item_calineshmoo_arg,       item_result_rsp,   NULL,              NULL,  &calineshmoo_dotest,  &calineshmoo_postest},
  {NAME_STR_ITEM_E,    1,       item_stress_arg,            item_result_rsp,   &stress_init,      NULL,  &stress_dotest     ,  &stress_postest},
  {NULL,               NULL,    NULL,                       NULL,              NULL,              NULL,  NULL,                 NULL}
};

ddi_firehose_info ddi_firehose_pack_info[DDI_FIREHOSE_SIZE_INFO_MAX] = {
  {NULL, NULL}
};

ddi_firehose_item_set ddi_firehose_pack = {
  DDI_PLATFORM,               //platform
  DDI_VERSION,                //version
  ddi_firehose_pack_info,     //info
  ddi_firehose_pack_items,    //items
  &ddi_firehose_pre_work,     //preworker
  &ddi_firehose_post_work     //postworker
};

boolean signalines_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs)
{
  *doneOutArgs = (boolean*)malloc(sizeof(boolean));
  if(NULL == *doneOutArgs) return FALSE;

  if(ddr_basically_test(DDR_TEST_SUITE_ACCESS_RAM_TEST,0,SDRAM_BOTH))
    *(boolean*)(*doneOutArgs) = TRUE;
  else
    *(boolean*)(*doneOutArgs) = FALSE;
  
  return TRUE;
}

boolean signalines_postest(void* this_p, void *doneOutArgsObj)
{
  ddi_firehose_item_p item_this;
  
  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  ddi_firehose_print_response(item_this->item_name,
                              NAME_STR_RESULT,
                              ddi_firehose_bool_name[*(boolean*)doneOutArgsObj]);

  free(doneOutArgsObj);
  return TRUE;
}

boolean basic_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs)
{
  *doneOutArgs = (boolean*)malloc(sizeof(boolean));
  if(NULL == *doneOutArgs) return FALSE;

  if(ddr_basically_test(DDR_TEST_SUITE_ACCESS_RAM_TEST,1,SDRAM_BOTH))
    *(boolean*)(*doneOutArgs) = TRUE;
  else
    *(boolean*)(*doneOutArgs) = FALSE;

  return TRUE;
}

boolean basic_postest(void* this_p, void *doneOutArgsObj)
{
  ddi_firehose_item_p item_this;

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  ddi_firehose_print_response(item_this->item_name,
                              NAME_STR_RESULT,
                              ddi_firehose_bool_name[*(boolean*)doneOutArgsObj]);
  free(doneOutArgsObj);
  return TRUE;
}

boolean dqlineshmoo_init(void* this_p)
{
  ddi_firehose_item_p item_this;
  clock_info_p tmpClockInfo;
  ddi_firehose_params_p tmpPar;
  ddr_size_info ddr_info;
  ddr_info = boot_ddr_get_size();

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  tmpClockInfo = ddr_debug_get_clock_info();
  if(NULL == tmpClockInfo) return FALSE;

  // update NAME_STR_CLKLVL
  if(NULL != item_this->item_params_arr){
    tmpPar = item_this->item_params_arr;
    while(NULL != tmpPar->param_name){
      if(0 == strncmp(tmpPar->param_name, NAME_STR_CLKLVL, DDI_FIREHOSE_SIZE_NAME)){
        tmpPar->param_max = tmpClockInfo->ClockLevels - 1;
        tmpPar->param_default = tmpClockInfo->ClockLevels - 1;
      }
      if(0 == strncmp(tmpPar->param_name, NAME_STR_STRTADDR, DDI_FIREHOSE_SIZE_NAME)){
        tmpPar->param_min = (ddr_info.sdram0_cs0_addr < ddr_info.sdram0_cs1_addr)? ddr_info.sdram0_cs0_addr:ddr_info.sdram0_cs1_addr;
        tmpPar->param_max = tmpPar->param_min + ((ddr_info.sdram0_cs0 + ddr_info.sdram0_cs1 + ddr_info.sdram1_cs0 + ddr_info.sdram1_cs1)<<20)-1;
        tmpPar->param_default = tmpPar->param_min;
      }
      if(0 == strncmp(tmpPar->param_name, NAME_STR_TST_SIZE, DDI_FIREHOSE_SIZE_NAME)){
        tmpPar->param_min = 1024*1024;
        tmpPar->param_max = ((ddr_info.sdram0_cs0 + ddr_info.sdram0_cs1 + ddr_info.sdram1_cs0 + ddr_info.sdram1_cs1)<<20);
        tmpPar->param_default = tmpPar->param_min;
      }
      tmpPar++;
    }
  }
  
  return TRUE;
}

boolean dqlineshmoo_dotest(void* this_p, void *preOutArgsObj, void **doneOutArgs)
{
  ddi_firehose_item_p item_this;
  arg_dqshmoo dqArgs;
  boolean fetRes = FALSE;
  
  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;
  
  dqArgs.cur_clocklevel = *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_CLKLVL, &fetRes);
  if(FALSE == fetRes) return FALSE;

  dqArgs.cur_vref = *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_VREF, &fetRes);
  if(FALSE == fetRes) return FALSE;

  dqArgs.cur_rout= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_ROUT, &fetRes);
  if(FALSE == fetRes) return FALSE;

  dqArgs.cur_rwtype= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_RWTYPE, &fetRes);
  if(FALSE == fetRes) return FALSE;

  dqArgs.cur_startaddr= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_STRTADDR, &fetRes);
  if(FALSE == fetRes) return FALSE;

  dqArgs.cur_testsize= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_TST_SIZE, &fetRes);
  if(FALSE == fetRes) return FALSE;
  
  *doneOutArgs = (boolean*)malloc(sizeof(boolean));
  if(NULL == *doneOutArgs) return FALSE;

  if(TRUE == ddr_debug_dq_cdc_shmoo(&dqArgs))
    *(boolean*)(*doneOutArgs) = TRUE;
  else
    *(boolean*)(*doneOutArgs) = FALSE;

  return TRUE;
}

boolean dqlineshmoo_postest(void *this_p, void *doneOutArgsObj)
{
  ddi_firehose_item_p item_this;

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  ddi_firehose_print_response(item_this->item_name,
                              NAME_STR_RESULT,
                              ddi_firehose_bool_name[*(boolean*)doneOutArgsObj]);
  free(doneOutArgsObj);
  return TRUE;
}


boolean calineshmoo_dotest(void *this_p, void *preOutArgsObj, void **doneOutArgs)
{
  ddi_firehose_item_p item_this;
  arg_cashmoo caArgs;
  boolean fetRes = FALSE;
  
  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  caArgs.cur_rout= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_CA_ROUT, &fetRes);
  if(FALSE == fetRes) return FALSE;

  caArgs.cur_vref = *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_VREF, &fetRes);
  if(FALSE == fetRes) return FALSE;
  
  *doneOutArgs = (boolean*)malloc(sizeof(boolean));
  if(NULL == *doneOutArgs) return FALSE;
  if(TRUE == ddr_debug_ca_cdc_sweep(&caArgs))
    *(boolean*)(*doneOutArgs) = TRUE;
  else
    *(boolean*)(*doneOutArgs) = FALSE;

  return TRUE;
}

boolean calineshmoo_postest(void *this_p, void *doneOutArgsObj)
{
  ddi_firehose_item_p item_this;

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  ddi_firehose_print_response(item_this->item_name,
                              NAME_STR_RESULT,
                              ddi_firehose_bool_name[*(boolean*)doneOutArgsObj]);
  free(doneOutArgsObj);
  return TRUE;
}

boolean stress_init(void* this_p)
{
  ddi_firehose_item_p item_this;
  clock_info_p tmpClockInfo;
  ddi_firehose_params_p tmpPar;

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  tmpClockInfo = ddr_debug_get_clock_info();
  if(NULL == tmpClockInfo) return FALSE;

  // update NAME_STR_CLKLVL
  if(NULL != item_this->item_params_arr){
    tmpPar = item_this->item_params_arr;
    while(NULL != tmpPar->param_name){
      if(0 == strncmp(tmpPar->param_name, NAME_STR_CLKLVL, DDI_FIREHOSE_SIZE_NAME)){
        tmpPar->param_max = tmpClockInfo->ClockLevels - 1;
        tmpPar->param_default = tmpClockInfo->ClockLevels - 1;
        break;
      }
      tmpPar++;
    }
  }
  
  return TRUE;
}


boolean stress_dotest(void *this_p, void *preOutArgsObj, void **doneOutArgs)
{
  ddi_firehose_item_p item_this;
  arg_stress stressArgs;
  boolean fetRes = FALSE;
  
  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  stressArgs.cur_clocklevel= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_CLKLVL, &fetRes);
  if(FALSE == fetRes) return FALSE;

  stressArgs.cur_startaddr= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_STRTADDR, &fetRes);
  if(FALSE == fetRes) return FALSE;

  stressArgs.cur_testsize= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_TST_SIZE, &fetRes);
  if(FALSE == fetRes) return FALSE;

  stressArgs.cur_looptime= *(uint32*)ddi_firehose_fetch_entiy(item_this->item_name, NAME_STR_LOOP_TIME, &fetRes);
  if(FALSE == fetRes) return FALSE;
  
  *doneOutArgs = (boolean*)malloc(sizeof(boolean));
  if(NULL == *doneOutArgs) return FALSE;  
  if(TRUE == ddr_debug_stress_test(&stressArgs))
    *(boolean*)(*doneOutArgs) = TRUE;
  else
    *(boolean*)(*doneOutArgs) = FALSE;
  return TRUE;
 
}
boolean stress_postest(void *this_p, void *doneOutArgsObj)
{
  ddi_firehose_item_p item_this;

  if(NULL == this_p) return FALSE;
  item_this = (ddi_firehose_item_p)this_p;

  ddi_firehose_print_response(item_this->item_name,
                              NAME_STR_RESULT,
                              ddi_firehose_bool_name[*(boolean*)doneOutArgsObj]);
  free(doneOutArgsObj);
  return TRUE;
}

boolean ddi_firehose_pre_work(void* this_p)
{

  uint32 ii = 0;
  clock_info_p tmpClockInfo;
  char tmpKey[DDI_FIREHOSE_SIZE_NAME];
  //uint32 cdc_val = 0;
  char *platform_id =NULL;

    
  DalDeviceHandle *hChipinfo;
  DALResult ret;
  ddr_size_info ddr_info;
#if 0
  ddi_firehose_item_p item_p;

  if(this_p ==NULL)
    return FALSE;
  
  item_p = (ddi_firehose_item_p) this_p;
  
#endif
  platform_id = (char *)malloc(10);
  ret = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &hChipinfo);
  if((ret == DAL_SUCCESS) && (hChipinfo != NULL)) {
     DalChipInfo_GetChipIdString(hChipinfo, platform_id,10);
     DAL_DeviceDetach(hChipinfo);
  }
  
  ddi_firehose_pack.platform = platform_id;

  ddr_info = boot_ddr_get_size();
  
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram0_cs0_addr",'x',&ddr_info.sdram0_cs0_addr);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram1_cs0_addr",'x',&ddr_info.sdram1_cs0_addr);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram0_cs1_addr",'x',&ddr_info.sdram0_cs1_addr);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram1_cs1_addr",'x',&ddr_info.sdram1_cs1_addr);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram0_cs0_size",'x',&ddr_info.sdram0_cs0);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram1_cs0_size",'x',&ddr_info.sdram1_cs0);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram0_cs1_size",'x',&ddr_info.sdram0_cs1);
  ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, "sdram1_cs1_size",'x',&ddr_info.sdram1_cs1);



  tmpClockInfo = ddr_debug_get_clock_info();
  if(NULL == tmpClockInfo) return FALSE;
  

  for(ii = 0; ii < tmpClockInfo->ClockLevels; ii++){
    snprintf(tmpKey,
             DDI_FIREHOSE_SIZE_NAME,
             "clock_level_%d",
             ii);
    ddi_firehose_add_ddi_infomation(&ddi_firehose_pack.info_arr, tmpKey, 'u', &tmpClockInfo->ClockFreqKHz[ii]);
  }


  

  
  return TRUE;
}

boolean ddi_firehose_post_work(void* this_p)
{
  return TRUE;
}

void ddr_debug(void)
{
  // register DDI command/item package to DDI framework
  ddi_firehose_update_item_set(&ddi_firehose_pack);

  // Call setlocale to set the locale to the default values
  setlocale(LC_ALL, "C");
  ddi_firehose_main();
}

