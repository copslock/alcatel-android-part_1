/*==================================================================
 *
 * FILE:        ddi_firehose.h
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2013 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/tools/ddi/src/firehose/ddi_firehose.h#3 $ 
 *   $DateTime: 2016/04/21 08:39:38 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2016-03-18   qbz     Re-contruct DDI framework, easier to maintain
 * 2013-05-31   ab      Initial checkin
 */

#ifndef DDR_DEBUG_FIREHOSE_H
#define DDR_DEBUG_FIREHOSE_H

#include "com_dtypes.h"

//#define FREE_DDI_INFOMATION

#define DDI_FIREHOSE_SIZE_NAME        (64)
#define DDI_FIREHOSE_SIZE_INFOVAL     (256)
#define DDI_FIREHOSE_SIZE_TYPELEN     (16)
#define DDI_FIREHOSE_SIZE_BOOLLEN     (8)
#define DDI_FIREHOSE_SIZE_MAXPAR      (32)
#define DDI_FIREHOSE_SIZE_STR_INT32   (12)
#define DDI_FIREHOSE_SIZE_INFO_MAX    (64) //max number of ddi infomation supported

extern const char ddi_firehose_bool_name[2][DDI_FIREHOSE_SIZE_BOOLLEN];

typedef enum{
  DDI_FIREHOSE_NULL = 0,
  DDI_FIREHOSE_INT8,
  DDI_FIREHOSE_UINT8,
  DDI_FIREHOSE_INT16,
  DDI_FIREHOSE_UINT16,
  DDI_FIREHOSE_INT32,
  DDI_FIREHOSE_UINT32,
  DDI_FIREHOSE_STRING,
  DDI_FIREHOSE_BOOLEAN,
  DDI_FIREHOSE_ARRAY_UINT32,
}ddi_firehose_type;

extern const char ddi_firehose_type_name[DDI_FIREHOSE_ARRAY_UINT32+1][DDI_FIREHOSE_SIZE_TYPELEN];

typedef struct
{
  char *info_key;
  char *info_val;
}ddi_firehose_info, *ddi_firehose_info_p;

typedef struct
{
  const char *param_name;
  ddi_firehose_type param_type;
  uint32 param_min;
  uint32 param_max;
  uint32 param_default;
  void* param_entity;
}ddi_firehose_params, *ddi_firehose_params_p;

typedef struct
{
  const char *rspons_name;
  ddi_firehose_type rspons_type;
}ddi_firehose_rspons, *ddi_firehose_rspons_p;

typedef struct
{
  const char *item_name;
  uint32 item_ver;

  // item parameters array
  ddi_firehose_params_p item_params_arr;

  // item responses array
  ddi_firehose_rspons_p item_rspons_arr;

  boolean (*item_exec_init)(void *this_p); //input: list from QDUTT(strictly same order as defined), output: pretest args
  
  // executors
  boolean (*item_exec_pretest)(void *this_p, void **preOutArgs); //input: list from QDUTT(strictly same order as defined), output: pretest args
  boolean (*item_exec_dotest)(void *this_p, void *preOutArgsObj, void **doneOutArgs); //input pretest output args, output test done args
  boolean (*item_exec_postest)(void *this_p, void *doneOutArgsObj); //input done test args
}ddi_firehose_item, *ddi_firehose_item_p;

typedef struct
{
  char* platform;
  uint32 version;
  
  // ddi extra infomation array
  ddi_firehose_info_p info_arr;

  // ddi items array
  ddi_firehose_item_p item_arr;

  // for ddi framework to hadle
  boolean (*worker_exec_pre)(void* this_p);
  boolean (*worker_exec_post)(void* this_p);
}ddi_firehose_item_set, *ddi_firehose_item_set_p;

void ddi_firehose_update_item_set(ddi_firehose_item_set_p ddiItemSet);
void* ddi_firehose_fetch_entiy(const char* itemName, const char* parName, boolean* fetchRes);
boolean ddi_firehose_add_ddi_infomation(ddi_firehose_info **modInfo, const char *key, const char type, void *val);

void ddi_firehose_main(void); //main entry for firehose DDI
void ddi_firehose_quit(void); // quit DDI loop, need to send command first
void ddi_firehose_print_debug(const char* format, ...); //debug ouput, will print to uart
void ddi_firehose_print_log(const char* format, ...); //log print, will print to QDUTT
void ddi_firehose_print_response(const char* item_name, const char* rsp_name, const char* rsp_val); //response print, will print to QDUTT

#endif

