/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/settings/target_config.c#3 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/11/14   arindamm     Initial creation for 8994 V1-only code.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"

extern uint32 num_of_tests;
extern uint32 dram_tmrs_array[4][20];

uint8 dll_analog_freq_range[]= {
   15 ,  /*  <300MHz  */
   7 ,  /*  <400MHz  */   
   3 ,  /*  <500MHz  */
   1 ,  /*  <700MHz  */
   0    /*  < 1.89GHz  */
};

uint32 dll_analog_freq_range_table[]    = {300000, 400000, 500000, 700000, 1890000};
uint8  dll_analog_freq_range_table_size = sizeof(dll_analog_freq_range_table)/sizeof(uint8);


//Jacala

uint8 retimer_ref[2][NUM_RETIMER_LEVELS] = {{1,1,2},    // for  GCC 
                                            {0,0,1}};   // for  DDRCC

uint8 retimer_map[NUM_RETIMER_LEVELS]    =  {1,1,2};    // for  GCC 

                                                                         
                //                          1           4            9                           
uint8  bit_remapping_phy2bimc_DQ[NUM_CH][NUM_DQ_PCH][PINS_PER_PHY] = {
       {//channel 0
       //0  1  2          3  4  5  6  7  8
       { 5, 6, 7, PHYBIT_NC, 2, 0, 4, 3, 1}, //byte0
       { 2, 1, 0, PHYBIT_NC, 6, 7, 3, 4, 5}, //byte1
       { 2, 1, 5, PHYBIT_NC, 3, 6, 0, 7, 4}, //byte2
       { 1, 0, 4, PHYBIT_NC, 7, 3, 5, 2, 6}  //byte3
       }           
       };
//                                       2          4           8
uint8  bit_remapping_bimc2phy_DQ[NUM_CH][NUM_DQ_PCH][PINS_PER_PHY_CONNECTED_NO_DBI] = {
       {//channel 0
       //0  1  2  3  4  5  6  7
       { 5, 8, 4, 7, 6, 0, 1, 2 },
       { 2, 1, 0, 6, 7, 8, 4, 5 },
       { 6, 1, 0, 4, 8, 2, 5, 7 },
       { 1, 0, 7, 5, 2, 6, 8, 4 } 
       }
       };
//  uint8  bit_remapping_phy2bimc_CA[CHNL_NUM][NUM_VREF_CA_PCH][CA_PHY_BIT_NUM] = {
    uint8  ca_phy_connected_bit_mapping[NUM_CH][NUM_CA_PCH][NUM_CA_PHY_BIT] = {
  
       {//channel 0
   //               0          1          2          3  4  5  6          7  8          9
          { PHYBIT_NC, PHYBIT_NC, PHYBIT_NC, PHYBIT_NC, 7, 8, 5, PHYBIT_NC, 6,         9 }, //ch0, CA0 
          { PHYBIT_NC, PHYBIT_NC, PHYBIT_NC, PHYBIT_NC, 2, 3, 0,         1, 4, PHYBIT_NC } //ch0, CA1

         }
        };

		
		
		
  uint8  CA01_bit_remapping_bimc2phy_CA[NUM_CH][NUM_CA_PHY_BIT] = {
        //channel 0
        //0  1  2  3  4  5  6  7  8  9
        { 6, 7, 4, 5, 8, 6, 8, 4, 5, 9} //ch0, bit
       };
  uint8  CA01_line_remapping_bimc2phy_CA[NUM_CH][NUM_CA_PHY_BIT] = {
        //channel 0
        //0  1  2  3  4  5  6  7  8  9
        { 1, 1, 1, 1, 1, 0, 0, 0, 0, 0} //ch0, CA0/CA1
       };
  uint8 dm_bit = 3; 
//================================================================================================//
// DDR Pre-Init function. Used to apply target- and revision-specific workaournds if any.
//================================================================================================//
/*uint32 DDRSS_Pre_Init(DDR_STRUCT *ddr)
{
    return TRUE;
}*/

/* =============================================================================
**  Function : DDR_CC_Pre_Clock_Switch
** =============================================================================
*/
/**
*   @brief
*   DDRCC PLL programming in the pre-clock-switch routine. Platform-specific code,
*   as DDRCC PLL programming characteristics and CSRs are different across targets.
*   Targets 8992 and 8994 are the same, 8996 is different.
*
*   @param[in]  ddr              Pointer to ddr conifiguration struct
*   @param[in]  new_clk_index    Index into the clock plan structure corresponding to the requested frequency.
*   @param[in]  ddrcc_target_pll Parameter that selects which PLL to program.
*
*   @retval  None.
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
//================================================================================================//
// Based on Freq_Switch_Params band, get an index for selecting in Freq_Switch_Params table
//================================================================================================//
uint8 DDRCC_dll_analog_freq_range_table_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 clk_idx = 0;
   uint32* dll_analog_freq_range_table_mapped;
   uint8  dll_analog_freq_range_table_size_mapped;

   //Map to the current active processor address map for accessibility
   dll_analog_freq_range_table_mapped = (ddr_external_shared_addr((uint32 *)&dll_analog_freq_range_table));
   dll_analog_freq_range_table_size_mapped = *((uint8 *)ddr_external_shared_addr((uint32 *)&dll_analog_freq_range_table_size));

   for (clk_idx = 0; (clk_idx < dll_analog_freq_range_table_size_mapped); clk_idx++)
   {
      if (clk_freq_khz <= dll_analog_freq_range_table_mapped[clk_idx])
         break;
   }

   return clk_idx;
}
void DDR_CC_Pre_Clock_Switch(DDR_STRUCT *ddr, uint8 ch, uint8 new_clk_idx, uint8 ddrcc_target_pll)
{
    uint8  dll_params_idx;
        uint32 new_clk_khz;
        uint32 reg_offset_ddr_cc;
        
        new_clk_khz = ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz;
        dll_params_idx  = DDRCC_dll_analog_freq_range_table_Index (ddr, new_clk_khz);

        reg_offset_ddr_cc = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET;
        
    if (ddrcc_target_pll) {
        //Step 1
        //Step 3
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_SYSCLK_EN_RESET,0x1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_SYSCLK_EN_RESET,0x0);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_DEC_START,
                  ddr->misc.clock_plan[new_clk_idx].pll_dec_start);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_DIV_FRAC_START3,
                   ddr->misc.clock_plan[new_clk_idx].pll_div_frac_start3);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_PLLLOCK_CMP1,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_PLLLOCK_CMP2,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp2);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_VCO_COUNT1,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_VCO_COUNT2,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count2);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_PLL_LPF2_POSTDIV,
                   ddr->misc.clock_plan[new_clk_idx].pll_pll_lpf2_postdiv);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL1_KVCO_CODE,
                   ddr->misc.clock_plan[new_clk_idx].pll_kvco_code);                                                       
        // HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL1_ANALOG_CFG0, FREQRANGE,
                   // dll_analog_freq_range[new_clk_idx]);                                 
        HWIO_OUTXF (reg_offset_ddr_cc, DDR_CC_DDRCC_PHYDLL1_ANALOG_CFG0, FREQRANGE,
                    ((uint8 *)ddr_external_shared_addr((uint32 *)&dll_analog_freq_range))[dll_params_idx]);                                        
                                   //step 4
        //step 5
      // while(HWIO_INXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_RESET_SM_READY_STATUS,CORE_READY) == 0);
        
    } else {
        //Step 1
        //step 3
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_SYSCLK_EN_RESET,0x1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_SYSCLK_EN_RESET,0x0); 
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_DEC_START,
                  ddr->misc.clock_plan[new_clk_idx].pll_dec_start);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_DIV_FRAC_START3,
                   ddr->misc.clock_plan[new_clk_idx].pll_div_frac_start3);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_PLLLOCK_CMP1,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_PLLLOCK_CMP2,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp2);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_VCO_COUNT1,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count1);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_VCO_COUNT2,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count2);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_PLL_LPF2_POSTDIV,
                   ddr->misc.clock_plan[new_clk_idx].pll_pll_lpf2_postdiv);
        HWIO_OUTX (reg_offset_ddr_cc, DDR_CC_DDRCC_SDPLL0_KVCO_CODE,
                   ddr->misc.clock_plan[new_clk_idx].pll_kvco_code);
        // HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL0_ANALOG_CFG0, FREQRANGE,
                   // dll_analog_freq_range[new_clk_idx]);
        HWIO_OUTXF (reg_offset_ddr_cc, DDR_CC_DDRCC_PHYDLL0_ANALOG_CFG0, FREQRANGE,
                   ((uint8 *)ddr_external_shared_addr((uint32 *)&dll_analog_freq_range))[dll_params_idx]);                                 
        //step 4 
        //step 5
      // while(HWIO_INXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_RESET_SM_READY_STATUS,CORE_READY) == 0);                                                         
    }
}

#ifdef DSF_LPDDR3_TMRS
void DRAM_TestMode(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint32 (*dram_tmrs)[20];
   uint8 i;
   uint32 reg_offset_shke,reg_offset_dpe;
   uint32 selected_testmode;
   uint32 size = 0;
   dram_tmrs = &dram_tmrs_array[0];
   selected_testmode = ddr->misc.misc_cfg[2];
   if ( selected_testmode <= num_of_tests) {
    reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(CH_INX(channel));
    reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(CH_INX(channel));
    switch(selected_testmode)
    {
        case 0 :
        dram_tmrs = &dram_tmrs_array[0];
        size = sizeof(dram_tmrs_array[0]) / sizeof(uint32);
        break;
        case 1 :
        dram_tmrs = &dram_tmrs_array[1];
        size = sizeof(dram_tmrs_array[1]) / sizeof(uint32);
        break;
        case 2 :
        dram_tmrs = &dram_tmrs_array[2];
        size = sizeof(dram_tmrs_array[2]) / sizeof(uint32);
        break;
        case 3 :
        dram_tmrs = &dram_tmrs_array[3];
        size = sizeof(dram_tmrs_array[3]) / sizeof(uint32);       
        break;
    };
    HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x0, 0x0);          //disable power down
    BIMC_All_Periodic_Ctrl (ddr, channel, chip_select, 0);                                          //disables hw selfrefresh, auto refresh, periodic ZQCAL, periodic SRR
    for (i = 0; i < size; i++) {
        HWIO_OUTX(reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, * (dram_tmrs + i));
        HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_WRITE, chip_select, 1);
        while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_WRITE));
    }
    BIMC_All_Periodic_Ctrl (ddr, channel, chip_select, 1);
    HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x1, 0x1); 
   }
}
#endif

uint32 DDRSS_Post_Init(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select )
{
#ifdef DSF_LPDDR3_TMRS
   uint8 ch_inx  = 0;
   uint8 cs = 0;
   DDR_CHIPSELECT cs_1hot  = DDR_CS0;  
   uint8 device_density = 0; 
   
   if ( ddr->misc.misc_cfg[1] == 1) {   
      ch_inx = CH_INX (channel);
      for (cs = 0; cs < 2; cs++)  {
      cs_1hot = CS_1HOT(cs);
          if ((chip_select >> cs) & 0x1)
          { 
              device_density  = ( (BIMC_MR_Read(ddr, channel, cs_1hot, JEDEC_MR_8)) & 0x3C) >> 2;
              if (((DDR_MANUFACTURES)ddr->cdt_params[ch_inx].common.manufacture_name == HYNIX) && (device_density == 14))  {    //checks if its HYNIX 6Gb device. If yes perform the HYNIX testmode sequence.
              DRAM_TestMode( ddr, channel, cs_1hot);
              }
              
              if ((DDR_MANUFACTURES)ddr->cdt_params[ch_inx].common.manufacture_name == SAMSUNG)  {                              //checks if its SAMSUNG device. If yes perform the SS testmode sequence.
              DRAM_TestMode( ddr, channel, cs_1hot);              
              }
          }
      }
   }
#endif
    return TRUE;
}



void DDRSS_Copy_Dram_Latency_Struct(struct ecdt_dram_latency_runtime_struct *source_ptr, struct ecdt_dram_latency_runtime_struct *dest_ptr)
{
    dest_ptr->RL                = source_ptr->RL;
    dest_ptr->WL                = source_ptr->WL;
    dest_ptr->MR2               = source_ptr->MR2;
    dest_ptr->rl_wl_freq_in_kHz = source_ptr->rl_wl_freq_in_kHz;
}

void DDRSS_Copy_Frequency_Switch_Params_Struct(struct ecdt_bimc_freq_switch_params_runtime_struct *source_ptr, struct ecdt_bimc_freq_switch_params_runtime_struct *dest_ptr)
{
    dest_ptr->MR3                      = source_ptr->MR3;                      
    dest_ptr->freq_switch_range_in_kHz = source_ptr->freq_switch_range_in_kHz; 
}

void DDRSS_Create_Ecdt_Runtime_Structs(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt)
{
    uint8 ecdt_index;
    uint8 runtime_index; 

    if(ecdt != NULL)
    {    
        // Create DRAM latency run-time structures.
        ddr->extended_cdt_runtime.hw_self_refresh_enable   = ecdt->hw_self_refresh_enable;
        ddr->extended_cdt_runtime.MR4_polling_enable       = ecdt->MR4_polling_enable;      
        //ddr->extended_cdt_runtime.periodic_training_enable = ecdt->periodic_training_enable;
        ddr->extended_cdt_runtime.page_close_timer         = ecdt->page_close_timer;   
    }
    else
    {
        ddr->extended_cdt_runtime.hw_self_refresh_enable   = 1;
        ddr->extended_cdt_runtime.MR4_polling_enable       = 1;      
        //ddr->extended_cdt_runtime.periodic_training_enable = 0;
        ddr->extended_cdt_runtime.page_close_timer         = 256;       
    }
    
    // First copy all elements of default DRAM latency structures into DDR_STRUCT.extended_cdt_runtime.
    for(runtime_index = 0; runtime_index < NUM_ECDT_DRAM_LATENCY_STRUCTS; runtime_index++) 
    {
        DDRSS_Copy_Dram_Latency_Struct(&RL_WL_lpddr_struct[runtime_index], &ddr->extended_cdt_runtime.dram_latency[runtime_index]);
    }
    
    // Then run thru the ECDT DRAM Latency input structures, and for those which have the apply_override flag set, find which band its
    // frequency falls into from in the default tables (RL_WL_lpddr_struct[]). Then copy the ECDT structure into the same band in the 
    // DDR_STRUCT.extended_cdt_runtime.dram_latency. At the end of this process, the runtime DDR latency structures in DDR_STRUCT will
    // contain structures from the ECDT if they were apply_override'd, or from the default RL_WL_lpddr_struct[] if they were not. Subsequent
    // DSF operations, both boot- and run-time will refer to the runtime structures in DDR_STRUCT, which are visible to both boot code and 
    // run-time code. 
    if(ecdt != NULL)
    {
        for(ecdt_index = 0; ecdt_index < NUM_ECDT_DRAM_LATENCY_STRUCTS; ecdt_index++) 
        {
            if(ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].apply_override == 1)
            {
                runtime_index = BIMC_RL_WL_Freq_Index(ddr, ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency.rl_wl_freq_in_kHz);
                if(runtime_index >= NUM_ECDT_DRAM_LATENCY_STRUCTS)
                {
                    ddr_printf(DDR_ERROR, "Error: ECDT DRAM latency structure: Frequency out of limits: %uMHz\n\n", 
                                            ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency.rl_wl_freq_in_kHz);
                }
                else
                {
                    DDRSS_Copy_Dram_Latency_Struct(&ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency, 
                                                &ddr->extended_cdt_runtime.dram_latency[runtime_index]);
                }
            }
        }
    }

    // Create frequency switch parameter run-time structures.
    // First copy all elements of default DRAM frequency switch parameter structures into DDR_STRUCT.extended_cdt_runtime.bimc_freq_switch[].
    for(runtime_index = 0; runtime_index < NUM_ECDT_FREQ_SWITCH_STRUCTS; runtime_index++) 
    {
        DDRSS_Copy_Frequency_Switch_Params_Struct(&bimc_freq_switch_params_struct[runtime_index], &ddr->extended_cdt_runtime.bimc_freq_switch[runtime_index]);
    }

    // Then run thru the ECDT frequency switch parameter input structures, and for those with the apply_override flag set, copy them to the appropriate
    // frequency band in the run-time structures in DDR_STRUCT. 
    if(ecdt != NULL)
    {
        for(ecdt_index = 0; ecdt_index < NUM_ECDT_FREQ_SWITCH_STRUCTS; ecdt_index++) 
        {
            if(ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].apply_override == 1)
            {
                runtime_index = BIMC_Freq_Switch_Params_Index(ddr, ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params.freq_switch_range_in_kHz);   
                if(runtime_index >= NUM_ECDT_FREQ_SWITCH_STRUCTS)
                {
                    ddr_printf(DDR_ERROR, "Error: ECDT Frequency Switch structure: Frequency out of limits: %uMHz\n\n", 
                                            ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params.freq_switch_range_in_kHz);
                }
                else
                {
                    DDRSS_Copy_Frequency_Switch_Params_Struct(&ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params, 
                                                        &ddr->extended_cdt_runtime.bimc_freq_switch[runtime_index]);
                }
            }
        }
    }
    
}

//================================================================================================//
// DDR Pre-Init function. Used to apply target- and revision-specific workaournds if any.
//================================================================================================//
uint32 DDRSS_Pre_Init(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt)
{
    DDRSS_Create_Ecdt_Runtime_Structs(ddr, ecdt);
    return TRUE;
}
