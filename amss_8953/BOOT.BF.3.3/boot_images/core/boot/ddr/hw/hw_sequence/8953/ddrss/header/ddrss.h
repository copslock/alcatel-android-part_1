/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/header/ddrss.h#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#ifndef __DDRSS_H__
#define __DDRSS_H__

#include "HAL_SNS_DDR.h"
#include "ddr_ss_seq_hwioreg.h"
#include "ddr_ss_seq_hwiobase.h"

#define PHY_LOG_PRINT_ENABLE          1

#define NUM_CH                    1 // Number of DDR channels
#define NUM_CS                    2 // Number of ranks (chip selects)
#define NUM_TRAINED_CS            1 // Number of ranks (chip selects)
#define NUM_DQ_PCH                4 // Number of DQ PHYs
#define NUM_CA_PCH                2 // Number of CA PHYs
#define NUM_CA_4WRLVL_PCH         1 // Number of CA PHYs Per Channel
#define NUM_PLL                   2 // Number of PLLs in DDRCC
#define NUM_RETIMER_LEVELS        3 // Number of Re-timer levels
#define PINS_PER_PHY              9
 #define SLICES_PER_PHY            10

#include "ddrss_training.h"
#include "bimc.h"
#include "ddr_phy.h"
#include "ddr_phy_config.h"

#define DDR_PHY_OFFSET           0x1000     // DDR PHY Address offset (2k Bytes)
#define CA0_DDR_PHY_OFFSET       0x0000
#define CA1_DDR_PHY_OFFSET       0x1000
#define DQ0_DDR_PHY_OFFSET       0x2000
#define DQ1_DDR_PHY_OFFSET       0x3000
#define DQ2_DDR_PHY_OFFSET       0x4000
#define DQ3_DDR_PHY_OFFSET       0x5000
#define DDR_CC_OFFSET            0x6000

#define PRECS_RISE                                                   0
#define PRECS_FALL                                                   1
#define CS_RISE                                                      2
#define CS_FALL                                                      3
#define POSTCS_RISE                                                  4
#define POSTCS_FALL                                                  5
#define CA_PATTERN_NUM                                               4 //7

#define REG_OFFSET_DDR_PHY_CH(ch) ((ch == 0)  ? \
                                  0x0 : \
                                  0x3800)

#define BROADCAST_BASE SEQ_DDR_SS_DDRSS_AHB2PHY_BROADCAST_SWMAN1_OFFSET

extern uint32 training_address[2][2];

// Define in ddrss_phy_wrapper.c
void DDR_PHY_CC_Config(DDR_STRUCT *ddr);
void DDR_PHY_CC_init (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 clk_freq_khz);

uint32 DDRSS_Post_Init(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
// PHY eCDT.
void DDR_PHY_CC_eCDT_Override(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt, DDR_CHANNEL channel); 
/***************************************************************************************************
 Common training functions
 ***************************************************************************************************/
boolean DDRSS_boot_training_lpddr3 (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
boolean DDRSS_Post_Boot_Training(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void DDRSS_update_CGC_optimization(DDR_STRUCT *ddr, uint32 cgc_bypass);
void DDRSS_passing_region_scan( uint8 *start, uint8 *stop, uint8 fail_count_histogram[HISTOGRAM_SIZE]);

uint8 DDRSS_Get_Freq_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz);
uint32 DDRSS_Get_Freq_Range (DDR_STRUCT *ddr, uint32 prfs_index);
uint8 DDRSS_Retimer_Calc   ( DDR_STRUCT *ddr, uint8 coarse_delay, uint8 fine_delay, uint32 period);

 // Define in ddrss_common.c
void DDRSS_Get_Training_Address(DDR_STRUCT *ddr);
void DDRSS_mem_write (DDR_STRUCT *ddr, uint8 ch, uint8 cs);

void DDRSS_MR_Write_per_die (uint8 ch,
                             uint8 cs,
                             uint8 MR_addr,
                             uint8 MR_data_die1, 
                             uint8 MR_data_die2);
                             
void DDRSS_Post_Histogram_Coarse_Horizon_Eye (best_eye_struct *best_eye_ptr,
                                              uint8 (*coarse_fail_count_ptr)[COARSE_CDC],
                                              training_data *training_data_ptr,
                                              training_params_t *training_params_ptr,
                                              uint8 training_type,
                                              uint8 phy_inx,
                                              uint32 clk_freq_khz
                                             );

void DDRSS_Post_Histogram_Fine_Best_Eye_Cal (best_eye_struct (*best_eye_ptr)[PINS_PER_PHY_CONNECTED_CA],
                                             uint8 (*left_boundary_fine_cdc_ptr)[PINS_PER_PHY_CONNECTED_CA],
                                             uint8 (*right_boundary_fine_cdc_ptr)[PINS_PER_PHY_CONNECTED_CA],
                                              training_data *training_data_ptr,
                                              training_params_t *training_params_ptr,
                                             uint8 training_type, /* 0: CA training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                              uint8 phy_inx,
                                              uint8 bit
                                             );

void DDRSS_Post_Histogram_Fine_Each_Boundary ( uint8 (*fine_fail_count_ptr)[PINS_PER_PHY][FINE_CDC],
                                               uint8 (*boundary_fine_cdc_ptr)[PINS_PER_PHY],
                                             training_data *training_data_ptr,
                                             training_params_t *training_params_ptr,
                                               uint8 left_right,  // 0:Left side fine training; 1:Right side fine training
                                               uint8 training_type, /* 0: CA training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                             uint8 phy_inx,
                                               uint8 bit );

void DDRSS_CA_Post_Histogram_Fine_Each_Boundary ( uint8 (*fine_fail_count_ptr)[PINS_PER_PHY_CONNECTED_CA][FINE_CDC],
                                                  uint8 (*boundary_fine_cdc_ptr)[PINS_PER_PHY_CONNECTED_CA],
                                                  training_data *training_data_ptr,
                                                  training_params_t *training_params_ptr,
                                                  uint8 left_right,  // 0:Left side fine training; 1:Right side fine training
                                                  uint8 training_type, /* 0: CA training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                                  uint8 phy_inx,
                                                  uint8 bit );

void DDRSS_Post_Histogram_Perbit_Processing (DDR_STRUCT *ddr,
                                             best_eye_struct (*best_eye_ptr)[PINS_PER_PHY_CONNECTED_CA],
                                             uint8 (*perbit_mid_max),
                                             uint8 (*perbit_mid_min),
                                             uint8 ch,
                                             uint8 cs,
                                             training_data *training_data_ptr,
                                             training_params_t *training_params_ptr,
                                             uint8 training_type
                                            );
uint8 DDRSS_CDC_Retimer (DDR_STRUCT *ddr,
                        uint8 cs,
                        uint8 coarse_dqs_delay,
                        uint8 fine_dqs_delay,
                         uint8 coarse_wrlvl_delay,
                         uint8 fine_wrlvl_delay,
                        uint32 cadq_ddr_phy_base,
                        uint32 clk_freq_khz
                       );




boolean DDRSS_midpoint_to_CDC_lpddr3 ( DDR_STRUCT *ddr,
                                      uint32 middle, 
                                      uint32 clk_freq_in_khz,  
                                      uint8  ch,
                                      uint8  cs,
                                      training_data *training_data_ptr,
                                      uint8  training_ddr_freq_indx,
                                      uint8  training_type, /* 0: CA training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                      uint8  phy_inx,
                                      uint8  prfs_indx
									  
                                     );                    


void DDRSS_writing_ext_CSR_lpddr3 ( DDR_STRUCT *ddr,
                                   uint8 ch, 
                                   uint8 cs, 
                                   wrlvl_params_struct *convert_cdc_ptr,  
                                   training_data *training_data_ptr,
                                   uint8 training_type, /* 0: CA training; 1:wr_dqdqs training; 2: rd_dqdqs training */
                                   uint8 phy_inx,
                                   uint8 freq_inx,
                                   uint8 training_freq_inx
				   );

void DDRSS_dqs_convert(wrlvl_params_struct *wrlvl_params_ptr, uint32 wrlvl_delay, uint32 period);

uint16 DDRSS_dq_remapping (uint8 pattern);
void ddr_mem_copy(uint32 * source, uint32 * destination, uint32 size, uint32 burst);

void DDRSS_ddr_phy_sw_freq_switch(DDR_STRUCT *ddr, uint32 clk_freq_khz, uint8 ch);
void DDRSS_training_restore_lpddr3 (DDR_STRUCT *ddr);

/***************************************************************************************************
 CA training
 ***************************************************************************************************/
void Scale_ca_dqdqs_lpddr3 (DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index);


void    DDRSS_ca_vref_lpddr3 (DDR_STRUCT *ddr,
                                   ddrss_ca_vref_local_vars *local_vars,
                              uint8 ch,
                              uint8 cs,
                              training_params_t *training_params_ptr,
                              uint32 clk_freq_khz,
                              uint8  training_ddr_freq_indx,
                              uint8  prfs_indx
                             );



/***************************************************************************************************
 DCC Training
 ***************************************************************************************************/
void DDRSS_dcc_boot (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 training_sel, uint32 clk_freq_khz);

/***************************************************************************************************
 Write Leveling
 ***************************************************************************************************/                       
void DDRSS_wrlvl (DDR_STRUCT *ddr,
                  uint8 ch,
                  uint8 cs,
                  uint32 clk_freq_khz,
                  uint8  wrlvl_clk_freq_idx,
                  training_params_t *training_params_ptr);

void DDRSS_wrlvl_ca(DDR_STRUCT *ddr,
                    uint8 ch,
                    uint8 cs,
                    uint32 clk_freq_khz,
                    training_params_t *training_params_ptr,
                    uint8  wrlvl_clk_freq_idx);

void DDRSS_wrlvl_dqs(DDR_STRUCT *ddr,
                     uint8 ch,
                     uint8 cs,
                     uint32 clk_freq_khz,
                     training_params_t *training_params_ptr,
                     uint8  wrlvl_clk_freq_idx);

void DDRSS_wrlvl_scale(DDR_STRUCT *ddr,
                                    uint8 ch,
                                    uint8 cs,
                       uint32 clk_freq_khz);

/***************************************************************************************************
 Write and Read training
 ***************************************************************************************************/
void DDRSS_rd_dqdqs_dcc_schmoo (DDR_STRUCT *ddr, 
                               uint8 ch,
                               uint8 cs,
                                uint8 *fine_left_start_cdc_value,
                               training_params_t *training_params_ptr,
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 clk_freq_khz);

boolean DDRSS_rd_dqdqs_lpddr3 (DDR_STRUCT *ddr,
                               uint8 ch,
                               uint8 cs,
                               training_params_t *training_params_ptr,
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 curr_training_freq_khz, 
                               uint8  training_ddr_freq_indx,
                               uint8  max_prfs_index);

void DDRSS_RD_CDC_Coarse_Schmoo (DDR_STRUCT *ddr,
                                      uint8 ch,
                                      uint8 cs,
                                      training_data *training_data_ptr,
                                      training_params_t *training_params_ptr,
                                      best_eye_struct *best_eye_ptr,
                                      ddrss_rdwr_dqdqs_local_vars *local_vars,
                                      uint32 clk_freq_khz);

void DDRSS_RD_CDC_Fine_Schmoo (DDR_STRUCT *ddr,
                                    uint8 ch,
                                    uint8 cs,
                                    training_data *training_data_ptr,
                                    training_params_t *training_params_ptr,
                                    uint8 *coarse_cdc_value,
                                    uint8 *rd_boundary_fine_cdc_ptr,
                                    uint8 direction,
                                    ddrss_rdwr_dqdqs_local_vars *local_vars,
                                    uint32 clk_freq_khz);

void DDRSS_RD_perbit_align_phase (DDR_STRUCT *ddr,
                                  uint8 ch,
                                  uint8 cs,
                                  uint8 byte_lane,
                                  uint8 fine_left_start_cdc_value,
                                  training_params_t *training_params_ptr,
                                  uint32 clk_freq_khz,
                                  uint8 phase);

void DDRSS_RD_sweep_DQS_DCC (DDR_STRUCT *ddr,
                             uint8 ch,
                             uint8 cs,
                             uint8 byte_lane,
                             uint8 fine_left_start_cdc_value,
                             training_params_t *training_params_ptr,
                             ddrss_rdwr_dqdqs_local_vars *local_vars,
                             uint32 clk_freq_khz,
                             uint8 phase);

void DDRSS_RD_phase_schmoo (DDR_STRUCT *ddr,
                            uint8 ch,
                            uint8 cs,
                            uint8 byte_lane,
                            training_params_t *training_params_ptr,
                            ddrss_rdwr_dqdqs_local_vars *local_vars,
                            uint32 clk_freq_khz
                           );

void DDRSS_RD_left_edge_search  (DDR_STRUCT *ddr, 
                                 uint8 ch, 
                                 uint8 cs, 
                                 training_data *training_data_ptr,
                                 training_params_t *training_params_ptr,
                                 best_eye_struct *best_eye_ptr);

void Scale_rd_dqdqs_lpddr3 (  DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index);


void DDRSS_set_global_vref ( uint32 global_vref,  uint32 ch );

//RCW related

void DDRSS_rcw (DDR_STRUCT *ddr,
                uint8 ch,
                uint8 cs,
                training_params_t *training_params_ptr,
                uint8 rcw_clk_idx);

void read_rcw_status (uint8 *byte_one_cnt, uint8 *byte_zero_cnt, uint8 ch);
void set_rcw_cdc_delay ( uint8 rcw_delay,  uint8 coarse_fine, uint8 mode, uint8 ch, uint8 cs, uint8 enable[NUM_DQ_PCH] );
void cdc_delay_search (DDR_STRUCT *ddr, uint8 loop_cnt, uint8 max_coarse_delay, uint8 max_fine_delay, uint8 coarse_fine_sel,  uint8 *all_found, uint8 ch, uint8 cs, uint8 *byte_done, uint8 *byte_not_done, uint8 num_half_cycle);
void set_rcw_enable (uint8 enable[4],  uint8 num_cycle,  uint8 ch);
void DDRSS_bimc_rcw_start_delay_write_flash_params (DDR_STRUCT *ddr,
                                                    uint8 ch,
                                                    uint8 cs,
                                                    training_params_t *training_params_ptr,
                                                    uint8 rcw_clk_idx);

/***************************************************************************************************
VREF DQ WRITE TRAINING
 ***************************************************************************************************/

boolean DDRSS_wr_dqdqs_lpddr3 (DDR_STRUCT *ddr,
                               uint8 ch,
                               uint8 cs,
                               training_params_t *training_params_ptr,
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 curr_training_freq_khz,
                               uint8  training_ddr_freq_indx,
                               uint8  max_prfs_index);

void DDRSS_WR_CDC_Coarse_Schmoo (DDR_STRUCT *ddr,
                               uint8 ch,
                               uint8 cs,
                                      training_data *training_data_ptr,
                               training_params_t *training_params_ptr,
                                      best_eye_struct *best_eye_ptr,
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 clk_freq_khz,
                                 uint8 training_ddr_freq_indx);

void DDRSS_wr_pbit_schmoo (DDR_STRUCT *ddr, 
                           uint8 ch, 
                           uint8 cs, 
                           training_params_t *training_params_ptr, 
                           ddrss_rdwr_dqdqs_local_vars *local_vars,
                           uint8 *coarse_cdc_right_start_value,
                           uint32 clk_freq_khz,
                           uint8 training_ddr_freq_indx);

void DDRSS_WR_CDC_1D_Fine_Schmoo (DDR_STRUCT *ddr, 
                                      uint8 ch,
                                      uint8 cs,
                                      training_data *training_data_ptr,
                                      training_params_t *training_params_ptr,
                                  ddrss_rdwr_dqdqs_local_vars *local_vars,
                                  uint8 *left_boundary_eye_cdc_value, 
                                  uint8 *right_boundary_eye_cdc_value, 
                                  uint8 *coarse_cdc_value,
                                  uint32 clk_freq_khz,
                                  uint8 training_ddr_freq_indx,
                                  uint8 direction);

void DDRSS_WR_CDC_1D_Schmoo (DDR_STRUCT *ddr, 
                                    uint8 ch,
                                    uint8 cs,
                                    training_data *training_data_ptr,
                                    training_params_t *training_params_ptr,
                                    ddrss_rdwr_dqdqs_local_vars *local_vars,
                             best_eye_struct *best_eye_ptr,
                             uint8 *coarse_cdc_left_start_value,
                             uint8 *coarse_cdc_right_start_value,
                                    uint32 clk_freq_khz,
                             uint8 training_ddr_freq_indx);
void Scale_wr_dqdqs_lpddr3 (  DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index);

/***************************************************************************************************
 Memory read/write routine used for training. 
 ***************************************************************************************************/
void __blocksCopy(uint32 *source, uint32 *destination, uint32 num_words);
uint16 get_ca_exp_pattern_lpddr3( uint16 ca_pat_rise, uint16 ca_pat_fall, uint8 ca_train_mapping);
uint16 DDRSS_ca_remapping_lpddr3 (uint16 pattern, uint8 remaping_LUT[PINS_PER_PHY]);
uint16 DDRSS_dq_remapping_lpddr3 (uint16 pattern, uint8 remaping_LUT[8]);
void DDRSS_device_reset_cmd(void);

void DDRSS_ca_training_bit_histogram_update_rise_fall(uint8 ca_mapping, uint16 failure, uint8 fail_count_histogram_perbit[3][NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][HISTOGRAM_SIZE],uint16 nobe);
void DDRSS_ca_training_bit_histogram_update(uint8 ca_mapping, uint16 failure, uint8 fail_count_histogram_perbit[PINS_PER_PHY_CONNECTED_CA]);

uint8 *DDRSS_mem_read_per_byte_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 phase);
uint8 *DDRSS_mem_read_per_bit_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 wr_rd, uint8 byte_lane, uint8 phase);

int ddrss_lib_uidiv (int numerator, int denominator);
int ddrss_lib_idiv  (int numerator, int denominator);
int ddrss_lib_modulo  (int numerator, int denominator);

#endif /* __DDRSS_H__ */
 
