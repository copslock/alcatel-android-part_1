/****************************************************************************

 QUALCOMM Proprietary Design Data

 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.

 ****************************************************************************/

/*==============================================================================

                                EDIT HISTORY



$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_libs.c#1 $

$DateTime: 2016/03/11 01:25:05 $

$Author: pwbldsvc $

================================================================================

when       who          what, where, why

--------   ---          --------------------------------------------------------
05/11/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"

int ddrss_lib_uidiv (int numerator, int denominator)
{
            int quotient = 0;
            int temp, carry = 0;



            if (denominator == 0) return 0; 
            
            if (numerator < 0) numerator = -numerator; 

            temp = denominator ; 

            while (1) {
                  if (temp <= (numerator >> 1))
                        temp =  temp << 1; 
                  if (temp > (numerator >> 1))
                        break;
            }

            while(1) {

                  if (numerator > temp) {
                        numerator = numerator - temp;
                        carry = 1;
                  }
                  
                  quotient = (quotient << 1) + carry;
                  temp = temp >> 1;
                  carry = 0;

                  if (temp < denominator) 
                        break;
            } 

            if(numerator==denominator) {
                  quotient++;
            }
            return (quotient);

}

int ddrss_lib_modulo (int numerator, int denominator)
    {
            int quotient = 0;
            int temp, carry = 0;
            int remainder = 0;


            if (denominator == 0) return 0; 
            
            if (numerator < 0) numerator = -numerator; 

            temp = denominator ; 

            while (1) {
                  if (temp <= (numerator >> 1))
                        temp =  temp << 1; 
                  if (temp > (numerator >> 1))
                        break;
            }

            while(1) {

                  if (numerator > temp) {
                        numerator = numerator - temp;
                        carry = 1;
                  }
                  
                  quotient = (quotient << 1) + carry;
                  temp = temp >> 1;
                  carry = 0;

                  if (temp < denominator) 
                        break;
            } 

            if(numerator==denominator) {
                  quotient++;
    }
            else{
                  remainder = numerator;
            }

            return (remainder);

    }

int ddrss_lib_idiv (int numerator, int denominator)
{
            int quotient = 0;
            int temp, carry = 0;
int sign = 1;
	if (denominator == 0) return 0; 
    if(numerator < 0){
        numerator = -numerator;
        sign      = -sign;
    }
    if(denominator< 0){
        denominator = -denominator;
        sign        = -sign;
}
            temp = denominator ; 

            while (1) {
                  if (temp <= (numerator >> 1))
                        temp =  temp << 1; 
                  if (temp > (numerator >> 1))
                        break;
            }

            while(1) {

                  if (numerator > temp) {
                        numerator = numerator - temp;
                        carry = 1;
                  }

                  quotient = (quotient << 1) + carry;
                  temp = temp >> 1;
                  carry = 0;

                  if (temp < denominator) 
                        break;
            } 

            if(numerator==denominator) {
                  quotient++;
    }
            return (quotient* sign);
}
