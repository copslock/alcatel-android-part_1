/**
 * @file  bimc_config.h
 * @brief  This is header file for bimc_config_xxxx.c implementation, including the static setting,
 * read latency, write latency, MR values based on different frequency plans
 * 
 */
/*==============================================================================
                                EDIT HISTORY

$Header $ $DateTime $
$Author: pwbldsvc $
================================================================================
when         who         what, where, why
----------   --------     -------------------------------------------------------------
04/02/15   sc      Initial version.
================================================================================
                   Copyright 2013-2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/

#ifndef __DDRSS_INIT_SDI_H__
#define __DDRSS_INIT_SDI_H__
/*==============================================================================
                                                             INCLUDES
==============================================================================*/
#include "HALhwio.h"
#include "ddr_phy_ddrss.h"
#include "ebi.h"
#include "ddr_params.h"
#include "bimc_struct.h"

#ifndef SEQ_BIMC_BIMC_S_DDR1_SCMO_OFFSET
#define SEQ_BIMC_BIMC_S_DDR1_SCMO_OFFSET SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET
#endif

#ifndef SEQ_BIMC_BIMC_S_DDR1_DPE_OFFSET
#define SEQ_BIMC_BIMC_S_DDR1_DPE_OFFSET SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET
#endif

#ifndef SEQ_BIMC_BIMC_S_DDR1_SHKE_OFFSET
#define SEQ_BIMC_BIMC_S_DDR1_SHKE_OFFSET SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET
#endif

#ifndef SEQ_BIMC_GLOBAL0_OFFSET
#define SEQ_BIMC_GLOBAL0_OFFSET SEQ_BIMC_BIMC_MISC_OFFSET
#endif

#define REG_OFFSET_SCMO(INTERFACE) ((INTERFACE == SDRAM_INTERFACE_0) ? \
SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET : \
    SEQ_BIMC_BIMC_S_DDR1_SCMO_OFFSET)

#define REG_OFFSET_DPE(INTERFACE)  ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET  : \
    SEQ_BIMC_BIMC_S_DDR1_DPE_OFFSET)

#define REG_OFFSET_SHKE(INTERFACE)  ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET  : \
    SEQ_BIMC_BIMC_S_DDR1_SHKE_OFFSET)

extern uint32 ddr_divide_func(int32 a, uint32 b);

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/ 
/* =============================================================================
 **  Function : BIMC_GLOBAL0_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_GLOBAL0 static settings
 *
 *   @param[in] reg_base BIMC GLOBAL0 base address
 *   @param[in] interface  interface to set  
 *
 *   @dependencies None
 *
 *   @sideeffects  None
 **/
void BIMC_GLOBAL0_Set_Config_SDI(uint32 reg_base, SDRAM_INTERFACE interface);

/* =============================================================================
 **  Function : BIMC_SCMO_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SCMO static settings
 *
 *   @param[in] reg_base BIMC SCMO base address
 *   @param[in] interface  interface to set  
 *
 *   @dependencies None
 *
 *   @sideeffects  None
 **/
void BIMC_SCMO_Set_Config_SDI(uint32 reg_base, SDRAM_INTERFACE interface);

/* =============================================================================
 **  Function : BIMC_DPE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_DPE static settings
 *
 *   @param[in] reg_base BIMC DPE base address
 *   @param[in] interface  interface to set  
 *
 *   @dependencies None
 *
 *   @sideeffects  None
 **/
void BIMC_DPE_Set_Config_SDI(uint32 reg_base, SDRAM_INTERFACE interface);

/* =============================================================================
 **  Function : BIMC_SHKE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SHKE static settings
 *
 *   @param[in] reg_base BIMC SHKE base address
 *   @param[in] interface  interface to set  
 *
 *   @dependencies None
 *
 *   @sideeffects  None
 **/
void BIMC_SHKE_Set_Config_SDI(uint32 reg_base, SDRAM_INTERFACE interface);

/* =============================================================================
 **  Function : BIMC_Pre_Init_Setup_SDI
 ** =============================================================================
 */
/**
 *   @brief
 *      one-time settings for SCMO/DPE/SHKE/BIMC_MISC; values are provided in bimc_config.c parsing from SWI
 *      AC Timing Parameters is update by SW CDT at the end
 *      ODT is on (now hard-coded for initialzing ODT for rank 0)
 *
 *   @param[in]  reg_base   BIMC reg_base
 *   @param[in]  interface    Interface to initialize for
 *   @param[in] clk_freq_in_khz  clock frequency (in khz) to update 
 *   @param[in] clk_period_in_ps clock period(in ps) to update 
 *
 *   @dependencies None
 *
 *   @sideeffects  None
 */
void
BIMC_Pre_Init_Setup_SDI (uint32 reg_base, SDRAM_INTERFACE interface,
    uint32 clk_freq_in_khz, uint32 clk_period_in_ps);

/* =============================================================================
**  Function : HAL_SDRAM_SDI_Init
** =============================================================================
*/
/**
*   @brief
*   Initialize DDR controller
*
*   @param[in]  interface    Interface to initialize for
*   @param[in]  chip_select  Chip select to initialize for
*   @param[in]  clk_speed    Clock speed (in KHz) to initialize for
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/

void HAL_SDRAM_SDI_Init(SDRAM_INTERFACE interface, SDRAM_CHIPSELECT chip_select, uint32 clk_speed);
    



#endif /* __DDRSS_INIT_SDI_H__ */
