/***************************************************************************************************

 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.

 ***************************************************************************************************/
// DDR System Firmware Version
#define TARGET_DDR_SYSTEM_FIRMWARE_VERSION     48

// Define silicon target or emulation target. Some portions of the DDR System Firmware will or will not
// be compiled depending on what this macro is defined as. 
// If TARGET_SILICON is defined as a 0, it implies an emulation build.
// If TARGET_SILICON is defined as a 1, it implies a real-silicon build.
//#define TARGET_SILICON                         1 
//Replaced TARGET_SILICON with FEATURE_RUMI_BOOT based on SW request.
// If FEATURE_RUMI_BOOT is defined , it implies an emulation build.
// If FEATURE_RUMI_BOOT is not defined, it implies a real-silicon build.

//define FEATURE_RUMI_BOOT 1		//define this for RUMI
// BIMC and PHY Core Architecture, Major and Minor versions.
#define TARGET_BIMC_ARCH_VERSION               2
#define TARGET_BIMC_CORE_MAJOR_VERSION         3
#define TARGET_BIMC_CORE_MINOR_VERSION         0
#define TARGET_PHY_CORE_MAJOR_VERSION          4
#define TARGET_PHY_CORE_MINOR_VERSION          0

// Define DDR base address in the chip address map from APPS viewpoint
#define DDR_BASE                        0x80000000
#define SCMO_BASE                       0x0 //Base address from SCMO point view, should always start from 0

// Size of statically allocated area in ZI data area that training functions will
// use in place of local variables to reduce stack growth pressure.
// Used in the function HAL_DDR_Boot_Training().
#define LOCAL_VARS_AREA_SIZE            0x10000 /* 64KB */

// The flat 32-bit system address from APPS or RPM viewpoint where PHY training code will write
// training patterns to and read back from. Training addresses currently are hard-coded to assume
// that System Address Bit 10 is the interleave bit, hence the address for channel 1 is 0x00000400.
// The training address is at 0x50000000 if we are compiling for RPM processor.
// The training address is at 0x10000000 if we are compiling for APPS processor.
// COMPILING_RPM_PROC is a RPM-specific build flag.
#ifdef COMPILING_RPM_PROC
    #define TRAINING_BASE_ADDRESS_OFFSET     0x50000000
#else
    #define TRAINING_BASE_ADDRESS_OFFSET     0x10000000
#endif  // COMPILING_RPM_PROC

#define TRAINING_INTERLEAVE_ADDRESS_OFFSET  0x00000400


#define DSF_CA_TRAINING_EN                      1
#define DSF_WRLVL_TRAINING_EN                        1
#define DSF_RCW_TRAINING_EN                          0

#define DSF_PASS_1_DCC_TRAINING_EN                   1 
#define DSF_PASS_2_DCC_TRAINING_EN                   0    //DCC training after CA_Vref
#define DSF_PASS_3_DCC_TRAINING_EN                   0    //DCC training after WRLVL
#define DSF_PASS_4_DCC_TRAINING_EN                   0    //DCC training after WR
#define DSF_DCC_TRAINING_DQ_DCC_EN                   1    //DQ DCC enabled.
#define DSF_DCC_TRAINING_CA_DCC_EN                   1    //CA DCC enabled.

#define DSF_RD_DQDQS_TRAINING_EN                     1
#define DSF_CLK_DCC_RD_DQDQS                         0
#define DSF_RD_DQDQS_DCC                             1


#define DSF_WR_DQDQS_TRAINING_EN                     1

// Build flag to work around emulation platform problem with mode register reads. 
#define DSF_EMULATION_WORKAROUND_MRR_8953 0
//------------------------------------------------------------------------------------------------------
//-------------Broadcast Mode Defines  Starts     8953     ---------------------------------------------
//------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------
//         |           CHANNEL 1         |        CHANNEL 0           |
// ----------------------------------------------------------------------
// { na| na| CC|DQ3}{DQ2|DQ1|DQ0|CA1}{CA0| CC|DQ3|DQ2}{DQ1|DQ0|CA1|CA0}
// ----------------------------------------------------------------------
// { 15| 14| 13| 12}{ 11| 10|  9|  8}{  7|  6|  5|  4}{  3|  2|  1|  0}
// {---------------}{---------------}{---------------}{---------------}--
#define PHY_BC_DISABLE         0x0000

//Channel 0                 
#define CS_CH0_DDRPHY_CA0      0x0001 
#define CS_CH0_DDRPHY_CA1      0x0002 
#define CS_CH0_DDRPHY_DQ0      0x0004 
#define CS_CH0_DDRPHY_DQ1      0x0008 
#define CS_CH0_DDRPHY_DQ2      0x0010 
#define CS_CH0_DDRPHY_DQ3      0x0020 
#define CS_CH0_DDRCC           0x0040 
#define ALL_CH0_CAs           ( CS_CH0_DDRPHY_CA0 |CS_CH0_DDRPHY_CA1  )
#define ALL_CH0_DQs           ( CS_CH0_DDRPHY_DQ0 |CS_CH0_DDRPHY_DQ1 |CS_CH0_DDRPHY_DQ2 |CS_CH0_DDRPHY_DQ3  )

//Channel 1                               
#define CS_CH1_DDRPHY_CA0      0x0080  
#define CS_CH1_DDRPHY_CA1      0x0100  
#define CS_CH1_DDRPHY_DQ0      0x0200  
#define CS_CH1_DDRPHY_DQ1      0x0400  
#define CS_CH1_DDRPHY_DQ2      0x0800  
#define CS_CH1_DDRPHY_DQ3      0x1000  
#define CS_CH1_DDRCC           0x2000  
#define ALL_CH1_CAs           ( CS_CH1_DDRPHY_CA0 |CS_CH1_DDRPHY_CA1  )
#define ALL_CH1_DQs           ( CS_CH1_DDRPHY_DQ0 |CS_CH1_DDRPHY_DQ1 |CS_CH1_DDRPHY_DQ2 |CS_CH1_DDRPHY_DQ3  )

//All Channels
#define ALL_CA0               ( CS_CH0_DDRPHY_CA0  | CS_CH1_DDRPHY_CA0 )
#define ALL_CA1               ( CS_CH0_DDRPHY_CA1  | CS_CH0_DDRPHY_CA1 )

#define ALL_CAs               ( ALL_CH0_CAs  | ALL_CH1_CAs )
#define ALL_DQs               ( ALL_CH0_DQs  | ALL_CH1_DQs )
#define ALL_CCs               ( CS_CH0_DDRCC | CS_CH1_DDRCC)

#define ALL_CAsDQs            ( ALL_CAs      | ALL_DQs     )

//------------------------------------------------------------------------------------------------------
//-------------Broadcast Mode Defines  Ends              ---------------------------------------------
//------------------------------------------------------------------------------------------------------

/* =============================================================================
**  Function : DDRSS_DCO_Calibration
** =============================================================================
*/
/**
*   @brief
*   DCO Calibration workaround for 8994 V1 only.
*
*   @param[in]  ddr          Pointer to ddr conifiguration struct
*
*   @retval  Always TRUE.
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
//uint32 DDRSS_Pre_Init(DDR_STRUCT *ddr);
uint32 DDRSS_Pre_Init(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt);
void DDR_CC_Pre_Clock_Switch(DDR_STRUCT *ddr, uint8 ch, uint8 new_clk_idx, uint8 ddrcc_target_pll);
uint8 DDRCC_dll_analog_freq_range_table_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz);

/* =============================================================================
**  Function : DRAM_TestMode_SAMSUNG
** =============================================================================
*/

void DRAM_TestMode_SAMSUNG(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);

