/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/bimc_training.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"

//================================================================================================//
// BIMC_CA_Training_Entry
// Update the bimc CA training pattern into the DPE CA training sequence registers
//================================================================================================//
/*    @inputs details
 *    ch : 0 -> channel 0 , 1-> channel 1
 *    chip_select:   DDR_CS_NONE =0 , DDR_CS0 = 1, DDR_CS1 = 2, DDR_CS_BOTH = 3,
 *    mapping : 1 -> for CA4 and CA9; 0 ->for other CAs. As there are CA0-9 but only sixteen DQs , so two mappings are needed
 */
void BIMC_CA_Training_Entry_Lpddr3( DDR_STRUCT *ddr, uint8 ch, uint8 chip_select, uint8 mapping)
{
   uint32 reg_offset_shke = 0;
   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
   
    // disable rank init complete signal
   if (CS_1HOT(chip_select) & DDR_CS0) {
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 0);
   }
   if (CS_1HOT(chip_select) & DDR_CS1) {
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 0);
   }    

   //for(i=0; i<2000;i++){}; 
   
   //enable CKE on before writing into the CA specific mode registers
   HWIO_OUTXF2 (reg_offset_shke , SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, CS_1HOT(chip_select), 1);
   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));
   
   if (mapping == 0){  // setting MR41 calibrates the address pins CA0, CA1, CA2, CA3, CA5, CA6, CA7 and CA8.
      BIMC_MR_Write ( ddr, CH_1HOT(ch),  CS_1HOT(chip_select),  0x29, 0xA4) ;   
   }
   else  {    //setting MR48 calibrates the remaining address pins - CA4 and CA9.
      BIMC_MR_Write ( ddr, CH_1HOT(ch),  CS_1HOT(chip_select),  0x30, 0xC0) ;   
   }

//for(i=0; i<2000;i++){};

   // Drive CKE low for the rank being calibrated
   HWIO_OUTXF2 (reg_offset_shke , SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, CS_1HOT(chip_select), 1);
   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));
}

 /* =============================================================================
 **  Function : BIMC_Set_CA_Training_Pattern
 ** =============================================================================
 *    @details
 *      Update the bimc CA training pattern into the DPE CA training sequence registers
 *
 *    @inputs:
 *    ch : 0 - channel 0 , 1- channel -1
 *    chip_select:   DDR_CS_NONE =0 , DDR_CS0 = 1, DDR_CS1 = 2, DDR_CS_BOTH = 3,
 *    mapping : 1 -> for CA4 and CA9; 0 ->for other CAs. As there are CA0-9 but only sixteen DQs , so two mappings
 **/
void BIMC_Set_CA_Training_Pattern( DDR_STRUCT *ddr, uint8 ch, uint16 pre_rising_pattern, uint16 pre_falling_pattern, uint16 rising_pattern, uint16 falling_pattern, uint16 post_rising_pattern, uint16 post_falling_pattern)
{
   uint32 reg_offset_dpe = 0;
   reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
   
   /// load the address pattern to be driven onto the CA bus for calibration
   HWIO_OUTXF2 (reg_offset_dpe, DPE_CA_TRAIN_PRE_CS , CA_BUS_1, CA_BUS_2, pre_rising_pattern  , pre_falling_pattern  );
   HWIO_OUTXF2 (reg_offset_dpe, DPE_CA_TRAIN_CS     , CA_BUS_3, CA_BUS_4, rising_pattern      , falling_pattern      );
   HWIO_OUTXF2 (reg_offset_dpe, DPE_CA_TRAIN_POST_CS, CA_BUS_5, CA_BUS_6, post_rising_pattern , post_falling_pattern );
}

// BIMC_CA_Training_Exit
//================================================================================================//
void BIMC_CA_Training_Exit_Lpddr3 ( DDR_STRUCT *ddr, uint8 ch, uint8 chip_select)
{
   uint32 reg_offset_shke = 0;
   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

   //enable CKE
   HWIO_OUTXF2 (reg_offset_shke , SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, CS_1HOT(chip_select), 1);
   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

   //Exit CA training Mode
   BIMC_Extended_MR_Write ( ddr, CH_1HOT(ch),  CS_1HOT(chip_select),  0x2A, 0xA8) ;

   //Re-enable rank init so that SHKE is put to operating state
   if (CS_1HOT(chip_select) & DDR_CS0) {
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 1);
   }
   if (CS_1HOT(chip_select) & DDR_CS1) {
      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 1);
   }
}


//================================================================================================//
// BIMC_Send_CA_Pattern
// sending out and receiving CA pattern on DQ bus
// chip_select = CS_BOTH is not supported
//================================================================================================//
void BIMC_Send_CA_Pattern (DDR_STRUCT *ddr, uint8 ch, uint8 cs)
{

   uint32 reg_offset_shke = 0;

   reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

   //send out CA pattern
   //RANK_SEL cannot be set to '11'
   HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CA_TRAIN_PATTERN_CMD, CS_1HOT(cs), 1);
   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CA_TRAIN_PATTERN_CMD));


}

