/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_boot_training_lpddr3.c#2 $
$DateTime: 2016/06/21 00:04:59 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/
#include "ddrss.h"
extern  uint8  CA01_line_remapping_bimc2phy_CA[NUM_CH][NUM_CA_PHY_BIT] ;
extern  uint8  CA01_bit_remapping_bimc2phy_CA [NUM_CH][NUM_CA_PHY_BIT] ;

//#pragma O0
boolean HAL_DDR_Boot_Training (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 mode)
{
//================================================================================================//
// Training mode check. 
// Current implementation:
//  - Executes training result restore and returns TRUE if DSF version has not changed, 
//    when called with DDR_TRAINING_MODE_CHECK
//  - Returns FALSE if DSF version has changed, when called with DDR_TRAINING_MODE_CHECK
//  - Executes training and returns TRUE if called with DDR_TRAINING_MODE_INIT
//  - Returns FALSE for any other value of uint32 mode.
//================================================================================================//
 
    if (mode == DDR_TRAINING_MODE_INIT) 
    { 
        ddr_printf(DDR_STATUS, "DDR Training, Start");
        DDRSS_boot_training_lpddr3 (ddr, channel, chip_select);
        DDRSS_update_CGC_optimization(ddr,0/*cgc_bypass*/);
        ddr_printf(DDR_STATUS, "DDR Training, End");
        ddr->flash_params.version = TARGET_DDR_SYSTEM_FIRMWARE_VERSION;
        return TRUE; 
    }
    else if (mode == DDR_TRAINING_MODE_CHECK) 
    { 
        // Restore only if DSF version number matches, which indicates training has already been done 
        // for this version of the DSF at least once before.
        // More checks will be applied in the future such as frequency plan change, etc.

        if (ddr->flash_params.version == TARGET_DDR_SYSTEM_FIRMWARE_VERSION)
        {
           ddr_printf(DDR_STATUS, "DDR Training restore, Start");           
	   DDRSS_training_restore_lpddr3 (ddr);
           DDRSS_update_CGC_optimization(ddr,0/*cgc_bypass*/);
           ddr_printf(DDR_STATUS, "DDR Training restore, End");
           return TRUE; 
        }
        // If the version number does not match, return FALSE, so that caller of DSF can 
        // call HAL_DDR_Boot_Training() with INIT in order to re-train.
        else
        {
            ddr_printf(DDR_NORMAL, "\nDSF version changed, triggering re-training..\n");
            ddr_printf(DDR_NORMAL, " Previous DSF version : %u\n", ddr->flash_params.version);
            ddr_printf(DDR_NORMAL, " Current  DSF version : %u\n\n", TARGET_DDR_SYSTEM_FIRMWARE_VERSION);
            return FALSE;
        }
    }
    else
    { 
        ddr_printf(DDR_NORMAL, "\nHAL_DDR_Boot_Training(): Invalid option!\n");
        return FALSE; 
    }

}

//================================================================================================//
// DDRSS Boot Training
//================================================================================================//

boolean DDRSS_boot_training_lpddr3 (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
    uint8  ch                = 0;
    uint32 reg_offset_dpe    = 0;

#if   DSF_CA_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_WRLVL_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_WR_DQDQS_TRAINING_EN
    uint8  cs                = 0;
#endif

//    DDR_CHIPSELECT qualified_cs = (DDR_CHIPSELECT)(chip_select & ddr->cdt_params[0].common.populated_chipselect);
    
#if  DSF_WRLVL_TRAINING_EN
    uint8   new_RL_WL_idx       = 0;
    uint32  MR2_wrlvl_opcode    = 0;
#endif

#if DSF_CA_TRAINING_EN
    uint32 boot_ddr_freq;        //384000;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint32 boot_ddr_freq;        //384000;
#endif

    int8   training_freq_indx         = 0;    
    //uint8  training_freq_indx         = 0; 
    int8   training_indx              = 0;
    uint8  max_training_ddr_freq_indx = TRAINING_NUM_FREQ_LPDDR3 - 1;  //Set the max number of training frequecies  
    uint8  prfs_index                 = TRAINING_START_PRFS; 
    uint8  max_clock_index            = ddr->misc.ddr_num_clock_levels - 1;
    uint32 curr_training_freq_khz     = 0;
    

    // Training params structure
    static training_params_t training_params;
    training_params_t *training_params_ptr;
 
    // Set aside an area to be used by training algorithm for computation local_vars area.
    // This area is declared as a static local array so as to get it into the ZI data section
    // of the executable. Training algorithms will receive a pointer to this area, and can
    // use it as a storage for what would otherwise have been local variables, consuming
    // huge amounts of stack space.
    static   uint8   *local_vars;
    uint32 local_var_size = LOCAL_VARS_AREA_SIZE;
    
    boolean bResult = ddr_external_get_buffer((void **)&local_vars, &local_var_size);
    
    if (bResult != TRUE)
    {
         return bResult;
    }

    // Get ddr boot frequency
    boot_ddr_freq = ddr->misc.ddr_boot_freq;

    // Training params structure pointer
    training_params_ptr = &training_params;

    
//================================================================================================//
// Pre training setup
//================================================================================================//
    // Set all training parameters to recommended values
    DDRSS_set_training_params(training_params_ptr);
    DDRSS_Get_Training_Address(ddr);

    // Populate training table with the training frequency in khz
    /*
     19200,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 0] in misc
    100800,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 1] in misc
    211200,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 2] in misc
    278400,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 3] in misc
    384000,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 4] in misc
    422400,  0, 0,   0, 0,       0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 5] in misc
    556800,  0, 0,       0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 6] in misc
    672000,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 7] in misc
    768000,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 8] in misc
    806400,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 9] in misc
    844800,  0, 0,   0, 0,   0,  0,   0, 0,  0,  0, 0, 0,  // clock_plan[ 10] in misc
    931200,  0, 97,  0, 179, 9,  163, 3, 19, 44, 0, 0, 0,  // clock_plan[ 11] in misc

     F_RANGE_0                   250000
     F_RANGE_1                   450000
     F_RANGE_2                   790000
     F_RANGE_3                   900000
     F_RANGE_4                   1066000
     F_RANGE_5                   1066000
     F_RANGE_6                   1066000
     F_RANGE_7                   1066000
   
    training_freq_table[1] = ddr->misc.clock_plan[11].clk_freq_in_khz;  //931200
    training_freq_table[0] = ddr->misc.clock_plan[10].clk_freq_in_khz;  //844800
    */
    // Disable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
    BIMC_All_Periodic_Ctrl (ddr, channel, DDR_CS_BOTH, FEATURE_DISABLE/*0 for disable*/);

    for (ch = 0; ch < NUM_CH; ch++)
    {
        reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
        if ((channel >> ch) & 0x1)
        {
          // Disable Power Down
          HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x0, 0x0);
        }
    }

//================================================================================================//
// Training frequency loop
//================================================================================================//
    // Set the maximum training frequency index
    //max_training_ddr_freq_indx = TRAINING_NUM_FREQ_LPDDR3 - 1;
 
    // Train at selected number of training frequecies 
    for (training_indx = max_training_ddr_freq_indx; training_indx >= 0; training_indx--) // loop for number of times training is to be done
    {
        for(training_freq_indx = max_clock_index; training_freq_indx > 0; training_freq_indx--)// loop for clock frequency table
        {
            if((ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] ) && (prfs_index > 0))
            {
                while( (prfs_index > 0) && (ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz < freq_range[prfs_index -1]) )
                {
                    prfs_index--;
                }
                break;
            }
            else if (prfs_index ==0)
            {
                if(ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] )
                {
                 break;
                }
            }
        }

        retimer_map[0]=retimer_ref[ddr->misc.clock_plan[training_freq_indx].clk_mode ][0];
        retimer_map[1]=retimer_ref[ddr->misc.clock_plan[training_freq_indx].clk_mode ][1];
        retimer_map[2]=retimer_ref[ddr->misc.clock_plan[training_freq_indx].clk_mode ][2];

        curr_training_freq_khz = ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz;
        max_clock_index        = training_freq_indx--;
        
      ddr_printf(DDR_NORMAL, "\n========================================================\n");
      ddr_printf(DDR_NORMAL, "START: DDR training at freq: %u, prfs level at: %d .\n", curr_training_freq_khz,prfs_index);
      ddr_printf(DDR_NORMAL,"========================================================\n\n");
        
      // Switch to the training frequency
      ddr_external_set_clk_speed (curr_training_freq_khz);

//================================================================================================//
// DCC
//================================================================================================//
#if DSF_PASS_1_DCC_TRAINING_EN 
   
        // Train DCC at the higest frequency( highest prfs band)
        if (training_indx == max_training_ddr_freq_indx)
        {
          // DCC Training for WRLVL CDC, WR90 CDC and IO DCC at training freq
          ddr_printf(DDR_NORMAL, "\n  START: DCC training at freq: %u\n", curr_training_freq_khz);
          DDRSS_dcc_boot (ddr, channel, DCC_TRAINING_WRLVL_WR90_IO, curr_training_freq_khz );  
          ddr_printf(DDR_NORMAL, "\n  END: DCC training at freq: %u\n\n", curr_training_freq_khz);

#ifdef TARGET_8992_COMPATIBLE
        // Switch to the boot frequency
          ddr_external_set_clk_speed (boot_ddr_freq);

        // Switch to the training frequency
          ddr_external_set_clk_speed (curr_training_freq_khz);
#endif
        }

#endif // DSF_PASS_1_DCC_TRAINING_EN
   


        for (ch = 0; ch < NUM_CH; ch++)
        {
           // reg_offset_ddr_phy = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH (ch);
            //reg_offset_global0 = ddr->base_addr.bimc_base_addr + REG_OFFSET_GLOBAL0;
            
            if ((channel >> ch) & 0x1)
            {
//================================================================================================//
// CA-Vref
//================================================================================================//
#if DSF_CA_TRAINING_EN
                // Disable DPE and SHKE so that in freq switch, MR write will not happen
               // HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xE);
               // HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x6);
               //
               // tmpcatdq0_1 = HWIO_INXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS );
               // tmpcatdq0_2 = HWIO_INXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   );
               // tmpcatdq1_1 = HWIO_INXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS );
               // tmpcatdq1_2 = HWIO_INXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   );
               // tmpcatdq2_1 = HWIO_INXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS );
               // tmpcatdq2_2 = HWIO_INXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   );
               // tmpcatdq3_1 = HWIO_INXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS );
               // tmpcatdq3_2 = HWIO_INXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   );
               //
               //
               // // PHY CA0 training entry
               // DDR_PHY_hal_cfg_ca_dq_in  (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET);
               // DDR_PHY_hal_cfg_ca_dq_in  (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET);
               // DDR_PHY_hal_cfg_ca_dq_in  (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET);
               // DDR_PHY_hal_cfg_ca_dq_in  (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET);
                //DDR_PHY_hal_cfg_ca_ca     (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET);
               // 
               // // Enable DQ PAD IE
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x3FF);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x3FF);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x3FF);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x3FF);

                       ddr_printf(DDR_NORMAL, "\n  START: CA training on Ch: %u Rank: %u\n", ch, cs);

                       // CA Training call


                       DDRSS_ca_vref_lpddr3 (ddr,                                          
                                   (ddrss_ca_vref_local_vars *)local_vars,                                                     
                                             ch,
                                             cs,
                                             training_params_ptr,
                                             curr_training_freq_khz,
                                             training_indx,
                                             prfs_index
                                            );
                       // Store local vars in flash and zero-initialize for use by the next training function.
                       ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);
                       

                       ddr_printf(DDR_NORMAL, "\n  END: CA training on Ch: %u Rank: %u\n\n", ch, cs);
               
                // Disable DQ PAD IE
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x0 );
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x0 );
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x0 );
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_MODE_IE_DQ, 0x0 );
               //
               //                 
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS ,tmpcatdq0_1);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   ,tmpcatdq0_2);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS ,tmpcatdq1_1);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   ,tmpcatdq1_2);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS ,tmpcatdq2_1);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   ,tmpcatdq2_2);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_N_DQS ,tmpcatdq3_1);
               // HWIO_OUTXF ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET) ,   DDR_PHY_DDRPHY_PAD_CNTL_0_CFG , PULL_DQS   ,tmpcatdq3_2);
               //                 
               //                 
               //                 
               //                 
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, 0);
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
               //
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0);
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
               //
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0);
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
               //
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, 0);
               // HWIO_OUTX ((reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, 0);
               //
               // // Enable DPE and SHKE so that in freq switch, MR write will not happen
               // HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xF);
               // HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x7);
        
#endif  // DSF_CA_VREF_TRAINING_EN


 //================================================================================================//
 // Write Leveling
 // This training needs to be done for NUM_TRAINING_FREQS starting from the max freq.
 //===============================================================================================//
#if DSF_WRLVL_TRAINING_EN
     ddr_external_set_clk_speed (curr_training_freq_khz);
    
     // Enable write leveling in the DRAM using MR2 write
     new_RL_WL_idx  = BIMC_RL_WL_Freq_Index (ddr, curr_training_freq_khz);

     ddr_printf(DDR_NORMAL, "  START: WRLVL training \n");                        

     // Enable write leveling in the DRAM using MR2 write
     MR2_wrlvl_opcode = ((1 << 7) | ddr->extended_cdt_runtime.dram_latency[new_RL_WL_idx].MR2);

           // Train both ranks and use the maximum value
                   BIMC_MR_Write (ddr, CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, MR2_wrlvl_opcode);
                   DDRSS_wrlvl_ca (ddr, 
                                   ch, 
                                   cs, 
                                   curr_training_freq_khz, // frequency
                                   training_params_ptr, 
                                   training_indx                       
                                   );
       

                   BIMC_MR_Write (ddr, CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_2, MR2_wrlvl_opcode);    
               //  ddr_printf(DDR_NORMAL, "START: WRLVL training on Ch: %u Rank: %u\n", ch, cs);                        
                   DDRSS_wrlvl (ddr, 
                                ch, 
                                cs, 
                                curr_training_freq_khz, 
                                training_indx, 
                                training_params_ptr);

     BIMC_MR_Write (ddr, channel,chip_select,JEDEC_MR_2,(ddr->extended_cdt_runtime.dram_latency[new_RL_WL_idx].MR2));       
     ddr_printf(DDR_NORMAL, "  END: WRLVL training \n");
 
#endif  // DSF_WRLVL_TRAINING_EN


//================================================================================================//
// Read Training (DQ-DQS and RCW)
//================================================================================================//
#if DSF_RD_DQDQS_TRAINING_EN
        // Switch to boot freq to do a low speed memory write to be used for read back during training
        ddr_external_set_clk_speed (boot_ddr_freq);
    
        // Enable refresh so that memory contents are retained after low speed write
        BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_ENABLE/*1 for enable*/);
         
        // Memory write at low speed
                    DDRSS_mem_write (ddr, ch, cs);

        // Switch to max freq to do dq-dqs training
        ddr_external_set_clk_speed (curr_training_freq_khz);
    
                    ddr_printf(DDR_NORMAL, "\n  START: Read training on Ch: %u Rank: %u\n", ch, cs);
                    DDRSS_rd_dqdqs_lpddr3 (ddr, 
                                           ch, 
                                           cs, 
                                           training_params_ptr, 
                                           (ddrss_rdwr_dqdqs_local_vars *)local_vars, 
                                           curr_training_freq_khz,
                                           training_indx,
                                           prfs_index);
                    ddr_printf(DDR_NORMAL, "\n  END: Read training on Ch: %u Rank: %u\n\n", ch, cs);
    
                    // Store local vars in flash and zero-initialize for use by the next training function.
                    ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);

#endif  // DSF_RD_DQDQS_TRAINING_EN


//================================================================================================//
// Write DQ-DQS Training
//================================================================================================//
#if DSF_WR_DQDQS_TRAINING_EN
        
                        ddr_printf(DDR_NORMAL, "\n  START: Write training on Ch: %u Rank: %u\n", ch, cs);
                        DDRSS_wr_dqdqs_lpddr3 (ddr, 
                                               ch, 
                                               cs, 
                                               training_params_ptr, 
                                               (ddrss_rdwr_dqdqs_local_vars *)local_vars, 
                                               curr_training_freq_khz,
                                               training_indx,
                                               prfs_index);
                        
                        ddr_printf(DDR_NORMAL, "\n  END: Write training on Ch: %u Rank: %u\n\n", ch, cs);
                        
                        // Store local vars in flash and zero-initialize for use by the next training function.
                        ddr_external_page_to_storage(local_vars, LOCAL_VARS_AREA_SIZE);               
       

        
#endif  // DSF_WR_DQDQS_TRAINING_EN
            }
        }

        // Disable Auto refresh at the beginning of training of each frequency
        BIMC_Auto_Refresh_Ctrl (ddr, channel, chip_select, FEATURE_DISABLE/*0 for disable*/);        

        // Put back the passed in chip_select saved at the beginning    
        //chip_select = qualified_cs;   
        ddr_printf(DDR_NORMAL,"========================================================\n");
        ddr_printf(DDR_NORMAL, "END: DDR training at freq: %u\n", curr_training_freq_khz);
        ddr_printf(DDR_NORMAL,"========================================================\n\n");
        
        if(prfs_index >0) // KW compliance
        {
          prfs_index--;
        }

    } // end of freq loop.
    

    for (ch = 0; ch < NUM_CH; ch++)
    {
        reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
        if ((channel >> ch) & 0x1)
        {
            // Enable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR
            BIMC_All_Periodic_Ctrl (ddr, CH_1HOT(ch), 
                                   (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect, 
                                    FEATURE_ENABLE/*1 for enable*/);

            // Enable Power Down
            HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x1, 0x1);
        }
    }
    
    return TRUE;
    
}  // END of DDRSS_boot_training_lpddr3.


// --------------------------------------------------
// Training results restore
// --------------------------------------------------
void DDRSS_training_restore_lpddr3 (DDR_STRUCT *ddr)
{
    uint8  ch                = 0;
    uint8  ca                = 0;
    uint32 dq0_ddr_phy_base  = 0;
    uint32 ca0_ddr_phy_base  = 0;
		uint8      remapped_line=0;
		uint8      remapped_bit=0;
uint32 reg_offset_ddr_ca[NUM_CA_PCH]  = {0}; 
    
#if   DSF_CA_TRAINING_EN
    uint8  cs                = 0;
   #elif DSF_WRLVL_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint8  cs                = 0;
#elif DSF_WR_DQDQS_TRAINING_EN
    uint8  cs                = 0;

#endif

#if DSF_DCC_TRAINING_DQ_DCC_EN  
    uint8  dq                = 0;
#elif DSF_WRLVL_TRAINING_EN
    uint8  dq                = 0;
#elif DSF_RD_DQDQS_TRAINING_EN
    uint8  dq                = 0;
#elif DSF_WR_DQDQS_TRAINING_EN
    uint8  dq                = 0;
#endif

#if DSF_RD_DQDQS_TRAINING_EN
    uint8  bit               = 0;
#elif DSF_WR_DQDQS_TRAINING_EN
    uint8  bit               = 0;
#endif

    int8  training_freq_indx         = 0; 
    int8  training_indx              = 0;
    uint8  max_training_ddr_freq_indx = TRAINING_NUM_FREQ_LPDDR3 - 1;  //Set the max number of training frequecies  
    uint8  prfs_index                 = TRAINING_START_PRFS; 
    uint8  max_clock_index            = ddr->misc.ddr_num_clock_levels - 1;
    uint32 curr_training_freq_khz     = 0;

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Restore each training frequency

    // Train at selected number of training frequecies 
    for (training_indx = max_training_ddr_freq_indx; training_indx >= 0;training_indx--) 
    {
        for(training_freq_indx = max_clock_index; training_freq_indx > 0; training_freq_indx--)// loop for clock frequency table
        {
            if((ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] ) && (prfs_index > 0))
            {
                while( (prfs_index > 0) && (ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz < freq_range[prfs_index -1]) )
                {
                    prfs_index--;
                }
                break;
            }
            else if (prfs_index ==0)
            {
                if(ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz<= freq_range[prfs_index] )
                {
                 break;
                }
            }
        }

          curr_training_freq_khz = ddr->misc.clock_plan[training_freq_indx].clk_freq_in_khz;
          max_clock_index        = training_freq_indx--;


      //--------------------------------------------------------------------------------------
      // Restore DCC values
      //--------------------------------------------------------------------------------------
      for (ch = 0; ch < NUM_CH; ch++) 
      {
#if DSF_DCC_TRAINING_DQ_DCC_EN  
        // DDR PHY DQ base address
        if(training_indx == max_training_ddr_freq_indx)
        {
             dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
        
        // Restore DQ PHY DCC
            for (dq = 0; dq < NUM_DQ_PCH; dq++) 
            {
            // Select the CSR for the adjusters
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,CM_MODE,  0); // WRLVL
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,CM_ADJ_DQ_SW_OVRD, 1); // W90
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,IO_MODE,  0); // IO

            // IO DQ not trained
            //HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,IO_ADJ_DQ_SW_OVRD,  1);
            
            // Open the latch enable to drive the trained value into the adjuster
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WR_CDC_LATCH_EN, 1);
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WRLVL_CDC_LATCH_EN, 1);
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_4_CFG, PAD_DCC_DQS_LAT_EN             , 1); 

            // Restore the training registers from the data structure
  
            // WRLVL CDC
                 HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,CM_MAN_ADJ, 
                        training_data_ptr->results.dcc.wrlvl_stat_dq [ch][dq]);

            // WR90 CDC
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,CM_ADJ_DQ_SW_OVRD_VAL,
                        training_data_ptr->results.dcc.t90_stat_dq[ch][dq]);

            // IO DQS
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_1_CFG,IO_MAN_ADJ,
                        training_data_ptr->results.dcc.iodqs_stat_dq [ch][dq]);

            // IO DQ not trained
            //HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,IO_ADJ_DQ_SW_OVRD_VAL,  
            //            training_data_ptr->results.dcc.iodq_stat_dq[ch][dq]);

            // Close the latch enables 
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WR_CDC_LATCH_EN, 0);
                HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WRLVL_CDC_LATCH_EN, 0);
        
            }//dq
         }
#endif

#if DSF_DCC_TRAINING_CA_DCC_EN  
        if(training_indx == max_training_ddr_freq_indx)
        {
        // Restore CA PHY DCC
             ca0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);
            for (ca = 0; ca < NUM_CA_PCH; ca++) 
            {
            // Set up DCC restore mode
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,CM_MODE,  0); // WRLVL
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,CM_ADJ_DQ_SW_OVRD, 1); // W90
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,IO_MODE,  0); // IO
            
            // IO DQ not trained
            //HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,IO_ADJ_DQ_SW_OVRD,  1);
            
            // Open the latch enable to drive the trained values into the adjusters
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WRLVL_CDC_LATCH_EN, 1);
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WR_CDC_LATCH_EN, 1);
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_4_CFG, PAD_DCC_DQS_LAT_EN             , 1); 

            // Restore the training registers from the data structure

            // WRLVL CDC
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_0_CFG,CM_MAN_ADJ, 
                        training_data_ptr->results.dcc.wrlvl_stat_ca [ch][ca]);

            // WR90 CDC
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,CM_ADJ_DQ_SW_OVRD_VAL,
                        training_data_ptr->results.dcc.t90_stat_ca[ch][ca]);

            // IO DQS
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_1_CFG,IO_MAN_ADJ,
                        training_data_ptr->results.dcc.iodqs_stat_ca [ch][ca]);

            // IO DQ not trained
            //HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG,IO_ADJ_DQ_SW_OVRD_VAL,  
            //            training_data_ptr->results.dcc.iodq_stat_ca[ch][ca]);

            // Close the latch enable 
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WRLVL_CDC_LATCH_EN, 0);
                HWIO_OUTXF (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_DCC_TOP_3_CFG, CM_DCC_CTLR_WR_CDC_LATCH_EN, 0);

        
            }//ca
        }
#endif

    //--------------------------------------------------------------------------------------
    // Restore CA VREF values
    //--------------------------------------------------------------------------------------
#if DSF_CA_TRAINING_EN
            // Set ddr phy base address for CA PHYs
            ca0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);
    reg_offset_ddr_ca[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_ca[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;

	 if(training_indx == max_training_ddr_freq_indx)
    {

        //for (ca = 0; ca < NUM_CA_PCH; ca++) 
        //   {
               for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++) 
               {
                   remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
                   remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
		   
                   DDR_PHY_hal_cfg_pbit_dq_delay(reg_offset_ddr_ca[remapped_line], 
                                                  remapped_bit, 
                                                  1, // 1 for Tx
                                                  cs, 
                                                  training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]);  
           }
        //  }
	}
                    for (ca = 0; ca < NUM_CA_PCH; ca++) 
                    {
                        // Restore the Coarse CDC
                        DDR_PHY_hal_cfg_cdcext_slave_wr (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), 
                                                         cs, 
                                                         training_data_ptr->results.ca.coarse_cdc[training_indx][ch][cs][ca], 
                                                         1/*coarse*/, 
                                                         HP_MODE, 
                                                         prfs_index
                                                        );               
                        
                        // Restore the Fine CDC
                        DDR_PHY_hal_cfg_cdcext_slave_wr (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), 
                                                         cs, 
                                                         training_data_ptr->results.ca.fine_cdc[training_indx][ch][cs][ca], 
                                                         0/*fine*/, 
                                                         HP_MODE, 
                                                         prfs_index
                                                        );
                                        
                        // Restore the CA retimer values
                        DDR_PHY_hal_cfg_wrlvlext_ctl_update (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), 
                                                             prfs_index, 
                                                             cs, 
                                                             training_data_ptr->results.ca.dqs_retmr     [training_indx][ch][cs][ca], 
                                                             training_data_ptr->results.ca.dqs_half_cycle[training_indx][ch][cs][ca], 
                                                             training_data_ptr->results.ca.dqs_full_cycle[training_indx][ch][cs][ca]
                                                            );

                    } //ca
					 if (training_indx == 0)
                         { 
                            Scale_ca_dqdqs_lpddr3 (  ddr, 
                                                     ch,
                                                     cs,
                                                     training_indx,
                                                     curr_training_freq_khz, 
                                                     prfs_index - 1);
                          }
#endif
  
    //--------------------------------------------------------------------------------------
    // Restore WRLVL values
    //--------------------------------------------------------------------------------------
#if  DSF_WRLVL_TRAINING_EN

            // DDR PHY base address
            dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            // Set ddr phy base address for CA PHYs
            ca0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);

            // Restore WRLVL CA CDC values ; CA does not do rank switching
            for (ca = 0; ca < NUM_CA_PCH; ca++) 
            {
                // Restore CA Retimer values
                DDR_PHY_hal_cfg_wrlvlext_ctl_update(ca0_ddr_phy_base  + (ca * DDR_PHY_OFFSET), 
                                                    prfs_index, 
                                                    0, 
                                                    training_data_ptr->results.wrlvl.ca_dqs_retmr     [training_indx][ch][cs][ca], 
                                                    training_data_ptr->results.wrlvl.ca_dqs_half_cycle[training_indx][ch][cs][ca],
                                                    training_data_ptr->results.wrlvl.ca_dqs_full_cycle[training_indx][ch][cs][ca]);
                
                // Restore CA Coarse CDC values
                DDR_PHY_hal_cfg_cdcext_wrlvl_update(ca0_ddr_phy_base  + (ca * DDR_PHY_OFFSET),
                                              prfs_index, 
                                              0,
                                              0,
                                              training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[training_indx][ch][cs][ca]);
            }// ca           

                for (dq = 0; dq < NUM_DQ_PCH; dq++) 
                {
                    // Restore WRLVL Retimer values
                    DDR_PHY_hal_cfg_wrlvlext_ctl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET), 
                                                        prfs_index, 
                                                        cs, 
                                                        training_data_ptr->results.wrlvl.dq_dqs_retmr     [training_indx][ch][cs][dq], 
                                                        training_data_ptr->results.wrlvl.dq_dqs_half_cycle[training_indx][ch][cs][dq],
                                                        training_data_ptr->results.wrlvl.dq_dqs_full_cycle[training_indx][ch][cs][dq]);
                    
                    // Restore WRLVL DQS CDC values
                    DDR_PHY_hal_cfg_cdcext_wrlvl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET),
                                                  prfs_index, 
                                                  cs,
                                                  training_data_ptr->results.wrlvl.dq_fine_dqs_delay  [training_indx][ch][cs][dq],
                                                  training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[training_indx][ch][cs][dq]);
                    
        }// ch
#endif

    //--------------------------------------------------------------------------------------
    // Restore RCW values
    //--------------------------------------------------------------------------------------
    
    // No RCW restore required.
    
    //--------------------------------------------------------------------------------------
    // Restore RD_DQDQS values
    //--------------------------------------------------------------------------------------
#if DSF_RD_DQDQS_TRAINING_EN
    if(training_indx == max_training_ddr_freq_indx)
    {
        // Set DDR PHY base address
            dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
                
                for (dq = 0; dq < NUM_DQ_PCH; dq++) 
                {
                // Restore Per-Bit CDC values
                    for (bit = 0; bit < PINS_PER_PHY; bit++) 
                    {
                        DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET)), 
                                                  bit, 
                                                  0, // 0 for RX 
                                                  cs, 
                                                  training_data_ptr->results.rd_dqdqs.perbit_delay[ch][cs][dq][bit]); 
                    }//for
                    // Write the DCC_CTL register with the final value
                    HWIO_OUTXF((dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET)), 
                                DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                                DCC_CTL, 
                                training_data_ptr->results.rd_dqdqs.rd_dcc[ch][dq]);
                }//dq    
     }//if
    // Restore rd_dqdqs CDC values
    
            // Set DDR PHY base address
            dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
                    
                for (dq = 0; dq < NUM_DQ_PCH; dq++) 
                {
                    DDR_PHY_hal_cfg_cdcext_slave_rd (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), 
                                                     cs, 
                                                     training_data_ptr->results.rd_dqdqs.coarse_cdc [training_indx][ch][cs][dq], 
                                                     1/*coarse*/, 
                                                     HP_MODE, 
                                                     prfs_index
                                                    );   
                    // Restore fine CDC values
                    DDR_PHY_hal_cfg_cdcext_slave_rd (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), 
                                                     cs, 
                                                     training_data_ptr->results.rd_dqdqs.fine_cdc [training_indx][ch][cs][dq], 
                                                     0/*fine*/, 
                                                     HP_MODE, 
                                                     prfs_index
                                                    ); 
                }
                if (training_indx == 0) 
                { //offset calculation:  offset = calculated center - T/4 
                     Scale_rd_dqdqs_lpddr3 (  ddr, 
                                 ch,
                                 cs,
                                 training_indx,
                                 curr_training_freq_khz, 
                                 prfs_index - 1);
                 }   
#endif

    //--------------------------------------------------------------------------------------
    // Restore WR_DQDQS values
    //--------------------------------------------------------------------------------------
#if DSF_WR_DQDQS_TRAINING_EN
     
            // Set the PHY base address
            dq0_ddr_phy_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
                for (dq = 0; dq < NUM_DQ_PCH; dq++) 
                {
                 if(training_indx == max_training_ddr_freq_indx)
                 {   
                    // Restore Per-bit CDC
                    for (bit = 0; bit < PINS_PER_PHY; bit++) 
                    {
                        DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET)), 
                                                     bit, 
                                                     1, // 1 for Tx
                                                     cs, 
                                                     training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][dq][bit]);  
                    }
                 }//END OF if(training_indx
                    // Restore the Coarse CDC
                    DDR_PHY_hal_cfg_cdcext_slave_wr (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), 
                                                    cs, 
                                                    training_data_ptr->results.wr_dqdqs.coarse_cdc[training_indx][ch][cs][dq], 
                                                    1/*coarse*/, 
                                                    HP_MODE, 
                                                    prfs_index
                                                   );
                    
                    
                    // Restore the Fine CDC
                    DDR_PHY_hal_cfg_cdcext_slave_wr (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), 
                                                    cs, 
                                                    training_data_ptr->results.wr_dqdqs.fine_cdc[training_indx][ch][cs][dq], 
                                                    0/*fine*/, 
                                                    HP_MODE, 
                                                    prfs_index
                                                   );

                    // Restore Retimer, HC and FC values
                    DDR_PHY_hal_cfg_wrlvlext_ctl_update (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), 
                                                        prfs_index, 
                                                        cs, 
                                                        training_data_ptr->results.wr_dqdqs.dqs_retmr     [training_indx][ch][cs][dq], 
                                                        training_data_ptr->results.wr_dqdqs.dqs_half_cycle[training_indx][ch][cs][dq], 
                                                        training_data_ptr->results.wr_dqdqs.dqs_full_cycle[training_indx][ch][cs][dq]
                                                       );
                } // dq
                if (training_indx == 0)
                { 
                     Scale_wr_dqdqs_lpddr3 (  ddr, 
                             ch,
                             cs,
                             training_indx,
                             curr_training_freq_khz, 
                             prfs_index - 1);
                }
#endif  
        }//ch
    if(prfs_index >0)
    {
    prfs_index--;
    }
  } // for training freq
//ddr_external_set_clk_speed (curr_training_freq_khz);
} // DDRSS_restore_lpddr3

