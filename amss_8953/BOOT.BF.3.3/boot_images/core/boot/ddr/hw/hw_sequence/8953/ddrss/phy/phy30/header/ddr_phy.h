////////////////////////////////////////////////////////////////////////////////////////////////
//===========================================================================
//  Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
//  QUALCOMM Proprietary and Confidential. 
//===========================================================================
////////////////////////////////////////////////////////////////////////////////////////////////
#include "HAL_SNS_DDR.h"
#include "ddr_ss_seq_hwiobase.h"
#include "ddr_ss_seq_hwioreg.h"
#include "target_config.h"
#define DQ_PER_BYTE  9

//#include "ddr_phy_hal_sta_ca_vref.h"
uint32 DDR_PHY_hal_sta_ca( uint32 _inst_ );
void DDR_PHY_hal_cfg_ca_dq_in( uint32 _inst_ );
void DDR_PHY_hal_cfg_dq_ca_training_entry( uint32 _inst_ );
void DDR_PHY_hal_cfg_dq_ca_training_exit( uint32 _inst_ );
void DDR_PHY_hal_cfg_ca_ca( uint32 _inst_ );


//#include "ddr_phy_hal_cfg_init.h"

void DDR_PHY_hal_cfg_sw_iocal( uint32 _inst_, uint8 master_phy );
void DDR_PHY_hal_cfg_sw_handshake_start( uint32 _inst_, uint32 clk_freq_khz );
void DDR_PHY_hal_cfg_sw_handshake_stop( uint32 _inst_, uint32 clk_freq_khz );
void DDR_PHY_hal_cfg_sw_handshake_complete( uint32 _inst_ );
void DDR_PHY_hal_hpcdc_enable(DDR_STRUCT *ddr, uint8 enable);
//#include "ddr_phy_hal_cfg_cdcext.h"

uint8 DDR_PHY_cdcext_msb_lsb( uint32 _inst_, uint16 rank, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no, uint8 msb_lsb );
void DDR_PHY_hal_cfg_cdcext_slave_rcw( uint32 _inst_, uint16 rank, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no );
void DDR_PHY_hal_cfg_cdcext_slave_rd( uint32 _inst_, uint16 rank, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no );
void DDR_PHY_hal_cfg_cdcext_slave_wr( uint32 _inst_, uint16 rank, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint16 prfs_no );

//#include "ddr_phy_hal_cfg_cdc.h"

void DDR_PHY_hal_cfg_cdc_rcw( uint32 _inst_, uint32 enable, uint32 num_cycle );
void DDR_PHY_hal_cfg_cdc_slave_rcw( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank );
void DDR_PHY_hal_cfg_cdc_slave_rd( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank );
void DDR_PHY_hal_cfg_cdc_slave_wrlvl( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank );
void DDR_PHY_hal_cfg_cdc_slave_wr( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank );
void DDR_PHY_hal_cfg_cdc_slave_wr_bcm( uint32 _inst_, uint32 delay, uint16 coarse_delay, uint16 hp_mode, uint8 rank );
void DDR_PHY_hal_cfg_cdc_slave_wr_cdc( uint32 _inst_, uint32 cdc );

//#include "ddr_phy_hal_cfg_dcc.h"

//void DDR_PHY_hal_cfg_dcc_wrlvl_t90_boot( uint32 _inst_, uint16 start, uint16 div_ref );
//void DDR_PHY_hal_cfg_dcc_wrlvl_tmin_boot( uint32 _inst_, uint16 start, uint16 div_ref );

//#include "ddr_phy_hal_cfg_pbit.h"

void DDR_PHY_hal_cfg_pbit_dq_all( uint32 _inst_, uint32 txrx, uint16 rank, uint16 delay, uint16 all, uint8 vector[DQ_PER_BYTE] );
void DDR_PHY_hal_cfg_pbit_dq_delay( uint32 _inst_, uint16 dq_num, uint16 txrx, uint16 sel, uint16 delay );
void DDR_PHY_hal_cfg_pbit_dqs_delay( uint32 _inst_, uint16 txrx, uint16 rank, uint16 delay );



//#include "ddr_phy_hal_sta_wrlvl.h"

uint32 DDR_PHY_hal_sta_wrlvl_coarse( uint32 _inst_, uint8 rank );
uint32 DDR_PHY_hal_sta_wrlvl_fine( uint32 _inst_, uint8 rank );
uint32 DDR_PHY_hal_sta_wrlvl_training( uint32 _inst_ );
uint32 DDR_PHY_hal_sta_wrlvl_full( uint32 _inst_, uint8 rank );

//#include "ddr_phy_hal_cfg_wrlvl.h"

uint8 DDR_PHY_hal_sta_rcw_coarse( uint32 _inst_, uint8 rank );
uint8 DDR_PHY_hal_sta_rcw_fine( uint32 _inst_, uint8 rank );
uint8 DDR_PHY_hal_sta_rcw_num_cycle( uint32 _inst_ );
void DDR_PHY_rd_mon_status( uint32 _inst_, uint8 pass[1] );
void DDR_PHY_hal_cfg_wrlvl_delay_set( uint32 _inst_, uint8 rank, uint32 fine_delay, uint32 coarse_delay );
void DDR_PHY_hal_cfg_cdcext_wrlvl_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 fine_delay, uint32 coarse_delay );
void DDR_PHY_hal_cfg_wrlvl_set( uint32 _inst_, uint32 rank, uint32 dqs_retmr, uint32 dqs_half_cycle, uint32 dqs_full_cycle );
void DDR_PHY_hal_cfg_wrlvl_retmr( uint32 _inst_, uint8 rank, uint8 dqs_retmr, uint8 half_cycle, uint8 full_cycle );
void DDR_PHY_hal_cfg_wrlvlext_ctl_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 retmr, uint32 half_cycle, uint32 full_cycle );
void DDR_PHY_hal_cfg_wrlvl_dq_set( uint32 _inst_, uint32 rank, uint32 dq_retmr, uint32 dq_half_cycle, uint32 dq_full_cycle );
void DDR_PHY_hal_cfg_wrlvl_half( uint32 _inst_, uint32 rank, uint32 dq_half_cycle );
void DDR_PHY_hal_cfg_wrlvlext_retimer_update( uint32 _inst_, uint8 period_index, uint8 rank, uint32 retmr);

//#include "ddr_phy_rtn_training_dcc.h"

uint32 DDR_PHY_rtn_training_dcc_wrlvl(DDR_STRUCT *ddr, uint32 _inst_, uint8 freq_band, uint8 pll);
uint32 DDR_PHY_rtn_training_dcc_t90(DDR_STRUCT *ddr, uint32 _inst_, uint8 freq_band);
uint32 DDR_PHY_rtn_training_dcc_io_dqs(DDR_STRUCT *ddr, uint32 _inst_, uint8 freq_band);
void DDR_PHY_hal_dcc_ext (uint32 _inst_, uint8 freq_band, uint8 pll, uint8 cm_io, uint8 wrlvl_t90, uint32 stat);

//#include "ddr_phy_hal_cfg_dq_vref.h"

void DDR_PHY_hal_cfg_dq_vref( uint32 _inst_ );
//void DDR_PHY_hal_cfg_dq_vref_dqs2dq_max( uint32 _inst_, uint8 rank, uint8 prfs_num, uint8 delay, uint8 coarse_dly );
//void DDR_PHY_hal_cfg_dq_vref_dqs2dq_min( uint32 _inst_, uint8 rank, uint8 prfs_num, uint8 delay, uint8 coarse_dly );
//void DDR_PHY_hal_cfg_dq_vref_cen_cfg_min( uint32 _inst_, uint8 rank, uint8 prfs_num, uint8 full_cycle, uint8 half_cycle, uint8 retimer );
//void DDR_PHY_hal_cfg_dq_vref_cen_cfg_max( uint32 _inst_, uint8 rank, uint8 prfs_num, uint8 full_cycle, uint8 half_cycle, uint8 retimer );


//#include "ddr_phy_hal_cfg_ca_vref.h"

void DDR_PHY_hal_cfg_ca_dq_retmr( uint32 _inst_, uint8 rank, uint32 dq_retmr );
