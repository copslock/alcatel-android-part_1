/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"

#define COMPILE_TIME_ASSERTION(exp) ((void)sizeof(char[1 - 2*!!((exp)-1)]))
//================================================================================================//
// DDR Initialization
//================================================================================================//
boolean HAL_DDR_Init (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,
                      uint32 clk_freq_khz)
{
   DDR_CHANNEL ch_1hot = DDR_CH_NONE;
   uint8 ch = 0;

   EXTENDED_CDT_STRUCT *ecdt;
   COMPILE_TIME_ASSERTION(sizeof(ddrss_rdwr_dqdqs_local_vars) < 65536);
   COMPILE_TIME_ASSERTION(sizeof(ddrss_ca_vref_local_vars) < 65536);
   // Fill in the current version numbers for the DDR System Firmware, the BIMC Core and the PHY core.
   ddr->version            = TARGET_DDR_SYSTEM_FIRMWARE_VERSION;
   ddr->ctlr.version.arch  = TARGET_BIMC_ARCH_VERSION;
   ddr->ctlr.version.major = TARGET_BIMC_CORE_MAJOR_VERSION;
   ddr->ctlr.version.minor = TARGET_BIMC_CORE_MAJOR_VERSION;
   ddr->phy.version.major  = TARGET_PHY_CORE_MAJOR_VERSION;
   ddr->phy.version.minor  = TARGET_PHY_CORE_MAJOR_VERSION;

   //update DDR bootup frequency in DDR_STRUCT
   ddr->misc.ddr_boot_freq = clk_freq_khz;

   ecdt = (EXTENDED_CDT_STRUCT *)((size_t)(ddr->ecdt_input));
   // Execute Pre-Init function for workarounds, if any.
   //DDRSS_Pre_Init(ddr);
   DDRSS_Pre_Init(ddr, ecdt);
   // BIMC one-time settings
   BIMC_Config(ddr);

   #if !defined(FEATURE_RUMI_BOOT)// Don't configure Phy & clock controller on RUMI --- they are not included in the HW build
   // DDR PHY and CC one-time settings
   DDR_PHY_CC_Config(ddr);
   #endif // #if FEATURE_RUMI_BOOT

   BIMC_Pre_Init_Setup (ddr, channel, chip_select, clk_freq_khz);

   #if !defined(FEATURE_RUMI_BOOT)// Don't configure Phy & clock controller on RUMI --- they are not included in the HW build
   DDR_PHY_CC_init (ddr, channel, clk_freq_khz);  //workaround 19.2MHz SW switching hang issue.
   #endif // #if FEATURE_RUMI_BOOT

   // For LPDDR4, deassert reset pin before initialization
   if (ddr->cdt_params[0].common.device_type == DDR_TYPE_LPDDR4)
   {
      HWIO_OUTX (ddr->base_addr.ddrss_base_addr + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD, 1);
   }

   BIMC_Memory_Device_Init (ddr, channel, chip_select, clk_freq_khz);

   for (ch = 0; ch < NUM_CH; ch++)
   {
      ch_1hot = CH_1HOT(ch);

      if ((channel >> ch) & 0x1)
      {
        ddr->cdt_params[ch].common.populated_chipselect = BIMC_DDR_Topology_Detection (ddr, ch_1hot, chip_select);
      }
   }

   for (ch = 0; ch < NUM_CH; ch++)
   {
      ch_1hot = CH_1HOT(ch);

      if ((channel >> ch) & 0x1)
      {
         BIMC_Post_Init_Setup (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
      }
   }

     for (ch = 0; ch < NUM_CH; ch++)
     {
        ch_1hot = CH_1HOT(ch);
        if ((channel >> ch) & 0x1)
        {
          DDRSS_Post_Init(ddr , ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
        }
     }
   return TRUE;
}

//================================================================================================//
// DDR Device Mode Register Read
//================================================================================================//
uint32 HAL_DDR_Read_Mode_Register (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,
                                   uint32 MR_addr)
{
   return BIMC_MR_Read (ddr, channel, chip_select, MR_addr);
}

//================================================================================================//
// DDR Device Mode Register Write
//================================================================================================//
boolean HAL_DDR_Write_Mode_Register (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,
                                     uint32 MR_addr, uint32 MR_data)
{
   BIMC_MR_Write (ddr, channel, chip_select, MR_addr, MR_data);

   return TRUE;
}

//================================================================================================//
// ZQ Calibration
//================================================================================================//
boolean HAL_DDR_ZQ_Calibration (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_ZQ_Calibration (ddr, channel, chip_select);

   return TRUE;
}

//============================================================================
// HAL_DDR_Enter_Deep_Power_Down
//============================================================================
boolean HAL_DDR_Enter_Deep_Power_Down(DDR_STRUCT *ddr, DDR_CHANNEL channel,
  DDR_CHIPSELECT chip_select)
{
    return(BIMC_Enter_Deep_Power_Down(ddr, channel, chip_select));
}

//============================================================================
// HAL_DDR_Exit_Deep_Power_Down
//============================================================================
boolean HAL_DDR_Exit_Deep_Power_Down(DDR_STRUCT *ddr, DDR_CHANNEL channel,
  DDR_CHIPSELECT chip_select, uint32 clkspeed)
{
    return(BIMC_Exit_Deep_Power_Down(ddr, channel, chip_select));
}



/*
//================================================================================================//
// DDR Post Boot Training Setup.
//================================================================================================//
boolean DDRSS_Post_Boot_Training(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{    
    EXTENDED_CDT_STRUCT *ecdt;
    //DDR_CHIPSELECT qualified_cs = (DDR_CHIPSELECT)(chip_select & ddr->cdt_params[0].common.populated_chipselect);
    ecdt = (EXTENDED_CDT_STRUCT *)((size_t)(ddr->ecdt_input));

    // PHY eCDT.
    if(ecdt != NULL)
    {
        DDR_PHY_CC_eCDT_Override(ddr, ecdt, channel); 
    }
    
    // Refresh rate control through TUF bit override
    //BIMC_Refresh_Rate_Ctrl (channel, qualified_cs);
    // Enables DownTime requests
    //BIMC_Downtime_Request_Ctrl ( channel, 1);
    

    
   return TRUE;
}
*/


