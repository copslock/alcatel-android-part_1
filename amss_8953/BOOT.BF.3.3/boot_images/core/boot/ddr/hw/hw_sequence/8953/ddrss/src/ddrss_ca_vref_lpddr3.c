/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_ca_vref_lpddr3.c#3 $
$DateTime: 2016/06/21 00:04:59 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
  
//================================================================================================//
// DDR PHY CA VREF training
//================================================================================================//

  uint16 ca_training_pattern_lpddr3[CA_PATTERN_NUM][6] = {

  { 0x0000, 0x0000, 0x03FF, 0x0000, 0x03FF, 0x03FF },   //ORIGINAL PATERN
  { 0x03FF, 0x03FF, 0x0000, 0x03FF, 0x0000, 0x0000 },   //ORIGINAL PATERN
  { 0x0155, 0x0155, 0x02aa, 0x0155, 0x02aa, 0x02aa },   //ORIGINAL PATERN
  { 0x02aa, 0x02aa, 0x0155, 0x02aa, 0x0155, 0x0155 }};  //ORIGINAL PATERN





void DDRSS_ca_training_bit_histogram_update(uint8 ca_mapping, uint16 failure, uint8 fail_count_histogram_perbit[PINS_PER_PHY_CONNECTED_CA])
{
  if(ca_mapping == 0)  {
    if ( failure & 0x0003 ) {fail_count_histogram_perbit[0] += 1; }
    if ( failure & 0x000C ) {fail_count_histogram_perbit[1] += 1; }
    if ( failure & 0x0030 ) {fail_count_histogram_perbit[2] += 1; }
    if ( failure & 0x00C0 ) {fail_count_histogram_perbit[3] += 1; }
    if ( failure & 0x0300 ) {fail_count_histogram_perbit[5] += 1; }
    if ( failure & 0x0C00 ) {fail_count_histogram_perbit[6] += 1; }
    if ( failure & 0x3000 ) {fail_count_histogram_perbit[7] += 1; }
    if ( failure & 0xC000 ) {fail_count_histogram_perbit[8] += 1; } //bypassing removed BIMC bug if ( failure & 0xC000 ) {fail_count_histogram_perbit[8] += 1; }
  }else  { //ca_mapping == 1
    if ( failure & 0x0003 ) {fail_count_histogram_perbit[4] += 1; }
    if ( failure & 0x0300 ) {fail_count_histogram_perbit[9] += 1; } //bypassing removed BIMC bug    if ( failure & 0x0300 ) {fail_count_histogram_perbit[9] += 1; }
  }                  
}

 void DDRSS_ca_training_bit_histogram_update_rise_fall(uint8 ca_mapping, uint16 failure, uint8 fail_count_histogram_perbit[3][NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA][HISTOGRAM_SIZE],uint16 nobe)
{
  if(ca_mapping == 0)  {
    if ( failure & 0x0001 ) {fail_count_histogram_perbit[0][0][0][nobe] += 1; fail_count_histogram_perbit[2][0][0][nobe] += 1; }
    if ( failure & 0x0004 ) {fail_count_histogram_perbit[0][0][1][nobe] += 1; fail_count_histogram_perbit[2][0][1][nobe] += 1; }
    if ( failure & 0x0010 ) {fail_count_histogram_perbit[0][0][2][nobe] += 1; fail_count_histogram_perbit[2][0][2][nobe] += 1; }
    if ( failure & 0x0040 ) {fail_count_histogram_perbit[0][0][3][nobe] += 1; fail_count_histogram_perbit[2][0][3][nobe] += 1; }
    if ( failure & 0x0100 ) {fail_count_histogram_perbit[0][0][5][nobe] += 1; fail_count_histogram_perbit[2][0][5][nobe] += 1; }
    if ( failure & 0x0400 ) {fail_count_histogram_perbit[0][0][6][nobe] += 1; fail_count_histogram_perbit[2][0][6][nobe] += 1; }
    if ( failure & 0x1000 ) {fail_count_histogram_perbit[0][0][7][nobe] += 1; fail_count_histogram_perbit[2][0][7][nobe] += 1; }
    if ( failure & 0x4000 ) {fail_count_histogram_perbit[0][0][8][nobe] += 1; fail_count_histogram_perbit[2][0][8][nobe] += 1; }

    if ( failure & 0x0002 ) {fail_count_histogram_perbit[1][0][0][nobe] += 1; fail_count_histogram_perbit[2][0][0][nobe] += 1; }
    if ( failure & 0x0008 ) {fail_count_histogram_perbit[1][0][1][nobe] += 1; fail_count_histogram_perbit[2][0][1][nobe] += 1; }
    if ( failure & 0x0020 ) {fail_count_histogram_perbit[1][0][2][nobe] += 1; fail_count_histogram_perbit[2][0][2][nobe] += 1; }
    if ( failure & 0x0080 ) {fail_count_histogram_perbit[1][0][3][nobe] += 1; fail_count_histogram_perbit[2][0][3][nobe] += 1; }
    if ( failure & 0x0200 ) {fail_count_histogram_perbit[1][0][5][nobe] += 1; fail_count_histogram_perbit[2][0][5][nobe] += 1; }
    if ( failure & 0x0800 ) {fail_count_histogram_perbit[1][0][6][nobe] += 1; fail_count_histogram_perbit[2][0][6][nobe] += 1; }
    if ( failure & 0x2000 ) {fail_count_histogram_perbit[1][0][7][nobe] += 1; fail_count_histogram_perbit[2][0][7][nobe] += 1; }
    if ( failure & 0x8000 ) {fail_count_histogram_perbit[1][0][8][nobe] += 1; fail_count_histogram_perbit[2][0][8][nobe] += 1; }

 	
	
	
  }else  { //ca_mapping == 1                            
    if ( failure & 0x0001 ) {fail_count_histogram_perbit[0][0][4][nobe] += 1; fail_count_histogram_perbit[2][0][4][nobe] += 1;}
    if ( failure & 0x0100 ) {fail_count_histogram_perbit[0][0][9][nobe] += 1; fail_count_histogram_perbit[2][0][9][nobe] += 1;}
                                                                                                 
    if ( failure & 0x0002 ) {fail_count_histogram_perbit[1][0][4][nobe] += 1; fail_count_histogram_perbit[2][0][4][nobe] += 1;}
    if ( failure & 0x0200 ) {fail_count_histogram_perbit[1][0][9][nobe] += 1; fail_count_histogram_perbit[2][0][9][nobe] += 1;}
	
	}
}  

void Scale_ca_dqdqs_lpddr3 (DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index)
{
    uint8 ca                                     =  0;
    uint8 clk_idx                                =  0;
    uint8 coarse_wrlvl_delay[NUM_CA_PCH]         = {0};
    uint8 fine_wrlvl_delay  [NUM_CA_PCH]         = {0};
    uint32 ca0_ddr_phy_base                      =  0;
    uint8 scale_prfs_indx                        =  0; 
    int   scale_freq_indx                        =  0; 
    int32 scale_offset_in_ps[NUM_CA_PCH]        = {0};
    uint32 scale_center_in_ps                    =  0;  
    uint32 scale_ca_coarse_cdc                   =  0;  
    uint32 scale_ca_fine_cdc                     =  0;
    uint32 scale_retimer                         =  0;
    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
    // Set DQ0 base for addressing
    ca0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    
    for(ca = 0; ca < NUM_CA_PCH; ca++) //calculate scale offset bytewise
    {
        scale_offset_in_ps[ca] = ((training_data_ptr->results.ca.coarse_cdc[training_ddr_freq_indx][ch][cs][ca] * COARSE_STEP_IN_PS)
                                      +
                                      (training_data_ptr->results.ca.fine_cdc[training_ddr_freq_indx][ch][cs][ca] * FINE_STEP_IN_PS))
                                      -
                                      ((1000000000 / trained_clk_freq_khz)/4); // determine the offset for scaling based on the last trained values.
    }
    clk_idx = ddr->misc.ddr_num_clock_levels - 1;//max clock frequency index (corresponding to 932 MHz).
    for(scale_prfs_indx = scale_start_prfs_index; scale_prfs_indx >= SCALING_STOP_PRFS; scale_prfs_indx--)//Scale all the scaling bands
    {
        //Determine the scaling frequency in khz
        for(scale_freq_indx = clk_idx; scale_freq_indx >= 0; scale_freq_indx--)
        {
            if (ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz <= freq_range[scale_prfs_indx])
                break;
        }
        ddr_printf(DDR_NORMAL,"\n  Scaling frequency = %u , prfs_level = %d\n",ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz,
                                                                scale_prfs_indx);
        clk_idx = scale_freq_indx;

        for(ca = 0; ca < NUM_CA_PCH; ca++)
        {
#if  DSF_WRLVL_TRAINING_EN
           coarse_wrlvl_delay[ca] = training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][ca];
           fine_wrlvl_delay  [ca] = 0; // (wrlvl training doesn't use fine CDC for CK )training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][ca];
#endif

             scale_center_in_ps  = scale_offset_in_ps[ca]
                                 +
                                 ddrss_lib_uidiv(ddrss_lib_uidiv(1000000000 , ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz),4);
            scale_ca_coarse_cdc = ddrss_lib_uidiv(scale_center_in_ps ,COARSE_STEP_IN_PS);
            scale_ca_fine_cdc   = ddrss_lib_uidiv(ddrss_lib_modulo(scale_center_in_ps, COARSE_STEP_IN_PS), FINE_STEP_IN_PS);
            scale_retimer = DDRSS_CDC_Retimer(ddr, 
                                              cs, 
                                              scale_ca_coarse_cdc,
                                              scale_ca_fine_cdc, 
                                              coarse_wrlvl_delay[ca],
                                              fine_wrlvl_delay[ca],
                                              (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET)), 
                                              ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz); 
            DDR_PHY_hal_cfg_cdcext_slave_wr((ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET)),
                                             cs,
                                             scale_ca_coarse_cdc,
                                             1,//coarse 
                                             HP_MODE,
                                             scale_prfs_indx
                                             ); 
            DDR_PHY_hal_cfg_cdcext_slave_wr((ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET)),
                                             cs,
                                             scale_ca_fine_cdc,
                                             0,//fine
                                             HP_MODE,
                                             scale_prfs_indx  
                                             );
            DDR_PHY_hal_cfg_wrlvlext_ctl_update((ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET)),
                                                 scale_prfs_indx,
                                                 cs,
                                                 scale_retimer,
                                                 0, // half cycle
                                                 0 // full cycle
                                                 );
            ddr_printf(DDR_NORMAL,"    Scaled_frequency = %d   CA%u Data Eye Center Coarse = %d Fine = %d  scale retmr= %d \n",
                      ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz,
                      ca,                                 
                      scale_ca_coarse_cdc,                       
                      scale_ca_fine_cdc,
					  scale_retimer);                        

        }
              
    }
    
}
//End of CA scaling

uint16 get_ca_exp_pattern_lpddr3( uint16 ca_pat_rise, uint16 ca_pat_fall, uint8 ca_train_mapping)
{
        uint16 i;
        uint16 index0;
        uint16 index1;
        uint16 exp_pattern_ph0;
        uint16 exp_pattern_ph1;

        index0 = 0;
        index1 = 0;
        exp_pattern_ph0 = 0;
        exp_pattern_ph1 = 0;
        
 for (i = 0; i <= 9;  i ++)
 {
    if (i != 4 && i != 9 && ca_train_mapping == 0)
    {
      exp_pattern_ph0 = exp_pattern_ph0 | (((ca_pat_rise >> i) & 0x1) << index0);
      exp_pattern_ph0 = exp_pattern_ph0 | (((ca_pat_fall >> i) & 0x1) << (index0 + 1));
      index0 = index0 + 2;
    }
    else
    {
      if(ca_train_mapping == 1 && (i == 4 || i == 9))
      {
        exp_pattern_ph1 = exp_pattern_ph1 | (((ca_pat_rise >> i) & 0x1) << index1);
        exp_pattern_ph1 = exp_pattern_ph1 | (((ca_pat_fall >> i) & 0x1) << (index1 + 1));
        index1 = index1 + 8;
      }
    }
 }
 if(ca_train_mapping == 0)
  {
    return exp_pattern_ph0;
  }
 else
 {
    return exp_pattern_ph1;
 }
}


void DDRSS_writing_ext_CSR_lpddr3 (DDR_STRUCT *ddr,
                                   uint8 ch, 
                                   uint8 cs, 
                                   wrlvl_params_struct *convert_cdc_ptr,  
                                   training_data *training_data_ptr,
                                   uint8 training_type, // 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs training 
                                   uint8 phy_inx,
                                   uint8 freq_band,
                                   uint8 training_freq_inx
                                  )
{
   uint32 ddr_phy_chn_base;

   ddr_phy_chn_base = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);

      // CA Vref training 
      if (training_type == TRAINING_TYPE_CA)  
      {
         DDR_PHY_hal_cfg_cdcext_slave_wr ((ddr_phy_chn_base + CA0_DDR_PHY_OFFSET) + (phy_inx * DDR_PHY_OFFSET), 
                                          cs, 
                                          (*convert_cdc_ptr).coarse_dqs_delay, 
                                          1,//coarse 
                                          HP_MODE, 
                                          freq_band
                                         );
      
         DDR_PHY_hal_cfg_cdcext_slave_wr ((ddr_phy_chn_base + CA0_DDR_PHY_OFFSET) + (phy_inx * DDR_PHY_OFFSET), 
                                          cs, 
                                          (*convert_cdc_ptr).fine_dqs_delay,   
                                          0,//fine   
                                          HP_MODE, 
                                          freq_band
                                         );
      
         DDR_PHY_hal_cfg_wrlvlext_ctl_update ((ddr_phy_chn_base + CA0_DDR_PHY_OFFSET) + (phy_inx * DDR_PHY_OFFSET), 
                                              freq_band, 
                                              cs, 
                                              (*convert_cdc_ptr).dqs_retmr, 
                                              (*convert_cdc_ptr).dqs_half_cycle, 
                                              (*convert_cdc_ptr).dqs_full_cycle
                                             );

         training_data_ptr->results.ca.coarse_cdc     [training_freq_inx][ch][cs][phy_inx] = (*convert_cdc_ptr).coarse_dqs_delay;
         training_data_ptr->results.ca.fine_cdc       [training_freq_inx][ch][cs][phy_inx] = (*convert_cdc_ptr).fine_dqs_delay;
         training_data_ptr->results.ca.dqs_retmr      [training_freq_inx][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_retmr;
         training_data_ptr->results.ca.dqs_half_cycle [training_freq_inx][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_half_cycle;
         training_data_ptr->results.ca.dqs_full_cycle [training_freq_inx][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_full_cycle;
      }
      
}

void DDRSS_ca_vref_lpddr3 (DDR_STRUCT              *ddr,
                                   ddrss_ca_vref_local_vars *local_vars,
                                   uint8                     ch, 
                                   uint8                     cs, 
                                   training_params_t        *training_params_ptr,
                                   uint32                    clk_freq_khz,
                                   uint8                     training_ddr_freq_indx,
                                   uint8                     prfs_indx
                                 )
{
// ============================================================================================================================
// ======================================      D E F I N I T I O N S                          =================================
// ============================================================================================================================
    uint8                   loop_cnt = 0;
    uint8                        bit = 0;
    uint8                 ca_mapping = 0;
    uint8              pattern_index = 0;
    uint32                 cdc_value = 0;
    uint16            expect_pattern = 0;
    uint16                      nobe = 0;
    uint32 reg_offset_ddr_ca[NUM_CA_PCH]  = {0}; 
    uint32 reg_offset_ddr_dq[NUM_DQ_PCH]  = {0};
    uint32       reg_offset_global0   =0;
	uint32            safe_cdc_value  = 0;
    uint32             safe_coarse    = 0;
    uint32             safe_fine      = 0;
    uint16     feedback_16bit_lpddr3_ms = 0;
    uint16     feedback_16bit_lpddr3_ls = 0;
    uint32                        width = 0;
    uint16                   ca_failure = 0;
	uint8      phy_x;
	uint8      remapped_line=0;
	uint8      remapped_bit=0;
	
    uint32     pr_min_strt[NUM_CA_PCH]  = {110,110};
    uint32     pr_min_midp[NUM_CA_PCH]  = {110,110};
    uint32     pr_min_stop[NUM_CA_PCH]  = {110,110};
    uint32     pr_min_eyew[NUM_CA_PCH]  = {110,110};
    uint32     pr_max_strt[NUM_CA_PCH]  = {0,0};
    uint32     pr_max_midp[NUM_CA_PCH]  = {0,0};
    uint32     pr_max_stop[NUM_CA_PCH]  = {0,0};
    uint32     pr_max_eyew[NUM_CA_PCH]  = {0,0};
	uint32      retimer;
#if PHY_LOG_PRINT_ENABLE	
   int      rf_x                     = 0    ;
    uint32     pr_bus_eyew[NUM_CA_PCH]  = {0,0};
    uint32     pr_bus_skwr[NUM_CA_PCH]  = {0,0};
    uint32     pr_x  =0;
   uint32   safe_delay               = 0    ;
    char     pr_string[4][25]  ={
		                           " passing region start",
		                           " passing region stop",
		                           " passing region midp",
		                           " passing region eyew"
	                                };
 #endif                
  extern  uint8  CA01_bit_remapping_bimc2phy_CA [NUM_CH][NUM_CA_PHY_BIT] ;
  extern  uint8  CA01_line_remapping_bimc2phy_CA[NUM_CH][NUM_CA_PHY_BIT] ;	
  extern  uint16 ca_training_pattern_lpddr3[CA_PATTERN_NUM][6] ;
  extern  uint8  ca_phy_connected_bit_mapping[NUM_CH][NUM_CA_PCH][PINS_PER_PHY_CONNECTED_CA] ;
 
  //    uint8   failure_cnt[3][PINS_PER_PHY_CONNECTED_CA] = {0};
  uint16  feedback_data[NUM_DQ_PCH] = {0};
  uint32  coarse_x       = 0 ;
  uint32  fine_x         = 0 ;
  uint32  delay_ps       = 0 ;
  uint32  csr_val        = 0 ;
  uint32  safe_rtm_value = 0 ;
  uint32  cdc_ref[HISTOGRAM_SIZE][4]={0};
  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);


	
// ============================================================================================================================
// ======================================      I N I T I A L I Z A T I O N                    =================================
// ============================================================================================================================
    // Set DDR Base address registers
    reg_offset_ddr_ca[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_ca[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[2]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[3]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;
    reg_offset_global0         = ddr->base_addr.bimc_base_addr  + REG_OFFSET_GLOBAL0;

    // Disable DPE and SHKE so that in freq switch, MR write will not happen
	HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xE);
    HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x6);

   if(training_ddr_freq_indx == (TRAINING_NUM_FREQ_LPDDR3 - 1))  
   {
	   for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
	      {
	        for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
               {
                  DDR_PHY_hal_cfg_pbit_dq_delay(reg_offset_ddr_ca[phy_x],
                                              bit, 
                                              1, // 1 for TX 
                                              cs, 
                                              0);
               }//bit 
	      }//phy_x
   }	
    // Get the safe CDC value from the Shadow (ext) register
  safe_cdc_value = HWIO_INX ( reg_offset_ddr_ca[0] , DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ) ;
  safe_rtm_value = HWIO_INX ( reg_offset_ddr_ca[0] , DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ) ;
    // Parse the CA CDC for the safe mode
  safe_coarse =  safe_cdc_value     & 0x1F;
  safe_fine   = (safe_cdc_value>>5) & 0x1F;
  safe_coarse = safe_coarse;
  safe_fine   = safe_fine  ;
 
//initializing to Zero
    for(cdc_value=0;cdc_value<HISTOGRAM_SIZE;cdc_value++)
	{
         for(bit=0;bit<PINS_PER_PHY_CONNECTED_CA;bit++)
		 {
           local_vars->failcount_histogram [2][0][bit][cdc_value]  =0;          
           local_vars->failcount_histogram [1][0][bit][cdc_value]  =0;          
           local_vars->failcount_histogram [0][0][bit][cdc_value]  =0;          
         }//bit
    }   //cdc_value

    // Put the DQ PHYs into CA training mode
  	for (phy_x = 0; phy_x < NUM_DQ_PCH; phy_x++)
	{
        DDR_PHY_hal_cfg_dq_ca_training_entry( reg_offset_ddr_dq[phy_x]);
//	        HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, LOAD, 1);
//          HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, EN  , 1);
//          HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, GATE, 0);
	}
// ======================================      cdc ref                   =================================
for (coarse_x=0; coarse_x< COARSE_CDC_MAX_VALUE; coarse_x++)
 {
    for (fine_x=0; fine_x< FINE_STEPS_PER_COARSE; fine_x++)
    { nobe                   = coarse_x * FINE_STEPS_PER_COARSE + fine_x ;
      delay_ps                = coarse_x * COARSE_STEP_IN_PS     + fine_x * FINE_STEP_IN_PS;
	  csr_val                 = 0;
	  csr_val                |= coarse_x      ;
	  csr_val                |= fine_x   <<5    ;
      cdc_ref[nobe][COARSE_L] = coarse_x  ;
      cdc_ref[nobe][FINE_L  ] = fine_x    ;
      cdc_ref[nobe][DELAY_L ] = delay_ps  ;
      cdc_ref[nobe][CDC_L   ] = csr_val ;
    }
 }
// ============================================================================================================================
// ======================================     C A       C D C           S W E E P I N G       =================================
// ======================================     TO   C A P T U R E      H I S T O G R A M       =================================
// ============================================================================================================================
    // Train CA bits in two passes: (mapping 0) bits[0-3 & 5-8] (mapping 1) bits[4 & 9]
    for (ca_mapping = 0; ca_mapping <= 1; ca_mapping ++)
    {        
       //  =================================safe  nobe before  MRW
  	   for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
	   {
		HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,safe_cdc_value);
		HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ,safe_rtm_value);
	   }
       //  =================================safe nobe
      // Enter DRAM CA training mode
      BIMC_CA_Training_Entry_Lpddr3(ddr,ch, cs, ca_mapping);
      // Sweep the Fine CDC to paint the edge of the Eye
	for (nobe = 0;  nobe < HISTOGRAM_SIZE;  nobe++) 
    {
      //  =================================Set nobe start
	    for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
        {
  	       //HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,cdc_ref[nobe][CDC_L]);
	       //ddr_printf (DDR_NORMAL, "%6X,%6X,\n",reg_offset_ddr_ca[phy_x],cdc_ref[nobe][CDC_L]);
           DDR_PHY_hal_cfg_cdc_slave_wr_cdc( reg_offset_ddr_ca[phy_x],cdc_ref[nobe][CDC_L] );
                 DDRSS_CDC_Retimer (ddr,
                         cs,
                         cdc_ref[nobe][COARSE_L ],        // Coarse CDC delay value 
                         cdc_ref[nobe][FINE_L   ],          // Safe Fine CDC
                         training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][phy_x],
                         0, // (wrlvl training doesn't use fine CDC for CK )
                         reg_offset_ddr_ca[phy_x], // Base address of CA0 
                         clk_freq_khz
                        );      
  	     }//phy_x
         //  =================================Set nobe end
           // Loop through the training patterns
          for (pattern_index = 0; pattern_index < CA_PATTERN_NUM; pattern_index++) 
          { 
              BIMC_Set_CA_Training_Pattern(
                                    ddr,
                                    ch,
                                    ca_training_pattern_lpddr3[pattern_index][PRECS_RISE], 
                                    ca_training_pattern_lpddr3[pattern_index][PRECS_FALL], 
                                    ca_training_pattern_lpddr3[pattern_index][CS_RISE], 
                                    ca_training_pattern_lpddr3[pattern_index][CS_FALL], 
                                    ca_training_pattern_lpddr3[pattern_index][POSTCS_RISE], 
                                    ca_training_pattern_lpddr3[pattern_index][POSTCS_FALL]);
             // Expect pattern is after the JEDEC mapping
             expect_pattern = get_ca_exp_pattern_lpddr3(ca_training_pattern_lpddr3[pattern_index][CS_RISE],  
                                                        ca_training_pattern_lpddr3[pattern_index][CS_FALL],
                                                       ca_mapping);
             // Loop through each CA training pattern for max_loopcnt
             for (loop_cnt = training_params_ptr->ca.max_loopcnt; loop_cnt > 0; loop_cnt --) 
             {
                 // The MC sends the CA patterns
                 BIMC_Send_CA_Pattern (ddr, ch, cs);
                 // Retrieve the DQ0 data from the PHY BISC capture register
                 feedback_data[0]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[0]); 
                 feedback_data[1]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[1]); 
                 feedback_data[0]         = DDRSS_dq_remapping_lpddr3 (feedback_data[0], bit_remapping_phy2bimc_DQ[ch][0]);
                 feedback_data[1]         = DDRSS_dq_remapping_lpddr3 (feedback_data[1], bit_remapping_phy2bimc_DQ[ch][1]); 
                 feedback_16bit_lpddr3_ls = feedback_data[1];
                 feedback_16bit_lpddr3_ls = feedback_16bit_lpddr3_ls <<8;
                 feedback_16bit_lpddr3_ls = feedback_16bit_lpddr3_ls | feedback_data[0];
                           
                 width = cs ? ddr->cdt_params[ch].common.interface_width_cs1 : ddr->cdt_params[ch].common.interface_width_cs0;
                 if (width ==16)
                 {
                   feedback_data[2]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[2]); 
                   feedback_data[3]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[3]); 
                   feedback_data[2]         = DDRSS_dq_remapping_lpddr3 (feedback_data[2], bit_remapping_phy2bimc_DQ[ch][2]); 
                   feedback_data[3]         = DDRSS_dq_remapping_lpddr3 (feedback_data[3], bit_remapping_phy2bimc_DQ[ch][3]); 
                   feedback_16bit_lpddr3_ms = feedback_data[1];
                   feedback_16bit_lpddr3_ms = feedback_16bit_lpddr3_ms <<8;
                   feedback_16bit_lpddr3_ms = feedback_16bit_lpddr3_ms | feedback_data[0];
                 }
                 else
                 {
                         feedback_16bit_lpddr3_ms=feedback_16bit_lpddr3_ls;
                 } 
                 // Mask the  6 bits of the byte for 2 bit training               
                 if(ca_mapping == 1) 
                 {
                   feedback_16bit_lpddr3_ls &= 0x0303;
                   feedback_16bit_lpddr3_ms &= 0x0303;
                   expect_pattern           &= 0x0303;
                 }
                ca_failure = (feedback_16bit_lpddr3_ls ^ expect_pattern) |(feedback_16bit_lpddr3_ms ^ expect_pattern) ;
                if (feedback_16bit_lpddr3_ls!= feedback_16bit_lpddr3_ms)  
				{
	     	     ddr_printf (DDR_NORMAL,"======================================================================\n");
                 ddr_printf (DDR_NORMAL, "Chnl=, %X ,PtrnR=,%X,PtrnF=,%X, Expt=,%X,   \n Fbck_ms=,%X , Fbck_ls=,%X , Fail=,%X ,\n",
                          ch,
                          ca_training_pattern_lpddr3[pattern_index][CS_RISE], 
                          ca_training_pattern_lpddr3[pattern_index][CS_FALL], 
                          expect_pattern,
                          feedback_16bit_lpddr3_ms,
                          feedback_16bit_lpddr3_ls,
                          ca_failure);
	     	    }
	     	    DDRSS_ca_training_bit_histogram_update_rise_fall( ca_mapping, ca_failure,  local_vars->failcount_histogram, nobe );
           }//loop count
         }//pattern
    }  //End of nobe loop
    //  safe  nobe
 	for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
	{  HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,safe_cdc_value);
	   HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ,safe_rtm_value);
	}
    // Exit DRAM CA training mode
    BIMC_CA_Training_Exit_Lpddr3 (ddr, ch, cs);
  } //ca_mapping   
// ============================================================================================================================
// ======================================      Exit PHY CA training mode                      =================================
// ============================================================================================================================
  // Exit PHY CA training mode
  for (phy_x = 0; phy_x < NUM_DQ_PCH; phy_x++)
  {
    DDR_PHY_hal_cfg_dq_ca_training_exit(  reg_offset_ddr_dq[phy_x]);
  }
  // Enable DPE and SHKE so that in freq switch, MR write will not happen
  HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xF);
  HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x7);
		
 //for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
 //	{
 //        HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, LOAD, 0);
 //      //HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, EN  , 0);
 //      //HWIO_OUTXF (reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, GATE, 1);
 //	}
    
// ============================================================================================================================
// ======================================      calculation                   =================================
// ============================================================================================================================
        for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
        {
		  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
		  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
          DDRSS_passing_region_scan( &local_vars->pass_reg[remapped_line][remapped_bit][STRT], 
		                             &local_vars->pass_reg[remapped_line][remapped_bit][STOP], 
									  local_vars->failcount_histogram [2][0][bit]);
          local_vars->pass_reg[remapped_line][remapped_bit][MIDP]  = (local_vars->pass_reg[remapped_line][remapped_bit][STOP]   +  local_vars->pass_reg[remapped_line][remapped_bit][STRT]) >>1 ; // midpoin equal to average 
          local_vars->pass_reg[remapped_line][remapped_bit][EYEW]  = (local_vars->pass_reg[remapped_line][remapped_bit][STOP]   -  local_vars->pass_reg[remapped_line][remapped_bit][STRT] )  ; // eywidth  equal to stop - start 
          if ( pr_min_strt[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][STRT] ) { pr_min_strt[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STRT] ; }
          if ( pr_min_stop[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][STOP] ) { pr_min_stop[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STOP] ; }
          if ( pr_min_midp[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ) { pr_min_midp[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ; }
          if ( pr_min_eyew[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ) { pr_min_eyew[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ; }
          if ( pr_max_strt[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][STRT] ) { pr_max_strt[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STRT] ; }
          if ( pr_max_stop[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][STOP] ) { pr_max_stop[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STOP] ; }
          if ( pr_max_midp[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ) { pr_max_midp[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ; }
          if ( pr_max_eyew[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ) { pr_max_eyew[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ; }
		  }			
 // ============================================================================================================================
 // ==========================================Setting coarse and fine values =====================================================
 // ============================================================================================================================
      for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
     {
          training_data_ptr->results.ca.coarse_cdc [training_ddr_freq_indx][ch][cs][phy_x] =  cdc_ref[pr_min_midp[phy_x]][COARSE_L]	;
          training_data_ptr->results.ca.fine_cdc   [training_ddr_freq_indx][ch][cs][phy_x] =  cdc_ref[pr_min_midp[phy_x]][FINE_L  ]	;
		  retimer = DDRSS_CDC_Retimer(ddr, 
                                      cs, 
                                      training_data_ptr->results.ca.coarse_cdc            [training_ddr_freq_indx][ch][cs][phy_x],
                                      training_data_ptr->results.ca.fine_cdc              [training_ddr_freq_indx][ch][cs][phy_x], 
                                      training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[training_ddr_freq_indx][ch][cs][phy_x],
                                      0,// (wrlvl training doesn't use fine CDC for CK )training_data_ptr->results.wrlvl.dq_fine_dqs_delay[training_ddr_freq_indx][ch][cs][ca];
                                      reg_offset_ddr_ca[phy_x], 
                                      clk_freq_khz); 
		 training_data_ptr->results.ca.dqs_retmr [training_ddr_freq_indx][ch][cs][phy_x]=retimer;
         // ============================================================================================================================
          DDR_PHY_hal_cfg_cdc_slave_wr_cdc( reg_offset_ddr_ca[phy_x],cdc_ref[pr_min_midp[phy_x]][CDC_L] );
         // ============================================================================================================================
          DDR_PHY_hal_cfg_cdcext_slave_wr(reg_offset_ddr_ca[phy_x] ,
                                          cs, 
                                          training_data_ptr->results.ca.coarse_cdc [training_ddr_freq_indx][ch][cs][phy_x],
                                          1/*coarse*/, 
                                          HP_MODE, 
                                          prfs_indx
                                          );
          DDR_PHY_hal_cfg_cdcext_slave_wr(reg_offset_ddr_ca[phy_x],
                                          cs, 
                                          training_data_ptr->results.ca.fine_cdc [training_ddr_freq_indx][ch][cs][phy_x],
                                          0/*fine*/,   
                                          HP_MODE, 
                                          prfs_indx
                                          );
	     DDR_PHY_hal_cfg_wrlvlext_ctl_update(reg_offset_ddr_ca[phy_x],
                                                 prfs_indx,
                                                 cs,
                                                 retimer,
                                                 0, // half cycle
                                                 0 // full cycle
                                                 );
     }
    // setting perbit cdc only for maximum frequency
    if(training_ddr_freq_indx == (TRAINING_NUM_FREQ_LPDDR3 - 1))  
    {      
	for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
       {
           remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
           remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]=
										 local_vars->pass_reg[remapped_line][remapped_bit][MIDP]-  pr_min_midp[remapped_line];
										 
           DDR_PHY_hal_cfg_pbit_dq_delay(reg_offset_ddr_ca[remapped_line], 
                                         remapped_bit, 
                                         1, // 1 for Tx
                                         cs, 
                                         training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]); 
       }
	}	   
// ============================================================================================================================
// ======================================              scaling                     =================================
// ============================================================================================================================
 if (training_ddr_freq_indx == 0)
 { 
    Scale_ca_dqdqs_lpddr3 (  ddr,ch, cs, training_ddr_freq_indx,clk_freq_khz,prfs_indx - 1);
 }

 #if PHY_LOG_PRINT_ENABLE	
// ============================================================================================================================
// ======================================              L O G  P R I N T                       =================================
// ============================================================================================================================
   safe_delay  =  safe_coarse * COARSE_STEP_IN_PS     + safe_fine * FINE_STEP_IN_PS;
         pr_bus_eyew[0]  =cdc_ref[pr_min_stop[0]][DELAY_L ]-cdc_ref[pr_max_strt[0]][DELAY_L ];
         pr_bus_eyew[1]  =cdc_ref[pr_min_stop[1]][DELAY_L ]-cdc_ref[pr_max_strt[1]][DELAY_L ];
         pr_bus_skwr[0]  =cdc_ref[pr_max_midp[0]][DELAY_L ]-cdc_ref[pr_min_midp[0]][DELAY_L ];
         pr_bus_skwr[1]  =cdc_ref[pr_max_midp[1]][DELAY_L ]-cdc_ref[pr_min_midp[1]][DELAY_L ];
		 
		 
		 
   for (nobe=0; nobe < HISTOGRAM_SIZE-1; nobe++)
   {
      for (rf_x = 2; rf_x >= 0; rf_x--)
      {   
        for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
        {
  	 	    remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	    remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
            // ========================================================================================
           if(nobe==local_vars->pass_reg[remapped_line][remapped_bit][MIDP] )                         
            {                                                                                      
             ddr_printf (DDR_NORMAL, "  X ,");                                                    
            }                                                                                      
            else                                                                                   
            {                                                                                      
             ddr_printf (DDR_NORMAL, "%4d,",local_vars->failcount_histogram[rf_x][0][bit][nobe]);
            }                                                                                      
           // ========================================================================================
        }	
        ddr_printf (DDR_NORMAL, "|X|,");
      }
       ddr_printf(DDR_NORMAL,"%6d,%6d,%6d,%6d,%6X,",nobe,cdc_ref[nobe][COARSE_L],cdc_ref[nobe][FINE_L],cdc_ref[nobe][DELAY_L],cdc_ref[nobe][CDC_L]);
       // ============================================================================================
       if ( (safe_delay >= cdc_ref[nobe][DELAY_L ] ) && (safe_delay <cdc_ref[nobe+1][DELAY_L]) )
     	{	
          ddr_printf (DDR_NORMAL, "<<<<<<< ,delay=,%d, coarse=,%d, fine=,%d, ",safe_delay,safe_coarse,safe_fine);	 
	    }
       // ============================================================================================
       if (nobe == pr_min_strt[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_strt 0  %d, ",nobe);	    }
       if (nobe == pr_min_stop[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_stop 0  %d, ",nobe);	    }
       if (nobe == pr_min_midp[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_midp 0  %d, ",nobe);	    }
       if (nobe == pr_max_strt[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_strt 0  %d, ",nobe);	    }
       if (nobe == pr_max_stop[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_stop 0  %d, ",nobe);	    }
       if (nobe == pr_max_midp[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_midp 0  %d, ",nobe);	    }
       if (nobe == pr_min_strt[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_strt 1  %d, ",nobe);	    }
       if (nobe == pr_min_stop[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_stop 1  %d, ",nobe);	    }
       if (nobe == pr_min_midp[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_midp 1  %d, ",nobe);	    }
       if (nobe == pr_max_strt[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_strt 1  %d, ",nobe);	    }
       if (nobe == pr_max_stop[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_stop 1  %d, ",nobe);	    }
       if (nobe == pr_max_midp[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_midp 1  %d, ",nobe);	    }
       // ============================================================================================
       ddr_printf (DDR_NORMAL, "\n");
   }
   ddr_printf(DDR_NORMAL, "\n");
// ============================================================================================================================
for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
   {
		  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
          ddr_printf (DDR_NORMAL, "%4d,",remapped_line);
   }	
   ddr_printf (DDR_NORMAL, "remapped_line\n");
// ============================================================================================================================
for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
   {
		  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
         ddr_printf (DDR_NORMAL, "%4d,",remapped_bit);
   }				   
   ddr_printf (DDR_NORMAL, "remapped_bit\n");
// ============================================================================================================================
  for (pr_x = 0; pr_x < 4; pr_x++)
  {
   for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
    {
	 	  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           ddr_printf (DDR_NORMAL, "%4d,",local_vars->pass_reg[remapped_line][remapped_bit][pr_x]);
    }				   
    ddr_printf (DDR_NORMAL, "%s\n",pr_string[pr_x]);
  }
// ============================================================================================================================
   for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
    {
	 	  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           ddr_printf (DDR_NORMAL, "%4d,",training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]);
    }				   
// ============================================================================================================================
    ddr_printf (DDR_NORMAL, "%s\n","perbit Delay");
    ddr_printf (DDR_NORMAL, "pr_bus_eyew[0]=, %d ,\n",pr_bus_eyew[0]);
    ddr_printf (DDR_NORMAL, "pr_bus_eyew[1]=, %d ,\n",pr_bus_eyew[1]);
    ddr_printf (DDR_NORMAL, "pr_bus_skwr[0]=, %d ,\n",pr_bus_skwr[0]);
    ddr_printf (DDR_NORMAL, "pr_bus_skwr[1]=, %d ,\n",pr_bus_skwr[1]);
	
// ============================================================================================================================
// ======================================      I N I T I A L I Z A T I O N                    =================================
// ============================================================================================================================
	
	pr_min_strt[0]  = 333;  pr_min_strt[1]  = 333;
	pr_min_midp[0]  = 333;  pr_min_midp[1]  = 333;
	pr_min_stop[0]  = 333;  pr_min_stop[1]  = 333;
	pr_min_eyew[0]  = 333;  pr_min_eyew[1]  = 333;
	pr_max_strt[0]  = 0;    pr_max_strt[1]  = 0;
	pr_max_midp[0]  = 0;    pr_max_midp[1]  = 0;
	pr_max_stop[0]  = 0;    pr_max_stop[1]  = 0;
	pr_max_eyew[0]  = 0;    pr_max_eyew[1]  = 0;
	pr_bus_eyew[0]  = 0;    pr_bus_eyew[1]  = 0;
	pr_bus_skwr[0]  = 0;    pr_bus_skwr[1]  = 0;
	
    // Set DDR Base address registers
    reg_offset_ddr_ca[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_ca[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[0]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[1]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[2]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET;
    reg_offset_ddr_dq[3]       = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;
    reg_offset_global0         = ddr->base_addr.bimc_base_addr  + REG_OFFSET_GLOBAL0;

    // Disable DPE and SHKE so that in freq switch, MR write will not happen
	HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xE);
    HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x6);


    // Get the safe CDC value from the Shadow (ext) register
  safe_cdc_value = HWIO_INX ( reg_offset_ddr_ca[0] , DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ) ;
  safe_rtm_value = HWIO_INX ( reg_offset_ddr_ca[0] , DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ) ;
    // Parse the CA CDC for the safe mode
  safe_coarse =  safe_cdc_value     & 0x1F;
  safe_fine   = (safe_cdc_value>>5) & 0x1F;
  safe_delay  =  safe_coarse * COARSE_STEP_IN_PS     + safe_fine * FINE_STEP_IN_PS;
 
//initializing to Zero
    for(cdc_value=0;cdc_value<HISTOGRAM_SIZE;cdc_value++)
	{
         for(bit=0;bit<PINS_PER_PHY_CONNECTED_CA;bit++)
		 {
           local_vars->failcount_histogram [2][0][bit][cdc_value]  =0;          
           local_vars->failcount_histogram [1][0][bit][cdc_value]  =0;          
           local_vars->failcount_histogram [0][0][bit][cdc_value]  =0;          
         }//bit
    }   //cdc_value
    // Put the DQ PHYs into CA training mode
  	for (phy_x = 0; phy_x < NUM_DQ_PCH; phy_x++)
	{
 	DDR_PHY_hal_cfg_dq_ca_training_entry(  reg_offset_ddr_dq[phy_x]);		 
	}
// ======================================      cdc ref                   =================================
for (coarse_x=0; coarse_x< COARSE_CDC_MAX_VALUE; coarse_x++)
 {
    for (fine_x=0; fine_x< FINE_STEPS_PER_COARSE; fine_x++)
    { nobe                   = coarse_x * FINE_STEPS_PER_COARSE + fine_x ;
      delay_ps                = coarse_x * COARSE_STEP_IN_PS     + fine_x * FINE_STEP_IN_PS;
	  csr_val                 = 0;
	  csr_val                |= coarse_x      ;
	  csr_val                |= fine_x   <<5    ;
//	  csr_val                |= coarse_x <<10 ;
//	  csr_val                |= fine_x   <<15   ;
      cdc_ref[nobe][COARSE_L] = coarse_x  ;
      cdc_ref[nobe][FINE_L  ] = fine_x    ;
      cdc_ref[nobe][DELAY_L ] = delay_ps  ;
      cdc_ref[nobe][CDC_L   ] = csr_val ;
    }
 }
// ============================================================================================================================
// ======================================      S W E E P I N G                    =================================
// ============================================================================================================================
    // Train CA bits in two passes: (mapping 0) bits[0-3 & 5-8] (mapping 1) bits[4 & 9]
    for (ca_mapping = 0; ca_mapping <= 1; ca_mapping ++)
    {        
     //  safe  nobe
  	   for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
	   {
		HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,safe_cdc_value);
		HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ,safe_rtm_value);
	   }
      // Enter DRAM CA training mode
      BIMC_CA_Training_Entry_Lpddr3(ddr,ch, cs, ca_mapping);
      // Sweep the Fine CDC to paint the edge of the Eye
	for (nobe = 0;  nobe < HISTOGRAM_SIZE;  nobe++) 
    {
      //  =================================Set nobe start
	    for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
        {
  	       //HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,cdc_ref[nobe][CDC_L]);
	       //ddr_printf (DDR_NORMAL, "%6X,%6X,\n",reg_offset_ddr_ca[phy_x],cdc_ref[nobe][CDC_L]);
           DDR_PHY_hal_cfg_cdc_slave_wr_cdc( reg_offset_ddr_ca[phy_x],cdc_ref[nobe][CDC_L] );
           //DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_ca[phy_x], cdc_ref[nobe][COARSE_L ],1,1,cs );
          // DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_ca[phy_x], cdc_ref[nobe][FINE_L   ],0,1,cs  );
                 DDRSS_CDC_Retimer (ddr,
                         cs,
                         cdc_ref[nobe][COARSE_L ],        // Coarse CDC delay value 
                         cdc_ref[nobe][FINE_L   ],          // Safe Fine CDC
                         0, // wrlvl coarse
                         0, // wrlvl fine
                         reg_offset_ddr_ca[phy_x], // Base address of CA0 
                         clk_freq_khz
                        );      
  	     }//phy_x
                                // Loop through the training patterns
         for (pattern_index = 0; pattern_index < CA_PATTERN_NUM; pattern_index++) 
             { 
                 BIMC_Set_CA_Training_Pattern(
                                       ddr,
                                       ch,
                                       ca_training_pattern_lpddr3[pattern_index][PRECS_RISE], 
                                       ca_training_pattern_lpddr3[pattern_index][PRECS_FALL], 
                                       ca_training_pattern_lpddr3[pattern_index][CS_RISE], 
                                       ca_training_pattern_lpddr3[pattern_index][CS_FALL], 
                                       ca_training_pattern_lpddr3[pattern_index][POSTCS_RISE], 
                                       ca_training_pattern_lpddr3[pattern_index][POSTCS_FALL]);
                // Expect pattern is after the JEDEC mapping
                expect_pattern = get_ca_exp_pattern_lpddr3(ca_training_pattern_lpddr3[pattern_index][CS_RISE],  
                                                           ca_training_pattern_lpddr3[pattern_index][CS_FALL],
                                                          ca_mapping);
                // Loop through each CA training pattern for max_loopcnt
                for (loop_cnt = training_params_ptr->ca.max_loopcnt; loop_cnt > 0; loop_cnt --) 
                {
                  // The MC sends the CA patterns
                  BIMC_Send_CA_Pattern (ddr, ch, cs);
                    // Retrieve the DQ0 data from the PHY BISC capture register
                    feedback_data[0]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[0]); 
                    feedback_data[1]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[1]); 
                    feedback_data[0]         = DDRSS_dq_remapping_lpddr3 (feedback_data[0], bit_remapping_phy2bimc_DQ[ch][0]);
                    feedback_data[1]         = DDRSS_dq_remapping_lpddr3 (feedback_data[1], bit_remapping_phy2bimc_DQ[ch][1]); 
                    feedback_16bit_lpddr3_ls = feedback_data[1];
                    feedback_16bit_lpddr3_ls = feedback_16bit_lpddr3_ls <<8;
                    feedback_16bit_lpddr3_ls = feedback_16bit_lpddr3_ls | feedback_data[0];
                    width = cs ? ddr->cdt_params[ch].common.interface_width_cs1 : ddr->cdt_params[ch].common.interface_width_cs0;
                    if (width ==16)
                    {
                      feedback_data[2]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[2]); 
                      feedback_data[3]         = DDR_PHY_hal_sta_ca (reg_offset_ddr_dq[3]); 
                      feedback_data[2]         = DDRSS_dq_remapping_lpddr3 (feedback_data[2], bit_remapping_phy2bimc_DQ[ch][2]); 
                      feedback_data[3]         = DDRSS_dq_remapping_lpddr3 (feedback_data[3], bit_remapping_phy2bimc_DQ[ch][3]); 
                      feedback_16bit_lpddr3_ms = feedback_data[1];
                      feedback_16bit_lpddr3_ms = feedback_16bit_lpddr3_ms <<8;
                      feedback_16bit_lpddr3_ms = feedback_16bit_lpddr3_ms | feedback_data[0];
                    }
                    else
                      {
                              feedback_16bit_lpddr3_ms=feedback_16bit_lpddr3_ls;
                      } 
                    // Mask the upper 6 bits of the byte for 2 bit training               
                    if(ca_mapping == 1) 
                    {
                      feedback_16bit_lpddr3_ls &= 0x0303;
                      feedback_16bit_lpddr3_ms &= 0x0303;//bypassing BIMC bug
                      expect_pattern           &= 0x0303;
                    }
                   ca_failure = (feedback_16bit_lpddr3_ls ^ expect_pattern) |(feedback_16bit_lpddr3_ms ^ expect_pattern) ;
                   if (feedback_16bit_lpddr3_ls!= feedback_16bit_lpddr3_ms)     {
            	  ddr_printf (DDR_NORMAL,"====================================================================================================================================\n");
                   ddr_printf (DDR_NORMAL, "Chnl=, %X ,PtrnR=,%X,PtrnF=,%X, Expt=,%X,   \n Fbck_ms=,%X , Fbck_ls=,%X , Fail=,%X ,\n",
                             ch,
                             ca_training_pattern_lpddr3[pattern_index][CS_RISE], 
                             ca_training_pattern_lpddr3[pattern_index][CS_FALL], 
                             expect_pattern,
                             feedback_16bit_lpddr3_ms,
                             feedback_16bit_lpddr3_ls,
                             ca_failure);
            	  }
            	 DDRSS_ca_training_bit_histogram_update_rise_fall( ca_mapping, ca_failure,  local_vars->failcount_histogram, nobe );
                //}// if ca_cdc_flag
              }//loop count
            }//pattern
    }  //End of nobe loop
//  =================================safe  nobe
 	   for (phy_x = 0; phy_x < NUM_CA_PCH; phy_x++)
	   {  HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG ,safe_cdc_value);
		  HWIO_OUTX ( reg_offset_ddr_ca[phy_x], DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG ,safe_rtm_value);
	   }
//  ================================= safe  nobe
    // Exit DRAM CA training mode
  BIMC_CA_Training_Exit_Lpddr3 (ddr, ch, cs);
  HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE1_HANDSHAKE_CTRL, ch, 0xF);
  HWIO_OUTXI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_HFSC_STAGE2_HANDSHAKE_CTRL, ch, 0x7);
  }//ca_mapping   
// ============================================================================================================================
// ======================================      Exit PHY CA training mode                   =================================
// ============================================================================================================================
   // Exit PHY CA training mode
  	for (phy_x = 0; phy_x < NUM_DQ_PCH; phy_x++)
	{
 	  DDR_PHY_hal_cfg_dq_ca_training_exit(  reg_offset_ddr_dq[phy_x]);
	}
// ============================================================================================================================
// ======================================      calculation                   =================================
// ============================================================================================================================
        for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
        {
		  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
		  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
          DDRSS_passing_region_scan( &local_vars->pass_reg[remapped_line][remapped_bit][STRT], 
		                             &local_vars->pass_reg[remapped_line][remapped_bit][STOP], 
									  local_vars->failcount_histogram [2][0][bit]);
          local_vars->pass_reg[remapped_line][remapped_bit][MIDP]  = (local_vars->pass_reg[remapped_line][remapped_bit][STOP]   +  local_vars->pass_reg[remapped_line][remapped_bit][STRT]) >>1 ; // midpoin equal to average 
          local_vars->pass_reg[remapped_line][remapped_bit][EYEW]  = (local_vars->pass_reg[remapped_line][remapped_bit][STOP]   -  local_vars->pass_reg[remapped_line][remapped_bit][STRT] )  ; // eywidth  equal to stop - start 
          if ( pr_min_strt[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][STRT] ) { pr_min_strt[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STRT] ; }
          if ( pr_min_stop[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][STOP] ) { pr_min_stop[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STOP] ; }
          if ( pr_min_midp[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ) { pr_min_midp[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ; }
          if ( pr_min_eyew[remapped_line] > local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ) { pr_min_eyew[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ; }
          if ( pr_max_strt[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][STRT] ) { pr_max_strt[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STRT] ; }
          if ( pr_max_stop[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][STOP] ) { pr_max_stop[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][STOP] ; }
          if ( pr_max_midp[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ) { pr_max_midp[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][MIDP] ; }
          if ( pr_max_eyew[remapped_line] < local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ) { pr_max_eyew[remapped_line]  = local_vars->pass_reg[remapped_line][remapped_bit][EYEW] ; }
		  }			
         pr_bus_eyew[0]  =cdc_ref[pr_min_stop[0]][DELAY_L ]-cdc_ref[pr_max_strt[0]][DELAY_L ];
         pr_bus_eyew[1]  =cdc_ref[pr_min_stop[1]][DELAY_L ]-cdc_ref[pr_max_strt[1]][DELAY_L ];
         pr_bus_skwr[0]  =cdc_ref[pr_max_midp[0]][DELAY_L ]-cdc_ref[pr_min_midp[0]][DELAY_L ];
         pr_bus_skwr[1]  =cdc_ref[pr_max_midp[1]][DELAY_L ]-cdc_ref[pr_min_midp[1]][DELAY_L ];
// ============================================================================================================================
	//for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
    //   {
    //       remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
    //       remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
    //       training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]=
	//									 local_vars->pass_reg[remapped_line][remapped_bit][MIDP]-pr_min_midp[remapped_line];
    //   }
// ============================================================================================================================
// ======================================              L O G  P R I N T                       =================================
// ============================================================================================================================
   for (nobe=0; nobe < HISTOGRAM_SIZE-1; nobe++)
   {
      for (rf_x = 2; rf_x >= 0; rf_x--)
      {   
        for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
        {
  	 	    remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	    remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           // ========================================================================================
           if(nobe==local_vars->pass_reg[remapped_line][remapped_bit][MIDP] )                         
			   {                                                                                      
			     ddr_printf (DDR_NORMAL, "  X ,");                                                    
		       }                                                                                      
			   else                                                                                   
			   {                                                                                      
			      ddr_printf (DDR_NORMAL, "%4d,",local_vars->failcount_histogram[rf_x][0][bit][nobe]);
			   }                                                                                      
           // ========================================================================================
        }	
        ddr_printf (DDR_NORMAL, "|X|,");
      }
       ddr_printf(DDR_NORMAL,"%6d,%6d,%6d,%6d,%6X,",nobe,cdc_ref[nobe][COARSE_L],cdc_ref[nobe][FINE_L],cdc_ref[nobe][DELAY_L],cdc_ref[nobe][CDC_L]);
       // ============================================================================================
       if ( (safe_delay >= cdc_ref[nobe][DELAY_L ] ) && (safe_delay <cdc_ref[nobe+1][DELAY_L]) )
     	{	
          ddr_printf (DDR_NORMAL, "<<<<<<< ,delay=,%d, coarse=,%d, fine=,%d, ",safe_delay,safe_coarse,safe_fine);	 
	    }
       // ============================================================================================
       if (nobe == pr_min_strt[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_strt 0  %d, ",nobe);	    }
       if (nobe == pr_min_stop[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_stop 0  %d, ",nobe);	    }
       if (nobe == pr_min_midp[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_midp 0  %d, ",nobe);	    }
       if (nobe == pr_max_strt[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_strt 0  %d, ",nobe);	    }
       if (nobe == pr_max_stop[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_stop 0  %d, ",nobe);	    }
       if (nobe == pr_max_midp[0]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_midp 0  %d, ",nobe);	    }
       if (nobe == pr_min_strt[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_strt 1  %d, ",nobe);	    }
       if (nobe == pr_min_stop[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_stop 1  %d, ",nobe);	    }
       if (nobe == pr_min_midp[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_min_midp 1  %d, ",nobe);	    }
       if (nobe == pr_max_strt[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_strt 1  %d, ",nobe);	    }
       if (nobe == pr_max_stop[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_stop 1  %d, ",nobe);	    }
       if (nobe == pr_max_midp[1]  ) {	 ddr_printf (DDR_NORMAL, "<<pr_max_midp 1  %d, ",nobe);	    }
       // ============================================================================================
      ddr_printf (DDR_NORMAL, "\n");
   }
    ddr_printf(DDR_NORMAL, "\n");
// ============================================================================================================================
for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
   {
		  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
          ddr_printf (DDR_NORMAL, "%4d,",remapped_line);
   }	
   ddr_printf (DDR_NORMAL, "remapped_line\n");
// ============================================================================================================================
for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
   {
		  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
         ddr_printf (DDR_NORMAL, "%4d,",remapped_bit);
   }				   
   ddr_printf (DDR_NORMAL, "remapped_bit\n");
// ============================================================================================================================
  for (pr_x = 0; pr_x < 4; pr_x++)
  {
   for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
    {
	 	  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           ddr_printf (DDR_NORMAL, "%4d,",local_vars->pass_reg[remapped_line][remapped_bit][pr_x]);
    }				   
    ddr_printf (DDR_NORMAL, "%s\n",pr_string[pr_x]);
  }
// ============================================================================================================================
   for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit++)
    {
	 	  remapped_line = CA01_line_remapping_bimc2phy_CA [ch][bit];
	 	  remapped_bit  = CA01_bit_remapping_bimc2phy_CA  [ch][bit];
           ddr_printf (DDR_NORMAL, "%4d,",training_data_ptr->results.ca.perbit_delay[ch][cs][remapped_line][remapped_bit]);
    }				   
    ddr_printf (DDR_NORMAL, "%s\n","perbit Delay");
// ============================================================================================================================
    ddr_printf (DDR_NORMAL, "pr_bus_eyew[0]=, %d ,\n",pr_bus_eyew[0]);
    ddr_printf (DDR_NORMAL, "pr_bus_eyew[1]=, %d ,\n",pr_bus_eyew[1]);
    ddr_printf (DDR_NORMAL, "pr_bus_skwr[0]=, %d ,\n",pr_bus_skwr[0]);
    ddr_printf (DDR_NORMAL, "pr_bus_skwr[1]=, %d ,\n",pr_bus_skwr[1]);
#endif	

}// END OF FUNCTION


