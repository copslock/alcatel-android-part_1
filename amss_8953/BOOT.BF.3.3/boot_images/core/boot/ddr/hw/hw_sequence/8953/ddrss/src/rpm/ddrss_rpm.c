/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/rpm/ddrss_rpm.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          -------------------------------------------------------- 
05/07/14   ejose        Initial version
================================================================================*/

#include "ddrss.h"
#include "target_config.h"


//================================================================================================//
// Enter Power Collapse
//================================================================================================//
boolean HAL_DDR_Enter_Power_Collapse(DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
   BIMC_Enter_Power_Collapse (ddr, channel);

   // Enable clamps.
   ddr_mpm_config_ebi1_freeze_io(1);
   
   // Turn on cfg clock before accessing DDRSS CSRs
   ddr_external_set_ddrss_cfg_clk (TRUE);
   
   //disable HPCDC master clock
    DDR_PHY_hal_hpcdc_enable (ddr, 0);   

   // Turn off cfg clock after accessing DDRSS CSRs
   ddr_external_set_ddrss_cfg_clk (FALSE);
   
    return TRUE;
}

//================================================================================================//
// Exit Power Collapse
//================================================================================================//
boolean HAL_DDR_Exit_Power_Collapse(DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
   BIMC_Pre_Exit_Power_Collapse (ddr, channel);
    // Turn on cfg clock before accessing DDRSS CSRs
   ddr_external_set_ddrss_cfg_clk (TRUE);

   //enable HPCDC master clock
   DDR_PHY_hal_hpcdc_enable (ddr, 1);     
   
   //DDRSS_device_reset_cmd();   // LPDDR4 :Device reset register in DDRSS gets reset during power collapse. 
  
  // Turn off cfg clock after accessing DDRSS CSRs
   ddr_external_set_ddrss_cfg_clk (FALSE);

   // Disable clamps.
   ddr_mpm_config_ebi1_freeze_io(0);

   BIMC_Exit_Power_Collapse (ddr, channel);

   return TRUE;
}



//============================================================================
// HAL_DDR_IOCTL
//============================================================================
boolean HAL_DDR_IOCTL (DDR_STRUCT *ddr, IOCTL_CMD ioctl_cmd, IOCTL_ARG *ioctl_arg)
{
    boolean return_value;

    switch (ioctl_cmd)
    {
        case IOCTL_CMD_GET_CORE_REGISTERS_FOR_CRASH_DEBUG:
            ioctl_arg->result_code = IOCTL_RESULT_COMMAND_NOT_IMPLEMENTED;
            return_value = FALSE;
            break;

        /* For the IOCTL_CMD_GET_HIGHEST_BANK_BITcommand, ioctl_arg->results
           is where this API will write the highest DRAM bank bit. */
        case IOCTL_CMD_GET_HIGHEST_BANK_BIT:
            // Calculate highest DRAM bank bit in the 32-bit system address here.
            // Starting with LSB traveling towards MSB in the system address, we have
            //
            // Byte Address: 1b for x16 device, 2b for x32 device.
            // Remove SA[10] for interleave, assign next set of LSBs for column address bits, which is 10 for LPDDR4 parts supported.
            // Next set of LSBs is bank address, 3 bits.
            // Next set of LSBs is row address.
            //
            // This code assumes total symmetry of geometry across all dies, across all channels.
            // Given that assumption, this API simply resolves to:
            //
            //LSB 2 bits for byte addressing
            //Next set of LSBs: If interleave is enabled, 1 bit is added along with the number of column address bits
            //                  If interleave is disabled, just the number of column address bits is added
            //Next set of LSBs: As LPDDR3 supports 8 banks for all types of allowed memory configurations,
            //                  3 bits is added to count for 8 banks

            ioctl_arg->results = (2 + ddr->cdt_params[0].common.num_cols_cs0 + ddr->cdt_params[0].common.interleave_en + 3) - 1;

            // No need to update ioctl_arg->result_code, since we are returning return_value as TRUE, caller software
            // should ignore result_code.
            // ioctl_arg->result_code = IOCTL_RESULT_COMMAND_NOT_IMPLEMENTED;
            return_value = TRUE;
            break;
		case IOCTL_CMD_TEMP_CTRL_MIN_FREQ:
            ioctl_arg->results = BIMC_Temp_Ctrl_Min_Freq (ddr, DDR_CH_BOTH);

            return_value = TRUE;
            break;
        /* Bad command, return appropriate result code. */
        default:
            ioctl_arg->result_code = IOCTL_RESULT_ILLEGAL_COMMAND;
            return_value = FALSE;
    }

    return return_value;
}




//================================================================================================//
// DDR Software Self Refresh Enter
//================================================================================================//
boolean HAL_DDR_Enter_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_Enter_Self_Refresh (ddr, channel, chip_select);

   return TRUE;
}

//================================================================================================//
// DDR Software Self Refresh Exit
//================================================================================================//
boolean HAL_DDR_Exit_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_Exit_Self_Refresh (ddr, channel, chip_select);

   return TRUE;
}



