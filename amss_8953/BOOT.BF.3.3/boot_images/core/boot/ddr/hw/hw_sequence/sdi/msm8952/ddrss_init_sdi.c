/**
 * @file  bimc_config_8936.c
 * @brief  the bimc configurations for MSM8936, including the static setting,
 * read latency, write latency, MR values based on different frequency plans
 * 
 */
/*==============================================================================
                                EDIT HISTORY

$Header $ $DateTime $
$Author: pwbldsvc $
================================================================================
when         who         what, where, why
----------   --------     ------------------------------------------------------
07/30/15   sk      Changes for device Freeze at ddr init
07/30/15   sk      Changes for device stuck in SDI path at self-refresh
04/02/15   sc      Initial version.
================================================================================
                   Copyright 2013-2015 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                                             INCLUDES
==============================================================================*/

#include "ddrss_init_sdi.h"
#include "bimc_config.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HALhwio.h"
#include "ddrss_seq_hwiobase.h"
#include "ddrss_seq_hwioreg.h"
#include "bimc_struct.h"

#ifndef SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET
#define SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET SEQ_DDRSS_BIMC_BIMC_S_DDR0_SHKE_OFFSET
#endif

#define REG_OFFSET_SHKE_ABSOLUTE(INTERFACE) ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_DDRSS_BIMC_BIMC_S_DDR0_SHKE_OFFSET : \
    SEQ_DDRSS_BIMC_BIMC_S_DDR1_SHKE_OFFSET)

#ifndef SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET
#define SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET SEQ_DDRSS_EBI1_PHY_CH0_CA_DDRPHY_CA_OFFSET
#endif

#define REG_BASE_DDRPHY_CA(INTERFACE) ((INTERFACE == SDRAM_INTERFACE_0) ? \
    SEQ_DDRSS_EBI1_PHY_CH0_CA_DDRPHY_CA_OFFSET: \
    SEQ_DDRSS_EBI1_PHY_CH1_CA_DDRPHY_CA_OFFSET)
	
void HAL_SDRAM_PHY_Disable_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_Manual_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_Enable_IO_Cal(SDRAM_INTERFACE interface);
void HAL_SDRAM_PHY_CA_Manual_IO_Cal(SDRAM_INTERFACE interface);

/*==============================================================================
                                           VARIABLE DEFINITIONS                     
==============================================================================*/

uint32 LPDDR3_bimc_global0_config_SDI[][2] = { 
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_PERIOD_ADDR(0,0), 0x10000430},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DDRCC_CLK_EXTEND_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_CAPHY_CLK_EXTEND_ADDR(0,0), 0x00000000},
  {HWIO_BIMC_MISC_GLOBAL_CSR_DDR_CHN_DQPHY_CLK_EXTEND_ADDR(0,0), 0x00000000},  
  {0x0, 0x0} };
 
uint32 LPDDR3_bimc_scmo_config_SDI[][2] = {
  {HWIO_SCMO_CFG_GLOBAL_MON_CFG_ADDR(0), 0x00000013},
  {HWIO_SCMO_CFG_CMD_BUF_CFG_ADDR(0), 0x00000001},
  {HWIO_SCMO_CFG_RCH_SELECT_ADDR(0), 0x00000002},
  {HWIO_SCMO_CFG_RCH_BKPR_CFG_ADDR(0), 0x30307070},
  {HWIO_SCMO_CFG_WCH_BUF_CFG_ADDR(0), 0x00000001},
  {HWIO_SCMO_CFG_FLUSH_CFG_ADDR(0), 0x00320F08},
  {HWIO_SCMO_CFG_FLUSH_CMD_ADDR(0), 0x00000000},
  {HWIO_SCMO_CFG_CMD_OPT_CFG0_ADDR(0), 0x00021110},
  {HWIO_SCMO_CFG_CMD_OPT_CFG1_ADDR(0), 0x030A0B1F},
  {HWIO_SCMO_CFG_CMD_OPT_CFG2_ADDR(0), 0x00000804},
  {HWIO_SCMO_CFG_CMD_OPT_CFG3_ADDR(0), 0x0004001F},
  {HWIO_SCMO_CFG_CMD_OPT_CFG4_ADDR(0), 0x00000011}, 
  {0x0, 0x0} };

uint32 LPDDR3_bimc_dpe_config_SDI[][2] = { 
  {HWIO_ABHN_DPE_CONFIG_0_ADDR(0), 0x11020011},
  {HWIO_ABHN_DPE_CONFIG_1_ADDR(0), 0x32000808},
  {HWIO_ABHN_DPE_CONFIG_2_ADDR(0), 0x00810800},
  {HWIO_ABHN_DPE_CONFIG_3_ADDR(0), 0x040F0012},
  {HWIO_ABHN_DPE_CONFIG_5_ADDR(0), 0x7FFFFFC7},
  {HWIO_ABHN_DPE_ODT_CONFIG_1_ADDR(0), 0x00000100},
  {HWIO_ABHN_DPE_RCW_CTRL_ADDR(0), 0x000000AA},
  {HWIO_ABHN_DPE_PWR_CTRL_0_ADDR(0), 0x01110060},
  {HWIO_ABHN_DPE_OPT_CTRL_0_ADDR(0), 0x00000000},
  {HWIO_ABHN_DPE_OPT_CTRL_1_ADDR(0), 0x00001080},
  {HWIO_ABHN_DPE_TIMER_0_ADDR(0), 0x02302000},
  {HWIO_ABHN_DPE_TIMER_1_ADDR(0), 0x00002140},
  {HWIO_ABHN_DPE_TIMER_2_ADDR(0), 0x0000DAF4},
  {HWIO_ABHN_DPE_TIMER_3_ADDR(0), 0x00000432},
  {HWIO_ABHN_DPE_DRAM_TIMING_0_ADDR(0), 0x000031A4},
  {HWIO_ABHN_DPE_DRAM_TIMING_1_ADDR(0), 0x409630B4},
  {HWIO_ABHN_DPE_DRAM_TIMING_2_ADDR(0), 0x404B2064},
  {HWIO_ABHN_DPE_DRAM_TIMING_3_ADDR(0), 0x05140258},
  {HWIO_ABHN_DPE_DRAM_TIMING_4_ADDR(0), 0x0000404B},
  {HWIO_ABHN_DPE_DRAM_TIMING_5_ADDR(0), 0x60D260B4},
  {HWIO_ABHN_DPE_DRAM_TIMING_6_ADDR(0), 0x81F4304B},
  {HWIO_ABHN_DPE_DRAM_TIMING_7_ADDR(0), 0x00000384},
  {HWIO_ABHN_DPE_DRAM_TIMING_9_ADDR(0), 0x00003096},
  {HWIO_ABHN_DPE_DRAM_TIMING_10_ADDR(0), 0x25782578},
  {HWIO_ABHN_DPE_DRAM_TIMING_11_ADDR(0), 0x304B304B},
  {HWIO_ABHN_DPE_DRAM_TIMING_12_ADDR(0), 0x304B304B},
  {HWIO_ABHN_DPE_DRAM_TIMING_13_ADDR(0), 0x40044004},
  {HWIO_ABHN_DPE_DRAM_TIMING_14_ADDR(0), 0x00001046},
  {HWIO_ABHN_DPE_DRAM_TIMING_15_ADDR(0), 0x000080E6},
  {HWIO_ABHN_DPE_DRAM_TIMING_16_ADDR(0), 0x00040E08},
  {HWIO_ABHN_DPE_DRAM_TIMING_17_ADDR(0), 0x00000E10},
  {HWIO_ABHN_DPE_DRAM_TIMING_23_ADDR(0), 0x0A122301},
  {0x0, 0x0} };

uint32 LPDDR3_bimc_shke_config_SDI[][2] = {  
  {HWIO_ABHN_SHKE_CONFIG_ADDR(0), 0x001F8001},
  {HWIO_ABHN_SHKE_PERIODIC_MRR_ADDR(0), 0x00404000},
  {HWIO_ABHN_SHKE_PERIODIC_ZQCAL_ADDR(0), 0x00000800},
  {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_ADDR(0), 0x00000049},
  {HWIO_ABHN_SHKE_SELF_REFRESH_CNTL_ADDR(0), 0x00032100},
  {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_1_ADDR(0), 0x00000049},
  {HWIO_ABHN_SHKE_AUTO_REFRESH_CNTL_2_ADDR(0), 0x00000000},
  {0x0, 0x0} };

  
/*==============================================================================
                                  FUNCTIONS
==============================================================================*/ 

/* =============================================================================
 **  Function : BIMC_GLOBAL0_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_GLOBAL0 static settings
 **/
void 
BIMC_GLOBAL0_Set_Config_SDI( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;

    if (LPDDR3_bimc_global0_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_global0_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_global0_config_SDI[reg][0] + reg_base, LPDDR3_bimc_global0_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : BIMC_SCMO_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SCMO static settings
 **/
void 
BIMC_SCMO_Set_Config_SDI( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg; 
 
    if (LPDDR3_bimc_scmo_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_scmo_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_scmo_config_SDI[reg][0] + reg_base, LPDDR3_bimc_scmo_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : BIMC_DPE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_DPE static settings
 **/
void 
BIMC_DPE_Set_Config_SDI ( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;

    if (LPDDR3_bimc_dpe_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_dpe_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_dpe_config_SDI[reg][0] + reg_base, LPDDR3_bimc_dpe_config_SDI[reg][1]);
      }
    }

}

/* =============================================================================
 **  Function : BIMC_SHKE_Set_Config_SDI
 ** =============================================================================
 */
/**
 *    @details
 *     program BIMC_SHKE static settings
 **/
void 
BIMC_SHKE_Set_Config_SDI ( uint32 reg_base, SDRAM_INTERFACE interface)
{
  uint32 reg;
  
    if (LPDDR3_bimc_shke_config_SDI != NULL)
    {
      for (reg = 0; LPDDR3_bimc_shke_config_SDI[reg][0] != 0; reg++)
      {
        out_dword(LPDDR3_bimc_shke_config_SDI[reg][0] + reg_base, LPDDR3_bimc_shke_config_SDI[reg][1]);
      }
    }
}

/* =============================================================================
 **  Function : HAL_SDRAM_SDI_Init
 ** =============================================================================
 */
/**
 *   @brief
 *   Initialize DDR controller, as well as DDR device.
 **/
void HAL_SDRAM_SDI_Init(SDRAM_INTERFACE interface, SDRAM_CHIPSELECT chip_select, uint32 clk_freq_in_khz)
{

  uint32 clk_period_in_ps;
  uint32 shke_config;
  
  clk_period_in_ps = ddr_divide_func(1000000000, clk_freq_in_khz);

  /* for wdog reset path save the shke_config */
  shke_config = in_dword(REG_OFFSET_SHKE_ABSOLUTE(interface) + HWIO_ABHN_SHKE_CONFIG_ADDR(0));

  #ifndef FEATURE_RUMI_BOOT
  /* PHY Initialization */
  /* clk_mode = 1, odt_en = 0 */
  EBI1_PHY_CFG_phy_init(SEQ_DDRSS_EBI1_PHY_OFFSET, 0, interface, dynamic_legacy, 0 );
  EBI1_PHY_CFG_DDR_CC_Pre_Init_Setup(SEQ_DDRSS_EBI1_PHY_OFFSET,interface);
  #endif 
  
  BIMC_Pre_Init_Setup_SDI (SEQ_DDRSS_BIMC_OFFSET, interface, clk_freq_in_khz, clk_period_in_ps);
  
  #ifndef FEATURE_RUMI_BOOT
  HAL_SDRAM_PHY_CA_Manual_IO_Cal(interface);
  #endif

  ABHN_SHKE_Disable_All_Periodic(REG_OFFSET_SHKE_ABSOLUTE(interface), chip_select);
  
  out_dword(REG_OFFSET_SHKE_ABSOLUTE(interface) + HWIO_ABHN_SHKE_CONFIG_ADDR(0),shke_config);
  
  /* Disable WDOG self-refresh hack code*/
  HWIO_ABHN_SHKE_DRAM_MANUAL_0_OUTM(SEQ_DDRSS_BIMC_BIMC_S_DDR0_SHKE_OFFSET, 0x2000, 0x2000);  

  BIMC_Post_Init_Setup (SEQ_DDRSS_BIMC_OFFSET,interface,chip_select,0);  
  
  HAL_SDRAM_Exit_Self_Refresh(interface, chip_select);

} /* HAL_SDRAM_SDI_Init */

/* =============================================================================
 **  Function : BIMC_Pre_Init_Setup_SDI
 ** =============================================================================
 */
/**
 *   @brief
 *      one-time settings for SCMO/DPE/SHKE/BIMC_MISC; values are provided in bimc_config.c parsing from SWI
 *      AC Timing Parameters is update by SW CDT at the end
 *      ODT is on (now hard-coded for initialzing ODT for rank 0)
 **/
void
BIMC_Pre_Init_Setup_SDI (uint32 reg_base, SDRAM_INTERFACE interface,
    uint32 clk_freq_in_khz, uint32 clk_period_in_ps)
{
  uint32 clk_mode = 0;

  if(interface == SDRAM_INTERFACE_0) {
    BIMC_GLOBAL0_Set_Config_SDI( reg_base + SEQ_BIMC_GLOBAL0_OFFSET, interface) ;       
  }

  clk_mode = Get_BIMC_LUT (interface, clk_freq_in_khz, 5);


  BIMC_MISC_GLOBAL_CSR_Mode_Switch(reg_base + SEQ_BIMC_GLOBAL0_OFFSET, interface, clk_mode);    
  BIMC_MISC_GLOBAL_CSR_Update_Clock_Period (reg_base + SEQ_BIMC_GLOBAL0_OFFSET,
      interface, CLK_PERIOD_RESOLUTION_1PS, clk_period_in_ps);

  BIMC_SCMO_Set_Config_SDI(reg_base + REG_OFFSET_SCMO(interface), interface);

  BIMC_DPE_Set_Config_SDI(reg_base+ REG_OFFSET_DPE(interface), interface);

  BIMC_SHKE_Set_Config_SDI(reg_base+ REG_OFFSET_SHKE(interface), interface);

  BIMC_Update_AC_Parameters(reg_base, interface, clk_freq_in_khz, clk_period_in_ps);
}
