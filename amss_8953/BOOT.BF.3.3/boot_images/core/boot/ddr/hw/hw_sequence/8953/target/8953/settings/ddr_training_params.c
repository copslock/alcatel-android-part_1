/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/target/8953/settings/ddr_training_params.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
06/20/14   ejose        First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"

void DDRSS_set_training_params (training_params_t *training_params_ptr)
{

   training_params_ptr->dcc.max_loopcnt = 0;

   training_params_ptr->ca.max_loopcnt              = 10;//5
   training_params_ptr->ca.coarse_cdc_start_value   = 1;
   training_params_ptr->ca.coarse_cdc_max_value     = 24;
   training_params_ptr->ca.coarse_cdc_step          = 1;
   training_params_ptr->ca.fine_training_enable     = 1;
   training_params_ptr->ca.fine_cdc_max_value       =12;
   training_params_ptr->ca.fine_cdc_step            = 1;

   training_params_ptr->wrlvl.max_loopcnt      = 16;
   training_params_ptr->wrlvl.max_coarse_cdc   = 24;
   training_params_ptr->wrlvl.max_fine_cdc     = 12;
   training_params_ptr->wrlvl.coarse_cdc_step  = 1;
   training_params_ptr->wrlvl.fine_cdc_step    = 1;
   training_params_ptr->wrlvl.feedback_percent = 100;
   
   training_params_ptr->rcw.max_loopcnt     = 1; //0;
   training_params_ptr->rcw.max_coarse_cdc  = 24; //0;
   training_params_ptr->rcw.max_fine_cdc    = 6; //0;
   training_params_ptr->rcw.coarse_cdc_step                    = COARSE_STEP_IN_PS; //0;
   training_params_ptr->rcw.fine_cdc_step                      = FINE_STEP_IN_PS; //0;

   training_params_ptr->rd_dqdqs.max_loopcnt             = 5;
   training_params_ptr->rd_dqdqs.coarse_cdc_start_value  = 0;
   training_params_ptr->rd_dqdqs.coarse_cdc_max_value    = 15;
   training_params_ptr->rd_dqdqs.coarse_cdc_step         = 1;
   training_params_ptr->rd_dqdqs.fine_training_enable    = 1;
   training_params_ptr->rd_dqdqs.fine_cdc_start_value    = 0;
   training_params_ptr->rd_dqdqs.fine_cdc_max_value      = 15;
   training_params_ptr->rd_dqdqs.fine_cdc_step           = 1;
   training_params_ptr->rd_dqdqs.pbit_start_value        = 0;

   training_params_ptr->wr_dqdqs.max_loopcnt             = 5;
   training_params_ptr->wr_dqdqs.coarse_cdc_start_value  = 0;
   training_params_ptr->wr_dqdqs.coarse_cdc_max_value    = 15;
   training_params_ptr->wr_dqdqs.coarse_cdc_step         = 1;
   training_params_ptr->wr_dqdqs.fine_training_enable    = 1;
   training_params_ptr->wr_dqdqs.fine_cdc_top_freq_start_value      = 10;
   training_params_ptr->wr_dqdqs.fine_cdc_start_value               = 0;
   training_params_ptr->wr_dqdqs.fine_cdc_top_freq_max_value        = 15;
   training_params_ptr->wr_dqdqs.fine_cdc_max_value      = 15;
   training_params_ptr->wr_dqdqs.fine_cdc_step           = 1;
   training_params_ptr->wr_dqdqs.pbit_start_value        = 0;

}

