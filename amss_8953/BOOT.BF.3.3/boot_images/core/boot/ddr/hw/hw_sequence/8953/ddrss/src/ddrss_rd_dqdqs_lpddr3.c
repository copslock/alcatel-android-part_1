/*************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_rd_dqdqs_lpddr3.c#2 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#include "ddrss.h"
#include <string.h>

// -------------------------------------------------------------------------
// DDR PHY RD DQ-DQS Training 
// -------------------------------------------------------------------------
boolean DDRSS_rd_dqdqs_lpddr3 (DDR_STRUCT *ddr,
                                        uint8 ch,
                                        uint8 cs,
                                        training_params_t *training_params_ptr,                               
                                        ddrss_rdwr_dqdqs_local_vars *local_vars,
                                        uint32 curr_training_freq_khz, // current clock frequency
                                        uint8  training_ddr_freq_indx, // current training loop index
                                        uint8  prfs_index)             // current prfs index
{ 
    uint8 byte_lane       = 0;
    uint8 bit             = 0;


    
    best_eye_struct rd_best_eye_coarse[NUM_DQ_PCH];
   
    uint8 fine_left_start_cdc_value[NUM_DQ_PCH]  = {0};    
    uint8 fine_right_start_cdc_value[NUM_DQ_PCH] = {0}; 
    uint8 max_training_freq_index                = TRAINING_NUM_FREQ_LPDDR3 - 1;
    uint32 dq0_ddr_phy_base                      =  0;




    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
 
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

   
    ddr_printf (DDR_NORMAL,"    RD Current clock  = %d prfs_index = %d\n\n",curr_training_freq_khz ,prfs_index);

    //Clear the Per-bit, Coarse and Fine CDC registers before coarse training
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      // Retain the per bit training values if not the highest frequency
      if(training_ddr_freq_indx == max_training_freq_index )  
      {
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
          // Set the perbit start values
          DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                         bit, 
                                         0,   // 0 for RX.
                                         cs, 
                                         training_params_ptr->rd_dqdqs.pbit_start_value);
        }
      }
       // Clear the Coarse CDC values before Coarse training
       DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                     0,
                                     1, //coarse 
                                     1, 
                                     cs);

       rd_best_eye_coarse[byte_lane].best_cdc_value = 0;

       // Clear Fine CDC values before Coarse training
       DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                     0,
                                     0, //fine
                                     1, 
                                     cs);

       rd_best_eye_coarse[byte_lane].best_fine_cdc_value = 0;
       rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value = 0;              

    }

    
    // ---------------------------------------------------------------------------------
    // Calibrate the per bit skew and DQS DCC at the highest frequency
    // ---------------------------------------------------------------------------------
    if(training_ddr_freq_indx == max_training_freq_index)  
    {
      // ---------------------------------------------------------------------------------
      // Determine the start coarse CDC for per-bit alignment
      // ---------------------------------------------------------------------------------
      DDRSS_RD_left_edge_search (ddr, 
                                 ch, 
                                 cs, 
                                 training_data_ptr,
                                 training_params_ptr,
                                 rd_best_eye_coarse); 

      // ---------------------------------------------------------------------------------
      // Calibrate the per-bit alignment 
      // ---------------------------------------------------------------------------------
      for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {            
        fine_left_start_cdc_value[byte_lane] = rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value;              

        // Per bit calibration
        ddr_printf (DDR_NORMAL,"    RD Per-Bit CDC Training\n");
        DDRSS_RD_perbit_align_phase(ddr,
                                    ch,
                                    cs,
                                    byte_lane,
                                    fine_left_start_cdc_value[byte_lane],
                                    training_params_ptr,
                                    curr_training_freq_khz,
                                    0); // phase 0

      }

      // Optionally adjust the Read DQS Duty cycle
      #if DSF_RD_DQDQS_DCC 
        ddr_printf (DDR_NORMAL,"    RD DQS DCC Training\n");
        DDRSS_rd_dqdqs_dcc_schmoo (ddr, 
                                   ch, 
                                   cs, 
                                   fine_left_start_cdc_value,
                                   training_params_ptr, 
                                   local_vars, 
                                   curr_training_freq_khz);
      #endif
    }

    // ---------------------------------------------------------------------------------
    // Coarse Training.    
    // ---------------------------------------------------------------------------------
    DDRSS_RD_CDC_Coarse_Schmoo (ddr,
                                     ch,
                                     cs,
                                     training_data_ptr,
                                     training_params_ptr, 
                                     rd_best_eye_coarse, 
                                     local_vars, 
                                     curr_training_freq_khz);
   
    // Post process the Coarse data eye
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      // Abort the routine if no Read eye found on any byte lane
      if(rd_best_eye_coarse[byte_lane].all_fail_flag == 1)
      {
          return FALSE;
      }
    }   

    // ---------------------------------------------------------------------------------
    // Fine Training (optional)    
    // ---------------------------------------------------------------------------------
    if(training_params_ptr->rd_dqdqs.fine_training_enable == 1)
    {
        ddr_printf (DDR_NORMAL,"    RD Fine Training\n");
        for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {    
          // At max frequency, the left boundary is already aligned to the Coarse CDC by the Per-bit
          if(training_ddr_freq_indx != max_training_freq_index)  
          {
            // Start the Fine CDC search from the first failing coarse 
            if (rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value != 0) 
            {
              rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value--;
            }
            fine_left_start_cdc_value[byte_lane] = rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value;              
          }
          fine_right_start_cdc_value[byte_lane] = rd_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value; 
        }
    
        // At the maximum frequency, the left edge is aligned to the coarse boundary by per-bit training
        if(training_ddr_freq_indx != max_training_freq_index)  
        {
             memset(local_vars->fine_schmoo.fine_dq_passband_info_left, 
             training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
             NUM_DQ_PCH * FINE_CDC);
          // Search the fine left edge
          DDRSS_RD_CDC_Fine_Schmoo (ddr, 
                                    ch, 
                                    cs, 
                                    training_data_ptr, 
                                    training_params_ptr, 
                                    fine_left_start_cdc_value, 
                                    local_vars->ddrss_rdwr_fine_cdc_boundary.fine_left_boundary_cdc_value, 
                                    SWEEP_LEFT,
                                    local_vars,
                                    curr_training_freq_khz);
        }       
         memset(local_vars->fine_schmoo.fine_dq_passband_info_right, 
         training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
         NUM_DQ_PCH * FINE_CDC);

        // Search the fine right edge
        DDRSS_RD_CDC_Fine_Schmoo (ddr, 
                                  ch, 
                                  cs, 
                                  training_data_ptr, 
                                  training_params_ptr, 
                                  fine_right_start_cdc_value, 
                                  local_vars->ddrss_rdwr_fine_cdc_boundary.fine_right_boundary_cdc_value, 
                                  SWEEP_RIGHT, 
                                  local_vars,
                                  curr_training_freq_khz);
        
        
        // Find the CDC edges 
        for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
                if(training_ddr_freq_indx != max_training_freq_index)  
                {
                    ddr_printf (DDR_NORMAL,"    RD Byte %d Fine Left = %d  Right = %d\n",byte_lane,
                       local_vars->ddrss_rdwr_fine_cdc_boundary.fine_left_boundary_cdc_value[byte_lane],
                       local_vars->ddrss_rdwr_fine_cdc_boundary.fine_right_boundary_cdc_value[byte_lane]);
                }
                else
                {
                    ddr_printf (DDR_NORMAL,"    RD Byte %d Fine Left = Not Trained  Right = %d\n",byte_lane,
                       local_vars->ddrss_rdwr_fine_cdc_boundary.fine_right_boundary_cdc_value[byte_lane]);
                }
          // Sum the left and right fine offsets 
          rd_best_eye_coarse[byte_lane].best_fine_cdc_value = 
            ddrss_lib_uidiv((local_vars->ddrss_rdwr_fine_cdc_boundary.fine_left_boundary_cdc_value[byte_lane] +
                             local_vars->ddrss_rdwr_fine_cdc_boundary.fine_right_boundary_cdc_value[byte_lane]),2);
        }
        ddr_printf (DDR_NORMAL,"\n");
    } // if (fine_training

    
    // ---------------------------------------------------------------------------------
    // Post proceess the training results and store to the data structure and registers
    // ---------------------------------------------------------------------------------
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {     
      // Calculate the coarse center of the eye 
      rd_best_eye_coarse[byte_lane].best_cdc_value = ddrss_lib_uidiv((rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value +
                                                                      rd_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value),2);

      // Write the coarse CDC to the results data structure
      training_data_ptr->results.rd_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane] = 
                         rd_best_eye_coarse[byte_lane].best_cdc_value;

      // Add an extra offset if the coarse center is odd
      if ((ddrss_lib_modulo((rd_best_eye_coarse[byte_lane].max_eye_left_boundary_cdc_value + 
                             rd_best_eye_coarse[byte_lane].max_eye_right_boundary_cdc_value) , 2)) != 0) 
      {    
        rd_best_eye_coarse[byte_lane].best_fine_cdc_value += ddrss_lib_uidiv(FINE_STEPS_PER_COARSE,2);
      }

      // Write the fine CDC to the results data structure
      training_data_ptr->results.rd_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane] = 
                         rd_best_eye_coarse[byte_lane].best_fine_cdc_value;

      // Write the trained coarse center to the active CDC register
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    training_data_ptr->results.rd_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane],
                                    0x1, // coarse set
                                    0x1, 
                                    cs);   

      // Fine trained fine value
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    training_data_ptr->results.rd_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane],
                                    0x0, // fine set
                                    0x1, 
                                    cs);   
 
      ddr_printf(DDR_NORMAL,"    RD Byte %u Data Eye Center Coarse = %d Fine = %d \n",
                  byte_lane,
                  training_data_ptr->results.rd_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane],
                  training_data_ptr->results.rd_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane]);

      //---------------------------------------------------------------------------------------------------------------
      // Write the Ext registers and scale the trained results for lower bands
      //---------------------------------------------------------------------------------------------------------------

      // Write the trained coarse and fine to the EXT registers
      DDR_PHY_hal_cfg_cdcext_slave_rd ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       cs, 
                                       training_data_ptr->results.rd_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane],
                                       1/*coarse*/, 
                                       HP_MODE, 
                                       prfs_index
                                      );
            
      DDR_PHY_hal_cfg_cdcext_slave_rd ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       cs, 
                                       training_data_ptr->results.rd_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane],
                                       0/*fine*/,   
                                       HP_MODE, 
                                       prfs_index
                                      );

  } // byte_lane
  if (training_ddr_freq_indx == 0) 
     { //offset calculation:  offset = calculated center - T/4 
        Scale_rd_dqdqs_lpddr3 (  ddr, 
                                 ch,
                                 cs,
                                 training_ddr_freq_indx,
                                 curr_training_freq_khz, 
                                 prfs_index - 1);
     }   
    ddr_printf(DDR_NORMAL,"\n");
  return TRUE;   
}// DDRSS_rd_dqdqs_lpddr3

void DDRSS_RD_CDC_Coarse_Schmoo (DDR_STRUCT *ddr, 
                                 uint8 ch, 
                                 uint8 cs, 
                                 training_data *training_data_ptr,
                                 training_params_t *training_params_ptr,
                                 best_eye_struct *best_eye_ptr, 
                                 ddrss_rdwr_dqdqs_local_vars *local_vars,
                                 uint32 clk_freq_khz)
{
    uint8 coarse_cdc_value  = 0;
    uint8 byte_lane         = 0;
    uint8 loopcnt           = 0;
    uint8 *compare_result;  
    uint8 compare_result_acc[NUM_DQ_PCH]    = {0};  

    uint8 coarse_dq_error_count[NUM_DQ_PCH] = {0};
    uint32 dq0_ddr_phy_base = 0;

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
     
    // Initialize the fail flag and best cdc
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        best_eye_ptr[byte_lane].all_fail_flag = 0;  // Assume all sane values to begin with.
        best_eye_ptr[byte_lane].best_cdc_value = 0; // Assume initial value = 0
    }

      memset(local_vars->coarse_schmoo.coarse_dq_passband_info, 
             training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
             NUM_DQ_PCH * COARSE_CDC);
    
        // CDC Schmoo loop
        for(coarse_cdc_value  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; 
            coarse_cdc_value <= training_params_ptr->rd_dqdqs.coarse_cdc_max_value; 
            coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
        {
           // Update RD CDC
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
                DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                              coarse_cdc_value, 
                                              0x1, 
                                              0x1, 
                                              cs);   
                compare_result_acc[byte_lane] = 0;
            }

            // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
            for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt; loopcnt++) 
            {
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x3);

                // Accumulate the compare results per byte
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                {
                  compare_result_acc[byte_lane] += compare_result[byte_lane];
                }
            }
             
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
            { 
                // If fail, increment error_count.
                coarse_dq_error_count[byte_lane] = compare_result_acc[byte_lane];
                local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value] = coarse_dq_error_count[byte_lane]; 
            } // byte_lane 
        } // CDC_LOOP 
        
        // Print the RD Coarse Histogram
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
        { 
          ddr_printf(DDR_NORMAL,"    RD Byte %d : ",byte_lane);
          for (coarse_cdc_value=0;coarse_cdc_value<training_params_ptr->rd_dqdqs.coarse_cdc_max_value;coarse_cdc_value++)
          {
            ddr_printf(DDR_NORMAL,"%d ",local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value]);
          }
          ddr_printf(DDR_NORMAL,"\n");
        }
        ddr_printf(DDR_NORMAL,"\n");
    
    // Calculate the left and right boundaries of the eye (first pass and last pass)
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value  = 0xFF;

      // Search the pass/fail histogram for the boundaries
      for (coarse_cdc_value=0;
           coarse_cdc_value<training_params_ptr->rd_dqdqs.coarse_cdc_max_value;
           coarse_cdc_value++)
      { 
        if (local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_cdc_value] == 0)
        {
          best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value = coarse_cdc_value;

          if (best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value > coarse_cdc_value)
          {
            best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value  = coarse_cdc_value;
          }
        }
      }
        
      ddr_printf(DDR_NORMAL, "    Byte %u Coarse L<->R Edge: %d <-> %d : Width = %d \n",
                byte_lane,
                best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value,
                best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value,
                ((best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value -
                  best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value) + 1));

    } // byte_lane
    ddr_printf(DDR_NORMAL, "\n");
}//DDRSS_RD_CDC_Coarse_Schmoo

void DDRSS_RD_CDC_Fine_Schmoo (DDR_STRUCT *ddr, 
                               uint8 ch, 
                               uint8 cs, 
                               training_data *training_data_ptr,
                               training_params_t *training_params_ptr,
                               uint8 *coarse_cdc_value,
                               uint8 *rd_boundary_fine_cdc_ptr,
                               uint8 direction, 
                               ddrss_rdwr_dqdqs_local_vars *local_vars,
                               uint32 clk_freq_khz)
{
    uint8  fine_cdc_value   = 0;
    uint8  byte_lane        = 0;
    uint8  loopcnt          = 0;
    uint32 dq0_ddr_phy_base = 0;

    uint8 *compare_result;
    uint8 compare_result_acc[NUM_DQ_PCH] = {0}; 

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      // Set coarse start for fine search
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    coarse_cdc_value[byte_lane], 
                                    0x1, // 1 for coarse delay
                                    0x1, 
                                    cs);  
    }

    // Fine CDC Search
    for(fine_cdc_value  = training_params_ptr->rd_dqdqs.fine_cdc_start_value; 
        fine_cdc_value <= training_params_ptr->rd_dqdqs.fine_cdc_max_value; 
        fine_cdc_value += training_params_ptr->rd_dqdqs.fine_cdc_step)
    {
      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {
        // Update CDC
        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                      fine_cdc_value, 
                                      0x0, // 0 for fine delay
                                      0x1, 
                                      cs); 

        compare_result_acc[byte_lane] = 0;
      }
        
      // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
      for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt; loopcnt++) 
      {
        // DRAM read 
        compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1,0x3);
       
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
          // Accumulate the compare results 
          compare_result_acc [byte_lane] += compare_result[byte_lane];
        }
      }

      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {
        // Save the results into the fine histograms 
        if(direction == SWEEP_LEFT) 
        {
            local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][fine_cdc_value] =
                        compare_result_acc[byte_lane]; 
        }
  
        if(direction == SWEEP_RIGHT) 
        {
            local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][fine_cdc_value] = 
                       compare_result_acc[byte_lane]; 
        }
      }
    } // CDC_LOOP 
    
    // Print the RD Fine histogram
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      if(direction == SWEEP_LEFT) 
      {
        ddr_printf(DDR_NORMAL,"    RD Byte %d Fine Left : ",byte_lane);
        for (fine_cdc_value=0;fine_cdc_value<training_params_ptr->rd_dqdqs.fine_cdc_max_value;fine_cdc_value++)
        {
          ddr_printf(DDR_NORMAL,"%d ",local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][fine_cdc_value]);
        }
        ddr_printf(DDR_NORMAL,"\n");
      }

      if(direction == SWEEP_RIGHT) 
      {
        ddr_printf(DDR_NORMAL,"    RD Byte %d Fine Right : ",byte_lane);
        for (fine_cdc_value=0;fine_cdc_value<=training_params_ptr->rd_dqdqs.fine_cdc_max_value;fine_cdc_value++)
        {
          ddr_printf(DDR_NORMAL,"%d ",local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][fine_cdc_value]);
        }
        ddr_printf(DDR_NORMAL,"\n");
      }
    }
    ddr_printf(DDR_NORMAL,"\n");

    // Search the passband histogram to find the first passing value
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      rd_boundary_fine_cdc_ptr[byte_lane] = 0x0;

      if(direction == SWEEP_LEFT) 
      {
        for (fine_cdc_value=0;fine_cdc_value<training_params_ptr->rd_dqdqs.fine_cdc_max_value;fine_cdc_value++)
        {
          if (local_vars->fine_schmoo.fine_dq_passband_info_left[byte_lane][fine_cdc_value] == 0)
          {
            rd_boundary_fine_cdc_ptr[byte_lane] = fine_cdc_value;
          //ddr_printf(DDR_NORMAL,"    RD Byte %d Fine boundary = %d\n",byte_lane,rd_boundary_fine_cdc_ptr[byte_lane]);
            break;
          }
        }
      }
      
      if(direction == SWEEP_RIGHT) 
      {
        for (fine_cdc_value=0;fine_cdc_value<training_params_ptr->rd_dqdqs.fine_cdc_max_value;fine_cdc_value++)
        {
          if (local_vars->fine_schmoo.fine_dq_passband_info_right[byte_lane][fine_cdc_value] != 0 && fine_cdc_value != 0)
          {
            rd_boundary_fine_cdc_ptr[byte_lane] = fine_cdc_value - 1;
          //ddr_printf(DDR_NORMAL,"    RD Byte %d Fine boundary = %d\n",byte_lane,rd_boundary_fine_cdc_ptr[byte_lane]);
            break;
          }
        }
      }
    } // byte_lane
} // Fine Schmoo

///================================================================================================//
//  Function: DDRSS_rd_dqdqs_dcc_schmoo().
//  Brief description: This function performs dcc search at the maximum frequency per each CS and
//                       sets the average DCC for both the CS's during read training.
//================================================================================================//
void DDRSS_rd_dqdqs_dcc_schmoo (DDR_STRUCT *ddr, 
                                uint8 ch, 
                                uint8 cs, 
                                uint8 *fine_left_start_cdc_value,
                                training_params_t *training_params_ptr, 
                                ddrss_rdwr_dqdqs_local_vars *local_vars,
                                uint32 clk_freq_khz)
{
    uint8  byte_lane                    = 0;
    uint32 dq0_ddr_phy_base             = 0;
    uint8  index                        = 0;
    uint8  phase                        = 0;
    uint8  best_dcc_value               = 0;
    uint8 dq_min[NUM_DQ_PCH][PHASE]     = {{0x0}};
    uint8 dq_max[NUM_DQ_PCH][PHASE]     = {{0x0}};

    // Training data structure pointer 
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    // Initialize the fail flag, best cdc, coarse and fine 
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Start search at left edge of pass/fail region
        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                      0, 
                                      1, 
                                      1, 
                                      cs);
        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                      0, 
                                      0, 
                                      1, 
                                      cs);            
    }
    
    //-----------------------------------------------------------------------
    // Train each byte lane separately for DQS DCD
    //-----------------------------------------------------------------------
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      // Generate histograms for both phases
      DDRSS_RD_phase_schmoo (ddr,
                             ch,
                             cs,
                             byte_lane,
                             training_params_ptr,
                             (ddrss_rdwr_dqdqs_local_vars *)local_vars,
                             clk_freq_khz);

    // Build a data structure to hold the eye widths and edges
    // Evalute both Phases
    for (phase = 0; phase < 2; phase++) 
    {
        // Initialize the dcc_width to 0
        local_vars->coarse_schmoo.dcc_width[byte_lane][cs][phase] = 0;
        dq_min[byte_lane][phase] = 64;

        // Identify the histogram data eye
        for(index = 0; index < 64; index++)
        {
            if (local_vars->coarse_schmoo.coarse_dq_dcc_schmoo[byte_lane][cs][phase][index] == 0) 
            {
                // Determine the left edge
                if (dq_min[byte_lane][phase] >= index)
                { 
                    dq_min[byte_lane][phase] =  index;
                }

                // Determine the right edge
                if (dq_max[byte_lane][phase] < index)
                { 
                    dq_max[byte_lane][phase] =  index;
                }

                // Calculate the eye width by counting the zeros
                local_vars->coarse_schmoo.dcc_width[byte_lane][cs][phase]++;
            }
        }
      //ddr_printf(DDR_NORMAL,"    Byte %d CS %d Phase %d Left <=> Right Edge %d <=> %d Width = %d -> %d ps\n",
      //        byte_lane,cs,phase,
      //        dq_min[byte_lane][phase], 
      //        dq_max[byte_lane][phase],
      //        local_vars->coarse_schmoo.dcc_width[byte_lane][cs][phase],
      //        local_vars->coarse_schmoo.dcc_width[byte_lane][cs][phase] * FINE_STEP_IN_PS);

        //printf("              Phase %d Left  delay = %d -> %d ps\n\n",
        //                                      phase,
        //                                      dq_min[byte_lane][phase], 
        //                                      dq_min[byte_lane][phase] * FINE_STEP_IN_PS);
    } // phase
    ddr_printf(DDR_NORMAL,"\n");

    // Determine if there is DCD in DQS by inspecting the edges
    if (dq_min[byte_lane][0] < dq_min[byte_lane][1])
    {
      ddr_printf(DDR_NORMAL,"    RD Byte %d Case 1 : DQS Phase 0 is less than 50 percent\n",byte_lane);
      //  Sweep the RD DCC by delaying the falling edge of DQS until
      //  the search finds the left edge of Phase 1\
      //  
      //               FFFFFP          FFFFFP
      //            __ --->|        __ --->|  
      //     DQS   |  |____________|  |_______
      //            ___________________________ 
      //     DQ    X_______X_______X_______X____
      //     Phase     0       1       0       
  
      DDRSS_RD_sweep_DQS_DCC (ddr,
                              ch,
                              cs,
                              byte_lane,
                              fine_left_start_cdc_value[byte_lane],
                              training_params_ptr,
                              (ddrss_rdwr_dqdqs_local_vars *)local_vars,
                              clk_freq_khz,
                              1); // phase = 1
  

    } 
    else if (dq_min[byte_lane][1] < dq_min[byte_lane][0])
    {
      ddr_printf(DDR_NORMAL,"    RD Byte %d Case 2 : DQS Phase 0 is greater than 50 percent\n",byte_lane);
      //  Sweep the RD DCC by delaying the rising edge of DQS until
      //  the search finds the left edge of Phase 0
      //  
      //           FFFFFP          FFFFFP
      //            ___________     ___________
      //     DQS   |           |___|           |__
      //           --->            --->
      //           ____ ___________________________ 
      //     DQ    ____X_______X_______X_______X____
      //     Phase         0       1       0       
  
      // Align the DQ per bit to the Phase 1 DQS at CDC = 0
        DDRSS_RD_perbit_align_phase(ddr,
                                    ch,
                                    cs,
                                    byte_lane,
                                    fine_left_start_cdc_value[byte_lane],
                                    training_params_ptr,
                                    clk_freq_khz,
                                    1); // phase 1

      // Sweep the DQS DCC to find the edge of Phase 0
      DDRSS_RD_sweep_DQS_DCC (ddr,
                              ch,
                              cs,
                              byte_lane,
                              fine_left_start_cdc_value[byte_lane],
                              training_params_ptr,
                              (ddrss_rdwr_dqdqs_local_vars *)local_vars,
                              clk_freq_khz,
                              0); // phase = 0

    }
  } // for (byte_lane

    // Average the RD DCC per rank
  if(cs == 1)
  {
      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
      {
        // Check for mixed rising and falling adjustment between ranks
        if ((((local_vars->coarse_schmoo.best_dcc[byte_lane][0]>>4) > 0) && (((local_vars->coarse_schmoo.best_dcc[byte_lane][1]) & 0xF) > 0)) ||
            (((local_vars->coarse_schmoo.best_dcc[byte_lane][1]>>4) > 0) && (((local_vars->coarse_schmoo.best_dcc[byte_lane][0]) & 0xF) > 0)))
        {
          // zero out DCC adjustment in the mixed case
          best_dcc_value = 0;
        }
        // rank0 or rank1 have falling edge adjustments (either rank could be 0)
        else if (((local_vars->coarse_schmoo.best_dcc[byte_lane][0]>>4) > 0) || 
                 ((local_vars->coarse_schmoo.best_dcc[byte_lane][1]>>4) > 0))
        {
          best_dcc_value = (((local_vars->coarse_schmoo.best_dcc[byte_lane][0]>>4) + 
                             (local_vars->coarse_schmoo.best_dcc[byte_lane][1]>>4)) / 2) << 4;
        }
        // rank0 or rank1 have rising edge adjustments (either rank could be 0)
        else if ((local_vars->coarse_schmoo.best_dcc[byte_lane][0] != 0) && 
                 (local_vars->coarse_schmoo.best_dcc[byte_lane][1] != 0))
        {
          best_dcc_value = (((local_vars->coarse_schmoo.best_dcc[byte_lane][0] + 
                              local_vars->coarse_schmoo.best_dcc[byte_lane][1]) / 2));
        }
  
        // Write the data structure with the final value
        training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane] = best_dcc_value;

        // Write the DCC_CTL register with the final value
        HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                    DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                    DCC_CTL, 
                    training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);
    }
  }   


} // DDRSS_rd_dqdqs_dcc_schmoo

void DDRSS_RD_perbit_align_phase (DDR_STRUCT *ddr,
                                  uint8 ch,
                                  uint8 cs,
                                  uint8 byte_lane,
                                  uint8 fine_left_start_cdc_value,
                                  training_params_t *training_params_ptr,
                                  uint32 clk_freq_khz,
                                  uint8 phase)
{
    uint32 dq0_ddr_phy_base            = 0;
    uint8  bit                         = 0;
    uint8  loopcnt                     = 0; 

    uint8 *compare_result;  

    uint8  perbit                                      = 0;
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};     
    uint8 perbit_cdc[NUM_DQ_PCH][PINS_PER_PHY]         = {{0}};

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    // Start the search at the first coarse fail boundary
    DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                  fine_left_start_cdc_value, 
                                  1, 
                                  1, 
                                  cs);

    // Zero out the fine CDC values before training
    DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                  0, 
                                  0, 
                                  1, 
                                  cs);            

    for(bit = 0; bit < PINS_PER_PHY; bit++)
    {
      perbit_cdc [byte_lane][bit] = 0xF;
    }

  //Set the perbit values to align Phase 0 to the left edge (CDC=0)
    for(perbit = 0; perbit <= PERBIT_CDC_MAX_VALUE; perbit++)
    {
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
            // Sweep all of the perbit at once
            DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           bit, 
                                           0,   // 0 for RX.
                                           cs, 
                                           perbit);
        }
        
        // Clear the compare results
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
          compare_result_acc[byte_lane][bit]  = 0x0;
        }

        for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
        {
            compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, byte_lane, phase);
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                // compare results are in PHY order
                compare_result_acc [byte_lane][bit] += compare_result [(byte_lane*PINS_PER_PHY) + bit];  
            }
        }
        for (bit = 0; bit < PINS_PER_PHY; bit++)
        {
          //ddr_printf(DDR_NORMAL,"    Byte = %d Bit = %d Perbit = %d result = %d\n",
          //             byte_lane,bit,perbit,compare_result_acc[byte_lane][bit]);
            // Find the perbit setting where all iterations fail
            if ((compare_result_acc[byte_lane][bit] == training_params_ptr->rd_dqdqs.max_loopcnt) && 
                                                       (perbit<perbit_cdc[byte_lane][bit]))
            { 
                // perbit_cdc is in PHY order
                if (perbit != 0)
                {
                  perbit_cdc[byte_lane][bit] = perbit - 1;
                }
                else
                {
                  perbit_cdc[byte_lane][bit] = perbit; 
                }
            }
        }
    } // perbit loop

    // Populate the PHY per-bit lanes with the trained values
    for (bit = 0; bit < PINS_PER_PHY; bit ++)
    {
        // Write the perbit values into the PHY SLICE registers including the gaps (unused)
        DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                    bit, 
                                    0,   // 0 for RX.
                                    cs, 
                                    perbit_cdc[byte_lane][bit]);
   
        // Store the perbit values into the data structure including the gaps (unused)
        training_data_ptr->results.rd_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = perbit_cdc[byte_lane][bit];
  
        ddr_printf(DDR_NORMAL,"    Byte %d Phase %d PHY bit %d DCC Perbit = %d\n",byte_lane, phase,bit,perbit_cdc[byte_lane][bit]);
    }
    ddr_printf(DDR_NORMAL,"\n");
}// DDRSS_RD_perbit_align_phase


void DDRSS_RD_sweep_DQS_DCC (DDR_STRUCT *ddr,
                             uint8 ch,
                             uint8 cs,
                             uint8 byte_lane,
                             uint8 fine_left_start_cdc_value,
                             training_params_t *training_params_ptr,
                             ddrss_rdwr_dqdqs_local_vars *local_vars,
                             uint32 clk_freq_khz,
                             uint8 phase)
{

  uint32 dq0_ddr_phy_base   = 0;
  uint8  dcc_ctl            = 0;
  uint8  loopcnt            = 0;
  uint8  compare_result_byte = 0;

  uint8 dq_max_dcc[NUM_DQ_PCH] = {0x0};

  uint8 *compare_result;  

  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

  dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

  // Set background pattern in the dcc histogram to max_fail + 1
  memset(local_vars->coarse_schmoo.coarse_dq_dcc_info, 
         training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
         (NUM_DQ_PCH * NUM_CS * RX_DCC ));
      
    // Set the RD CDC Coarse to fine_left edge and Fine = 0
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                  fine_left_start_cdc_value, 
                                  0x1,  // 1 for coarse_delay
                                  0x1, 
                                  cs);   
 
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                  0, 
                                  0x0, // 0 for fine_delay
                                  0x1, 
                                  cs);   
        
    // Sweep the RD DCC and record the pass/fail histogram for phase 1
    for (dcc_ctl=0;dcc_ctl<RX_DCC;dcc_ctl++)
    {
        // Set the RD DCC
        HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                    DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                    DCC_CTL, 
                    dcc_ctl<<(phase * 4));

        compare_result_byte = 0;
      
      // Read-back and compare. Fail info returned on a byte_lane basis, in an array of 4.
      for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
      {
          compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, phase);
          compare_result_byte += compare_result[byte_lane];
      }
      
      // Post the error count into the histogram
          local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl] = 
                      compare_result_byte; 
  
    } // dcc_ctl 
    
    //Print DCC Histogram
      ddr_printf(DDR_NORMAL, "\n    RD DQS DCC Correction \n");
      ddr_printf(DDR_NORMAL, "    RD Byte %d : ",byte_lane);
      for (dcc_ctl=0;dcc_ctl<=9;dcc_ctl++)
      {
        ddr_printf(DDR_NORMAL,"%d ",local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl]);
      }
      ddr_printf(DDR_NORMAL,"\n"); 

  // Find the RD DCC adjustment with the first pattern pass at CDC = 0
  // Subtract 1 from the RD DCC adjustment to derive the last failing adjustment
    dq_max_dcc[byte_lane] = RX_DCC;

    // Sweep the RD DCC
    for (dcc_ctl = 0; dcc_ctl < RX_DCC; dcc_ctl++) 
    {
      if ((local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl] == 0) && (dcc_ctl < dq_max_dcc[byte_lane]))
      {
        dq_max_dcc[byte_lane] = dcc_ctl;
      }
    }  

    // Write the data structure with the final value
    local_vars->coarse_schmoo.best_dcc[byte_lane][cs]         = (dq_max_dcc[byte_lane] == 0) ? 0 : (dq_max_dcc[byte_lane]-1);
    training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane] = local_vars->coarse_schmoo.best_dcc[byte_lane][cs]<<(phase * 4);
  
    // Write the DCC_CTL register with the final value
    HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                DCC_CTL, 
                training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);                              

    ddr_printf(DDR_NORMAL, "    Byte %d CS %d RD_DCC 0x%x\n\n",
                 byte_lane,cs, training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);
  
} //  DDRSS_RD_sweep_DQS_DCC 

void DDRSS_RD_phase_schmoo (DDR_STRUCT *ddr,
                            uint8 ch,
                            uint8 cs,
                            uint8 byte_lane,
                            training_params_t *training_params_ptr,
                            ddrss_rdwr_dqdqs_local_vars *local_vars,
                            uint32 clk_freq_khz
                           )
{
  uint32 dq0_ddr_phy_base    = 0;
  uint8  compare_result_byte = 0;
  uint8 coarse_cdc_value     = 0;
  uint8 fine_cdc_value       = 0;
  uint8 loopcnt              = 0;
  uint8 index                = 0;
  uint8 index_stop           = 0;
  uint8 phase                = 0;

  uint8 *compare_result;  

  // Set the PHY DQ base address
  dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

  // One dimensional shmoo of the CDC for each phase
  for (phase=0; phase<2; phase++)
  {
      // CDC Schmoo loop
      for(coarse_cdc_value  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; 
          coarse_cdc_value <= training_params_ptr->rd_dqdqs.coarse_cdc_max_value; 
          coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
      {
          for(fine_cdc_value = training_params_ptr->rd_dqdqs.fine_cdc_start_value; 
              fine_cdc_value < FINE_STEPS_PER_COARSE; 
              fine_cdc_value += training_params_ptr->rd_dqdqs.fine_cdc_step)
          {
              // Set the RD DCC to 0
              HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                          DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                          DCC_CTL, 
                          0);
         
              // Sweep the coarse and fine CDCs
              DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                          coarse_cdc_value, 
                                          0x1, 
                                          0x1, 
                                          cs);   
 
              DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                          fine_cdc_value, 
                                          0x0, 
                                          0x1, 
                                          cs);   // 0 for fine_delay_mode.
              
              compare_result_byte = 0;
              
              index = ((coarse_cdc_value * FINE_STEPS_PER_COARSE) + fine_cdc_value);
              
              // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.
              for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
              {
                  compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, phase);
                  // If fail, increment error_count.
                  compare_result_byte += compare_result[byte_lane];
              }
              
              local_vars->coarse_schmoo.coarse_dq_dcc_schmoo[byte_lane][cs][phase][index] = 
                          compare_result_byte; 

              if (compare_result_byte == 0)
                break;
          } // fine_cdc_value.
          if (compare_result_byte == 0)
            break;
      } // CDC_LOOP

      // Save the final count
      index_stop = index;

      //Print DCC Histogram
        ddr_printf(DDR_NORMAL,"\n");
        ddr_printf(DDR_NORMAL,"    Byte %d CS %d Phase %d Left Edge Fine: ",byte_lane,cs,phase);
        for(index = 0; index < index_stop; index++)
        {
            ddr_printf(DDR_NORMAL,"%d,", local_vars->coarse_schmoo.coarse_dq_dcc_schmoo[byte_lane][cs][phase][index]);
        }
  } // phase Loop
  ddr_printf(DDR_NORMAL,"\n\n");
} //DDRSS_RD_phase_schmoo


void DDRSS_RD_left_edge_search  (DDR_STRUCT *ddr, 
                                 uint8 ch, 
                                 uint8 cs, 
                                 training_data *training_data_ptr,
                                 training_params_t *training_params_ptr,
                                 best_eye_struct *best_eye_ptr) 
{
    uint8 coarse_cdc_value                  = 0;
    uint8 byte_lane                         = 0;
    uint8 loopcnt                           = 0;
    uint8 byte_lane_done                    = 0;
    uint8 *compare_result;  
    uint8 compare_result_acc[NUM_DQ_PCH]    = {0};  
    uint8 byte_lane_found[NUM_DQ_PCH]       = {0};
    uint8 byte_lane_edge[NUM_DQ_PCH]        = {0};
    uint32 dq0_ddr_phy_base                 = 0;

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
     
    // Initialize the fail flag and best cdc
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        best_eye_ptr[byte_lane].all_fail_flag = 0;  // Assume all sane values to begin with.
    }
    // CDC Schmoo loop
    for(coarse_cdc_value  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; 
        coarse_cdc_value <= training_params_ptr->rd_dqdqs.coarse_cdc_max_value; 
        coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
    {
       // Update RD CDC
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
            DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                          coarse_cdc_value, 
                                          0x1, 
                                          0x1, 
                                          cs);   
            compare_result_acc[byte_lane] = 0;
        }

        // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
        for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt; loopcnt++) 
        {
            compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x0);

            // Accumulate the compare results per byte
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
              compare_result_acc[byte_lane] += compare_result[byte_lane];
            }
        }
         
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
        { 
          // If pass, capture left edge
          if(compare_result_acc[byte_lane] == 0)
          {
            if (byte_lane_found[byte_lane] == 0)
            {
              byte_lane_done++;
              byte_lane_found[byte_lane]++;
              byte_lane_edge[byte_lane] = coarse_cdc_value;
            }
            if (byte_lane_done == NUM_DQ_PCH)
            {
              break;
            }
          }
        } // byte_lane 
    } // CDC_LOOP 
        
    // Calculate the left boundary of the eye
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
      best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value = byte_lane_edge[byte_lane];

      ddr_printf(DDR_NORMAL, "    RD Byte %d Coarse Left Edge: %d \n",
                byte_lane,
                best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value);

    } // byte_lane
    ddr_printf(DDR_NORMAL, "\n");
}//DDRSS_RD_left_edge_search
/* This API performs read scaling based on the last trained frequency*/
void Scale_rd_dqdqs_lpddr3 (  DDR_STRUCT *ddr, 
                                        uint8 ch,
                                        uint8 cs,
                                        uint8 training_ddr_freq_indx,
                                        uint32 trained_clk_freq_khz, 
                                        uint8  scale_start_prfs_index)
{
    uint8 byte_lane                              =  0;
    uint8 clk_idx                                =  0;
    uint32 dq0_ddr_phy_base                      =  0;
    uint8 scale_prfs_indx                        =  0; 
    int   scale_freq_indx                        =  0; 
    int32 scale_offset_in_ps[NUM_DQ_PCH]        =  {0};
    uint32 scale_center_in_ps                    =  0;  
    uint32 scale_rd_coarse_cdc                   =  0;  
    uint32 scale_rd_fine_cdc                     =  0;
    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) //calculate scale offset bytewise
    {
        scale_offset_in_ps[byte_lane] = ((training_data_ptr->results.rd_dqdqs.coarse_cdc[training_ddr_freq_indx][ch][cs][byte_lane] * COARSE_STEP_IN_PS)
                                      +
                                      (training_data_ptr->results.rd_dqdqs.fine_cdc[training_ddr_freq_indx][ch][cs][byte_lane] * FINE_STEP_IN_PS))
                                      -
                                      ((1000000000 / trained_clk_freq_khz)/4); // determine the offset for scaling based on the last trained values.
    }
    clk_idx = ddr->misc.ddr_num_clock_levels - 1;//max clock frequency index (corresponding to 932 MHz).
    for(scale_prfs_indx = scale_start_prfs_index; scale_prfs_indx >= SCALING_STOP_PRFS; scale_prfs_indx--)//Scale all the scaling bands
    {
        //Determine the scaling frequency in khz
        for(scale_freq_indx = clk_idx; scale_freq_indx >= 0; scale_freq_indx--)
        {
            if (ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz <= freq_range[scale_prfs_indx ])
                break;
        }
        ddr_printf(DDR_NORMAL,"\n  Scaling frequency = %u , prfs_level = %d\n",ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz,
                                                                scale_prfs_indx);
        clk_idx = scale_freq_indx;
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
        {
            scale_center_in_ps = scale_offset_in_ps[byte_lane]
                                 +
                                 ddrss_lib_uidiv(ddrss_lib_uidiv(1000000000 , ddr->misc.clock_plan[scale_freq_indx].clk_freq_in_khz),4);
            scale_rd_coarse_cdc = ddrss_lib_uidiv(scale_center_in_ps ,COARSE_STEP_IN_PS);
            scale_rd_fine_cdc   = ddrss_lib_uidiv(ddrss_lib_modulo(scale_center_in_ps, COARSE_STEP_IN_PS), FINE_STEP_IN_PS); 
            DDR_PHY_hal_cfg_cdcext_slave_rd ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                              cs,
                                              scale_rd_coarse_cdc,
                                              1,//coarse,
                                              HP_MODE,
                                              scale_prfs_indx 
                                              );
            DDR_PHY_hal_cfg_cdcext_slave_rd ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                             cs,
                                             scale_rd_fine_cdc,
                                             0,//fine,
                                             HP_MODE,
                                             scale_prfs_indx
                                             );
            ddr_printf(DDR_NORMAL,"    RD Byte %u Data Eye Center Coarse = %d Fine = %d \n",
                   byte_lane,
                   scale_rd_coarse_cdc,
                   scale_rd_fine_cdc);   
        }
              
    }
    
}
//End of read scaling

