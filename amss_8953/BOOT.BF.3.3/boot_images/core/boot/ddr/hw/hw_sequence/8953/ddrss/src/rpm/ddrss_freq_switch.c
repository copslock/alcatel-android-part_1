/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/rpm/ddrss_freq_switch.c#2 $
$DateTime: 2016/05/18 04:11:19 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/07/14   ejose        Initial version
================================================================================*/

#include "ddrss.h"


//================================================================================================//
// DDR Pre-Clock Frequency Switch
//================================================================================================//
boolean HAL_DDR_Pre_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                  uint32 new_clk_khz)
{

   uint8 new_clk_idx = 0;
   uint8 ch = 0;
   uint8 ddrcc_target_pll = 0;

   // get the clk index in clock plan
   for (new_clk_idx = 0; new_clk_idx < ddr->misc.ddr_num_clock_levels; new_clk_idx++)
   {
      if (new_clk_khz <= ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz)
         break;
   }

   BIMC_Pre_Clock_Switch (ddr, channel, curr_clk_khz, new_clk_khz, new_clk_idx);

   // Check if the clock mode is DDRCC to configure DDRCC PLLs
   if (ddr->misc.clock_plan[new_clk_idx].clk_mode == 1)
   {
      // Turn on cfg clock before accessing DDRCC CSRs
      ddr_external_set_ddrss_cfg_clk (TRUE);

      for (ch = 0; ch < NUM_CH; ch++)
      {
         if ((channel >> ch) & 0x1)
         {
            ddrcc_target_pll =  ! (HWIO_INXF (ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET,
                                   DDR_CC_DDRCC_PLLCTRL_STATUS, ACTIVE_PLL));

            // Execute target-specific sequence to program the PLLs in the DDRCC.
            // Target-specific code, as DDRCC PLL programming characteristics and CSRs
            // are different across targets. Targets 8992 and 8994 are the same, 8996 is different.
            // Sequence is in target_config.c
            DDR_CC_Pre_Clock_Switch(ddr, ch, new_clk_idx, ddrcc_target_pll);
         }
      }
   }

   return TRUE;
}

//================================================================================================//
// DDR Post-Clock Frequency Switch
//================================================================================================//
boolean HAL_DDR_Post_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                   uint32 new_clk_khz)
{
   uint8 new_clk_idx = 0;
   uint8 curr_clk_idx = 0;
   uint8 ch = 0;
#ifdef DSF_LPDDR3_TMRS  
   uint8 ch_inx  = 0;
   uint8 cs = 0;
   DDR_CHANNEL ch_1hot = DDR_CH_NONE;
   DDR_CHIPSELECT cs_1hot  = DDR_CS0;
#endif  
   BIMC_Post_Clock_Switch (ddr,channel,curr_clk_khz,new_clk_khz);
#ifdef DSF_LPDDR3_TMRS
   if ( ddr->misc.misc_cfg[1] == 1) {   
      ch_inx = CH_INX (channel);
      if ((DDR_MANUFACTURES)ddr->cdt_params[ch_inx].common.manufacture_name == SAMSUNG) {         //checks if its SAMSUNG device. If yes perform the SS testmode sequence.
        for (ch = 0; ch < NUM_CH; ch++)  {
          ch_1hot = CH_1HOT(ch);
          if ((channel >> ch) & 0x1)
          {
            for (cs = 0; cs < NUM_CS; cs++)  {
              cs_1hot = CS_1HOT(cs);
              if ((((DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect) >> cs) & 0x1)
              {             
                DRAM_TestMode_SAMSUNG( ddr, ch_1hot, cs_1hot);      
              }
            }
          }
        }
      }
   }   
#endif
   // get the clk index in clock plan
   for (new_clk_idx = 0; new_clk_idx < ddr->misc.ddr_num_clock_levels; new_clk_idx++)
   {
      if (new_clk_khz <= ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz)
         break;
   }

   for (curr_clk_idx = 0; curr_clk_idx < ddr->misc.ddr_num_clock_levels; curr_clk_idx++)
   {
      if (curr_clk_khz <= ddr->misc.clock_plan[curr_clk_idx].clk_freq_in_khz)
         break;
   }

   // Check if the new clock mode is DDRCC
   if (ddr->misc.clock_plan[new_clk_idx].clk_mode == 1)
   {
       // If cfg clock was turned on, turn off cfg clock
       ddr_external_set_ddrss_cfg_clk (FALSE);
   }   

   for (ch = 0; ch < NUM_CH; ch++)
   {
      if ((channel >> ch) & 0x1)
      {
         // Check for a switch from DDRCC to GCC mode
         if ((ddr->misc.clock_plan[curr_clk_idx].clk_mode == 1) && (ddr->misc.clock_plan[new_clk_idx].clk_mode  == 0 ) && (HWIO_INXF (ddr->base_addr.ddrss_base_addr+REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PLLCTRL_STATUS, CLK_SWITCH_SM) != 0))
         {
            // Run MC clock to DDRCC for 1us to get DDRCC FSM to go to IDLE state
            HWIO_OUTXFI (ddr->base_addr.bimc_base_addr+REG_OFFSET_GLOBAL0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC, ch, CLKEN_DDRCC_ENA, 1);
            HWIO_OUTXFI (ddr->base_addr.bimc_base_addr+REG_OFFSET_GLOBAL0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC, ch, CLKEN_DDRCC_SET, 1);
            while (TRUE)
            {
                 if(HWIO_INXF (ddr->base_addr.ddrss_base_addr+REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PLLCTRL_STATUS, CLK_SWITCH_SM) == 0)
                    break;
               
            }
//            seq_wait(1, US);
            HWIO_OUTXFI (ddr->base_addr.bimc_base_addr+REG_OFFSET_GLOBAL0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC, ch, CLKEN_DDRCC_ENA, 0);
            HWIO_OUTXFI (ddr->base_addr.bimc_base_addr+REG_OFFSET_GLOBAL0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_SW_CLKEN_DDRCC, ch, CLKEN_DDRCC_SET, 0);	
         }   
      }
   }

   return TRUE;
}






