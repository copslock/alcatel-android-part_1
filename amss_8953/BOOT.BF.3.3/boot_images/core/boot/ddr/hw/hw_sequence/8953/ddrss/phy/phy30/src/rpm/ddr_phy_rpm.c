//===========================================================================
//  Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
//  QUALCOMM Proprietary and Confidential. 
//===========================================================================
////////////////////////////////////////////////////////////////////////////////////////////////

#include "ddrss.h"
#include "ddr_phy.h"

void DDR_PHY_hal_hpcdc_enable(DDR_STRUCT *ddr, uint8 enable)
{
    uint32   reg_ddrss_base = 0;

    reg_ddrss_base = ddr->base_addr.ddrss_base_addr;

   // Enable broadcast mode for all DQ and CA PHYs on both channels
    HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),	
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CAsDQs);  //write to both CA and DQ
    HWIO_OUTXF2 (reg_ddrss_base + BROADCAST_BASE, DDR_PHY_DDRPHY_CMCDCMSTR_TOP_CFG, HP_EN, LP_EN, enable, ~enable);
   // Disable broadcast mode for all DQ and CA PHYs on both channels
    HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), 
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE);
     while ( HWIO_INX ((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),    
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER) != PHY_BC_DISABLE);
}
