#ifndef __DDR_PHY_CONFIG_H__
#define __DDR_PHY_CONFIG_H__


/*==============================================================================
                      Warning: This file is auto-generated
================================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddrss.h"
#include "ddr_common.h"


/*==============================================================================
                                  DATA
==============================================================================*/
extern uint32 (*ddr_phy_dq_config_base)[2];
extern uint32 (*ddr_phy_ca0_config_base)[2];
extern uint32 (*ddr_phy_ca1_config_base)[2];
extern uint32 (*ddr_cc_config_base)[2];

extern uint32 (*ddr_phy_dq_config_delta)[2];
extern uint32 (*ddr_phy_ca0_config_delta)[2];
extern uint32 (*ddr_phy_ca1_config_delta)[2];
extern uint32 (*ddr_cc_config_delta)[2];


/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
void ddr_phy_dq_set_config
(
  DDR_STRUCT *ddr,
  uint32 offset,
  uint32 (*ddr_phy_dq_config_base)[2], uint32 (*ddr_phy_dq_config_delta)[2]
);

void ddr_phy_ca0_set_config
(
  DDR_STRUCT *ddr,
  uint32 offset,
  uint32 (*ddr_phy_ca0_config_base)[2], uint32 (*ddr_phy_ca0_config_delta)[2]
);

void ddr_phy_ca1_set_config
(
  DDR_STRUCT *ddr,
  uint32 offset,
  uint32 (*ddr_phy_ca1_config_base)[2], uint32 (*ddr_phy_ca1_config_delta)[2]
);
void ddr_cc_set_config
(
  DDR_STRUCT *ddr,
  uint32 offset,
  uint32 (*ddr_cc_config_base)[2], uint32 (*ddr_cc_config_delta)[2]
);


#endif
