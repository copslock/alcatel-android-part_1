/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2015, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/src/ddrss_common.c#3 $
$DateTime: 2016/07/18 22:34:40 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
================================================================================*/

#include "ddrss.h"
#include "target_config.h"
#include "string.h" 

extern uint8  bit_remapping_bimc2phy_DQ[NUM_CH][NUM_DQ_PCH][PINS_PER_PHY_CONNECTED_NO_DBI];

/*Freq ranges:               250000,    350000,    600000,    800000,    1066000,   1488000,   1603000,   1894000. */
uint32 PRFS_index_table[] = {F_RANGE_0, F_RANGE_1, F_RANGE_2, F_RANGE_3, F_RANGE_4, F_RANGE_5, F_RANGE_6, F_RANGE_7};

//================================================================================================//
// Get the flat 32-bit system address to which training patterns will be written to and read from.
// Adjust address for execution from RPM if needed.
// ===============================================================================================//
void DDRSS_Get_Training_Address(DDR_STRUCT *ddr)
{

    // Adjust if needed for RPM view of system DRAM address map.
    training_address[0][0] = (uint32)(ddr->ddr_size_info.ddr0_cs0_remapped_addr) + TRAINING_BASE_ADDRESS_OFFSET;

    training_address[0][1] = (uint32)(ddr->ddr_size_info.ddr0_cs1_remapped_addr) + TRAINING_BASE_ADDRESS_OFFSET;

    training_address[1][0] = (uint32)(ddr->ddr_size_info.ddr1_cs0_remapped_addr + TRAINING_BASE_ADDRESS_OFFSET +
                             ((ddr->cdt_params[0].common.interleave_en & DDR_CS0_INTERLEAVE) ? 
                                 TRAINING_INTERLEAVE_ADDRESS_OFFSET 
                              : 
                                 0));

    training_address[1][1] = (uint32)(ddr->ddr_size_info.ddr1_cs1_remapped_addr + TRAINING_BASE_ADDRESS_OFFSET +
                             ((ddr->cdt_params[0].common.interleave_en & DDR_CS1_INTERLEAVE) ? 
                                 TRAINING_INTERLEAVE_ADDRESS_OFFSET 
                             : 
                                 0)); 

}
//================================================================================================//
// Get an index based on Frequency provided.
//================================================================================================
uint8 DDRSS_Get_Freq_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 prfs_index;

   for (prfs_index = 0; (prfs_index < sizeof(PRFS_index_table)/sizeof(uint32)); prfs_index++)
   {
      if (clk_freq_khz < PRFS_index_table[prfs_index])
         break;
   }

   return prfs_index;
}

//================================================================================================//
// Get Frequency based on PRFS index.
//================================================================================================//
uint32 DDRSS_Get_Freq_Range (DDR_STRUCT *ddr, uint32 prfs_index)
{
   uint32 f_range;

        f_range = PRFS_index_table[prfs_index];

   return f_range;
}
//================================================================================================//
// Common Retimer calculation
//================================================================================================//

uint8 DDRSS_Retimer_Calc( DDR_STRUCT *ddr, uint8 coarse_delay, uint8 fine_delay, uint32 period)
{
    uint32 retimer=1;
        uint32 coarse_dqs_delay_ps;
        uint32 fine_dqs_delay_ps;
        uint32 T4;
        uint32 T34;
        
      //T4  =      period / 4;
        T4  =      ddrss_lib_uidiv (period, 4);
      //T34 = (3 * period) / 4;
        T34 = (3 * T4);

        coarse_dqs_delay_ps = coarse_delay * COARSE_STEP_IN_PS;
        fine_dqs_delay_ps   = fine_delay   * FINE_STEP_IN_PS;

        // Increment the retimer when the delay exceeds the T/4 period boundaries
        if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T4) {
              //retimer = 0;
                retimer = retimer_map[0];
        }
        else if (((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T4) && ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T34)) {
                retimer  = retimer_map[1];
        }
        else if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T34) {
                retimer  = retimer_map[2];
        }    
    return ((retimer));
}

//================================================================================================//
// Memory read from DRAM.
// This function will read data of a given size to a given address supplied by the caller.
//================================================================================================//
void ddr_mem_copy(uint32 * source, uint32 * destination, uint32 size, uint32 burst)
{
    uint32 i;

    if(burst)
    {
        /* perform a burst write */
        #ifndef CDVI_SVTB
        __blocksCopy(source, destination, size);
        #endif
    }
    else
    {
        for(i = 0; i < size; i++)
        {
            destination[i] = source[i];
        }
    }
}

//================================================================================================//
// CDC retimer  
//================================================================================================//
uint8 DDRSS_CDC_Retimer (DDR_STRUCT *ddr,
                        uint8 cs,
                        uint8 coarse_dqs_delay, 
                        uint8 fine_dqs_delay, 
                        uint8 coarse_wrlvl_delay, 
                        uint8 fine_wrlvl_delay, 
                        uint32 cadq_ddr_phy_base,
                        uint32 clk_freq_khz
                       )
{ 
   uint8             dq_retmr = 0;
   uint32 coarse_fine_dqs_delay_ps = 0;
   uint32              period = 0;
   uint32                  T4 = 0;
   uint32                 T34 = 0;

   // Determine the Clock period normalized to ps
   // period = 1000000000/clk_freq_khz;
   period = ddrss_lib_uidiv(1000000000, clk_freq_khz);
   
   // Determine the period boundaries (T/4 and 3T/4)
   T4  = ddrss_lib_uidiv(period , 4);
   T34 = (T4 * 3);
   
   coarse_fine_dqs_delay_ps = (coarse_dqs_delay + coarse_wrlvl_delay) * COARSE_STEP_IN_PS + 
                              (  fine_dqs_delay +   fine_wrlvl_delay) *   FINE_STEP_IN_PS;

   // Increment the retimer when the delay exceeds the T/4 period boundaries
   if      ((coarse_fine_dqs_delay_ps) <=  T4) {  dq_retmr  = retimer_map[0]; } 
   else if ((coarse_fine_dqs_delay_ps) >  T34) {  dq_retmr  = retimer_map[2]; }
   else                                        {  dq_retmr  = retimer_map[1]; }

   // Update the re-timer registers at T/4 boundaries
   //Write wrlvl delay values back to each PHY (non-broadcast)
   DDR_PHY_hal_cfg_ca_dq_retmr (cadq_ddr_phy_base, 
                                        cs,
                                        dq_retmr);
   return dq_retmr;
}



	
// Divide and remainder calculation functions for WRLVL CDC conversion
uint32 calc (uint32 step, uint32 i_delay, uint8 set_rem_out)
{
   uint32 div_rem = 0;

   // Divide or Mod depending on set_rem_out decode
   if ((step <= i_delay) && (step != 0)) {
      // Mod delay by step
      if (set_rem_out == 1) {
        // div_rem   = (i_delay % step);
        div_rem = i_delay - (ddrss_lib_uidiv(i_delay, step) * step);
      }
      else {
         // Divide delay by step
//       div_rem   = (i_delay / step);
         div_rem   = ddrss_lib_uidiv(i_delay, step);
         
         if (set_rem_out == 2) {
             div_rem = div_rem  * step;
         }
      }
   }
   // Bypass the result if illegal
   else {
       if (set_rem_out == 1) {
           div_rem = i_delay;
       }
   }

   // Return the result
   return div_rem;
}

// Optimize Coarse and Fine CDC, Half, and Retimer values based on trained results
void DDRSS_dqs_convert (wrlvl_params_struct *wrlvl_params_ptr, uint32 wrlvl_delay, uint32 period)
{

    //  Calculation Constants
    uint8  opt_unit_cdcc   = COARSE_STEP_IN_PS; // CDC coarse unit delay (ps)
    uint8  opt_unit_cdcf   = FINE_STEP_IN_PS;   // CDC fine unit delay (ps)
    uint8  opt_step_cdcc   = 25;                // Number of CDC coarse delay steps

    // Calculation variables
    uint16 dqs_wrlvl_delay;
    uint16 dqs_wrlvl_full_setting;
    uint16 dqs_wrlvl_half_rem_delay;
    uint16 dqs_wrlvl_half_setting;
    uint16 dqs_wrlvl_cdcc_setting;
    uint16 dqs_wrlvl_cdcc_rem_delay;
    uint16 dqs_wrlvl_cdcf_setting;
    uint16 dqs_wrlvl_cdc_delay;
    uint16 dqs_retimer_sel;
    uint16 t2;
    uint16 t34;
    uint16 t4;

    //--------------------------------------------------------------------
    //CDC Calculations
    //--------------------------------------------------------------------

     // Calculate t/4 and 3t/4
  //t2  = (period / 2);
    t2  = ddrss_lib_uidiv(period, 2);
  //t4  = period / 4;
    t4  = ddrss_lib_uidiv(period, 4);
    t34 = 3 * t4;

    // Do not calculate full setting due to CDC range requirement
    dqs_wrlvl_full_setting = 0;

    // Trained WRLVL skew relative to CK 
    dqs_wrlvl_delay = wrlvl_delay;

    // Calculate WRLVL CDC Half Delay
    dqs_wrlvl_half_setting   = calc(t2,dqs_wrlvl_delay,0x0);
    dqs_wrlvl_half_rem_delay = calc(t2,dqs_wrlvl_delay,0x1);

    // Correct half setting if greater than one
    if (dqs_wrlvl_half_setting > 1) {
       dqs_wrlvl_half_rem_delay += (dqs_wrlvl_half_setting - 1) * t2;
       dqs_wrlvl_half_setting = 1;
    }

    // Calculate WRLVL CDC Coarse Settings
    dqs_wrlvl_cdcc_setting   = calc(opt_unit_cdcc,dqs_wrlvl_half_rem_delay,0x0);
    dqs_wrlvl_cdcc_rem_delay = calc(opt_unit_cdcc,dqs_wrlvl_half_rem_delay,0x1);

    // Check WRLVL CDC coarse setting against the maximum
    if (dqs_wrlvl_cdcc_setting > opt_step_cdcc) {
       dqs_wrlvl_cdcc_setting = opt_step_cdcc;
       dqs_wrlvl_cdcc_rem_delay = dqs_wrlvl_half_rem_delay - (opt_step_cdcc * opt_unit_cdcc);
    }

    // Calculate WRLVL Fine setting
    dqs_wrlvl_cdcf_setting   = calc(opt_unit_cdcf,dqs_wrlvl_cdcc_rem_delay,0x0);

    // Round up fine delay if the a half delay is used and the coarse CDC remainder 
    //is greater than the fine CDC unit
    if ((dqs_wrlvl_half_setting == 1) && (dqs_wrlvl_cdcc_rem_delay > 0)) {
       dqs_wrlvl_cdcf_setting++;
    }

    // Calculate the WRLVL CDC delay
    dqs_wrlvl_cdc_delay = (dqs_wrlvl_cdcc_setting * opt_unit_cdcc) + 
                          (dqs_wrlvl_cdcf_setting * opt_unit_cdcf);

    // Determine Optimal Retimer Settings
    if (dqs_wrlvl_cdc_delay <= t4) {
     //dqs_retimer_sel = 0;
       dqs_retimer_sel = retimer_map[0];
    } 
    else if ((dqs_wrlvl_cdc_delay >  t4) && (dqs_wrlvl_cdc_delay <= t34)) {
       dqs_retimer_sel = retimer_map[1];
    } 
    else {
       dqs_retimer_sel = retimer_map[2];
    } 

    // Push the converted values into the wrlvl_delay struct
    wrlvl_params_ptr->dqs_retmr        = dqs_retimer_sel;
    wrlvl_params_ptr->coarse_dqs_delay = dqs_wrlvl_cdcc_setting;
    wrlvl_params_ptr->fine_dqs_delay   = dqs_wrlvl_cdcf_setting;
    wrlvl_params_ptr->dqs_full_cycle   = dqs_wrlvl_full_setting;
    wrlvl_params_ptr->dqs_half_cycle   = dqs_wrlvl_half_setting;
    
} // DDR_PHY_dqs_convert


void DDRSS_ddr_phy_sw_freq_switch (DDR_STRUCT *ddr, uint32 clk_freq_khz, uint8 ch)
{

    uint32   reg_offset_ddr_phy = 0;
    uint32   reg_ddrss_base = 0;
    uint8 sw_handshake_complete = 1;

    // Configure the DDR PHY address offset
    reg_offset_ddr_phy = ddr->base_addr.ddrss_base_addr + REG_OFFSET_DDR_PHY_CH(ch);
    reg_ddrss_base = ddr->base_addr.ddrss_base_addr;

    // Enable broadcast mode for 4 DQ PHYs and 2 CA PHYs
    HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, ALL_CAsDQs << (ch * 7));

    /// set FPM_INIT_START to 1 to start frequency switch
    DDR_PHY_hal_cfg_sw_handshake_start ((reg_ddrss_base + BROADCAST_BASE), clk_freq_khz);

    /// Poll for DDRPHY_FPM_TOP_STA[FPM_SW_INIT_COMP]
    while (sw_handshake_complete == 0x1) {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// set FPM_INIT_START to 0 to stop frequency switch
    DDR_PHY_hal_cfg_sw_handshake_stop ((reg_ddrss_base + BROADCAST_BASE), clk_freq_khz);

    // Poll for handshake complete
    while (sw_handshake_complete == 0x0) {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// disable software trigger handshake, and enable hardware FPM
    DDR_PHY_hal_cfg_sw_handshake_complete (reg_ddrss_base + BROADCAST_BASE);

    // Disable broadcast mode
    HWIO_OUTX((reg_ddrss_base + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, PHY_BC_DISABLE);
}

void DDRSS_passing_region_scan( uint8 *start, uint8 *stop, uint8 fail_count_histogram[HISTOGRAM_SIZE])
{
	uint8 lenght_cur = 0;
	uint8 start_cur = 0;
	uint8 stop_cur = 0;
	uint8 lenght_stop = 0;
	uint8 sweep;

	*stop = 0;
	*start = 0;
	
	for(sweep = 1; sweep < HISTOGRAM_SIZE - 1; sweep++)
  {
		if((fail_count_histogram[sweep - 1] != 0) && (fail_count_histogram[sweep] == 0))
    {
			start_cur = sweep;
			stop_cur = sweep;
			lenght_cur = 0;
		}
		if((fail_count_histogram[sweep] == 0) && (fail_count_histogram[sweep + 1] != 0))
    {
			stop_cur = sweep;
			lenght_cur = stop_cur - start_cur + 1;
		}
		if((sweep == HISTOGRAM_SIZE - 2) && (fail_count_histogram[sweep] == 0) && (fail_count_histogram[sweep + 1] == 0))
    {
			stop_cur = sweep + 1;
			lenght_cur = stop_cur - start_cur + 1;
		}
		if(lenght_cur > lenght_stop)
    {
			lenght_stop = lenght_cur;
			*start = start_cur;
			*stop = stop_cur;
		}
	}
}

