/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/bimc.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"
#include "ddrss.h"

//================================================================================================//
// LPDDR3 Device Initialization
// Does device initialization steps of enabling CK and CKE sequentially
//================================================================================================//
void BIMC_Memory_Device_Init_Lpddr3 (DDR_STRUCT *ddr, DDR_CHANNEL channel ,DDR_CHIPSELECT chip_select)
{
   uint8 ch      = 0;
   uint8 ch_1hot = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < NUM_CH; ch++)  
   {
      ch_1hot = CH_1HOT(ch);
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         // Ensure CKE OFF for 100ns(2 tck) before reset_n de-asserts
         // CKE is off, set CKE off again to trigger timer, so it's just a fake timer.
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x02);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));

         // Turn CK on and wait 5tck
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x05);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_ON));

         // Turn CKE on and keep 200 us
         // Convert tINIT3 to XO clock cycle(0.052us)
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0xF06);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

         // Reset manual_1 timer
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x00);

         // Reset MRW, reset value is 0 at boot freq(10~55MHz), 0xFC at high freq
         BIMC_Extended_MR_Write (ddr, (DDR_CHANNEL)ch_1hot, chip_select, 0x3F/*Addr=MR63*/, 0xFC/*opcode*/);

         // Wait tINIT4 1us after MRW reset
         // Convert tINIT4 to XO clock cycle(0.052us)
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x14);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

         // Reset manual_1 timer
         BIMC_Wait_Timer_Setup (ddr, (DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x00);

      }
   }
}

//================================================================================================//
// Enter Deep Power Down
//================================================================================================//
boolean BIMC_Enter_Deep_Power_Down (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint32 reg_offset_dpe = 0;
   uint32 reg_offset_shke = 0;
   uint8  ch = 0;
   for (ch = 0; ch < NUM_CH; ch++) 
   {
	  reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      if ((channel >> ch) & 0x1)
      {
	     //Disable HW self refresh for the ranks to be placed in DPD. This will trigger a SR exit if the ranks were in SR state.
         BIMC_HW_Self_Refresh_Ctrl (ddr, channel, chip_select, 0);
		 // Check that the ranks not in self refresh state.
         if (chip_select & DDR_CS0) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_0_SR) == 1);
         }
         if (chip_select & DDR_CS1) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_1_SR) == 1);
         }
		 //SW then issues the manual DPD command to the appropriate ranks. If both ranks need to be placed in DPD mode, it needs to be done sequentially
		 if (chip_select & DDR_CS0) {
			HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ENTER_DEEP_PD, DDR_CS0, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ENTER_DEEP_PD));
		 }
		 if (chip_select & DDR_CS1) {
			HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ENTER_DEEP_PD, DDR_CS1, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ENTER_DEEP_PD));
		 }
		 //Ensure that the specific rank is in deep power down
		 if (chip_select & DDR_CS0) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_0_DPD) == 0);
         }
         if (chip_select & DDR_CS1) {
            while (HWIO_INXF (reg_offset_dpe, DPE_DRAM_STATUS_0, RANK_1_DPD) == 0);
         }
		 //Disable the CKE to the rank which entered DPD
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF) == 1);
		 //Disable the CK to the rank which entered DPD
		 HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_OFF, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_OFF) == 1);
		 // clear rank init complete signal
         if (chip_select & DDR_CS0) {
            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 0);
         }
         if (chip_select & DDR_CS1) {
            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 0);
         }
		 // Disable SHKE Rank
		  if (chip_select & DDR_CS0) {
            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_EN, 0);
         }
         if (chip_select & DDR_CS1) {
            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_EN, 0);
         }		 
      }
   }
   return TRUE;
}

//================================================================================================//
// Exit Deep Power Down
//================================================================================================//
boolean BIMC_Exit_Deep_Power_Down (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint32 reg_offset_shke = 0;
   uint8  ch = 0;
   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_shke = ddr->base_addr.bimc_base_addr + REG_OFFSET_SHKE(ch);
      if ((channel >> ch) & 0x1)
      {
	     //SW then issues the manual DPD exit command to the appropriate ranks. If both ranks, need to be exited from DPD mode, it needs to be done sequentially
		 if (chip_select & DDR_CS0) {
			HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXIT_DEEP_PD, DDR_CS0, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXIT_DEEP_PD));
		 }
		 if (chip_select & DDR_CS1) {
			HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXIT_DEEP_PD, DDR_CS1, 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXIT_DEEP_PD));
		 }		  
      }
   }
   return TRUE;
}

