/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/hw_sequence/8953/ddrss/bimc/mc230/src/rpm/bimc_freq_switch_lpddr3.c#1 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"
#include "ddrss.h"

//================================================================================================//
// BIMC PRE CLOCK SWITCH SEQUENCE for No-ZQCAL option-R1
//================================================================================================//
boolean BIMC_Pre_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                               uint32 new_clk_khz, uint8 new_clk_idx)
{
   uint32 reg_offset_dpe     = 0;
   uint32 reg_offset_global0 = 0;
   uint32 period             = 0;
   uint8  rl                 = 0;
   uint8  wl                 = 0;
   uint8  mr2_wr_val         = 0;
   uint8  ch                 = 0;
   uint8 new_RL_WL_idx       = 0;
   uint8  new_params_idx     = 0;
   uint8  MR3_wrdata         = 0;

   uint32* dpe_config_8_mapped; 

   //Map to the current active processor address map for accessibility
   dpe_config_8_mapped = (ddr_external_shared_addr((uint32 *)&dpe_config_8));

   new_params_idx  = BIMC_Freq_Switch_Params_Index (ddr, new_clk_khz);
   MR3_wrdata = ddr->extended_cdt_runtime.bimc_freq_switch[new_params_idx].MR3;

   new_RL_WL_idx  = BIMC_RL_WL_Freq_Index (ddr, new_clk_khz);
   rl         = ddr->extended_cdt_runtime.dram_latency[new_RL_WL_idx].RL;
   wl         = ddr->extended_cdt_runtime.dram_latency[new_RL_WL_idx].WL;
   mr2_wr_val = ddr->extended_cdt_runtime.dram_latency[new_RL_WL_idx].MR2;

// period = 1000000000 / new_clk_khz; //unit in ps
   period = ddrss_lib_uidiv(1000000000, new_clk_khz); 

   reg_offset_global0 = ddr->base_addr.bimc_base_addr + REG_OFFSET_GLOBAL0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      reg_offset_dpe = ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch);
      if ((channel >> ch) & 0x1)
      {
         HWIO_OUTXFI (reg_offset_global0, BIMC_MISC_GLOBAL_CSR_DDR_CHN_CLK_PERIOD, ch, PERIOD, period);
         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_16, WR_LATENCY, RD_LATENCY, wl, rl);
         HWIO_OUTXF2 (reg_offset_dpe, DPE_MRW_AFTER_FREQ_SWITCH_0, W_ADDR, W_DATA, JEDEC_MR_2, mr2_wr_val);
         //Update DRAM ROUT as per the new frequency in MR3
         HWIO_OUTXF2 (reg_offset_dpe, DPE_MRW_AFTER_FREQ_SWITCH_1, W_ADDR, W_DATA, JEDEC_MR_3, MR3_wrdata);
         HWIO_OUTX   (reg_offset_dpe, DPE_MRW_FREQ_SWITCH, 0x00000003);  
         
         //update DPE CONFIG_8 based on new_freq_index
         HWIO_OUTX (reg_offset_dpe, DPE_CONFIG_8, dpe_config_8_mapped[new_clk_idx]);
	  }
   }

   return TRUE;
}


//================================================================================================//
// BIMC POST CLOCK SWITCH SEQUENCE
//================================================================================================//
boolean BIMC_Post_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel,uint32 curr_clk_khz ,uint32 new_clk_khz)
{
   uint8 ch = 0;

   for (ch = 0; ch < NUM_CH; ch++) 
   {
      if ((channel >> ch) & 0x1)
      {
         //Poll on the status bit to track MRW_AFTER_FREQ_SWITCH completion
		 while (HWIO_INXF (ddr->base_addr.bimc_base_addr + REG_OFFSET_DPE(ch), DPE_MEMC_STATUS_0, FREQSW_IN_PROGRESS));
      }
   }
   
   return TRUE;
}



