/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/msm8917/ddr_target.c#14 $
$DateTime: 2016/03/17 05:12:32 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
03/01/16   sk      CA Training changes
01/25/16   sk      DDR Training changes
12/30/15   sk      Gen1 PHY DQ read and write training changes
05/27/15   aus     Update WR CDC delay and CA CDC delay for 533 and 400
01/22/15   yps     Use memsmove instead of memmove
12/15/14   xl      Add ddr parameters enhance set
02/24/14   tw      Updated update clock period function pointer
02/09/14   tw      added support for common ddr library
01/24/14   tw      initial port for 8916
                   added support for sbl\rpm shared ddr driver
09/05/13   sr      changes to not support training in boot.
06/20/13   rj      Added support for lpddr2 on GIMLI
06/06/13   rj      Updated 8926 delta settings and added function
                   related to ddr draining required.
05/13/13   sl      Removed ddr_apply_delta_settings().
04/10/13   sl      Use voltage mode instead of voltage number.
04/03/13   sl      Added macros for vddmx voltage levels.
03/01/13   tw      Added stubs for applying delta settings
02/21/13   sl      Initial version.
================================================================================
                      Copyright 2013-2014, 2015-2016 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_params.h"
#include "ddr_internal.h"
#include "ddr_target.h"
#include "ddr_config.h"
#include "ddr_log.h"
#include "DDIChipInfo.h"
#include "BIMC.h"
#include "DDR_PHY.h"
#include "ddr_func_tbl.h"
#include "DDIChipInfo.h"
#include "mpm_hwio.h"
#include <stringl/stringl.h>
#include "boot_extern_clk_interface.h"
#include "ddr_phy_training.h"
#include "msmhwioreg.h"
#include "boot_error_if.h"

/*==============================================================================
                                  MACROS
==============================================================================*/
#define ONE_TIME_TRAINING TRUE

/*==============================================================================
                                  DATA
==============================================================================*/
uint32 ddr_fmax = 0;

/* Initial DDR PHY CDC delay */
__attribute__((section("ddr_func_ptr"))) uint32 DDR_PHY_INIT_CDC_DELAY;

__attribute__((section("ddr_func_ptr"))) ddr_func ddr_func_tbl;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/

/* =============================================================================
**  Function : ddr_target_init
** =============================================================================
*/
/**
*   @brief
*   Function called at the end of ddr_init() to initialize target specific
*   configurations.
*
*   @param  None
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_target_init()
{
  struct ddr_shared_params *header;
  
  extern uint32 ddr_bimc_config_MSM8x17_LPDDR3[][2];
  extern uint32 ddr_phy_config_MSM8x17_LPDDR3[][2];
  extern uint32 ddr_caphy_config_MSM8x17_LPDDR3[][2];
  extern uint32 ddr_dqphy_config_MSM8x17_LPDDR3[][2];
  
  //if (DalChipInfo_ChipFamily() == DALCHIPINFO_ID_MSM8916)
  {
    if (ddr_get_params(SDRAM_INTERFACE_0)->common.device_type == DDR_TYPE_LPDDR3)
    {
      ddr_bimc_config_delta = ddr_bimc_config_MSM8x17_LPDDR3;
      ddr_phy_config_delta = ddr_phy_config_MSM8x17_LPDDR3;
      ddr_caphy_config_delta = ddr_caphy_config_MSM8x17_LPDDR3;
      ddr_dqphy_config_delta = ddr_dqphy_config_MSM8x17_LPDDR3;
    }
    DDR_PHY_INIT_CDC_DELAY = 0x1A4;
  }
  
  /* initialization of function tables for shared ddr library 
   * taking into account RPM view of shared library is different 
   */
  ddr_func_tbl.enter_self_refresh = (void (*)(uint32, SDRAM_INTERFACE, SDRAM_CHIPSELECT, ddr_wait))
                                    ((uint8 *)&HAL_SDRAM_Enter_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.exit_self_refresh = (void (*)(uint32, SDRAM_INTERFACE, SDRAM_CHIPSELECT, ddr_wait))
                                   ((uint8 *)&HAL_SDRAM_Exit_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  
  ddr_func_tbl.get_read_latency = (uint32 (*) (DDR_TYPE , uint32 )) 
                                  ((uint8 *)&HAL_SDRAM_RL - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.get_write_latency = (uint32 (*) (DDR_TYPE , uint32 )) 
                                   ((uint8 *)&HAL_SDRAM_WL - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_update_clock_period = (void (*)(uint32, uint32, ddr_divide )) 
                                          ((uint8 *)&HAL_SDRAM_BIMC_Update_Clock_Period - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_dpe_pre_clock_switch = (void (*)(uint32 , SDRAM_INTERFACE , uint32 , uint32 )) 
                                           ((uint8 *)&HAL_SDRAM_DPE_Pre_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_dpe_post_clock_switch = (void (*)(uint32 , SDRAM_INTERFACE , uint32 , uint32 )) 
                                            ((uint8 *)&HAL_SDRAM_DPE_Post_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  
  ddr_func_tbl.phy_manual_io_cal = (void (*)(uint32 , SDRAM_INTERFACE , ddr_wait )) 
                                   ((uint8 *)&HAL_SDRAM_PHY_Manual_IO_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_cdc_config = (void (*)(uint32 , SDRAM_INTERFACE , uint32 )) 
                                    ((uint8 *)&HAL_SDRAM_PHY_Update_CDC_Config - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_cdc_config_training = (void (*)(uint32 , SDRAM_INTERFACE , uint32 , uint32 , uint32)) 
                                    ((uint8 *)&HAL_SDRAM_PHY_Update_CDC_Config_Training - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_manual_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                    ((uint8 *)&HAL_SDRAM_PHY_Manual_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_drive_strength = (void (*) (uint32 ,SDRAM_INTERFACE, uint32 , uint32 , uint32 )) 
                                           ((uint8 *)&HAL_SDRAM_PHY_Update_Drive_Strength - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_cdc_delay = (void (*)(uint32 , SDRAM_INTERFACE , boolean read, uint32 cdc_delay, uint32 voltage)) 
                                      ((uint8 *)&HAL_SDRAM_PHY_Update_CDC_Delay_Target - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_enable_rapid_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                          ((uint8 *)&HAL_SDRAM_PHY_Enable_Rapid_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_disable_rapid_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                           ((uint8 *)&HAL_SDRAM_PHY_Disable_Rapid_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  
   
  /* clear out shared header and fill in with updated information */
  header = ddr_get_shared_header();
  memset(header, 0, sizeof(struct ddr_shared_params));
  
  header->ddr_func_tbl_ptr = (void *) ((uint8 *)&ddr_func_tbl - SCL_RPM_CODE_RAM_BASE);
  header->size = sizeof(ddr_func);
} /* ddr_target_init */

/* =============================================================================
**  Function : ddr_post_init
** =============================================================================
*/
/**
*   @brief
*   Function called after DDR is initialized for DDR training if required.
*
*   @param  None
*
*   @retval  TURE   DDR training required
*   @retval  FALSE  DDR training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_post_init()
{
  volatile uint32 * ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;

  if(*ddr_training_cookie == DDR_TRAINING_NOT_REQUIRED)
  {
    return FALSE;
  }
  else
  {
    return ((*ddr_training_cookie) == DDR_TRAINING_DATA_UPDATED);
  }

} /* ddr_post_init */

/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
#if (!defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED)) && (!defined(FEATURE_RUMI_BOOT))

  struct ddr_params_partition * ddr_training_partition; 
  uint32 size;
  
  ddr_training_partition = ddr_params_get_training_data(&size);
  
  if(ddr_training_partition == NULL)
	  return TRUE;
  
  if(ddr_training_partition->version != DDR_TRAINING_PARAM_VERSION)
  {
    return TRUE;
    /* parameter version not matched, force retrain */
  }

  return FALSE;
#else
  return FALSE;
#endif
} /* ddr_params_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. 
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
#if (!defined(FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED)) && (!defined(FEATURE_RUMI_BOOT))
  return FALSE;
#else
  return TRUE;
#endif
} /* ddr_params_is_training_on_rpm_required */

/* ============================================================================
**  Function : ddr_phy_training
** ============================================================================
*/
/*!
*   @brief
*   This function will train the ddr phy at the highest frequency supported.
*
*   @details
*   This function will train the ddr phy at the highest frequency supported.
*   the rest of frequencies will be interpolated.
*
*   @param interface_name     -  [IN] the interface to train for
*   @param chip_select        -  [IN] the chip select to train for
*   @param training_params    -  [IN/OUT] pointer to training parameter to
*                                         populate trained results.
*
*   @par Dependencies
*
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
void ddr_phy_training(SDRAM_INTERFACE interface_name,
    SDRAM_CHIPSELECT chip_select,
    union ddr_trained_params *training_params)
{   
  uint32 ddr_params_training_data_size = 0;
  
  ddr_params_get_training_data(&ddr_params_training_data_size);  

  /* PLL 0 */
  /* Configure BIMC clock period */
  HAL_SDRAM_BIMC_Update_Clock_Period(0, ddr_fmax, ddr_divide_func);
  HAL_SDRAM_DPE_Pre_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);     
  boot_clock_set_bimcspeed(ddr_fmax);
  HAL_SDRAM_DPE_Post_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);
  
  BIMC_Disable_All_Periodic(0, interface_name, chip_select); 
  
  
  HAL_SDRAM_ddr_phy_ca_training( 0, /* sequence is expecting driver to send zero for phy_base, they calculate the right offsets */
                 interface_name,
                 chip_select,
                 8,                                   /*max_loop_count*/                 
                 0xA,                                   /*coarse_init_val*/                 
				 &(training_params->lpddr3_training.phy_cdc_params.ca_training),
                 ddr_fmax/*pClockPlan[nNumLevel-2].nFreqKHz*/); 
  
  DDRSS_ddr_phy_dq_rd_training_ddrss( 0,
                 interface_name,
                 chip_select,
                 8,	/*max_loop_count*/
				 0xA,	/*coarse_init_val*/
                 training_params->lpddr3_training.phy_cdc_params.dq_read_training,                 
                 ddr_fmax/*pClockPlan[nNumLevel-2].nFreqKHz*/ );

  DDRSS_ddr_phy_dq_wr_training_ddrss( 0,
              interface_name,
		      chip_select,		      
		      8,	/*max_loop_count*/
		      0xA,	/*coarse_init_val*/		      
		      training_params->lpddr3_training.phy_cdc_params.dq_write_training,
		      ddr_fmax /*pClockPlan[nNumLevel-2].nFreqKHz*/ );
			  
  training_params->lpddr3_training.voltage_checksum = 0;
    
  BIMC_Enable_All_Periodic(0, interface_name, chip_select);
}

/* ============================================================================
**  Function : ddr_training_is_required
** ============================================================================
*/
/*!
*   @brief
*   This function will parse the magic cookie and clock plan to determine
*   if the runtime configuration has changed since the last time we trained
*   and if it has return TRUE for training required.
*   Otherwise return FALSE for no training required.
*
*   @details
*   This function will parse the magic cookie and clock plan to determine
*   if the runtime configuration has changed since the last time we trained
*   and if it has return TRUE for training required.
*   Otherwise return FALSE for no training required.
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  boolean - Training required/Not required
*
*   @sa None
*/
boolean ddr_training_is_required(uint32 clk_plan[])
{
  uint32 * ddr_shared_imem_ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;
  union ddr_trained_params *training_params;
  struct ddr_params_partition * ddr_training_partition;
  struct ddr_device_params_common *ddr_params_ch0;
  uint32 size;
  uint32 serial_number;
  
  uint16 railway_checksum_vddcx = 0;
  uint16 railway_checksum_vddmx = 0;
  uint16 rbcpr_checksum = 0;  
  uint16 recalculated_checksum;
  uint32 i; 
  
  /* Get DDR Training parameters */
  training_params = (ddr_get_trained_params(SDRAM_INTERFACE_0));
  
  ddr_params_ch0 = &(ddr_get_params(SDRAM_INTERFACE_0)->common);
  
  if(training_params == NULL)
  {
    /* training parameters not found. */
    return TRUE;
  }

  /* Cookie indicates training is required, check training partition to see if
   * magic cookie is set
   */
  
  if(ddr_get_training_cookie() != DDR_PARAMS_MAGIC_NUM)
  {
    /* cookie does not match training parameter cookie */
    /* Training is required */
    return TRUE;
  }
  
  ddr_training_partition = ddr_params_get_training_data(&size);
  
  if(ddr_training_partition->version != DDR_TRAINING_PARAM_VERSION)
  {
    return TRUE;
    /* parameter version not matched, force retrain */
  }
 
  /* Check checksum for training data to see if training is required */
  serial_number = BOOT_HWIO_IN(QFPROM_RAW_SERIAL_NUM , 0);
  if(ddr_training_partition->checksum != ddr_get_training_checksum(serial_number))
  {
	return TRUE;
  }
  
  /*Check clock plan, if clock plan changes DDR training is required.*/
   for(i = 0 ;i < DDR_FREQ_MAX_NUM_LEVELS; i++)
 	{
 	  if( (clk_plan[i] != training_params->lpddr3_training.freq[i]))
 	  {
 	    /* if frequency does not match what we have trained,
 	     * retraining is required.
 	    */
 	    return TRUE;
 	  }    
 	}
	
  /* Check cookie to see if training is required */
  if(*ddr_shared_imem_ddr_training_cookie != DDR_TRAINING_REQUIRED)
  {
    return FALSE;
  }

/* Recalculate cx/mx checksum to determine if training is required */
  recalculated_checksum = railway_checksum_vddcx ^ railway_checksum_vddmx  ^ rbcpr_checksum;
  if(ddr_params_ch0->device_type == DDR_TYPE_LPDDR3){
	if(recalculated_checksum != training_params->lpddr3_training.voltage_checksum){
		return TRUE;
	}
  }

  return FALSE;
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   function called to do the DDR PHY training 
*
*   @param  None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_do_phy_training( void )
{
  SDRAM_CHIPSELECT chip_select_ch0;
  struct ddr_device_params_common *ddr_params_ch0;
  uint32          nNumLevel=0x0;
  uint32 serial_number;
  ClockPlanType   *pClockPlan;
  union ddr_trained_params *ddr_params;
  int32 i = 0;  
  
#if ONE_TIME_TRAINING
  //uint32 next_freq =0;
  //uint32 clk_mode;
  volatile uint32 * ddr_shared_imem_ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;
  uint32 clk_plan[DDR_FREQ_MAX_NUM_LEVELS] = {0};
  int32 j ;
#endif

//  uint32 clk_plan[DDR_FREQ_MAX_NUM_LEVELS] = {0};
  
  struct ddr_params_partition * ddr_training_partition;
  uint32 size = 0;
  ddr_training_partition = ddr_params_get_training_data(&size);
  
  
  /* Get DDR device parameters */
  ddr_params_ch0 = &(ddr_get_params(SDRAM_INTERFACE_0)->common);

  if(ddr_params_ch0 == NULL)
  {
    return TRUE;
  }

  /* Call BIMCQuery to get the Num Perf Levels */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);

  /* Call BIMCQuery to get the Clock Plan */
  boot_query_bimc_clock(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);

  if ( pClockPlan == NULL ) 
  {
    return TRUE;
  }
  else 
  {
    ddr_fmax = pClockPlan[nNumLevel-1].nFreqKHz;    
  }

#if ONE_TIME_TRAINING
  i = nNumLevel-2;
  j = DDR_FREQ_MAX_NUM_LEVELS-1; // max clock freq supported by phy is 7 .
  while((i >= 0) && (j >= 0))
 	{
 	  clk_plan[j] = pClockPlan[i+1].nFreqKHz;
     /* if(clk_plan[j] == 268800)
      {
        next_freq = j+1;
      }*/
 	  i--;j--;
 	}
  /*Check if legacy to jcpll threshold is correct in LUT*/
  /*clk_mode = Get_BIMC_LUT (SDRAM_INTERFACE_0, 537600, 5);
  BL_VERIFY(clk_mode == 0, BL_ERR_INVALID_CLOCK_PLAN|BL_ERROR_GROUP_DDR);
  clk_mode = Get_BIMC_LUT (SDRAM_INTERFACE_0, clk_plan[next_freq], 5);
  BL_VERIFY(clk_mode == 1, BL_ERR_INVALID_CLOCK_PLAN|BL_ERROR_GROUP_DDR);*/

#endif

  chip_select_ch0 = SDRAM_CS_NONE;
  if (ddr_params_ch0->num_banks_cs0 != 0)
  {
    chip_select_ch0 |= SDRAM_CS0;
  }
  if (ddr_params_ch0->num_banks_cs1 != 0)
  {
    chip_select_ch0 |= SDRAM_CS1;
  }

#if ONE_TIME_TRAINING
  if(!ddr_training_is_required(clk_plan)) {             
	 BIMC_Disable_All_Periodic(0, SDRAM_INTERFACE_0, chip_select_ch0); 
	 
     HAL_SDRAM_BIMC_Update_Clock_Period(0, ddr_fmax, ddr_divide_func);
     HAL_SDRAM_DPE_Pre_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);
     Clock_SetBIMCSpeed(ddr_fmax);
     HAL_SDRAM_DPE_Post_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax); 

     boot_log_message("DDR Training restore, Start");
     boot_log_start_timer();
     DDRSS_EBI1_PHY_Set_Training_Data(SDRAM_INTERFACE_0, ddr_training_partition->trained_params[0]);
     boot_log_stop_timer("DDR Training restore, Delta");	 
     
	 HAL_SDRAM_BIMC_Update_Clock_Period(0, ddr_fmax, ddr_divide_func);
     HAL_SDRAM_DPE_Pre_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);
     Clock_SetBIMCSpeed(ddr_fmax);
     HAL_SDRAM_DPE_Post_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);
     
	 BIMC_Enable_All_Periodic(0, SDRAM_INTERFACE_0, chip_select_ch0);     
     DALSYS_Free(pClockPlan);
     return TRUE;
  }
#endif

  //reset ddr training params partition
  memset(ddr_training_partition, 0, size);
  
  ddr_params = ddr_get_trained_params(SDRAM_INTERFACE_0);

  BL_VERIFY(ddr_params != NULL, BL_ERR_DDR_NULL_TRAINED_PARAMS|BL_ERROR_GROUP_DDR);
  
   i = DDR_FREQ_MAX_NUM_LEVELS-1;
  ddr_params->lpddr3_training.nlevels = DDR_FREQ_MAX_NUM_LEVELS;
  while(i>=0)
  {
    ddr_params->lpddr3_training.freq[i] = clk_plan[i];
    i--;
  }
  
  if(chip_select_ch0 != SDRAM_CS_NONE)
  {
    boot_log_message("DDR Training for Channel 0, Start");
    boot_log_start_timer();
    ddr_phy_training(SDRAM_INTERFACE_0, chip_select_ch0, ddr_params);
    boot_log_stop_timer("DDR Training for Channel 0, Delta");	 
  }
  
  /* update training cookie */
  ddr_set_training_cookie(DDR_PARAMS_MAGIC_NUM);
  
  serial_number = BOOT_HWIO_IN(QFPROM_RAW_SERIAL_NUM , 0);
  /* update training data checksum */
  ddr_set_training_checksum(serial_number);

//#if 1
#if ONE_TIME_TRAINING
  #ifdef FEATURE_DDR_PARAM_SAVE_TO_EMMC
        *ddr_shared_imem_ddr_training_cookie = DDR_TRAINING_DATA_UPDATED;
  #else
        *ddr_shared_imem_ddr_training_cookie = DDR_TRAINING_DATA_NOT_UPDATED;      
  #endif
#endif

  ddr_training_partition->version = DDR_TRAINING_PARAM_VERSION;

  HAL_SDRAM_BIMC_Update_Clock_Period(0, ddr_fmax, ddr_divide_func);
  HAL_SDRAM_DPE_Pre_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax);     
  Clock_SetBIMCSpeed(ddr_fmax);
  HAL_SDRAM_DPE_Post_Clock_Switch(0, SDRAM_INTERFACE_0, 0, ddr_fmax); 
  
  DALSYS_Free(pClockPlan);

  return TRUE;

} /* ddr_do_phy_training */


/* ============================================================================
**  Function : ddr_remapper
** ============================================================================
*/
/**
*   @brief
*   remap DDR base addresses
*                
*
*   @return
*   None
*
*   @dependencies
*  None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_remapper(void)
{
  
} /* ddr_remapper */

/* =============================================================================
**  Function : ddr_preinit_rpm
** =============================================================================
*/
/**
*   @brief
*   function called to initialize DDR wrapper in RPM
*
*   @retval  NONE
*
*   @dependencies
*   Needs to be called only after all the DDR subsystem register read/write
*   sequences are completed in DDR driver in SBL 
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_preinit_rpm( void )
{

} /* ddr_preinit_rpm */
