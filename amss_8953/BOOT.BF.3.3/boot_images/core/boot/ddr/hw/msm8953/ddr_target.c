/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/msm8953/ddr_target.c#30 $
$DateTime: 2016/05/24 06:36:50 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/16/16   sc      Updated DDR_CC regiters list for status register dump feature
04/06/16   sc      Enable PWM mode for vdd_px1 before DDR training
01/28/16   sc      Disable training for deviceprogrammer
19/01/16   sc      Disable QSEE reset in case of One time training
12/22/15   sk      Enable PWM mode before training
09/02/15   sc      Shared driver changes
06/20/14   tw      added ddr_pre_init api to capture any target specific 
                   workarounds that needs to be applied prior to ddr init
05/28/14   tw      cleaned up sbl <-> ddr driver dependencies around ddr training
                   implementation of cx\mx\cpr hash to force retraining
03/12/14   sr      Initial version.
================================================================================
                   Copyright 2014-2015 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/

/*==============================================================================
                                  INCLUDES
==============================================================================*/

#include "bimc_seq_hwioreg.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HAL_SNS_DDR.h"
#include "ddr_config.h"
#include "boot_extern_clk_interface.h"
#include "icbcfg.h"
#include "ddrss_init_sdi.h"
#include "boot_error_if.h"
#include "boot_extern_bus_interface.h"

#include "msmhwiobase.h"
#include "msmhwioreg.h"
#include <stddef.h>
#include "bimc.h"
#include "HALhwio.h"
#include "pm.h"
#include "ddr_external.h"
#include "ddr_func_tbl.h"
#include "boot_extern_power_interface.h"
#include "boot_extern_pmic_interface.h"

#define ONE_TIME_TRAINING TRUE
#define DISABLE_TRAINING  FALSE
#define DISABLE_FREQ_SWITCH  FALSE
/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF

/*==============================================================================
                                  DATA
==============================================================================*/

extern ddr_func ddr_func_tbl;

/* DDR interface status, keeps track of active interfaces and their status */
extern ddr_interface_state ddr_status;

extern uint32 ddr_bus_width;

/* DDR Partition information */
ddr_size_partition ddr_size_partition_info;

uint32 msm_revision = DDR_TLMM_HW_REV_ID_V10;

extern ddr_info ddr_physical_size;
extern ddr_size_info ddr_system_size;

/* BIMC/PHY Status register addresses to be dumped during pre/post clock switch or abort */
/* Make sure to update marker 0xFAEEFAEE at the end of bimc registers */

uint32 ddr_status_reg_addr[] = {
0xFAEEFAEE,
HWIO_DDR_CC_DDRCC_PLLCTRL_CLK_SWITCH_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_PLLCTRL_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_DLLCTRL_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_TXPHYCTRL_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_TXMCCTRL_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_PLL0_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_PLL1_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_TX0_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_TX1_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_TXPHY_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_DLL0_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_DLL1_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL0_RESET_SM_READY_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL0_CORE_VCO_TUNE_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL0_CORE_KVCO_CODE_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL1_RESET_SM_READY_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL1_CORE_VCO_TUNE_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
HWIO_DDR_CC_DDRCC_SDPLL1_CORE_KVCO_CODE_STATUS_ADDR(SEQ_DDR_SS_CH0_DDR_CC_OFFSET),
NULL
};  // end of the array with status register addresses .

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
/* ============================================================================
**  Function : ddr_target_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called at the end of ddr init to initialize any
*   target specific configurations
*
*   @details
*   Target specific configurations such as override of timing registers and
*   calling target specific init functions will be captured in this api
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_target_init()
{
  uint32 chip_plat_ver_info;
  struct ddr_shared_params *header;
  
  ddr_bus_width = 32;
  
  /* extract upper 16 bits which store the BOOT major and minor version */
  ddrsns_share_data->misc.boot_rpm_version = (BOOT_MAJOR_VERSION << 24) | (BOOT_MINOR_VERSION << 16); 
  
  chip_plat_ver_info =  HWIO_IN(TCSR_SOC_HW_VERSION);
  /* extract upper 16 bit and store it in platform field of ddr_misc */
  ddrsns_share_data->misc.platform_id  = chip_plat_ver_info >> 16 /*& 0xFFFF0000*/ ;
  /* extract lower 16 bits and store it in version field of ddr_misc */
  ddrsns_share_data->misc.chip_version  = chip_plat_ver_info & 0x0000FFFF ;
  
 
  /* initialization of function tables for shared ddr library 
   * taking into account RPM view of shared library is different 
   */
  ddr_func_tbl.pre_ddr_clock_switch = (boolean (*)(void *, DDR_CHANNEL , uint32 ,uint32 )) 
                                   ((uint8 *)&HAL_DDR_Pre_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.post_ddr_clock_switch = (boolean (*)(void *, DDR_CHANNEL , uint32 ,uint32 )) 
                                           ((uint8 *)&HAL_DDR_Post_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  										   
  ddr_func_tbl.enter_ddr_power_collapse = (boolean (*)(void *, DDR_CHANNEL )) 
                                   ((uint8 *)&HAL_DDR_Enter_Power_Collapse - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.exit_ddr_power_collapse = (boolean (*)(void *, DDR_CHANNEL )) 
                                           ((uint8 *)&HAL_DDR_Exit_Power_Collapse - SCL_RPM_CODE_RAM_BASE);

  ddr_func_tbl.enter_self_refresh = (boolean (*)(void *, DDR_CHANNEL, DDR_CHIPSELECT )) 
                                   ((uint8 *)&HAL_DDR_Enter_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.exit_self_refresh = (boolean (*)(void *, DDR_CHANNEL, DDR_CHIPSELECT )) 
                                           ((uint8 *)&HAL_DDR_Exit_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.Get_DDR_Struct_Addr = (void* (*)())((uint8 *)&HAL_DDR_Get_DDR_Struct_Addr - SCL_RPM_CODE_RAM_BASE);

  ddr_func_tbl.Get_CDT_Ptr = (void* (*)())((uint8 *)&HAL_DDR_Get_CDT_ptr - SCL_RPM_CODE_RAM_BASE);

  ddr_func_tbl.Get_Status_Reg_Addr = (void* (*)())((uint8 *)&HAL_DDR_Get_Status_Reg_Addr - SCL_RPM_CODE_RAM_BASE);

  /* clear out shared header and fill in with updated information */
  header = ddr_get_shared_header();
  memset(header, 0, sizeof(struct ddr_shared_params));

  header->ddr_func_tbl_ptr = (void *) ((uint8 *)&ddr_func_tbl - SCL_RPM_CODE_RAM_BASE);
  header->size = sizeof(ddr_func);
  
}

/* ============================================================================
**  Function : ddr_pre_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called before ddr is initialized. It will take care of any
*   pre initialization workarounds.
*
*   @details
*   This function is called before ddr is initialized. It will take care of any
*   pre initialization workarounds.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
boolean ddr_pre_init()
{
  uint8 ch_inx;
  /* Override CDT timing parameters here */
  for(ch_inx = 0 ; ch_inx < 2 ; ch_inx++){
		ddrsns_share_data->cdt_params[ch_inx].lpddr.tFC = 2500;
		ddrsns_share_data->cdt_params[ch_inx].lpddr.tVRCG_DISABLE  = 1000;
		ddrsns_share_data->cdt_params[ch_inx].lpddr.tDQS2DQMIN  = 2;
		ddrsns_share_data->cdt_params[ch_inx].lpddr.tDQS2DQMAX  = 8;
  }
  return TRUE;
}


/* ============================================================================
**  Function : ddr_post_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @details
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
boolean ddr_post_init()
{
  #ifndef FEATURE_RUMI_BOOT
  uint32 nNumLevel;
  uint8 i;
  ClockPlanType   *pClockPlan;
  
  /* Call BIMCQuery to get the Num Perf Levels */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);
  
  if ( pClockPlan == NULL ) 
  {
    return FALSE;
  }
  
  /* Call BIMCQuery to get the Clock Plan */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);

  ddrsns_share_data->misc.ddr_num_clock_levels = nNumLevel;	  

  BL_VERIFY( sizeof(ddrsns_share_data->misc.clock_plan) >= (sizeof(ddrsns_share_data->misc.clock_plan[0]) * nNumLevel),
    BL_ERROR_GROUP_DDR | BL_ERR_INVALID_CLOCK_PLAN);

  /* Fill the ddr_misc for use by the DSF SNS driver */
  for(i=0;i<nNumLevel;i++)
  {
     ddrsns_share_data->misc.clock_plan[i].clk_freq_in_khz  = pClockPlan[i].nFreqKHz;
     ddrsns_share_data->misc.clock_plan[i].clk_mode         = !(pClockPlan[i].eMode);
     if(ddrsns_share_data->misc.clock_plan[i].clk_mode == 1)
     {
       ddrsns_share_data->misc.clock_plan[i].pll_dec_start = pClockPlan[i].sDSFConfig.pll_dec_start;
       ddrsns_share_data->misc.clock_plan[i].pll_div_frac_start3 = pClockPlan[i].sDSFConfig.pll_div_frac_start3;
       ddrsns_share_data->misc.clock_plan[i].pll_plllock_cmp1 = pClockPlan[i].sDSFConfig.pll_plllock_cmp1;
       ddrsns_share_data->misc.clock_plan[i].pll_plllock_cmp2 = pClockPlan[i].sDSFConfig.pll_plllock_cmp2;
       ddrsns_share_data->misc.clock_plan[i].pll_vco_count1 = pClockPlan[i].sDSFConfig.pll_vco_count1;
       ddrsns_share_data->misc.clock_plan[i].pll_vco_count2 = pClockPlan[i].sDSFConfig.pll_vco_count2;
       ddrsns_share_data->misc.clock_plan[i].pll_pll_lpf2_postdiv = pClockPlan[i].sDSFConfig.pll_pll_lpf2_postdiv;
       ddrsns_share_data->misc.clock_plan[i].pll_kvco_code = pClockPlan[i].sDSFConfig.pll_kvco_code;
     }
  }
  /* free clock resource */
  DALSYS_Free(pClockPlan);
  #endif
 
   /* Enable any workarounds here */
  ddr_pre_setup_forDSF();
  
  return FALSE;
}

/* ============================================================================
**  Function : ddr_pre_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before clock switching occures.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be stalled and new timing parameter is loaded into shadow.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk   -   [IN] the current clock speed
*   @param new_clk    -  [IN] the clock speed we are switching to
*   @param new_clk    -  [IN] interface to switch clock for
*
*   @par Dependencies
*
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_pre_clock_switch(uint32 curr_clk, uint32 new_clk , SDRAM_INTERFACE interface_name)
{
     HAL_DDR_Pre_Clock_Switch(ddrsns_share_data, DDR_CH0, curr_clk, new_clk);
} /* ddr_pre_clock_switch */

/* ============================================================================
**  Function : ddr_post_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after clock switching occurs.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be unstalled.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk          -  [IN] the current clock speed
*   @param new_clk           -  [IN] the clock speed we are switching to
*   @param interface_name    -  [IN] interface to switch clock for
*
*   @par Dependencies
*   This code has to be on IRAM because ddr is unavailable during clock switching
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_post_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
     HAL_DDR_Post_Clock_Switch(ddrsns_share_data, DDR_CH0, curr_clk, new_clk);
} /* ddr_post_clock_switch */

/* ============================================================================
**  Function : ddr_pre_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_pre_vddmx_switch(uint32 vddmx_microvolts)
{
  /* Stepping Down in VDDCX voltage */
  ddr_status.vddmx_voltage = vddmx_microvolts;
} /* ddr_pre_vddmx_switch */

/* ============================================================================
**  Function : ddr_post_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddmx_switch(uint32 vddmx_microvolts)
{

} /* ddr_post_vddmx_switch */

/* ============================================================================
**  Function : ddr_pre_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before vddcx switch.
*
*   @param settings - contains the VDDCX voltage level we just switched to
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/
void ddr_pre_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_pre_vddcx_switch */

/* ============================================================================
**  Function : ddr_post_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after VDDCX is switched
*
*   @param none
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_post_vddcx_switch */

/* ============================================================================
**  Function : ddr_pre_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right before XO shutdown. Puts DDR into self refresh mode and
*   disables CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_post_xo_shutdown
*/

void ddr_pre_xo_shutdown(uint32 clk_speed)
{
  ddr_enter_self_refresh_all(clk_speed);
} /* ddr_pre_xo_shutdown */

/* ============================================================================
**  Function : ddr_post_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right after XO wakeup. Takes DDR out of self refresh mode and enables
*   CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_pre_xo_shutdown
*/

void ddr_post_xo_shutdown(uint32 clk_speed)
{
  ddr_exit_self_refresh_all(clk_speed);

} /* ddr_post_xo_shutdown */

/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
#ifndef FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED

  #ifdef FEATURE_RUMI_BOOT
     return FALSE;
  #else
    #if !DISABLE_TRAINING
       struct ddr_params_partition * ddr_training_partition; 
       uint32 size;
  
       ddr_training_partition = ddr_params_get_training_data(&size);
  
       if(ddr_training_partition->training_data_size == 0)
	      return TRUE;
       else
          return FALSE;
    #else
       return FALSE;
    #endif
  #endif
#else
  return FALSE;
#endif
} /* ddr_params_is_training_required */


/* ============================================================================
**  Function : ddr_is_training_required
** ============================================================================
*/
/*!
*   @brief
*   This function will parse the crc hash and determine if training is required
*   based on serial number, mx\cx\rbcpr hash
*
*   @details
*   This function will parse the crc hash and determine if training is required
*   based on serial number, mx\cx\rbcpr hash
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  boolean - Training required/Not required
*
*   @sa None
*/
boolean ddr_is_training_required(void)
{
  uint32 serial_number;
    
  /* cx\mx hash variables */
  uint32 combined_checksum = boot_railway_setting_checksum (boot_rail_id("vddcx")) ^ 
                             (boot_railway_setting_checksum (boot_rail_id("vddmx")) << 16) ^ 
                             boot_rbcpr_cx_mx_settings_checksum();
 
  /* compare checksum for training data in our partition with DDR_STRUCT to see if training is required */
  serial_number = BOOT_HWIO_IN(QFPROM_RAW_SERIAL_NUM, 0); 
  
  ddr_printf ( DDR_NORMAL, "DDR: The serial number is %d", serial_number);
  
  /* combine serial number with voltage checksum for a new seed */
  combined_checksum = serial_number ^ combined_checksum;
  
  ddr_printf (DDR_NORMAL, "DDR: Checksum on flash is %d", ddrsns_share_data->flash_params.checksum);
  ddr_printf (DDR_NORMAL, "DDR: Recomputed checksum is %d", ddr_get_training_checksum(combined_checksum));
  
  if (ddrsns_share_data->flash_params.checksum  != ddr_get_training_checksum(combined_checksum)) 
  {
    ddr_printf (DDR_NORMAL, "DDR: Training is required");
    return TRUE;
  }
  
  ddr_printf (DDR_NORMAL, "DDR: Training is not required");
  return FALSE;
} /* ddr_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid.
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
#ifndef FEATURE_BOOT_EXTERN_DEVICEPROGRAMMER_DDR_COMPLETED
  #if defined(FEATURE_RUMI_BOOT)
    return TRUE;
  #else
    #if !DISABLE_TRAINING
      return FALSE;
    #else
      return TRUE;
    #endif
 #endif
#else
  return TRUE;
#endif
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_preinit_rpm
** =============================================================================
*/
/**
*   @brief
*   function called to initialize DDR wrapper in RPM
*
*   @retval  NONE
*
*   @dependencies
*   Needs to be called only after all the DDR subsystem register read/write
*   sequences are completed in DDR driver in SBL 
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_preinit_rpm( void )
{
  uint32 i,j;

  ddrsns_share_data->base_addr.bimc_base_addr = RPM_EBI1_BIMC_BASE;
  ddrsns_share_data->base_addr.ddrss_base_addr = RPM_EBI1_PHY_CFG_BASE;

  for (i = 0; (ddr_status_reg_addr[i] != 0xFAEEFAEE); i++)
  {
    ddrsns_share_data->status_reg_addr[i] = ddr_status_reg_addr[i] + RPM_EBI1_BIMC_BASE;
  }

  for (j = i, ++i; ddr_status_reg_addr[i] != NULL; i++,j++)
  {
    ddrsns_share_data->status_reg_addr[j] = ddr_status_reg_addr[i] + RPM_EBI1_PHY_CFG_BASE;
  }

  ddrsns_share_data->status_reg_addr[j] = NULL;
  BL_VERIFY(j <= (MAX_STATUS_REG - 1), BL_ERR_DDR_ARRAY_OUT_OF_BOUNDS|BL_ERROR_GROUP_DDR);

} /* ddr_preinit_rpm */

/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   Indicates that PHY training needs to be done in SBL1.
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_do_phy_training( void )
{
  uint32 serial_number;
  uint32 combined_checksum = boot_railway_setting_checksum (boot_rail_id("vddcx")) ^ 
                             (boot_railway_setting_checksum (boot_rail_id("vddmx")) << 16) ^ 
                             boot_rbcpr_cx_mx_settings_checksum();
    
   /* Vote for SMPS PWM mode*/
  boot_pm_smps_sw_mode(0, PM_SMPS_2, PM_SW_MODE_NPM);
  boot_pm_smps_sw_mode(0, PM_SMPS_7, PM_SW_MODE_NPM);
  boot_pm_smps_sw_mode(0, PM_SMPS_3, PM_SW_MODE_NPM);
    
#if ONE_TIME_TRAINING
  /* Boot training */
  if(!((ddr_is_training_required() == FALSE)&&
				(HAL_DDR_Boot_Training(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH, DDR_TRAINING_MODE_CHECK) == TRUE)))
#endif 
  {

     /* training data corrupted, memclr training data to force a retraining */
     memset(&ddrsns_share_data->flash_params, 0x0, sizeof(struct ddr_params_partition));
	
     ddr_printf (DDR_NORMAL, "DDR: Start of HAL DDR Boot Training");
 
     HAL_DDR_Boot_Training(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH, DDR_TRAINING_MODE_INIT);
     
     ddr_pre_clock_switch(0, ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz, SDRAM_INTERFACE_0);     
     boot_clock_set_bimcspeed(ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz);
     ddr_post_clock_switch(0, ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz, SDRAM_INTERFACE_0);
  
	 ddr_printf (DDR_NORMAL, "DDR: End of HAL DDR Boot Training");
  }
#if ONE_TIME_TRAINING
  else {
      ddr_pre_clock_switch(0, ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz, SDRAM_INTERFACE_0);
      boot_clock_set_bimcspeed(ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz);
      ddr_post_clock_switch(0, ddrsns_share_data->misc.clock_plan[ddrsns_share_data->misc.ddr_num_clock_levels - 1].clk_freq_in_khz, SDRAM_INTERFACE_0);
      return FALSE;
  }
#endif
 
  serial_number = BOOT_HWIO_IN(QFPROM_RAW_SERIAL_NUM, 0);

  /* combine serial number with voltage checksum for a new seed */
  combined_checksum = serial_number ^ combined_checksum;
  
  /* update training data checksum */
  ddr_set_training_checksum(combined_checksum);
 
  /* update training data and log size */
  ddrsns_share_data->flash_params.training_data_size = sizeof(ddrsns_share_data->flash_params.training_data);
  ddrsns_share_data->flash_params.training_log_size = ddr_external_get_training_log_size();
  
  #if ONE_TIME_TRAINING
     #ifdef FEATURE_DDR_PARAM_SAVE_TO_EMMC
        return TRUE;
     #else
        return FALSE;      
     #endif
  #else
     return FALSE;
  #endif 

} /* ddr_do_phy_training */

/* ============================================================================
**  Function : ddr_pre_setup_forDSF
** ============================================================================
*/
/*!
*   @brief
*   includes any SW workarounds required for HW issues 
*
*   @details
*   includes any SW workarounds required for HW issues 
*
*   @param  None.
*
*   @par Dependencies
*   None
*
*   @par Side Effects
*   None
*
*   @retval
*   None
*
*   @sa None
*/

void ddr_pre_setup_forDSF(void) {

  /* Force Apply the HW self Refresh to SHKE registers */
  ddr_enter_self_refresh(SDRAM_INTERFACE_0, SDRAM_BOTH, 200000);
  ddr_exit_self_refresh(SDRAM_INTERFACE_0, SDRAM_BOTH, 200000);

#if 0
  /* if it is a ver 1 chip & Elessar HW */
  if ( ddrsns_share_data->misc.chip_version == 0x100 && 
       ddrsns_share_data->misc.platform_id  == 0x000B){
       /* if size is > 2GB , then disable Hw Self Refresh */
     if( ddrsns_share_data->ddr_size_info.ddr0_cs0_mb + 
         ddrsns_share_data->ddr_size_info.ddr0_cs1_mb +
         ddrsns_share_data->ddr_size_info.ddr1_cs0_mb +
         ddrsns_share_data->ddr_size_info.ddr1_cs1_mb > 0x800 )		 
     ddrsns_share_data->misc.misc_cfg[0] = 0 ; // disable HW self Refresh 
  }
#endif
  return;
}


/* ============================================================================
**  Function : ddr_set_params_enhance,enhance set
** ============================================================================
*/
/**
*   @brief
*   This function sets the DDR DC timing parameters stored in CDB2 and CDB3.enhance set.
*   For CDB2,this function will directly set the registers of DDR driver .
*   For CDB3,this function will put the data in IMEM for RPM setting.
*   @param[in]  ddr_params_ptr   pointer to the base address of CDB2 or CDB3 in memory
*                
*
*   @return
*   TRUE if the parameters was successfully set.
*
*   @dependencies
*  None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_set_params_enhance(void *ddr_params_ptr, uint32 ddr_params_size)
{
  return TRUE;
} /*ddr_set_params_enhance*/


/* ============================================================================
**  Function : ddr_remapper
** ============================================================================
*/
/**
*   @brief
*   remap DDR base addresses
*                
*
*   @return
*   None
*
*   @dependencies
*  None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_remapper(void)
{
  icbcfg_remap_info_type remap1, remap2;
  ddr_info sbl1_ddr_info;
  bool dual_remap = TRUE;
  bool remap_required = TRUE;

  sbl1_ddr_info = ddr_get_info();
  // fill in remap structure
  remap1.interleaved = ICBCFG_REMAP_INTERLEAVE_DEFAULT;
  remap1.deinterleave = FALSE;
  remap2.interleaved = ICBCFG_REMAP_INTERLEAVE_DEFAULT;
  remap2.deinterleave = FALSE;

  /* Incase of Single channel + dual rank + 1536 MB per each rank.
  Base address to be aligned with size at BIMC and so remapping at buses to fit into memory map
  Address 0x0 is being mapped to 0x6000 0000 for a block of 0.5GB,
  Address 0x2000 0000 is being mapped to 0xE000 0000 for a block of 0.5 GB.
  */
  if ((ddr_physical_size.ddr_size.sdram0_cs0 == 1536) && (ddr_physical_size.ddr_size.sdram0_cs1 == 1536)
             && (ddr_physical_size.ddr_size.sdram1_cs0 == 0) &&  (ddr_physical_size.ddr_size.sdram1_cs1 == 0 )) {
      remap1.src_addr = 0x0;
      remap1.size     = 0x20000000;
      remap1.dest_addr = 0x60000000;

      remap2.src_addr = 0x20000000;
      remap2.size     = 0x20000000;
      remap2.dest_addr = 0xE0000000;
  }
  /* No remap required for for 4GB*/
  else if ((ddr_physical_size.ddr_size.sdram0_cs0 == 2048) && (ddr_physical_size.ddr_size.sdram0_cs1 == 2048)
             && (ddr_physical_size.ddr_size.sdram1_cs0 == 0) &&  (ddr_physical_size.ddr_size.sdram1_cs1 == 0 )) {
     remap_required = FALSE;
  }
  // Case of 2GB + 1GB
  else if ((ddr_physical_size.ddr_size.sdram0_cs0 == 2048) && (ddr_physical_size.ddr_size.sdram0_cs1 == 1024)
             && (ddr_physical_size.ddr_size.sdram1_cs0 == 0) &&  (ddr_physical_size.ddr_size.sdram1_cs1 == 0 )) {
      remap1.src_addr = 0x0;
      remap1.size     = 0x40000000;
      remap1.dest_addr = 0xC0000000;
      dual_remap = FALSE;
  }
  else if (ddr_physical_size.ddr_size.sdram0_cs0 + ddr_physical_size.ddr_size.sdram0_cs1 <= 2048){
      remap1.src_addr = 0x0;
      remap1.dest_addr = 0x80000000;
      // Incase of discontiguous memory address allocations
      if ((ddr_physical_size.ddr_size.sdram0_cs1_addr - ddr_physical_size.ddr_size.sdram0_cs0_addr) > 
          (ddr_physical_size.ddr_size.sdram0_cs0 * 1024 * 1024))
      {
          remap1.size = ddr_physical_size.ddr_size.sdram0_cs0 * 1024 * 1024;

          remap2.src_addr = ddr_physical_size.ddr_size.sdram0_cs1_addr;
          remap2.dest_addr = 0x80000000 + ddr_physical_size.ddr_size.sdram0_cs1_addr;
          remap2.size = ddr_physical_size.ddr_size.sdram0_cs1 * 1024 * 1024;
      }
      else
      {
          remap1.size = (ddr_physical_size.ddr_size.sdram0_cs0 + ddr_physical_size.ddr_size.sdram0_cs1) * 1024 * 1024 ;
          dual_remap = FALSE;
      }
  }

  if(remap_required)
  {
    boot_ICB_RemapEx("/dev/icbcfg/boot", &sbl1_ddr_info, 0, &remap1);
    if (dual_remap)
    {
      boot_ICB_RemapEx("/dev/icbcfg/boot", &sbl1_ddr_info, 1, &remap2);
    }  
  }
  
  #if DISABLE_TRAINING  
  #ifndef FEATURE_RUMI_BOOT
     #if !DISABLE_FREQ_SWITCH
     ddr_pre_clock_switch(0, 768000, SDRAM_INTERFACE_0);
     boot_clock_set_bimcspeed(768000);
     ddr_post_clock_switch(0, 768000, SDRAM_INTERFACE_0);
     #endif
  #endif
  #endif
  
} /* ddr_remapper */

 

