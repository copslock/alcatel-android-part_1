/*
===========================================================================

FILE:         ddr_external_api.c

DESCRIPTION:  
  This is external APIs to support usage of SCALe sequences

  Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 
===========================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.3/boot_images/core/boot/ddr/hw/msm8953/ddr_external_api.c#11 $
$DateTime: 2016/03/11 01:25:05 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
04/05/14   sr      Initial version.
================================================================================
*/

/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include <string.h>
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "seq_hwio.h"
#include "ddr_external.h"
#include "ddr_params.h"
#include "boot_extern_clk_interface.h"
#include "ddr_target.h"
#include "msmhwiobase.h"
#include "mpm_hwio.h"
#include "ddrss.h"

#if FEATURE_BOOT_EXTERN_DDR_COMPLETED

/*==============================================================================
                                  MACROS
==============================================================================*/

#define QTIMER_BASE_ADDR 0x00084000
#define BUSYWAIT_XO_FREQUENCY_IN_KHZ 19200

#define MAX_AVAILABLE_TEMP_TRAINING_BUFFER ( 64 * 1024 ) /* 100kb */
#define TEMP_TRAINING_BUFFER 0x205000 /* First 0x5000 is used for PBL page table*/

/*==============================================================================
                                  DATA
==============================================================================*/

/* reserve 20kb for training data */
static uint32 training_data_offset = 20 * 1024;

/* starting from offset of 0 for extended training log */
static uint32 training_log_offset_size = 0; 
/*==============================================================================
                                  ASSEMBLY
==============================================================================*/


/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
void sbl1_save_ddr_training_data_to_partition
(
  uint8* ddr_training_data_ptr,
  uint32 ddr_training_data_size,
  uint32 ddr_training_data_offset
);
/* ============================================================================
**  Function : ddr_external_set_clk_speed
** ============================================================================
*/
/*!
*   @brief
*   This function is called by external library to set the clock speed
*   
*   @details
*   This api will set the clock speed accordingly as specified in the parameter to this function
*   
*   @param 
*   @param clk_in_khz       -  [IN] clock speed in khz 
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  boolean
*   
*   @sa None
*/

boolean ddr_external_set_clk_speed(uint32 clk_in_khz)
{
     ddr_pre_clock_switch(0, clk_in_khz, SDRAM_INTERFACE_0);
     boot_clock_set_bimcspeed(clk_in_khz);
     ddr_post_clock_switch(0, clk_in_khz, SDRAM_INTERFACE_0);
     return TRUE;
}

/* ============================================================================
**  Function : ddr_mpm_config_ebi1_freeze_io
** ============================================================================
*/
/*!
*   @brief
*   This function is called by external library while entering/exiting Power Collapse
*   
*   @details
*   This api will be called by SNS library to enable/disable IO clamps during PC
*   
*   @param 
*   @param flag         -  [IN] boolean flag to enable/disable IO clamps
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_mpm_config_ebi1_freeze_io( boolean flag ) 
{ 
    uint32 func_addr = (uint32)&ddr_mpm_config_ebi1_freeze_io;
    uint32 reg_base;

    if (func_addr & SCL_RPM_CODE_RAM_BASE)
      reg_base = MPM2_PSHOLD_REG_BASE;
    else 
      reg_base = RPM_MPM2_PSHOLD_REG_BASE;

    HWIO_OUTXF (reg_base, MPM2_MPM_DDR_PHY_FREEZEIO_EBI1, DDR_PHY_FREEZEIO_EBI1, flag);
    return; 
}


/* ============================================================================
**  Function : ddr_external_get_buffer
** ============================================================================
*/
/*!
*   @brief
*   This function is called by external library to get a buffer for training
*   intermediate data
*   
*   @details
*   This function is called by external library to get a buffer for training
*   intermediate data
*   
*   @param 
*   @param buffer             -  [IN/OUT] uint32 ** buffer where we will populate 
*                                                   with pointer to buffer
*   @param size               -  [IN] uint32 *  we will populate with size of 
*                                                   buffer
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  boolean
*   reutrn operation success\failure
*   
*   @sa None
*/
boolean ddr_external_get_buffer( void ** buffer, uint32 *size )
{
  *buffer = (void *)TEMP_TRAINING_BUFFER;
  if ( *size > MAX_AVAILABLE_TEMP_TRAINING_BUFFER )
	  return FALSE ;
  return TRUE;
}

/* ============================================================================
**  Function : ddr_external_page_to_storage
** ============================================================================
*/
/*!
*   @brief
*   This function is called by external library to save ddr training logs to 
* 	external non-volatile storage
*   
*   @details
*   This function is called by external library to save ddr training logs to 
* 	external non-volatile storage. This api has to take care of offsets for 
* 	saving to external storage so we do not run out of space.
*   api will perform a memset 0 on input buffer of input size before returning
*   
*   @param 
*   @param buffer             -  [IN] uint32 * buffer where traiing logs are located
*   @param size               -  [IN] uint32 * size of data in buffer
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  void
*
*   
*   @sa None
*/
void ddr_external_page_to_storage( void * buffer, uint32 size )
{
	if(buffer != NULL && 
     size != 0 && 
     (size+training_log_offset_size+training_data_offset) <= DDR_MAX_LOG_SIZE)
	{
    /* call sbl1 function to save into flash storage */
    
    /* temporarly disable until partition.xml is updated to 2mb */
    //sbl1_save_ddr_training_data_to_partition(buffer, 
    //                                         size, 
    //                                         training_log_offset_size + training_data_offset);	
    
    /* zero initialize buffer after we save it */
    memset(buffer, 0, size);
    
    /* increment offset for next page*/
    training_log_offset_size += size;
    
    /* check to see if offset is 1k aligned, if not we need to pad it */
    if(training_log_offset_size%1024 != 0)
    {
      /* divide by 1024 (round down divide), add 1 to the result and multiply by
       * 1024 again to get the rounded up to 1k align size
       * this is equivalent operation of doing a round up divide by 1024 then 
       * multiply by 1024 again
       */
      training_log_offset_size =  ((training_log_offset_size/1024)+1) * 1024;
    }
	}
  else
  {
    /* sanity check failed, either bad input or out of allocated partition space */
    /* TODO: add abort */
  }
}

/* ============================================================================
**  Function : ddr_external_get_training_log_size
** ============================================================================
*/
/*!
*   @brief
*   This function is used to return the current stored training log size
*   
*   @details
*   This function is used to return the current stored training log size
*   
*   @param 
*   None
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  uint32
*   size of current trainig log saved to emmc
*   
*   @sa None
*/
uint32 ddr_external_get_training_log_size(void)
{
  return training_log_offset_size;
}

/* ============================================================================
**  Function : ddr_external_get_bootfreq
** ============================================================================
*/
/*!
*   @brief
*   This function is used to get DDR boot frequency
*   
*   @details
*   
*   @param 
*   None
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  uint32
*   
*   @sa None
*/
uint32 ddr_external_get_bootfreq(void)
{
  ddr_interface_state status;

  status = ddr_get_status();
  return status.clk_speed;
}

boolean ddr_external_set_ddrss_cfg_clk(boolean on_off)
{
/* Clock_EnableClock & Clock_DisableClock APIs are not 
   supported in SBL1 and only supported on RPM */ 
   return TRUE;
}

/* TODO: Update this function to work on both RPM and
   APSS processor */
void ddr_busywait (uint32 pause_time_us)
{
  uint32 start_count = 0;
  uint64 delay_count = 0;

  start_count = in_dword(QTIMER_BASE_ADDR);
  /*
  * Perform the delay and handle potential overflows of the timer.
  */
  
  delay_count = ddrss_lib_uidiv((pause_time_us * (uint64)BUSYWAIT_XO_FREQUENCY_IN_KHZ),1000);
  while ((in_dword(QTIMER_BASE_ADDR) - start_count) < delay_count);
}

void seq_wait(uint32 time_value, SEQ_TimeUnit time_unit)
  {

    if(time_unit == SEC)
    {
       ddr_busywait(time_value * 1000 * 1000);
    }
    else if(time_unit == MS)
    {
       ddr_busywait(time_value * 1000);
    }
    else if(time_unit == US)
    {
       ddr_busywait(time_value);
    }
    else
    {
       /* time_unit == NS */
       ddr_busywait(time_value / 1000);
    }

}

uint32 * ddr_external_shared_addr(uint32 *addr)
{
  uint32 func_addr = (uint32)&ddr_external_shared_addr;
  
  if (func_addr & SCL_RPM_CODE_RAM_BASE)
    return addr;
  else
  	return (uint32 *)((uint32)addr & (~0x200000));
}

#endif
