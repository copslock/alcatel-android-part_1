//============================================================================
//  Name:                                                                     
//    std_loadbuild_wp.cmm 
//
//  Description:                                                              
//    WA Specific Build loading script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 02/01/2016 c_akaki       Modified scripts for 8953 WP
// 04/06/2015 JBILLING      changed path for devcfgtz back now that workaround finished.
// 04/01/2015 JBILLING      changed path for devcfgtz
// 03/17/2015 JBILLING      Added TZDevcfg
// 01/26/2015 JBILLING      Changed for 8996. Removed SDI
// 07/01/2014 AJCheriyan    Added change to load pmic.mbn
// 02/21/2013 AJCheriyan    Changed SDI binary name
// 01/18/2013 AJCheriyan    Added TZAPPS binary
// 08/13/2012 AJCheriyan    Ported from 8974 LA, Added FAT16 binary
// 08/08/2012 AJCheriyan    Fixed issue with paths for mjsdload
// 07/19/2012 AJCheriyan    Created for B-family 

// ARG0 - Load option - Supported : ERASEONLY, LOADCOMMON, LOADFULL, LOADIMG
// ARG1 - Valid image name. Can be used only with LOADIMG option.
ENTRY &ARG0 &ARG1

LOCAL &CWD &SEARCHPATHS &STORAGE_OPTION &STORAGE_TYPE &PROGRAMMER &PARTITION
LOCAL &XML &MAX_PARTITIONS &XML_LOCATION &BINARY &BINARYPATH &METASEARCHPATH

&PROGRAMMER="boot_images/core/storage/tools/jsdcc/mjsdload"
&MAX_PARTITIONS=1
&XML_LOCATION="&METASCRIPTSDIR/../../../build"
&PARTITION=0

MAIN:
	// We have checked for all the intercom sessions at this point and we don't need any error
	// Save the argument
	&LOAD_OPTION="&ARG0"

	// Switch to the tools directory
	&CWD=OS.PWD()
	
	cd &CWD
	
	&SEARCHPATHS="&XML_LOCATION"
	
	// Erase only
	IF (("&ARG0"=="ERASEONLY")||("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
	(
		// Only erase the chip and exit
		CD.DO &BOOT_BUILDROOT/&PROGRAMMER ERASE
	)

    // Load common images
    IF (("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
    (
        // Check for all the common images 

        // Check for the presence of all the binaries
        // Not needed because meta-build should have populated all this information
        // SBL , TZ, RPM, UEFI

        do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
        do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
		do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QSEE_BINARY
		do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&DEVCFG_BINARY
		do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&WINSECAPP_BINARY
		do std_utils FILEXIST FATALEXIT &UEFI_BUILDROOT/&UEFI_BINARY
		do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&APPSCONFIG_BINARY
		do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&EFIESP_BINARY
		do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&TZAPPS_BINARY
        
        // Now flash them all one by one 
        // Flash the partition table
		&PARTITION=0
		WHILE (&PARTITION<&MAX_PARTITIONS)
		(
			&XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
        	&FILES="gpt_main"+FORMAT.DECIMAL(1, &PARTITION)+".bin,"+"gpt_backup"+FORMAT.DECIMAL(1,&PARTITION)+".bin"
       
			CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=&FILES
			&PARTITION=&PARTITION+1
        )
		&SEARCHPATHS="&XML_LOCATION,"+OS.FILE.PATH(&BOOT_BUILDROOT/&BOOT_BINARY)+","+OS.FILE.PATH(&TZ_BUILDROOT/&QSEE_BINARY)+","+OS.FILE.PATH(&RPM_BUILDROOT/&RPM_BINARY)+","+OS.FILE.PATH(&UEFI_BUILDROOT/&UEFI_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&EFIESP_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&APPSCONFIG_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&TZAPPS_BINARY)
 
        // Apply the disk patches
        &PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="patch"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER PATCH searchpaths=&SEARCHPATHS xml=&XML
            &PARTITION=&PARTITION+1
        )
        	
		&PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=sbl1.mbn,tz.mbn,devcfg.mbn,rpm.mbn,uefi.mbn,tzapps.bin,EFIESP.bin,fat16.bin,winsecapp.mbn
		    &PARTITION=&PARTITION+1
        )
    )
    
    // Load common images
    IF ("&ARG0"=="LOADIMG")
    (
        // Check for the binary first 
        IF ("&ARG1"=="sbl")
        (
            do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
            &BINARY="sbl1.mbn"
			&BINARYPATH=OS.FILE.PATH("&BOOT_BUILDROOT/&BOOT_BINARY")    
        )
        IF ("&ARG1"=="tz")
        (
          	do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QSEE_BINARY
			//do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QHEE_BINARY
            &BINARY="tz.mbn,devcfg.mbn"
            &BINARYPATH=OS.FILE.PATH("&TZ_BUILDROOT/&QSEE_BINARY")
        )
        IF ("&ARG1"=="rpm")
        (
            do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
            &BINARY="rpm.mbn"
            &BINARYPATH=OS.FILE.PATH("&RPM_BUILDROOT/&RPM_BINARY")
        )
        IF ("&ARG1"=="uefi")
        (
            do std_utils FILEXIST FATALEXIT &UEFI_BUILDROOT/&UEFI_BINARY
			&BINARY="uefi.mbn"
            &BINARYPATH=OS.FILE.PATH("&UEFI_BUILDROOT/&UEFI_BINARY")
        )
        
        // Flash the image now
        //&SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+"&BINARYPATH"
		&SEARCHPATHS="&XML_LOCATION,"+"&BINARYPATH"
	
        &PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=&BINARY
            &PARTITION=&PARTITION+1
        )
    )

    // Load HLOS images
    IF ("&LOAD_OPTION"=="LOADFULL")
    (
        // Call the HLOS specific loading script
        do std_utils FILEXIST FATALEXIT &METASCRIPTSDIR/../../../config/loadbuild_wp.py
        OS.COMMAND cmd /k python &METASCRIPTSDIR/../../../config/loadbuild_wp.py
    )

    // Return to the old directory
    CD &CWD

    GOTO EXIT


FATALEXIT:
    END

EXIT:
    ENDDO