#ifndef __ICB_RPM_HWIO_H__
#define __ICB_RPM_HWIO_H__
/*
===========================================================================
*/
/**
  @file icb_rpm_hwio.h

  @brief Additional HWIO definitions for ICB RPM driver.
*/
/*
===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

===========================================================================

  $Header: //components/rel/rpm.bf/2.4/core/buses/icb/src/8953/icb_rpm_hwio.h#1 $
  $DateTime: 2015/12/10 03:27:49 $
  $Author: pwbldsvc $

===========================================================================
*/

#endif /* __ICB_RPM_HWIO_H__ */
