/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/10/20 23:07:10 $
 * $Header: //components/rel/rpm.bf/2.4/core/buses/spmi/src/platform/os/rpm/SpmiOsLogs.c#1 $
 * $Change: 9262159 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOsLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

ULogHandle spmiLogHandle;
