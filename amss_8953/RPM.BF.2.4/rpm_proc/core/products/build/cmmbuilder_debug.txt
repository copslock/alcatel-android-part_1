Dictionary Debug:image:RPM
 
scripts:{'../systemdrivers/pmic/scripts/swevent_log_dump.cmm': 'PMIC SW Event Dump', '../systemdrivers/pmic/scripts/PMICDump.cmm': 'PMIC Dump', '../systemdrivers/pmic/scripts/PMICPeek.cmm': 'PMIC Peek/Poke', '../systemdrivers/pmic/scripts/PMICRegisters.cmm': 'Registers - PMIC', '../systemdrivers/pmic/scripts/PMICDashBoard.cmm': 'PMIC Dashboard'}
 
Arguments:[]
 
area:Systemdrivers
 
