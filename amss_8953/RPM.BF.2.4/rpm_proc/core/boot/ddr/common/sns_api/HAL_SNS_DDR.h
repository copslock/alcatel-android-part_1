#ifndef __HAL_SNS_DDR_H__
#define __HAL_SNS_DDR_H__

/**---------------------------------------------------------------------------
* @file     HAL_sns_DDR.h
*
* @brief    DDR HW abstraction layer
*
* Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
*--------------------------------------------------------------------------*/

/*=============================================================================

                                   DDR HAL
                            H e a d e r  F i l e
GENERAL DESCRIPTION
This is the header file for ddr HAL. This file is shared across targets

EXTERNALIZED FUNCTIONS
  
INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.4/core/boot/ddr/common/sns_api/HAL_SNS_DDR.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
09/02/15   sc      Initial version.
=============================================================================*/


/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "HALhwio.h"

#include "ddr_common.h"
#include "dev_cdt_params.h"
#include "ddr_log.h"

/* ============================================================================
**  Function : HAL_DDR_Enter_Self_Refresh
** ============================================================================
*/
/*!
*   @brief
*   This function triggers the ddr to enter self refresh mode for channel EBI0/1
*   
*   @details
*   This function triggers the ddr to enter self refresh mode for channel EBI0/1
*   
*   
*   @par Dependencies
*   This code must not reside on the ddr that is being put to self refresh.
*   
*   @par Side Effects
*   DDR is in self refresh mode and if both CS is selected, or only 1 CS is physically
*   present, then the ddr clock is turned off, at this point, any writes to ddr will
*   fail since the controller is not going to respond.
*   
*   @retval  None
*   
*   @sa None
*/
boolean HAL_DDR_Enter_Self_Refresh(void *ddr, DDR_CHANNEL channel, 
  DDR_CHIPSELECT chip_select);

/* ============================================================================
**  Function : HAL_DDR_Exit_Self_Refresh
** ============================================================================
*/
/*!
*   @brief
*   This function triggers the ddr to exit self refresh mode for channel EBI0/1
*   
*   @details
*   This function triggers the ddr to exit self refresh mode for channel EBI0/1
*   
*   @par Dependencies
*   
*   
*   @par Side Effects
*   
*   @retval  None
*   
*   @sa None
*/
boolean HAL_DDR_Exit_Self_Refresh(void *ddr, DDR_CHANNEL channel, 
  DDR_CHIPSELECT chip_select);

/* =============================================================================
**  Function : HAL_DDR_Enter_Power_Collapse
** =============================================================================
*/
/**
*   @brief
*   Init state before entry of power collapse
*
*   @param[in]  ddr          Pointer to ddr conifiguration struct
*   @param[in]  channel    channel to initialize for
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean HAL_DDR_Enter_Power_Collapse(void *ddr, DDR_CHANNEL channel);

/* =============================================================================
**  Function : HAL_DDR_Enter_Power_Collapse
** =============================================================================
*/
/**
*   @brief
*   Init state after exit of power collapse
*
*   @param[in]  ddr          Pointer to ddr conifiguration struct
*   @param[in]  channel    channel to initialize for
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean HAL_DDR_Exit_Power_Collapse(void *ddr, DDR_CHANNEL channel);

/* ============================================================================
**  Function : HAL_DDR_Enter_Deep_Power_Down
** ============================================================================
*/
/*!
*   @brief
*   This function will put the ddr into DPD mode
*   
*   @details
*   This function will trigger the ddr to be put into deep power down mode.
*   In the case where we only have 1 device on the channel (CS0 only), or both
*   chip select are chosen, we will also turn off ddr clock.
*   
*   @param channel   -  [IN] channel to configure DPD for
*   
*   @par Dependencies
*   The desired CS to be configured for Deep Power Down Must be configured first by
*   calling HAL_DDR_Chipselect().
*   
*   @par Side Effects
*   After ddr has gone into Deep Power Down, all the data stored will be lost.
*   Also, when returning from deep power down, we must reinitialize the DDR.
*   
*   @retval  None
*   
*   @sa None
*/
boolean HAL_DDR_Enter_Deep_Power_Down(void *ddr, DDR_CHANNEL channel, 
  DDR_CHIPSELECT);

/* ============================================================================
**  Function : HAL_DDR_Exit_Deep_Power_Down
** ============================================================================
*/
/*!
*   @brief
*   Brief_Description_Goes_here
*   
*   @details
*   Detailed_Description_Goes_here
*   
*   @param clkspeed   -  [IN] Clock speed to reinitialize ddr for
*   
*   @par Dependencies
*   Clock must be setup for initialization speed.
*   LPDDR1 - no limitation
*   LPDDR2 - < 50Mhz
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/
boolean HAL_DDR_Exit_Deep_Power_Down(void *ddr, DDR_CHANNEL channel, 
  DDR_CHIPSELECT, uint32 clkspeed);

#endif /* __HAL_SNS_DDR_H__ */
