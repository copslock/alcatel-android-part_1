/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/2.4/core/boot/ddr/hw/msm8953/ddr_target.c#6 $
$DateTime: 2016/05/25 02:16:14 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/17/16   sc      Enabled DDR status dump only in clock switch hang scenario
12/22/15   sk      ddr_init_smps_mode is added
09/02/15   sc      Initial version
================================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "ddr_func_tbl.h"
#include "ddr_log.h"
#include "npa.h"
#include "ClockDefs.h"
#include "Clock.h"
#include "mpm.h"
#include "railway.h"
#include "HAL_SNS_DDR.h"
//#include "pm_railway.h"
#include "PlatformInfo.h"
#include "ddr_status_reg.h"
//#include "ddr_health_monitor.h"
//#include "rpm_global_hash.h"
#include "CoreVerify.h"
//#include "QDSSLite.h"
#include "pmapp_npa.h"

/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

ddr_func *ddr_func_tbl;

/*Pointer to CDT params*/
static ddr_device_params *shared_ddr_device_table_wdog;

/*Pointer to DDR STRUCT in Boot*/
void * ddrsns_share_data;

extern uint32 *ddr_status_reg_addr;

/*Pointer to DDRSS and BIMC base address*/
struct config_base_addr * shared_base_addr_ptr;

/* DDR Partition information */
ddr_size_partition ddr_size_partition_info;

/* DDR logging level */
enum ddr_log_level ddr_log_level = DDR_ERROR;

extern boolean ddr_init_done;

#if 0
/* runtime hash of ddr shared parameter data */
static uint32 ddr_data_hash;
#endif

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

boolean HAL_DDR_Pre_Clock_Switch(void *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz, 
  uint32 new_clk_khz)
{
  return ddr_func_tbl->pre_ddr_clock_switch(ddr, channel, curr_clk_khz, new_clk_khz);
}

boolean HAL_DDR_Post_Clock_Switch(void *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz, 
  uint32 new_clk_khz)
{
  return ddr_func_tbl->post_ddr_clock_switch(ddr, channel, curr_clk_khz, new_clk_khz);
}

boolean HAL_DDR_Enter_Power_Collapse(void *ddr, DDR_CHANNEL channel)
{
  return ddr_func_tbl->enter_ddr_power_collapse(ddr, channel);
}

boolean HAL_DDR_Exit_Power_Collapse(void *ddr, DDR_CHANNEL channel)
{
  return ddr_func_tbl->exit_ddr_power_collapse(ddr, channel);
}

boolean HAL_DDR_Enter_Self_Refresh(void *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
  return ddr_func_tbl->enter_self_refresh(ddr, channel, chip_select);
}

boolean HAL_DDR_Exit_Self_Refresh(void *ddr, DDR_CHANNEL channel,DDR_CHIPSELECT chip_select)
{
  return ddr_func_tbl->exit_self_refresh(ddr, channel, chip_select);
}

boolean HAL_DDR_Enter_Deep_Power_Down(void *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chipselect)
{
  return TRUE;
}

boolean HAL_DDR_Exit_Deep_Power_Down(void *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chipselect, uint32 clkspeed)
{
  return TRUE;
}
//TODO: Enable QDSS, store status registers, health monitor one-by-one
#if 0
extern boolean Clock_IsQDSSOn(void);
#endif

/* ============================================================================
**  Function : ddr_get_params
** ============================================================================
*/
/**
*   @brief
*   Get DDR device parameters.
*
*   @param[in]  interface_name  Interface to get DDR device parameters for
*
*   @return
*   DDR device parameters
*
*   @dependencies
*   None
*
*   @sa
*   None
*
*   @sa
*   None
*/
ddr_device_params *ddr_get_params(DDR_CHANNEL interface_name)
{
  ddr_device_params *device_params, *device_table;
  uint32 device_params_size;

  device_table = shared_ddr_device_table_wdog;

  if (interface_name == DDR_CH0)
  {
    device_params = device_table;
  }
  else
  {
    device_params_size = sizeof(struct ddr_device_params_lpddr);
    device_params = (ddr_device_params *)
                     ((uint32)device_table + device_params_size);
  }
  return device_params;
}

/* ============================================================================
**  Function : ddr_target_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called at the end of ddr init to initialize any
*   target specific configurations
*
*   @details
*   Target specific configurations such as override of timing registers and
*   calling target specific init functions will be captured in this api
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

 __attribute__((section("ddr_init_funcs"))) void ddr_target_init()
{
#if 0
  /* Railway voter variables */
  const int cx_rail_id = rail_id("vddcx");
  const int mx_rail_id = rail_id("vddmx");

  /* Update partition information */
  ddr_size_partition_info = ddr_get_partition();

#if (!defined BUILD_BOOT_CHAIN) && (!defined BUILD_EHOSTDL)

  /* Register ddr driver for railway callback */
  railway_set_callback(cx_rail_id,RAILWAY_PRECHANGE_CB, ddr_pre_vddcx_switch,NULL);
  railway_set_callback(cx_rail_id,RAILWAY_POSTCHANGE_CB, ddr_post_vddcx_switch,NULL);
  railway_set_callback(mx_rail_id,RAILWAY_PRECHANGE_CB, ddr_pre_vddmx_switch,NULL);
  railway_set_callback(mx_rail_id,RAILWAY_POSTCHANGE_CB, ddr_post_vddmx_switch,NULL);
#endif

#endif
  
  uint32 *ddr_func_tbl_ptr = (uint32 *)(SHARED_IMEM_DDR_PARAM_BASE + 4);
  ddr_func_tbl = (ddr_func *)(*ddr_func_tbl_ptr);

  ddrsns_share_data = ddr_func_tbl->Get_DDR_Struct_Addr();
  shared_ddr_device_table_wdog = (ddr_device_params *)(ddr_func_tbl->Get_CDT_Ptr());
  //shared_ddr_size_ptr = ((struct ddr_shared_params * SHARED_IMEM_DDR_PARAM_BASE)->ddr_size_ptr);

  ddr_status_reg_addr = ddr_func_tbl->Get_DDR_Status_Reg_Addr();
}

/* ============================================================================
**  Function : ddr_post_init
** ============================================================================
*/
/*!
*   @brief
*   This function will train the ddr at every voltage and frequency level
*   supported
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

boolean ddr_post_init()
{
  return TRUE;
}

/* ============================================================================
**  Function : ddr_pre_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before clock switching occures.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be stalled and new timing parameter is loaded into shadow.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk   -   [IN] the current clock speed
*   @param new_clk    -  [IN] the clock speed we are switching to
*   @param new_clk    -  [IN] interface to switch clock for
*
*   @par Dependencies
*
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_pre_clock_switch(uint32 curr_clk, uint32 new_clk,SDRAM_INTERFACE interface)
{
  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  #endif

  HAL_DDR_Pre_Clock_Switch(ddrsns_share_data, DDR_CH0, curr_clk, new_clk);
  
  #if LDO_HEADROOM_FEATURE_EN
  if(new_clk > DDR_PMIC_AUTOMODE_THRESHOLD_0 && curr_clk <= DDR_PMIC_AUTOMODE_THRESHOLD_0)
  {
    /* new frequency is higher than current frequency, apply new voltage mode 
     * now so new frequencies can work.
     */
    pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_HIGH_SPEED);
  }
  #endif
} /* ddr_pre_clock_switch */

/* ============================================================================
**  Function : ddr_post_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after clock switching occurs.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be unstalled.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk          -  [IN] the current clock speed
*   @param new_clk           -  [IN] the clock speed we are switching to
*   @param interface_name    -  [IN] interface to switch clock for
*
*   @par Dependencies
*   This code has to be on IRAM because ddr is unavailable during clock switching
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_post_clock_switch(uint32 curr_clk, uint32 new_clk,SDRAM_INTERFACE interface)
{

  HAL_DDR_Post_Clock_Switch(ddrsns_share_data, DDR_CH0, curr_clk, new_clk);

  #if LDO_HEADROOM_FEATURE_EN
  if(new_clk <= DDR_PMIC_AUTOMODE_THRESHOLD_0 && curr_clk > DDR_PMIC_AUTOMODE_THRESHOLD_0)
  {
    pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_LOW_SPEED);
  }
  #endif

  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  #endif
} /* ddr_post_clock_switch */

#if 0
/* ============================================================================
**  Function : ddr_pre_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_pre_vddmx_switch(const railway_settings *proposal, void * callback_cookie)
{
  #if 0
  ddr_store_status_regs(PRE_CLK_VOL_SWITCH_LOGGING);
  // need to call voltage switch API in DDRSNS .

  /* perform sanity check */
  ddr_health_monitor();
  #endif
} /* ddr_pre_vddmx_switch */

/* ============================================================================
**  Function : ddr_post_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddmx_switch(const railway_settings *proposal, void * callback_cookie)
{
  #if 0
  ddr_store_status_regs(POST_CLK_VOL_SWITCH_LOGGING);
  // need to call voltage switch API in DDRSNS .

  /* perform sanity check */
  ddr_health_monitor();
  #endif
} /* ddr_post_vddmx_switch */
#endif

/* ============================================================================
**  Function : ddr_pre_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before vddcx switch.
*
*   @param settings - contains the VDDCX voltage level we just switched to
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/
void ddr_pre_vddcx_switch(const railway_settings *proposal, void * callback_cookie)
{
  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  #endif
} /* ddr_pre_vddcx_switch */

/* ============================================================================
**  Function : ddr_post_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after VDDCX is switched
*
*   @param none
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddcx_switch(const railway_settings *proposal, void * callback_cookie)
{
  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  #endif
} /* ddr_post_vddcx_switch */

/*=============================================================================
    Function : ddr_init_smps_mode
=============================================================================*/

/**
  Initializes/sets SMPS mode to auto/PWM based on bootup frequency.

  This function is called during clock_init

  @param[in] freq        Current clock speed.

  @return
  None.

  @dependencies
  The ddr_init_smps_mode() function must be called only during clock init
  where timing parameters for the bootup frequency within DDR driver are
  already set.

*/
void ddr_init_smps_mode(uint32 freq)
{
  uint32 ddr_freq_enum = 0;

  if(freq > DDR_PMIC_AUTOMODE_THRESHOLD_0)
  {
    ddr_freq_enum = PMIC_NPA_MODE_ID_DDR_HIGH_SPEED;
  }       
  else
  {
	  ddr_freq_enum = PMIC_NPA_MODE_ID_DDR_LOW_SPEED;
  }
  pm_app_rpm_ldo_hr_config(ddr_freq_enum);
} /* ddr_init_smps_mode */

/* ============================================================================
**  Function : ddr_pre_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right before XO shutdown. Puts DDR into self refresh mode and
*   disables CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_post_xo_shutdown
*/

void ddr_pre_xo_shutdown(uint32 clk_speed)
{

  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  
  /* perform hash of ddr struct to ensure data integrity */
  ddr_data_hash = rpm_hash(sizeof(DDR_STRUCT),  ddrsns_share_data);
  #endif
  
  ddr_enter_self_refresh_all(clk_speed);

  pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_SLEEP_SPEED);

} /* ddr_pre_xo_shutdown */

/* ============================================================================
**  Function : ddr_post_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right after XO wakeup. Takes DDR out of self refresh mode and enables
*   CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_pre_xo_shutdown
*/

void ddr_post_xo_shutdown(uint32 clk_speed)
{

  #if 0
  /* perform hash check first before taking ddr out of self refresh*/
  if(ddr_data_hash != rpm_hash(sizeof(DDR_STRUCT),  ddrsns_share_data))
  {
    CORE_VERIFY(0);
  }
  #endif

  pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_LOW_SPEED);

  ddr_exit_self_refresh_all(clk_speed);
  
  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  #endif

} /* ddr_post_xo_shutdown */

#if 0
/* ============================================================================
**  Function : ddr_increase_clock_speed
** ============================================================================
*/
/*!
*   @brief
*   This function will bump up the clock speed for ddr to the maximum nominal
*   frequency
*
*   @details
*   This function will use clkrgm npa voting to vote for max nominal nominal
*   frequency
*
*   @param None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval
*   boolean - operation successful or not.
*
*   @sa None
*/

boolean ddr_increase_clock_speed(void)
{
  /* variables for querying clkrgm */
  int             i;
  uint32          nNumLevel;
  npa_query_type  query_result;
  ClockPlanType   *pClockPlan;
  ClockIdType     nClock;
  DALResult       eResult;


  /* Query clkrgm for frequency voltage table */
    npa_query_by_name("/clk/bimc", CLOCK_NPA_QUERY_NUM_PERF_LEVELS,
        &query_result);
    nNumLevel = query_result.data.value;

    DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&query_result.data.reference);
    if ( query_result.data.reference == NULL )
    {
      return FALSE;
    }

    npa_query_by_name("/clk/bimc", CLOCK_NPA_QUERY_ALL_FREQ_KHZ, &query_result);
    pClockPlan = (ClockPlanType *) query_result.data.reference;

    eResult = Clock_GetClockId("gcc_bimc_clk", &nClock);
    if (eResult == DAL_SUCCESS)
    {
      /* find the highest frequency nominal voltage supports */

      for(i = nNumLevel -1; i >= 0; i--)
      {
        if(pClockPlan[i].eVRegLevel == CLOCK_VREG_LEVEL_NOMINAL)
        {
          /* found highest frequency level with nominal voltage */
          break;
        }
      }
      Clock_SetDirectBusSpeed(CLOCK_RESOURCE_BIMC, pClockPlan[i].nFreqKHz);
    }
    else
    {
      DALSYS_Free(pClockPlan);
      return FALSE;
    }

    DALSYS_Free(pClockPlan);
    return TRUE;
}
#endif

/* =============================================================================
**  Function : ddr_enter_power_collapse
** =============================================================================
*/
/**
*   @brief
*   Function called before DDR controller goes into power collapse.
*
*   @param[in]  clk_speed  Current clock speed (in KHz)
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_enter_power_collapse(uint32 clk_speed)
{
  #if 0
  if (Clock_IsQDSSOn()) 
  {
     QDSSDDRTraceDisable();
  }

  /* perform sanity check */
  ddr_health_monitor();
  
  /* perform hash of ddr struct to ensure data integrity */
  ddr_data_hash = rpm_hash(sizeof(DDR_STRUCT),  ddrsns_share_data);
  #endif
  
 /* ENH: please check the CDT and pass the right channel information on target basis */
  HAL_DDR_Enter_Power_Collapse(ddrsns_share_data, DDR_CH0);

  pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_SLEEP_SPEED);

} /* ddr_enter_power_collapse */

/* =============================================================================
**  Function : ddr_exit_power_collapse
** =============================================================================
*/
/**
*   @brief
*   Function called after DDR controller comes out of power collapse.
*
*   @param[in]  clk_speed  Current clock speed (in KHz)
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_exit_power_collapse(uint32 clk_speed)
{

  pm_app_rpm_ldo_hr_config(PMIC_NPA_MODE_ID_DDR_LOW_SPEED);

  #if 0
  /* perform hash check first before taking ddr out of self refresh*/
  if(ddr_data_hash != rpm_hash(sizeof(DDR_STRUCT),  ddrsns_share_data))
  {
    CORE_VERIFY(0);
  }
  #endif

  /* ENH: please check the CDT and pass the right channel information on target basis */
  HAL_DDR_Exit_Power_Collapse(ddrsns_share_data, DDR_CH0);

  #if 0
  /* perform sanity check */
  ddr_health_monitor();
  if (Clock_IsQDSSOn()) 
  {
     QDSSDDRTraceEnable();
  }
  #endif

} /* ddr_exit_power_collapse */


