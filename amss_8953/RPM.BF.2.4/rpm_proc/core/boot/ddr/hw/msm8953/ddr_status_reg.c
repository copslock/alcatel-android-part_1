/**
 * @file ddr_status_reg.c
 * @brief
 * DDR Status Register logging file for DDR driver.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/2.4/core/boot/ddr/hw/msm8953/ddr_status_reg.c#3 $
$DateTime: 2016/05/25 02:16:14 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/16/16   sc      Enable DDR status dump for clock switch hang scenario
09/02/15   sc      Initial version.
================================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HAL_SNS_DDR.h"
#include "ddr_status_reg.h"
#include "Clock.h"
/*==============================================================================
                                  MACROS
==============================================================================*/

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

/* This array contain DDR BIMC/PHY register address ,
 * this list may shrink and grow dependending upon the availability of space in RPM codeRAM for other important struct elements in the big Struct DDR_STRUCT & 
 * this list is flexible , one can edit and update the list and ensure to match it with corresponding struct element 
 * resides in ddr_status_reg.h file */

uint32 *ddr_status_reg_addr = NULL;

ddr_status_struct ddr_status_var;


/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

/* ============================================================================
**  Function : ddr_store_status_regs
** ============================================================================
*/
/*!
*   @brief
*   This function is called before and after pre/post clock/voltage switch to store the
*   status information of the important DDR PHY/BIMC registers .
*
*   @details
*   This function store the status register values, BIMC/PHY status registers ( for pre_clock , pre_voltage switch) in 0th index , 
*   BIMC/PHY status registers ( for post_clock , post_voltage switch) in 1st index of ddr_status_reg field of DDR_STRUCT 
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_store_status_regs(reg_status_log pre_post)
{
  uint32 i = 0;
  uint32 count = 0;
  static ddr_status_reg   pre_post_status_reg;
  uint32 *var = (uint32*)&pre_post_status_reg;
  ddr_interface_state ddr_status = ddr_get_status();
  
  if((ddr_status.sdram0_cs0 == DDR_ACTIVE) ||
	 (ddr_status.sdram0_cs1 == DDR_ACTIVE)) {

  	do{
	      *var = *((uint32*)(ddr_status_reg_addr[i]));
	      var++;
	      if( pre_post != ERROR_ABORT ){
		      count++;
	      }
	      if ( count == 10 ) break; /* Max 10 registers we can log during clock switch to avoid latency issues */
	  }while(ddr_status_reg_addr[++i] !=NULL);

    if(pre_post == ERROR_ABORT)
      pre_post =  PRE_CLK_VOL_SWITCH_LOGGING;

    ddr_status_var.status_reg[pre_post] = pre_post_status_reg;
   }
}

/* ============================================================================
**  Function : ddr_store_status_regs_error_fatal
** ============================================================================
*/
/*!
*   @brief
*   This function is called before software triggers for watchdog reset
*
*   @details
*   This function is called before software triggers for watchdog reset. This 
*   function will take a snapshot of the entire bimc and ddrphy register into
*   predetermined code region, overwriting the code segment of ddr driver.
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   ddr code segment will be trashed. This is normally safe because this code 
*   should only be called when rpm has already crashed.
*
*   @retval  None
*
*   @sa None
*/
boolean ddr_external_set_ddrss_cfg_clk(boolean on_off)
{
  static ClockIdType dimClk;
  DALResult   eRes;
	
  if (dimClk == 0)
  {
  	eRes = Clock_GetClockId( "gcc_ddr_dim_cfg_clk", &dimClk);
  	assert( eRes == DAL_SUCCESS);
  }

  if(on_off)
     Clock_EnableClock(dimClk);
  else
     Clock_DisableClock(dimClk);
  return TRUE;
}

/* ============================================================================
**  Function : ddr_store_status_regs_error_fatal
** ============================================================================
*/
/*!
*   @brief
*   This function is called before software triggers for watchdog reset
*
*   @details
*   This function is called before software triggers for watchdog reset. This 
*   function will take a snapshot of the entire bimc and ddrphy register into
*   predetermined code region, overwriting the code segment of ddr driver.
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   ddr code segment will be trashed. This is normally safe because this code 
*   should only be called when rpm has already crashed.
*
*   @retval  None
*
*   @sa None
*/

void ddr_store_status_regs_error_fatal(void)
{
  ddr_store_status_regs(ERROR_ABORT);
}

