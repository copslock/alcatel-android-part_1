#include "DALPropDef.h"
#include "DALPropDef.h"
#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "DALDeviceId.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "pm_dal_prop_ids.h"
#include "vsense_dal_prop_ids.h"
#include "DALStdDef.h" 
#include "DALSysTypes.h" 
#include "dalconfig.h" 

#ifndef DAL_CONFIG_IMAGE_RPM 
#define DAL_CONFIG_IMAGE_RPM 
#endif 
extern void * icb_rpm_system;
extern void * SourceConfig;
extern void * RPMClockConfig;
extern void * SystemNOCClockConfig;
extern void * SystemMMNOCClockConfig;
extern void * PCNOClockConfig;
extern void * BIMCClockConfig;
extern void * BIMCGPUClockConfig;
extern void * BIMCClockPlans;
extern void * APSSTCUASYNCClockConfig;
extern void * APSSAXIClockConfig;
extern void * Q6TBUClockConfig;
extern void * QDSSATClockConfig;
extern void * QDSSTraceClockConfig;
extern void * QDSSSTMClockConfig;
extern void * QDSSTSCTRDiv2ClockConfig;
extern void * RBCPRClockConfig;
extern void * SPMIAHBClockConfig;
extern void * SPMISERClockConfig;
extern void * IPAClockConfig;
extern void * VSClockConfig;
extern void * ClockLogDefaultConfig;
extern void * ClockVregRailMapConfig;
extern void * ClockBIMCMMNOCMapConfig;
extern void * clk_reg;
extern void * smps_reg;
extern void * ldo_reg;
extern void * vs_reg;
extern void * boost_reg;
extern void * boost_byp_reg;
extern void * fts_volt;
extern void * fts2p5_volt;
extern void * hfs_volt;
extern void * boost_volt;
extern void * boost_byp_volt;
extern void * nmos_volt;
extern void * n600_volt;
extern void * n1200_volt;
extern void * pmos_volt;
extern void * ln_volt;
extern void * fts_range;
extern void * fts2p5_range;
extern void * hfs_range;
extern void * boost_range;
extern void * boost_byp_range;
extern void * nmos_range;
extern void * n600_range;
extern void * n1200_range;
extern void * pmos_range;
extern void * ln_range;
extern void * ult_nmos_volt;
extern void * ult_pmos_volt;
extern void * ult_nmos_range;
extern void * ult_pmos_range;
extern void * ult_buck_range_1;
extern void * ult_buck_volt_1;
extern void * ult_buck_range_2;
extern void * ult_buck_volt_2;
extern void * unified_rails_stepper_rate;
extern void * ult_ldo_stepper_rate;
extern void * ult_smps_stepper_rate;
extern void * ldo_stepper_rate;
extern void * smps_stepper_rate;
extern void * ldo_vreg_ok_time;
extern void * smps_vreg_ok_time;
extern void * vs_vreg_ok_time;
extern void * boost_vreg_ok_time;
extern void * clk_sleep_reg;
extern void * clk_xo_reg;
extern void * clk_common;
extern void * num_of_smps;
extern void * num_of_ldo;
extern void * ldo_rail;
extern void * smps_rail;
extern void * clk_info;
extern void * smps_dep;
extern void * ldo_dep;
extern void * clk_dep;
extern void * pm_mpm_active_cfg;
extern void * pm_mpm_sleep_cfg;
extern void * pm_mpm_cmd_index;
extern void * pm_mpm_rails_info;
extern void * sleep_enter_info;
extern void * sleep_exit_info;
extern void * pm_npa_rpm_pam_node_rsrcs;
extern void * num_of_pm_rpm_nodes;
extern void * vsense_config_data;

const void * DALPROP_StructPtrs[88] =  {
	&icb_rpm_system,
	&SourceConfig,
	&RPMClockConfig,
	&SystemNOCClockConfig,
	&SystemMMNOCClockConfig,
	&PCNOClockConfig,
	&BIMCClockConfig,
	&BIMCGPUClockConfig,
	&BIMCClockPlans,
	&APSSTCUASYNCClockConfig,
	&APSSAXIClockConfig,
	&Q6TBUClockConfig,
	&QDSSATClockConfig,
	&QDSSTraceClockConfig,
	&QDSSSTMClockConfig,
	&QDSSTSCTRDiv2ClockConfig,
	&RBCPRClockConfig,
	&SPMIAHBClockConfig,
	&SPMISERClockConfig,
	&IPAClockConfig,
	&VSClockConfig,
	&ClockLogDefaultConfig,
	&ClockVregRailMapConfig,
	&ClockBIMCMMNOCMapConfig,
	&clk_reg,
	&smps_reg,
	&ldo_reg,
	&vs_reg,
	&boost_reg,
	&boost_byp_reg,
	&fts_volt,
	&fts2p5_volt,
	&hfs_volt,
	&boost_volt,
	&boost_byp_volt,
	&nmos_volt,
	&n600_volt,
	&n1200_volt,
	&pmos_volt,
	&ln_volt,
	&fts_range,
	&fts2p5_range,
	&hfs_range,
	&boost_range,
	&boost_byp_range,
	&nmos_range,
	&n600_range,
	&n1200_range,
	&pmos_range,
	&ln_range,
	&ult_nmos_volt,
	&ult_pmos_volt,
	&ult_nmos_range,
	&ult_pmos_range,
	&ult_buck_range_1,
	&ult_buck_volt_1,
	&ult_buck_range_2,
	&ult_buck_volt_2,
	&unified_rails_stepper_rate,
	&ult_ldo_stepper_rate,
	&ult_smps_stepper_rate,
	&ldo_stepper_rate,
	&smps_stepper_rate,
	&ldo_vreg_ok_time,
	&smps_vreg_ok_time,
	&vs_vreg_ok_time,
	&boost_vreg_ok_time,
	&clk_sleep_reg,
	&clk_xo_reg,
	&clk_common,
	&num_of_smps,
	&num_of_ldo,
	&ldo_rail,
	&smps_rail,
	&clk_info,
	&smps_dep,
	&ldo_dep,
	&clk_dep,
	&pm_mpm_active_cfg,
	&pm_mpm_sleep_cfg,
	&pm_mpm_cmd_index,
	&pm_mpm_rails_info,
	&sleep_enter_info,
	&sleep_exit_info,
	&pm_npa_rpm_pam_node_rsrcs,
	&num_of_pm_rpm_nodes,
	&vsense_config_data,
	 NULL }; 

const uint32 DALPROP_PropBin[] = {

			0x000007d8, 0x00000028, 0x000002b4, 0x000002b4, 0x000002b4, 
			0x00000002, 0x02000145, 0x00000344, 0x0200009b, 0x000007a0, 
			0x74737973, 0x70006d65, 0x5f63696d, 0x5f627261, 0x65736162, 
			0x6464615f, 0x776f0072, 0x0072656e, 0x65746e69, 0x70757272, 
			0x6d730074, 0x6e695f64, 0x655f7274, 0x6c62616e, 0x6c006465, 
			0x625f6d69, 0x00636d69, 0x5f6d696c, 0x636f6e73, 0x6d696c00, 
			0x7379735f, 0x6f6e6d6d, 0x696c0063, 0x63705f6d, 0x00636f6e, 
			0x5f6d696c, 0x00757063, 0x5f6b6c63, 0x746c6f76, 0x726f635f, 
			0x6c43006e, 0x536b636f, 0x6372756f, 0x67007365, 0x725f6363, 
			0x705f6d70, 0x5f636f72, 0x6b6c6366, 0x63636700, 0x7379735f, 
			0x636f6e5f, 0x6978615f, 0x6b6c635f, 0x63636700, 0x7379735f, 
			0x5f6d6d5f, 0x5f636f6e, 0x5f697861, 0x006b6c63, 0x5f636367, 
			0x6f6e6370, 0x68615f63, 0x6c635f62, 0x6367006b, 0x69625f63, 
			0x645f636d, 0x635f7264, 0x635f3068, 0x67006b6c, 0x625f6363, 
			0x5f636d69, 0x5f757067, 0x006b6c63, 0x434d4942, 0x636f6c43, 
			0x616c506b, 0x6700736e, 0x625f6363, 0x5f636d69, 0x73737061, 
			0x7563745f, 0x6b6c635f, 0x63636700, 0x7370615f, 0x78615f73, 
			0x6c635f69, 0x6367006b, 0x736d5f63, 0x36715f73, 0x6d69625f, 
			0x78615f63, 0x6c635f69, 0x6367006b, 0x64715f63, 0x615f7373, 
			0x6c635f74, 0x6367006b, 0x64715f63, 0x745f7373, 0x65636172, 
			0x696b6c63, 0x6c635f6e, 0x6367006b, 0x64715f63, 0x735f7373, 
			0x635f6d74, 0x67006b6c, 0x715f6363, 0x5f737364, 0x74637374, 
			0x69645f72, 0x635f3276, 0x67006b6c, 0x725f6363, 0x72706362, 
			0x6b6c635f, 0x63636700, 0x6d70735f, 0x68615f69, 0x6c635f62, 
			0x6367006b, 0x70735f63, 0x735f696d, 0x635f7265, 0x67006b6c, 
			0x695f6363, 0x635f6170, 0x67006b6c, 0x765f6363, 0x5f616464, 
			0x746c6f76, 0x5f656761, 0x6f6f7264, 0x65645f70, 0x74636574, 
			0x675f726f, 0x306c6c70, 0x6b6c635f, 0x63636700, 0x5f786d5f, 
			0x746c6f76, 0x5f656761, 0x6f6f7264, 0x65645f70, 0x74636574, 
			0x675f726f, 0x306c6c70, 0x6b6c635f, 0x6f6c4300, 0x6f4c6b63, 
			0x66654467, 0x746c7561, 0x6c430073, 0x566b636f, 0x52676572, 
			0x4d6c6961, 0x43007061, 0x6b636f6c, 0x434d4942, 0x4f4e4d4d, 
			0x70614d43, 0x46454400, 0x544c5541, 0x4552465f, 0x4e455551, 
			0x51005943, 0x454d4954, 0x43415f52, 0x5341425f, 0x54510045, 
			0x52454d49, 0x5341425f, 0x00000045, 0x00000001, 0xffffffff, 
			0xffffffff, 0x00000001, 0xffffffff, 0xffffffff, 0x00000001, 
			0xffffffff, 0xffffffff, 0x00000001, 0xffffffff, 0xffffffff, 
			0x00000001, 0xffffffff, 0xffffffff, 0x00000000, 0xffffffff, 
			0x00000012, 0x80000000, 0x00000000, 0xff00ff00, 0x00000002, 
			0x80000007, 0x02000000, 0x00000002, 0x8000001a, 0x00000004, 
			0x00000002, 0x80000020, 0x00000030, 0xff00ff00, 0x00000008, 
			0x8000002a, 0x01010106, 0x01000100, 0xff00ff00, 0x00000014, 
			0x8000003b, 0x00000000, 0x00000014, 0x80000044, 0x0000000c, 
			0x00000014, 0x8000004d, 0x00000018, 0x00000014, 0x8000005a, 
			0x00000024, 0x00000014, 0x80000064, 0x00000030, 0x00000014, 
			0x8000006c, 0x0000003c, 0x00000012, 0x8000007a, 0x00000001, 
			0x00000012, 0x80000087, 0x00000002, 0x00000012, 0x80000099, 
			0x00000003, 0x00000012, 0x800000ad, 0x00000004, 0x00000012, 
			0x800000c4, 0x00000005, 0x00000012, 0x800000d6, 0x00000006, 
			0x00000012, 0x800000eb, 0x00000007, 0x00000012, 0x800000fc, 
			0x00000008, 0x00000012, 0x8000010b, 0x00000009, 0x00000012, 
			0x80000121, 0x0000000a, 0x00000012, 0x80000132, 0x0000000b, 
			0x00000012, 0x8000014a, 0x0000000c, 0x00000012, 0x8000015a, 
			0x0000000d, 0x00000012, 0x80000172, 0x0000000e, 0x00000012, 
			0x80000183, 0x0000000f, 0x00000012, 0x8000019b, 0x00000010, 
			0x00000012, 0x800001a9, 0x00000011, 0x00000012, 0x800001ba, 
			0x00000012, 0x00000012, 0x800001cb, 0x00000013, 0x00000012, 
			0x800001d7, 0x00000014, 0x00000012, 0x80000201, 0x00000014, 
			0x00000012, 0x80000229, 0x00000015, 0x00000012, 0x8000023a, 
			0x00000016, 0x00000012, 0x8000024b, 0x00000017, 0xff00ff00, 
			0x00000012, 0x00000001, 0x00000018, 0x00000012, 0x00000002, 
			0x00000019, 0x00000012, 0x00000003, 0x0000001a, 0x00000012, 
			0x00000004, 0x0000001b, 0x00000012, 0x00000005, 0x0000001c, 
			0x00000012, 0x00000029, 0x0000001d, 0x00000012, 0x00000006, 
			0x0000001e, 0x00000012, 0x0000001e, 0x0000001f, 0x00000012, 
			0x00000007, 0x00000020, 0x00000012, 0x00000008, 0x00000021, 
			0x00000012, 0x0000002a, 0x00000022, 0x00000012, 0x00000009, 
			0x00000023, 0x00000012, 0x0000000a, 0x00000024, 0x00000012, 
			0x0000000b, 0x00000025, 0x00000012, 0x0000000c, 0x00000026, 
			0x00000012, 0x0000000d, 0x00000027, 0x00000012, 0x0000000e, 
			0x00000028, 0x00000012, 0x0000001f, 0x00000029, 0x00000012, 
			0x0000000f, 0x0000002a, 0x00000012, 0x00000010, 0x0000002b, 
			0x00000012, 0x00000028, 0x0000002c, 0x00000012, 0x00000011, 
			0x0000002d, 0x00000012, 0x00000012, 0x0000002e, 0x00000012, 
			0x00000013, 0x0000002f, 0x00000012, 0x00000014, 0x00000030, 
			0x00000012, 0x00000015, 0x00000031, 0x00000012, 0x00000027, 
			0x00000032, 0x00000012, 0x00000025, 0x00000033, 0x00000012, 
			0x00000026, 0x00000034, 0x00000012, 0x00000024, 0x00000035, 
			0x00000012, 0x00000020, 0x00000036, 0x00000012, 0x00000021, 
			0x00000037, 0x00000012, 0x00000022, 0x00000038, 0x00000012, 
			0x00000023, 0x00000039, 0x00000012, 0x00000030, 0x0000003a, 
			0x00000012, 0x0000002b, 0x0000003b, 0x00000012, 0x0000002c, 
			0x0000003c, 0x00000012, 0x0000002e, 0x0000003d, 0x00000012, 
			0x0000002f, 0x0000003e, 0x00000012, 0x00000016, 0x0000003f, 
			0x00000012, 0x00000017, 0x00000040, 0x00000012, 0x00000018, 
			0x00000041, 0x00000012, 0x00000019, 0x00000042, 0x00000012, 
			0x0000001a, 0x00000043, 0x00000012, 0x0000001b, 0x00000044, 
			0x00000012, 0x0000001c, 0x00000045, 0xff00ff00, 0x00000012, 
			0x00000065, 0x00000046, 0x00000012, 0x00000067, 0x00000047, 
			0x00000012, 0x00000086, 0x00000048, 0x00000012, 0x00000087, 
			0x00000049, 0x00000012, 0x00000074, 0x0000004a, 0x00000012, 
			0x0000006e, 0x0000004b, 0x00000012, 0x0000006d, 0x0000004c, 
			0x00000012, 0x00000071, 0x0000004d, 0x00000012, 0x0000012d, 
			0x0000004e, 0x00000012, 0x0000012e, 0x0000004f, 0x00000012, 
			0x0000012f, 0x00000050, 0x00000012, 0x00000130, 0x00000051, 
			0x00000012, 0x00000075, 0x00000052, 0x00000012, 0x00000076, 
			0x00000053, 0x00000012, 0x00000072, 0x00000054, 0x00000012, 
			0x00000073, 0x00000055, 0xff00ff00, 0x00000002, 0x8000025d, 
			0x0124f800, 0x00000002, 0x8000026f, 0x00082000, 0x00000002, 
			0x8000027e, 0x00084000, 0xff00ff00, 0x00000012, 0x00000001, 
			0x00000056, 0xff00ff00 };



 const StringDevice driver_list[] = {
			{"/dev/icb/rpm",2545934574u, 760, NULL, 0, NULL },
			{"DALDEVICEID_SPMI_DEVICE",3290583706u, 776, NULL, 0, NULL },
			{"/core/mproc/smd",1450287328u, 816, NULL, 0, NULL },
			{"/rpm/pmic/common",2887753651u, 1200, NULL, 0, NULL },
			{"/rpm/pmic/target",3536625265u, 1756, NULL, 0, NULL },
			{"/sysdrivers/vsense",192366965u, 1992, NULL, 0, NULL }
};


 const DALProps DALPROP_PropsInfo = {(const byte*)DALPROP_PropBin, DALPROP_StructPtrs , 6, driver_list};
