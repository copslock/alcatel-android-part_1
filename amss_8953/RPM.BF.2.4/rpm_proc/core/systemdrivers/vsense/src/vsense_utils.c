#include "com_dtypes.h"


uint64 vsense_convert_time_to_timetick(uint64 time_us)
{
    return (time_us*19200000)/1000000;
}

uint64 vsense_convert_timetick_to_time(uint64 time_tick)
{

    return (time_tick*1000000)/19200000;
}

