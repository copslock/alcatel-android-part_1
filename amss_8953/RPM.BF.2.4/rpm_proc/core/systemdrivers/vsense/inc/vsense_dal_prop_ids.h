#ifndef VSENSE_DAL_PROP_IDS_H
#define VSENSE_DAL_PROP_IDS_H
/*! \file vsense_dal_prop_ids.h
 *  
 *  \brief   This file contains all the available Vsense DAL device config property values.
 *  \details This file contains all the available Vsense DAL device config property values.
 *  
 *  &copy; Copyright 2014-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/14   aks      Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

//Vsense  specific dal properties -- starting from 1
#define VSENSE_PROP_CONFIG                                  1
#define VSENSE_PROP_FUSE1_VOLT                              2
#define VSENSE_PROP_FUSE2_VOLT                              3
#define VSENSE_PROP_MODE                                    4

 
 
 #endif 

