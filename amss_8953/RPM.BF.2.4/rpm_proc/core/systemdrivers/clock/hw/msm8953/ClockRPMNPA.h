#ifndef CLOCKRPMNPA_H
#define CLOCKRPMNPA_H
/*
===========================================================================
*/
/**
  @file ClockRPMNPA.h

  NPA node definitions for the MSM8974 RPM clock driver.
*/
/*
  ====================================================================

  Copyright (c) 2011 - 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/rpm.bf/2.4/core/systemdrivers/clock/hw/msm8953/ClockRPMNPA.h#3 $
  $DateTime: 2015/12/29 04:09:27 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  11/10/12   vtw     Ported from 8974, updated for 8x10.

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include <DALSys.h>
#include <railway.h>
#include <npa.h>
#include <npa_resource.h>
#include "ClockDriver.h"


/*=========================================================================
      Macros
==========================================================================*/

/*=========================================================================
      Type Definitions
==========================================================================*/

#define XO_LEVEL                0
/*
 * BIMCStateType
 *
 * BIMC enum state type
 */
typedef enum
{
  BIMC_ON,           /* BIMC is on */
  BIMC_SAVING,       /* BIMC is in power collapsed, save DEHR */
  BIMC_COLLAPSED,    /* BIMC is in power collapsed */
  BIMC_RESTORING,    /* BIMC is in restoring state */
} BIMCStateType;

/*
 * ClockQDSSSpeedType
 *
 * QDSS enum speed type
 */
typedef enum
{
  CLOCK_QDSS_SPEED_XO,
  CLOCK_QDSS_SPEED_LOW_MINUS,
  CLOCK_QDSS_SPEED_LOW,
  CLOCK_QDSS_SPEED_NORMAL,
  CLOCK_QDSS_NUM_SPEEDS,
} ClockQDSSSpeedType;


/*
 * NPAResourceType
 *
 * Structure containing the NPA clocks resource info
 */
typedef struct
{
  npa_resource_definition Resource;
  npa_node_definition     Node;
} NPAResourceType;

/*
 * ClockResourceType
 *
 * Structure containing the clocks resource info
 */
typedef struct
{
  NPAResourceType         NPANode;
  ClockNodeType           *pClock;
  uint32                  nMinLevel;
  uint32                  nMaxLevel;
  uint32                  nCurLevel;
  uint32                  nSavLevel;
} ClockResourceType;

/*
 * BIMCResourceType *
 * Structure containing the BIMC resource info
 */
typedef struct
{
  boolean                 bPowerCollapsed;  // DDR in PC or self-refresh
  BIMCStateType           eXOBIMCState;     // BIMC state in XO shutdown
  BIMCStateType           eBIMCState;
  ClockIdType             nClockDehr;
  ClockPowerDomainIdType  nBIMCPowerID; 
  uint32                 *mBIMCMMNOCMap;   /**<BIMC MMNOC mapping table>*/  
} BIMCResourceType;

/*
 * ClockFreqPlanResourceType
 *
 * Structure containing the clocks resource info
 */
typedef struct
{
  NPAResourceType         NPANode;
  uint32                  nCurrentPlan;
  uint32                  nPlanMax;
  const uint32            **ppPlanFreqs;
} ClockFreqPlanResourceType;

/*
 * ClockQDSSResourceType
 *
 * Structure containing the QDSS clocks info
 */
typedef struct
{
  NPAResourceType         NPANode;
  boolean                 bTraceAccess;
  boolean                 bQDSSEnabled;
  ClockVRegLevelType      nQDSSVoltageLevel;
  ClockQDSSLevelType      nQDSSCurrLevel;
  ClockQDSSSpeedType      nQDSSCurrSpeed;
  ClockQDSSSpeedType      nQDSSSaveSpeed;
  ClockNodeType           *pQDSSCfgAHBClk;
  ClockNodeType           *pQDSSDapAHBClk;
  ClockNodeType           *pQDSSEtrUSBClk;
  ClockNodeType           *pQDSSATClk;
  ClockNodeType           *pQDSSSysNocATClk;
  ClockNodeType           *pQDSSPCNocATClk;
  ClockNodeType           *pQDSSTraceClk;
  ClockNodeType           *pQDSSSTMClk;
  ClockNodeType           *pQDSSSTMAXIClk;
  ClockNodeType           *pQDSSDapClk;
  ClockNodeType           *pQDSSAPB2JTAGClk;
  ClockNodeType           *pQDSSTSCTRDiv2Clk;
  ClockNodeType           *pQDSSTSCTRDiv3Clk;
  ClockNodeType           *pQDSSTSCTRDiv4Clk;
  ClockNodeType           *pQDSSTSCTRDiv8Clk;
  ClockNodeType           *pQDSSTSCTRDiv16Clk;
} ClockQDSSResourceType;


/*
* IPAResourceType *
* Structure containing the IPA resource info
*/
typedef struct
{
  boolean                 bEnable;          // IPA enable
  ClockNodeType           *pIPAAHBClk;
  ClockNodeType           *pIPASysAxiClk;
  ClockNodeType           *pIPATBUClk;
  ClockNodeType           *pIPA2XClk;
  ClockNodeType           *pIPASleepClk;
  npa_client_handle       XOHandle;
  npa_client_handle       BIMCHandle;
} IPAResourceType;



/*
 * Clock_NPAResourcesType
 *
 * Structure containing the NPA node and resource data for local nodes.
 */
typedef struct
{
  uint32                  vol_level;
  int                     nCXRailID;
  uint32                  eGFXmicroVolts;
  NPAResourceType         CXONPANode;
  NPAResourceType         DCVSEnaNPANode;
  ClockResourceType       CPUClockResource;
  ClockResourceType       SNOCClockResource;
  ClockResourceType       SysMMNOCClockResource;
  ClockResourceType       PCNOCClockResource;
  BIMCResourceType        BIMCResource;
  ClockResourceType       BIMCClockResource;
  ClockResourceType       APSSTCUASYNCClockResource;
  ClockResourceType       Q6TBUClockResource;
  ClockResourceType       BIMCGPUClockResource; 
  ClockResourceType       APSSAXIClockResource;  
  ClockQDSSResourceType   QDSSClockResource;
  ClockResourceType       IPAClockResource;
  IPAResourceType         IPAResource;
  ClockFreqPlanResourceType BIMCFreqPlanResource;
} Clock_NPAResourcesType;


/*
 * Clock_DebugCfgType
 *
 * Structure containing the clock resource pointer and the string used in the clockbsp.xml
 */

typedef struct 
{
  ClockResourceType* ClockResDbg;
  char* pDbgDalStrProp;
} Clock_DebugCfgType;
/*=========================================================================
      Prototypes
==========================================================================*/

/*===========================================================================

  FUNCTION      Clock_SetQDSSClocks

  DESCRIPTION   This QDSS function is used for toggle the QDSS clocks ON/OFF

  PARAMETERS    nCurrentLevel - Current QDSS level
                nNewLevel - New QDSS level

  DEPENDENCIES  None.

  RETURN VALUE  None

  SIDE EFFECTS  None.

===========================================================================*/
void Clock_SetQDSSClocks
(
  ClockQDSSResourceType *pQDSSResource,
  ClockQDSSSpeedType eSpeed
);


/*===========================================================================

  FUNCTION      Clock_ChangeQDSSLevel

  DESCRIPTION   This function is used for change QDSS clock to new Level

  PARAMETERS    eCurLevel - (0=Off, 1=Debug, 2=High Speed)
                eNewLevel - (0=Off, 1=Debug, 2=High Speed)

  DEPENDENCIES  None.

  RETURN VALUE  New Level.

  SIDE EFFECTS  None.

===========================================================================*/
ClockQDSSLevelType Clock_ChangeQDSSLevel
(
  ClockQDSSResourceType *pQDSSResource,
  ClockQDSSLevelType    eCurLevel,
  ClockQDSSLevelType    eNewLevel
);


/*===========================================================================

  FUNCTION      Clock_InitNPA

  DESCRIPTION   This function is used setup NPA node to all the clock
                resource

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None

  SIDE EFFECTS  None.

===========================================================================*/
DALResult Clock_InitNPA
(
  void
);


/*===========================================================================

  FUNCTION      Clock_GetNPAResources

  DESCRIPTION   This function is for getting the point to all clocks NPA node

  PARAMETERS    None

  DEPENDENCIES  None.

  RETURN VALUE  Pointer to all clocks NPA resource

  SIDE EFFECTS  None.

===========================================================================*/
Clock_NPAResourcesType* Clock_GetNPAResources
(
  void
);

/*===========================================================================

  FUNCTION      Clock_SetClockSpeed

  DESCRIPTION   This function is for setting the clock speed
                  
  PARAMETERS    pClockResource [in] - Clock to be change
                nFreqHz [in]        - New clock speed

  DEPENDENCIES  None.

  RETURN VALUE  New clock speed

  SIDE EFFECTS  None.

===========================================================================*/
npa_resource_state Clock_SetClockSpeed
(
  ClockResourceType   *pClockResource,
  uint32              nFreqHz
);

/*===========================================================================

  FUNCTION      Clock_DalDebugInit

  DESCRIPTION   This function is for setting the min and max frequencies of the clocks
                            based on the value provided in the clockchipset.xml
  PARAMETERS    None

  DEPENDENCIES  None.

  RETURN VALUE  New clock speed

  SIDE EFFECTS  None.

===========================================================================*/


boolean Clock_DalDebugInit (Clock_NPAResourcesType  *pNPAResources);

/*===========================================================================
  FUNCTION      Clock_SetQDSSDebugSpeed

  DESCRIPTION   This function is for setting up frequencies of the clocks
                            based on corner
  PARAMETERS    None

  DEPENDENCIES  None.

  RETURN VALUE  New clock speed

  SIDE EFFECTS  None.

===========================================================================*/
static void Clock_SetQDSSDebugSpeed(
  const railway_settings  *settings,
  ClockQDSSResourceType   *pQDSSResource,
  boolean                 bForceSwitch
);

#endif /* #ifndef CLOCKRPMNPA_H */
