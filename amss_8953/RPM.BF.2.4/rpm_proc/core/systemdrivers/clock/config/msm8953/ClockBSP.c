/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock regime bsp data for DAL based driver.

==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/clock/config/msm8953/ClockBSP.c#4 $

when       who     what, where, why

10/24/12   kma     Ported from 8x26 project.

==============================================================================
            Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{

  /*-----------------------------------------------------------------------*/
  /* XO                                                                    */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_XO,
    /* .HALConfig          = */ { HAL_CLK_SOURCE_NULL },
    /* .nConfigMask        = */ 0,
    /* .nFreqHz            = */ 19200 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL0 - General purpose PLL                                           */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_GPLL0,
    /* .HALConfig            */ {
    /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_XO,
    /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO2,
    /* .HALConfig.nPreDiv  = */   1,
    /* .HALConfig.nPostDiv = */   1,
    /* .HALConfig.nL       = */   41,
    /* .HALConfig.nM       = */   0,
    /* .HALConfig.nN       = */   0,
    /* .nVCOMultiplier     = */   0,
    /* .nAlpha             = */   0xAA000000,
    /* .nAlphaU            = */   0xAA,
                                },
    /* .nConfigMask        = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    /* .nFreqHz            = */ 800000 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },
 
 
  /*-----------------------------------------------------------------------*/
  /* GPLL0_DIV2 - Branch of GPLL0                                          */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_GPLL0_DIV2,
    /* .HALConfig            */ {
    /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_GPLL0,
    /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO2,
    /* .HALConfig.nPreDiv  = */   0,
    /* .HALConfig.nPostDiv = */   0,
    /* .HALConfig.nL       = */   0,
    /* .HALConfig.nM       = */   0,
    /* .HALConfig.nN       = */   0,
    /* .nVCOMultiplier     = */   0,
    /* .nAlpha             = */   0,
    /* .nAlphaU            = */   0,
                                },
    /* .nConfigMask        = */ 0,
    /* .nFreqHz            = */ 400000 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },
   
  /*-----------------------------------------------------------------------*/
  /* GPLL2 - General Purpose PLL                                        */
  /*-----------------------------------------------------------------------*/

  {
      /* .eSource            = */ HAL_CLK_SOURCE_GPLL2,
      /* .HALConfig            */ {
      /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_XO,
      /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO2,
      /* .HALConfig.nPreDiv  = */   1,
      /* .HALConfig.nPostDiv = */   1,
      /* .HALConfig.nL       = */   48,
      /* .HALConfig.nM       = */   0,
      /* .HALConfig.nN       = */   0,
      /* .nVCOMultiplier     = */   0,
      /* .nAlpha             = */   0x00000000,
      /* .nAlphaU            = */   0x70,
                                   },
      /* .nConfigMask        = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
      /* .nFreqHz            = */ 930000 * 1000,
      /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL4 - General purpose PLL                                           */
  /*-----------------------------------------------------------------------*/

  {
      /* .eSource            = */ HAL_CLK_SOURCE_GPLL4,
      /* .HALConfig            */ {
      /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_XO,
      /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO0,
      /* .HALConfig.nPreDiv  = */   1,
      /* .HALConfig.nPostDiv = */   1,
      /* .nL                 = */   60,
      /* .nM                 = */   0,
      /* .nN                 = */   0,
      /* .nVCOMultiplier     = */   0,
      /* .nAlpha             = */   0x00000000,
      /* .nAlphaU            = */   0x00
                                   },
      /* .nConfigMask        = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
      /* .nFreqHz            = */ 1152000 * 1000,
      /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  /*-----------------------------------------------------------------------*/
  /* GPLL5											 */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_GPLL5,
    /* .HALConfig            */ {
    /* .HALConfig.eSource  = */  HAL_CLK_SOURCE_XO,
    /* .HALConfig.eVCO	   = */  HAL_CLK_PLL_VCO1, /*DUMMY, BRAMMO PLL runs on single PLL config*/
    /* .HALConfig.nPreDiv  = */	 1,
    /* .HALConfig.nPostDiv = */	 1,
    /* .HALConfig.nL       = */  40,
    /* .HALConfig.nM       = */  0,
    /* .HALConfig.nN       = */  0,
    /* .nVCOMultiplier     = */  0,
    /* .nAlpha             = */  0,
    /* .nAlphaU            = */  0,
                                },
    /* .nConfigMask        = */ 0,
    /* .nFreqHz            = */ 768000 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },


  /*-----------------------------------------------------------------------*/
  /* BIMC_PLL        */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_BIMCPLL,
    /* .HALConfig            */ {
    /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_XO,
    /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO1, /*DUMMY, BRAMMO PLL runs on single PLL config*/
    /* .HALConfig.nPreDiv  = */   1,
    /* .HALConfig.nPostDiv = */   1,
    /* .nL                 = */   48,
    /* .nM                 = */   0,
    /* .nN                 = */   0,
    /* .nVCOMultiplier     = */   0,
    /* .nAlpha             = */   0x00000000,
    /* .nAlphaU            = */   0x80
                                },
    /* .nConfigMask        = */ 0,
    /* .nFreqHz            = */ 931200 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },
  
  /*-----------------------------------------------------------------------*/
  /* GPLL6        */
  /*-----------------------------------------------------------------------*/

  {
    /* .eSource            = */ HAL_CLK_SOURCE_GPLL6,
    /* .HALConfig            */ {
    /* .HALConfig.eSource  = */   HAL_CLK_SOURCE_XO,
    /* .HALConfig.eVCO     = */   HAL_CLK_PLL_VCO0, /*DUMMY, BRAMMO PLL runs on single PLL config*/
    /* .HALConfig.nPreDiv  = */   1,
    /* .HALConfig.nPostDiv = */   1,
    /* .nL                 = */   56,
    /* .nM                 = */   0,
    /* .nN                 = */   0,
    /* .nVCOMultiplier     = */   0,
    /* .nAlpha             = */   0x00000000,
    /* .nAlphaU            = */   0x40,
                                },
    /* .nConfigMask        = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    /* .nFreqHz            = */ 1080000 * 1000,
    /* .eVRegLevel         = */ CLOCK_VREG_LEVEL_LOW_MINUS,
  },

  { HAL_CLK_SOURCE_NULL }

};


/* =========================================================================
**    nFreqHz       { eSource, nDiv2x, nM, nN, n2D },      eVRegLevel
** =========================================================================*/

const ClockMuxConfigType RPMClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0_DIV2, 6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 228570000, { HAL_CLK_SOURCE_GPLL0,      7, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 266670000, { HAL_CLK_SOURCE_GPLL0,      6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 400000000, { HAL_CLK_SOURCE_GPLL0,      4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL},
  { 432000000, { HAL_CLK_SOURCE_GPLL6,      5, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * System NOC clock configurations
 */
const ClockMuxConfigType SystemNOCClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,          1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  50000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0,      16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 133330000, { HAL_CLK_SOURCE_GPLL0,      12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 160000000, { HAL_CLK_SOURCE_GPLL0,      10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,       8, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 228570000, { HAL_CLK_SOURCE_GPLL0,       7, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS }, 
  { 266670000, { HAL_CLK_SOURCE_GPLL0,       6, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * System MMNOC AXI clock configurations.
 */
const ClockMuxConfigType SystemMMNOCClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  80000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 160000000, { HAL_CLK_SOURCE_GPLL0, 10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,  8, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 240000000, { HAL_CLK_SOURCE_GPLL6,  9, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 320000000, { HAL_CLK_SOURCE_GPLL0,  5, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 360000000, { HAL_CLK_SOURCE_GPLL6,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS },
  { 400000000, { HAL_CLK_SOURCE_GPLL0,  4, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * Peripheral Config NOC clock configurations.
 */
const ClockMuxConfigType PCNOClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  50000000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  {  80000000, { HAL_CLK_SOURCE_GPLL0, 20, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 114290000, { HAL_CLK_SOURCE_GPLL0, 14, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * IPA clock configurations.
 * dividers corresponding Div2X clock but frequency will be 1x
 */
const ClockMuxConfigType IPAClockConfig[] =
{
  {   9600000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  40000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  80000000, { HAL_CLK_SOURCE_GPLL0, 10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 154290000, { HAL_CLK_SOURCE_GPLL6,  7, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL},
  { 0 }
};

/*
 * VS clock configurations.
 */
const ClockMuxConfigType VSClockConfig[] =
{
  {   19200000,   { HAL_CLK_SOURCE_XO,    1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  400000000,   { HAL_CLK_SOURCE_GPLL0, 4, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  {  576000000,   { HAL_CLK_SOURCE_GPLL4, 4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * BIMC configurations. The PLL number is changed at runtime.
 */
static ClockSourceConfigType BIMCPLLConfig[] =
{
  /* ============================================================================================================
  **  eSource, { eSource, eVCO, nPreDiv, nPostDiv, nL, nM, nN, nVCOMultipler, nAlpha, nAlpahU}, nConfigMask, nFreqHz, eVRegLevel, bInit, bOwner
  ** ============================================================================================================*/
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 29,  0,  1, 0, 0, 0 }, 0, 556800000}, /* Index =  0 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 31,  0,  1, 0, 0, 0x80}, 0, 604800000}, /* Index =  1 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 33,  0,  1, 0, 0, 0 }, 0,  633600000}, /* Index =  2 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 35,  0,  1, 0, 0, 0 }, 0,  672000000}, /* Index =  3 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 40,  0,  1, 0, 0, 0 }, 0,  768000000}, /* Index =  4 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 42,  0,  1, 0, 0, 0 }, 0,  806400000}, /* Index =  5 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 44,  0,  1, 0, 0, 0 }, 0,  844800000}, /* Index =  6 */
  { HAL_CLK_SOURCE_RAW1, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 1, 48,  0,  1, 0, 0, 0x80 }, 0, 931200000}, /* Index =  7 */
};


/*
 * ClockBIMCMMNOCMapConfig.
 */
const uint32 ClockBIMCMMNOCMapConfig[] =
{
  19200000,    /*BIMC = 19.20Mhz*/
  80000000,    /*BIMC = 100.80Mhz*/
  80000000,    /*BIMC = 211.20Mhz*/
  160000000,   /*BIMC = 278.40Mhz*/
  160000000,   /*BIMC = 384.00Mhz*/
  160000000,   /*BIMC = 422.40Mhz*/
  240000000,   /*BIMC = 556.80Mhz*/
  240000000,   /*BIMC = 672.00Mhz*/
  240000000,   /*BIMC = 768.00Mhz*/
  320000000,   /*BIMC = 806.40Mhz*/
  320000000,   /*BIMC = 844.80Mhz*/
  360000000    /*BIMC = 931.20Mhz*/
};



/*
 * BIMC clock configurations.
 * Program Dividers for 2x Clock.
 While adding or removing any  perf level update the below as well:
 1) ClockBIMCMMNOCMapConfig
 2) BIMCGPUClockConfig
 3) APSSAXIClockConfig
 4) APSSTCUASYNCClockConfig
 5) Q6TBUClockConfig
 */
ClockMuxConfigType BIMCClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,    2, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100800000, { HAL_CLK_SOURCE_RAW1, 12, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[1]},
  { 211200000, { HAL_CLK_SOURCE_RAW1,  6, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[2]},
  { 278400000, { HAL_CLK_SOURCE_RAW1,  4, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[0]},
  { 384000000, { HAL_CLK_SOURCE_RAW1,  4, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[4]},
  { 422400000, { HAL_CLK_SOURCE_RAW1,  4, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[6]},
  { 556800000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[0]},
  { 672000000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[3]},
  { 768000000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[4]},
  { 806400000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[5]},
  { 844800000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[6]},
  { 931200000, { HAL_CLK_SOURCE_RAW1,  2, 1,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS, 0, CHIPINFO_FAMILY_UNKNOWN, &BIMCPLLConfig[7]},
  { 0 }
};

/*
 * BIMC GPU clock configurations.
 */
ClockMuxConfigType BIMCGPUClockConfig[] =
{
  {   9600000, { HAL_CLK_SOURCE_XO,    4, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  50400000, { HAL_CLK_SOURCE_RAW1, 24, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 105600000, { HAL_CLK_SOURCE_RAW1, 12, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 139200000, { HAL_CLK_SOURCE_RAW1,  8, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 192000000, { HAL_CLK_SOURCE_RAW1,  8, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW},
  { 211200000, { HAL_CLK_SOURCE_RAW1,  8, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW},
  { 278400000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS},
  { 336000000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS},
  { 384000000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS},
  { 403200000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL},
  { 422400000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL},
  { 465600000, { HAL_CLK_SOURCE_RAW1,  4, 0,  0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS},
  { 0 }
};


ClockMuxConfigType APSSAXIClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0_DIV2, 6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0_DIV2, 6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 266670000, { HAL_CLK_SOURCE_GPLL0,      6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },  
  { 266670000, { HAL_CLK_SOURCE_GPLL0,      6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW},
  { 266670000, { HAL_CLK_SOURCE_GPLL0,      6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 320000000, { HAL_CLK_SOURCE_GPLL0,      5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS },
  { 320000000, { HAL_CLK_SOURCE_GPLL0,      5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS},
  { 320000000, { HAL_CLK_SOURCE_GPLL0,      5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS},
  { 400000000, { HAL_CLK_SOURCE_GPLL0,      4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL},
  { 400000000, { HAL_CLK_SOURCE_GPLL0,      4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL},
  { 465000000, { HAL_CLK_SOURCE_GPLL2,      4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL_PLUS},
  { 0 } 
};

/* 
 * The following clock plans need to match exactly the frequencys in BIMCClockConfig[].
 * 
 * A: 19.2, 100.8, 211.2, 278.4, 384, 422.4, 556.8, 672, 768, 806.4, 844.8, 931.2
 * B: 19.2, 100.8, 211.2, 278.4, 384, 422.4, 556.8, 672, 768, 806.4, 844.8, 931.2
 */
const uint32 BIMCClockPlanData[2][13] = 
{
  /* Plan A */ {19200000, 100800000, 211200000, 278400000, 384000000, 422400000, 556800000, 672000000, 768000000, 806400000, 844800000, 931200000, 0},
  /* Plan B */ {19200000, 100800000, 211200000, 278400000, 384000000, 422400000, 556800000, 672000000, 768000000, 806400000, 844800000, 931200000, 0},
};
const uint32 *BIMCClockPlans[] = {
  BIMCClockPlanData[0], 
  BIMCClockPlanData[1], 
  NULL 
};

/* 
 * gcc_apss_tcu_async_clk BSP configurations
 * This clock is sclaed with BIMC according to the BIMC voltage level.
 * No separate NPA node for this and hence needs to be taken care of along with BIMC.
 */
const ClockMuxConfigType APSSTCUASYNCClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO, 1, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0_DIV2, 6, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0_DIV2, 6, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 266670000, { HAL_CLK_SOURCE_GPLL0,  6, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW },  
  { 266670000, { HAL_CLK_SOURCE_GPLL0,  6, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW},
  { 266670000, { HAL_CLK_SOURCE_GPLL0,  6, 0,  0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 320000000, { HAL_CLK_SOURCE_GPLL0,  5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS },
  { 320000000, { HAL_CLK_SOURCE_GPLL0,  5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS},
  { 320000000, { HAL_CLK_SOURCE_GPLL0,  5, 0, 0, 0 },  CLOCK_VREG_LEVEL_LOW_PLUS},
  { 400000000, { HAL_CLK_SOURCE_GPLL0,  4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL},
  { 400000000, { HAL_CLK_SOURCE_GPLL0,  4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL},
  { 465000000, { HAL_CLK_SOURCE_GPLL2,  4, 0, 0, 0 },  CLOCK_VREG_LEVEL_NOMINAL_PLUS},
   { 0 }
};  

/*
* Q6 TBU/ clock configurations.
 */
const ClockMuxConfigType Q6TBUClockConfig[] =
{
   { 19200000, { HAL_CLK_SOURCE_XO,          1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
   {133330000, { HAL_CLK_SOURCE_GPLL0_DIV2,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
   {133330000, { HAL_CLK_SOURCE_GPLL0_DIV2,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
   {266670000, { HAL_CLK_SOURCE_GPLL0,    6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
   {266670000, { HAL_CLK_SOURCE_GPLL0,    6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
   {266670000, { HAL_CLK_SOURCE_GPLL0,    6, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
   {320000000, { HAL_CLK_SOURCE_GPLL0,    5, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
   {320000000, { HAL_CLK_SOURCE_GPLL0,    5, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS },
   {320000000, { HAL_CLK_SOURCE_GPLL0,    5, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_PLUS }, 
   {400000000, { HAL_CLK_SOURCE_GPLL0,    4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL }, 
   {400000000, { HAL_CLK_SOURCE_GPLL0,    4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL }, 
   {465000000, { HAL_CLK_SOURCE_GPLL2,    4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL_PLUS },
   { 0 }
};  

/*
 * QDSS AT clock configurations.
 */
const ClockMuxConfigType QDSSATClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  66670000, { HAL_CLK_SOURCE_GPLL0_DIV2, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 133330000, { HAL_CLK_SOURCE_GPLL0,  12, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 266670000, { HAL_CLK_SOURCE_GPLL0,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * QDSS Trace clock configurations.
 * Using GPLL0 we can get 160/320 but it would need fractional dividers
 * which causes large jitter. This is jitter sensitive path having half cycle paths, 
 * i.e. flop - flop data should travel in half clock period
 * Hence we should not use GPLL0
 */
const ClockMuxConfigType QDSSTraceClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  80000000, { HAL_CLK_SOURCE_GPLL0_DIV2,  10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 160000000, { HAL_CLK_SOURCE_GPLL0,  10, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 308570000, { HAL_CLK_SOURCE_GPLL6,   7, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * QDSS STM clock configurations.
 */
const ClockMuxConfigType QDSSSTMClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  50000000, { HAL_CLK_SOURCE_GPLL0_DIV2,  16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 100000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 200000000, { HAL_CLK_SOURCE_GPLL0,  8, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 266670000, { HAL_CLK_SOURCE_GPLL0,  6, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * QDSS TSCTR Div-2 clock configurations.
 * The divider configs correspond to Div1 because 
 * there is a static divider of 2 after the mux. So 
 * below frequencies match with div2 of the freq after mux.
 */
const ClockMuxConfigType QDSSTSCTRDiv2ClockConfig[] =
{
  {   9600000,  { HAL_CLK_SOURCE_XO,         1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   8000000,  { HAL_CLK_SOURCE_GPLL0_DIV2, 5, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 160000000, { HAL_CLK_SOURCE_GPLL0,      5, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 288000000, { HAL_CLK_SOURCE_GPLL4,      4, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * RBCPR clock configurations.
 */
const ClockMuxConfigType RBCPRClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,     1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 50000000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  { 0 }
};


/*
 * SPMI AHB clock configurations.
 */
const ClockMuxConfigType SPMIAHBClockConfig[] =
{
  { 25000000, { HAL_CLK_SOURCE_GPLL0_DIV2, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },	
  { 50000000, { HAL_CLK_SOURCE_GPLL0, 32, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW },
  {100000000, { HAL_CLK_SOURCE_GPLL0, 16, 0, 0, 0 }, CLOCK_VREG_LEVEL_NOMINAL },
  {133330000, { HAL_CLK_SOURCE_GPLL0, 12, 0, 0, 0 }, CLOCK_VREG_LEVEL_HIGH },
  { 0 }
};

/*
 * SPMI SER clock configurations.
 */
const ClockMuxConfigType SPMISERClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,  1, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 0 }
};

/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};
/*
 * Mapping for Vreg and Railway driver levels.
 *
 */

rail_voltage_level ClockVregRailMapConfig[CLOCK_VREG_NUM_LEVELS] = 
{
  RAIL_VOLTAGE_LEVEL_OFF,            // CLOCK_VREG_LEVEL_OFF
  RAIL_VOLTAGE_LEVEL_RETENTION,      // CLOCK_VREG_LEVEL_RETENTION
  RAIL_VOLTAGE_LEVEL_SVS_LOW,      // CLOCK_VREG_LEVEL_LOW_MINUS
  RAIL_VOLTAGE_LEVEL_SVS,            // CLOCK_VREG_LEVEL_LOW
  RAIL_VOLTAGE_LEVEL_SVS_HIGH,            // CLOCK_VREG_LEVEL_LOW_PLUS
  RAIL_VOLTAGE_LEVEL_NOMINAL,        // CLOCK_VREG_LEVEL_NOMINAL
  RAIL_VOLTAGE_LEVEL_NOMINAL_HIGH,   // CLOCK_VREG_LEVEL_NOMINAL_PLUS
  RAIL_VOLTAGE_LEVEL_TURBO           // CLOCK_VREG_LEVEL_HIGH
};
