/*
==============================================================================

FILE:         HALclkVDDAVOLTAGEDROOPDETECTOR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   VDDAVOLTAGEDROOPDETECTOR clocks.

   List of clock domains:
     - HAL_clk_mGCCVDDAVOLTAGEDROOPDETECTORClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/hal/clk/hw/msm8953/src/GCC/HALclkVDDAVOLTAGEDROOPDETECTOR.c#2 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mVDDAVOLTAGEDROOPDETECTORClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mVDDAVOLTAGEDROOPDETECTORClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_vdda_voltage_droop_detector_gpll0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_VDDA_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR), HWIO_OFFS(GCC_VDDA_VOLTAGE_DROOP_DETECTOR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_VDDA_VOLTAGE_DROOP_DETECTOR_GPLL0_CLK
  },
};


/*
 * HAL_clk_mGCCVDDAVOLTAGEDROOPDETECTORClkDomain
 *
 * VDDAVOLTAGEDROOPDETECTOR clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCVDDAVOLTAGEDROOPDETECTORClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_VDDA_VOLTAGE_DROOP_DETECTOR_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mVDDAVOLTAGEDROOPDETECTORClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mVDDAVOLTAGEDROOPDETECTORClkDomainClks)/sizeof(HAL_clk_mVDDAVOLTAGEDROOPDETECTORClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

