/*
==============================================================================

FILE:         HALclkGenericPLL.c

DESCRIPTION:
  This file contains the generic clock HAL code for the PLL control.
  These PLLs are of the SR, SR2 and HF PLL variety.

==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/hal/clk/hw/v4/HALclkGenericPLL.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
09/14/12   lil     Modified enabling sequence for SR and SR2 PLLs
02/24/11   gfr     Created

==============================================================================
            Copyright (c) 2011-2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/

#include "HALhwio.h"
#include "HALclkGenericPLL.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Definitions
** ==========================================================================*/

extern HAL_clk_SourceControlType HAL_clk_SparkPLLControl;
extern HAL_clk_SourceControlType HAL_clk_BrammoPLLControl;


/* ===========================================================================
**  HAL_clk_InstallPLL
**
** ======================================================================== */

void HAL_clk_InstallPLL
(
  HAL_clk_SourceType      eSource,
  HAL_clk_PLLContextType *pCtxt,
  uint32                  nBaseAddress
)
{
  /*
   * Add base address
   */
  pCtxt->nAddr += nBaseAddress;

  if (pCtxt->VoterRegister.nAddr != 0)
  {
    pCtxt->VoterRegister.nAddr += nBaseAddress;
  }

  /*
   * Install PLL handler
   */
  
  if (pCtxt->ePLLType == HAL_CLK_PLL_SPARK)
  {
    HAL_clk_InstallSource(eSource, &HAL_clk_SparkPLLControl, pCtxt);
  }
  else
  {
    HAL_clk_InstallSource(eSource, &HAL_clk_BrammoPLLControl, pCtxt);
  }
} /* END HAL_clk_InstallPLL */
