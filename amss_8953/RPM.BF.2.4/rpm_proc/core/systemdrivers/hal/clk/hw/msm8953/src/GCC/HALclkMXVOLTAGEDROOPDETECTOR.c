/*
==============================================================================

FILE:         HALclkMXVOLTAGEDROOPDETECTOR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MXVOLTAGEDROOPDETECTOR clocks.

   List of clock domains:
     - HAL_clk_mGCCMXVOLTAGEDROOPDETECTORClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/hal/clk/hw/msm8953/src/GCC/HALclkMXVOLTAGEDROOPDETECTOR.c#2 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mMXVOLTAGEDROOPDETECTORClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMXVOLTAGEDROOPDETECTORClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_mx_voltage_droop_detector_gpll0_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MX_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR), HWIO_OFFS(GCC_MX_VOLTAGE_DROOP_DETECTOR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ 0
  },
};


/*
 * HAL_clk_mGCCMXVOLTAGEDROOPDETECTORClkDomain
 *
 * MXVOLTAGEDROOPDETECTOR clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCMXVOLTAGEDROOPDETECTORClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_MX_VOLTAGE_DROOP_DETECTOR_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMXVOLTAGEDROOPDETECTORClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMXVOLTAGEDROOPDETECTORClkDomainClks)/sizeof(HAL_clk_mMXVOLTAGEDROOPDETECTORClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

