/*
==============================================================================

FILE:         HALclkMainRPM.c

DESCRIPTION:
  This file contains the main platform initialization code for the clock
  HAL on the RPM processor on MSM8974.

==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/hal/clk/hw/msm8953/src/HALclkMainRPM.c#3 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
01/20/12   vph      Created.

==============================================================================
            Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include "HALclkInternal.h"
#include "HALclkGeneric.h"
#include "HALclkGenericPLL.h"
#include "HALhwio.h"
#include "HALclkHWIO.h"
#include "assert.h"

/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_PlatformInitSources(void);

/* ============================================================================
**    Externs
** ==========================================================================*/

extern void HAL_clk_PlatformInitRPM(void);
extern void HAL_clk_PlatformInitGCCMain(void);


/* ============================================================================
**    Data
** ==========================================================================*/

/*
 * HAL_clk_aInitFuncs
 *
 * Declare array of module initialization functions.
 */
static HAL_clk_InitFuncType HAL_clk_afInitFuncs[] =
{
  /*
   * Sources
   */
  HAL_clk_PlatformInitSources,
  
  /*
   * CPU
   */
  HAL_clk_PlatformInitGCCMain,


  NULL
};


/*
 * Declare the base pointers for HWIO access.
 */
uint32 HAL_clk_nHWIOBaseTop     = CLK_CTL_BASE_PHYS;



/*
 * HAL_clk_aHWIOBases
 *
 * Declare array of HWIO bases in use on this platform.
 */
static HAL_clk_HWIOBaseType HAL_clk_aHWIOBases[] =
{
  { CLK_CTL_BASE_PHYS,  CLK_CTL_BASE_SIZE, &HAL_clk_nHWIOBaseTop },
  { 0, 0, NULL }
};


/*
 * HAL_clk_Platform;
 * Platform data.
 */
HAL_clk_PlatformType HAL_clk_Platform =
{
  HAL_clk_afInitFuncs,
  HAL_clk_aHWIOBases
};


/*
 * GPLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextGPLL[] =
{
  {
    HWIO_OFFS(GCC_GPLL0_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL0),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(GCC_GPLL2_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL2),
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(GCC_GPLL4_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL4),
    HAL_CLK_PLL_SPARK
  },
  {
    HWIO_OFFS(GCC_GPLL5_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },
  {
    HWIO_OFFS(GCC_BIMC_PLL_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },
  {
    HWIO_OFFS(GCC_GPLL6_MODE),
    HAL_CLK_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL6),
    HAL_CLK_PLL_BRAMMO
  },
  
};


/* ============================================================================
**    Functions
** ==========================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitSources
**
** ======================================================================== */

void HAL_clk_PlatformInitSources (void)
{
  /*
   * Install PLL handlers.
   */
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL0,   &HAL_clk_aPLLContextGPLL[0], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL2,   &HAL_clk_aPLLContextGPLL[1], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL4,   &HAL_clk_aPLLContextGPLL[2], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL5,   &HAL_clk_aPLLContextGPLL[3], CLK_CTL_BASE);
	
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_BIMCPLL, &HAL_clk_aPLLContextGPLL[4], CLK_CTL_BASE);
	
  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL6,   &HAL_clk_aPLLContextGPLL[5], CLK_CTL_BASE);

} /* END HAL_clk_PlatformInitSources */


/* ===========================================================================
**  HAL_clk_GCCSourceMapToHW - General Clock Control
**
** ======================================================================== */

uint32 HAL_clk_GCCSourceMapToHW
(
  HAL_clk_SourceType eSource
)
{
  /*
   * Determine the source selection value.
   */
  switch (eSource)
  {
      case HAL_CLK_SOURCE_XO:       return 0;
      case HAL_CLK_SOURCE_GPLL0:    return 1;
      case HAL_CLK_SOURCE_GPLL6:    return 2;
      case HAL_CLK_SOURCE_GPLL2:    return 3;
      case HAL_CLK_SOURCE_GPLL0_DIV2:    return 4;
        
      /* RAW Source clock without any PLL */
      case HAL_CLK_SOURCE_RAW0:     return 0;
      case HAL_CLK_SOURCE_RAW1:     return 1;
      case HAL_CLK_SOURCE_RAW2:     return 2;
    default:
      return 0;
  }

} /* END HAL_clk_GCCSourceMapToHW */


/* ===========================================================================
**  HAL_clk_GCCSourceMapFromHW - General Clock Control
**
** ======================================================================== */

HAL_clk_SourceType HAL_clk_GCCSourceMapFromHW
(
  uint32 nSourceSelect
)
{
  /*
   * Determine the source selection value.
   */
  switch (nSourceSelect)
  {
    case 0: return HAL_CLK_SOURCE_XO;
    case 1: return HAL_CLK_SOURCE_GPLL0;
    case 2: return HAL_CLK_SOURCE_GPLL6;
    case 3: return HAL_CLK_SOURCE_GPLL2;
    case 4: return HAL_CLK_SOURCE_GPLL0_DIV2;
    default:
      return HAL_CLK_SOURCE_XO;
  }
} /* END HAL_clk_GCCSourceMapFromHW */



/* ===========================================================================
**  HAL_clk_Save
**
** ======================================================================== */

void HAL_clk_Save (void)
{
  /*
   * Nothing to save.
   */

} /* END HAL_clk_Save */


/* ===========================================================================
**  HAL_clk_Restore
**
** ======================================================================== */

void HAL_clk_Restore (void)
{
  /*
   * Nothing to restore.
   */
  
} /* END HAL_clk_Restore */


/* ===========================================================================
**  HAL_clk_GFXRailPowerDown
**
** ======================================================================== */

void HAL_clk_GFXRailPowerDown( void )
{
#if 0
  HWIO_OUTF( MMSS_GX_DOMAIN_MISC, OCMEM_FIFO_HALT, 1 );

  HWIO_OUT(MMSS_GX_DOMAIN_MISC, 0x1111);
#endif
}


/* ===========================================================================
**  HAL_clk_GFXRailPowerUp
**
** ======================================================================== */

void HAL_clk_GFXRailPowerUp( void )
{
#if 0
  HWIO_OUT( MMSS_GX_DOMAIN_MISC, 0x1111 );
  HAL_clk_BusyWait( 1 );

  HWIO_OUTF(MMSS_GX_DOMAIN_MISC, OCMEM_FIFO_HALT, 1);
  
  HWIO_OUTF(MMSS_GX_DOMAIN_MISC, OXILI_OCMEM_RESET, 0x0);

  HWIO_OUTF(MMSS_GX_DOMAIN_MISC, OXILI_OCMEM_CLAMP_IO, 0x0);

  HWIO_OUT(MMSS_GX_DOMAIN_MISC, 0x0);
#endif
}

/* ===========================================================================
**  HAL_clk_GPLL0_disable_deep_sleep
**
** ======================================================================== */
void HAL_clk_GPLL0_disable_deep_sleep(void)
{
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_BYPASSNL,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_RESET_N ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_OUTCTRL ,0);
}
/* ===========================================================================
**  HAL_clk_GPLL0_FSM_reset_deep_sleep
**
** ======================================================================== */
void HAL_clk_GPLL0_FSM_reset_deep_sleep(void)
{
     

  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_RESET ,1);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_BYPASSNL,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_RESET_N ,0);
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_OUTCTRL ,0);
   
  HAL_clk_BusyWait( 1 );
        
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA ,1);
  
  HAL_clk_BusyWait( 1 );
  
  HWIO_OUTF(GCC_GPLL0_MODE, PLL_VOTE_FSM_RESET ,0);
  
}  


#define OFFS_ADDR           0x60000000

#define NUM_WAITS           10
#define WAIT_TIME           20
#define DELAY_TIME          100
#define IPA_SAVE_SUCCESS    0xFADEFADE
#define IPA_SAVE_FAIL       0x0BADFADE
#define IPA_RESTORE_SUCCESS 0xCAFECAFE
#define IPA_RESTORE_FAIL    0x0BADCAFE
#define IPA_RESTORE_INTR    0xFADECAFE

static uint32 nIPASave = 0;
static uint32 nIPARestore = 0;
static uint32 nIPASaveTimes = 0;
//static uint32 nIPARestoreTimes = 0;

typedef enum
{
  IPA_SAVE_ENTER = 0,
  IPA_SAVE_COMPLETE,
  IPA_RESTORE_ENTER,
  IPA_RESTORE_COMPLETE,
  IPA_RESTORE_PW_ENABLED,
  IPA_RESTORE_IN_LOOP,
  IPA_RESTORE_OUT_LOOP,
  IPA_RESTORE_BEFORE_WAIT
}ipa_pc_state_e;

ipa_pc_state_e pc_state;



void HAL_clk_IPAGating(boolean bIPAGating)
{
  if(!bIPAGating) //ungate here; while enablong clock
  {
    
    pc_state = IPA_RESTORE_ENTER;
      
  //  HAL_clk_BusyWait( DELAY_TIME );
    
    // Write to MBOX for trigger interrupt to uC
    HWIO_OUTI2( IPA_UC_MAILBOX_m_n, 0, 18, IPA_RESTORE_INTR); //m=0, n=18 
    
    // Keep track number of IPA Restore for debug purpose
    nIPARestore++;
    pc_state = IPA_RESTORE_COMPLETE;
  }
  else //Gate here, while disabling the clock
  {
    
    nIPASaveTimes = NUM_WAITS;
    pc_state = IPA_SAVE_ENTER;
    
    // Invalidate mailbox 17 before polling
    HWIO_OUTI2( IPA_UC_MAILBOX_m_n, 0, 17, 0 );
  
    // Write to MBOX for trigger interrupt to uC
    HWIO_OUTI2( IPA_UC_MAILBOX_m_n, 0, 16, HWIO_ADDRI2(IPA_UC_MAILBOX_m_n, 0, 17) - OFFS_ADDR );
    
    while ( (HWIO_INI2(IPA_UC_MAILBOX_m_n, 0, 17) != IPA_SAVE_SUCCESS) && nIPASaveTimes--  )
    {
      HAL_clk_BusyWait( WAIT_TIME );
    }
    
    assert( HWIO_INI2(IPA_UC_MAILBOX_m_n, 0, 17) == IPA_SAVE_SUCCESS );
  
    // Keep track number of IPA Save for debug purpose
    nIPASave++;
    
    pc_state = IPA_SAVE_COMPLETE;
  }

}

