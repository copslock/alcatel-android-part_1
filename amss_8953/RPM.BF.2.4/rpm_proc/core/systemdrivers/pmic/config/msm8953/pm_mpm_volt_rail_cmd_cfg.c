/*! \file pm_mpm_volt_rail_cmd_cfg.c
*  \n
*  \brief This file contains pmic configuration data specific for mpm shutdown/wakeup voltage rail cmds.
*  \n
*  \n &copy; Copyright 2013-2015 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/pmic/config/msm8953/pm_mpm_volt_rail_cmd_cfg.c#3 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/10/15   sv      SMPS4 should set to 1.896V during sleep. (CR-948580)
08/27/15   akt     PMIC Dev Changes for Jacala
10/17/14   rk      PMIC Dev Changes for SAHI - First Drop (CR - 740886)
02/12/14   vtw     Updated with 8994 MPM sequence settings.
10/31/13   vtw     Created.
========================================================================== */

/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_mpm.h"
#include "pm_mpm_internal.h"
#include "pm_mpm_target.h"

/*===========================================================================
Data Declarations
===========================================================================*/

pm_mpm_cfg_type
pm_mpm_sleep_cfg[] =
{
  [PM_MPM_SLEEP_CMD_CX_VSET_LB]      = {0x1, 0x01740, 0x0, 0x0}, /* Set VDD_CX retention. */
  [PM_MPM_SLEEP_CMD_CX_VSET_UB]      = {0x1, 0x01741, 0x0, 0x0}, /* Set VDD_CX retention. */
  [PM_MPM_SLEEP_CMD_MX_VSET_LB]      = {0x1, 0x02640, 0x0, 0x0}, /* Set VDD_MX retention */
  [PM_MPM_SLEEP_CMD_MX_VSET_UB]      = {0x1, 0x02641, 0x0, 0x0}, /* Set VDD_MX retention */
  [PM_MPM_SLEEP_CMD_S4_VSET_LB]      = {0x1, 0x01D40, 0x68, 0x0}, /* Set S4 with retention voltage(1.896V). */
  [PM_MPM_SLEEP_CMD_S4_VSET_UB]      = {0x1, 0x01D41, 0x07, 0x0}, /* Set S4 with retention voltage. */
  [PM_MPM_SLEEP_CMD_PBS_TRIG]        = {0xE, 0x07642, 0x1, 0x6}, /* Program PBS SW trigger. */
  [PM_MPM_SLEEP_CMD_END]             = {0x0, 0x0,     0x0,  0x0}, /* End of sleep sequence. */
};


pm_mpm_cfg_type
pm_mpm_active_cfg[] =
{
  [PM_MPM_ACTIVE_CMD_PBS_TRIG]          = {0xE, 0x07642, 0x1,  0x6}, /* Program SW PBS trigger for ALL PMICS. */
  [PM_MPM_ACTIVE_CMD_MX_VSET_LB]        = {0x1, 0x02640, 0x0, 0x0},  /* Turn on MX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_MX_VSET_UB]        = {0x1, 0x02641, 0x0, 0x0},  /* Turn on MX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_CX_VSET_LB]        = {0x1, 0x01740, 0x0, 0x0},  /* Turn on CX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_CX_VSET_UB]        = {0x1, 0x01741, 0x0, 0x0},  /* Turn on CX with active voltage.  */
  [PM_MPM_ACTIVE_CMD_S4_VSET_LB]        = {0x1, 0x01D40, 0xF8, 0x0}, /* Set S4 with active voltage(2.04V). */
  [PM_MPM_ACTIVE_CMD_S4_VSET_UB]        = {0x1, 0x01D41, 0x07, 0x0}, /* Set S4 with active voltage. */
  [PM_MPM_ACTIVE_CMD_END]               = {0x0, 0x0,     0x0, 0x0},  /* End active sequence. */
};


/**
  * MPM cmd index buffer.
  */
const pm_mpm_cmd_index_type pm_mpm_cmd_index[] =
{
                             /* rail retention index, rail active index, rail sw mode before sleep */
  [PM_MPM_CX_INDEX]        = {PM_MPM_SLEEP_CMD_CX_VSET_LB, PM_MPM_ACTIVE_CMD_CX_VSET_LB, PM_MPM_INVALID_CMD},
  [PM_MPM_MX_INDEX]        = {PM_MPM_SLEEP_CMD_MX_VSET_LB, PM_MPM_ACTIVE_CMD_MX_VSET_LB, PM_MPM_INVALID_CMD},
};


/**
  * MPM rails info.
  */
pm_mpm_rails_info_type pm_mpm_rails_info[] =
{
                             /* resource_type, resource_index, sleep_headroom_uv */
  [PM_MPM_CX_INDEX]        = {  RPM_SMPS_A_REQ,              2,              0},
  [PM_MPM_MX_INDEX]        = {  RPM_SMPS_A_REQ,              7,              0},
};
