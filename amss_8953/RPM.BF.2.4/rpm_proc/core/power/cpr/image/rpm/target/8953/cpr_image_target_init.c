/**
 * @file:  cpr_image_init.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/24 05:14:41 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/target/8953/cpr_image_target_init.c#4 $
 * $Change: 9950595 $
 */
#include <string.h>
#include "cpr_logs.h"
#include "cpr_data.h"
#include "cpr_rail.h"
#include "cpr_smem.h"
#include "cpr_image.h"
#include "cpr_measurements.h"
#include "cpr_cfg.h"
#include "cpr_hal.h"
#include "cpr_utils.h"
#include "cpr_image_target_init.h"

//******************************************************************************
// Local Helper Functions
//******************************************************************************
extern void cpr_hal_write_temp_margin_init_registers(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement);

static cpr_cfg_funcs* init_open_loop(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Initializing open loop on %s", rail->name );
    cpr_rail_set_initial_voltages( rail, false, false );
    return NULL;
}

static cpr_cfg_funcs* init_closed_loop(cpr_rail* rail)
{
    init_open_loop(rail);

    CPR_LOG_TRACE( "Configuring closed loop on %s", rail->name );

    cpr_rail_update_target_quotients( rail, &cpr_info.railStates[rail->railIdx] );
    cpr_hal_init_rail_hw( &rail->hal, &rail->halRailCfg );
    /*
     * Set the SDELTA table up for the specific rail. Assume we are at turbo mode to start.
     */
    // Temperature adjustment feature init for CPR4.
    cpr_hal_write_temp_margin_init_registers(rail->vp->modesCount-1, &rail->hal, rail->vp, rail->enablement);    

    return NULL;
}

static cpr_cfg_funcs* enable_closed_loop(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Enabling closed loop on %s", rail->name );

    cpr_rail_register_isr( rail );

    return NULL;
}


static cpr_cfg_funcs* init_smem(cpr_rail* rail)
{
    cpr_smem_deserialize_config( rail, &cpr_info.railStates[rail->railIdx] );

    return NULL;
}

static cpr_cfg_funcs* enable_smem(cpr_rail* rail)
{
    switch(cpr_info.railStates[rail->railIdx].cMode)
    {
        case CPR_CONTROL_HW_CLOSED_LOOP:
        case CPR_CONTROL_SW_CLOSED_LOOP:
            CPR_LOG_TRACE( "Enabling closed loop on %s (remotely configured)", rail->name );
            enable_closed_loop( rail );
            break;

        case CPR_CONTROL_OPEN_LOOP:
            CPR_LOG_TRACE( "Enabling open loop on %s (remotely configured)", rail->name );
            break;
			
        case CPR_CONTROL_NONE:
            CPR_LOG_TRACE( "Disabling CPR %s (remotely configured)", rail->name );
            break;
        default:
            CPR_LOG_FATAL( "Unknown control mode: %u", cpr_info.railStates[rail->railIdx].cMode );
    }

    return NULL;
}



//******************************************************************************
// Default Enablement Structures
//******************************************************************************

cpr_cfg_funcs CPR_INIT_NONE           = {.cMode = CPR_CONTROL_NONE,           .init = NULL,             .enable = NULL};
cpr_cfg_funcs CPR_INIT_SMEM           = {.cMode = CPR_CONTROL_NONE,           .init = init_smem,        .enable = enable_smem};
cpr_cfg_funcs CPR_INIT_OPEN_LOOP      = {.cMode = CPR_CONTROL_OPEN_LOOP,      .init = init_open_loop,   .enable = NULL};
cpr_cfg_funcs CPR_INIT_SW_CLOSED_LOOP = {.cMode = CPR_CONTROL_SW_CLOSED_LOOP, .init = init_closed_loop, .enable = enable_closed_loop};
