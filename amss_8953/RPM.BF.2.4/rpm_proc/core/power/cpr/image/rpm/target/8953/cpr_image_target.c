/**
 * @file:  cpr_image_target.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/21 00:15:42 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/target/8953/cpr_image_target.c#2 $
 * $Change: 10106901 $
 */

#include "cpr_image_target.h"
#include "smem.h"
#include "page_select.h"
#include "Chipinfo.h"


void cpr_image_target_open_remote_cfg(void** cfg, uint32* size)
{
    *cfg = smem_get_addr( SMEM_CPR_CONFIG, size );
    set_page_select( 2 );
}

void cpr_image_target_close_remote_cfg(void)
{
    set_page_select( 0 );
}

cpr_foundry_id cpr_image_target_get_foundry(void)
{
    /*cpr_foundry_id cpr_foundry;
    uint32 chipInfo_foundry = Chipinfo_GetFoundryId();

    switch(chipInfo_foundry)
    {
        case(CHIPINFO_FOUNDRYID_TSMC):
            cpr_foundry = CPR_FOUNDRY_TSMC;
            break;
        case(CHIPINFO_FOUNDRYID_GF):
            cpr_foundry = CPR_FOUNDRY_GF;
            break;
        case(CHIPINFO_FOUNDRYID_SS):
            cpr_foundry = CPR_FOUNDRY_SS;
            break;
        case(CHIPINFO_FOUNDRYID_IBM):
            cpr_foundry = CPR_FOUNDRY_IBM;
            break;
        case(CHIPINFO_FOUNDRYID_UMC):
            cpr_foundry = CPR_FOUNDRY_UMC;
            break;
        default:
            CPR_ASSERT( 0 ); // Chip foundry is not valid
    }*/

    return CPR_FOUNDRY_SS;

}
