/**
 * @file:  cpr_image_target.h
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/13 00:13:30 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/inc/cpr_image_target.h#1 $
 * $Change: 9714231 $
 */

#ifndef CPR_IMAGE_TARGET_H
#define CPR_IMAGE_TARGET_H

#include "cpr_defs.h"
#include "cpr_types.h"

void cpr_image_target_open_remote_cfg(void** cfg, uint32* size);
void cpr_image_target_close_remote_cfg(void);
cpr_foundry_id cpr_image_target_get_foundry(void);

#endif /*CPR_IMAGE_TARGET_H*/
