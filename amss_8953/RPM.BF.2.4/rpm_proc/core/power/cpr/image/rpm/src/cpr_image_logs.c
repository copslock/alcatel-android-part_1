/**
 * @file:  cpr_image_logs.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/src/cpr_image_logs.c#1 $
 * $Change: 9561832 $
 */

#include "cpr_image_logs.h"

ULogHandle cprLogHandle;
