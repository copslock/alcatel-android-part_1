/**
 * @file:  cpr_controller.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/common/inc/cpr_controller.h#1 $
 * $Change: 9561832 $
 */
#ifndef CPR_CONTROLLER_H
#define	CPR_CONTROLLER_H

#include "cpr_cfg.h"

void cpr_controller_init(cpr_controller* controller);

#endif

