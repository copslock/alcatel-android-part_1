/**
 * @file:  cpr_hal.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/04/06 04:36:14 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/common/hal/v4/src/cpr_hal.c#3 $
 * $Change: 10214530 $
 *
 */

#include "cpr_hal.h"
#include "cpr_logs.h"
#include "cpr_data.h"
#include "cpr_image.h"
#include "cpr_voltage_plan.h"

extern cpr_sw_settings sw_settings;
//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

#define ROSC_COUNT 16
#define HAL_CPR_TIMER_DEFAULT_INTERVAL  (5 * 19200)   //5ms * 19.2MHz
#define CPR_DEFAULT_GCNT 19     //We always use 19 since we always run the CPR Ref Clk at 19.2MHz
#define CPR_AGING_ROSC_COUNT 2 //gcnt0 and gcnt1

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static void updateBitMask32(uint32 readAddr, uint32 writeAddr, uint8* bitList, uint32 count, boolean set)
{
    uint32 reg_cur = 0xffffffff; /* 0 is valid value. Use 0xffffffff as invalid value */
    uint32 reg_val = 0;

    for(int i=0; i < count; i++)
    {
        /*
         * Compute the register to write to and bit position
         */
        uint32 reg_new = (bitList[i] / 32);
        uint32 bit     = (bitList[i] % 32);
        uint32 bmask   = (1 << bit);

        /*
         * Write the value to the register before moving to new register
         */
        if(reg_cur != 0xffffffff && reg_new != reg_cur )
        {
            CPR_HWIO_OUT( writeAddr + (sizeof(uint32) * reg_cur), reg_val );
        }

        /*
         * Read the register value if not read yet
         */
        if(reg_cur == 0xffffffff || reg_new != reg_cur )
        {
            reg_val = CPR_HWIO_IN( readAddr + (sizeof(uint32) * reg_new) );
            reg_cur = reg_new;
        }

        /* Mask Sensor */
        if(set) {
            reg_val |= bmask;
        }
        else /* Unmask sensor */ {
            reg_val &= ~bmask;
        }
    }

    /*
     * Write the value to the register
     */
    if(reg_cur != 0xffffffff)
    {
        CPR_HWIO_OUT( writeAddr + (sizeof(uint32) * reg_cur), reg_val );
    }
}

static void updateBitMaskAll32(uint32 readAddr, uint32 writeAddr, uint8* bitList, uint32 count, boolean set)
{
    uint32 reg_prev = 0xFFFFFFFF;

    for(int i=0; i < count; i++)
    {
        /* Compute the register to write to */
        uint32 reg = (bitList[i] / 32);
        uint32 val;

        if(reg == reg_prev)
        {
            continue;
        }

        if(set) /* mask all sensors */
        {
            val = 0xFFFFFFFF;
        }
        else /* unmask all sensors */
        {
            val = 0x0;
        }

        CPR_HWIO_OUT( writeAddr + (sizeof(uint32) * reg), val );

        reg_prev = reg;
    }
}

//******************************************************************************
// Public API Functions
//******************************************************************************

void cpr_hal_clear_interrupts(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT( HWIO_CPR_IRQ_CLEAR_t_ADDR( hdl->base, hdl->thread ), ~0 );
}

void cpr_hal_ack_result(cpr_hal_handle* hdl, boolean ack)
{
    /*
     * set VDD_CHANGED_ONE_STEP bit (bit 0) of CPR_CONT_CMD_t register
     *
     * A write of any data value to this address will send a pulse to cpr_master.cont_ack.
     * The meaning of the cont_ack signal is to indicate that the cpr_master should continue
     * with a next measurement iteration, and that the recommendation of the prior iteration was taken.
     *
     * Tell the controller that the VDD has been changed by
     * exactly one PMIC step compared to the previous measurement.
     * This CSR is per thread
     */
    CPR_HWIO_OUT( HWIO_CPR_CONT_CMD_t_ADDR( hdl->base, hdl->thread ), ack );
}

void cpr_hal_set_count_mode(cpr_hal_handle* hdl, cpr_hal_count_mode count_mode)
{
    /*
     * NOTE: Provided BUSY=0, COUNT_MODE can change when LOOP_EN=1.
     * When set to ALL_AT_ONCE_AGE, 'gcnt[0]' is for AGE_MAX; 'gcnt[1]' for AGE_MIN RO and
     * controller will automatically force 0 onto 'gcnt[2..15]' to so skip all other RO slots.
     */
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_COUNT_MODE, count_mode);
}

void cpr_hal_bypass_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count, boolean bypass)
{
    updateBitMask32( HWIO_CPR_SENSOR_BYPASS_READn_ADDR( hdl->base, 0 ),
                     HWIO_CPR_SENSOR_BYPASS_WRITEn_ADDR( hdl->base, 0 ),
                     sensors,
                     count,
                     bypass );
}

void cpr_hal_mask_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count, boolean mask)
{
    updateBitMask32( HWIO_CPR_SENSOR_MASK_READn_ADDR( hdl->base, 0 ),
                     HWIO_CPR_SENSOR_MASK_WRITEn_ADDR( hdl->base, 0 ),
                     sensors,
                     count,
                     mask );
}

#define cpr_hal_disable_all_sensors(hdl, sensors, count)    cpr_hal_mask_all_sensors(hdl, sensors, count, true)

static void cpr_hal_mask_all_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count, boolean mask)
{
    updateBitMaskAll32( HWIO_CPR_SENSOR_MASK_READn_ADDR( hdl->base, 0 ),
                        HWIO_CPR_SENSOR_MASK_WRITEn_ADDR( hdl->base, 0 ),
                        sensors,
                        count,
                        mask );
}

/*
 * Set the number of clk cycles to count between activating cont_ack signal.
 * The purpose of the timer is to allow a PMIC transaction to settle with a new Vdd level,
 * before kicking off a next measurement.
 */
static void cpr_hal_set_timer_auto_cont_interval(cpr_hal_handle* hdl, uint32 interval)
{
    CPR_HWIO_OUT( HWIO_CPR_TIMER_AUTO_CONT_ADDR( hdl->base ), interval );
}

/*
 * Set number of times to perform back-to-back measurement in the VDD_MAX/MIN count mode
 * - forced to 1 for AGE_PAGE mode
 * - forced to number of non-bypassed sensors for STAGGERED mode
 * - S/W can program value 1 into this CSR in order to mimic Rev2 legacy ALL_AT_ONCE mode
 */
static void cpr_hal_set_count_repeat(cpr_hal_handle* hdl, uint32 count)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_COUNT_REPEAT, count );
}

void cpr_hal_configure_controller(cpr_hal_handle* hdl, uint16 stepQuotMin, uint16 stepQuotMax)
{
    // TODO - needed?
    //CPR_HWIO_OUT_FIELD(HWIO_CPR_BIST_CHAIN_CHECK0_ADDR(hdl->hw_base_address), HWIO_CPR_BIST_CHAIN_CHECK0_SCLK_CNT_EXPECTED, 32*6);

    cpr_hal_set_timer_auto_cont_interval(hdl, HAL_CPR_TIMER_DEFAULT_INTERVAL);
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_IDLE_CLOCKS, 15 );

    CPR_HWIO_OUT_FIELD( HWIO_CPR_STEP_QUOT_INIT_ADDR( hdl->base ), HWIO_CPR_STEP_QUOT_INIT_STEP_QUOT_INIT_MAX, stepQuotMax );
    CPR_HWIO_OUT_FIELD( HWIO_CPR_STEP_QUOT_INIT_ADDR( hdl->base ), HWIO_CPR_STEP_QUOT_INIT_STEP_QUOT_INIT_MIN, stepQuotMin );

    cpr_hal_set_count_mode(hdl, CPR_HAL_COUNT_MODE_ALL_AT_ONCE_MIN);

    cpr_hal_set_count_repeat(hdl, 1);

    /*
     * NOTE: CPR controller is enabled in cpr_hal_enable_rail()
     */
}

void cpr_hal_enable_controller(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN, 1 );

    CPR_LOG_TRACE("--- cpr_hal_enable_controller (LOOP_EN:%u) ---",
                  CPR_HWIO_IN_FIELD(HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN));
}

void cpr_hal_disable_controller(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN, 0 );

    CPR_LOG_TRACE("--- cpr_hal_disable_controller (LOOP_EN:%u) ---",
                  CPR_HWIO_IN_FIELD(HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN));
}

void cpr_hal_enable_thread(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), HWIO_CPR_MASK_THREAD_t_DISABLE_THREAD, 0u );
    CPR_LOG_TRACE("--- cpr_hal_enable_thread %d ---", hdl->thread);
}

void cpr_hal_disable_thread(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), HWIO_CPR_MASK_THREAD_t_DISABLE_THREAD, 1u );
    CPR_LOG_TRACE("--- cpr_hal_disable_thread %d ---", hdl->thread);
}

void cpr_hal_read_results( cpr_hal_handle* hdl, cpr_results* rslts)
{
    uint32 status = CPR_HWIO_IN( HWIO_CPR_RESULT0_t_ADDR( hdl->base, hdl->thread ) );

    rslts->busy = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_BUSY );
    rslts->up = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_STEP_UP );
    rslts->down = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_STEP_DN );
    rslts->steps = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_ERROR_STEPS );

    status = CPR_HWIO_IN( HWIO_CPR_RESULT1_t_ADDR( hdl->base, hdl->thread ) );

    rslts->quotMax = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_QUOT_MAX );
    rslts->quotMin = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_QUOT_MIN );
    rslts->selMax  = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_SEL_MAX );
    rslts->selMin  = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_SEL_MIN );
}

boolean cpr_hal_get_results(cpr_hal_handle* hdl, cpr_results* rslts, uint32 timeout)
{
    boolean ret = true;

    do {
        cpr_hal_read_results( hdl, rslts );

        if(timeout == 0) {
            break;
        }
        else {
            timeout--;
        }

        if(rslts->busy) {
            cpr_image_wait(3); // 3 usec is recommended by HW team
        }
    } while(rslts->busy);

    if(!rslts->busy) {
        CPR_LOG_TRACE( " Results - (up: %d) (dn: %d) (steps: %d) (QuotMin:%d) (QuotMax:%d) (SelMin:%d) (SelMax:%d)",
                      rslts->up, rslts->down, rslts->steps,
                      rslts->quotMin, rslts->quotMax, rslts->selMin, rslts->selMax);
    }
    else {
        CPR_LOG_ERROR( "Timeout waiting for results" );
        ret = false;
    }

    return ret;
}

void cpr_hal_init_rail_hw(cpr_hal_handle* hdl, cpr_hal_rail_cfg* cfg)
{
    /*
     * Specify which sensor belongs to which thread
     */
    for(int i = 0; i < cfg->verCfg.v3.sensorsCount; i++) {
        CPR_HWIO_OUT( HWIO_CPR_SENSOR_THREAD_n_ADDR( hdl->base, cfg->verCfg.v3.sensors[i] ), hdl->thread );
    }

    uint32 thresholds = CPR_HWIO_SET_FIELD_VALUE( cfg->upThresh, HWIO_CPR_THRESHOLD_t_UP_THRESHOLD ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->dnThresh, HWIO_CPR_THRESHOLD_t_DN_THRESHOLD ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->consecUp, HWIO_CPR_THRESHOLD_t_CONSECUTIVE_UP ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->consecDn, HWIO_CPR_THRESHOLD_t_CONSECUTIVE_DN );

    CPR_HWIO_OUT( HWIO_CPR_THRESHOLD_t_ADDR( hdl->base, hdl->thread ), thresholds );

    // disable the thread and all 16 ROs in the thread.
    CPR_HWIO_OUT( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), ~0 );

    // set gate count to 19 only for the needed ROs in the thread.

    for(int i = 0; i < ROSC_COUNT; i++)
        {
            uint32 gcntOffset = (HWIO_CPR_GCNT1_ADDR( 0 ) - HWIO_CPR_GCNT0_ADDR( 0 )) * i;
            CPR_HWIO_OUT_FIELD( HWIO_CPR_GCNT0_ADDR( hdl->base ) + gcntOffset, HWIO_CPR_GCNT0_GCNT0,  sw_settings.gcnts[i] );
        }
}

void cpr_hal_set_targets(cpr_hal_handle* hdl, cpr_voltage_mode mode, uint32 freq, cpr_quotient* tgts, uint32 count)
{
    for(int i = 0; i < count; i++)
    {
        if(tgts[i].quotient != 0)
        {
            CPR_LOG_TRACE( " Setting RO %u to %u", tgts[i].ro, tgts[i].quotient );

            uint32 targetOffset = (HWIO_CPR_TARGET1_t_m_ADDR( 0, 0, 0 ) - HWIO_CPR_TARGET0_t_m_ADDR( 0, 0, 0 )) * tgts[i].ro;

            // Unmask RO
            CPR_HWIO_OUT_CLEAR( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), 1 << tgts[i].ro );

            CPR_HWIO_OUT_FIELD( HWIO_CPR_TARGET0_t_m_ADDR( hdl->base, hdl->thread, 0 ) + targetOffset,
                                HWIO_CPR_TARGET0_t_m_TARGET0, tgts[i].quotient );
        }
    }
}

void cpr_hal_start_poll(cpr_hal_handle* hdl)
{
    CPR_LOG_TRACE("Start polling");

    cpr_hal_enable_rail( hdl, true, true, false, true );
}

void cpr_hal_stop_poll(cpr_hal_handle* hdl)
{
    cpr_hal_disable_rail( hdl );

    CPR_LOG_TRACE("Stopped polling");
}

void cpr_hal_enable_up_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_UP_FLAG_EN,
                        enable );
}

void cpr_hal_enable_down_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_DOWN_FLAG_EN,
                        enable );
}

void cpr_hal_enable_mid_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_MID_FLAG_EN,
                        enable );
}

static void cpr_hal_disable_interrupts(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ), 0 );
}

/*
 * To re-enable CPR after setting parameters for new mode
 * - Enable Thread by simply writing into related CSR bit  - or set loop_en=1
 */
void cpr_hal_enable_rail(cpr_hal_handle* hdl, boolean up, boolean down, boolean mid, boolean swControl)
{
    if(swControl)
    {
        cpr_hal_enable_up_interrupt(hdl, up);
        cpr_hal_enable_down_interrupt(hdl, down);
        cpr_hal_enable_mid_interrupt(hdl, mid);

            cpr_hal_enable_controller( hdl );
        cpr_hal_enable_thread( hdl );
    }
    else if(hdl->type == CPR_CONTROLLER_TYPE_HW_CL_CAPABLE)
    {
#if defined HWIO_APCS_APCC_CPR_SW_MODE_ADDR
        CPR_HWIO_OUT_FIELD( HWIO_APCS_APCC_CPR_SW_MODE_ADDR( hdl->base ), HWIO_APCS_APCC_CPR_SW_MODE_CPR_SW_MODE, swControl );
#endif
        cpr_hal_enable_controller( hdl );
    }
}

void cpr_hal_disable_rail(cpr_hal_handle* hdl)
{
    cpr_hal_disable_interrupts(hdl);
    cpr_hal_clear_interrupts(hdl);

    /*
     * Disable this rail
     *
     * From HPG:
     *
     * 1. One controller, one rail, one thread
     *  In this case (like CX, GFX) each rail has its own controller and the controller only has one thread.
     *  We can use LOOP_EN for mode switching which is: disable cpr (LOOP_EN=0), reconfigure the mode,
     *  enable CPR (LOOP_EN=1). SW only needs to tell controller the new mode since the targets are already
     *  programmed in controller. We have per mode target registers in CPR3 controller.
     *
     * 2. One controller , one rail, two threads (APPS CPR)
     *  In this case (like HMSS in Istari) the threads are aggregated in HW and SW receives only one interrupt.
     *  In this case SW can use the LOOP_EN approach described in 1.
     *
     * 3. One controller, multi rail, multi thread
     *  In this case (like MX, DDR) SW needs to follow the THREAD disable approach and use the thread disable
     *  CSR for disabling a thread/rail on which mode switch is happening.
     */
    if(hdl->type == CPR_CONTROLLER_TYPE_SW_CL_MULTI_RAILS) {
        cpr_hal_disable_thread( hdl );
    }
    else {
        cpr_hal_disable_controller( hdl );
    }
}

/*
 * Configure CPR controller to read the QUOTs of AGE_Max RO and AGE_Min RO.
 * After configured, gcnt[0] is for AGE_Max RO, gcnt[1] is for AGE_Min RO.
 * Controller will force zero value to the rest and skip all other ROs measurement.
 */
void cpr_hal_enable_aging(cpr_hal_handle* hdl, cpr_hal_rail_cfg* rail_cfg, cpr_aging_cfg* aging_cfg)
{
    // Set cont mode to VDD_MIN(0) and count_repeat=0
    cpr_hal_set_count_mode(hdl, CPR_HAL_COUNT_MODE_ALL_AT_ONCE_MIN);
    cpr_hal_set_count_repeat(hdl, 0);

    // Set all timer intervals to 0
    cpr_hal_set_timer_auto_cont_interval(hdl, 0);

    // step_quot_init_max/min are already configured in cpr_hal_configure_controller().

    // Program gcnt 0 and 1 to 19
    for(int i = 0; i < CPR_AGING_ROSC_COUNT; i++)
    {
        uint32 gcntOffset = (HWIO_CPR_GCNT1_ADDR( 0 ) - HWIO_CPR_GCNT0_ADDR( 0 )) * i;
        CPR_HWIO_OUT_FIELD( HWIO_CPR_GCNT0_ADDR( hdl->base ) + gcntOffset, HWIO_CPR_GCNT0_GCNT0, CPR_DEFAULT_GCNT );
    }

    // Mask all the sensors except the sensor which has selected to be aging sensor.
    cpr_hal_disable_all_sensors(hdl, rail_cfg->verCfg.v3.sensors, rail_cfg->verCfg.v3.sensorsCount);
    cpr_hal_enable_sensor(hdl, aging_cfg->sensorID);

    // Bypass the sensors
    // Not in the de-aging flow pseudo code but according to the "SoC specific HPG" section,
    //     "we should bypass collapsible sensors to avoid CPR measurement being reset by power ON/OFF of collapsible blocks"
    //     "Note that, SW should mask all the sensors except AGE sensor ID and that is besides these bypass settings."
    cpr_hal_remove_sensors(hdl, aging_cfg->bypassSensorIDs, aging_cfg->bypassSensorIDsCount);

    // If CPR controller support more than 1 thread, write 0 to CPR_SENSOR_THREAD_N where N is AGE_SENSOR_ID
    if(hdl->type == CPR_CONTROLLER_TYPE_SW_CL_MULTI_RAILS) {
        CPR_HWIO_OUT( HWIO_CPR_SENSOR_THREAD_n_ADDR( hdl->base, aging_cfg->sensorID ), 0 );
    }

    /*
     * Enable up/mid/down interrupts for polling and enable CPR controller.
     *
     * For aging algorithm in CPR3 we have to enable all UP/DOWN/MID interrupts
     * to avoid auto_cont mode of cpr controller.
     * Note that if one of this interrupt is disabled, CPR goes to auto mode if that event happens.
     */
    cpr_hal_enable_rail( hdl, true, true, true, true );
}

void cpr_hal_disable_aging(cpr_hal_handle* hdl, cpr_hal_rail_cfg* rail_cfg, cpr_aging_cfg* aging_cfg)
{
    // Disable up/mid/down interrupts, clear interrupts, and disable CPR controller
    cpr_hal_disable_rail(hdl);

    // Unbypass sensors
    cpr_hal_add_sensors(hdl, aging_cfg->bypassSensorIDs, aging_cfg->bypassSensorIDsCount);

    // Unmask all the sensors except the sensor which has selected to be aging sensor.
    cpr_hal_disable_sensor(hdl, aging_cfg->sensorID);
    cpr_hal_enable_sensors(hdl, rail_cfg->verCfg.v3.sensors, rail_cfg->verCfg.v3.sensorsCount);

    // Reset gcnt 0 and 1
    for(int i = 0; i < CPR_AGING_ROSC_COUNT; i++)
    {
        uint32 gcntOffset = (HWIO_CPR_GCNT1_ADDR( 0 ) - HWIO_CPR_GCNT0_ADDR( 0 )) * i;
        CPR_HWIO_OUT_FIELD( HWIO_CPR_GCNT0_ADDR( hdl->base ) + gcntOffset, HWIO_CPR_GCNT0_GCNT0, 0);
    }

    cpr_hal_set_timer_auto_cont_interval(hdl, HAL_CPR_TIMER_DEFAULT_INTERVAL);

    cpr_hal_set_count_repeat(hdl, 1);
    cpr_hal_set_count_mode(hdl, CPR_HAL_COUNT_MODE_ALL_AT_ONCE_MIN);
}

static boolean cpr_hal_is_aging_result_valid(cpr_hal_handle* hdl)
{
    /*
     * 1 - QUOT_MIN/MAX, SEL_MIN/MAX & SENSOR_MIN/MAX contain AGE readouts.
     * 0 - They contain data for regular CPR ROs.
     *
     * PAGE_IS_AGE has same timing as BUSY and is only valid when BUSY=0,
     * which is until 'cont=1' arrives.
     */
    return CPR_HWIO_IN_FIELD( HWIO_CPR_FSM_STA_ADDR( hdl->base ), HWIO_CPR_FSM_STA_PAGE_IS_AGE );
}

boolean cpr_hal_get_aging_delta(cpr_hal_handle* hdl, int32 *aging_delta, uint32 *retry_count)
{
    cpr_results rslts;
    boolean ret = true;

    cpr_image_wait(10); // 10usec is from HPG

    // wait for busy to be low
    if(!cpr_hal_get_results( hdl, &rslts, RESULTS_TIMEOUT_5MS ))
    {
        /*
         * If busy remains high more than 5ms, set loop_en to low and start from beginning for 2nd time.
         * If in 2nd time , busy remain low as well, exit the algorithm and set age compensation
         * to MAX_COMPENSATION provided in Age spreadsheet
         */
        *retry_count += 1;
        return false;
    }

    // set age_page mode
    cpr_hal_set_count_mode(hdl, CPR_HAL_COUNT_MODE_ALL_AT_ONCE_AGE);

    // send CONT_NACK
    cpr_hal_ack_result(hdl, false);

    cpr_image_wait(10); // 10 usec is from HPG

    if(!cpr_hal_get_results(hdl, &rslts, RESULTS_TIMEOUT_5MS ))
    {
        /*
         * If busy remains high more than 5ms, set loop_en to low and start from beginning for 2nd time.
         * If in 2nd time , busy remain low as well, exit the algorithm and set age compensation
         * to MAX_COMPENSATION provided in Age spreadsheet
         */
        *retry_count += 1;
        return false;
    }

    // check for valid aging data
    if(cpr_hal_is_aging_result_valid(hdl))
    {
        // ALWAYS CALCULATE DELTA= RO#1 - RO#0
        *aging_delta = (rslts.selMax == 1) ? (rslts.quotMax - rslts.quotMin) : (rslts.quotMin - rslts.quotMax);

        CPR_LOG_INFO("Aging Delta: %d", *aging_delta);
    }
    else
    {
        /*
         * Do not increment error count.
         * HPG does not define what to do when PAGE_IS_AGE was not set for multiple trials.
         */
        CPR_LOG_ERROR("PAGE_IS_AGE is not set");
        ret = false;
    }

    return ret;
}


static void cpr_hal_write_temp_margin_table(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp) 
{
    int i=0;
    uint32 dataAddress;
    uint32 dataValue;

    dataAddress = HWIO_CPR_MARGIN_TEMP_CORE0_ADDR(hdl->base);
  
    CPR_LOG_INFO( "Setting SDELTA Table" );

    // Write the margin table to registers
    for(i = 0; i < vp->modes[modeIndex].marginModeTableSize; i++)
    {
        dataValue = CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand0, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND0) |
                    CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand1, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND1) |
                    CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand2, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND2) |
                    CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand3, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND3);
      CPR_HWIO_OUT(dataAddress, dataValue);
      dataAddress += 0x4;
    }
}

void cpr_hal_mode_change_temp_adj(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{    
    // If temperature adjustment is not enabled
    if (vp->tempAdjControls == NULL || enablement->enabletempadj != true)
    {
      // By HPG specification. If temperature adjustment is not enabled then set 0 to kvMarginAdjEn
      CPR_LOG_INFO( "Temperature Adjustment is not enabled, exiting temperature adjustment init");
      CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base), HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 0);
      return;
    }
    // Write the new SDELTA table for the new corner
    cpr_hal_write_temp_margin_table(modeIndex, hdl, vp);
}

void cpr_hal_write_temp_margin_init_registers(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{
    // If temperature adjustment is not enabled
    if (vp->tempAdjControls == NULL || enablement->enabletempadj != true)
    {
      // By HPG specification. If temperature adjustment is not enabled then set 0 to kvMarginAdjEn
      CPR_LOG_INFO( "Temperature Adjustment is not enabled, exiting temperature adjustment init");
      CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base), HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 0);
      return;
    }

    // Write the CPR MISC Register which contains the temp sensor id start and end
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MISC_REGISTER_ADDR(hdl->base), HWIO_CPR_MISC_REGISTER_TEMP_SENSOR_ID_START, vp->tempAdjControls->tempAdjMiscReg->tempSensorStartId);
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MISC_REGISTER_ADDR(hdl->base), HWIO_CPR_MISC_REGISTER_TEMP_SENSOR_ID_END, vp->tempAdjControls->tempAdjMiscReg->tempSensorEndId); 

    // Write the point 0, 1, 2 temperature thresholds
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_TEMP_POINT0N1_ADDR(hdl->base), HWIO_CPR_MARGIN_TEMP_POINT0N1_POINT0, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint0 * 10);
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_TEMP_POINT0N1_ADDR(hdl->base), HWIO_CPR_MARGIN_TEMP_POINT0N1_POINT1, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint1 * 10);
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_TEMP_POINT2_ADDR(hdl->base), HWIO_CPR_MARGIN_TEMP_POINT2_POINT2, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint2 * 10);   

    cpr_hal_write_temp_margin_table(modeIndex, hdl, vp);

    // DEBUGGING PURPOSES ONLY
    if (vp->tempAdjControls->tempAdjCtl->perRoKvMarginEn)
    {
      CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base), HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_SINGLE_STEP_QUOT, vp->tempAdjControls->tempAdjCtl->singleStepQuot);
      CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base), HWIO_CPR_MARGIN_ADJ_CTL_PER_RO_KV_MARGIN_EN, vp->tempAdjControls->tempAdjCtl->perRoKvMarginEn);
    }

    // Enable the feature
    CPR_HWIO_OUT_FIELD(HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base), HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 1);
}
