/**
 * @file:  cpr_utils.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/21 00:15:42 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/common/src/cpr_utils.c#4 $
 * $Change: 10106901 $
 */

#include "cpr_utils.h"


cpr_margin_data* cpr_utils_get_margins(cpr_margin_cfg* marginList)
{
    if(marginList != NULL)
    {
        for(int i = 0; i < marginList->count; i++)
        {
            if (cpr_info.cprRev >= marginList->data[i].cprRevMin && cpr_info.cprRev <= marginList->data[i].cprRevMax) {
                return &marginList->data[i];
            }
        }
    }

    return NULL;
}

void cpr_utils_get_mode_settings(cpr_domain_id railId, cpr_domain_info* info, cpr_mode_settings** mode, cpr_submode_settings** submode)
{
    cpr_rail* rail = cpr_get_rail( railId );
    cpr_rail_state* railState = &cpr_info.railStates[rail->railIdx];

    if(info->type == CPR_DOMAIN_TYPE_MODE_BASED)
    {
        cpr_mode_settings* ms = &railState->modeSettings[rail->vp->idxLookupFunc( info->u.mode )];
        if(mode) *mode = ms;
        if(submode) *submode = &ms->subModes[ms->subModesCount - 1];
    }
    else
    {
        for(int i = 0; i < railState->modeSettingsCount; i++)
        {
            cpr_mode_settings* ms = &railState->modeSettings[i];

            if(info->u.freq <= ms->subModes[ms->subModesCount-1].freq)
            {
                for (int k = 0; k < ms->subModesCount; k++)
                {
                    if(info->u.freq == ms->subModes[k].freq) {
                        if(mode) *mode = ms;
                        if(submode) *submode = &ms->subModes[k];
                        return;
                    }
                }
            }
        }

        CPR_LOG_FATAL("Virtual corner (freq %u) not supported on rail 0x%x", info->u.freq, railId);
    }
}

int16 cpr_utils_decode_fuse_value(cpr_fuse* fuse, uint8 packingFactor, boolean isSigned)
{
    int bits_assigned = 0;
    uint32 cpr_fuse_bits = 0;

    CPR_ASSERT(fuse != NULL);

    if(!fuse->count)
    {
        return 0;
    }

    /*
     * NOTE:
     * First entry is MSB of raw fuse value.
     * Last entry is LSB (bit0) of raw fuse value.
     */
    for(int i = 0; i < fuse->count; i++)
    {
        uint32 raw_fuse_value = CPR_HWIO_IN( fuse->data[i].address );

        //This will give us the bits we care about from the fuse in the lowest bits of bits_we_care_about.
        const uint32 bits_we_care_about = (raw_fuse_value & fuse->data[i].mask) >> fuse->data[i].offset;

        //Now need to know how many bits of the fuse this accounts for. Could use __clz if only I knew what it did on ARM64/Q6?
        uint32 number_of_bits_we_care_about = 0;
        uint32 mask_for_counting = fuse->data[i].mask >> fuse->data[i].offset;

        while(mask_for_counting) {
            number_of_bits_we_care_about++;
            mask_for_counting = mask_for_counting >> 1;
        }

        //Move up the bits we've got previously to make way for the new bits we've parsed.
        cpr_fuse_bits = cpr_fuse_bits << number_of_bits_we_care_about;

        //Now pull in the new bits we just parsed.
        cpr_fuse_bits |= bits_we_care_about;

        //Record the total number of bits in the fuse.
        bits_assigned += number_of_bits_we_care_about;
    }

    //We have the CPR fuse as a raw bitfied in cpr_fuse_bits. Now need to convert to a signed int based on the
    //highest bit of the fuse. If the highest bit is set then the fuse value is -ve, otherwise +ve

    //Create a mask for the value part of the fuse. This mask is to pull out the bottom (rail_fuse_config->fuse_total_bits-1) bits.
    const uint32 value_mask = (1 << (isSigned ? bits_assigned - 1 : bits_assigned)) - 1;

    int16 value = (int16) ((cpr_fuse_bits & value_mask) * packingFactor);

    //Now check the top bit to see if we need to flip the sign.
    if(isSigned && (cpr_fuse_bits & (1 << (bits_assigned - 1)))) {
        value = -value;
    }

    return value;
}

void cpr_utils_set_active_mode_setting(cpr_domain_id railId, cpr_mode_settings* newModeSetting, cpr_submode_settings* newSubModeSetting)
{
    uint32 railIdx = cpr_get_rail_idx( railId );

    /*
     * Store the current active mode to the previous mode
     * and update the active mode with the new mode
     */
    cpr_info.railStates[railIdx].previousMode = cpr_info.railStates[railIdx].activeMode;
    cpr_info.railStates[railIdx].activeMode   = newModeSetting;

    if(cpr_info.railStates[railIdx].activeMode)
    {
        /*
         * increment mode enablement counter for the new mode
         */
        cpr_info.railStates[railIdx].activeMode->enableCount++;

        /*
         * Set active submode if the new submode is not NULL
         */
        if(newSubModeSetting)
        {
            cpr_info.railStates[railIdx].activeMode->activeSubMode = newSubModeSetting;
        }
    }

    CPR_LOG_TRACE(" Active mode change: %u -> %u",
                  cpr_info.railStates[railIdx].previousMode ? cpr_info.railStates[railIdx].previousMode->mode : 0,
                  cpr_info.railStates[railIdx].activeMode   ? cpr_info.railStates[railIdx].activeMode->mode : 0);
    CPR_STATIC_LOG_RAIL_INFO(&cpr_info.railStates[railIdx]);
}

uint32 cpr_utils_get_global_fuse_rev(void)
{
    return cpr_info.cprRev;	
}
