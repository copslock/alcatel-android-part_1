/*============================================================================
  FILE:         sleep_target_config.c

  OVERVIEW:     This file provides target-specific functionality for the RPM.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

  $Header: //components/rel/rpm.bf/2.4/core/power/sleep/src/8953/sleep_target_config.c#2 $
  $DateTime: 2016/02/23 21:52:38 $
  $Author: pwbldsvc $
============================================================================*/


#include "msmhwio.h"
#include "msmhwiobase.h"
#include "qfprom_pte_hwio.h"

// retention programmed in uV ( 600000uV = 0.6V )
static const uint32 vddcx_pvs_retention_data[8] =
{
	//TODO
  /* 000 */ 600000,
  /* 001 */ 552000, 
  /* 010 */ 504000, 
  /* 011 */ 448000, 
  /* 100 */ 400000, 
  /* 101 */ 600000, 
  /* 110 */ 600000,
  /* 111 */ 600000
};

// retention programmed in uV ( 600000uV = 0.6V )
static const uint32 vddmx_pvs_retention_data[8] =
{
  /* 000 */ 696000,
  /* 001 */ 648000, 
  /* 010 */ 584000, 
  /* 011 */ 552000, 
  /* 100 */ 488000, 
  /* 101 */ 696000, 
  /* 110 */ 696000,
  /* 111 */ 696000
};

void set_vdd_mem_sleep_voltage (uint32 *vdd_mem)
{

  /* 3 MSB bits contain the values we need */
  uint32 pte_masked_val = HWIO_INF(QFPROM_CORR_PTE1, MX_RET_BIN);
//  uint32 lookup_val = pte_masked_val;
  *vdd_mem = vddmx_pvs_retention_data[pte_masked_val];
}
void set_vdd_dig_sleep_voltage (uint32 *vdd_dig)
{
  uint32 pte_masked_val; 	
  *vdd_dig = 592000;

  /* 3 MSB bits contain the values we need */
  pte_masked_val = HWIO_INF(QFPROM_CORR_PTE1, LOGIC_RETENTION);
//  uint32 lookup_val = pte_masked_val >> 5;
  *vdd_dig = vddcx_pvs_retention_data[pte_masked_val];
}


