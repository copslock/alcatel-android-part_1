/*
===========================================================================

FILE:         rpm_resources_config.c

DESCRIPTION:
  Per target resource configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/power/rpm/server/rpm_resources_config.c#2 $
$Date: 2015/12/15 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <stddef.h>
#include "rpm_resources_config.h"

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

#if (DAL_CONFIG_TARGET_ID == 0x9645)

static const rpm_resource_config_t temp_config_data[] =
{
    { RPM_LDO_A_REQ,        9 },  // VDDMX
    { RPM_SMPS_A_REQ,       5 },  // VDDCX
    { (rpm_resource_type)0, 0 }
};

// Active->Sleep
static const rpm_resource_config_t bimc_preprocess_dep[] =
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
    { RPM_BUS_MASTER_REQ,         3 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
    { (rpm_resource_type)0,       0 }
};

// Sleep->Active
static const rpm_resource_config_t bimc_postprocess_dep[] =
{
    { RPM_LDO_A_REQ,              9 },  // VDDMX
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
    { RPM_BUS_MASTER_REQ,         3 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
    { (rpm_resource_type)0,       0 },
};

#elif (DAL_CONFIG_TARGET_ID == 0x8952)

static const rpm_resource_config_t temp_config_data[] =
{
    { RPM_LDO_A_REQ,        3 },  // VDDMX
    { RPM_SMPS_A_REQ,       2 },  // VDDCX
	{ RPM_SMPS_A_REQ,       3 },  // S3
    { (rpm_resource_type)0, 0 }
};

// Active->Sleep
static const rpm_resource_config_t bimc_preprocess_dep[] =
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
    { RPM_BUS_MASTER_REQ,         3 },
    { (rpm_resource_type)0,       0 }
};

// Sleep->Active
static const rpm_resource_config_t *const bimc_postprocess_dep = NULL;

#elif (DAL_CONFIG_TARGET_ID == 0x8976)

static const rpm_resource_config_t temp_config_data[] =
{
    { RPM_SMPS_A_REQ,       6 },  // VDDMX
	{ RPM_SMPS_A_REQ,       2 },  // VDDCX
	{ (rpm_resource_type)0, 0 }
};

// Active->Sleep
static const rpm_resource_config_t bimc_preprocess_dep[] = 
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
	{ RPM_BUS_SLAVE_REQ,         24 },
	{ RPM_BUS_SLAVE_REQ,        198 },
	{ RPM_BUS_MASTER_REQ,         0 },
	{ RPM_BUS_MASTER_REQ,         1 },
	{ RPM_BUS_MASTER_REQ,       135 },
    { RPM_BUS_MASTER_REQ,         3 },
	{ RPM_BUS_MASTER_REQ,       102 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
	{ (rpm_resource_type)0,       0 }
};

// Sleep->Active
static const rpm_resource_config_t bimc_postprocess_dep[] = 
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
	{ RPM_BUS_SLAVE_REQ,         24 },
	{ RPM_BUS_SLAVE_REQ,        198 },
	{ RPM_BUS_MASTER_REQ,         0 },
	{ RPM_BUS_MASTER_REQ,         1 },
	{ RPM_BUS_MASTER_REQ,       135 },
    { RPM_BUS_MASTER_REQ,         3 },
	{ RPM_BUS_MASTER_REQ,       102 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
	{ (rpm_resource_type)0,       0 }
};

#elif (DAL_CONFIG_TARGET_ID == 0x8953)

static const rpm_resource_config_t temp_config_data[] =
{
    { RPM_SMPS_A_REQ,       7 },  // VDDMX
    { RPM_SMPS_A_REQ,       2 },  // VDDCX
    { (rpm_resource_type)0, 0 }
};
// Active->Sleep
static const rpm_resource_config_t bimc_preprocess_dep[] = 
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
	{ RPM_BUS_SLAVE_REQ,         24 },
	{ RPM_BUS_SLAVE_REQ,        104 },
	{ RPM_BUS_SLAVE_REQ,        137 },
	{ RPM_BUS_MASTER_REQ,         0 },
	{ RPM_BUS_MASTER_REQ,         1 },
	{ RPM_BUS_MASTER_REQ,         3 },
    { RPM_BUS_MASTER_REQ,         6 },
    { RPM_BUS_MASTER_REQ,        76 },
    { RPM_BUS_MASTER_REQ,       102 },
	{ RPM_BUS_MASTER_REQ,       108 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
	{ (rpm_resource_type)0,       0 }
};

// Sleep->Active
static const rpm_resource_config_t bimc_postprocess_dep[] = 
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
	{ RPM_BUS_SLAVE_REQ,         24 },
	{ RPM_BUS_SLAVE_REQ,        104 },
	{ RPM_BUS_SLAVE_REQ,        137 },
	{ RPM_BUS_MASTER_REQ,         0 },
	{ RPM_BUS_MASTER_REQ,         1 },
	{ RPM_BUS_MASTER_REQ,         3 },
    { RPM_BUS_MASTER_REQ,         6 },
    { RPM_BUS_MASTER_REQ,        76 },
    { RPM_BUS_MASTER_REQ,       102 },
	{ RPM_BUS_MASTER_REQ,       108 },
    { RPM_BUS_MASTER_LATENCY_REQ, 1 },
	{ (rpm_resource_type)0,       0 }
};

#else

static const rpm_resource_config_t *const temp_config_data = NULL;

// Active->Sleep
static const rpm_resource_config_t bimc_preprocess_dep[] =
{
    { RPM_BUS_SLAVE_REQ,          0 },
    { RPM_BUS_SLAVE_REQ,          2 },
    { RPM_BUS_MASTER_REQ,         3 },
    { (rpm_resource_type)0,       0 }
};

// Sleep->Active
static const rpm_resource_config_t *const bimc_postprocess_dep = NULL;

#endif

const rpm_resource_config_t * const RESOURCE_CONFIG_DATA = temp_config_data;
const rpm_resource_config_t * const BIMC_PREPROCESS_DEP  = bimc_preprocess_dep;
const rpm_resource_config_t * const BIMC_POSTPROCESS_DEP = bimc_postprocess_dep;

