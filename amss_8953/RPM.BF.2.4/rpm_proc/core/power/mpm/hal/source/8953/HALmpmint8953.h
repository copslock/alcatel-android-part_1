#ifndef HALMPMINT8953_H
#define HALMPMINT8953_H
/*
===========================================================================

FILE:         HALmpmint8936.h

DESCRIPTION:
  Target-specific enumerations for HALmpmint.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/power/mpm/hal/source/8953/HALmpmint8953.h#2 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
             QUALCOMM Proprietary and Confidential
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif


/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */

#include "comdef.h"

/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */

/*
 * HAL_mpmint_IsrType
 *
 * List of possible interrupt sources.  All of these are not necessarily
 * supported on the HW.
 *
 * NOTE: This list must be in the same order as the lists in the
 *       MPM driver.
 */
typedef enum
{
  /* Hard Wired */
  HAL_MPMINT_QTIMER_ISR                =  0,
  HAL_MPMINT_PEN_ISR                   =  1,
  HAL_MPMINT_TSENS_UPPER_LOWER_ISR     =  2,

  /* High Voltage */
  HAL_MPMINT_GPIO38_ISR                =  3,
  HAL_MPMINT_GPIO1_ISR                 =  4,
  HAL_MPMINT_GPIO5_ISR                 =  5,
  HAL_MPMINT_GPIO9_ISR                 =  6,
  HAL_MPMINT_APCS_WAKEUP_ISR           =  7,
  HAL_MPMINT_GPIO37_ISR                =  8,
  HAL_MPMINT_GPIO36_ISR                =  9,
  HAL_MPMINT_GPIO13_ISR                = 10,
  HAL_MPMINT_GPIO35_ISR                = 11,
  HAL_MPMINT_GPIO17_ISR                = 12,
  HAL_MPMINT_GPIO21_ISR                = 13,
  HAL_MPMINT_GPIO54_ISR                = 14,
  HAL_MPMINT_GPIO34_ISR                = 15,
  HAL_MPMINT_GPIO31_ISR                = 16,
  HAL_MPMINT_GPIO58_ISR                = 17,
  HAL_MPMINT_GPIO28_ISR                = 18,
  HAL_MPMINT_GPIO42_ISR                = 19,
  HAL_MPMINT_GPIO25_ISR                = 20,
  HAL_MPMINT_GPIO12_ISR                = 21,
  HAL_MPMINT_GPIO43_ISR                = 22,
  HAL_MPMINT_GPIO44_ISR                = 23,
  HAL_MPMINT_GPIO45_ISR                = 24,
  HAL_MPMINT_GPIO46_ISR                = 25,
  HAL_MPMINT_GPIO48_ISR                = 26,
  HAL_MPMINT_GPIO65_ISR                = 27,
  HAL_MPMINT_GPIO93_ISR                = 28,
  HAL_MPMINT_GPIO97_ISR                = 29,
  HAL_MPMINT_GPIO63_ISR                = 30,
  HAL_MPMINT_GPIO70_ISR                = 31,
  HAL_MPMINT_GPIO71_ISR                = 32,
  HAL_MPMINT_GPIO72_ISR                = 33,
  HAL_MPMINT_GPIO81_ISR                = 34,
  HAL_MPMINT_GPIO85_ISR                = 35,
  HAL_MPMINT_GPIO90_ISR                = 36,
  HAL_MPMINT_USB3_IFPS_RXTERM_IRQ      = 37,
  //UNUSED                             = 38,
  //UNUSED                             = 39,
  //UNUSED                             = 40,
  //UNUSED                             = 41,
  HAL_MPMINT_SDC1_DAT1_ISR             = 42,
  HAL_MPMINT_SDC1_DAT3_ISR             = 43,
  HAL_MPMINT_SDC2_DAT1_ISR             = 44,
  HAL_MPMINT_SDC2_DAT3_ISR             = 45,
  HAL_MPMINT_SRST_N_ISR                = 46,
  //UNUSED                             = 47,
  //UNUSED                             = 48,
  HAL_MPMINT_USB2_DPSE_HV_ISR          = 49,
  HAL_MPMINT_GPIO67_ISR                = 50,
  HAL_MPMINT_GPIO73_ISR                = 51,
  HAL_MPMINT_GPIO74_ISR                = 52,
  HAL_MPMINT_GPIO62_ISR                = 53,
  //UNUSED                             = 54,
  //UNUSED                             = 55,
  //UNUSED                             = 56,
  //UNUSED                             = 57,
  HAL_MPMINT_USB2_DMSE_HV_ISR          = 58,
  HAL_MPMINT_GPIO59_ISR                = 59,
  HAL_MPMINT_GPIO60_ISR                = 60,
  HAL_MPMINT_GPIO61_ISR                = 61,
  HAL_MPMINT_GPIO86_ISR                = 62,
  HAL_MPMINT_GPIO87_ISR                = 63,
  HAL_MPMINT_GPIO91_ISR                = 64,
  HAL_MPMINT_GPIO129_ISR               = 65,
  HAL_MPMINT_GPIO130_ISR               = 66,
  HAL_MPMINT_GPIO131_ISR               = 67,
  HAL_MPMINT_GPIO132_ISR               = 68,
  HAL_MPMINT_GPIO133_ISR               = 69,
  HAL_MPMINT_GPIO137_ISR               = 70,
  HAL_MPMINT_GPIO138_ISR               = 71,
  HAL_MPMINT_GPIO139_ISR               = 72,
  HAL_MPMINT_GPIO140_ISR               = 73,
  HAL_MPMINT_GPIO141_ISR               = 74,
  //UNUSED                             = 75,
  //UNUSED                             = 76,
  //UNUSED                             = 77,
  //UNUSED                             = 78,
  //UNUSED                             = 79,
  //UNUSED                             = 80,
  //UNUSED                             = 81,
  //UNUSED                             = 82,
  //UNUSED                             = 83,
  //UNUSED                             = 84,
  //UNUSED                             = 85,
  HAL_MPMINT_UIM_CARD_EVENT_IRQ        = 86,  
  HAL_MPMINT_UIM_BAT_IRQ               = 87,
  HAL_MPMINT_MPM_SPM_WAKE_ISR          = 88,    
  HAL_MPMINT_USB30_POWER_EVENT_IRQ     = 89,  
  //UNUSED                             = 90,
  //UNUSED                             = 91,
  //UNUSED                             = 92,
  //UNUSED                             = 93,
  //UNUSED                             = 94,
  //UNUSED                             = 95,  
  HAL_MPMINT_NUM                       = 96,

  /* Interrupts below this point are legacy definitions, not supported by this
   * version of the hardware. */
  HAL_MPMINT_NOT_DEFINED_ISR       = HAL_MPMINT_NUM,


} HAL_mpmint_IsrType;

#ifdef __cplusplus
}
#endif

#endif /* HALMPMINT8953_H */



