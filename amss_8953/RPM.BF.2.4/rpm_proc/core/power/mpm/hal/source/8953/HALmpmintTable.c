/*
===========================================================================

FILE:         HALmpmintPlatform.c

DESCRIPTION:
  This is the platform hardware abstraction layer implementation for the
  MPM interrupt controller block.
  This platform is for the RPM on 8953.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/power/mpm/hal/source/8953/HALmpmintTable.c#2 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <HALmpmint.h>
#include "HALmpmintInternal.h"

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

/*
 * DEFINE_IRQ
 *
 * Macro to define an IRQ mpmint_irq_data_type entry in the table
 */
#define DEFINE_IRQ( trigger, gpio, padPos ) \
  {                                                 \
    HAL_MPMINT_TRIGGER_##trigger,                   \
    gpio,                                           \
    padPos,                                         \
  }

/*
 * Target-specific interrupt configurations
 */
HAL_mpmint_PlatformIntType aInterruptTable[HAL_MPMINT_NUM] =
{
  /*          Trigger            GPIO                      Pad Bit          */
  /*          -------  -----------------------   -------------------------- */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_QTIMER_ISR                             00*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_PEN_ISR                                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_TSENS_UPPER_LOWER_ISR                    */
  DEFINE_IRQ( RISING,  38,                       13                         ), /* HAL_MPMINT_GPIO38_ISR                               */
  DEFINE_IRQ( RISING,  1,                        0                          ), /* HAL_MPMINT_GPIO1_ISR                                */
  DEFINE_IRQ( RISING,  5,                        1                          ), /* HAL_MPMINT_GPIO5_ISR                              05*/
  DEFINE_IRQ( RISING,  9,                        32+3                       ), /* HAL_MPMINT_GPIO9_ISR                                */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_APCS_WAKEUP                              */
  DEFINE_IRQ( RISING,  37,                       12                         ), /* HAL_MPMINT_GPIO37_ISR                               */
  DEFINE_IRQ( RISING,  36,                       11                         ), /* HAL_MPMINT_GPIO36_ISR                               */
  DEFINE_IRQ( RISING,  13,                       3                          ), /* HAL_MPMINT_GPIO13_ISR                             10*/
  DEFINE_IRQ( RISING,  35,                       10                         ), /* HAL_MPMINT_GPIO35_ISR                               */
  DEFINE_IRQ( RISING,  17,                       32+15                      ), /* HAL_MPMINT_GPIO17_ISR                               */
  DEFINE_IRQ( RISING,  21,                       5                          ), /* HAL_MPMINT_GPIO21_ISR                               */
  DEFINE_IRQ( RISING,  54,                       32+14                      ), /* HAL_MPMINT_GPIO54_ISR                               */
  DEFINE_IRQ( RISING,  34,                       9                          ), /* HAL_MPMINT_GPIO34_ISR                             15*/
  DEFINE_IRQ( RISING,  31,                       8                          ), /* HAL_MPMINT_GPIO31_ISR                               */
  DEFINE_IRQ( RISING,  58,                       32+29                      ), /* HAL_MPMINT_GPIO58_ISR                               */
  DEFINE_IRQ( RISING,  28,                       7                          ), /* HAL_MPMINT_GPIO28_ISR                               */
  DEFINE_IRQ( RISING,  42,                       32+18                      ), /* HAL_MPMINT_GPIO42_ISR                               */
  DEFINE_IRQ( RISING,  25,                       6                          ), /* HAL_MPMINT_GPIO25_ISR                             20*/
  DEFINE_IRQ( RISING,  12,                       2                          ), /* HAL_MPMINT_GPIO12_ISR                               */
  DEFINE_IRQ( RISING,  43,                       32+19                      ), /* HAL_MPMINT_GPIO43_ISR                               */
  DEFINE_IRQ( RISING,  44,                       32+20                      ), /* HAL_MPMINT_GPIO44_ISR                               */
  DEFINE_IRQ( RISING,  45,                       32+21                      ), /* HAL_MPMINT_GPIO45_ISR                               */
  DEFINE_IRQ( RISING,  46,                       32+22                      ), /* HAL_MPMINT_GPIO46_ISR                             25*/
  DEFINE_IRQ( RISING,  48,                       32+23                      ), /* HAL_MPMINT_GPIO48_ISR                               */
  DEFINE_IRQ( RISING,  65,                       32+13                      ), /* HAL_MPMINT_GPIO65_ISR                               */
  DEFINE_IRQ( RISING,  93,                       32+28                      ), /* HAL_MPMINT_GPIO93_ISR                               */
  DEFINE_IRQ( RISING,  97,                       21                         ), /* HAL_MPMINT_GPIO97_ISR                               */
  DEFINE_IRQ( RISING,  63,                       31                         ), /* HAL_MPMINT_GPIO63_ISR                             30*/
  DEFINE_IRQ( RISING,  70,                       23                         ), /* HAL_MPMINT_GPIO70_ISR                               */
  DEFINE_IRQ( RISING,  71,                       24                         ), /* HAL_MPMINT_GPIO71_ISR                               */
  DEFINE_IRQ( RISING,  72,                       25                         ), /* HAL_MPMINT_GPIO72_ISR                               */
  DEFINE_IRQ( RISING,  81,                       26                         ), /* HAL_MPMINT_GPIO81_ISR                               */
  DEFINE_IRQ( RISING,  85,                       27                         ), /* HAL_MPMINT_GPIO85_ISR                             35*/
  DEFINE_IRQ( RISING,  90,                       32+27                      ), /* HAL_MPMINT_GPIO90_ISR                               */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB3_IFPS_RXTERM_IRQ                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            40*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  32+7                       ), /* HAL_MPMINT_SDC1_DAT1_ISR                            */      
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  32+8                       ), /* HAL_MPMINT_SDC1_DAT3_ISR                            */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  32+9                       ), /* HAL_MPMINT_SDC2_DAT1_ISR                            */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  32+10                      ), /* HAL_MPMINT_SDC2_DAT3_ISR                          45*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  32+6                       ), /* HAL_MPMINT_SRST_N_ISR                               */
  DEFINE_IRQ( DUAL  ,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                              */
  DEFINE_IRQ( DUAL  ,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                */
  DEFINE_IRQ( DUAL  ,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_DPSE_HV                              */
  DEFINE_IRQ( RISING,  67,                       32+24                      ), /* HAL_MPMINT_GPIO67_ISR                             50*/
  DEFINE_IRQ( RISING,  73,                       32+25                      ), /* HAL_MPMINT_GPIO73_ISR                               */
  DEFINE_IRQ( RISING,  74,  					 32+26						), /* HAL_MPMINT_GPIO74_ISR                               */
  DEFINE_IRQ( RISING,  62,  					 32+5						), /* HAL_MPMINT_GPIO62_ISR                               */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            55*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( DUAL  ,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_DMSE_HV                              */
  DEFINE_IRQ( RISING,  59,                       32+1                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  60,                       32+2                       ), /* UNUSED                                            60*/
  DEFINE_IRQ( RISING,  61,                       32+4                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  86,                          4                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  87,                         14                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  91,                         32+11                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  129,                        15                       ), /* UNUSED                                            65*/
  DEFINE_IRQ( RISING,  130,                        16                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  131,                        17                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  132,                        18                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  133,                        20                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  137,                        22                       ), /* UNUSED                                            70*/
  DEFINE_IRQ( RISING,  138,                        30                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  139,                        19                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  140,                        28                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  141,                        29                       ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            75*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            80*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            85*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_UIM_CARD_EVENT                           */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_UIM_BAT_IRQ                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_MPM_SPM_WAKE_ISR                         */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB30_POWER_EVENT                        */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            90*/
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                              */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* UNUSED                                            95*/

};

