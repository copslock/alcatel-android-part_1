/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.4/core/power/railway_v2/src/8953/railway_config.c#4 $
$Date: 2016/01/21 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2012 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "CoreVerify.h"
#include "kvp.h"
#include "msmhwio.h"
#include "rpmserver.h"
#include "railway_config.h"
#include "pm_version.h"
#include "railway_adapter.h"
#include "cpr.h"

extern uint32 railway_get_corner_voltage(int rail, railway_corner corner);

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

//
// BEGIN config data; should migrate to the system enumeration data method
//
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .rail_type      = RAILWAY_RAIL_TYPE_MX,
            .vreg_name      = "vddmx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 7,

            .pmic_step_size = 8000,    // not used

            .initial_corner = RAILWAY_NOMINAL,

            .cpr_rail_id = CPR_RAIL_MX,
        },

        // VDDCX
        {
            .rail_type      = RAILWAY_RAIL_TYPE_LOGIC,
            .vreg_name      = "vddcx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 2,

            .pmic_step_size = 8000,     // not used

            .initial_corner = RAILWAY_NOMINAL,

            .cpr_rail_id = CPR_RAIL_CX,
        },
    },

    .num_rails = 2,
};
//
// END config data
//

const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */
void railway_init_early_proxy_votes(void)
{

}

void railway_init_proxies_and_pins(void)
{
      kvp_t *kvp;
      uint32 req = 1;
	  kvp = kvp_create(0);
	  kvp_reset(kvp);
	  req = (uint32_t)RAIL_VOLTAGE_LEVEL_TURBO;
	  kvp_put(kvp, PM_NPA_KEY_LEVEL_RAIL_VOLTAGE, sizeof(req), (void *)&req);
	  rpm_send_init_proxy_vote(RPM_SMPS_A_REQ,  // Cx is S2
	                           2,
	                           0,               // APPS is 0
	                           kvp);	
	  
}

void railway_target_init(void)
{
}

void railway_remap_target_specific_corners(rpm_translation_info *info, railway_voter_irep *req)
{
}

