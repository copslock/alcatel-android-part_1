/**
 * @file:  cpr_isr.h
 * @brief: 
 * 
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2016/01/22 01:57:42 $
 * $Header: //components/rel/rpm.bf/2.4/core/api/power/cpr_isr.h#2 $
 * $Change: 9769842 $
 * 
 */
#ifndef CPR_ISR_H
#define	CPR_ISR_H

#include "cpr_defs.h"

/**
 * Gets the CPR interrupt number for the given rail
 */
boolean cpr_isr_get_interrupt(cpr_rail_id_t railId, uint32 *interrupt);

/**
 * Runs the CPR ISR for the given rail.
 */
void cpr_isr_process(cpr_rail_id_t railId);

#endif

