# ==========================================================================
#
#  RPM build system launcher
#
# Copyright (c) 2015 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
# ==========================================================================
# $Header: //components/rel/rpm.bf/2.4/build/build_8953.sh#1 $

export BUILD_ASIC=8953
export MSM_ID=8953
export HAL_PLATFORM=8953
export TARGET_FAMILY=8953
export CHIPSET=msm8953
export SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN
#cd './rpm_proc/build/'

python ./build_common.py $@
