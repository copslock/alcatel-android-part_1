@echo off
rem ==========================================================================
rem
rem  RPM build system launcher
rem
rem Copyright (c) 2015 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem ==========================================================================
rem $Header: //components/rel/rpm.bf/2.4/build/build_8953.bat#1 $

SET BUILD_ASIC=8953
SET MSM_ID=8953
SET HAL_PLATFORM=8953
SET TARGET_FAMILY=8953
SET CHIPSET=msm8953
SET SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN
python build_common.py %*
