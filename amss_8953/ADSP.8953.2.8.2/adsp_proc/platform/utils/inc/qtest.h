#ifndef QTEST_H
#define QTEST_H

#include <stdint.h>
#include <stdio.h>
/**
 * @returns, 0 N number of times then returns non zero.
 *           N can be specified as the passes param to
 *           qtest_init.
 */
int qtest_test_failure(void);

/**
 * push the context to the trace stack
 */
void qtest_trace_push(const char*);

/**
 * pop the context from the trace stack
 */
void qtest_trace_pop(void);

/**
 * init the unit test environment
 * @param allocs, max number of outstanding allocations to track
 * @param tracedepth, the depth of the trace stack
 * @param passes, number of times qtest_test_failure
 *        will return 0
 */
void qtest_init(int allocs, int tracedepth, int64_t passes);

/**
 * @param verbose, if non 0, the leaks are printed to stderr
 * @param needed, returns the number of outstanding allocations needed
 * @return, 0 on success, -1, if needed exceeded allocs or number of leak occurred
 */
int qtest_deinit(FILE* verbose, int* needed);

/**
 * overrides stdlib's allocators to use the qtest ones
 */
#include <stdlib.h>

#ifdef QTEST

#define qtest_real_malloc(sz)         ((malloc)(sz))
#define qtest_real_calloc(cnt, sz)    ((calloc)(cnt, sz))
#define qtest_real_realloc(ptr, sz)   ((realloc)(ptr, sz))
#define qtest_real_free(ptr)          ((free)(ptr))

void *qtest_malloc(size_t size);
void qtest_free(void *ptr);
void *qtest_calloc(size_t nmemb, size_t size);
void *qtest_realloc(void *ptr, size_t size);

#define malloc(sz)         qtest_malloc(sz)
#define calloc(cnt, sz)    qtest_calloc(cnt, sz)
#define realloc(ptr, sz)   qtest_realloc(ptr, sz)
#define free(ptr)          qtest_free(ptr)

/**
 * overrides VERIFY to push and pop functions to trace
 */
#include "verify.h"
#undef VERIFY
#define VERIFY(val) \
   do {\
      VERIFY_IPRINTF(":info: calling: %s", #val);\
      qtest_trace_push(__V_FILE_LINE__  " " #val); \
      if ((0 != qtest_test_failure()) || (0 == (val))) {\
         nErr = nErr == 0 ? -1 : nErr;\
         VERIFY_EPRINTF(":error: %d: %s", nErr, #val);\
         qtest_trace_pop(); \
         goto bail;\
      } else {\
         VERIFY_IPRINTF(":info: passed: %s", #val);\
         qtest_trace_pop(); \
      }\
   } while(0)
#endif //QTEST

#endif //QTEST_H
