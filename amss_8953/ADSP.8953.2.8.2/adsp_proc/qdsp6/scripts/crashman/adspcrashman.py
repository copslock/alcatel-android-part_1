# ::*****************************************************************************************************************************************************************
# :: Filename: adspcrashman.py
# ::
# :: Version : External_Release_1.33
# ::
# :: Usage:
# ::    adspcrashman.py -t Target_Name[8994/8974/9x25/8x10/9x35/8084] -b EBI_Binary_File -o Output_folderpath -c CRM_Buildpath -e CustomerprovidedOBJPath[Optional]
# ::
# :: Example:
# ::    adspcrashman.py  -t 8974 -b C:\Dropbox\DDRSCO.bin -o C:\dropbox\123456 -c C:\YUG_M8960AAAAANAZL1047\lpass_proc -e C:\Dropbox\8k_dumps\
# ::
# ::*****************************************************************************************************************************************************************
import os
import re
import subprocess
import sys
import time
import datetime
import fnmatch
import shutil, errno
import socket

VALID_TARGETS=["8994","8974","9x25","9x35","8x10","8084","8x26","8996","8998","8952","8992","8976","8953","8937"]

usage = "\n\
python adspcrashman.py -t[arget] <TARGET> -d[ump] <DumpFile> -o[utput] <Output_Path> -b[uild] <CRM_Buildpath> -e[lf] <CustomerprovidedOBJPath[Optional]>\n\n\
Mandatory Arguments:\n \
  -t, --target    Targets Supported on Crashman \n \
  -d, --dump      Dump File along with path \n \
  -o, --output    Output folder \n \
  -b, --build     ADSP Build Path Location\n\n\
Optional Arguments:\n\
   -e, --elf              Customer ELF Location \n\
   -gdb,--gdb             GDB based crashman\n\
   -lite, --lite          Crashman Lite Version(Only Dumps are loaded)\n\
   -fullload, --fullload  Loads the complete RAM Dumps\n\
   -sofile, --sofile      Provide dynamic So files location. Presently Supports only 8952/8953/8937/8976/8996/8998/8910\n\
   -use_t32,--use_t32     Use T32 from a shared location\n\
   -smmu32, --smmu32      Provide 32 bit vmlinux path\n\
   -smmu64, --smmu64      Provide 64 bit vmlinux path\n\
   -load_etm, --load_etm  Enable etm \n\
   -vmlinux32,--vmlinux32 Provide 32 bit vmlinux path\n\
   -vmlinux64,--vmlinux64 Provide 64 bit vmlinux path\n\
\n\nExample:\n\n\
  python adspcrashman.py -t 8994 -d C:\DDRCS0.BIN -o C:\output -b \\\\snowcone\\builds698\\PROD\\ADSP.8994.2.6.1-00120-00374-1\\adsp_proc\n\n\
Example with -elf argument: \n\n\
  python adspcrashman.py  -target 9x35 -dump C:\DDRCS0.BIN -output C:\output -build \\\\cone\\builds634\\PROD\\ADSP.BF.2.4.1.c1-00021-M9635AAAAANAZL-1 -elf \\\\rover\\hyd_dspfw\\ADSP_Tools\\TriageTeam\\Users\\Praveen\\Testing\9x35\\ELFS\n\n\
Example with -fullload argument: \n\n\
  python adspcrashman.py -t 8952 -o C:\Temp -d \\lab5385\TriageTeam\Crashman\TestSuite\8952\mser_171\DDRCS0.BIN -b \\snowcone\builds677\PROD\ADSP.8952.2.6-00107-00000-1 -elf \\lab5385\TriageTeam\Crashman\TestSuite\8952\mser_171 -fullload \\lab5385\TriageTeam\Crashman\TestSuite\8952\mser_171\n\n\
Example with -sofile argument: \n\n\
  python adspcrashman.py -t 8910  -d \\lab5385\TriageTeam\Crashman\TestSuite\8910\evernote_dead_ramdump_1\DDRCS0.BIN -o c:\output -b \\chronicle\zipbuild225\PROD\ADSP.BF.2.2.1-00023-M8610AAAAAAAZL-1 -elf \\lab5385\TriageTeam\Crashman\TestSuite\8910\jpege_CAMERA_JPEGE_01_01_07 -sofile \\lab5385\TriageTeam\Crashman\TestSuite\8910\jpege_CAMERA_JPEGE_01_01_07\n\n \
Example with -use_t32 argument: \n\n\
  python adspcrashman.py  -target 9x35 -dump C:\DDRCS0.BIN -output C:\output -build \\\\cone\\builds634\\PROD\\ADSP.BF.2.4.1.c1-00021-M9635AAAAANAZL-1 -e \\\\rover\\hyd_dspfw\\ADSP_Tools\\TriageTeam\\Users\\Praveen\\Testing\9x35\\ELFS -use_t32 \\\\rover\\hyd_dspfw\\ADSP_Tools\\T32 \n\n\
Example with -smmu64 argument: \n\n\
  python adspcrashman.py -t 8952 -o C:\Temp -d \\\\lab3079\\Sahara\\matting_171\\DDRCS0.BIN -b \\\\snowcone\\builds677\\PROD\\ADSP.8952.2.6-00107-00000-1 -smmu64 \\\\holi\\builds539\\TEST\\LA.BR.1.3.1-27101-8952.1-1\\LINUX\\android\\out\\target\\product\\msm8952_64\\obj\\KERNEL_OBJ\\vmlinux\n\n \
Example with -load_etm argument: \n\n\
  python adspcrashman.py -t 8952 -o C:\Temp -d \\\\lab5385\\TriageTeam\\Crashman\\TestSuite\\8952\\Port_COM82_log2ETM\\DDRCS0.BIN -b \\\\snowcone\\builds732\\INTEGRATION\\ADSP.8952.2.6-00137-00000-1 -load_etm  -vmlinux64 \\\\diwali\\builds534\\TEST\\LA.BR.1.3.1-34103-8952.1-5\\LINUX\\android\\out\\target\\product\\msm8952_64\\obj\\KERNEL_OBJ\\vmlinux\n\n \
Example with -linux_ramdump_parser argument for External Customers: \n\n\
  python adspcrashman.py -t 8976 -o C:\Temp -d \\lab5385\TriageTeam\Crashman\TestSuite\8952\fastcv\DDRCS0.BIN -e \\lab5385\TriageTeam\Crashman\TestSuite\8952\fastcv -smmu64 \\lab5385\TriageTeam\Crashman\TestSuite\8952\fastcv\vmlinux_8976 -b \\chronicle\zipbuild222\PROD\ADSP.8976.2.6-00087-00000-1 -linux_ramdump_parser \\rover\hyd_dspfw\ADSP_Tools\TriageTeam\Users\linux-ramdump-parser-v2\n\n \
"


hostname = socket.getfqdn()
customerflag = False
hydserver = True

if not "qualcomm.com" in hostname:
    customerflag = True
elif "ap.qualcomm.com" in hostname:
    hydserver = True
else:    
    hydserver = False

    
if hydserver==True:
    ramparse_path = "\\\\rover\\hyd_dspfw\\ADSP_Tools\\linux-ramdump-parser-v2"
    T32_path = "\\\\rover\\hyd_dspfw\\ADSP_Tools\\T32"
    DDModular_Script = "\\\\rover\hyd_dspfw\ADSP_Tools\TriageTeam\DD_Modular_Script"
else:    
    ramparse_path = "\\\\V7-SAN1P80362\\Crashman\\linux-ramdump-parser-v2"
    T32_path = "\\\\V7-SAN1P80362\\Crashman\\T32"
    DDModular_Script = "\\\\V7-SAN1P80362\\Crashman\\DD_Modular_Script"
    
if len(sys.argv) < 7:
    print usage
    sys.exit()
    
CurrDirectory =  os.getcwd()
curdir = sys.argv[0]
if "\\" in curdir or "//" in curdir:
    CurrDirectory = curdir[:-16]

Crashman_Version = "External_Release_1.33"
print "************************************"
print "*******Crashman Started*************"
print "************************************"
print "Crashman version          : ",Crashman_Version

try:
   import argparse
   from argparse import RawTextHelpFormatter
except ImportError:
   print 'Python Version is: ' + sys.version
   print 'Crashman requires Python version 2.7.6 and above.'
   print 'If you have Python version 2.7.6 installed, please check the environment path.' 
   sys.exit(0)
   
   
if sys.version:
   print "Python Version Installed  : ", ((sys.version.rstrip('\n')).split('(default')[0]).split(' ')[0]
   if sys.version_info[0] != 2 and sys.version_info[1] != 7:
       print "ERROR:: You are not using Python 2.7.6. Please use 2.7.6, preferably 2.7.6 and above"
       sys.exit(0)
else:
   print '\n\nERROR: Python not installed!!!'
   print 'Recommended Python Installation v2.7.8'
   print 'Crashman tested on Python versions- v2.7.6  v2.5.2  v2.6.2  v2.7.2  v3.0.1  v2.7.6'
   print 'If installed already, please verify if respective path added to PATH environment variable!!!\n\n'
   sys.exit(0)

tool_version = ''.join(['perl -v'])
proc = subprocess.Popen(tool_version, stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
if out:
    try:
        if " (v" in out:
            perl_version =  out.split('(')[1].split(')')[0]
        elif "This is perl," in out:    
            perl_version =  out.split('This is perl,')[1].split('built')[0]
        else:
            perl_version = "Untested Perl Version"
        print "Perl Version Installed    :", perl_version
    except:
        print "Perl Version Installed    :"
else:
   print '\n\nERROR: Perl not installed!!!'
   print 'Recommended perl Installation v5.6.1'
   print 'Crashman tested on Perl versions- v5.10.1 , v5.6.1 , v5.8.7 , v5.12.4 , v5.14.2 '
   print 'If installed already, please verify if respective path added to PATH environment variable!!!\n\n'
   sys.exit(0)


parser = argparse.ArgumentParser(description=usage, formatter_class=RawTextHelpFormatter)  
parser.add_argument('-target','--target', help='TARGET: '+', '.join(VALID_TARGETS), dest='TARGET', action='store')
parser.add_argument('-dump','--dump', help="DUMPFILE: Please Provide Proper Dump File Location", dest='DUMPFILE', action='store')
parser.add_argument('-output','--output', help="OUTPUT: Please Provide Proper Output Path Location", dest='OUTPUT', action='store')
parser.add_argument('-build','--build', help="CRM: Please Provide Proper CRM Build Path Location", dest='BUILD', action='store')
parser.add_argument('-elf','--elf', default="",help="ELF: Please Provide Proper ELF Path Location", dest='ELF', action='store')
parser.add_argument('-lite','--lite',help="To Enbale Crashman Lite give -lite or --lite",action='store_true')
parser.add_argument('-asha','--asha',help="To Enbale Crashman for ASHA give -a or -asha",action='store_true')
parser.add_argument('-frameworktest','--frameworktest',help="for automated testing give -frameworktest or --frameworktest",action='store_true')
parser.add_argument('-gdb','--gdb',help="To Enbale GDB Simulator give -g or -gdb",action='store_true')
parser.add_argument('-slpi','--slpi',help="To Enable SLPI for 8996 give -s or -slpi",action='store_true')
parser.add_argument('-sofile','--sofile', default="",help="ELF: Please Provide Dynamic so Path Location", dest='SOFILE', action='store')
parser.add_argument('-fullload','--fullload', default="",help="Full Load: please provide full dump path", dest='FL',action='store')
parser.add_argument('-smmu32','--smmu32', help="vmlinux 32 bit: Please Provide Proper 32 bit vmlinux Path Location", dest='SMMU32', action='store')
parser.add_argument('-smmu64','--smmu64', help="vmlinux 64 bit: Please Provide Proper 64 bit vmlinux Path Location", dest='SMMU64', action='store')
parser.add_argument('-use_t32','--use_t32', help="Please Provide T32 Path Location", dest='trace32', action='store')
parser.add_argument('-load_etm','--load_etm', help="To Enable ETM give -load_etm", action='store_true')
parser.add_argument('-vmlinux32','--vmlinux32', help="vmlinux 32 bit: Please Provide Proper 32 bit vmlinux Path Location", dest='VMLINUX32', action='store')
parser.add_argument('-vmlinux64','--vmlinux64', help="vmlinux 64 bit: Please Provide Proper 64 bit vmlinux Path Location", dest='VMLINUX64', action='store')
parser.add_argument('-frpcshell','--frpcshell', default="",help="Full Load: please provide fast rpc shell name along with path", dest='FRPC',action='store')
parser.add_argument('-linux_ramdump_parser','--linux_ramdump_parser', help="Linux Ram Dump Parser: Please Provide linux ram dump parser scripts path", dest='LINUX_RDP', action='store')
parser.add_argument('-start_address','--start_address', help='Provide the Start Address', dest='STARTADDR', action='store')
parser.add_argument('-wp_smmu','--wp_smmu', help="WP SMMU pagetable path: Please Provide Proper WP SMMU pagetable Path Location", dest='WP_SMMU', action='store')

opts = parser.parse_args()


t32_loc = opts.trace32
start_address = opts.STARTADDR
if not start_address: start_address = False
if not t32_loc: t32_loc=False
if t32_loc != False:
    T32_path = t32_loc

wp_smmu_enable = opts.WP_SMMU
if not wp_smmu_enable: wp_smmu_enable = False
smmu_32bt = opts.SMMU32
if not smmu_32bt: smmu_32bt=False
smmu_64bt = opts.SMMU64
if not smmu_64bt: smmu_64bt=False
vmlinux_32bt = opts.VMLINUX32
if not vmlinux_32bt: vmlinux_32bt=False
vmlinux_64bt = opts.VMLINUX64
if not vmlinux_64bt: vmlinux_64bt=False
targetid = opts.TARGET
if targetid == "8917":
    targetid = "8937"
INDumpPath = opts.DUMPFILE
CustomerPath = opts.ELF
if not CustomerPath: CustomerPath=False
SOFILE = opts.SOFILE
if not SOFILE: SOFILE=False
UniqueNumber="NotGiven"
crashman_lite = opts.lite
load_dump_full = opts.FL
if not load_dump_full: load_dump_full=False
frpcshell = opts.FRPC
if not frpcshell: 
    frpcshell_name = False
    frpcshell=False
if frpcshell: 
    frpcshell_name = frpcshell.split('\\')[-1].split('.')[0]
asha_flag = opts.asha
quit_flag = opts.frameworktest
gdb_flag  = opts.gdb
slpi_flag = opts.slpi
etm_flag = opts.load_etm
smmu_enable=False
if customerflag==True:
    ramparse_path = opts.LINUX_RDP
    if not ramparse_path: ramparse_path="False"
        
if targetid == "8937" or targetid == "8953":
    if customerflag==False:
        if hydserver==True:
            ramparse_path = "\\\\rover\\hyd_dspfw\\ADSP_Tools\\TriageTeam\\8937\\linux-ramdump-parser-v2"
        else:
            ramparse_path = "\\\\V7-SAN1P80362\\Crashman\\8937\\linux-ramdump-parser-v2"

if load_dump_full!=False:
    if not os.path.isfile(load_dump_full+"\\load.cmm"):
        print "**************************************************************"
        print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)
    if not os.path.isfile(load_dump_full+"\\DDRCS0.BIN") or not os.path.isfile(load_dump_full+"\\DDRCS0.bin"):
        print "**************************************************************"
        print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)
    if not os.path.isfile(load_dump_full+"\\DDRCS1.BIN") or not os.path.isfile(load_dump_full+"\\DDRCS1.bin"):
        print "**************************************************************"
        print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)
    

if crashman_lite==True and gdb_flag==True:
    print "Crashman Lite and GDB are not concurrently supported. Please select any one option"
    sys.exit(0)
    
if not os.path.isfile(INDumpPath):
    print "Dump File is not accessible/available. Please Check the File Path."
    sys.exit(0)


i = datetime.datetime.now()
dateref = str("{0:0=2d}".format(i.month))+"_"+str("{0:0=2d}".format(i.day))+"_"+str(i.hour)+"h"+str(i.minute)+"m"+str(i.second)+"s"
if not opts.OUTPUT:
    opts.OUTPUT = "C:\\Temp_"+targetid
if len(opts.OUTPUT) > 34:
    print "Output folder path length is too long. Please reduce the output path length"
OUTDumpPath = opts.OUTPUT+"\\Crashman_"+dateref+"\\Logs"
if not os.path.exists(OUTDumpPath):
    try:    
        os.makedirs(OUTDumpPath) 
        os.makedirs(OUTDumpPath+"\\temp") 
        if gdb_flag != True:
            os.makedirs(OUTDumpPath+"\\Qurt_logs") 
            os.makedirs(OUTDumpPath+"\\ulog") 
            os.makedirs(OUTDumpPath+"\\adsppm") 
    except:
        print "Output Folder is not accessible. Please Give Proper Output path."
        sys.exit(0)
        
print "Crashman Output Folder    : ",OUTDumpPath
print "Processing Inputs Please Wait..."

CRM_buildpath = opts.BUILD
if os.path.exists(CRM_buildpath+"\\adsp_proc"):
    CRM_buildpath = CRM_buildpath+"\\adsp_proc"
if os.path.exists(CRM_buildpath+"\\slpi_proc"):
    slpi_flag=True
    CRM_buildpath = CRM_buildpath+"\\slpi_proc"

if "slpi_proc" in CRM_buildpath:
    slpi_flag=True

if not os.path.exists(CRM_buildpath):
    print "ADSP Build Path is not accessible/available. Please Check the Build Path."
    sys.exit(0)
    
if not os.path.exists(CRM_buildpath+"\\core"):
    print "Core folder is not present in ADSP Build. Please provide proper CRM Build Path."
    sys.exit(0)

Temp_DIR = "C:\Temp"
if not os.path.exists(Temp_DIR):
    os.makedirs(Temp_DIR) 

   
dump_path=None
if os.path.isfile(INDumpPath):
    if "DDRCS0.BIN" in INDumpPath or "DDRCS1.BIN" in INDumpPath or "DDRCS0.bin" in INDumpPath or "DDRCS1.bin" in INDumpPath:
        dump_path = INDumpPath.split('\DDRCS0.BIN')[0].split('\DDRCS1.BIN')[0].split('\DDRCS0.bin')[0].split('\DDRCS1.bin')[0]
    elif "DDRCS0_0.BIN" in INDumpPath or "DDRCS1_1.BIN" in INDumpPath or "DDRCS0_0.bin" in INDumpPath or "DDRCS1_1.bin" in INDumpPath:
        dump_path = INDumpPath.split('\DDRCS0.BIN')[0].split('\DDRCS1.BIN')[0].split('\DDRCS0.bin')[0].split('\DDRCS1.bin')[0]

if smmu_32bt!=False or smmu_64bt!=False:
    ddrcs0_offset = None
    ddrcs1_offset = None
    ocimem_offset = None
    try:
        if os.path.isfile(dump_path+"\\dump_info.txt"):
            f_dump = open(dump_path+"\\dump_info.txt",'r')
            for line in f_dump:
                if "DDRCS0" in line:
                    offset = line.split()
                    ddrcs0_offset = offset[1]
                    ddrcs0_size = offset[2]
                if "DDRCS1" in line:
                    offset = line.split()
                    ddrcs1_offset = offset[1]
                    ddrcs1_size = offset[2]
                if "OCIMEM.BIN" in line:
                    offset = line.split()
                    ocimem_offset = offset[1]
                    ocimem_size = offset[2]
                    
            f_dump.close()
    except:
        print "dump_info.txt not exists!!"
        
    offset = " "
    if ddrcs0_offset!=None and ddrcs1_offset!=None:
        ddrcs0_offset = hex(int(ddrcs0_offset,16)).replace('L','')
        ddrcs1_offset = hex(int(ddrcs1_offset,16)).replace('L','')
        ocimem_offset = hex(int(ocimem_offset,16)).replace('L','')
        
        ddrcs0_size = hex(int(ddrcs0_offset,16) + int(hex(int(ddrcs0_size)).replace('L',''),16) - 1).replace('L','')
        ddrcs1_size = hex(int(ddrcs1_offset,16) + int(hex(int(ddrcs1_size)).replace('L',''),16) - 1).replace('L','')
        ocimem_size = hex(int(ocimem_offset,16) + int(hex(int(ocimem_size)).replace('L',''),16) - 1).replace('L','')

        phy_offset  = ddrcs0_offset
        if int(ddrcs1_offset,16) < int(ddrcs0_offset,16):
            phy_offset = ddrcs1_offset
        offset = '''--ram-file %s\\DDRCS0.BIN %s %s --ram-file %s\\DDRCS1.BIN %s %s --ram-file %s\\OCIMEM.BIN %s %s --phys-offset %s'''%(dump_path,ddrcs0_offset,ddrcs0_size,dump_path,ddrcs1_offset,ddrcs1_size,dump_path,ocimem_offset,ocimem_size,phy_offset)
        # print offset
        
if vmlinux_32bt!=False or vmlinux_64bt!=False:
    check_path=dump_path
    if check_path!=None:
        if not os.path.isfile(check_path+"\\load.cmm"):
            print "**************************************************************"
            print "If you are using -vmlinux32/vmlinux64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(check_path+"\\DDRCS0.BIN"):
            print "**************************************************************"
            print "If you are using -vmlinux32/vmlinux64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(check_path+"\\DDRCS1.BIN"):
            print "**************************************************************"
            print "If you are using -vmlinux32/vmlinux64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
    else:        
        print "**************************************************************"
        print "If you are using -vmlinux32/vmlinux64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)

    if vmlinux_64bt!=False:
        if not os.path.exists(ramparse_path):
            print "**************************************************************"
            print "Linux ram dump parser path is not accessible/available. Please Check the Path"
            print "Expecting aarch64-linux-gnu-gdb.exe/aarch64-linux-gnu-nm.exe in linux ram dump parser path location!!!"
            print "**************************************************************"
            sys.exit(0)
        if os.path.isfile(vmlinux_64bt):
            command = '''python %s\\ramparse.py --64-bit -a %s  -o %s   -v %s  --parse-debug-image --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe'''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",vmlinux_64bt,ramparse_path,ramparse_path)
        else:
            print "**************************************************************"
            print "vmlinux path is not accessible/available. Please Check the Path "
            print "**************************************************************"
            sys.exit(0)
    if vmlinux_32bt!=False:
        if not os.path.exists(ramparse_path):
            print "**************************************************************"
            print "Linux ram dump parser path is not accessible/available. Please Check the Path "
            print "Expecting aarch64-linux-gnu-gdb.exe/aarch64-linux-gnu-nm.exe in linux ram dump parser path location!!!"
            print "**************************************************************"
            sys.exit(0)
        if os.path.isfile(vmlinux_32bt):
            command = '''python %s\\ramparse.py -a %s  -o %s   -v %s  --parse-debug-image --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe '''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",vmlinux_32bt,ramparse_path,ramparse_path)
        else:
            print "**************************************************************"
            print "vmlinux path is not accessible/available. Please Check the Path "
            print "**************************************************************"
            sys.exit(0)
    
    print "Processing ETM Dump... it might take 4-5 minutes. Please wait..."
    os.system(command)

if etm_flag == True:
    dumpfile = None
    if "tmc-etr.bin" in INDumpPath:
        dumpfile = INDumpPath
    elif "etr.bin" in INDumpPath:
        dumpfile = INDumpPath
    elif os.path.isfile(OUTDumpPath+"\\temp\\tmc-etr.bin"):
        dumpfile = OUTDumpPath+"\\temp\\tmc-etr.bin"
    elif os.path.isfile(OUTDumpPath+"\\temp\\etr.bin"):
        dumpfile = OUTDumpPath+"\\temp\\etr.bin"
        
    if dumpfile == None:
        print "ETM Dump File is not accessible/available. Please Check the File Path."
        sys.exit(0)

    if CustomerPath==False:
        CustomerPath = CRM_buildpath+"\\build\\ms"
        
    elfname = CustomerPath+"\\M"+targetid+"AAAAAAAAQ1234.elf"
    if not os.path.isfile(elfname):
        print "ELF file is not accessible/available @ "+ elfname
        sys.exit(0)

    
    print "\nStarting T32 for ETM Dump."
    print "************************************"
    t32_file = OUTDumpPath+"\\temp\\config_sim_usb.t32"
    f_t32 = open(t32_file,'w+')
    f_t32.write("OS=\n")
    f_t32.write("ID=SIM_${0}            ; unique ID needed for temporary file name uniqueness\n")
    f_t32.write("TMP="+OUTDumpPath+"\\temp"+"           ; T32 temp file names are prepended with the unique ID above\n")
    if quit_flag == True:
        f_t32.write("SYS="+T32_path+"                ; make system directory the same as the executable directory\n\n")
    else:
        if t32_loc != False:
            f_t32.write("SYS="+T32_path+"                ; make system directory the same as the executable directory\n\n")
        else:
            f_t32.write("SYS=C:\T32                ; make system directory the same as the executable directory\n\n")
    f_t32.write("SCREEN=\n")
    f_t32.write("HEADER=${0}\n\n")
    f_t32.write("; Ethernet on Host information \n")
    f_t32.write("PBI=SIM\n")
    f_t32.write("\n")
    f_t32.write("; Printer settings\n")
    f_t32.write("PRINTER=WINDOWS\n\n")
    f_t32.write("; Screen fonts\n")
    f_t32.write("SCREEN=\n")
    f_t32.write("FONT=SMALL\n\n")
    f_t32.close()

    if t32_loc != False:
        Path = T32_path+"\\bin\\windows64"
    else:
        T32_path = "C:\\T32"
        Path = "C:\\T32\\bin\\windows64"
    
    if os.path.exists(Path):
        command = '''start %s\\bin\\windows64\\t32mqdsp6.exe -c %s, %s %s %s '''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\load_adsp_etm_dump.cmm",elfname,dumpfile)
        os.system(command)
    else:    
        command = '''start %s\\t32mqdsp6.exe -c %s, %s %s %s '''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\load_adsp_etm_dump.cmm",elfname,dumpfile)
        os.system(command)
    sys.exit(0)
        
        
flag = 0
if smmu_32bt!=False:
    if not os.path.exists(ramparse_path):
        print "**************************************************************"
        print "Linux ram dump parser path is not accessible/available. Please Check the Path "
        print "Expecting aarch64-linux-gnu-gdb.exe/aarch64-linux-gnu-nm.exe in linux ram dump parser path location!!!"
        print "**************************************************************"
        sys.exit(0)
    if os.path.isfile(smmu_32bt):
        if targetid == "8953":
            command = '''python %s\\ramparse.py  -a %s  -o %s   -v %s  --print-iommu-pg-tables --force-hardware %s %s --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe'''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",smmu_32bt,"8937",offset,ramparse_path,ramparse_path)
        else:
            command = '''python %s\\ramparse.py  -a %s  -o %s   -v %s  --print-iommu-pg-tables --force-hardware %s %s --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe'''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",smmu_32bt,targetid,offset,ramparse_path,ramparse_path)
        print "Processing SMMU pagetable parsing... it might take 4-5 minutes. Please wait..."
        # print command
        os.system(command)
        smmu_enable=True
    else:
        print "**************************************************************"
        print "vmlinux path is not accessible/available. Please Check the Path "
        print "**************************************************************"
    load_dump_full=dump_path
    if dump_path!=None:
        if not os.path.isfile(dump_path+"\\load.cmm"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(dump_path+"\\DDRCS0.BIN") or not os.path.isfile(dump_path+"\\DDRCS0.bin"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(dump_path+"\\DDRCS1.BIN") or not os.path.isfile(dump_path+"\\DDRCS1.bin"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
    else:        
        print "**************************************************************"
        print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)
    flag = 1
    
if smmu_64bt!=False:
    if not os.path.exists(ramparse_path):
        print "**************************************************************"
        print "Linux ram dump parser path is not accessible/available. Please Check the Path "
        print "Expecting aarch64-linux-gnu-gdb.exe/aarch64-linux-gnu-nm.exe in linux ram dump parser path location!!!"
        print "**************************************************************"
        sys.exit(0)
    if os.path.isfile(smmu_64bt):
        if targetid == "8953":
            command = '''python %s\\ramparse.py --64-bit -a %s  -o %s   -v %s  --print-iommu-pg-tables --force-hardware %s %s --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe '''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",smmu_64bt,"8937",offset,ramparse_path,ramparse_path)
        else:
            command = '''python %s\\ramparse.py --64-bit -a %s  -o %s   -v %s  --print-iommu-pg-tables --force-hardware %s %s --nm-path %s\\aarch64-linux-gnu-nm.exe --gdb-path %s\\aarch64-linux-gnu-gdb.exe '''%(ramparse_path,dump_path,OUTDumpPath+"\\temp",smmu_64bt,targetid,offset,ramparse_path,ramparse_path)
        print "Processing SMMU pagetable parsing... it might take 4-5 minutes. Please wait..."
        # print command
        os.system(command)
    else:
        print "**************************************************************"
        print "vmlinux path is not accessible/available. Please Check the Path "
        print "**************************************************************"
    smmu_enable=True
    load_dump_full=dump_path
    if dump_path!=None:
        if not os.path.isfile(dump_path+"\\load.cmm"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(dump_path+"\\DDRCS0.BIN") or not os.path.isfile(dump_path+"\\DDRCS0.bin"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
        if not os.path.isfile(dump_path+"\\DDRCS1.BIN") or not os.path.isfile(dump_path+"\\DDRCS1.bin"):
            print "**************************************************************"
            print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
            print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
            print "**************************************************************"
            sys.exit(0)
    else:        
        print "**************************************************************"
        print "If you are using -smmu32/smmu64 crahman option , the dump argument given in crashman must point to complete ramdump.\n"
        print "Mandatory contents to be present in dump path - load.cmm, DDRCS0.BIN , DDRCS1.BIN"
        print "**************************************************************"
        sys.exit(0)
    flag = 1
    
if flag == 1:
    if not os.path.isfile(OUTDumpPath+"\\temp\\msm_iommu_domain_02.txt"):
        print "**************************************************************"
        print "Not able to generate SMMU pagetable mappings with provided dumps and vmlinux."
        print "Please provide matching vmlinux and dumps"
        print "**************************************************************"
        sys.exit(0)
    
input = OUTDumpPath+"\\temp\\version_check.txt"
f_out = open(input,"w")
Pythonversion = sys.version
Pythonversion1 = Pythonversion.split() 
f_out.write('Python Version Installed : v' + Pythonversion1[0] + '\n' )
f_out.write('Perl   Version Installed : ' + perl_version + '\n' )
f_out.write('Crashman Version         : '+ Crashman_Version + '\n')
f_out.write('VersionFileEnd\n')
f_out.close()
win = 0
if ".dmp" == INDumpPath[-4:]:
    win = 1
elif ".tmp" == INDumpPath[-4:]:
    win = 1

if win == 1:
    print "\n\n************************************"
    print "Windows Dumps Extraction Started...\n"
    os.makedirs(OUTDumpPath+"\\Dexter") 
    command = '''%s\dexter.exe /v /i %s /o %s '''%(CurrDirectory,INDumpPath,OUTDumpPath+"\\Dexter")
    print command
    os.system(command)
    
    INDumpPath=OUTDumpPath+"\\Dexter\\ADSP0.bin"
    if slpi_flag == True:
        INDumpPath=OUTDumpPath+"\\Dexter\\SCSS0.bin"
    for r,d,f in os.walk(OUTDumpPath+"\\Dexter\\"):
        for file in f:
            win_dump  = os.path.join(r,file)
            # print win_dump
            if "ADSP0.bin" in win_dump or "ADSP0_" in win_dump or ("ADSP__2" in win_dump and ".bin" in win_dump):
                INDumpPath = win_dump
                break
            if slpi_flag == True:
                if "SCSS0.bin" in win_dump or "SCSS0_" in win_dump:
                    INDumpPath = win_dump
                    break
        
    print "\nWindows Dumps Extraction Done\n"
    print "************************************\n\n\n"
        
# command = '''dir %s /D /-C >%s'''%(INDumpPath,OUTDumpPath+"\\temp\\binarylength.txt")
# os.system(command)
# command = '''perl %s\Check_Ram_Size.pl %s'''%(CurrDirectory,OUTDumpPath+"\\temp\\binarylength.txt")
# process = subprocess.Popen(command, stdout=subprocess.PIPE)
# process.wait() 

if not os.path.isfile(INDumpPath):
    print "Dump File is not accessible/available. Please Check the File Path."
    sys.exit(0)
bin = OUTDumpPath+"\\temp\\binarylength.txt"
f_bin = open(bin,'w')
size = os.path.getsize(INDumpPath)
f_bin.write(str(size)+'\n')
f_bin.close()

if start_address == False:
    # print datetime.datetime.now()
    command = '''python %s\Start_Address.py  %s %s %s %s'''%(CurrDirectory,INDumpPath,OUTDumpPath,targetid,slpi_flag)
    # print command
    process = subprocess.Popen(command, stdout=subprocess.PIPE)
    process.wait()
    # print datetime.datetime.now()
else:
     if not os.path.isfile(OUTDumpPath+"\\temp\\adspstartaddr.txt"):
        extension = os.path.splitext(str(INDumpPath))[1]
        if extension == "":
            if binascii.hexlify(data) == "7f454c46":
                extension = ".ELF"
        file_dump = open(OUTDumpPath+"\\temp\\dumpformant.txt",'w')
        if (extension == ".elf" or extension == ".ELF"):
            file_dump.write('2')
        else:
            file_dump.write('1')
        file_dump.close()
        file = open(OUTDumpPath+"\\temp\\adspstartaddr.txt",'w')
        file.write(start_address)
        file.close()

if (((targetid == "8996") and (slpi_flag == True)) or ((targetid == "8998")and(slpi_flag == True))):
    if not os.path.isfile(OUTDumpPath+"\\temp\\alignment.txt"):
        if "DDRCS0" in INDumpPath:
            INDumpPath = INDumpPath.replace("DDRCS0","DDRCS1")
            if not os.path.isfile(INDumpPath):
                print "Dump File DDRCS1.BIN is not accessible/available. Please Check the Dump Path."
                sys.exit(0)
            if start_address == False:
                command = '''python %s\Start_Address.py  %s %s %s %s'''%(CurrDirectory,INDumpPath,OUTDumpPath,targetid,slpi_flag)
                process = subprocess.Popen(command, stdout=subprocess.PIPE)
                process.wait() 
        elif "DDRCS1" in INDumpPath:
            INDumpPath = INDumpPath.replace("DDRCS1","DDRCS0")
            if not os.path.isfile(INDumpPath):
                print "Dump File DDRCS0.BIN is not accessible/available. Please Check the Dump Path."
                sys.exit(0)
            if start_address == False:
                command = '''python %s\Start_Address.py  %s %s %s %s'''%(CurrDirectory,INDumpPath,OUTDumpPath,targetid,slpi_flag)
                process = subprocess.Popen(command, stdout=subprocess.PIPE)
                process.wait() 
else:
    if not os.path.isfile(OUTDumpPath+"\\temp\\adspstartaddr.txt"):
        if "DDRCS0" in INDumpPath:
            INDumpPath = INDumpPath.replace("DDRCS0","DDRCS1")
            if not os.path.isfile(INDumpPath):
                print "Dump File DDRCS1.BIN is not accessible/available. Please Check the Dump Path."
                sys.exit(0)
            if start_address == False:
                command = '''python %s\Start_Address.py  %s %s %s %s'''%(CurrDirectory,INDumpPath,OUTDumpPath,targetid,slpi_flag)
                process = subprocess.Popen(command, stdout=subprocess.PIPE)
                process.wait() 
        elif "DDRCS1" in INDumpPath:
            INDumpPath = INDumpPath.replace("DDRCS1","DDRCS0")
            if not os.path.isfile(INDumpPath):
                print "Dump File DDRCS0.BIN is not accessible/available. Please Check the Dump Path."
                sys.exit(0)
            if start_address == False:
                command = '''python %s\Start_Address.py  %s %s %s %s'''%(CurrDirectory,INDumpPath,OUTDumpPath,targetid,slpi_flag)
                process = subprocess.Popen(command, stdout=subprocess.PIPE)
                process.wait() 


if gdb_flag == True:
    bin = OUTDumpPath+"\\temp\\binarylength.txt"
    line1 = 0
    f_bin = open(bin,'r+')
    for line in f_bin:
        line1 = line
        break;
    f_bin.close()
    bin = OUTDumpPath+"\\temp\\adspstartaddr.txt"
    start_address = 0
    f_bin = open(bin,'r+')
    for line in f_bin:
        start_address = line
        # print start_address
        break;
    f_bin.close()
    
    if targetid == "8996" or targetid == "8998":
        print "8996/8998 GDB simulator is not supported\n"
        sys.exit(0)
    DUMP_SIZE_CHECK = 36700160 #Dump file check as 35MB
    if (((".bin" in INDumpPath) or (".BIN" in INDumpPath)) and (int(line1) > DUMP_SIZE_CHECK)):
        if CustomerPath==False:
            CustomerPath = CRM_buildpath+"\\build\\ms"
            
        command = '''python %s\gdb\gdbADSPcrashman.py %s %s %s %s %s %s %s %s'''%(CurrDirectory,targetid,INDumpPath,OUTDumpPath,CRM_buildpath,CustomerPath,SOFILE,start_address.rstrip('\n'),smmu_enable)
        # print command
        os.system(command)
    else:
        print "Please provide proper dumps for GDB simulator. Its supports only .BIN files"
        sys.exit(0)
        
    if targetid == "8952"  or targetid == "8953"  or targetid == "8937"  or targetid == "8976" :
        gdb_ui = '''
********************************************************************
Disclaimer for GDB UI Simulator 
********************************************************************
    
Prerequisites:

    Hexagon Tools Version 5.0.14 or Later
    Hexagon SDK Version 2.0 or Later
    Hexagon Plug-in for Crash Simulator

Update Environment:
    Update the {Hexagon_Tools_Install_Location} to ENV PATH
    Update the {Hexagon_SDK_Install_Location}/tools/eclipse to ENV PATH

Install Crash Sim Plug-in:
    To use crash simulator you need to have eclipse and IDE.zip plug-ins.
    installing the Hexagon plug ins can be done in two ways:
        1. Install using Help -> Install new software in eclipse
        2. Install using manage_plugins.py 
           [ python manage_plugins.py -E <eclipse_location> -Z <full_path_of_IDE.zip>
             manage_plugins.py is in Hexagon SDK at {Hexagon_SDK_Install_Location}/tools/scripts
             ex: python manage_plugins.py -E C:\Qualcomm\Hexagon_SDK\\2.0\\tools\eclipse -Z C:\Dropbox\IDE.zip ]

*************************************************************************************************
    '''
        print gdb_ui
        BUILD_ASIC=targetid
        BUILD_ID="AAAAAAAAQ"
        BUILD_VER="1234"
        F010 = ['8994','8996','8998','8992','8952','8953','8937']        
        if targetid in F010: va_start="0xf0100000"
        else: va_start="0xf0000000"
        print "Do you want to open UI Simulator(yes/no):"
        a = raw_input()
        if a == "yes" or a == "y":
            command = '''eclipse.exe -G 9010 -dump %s 0x80000000 0x1FFFFFFF -dpage %s %s 0x1400000 -asid 0x0 -elf %s --launch_crash_sim --tools_path C:\\Qualcomm\\HEXAGON_Tools\\5.0.14\\ --crash_sim %s\\gdb\crash_sim.exe  -data %s'''%(INDumpPath,va_start,start_address.rstrip('\n'),CustomerPath+"\\M"+BUILD_ASIC+BUILD_ID+BUILD_VER+".elf",CurrDirectory,OUTDumpPath+"\\temp\\eclipse")
            os.system(command)
    sys.exit(0)

print "\nStarting T32 for further processing."
print "************************************"
t32_file = OUTDumpPath+"\\temp\\config_sim_usb.t32"
f_t32 = open(t32_file,'w+')
f_t32.write("OS=\n")
f_t32.write("ID=SIM_${2}            ; unique ID needed for temporary file name uniqueness\n")
f_t32.write("TMP="+OUTDumpPath+"\\temp"+"           ; T32 temp file names are prepended with the unique ID above\n")
if quit_flag == True:
    f_t32.write("SYS="+T32_path+"                ; make system directory the same as the executable directory\n\n")
else:
    if t32_loc != False:
        f_t32.write("SYS="+T32_path+"                ; make system directory the same as the executable directory\n\n")
    else:
        f_t32.write("SYS=C:\T32                ; make system directory the same as the executable directory\n\n")
f_t32.write("SCREEN=\n")
f_t32.write("HEADER=${2}\n\n")
f_t32.write("; Ethernet on Host information \n")
f_t32.write("PBI=SIM\n")
f_t32.write("\n")
f_t32.write("; Printer settings\n")
f_t32.write("PRINTER=WINDOWS\n\n")
f_t32.write("; Screen fonts\n")
f_t32.write("SCREEN=\n")
f_t32.write("FONT=SMALL\n\n")
f_t32.close()

if SOFILE:
    if SOFILE != "8952" and SOFILE != "8953" and SOFILE != "8937" and SOFILE != "8910"  and SOFILE != "8996" and SOFILE != "8998" and SOFILE != "8976":
        print "Crashman Presently Support dynamic loading only for 8952/8953/8937/8976/8996/8998/8910."

if quit_flag == True:
    Path = T32_path+"\\bin\\windows64"
    if os.path.exists(Path):
        command = '''%s\\bin\\windows64\\t32mqdsp6.exe -c %s, %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s'''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\DSP_load_memorydump_crashman.cmm",targetid,UniqueNumber,INDumpPath,OUTDumpPath,CRM_buildpath,CurrDirectory,CustomerPath,crashman_lite,slpi_flag,asha_flag,SOFILE,load_dump_full,quit_flag,smmu_enable,frpcshell,frpcshell_name,wp_smmu_enable,INDumpPath.split('\\')[-1])
        process_t32 = subprocess.Popen(command, stdout=subprocess.PIPE)
        # os.system(command)
    else:    
        command = '''%s\\t32mqdsp6.exe -c %s, %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s'''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\DSP_load_memorydump_crashman.cmm",targetid,UniqueNumber,INDumpPath,OUTDumpPath,CRM_buildpath,CurrDirectory,CustomerPath,crashman_lite,slpi_flag,asha_flag,SOFILE,load_dump_full,quit_flag,smmu_enable,frpcshell,frpcshell_name,wp_smmu_enable,INDumpPath.split('\\')[-1])
        process_t32 = subprocess.Popen(command, stdout=subprocess.PIPE)
        # os.system(command)
else:        
    if t32_loc != False:
        Path = T32_path+"\\bin\\windows64"
    else:
        T32_path = "C:\\T32"
        Path = "C:\\T32\\bin\\windows64"
    if os.path.exists(Path):
        command = '''start %s\\bin\\windows64\\t32mqdsp6.exe -c %s, %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s'''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\DSP_load_memorydump_crashman.cmm",targetid,UniqueNumber,INDumpPath,OUTDumpPath,CRM_buildpath,CurrDirectory,CustomerPath,crashman_lite,slpi_flag,asha_flag,SOFILE,load_dump_full,quit_flag,smmu_enable,frpcshell,frpcshell_name,wp_smmu_enable,INDumpPath.split('\\')[-1])
        os.system(command)
    else:    
        command = '''start %s\\t32mqdsp6.exe -c %s, %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s'''%(T32_path,OUTDumpPath+"\\temp\\config_sim_usb.t32",CurrDirectory+"\\DSP_load_memorydump_crashman.cmm",targetid,UniqueNumber,INDumpPath,OUTDumpPath,CRM_buildpath,CurrDirectory,CustomerPath,crashman_lite,slpi_flag,asha_flag,SOFILE,load_dump_full,quit_flag,smmu_enable,frpcshell,frpcshell_name,wp_smmu_enable,INDumpPath.split('\\')[-1])
        os.system(command)

if asha_flag == True:
    file_check=OUTDumpPath+"\\temp\\dumpsloaddone.txt"
    while True:
        time.sleep(10) #wait for 1min for checking
        if os.path.exists(file_check):
            print "Dumps and ELF Loading Done!!!!!"
            break

    minutes=10
    dsp_check=OUTDumpPath+"\\temp\\DSPAnalysis.end"
    dsp_analy=OUTDumpPath+"\\DSPAnalysis.txt"

    cond = 0
    for i in range(0,minutes):
        time.sleep(60) #wait for 1min for checking
        if os.path.exists(dsp_check):
            cond = 1
            break
                
    if cond == 0:    
        f_dsp_check = open(dsp_check,'w+')
        f_dsp_analy = open(dsp_analy,'r+')
        for line in f_dsp_analy:
            f_dsp_check.write(line)
        f_dsp_analy.close()
        f_dsp_check.close()
    print "Searching for JIRA tickets"
    log=OUTDumpPath+"\\temp\\JiraLog.txt"
    command = '''python %s\DD_module.py %s > %s'''%(DDModular_Script,OUTDumpPath,log)
    os.system(command)
    dsp_check=OUTDumpPath+"\\temp\\DSPOutput.complete"
    dsp_analy=OUTDumpPath+"\\temp\\DSPAnalysis.end"
    f_dsp_check = open(dsp_check,'w+')
    f_dsp_analy = open(dsp_analy,'r+')
    for line in f_dsp_analy:
        f_dsp_check.write(line)
    f_dsp_analy.close()
    f_dsp_check.close()
    
    print "Searching for JIRA tickets Done"

    
if quit_flag == True:
    minutes=20
    dsp_check=OUTDumpPath+"\\temp\\DSPAnalysis.end"
    cond_1 = 0
    for i in range(0,minutes):
        time.sleep(60) #wait for 1min for checking
        if os.path.exists(dsp_check):
            cond_1 = 1
            break
    if cond_1 == 0:
        process_t32.terminate()
        raise ValueError('Crashman stuck or not able to load dumps properly.')
        
        
file_check=OUTDumpPath+"\\temp\\DSPAnalysis.end"
while True:
    time.sleep(10) #wait for 1min for checking
    if os.path.exists(file_check):
        if os.path.exists(OUTDumpPath+"\\AvailableLogs.txt"):
            os.rename(OUTDumpPath+"\\AvailableLogs.txt", OUTDumpPath+"\\temp\\AvailableLogs.txt")
        if os.path.exists(OUTDumpPath+"\\f3tokens_temp.txt"):
            os.rename(OUTDumpPath+"\\f3tokens_temp.txt", OUTDumpPath+"\\temp\\f3tokens_temp.txt")
        if os.path.exists(OUTDumpPath+"\\BAM.ulog"):
            os.rename(OUTDumpPath+"\\BAM.ulog", OUTDumpPath+"\\temp\\BAM.ulog")
        if os.path.exists(OUTDumpPath+"\\NPA Log.ulog"):
            os.rename(OUTDumpPath+"\\NPA Log.ulog", OUTDumpPath+"\\temp\\NPA Log.ulog")
        if os.path.exists(OUTDumpPath+"\\SLIMBUS.ulog"):
            os.rename(OUTDumpPath+"\\SLIMBUS.ulog", OUTDumpPath+"\\ulog\\SLIMBUS.ulog")
        if os.path.exists(OUTDumpPath+"\\search_op.json"):
            os.rename(OUTDumpPath+"\\search_op.json", OUTDumpPath+"\\temp\\search_op.json")
        if os.path.exists(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_GuestOS.txt"):
            shutil.copy2(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_GuestOS.txt", OUTDumpPath+"\\Def_Heap\\Heap_Analysis_GuestOS.txt")
        if os.path.exists(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_Audio.txt"):
            shutil.copy2(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_Audio.txt", OUTDumpPath+"\\Def_Heap_for_AUDIO_img\\Heap_Analysis_Audio.txt")
        if os.path.exists(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_Sensors.txt"):
            shutil.copy2(OUTDumpPath+"\\Qurt_logs\\Heap_Analysis_Sensors.txt", OUTDumpPath+"\\Def_Heap_forSensors_img\\Heap_Analysis_Sensors.txt")
        if os.path.isfile(CRM_buildpath+"\\performance\\tools\\SysmonParser.exe"):
            if os.path.isfile(OUTDumpPath+"\\temp\\sysmon_dump.bin"):
                print "Processing sysmonparser.exe."
            else:
                print "Please wait..."
                time.sleep(10)
            if os.path.isfile(OUTDumpPath+"\\temp\\sysmon_dump.bin"):
                try:
                    print "Processing sysmonparser.exe. Please wait..."
                    command = '''%s\\performance\\tools\\SysmonParser.exe %s\\temp\\sysmon_dump.bin %s\\sysmon_dump default  --crashman=%s\\temp'''%(CRM_buildpath,OUTDumpPath,OUTDumpPath,OUTDumpPath)
                    os.system(command)
                except:
                    print "sysmon parser is not working"
        break
