#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "rpcmem.h"
#include "adsp.h"

#include "adspmsgd.h"

#include <android/log.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <pthread.h>

#define CUSTOM_KILL_SIG 1

pthread_t query_thread = {0};


// Remember the current L2 cache partition state for future use
int cachePartitionState;


// This variable also to remember the size to be locked for unlock in the future
int cacheSizeToLockWithSuccess = 0;

// TODO, as we have multiple HVXx,
// Do we need to use an array, to save all HVXs, and their related modes?
// The number of HVX lock
int hvxLockNumber = 0;

// QURT_HVX_MODE_64B = 0,    
// QURT_HVX_MODE_128B = 1   
int hvxLockMode = 0;

// The following is for debug
// logcat does not work on 8994 now
int logcat = 0;

int returnVal;

int pmu_index;
unsigned char *pmu_ptr = NULL;
void __exidx_start() {}
void __exidx_end() {}

char argument1[20];
int argument2;


static int printCommandHelp (void)
{
	printf("    -cache_partition> <[0-3] partition state>\n");
	printf("    -cache_lock <cacheSize to lock in KB>\n");
	printf("    -cache_unlock\n");
	printf("    -hvx_lock <mode, 64 or 128>\n");
	printf("    -hvx_unlock\n");
	printf("    -Ctrl+c, exits\n");
	return 0;
}

static int printL2CachePartitionStates (void)
{

	switch (cachePartitionState)
	{
	case 0 :
		printf("----cache partition to FULL_SIZE = 0, Fully shared cache, without partitioning.\n");
		break;
		
	case 1 :
		printf("----cache partition to ALF_SIZE = 1, 1/2 for main, 1/2 for auxiliary.\n");
		break;
		
	case 2 :
		printf("----cache partition to SEVEN_EIGHTHS_SIZE = 3, 7/8 for main, 1/8 for auxiliary.\n");
		break;
		
	case 3 :
		printf("----cache partition to SEVEN_EIGHTHS_SIZE = 3, 7/8 for main, 1/8 for auxiliary.\n");
		break;
		
		default :
		printf("----cache partition invalid state, 0-3 are valid states, %d\n", cachePartitionState);
		break;
	}
	return 0;
}


static int printL2States (void)
{
	printf("\n----Current cache states:\n");
	printf("----L2 cache locked %dKB\n", cacheSizeToLockWithSuccess);
	printL2CachePartitionStates();
	printf("\n");
	return 0;
}


int main(int argc, char **argv)
{
	// Command line argument procesing part
	if(argc == 1 || argc > 3)
	{
		printf("Improper no. of arguments\n");
		printf("Usage:\n");
		printCommandHelp();
		return 0;
	}

	int action = atoi(argv[1]);
	if (action == 0 && argc == 2)
	{
		printCommandHelp();
		return 0;
	}

	if (strcmp(argv[1], "-cache_partition") == 0 && argc == 3)
	{
		argument2 = atoi(argv[2]);
		action = 0;
	}
	else if (strcmp(argv[1], "-cache_lock") == 0 && argc == 3)
	{
		argument2 = atoi(argv[2]);
		action = 1;
	}
	else if (strcmp(argv[1], "-cache_unlock") == 0 && argc == 2)
	{
		action = 2;
	}
	else if (strcmp(argv[1], "-hvx_lock") == 0 && argc == 3)
	{
		argument2 = atoi(argv[2]);
		action = 3;
	}
	else if (strcmp(argv[1], "-hvx_unlock") == 0 && argc == 2)
	{
		action = 4;
	}
	else
	{
		action = 5;
	}
	// end of command line argument processing
	
	while (1)
	{
		switch (action)
		{
		case 0 :
			{
				returnVal = adsp_cache_partition (argument2);
				
				if (returnVal == 0)
				{
					printf("adsp_cache_partition success\n");
					cachePartitionState = argument2;
				}
				else
				{
					printf("adsp_cache_partition failed; returnval = %d\n", returnVal);
				}
			}
			break;
			
		case 1 :
			
			printf("    adsp cache line lock %dKB\n", argument2);
			if (cacheSizeToLockWithSuccess == 0)
			{
				returnVal = adsp_mem_l2cache_line_lock (argument2);
				if (returnVal == 0)
				{
					printf("adsp_mem_l2cache_line_lock success\n");
					cacheSizeToLockWithSuccess = argument2;
				}
				else
				{
					printf("adsp_mem_l2cache_line_lock failed; returnval = %d\n", returnVal);
					cacheSizeToLockWithSuccess = argument2;
				}
			}
			else
			{
				printf("adsp_mem_l2cache_line_lock has been locked %d, unlock first\n", cacheSizeToLockWithSuccess);
			}
			break;
			
		case 2 :
			printf("    adsp cache line unlock %dK\n", cacheSizeToLockWithSuccess);
			returnVal = adsp_mem_l2cache_line_unlock (cacheSizeToLockWithSuccess);
			if (returnVal == 0)
			{
				printf("adsp_mem_l2cache_line_unlock success\n");
				cacheSizeToLockWithSuccess = 0;
			}
			else
			{
				printf("adsp_mem_l2cache_line_unlock failed; returnval = %d\n", returnVal);
			}
			break;
			
		case 3 :
			// No document yet, check source code at:
			// source/qcom/qct/core/kernel/qurt/main/latest/api/qurt/qurt_hvx.h
			printf("    adsp HVX lock in mode 64\n");
			
			// returnVal = qurt_hvx_try_lock(argument2);
			returnVal = 0;
			if (returnVal == 0)
			{
				printf("adsp dumy HVX lock success\n");
				// save state here
			}
			else
			{
				printf("qurt_hvx_try_lock failed; returnval = %d\n", returnVal);
			}
			break;
			
		case 4 :
			printf("    adsp HVX unlock\n");
			
			// returnVal = qurt_hvx_unlock();
			returnVal = 0;
			if (returnVal == 0)
			{
				printf("adsp dummy HVX unlock success\n");
				// save state
			}
			else
			{
				printf("qurt_hvx_unlock failed; returnval = %d\n", returnVal);
			}
			break;
			
			default :
			printf("Invalid argument\n");
			printCommandHelp();
			break;
		}
		
		// scanf part
		printL2States();
		printf("\n\nInput another command\n");
		scanf ("%s", argument1);

		// process and dispatch the input
		if (strcmp(argument1, "-cache_partition") == 0)
		{
			scanf ("%d", &argument2);
			action = 0;
		}
		else if (strcmp(argument1, "-cache_lock") == 0)
		{
			scanf ("%d", &argument2);
			action = 1;
		}
		else if (strcmp(argument1, "-cache_unlock") == 0)
		{
			action = 2;
		}
		else if (strcmp(argument1, "-hvx_lock") == 0)
		{
			scanf ("%d", &argument2);
			action = 3;
		}
		else if (strcmp(argument1, "-hvx_unlock") == 0)
		{
			action = 4;
		}
		else
		{
			action = 5;
		}
		fflush(stdin);
	}

	fflush(stdout);

	return 0;
}