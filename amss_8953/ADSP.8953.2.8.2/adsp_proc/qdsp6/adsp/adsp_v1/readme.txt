To sign the device
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>testsig.cmd
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>adb push \\harv-viyer\Dropbox\geerial\android_ReleaseG_aarch64\getserial /data
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>adb shell chmod 755 /data/getserl
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>adb shell /data/getserial
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>cd elfsigner
C:\Qualcomm\Hexagon_SDK\1.2.2\tools\elfsigner>python elfsigner.py -t 0x1f3637  --no_disclaimer
then reboot the device


To view F3 messages
QXDM configuration
Options->Message View Config... (ctrl + F5)
Known message by subsystem, check QDSP6
Known message will be checked by level, check low, medium, high, error, fatal.

Press F3 to pop up the message view window


To compile the code, after install latest Hexagon SDK from go/hexagonask, go to C:\Qualcomm\Hexagon_SDK\1.2.2\examples\common\adsp

..\..\..\setup_sdk_env.cmd

To clean the code
make tree V=android_Debug clean
make tree V=hexagon_Debug_dynamic clean

and compile the code
make tree V=android_Debug
make tree V=hexagon_Debug_dynamic


To install the code to target, 
Use adb as root and remount system read/write

adb root
adb wait-for-device
adb remount

The HLOS side executable and supporting stub library must be pushed onto the device as follows:

adb push android_Debug/ship/adsp /data/
adb shell chmod 777 /data/adsp

No need to run the following command as we already merge into one
adb push android_Debug/ship/libadsp.so /system/lib/

The Hexagon Shared Object must be pushed on to the device's file system as follows:
adb shell mkdir /system/lib/rfsa
adb shell mkdir /system/lib/rfsa/adsp
adb push hexagon_Debug_dynamic/ship/libadsp_skel.so /system/lib/rfsa/adsp/


To run the adsp examples
adb shell
cd /data
#./adsp -cache_partition 0
#./adsp -cache_lock 128
#./adsp -cache_unlock
#./adsp -hvx_lock 64
#./adsp -hvx_unlock

To monitor the debug logs
cd C:\Qualcomm\Hexagon_SDK\1.2.2\tools\mini-dm\WinNT_Debug

At ADSP side, we need to export all calling functions at \adsp_proc\platform\exports\dl_base_symbols.lst.
No need to export QDI internal function.

We have two options to call functions where the caller needs to have QurtOS privilege to use this API.
Option 1:
If we delete /vendor/lib/rfsa/adsp/fastrpc_shell_o, then fastRPC itself can run on guestOS PD and thus can call some restricted QuRT functions without writing QDI;

Option 2:
Otherwise, we need to write QDI, which is located at guestOS PD and call function
int qurt_mem_configure_cache_partition( 
    qurt_cache_type_t cache_type, 
    qurt_cache_partition_size_t partition_size
);
while fastRPC is located at dynamic userPD and call the QDI functions.



