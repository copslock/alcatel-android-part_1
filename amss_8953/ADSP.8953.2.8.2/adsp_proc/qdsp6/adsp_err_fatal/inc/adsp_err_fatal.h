#ifndef ADSP_ERR_FATAL_H
#define ADSP_ERR_FATAL_H
#ifdef __cplusplus
extern "C" {
#endif

void AdspfatalerrApi(char* err_str,int strlength);

void Adspfatalerr_Init_Client(void);

void Adspfatalerr_Deinit_Client(void);


#ifdef __cplusplus
}
#endif

#endif



