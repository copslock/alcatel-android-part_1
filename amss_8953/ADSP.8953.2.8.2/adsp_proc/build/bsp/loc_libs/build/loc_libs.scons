#===============================================================================
#
# Basic skeleton image scrip
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/dspbuild.adsp/2.7.3/bsp/loc_libs/build/loc_libs.scons#1 $
#  $DateTime: 2016/04/07 00:14:17 $
#  $Change: 10221894 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
import os
Import('env')

#------------------------------------------------------------------------------
# Init image vars 
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Init aliases array.
# first alias (0th elemten in array) is always unique name, there should 
# never be two images with the same unique name
aliases = ['loc_libs', 'adsp_proc', 'adsp_images', 'adsp_mpd_images', 'adsp_core_images', 'adsp_mpd_core_images',
           'msm8992_MPD','msm8994_MPD', 'msm8996_MPD', 'msm8952_MPD', 'msm8976_MPD', 'msm8953_MPD', 'msm8937_MPD'
          ]

if 'USES_DISABLE_LOC' in env or 'BUILD_BAREBONE' in env or not env.PathExists("${BUILD_ROOT}/loc"):
   print "USES_DISABLE_LOC/BUILD_BAREBONE/no loc folder, bypass LOC loc_libs.scons compilation"
   Return()

#------------------------------------------------------------------------------
# Init environment variables

build_tools = ['buildspec_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/swe_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/sleep_lpr_builder.py']

# Declaring the SCons Image build tags. 
build_tags = ['CORE_QDSP6_SW', 'LOC_ADSP']
#------------------------------------------------------------------------------
# Init environment variables
env.InitImageVars(
   alias_list=aliases,  # aliases list
   proc='qdsp6',      # proc (depending on tool chain arm, hexago, etc)
   config='adsp',
   plat='qurt',           # platform (l4, blast, foo, bar, etc)
#build_tags = ['BASIC_IMAGE_EXAMPLE'],
   buildpath = str(aliases[0]).lower()+'/'+env['BUILD_ASIC'],
   build_tags = build_tags,
   tools = build_tools
)

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
if not env.CheckAlias():
   Return()

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.InitBuildConfig()
env.Replace(USES_DEVCFG = 'yes')
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_IMG')

env.PrintInfo("location libs")
env.AddUsesFlags('USES_RCINIT_PLAYBOOK')
#---------------------------------------------------------------------------
# Load in the tools-set init scripts
#---------------------------------------------------------------------------

#Add ADSP Image specific linker inputs here
env.LoadToolScript('hexagon_adsp', toolpath = ['${BUILD_ROOT}/build'])


if 'USES_MALLOC_WRAPPER_ENABLED' in env:
    env.Append(CFLAGS='-DMALLOC_WRAPPER ')

#---------------------------------------------------------------------------
# Libs/Objs
#---------------------------------------------------------------------------
image_libs = []
image_objs = []

#---------------------------------------------------------------------------
# Libraries Section
#---------------------------------------------------------------------------

# load LOC libs/objs build rules.
if env.PathExists("${BUILD_ROOT}/loc"):
   au_items = env.LoadAreaSoftwareUnits('loc')
   image_libs.extend(au_items['LIBS'])
   image_objs.extend(au_items['OBJS'])

image_units = [image_objs, image_libs]

#--- RCINIT Playbook Extension, Library Specific Details -------------------

PLAYLISTS = [ ] # NONE USED

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
#hexagon_adsp.py defined LPASS_BUILDPATH 
if not os.path.exists(env.RealPath('${LPASS_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${LPASS_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_RCINIT' in env and 'USES_RCINIT_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYLIST OUTPUT THIS LIBRARY
   rcinit_out_rcpl = playbook_env.RealPath('${LPASS_BUILDPATH}/rcinit_playlist.rcpl')
   playbook_env.AddRCInitPlaylist(build_tags, rcinit_out_rcpl)
   playbook_env.AddArtifact(build_tags, rcinit_out_rcpl)
   playbook_env.Depends(build_tags, rcinit_out_rcpl)     # Manage explicit detail outside of AU
   image_units.append(rcinit_out_rcpl)                   # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS LIBRARY
   rcinit_out_txt = playbook_env.RealPath('${LPASS_BUILDPATH}/rcinit_playlist.txt')
   playbook_env.AddRCInitPlaybook(build_tags, rcinit_out_txt, None)
   playbook_env.AddArtifact(build_tags, rcinit_out_txt)
   playbook_env.Depends(rcinit_out_txt, rcinit_out_rcpl) # Manage explicit detail outside of AU
   image_units.append(rcinit_out_txt)                    # Manage explicit detail outside of AU

#--- RCINIT Playbook Extension, Library Specific Details -------------------

#------------------------------------------------------------------------------
# Putting the image toghther
#------------------------------------------------------------------------------

if 'IMAGE_BUILD_LOCAL_FILES' in env:
   #-------------------------------------------------------------------------
   # Local Files
   #-------------------------------------------------------------------------
   
   # this is where local files are created, for example link scripts (lcs)
   # for qdsp6 like images, or scatter load files (scl) for amr like images.
   local_itmes= []
   
   image_units += local_itmes
      
if 'IMAGE_BUILD_LINK' in env:
   #-------------------------------------------------------------------------
   # Link image
   #-------------------------------------------------------------------------

   # this is where the rule to "link" is done.
   #image_elf = env.AddProgram("${LPASS_BUILDPATH}/${TARGET_NAME}", image_objs, LIBS=image_libs)
   
   # this is just to create something for testing
   if [image_objs,image_libs] != 0:
    
      image_elf = env.ListFileBuilder("${LPASS_BUILDPATH}/${TARGET_NAME}.txt", [image_objs, image_libs], 
         add_header=False, relative_path="${BUILD_ROOT}/build/ms", posix=True)
   else:
      image_elf=[]
      
if 'IMAGE_BUILD_POST_LINK' in env:
   #-------------------------------------------------------------------------
   # Post process image
   #-------------------------------------------------------------------------
   
   # this is where any aditional rules after linking are done.

   #=========================================================================
   # Define targets needed 
   #
   image_units += [
      image_elf,
   ]

#=========================================================================
# Finish up...
env.BindAliasesToTargets(image_units)
