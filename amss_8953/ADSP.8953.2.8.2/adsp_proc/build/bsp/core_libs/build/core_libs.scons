#===============================================================================
#
# Basic skeleton image scrip
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/dspbuild.adsp/2.7.3/bsp/core_libs/build/core_libs.scons#1 $
#  $DateTime: 2016/04/07 00:14:17 $
#  $Change: 10221894 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 2015/01/12 sm      Add 'CORE_ADSP_ROOT' to the build tags.
#===============================================================================

import os
Import('env')

#------------------------------------------------------------------------------
# Init image vars 
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Init aliases array.
# first alias (0th elemten in array) is always unique name, there should
# never be two images with the same unique name
aliases = ['core_libs', 'adsp_proc', 'adsp_images', 'adsp_mpd_images', 'adsp_core_images', 'adsp_mpd_core_images',
           'msm8992_MPD', 'msm8994_MPD', 'msm8996_MPD', 'msm8952_MPD', 'msm8976_MPD', 'msm8953_MPD', 'msm8937_MPD'
          ]

#------------------------------------------------------------------------------
# Init environment variables

build_tools = ['buildspec_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/swe_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/sleep_lpr_builder.py']

			
if env.get('CHIPSET') == "msm8994" or env.get('CHIPSET') == "msm8992" or env.get('CHIPSET') == "msm8952" or env.get('CHIPSET') == "msm8976" or env.get('CHIPSET') == "msm8953" or env.get('CHIPSET') == "msm8937":
   build_tools += [
            '${BUILD_ROOT}/core/bsp/build/scripts/island_builder.py']

# Define the SCons image build tags.
build_tags = ['CORE_QDSP6_SW', 'BUILD_TEST_MODEM', 'QDSS_EN_IMG','AVS_ADSP','VIDEO_ADSP', 'QMIMSGS_ADSP', 'CORE_ADSP',
              'IMAGE_TREE_VERSION_AUTO_GENERATE',
              'IMAGE_TREE_UUID_AUTO_GENERATE', 'CORE_ADSP_ROOT', 'ROOT_PD'
             ]
            
env.InitImageVars(
   alias_list=aliases,  # aliases list
   proc='qdsp6',      # proc (depending on tool chain arm, hexago, etc)
   config='adsp',
   plat='qurt',           # platform (l4, blast, foo, bar, etc)
#build_tags = ['BASIC_IMAGE_EXAMPLE'],
   buildpath = str(aliases[0]).lower()+'/'+env['BUILD_ASIC'],
   build_tags = build_tags,
   deprecated_build_tags = ['CBSP_QDSP6_SW_IMAGE'],
   tools = build_tools
)

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
if not env.CheckAlias():
   Return()

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.InitBuildConfig()
env.Replace(USES_DEVCFG = 'yes')
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_IMG')
if ('USES_FEATURE_DYNAMIC_LOADING_GLOBAL' in env) :
   env.Replace(USES_FEATURE_DYNAMIC_LOADING = 'yes')
   #import pdb; pdb.set_trace()
   env.LoadToolScript('sharedlib_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
   env.LoadToolScript('dlinker_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])

#Add ADSP Image specific linker inputs here
env.LoadToolScript('hexagon_adsp', toolpath = ['${BUILD_ROOT}/build'])

if 'USES_MULTI_PD' in env:
   env.PrintInfo("This adsp core image is being built for Multi PD version")
   env.AddUsesFlags('USES_MPD')
   env.AddUsesFlags('USES_QURTOS_IMG') #this is the flag to be used for mpd builds to compile for guest os
   env.Append(CPPDEFINES="SENSOR_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_SENSOR.pbn\\\"")
   env.Append(CPPDEFINES="AUDIO_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO.pbn\\\"")
   env.Append(CPPDEFINES=["MULTI_PD_BUILD"])
else:
   env.PrintInfo("This adsp core image is being built for Single PD version")
env.AddUsesFlags('USES_RCINIT_PLAYBOOK')
#---------------------------------------------------------------------------
# Include PWRDB to the build via USES_PWRDB flag
#---------------------------------------------------------------------------
env.AddUsesFlags('USES_PWRDB')

#if image wants to own "main": # image calls core_main to init cbsp
env.Append(CPPDEFINES=["COREMAIN_EXCLUDE_MAIN"])

#---------------------------------------------------------------------------
# Load in the tools-set init scripts
#---------------------------------------------------------------------------
if 'USES_MALLOC_WRAPPER_ENABLED' in env:
    env.Append(CFLAGS='-DMALLOC_WRAPPER ')

#Append sim flag if building for sim
if 'USES_AVS_TEST' in env:
    env.Append(CFLAGS='-DSIM')


# Enable Remote Debugger only for 8974
if env.get('CHIPSET') == "msm8974":
    env.AddUsesFlags('USES_REMOTE_DEBUGGER')

#top level control enable/disable Power Collapse feature
#define the flag to disable PC
if 'USES_FEATURE_DISABLE_SLEEP_MODES' in env:
    env.Append(CPPDEFINES=['FEATURE_DISABLE_SLEEP_MODES'])

#---------------------------------------------------------------------------
# Libs/Objs
#---------------------------------------------------------------------------
image_libs = []
image_objs = []
img_shlibs = []
#---------------------------------------------------------------------------
# Libraries Section
#---------------------------------------------------------------------------

# load au_name libs/objs build rules.
au_name_items = env.LoadAreaSoftwareUnits('core')
image_libs.extend(au_name_items['LIBS'])
image_objs.extend(au_name_items['OBJS'])
if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING'):
   img_shlibs.extend(au_name_items['SHARED_LIBS'])


image_units = [image_objs, image_libs, img_shlibs]

if 'USES_ISLAND' in env:
   island_list = env.GenerateIslandList()
   image_units += [
      island_list,
   ]

#--- RCINIT Playbook Extension, Library Specific Details -------------------

PLAYLISTS = [ ] # NONE USED

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
#hexagon_adsp.py defined LPASS_BUILDPATH 
if not os.path.exists(env.RealPath('${LPASS_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${LPASS_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_RCINIT' in env and 'USES_RCINIT_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYLIST OUTPUT THIS LIBRARY
   rcinit_out_rcpl = playbook_env.RealPath('${LPASS_BUILDPATH}/rcinit_playlist.rcpl')
   playbook_env.AddRCInitPlaylist(build_tags, rcinit_out_rcpl)
   playbook_env.AddArtifact(build_tags, rcinit_out_rcpl)
   playbook_env.Depends(build_tags, rcinit_out_rcpl)     # Manage explicit detail outside of AU
   image_units.append(rcinit_out_rcpl)                   # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS LIBRARY
   rcinit_out_txt = playbook_env.RealPath('${LPASS_BUILDPATH}/rcinit_playlist.txt')
   playbook_env.AddRCInitPlaybook(build_tags, rcinit_out_txt, None)
   playbook_env.AddArtifact(build_tags, rcinit_out_txt)
   playbook_env.Depends(rcinit_out_txt, rcinit_out_rcpl) # Manage explicit detail outside of AU
   image_units.append(rcinit_out_txt)                    # Manage explicit detail outside of AU

#--- RCINIT Playbook Extension, Library Specific Details -------------------

#------------------------------------------------------------------------------
# Putting the image together
#------------------------------------------------------------------------------

if 'IMAGE_BUILD_LOCAL_FILES' in env:
   #-------------------------------------------------------------------------
   # Local Files
   #-------------------------------------------------------------------------

   # this is where local files are created, for example link scripts (lcs)
   # for qdsp6 like images, or scatter load files (scl) for amr like images.
   local_itmes= []

   image_units += local_itmes

   #------------------------------------------------------------------------------
   # Dynamic Loading Info
   #------------------------------------------------------------------------------
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING'):
      dll_itmes= []
      
      dlexpose_sym_lst= env.RealPath('${LPASS_BUILDPATH}/DLEXPOSE_CORE_SYMS_AAAAAAAAQ.txt')
      dlexpose_sym_lst_node = env.DLExposeGenerateList([dlexpose_sym_lst], None)
      dll_itmes += dlexpose_sym_lst_node
      
      if(len(img_shlibs) !=0) :
         sh_libs_sym_lst= env.RealPath('${LPASS_BUILDPATH}/SHLIBS_CORE_SYMS_AAAAAAAAQ.txt')
         sh_libs_sym_lst_node = env.SharedLibGenerateList([sh_libs_sym_lst], img_shlibs)
         dll_itmes += sh_libs_sym_lst_node
      
         sym_lst= env.RealPath('${LPASS_BUILDPATH}/CORE_SYMS_AAAAAAAAQ.txt')
         sym_lst_node = env.ConsolidateSymsList([sym_lst], [dlexpose_sym_lst_node[0], sh_libs_sym_lst_node[0]])
         dll_itmes += sym_lst_node
      
      else:
         sym_lst= env.RealPath('${LPASS_BUILDPATH}/CORE_SYMS_AAAAAAAAQ.txt')
         sym_lst_node = env.ConsolidateSymsList([sym_lst], [dlexpose_sym_lst_node[0]])
         dll_itmes += sym_lst_node
      
      image_units += dll_itmes

   
if 'IMAGE_BUILD_LINK' in env:
   #-------------------------------------------------------------------------
   # Link image
   #-------------------------------------------------------------------------

   # this is where the rule to "link" is done.
   #image_elf = env.AddProgram("${LPASS_BUILDPATH}/${TARGET_NAME}", image_objs, LIBS=image_libs)

   # this is just to create something for testing
   image_elf = env.ListFileBuilder("${LPASS_BUILDPATH}/${TARGET_NAME}.txt", [image_objs, image_libs],
         add_header=False, relative_path="${BUILD_ROOT}/build/ms", posix=True)
   
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING') and len(img_shlibs) !=0 :
      shlib_elf = env.ListFileBuilder("${LPASS_BUILDPATH}/${TARGET_NAME}" + "_SYMS.txt", [img_shlibs],
         add_header=False, relative_path="${BUILD_ROOT}/build/ms", posix=True)

if 'IMAGE_BUILD_POST_LINK' in env:
   #-------------------------------------------------------------------------
   # Post process image
   #-------------------------------------------------------------------------

   # this is where any aditional rules after linking are done.
   if 'USES_CMMBUILDER' in env:
      cmm_image_list_file=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/${LPASS_BUILDPATH}/cmmoutputlist.txt')
      cmmfiles = env.CreateCMMImageFile(cmm_image_list_file,[])
      image_units += [cmmfiles]
   #=========================================================================
   # Define targets needed
   #
   image_units += [
      image_elf,
   ]
  
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING') and len(img_shlibs) !=0 :
      image_units += [
      shlib_elf,
      ]
 
#=========================================================================
# Finish up...
env.BindAliasesToTargets(image_units)
env.CMMBuilder(None, None)
