
/* ======================================================================== */
/**
   @file capi_v2_gain_lib.c

   C source file to implement decimation.
 */

/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "capi_v2_gain_lib.h"

/*------------------------------------------------------------------------
  Function name: pcm_gain
  Performs apply gain operation
 * -----------------------------------------------------------------------*/
void pcm_gain(int16_t* primary_mic_ptr,
              int16_t* out_ptr,
              uint32_t gain_val,
              uint32_t num_samples)
{
  uint32_t i;
  uint32_t out;
  for (i = 0; i < num_samples; i++) {
    out = gain_val * primary_mic_ptr[i];
    out_ptr[i] = (out >> 13);
  }
}