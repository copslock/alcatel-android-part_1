#!/usr/bin/env python

class qurt_config:
    def genheader_subcommand(self, arglist):
        from lib.genheader import genheader_cmd
        return genheader_cmd(arglist)
    def update_subcommand(self, arglist):
        from lib.merge import merge_cmd
        return merge_cmd(arglist)
    def usage(self):
        cmds = sorted([z.rsplit('_',1)[0] for z in dir(self) if z.endswith('_subcommand')])
        str = 'First argument must be one of:\n  ' + ', '.join(cmds)
        raise Exception(str)
    def run_command(self, argv):
        from traceback import format_exc as tbstr
        progname = argv[0]
        try:
            print ' '.join(argv)
            raw_args = argv[1:]
            args = [s for s in raw_args if not s == '--traceback']
            if args == raw_args:
                tbstr = None
            try:
                subfunc = getattr(self, '%s_subcommand' % args[0])
            except StandardError:
                self.usage()
            return subfunc(args[1:])
        except (SystemExit, KeyboardInterrupt):
            raise
        except Exception, err:
            if tbstr:
                print tbstr()
            print '%s: Error:\n*** %s' % (progname, err)
        except:
            raise
        return 1
    def main(self):
        import sys
        sys.exit(self.run_command(sys.argv))

qurt_config().main()    # Never returns

# Signatures of the files that this depends on
# 2f4e124464edb0bb937e77bfb7fae295 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/cust_config_template.c
# 73a289a6a2027ed72dc9646d357f9333 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/default_build_config.def
# 1633b5b0296de87a86466ec793dd9a6f /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/static_build_config.def
# fb029620d16a7ed5f97289883f996027 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/qurt_tlb_unlock.xml
# 206b9e12f5643aab9b5c41da144c95f3 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/__init__.py
# 992309777e70b115ee88d151ceea0f4d /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/build_qurt_config.py
# 9dbe95fb17059e23e02557c064bf4bee /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/build_xml.py
# 4fbc2f197d53ae0e77b5bbfdc445d71d /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/elf_info_patch.py
# 92948080d10d75e5f94f4777d21bb646 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/ezxml.py
# 476321ba022da3266d0982e4e716cad1 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/genheader.py
# c083132b08dbc9fe4f0e2ad37e4cab81 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/interrupt_xml.py
# f7d3003275ac9462e5e88ad5ada16486 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/kernel_xml.py
# 95acc27b7e3bddf9e00b564bd1346090 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/machine_xml.py
# 0dcdb7dcc6e15226b8ea7e4febe611ae /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/memsection_xml.py
# 8d2f2a0df1c87bbd05ff68f33b22c1a1 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/merge.py
# 4ffa9ed630bd335beb3517b8494d7c1b /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/parse_build_params.py
# 77c942c618e3edec510cc51245a617d3 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/parse_spec.py
# 31733f66f57d2783f3b7e99338fdb9ae /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/physpool_xml.py
# 94ea469cffbb6a63290a6412f2ca6361 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/program_xml.py
# 11d5f91dad132dd4b7872858203efb5b /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/lib/qurt.py
# 97ab6879e1de18acc150cf8b7dfd8065 /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/build_params.txt
# 0a290ab60195d2858f809ff5a81a4f0c /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/Input/cust_config.c
# 37e9d3f41976437003afe922dcc3096c /local/mnt/workspace/CRMBuilds/ADSP.8953.2.8.2-00030-00000-1_20160901_003410/b/adsp_proc/core/kernel/qurt/build/sensor_img/8937/install/ADSPv56MP/scripts/qurt-image-build.py
