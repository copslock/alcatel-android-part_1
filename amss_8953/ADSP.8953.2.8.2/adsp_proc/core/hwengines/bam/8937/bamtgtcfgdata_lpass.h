/**
  @file bamtgtcfgdata_lpass.h
  @brief
  This file contains configuration data for the BAM driver for the 
  8937 lpass sub-system.

*/
/*
===============================================================================

                             Edit History


when       who     what, where, why
--------   ---     ------------------------------------------------------------
07/24/15   SA      Created

===============================================================================
                   Copyright (c) 2015 Qualcomm Technologies Incorporated.
                          All Rights Reserved.
                        Qualcomm Confidential and Proprietary.
===============================================================================
*/

#include "bamtgtcfg.h"

/** Configs supported for
    CE1
    BLSP1
    BLSP2
    SLIMBUS
    IPA
	 QDSS
    USB
   */

#define BAM_CNFG_BITS_VAL 0xFFFFF004

bam_sec_config_type bam_tgt_slimbus_secconfig_8937 =
{
    /*.ee*/         {
                       /*[0]*/          {
                       /*.pipe_mask*/       0x001fff87,
                       /*.vmid*/            0x0,
                       /*.pipe_mask_ac*/    0x0,
                       /*.vmid_ac*/         0x0
                                        },
                       /*[1]*/          {
                       /*.pipe_mask*/       0x00600018,
                       /*.vmid*/            0x0,
                       /*.pipe_mask_ac*/    0x0,
                       /*.vmid_ac*/         0x0
                                        },
                       /*[2]*/          {
                       /*.pipe_mask*/       0x00000060,
                       /*.vmid*/            0x0,
                       /*.pipe_mask_ac*/    0x0,
                       /*.vmid_ac*/         0x0
                                         }
                    },
    /*.sg*/         {{0x0}},
    /*.ctrl_vmid*/  0x0,
    /*.bam_irq_ee*/ 0x0
};

const bam_target_config_type  bam_tgt_config[] = {
    {                // APSS_CE
      /* .bam_pa     */  0x00704000,
      /* .options    */  0x0,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  0,
      /* .sec_config */  NULL
    },
    {                // BLSP1_BAM
      /* .bam_pa     */  0x07884000,
      /* .options    */  BAM_TGT_CFG_SHARABLE,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  1,
      /* .sec_config */  NULL
    },
    {                // BLSP2_BAM
      /* .bam_pa     */  0x07AC4000,
      /* .options    */  BAM_TGT_CFG_SHARABLE,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  1,
      /* .sec_config */  NULL
    },
    {                // SLIMBUS_BAM
      /* .bam_pa     */  0x0C104000,
      /* .options    */  0x0,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  0,
      /* .sec_config */  &bam_tgt_slimbus_secconfig_8937
    },
    {               // IPA_BAM
     /* .bam_pa     */   0x07904000,
     /* .options    */   0x0,
     /* .cfg_bits   */   BAM_CNFG_BITS_VAL,
     /* .ee         */   0,
     /* .sec_config */   NULL
    },
    {                // QDSS_BAM
      /* .bam_pa     */  0x06044000,
      /* .options    */  0x0,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  0,
      /* .sec_config */  NULL
    },
    {                // USB_BAM
      /* .bam_pa     */  0x07104000,
      /* .options    */  0x0,
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  0,
      /* .sec_config */  NULL
    },
    {                // dummy config
      /* .bam_pa     */  BAM_TGT_CFG_LAST,
      /* .options    */  0,
      /* .cfg_bits   */  0,
      /* .ee         */  0,
      /* .sec_config */  NULL
    }
};


