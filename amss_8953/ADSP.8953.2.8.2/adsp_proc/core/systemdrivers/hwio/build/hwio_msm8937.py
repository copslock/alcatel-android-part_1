#===========================================================================
#
#  @file hwio_msm8937.py
#  @brief HWIO config file for the HWIO generation scripts for MSM8937.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_msm8937.py --flat=..\..\..\api\systemdrivers\hwio\msm8937\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2015 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/core.adsp/2.6.6/systemdrivers/hwio/build/hwio_msm8937.py#1 $
#  $DateTime: 2016/05/04 01:42:14 $
#  $Author: pwbldsvc $
#
#  ===========================================================================

CHIPSET = 'msm8937'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  # General bases
  'RPM',
  'RPM_SS_MSG_RAM_START_ADDRESS',
  'RPM_SS_MSG_RAM_END_ADDRESS',
  'SPDM_WRAPPER_TOP',
  'MPM2_MPM',
  'PMIC_ARB',
  'QDSS_WRAPPER_TOP',
  'QDSS_APB_DEC_QDSS_APB',
  'QDSS_AHB_DEC_QDSS_AHB',
  'PDM_PERPH_WEB',
  'CLK_CTL',
  'PC_NOC',
  'CORE_TOP_CSR',
  'SECURITY_CONTROL',
  'SYSTEM_NOC',
  'TLMM',
  'BLSP1_BLSP',
  'BLSP2_BLSP',

  # LPASS bases
  'LPASS',
  'PRNG_PRNG_TOP'  
]

base_resize = {
  'PC_NOC':                         0x15000,
  'SYSTEM_NOC':                     0x17000,
  'MPM2_MPM':                        0xc000,
  'RPM':                             0x9000,
  'RPM_SS_MSG_RAM_START_ADDRESS':    0x6000,
  'RPM_SS_MSG_RAM_END_ADDRESS':         0x0,
  'CLK_CTL':                        0x82000,
  'PMIC_ARB':                     0x1908000,
  'QDSS_WRAPPER_TOP':               0x33000,  
  'QDSS_APB_DEC_QDSS_APB':          0x29000,
  'QDSS_AHB_DEC_QDSS_AHB':          0x19000,
  'PDM_PERPH_WEB':                   0x4000,
  'CORE_TOP_CSR':                   0x58000,
  'LPASS':                         0x2b1000,
  'SECURITY_CONTROL':               0x0F000,
  'SPDM_WRAPPER_TOP':               0x05000,
  'TLMM':                          0x301000,
  'BLSP1_BLSP':                     0x39000,
  'BLSP2_BLSP':                     0x39000,
  'PRNG_PRNG_TOP':                  0x10000
}


HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiobase.h',
    'bases': bases,
    'map-type': 'qurt',
    'virtual-address-start': 0xE0000000,
    'virtual-address-end': 0xF0000000,
    'resize': base_resize,
    'qurt-memsection-filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwioplat.xml',
    'default-cache-policy': 'uncached',
    'devcfg-filename': '../config/' + CHIPSET + '/HWIOBaseMap.c',
    'check-sizes': True,
    'check-for-overlaps': True,
    
    # We need to manually map the LPASS region because the LPM region
    # which is right in the middle of LPASS needs special cache
    # attributes.  This is handled in the top-level qdsp6.xml file.
    'fixed-virtual-address': { 'LPASS': 0xEE000000 },
    'skip-memsection': ['LPASS'],
  }
]


# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# HWIO_REGISTER_FILES
# ============================================================================

HWIO_REGISTER_FILES = [
  {
    'filename': '../hw/' + CHIPSET + '/msmhwioreg.h',
    'bases': bases,
    'filter-exclude': ['RESERVED', 'DUMMY'],
    'header': '''
#error This file is for reference only and cannot be included.  See "go/hwio" or mail corebsp.sysdrv.hwio for help.
'''
  }
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)


