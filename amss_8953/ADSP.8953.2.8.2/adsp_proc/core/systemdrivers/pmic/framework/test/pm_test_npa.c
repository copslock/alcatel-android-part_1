/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM DRIVER FRAMEWORK TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-713705)
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"

#include "pm_npa.h"
#include "pmapp_npa.h"

#include "pm_test_framework.h"


/*===========================================================================

                VARIABLES DEFINITIONS

===========================================================================*/
npa_client_handle hNPA_test_RAILs = NULL;


/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_test_npa_framework ( void )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    g_pm_test_status[g_f_cnt].lvl = PM_TEST_NPA_LVL_0;

    hNPA_test_RAILs = npa_create_sync_client (PMIC_NPA_GROUP_ID_RAIL_CX, "PM_TEST_CX", NPA_CLIENT_REQUIRED);
    if (NULL != hNPA_test_RAILs)
    {
        npa_issue_required_request (hNPA_test_RAILs, PMIC_NPA_MODE_ID_CORE_RAIL_TURBO);
        npa_issue_required_request (hNPA_test_RAILs, PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL);
        npa_destroy_client (hNPA_test_RAILs);
    }
    else
    {
        pm_test_handle_error(PM_NPA_CX_VOTE, 2, PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED);
        err_flag = PM_ERR_FLAG__INVALID;
    }

    hNPA_test_RAILs = npa_create_sync_client (PMIC_NPA_GROUP_ID_RAIL_MX, "PM_TEST_MX", NPA_CLIENT_REQUIRED);
    if (NULL != hNPA_test_RAILs)
    {
        npa_issue_required_request (hNPA_test_RAILs, PMIC_NPA_MODE_ID_CORE_RAIL_TURBO);
        npa_issue_required_request (hNPA_test_RAILs, PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL);
        npa_destroy_client (hNPA_test_RAILs);
    }
    else
    {
        pm_test_handle_error(PM_NPA_CX_VOTE, 6, PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED);
        err_flag = PM_ERR_FLAG__INVALID;
    }

    return err_flag;
}

