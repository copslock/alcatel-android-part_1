/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.


==============================================================================

$Header: //components/rel/core.adsp/2.6.6/systemdrivers/clock/config/msm8937/ClockBSP.c#1 $

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"
#include "ClockLPASSCPU.h"
#include "pmapp_npa.h"

/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 *  SourceFreqConfig_XO
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    .nFreqHz    = 19200 * 1000,
    .HALConfig  = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x00, 0x00}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};

/*
 *  SourceFreqConfig_GPLL0
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    .nFreqHz    = 800000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO2,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 41,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0xaa000000,
      .nAlphaU        = 0xaa,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};

/*
 *  SourceFreqConfig_LPAPLL0
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL0[] =
{
  {
    .nFreqHz    = 614400 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO1,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 32,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};

/*
 *  SourceFreqConfig_LPAPLL1
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL1[] =
{
  {
    .nFreqHz    = 384000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 20,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 480000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 25,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 576000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 30,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 614400 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 32,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 691200 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 36,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0,
      .nAlphaU        = 0,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  ClockSourcesToInit
 *
 *  Array of sources and settings to initialize at runtime.
 *  Both PLLs initialized in hexagon_bsp_init for SAHI. 
 *  Left this as placeholder for future targets.
 */
const ClockSourceInitType ClockSourcesToInit[] =
{
   { HAL_CLK_SOURCE_NULL,    NULL}
};


/*
 * SourceConfig
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(XO),
    .nConfigMask       = 0,
    .pSourceFreqConfig = SourceFreqConfig_XO,
  },
  {
    SOURCE_NAME(GPLL0),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_GPLL0,
  },
  {
    SOURCE_NAME(LPAPLL0),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL0,
  },
  {
    SOURCE_NAME(LPAPLL1),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL1,
  },
  /* last entry */
  { SOURCE_NAME(NULL) }
};

/* ===================================================================================================================
**    nFreqHz,       { eSource, nDiv2x, nM, nN, n2D },      eVRegLevel,         { HW_MIN(maj, min), HW_MAX(maj, min) }
** ===================================================================================================================*/

/*----------------------------------------------------------------------*/
/* GCC Clock Configurations                                             */
/*----------------------------------------------------------------------*/

/*
 * QUP I2C clock configuration.
 */
const ClockMuxConfigType QUPI2CClockConfig[] =
{
  { 19200000,  { HAL_CLK_SOURCE_XO,      2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  { 50000000,  { HAL_CLK_SOURCE_GPLL0,  32,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  { 0 }
};

/*
 * UART clock configurations.
 */
const ClockMuxConfigType UARTClockConfig[] =
{
  {  3686400,  { HAL_CLK_SOURCE_GPLL0,   2,    72,  15625,  15625  },  CLOCK_VREG_LEVEL_LOW     },
  {  7372800,  { HAL_CLK_SOURCE_GPLL0,   2,   144,  15625,  15625  },  CLOCK_VREG_LEVEL_LOW     },
  { 14745600,  { HAL_CLK_SOURCE_GPLL0,   2,   288,  15625,  15625  },  CLOCK_VREG_LEVEL_LOW     },
  { 16000000,  { HAL_CLK_SOURCE_GPLL0,  20,     1,      5,      5  },  CLOCK_VREG_LEVEL_LOW     },
  { 19200000,  { HAL_CLK_SOURCE_XO,      2,     0,      0,      0  },  CLOCK_VREG_LEVEL_LOW     },
  { 24000000,  { HAL_CLK_SOURCE_GPLL0,   2,     3,    100,    100  },  CLOCK_VREG_LEVEL_LOW     },
  { 25000000,  { HAL_CLK_SOURCE_GPLL0,  32,     1,      2,      2  },  CLOCK_VREG_LEVEL_LOW     },
  { 32000000,  { HAL_CLK_SOURCE_GPLL0,   2,     1,     25,     25  },  CLOCK_VREG_LEVEL_LOW     },
  { 40000000,  { HAL_CLK_SOURCE_GPLL0,   2,     1,     20,     20  },  CLOCK_VREG_LEVEL_NOMINAL },
  { 46400000,  { HAL_CLK_SOURCE_GPLL0,   2,    29,    500,    500  },  CLOCK_VREG_LEVEL_NOMINAL },
  { 48000000,  { HAL_CLK_SOURCE_GPLL0,   2,     3,     50,     50  },  CLOCK_VREG_LEVEL_NOMINAL },
  { 51200000,  { HAL_CLK_SOURCE_GPLL0,   2,     8,     125,    125 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 56000000,  { HAL_CLK_SOURCE_GPLL0,   2,     7,     100,    100 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 58982400,  { HAL_CLK_SOURCE_GPLL0,   2,  1152,   15625,  15625 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 60000000,  { HAL_CLK_SOURCE_GPLL0,   2,     3,      40,     40 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 64000000,  { HAL_CLK_SOURCE_GPLL0,  25,     0,       0,      0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*----------------------------------------------------------------------*/
/* LPASS Clock Configurations                                           */
/*----------------------------------------------------------------------*/

/*
 * LPASS AHB clock configurations.
 * Note:  LPAUDIO_PLL main output has a CDIV-4 on the PLL output which sources 
 *        the RCG with a 153.6 MHz frequency, requiring a different RCG divider.
 */
const ClockMuxConfigType AHBClockConfig[] =
{
  {   3200000,  { HAL_CLK_SOURCE_XO,      12,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {   6400000,  { HAL_CLK_SOURCE_XO,       6,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {   9600000,  { HAL_CLK_SOURCE_XO,       4,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  19200000,  { HAL_CLK_SOURCE_XO,       2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  51200000,  { HAL_CLK_SOURCE_LPAPLL0,  6,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  61440000,  { HAL_CLK_SOURCE_LPAPLL0,  5,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  76800000,  { HAL_CLK_SOURCE_LPAPLL0,  4,  0,  0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 102400000,  { HAL_CLK_SOURCE_LPAPLL0,  3,  0,  0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 153600000,  { HAL_CLK_SOURCE_LPAPLL0,  2,  0,  0,  0 },  CLOCK_VREG_LEVEL_HIGH    },
  { 0 }
};

/*
 * Low Power Audio Interface (LPAIF) clocks
 */
const ClockMuxConfigType LPAIFOSRClockConfig[] =
{
  {   512000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,  16, 16 },  CLOCK_VREG_LEVEL_LOW     },
  {   768000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,  16, 16 },  CLOCK_VREG_LEVEL_LOW     },
  {  1024000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,   8,  8 },  CLOCK_VREG_LEVEL_LOW     },
  {  1536000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,   8,  8 },  CLOCK_VREG_LEVEL_LOW     },
  {  2048000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,   4,  4 },  CLOCK_VREG_LEVEL_LOW     },
  {  2400000,  { HAL_CLK_SOURCE_XO,       16,  0,   0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  3072000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,   4,  4 },  CLOCK_VREG_LEVEL_LOW     },
  {  4096000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,   2,  2 },  CLOCK_VREG_LEVEL_LOW     },
  {  6144000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,   2,  2 },  CLOCK_VREG_LEVEL_LOW     },
  {  8192000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  0,   0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 12288000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  0,   0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * Low Power Audio Interface (LPAIF) clocks
 */
const ClockMuxConfigType LPAIFPCMOEClockConfig[] =
{
  {    128000,  { HAL_CLK_SOURCE_LPAPLL0,  10,  1,  192,  192 },  CLOCK_VREG_LEVEL_LOW     },
  {    512000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,   16,   16 },  CLOCK_VREG_LEVEL_LOW     },
  {    768000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,   16,   16 },  CLOCK_VREG_LEVEL_LOW     },
  {   1024000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,    8,    8 },  CLOCK_VREG_LEVEL_LOW     },
  {   1536000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,    8,    8 },  CLOCK_VREG_LEVEL_LOW     },
  {   2048000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,    4,    4 },  CLOCK_VREG_LEVEL_LOW     },
  {   3072000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,    4,    4 },  CLOCK_VREG_LEVEL_LOW     },
  {   4096000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  1,    2,    2 },  CLOCK_VREG_LEVEL_LOW     },
  {   6144000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  1,    2,    2 },  CLOCK_VREG_LEVEL_LOW     },
  {   8192000,  { HAL_CLK_SOURCE_LPAPLL0,  30,  0,    0,    0 },  CLOCK_VREG_LEVEL_LOW     },
  {  12288000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  0,    0,    0 },  CLOCK_VREG_LEVEL_LOW     },
  {  61440000,  { HAL_CLK_SOURCE_LPAPLL0,   4,  0,    0,    0 },  CLOCK_VREG_LEVEL_LOW     },
  { 122880000,  { HAL_CLK_SOURCE_LPAPLL0,   2,  0,    0,    0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * Slimbus clock configurations. 
 *  
 * Note:  Slimbus requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 *  
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO PRIUS PLL 
 *        AUX2 selection which results in a 122.88 MHz frequency to
 *        supply the Slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType SlimbusClockConfig[] =
{
  { 24576000,  { HAL_CLK_SOURCE_LPAPLL0,  10,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  { 0 }
};

/*
 * Digcodec clock configurations. 
 *  
 * Note:  Digcodec requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 *  
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO  PLL 
 *        AUX2 selection which results in a 122.88 MHz frequency to
 *        supply the Slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType DigcodecClockConfig[] =
{
  {  9600000,  { HAL_CLK_SOURCE_XO,        4,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  { 19200000,  { HAL_CLK_SOURCE_XO,        2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  { 12288000,  { HAL_CLK_SOURCE_LPAPLL0,  20,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  { 24576000,  { HAL_CLK_SOURCE_LPAPLL0,  10,  0,  0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};

/*
 * QOS FIXED MONITOR clock configurations. 
 *  
 * Note:  QOS FIXED MONITOR  requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 *  
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO  PLL 
 *        AUX2 selection which results in a 122.88 MHz frequency to
 *        supply the Slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType QosMonitorClockConfig[] =
{
  {  19200000,  { HAL_CLK_SOURCE_XO,       2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  {  30720000,  { HAL_CLK_SOURCE_LPAPLL0,  8,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  {  61440000,  { HAL_CLK_SOURCE_LPAPLL0,  4,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  { 122880000,  { HAL_CLK_SOURCE_LPAPLL0,  2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW },
  { 0 }
};

/*
 * QUP SPI clock configurations.
 */
const ClockMuxConfigType  QUPSPIClockConfig[] =
{
  {   960000,  { HAL_CLK_SOURCE_XO,     20,  1,  2,  2 },  CLOCK_VREG_LEVEL_LOW     },
  {  4800000,  { HAL_CLK_SOURCE_XO,      8,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  {  9600000,  { HAL_CLK_SOURCE_XO,      4,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  { 16000000,  { HAL_CLK_SOURCE_GPLL0,  20,  1,  5,  5 },  CLOCK_VREG_LEVEL_LOW     },
  { 19200000,  { HAL_CLK_SOURCE_XO,      2,  0,  0,  0 },  CLOCK_VREG_LEVEL_LOW     },
  { 25000000,  { HAL_CLK_SOURCE_GPLL0,  32,  1,  2,  2 },  CLOCK_VREG_LEVEL_LOW     },
  { 50000000,  { HAL_CLK_SOURCE_GPLL0,  32,  0,  0,  0 },  CLOCK_VREG_LEVEL_NOMINAL },
  { 0 }
};


/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  { CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN, (void*)"lpass_q6core",                    CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_timeout_clk",       CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_timeout_clk",          CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_ixfabric_clk",         CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"q6ss_ahbm_clk",                   CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_dml_clk",              CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_lpaif_dma_clk",        CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_lpaif_csr_clk",        CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_slimbus_lfabif_clk",   CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_avsync_csr_clk",       CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_lpm_clk",              CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_smem_clk",          CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_lcc_csr_clk",       CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_efabric_clk",       CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_csr_clk",              CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_security_clk",         CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_security_clk",      CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_br_clk",            CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_sysnoc_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_aon_clk",              CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_wrapper_aon_clk",           CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"audio_core_digcodec_ahb_clk",     CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_CLOCK,        (void*)"gcc_lpass_mport_axi_clk",         CLOCK_FLAG_SUPPRESSIBLE },
  { CLOCK_FLAG_NODE_TYPE_NONE,         (void*)0,                                 0                       } 
};

/*
 * Resources that need to be published to domains outside GuestOS.
 */
static const char *ClockPubResource[] =
{
  CLOCK_NPA_RESOURCE_QDSS
};

ClockNPAResourcePubType ClockResourcePub = 
{
  SENSOR_PD,
  ClockPubResource,
  1
};

/*
 * Initial CX voltage level.
 */
const ClockVRegLevelType CXVRegInitLevelConfig[] =
{
  CLOCK_VREG_LEVEL_LOW
};


/*=========================================================================
      Type Definitions and Macros
==========================================================================*/

/*
 * Q6 Voltage levels in uv.
 */
#define CLOCK_CPU_VREG_LEVEL_LOW_UV      812500
#define CLOCK_CPU_VREG_LEVEL_NOMINAL_UV  900000
#define CLOCK_CPU_VREG_LEVEL_HIGH_UV    1050000


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Mux configuration for different CPU frequencies. 
 *  
 * Note:  LPAPLL2_AUX2 has a CDIV=2 coming out of the PLL output so the source 
 *        source coming into the RCG is 614.4/2 = 307.2 MHz.
 */
static ClockCPUConfigType Clock_QDSP6Config [] =
{
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   19200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_XO[0]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   115200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 10, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[2]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   144000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 8, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[2]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   230400 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 6, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[4]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   288000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[2]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   384000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[0]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
    /* .nStrapACCVal      */ 0
  },
{
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   480000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW_PLUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[1]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_NOMINAL_UV,
    /* .nStrapACCVal      */ 0
  },  
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   576000 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_NOMINAL,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[2]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_NOMINAL_UV,
    /* .nStrapACCVal      */ 0
  },
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   614400 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_NOMINAL_PLUS,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[3]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_HIGH_UV,
    /* .nStrapACCVal      */ 0
  },  
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    /* .Mux               */ {
    /* .nFreq             */   691200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_HIGH,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL1[4]
                             },
    /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_HIGH_UV,
    /* .nStrapACCVal      */ 0
  },
};


static ClockCPUConfigType IntermediateCPUConfig =
{
  /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCC,
  /* .Mux               */ {
    /* .nFreq             */   307200 * 1000,
    /* .HALConfig         */   { HAL_CLK_SOURCE_LPAPLL0, 2, 0, 0, 0 },
    /* .eVRegLevel        */   CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion         */   { {0x0, 0x0}, {0xFF, 0xFF} },
    /* .pSourceFreqConfig */   &SourceFreqConfig_LPAPLL0[0]
  },
  /* .nVDDCPU           */ CLOCK_CPU_VREG_LEVEL_LOW_UV,
  /* .nStrapACCVal      */ 0
};

/*
 * Enumeration of CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
enum
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,
  CLOCK_CPU_PERF_LEVEL_5,
  CLOCK_CPU_PERF_LEVEL_6,
  CLOCK_CPU_PERF_LEVEL_7,
  CLOCK_CPU_PERF_LEVEL_8,
  CLOCK_CPU_PERF_LEVEL_9,

  CLOCK_CPU_PERF_LEVEL_TOTAL
};

/*
 * Enumeration of generic CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
static const uint32 QDSP6PerfList[] =
{
  CLOCK_CPU_PERF_LEVEL_0,  /*  19.2 MHz */
  CLOCK_CPU_PERF_LEVEL_1,  /* 115.2 MHz */
  CLOCK_CPU_PERF_LEVEL_2,  /* 144.0 MHz */
  CLOCK_CPU_PERF_LEVEL_3,  /* 230.4 MHz */
  CLOCK_CPU_PERF_LEVEL_4,  /* 288.0 MHz */
  CLOCK_CPU_PERF_LEVEL_5,  /* 384.0 MHz */
  CLOCK_CPU_PERF_LEVEL_6,  /* 480.0 MHz */
  CLOCK_CPU_PERF_LEVEL_7,  /* 576.0 MHz */
  CLOCK_CPU_PERF_LEVEL_8,  /* 614.4 MHz */
  CLOCK_CPU_PERF_LEVEL_9,  /* 691.2 MHz */  
};


/*
 * Performance level configuration data for the Q6LPASS (CPU).
 */
static const ClockCPUPerfConfigType Clock_QDSP6LPASSPerfConfig[] =
{
  {
    /*
     * Define performance levels.
     */
   
    /*
     * min level
     */
    CLOCK_CPU_PERF_LEVEL_0,

    /*
     * max level
     */
    CLOCK_CPU_PERF_LEVEL_9,
   
    /*
     * Init performance level
     */
    CLOCK_CPU_PERF_LEVEL_5,
   
    /*
     * Total number of CPU configurations.
     */
    sizeof(Clock_QDSP6Config)/sizeof(ClockCPUConfigType),
   
    /*
     * Pointer to an array of supported performance level indices.
     */
    (uint32*)QDSP6PerfList
  }
};


/*
 * Main Image data structure.
 */
const ClockImageConfigType Clock_ImageConfig =
{
  /*
   * Pointer to the CPU frequency plan settings.
   */
   (ClockCPUConfigType*)Clock_QDSP6Config,

  /*
   * Pointer to the performance level configurations.
   */
   (ClockCPUPerfConfigType*)Clock_QDSP6LPASSPerfConfig,

   /*
    * Global enable flag for eLDO.
    */
   FALSE,

   (ClockCPUConfigType*)&IntermediateCPUConfig

};

/*
 *  Voltage levels for Rails
 */
const ClockVRegLevelType ClcokVRegLevelsMap[CLOCK_VREG_NUM_LEVELS] =
{
  PMIC_NPA_MODE_ID_CORE_RAIL_OFF,          /**< OFF. */
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION,    /**< Retention. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS,    /**< Low minus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW,          /**< Low. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_PLUS,     /**< Low plus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,      /**< Nominal. */
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL_PLUS, /**< Nominal plus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO,        /**< Turbo. */
};

/*
 * OCMEM is not used on this chipset.  Add additional flags to this as needed.
 */
const ClockNPARemoteNodeSupportType ClockNPARemoteNodeSupport = 
{
   /* bOCMEM  = */ FALSE,
};

/*
 * Stub flags.
 */
const ClockStubType ClockStubConfig =
{
  .bRUMI   = FALSE,
  .bVirtio = FALSE,
};
