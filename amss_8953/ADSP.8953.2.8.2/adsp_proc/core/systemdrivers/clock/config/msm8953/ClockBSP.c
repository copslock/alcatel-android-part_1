/*
==============================================================================

FILE:         ClockBSP.c 

DESCRIPTION:
  This file contains clock BSP data for the DAL based driver.


==============================================================================

$Header: //components/rel/core.adsp/2.6.6/systemdrivers/clock/config/msm8953/ClockBSP.c#1 $

==============================================================================
            Copyright (c) 2016 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"
#include "ClockLPASSCPU.h"
#include "pmapp_npa.h"

/*=========================================================================
      Data Declarations
==========================================================================*/

/*
 *  SourceFreqConfig_XO
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    .nFreqHz    = 19200 * 1000,
    .HALConfig  = { HAL_CLK_SOURCE_NULL },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    .nFreqHz    = 800000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 41,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0xAA000000,
      .nAlphaU        = 0xAA,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0_OUT_MAIN_DIV2
 *  Set of source frequency configurations.
 *
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0_OUT_MAIN_DIV2[] =
{
  {
    .nFreqHz    = 400000 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_GPLL0,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 2,
      .nPostDiv       = 0,
      .nL             = 0,
      .nM             = 0,
      .nN             = 1,
      .nVCOMultiplier = 0, /* Bypass multiplier for derived source */
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL0
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL0[] =
{
  {
    .nFreqHz    = 564480 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,    /* VCO Mode is not applicable to Brammo PLL */
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 29,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x66000000,
      .nAlphaU        = 0x00000066,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0, 0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL1
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL1[] =
{
  {
    .nFreqHz    = 499200 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 26,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 556800 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 29,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 652800 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 34,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 691200 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 36,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 748800 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 39,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  {
    .nFreqHz    = 844800 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 1,
      .nL             = 44,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_LPAPLL2
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_LPAPLL2[] =
{
  {
    .nFreqHz    = 614400 * 1000,
    .HALConfig  =
    {
      .eSource        = HAL_CLK_SOURCE_XO,
      .eVCO           = HAL_CLK_PLL_VCO3,
      .nPreDiv        = 1,
      .nPostDiv       = 4,
      .nL             = 32,
      .nM             = 0,
      .nN             = 0,
      .nVCOMultiplier = 0,
      .nAlpha         = 0x00000000,
      .nAlphaU        = 0x00,
    },
    .eVRegLevel = CLOCK_VREG_LEVEL_LOW_MINUS,
    .HWVersion  = { {0x0, 0x0}, {0xFF, 0xFF} },
  },
  /* last entry */
  { 0 }
};


/*
 *  ClockSourcesToInit
 *
 *  Array of sources and settings to initialize at runtime.
 *  Both PLLs initialized in hexagon_bsp_init for SAHI. 
 *  Left this as placeholder for future targets.
 */
const ClockSourceInitType ClockSourcesToInit[] =
{
   { HAL_CLK_SOURCE_LPAPLL0, 564480 * 1000 },
   { HAL_CLK_SOURCE_NULL,    NULL}
};


/*
 * SourceConfig
 *
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(XO),
    .nConfigMask       = 0,
    .pSourceFreqConfig = SourceFreqConfig_XO,
  },
  {
    SOURCE_NAME(GPLL0),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_GPLL0,
  },
  {
    SOURCE_NAME(GPLL0_OUT_MAIN_DIV2),
    .nConfigMask       = 0,
    .pSourceFreqConfig = SourceFreqConfig_GPLL0_OUT_MAIN_DIV2,
  },
  {
    SOURCE_NAME(LPAPLL0),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE
                       | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL0,
  },
  {
    SOURCE_NAME(LPAPLL1),
    .nConfigMask       = CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE
                       | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE
                       | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL1,
    .pCalibrationFreqConfig = &SourceFreqConfig_LPAPLL1[4],  //748.8 MHz
    .eDisableMode      = HAL_CLK_SOURCE_DISABLE_MODE_FREEZE
  },
  {
    SOURCE_NAME(LPAPLL2),
    .nConfigMask       = CLOCK_CONFIG_PLL_FSM_MODE_ENABLE
                       | CLOCK_CONFIG_PLL_AUX_OUTPUT_ENABLE
                       | CLOCK_CONFIG_PLL_AUX2_OUTPUT_ENABLE
                       | CLOCK_CONFIG_PLL_EARLY_OUTPUT_ENABLE,
    .pSourceFreqConfig = SourceFreqConfig_LPAPLL2,
  },
  /* last entry */
  { SOURCE_NAME(NULL) }
};


/* =========================================================================
**    nFreqHz,       { eSource, nDiv2x, nM, nN, n2D },      eVRegLevel      
** =========================================================================*/


/*----------------------------------------------------------------------*/
/* GCC clock configurations                                             */
/*----------------------------------------------------------------------*/


/*
 * QUP I2C clock configuration.
 */
const ClockMuxConfigType QUPI2CClockConfig[] =
{
  { 19200000, { HAL_CLK_SOURCE_XO,                    2,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 25000000, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,  32,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 50000000, { HAL_CLK_SOURCE_GPLL0,                32,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW       },
  { 0 }
};


/*
 * UART clock configurations.
 */
const ClockMuxConfigType UARTClockConfig[] =
{
  {  3686400, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,   2,   144,  15625,  15625 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  7372800, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,   2,   288,  15625,  15625 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 14745600, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,   2,   576,  15625,  15625 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 16000000, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,  10,     1,      5,      5 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 19200000, { HAL_CLK_SOURCE_XO,                    2,     0,      0,      0 }, CLOCK_VREG_LEVEL_LOW       },
  { 24000000, { HAL_CLK_SOURCE_GPLL0,                 2,     3,    100,    100 }, CLOCK_VREG_LEVEL_LOW       },
  { 25000000, { HAL_CLK_SOURCE_GPLL0,                32,     1,      2,      2 }, CLOCK_VREG_LEVEL_LOW       },
  { 32000000, { HAL_CLK_SOURCE_GPLL0,                 2,     1,     25,     25 }, CLOCK_VREG_LEVEL_LOW       },
  { 40000000, { HAL_CLK_SOURCE_GPLL0,                 2,     1,     20,     20 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 46400000, { HAL_CLK_SOURCE_GPLL0,                 2,    29,    500,    500 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 48000000, { HAL_CLK_SOURCE_GPLL0,                 2,     3,     50,     50 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 51200000, { HAL_CLK_SOURCE_GPLL0,                 2,     8,    125,    125 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 56000000, { HAL_CLK_SOURCE_GPLL0,                 2,     7,    100,    100 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 58982400, { HAL_CLK_SOURCE_GPLL0,                 2,  1152,  15625,  15625 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 60000000, { HAL_CLK_SOURCE_GPLL0,                 2,     3,     40,     40 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 64000000, { HAL_CLK_SOURCE_GPLL0,                25,     0,      0,      0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};


/*
 * QUP SPI clock configurations.
 */
const ClockMuxConfigType QUPSPIClockConfig[] =
{
  {   960000, { HAL_CLK_SOURCE_XO,                   20,      1,      2,     2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  4800000, { HAL_CLK_SOURCE_XO,                    8,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  9600000, { HAL_CLK_SOURCE_XO,                    4,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 12500000, { HAL_CLK_SOURCE_GPLL0_OUT_MAIN_DIV2,  32,      1,      2,     2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 16000000, { HAL_CLK_SOURCE_GPLL0,                20,      1,      5,     5 }, CLOCK_VREG_LEVEL_LOW       },
  { 19200000, { HAL_CLK_SOURCE_XO,                    2,      0,      0,     0 }, CLOCK_VREG_LEVEL_LOW       },
  { 25000000, { HAL_CLK_SOURCE_GPLL0,                32,      1,      2,     2 }, CLOCK_VREG_LEVEL_LOW       },
  { 50000000, { HAL_CLK_SOURCE_GPLL0,                32,      0,      0,     0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};


/*----------------------------------------------------------------------*/
/* LPASS clock configurations                                           */
/*----------------------------------------------------------------------*/

/*
 * LPASS AHB clock configurations.
 * Note:  LPAUDIO_PLL main output has a CDIV-4 on the PLL output which sources 
 *        the RCG with a 153.6 MHz frequency, requiring a different RCG divider.
 */
const ClockMuxConfigType AHBClockConfig[] =
{
  {   3200000, { HAL_CLK_SOURCE_XO,       12,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   6400000, { HAL_CLK_SOURCE_XO,        6,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   9600000, { HAL_CLK_SOURCE_XO,        4,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  19200000, { HAL_CLK_SOURCE_XO,        2,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  38400000, { HAL_CLK_SOURCE_LPAPLL2,   8,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  51200000, { HAL_CLK_SOURCE_LPAPLL2,   6,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW       },
  {  61440000, { HAL_CLK_SOURCE_LPAPLL2,   5,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW       },
  {  76800000, { HAL_CLK_SOURCE_LPAPLL2,   4,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW       },
  { 102400000, { HAL_CLK_SOURCE_LPAPLL2,   3,  0,  0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 153600000, { HAL_CLK_SOURCE_LPAPLL2,   2,  0,  0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};


/*
 * Low Power Audio Interface (LPAIF) clocks
 */
const ClockMuxConfigType LPAIFOSRClockConfig[] =
{
  {   512000, { HAL_CLK_SOURCE_LPAPLL2,  30,  1,  16, 16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   768000, { HAL_CLK_SOURCE_LPAPLL2,  20,  1,  16, 16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  1024000, { HAL_CLK_SOURCE_LPAPLL2,  30,  1,   8,  8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  1536000, { HAL_CLK_SOURCE_LPAPLL2,  20,  1,   8,  8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  2048000, { HAL_CLK_SOURCE_LPAPLL2,  30,  1,   4,  4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  2400000, { HAL_CLK_SOURCE_XO,       16,  0,   0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  3072000, { HAL_CLK_SOURCE_LPAPLL2,  20,  1,   4,  4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  4096000, { HAL_CLK_SOURCE_LPAPLL2,  30,  1,   2,  2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  6144000, { HAL_CLK_SOURCE_LPAPLL2,  20,  1,   2,  2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  8192000, { HAL_CLK_SOURCE_LPAPLL2,  30,  0,   0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2,  20,  0,   0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * Low Power Audio Interface (LPAIF) clocks
 */
const ClockMuxConfigType LPAIFPCMOEClockConfig[] =
{
  {    352800, { HAL_CLK_SOURCE_LPAPLL0, 20, 1,  32,  32 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {    128000, { HAL_CLK_SOURCE_LPAPLL2, 10, 1, 192, 192 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {    512000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1,  16,  16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {    705600, { HAL_CLK_SOURCE_LPAPLL0, 20, 1,  16,  16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {    768000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1,  16,  16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   1024000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1,   8,   8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   1411200, { HAL_CLK_SOURCE_LPAPLL0, 20, 1,   8,   8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   1536000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1,   8,   8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   2048000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1,   4,   4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   2822400, { HAL_CLK_SOURCE_LPAPLL0, 20, 1,   4,   4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   3072000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1,   4,   4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   4096000, { HAL_CLK_SOURCE_LPAPLL2, 30, 1,   2,   2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   5644800, { HAL_CLK_SOURCE_LPAPLL0, 20, 1,   2,   2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   6144000, { HAL_CLK_SOURCE_LPAPLL2, 20, 1,   2,   2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   8192000, { HAL_CLK_SOURCE_LPAPLL2, 30, 0,   0,   0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  11289600, { HAL_CLK_SOURCE_LPAPLL0, 20, 0,   0,   0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  12288000, { HAL_CLK_SOURCE_LPAPLL2, 20, 0,   0,   0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  61440000, { HAL_CLK_SOURCE_LPAPLL2,  4, 0,   0,   0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 122880000, { HAL_CLK_SOURCE_LPAPLL2,  2, 0,   0,   0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * Slimbus clock configurations.
 * Note:  Slimbus requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO DIG PLL AUX2 
 *        selection which results in a 122.88 MHz frequency to
 *        supply the slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType SlimbusClockConfig[] =
{
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 0 }
};

/*
 * Digcodec clock configurations. 
 * Note:  Digcodec requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO DIG PLL 
 *        AUX2 selection which results in a 122.88 MHz frequency to
 *        supply the slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType DigcodecClockConfig[] =
{
  {  9600000, { HAL_CLK_SOURCE_XO,         4,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 19200000, { HAL_CLK_SOURCE_XO,         2,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2,   20,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,  0,  0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};

/*
 * EXTMCLK clock configurations
 */
const ClockMuxConfigType EXTMCLKClockConfig[] =
{
  {   352800, { HAL_CLK_SOURCE_LPAPLL0,   20,  1, 32, 32 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   512000, { HAL_CLK_SOURCE_LPAPLL2,   30,  1, 16, 16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   705600, { HAL_CLK_SOURCE_LPAPLL0,   20,  1, 16, 16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {   768000, { HAL_CLK_SOURCE_LPAPLL2,   20,  1, 16, 16 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  1024000, { HAL_CLK_SOURCE_LPAPLL2,   30,  1,  8,  8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  1411200, { HAL_CLK_SOURCE_LPAPLL0,   20,  1,  8,  8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  1536000, { HAL_CLK_SOURCE_LPAPLL2,   20,  1,  8,  8 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  2048000, { HAL_CLK_SOURCE_LPAPLL2,   30,  1,  4,  4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  2822400, { HAL_CLK_SOURCE_LPAPLL0,   20,  1,  4,  4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  3072000, { HAL_CLK_SOURCE_LPAPLL2,   20,  1,  4,  4 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  4096000, { HAL_CLK_SOURCE_LPAPLL2,   30,  1,  2,  2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  5644800, { HAL_CLK_SOURCE_LPAPLL0,   20,  1,  2,  2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  6144000, { HAL_CLK_SOURCE_LPAPLL2,   20,  1,  2,  2 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  8192000, { HAL_CLK_SOURCE_LPAPLL2,   30,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  9600000, { HAL_CLK_SOURCE_XO,         4,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 11289600, { HAL_CLK_SOURCE_LPAPLL0,   20,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 12288000, { HAL_CLK_SOURCE_LPAPLL2,   20,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 24576000, { HAL_CLK_SOURCE_LPAPLL2,   10,  0,  0,  0 }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 0 }
};


/*
 * QOS FIXED MONITOR clock configurations. 
 *  
 * Note:  QOS FIXED MONITOR  requires nominal voltage, but is making requests
 *        directly to the PMIC driver so set the vote here for low.
 * Note:  There is a CDIV-5 on the output of the LPAAUDIO DIG PLL 
 *        AUX2 selection which results in a 122.88 MHz frequency to
 *        supply the slimbus core clock.  Further division reflects
 *        the proper frequency.
 */
const ClockMuxConfigType QosMonitorClockConfig[] =
{
  {  19200000, { HAL_CLK_SOURCE_XO,        2,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  30720000, { HAL_CLK_SOURCE_LPAPLL2,   8,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  61440000, { HAL_CLK_SOURCE_LPAPLL2,   4,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW_MINUS },
  { 122880000, { HAL_CLK_SOURCE_LPAPLL2,   2,  0,  0,  0 }, CLOCK_VREG_LEVEL_LOW       },
  { 0 }
};


/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0x12 will log only clock frequency
 *       changes and source state changes by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0x12
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  { 
    CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN,
    (void*)"lpass_q6core",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_timeout_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_timeout_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_ixfabric_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"q6ss_ahbm_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_dml_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_lpaif_dma_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_lpaif_csr_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_slimbus_lfabif_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_avsync_csr_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_lpm_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_smem_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_lcc_csr_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_efabric_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_csr_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_security_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_security_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_br_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_sysnoc_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_aon_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_wrapper_aon_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"audio_core_digcodec_ahb_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK,
    (void*)"gcc_lpass_mport_axi_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void*)"HAL_CLK_SOURCE_LPAPLL1",
    CLOCK_FLAG_SUPPORTS_SLEWING
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void*)"GPLL0_OUT_MAIN_DIV2",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_NONE,
    (void*)0,
    0
  }
};

/*
 * Resources that need to be published to domains outside GuestOS.
 */
static const char *ClockPubResource[] =
{
   CLOCK_NPA_RESOURCE_QDSS
};

ClockNPAResourcePubType ClockResourcePub = 
{
   SENSOR_PD,
   ClockPubResource,
   1
};



/*
 * Initial CX voltage level.
 */
const ClockVRegLevelType CXVRegInitLevelConfig[] =
{
  CLOCK_VREG_LEVEL_LOW_MINUS
};


/*=========================================================================
      Type Definitions and Macros
==========================================================================*/

/*
 * Q6 Voltage levels in uv.
 */
#define CLOCK_CPU_VREG_LEVEL_LOW_UV      812500
#define CLOCK_CPU_VREG_LEVEL_NOMINAL_UV  900000
#define CLOCK_CPU_VREG_LEVEL_HIGH_UV    1050000


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * QDSP6 clock configurations
 */
static ClockCPUConfigType Clock_QDSP6Config [] =
{
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   19200000,
      .HALConfig         =   { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW_MINUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_XO[0]
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_LOW_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   115200000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 12, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW_MINUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[3]  //691.2 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_LOW_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   230400000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 6, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW_MINUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[3]  //691.2 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_LOW_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   326400000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 4, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW_MINUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[2]  //652.8 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_LOW_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   499200000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[0]  //499.2 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_NOMINAL_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   556800000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_LOW_PLUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[1]  //556.8 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_NOMINAL_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   691200000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_NOMINAL,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[3]  //691.2 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_HIGH_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   748800000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_NOMINAL_PLUS,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[4]  //748.8 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_HIGH_UV,
    .nStrapACCVal    = 0
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX_SRCA,
    .Mux             =
    {
      .nFreqHz           =   844800000,
      .HALConfig         =   { HAL_CLK_SOURCE_LPAPLL1, 2, 0, 0, 0 },
      .eVRegLevel        =   CLOCK_VREG_LEVEL_HIGH,
      .HWVersion         =   { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig =   &SourceFreqConfig_LPAPLL1[5]  //844.4 MHz
    },
    .nVDDCPU         = CLOCK_CPU_VREG_LEVEL_NOMINAL_UV,
    .nStrapACCVal    = 0
  }
};

/*
 * Enumeration of CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
enum
{
  CLOCK_CPU_PERF_LEVEL_0,
  CLOCK_CPU_PERF_LEVEL_1,
  CLOCK_CPU_PERF_LEVEL_2,
  CLOCK_CPU_PERF_LEVEL_3,
  CLOCK_CPU_PERF_LEVEL_4,
  CLOCK_CPU_PERF_LEVEL_5,
  CLOCK_CPU_PERF_LEVEL_6,
  CLOCK_CPU_PERF_LEVEL_7,
  CLOCK_CPU_PERF_LEVEL_8,

  CLOCK_CPU_PERF_LEVEL_TOTAL
};

/*
 * Enumeration of generic CPU performance levels.  More performance 
 * levels exist here than are actually implemented.
 */
static const uint32 QDSP6PerfList[] =
{
  CLOCK_CPU_PERF_LEVEL_0,  /*  19.2 MHz */
  CLOCK_CPU_PERF_LEVEL_1,  /* 115.2 MHz */
  CLOCK_CPU_PERF_LEVEL_2,  /* 230.4 MHz */
  CLOCK_CPU_PERF_LEVEL_3,  /* 326.4 MHz */
  CLOCK_CPU_PERF_LEVEL_4,  /* 499.2 MHz */
  CLOCK_CPU_PERF_LEVEL_5,  /* 556.8 MHz */
  CLOCK_CPU_PERF_LEVEL_6,  /* 691.2 MHz */
  CLOCK_CPU_PERF_LEVEL_7,  /* 748.8 MHz */
  CLOCK_CPU_PERF_LEVEL_8,  /* 844.8 MHz */
};

/*
 * Performance level configuration data for the Q6LPASS (CPU).
 */
static const ClockCPUPerfConfigType Clock_QDSP6LPASSPerfConfig[] =
{
{
    /*
     * Define performance levels.
     */
   
    /*
     * min level
     */
    CLOCK_CPU_PERF_LEVEL_0,

    /*
     * max level
     */
    CLOCK_CPU_PERF_LEVEL_8,
   
    /*
     * Total number of CPU configurations.
     */
    sizeof(Clock_QDSP6Config)/sizeof(ClockCPUConfigType),

/*
     * Pointer to an array of supported performance level indices.
 */
    (uint32*)QDSP6PerfList
  }
};


/*
 * Main Image data structure.
 */
const ClockImageConfigType Clock_ImageConfig =
{
  /*
   * Pointer to the CPU frequency plan settings.
   */
   (ClockCPUConfigType*)Clock_QDSP6Config,

  /*
   * Pointer to the performance level configurations.
   */
   (ClockCPUPerfConfigType*)Clock_QDSP6LPASSPerfConfig,

 
   /*
    * Global enable flag for eLDO.
    */
   FALSE,
   
   NULL 

};

/*
 *  Voltage levels for Rails
 */
const ClockVRegLevelType ClcokVRegLevelsMap[CLOCK_VREG_NUM_LEVELS] =
{
  PMIC_NPA_MODE_ID_CORE_RAIL_OFF,          /**< OFF. */
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION,    /**< Retention. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS,    /**< Low minus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW,          /**< Low. */
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_PLUS,     /**< Low plus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,      /**< Nominal. */
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL_PLUS, /**< Nominal plus. */
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO,        /**< Turbo. */
};

/*
 * OCMEM is not used on this chipset.  Add additional flags to this as needed.
 */
const ClockNPARemoteNodeSupportType ClockNPARemoteNodeSupport = 
{
   /* bOCMEM  = */ FALSE,
};

/*
 * Stub flags.
 */
const ClockStubType ClockStubConfig =
{
  .bRUMI   = FALSE,
  .bVirtio = FALSE,
};
