/*
===========================================================================
*/
/**
  @file uClock.c
  @brief Public definitions include file for accessing the uClock device driver.
*/
/*===========================================================================
NOTE: The @brief description and any detailed descriptions above do not appear 
      in the PDF. 

      This file is the public header file for the sensors micro image (uImage)
      clock driver implementation.  It is meant to be used by a small subset
      of drivers supporting the sensors uImage feature, which cannot access
      DDR.

===========================================================================*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.adsp/2.6.6/systemdrivers/clock/hw/msm8952/src/uClock.c#1 $
  $DateTime: 2016/05/04 01:42:14 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     ---------------------------------------------------------- 
  12/17/13   dcf     Added for MSM8994 micro sensors functionality.

  ===========================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "com_dtypes.h"
#include "uClock.h"
#include "uClockHWIO.h"
#include "HALhwio.h"
#include "qurt.h"
#include "busywait.h"

/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * Micro-Clock state context structure.
 * Parameters:
 *   Mutex             - Synchronization mutex locking.
 *   bInitialized      - Specifies if initialization has taken place.
 *   anClockReferences - Array of clock references.
 */
typedef struct
{
   qurt_mutex_t Mutex;
   boolean      bInitialized;
   uint32       anClockReferences[CLOCK_TOTAL_CLOCK_ENUMS];

}uClockCtxtType;


/*=========================================================================
      Local Variables
==========================================================================*/

/*
 * Global Micro-Clock Driver Context.
 */
static uClockCtxtType uClockDriverCtxt;

/*=========================================================================
      Function Definitions.
==========================================================================*/

/* ============================================================================
**  Function : uClock_Init
**  Initilizes the uClock Driver. 
** ============================================================================
*/

void uClock_Init(void)
{
  if (FALSE == uClockDriverCtxt.bInitialized)
  {
    qurt_mutex_init(&uClockDriverCtxt.Mutex);
    uClockDriverCtxt.bInitialized = TRUE;
  }
}


/* ============================================================================
**  Function : uClock_IsOn
** ============================================================================
*/
/**
  Internal API which returns whether a clock is on or not.
    
  @param[in]  eClockId  - The ID of the clock to enable.

  @return
  TRUE  - The clock is on.
  FALSE - Otherwise.
  
  @dependencies
  None.

*/
boolean uClock_IsOn(uClockIdType eClockId)
{
  uint32 nVal = 0;
    
  switch(eClockId)
  {
    case CLOCK_GCC_BLSP1_QUP1_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP1_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP2_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP3_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP3_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP4_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP4_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP1_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP1_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP2_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP2_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP3_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP3_I2C_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP4_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP4_I2C_APPS_CBCR, CLK_OFF);
       break;
    case CLOCK_GCC_BLSP1_QUP1_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP1_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP2_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP2_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP3_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP3_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP1_QUP4_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP1_QUP4_SPI_APPS_CBCR, CLK_OFF);
       break;
	   
    case CLOCK_GCC_BLSP2_QUP1_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP1_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP2_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP2_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP3_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP3_SPI_APPS_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_QUP4_SPI_APPS_CLK:
       nVal = HWIO_INF(GCC_BLSP2_QUP4_SPI_APPS_CBCR, CLK_OFF);
       break;
	   
    case CLOCK_GCC_BLSP1_AHB_CLK:
       nVal = HWIO_INF(GCC_BLSP1_AHB_CBCR, CLK_OFF);
       break;

    case CLOCK_GCC_BLSP2_AHB_CLK:
       nVal = HWIO_INF(GCC_BLSP2_AHB_CBCR, CLK_OFF);
       break;
       
    default:
       break;
   }
   
   if(nVal != 0)
   {
     return(FALSE);
   }
   else
   {
     return(TRUE);
   }
   
} /* uClock_IsOn */


/* ============================================================================
**  Function : uClock_EnableClockInternal
** ============================================================================
*/
/**
  Enables a clock or disabled correctly.
    
  @param[in]  eClockId  - The ID of the clock to enable.
  @param[in]  bEnable   - Boolean on whether to enable or disable the clock.

  @return
  TRUE  - The clock was enabled or disabled correctly.
  FALSE - Otherwise.
  
  @dependencies
  None.

*/

static boolean uClock_EnableClockInternal(uClockIdType eClockId, boolean bEnable)
{
  boolean bRetVal = TRUE;
  uint32 nTimeout = 150;
  uint32 nOutput = 0;

  if (bEnable)
  {
    nOutput = 0x1;
  }

  if(uClockDriverCtxt.bInitialized == FALSE)
  {
    return FALSE;
  }

  qurt_mutex_lock(&uClockDriverCtxt.Mutex);

  if(eClockId < CLOCK_TOTAL_CLOCK_ENUMS)
  {
    if( ( uClockDriverCtxt.anClockReferences[eClockId] > 0 ) &&
        ( bEnable == FALSE ) )
    {
      uClockDriverCtxt.anClockReferences[eClockId]--;
    }

    if(uClockDriverCtxt.anClockReferences[eClockId] == 0)
    {
      switch(eClockId)
      {
        case CLOCK_GCC_BLSP1_QUP1_APPS_CLK:
           HWIO_OUTF(GCC_BLSP1_QUP1_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
           break;

        case CLOCK_GCC_BLSP1_QUP2_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP3_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP3_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP4_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP4_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP1_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP1_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP2_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP2_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP3_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP3_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP4_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP4_I2C_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP1_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP1_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP2_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP2_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP3_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP3_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_QUP4_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP1_QUP4_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP1_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP1_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP2_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP2_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP3_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP3_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP2_QUP4_SPI_APPS_CLK:
          HWIO_OUTF(GCC_BLSP2_QUP4_SPI_APPS_CBCR, CLK_ENABLE, nOutput);
          break;

        case CLOCK_GCC_BLSP1_AHB_CLK:
          HWIO_OUTF(GCC_SPARE_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, nOutput);
          break;

        case CLOCK_GCC_BLSP2_AHB_CLK:
          HWIO_OUTF(GCC_SPARE_CLOCK_BRANCH_ENA_VOTE, BLSP2_AHB_CLK_ENA, nOutput);
          break;

        default:
          bRetVal = FALSE;
          break;
      }

    }

    if(bEnable)
    {
      while(uClock_IsOn(eClockId) != TRUE) 
      {
        if(nTimeout == 0)
        {
          break;
        }
        busywait(2);
        nTimeout--;
      }

      /*
       * Increase the reference count on this clock.
       */
      if(nTimeout != 0)
      {
        uClockDriverCtxt.anClockReferences[eClockId]++;
        bRetVal = TRUE;
      }
      else
      {
        bRetVal = FALSE;
      }    
    }

    qurt_mutex_unlock(&uClockDriverCtxt.Mutex); 

    return(bRetVal);
  }

  qurt_mutex_unlock(&uClockDriverCtxt.Mutex); 

  return(FALSE);

} /* uClock_EnableClockInternal */


/* ============================================================================
**  Function : uClock_EnableClock
** ============================================================================
*/
/**
  Enables a clock.
    
  @param[in]  eClockId  - The ID of the clock to enable.

  @return
  TRUE  - The clock was enabled.
  FALSE - Otherwise.
  
  @dependencies
  None.

*/

boolean uClock_EnableClock(uClockIdType eClockId)
{
  return uClock_EnableClockInternal( eClockId, TRUE );
} /* uClock_EnableClock */


/* ============================================================================
**  Function : uClock_DisableClock
** ============================================================================
*/
/**
  Disables a clock.
    
  @param[in]  eClockId  - The ID of the clock to disable.

  @return
  TRUE  - The clock was disabled.
  FALSE - Otherwise.
  
  @dependencies
  None.

*/

boolean uClock_DisableClock(uClockIdType eClockId)
{
  return uClock_EnableClockInternal( eClockId, FALSE );
} /* uClock_DisableClock */


/* ============================================================================
**  Function : uClock_IsClockEnabled
** ============================================================================
*/
/**
  Returns whether a clock is enabled or not.
    
  @param[in]  eClockId  - The ID of the clock to disable.

  @return
  TRUE  - The clock is on.
  FALSE - The clock is off.
  
  @dependencies
  None.

*/

boolean uClock_IsClockEnabled(uClockIdType eClockId)
{
   return(uClock_IsOn(eClockId));
   
} /* uClock_IsClockEnabled */


/* ============================================================================
**  Function : uClock_SetClockDivider
** ============================================================================
*/
/**
  Sets the clock divider of a particular clock domain
    
  @param[in]  eClockId  - The ID of the clock to set the divider on.
  @param[in]  n2Div     - The 2*divider value to program.

  @return
  TRUE  - The divider was programmed.
  FALSE - The operation is not supported for the requested clock.
  
  @dependencies
  None.

*/

boolean uClock_SetClockDivider(uClockIdType eClock, uint32 n2Div)
{
  uint32 nCDiv = 0;
 
  /* Clock dividers supported are 1 - 16.
   * 0 is to BYPASS RCG.
   * n2Div must be b/w 1 - 32
   * We need to write 2N-1 into RCGR where N is the divide ratio.
   * For example, if the half integer divide ratio is 2.5, the src_div should be set at 4.
   */
  if((n2Div != 0) && (n2Div <= 32))
  {  
    nCDiv = n2Div-1;  
  }	

  /* 
   * BLSP QUP SPI clocks are expected to be sourced from XO in Big and uImages,
   * so SRC_SEL in RCGR should be 0 that means source is CXO. 
   */

  switch(eClock)
  {
    case CLOCK_GCC_BLSP1_QUP1_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP1_QUP2_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP1_QUP3_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP1_QUP4_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
    case CLOCK_GCC_BLSP2_QUP1_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP2_QUP1_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP2_QUP1_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP2_QUP1_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP2_QUP1_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP2_QUP2_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP2_QUP2_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP2_QUP2_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  
      HWIO_OUTF(GCC_BLSP2_QUP2_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP2_QUP2_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP2_QUP3_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP2_QUP3_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP2_QUP3_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  	  
      HWIO_OUTF(GCC_BLSP2_QUP3_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP2_QUP3_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
	case CLOCK_GCC_BLSP2_QUP4_SPI_APPS_CLK:
      HWIO_OUTF(GCC_BLSP2_QUP4_SPI_APPS_CFG_RCGR, SRC_DIV, nCDiv);
      HWIO_OUTF(GCC_BLSP2_QUP4_SPI_APPS_CFG_RCGR, SRC_SEL, 0);	  	  
      HWIO_OUTF(GCC_BLSP2_QUP4_SPI_APPS_CMD_RCGR, UPDATE, 0x1);
	  while(HWIO_INF(GCC_BLSP2_QUP4_SPI_APPS_CMD_RCGR, UPDATE));
      break;
	  
    default:
      return FALSE;
  }

  return TRUE;
}


