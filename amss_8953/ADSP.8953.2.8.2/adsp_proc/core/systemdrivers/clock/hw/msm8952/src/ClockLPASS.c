/*
===========================================================================
*/
/**
  @file ClockLPASS.c 
  
  Main entry point for the MSM8974 LPASS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All rights reserved.
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.adsp/2.6.6/systemdrivers/clock/hw/msm8952/src/ClockLPASS.c#1 $
  $DateTime: 2016/05/04 01:42:14 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  09/07/11   dcf     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "ClockDriver.h"
#include "ClockLPASS.h"
#include "ClockLPASSLDO.h"
#include "msmhwiobase.h"
#include "busywait.h"
#include "DDIChipInfo.h"
#include "DDIPlatformInfo.h"
#include "ClockSWEVT.h"
#include "DDITlmm.h"
#include "ClockLPASSCPU.h"

/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/

extern void Clock_EnableSPMCTL(void);
extern void Clock_DisableSPMCTL(void);
extern uint32 Clock_nHWIOBaseLPASS;

/*=========================================================================
      Data
==========================================================================*/

static ClockLPASSCtxtType Clock_LPASSCtxt;
static ClockImageConfigType Clock_ImageConfig;

static const char *ClockNPANodeName[] =
{
  CLOCK_NPA_RESOURCE_QDSS,
  CLOCK_NPA_RESOURCE_PCNOC,
  CLOCK_NPA_RESOURCE_SNOC,
  CLOCK_NPA_RESOURCE_BIMC,
  CLOCK_NPA_RESOURCE_OCMEM,
  CLOCK_NPA_NODE_NAME_CXO,
  CLOCK_NPA_RESOURCE_AGGR0,
  CLOCK_NPA_RESOURCE_AGGR1,
  CLOCK_NPA_RESOURCE_AGGR2,
  CLOCK_NPA_NODE_NAME_CPU
};

static const uint32 ClockQDSP6BISTLevel[] =
              {115200, 144000, 230400, 288000, 384000, 480000, 576000,
			   614400,  691200 };

/* =========================================================================
      Functions
==========================================================================*/

/* =========================================================================
**  Function : ClockStub_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult ClockStub_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32  i;
  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  for (i = 0; i < ARRAY_SIZE(ClockNPANodeName); i++)
  {
    npa_stub_resource(ClockNPANodeName[i]);
  }
  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END ClockStub_InitImage */


/* =========================================================================
**  Function : Clock_DetectQDSP6Config
** =========================================================================*/
/**
  Detects current configuration of QDSP6 clock
 
  This function is invoked at driver initialization to detect the current
  QDSP6 configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_DetectQDSP6Config
(
  ClockDrvCtxt    *pDrvCtxt
)
{
  uint32 nCfg;
  uint32 nDiv2x;
  uint32 nSrcIdx;
  ClockCPUConfigType *pCfg;
  ClockCPUConfigType **pClockCfg;
  HAL_clk_ClockMuxConfigType MuxCfg; 
  ClockNodeType *pClock;
  boolean bPerfFound = FALSE;

  /*-----------------------------------------------------------------------*/
  /* Get proper clock and configuration data                               */
  /*-----------------------------------------------------------------------*/

  pClock    = Clock_LPASSCtxt.QDSP6Ctxt.pQDSP6Clock;
  pClockCfg = &Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig;

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);  

  /*-----------------------------------------------------------------------*/
  /* Get the current hardware configuration.                               */
  /*-----------------------------------------------------------------------*/

  HAL_clk_DetectClockMuxConfig(pClock->pDomain->HALHandle, &MuxCfg);
  
  /*
   * If we got back divider of 0, set to 2 for comparison.
   */
  if ((MuxCfg.nDiv2x == 1) || (MuxCfg.nDiv2x == 0))
  {
    MuxCfg.nDiv2x = 2;
  }

  /*-----------------------------------------------------------------------*/
  /* Go through config array to find a matching configuration.             */
  /*-----------------------------------------------------------------------*/

  for (nCfg = 0; nCfg < Clock_LPASSCtxt.pImageConfig->nTotalSupportedConfigs; nCfg++)
  {
    pCfg = &Clock_LPASSCtxt.pImageConfig->pCPUConfig[nCfg];
    nDiv2x = pCfg->Mux.HALConfig.nDiv2x;
    if ((nDiv2x == 1) || (nDiv2x == 0))
    {    
        nDiv2x = 2;
    }

    if (MuxCfg.eSource  == pCfg->Mux.HALConfig.eSource &&
        MuxCfg.nDiv2x == nDiv2x)
    {
      bPerfFound = TRUE;
      break;
    }
  }

  if (FALSE == bPerfFound)
  {
    DALCLOCK_FREE(pDrvCtxt);  
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the clock node with the matched configuration.                 */
  /*-----------------------------------------------------------------------*/

  *pClockCfg = pCfg;

  pClock->pDomain->pActiveMuxConfig = &pCfg->Mux;

  nSrcIdx = pDrvCtxt->anSourceIndex[pCfg->Mux.HALConfig.eSource];

  if (nSrcIdx != 0xFF)
  {
    pClock->pDomain->pSource = &pDrvCtxt->aSources[nSrcIdx];
  }

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);  

  return DAL_SUCCESS;

}/* END Clock_DetectQDSP6Config */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult eRes;
  uint32 nMaxPL, nMaxQDSP6Config, nIdx, nSupportedIdx;
  ClockPropertyValueType PropVal = NULL;
  ClockImageConfigType *pImageCfg = NULL;
  ClockSourceInitType* pInitSources = NULL;
  DalDeviceHandle *hPlatInfo = NULL;
  DalPlatformInfoPlatformInfoType PlatformInfo;
  
  /*-----------------------------------------------------------------------*/
  /* Get the CPU Configurations.                                           */
  /*-----------------------------------------------------------------------*/

  if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfig", &PropVal))
  {
    return(DAL_ERROR);
  }

  pImageCfg = (ClockImageConfigType*)PropVal;
  nSupportedIdx = 0;

  if (pImageCfg != NULL)
  {
    /* 
     * Get the number of supported configs on this chipset.
     */
    for (nIdx = 0; nIdx < pImageCfg->pCPUPerfConfig->nTotalConfigs; nIdx++)
    {
      if (Clock_IsBSPSupported(&pImageCfg->pCPUConfig[nIdx].Mux.HWVersion))
      {
        nSupportedIdx++;
      }
    }

    if (nSupportedIdx == 0)
    {
      return(DAL_ERROR);
    }

    Clock_LPASSCtxt.pImageConfig = &Clock_ImageConfig;

    eRes = DALSYS_Malloc(sizeof(ClockCPUConfigType)*nSupportedIdx, (void **)&Clock_LPASSCtxt.pImageConfig->pCPUConfig);

    if (eRes != DAL_SUCCESS) 
    {
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to allocate pImageConfig buffer for Q6 clock controller.");
      return eRes;
    }
    memset(Clock_LPASSCtxt.pImageConfig->pCPUConfig, 0, sizeof(ClockCPUConfigType)*nSupportedIdx);

    nSupportedIdx = 0;

    /* 
     * Add each supported configuration into the new CPU configuration array.
     */
    for (nIdx=0; nIdx < pImageCfg->pCPUPerfConfig->nTotalConfigs; nIdx++)
    {
      if (Clock_IsBSPSupported(&pImageCfg->pCPUConfig[nIdx].Mux.HWVersion))
      {
        Clock_LPASSCtxt.pImageConfig->pCPUConfig[nSupportedIdx] = pImageCfg->pCPUConfig[nIdx];
        nSupportedIdx++;
      }
    }
  }
  else
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get QDSP6 Image Configuration information from the BSP!.");
    return(DAL_ERROR);
  }

  /* 
   * Fill in remaining fields from the BSP here.
   */
  Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig = pImageCfg->pCPUPerfConfig;
  Clock_LPASSCtxt.pImageConfig->nTotalSupportedConfigs  = nSupportedIdx;
  Clock_LPASSCtxt.pImageConfig->bGlobalLDOEnable = pImageCfg->bGlobalLDOEnable;
  Clock_LPASSCtxt.pImageConfig->bIsFusionMDM = pImageCfg->bIsFusionMDM;
  Clock_LPASSCtxt.pImageConfig->pIntermediateCPUConfig = pImageCfg->pIntermediateCPUConfig;
  
    /*-----------------------------------------------------------------------*/
  /* Initialize the Q6SS HWIO Base address.                                */
  /*-----------------------------------------------------------------------*/
  if(Clock_nHWIOBaseLPASS == 0)
  {
    Clock_MapHWIORegion(
      LPASS_BASE_PHYS, LPASS_BASE_SIZE, &Clock_nHWIOBaseLPASS);
	  
    /*
    * If we are unable to map a virtual address, assume the physical one.
    */
    if(Clock_nHWIOBaseLPASS == NULL)
    {
      Clock_nHWIOBaseLPASS = LPASS_BASE_PHYS;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Get QDSP6 core/cpu clock ID.                                          */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, "lpass_q6core",
      (ClockIdType *)&Clock_LPASSCtxt.QDSP6Ctxt.pQDSP6Clock);

  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get Q6LPASS core clock ID.");
    return eRes;

  }

  pDrvCtxt->pImageCtxt = &Clock_LPASSCtxt;

  /*-----------------------------------------------------------------------*/
  /* Detect initial QDSP6 core clock frequency configuration.              */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectQDSP6Config(pDrvCtxt);

  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect Q6LPASS core clock configuration.");
    return eRes;
  }

  if (DAL_SUCCESS != DAL_DeviceAttach(DALDEVICEID_PLATFORMINFO, &hPlatInfo))
  {
    return(DAL_ERROR);
  }

  if(DAL_SUCCESS != DalPlatformInfo_GetPlatformInfo(hPlatInfo,  &PlatformInfo))
  {
    return(DAL_ERROR);
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6PLL if applicable to this chipset.                   */
  /*-----------------------------------------------------------------------*/

  if(DAL_SUCCESS == Clock_GetPropertyValue("ClockSourcesToInit", &PropVal))
  {
    pInitSources = (ClockSourceInitType*)PropVal;

    if (pInitSources != NULL)
    {
      nIdx = 0;
      while (pInitSources[nIdx].eSource != HAL_CLK_SOURCE_NULL)
      {
        Clock_InitSource(pDrvCtxt,
                         pInitSources[nIdx].eSource,
                         pInitSources[nIdx].nFreqHz);
        nIdx++;
      }
    }
  }

  /* 
    There is no LDO in Sahi in LPASS. 
  */
#if 0  
  /*-----------------------------------------------------------------------*/
  /* Initialize LDO Voltage control for supported chipsets.                */
  /*-----------------------------------------------------------------------*/

  Clock_InitLDOVoltage(pDrvCtxt);
#endif  

  /*-----------------------------------------------------------------------*/
  /* Ensure that the Q6LPASS clock/domain/source reference counts are '1'. */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, 
    (ClockIdType)Clock_LPASSCtxt.QDSP6Ctxt.pQDSP6Clock);


  /*-----------------------------------------------------------------------*/
  /* Run QDSP6 at max performance level - if DCVS is enable then we'll     */
  /* go to lower performance level later.                                  */
  /*-----------------------------------------------------------------------*/

  nMaxPL = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->nInitPerfLevel;

  /* 
   * Configure the PLL to one less than max for low power initialization.
   */

  nMaxQDSP6Config = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->panPerfLevel[nMaxPL];
 

  if (Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig != 
        &Clock_LPASSCtxt.pImageConfig->pCPUConfig[nMaxQDSP6Config])
  {
    Clock_SetQDSP6Config(
      pDrvCtxt, &Clock_LPASSCtxt.pImageConfig->pCPUConfig[nMaxQDSP6Config]);
  }

  /*-----------------------------------------------------------------------*/
  /* GPLL0 Initialization for Stand alone Testing.                         */
  /*-----------------------------------------------------------------------*/
   Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_GPLL0, NULL);
	
  /*-----------------------------------------------------------------------*/
  /* Initialize the DCVS module.                                           */
  /*-----------------------------------------------------------------------*/

  Clock_InitDCVS(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize the XO module.                                             */
  /*-----------------------------------------------------------------------*/

  Clock_InitXO(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Now that NPA is initialized, allow Q6 scaling by the power manager.   */
  /*-----------------------------------------------------------------------*/
/* This feature is added for Bring up, will be removed later */
#ifndef FEATURE_DISABLE_DCVS_BRINGUP  
  Clock_EnableDCVS(pDrvCtxt);
#endif  

  /* There is no LDO in Sahi/msm8952 */
#if 0  
  /*-----------------------------------------------------------------------*/
  /* Initialize the NPA eLDO enable/disable functionality.                 */
  /*-----------------------------------------------------------------------*/
  Clock_InitVdd(pDrvCtxt);
#endif  
  
  /*-----------------------------------------------------------------------*/
  /* Run the NPA BIST if enabled.                                          */
  /*-----------------------------------------------------------------------*/
 
  if (Clock_LPASSCtxt.bNPABISTEnabled)
  {
    if (Clock_LPASSCtxt.nNPABISTLogSize == 0)
    {
      Clock_LPASSCtxt.nNPABISTLogSize = CLOCK_NPA_BIST_DEFAULT_LOG_SIZE;
    }  
	
    Clock_NPA_BIST(pDrvCtxt);
  }
  
  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_ProcessorSleep
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorSleep
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
)
{

  return DAL_SUCCESS;

} /* END Clock_ProcessorSleep */


/* =========================================================================
**  Function : Clock_ProcessorRestore
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorRestore
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
) 
{

  /*
   * Nothing to do here. HW_VOTE does SPMCTL override handling.
  */

  return DAL_SUCCESS;

} /* END Clock_ProcessorRestore */


/* =========================================================================
**  Function : Clock_SetCPUMaxFrequencyAtCurrentVoltage
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_SetCPUMaxFrequencyAtCurrentVoltage
(
  ClockDrvCtxt *pDrvCtxt,
  boolean      bRequired,
  uint32       *pnResultFreqHz
)
{
  DALResult            eRes;
  uint32               nMinPL, nMaxPL, nPL, nCfg;
  ClockVRegLevelType   eVReg;
  ClockCPUConfigType *pQDSP6Config = Clock_LPASSCtxt.pImageConfig->pCPUConfig;
  HAL_clk_SourceType eSource;

  nMinPL = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->nMinPerfLevel;
  nMaxPL = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->nMaxPerfLevel;

  if (TRUE == bRequired)
  {
    /*
    * Find the highest frequency configuration supported by the clock driver's
    * current vote (suppressible and non-suppressible) on the power rail.
    */

    eVReg = Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig->Mux.eVRegLevel;
    eSource = Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig->Mux.HALConfig.eSource;

    /*
     * Start with the max PL and move down from there to find a match 
     * with the voltage and source. 
    */
    for (nPL = nMaxPL; nPL > nMinPL; nPL--)
    {
      nCfg = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->panPerfLevel[nPL];

      if ((pQDSP6Config[nCfg].Mux.eVRegLevel == eVReg) &&
          (pQDSP6Config[nCfg].Mux.HALConfig.eSource == eSource))
      {
        break;
      }
    }

    /*
     * No frequency was found.  Return the current frequency.
     */
    if (nPL == nMinPL)
    {
      if(pnResultFreqHz != NULL)
      {
        *pnResultFreqHz = Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig->Mux.nFreqHz;
      }
      return(DAL_SUCCESS);
    }
    
    nCfg = Clock_LPASSCtxt.pImageConfig->pCPUPerfConfig->panPerfLevel[nPL];

    /*
     * Configure the Q6 to the desired performance level if different
     * from current configuration.
     */
    if (Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig != &pQDSP6Config[nCfg])
    {
      Clock_SetQDSP6Config(pDrvCtxt, &pQDSP6Config[nCfg]);

      /*
       * Overwrite the active state of the NPA node.
       */
      eRes = Clock_NPACPUResourceOverwriteActiveState(
        (npa_resource_state)pQDSP6Config[nCfg].Mux.nFreqHz/1000);

      if (eRes != DAL_SUCCESS)
      {
        if (pnResultFreqHz != NULL)
        {
          *pnResultFreqHz = Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig->Mux.nFreqHz;
        }

        return DAL_ERROR_INTERNAL;
      }
    }

    if (pnResultFreqHz != NULL)
    {
      *pnResultFreqHz = Clock_LPASSCtxt.QDSP6Ctxt.pCPUConfig->Mux.nFreqHz;
    }
    
    return DAL_SUCCESS;
  }
  else
  {
    /*
     * The max CPU at current voltage level is no longer required.  We can 
     * request that NPA re-aggregate the CPU requests. 
     */
    if (Clock_LPASSCtxt.QDSP6Ctxt.NPAImpulseHandle != NULL)
    {
      npa_issue_impulse_request( Clock_LPASSCtxt.QDSP6Ctxt.NPAImpulseHandle );
    }
  }

  return DAL_SUCCESS;

} /* END Clock_SetCPUMaxFrequencyAtCurrentVoltage */


/* =========================================================================
**  Function : Clock_AdjustSourceFrequency
** =========================================================================*/
/*
  See DDIClock.h
*/
DALResult Clock_AdjustSourceFrequency
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockSourceType eSource,
   int32           nDeltaLAlpha
)
{
  return(DAL_ERROR);

} /* Clock_AdjustSourceFrequency */


DALResult Clock_SelectClockSource
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockIdType     nClock,
   ClockSourceType eSource
)
{

  if(eSource == CLOCK_SOURCE_PRIMARY)
  {
    return(DAL_SUCCESS);
  }
  return(DAL_ERROR);

} /* Clock_SelectClockSource */


/* =========================================================================
**  Function : Clock_GetImageCtxt
** =========================================================================*/
/*
  See ClockLPASS.h
*/

ClockLPASSCtxtType* Clock_GetImageCtxt(void)
{
  return(&Clock_LPASSCtxt);
}

/*
 * Unused APIs are stubbed out here.
 */

DALResult Clock_ImageBIST
(
  ClockDrvCtxt  *pDrvCtxt,
  boolean       *bBISTPassed,
  uint32        *nFailedTests
)
{
  return(DAL_ERROR);
}

DALResult Clock_LoadNV
(
  ClockDrvCtxt  *pDrvCtxt
)
{
  return(DAL_ERROR_NOT_SUPPORTED);
}

DALResult Clock_EnableAVS
(
  ClockDrvCtxt  *pDrvCtxt
)
{
  return(DAL_ERROR_NOT_SUPPORTED);
}


/* =========================================================================
**  Function : Clock_NPA_BIST
** =========================================================================*/
/**
  See ClockLPASS.h

*/

void Clock_NPA_BIST(ClockDrvCtxt *pDrvCtxt)
{
  uint32               i, nFreqHz_NPA_state, nFreqHz_Calc;
  npa_query_type       qdsp6_state, test_state;
  npa_client_handle    qdsp6_h;
  DALResult            eResult;
  ClockLPASSCtxtType   *pLPASSCtxt;
  uint32               anSourceIndex;
  
  pLPASSCtxt = (ClockLPASSCtxtType *)pDrvCtxt->pImageCtxt; 
  
  /*
  * Increment the reference count on the LPAPLL0 to ensure Q6 clock 
  * switching does not disable the LPAPLL0 that the bus is currently on. 
  */
  anSourceIndex = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_LPAPLL0];
  pDrvCtxt->aSources[anSourceIndex].nReferenceCountSuppressible++;
  
  /*-----------------------------------------------------------------------*/
  /* Initialize the NPA BIST log.                                          */
  /*-----------------------------------------------------------------------*/

  ULogFront_RealTimeInit (&pLPASSCtxt->hClockNPABISTLog, "Clock NPA BIST Log",
                          pLPASSCtxt->nNPABISTLogSize,
                          ULOG_MEMORY_LOCAL, ULOG_LOCK_OS);
  ULogCore_HeaderSet     (pLPASSCtxt->hClockNPABISTLog,
                          "Content-Type: text/tagged-log-1.0;");

  /*-----------------------------------------------------------------------*/
  /* Test clocks through NPA                                               */
  /*-----------------------------------------------------------------------*/
   
  qdsp6_h = npa_create_sync_client(CLOCK_NPA_NODE_NAME_CPU, 
                                  "QDSP6", NPA_CLIENT_SUPPRESSIBLE);
  
  /* Save current QDSP6 processors state */
 
  npa_query_by_name(CLOCK_NPA_NODE_NAME_CPU, NPA_QUERY_CURRENT_STATE, 
                    &qdsp6_state);
  
  for(i = 0; i < ARRAY_SIZE(ClockQDSP6BISTLevel); i++)
  {
    
    ULOG_RT_PRINTF_1 (pLPASSCtxt->hClockNPABISTLog, 
                      "Testing Q6 clock for frequency: %d Hz", 
                      ClockQDSP6BISTLevel[i]*1000);

    npa_issue_scalar_request( qdsp6_h, ClockQDSP6BISTLevel[i] );
    npa_query_by_name(CLOCK_NPA_NODE_NAME_CPU, NPA_QUERY_CURRENT_STATE, 
                      &test_state);
					  
    nFreqHz_NPA_state = NPA_TO_HZ(test_state.data.state);
    ULOG_RT_PRINTF_1 (pLPASSCtxt->hClockNPABISTLog, 
                          "Q6 resource state frequency: %d Hz", 
                          nFreqHz_NPA_state);

    eResult = Clock_CalcClockFrequency(pDrvCtxt, 
                               (ClockIdType)pLPASSCtxt->QDSP6Ctxt.pQDSP6Clock, 
                               &nFreqHz_Calc);
    
    if (DAL_SUCCESS == eResult)
    {
      if (ABS_DIFF(nFreqHz_NPA_state, nFreqHz_Calc) > 
          (CLOCK_NPA_BIST_ERROR_MARGIN_PERCENT/100)*nFreqHz_Calc)
      {
        ULOG_RT_PRINTF_1 (pLPASSCtxt->hClockNPABISTLog, 
                          "Q6 ring oscillator calculated frequency: %d Hz", 
                          nFreqHz_Calc);
      }
      else
      {
        ULOG_RT_PRINTF_0 (pLPASSCtxt->hClockNPABISTLog, 
                          "Q6 CPU clock set frequency: SUCCESS");
      }
    }
    else
    {
      ULOG_RT_PRINTF_0 (pLPASSCtxt->hClockNPABISTLog, 
                        "Clock_CalcClockFrequency failed for Q6 clock");
    }
  }
  
  /* Restore original QDSP6 processors*/
  npa_issue_scalar_request( qdsp6_h, qdsp6_state.data.state );  
  
  /*
  * Decrement the reference count on the LPAPLL0 as we incremented
    it to avoid the LPAPLL0 getting disabled while Q6 clock is switched. 
  */
  anSourceIndex = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_LPAPLL0];
  pDrvCtxt->aSources[anSourceIndex].nReferenceCountSuppressible--;
  
  return;

}

