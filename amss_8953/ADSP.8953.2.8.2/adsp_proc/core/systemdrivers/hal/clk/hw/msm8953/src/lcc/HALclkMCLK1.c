/*
==============================================================================

FILE:         HALclkMCLK1.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MCLK1 clocks.

   List of clock domains:
   -HAL_clk_mLPASSMCLK1ClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/core.adsp/2.6.6/systemdrivers/hal/clk/hw/msm8953/src/lcc/HALclkMCLK1.c#1 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
11/28/2014            Auto-generated.

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mLPASSClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mMCLK1ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMCLK1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "audio_wrapper_mclk1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(LPASS_AUDIO_WRAPPER_MCLK1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_LPASS_TEST_AUDIO_WRAPPER_MCLK1_CLK
  },
};


/*
 * HAL_clk_mLPASSMCLK1ClkDomain
 *
 * MCLK1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mLPASSMCLK1ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(LPASS_MCLK1_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMCLK1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMCLK1ClkDomainClks)/sizeof(HAL_clk_mMCLK1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mLPASSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

