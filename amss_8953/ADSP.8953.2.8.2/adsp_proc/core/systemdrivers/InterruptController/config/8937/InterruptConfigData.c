/*==============================================================================

  D A L I N T E R R U P T   C O N T R O L L E R    

DESCRIPTION
 This file Contains the Interrupt configuration data for all the interrupts for 
 this platform.

REFERENCES

        Copyright (c) 2016 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.adsp/2.6.6/systemdrivers/InterruptController/config/8937/InterruptConfigData.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/28/16   shm     Added support for 8940
===========================================================================*/

#include "DALReg.h"
#include "DALDeviceId.h"
#include "DDIInterruptController.h"
#include "DALInterruptControllerConfig.h"



/*=========================================================================

                           DATA DEFINITIONS

===========================================================================*/

/*
 * Interrupt_Configs
 * BSP data for this target's interrupts.
 * The data layout is the following :
 * {  l2VIC interrupt vector number , Interrupt name }
 *
 */
static InterruptConfigType InterruptConfigs[] = 
{
  {0, "crpc_l2vic_wake_irq"},
  {1, "qdsp6_isdb_irq"},
  {2, "q6ss_qtmr_irq[0]"},
  {3, "q6ss_qtmr_irq[1]"},
  {4, "q6ss_qtmr_irq[2]"},
  {6, "mss_to_lpass_irq[1]"},
  {7, "rpm_ipc[8]"},
  {8, "APCS_adspIPCInterrupt[1]"},
  {9, "mss_to_lpass_irq[2]"},
  {10, "o_wcss_lpass_bt_tx_intr"},
  {11, "lpass_slimbus_core_ee0_irq"},
  {12, "lpass_slimbus_bam_ee0_irq"},
  {13, "rpm_ipc[10]"},
  {14, "rpm_ipc[11]"},
  {15, "o_wcss_lpass_mbox_intr"},
  {16, "o_wcss_lpass_fm_intr"},
  {17, "o_wcss_lpass_bt_rx_intr"},
  {18, "APCS_adspIPCInterrupt[0]"},
  {19, "audio_out2_irq"},
  {20, "bus_irq"},
  {21, "o_wcss_lpass_smd_med"},
  {22, "mss_to_lpass_irq[0]"},
  {23, "dm_irq"},
  {24, "dir_conn_irq_lpa_dsp[0]"},
  {25, "dir_conn_irq_lpa_dsp[1]"},
  {26, "dir_conn_irq_lpa_dsp[2]"},
  {27, "dir_conn_irq_lpa_dsp[3]"},
  {28, "dir_conn_irq_lpa_dsp[4]"},
  {29, "dir_conn_irq_lpa_dsp[5]"},
  {30, "rpm_ipc[9]"},
  {31, "q6ss_cti_irq"},
  {32, "q6ss_wdog_irq"},
  {33, "APCS_adspIPCInterrupt[2]"},
  {34, "mss_to_lpass_irq[3]"},
  {35, "o_wcss_lpass_smd_hi"},
  {36, "vfr"},
  {37, "adsp_ext_vfr_irq"},
  {38, "summary_irq_lpa_dsp"},
  {40, "vfr2_mss"},
  {46, "APCS_adspIPCInterrupt[3]"},
  {48, "ee6_lpass_spmi_periph_irq"},
  {49, "channel6_lpass_trans_done_irq"},
  {50, "avtimer_int_0"},
  {51, "vfr_irq_mux_out[0]"},
  {52, "o_wcss_lpass_smd_lo"},
  {53, "resampler_irq[1]"},
  {54, "usb_hs_irq"},
  {55, "bam_irq[1]"},
  {56, "bam_irq[1]"},
  {57, "vfe_0_sof_stb"},
  {58, "vfe_0_eof_stb"},
  {60, "ao_pen_irq"},
  {61, "spdm_realtime_irq[2]"},
  {62, "summary_irq_sensors"},
  {63, "q6ss_avs_sw_done_irq"},
  {64, "blsp1_peripheral_irq[2]"},
  {65, "blsp1_peripheral_irq[3]"},
  {66, "blsp1_peripheral_irq[4]"},
  {67, "blsp1_peripheral_irq[5]"},
  {70, "dir_conn_irq_sensors[0]"},
  {71, "dir_conn_irq_sensors[1]"},
  {72, "dir_conn_irq_sensors[2]"},
  {73, "dir_conn_irq_sensors[3]"},
  {74, "dir_conn_irq_sensors[4]"},
  {75, "dir_conn_irq_sensors[5]"},
  {76, "dir_conn_irq_sensors[6]"},
  {77, "dir_conn_irq_sensors[7]"},
  {80, "blsp2_peripheral_irq[2]"},
  {81, "blsp2_peripheral_irq[3]"},
  {82, "blsp2_peripheral_irq[4]"},
  {83, "blsp2_peripheral_irq[5]"},
  {86, "lpass_slimbus1_core_ee0_irq"},
  {90, "o_timeout_slave_lpass_summary_intr"},
  {91, "vfe_1_sof_stb"},
  {92, "vfe_1_eof_stb"},
  {94, "lpass_hdmitx_interrupt"},
  {96, "crypto_core_irq[0]"},
  {97, "crypto_bam_irq[0]"},
  {102, "dir_conn_irq_sensors[9]"},
  {103, "dir_conn_irq_sensors[8]"},
  {104, "phss_uart_lpass_int[0]"},
  {105, "phss_uart_lpass_int[1]"},
  {106, "phss_qup_lpass_int[0]"},
  {107, "phss_qup_lpass_int[1]"},
  {112, "lpass_core_qos_q6_interrupt"},
  {113, "lpass_qos_q6_interrupt"},
  {114, "APCS_adspVmmIPCInterrupt[0]"},
  {115, "APCS_adspVmmIPCInterrupt[1]"},
  {116, "APCS_adspVmmIPCInterrupt[2]"},
  {117, "APCS_adspVmmIPCInterrupt[3]"},
  {121, "lpass_hdmirx_interrupt"},
  {122, "lpass_spdif_tx_intr_q6"},
  {123, "sif_aud_dec_out_irq"},
  {124, "vfr_irq_mux_out[1]"},
  {125, "camss_irq0"},
  {126, "camss_irq1"},
  {127, "camss_irq2"},
  {128, "camss_irq3"},
  {129, "camss_irq4"},
  {131, "camss_irq6"},
  {132, "camss_irq7"},
  {133, "camss_irq8"},
  {134, "camss_irq9"},
  {135, "camss_irq10"},
  {138, "camss_vbif_vfe_0_irq"},
  {139, "camss_vbif_vfe_1_irq"},
  {140, "camss_vbif_cpp_irq"},
  {141, "camss_vbif_jpeg_irq"},
  {144, "camss_dsp_streaming_0_irq"},
  {145, "camss_dsp_streaming_1_irq"},
  {146, "lcc_spkr_ext_clk_detect_inactive_irq"},
  {147, "lcc_pri_ext_clk_detect_inactive_irq"},
  {148, "lcc_sec_ext_clk_detect_inactive_irq"},
  {149, "lcc_ter_ext_clk_detect_inactive_irq"},
  {150, "lcc_quad_ext_clk_detect_inactive_irq"},
  {151, "lcc_qui_ext_clk_detect_inactive_irq"},
  {152, "lcc_sen_ext_clk_detect_inactive_irq"},
  {INVALID_INTERRUPT , ""}
};


/*
 * InterruptPlatformDataType 
 * This is used by the Interrupt controller platform to query platform specific data. 
 */
InterruptPlatformDataType  pInterruptControllerConfigData[] =
{
  {
    InterruptConfigs,           /* pIRQConfigs */
    153
  }
};




  
