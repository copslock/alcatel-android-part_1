/*=============================================================================

  FILE:     i2c_adsp_8976.c

  OVERVIEW: This file has the devices for 8952 platform for adsp. 
 
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary 

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.adsp/2.6.6/buses/i2c/config/i2c_adsp_8976.c#1 $
  $DateTime: 2016/05/04 01:42:14 $$Author: pwbldsvc $

  When     Who    What, where, why
  -------- ---    -----------------------------------------------------------
  01/09/15  SG     Created.

  ===========================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cDriverTypes.h"
#include "I2cPlatSvc.h"
#include "I2cDevice.h"

#define I2C_NUM_PLATFORM_DEVICES         (1)


/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

const uint32              i2cDeviceNum = I2C_NUM_PLATFORM_DEVICES;

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/


#ifdef BUILD_FOR_ISLAND
#define ATTRIBUTE_ISLAND_CODE __attribute__((section("RX.island")))
#define ATTRIBUTE_ISLAND_CONST __attribute__((section("RO.island")))
#define ATTRIBUTE_ISLAND_DATA __attribute__((section("RW.island")))
#else
#define ATTRIBUTE_ISLAND_CODE /* empty */
#define ATTRIBUTE_ISLAND_CONST /* empty */
#define ATTRIBUTE_ISLAND_DATA /* empty */
#endif


/*-------------------------------------------------------------------------
 * Global Data Definitions
 * ----------------------------------------------------------------------*/
I2cPlat_PropertyType i2cPlatPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
//  I2CPLAT_PROPERTY_INIT(0x2001c023, 0x2001c033, 2, "BLSP1_BLSP", 0x35000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP1_APPS_CLK, "/clk/pcnoc"), //GPIO: 02 & 03 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c063, 0x2001c073, 2, "BLSP1_BLSP", 0x36000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP2_APPS_CLK, "/clk/pcnoc"), //GPIO: 06 & 07 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c0a3, 0x2001c0b3, 2, "BLSP1_BLSP", 0x37000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP3_APPS_CLK, "/clk/pcnoc"), //GPIO: 10 & 11 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//	I2CPLAT_PROPERTY_INIT(0x2001c0e3, 0x2001c0f3, 2, "BLSP1_BLSP", 0x38000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP4_APPS_CLK, "/clk/pcnoc"), //GPIO: 14 & 15 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma

    I2CPLAT_PROPERTY_INIT(0x2001c883, 0x2001c893, 2, "BLSP2_BLSP", 0x35000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP1_APPS_CLK, "/clk/pcnoc"), //GPIO: 136 & 137 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c163, 0x2001c173, 2, "BLSP2_BLSP", 0x36000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP2_APPS_CLK, "/clk/pcnoc"), //GPIO: 22 & 23 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c6e3, 0x2001c6f3, 2, "BLSP2_BLSP", 0x37000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP3_APPS_CLK, "/clk/pcnoc"), //GPIO: 110 & 111 -> Func: 4, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c123, 0x2001c133, 2, "BLSP2_BLSP", 0x38000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP4_APPS_CLK, "/clk/pcnoc"), //GPIO: 18 & 19 -> Func: 1, Dir: O/p, PullType: Up, Drive: 2ma
};

I2cDev_PropertyType i2cDevPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{

//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//	I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
 
    I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
  
};

I2cDrv_DriverProperty i2cDrvPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_1, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_2, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_3, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_4, FALSE, 0),
   
   I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_5, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_6, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_7, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_8, FALSE, 0),

};

I2cDrv_DescType i2cDrvDescArray[I2C_NUM_PLATFORM_DEVICES] ATTRIBUTE_ISLAND_DATA ;