#ifndef __HWIO_LPASS_TCSR_8952_H__
#define __HWIO_LPASS_TCSR_8952_H__
/*
===========================================================================
*/
/**
  @file hwio_lpass_tcsr_8952.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8952 (Sahi) [sahi_v1.0_p3q3r142_MTO_tcsr_blk_update]
 
  This file contains HWIO register definitions for the following modules:
    LPASS_LPASS_TCSR

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.adsp/2.6.6/power/dangergen/src/core/inc/hwio_lpass_tcsr.h#1 $
  $DateTime: 2016/05/04 01:42:14 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: LPASS_LPASS_TCSR
 *--------------------------------------------------------------------------*/

#define LPASS_LPASS_TCSR_REG_BASE                                                           (LPASS_BASE      + 0x00050000)
#define LPASS_LPASS_TCSR_REG_BASE_OFFS                                                      0x00050000

#define HWIO_LPASS_HW_VERSION_ADDR                                                          (LPASS_LPASS_TCSR_REG_BASE      + 0x00000000)
#define HWIO_LPASS_HW_VERSION_OFFS                                                          (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000000)
#define HWIO_LPASS_HW_VERSION_RMSK                                                          0xffffffff
#define HWIO_LPASS_HW_VERSION_POR                                                           0x30090001
#define HWIO_LPASS_HW_VERSION_POR_RMSK                                                      0xffffffff
#define HWIO_LPASS_HW_VERSION_IN          \
        in_dword_masked(HWIO_LPASS_HW_VERSION_ADDR, HWIO_LPASS_HW_VERSION_RMSK)
#define HWIO_LPASS_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_LPASS_HW_VERSION_ADDR, m)
#define HWIO_LPASS_HW_VERSION_MAJOR_BMSK                                                    0xf0000000
#define HWIO_LPASS_HW_VERSION_MAJOR_SHFT                                                          0x1c
#define HWIO_LPASS_HW_VERSION_MINOR_BMSK                                                     0xfff0000
#define HWIO_LPASS_HW_VERSION_MINOR_SHFT                                                          0x10
#define HWIO_LPASS_HW_VERSION_STEP_BMSK                                                         0xffff
#define HWIO_LPASS_HW_VERSION_STEP_SHFT                                                            0x0

#define HWIO_LPASS_TCSR_DEBUG_CTL_ADDR                                                      (LPASS_LPASS_TCSR_REG_BASE      + 0x00000004)
#define HWIO_LPASS_TCSR_DEBUG_CTL_OFFS                                                      (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000004)
#define HWIO_LPASS_TCSR_DEBUG_CTL_RMSK                                                         0x11fff
#define HWIO_LPASS_TCSR_DEBUG_CTL_POR                                                       0x00000000
#define HWIO_LPASS_TCSR_DEBUG_CTL_POR_RMSK                                                  0xffffffff
#define HWIO_LPASS_TCSR_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_LPASS_TCSR_DEBUG_CTL_ADDR, HWIO_LPASS_TCSR_DEBUG_CTL_RMSK)
#define HWIO_LPASS_TCSR_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_TCSR_DEBUG_CTL_ADDR, m)
#define HWIO_LPASS_TCSR_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_TCSR_DEBUG_CTL_ADDR,v)
#define HWIO_LPASS_TCSR_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_TCSR_DEBUG_CTL_ADDR,m,v,HWIO_LPASS_TCSR_DEBUG_CTL_IN)
#define HWIO_LPASS_TCSR_DEBUG_CTL_LPASS_DEBUG_EN_BMSK                                          0x10000
#define HWIO_LPASS_TCSR_DEBUG_CTL_LPASS_DEBUG_EN_SHFT                                             0x10
#define HWIO_LPASS_TCSR_DEBUG_CTL_SEL_ADDR_BMSK                                                 0x1f00
#define HWIO_LPASS_TCSR_DEBUG_CTL_SEL_ADDR_SHFT                                                    0x8
#define HWIO_LPASS_TCSR_DEBUG_CTL_SEL_BMSK                                                        0xff
#define HWIO_LPASS_TCSR_DEBUG_CTL_SEL_SHFT                                                         0x0

#define HWIO_LPASS_TCSR_MAXLIM_CTL_ADDR                                                     (LPASS_LPASS_TCSR_REG_BASE      + 0x00000008)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_OFFS                                                     (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000008)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_RMSK                                                           0xff
#define HWIO_LPASS_TCSR_MAXLIM_CTL_POR                                                      0x000000c4
#define HWIO_LPASS_TCSR_MAXLIM_CTL_POR_RMSK                                                 0xffffffff
#define HWIO_LPASS_TCSR_MAXLIM_CTL_IN          \
        in_dword_masked(HWIO_LPASS_TCSR_MAXLIM_CTL_ADDR, HWIO_LPASS_TCSR_MAXLIM_CTL_RMSK)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_TCSR_MAXLIM_CTL_ADDR, m)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_TCSR_MAXLIM_CTL_ADDR,v)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_TCSR_MAXLIM_CTL_ADDR,m,v,HWIO_LPASS_TCSR_MAXLIM_CTL_IN)
#define HWIO_LPASS_TCSR_MAXLIM_CTL_CPZ_BYPASS_BMSK                                                0x80
#define HWIO_LPASS_TCSR_MAXLIM_CTL_CPZ_BYPASS_SHFT                                                 0x7
#define HWIO_LPASS_TCSR_MAXLIM_CTL_BYPASS_BMSK                                                    0x40
#define HWIO_LPASS_TCSR_MAXLIM_CTL_BYPASS_SHFT                                                     0x6
#define HWIO_LPASS_TCSR_MAXLIM_CTL_MAX_WRITES_BMSK                                                0x3f
#define HWIO_LPASS_TCSR_MAXLIM_CTL_MAX_WRITES_SHFT                                                 0x0

#define HWIO_LPASS_AHBE_CFG_ADDR                                                            (LPASS_LPASS_TCSR_REG_BASE      + 0x00000100)
#define HWIO_LPASS_AHBE_CFG_OFFS                                                            (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000100)
#define HWIO_LPASS_AHBE_CFG_RMSK                                                                 0x111
#define HWIO_LPASS_AHBE_CFG_POR                                                             0x00000100
#define HWIO_LPASS_AHBE_CFG_POR_RMSK                                                        0xffffffff
#define HWIO_LPASS_AHBE_CFG_IN          \
        in_dword_masked(HWIO_LPASS_AHBE_CFG_ADDR, HWIO_LPASS_AHBE_CFG_RMSK)
#define HWIO_LPASS_AHBE_CFG_INM(m)      \
        in_dword_masked(HWIO_LPASS_AHBE_CFG_ADDR, m)
#define HWIO_LPASS_AHBE_CFG_OUT(v)      \
        out_dword(HWIO_LPASS_AHBE_CFG_ADDR,v)
#define HWIO_LPASS_AHBE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AHBE_CFG_ADDR,m,v,HWIO_LPASS_AHBE_CFG_IN)
#define HWIO_LPASS_AHBE_CFG_Q6SS_PRIORITY_BMSK                                                   0x100
#define HWIO_LPASS_AHBE_CFG_Q6SS_PRIORITY_SHFT                                                     0x8
#define HWIO_LPASS_AHBE_CFG_FABRIC_PRIORITY_BMSK                                                  0x10
#define HWIO_LPASS_AHBE_CFG_FABRIC_PRIORITY_SHFT                                                   0x4
#define HWIO_LPASS_AHBE_CFG_ROUND_ROBIN_EN_BMSK                                                    0x1
#define HWIO_LPASS_AHBE_CFG_ROUND_ROBIN_EN_SHFT                                                    0x0

#define HWIO_LPASS_AHBE_LOCK_CTL_ADDR                                                       (LPASS_LPASS_TCSR_REG_BASE      + 0x00000104)
#define HWIO_LPASS_AHBE_LOCK_CTL_OFFS                                                       (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000104)
#define HWIO_LPASS_AHBE_LOCK_CTL_RMSK                                                              0x3
#define HWIO_LPASS_AHBE_LOCK_CTL_POR                                                        0x00000000
#define HWIO_LPASS_AHBE_LOCK_CTL_POR_RMSK                                                   0xffffffff
#define HWIO_LPASS_AHBE_LOCK_CTL_IN          \
        in_dword_masked(HWIO_LPASS_AHBE_LOCK_CTL_ADDR, HWIO_LPASS_AHBE_LOCK_CTL_RMSK)
#define HWIO_LPASS_AHBE_LOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_AHBE_LOCK_CTL_ADDR, m)
#define HWIO_LPASS_AHBE_LOCK_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_AHBE_LOCK_CTL_ADDR,v)
#define HWIO_LPASS_AHBE_LOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AHBE_LOCK_CTL_ADDR,m,v,HWIO_LPASS_AHBE_LOCK_CTL_IN)
#define HWIO_LPASS_AHBE_LOCK_CTL_Q6SS_LOCK_EN_BMSK                                                 0x2
#define HWIO_LPASS_AHBE_LOCK_CTL_Q6SS_LOCK_EN_SHFT                                                 0x1
#define HWIO_LPASS_AHBE_LOCK_CTL_FABRIC_LOCK_EN_BMSK                                               0x1
#define HWIO_LPASS_AHBE_LOCK_CTL_FABRIC_LOCK_EN_SHFT                                               0x0

#define HWIO_LPASS_AHBE_STATUS_ADDR                                                         (LPASS_LPASS_TCSR_REG_BASE      + 0x00000108)
#define HWIO_LPASS_AHBE_STATUS_OFFS                                                         (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000108)
#define HWIO_LPASS_AHBE_STATUS_RMSK                                                                0x7
#define HWIO_LPASS_AHBE_STATUS_POR                                                          0x00000000
#define HWIO_LPASS_AHBE_STATUS_POR_RMSK                                                     0xffffffff
#define HWIO_LPASS_AHBE_STATUS_IN          \
        in_dword_masked(HWIO_LPASS_AHBE_STATUS_ADDR, HWIO_LPASS_AHBE_STATUS_RMSK)
#define HWIO_LPASS_AHBE_STATUS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AHBE_STATUS_ADDR, m)
#define HWIO_LPASS_AHBE_STATUS_AHBE_DECODE_BOUNDARY_ERROR_BMSK                                     0x4
#define HWIO_LPASS_AHBE_STATUS_AHBE_DECODE_BOUNDARY_ERROR_SHFT                                     0x2
#define HWIO_LPASS_AHBE_STATUS_AHBE_DECODE_ERROR_BMSK                                              0x2
#define HWIO_LPASS_AHBE_STATUS_AHBE_DECODE_ERROR_SHFT                                              0x1
#define HWIO_LPASS_AHBE_STATUS_AHBE_MISALIGNED_BMSK                                                0x1
#define HWIO_LPASS_AHBE_STATUS_AHBE_MISALIGNED_SHFT                                                0x0

#define HWIO_LPASS_AHBE_ACK_ADDR                                                            (LPASS_LPASS_TCSR_REG_BASE      + 0x0000010c)
#define HWIO_LPASS_AHBE_ACK_OFFS                                                            (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000010c)
#define HWIO_LPASS_AHBE_ACK_RMSK                                                                   0x7
#define HWIO_LPASS_AHBE_ACK_POR                                                             0x00000000
#define HWIO_LPASS_AHBE_ACK_POR_RMSK                                                        0xffffffff
#define HWIO_LPASS_AHBE_ACK_OUT(v)      \
        out_dword(HWIO_LPASS_AHBE_ACK_ADDR,v)
#define HWIO_LPASS_AHBE_ACK_AHBE_DECODE_BOUNDARY_ERROR_BMSK                                        0x4
#define HWIO_LPASS_AHBE_ACK_AHBE_DECODE_BOUNDARY_ERROR_SHFT                                        0x2
#define HWIO_LPASS_AHBE_ACK_AHBE_DECODE_ERROR_BMSK                                                 0x2
#define HWIO_LPASS_AHBE_ACK_AHBE_DECODE_ERROR_SHFT                                                 0x1
#define HWIO_LPASS_AHBE_ACK_AHBE_MISALIGNED_BMSK                                                   0x1
#define HWIO_LPASS_AHBE_ACK_AHBE_MISALIGNED_SHFT                                                   0x0

#define HWIO_LPASS_AHBE_INT_EN_ADDR                                                         (LPASS_LPASS_TCSR_REG_BASE      + 0x00000110)
#define HWIO_LPASS_AHBE_INT_EN_OFFS                                                         (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000110)
#define HWIO_LPASS_AHBE_INT_EN_RMSK                                                                0x7
#define HWIO_LPASS_AHBE_INT_EN_POR                                                          0x00000000
#define HWIO_LPASS_AHBE_INT_EN_POR_RMSK                                                     0xffffffff
#define HWIO_LPASS_AHBE_INT_EN_IN          \
        in_dword_masked(HWIO_LPASS_AHBE_INT_EN_ADDR, HWIO_LPASS_AHBE_INT_EN_RMSK)
#define HWIO_LPASS_AHBE_INT_EN_INM(m)      \
        in_dword_masked(HWIO_LPASS_AHBE_INT_EN_ADDR, m)
#define HWIO_LPASS_AHBE_INT_EN_OUT(v)      \
        out_dword(HWIO_LPASS_AHBE_INT_EN_ADDR,v)
#define HWIO_LPASS_AHBE_INT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AHBE_INT_EN_ADDR,m,v,HWIO_LPASS_AHBE_INT_EN_IN)
#define HWIO_LPASS_AHBE_INT_EN_AHBE_DECODE_BOUNDARY_ERROR_BMSK                                     0x4
#define HWIO_LPASS_AHBE_INT_EN_AHBE_DECODE_BOUNDARY_ERROR_SHFT                                     0x2
#define HWIO_LPASS_AHBE_INT_EN_AHBE_DECODE_ERROR_BMSK                                              0x2
#define HWIO_LPASS_AHBE_INT_EN_AHBE_DECODE_ERROR_SHFT                                              0x1
#define HWIO_LPASS_AHBE_INT_EN_AHBE_MISALIGNED_BMSK                                                0x1
#define HWIO_LPASS_AHBE_INT_EN_AHBE_MISALIGNED_SHFT                                                0x0

#define HWIO_LPASS_AHBE_SPARE_ADDR                                                          (LPASS_LPASS_TCSR_REG_BASE      + 0x00000114)
#define HWIO_LPASS_AHBE_SPARE_OFFS                                                          (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00000114)
#define HWIO_LPASS_AHBE_SPARE_RMSK                                                          0xffffffff
#define HWIO_LPASS_AHBE_SPARE_POR                                                           0x00000000
#define HWIO_LPASS_AHBE_SPARE_POR_RMSK                                                      0xffffffff
#define HWIO_LPASS_AHBE_SPARE_IN          \
        in_dword_masked(HWIO_LPASS_AHBE_SPARE_ADDR, HWIO_LPASS_AHBE_SPARE_RMSK)
#define HWIO_LPASS_AHBE_SPARE_INM(m)      \
        in_dword_masked(HWIO_LPASS_AHBE_SPARE_ADDR, m)
#define HWIO_LPASS_AHBE_SPARE_OUT(v)      \
        out_dword(HWIO_LPASS_AHBE_SPARE_ADDR,v)
#define HWIO_LPASS_AHBE_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AHBE_SPARE_ADDR,m,v,HWIO_LPASS_AHBE_SPARE_IN)
#define HWIO_LPASS_AHBE_SPARE_AHBE_SPARE_BMSK                                               0xffffffff
#define HWIO_LPASS_AHBE_SPARE_AHBE_SPARE_SHFT                                                      0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_ADDR                                               (LPASS_LPASS_TCSR_REG_BASE      + 0x00001000)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_OFFS                                               (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00001000)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_RMSK                                               0xff3fffff
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_POR                                                0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_POR_RMSK                                           0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_SPARE_BMSK                                    0xc0000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_SPARE_SHFT                                          0x1e
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_LPASS_CC_QUAD_DIG_CODEC_CLK_SEL_BMSK               0x20000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_LPASS_CC_QUAD_DIG_CODEC_CLK_SEL_SHFT                     0x1d
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_LPASS_CC_DIG_CODEC_CLK_SEL_BMSK                    0x10000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_LPASS_CC_DIG_CODEC_CLK_SEL_SHFT                          0x1c
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_TDM_MSTR_CLK_EN_BMSK                        0x8000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_TDM_MSTR_CLK_EN_SHFT                             0x1b
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_TDM_SLV_CLK_EN_BMSK                         0x4000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_TDM_SLV_CLK_EN_SHFT                              0x1a
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_MCLK_EN_BMSK                                   0x2000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_MCLK_EN_SHFT                                        0x19
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_SCLK_BIT_EXT_CLK_SEL_BMSK                   0x1000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_AUX_I2S_SCLK_BIT_EXT_CLK_SEL_SHFT                        0x18
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_WS_SLAVE_SEL_BMSK                                0x200000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_WS_SLAVE_SEL_SHFT                                    0x15
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_CODEC_WS_SLAVE_SEL_BMSK                              0x100000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_CODEC_WS_SLAVE_SEL_SHFT                                  0x14
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_DATAIN0_SEL_BMSK                                  0x80000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_DATAIN0_SEL_SHFT                                     0x13
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_DATAIN1_SEL_BMSK                                  0x40000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TER_DATAIN1_SEL_SHFT                                     0x12
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_WS_SLAVE_SEL_BMSK                                 0x30000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_WS_SLAVE_SEL_SHFT                                    0x10
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_SCLK_SEL_BMSK                                      0x8000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_SCLK_SEL_SHFT                                         0xf
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_DATAIN0_SEL_BMSK                                   0x4000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_DATAIN0_SEL_SHFT                                      0xe
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_DATAIN1_SEL_BMSK                                   0x2000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_QUA_DATAIN1_SEL_SHFT                                      0xd
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_OUT_SEL_BMSK                                   0x1800
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_OUT_SEL_SHFT                                      0xb
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_EN_SEL_BMSK                                     0x600
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_EN_SEL_SHFT                                       0x9
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_EN_VAL_BMSK                                     0x100
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_WS_EN_VAL_SHFT                                       0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATAIN0_SEL_BMSK                                    0x80
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATAIN0_SEL_SHFT                                     0x7
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATAIN1_SEL_BMSK                                    0x40
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATAIN1_SEL_SHFT                                     0x6
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA0_EN_SEL_BMSK                                   0x20
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA0_EN_SEL_SHFT                                    0x5
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA1_EN_SEL_BMSK                                   0x10
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA1_EN_SEL_SHFT                                    0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA0_EN_BMSK                                        0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA0_EN_SHFT                                        0x3
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA1_EN_BMSK                                        0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_DATA1_EN_SHFT                                        0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_SCLK_EN_BMSK                                         0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_SCLK_EN_SHFT                                         0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_CODEC_SLK_OUT_SEL_BMSK                               0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_CTL_TLMM_CODEC_SLK_OUT_SEL_SHFT                               0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_ADDR                                              (LPASS_LPASS_TCSR_REG_BASE      + 0x00001004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_OFFS                                              (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00001004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_RMSK                                              0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_POR                                               0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_POR_RMSK                                          0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_SPARE_BMSK                                   0xffe00000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_SPARE_SHFT                                         0x15
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_LPASS_CC_DIG_CODEC_CLK_SEL_BMSK                     0x100000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_LPASS_CC_DIG_CODEC_CLK_SEL_SHFT                         0x14
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_EN_SEL_BMSK                                  0xc0000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_EN_SEL_SHFT                                     0x12
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_WS_SLAVE_SEL_BMSK                                0x30000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_WS_SLAVE_SEL_SHFT                                   0x10
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_WS_SLAVE_SEL_BMSK                               0xc000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_WS_SLAVE_SEL_SHFT                                  0xe
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_DATAOUT1_SEC_DATAOUT0_SEL_BMSK                    0x2000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_DATAOUT1_SEC_DATAOUT0_SEL_SHFT                       0xd
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_DATAIN0_SEL_BMSK                                0x1000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_DATAIN0_SEL_SHFT                                   0xc
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_SEC_DATAOUT1_SEL_BMSK                              0x800
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_PRI_SEC_DATAOUT1_SEL_SHFT                                0xb
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_DATAIN1_SEL_BMSK                                 0x400
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_CODEC_DATAIN1_SEL_SHFT                                   0xa
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_SEC_WS_SLAVE_SEL_BMSK                                  0x300
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_SEC_WS_SLAVE_SEL_SHFT                                    0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_OUT_SEL_BMSK                                    0xc0
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_OUT_SEL_SHFT                                     0x6
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_EN_VAL_BMSK                                     0x20
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_WS_EN_VAL_SHFT                                      0x5
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_DATA0_EN_BMSK                                      0x10
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_DATA0_EN_SHFT                                       0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_DATA1_EN_BMSK                                       0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_DATA1_EN_SHFT                                       0x3
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_SCLK_EN_BMSK                                        0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_SCLK_EN_SHFT                                        0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_MCLK_EN_BMSK                                        0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_MCLK_EN_SHFT                                        0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_CODEC_SLK_OUT_SEL_BMSK                              0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_CTL_TLMM_CODEC_SLK_OUT_SEL_SHFT                              0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_ADDR                                               (LPASS_LPASS_TCSR_REG_BASE      + 0x00002000)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_OFFS                                               (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00002000)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_RMSK                                                     0xff
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_POR                                                0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_POR_RMSK                                           0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_QUI_SPARE_BMSK                                           0xfe
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_QUI_SPARE_SHFT                                            0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_QUI_SEL_BMSK                                              0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_CTL_QUI_SEL_SHFT                                              0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_ADDR                                          (LPASS_LPASS_TCSR_REG_BASE      + 0x00003000)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_OFFS                                          (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00003000)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_RMSK                                              0x1fff
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_POR                                           0x0000000c
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_POR_RMSK                                      0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEC_TLMM_SPARE_BMSK                               0x1f00
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEC_TLMM_SPARE_SHFT                                  0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEN_CLK_SEL_BMSK                                    0xc0
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEN_CLK_SEL_SHFT                                     0x6
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_CLK_EN_BMSK                                     0x20
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_CLK_EN_SHFT                                      0x5
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_WS_EN_BMSK                                      0x10
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_WS_EN_SHFT                                       0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_DATA1_EN_BMSK                                    0x8
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_DATA1_EN_SHFT                                    0x3
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_DATA0_EN_BMSK                                    0x4
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_WSA_DATA0_EN_SHFT                                    0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEL_BMSK                                             0x3
#define HWIO_LPASS_CSR_GP_IO_MUX_SEC_TLMM_CTL_SEL_SHFT                                             0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_ADDR                                               (LPASS_LPASS_TCSR_REG_BASE      + 0x00003004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_OFFS                                               (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00003004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_RMSK                                                     0xff
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_POR                                                0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_POR_RMSK                                           0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_SEN_SPARE_BMSK                                           0xfe
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_SEN_SPARE_SHFT                                            0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_CLK_SEL_BMSK                                              0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_CTL_CLK_SEL_SHFT                                              0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_ADDR                                       (LPASS_LPASS_TCSR_REG_BASE      + 0x00004000)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_OFFS                                       (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00004000)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_RMSK                                              0x3
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_POR                                        0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_POR_RMSK                                   0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_CODEC_MIC_MI2S_EXT_CLK_SEL_BMSK                   0x2
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_CODEC_MIC_MI2S_EXT_CLK_SEL_SHFT                   0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_MI2S_AUX_EXT_CLK_SEL_BMSK                         0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_MIC_EXT_CLK_CTL_MI2S_AUX_EXT_CLK_SEL_SHFT                         0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_ADDR                                      (LPASS_LPASS_TCSR_REG_BASE      + 0x00004004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_OFFS                                      (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00004004)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_RMSK                                             0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_POR                                       0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_POR_RMSK                                  0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_CODEC_SPKR_MI2S_EXT_CLK_SEL_BMSK                 0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SPKR_EXT_CLK_CTL_CODEC_SPKR_MI2S_EXT_CLK_SEL_SHFT                 0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_ADDR                                       (LPASS_LPASS_TCSR_REG_BASE      + 0x00004008)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_OFFS                                       (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00004008)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_RMSK                                              0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_POR                                        0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_POR_RMSK                                   0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_EXT_CLK_SEL_BMSK                                  0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_QUI_EXT_CLK_CTL_EXT_CLK_SEL_SHFT                                  0x0

#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_ADDR                                       (LPASS_LPASS_TCSR_REG_BASE      + 0x0000400c)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_OFFS                                       (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000400c)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_RMSK                                              0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_POR                                        0x00000000
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_POR_RMSK                                   0xffffffff
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_ADDR, HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_RMSK)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_ADDR, m)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_ADDR,v)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_ADDR,m,v,HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_IN)
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_EXT_CLK_SEL_BMSK                                  0x1
#define HWIO_LPASS_CSR_GP_IO_MUX_SEN_EXT_CLK_CTL_EXT_CLK_SEL_SHFT                                  0x0

#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_ADDR                                (LPASS_LPASS_TCSR_REG_BASE      + 0x00005000)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_OFFS                                (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00005000)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_RMSK                                       0x1
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_POR                                 0x00000000
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_POR_RMSK                            0xffffffff
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_ADDR, HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_RMSK)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_ADDR, m)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_ADDR,v)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_ADDR,m,v,HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_IN)
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_I2S_PCM_SEL_BMSK                           0x1
#define HWIO_LPASS_CSR_GP_LPAIF_PRI_PCM_PRI_MODE_MUXSEL_I2S_PCM_SEL_SHFT                           0x0

#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_ADDR                                (LPASS_LPASS_TCSR_REG_BASE      + 0x00006000)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_OFFS                                (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00006000)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_RMSK                                       0x1
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_POR                                 0x00000000
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_POR_RMSK                            0xffffffff
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_IN          \
        in_dword_masked(HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_ADDR, HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_RMSK)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_INM(m)      \
        in_dword_masked(HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_ADDR, m)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_OUT(v)      \
        out_dword(HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_ADDR,v)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_ADDR,m,v,HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_IN)
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_I2S_PCM_SEL_BMSK                           0x1
#define HWIO_LPASS_CSR_GP_LPAIF_QUI_PCM_SEC_MODE_MUXSEL_I2S_PCM_SEL_SHFT                           0x0

#define HWIO_LPASS_Q6SS_RST_EVB_SEL_ADDR                                                    (LPASS_LPASS_TCSR_REG_BASE      + 0x00007000)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_OFFS                                                    (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00007000)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_RMSK                                                           0x1
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_POR                                                     0x00000000
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_POR_RMSK                                                0xffffffff
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_IN          \
        in_dword_masked(HWIO_LPASS_Q6SS_RST_EVB_SEL_ADDR, HWIO_LPASS_Q6SS_RST_EVB_SEL_RMSK)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_INM(m)      \
        in_dword_masked(HWIO_LPASS_Q6SS_RST_EVB_SEL_ADDR, m)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_OUT(v)      \
        out_dword(HWIO_LPASS_Q6SS_RST_EVB_SEL_ADDR,v)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_Q6SS_RST_EVB_SEL_ADDR,m,v,HWIO_LPASS_Q6SS_RST_EVB_SEL_IN)
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_SEL_BMSK                                                       0x1
#define HWIO_LPASS_Q6SS_RST_EVB_SEL_SEL_SHFT                                                       0x0

#define HWIO_LPASS_Q6SS_RST_EVB_ADDR                                                        (LPASS_LPASS_TCSR_REG_BASE      + 0x00007004)
#define HWIO_LPASS_Q6SS_RST_EVB_OFFS                                                        (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00007004)
#define HWIO_LPASS_Q6SS_RST_EVB_RMSK                                                         0xffffff0
#define HWIO_LPASS_Q6SS_RST_EVB_POR                                                         0x00000000
#define HWIO_LPASS_Q6SS_RST_EVB_POR_RMSK                                                    0xffffffff
#define HWIO_LPASS_Q6SS_RST_EVB_IN          \
        in_dword_masked(HWIO_LPASS_Q6SS_RST_EVB_ADDR, HWIO_LPASS_Q6SS_RST_EVB_RMSK)
#define HWIO_LPASS_Q6SS_RST_EVB_INM(m)      \
        in_dword_masked(HWIO_LPASS_Q6SS_RST_EVB_ADDR, m)
#define HWIO_LPASS_Q6SS_RST_EVB_OUT(v)      \
        out_dword(HWIO_LPASS_Q6SS_RST_EVB_ADDR,v)
#define HWIO_LPASS_Q6SS_RST_EVB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_Q6SS_RST_EVB_ADDR,m,v,HWIO_LPASS_Q6SS_RST_EVB_IN)
#define HWIO_LPASS_Q6SS_RST_EVB_EVB_BMSK                                                     0xffffff0
#define HWIO_LPASS_Q6SS_RST_EVB_EVB_SHFT                                                           0x4

#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_ADDR                                      (LPASS_LPASS_TCSR_REG_BASE      + 0x00008000)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_OFFS                                      (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00008000)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_RMSK                                             0x3
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_POR                                       0x00000003
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_POR_RMSK                                  0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_HW_CTL_BMSK                                      0x2
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_HW_CTL_SHFT                                      0x1
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_CLK_ENABLE_BMSK                                  0x1
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_CGCR_CLK_ENABLE_SHFT                                  0x0

#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ADDR                                (LPASS_LPASS_TCSR_REG_BASE      + 0x00008004)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_OFFS                                (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00008004)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_RMSK                                       0x7
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_POR                                 0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_POR_RMSK                            0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                        0x7
#define HWIO_LPASS_AUDIO_WRAPPER_LCC_CSR_AON_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                        0x0

#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_ADDR                                         (LPASS_LPASS_TCSR_REG_BASE      + 0x00009000)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_OFFS                                         (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00009000)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_RMSK                                                0x3
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_POR                                          0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_POR_RMSK                                     0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_HW_CTL_BMSK                                         0x2
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_HW_CTL_SHFT                                         0x1
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_CLK_ENABLE_BMSK                                     0x1
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_CGCR_CLK_ENABLE_SHFT                                     0x0

#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ADDR                                   (LPASS_LPASS_TCSR_REG_BASE      + 0x00009004)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_OFFS                                   (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x00009004)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_RMSK                                          0x7
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_POR                                    0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_POR_RMSK                               0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                           0x7
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_AON_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                           0x0

#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_ADDR                                        (LPASS_LPASS_TCSR_REG_BASE      + 0x0000a000)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_OFFS                                        (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000a000)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_RMSK                                               0x3
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_POR                                         0x00000003
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_POR_RMSK                                    0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_HW_CTL_BMSK                                        0x2
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_HW_CTL_SHFT                                        0x1
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_CLK_ENABLE_BMSK                                    0x1
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_CGCR_CLK_ENABLE_SHFT                                    0x0

#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ADDR                                  (LPASS_LPASS_TCSR_REG_BASE      + 0x0000a004)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_OFFS                                  (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000a004)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_RMSK                                         0x7
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_POR                                   0x00000007
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_POR_RMSK                              0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                          0x7
#define HWIO_LPASS_AUDIO_WRAPPER_Q6AHB_AON_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                          0x0

#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_ADDR                                     (LPASS_LPASS_TCSR_REG_BASE      + 0x0000b000)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_OFFS                                     (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000b000)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_RMSK                                            0x3
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_POR                                      0x00000003
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_POR_RMSK                                 0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_HW_CTL_BMSK                                     0x2
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_HW_CTL_SHFT                                     0x1
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_CLK_ENABLE_BMSK                                 0x1
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CGCR_CLK_ENABLE_SHFT                                 0x0

#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ADDR                               (LPASS_LPASS_TCSR_REG_BASE      + 0x0000b004)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_OFFS                               (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000b004)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_RMSK                                      0x7
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_POR                                0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_POR_RMSK                           0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                       0x7
#define HWIO_LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                       0x0

#define HWIO_LPASS_TCSR_QOS_CTL_ADDR                                                        (LPASS_LPASS_TCSR_REG_BASE      + 0x0000b008)
#define HWIO_LPASS_TCSR_QOS_CTL_OFFS                                                        (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000b008)
#define HWIO_LPASS_TCSR_QOS_CTL_RMSK                                                               0x1
#define HWIO_LPASS_TCSR_QOS_CTL_POR                                                         0x00000000
#define HWIO_LPASS_TCSR_QOS_CTL_POR_RMSK                                                    0xffffffff
#define HWIO_LPASS_TCSR_QOS_CTL_IN          \
        in_dword_masked(HWIO_LPASS_TCSR_QOS_CTL_ADDR, HWIO_LPASS_TCSR_QOS_CTL_RMSK)
#define HWIO_LPASS_TCSR_QOS_CTL_INM(m)      \
        in_dword_masked(HWIO_LPASS_TCSR_QOS_CTL_ADDR, m)
#define HWIO_LPASS_TCSR_QOS_CTL_OUT(v)      \
        out_dword(HWIO_LPASS_TCSR_QOS_CTL_ADDR,v)
#define HWIO_LPASS_TCSR_QOS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_TCSR_QOS_CTL_ADDR,m,v,HWIO_LPASS_TCSR_QOS_CTL_IN)
#define HWIO_LPASS_TCSR_QOS_CTL_QOS_ENABLE_BMSK                                                    0x1
#define HWIO_LPASS_TCSR_QOS_CTL_QOS_ENABLE_SHFT                                                    0x0

#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_ADDR                                         (LPASS_LPASS_TCSR_REG_BASE      + 0x0000c000)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_OFFS                                         (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000c000)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_RMSK                                                0x3
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_POR                                          0x00000003
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_POR_RMSK                                     0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_HW_CTL_BMSK                                         0x2
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_HW_CTL_SHFT                                         0x1
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_CLK_ENABLE_BMSK                                     0x1
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_CGCR_CLK_ENABLE_SHFT                                     0x0

#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ADDR                                   (LPASS_LPASS_TCSR_REG_BASE      + 0x0000c004)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_OFFS                                   (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000c004)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_RMSK                                          0x7
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_POR                                    0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_POR_RMSK                               0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                           0x7
#define HWIO_LPASS_AUDIO_WRAPPER_TCSR_XPU_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                           0x0

#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_ADDR                                 (LPASS_LPASS_TCSR_REG_BASE      + 0x0000d000)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_OFFS                                 (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000d000)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_RMSK                                        0x3
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_POR                                  0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_POR_RMSK                             0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_HW_CTL_BMSK                                 0x2
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_HW_CTL_SHFT                                 0x1
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_CLK_ENABLE_BMSK                             0x1
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_CGCR_CLK_ENABLE_SHFT                             0x0

#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ADDR                           (LPASS_LPASS_TCSR_REG_BASE      + 0x0000d004)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_OFFS                           (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000d004)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_RMSK                                  0x7
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_POR                            0x00000007
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_POR_RMSK                       0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                   0x7
#define HWIO_LPASS_AUDIO_WRAPPER_SYSNOC_SWAY_CORE_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                   0x0

#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_ADDR                                   (LPASS_LPASS_TCSR_REG_BASE      + 0x0000e000)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_OFFS                                   (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000e000)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_RMSK                                          0x3
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_POR                                    0x00000001
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_POR_RMSK                               0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_HW_CTL_BMSK                                   0x2
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_HW_CTL_SHFT                                   0x1
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_CLK_ENABLE_BMSK                               0x1
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_CGCR_CLK_ENABLE_SHFT                               0x0

#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ADDR                             (LPASS_LPASS_TCSR_REG_BASE      + 0x0000e004)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_OFFS                             (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000e004)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_RMSK                                    0x7
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_POR                              0x00000007
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_POR_RMSK                         0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                     0x7
#define HWIO_LPASS_AUDIO_WRAPPER_QDSP_SWAY_CORE_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                     0x0

#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_ADDR                                             (LPASS_LPASS_TCSR_REG_BASE      + 0x0000f000)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_OFFS                                             (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000f000)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_RMSK                                                    0x3
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_POR                                              0x00000003
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_POR_RMSK                                         0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_ADDR, HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_HW_CTL_BMSK                                             0x2
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_HW_CTL_SHFT                                             0x1
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_CLK_ENABLE_BMSK                                         0x1
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_CGCR_CLK_ENABLE_SHFT                                         0x0

#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ADDR                                       (LPASS_LPASS_TCSR_REG_BASE      + 0x0000f004)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_OFFS                                       (LPASS_LPASS_TCSR_REG_BASE_OFFS + 0x0000f004)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_RMSK                                              0x7
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_POR                                        0x00000002
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_POR_RMSK                                   0xffffffff
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_IN          \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ADDR, HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_RMSK)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_INM(m)      \
        in_dword_masked(HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ADDR, m)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_OUT(v)      \
        out_dword(HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ADDR,v)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ADDR,m,v,HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_IN)
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ACTIVE_HYSTSEL_BMSK                               0x7
#define HWIO_LPASS_AUDIO_WRAPPER_SMEM_HYSTERESIS_ACTIVE_HYSTSEL_SHFT                               0x0


#endif /* __HWIO_LPASS_TCSR_8952_H__ */
