/*==============================================================================
   FILE:        MPMIntDevCfgData.c

  OVERVIEW:     MPM hardware to local interrupt/gpio mapping info. This
                mapping is target specific and may change for different 
                targets. Even mapping could change between masters within a
                same target as some master may want a mapped irq as wakeup
                interrupt while other may not.
 
  Reference chip release: Interrupts_Jacalav14.0		
 
  DEPENDENCIES: None

                Copyright (c) 2013-2015 QUALCOMM Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

===============================================================================
$Header: //components/rel/core.adsp/2.6.6/power/mpm/config/asic/8953/MPMIntDevCfgData.c#1 $
$DateTime: 2016/05/04 01:42:14 $
=============================================================================*/ 

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */
#include "vmpm_internal.h"

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

/**
 * Table mapping the MPM Pin number to the GPIO and/or IRQ pin 
 * number per the IP Catalog. 
 */
mpm_int_pin_mapping_type devcfg_MpmInterruptPinNum_Mapping[] =
{
  /* Trigger                 Local Pin#         Pin Type   */
  /* -------                 -------------      --------   */
  /*0*/
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, 38              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 1               , MPM_GPIO },
  /*5*/
  { MPM_TRIGGER_RISING_EDGE, 5               , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 9               , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, 37              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 36              , MPM_GPIO },
  /*10*/
  { MPM_TRIGGER_RISING_EDGE, 13              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 35              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 17              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 21              , MPM_GPIO },	
  { MPM_TRIGGER_RISING_EDGE, 54              , MPM_GPIO },
  /*15*/                                                 
  { MPM_TRIGGER_RISING_EDGE, 34              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 31              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 58              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 28              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 42              , MPM_GPIO },
  /*20*/
  { MPM_TRIGGER_RISING_EDGE, 25              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 12              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 43              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 44              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 45              , MPM_GPIO },
  /*25*/
  { MPM_TRIGGER_RISING_EDGE, 46              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 48              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 65              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 93              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 97              , MPM_GPIO },
  /*30*/                                                 
  { MPM_TRIGGER_RISING_EDGE, 63              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 70              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 71              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 72              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 81              , MPM_GPIO },
  /*35*/
  { MPM_TRIGGER_RISING_EDGE, 85              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 90              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  /*40*/
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  /*45*/
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  /*50*/
  { MPM_TRIGGER_RISING_EDGE, 67              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 73              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 74              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 62              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  /*55*/                                                
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, 59              , MPM_GPIO },
  /*60*/                                                
  { MPM_TRIGGER_RISING_EDGE, 60              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 61              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 86              , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 87              , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 91              , MPM_GPIO },
  /*65*/                                                
  { MPM_TRIGGER_RISING_EDGE, 129             , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 130             , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 131             , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE, 132             , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 133             , MPM_GPIO },
  /*70*/                                                
  { MPM_TRIGGER_RISING_EDGE, 137             , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE, 138             , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE,  139            , MPM_GPIO },
  { MPM_TRIGGER_RISING_EDGE,  140            , MPM_GPIO }, 
  { MPM_TRIGGER_RISING_EDGE,  141            , MPM_GPIO },
  /*75*/                                                
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },  
  /*80*/                                                
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },    
  /*85*/                                                
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, 89              , MPM_IRQ  }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  /*90*/                                                 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN }, 
  { MPM_TRIGGER_RISING_EDGE, MPM_UNMAPPED_INT, MPM_OPEN },  
  /*96 - End of the table entry */                       
  { MPM_TRIGGER_LEVEL_HIGH,  MPM_UNMAPPED_INT, MPM_EOT  },
};
