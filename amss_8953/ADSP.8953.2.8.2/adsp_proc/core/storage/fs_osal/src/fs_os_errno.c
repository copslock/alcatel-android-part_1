/***********************************************************************
 * fs_os_errno.c
 *
 * Error code mapping for all OSes.
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/src/fs_os_errno.c#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-03   dks   Create

===========================================================================*/
#include "fs_os_config_i.h"
#include "comdef.h"

#ifdef FEATURE_FS_OS_FOR_QURT
  #include "qurt_error.h"
#elif defined FEATURE_FS_OS_FOR_POSIX
  #if defined FEATURE_FS_OS_FOR_POSIX_FOR_TN
    #include "posix_errno.h"
  #else
    #include "errno.h"
  #endif
#elif defined FEATURE_FS_OS_FOR_REX
  #include "fs_errno.h"
#endif

#include "assert.h"

int
fs_os_get_errno (int local_os_errno)
{
#ifdef FEATURE_FS_OS_FOR_QURT
  return local_os_errno;

#elif defined FEATURE_FS_OS_FOR_POSIX

  return local_os_errno;

#else
  ASSERT (0);
  return local_os_errno;
#endif
}
