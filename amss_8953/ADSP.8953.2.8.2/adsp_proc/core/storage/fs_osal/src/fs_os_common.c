/***********************************************************************
 * fs_os_common.c
 *
 * OS common functionality
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 * Verbose Description
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/src/fs_os_common.c#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-13   dks   Create

===========================================================================*/

#include "fs_os_timetick.h"

void
fs_os_init (void)
{
  fs_os_timetick_init();
}

