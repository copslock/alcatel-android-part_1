/***********************************************************************
 * fs_os_err.h
 *
 * Err implemenation for all FS Modules
 *
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/inc/fs_os_err.h#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-13   dks   Create

===========================================================================*/

#ifndef __FS_OS_ERR_H__
#define __FS_OS_ERR_H__

#include "assert.h"
#include "err.h"

#define FS_OS_ASSERT(cond)                                      \
  do {                                                          \
      ASSERT (cond);                                            \
  } while (0)


#define FS_OS_ERR_FATAL(fmt,a,b,c) \
    do {                           \
      ERR_FATAL (fmt, a, b, c);    \
    } while (0)

#endif /* not __FS_OS_ERR_H__ */
