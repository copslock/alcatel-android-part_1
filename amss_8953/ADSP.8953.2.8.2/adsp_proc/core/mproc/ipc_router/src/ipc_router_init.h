#ifndef IPC_ROUTER_INIT_H
#define IPC_ROUTER_INIT_H
/*===========================================================================

                      I P C    R O U T E R    I N I T
                          H E A D E R    F I L E

   This file describes the OS-dependent interface to initialize the IPC Router.

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.adsp/2.6.6/mproc/ipc_router/src/ipc_router_init.h#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

void ipc_router_init(void);

#endif
