/*===========================================================================

                 GLINK Core<->transport Interface Source File


 Copyright (c) 2014 by QUALCOMM Technologies, Incorporated.  All Rights
 Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.adsp/2.6.6/mproc/glink/core/src/glink_core_if.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
3/22/14    bm     Initial version. 
===========================================================================*/


/*===========================================================================
                        INCLUDE FILES
===========================================================================*/

#include "glink_os.h"
#include "glink.h"
#include "glink_internal.h"
#include "smem_list.h"

/*===========================================================================
                              GLOBAL DATA DECLARATIONS
===========================================================================*/

/*===========================================================================
                    LOCAL FUNCTION DEFINITIONS
===========================================================================*/
static void glinki_free_intents(glink_channel_ctx_type *open_ch_ctx)
{
  glink_rx_intent_type *intent, *next_intent;
  glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);

  intent = smem_list_first(&open_ch_ctx->remote_intent_q);
  while(intent){
    next_intent = smem_list_next(intent);
    smem_list_delete(&open_ch_ctx->remote_intent_q, intent);
    glink_free(intent);
    intent = next_intent;
  }

  intent = smem_list_first(&open_ch_ctx->remote_intent_pending_tx_q);
  while(intent) {
    next_intent = smem_list_next(intent);
    smem_list_delete(&open_ch_ctx->remote_intent_pending_tx_q, intent);
    glink_free(intent);
    intent = next_intent;
  }

  intent = smem_list_first(&open_ch_ctx->local_intent_q);
  while(intent) {
    next_intent = smem_list_next(intent);
    smem_list_delete(&open_ch_ctx->local_intent_q, intent);
    glink_free(intent);
    intent = next_intent;
  }

  intent = smem_list_first(&open_ch_ctx->local_intent_client_q);
  while(intent) {
    next_intent = smem_list_next(intent);
    smem_list_delete(&open_ch_ctx->local_intent_client_q, intent);
    glink_free(intent);
    intent = next_intent;
  }
  glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);
}

/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      glink_link_up

DESCRIPTION   Indicates that transport is now ready to start negotiation 
              using the v0 configuration

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge
                         
RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_link_up
(
  glink_transport_if_type *if_ptr
)
{
  const glink_core_version_type *version_array;
  glink_core_xport_ctx_type *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  version_array = xport_ctx->version_array;

  /* Update the transport state */
  if_ptr->glink_core_priv->status = GLINK_XPORT_LINK_UP;

  /* Start the negtiation */
  if_ptr->tx_cmd_version(if_ptr, version_array->version, 
      version_array->features);
  
  GLINK_LOG_EVENT(GLINK_EVENT_LINK_UP, NULL, xport_ctx->xport, xport_ctx->remote_ss, 
      GLINK_STATUS_SUCCESS);

}

/*===========================================================================
FUNCTION      glink_rx_cmd_version

DESCRIPTION   Receive transport version for remote-initiated version 
              negotiation

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            version  Remote Version

            features Remote Features

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_version
(
  glink_transport_if_type *if_ptr,
  uint32                  version,
  uint32                  features
)
{
  const glink_core_version_type *ver;
  uint32 negotiated_features;
  glink_core_xport_ctx_type *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;
  
  /* The version to use must be a subset of supported version and features
   * on this host and remote host */
  ver = &xport_ctx->version_array[xport_ctx->version_indx];
  ASSERT(ver);

  /* Call the transport's negotiation function */
  negotiated_features = ver->negotiate_features(if_ptr, ver, features);
  

  /* If negotiated features match the provided features, version nogetiation
   * is complete */
  if(negotiated_features == features) {
    if_ptr->set_version(if_ptr, version, features);
    GLINK_LOG_EVENT(GLINK_EVENT_RX_CMD_VER, NULL, xport_ctx->xport, xport_ctx->remote_ss, 
        features);
    return;
  }

  /* Versions don't match, return ACK with the feature set that we support */
  if_ptr->tx_cmd_version_ack(if_ptr, version, negotiated_features);
  /* Next time use older version */
  ASSERT(xport_ctx->version_indx > 0);
  xport_ctx->version_indx--;
  
}

/*===========================================================================
FUNCTION      glink_rx_cmd_version_ack

DESCRIPTION   Receive ACK to previous glink_transport_if::tx_cmd_version 
              command

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            version  Remote Version

            features Remote Features
                         
RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_version_ack
(
  glink_transport_if_type *if_ptr,
  uint32                  version,
  uint32                  features
)
{
  const glink_core_version_type* ver;
  uint32 negotiated_features;
  glink_core_xport_ctx_type *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Check to see if version returned by remote end is supported by 
   * this host. Remote side would have acked only when the version/features
   * sent by this host did not match the remote */

  ver = &xport_ctx->version_array[xport_ctx->version_indx];
  ASSERT(ver);

  /* Call the transport's negotiation function */
  negotiated_features = ver->negotiate_features(if_ptr, ver, features);

  if(negotiated_features != features)
  {
    /* Continue negotiating */
    if_ptr->tx_cmd_version(if_ptr, version, negotiated_features);
  }
}

/*===========================================================================
FUNCTION      glink_rx_cmd_ch_remote_open

DESCRIPTION   Receive remote channel open request; Calls 
              glink_transport_if:: tx_cmd_ch_remote_open_ack as a result

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            rcid     Remote Channel ID

            *name    String name for logical channel
                         
RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_ch_remote_open
(
  glink_transport_if_type *if_ptr,
  uint32                  rcid,
  const char              *name
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_channel_ctx_type *remote_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx;
  
  ASSERT(if_ptr != NULL);
  ASSERT(name != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Allocate and initialize channel info structure */  
  remote_ch_ctx = glink_calloc(sizeof(glink_channel_ctx_type));
  if(remote_ch_ctx == NULL) {
    GLINK_LOG_EVENT(GLINK_EVENT_RM_CH_OPEN, name, xport_ctx->xport,
        xport_ctx->remote_ss, GLINK_STATUS_OUT_OF_RESOURCES);
    ASSERT(0);  
  }
 
  /* Fill in the channel info structure */
  glink_string_copy(remote_ch_ctx->name, name, sizeof(remote_ch_ctx->name));
  remote_ch_ctx->rcid = rcid;

  /* See if channel already exists in open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {
    if(strncmp(open_ch_ctx->name, name, strlen(name)) == 0 )
    {
      /* We've found a channel name is already in the list of open channel */

      /* Case A: Channel was opened before on the same host */
      if(open_ch_ctx->state == GLINK_CH_STATE_OPENING) {
        /* Set the channel state to OPEN and send ACK to transport */
        open_ch_ctx->state = GLINK_CH_STATE_OPEN;
        smem_list_init(&open_ch_ctx->remote_intent_q);
        smem_list_init(&open_ch_ctx->remote_intent_pending_tx_q);
        smem_list_init(&open_ch_ctx->local_intent_q);
        smem_list_init(&open_ch_ctx->local_intent_client_q);
        /* Set the rcid */
        open_ch_ctx->rcid = rcid;

        /* Send ACK and return */
        if_ptr->tx_cmd_ch_remote_open_ack(if_ptr, rcid);

        glink_free(remote_ch_ctx);

        GLINK_LOG_EVENT(GLINK_EVENT_RM_CH_OPEN, name,
            xport_ctx->xport, xport_ctx->remote_ss, GLINK_STATUS_SUCCESS);

        /* Inform the client */
        open_ch_ctx->notify_state(open_ch_ctx, open_ch_ctx->priv, GLINK_CONNECTED);
        return;
      } else { 
        /* Case B: Channel was already opened, return error */
        glink_free(remote_ch_ctx);
        GLINK_LOG_EVENT(GLINK_EVENT_RM_CH_OPEN, name,
           xport_ctx->xport, xport_ctx->remote_ss, GLINK_STATUS_FAILURE);
        return;
      }
    } /* end if match found */
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* Send the OPEN command ACK to transport */
  if_ptr->tx_cmd_ch_remote_open_ack(if_ptr, rcid);

  /* Set channel state */
  remote_ch_ctx->state = GLINK_CH_STATE_REMOTE_OPEN;
  /* Append the channel to the transport interface's open_list */
  glink_cs_lock(&xport_ctx->channel_q_cs, TRUE);
  smem_list_append(&if_ptr->glink_core_priv->open_list, remote_ch_ctx);
  glink_cs_lock(&xport_ctx->channel_q_cs, FALSE);  

  GLINK_LOG_EVENT(GLINK_EVENT_RM_CH_OPEN, name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_SUCCESS);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_ch_open_ack

DESCRIPTION   This function is invoked by the transport in response to 
              glink_transport_if:: tx_cmd_ch_open

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            lcid     Local Channel ID

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_ch_open_ack
(
  glink_transport_if_type *if_ptr,
  uint32                  lcid
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Move to closed state. Implies we clean up the channel from the 
   * open list */

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->lcid == lcid) {
      
      GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN_ACK, open_ch_ctx->name, 
          xport_ctx->xport, xport_ctx->remote_ss, lcid);
      return;
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }
  /* We are here in case we could not find the channel in the open list. */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_ch_close_ack

DESCRIPTION   This function is invoked by the transport in response to 
              glink_transport_if_type:: tx_cmd_ch_close

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            lcid      Local Channel ID

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_ch_close_ack
(
  glink_transport_if_type *if_ptr, /* Pointer to the interface instance */
  uint32                  lcid     /* Local channel ID */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Move to closed state. Implies we clean up the channel from the 
   * open list */

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->lcid == lcid) {
      /* Found channel , inform the client */
      open_ch_ctx->notify_state(open_ch_ctx, open_ch_ctx->priv, GLINK_L_DISCONNECTED);
      GLINK_LOG_EVENT(GLINK_EVENT_CH_CLOSE_ACK, open_ch_ctx->name, 
          xport_ctx->xport, xport_ctx->remote_ss, lcid);

      /* Transition to REMOTE_OPEN state */
      open_ch_ctx->state = GLINK_CH_STATE_REMOTE_OPEN;
      return;
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We are here in case we could not find the channel in the open list. */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_ch_remote_close

DESCRIPTION   Remote channel close request; will result in sending 
              glink_transport_if_type:: tx_cmd_ch_remote_close_ack

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            rcid      Remote Channel ID

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_ch_remote_close
(
  glink_transport_if_type *if_ptr, /* Pointer to the interface instance */
  uint32                  rcid     /* Remote channel ID */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {
    if(open_ch_ctx->rcid == rcid) {
      /* Found channel, transition it to appropriate state based
       * on current state */
      if(open_ch_ctx->state == GLINK_CH_STATE_OPEN) {
        open_ch_ctx->state = GLINK_CH_STATE_REMOTE_CLOSE;

        /* Inform the client */
        open_ch_ctx->notify_state(open_ch_ctx, open_ch_ctx->priv, GLINK_R_DISCONNECTED);
        GLINK_LOG_EVENT(GLINK_EVENT_CH_REMOTE_CLOSE, open_ch_ctx->name, 
              xport_ctx->xport, xport_ctx->remote_ss, rcid);
        return;
      } else if (open_ch_ctx->state == GLINK_CH_STATE_REMOTE_OPEN) {
        /* Local side never opened the channel */
        /* Free channel resources */
        glinki_free_intents(open_ch_ctx);
        glink_cs_lock(&xport_ctx->channel_q_cs, TRUE);
        smem_list_delete(&if_ptr->glink_core_priv->open_list, open_ch_ctx);
        glink_cs_lock(&xport_ctx->channel_q_cs, FALSE);

        GLINK_LOG_EVENT(GLINK_EVENT_CH_REMOTE_CLOSE, open_ch_ctx->name, 
            xport_ctx->xport, xport_ctx->remote_ss, rcid);
        glink_free(open_ch_ctx);
        return;
      } else {
        /* SHould not get this callback for a channel in any other state */
        ASSERT(0);
      }
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We are here in case we could not find the channel in the open list 
   * or OPEN state. */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_ch_state_local_trans

DESCRIPTION   Process local state transition

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            rcid      Remote Channel ID

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_ch_state_local_trans
(
  glink_transport_if_type  *if_ptr,  /* Pointer to the interface instance */
  uint32                   lcid,     /* Local channel ID */
  glink_ch_state_type      new_state /* New local channel state */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->lcid == lcid) {
      /* Found channel, transition it to appropriate state */
      open_ch_ctx->state = new_state;
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
    GLINK_LOG_EVENT(GLINK_EVENT_CH_STATE_TRANS, open_ch_ctx->name, 
          xport_ctx->xport, xport_ctx->remote_ss, new_state);
    return;
  }/* end while */

  /* We are here in case we could not find the channel in the open list 
   * or OPEN state. */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_remote_rx_intent_put

DESCRIPTION   Transport invokes this call on receiving remote RX intent

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge

            rcid      Remote Channel ID

            riid      Remote intent ID

            size      Size of 

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
/**  */
void glink_rx_cmd_remote_rx_intent_put
(
  glink_transport_if_type *if_ptr, /* Pointer to the interface instance */
  uint32                  rcid,    /* Remote channel ID */
  uint32                  riid,    /* Remote intent ID */
  size_t                  size     /* Size of receive intent */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_rx_intent_type *rm_intent;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;
  
  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->rcid == rcid) {
      /* Allocate an intent structure */
      rm_intent = glink_calloc(sizeof(glink_rx_intent_type));
      ASSERT(rm_intent != NULL);
      rm_intent->iid = riid;
      rm_intent->size = size;
      
      /* buffer allocated by transport */
      //rm_intent->buffer = glink_malloc(size); /* for non-zero copy case */
      //ASSERT(rm_intent->buffer);

      /* Found channel, queue the intent */
      glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);
      smem_list_append(&open_ch_ctx->remote_intent_q, rm_intent);
      glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);

      GLINK_LOG_EVENT(GLINK_EVENT_CH_INTENT_PUT, open_ch_ctx->name, 
            xport_ctx->xport, xport_ctx->remote_ss, size);
      return;      
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We are here in case we could not find the channel in the open list 
   * or OPEN state. */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_rx_data

DESCRIPTION   Transport invokes this call on receiving new data packet

ARGUMENTS   *if_ptr    Pointer to interface instance; must be unique
                       for each edge

            rcid       Remote Channel ID

            liid       Local intent ID
 
            total_size Total size of the incoming packet

            *data      Data pointer to the 1st data fragment
            
            size       Size of the current fragment

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_rx_data
(
  glink_transport_if_type *if_ptr,   /* Pointer to the interface instance */
  uint32                  rcid,      /* Remote channel ID */
  uint32                  liid,      /* Local intent ID */
  size_t                  total_size,/* Total size of the incoming packet */
  const void              *data,     /* Data pointer to the 1st data fragment */
  size_t                  size       /* Size of the current fragment */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_rx_intent_type *lc_intent;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL && data != NULL && size!= 0);

  xport_ctx = if_ptr->glink_core_priv;

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->rcid == rcid) {
      glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);
      /* Found channel, get the intent from the queue */
      lc_intent = smem_list_first(&open_ch_ctx->local_intent_q);
      while(lc_intent && lc_intent->iid != liid) {
        lc_intent = smem_list_next(lc_intent);
      }
      ASSERT(lc_intent);

      /* Remove the intent from the queue */
      smem_list_delete(&open_ch_ctx->local_intent_q, lc_intent);
      glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);

      lc_intent->data = (void*)data;
      lc_intent->pkt_sz = total_size;

      /* Check to see if complete pkt was received */
      if(size == total_size) {
        /* Add the intent to the list of intents currently with the client */
        glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);
        smem_list_append(&open_ch_ctx->local_intent_client_q, lc_intent);
        glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);  

        open_ch_ctx->notify_rx(open_ch_ctx, open_ch_ctx->priv, 
            lc_intent->pkt_priv, lc_intent->data, lc_intent->pkt_sz);
        open_ch_ctx->cur_read_intent = NULL;
      } else {
        lc_intent->used = size;
        lc_intent->pkt_sz = total_size;
        open_ch_ctx->cur_read_intent = lc_intent;
      }

      GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DATA, open_ch_ctx->name, 
            xport_ctx->xport, xport_ctx->remote_ss, size);
      return;

    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We end up here if we don't find the channel */
  ASSERT(0);
}


/*===========================================================================
FUNCTION      glink_rx_cmd_rx_data_fragment

DESCRIPTION   Transport invokes this call to inform GLink to gather the 
              remaining data packet fragments. Note that this call should be 
              preceeded by glink_core_if_type:: rx_cmd_rx_data

ARGUMENTS   *if_ptr    Pointer to interface instance; must be unique
                       for each edge

            rcid       Remote Channel ID

            liid       Local intent ID
 
            *data      Data pointer to the data fragment
            
            size       Size of the current fragment

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_rx_data_fragment
(
  glink_transport_if_type *if_ptr, /* Pointer to the interface instance */
  uint32                  rcid,    /* Remote channel ID */
  const void              *data,    /* Data pointer to the data fragment */
  size_t                  size     /* Size of the current fragment */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_rx_intent_type *lc_intent;
  glink_core_xport_ctx_type  *xport_ctx;
  
  ASSERT(if_ptr != NULL && data != NULL && size!= 0);

  xport_ctx = if_ptr->glink_core_priv;

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->rcid == rcid) {
      /* Found channel, make sure we have previously saved intent
       * that we were readin data into */
      ASSERT(open_ch_ctx->cur_read_intent);
      
      lc_intent = open_ch_ctx->cur_read_intent;
      
      ASSERT((lc_intent->size - lc_intent->used) >= size);

      lc_intent->used += size;

      /* If we have received full packet, pass it to the caller */
      if(lc_intent->used == lc_intent->pkt_sz) {
        /* Call caller regsitered Rx callback and free the intent from
         * the queue */
        open_ch_ctx->notify_rx(open_ch_ctx, open_ch_ctx->priv, 
            lc_intent->pkt_priv, lc_intent->data, lc_intent->pkt_sz);

        /* Add the intent to the list of intents currently with the client */
        glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);
        smem_list_delete(&open_ch_ctx->local_intent_client_q, lc_intent);
        glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);        

        GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DATA_FRAG, open_ch_ctx->name, 
              xport_ctx->xport, xport_ctx->remote_ss, size);
        return;
      }
    }
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We end up here if we don't find the channel */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_rx_cmd_tx_done

DESCRIPTION   Transport invokes this call to inform GLink that remote side is
              done with previous transmitted packet.

ARGUMENTS   *if_ptr    Pointer to interface instance; must be unique
                       for each edge

            rcid       Remote Channel ID

            riid       Remote intent ID

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_rx_cmd_tx_done
(
  glink_transport_if_type *if_ptr, /* Pointer to the interface instance */
  uint32                  rcid,    /* Remote channel ID */
  uint32                  riid     /* Remote intent ID */
)
{
  glink_channel_ctx_type *open_ch_ctx;
  glink_rx_intent_type *rm_intent;
  glink_core_xport_ctx_type  *xport_ctx;

  ASSERT(if_ptr != NULL);

  xport_ctx = if_ptr->glink_core_priv;

  /* Find channel in the open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {  
    if(open_ch_ctx->rcid == rcid) {
      /* Found channel, check the list of pending completion intents */
      rm_intent = smem_list_first(&open_ch_ctx->remote_intent_pending_tx_q);
      while(rm_intent != NULL) {
        if(rm_intent->iid == riid) {
          /* found remote intent, delete it */
          glink_cs_lock(&open_ch_ctx->intent_q_cs, TRUE);
          smem_list_delete(&open_ch_ctx->remote_intent_pending_tx_q, rm_intent);
          glink_cs_lock(&open_ch_ctx->intent_q_cs, FALSE);

          GLINK_LOG_EVENT(GLINK_EVENT_CH_TX_DONE, open_ch_ctx->name, 
                xport_ctx->xport, xport_ctx->remote_ss, riid);

          /* Let the client know that data was transmitted */
          open_ch_ctx->notify_tx_done(open_ch_ctx, open_ch_ctx->priv, 
              rm_intent->pkt_priv, rm_intent->data, rm_intent->pkt_sz);
          /* Free the intent */
          glink_free(rm_intent);
          return;
        } /* end if(intent match) */
        rm_intent = smem_list_next(rm_intent);
      }/* end while (intent_q) */
    }/* end if(channel_match) */
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */

  /* We end up here if we don't find the channel */
  ASSERT(0);
}

/*===========================================================================
FUNCTION      glink_tx_resume

DESCRIPTION   If transport was full and could not continue a transmit 
              operation, then it will call this function to notify the core 
              that it is ready to resume transmission.

ARGUMENTS   *if_ptr    Pointer to interface instance; must be unique
                       for each edge

RETURN VALUE  None.

SIDE EFFECTS  None
===========================================================================*/
void glink_tx_resume
(
  glink_transport_if_type *if_ptr /* Pointer to the interface instance */
)
{
  /* Not sure what to do here */
}
  
