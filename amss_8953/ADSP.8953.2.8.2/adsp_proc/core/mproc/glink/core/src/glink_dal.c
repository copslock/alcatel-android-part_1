/*=========================================================================

                    GLink Core Driver DAL-OS abstraction

Define the normal REX task entry point and the OS support routines. The OS
support routines are implemented using DAL functions.

Copyright (c) 2014 by QUALCOMM Technologies, Incorporated.  All Rights 
Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.adsp/2.6.6/mproc/glink/core/src/glink_dal.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/08/14   bm      Initial version
===========================================================================*/


/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
//#include "rex.h"

#if defined(FEATURE_RCINIT)
#include "rcevt.h"
#include "rcinit.h"
#endif

#include "glink_os.h"

#include "DALSys.h"
#include "DALDeviceId.h"

#include <string.h>

/*===========================================================================
                           MACRO DEFINITIONS
===========================================================================*/


/*===========================================================================
                              GLOBAL DATA DECLARATIONS
===========================================================================*/

/*===========================================================================
                    LOCAL FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION      glink_cs_init

DESCRIPTION   Initializes a Critical Section

PARAMETERS    cs - pointer to critical section object allocated by caller.

RETURN VALUE  The critical section.
===========================================================================*/
void glink_cs_init(glink_os_cs_type *cs)
{
  DALSYSSyncHandle  cs_ptr;

  /* Create the new critical section */
  if( DAL_SUCCESS != DALSYS_SyncCreate( DALSYS_SYNC_ATTR_RESOURCE,
                                        &cs_ptr,
                                        cs ) )
  {
    ERR_FATAL("glink_cs_init: Critical section initialization failed", 0, 0, 0);
  }

}/* glink_cs_init */

/*===========================================================================
FUNCTION      glink_cs_lock

DESCRIPTION   lock/unlock a critical section

PARAMETERS    cs - pointer the the critical section

              lock - lock or unlock the critical section

RETURN VALUE  None.
===========================================================================*/
void glink_cs_lock( void *cs, boolean lock )
{
  DALSYSSyncHandle *cs_ptr = ( DALSYSSyncHandle * ) cs;

  if( lock )
  {
    DALSYS_SyncEnter(cs_ptr);
  }
  else
  {
    DALSYS_SyncLeave(cs_ptr);
  }
}/* glink_cs_lock */

/*===========================================================================
FUNCTION      glink_lock_acquire

DESCRIPTION   lock a critical section

PARAMETERS    cs - pointer the the critical section

RETURN VALUE  None.
===========================================================================*/
void glink_lock_acquire( void *cs )
{
  DALSYS_SyncEnter(( DALSYSSyncHandle * ) cs);
}

/*===========================================================================
FUNCTION      glink_lock_release

DESCRIPTION   unlock a critical section

PARAMETERS    cs - pointer the the critical section

RETURN VALUE  None.
===========================================================================*/
void glink_lock_release( void *cs )
{
  DALSYS_SyncLeave(( DALSYSSyncHandle * ) cs);
}

/*===========================================================================
FUNCTION      glink_cs_create

DESCRIPTION   Create and initialize a Critical Section

PARAMETERS    None

RETURN VALUE  The critical section.
===========================================================================*/
void *glink_cs_create(void)
{
  DALSYSSyncHandle  cs;

  /* Create the new critical section */
  if( DAL_SUCCESS != DALSYS_SyncCreate( DALSYS_SYNC_ATTR_RESOURCE,
                                        &cs,
                                        NULL ) )
  {
    ERR_FATAL("glink_cs_create: Critical section creation failed", 0, 0, 0);
    return NULL;
  }

  return ((void *)cs);
}/* glink_cs_create */

/*===========================================================================
FUNCTION      glink_cs_destroy

DESCRIPTION   Destroys a Critical Section

PARAMETERS    None

RETURN VALUE  none.
===========================================================================*/
void glink_cs_destroy( void *cs )
{
  DALSYS_DestroyObject( ( DALSYSSyncHandle ) cs );
}/* glink_cs_destroy */

/*===========================================================================
  FUNCTION  glink_malloc
===========================================================================*/
/**
  Dynamically allocate a region of memory from the heap.  The region should be
  freed using \c glink_free when no longer used.

  @param[in]  size   The number of bytes to be allocated.

  @return    The pointer to the region of memory that was allocated.
             NULL if the allocation failed.
*/
/*=========================================================================*/
void *glink_malloc( size_t size )
{
  void *pMem;
  if (DALSYS_Malloc(size, &pMem) != DAL_SUCCESS)
  {
    return NULL;
  }
  else
  {
    return pMem;
  }
}

/*===========================================================================
  FUNCTION  glink_calloc
===========================================================================*/
/**
  Dynamically allocate a region of memory from the heap and initialize with 
  the zeroes.  The region should be freed using \c glink_free 
  when no longer used.

  @param[in]  size   The number of bytes to be allocated.

  @return    The pointer to the region of memory that was allocated.
             NULL if the allocation failed.
*/
/*=========================================================================*/
void *glink_calloc( size_t size )
{
  void *pMem;
  if (DALSYS_Malloc(size, &pMem) != DAL_SUCCESS)
  {
    return NULL;
  }
  else
  {
    memset(pMem, 0, size);
    return pMem;
  }
}

/*===========================================================================
  FUNCTION  glink_free
===========================================================================*/
/**
  Free a region of memory that was allocated by \c glink_malloc.

  @param[in] pMem    The reference to the region of memory to be freed.

  @return    0 if succesful.
             Negative value if an error occurred.
*/
/*=========================================================================*/
int32 glink_free( void *pMem )
{
  if (DALSYS_Free(pMem) != DAL_SUCCESS)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}

/*===========================================================================
  FUNCTION  glink_string_copy
===========================================================================*/
/**
  Copies the source string into the destination buffer until 
  size is reached, or until a '\0' is encountered.  If valid,
  the destination string will always be NULL deliminated.
  
  @param[in] dst    The destination string, contents will be updated.
  @param[in] src    The source string
  @param[in] size   The maximum copy size (size of dst)

  @return
  The destination string pointer, dst.
*/
/*==========================================================================*/
char *glink_string_copy(char *dst, const char *src, uint32 size)
{
  (void)strlcpy(dst, src, size);

  return dst;
}
/*===========================================================================
  FUNCTION  glink_atomic_increment_uint32
===========================================================================*/
/**
  Atomically increment an integer value by 1.

  @param[in/out] ploc    The reference to the location of the integer to be 
                         incremented.

  @return    Incremented value.
*/
/*=========================================================================*/
uint32 glink_atomic_increment_uint32( volatile uint32 *ploc )
{
  return DALFW_LockedIncrementW(ploc);
}

/*===========================================================================
  FUNCTION  glink_atomic_decrement_uint32
===========================================================================*/
/**
  Atomically decrement an integer value by 1.

  @param[in/out] ploc    The reference to the location of the integer to be 
                         decremented.

  @return    Decremented value.
*/
/*=========================================================================*/
uint32 glink_atomic_decrement_uint32( volatile uint32 *ploc )
{
  return DALFW_LockedDecrementW(ploc);
}

/*===========================================================================
FUNCTION      glink_atomic_exchange_uint32

DESCRIPTION   Swaps the provided value with the value at the given location

PARAMETERS    plocation  - pointer to location holding the value
              new_value  - new value to put at the location

RETURN VALUE  The previous value at the locaiton.
===========================================================================*/
uint32 glink_atomic_exchange_uint32( uint32* plocation, uint32 new_value )
{
  return DALFW_LockedExchangeW(plocation, new_value);
}

/*===========================================================================
FUNCTION      glink_timer_init

DESCRIPTION   initialized a timer

PARAMETERS    ptimer  - pointer to glink_timer data structure
              cb      - callback to be invoked with timer expiration

RETURN VALUE  TRUE if successfull, FALSE otherwise.
===========================================================================*/
boolean glink_timer_init( glink_timer* ptimer, glink_timer_cb_fn cb, void* cb_data )
{
  return timer_def_osal(ptimer, 
                        &timer_non_defer_group, 
                        TIMER_FUNC1_CB_TYPE, 
                        cb, 
                        (time_osal_notify_data)cb_data) ==
         TE_SUCCESS;
}/* glink_timer_init */

/*===========================================================================
FUNCTION      glink_timer_start

DESCRIPTION   Starts a timer with specified duration

PARAMETERS    ptimer    - pointer to glink_timer data structure
              duration  - timer expiry duration in ms

RETURN VALUE  TRUE if successfull, FALSE otherwise.
===========================================================================*/
boolean glink_timer_start( glink_timer* ptimer, uint32 duration )
{
  return timer_set_64(ptimer, duration, 0, T_MSEC) == TE_SUCCESS;
}/* glink_timer_start */

/*===========================================================================
FUNCTION      glink_timer_stop

DESCRIPTION   Stops timer

PARAMETERS    ptimer    - pointer to glink_timer data structure

RETURN VALUE  none.
===========================================================================*/
void glink_timer_stop( glink_timer* ptimer )
{
  timer_stop(ptimer, T_MSEC, NULL);
}/* glink_timer_stop */

