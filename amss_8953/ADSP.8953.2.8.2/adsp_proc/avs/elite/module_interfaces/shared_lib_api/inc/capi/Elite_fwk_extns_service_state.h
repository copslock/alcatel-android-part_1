#ifndef ELITE_FWK_EXTNS_SERVICE_STATE_H
#define ELITE_FWK_EXTNS_SERVICE_STATE_H

/**
  @file Elite_fwk_extns_service_state.h

  @brief Parameters required to to know the service state.

  This file defines a framework extension for service state.
*/

/*==============================================================================
  Copyright (c) 2012-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*==============================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7/elite/module_interfaces/shared_lib_api/inc/capi/Elite_fwk_extns_service_state.h#1 $

  when        who      what, where, why
  --------    ---      -------------------------------------------------------
  11/26/15    kn       Publishing as a framework extension in
                                    elite/module_interfaces/api/inc/
==============================================================================*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "qurt_elite_types.h"

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

#define FWK_EXTN_SERVICE_STATE 0x00010E92
/**< Unique identifier to represent that module needs service state Framework
 *   extension.
 */

/*------------------------------------------------------------------------------
 * Parameter definitions - Needs implementation in module/library
 *----------------------------------------------------------------------------*/

#define PARAM_ID_SERVICE_STATE 0x00010E93
/**< Parameter id to send service state information from DSP service to module client.
 *   Default state of the module should be stop state.
 *   Payload structure: service_state_payload_t
 */

/**< Data structure to send service state information from service to module
 */
typedef struct service_state_payload_t
{
  uint16_t main_state;
  /**< This parameter represent main state of the service
   * Refer service_main_state_id_t for supported service states.
   */

  uint16_t sub_state;
  /**< This parameter is reserved for future use.
   * This parameter should be set to zero by service.
   * Declare new sub state in service_sub_state_id_t
   */

}service_state_payload_t;

/**< IDs for the supported service main state
 */
typedef enum service_main_state_id_t
{
   SERVICE_STOP_STATE = 0,
   /** Used to indicate that service is in stop state.
    */
   SERVICE_RUN_STATE,
   /** Used to indicate that service is in run state.
    */
   SERVICE_MAX_MAIN_STATE = 0xFFFF

} service_main_state_id_t;


/**< IDs for the supported service sub state
 */
typedef enum service_sub_state_id_t
{
   SERVICE_MAX_SUB_STATE = 0xFFFF

} service_sub_state_id_t;

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* #ifndef ELITE_FWK_EXTNS_SERVICE_STATE_H */
