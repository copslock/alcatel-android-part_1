#ifndef ELITE_INTF_EXTNS_TTY_H
#define ELITE_INTF_EXTNS_TTY_H

/* ======================================================================== */
/**
@file Elite_intf_extns_tty.h

@brief Interface extension header for vocoders or modules using TTY

  This file defines the parameters, events and other behaviors associated
  with vocoders or modules using TTY
*/

/* =========================================================================
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   09/28/15   ka      Initial Version.
   ========================================================================= */
#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/


#include "Elite_CAPI_V2_types.h"

#define INTF_EXTN_TTY 0x00010E80
/**< Unique identifier to represent custom interface extension for TTY */

/*------------------------------------------------------------------------------
 * Parameter IDs
 *----------------------------------------------------------------------------*/

#define TTY_PARAM_MODE               (0x00010E72)
/**< Parameter that identifies the TTY mode being set
     TTY mode provided via command VSM_CMD_SET_TTY_MODE will be passed to the module via this param id.
     Associated data structure: tty_param_mode_t */

typedef struct tty_param_mode_t tty_param_mode_t;

struct tty_param_mode_t
{
   uint32_t mode;
   /**< TTY mode based on tty_modes_t */
};

typedef enum tty_modes_t
{
   TTY_MODE_OFF = 0,
   TTY_MODE_HCO,
   TTY_MODE_VCO,
   TTY_MODE_FULL
} tty_modes_t;

/** The below parameters are specific to CTM block */

#define TTY_PARAM_CTM_RESYNC                   (0x00010E73)
/**< Specifies that the CTM block needs to resync. This parameter has no payload. */

#define TTY_PARAM_CTM_RX_ENQUIRY_DETECTED      (0x00010E74)
/**< Informs the CTM-TX module that CTM-RX module has detected an enquiry burst. This parameter has no payload. */

#define TTY_PARAM_CTM_RX_DETECTED              (0x00010E75)
/**< Informs the CTM-TX module that CTM-RX module has detected CTM character related information
     from far end. This parameter has no payload.  */

#define TTY_PARAM_CTM_TX_CHAR_TRANSMITTED     (0x00010E76)
/**< Informs the CTM-RX module that CTM-TX module has transmitted a CTM character.
     Associated data structure: tty_char_t */

#define TTY_PARAM_CHAR       (0x00010E77)
/**< Param ID to provide or retrieve character in UTF-8
     For CTM, this informs that CTM-TX/RX module has detected a CTM character which can be logged for debugging purpose.
     For OOBTTY, this is a character to be converted to baudot tone by Rx OOBTTY or to
     retrieve detected character from Tx OOBTTY.
     Associated data structure: tty_char_t */

/* Below structure can be used for both CTM and OOBTTY*/
typedef struct tty_char_t tty_char_t;

struct tty_char_t
{
   uint32_t tty_char;
   /**< Character in UTF-8 format*/
};

/*------------------------------------------------------------------------------
 * Events
 *----------------------------------------------------------------------------*/

#define TTY_EVT_RX_TTY_DETECTED            (0x00010E78)
/**< Event to be raised by rx module or decoder when rx tty is detected and half duplex
     muting on tx is desired.
     Associated data structure: rx_tty_detected_t*/

typedef struct rx_tty_detected_t rx_tty_detected_t;

struct rx_tty_detected_t
{
   uint32_t rx_tty_detected_flag;
   /**< This variable can have TRUE & FALSE values.
    *   TRUE = when tty is detected in rx path. FALSE = when TTY is not detected on rx path.
    *   Default value should be set to FALSE.
    */
};

/** The below events are specific to CTM block */
#define TTY_EVT_CTM_RX_ENQUIRY_DETECTED    (0x00010E79)
/**< Event raised by CTM-RX module to indicate that an enquiry burst was detected. This is
     then provided to the tx module. This event has no payload. */

#define TTY_EVT_CTM_RX_DETECTED            (0x00010E7A)
/**< Event raised by CTM-Rx module to indicate that a CTM character has been detected. This is
     then provided to the tx module. This event has no payload. */

#define TTY_EVT_CTM_CHAR_TRANSMITTED       (0x00010E7B)
/**<  Event raised by CTM-Tx module to indicate that a character was transmitted on tx path. This character
      is provided to CTM-Rx. This event has no payload. */

#define TTY_EVT_CHAR_DETECTED              (0x00010E7C)
/**<  1. This event is raised by, LTETTY TX module on tx path when character is detected. when this event is received by service, it logs the character received in the payload and send character to uppar(CVS) layer.
      2. This event is raised by CTM TX & RX moudule on rx path when character is detected. When this event is received by service it logs the character received in the payload of the event.
      
       Associated data structure: tty_char_t*/

#define TTY_EVT_LTETTY_RX_CHAR_ACCEPTED    (0x00010E7D)
/**<  Event raised by LTETTY-Rx module to indicate service that pushed rx character is accepted.
      Associated data structure: tty_char_t*/

#ifdef __cplusplus
}
#endif //__cplusplus
#endif
