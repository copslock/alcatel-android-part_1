#ifndef _LISTENPROCSVC_LAB_HANDLER_H_
#define _LISTENPROCSVC_LAB_HANDLER_H_
/**
@file ListenProcSvc_lab_handler.h
@brief This file contains api for circular buffer implementation for LAB
 */

/*===========================================================================
NOTE: The @brief description and any detailed descriptions above do not appear
      in the PDF.

      The core_mainpage.dox file contains all file/group descriptions that
      are in the output PDF generated using Doxygen and Latex. To edit or
      update any of the file/group text in the PDF, edit the
      core_mainpage.dox file or contact Tech Pubs.
===========================================================================*/

/*===========================================================================
  Copyright (c) 2013-2015 QUALCOMM technologies Inc (QTI).
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/listen/services/dynamic_svcs/listen_proc_svc/src/ListenProcSvc_lab_handler.h#6 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
3/11/14   SivaNaga      Created file.

========================================================================== */
/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/
/* System */
#include "qurt_elite.h"
#include "ListenProcSvc_circ_buffer.h"
#include "ListenProcSvc_Includes.h"

/** Structure for Look Ahead Buffering (LAB)*/
typedef struct
{
  int8_t        *circ_buf_ptr;                 /** Pointer to the start of the circular buffer memory*/
  circ_buf_struct_t circ_buf_struct;           /** circular buffer structure*/
  uint32_t      apps_wakeup_latency_ms;        /** time taken by apps to wake up from the time
                                                   LSM sends detection success event*/
  uint32_t      max_kwed_delay_bytes;          /** maximum delay in bytes for keyword end discovery*/
  bool_t        lab_enable;                    /** indicated whether LAB is enabled or disabled*/
  bool_t        is_reg_with_afe;               /** indicates whether registered with AFE or not*/
  uint32_t      kwed_position_bytes;           /** kwend position in bytes*/
  bool_t        start_lab;                     /** Flag to indicate if Circ buf need to be adjusted for KWED Position */
}lsm_lab_struct_t;

/*
 * Function for initializing Look Ahead Buffer(LAB)
 * functionality :
 * return : Indication of success or failure.
 */
ADSPResult listen_proc_lab_init(lsm_lab_struct_t *lab_struct_ptr,
		                        uint32_t sampling_rate,
		                        int16_t bits_per_sample,
		                        uint32_t max_kwed_delay_bytes);

/*
 * Function for starting Look Ahead Buffer(LAB)
 * functionality :
 * return : Indication of success or failure.
 */
ADSPResult listen_proc_lab_start(listen_proc_svc_t *session_ptr,
		                         uint16_t bytes_per_sample);

/*
 * Function for copying samples into circular buffer for Look Ahead Buffer(LAB)
 * functionality :
 * return : Indication of success or failure.
 */
ADSPResult listen_proc_lab_process(lsm_lab_struct_t *lab_struct_ptr,
		                           int8_t *inp_ptr,
		                           uint32_t bytes_to_write,
		                           lsm_callback_handle_t *cb_data_ptr,
		                           int8_t algo_status);

/*
 * Function for stopping Look Ahead Buffer(LAB)
 * functionality :
 * return : Indication of success or failure.
 */
ADSPResult listen_proc_lab_stop(listen_proc_svc_t *session_ptr);

/*
 * Function for doing set/getparam for Look Ahead Buffer(LAB)
 * functionality :
 * return : cpe error status
 */
ADSPResult listen_proc_lab_set_get_param(listen_proc_svc_t *session_ptr,
                                         uint32_t param_id,
                                         uint32_t param_size,
                                         int8_t *params_buffer_ptr,
                                         bool_t set_flag,
                                         uint32_t *actual_param_buf_len);

/*
 * Function for deinitializing Look Ahead Buffer(LAB)
 * functionality :
 * return : Indication of success or failure.
 */
ADSPResult listen_proc_lab_deinit(listen_proc_svc_t *session_ptr);

/*
  This function copies data from internal listen data buffer to client buffer

  @param lab_struct_ptr [in] This points to the instance of lsm_lab_struct_t
         out_buf_params_ptr [in/out] This points to the lsm_proc_svc_out_buf_param_t

  @return
  None

  @dependencies
  None.
 */
void listen_proc_lab_copy_data_to_out_buf(lsm_lab_struct_t *lab_struct_ptr,
		                                     lsm_proc_svc_out_buf_param_t *out_buf_params_ptr);
#endif /* _LISTENPROCSVC_LAB_HANDLER_H_ */
