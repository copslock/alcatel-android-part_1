/* ======================================================================== */
/**
@file capi_v2_voicewakeup_v2.h

   Header file to implement the Common Audio Processor Interface V2(CAPI_V2)
   for voicewakeup algorithm   */

/* =========================================================================
    Copyright (c) 2013 Qualcomm  Technologies, Inc (QTI).  All rights reserved.
   Qualcomm Technologies Proprietary and Confidential.
  ========================================================================== */


/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __CAPI_V2_VOICEWAKEUP_V2_H
#define __CAPI_V2_VOICEWAKEUP_V2_H

#include "Elite_CAPI_V2.h"
/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

capi_v2_err_t capi_v2_voicewakeup_v2_get_static_properties (capi_v2_proplist_t *init_set_prop_ptr,
                                                            capi_v2_proplist_t *static_prop_ptr);


capi_v2_err_t capi_v2_voicewakeup_v2_init (capi_v2_t* capi_v2_ptr,
                                           capi_v2_proplist_t      *init_set_prop_ptr);
#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__CAPI_V2_VOICEWAKEUP_V2_H

