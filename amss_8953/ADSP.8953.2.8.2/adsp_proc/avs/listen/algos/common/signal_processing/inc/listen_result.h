/*======================= COPYRIGHT NOTICE ==================================*]
[* Copyright (c) 2012 by Qualcomm Technologies, Inc.  All rights reserved. All data and   *]
[* information contained in or disclosed by this document is confidental and *]
[* proprietary information of Qualcomm Technologies, Inc and all rights therein are       *]
[* expressly reserved. By accepting this material the recipient agrees that  *]
[* this  material and the information contained therein is held in confidence*]
[* and in trust and will not be used, copied, reproduced in whole or in part,*]
[* nor its contents revealed in  any manner to others without the express    *]
[* written permission of Qualcomm Technologies, Inc.                                      *]
[*===========================================================================*]
[*****************************************************************************]
[* FILE NAME:   listen_result.h				    TYPE: C-header file          *]
[* DESCRIPTION: Listen result code						                     *]
[*   when       who     what, where, why                                     *]
[*   --------   ---     -----------------------------------------------------*]
[*   2012-08-30 acho    Initial revision                                     *]
[*****************************************************************************/

#ifndef _LISTEN_RESULT_H_
#define _LISTEN_RESULT_H_

// Listen result value definition
typedef enum 
{ 
	// result
	LISTEN_SUCCESS=0,	
	LISTEN_FAIL,
	LISTEN_NEEDMORE,
	LISTEN_NOTPREPARED,	
	// errors
	LISTEN_NULLREF,
	LISTEN_WRONGSIZE,
	LISTEN_WRONGINDEX,

} ListenResult;

#endif