#ifndef C_AAC_PACKETIZER_LIB_H
#define C_AAC_PACKETIZER_LIB_H

/* ========================================================================
  Aac Packetizer library wrapper header file

  @file CAacPacketizerLib.h
  This is a wrapper code for Aac Core Packetizer library.

  Copyright (c) 2015 QUALCOMM Technologies Incorporated.
  All rights reserved. Qualcomm Technologies Proprietary and Confidential.
  ====================================================================== */

/* =========================================================================
                             Edit History

   when       who           what, where, why
   --------   ---           ------------------------------------------------
  04/17/13    YW            Created file.
  05/04/15   adeepak        Capiv2 Upgrade

   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

#include "Elite_CAPI_V2.h"


/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

capi_v2_err_t aac_packetizer_capi_v2_get_static_properties (
        capi_v2_proplist_t *init_set_properties,
        capi_v2_proplist_t *static_properties);


capi_v2_err_t aac_packetizer_capi_v2_init (
        capi_v2_t*              _pif,
        capi_v2_proplist_t      *init_set_properties);


#ifdef __cplusplus
}
#endif //__cplusplus

#endif /* C_AAC_PACKETIZER_LIB_H */
