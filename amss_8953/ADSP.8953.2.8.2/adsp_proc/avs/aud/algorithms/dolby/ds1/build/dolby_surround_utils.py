def add_prefix_path_suffix_src_ext(prefix_path,sample_list):
    """ adds the root path to each path """
	
    new_list = []
	
    for each_dir in sample_list:
        new_path = prefix_path + each_dir
        new_list.append(new_path+"/*.c")
        new_list.append(new_path+"/*.cpp")
        new_list.append(new_path+"/*.S")

    return new_list
	
def add_prefix_path(prefix_path,sample_list):
    """ adds the root path to each path """
	
    new_list = []
	
    for each_dir in sample_list:
        new_path = prefix_path + each_dir
        new_list.append(new_path)

    return new_list	
