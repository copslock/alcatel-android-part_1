/* ========================================================================
  @file CeAc3DecoderLib.h

  Header file to implement the Interface for DDP decoder library.

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
 *========================================================================= */

/* =========================================================================
                             Edit History

     when        who                  what, where, why
   --------   ---------     ------------------------------------
   3/16/2015   hbansal                Initial creation

   ========================================================================= */


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

#ifndef CAPI_V2_DDP_HEADER_H_
#define CAPI_V2_DDP_HEADER_H_


#include "Elite_CAPI_V2.h"

#define CAPI_V2_PARAM_ID_DDP_CONVERTER_PROFILE 0xFFFFFFF1
#define CAPI_V2_DDP_CONVERTER_PROFILE_ID 7

#ifdef __cplusplus
extern "C" {
#endif

capi_v2_err_t __attribute__ ((visibility ("default"))) ddp_dec_capi_v2_get_static_properties (capi_v2_proplist_t *init_set_properties,capi_v2_proplist_t *static_properties);

capi_v2_err_t __attribute__ ((visibility ("default"))) ddp_dec_capi_v2_init (capi_v2_t *_pif, capi_v2_proplist_t *init_set_properties);

#ifdef __cplusplus
} //extern "C"
#endif

#endif /* CAPI_V2_DDP_HEADER_H_ */
