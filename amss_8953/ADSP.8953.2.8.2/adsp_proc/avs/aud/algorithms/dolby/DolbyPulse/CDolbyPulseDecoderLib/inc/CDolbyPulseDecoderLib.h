#ifndef C_DOLBYPULSE_DECODER_LIB_H
#define C_DOLBYPULSE_DECODER_LIB_H

/* ========================================================================
   DolbyPulse decoder library wrapper header file

  *//** @file CDolbyPulseDecoder.h
  This is a wrapper code for DolbyPulse Core decoder library.

  Copyright (c) 2011 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who          what, where, why
   --------   ---         ------------------------------------------------------
   06/24/11    WJ        Created file.
  ========================================================================= */

#include "Elite_CAPI.h"


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CDolbyPulseDecoderLib : public ICAPI {
    public:

	  void *m_pDecState;

      enum eDolbyPulseAACBitStreamType
      {
         eDOLBYPULSE_BITSTREAM_TYPE_UNKNOWN = 0
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_ADIF = 1
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_ADTS = 2
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_LATM = 3
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_LATM_OUTOFBAND_CONFIG = 4
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_LOAS = 5
         ,eDOLBYPULSE_BITSTREAM_TYPE_AAC_RAW = 6
         ,eDOLBYPULSE_BITSTREAM_TYPE_MPG = 7
      };

    private:
        /**
         * Default Constructor of CDolbyPulseDecoderLib
         */
        CDolbyPulseDecoderLib ( );


    public:
        /* =======================================================================
         *                          Public Function Declarations
         * ======================================================================= */

        /**
        * Constructor of CDolbyPulseDecoderLib that creats an instance 
        * of decoder lib and returns the error code 
        */

        CDolbyPulseDecoderLib ( ADSPResult    &nResult );


        /**
         * Destructor of CDolbyPulseDecoderLib
         */
        ~CDolbyPulseDecoderLib ( );

        /*************************************************************************
         * CDolbyPulseDecoderLib Methods
         *************************************************************************/

        /**
         * Initialize the core decoder library
         *
         * @return     success/failure is returned
         */
        virtual int Init ( CAPI_Buf_t* pParams );

        /**
         * Re initialize the core decoder library in the case of repositioning or
         * when full initialization is not required
         *
         * @return     success/failure is returned
         */
        virtual int ReInit ( CAPI_Buf_t* pParams );

        /**
         * Gracefully exit the core decoder library
         *
         * @return     success/failure is returned
         */
        virtual int End ( void );

        /**
         * Get the value of the EaacPLus decoder parameters
         *
         * @param[in]   nParamIdx      Enum value of the parameter of interest
         * @param[out]  pnParamVal     Desired value of the parameter of interest
         *
         * @return   Success/fail
         */
        virtual int GetParam ( int nParamIdx, int *pnParamVal );

        /**
         * Get the value of the EaacPLus decoder parameters
         *
         * @param[in]   nParamIdx      Enum value of the parameter of interest
         * @param[out]  nPrarmVal      Desired value of the parameter of interest
         *
         * @return   Success/fail
         */
        virtual int SetParam ( int nParamIdx, int nParamVal );

        virtual int Process ( const CAPI_BufList_t* pInBitStream,
                                    CAPI_BufList_t*       pOutSamples,
                                    CAPI_Buf_t*       pOutParams );

    };


#endif /* C_DOLBYPULSE_DECODER_LIB_H */

