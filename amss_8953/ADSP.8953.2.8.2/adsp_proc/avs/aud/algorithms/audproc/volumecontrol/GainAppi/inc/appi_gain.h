/* ======================================================================== */
/**
@file appi_gain.h

   Header file to implement the Audio Post Processor Interface for Tx/Rx Tuning Gain block
*/

/* =========================================================================
  Copyright (c) 2013-2014 QUALCOMM Technology Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   10/26/10   c_liyenh      Introducing Audio Post Processor Interface for Tx/Rx Tuning Gain block
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_GAIN_H
#define __APPI_GAIN_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"
#include "adsp_error_codes.h"
#include "adsp_adm_api.h"
#include "adsp_asm_api.h"


   /**
    * This function get size of APPI definition structure to Gain block
    * @param[in] params_ptr - meta data-interface pointer
    * @param[out] size_ptr - return appi data structure size
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_gain_getsize(
      /*[in]*/ const appi_buf_t*    params_ptr,
      /*[out]*/ uint32_t*            size_ptr);

   /**
    * This function format APPI definitions to Gain block
    * @param[in] _pif - IAPPI pseudo-interface pointer
    * @param[in] pInfo - setup information in metadata (custom) form
    * @param[in] pInFormat - input format
    * @param[in] pOutFormat - output format
    * @param[in] pfInPlace - in-place computation indicator
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_gain_init(
      /*[in]*/ appi_t*                   _pif,
      /*[out]*/ bool_t*                    pfInPlace,
      /*[in]*/ const appi_format_t*   pInFormat,
      /*[in]*/ appi_format_t*         pOutFormat,
      /*[in]*/ appi_buf_t*                pInfo);


#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_GAIN_H

