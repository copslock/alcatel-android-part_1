/* ======================================================================== */
/**
@file appi_softvolumecontrols.h

   Header file to implement the Audio Post Processor Interface for Soft
   Volume Controls
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   11/17/10   ss      qurt_elite placement new/delete operators
   11/17/10   ss      Introducing APPI Rev B
   10/20/10   ss      Introducing Audio Post Processor Interface for Soft
                      Volume Controls
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_CSOFTVOLUMECONTROLS_H
#define __APPI_CSOFTVOLUMECONTROLS_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
#define AUDPROC_PARAM_ID_SOFT_PAUSE_START                (0x0002FFFF)
#define AUDPROC_PARAM_ID_SOFT_PAUSE_SET_RAMP_ON_RESUME   (0x0003FFFF)

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_softvolumecontrols_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_softvolumecontrols_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);


#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_CSOFTVOLUMECONTROLS_H

