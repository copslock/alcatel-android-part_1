/*==============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

/* =============================================================================
                             Edit History
   when       who           what, where, why
   --------   ---           -----------------------------------------------
   11/05/14   shridhar      Creation
   ===========================================================================*/

/*------------------------------------------------------------------------
 * Include files and Macro definitions
 * -----------------------------------------------------------------------*/
#ifndef APPI_MONO2STEREO_UPMIXER_H
#define APPI_MONO2STEREO_UPMIXER_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif

ADSPResult appi_mono2stereo_upmixer_getsize(
            const appi_buf_t* params_ptr,
            uint32_t* size_ptr);

ADSPResult appi_mono2stereo_upmixer_init(
            appi_t* _pif,
            bool_t* is_inplace_ptr,
            const appi_format_t* in_format_ptr,
            appi_format_t* out_format_ptr,
            appi_buf_t* info_ptr);

#ifdef __cplusplus
}
#endif

#endif // APPI_MONO2STEREO_UPMIXER_H

