/* ======================================================================== */
/**
@file appi_iir_common.h

   Header file to implement the common functions for IIR filter object.
*/

/* =========================================================================
  Copyright (c) 2011 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   03/24/11   WJ      Initial creation
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_IIR_COMMON_H
#define __APPI_IIR_COMMON_H

#include "CMultiBandIIRLib.h"


/**
  * This function sets params of the IIR filter
  * @param[in] pMultiBandIIR - pointer to IIR filter
  * @param[in] pMBIIRDataStruct - pointer to IIR filter data
  *       structure
  * @param[in] pPPIIRFilterStruct - pointer to IIR history
  * @param[in] uNumStages - number of stages of IIR filter
  * @param[in] param_id - parameter ID
  * @param[in] params_ptr - pointer to enable/disable parameters
  * @param[in] is_pan_needed - flag to indicate if Pan
  *       parameters is needed for IIR filter
  *
  * @return
  * - ADSP_EOK - The initialization was successful.
  * - error code - There was an error which needs to propagate.
  */
ADSPResult appi_iir_set_param(
                     CMultiBandIIR*          pMultiBandIIR,
                     MultiBandIIRCfgStruct*  pMBIIRDataStruct,
                     int32_t*                pPPIIRFilterStruct,
                     uint32_t                uNumStages,
                     uint32_t                param_id,
                     const appi_buf_t*       params_ptr,
                     bool_t                  is_pan_needed,
                     bool_t*                 fIIRChanged);


/**
  * This function gets params of the IIR filter
  * @param[in] pMBIIRDataStruct - pointer to IIR filter data
  *       structure
  * @param[in] pPPIIRFilterStruct - pointer to IIR history
  * @param[in] param_id - parameter ID
  * @param[in] params_ptr - pointer to enable/disable parameters
  *
  * @return
  * - ADSP_EOK - The initialization was successful.
  * - error code - There was an error which needs to propagate.
  */
ADSPResult appi_iir_get_param(
                     MultiBandIIRCfgStruct*  pMBIIRDataStruct,
                     int32_t*                pPPIIRFilterStruct,
                     uint32_t                param_id,
                     appi_buf_t*             params_ptr);

#endif //__APPI_IIR_COMMON_H

