#ifndef LIMITER_CALIBRATION_API_H
#define LIMITER_CALIBRATION_API_H
/*============================================================================
  @file limiter_calibration_api.h

  Calibration (Public) API for Zero-Crossing Low Distortion Limiter

        Copyright (c) 2008-2013 Qualcomm Technology Incorporated.
        All Rights Reserved.
        Qualcomm Confidential and Proprietary
============================================================================*/
#include "AudioComdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*----------------------------------------------------------------------------
   Parameters with IDs
----------------------------------------------------------------------------*/
#define LIMITER_PARAM_LIB_VER    (0)   // ** param: library version
typedef int32 limiter_version_t;       //    access: get only

#define LIMITER_PARAM_MODE       (1)   // ** param: processing mode
typedef enum limiter_mode_t {          //    access: get & set
   MAKEUPGAIN_ONLY = 0,                //    0: apply makeup gain, no delay
   NORMAL_PROC,                        //    1: normal processing
} limiter_mode_t;

#define LIMITER_PARAM_BYPASS     (2)   // ** param: bypass
typedef int32 limiter_bypass_t;        //    access: get & set
                                       //    range: [1: bypass 0: normal]

#define LIMITER_PARAM_TUNING     (3)   // ** param: per channel tuning param
                                       //    access: get & set
typedef struct limiter_tuning_t {      //    note: runtime ok, no mem flush
   int32             ch_idx;           //    channel index for this set 
   int32             threshold;        //    threshold (16bit: Q15; 32bit:Q27)
   int32             makeup_gain;      //    make up gain (Q8)
   int32             gc;               //    recovery const (Q15)
   int32             max_wait;         //    max wait (Q15 value of sec)
} limiter_tuning_t;

#define LIMITER_PARAM_RESET      (4)   // ** param: reset internal memories 
                                       //    access: set only

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LIMITER_CALIBRATION_API_H */
