/* ======================================================================== */
/**
@file appi_peakmeter.h

   Header file to implement the Audio Post Processor Interface for 
   applying gain to input signal
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   07/01/11   yw      APPI wrapper
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_PEAKMETER_H
#define __APPI_PEAKMETER_H

#include "Elite_APPI.h"
#include "adsp_audproc_api.h"

#ifdef APPI_EXAMPLE_STANDALONE
#include "appi_util.h"
#else
#include "qurt_elite.h"
#endif
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/



//     Number of samples per milliseceond. Per channel according to the data structure //
#define PEAKMETER_SAMPLES_PER_MS                    48



//peaks





/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_peakmeter_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_peakmeter_init( 
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);


#ifdef __cplusplus
}
#endif
#endif //__APPI_EXAMPLE_GAIN_H

