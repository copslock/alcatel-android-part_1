/******************************************************************************
* (C)1996 - 2011 DTS, Inc.
*
* Company                       : Tata Elxsi
* File                          : DTS_DwnMixApi.h
* Author                        : TEL Audio Team
* Version                       : 1.0
* Date                          : 15-03-2012
* Operating Environment         : Windows 2000/XP
* Compiler with Version Number  : Visual Studio 2008
* Description                   : DownMix Api Definition header file
* Revisers Name                 :
******************************************************************************/
#ifndef __DTS_DWNMIX_API_H__
#define __DTS_DWNMIX_API_H__


#define MAXDYNDWNINDX       32		/* Maximum Down Mix Coefficients */
#define	MAXNUM_CHANNELS    8
#define DTS_DWNMIX_CTXT_SIZE 704
#define MAXLBRCHANNELS	6

typedef struct _tagDTSDecExtDwnMixAudioBuffer{
	uint32*	pui32AudioFrame;			/* PCM Data buffer pointer */
	uint32	ui32NumSamples;				/* Number of Samples */
	uint32	ui32SamplingFrequency;		/* Sampling Frequency */
	uint32  ui32BitsPerSample;			/* Bits per sample */
	uint32	ui32ChMask;					/* Channel Mask */
	bool8	bLtRtFlag;					/* DTS REL 25.27 */
}stDTSExtDwnMixAudioBuffer;

typedef void* stDTSDwnMixContext;

#define _TAG_DTS_DWN_MIX_COMMON_DEC_SPECIFIED_CONFIG \
      bool8 bLBR;          /* Whether LBR or CORE Down Mix */ \
      uint8 ui8AMODE; \
      bool8 bLfeMix2Frnt;

#define _TAG_DTS_DWN_MIX_BC_DEC_RELATED_CONFIG \
      uint8 ui8LFEF;  \
      bool8 bAuxDynamCoeffFlag; \
      uint8 ui8NumDwnMixCodeCoeffs; \
      uint16   ui16SubSubFrameIndex;   \
      uint8 ui8DualMonoChPresent;   \
      uint16   aui16DwnMixCodeCoeffs[MAXDYNDWNINDX];  \
      uint8 ui8AuxPrmChDwnMixType;  \
      uint8 ui8AuxChPrevHierChSet;  \
      uint32   ui32FrameLength;  \


#define _TAG_DTS_DWN_MIX_LBR_DEC_RELATED_CONFIG \
      bool8    bEmbDwnMix; \
      uint32   ui32ChannelMask;  /* Channel Mask */ \
      uint32   ui32OutChannelMask;  /* Out Channel Mask */ \
      uint16   ui16SubFrameStep; \
      uint32   ui32SpkrOutMask;

typedef struct _tagDTSDwnMixBcDecSpecifiedConfig
{
   _TAG_DTS_DWN_MIX_COMMON_DEC_SPECIFIED_CONFIG
   _TAG_DTS_DWN_MIX_BC_DEC_RELATED_CONFIG
} stDTSDwnMixBcDecSpecifiedConfig;

typedef struct _tagDTSDwnMixLbrDecSpecifiedConfig
{
   _TAG_DTS_DWN_MIX_COMMON_DEC_SPECIFIED_CONFIG
   _TAG_DTS_DWN_MIX_BC_DEC_RELATED_CONFIG
   _TAG_DTS_DWN_MIX_LBR_DEC_RELATED_CONFIG
} stDTSDwnMixLbrDecSpecifiedConfig;

typedef struct _tagDTSDwnMixConfig {
   _TAG_DTS_DWN_MIX_COMMON_DEC_SPECIFIED_CONFIG
   _TAG_DTS_DWN_MIX_BC_DEC_RELATED_CONFIG
   _TAG_DTS_DWN_MIX_LBR_DEC_RELATED_CONFIG
   bool8 bInit;
   bool8 bDownMixEnabled;
   bool8 bDwnMixFlag;
   bool8 bDwnMixLtRt;      /* Whether Lt Rt Downmixing */
   uint16   ui16SubFrameCount;   /* LBR Sub Sub Frame Count */

}stDTSDwnMixConfig;

typedef enum eDTSDecDwnMix_Result {
	DTSDWNMIX_SUCCESS=0,					/* Success */
	DTSDWNMIX_ERROR,						/* Failure */
	DTSDWNMIX_BAD_PARAM
}eDTSDwnMixResult;


typedef enum 
{
	DTS_CHCFG_DWNMIX_ERROR              = -1,
	DTS_CHCFG_DWNMIX_CENTER				= 0x00000001,
	DTS_CHCFG_DWNMIX_LEFT				= 0x00000002,
	DTS_CHCFG_DWNMIX_RIGHT				= 0x00000004,
	DTS_CHCFG_DWNMIX_SRRD_LEFT			= 0x00000008,
	DTS_CHCFG_DWNMIX_SRRD_RIGHT			= 0x00000010,
	DTS_CHCFG_DWNMIX_LFE_1				= 0x00000020,
	DTS_CHCFG_DWNMIX_SRRD_CENTER		= 0x00000040,
	DTS_CHCFG_DWNMIX_REAR_SRRD_LEFT		= 0x00000080,
	DTS_CHCFG_DWNMIX_REAR_SRRD_RIGHT	= 0x00000100,
	DTS_CHCFG_DWNMIX_SIDE_SRRD_LEFT		= 0x00000200,
	DTS_CHCFG_DWNMIX_SIDE_SRRD_RIGHT	= 0x00000400,
	DTS_CHCFG_DWNMIX_LEFT_CENTER		= 0x00000800,
	DTS_CHCFG_DWNMIX_RIGHT_CENTER		= 0x00001000,
	DTS_CHCFG_DWNMIX_HIGH_LEFT			= 0x00002000,
	DTS_CHCFG_DWNMIX_HIGH_CENTER		= 0x00004000,
	DTS_CHCFG_DWNMIX_HIGH_RIGHT			= 0x00008000,
	DTS_CHCFG_DWNMIX_LFE_2				= 0x00010000,
	DTS_CHCFG_DWNMIX_LEFT_WIDE			= 0x00020000,
	DTS_CHCFG_DWNMIX_RIGHT_WIDE			= 0x00040000,
	DTS_CHCFG_DWNMIX_TOP_CENTER_SRRD	= 0x00080000,
	DTS_CHCFG_DWNMIX_HIGH_SIDE_LEFT		= 0x00100000,
	DTS_CHCFG_DWNMIX_HIGH_SIDE_RIGHT	= 0x00200000,
	DTS_CHCFG_DWNMIX_HIGH_REAR_CENTER	= 0x00400000,
	DTS_CHCFG_DWNMIX_HIGH_REAR_LEFT		= 0x00800000,
	DTS_CHCFG_DWNMIX_HIGH_REAR_RIGHT	= 0x01000000,
	DTS_CHCFG_DWNMIX_LOW_FRONT_CENTER	= 0x02000000,
	DTS_CHCFG_DWNMIX_LOW_FRONT_LEFT		= 0x04000000,
	DTS_CHCFG_DWNMIX_LOW_FRONT_RIGHT	= 0x08000000,
	DTS_CHCFG_DWNMIX_RESERVED4			= 0x10000000,
	DTS_CHCFG_DWNMIX_RESERVED5			= 0x20000000,
	DTS_CHCFG_DWNMIX_RESERVED6			= 0x40000000,
	DTS_CHCFG_DWNMIX_MONO				= 0x00000001,
	DTS_CHCFG_DWNMIX_DUAL_MONO_A		= 0x00000002,
	DTS_CHCFG_DWNMIX_DUAL_MONO_B		= 0x00000004,
	DTS_CHCFG_DWNMIX_SUM				= 0x00000008,
	DTS_CHCFG_DWNMIX_DIFF				= 0x00000010,
	DTS_CHCFG_DWNMIX_LT					= 0x00000020,
	DTS_CHCFG_DWNMIX_RT					= 0x00000040,
	DTS_CHCFG_DWNMIX_UNDEFINED			= 0x00000000
}DTSCHANNELCONFIG_DWNMIX;


typedef enum  {							/*SPKR_REMAP */
	DTSSPKROUT_DWNMIX_MASK_C 		= 0x00001, 	// DTSSPKROUT_C,
	DTSSPKROUT_DWNMIX_MASK_LR 		= 0x00002,	// DTSSPKROUT_LR,
	DTSSPKROUT_DWNMIX_MASK_LsRs 	= 0x00004,	// DTSSPKROUT_LsRs,
	DTSSPKROUT_DWNMIX_MASK_LFE1 	= 0x00008,	// DTSSPKROUT_LFE1,
	DTSSPKROUT_DWNMIX_MASK_Cs 		= 0x00010,	// DTSSPKROUT_Cs,
	DTSSPKROUT_DWNMIX_MASK_LhRh 	= 0x00020,	// DTSSPKROUT_LhRh,
	DTSSPKROUT_DWNMIX_MASK_LsrRsr	= 0x00040,	// DTSSPKROUT_LsrRsr,
	DTSSPKROUT_DWNMIX_MASK_Ch 		= 0x00080,	// DTSSPKROUT_Ch,
	DTSSPKROUT_DWNMIX_MASK_Oh 		= 0x00100,	// DTSSPKROUT_Oh,
	DTSSPKROUT_DWNMIX_MASK_LcRc		= 0x00200,	// DTSSPKROUT_LcRc,
	DTSSPKROUT_DWNMIX_MASK_LwRw		= 0x00400,	// DTSSPKROUT_LwRw,
	DTSSPKROUT_DWNMIX_MASK_LssRss	= 0x00800,	// DTSSPKROUT_LssRss,
	DTSSPKROUT_DWNMIX_MASK_LFE_2	= 0x01000,	// DTSSPKROUT_LFE_2,
	DTSSPKROUT_DWNMIX_MASK_LhsRhs	= 0x02000,	// DTSSPKROUT_LhsRhs,
	DTSSPKROUT_DWNMIX_MASK_Chr		= 0x04000,	// DTSSPKROUT_Chr,
	DTSSPKROUT_DWNMIX_MASK_LhrRhr	= 0x08000,	// DTSSPKROUT_LhrRhr,
	DTSSPKROUT_DWNMIX_MASK_Clf		= 0x10000,	// DTSSPKROUT_Clf,
	DTSSPKROUT_DWNMIX_MASK_LlfRlf	= 0x20000,	// DTSSPKROUT_LlfRlf,
	DTSSPKROUT_DWNMIX_MASK_LtRt		= 0x40000,	// DTSSPKROUT_LtRt,
}DTSSPKROUT_DWNMIX_MASK;

typedef enum _tag_eDmx_ParamID
{
	DWNMIX_Enable,				 		/* check Down Mix module is enabled */
	DWNMIX_Delay,						/*	Delay factor */
}eDmx_ParamID;

eDTSDwnMixResult DTSDecDwnMix_Reinit(stDTSDwnMixContext pvDwnMixHandle);

eDTSDwnMixResult DTSDecDwnMix(
							  stDTSDwnMixContext pvDwnMixHandle,
							  stDTSDwnMixConfig *pstDTSDwnMixCfg,
							  stDTSDwnMixContext pstDTSDecAudio
							  );

eDTSDwnMixResult DTSDecInitExtDwnMix(stDTSDwnMixContext pvDwnMixHandle,
									 stDTSDwnMixConfig *pstDTSDwnMixCfg
								     );
eDTSDwnMixResult DTSDecDwnMix_GetSize(uint32 *pui32Size);

eDTSDwnMixResult DTSDecDwnMix_GetParam(stDTSDwnMixContext pvDwnMixHandle,
								int32 param_id,
								int32 *params);

#endif

/************************ End of file: DTS_DwnMixApi.h************************/
