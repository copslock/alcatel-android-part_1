#ifndef DWN_MIX_API_H
#define DWN_MIX_API_H

/*======================================================================

 DTS_DwnMixLibApi --- H E A D E R F I L E

 GENERAL DESCRIPTION
 The header file includes Interface functions for DTS Downmixer code
 to be compatible with Matrix Mixer

 EXTERNALIZED FUNCTIONS

 DTSDwnMixInit()
 DTSDwnMixGetSize()
 DTSDwnMixSetParam()
 DTSDwnMixProcess()


 Copyright (c) 2012
 by Qualcomm Technologies, Inc. All Rights Reserved.
======================================================================*/

/*=====================================================================

 EDIT HISTORY FOR MODULE

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.


 when who what, where, why
 -------- --- --------------------------------------------------------
 -
 05/17/12 vg First version of the API to DTS Downmix Lib
======================================================================*/



/*=====================================================================
 Constants
======================================================================*/


/*=====================================================================
 Macros
======================================================================*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* DTS Library state */
#define DTS_DWNMIX_INIT_STATE      0
#define DTS_DWNMIX_PARAM_STATE     1
#define DTS_DWNMIX_RUN_STATE       2


/* DTS Setparam parameters */
#define COEFF_CMD                  1
/* Ensure that below two API defines are in sync with the defines in elite/static_svcs/AudioDevMgr/inc private APIs */
#define DOWNMIXER_PARAM_ID_MIX_LFE_TO_FRONT              0x00010338
#define DOWNMIXER_PARAM_ID_STEREO_MODE              0x00010339


/* DTS Max no of input and output channels */
#define MAX_SAMPLES_PER_CHANNEL    512

#define DTS_DWN_OUT_Q_FACTOR       23
#ifndef __qdsp6__
#define ELITE_32BIT_PCM_Q_FORMAT   27
#define PCM_16BIT_Q_FORMAT         15	/* Q15 */
#define MSG(x,y,z)
#define MSG_SSID_QDSP6
#define DBG_HIGH_PRIO
#define DBG_ERROR_PRIO
#endif /* __QDSP6__ */ 

typedef enum _DTSMixerChType
{
    DTS_MIXER_PCM_CH_NONE = 0,   //  None
    DTS_MIXER_PCM_CH_L =    1,   //  Front-left (FL)
    DTS_MIXER_PCM_CH_R =    2,   //  Front-right (FR)
    DTS_MIXER_PCM_CH_C =    3,   //  Front-center (FC)
    DTS_MIXER_PCM_CH_LS =   4,   //  Left-surround (LS)
    DTS_MIXER_PCM_CH_RS =   5,   //  Right-surround (RS)
    DTS_MIXER_PCM_CH_LFE_1 =6,   //  Low frequency effects (LFE-1)
    DTS_MIXER_PCM_CH_CS =   7,   //  Center-surround (CS, RC)
    DTS_MIXER_PCM_CH_LB =   8,   //  Left-back (LB, RL)
    DTS_MIXER_PCM_CH_RB =   9,   //  Right-back (RB, RR)
    DTS_MIXER_PCM_CH_TS =   10,  //  Top-surround (TS)
    DTS_MIXER_PCM_CH_CVH =  11,  //  Center-vertical-height (CVH)
    DTS_MIXER_PCM_CH_TFC =  DTS_MIXER_PCM_CH_CVH,  // Top Front Center (TFC)
    DTS_MIXER_PCM_CH_MS =   12,  //  Mono-surround (MS)
    DTS_MIXER_PCM_CH_FLC =  13,  //  Front left of center (FLC)
    DTS_MIXER_PCM_CH_FRC =  14,  //  Front Right of center (FRC)
    DTS_MIXER_PCM_CH_RLC =  15,  //  Rear left of center (RLC)
    DTS_MIXER_PCM_CH_RRC =  16,  //  Rear right of center (RRC)
    DTS_MIXER_PCM_CH_CB  =  DTS_MIXER_PCM_CH_CS,  //  Center-back (CB)
    DTS_MIXER_PCM_CH_LFE_2 =17,  //  Low frequency effects (LFE-2)
    DTS_MIXER_PCM_CH_SL  =  18,  //  Side-Left (SL)
    DTS_MIXER_PCM_CH_SR  =  19,  //  Side-Right(SR)
    DTS_MIXER_PCM_CH_TFL =  20,  //  Top Front Left (TFL)
    DTS_MIXER_PCM_CH_LVH =  DTS_MIXER_PCM_CH_TFL,  //  Left Vertical Height (LVH)
    DTS_MIXER_PCM_CH_TFR =  21,  //  Top Front Right (TFR)
    DTS_MIXER_PCM_CH_RVH =  DTS_MIXER_PCM_CH_TFR,  //  Right Vertical Height (RVH)
    DTS_MIXER_PCM_CH_TC  =  22,  //  Top Center (TC)
    DTS_MIXER_PCM_CH_TBL =  23,  //  Top Back Left (TBL)
    DTS_MIXER_PCM_CH_TBR =  24,  //  Top Back Right (TBR)
    DTS_MIXER_PCM_CH_TSL =  25,  //  Top Side Left (TSL)
    DTS_MIXER_PCM_CH_TSR =  26,  //  Top Side Right (TSR)
    DTS_MIXER_PCM_CH_TBC =  27,  //  Top Back Center (TBC)
    DTS_MIXER_PCM_CH_BFC =  28,  //  Bottom Front Center (BFC)
    DTS_MIXER_PCM_CH_BFL =  29,  //  Bottom Front Left (BFL)
    DTS_MIXER_PCM_CH_BFR =  30,  //  Bottom Front Right (BFR)
    DTS_MIXER_PCM_CH_LW  =  31,  //  Left Wide (LW)
    DTS_MIXER_PCM_CH_RW  =  32,  //  Right Wide (RW)
    DTS_MIXER_PCM_CH_LSD =  33,  //  Left Side Direct (LSD)
    DTS_MIXER_PCM_CH_RSD =  34,  //  Right Side Direct (RSD)
    DTS_MIXER_PCM_CH_MAX_TYPE = 35
} DTSMixerChType;


/* Per channel mapping of DTS */
typedef enum  {                     /*SPKR_REMAP*/
   LIB_DTSSPKROUT_MASK_C      = 0x00001,  // DTSSPKROUT_C,
   LIB_DTSSPKROUT_MASK_LR     = 0x00002,  // DTSSPKROUT_LR,
   LIB_DTSSPKROUT_MASK_LsRs   = 0x00004,  // DTSSPKROUT_LsRs,
   LIB_DTSSPKROUT_MASK_LFE1   = 0x00008,  // DTSSPKROUT_LFE1,
   LIB_DTSSPKROUT_MASK_Cs     = 0x00010,  // DTSSPKROUT_Cs,
   LIB_DTSSPKROUT_MASK_LhRh   = 0x00020,  // DTSSPKROUT_LhRh,
   LIB_DTSSPKROUT_MASK_LsrRsr = 0x00040,  // DTSSPKROUT_LsrRsr,
   LIB_DTSSPKROUT_MASK_Ch     = 0x00080,  // DTSSPKROUT_Ch,
   LIB_DTSSPKROUT_MASK_Oh     = 0x00100,  // DTSSPKROUT_Oh,
   LIB_DTSSPKROUT_MASK_LcRc   = 0x00200,  // DTSSPKROUT_LcRc,
   LIB_DTSSPKROUT_MASK_LwRw   = 0x00400,  // DTSSPKROUT_LwRw,
   LIB_DTSSPKROUT_MASK_LssRss = 0x00800,  // DTSSPKROUT_LssRss,
   LIB_DTSSPKROUT_MASK_LFE_2  = 0x01000,  // DTSSPKROUT_LFE_2,
   LIB_DTSSPKROUT_MASK_LhsRhs = 0x02000,  // DTSSPKROUT_LhsRhs,
   LIB_DTSSPKROUT_MASK_Chr    = 0x04000,  // DTSSPKROUT_Chr,
   LIB_DTSSPKROUT_MASK_LhrRhr = 0x08000,  // DTSSPKROUT_LhrRhr,
   LIB_DTSSPKROUT_MASK_Clf    = 0x10000,  // DTSSPKROUT_Clf,
   LIB_DTSSPKROUT_MASK_LlfRlf = 0x20000,  // DTSSPKROUT_LlfRlf,
   LIB_DTSSPKROUT_MASK_LtRt   = 0x40000,  // DTSSPKROUT_LtRt,
}LIB_DTSSPKROUT_MASK;

/* Speaker out channel mask */
enum LIB_DTSSPKRREMAP
{
   LIB_DTSSPKRREMAP_NATIVE                      = 0,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1            = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_CS              = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_Cs,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_OH              = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_Oh,
   LIB_DTSSPKRREMAP_L_R_LS_RS_LH_RH             = LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LhRh,
   LIB_DTSSPKRREMAP_L_R_LS_RS_LC_RC             = LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LcRc,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_LHS_RHS    = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_LhsRhs,
   LIB_DTSSPKRREMAP_C_L_R_LFE1_LSR_RSR_LSS_RSS  = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_LsrRsr|LIB_DTSSPKROUT_MASK_LssRss,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_LH_RH      = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_LhRh,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_LSR_RSR    = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_LsrRsr,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_CS_CH      = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_Cs|LIB_DTSSPKROUT_MASK_Ch,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_CS_OH      = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_Cs|LIB_DTSSPKROUT_MASK_Oh,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_LW_RW      = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_LwRw,
   LIB_DTSSPKRREMAP_C_L_R_LS_RS_LFE1_CS         = LIB_DTSSPKROUT_MASK_C|LIB_DTSSPKROUT_MASK_LR|LIB_DTSSPKROUT_MASK_LsRs|LIB_DTSSPKROUT_MASK_LFE1|LIB_DTSSPKROUT_MASK_Cs,
};
#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */
/*=====================================================================
 Typedefs
======================================================================*/



/*=====================================================================
 Structures
======================================================================*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */




#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */
/*=====================================================================
 Functions
======================================================================*/
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


void DTSDwnMixGetSize( uint32 *pui32Size );        /* The size of the DTS Downmixer Library state struct */

int32 DTSDwnMixInit( void *pCMState,                /* Pointer to the DTS Downmixer library state struct */
                                uint32 numInputChannels,       /* Number of Input channels to DTS Downmixer */
                                DTSMixerChType *inputChannels,  /* Pointer to the channel map of input channels */
                                uint32 numOutputChannels,      /* Number of Output channels to DTS Downmixer */
                                DTSMixerChType *outputChannels, /* Pointer to the channel map of output channels */
                                uint32 dataBitWidth);          /* BitWidth of input channels */

void DTSDwnMixProcess( void *pCMState,             /* Pointer to the DTS Downmixer library state struct */
                                   void **output,              /* Pointer -> to array of pointers of output buffer */
                                   void **input,               /* Pointer -> to array of pointers of input buffer */
                                   uint32 numSamples );        /* Number of samples for DTS downmixing process */

//dtq - check Deepan's code for requirement of nParamIdx
int32 DTSDwnMixSetParam ( int32 nParamIdx,          /* Setparams index command */
                                     void *pCMState,           /* Pointer to the DTS Downmixer library state struct */
                                     void* nParamVal ); /* DTS Config strucutre with coeffs */

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* DWN_MIX_API_H */
