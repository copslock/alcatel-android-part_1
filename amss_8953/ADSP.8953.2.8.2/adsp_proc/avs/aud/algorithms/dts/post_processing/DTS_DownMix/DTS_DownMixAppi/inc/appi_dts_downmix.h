/* ======================================================================== */
/**
@file appi_dts_downmix.h

   Header file for the DTS downmix APPI module.
*/

/* =========================================================================
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   08/09/13   rkc      Created the DTS downmix APPI
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DTS_DOWNMIX_H
#define __APPI_DTS_DOWNMIX_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/** Max num channels supported by DTS downmix (input or
 *  output) */
#define APPI_DTS_DOWNMIX_MAX_CHAN     (8)

/** Param ID for configuring the input/output channel map of the DTS mixer. */
#define DTS_DOWNMIX_PARAM_ID_IN_OUT_CH_MAP      (0)

/** Param ID for configuring DTS mixer.  */
#define DTS_DOWNMIX_PARAM_ID_CONFIG             (1)

/** Payload for DTS_DOWNMIX_PARAM_ID_IN_OUT_CH_MAP */
typedef struct _appi_dts_downmix_param_inout_chmap_
{
   uint32_t      num_in_ch;                               /**< Num input channels. */
   uint8_t       in_ch_map[APPI_DTS_DOWNMIX_MAX_CHAN];    /**< Input channel mappings. */
   uint32_t      num_out_ch;                              /**< Num output channels. */
   uint8_t       out_ch_map[APPI_DTS_DOWNMIX_MAX_CHAN];   /**< Output channel mapping. */
   uint32_t      bit_width;                               /**< Bits per sample for input/output. */
} appi_dts_downmix_param_inout_chmap_t;

/** Payload for DTS_DOWNMIX_PARAM_ID_CONFIG */
typedef struct _appi_dts_downmix_param_config_
{
   int32_t      config_id;                               /**< Configuration ID. */
   void*        payload_ptr;                             /**< Pointer to parameter payload. */
} appi_dts_downmix_param_config_t;

/*------------------------------------------------------------------------
  Function name: appi_dts_downmix_getsize
  Returns the memory required by this algorithm.
 * -----------------------------------------------------------------------*/
ADSPResult appi_dts_downmix_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

/*------------------------------------------------------------------------
  Function name: appi_dts_downmix_init
  Initializes the DTS downmix library as a pass through module.
 * -----------------------------------------------------------------------*/
ADSPResult appi_dts_downmix_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif //__cplusplus
       
#endif //__APPI_DTS_DOWNMIX_H


