#ifndef C_DTS_ENC_PACKETIZER_LIB_H
#define C_DTS_ENC_PACKETIZER_LIB_H

/* ========================================================================
   DTS encoder library wrapper header file

  *//** @file ComboDTSEncPacketizerlib.h
  This is a wrapper code for DTS Core encoder library.
  the function in this file are called by the CDTSEncoder media module.
  It is derived from CAudioProcLib class

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   23/04/12   SS     Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
/* Sizes of DTS Encoder Input and Output buffers in bytes */
#define DTSTRANSENC_WINDOW	512     // encode samples window size

#define UINT32_SIZE			4
//#define DTSENC_INPUT_SIZE_PER_CHANNEL (DTSTRANSENC_WINDOW * UINT32_SIZE)
#define DTSENC_OUTPUT_SIZE (DTSTRANSENC_WINDOW * UINT32_SIZE)

class ComboDTSEncPacketizerlib: public ICAPI
	{
	private:
		int CreateComboDTSEncPacketizer();
#if ((defined __hexagon__) || (defined __qdsp6__))
		int mEncOutBuf[DTSENC_OUTPUT_SIZE] __attribute__ ((__aligned__(8)));
#else
		int mEncOutBuf[DTSENC_OUTPUT_SIZE];
#endif

	public:

		void *m_pEncState;

		enum eComboParamIdx
       {
		   eComboPCMR = eIcapiMaxParamIndex,			// Bits per sample of source PCM
		   eComboFS,											// Sampling rate
           eComboBitrate,  								// Output bit rate
		   eComboActualOutputSize,
		   eComboPacketizerBitstreamId
       };

		/* =======================================================================
		*                          Public Function Declarations
		* ======================================================================= */


		/**
		* Constructor of ComboDTSEncPacketizerlib
		*/
		ComboDTSEncPacketizerlib ( );

		/**
		* Constructor of ComboDTSEncPacketizerlib
		*/
		ComboDTSEncPacketizerlib ( ADSPResult &result );

		/**
		* Destructor of ComboDTSEncPacketizerlib
		*/
		virtual ~ComboDTSEncPacketizerlib ( );

		/*************************************************************************
		* CAudioProcLib Methods
		*************************************************************************/

		/**
		* Initialize the core encoder library
		*
		* @return     success/failure is returned
		*/
		virtual int CDECL Init ( CAPI_Buf_t* pParams );

		/**
		* Re initialize the core encoder library in the case of repositioning or
		* when full initialization is not required
		*
		* @return     success/failure is returned
		*/
		virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

		/**
		* Gracefully exit the core encoder library
		*
		* @return     success/failure is returned
		*/
		virtual int CDECL End ( void );

		/**
		* Get the value of the DTS encoder parameters
		*
		* @param[in]   nParamIdx      Enum value of the parameter of interest
		* @param[out]  pnParamVal     Desired value of the parameter of interest
		*
		* @return   Success/fail
		*/
		virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );


		/**
		* Set the value of the DTS encoder parameters
		*
		* @param[in]   nParamIdx      Enum value of the parameter of interest
		* @param[out]  nPrarmVal      Desired value of the parameter of interest
		*
		* @return   Success/fail
		*/
		virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

		/**
		* Encode audio bitstream and produce one frame worth of samples
		*
		* @param[in]   pInBitStream     Pointer to input bit stream
		* @param[out]  pOutSamples      Pointer to output samples
		* @param[out]  pOutParams       Pointer to output parameters
		*
		* @return     Success/failure
		*/
		virtual int CDECL Process ( const CAPI_BufList_t* pInSamples,
								   CAPI_BufList_t*       pOutBitStream,
								   CAPI_Buf_t*       pOutParams );

};


#endif /* C_DTS_ENC_PACKETIZER_LIB_H */

