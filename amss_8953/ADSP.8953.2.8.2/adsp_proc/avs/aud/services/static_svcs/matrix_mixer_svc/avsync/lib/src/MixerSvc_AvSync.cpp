/**
@file MixerSvc_AvSync.cpp
@brief This file defines matrix mixer AV-Sync utilities.
 */

/*========================================================================
Copyright (c) 2015 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/static_svcs/matrix_mixer_svc/avsync/lib/src/MixerSvc_AvSync.cpp#2 $

when              who   what, where, why
--------              ---      -------------------------------------------------------
06/24/2015  kr      Created file.
========================================================================== */

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc_AvSync.h"
#include "MixerSvc.h"
#include "MixerSvc_Util.h"
#include "MixerSvc_InPortHandler.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "avsync_lib.h"
#include "adsp_media_fmt.h"
#include "adsp_asm_data_commands.h"
#include "adsp_adm_api.h"
#include "AudDevMgr_PrivateDefs.h"
#include "adsp_asm_session_commands.h"
#include "MixerSvc_MsgHandlers.h"
#include "MixerSvc_Util.h"
#include "EliteMsg_AdmCustom.h"
#include "MixerSvc_InPortHandler.h"
#include "AudDynaPPSvc.h"
#include "adsp_mtmx_strtr_api.h"
#include "MixerSvc_OutPortHandler.h"
#include "adsp_private_api_ext.h"

ADSPResult MtMx_InPortToHonorTimestamp(This_t *me, uint32_t unInPortID)
{
	ADSPResult                 result;
	MatrixInPortInfoType       *pCurrentInPort = me->inPortParams[unInPortID];
	elite_msg_data_buffer_t*   pInputBuf;
	int64_t                    unDataRcvdInUsec;
	uint32_t ullCurrentDevicePathDelay = 0, ullCurrentDevicePathDelayComp = 0;
	mt_mx_sampleslip_t	    *pSampleSlip        = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	avsync_rendering_decision_t rendering_decision = RENDER;
	int64_t delta = 0;

	//Make sure that the pCurrentInPort->unTopPrioOutPort is valid.
	if(pCurrentInPort->unTopPrioOutPort < MT_MX_MAX_OUTPUT_PORTS)
	{
		MatrixOutPortInfoType       *pTopPrioOutPort = me->outPortParams[pCurrentInPort->unTopPrioOutPort];

		//Make sure that the punAFEDelay, punCoppDelay, punMtMxOutDelay are all valid.
		if((NULL != pTopPrioOutPort) && (NULL != pTopPrioOutPort->punAFEDelay) && (NULL != pTopPrioOutPort->punCoppBufDelay) && (NULL != pTopPrioOutPort->punCoppAlgDelay) && (NULL != pCurrentInPort->punMtMxInDelay))
		{
			ullCurrentDevicePathDelay = *(pTopPrioOutPort->punAFEDelay) + *(pTopPrioOutPort->punCoppBufDelay) + *(pTopPrioOutPort->punCoppAlgDelay) + *(pCurrentInPort->punMtMxInDelay);

			// The session time calculation happens in the output port context. So the rendering decision should only take into account the delay from the
			// output port down. We should thus remove the delays of the acc buf and the internal buf. Based on when the buffer was sent down on the device,
			// this can vary from (frame duration) to 2*(frame duration). Here we take the average.
			ullCurrentDevicePathDelayComp = ullCurrentDevicePathDelay - (pTopPrioOutPort->unFrameDurationInUsec.int_part) - 0.5*(pTopPrioOutPort->unFrameDurationInUsec.int_part);
		}
	}

	pCurrentInPort->pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);
	pInputBuf = pCurrentInPort->pInputBuf;
	pCurrentInPort->bIsTimeStampValid = (bool_t)asm_get_timestamp_valid_flag(pInputBuf->nFlag);

	if (TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless)
	{
		//For gapless playback, reset session clk to zero before checking the timestamp validity flag so that in case the TS validity flag
		//is FALSE, session clk for the new stream begins from 0
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Reset Gapless SessionTime to 0", me->mtMxID, unInPortID);
		MtMx_ResetSessionTimeClock(me, unInPortID);
	}

	/* if i/p buffer timestamp is not valid, simply return. In such case:
      a. Render the buffer immediately (skip the below logic of checking whether buffer has to be held or dropped),
      b. Do not increment session clock if silence is inserted (done in MtMx_OutPortToHonorInPortsTimestamps), and
      c. Ignore timestamps till the end of playback
	 */
	if (!pCurrentInPort->bIsTimeStampValid)
	{
		if((TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS) ||
				(TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless))
		{
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = FALSE;
			pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = FALSE;
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
		}
		pCurrentInPort->bForceCheckTimeStampValidity = FALSE; //Set this to FALSE and never check for TS validity ever for this session (exception: 2nd gapless stream)
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: [TS flag = 0], ignoring all future TS's", me->mtMxID, unInPortID);

		// Since the input port timestamp validity has changed state from TRUE -> FALSE we need to trigger a rescan
		// on the output ports for any connected input ports with a valid timestamp.
		MtMx_ScanOutputPortsForInputsWithValidTimestamps(me);
		return ADSP_EOK;
	}

	/* The following procedure will calculate the current frame duration in microseconds. This procedure will
	 * avoid accumulating error over time due to truncation for frame sizes that aren't integer multiples
	 * of the sampling rate. The procedure will:
		1. Update number of sample received (over last one second).
      2. Calculate data received in microseconds from total number of samples received (over last one second).
      3. Calculate data received in microseconds for current frame by subtracting previous number.
         This will give current frame duration in microseconds.
      4. Roll over frame size and frame duration in microseconds every one second.
	 */

	//Update number of sample received (over last one second).
	pCurrentInPort->unDataRcvdInSamples += ((pInputBuf->nActualSize) / (pCurrentInPort->unNumChannels * pCurrentInPort->unBytesPerSample));

	//Calculate data received in microseconds from number of samples received (over last one second).
	unDataRcvdInUsec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->unDataRcvdInSamples, pCurrentInPort->unSampleRate);

	//Calculate data received in micro sec for current frame by subtracting previous number.
	//This will give current frame duration in usec.
	unDataRcvdInUsec -= pCurrentInPort->unDataRcvdInUsec.int_part;

	// roll over frame size and frame duration in usec to within one second
	pCurrentInPort->unDataRcvdInUsec.int_part += unDataRcvdInUsec;
	if (pCurrentInPort->unDataRcvdInSamples >= pCurrentInPort->unSampleRate)
	{
		pCurrentInPort->unDataRcvdInSamples -= pCurrentInPort->unSampleRate;
	}
	if (pCurrentInPort->unDataRcvdInUsec.int_part >= 1000000)
	{
		pCurrentInPort->unDataRcvdInUsec.int_part -= 1000000;
	}

	//Update timestamps
	pCurrentInPort->ullIncomingBufferTS = pInputBuf->ullTimeStamp;

#ifdef MT_MX_EXTRA_DEBUG
	MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu] usec, TSValidity: %d, current frame dur [%lu, %lu] usec", me->mtMxID, unInPortID,
			(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid,
			(uint32_t)(unDataRcvdInUsec>>32), (uint32_t)unDataRcvdInUsec);
#endif

	if(ASM_SESSION_CMD_RUN_START_TIME_RUN_AT_ABSOLUTE_TIME != pCurrentInPort->pInPortAvSync->unStartFlag &&
			ASM_SESSION_CMD_RUN_START_TIME_RUN_WITH_DELAY != pCurrentInPort->pInPortAvSync->unStartFlag)
	{
		avsync_lib_make_rendering_decision(pCurrentInPort->pInPortAvSync->pAVSyncLib,
				pInputBuf->ullTimeStamp,
				(uint64_t)0,
				(bool_t)TRUE,
				&delta,
				&rendering_decision);

		//If this is the first TS received after a RUN cmd with RUN_IMMEDIATE, then obtain session time from i/p buffer TS
		if (TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS)
		{
			//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats && HOLD != rendering_decision)
			{
				//Note that ADSP_ENOTREADY denotes that start and end of render windows haven't been set.
				if(ADSP_EOK ==avsync_lib_update_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib, delta, unDataRcvdInUsec))
				{
					pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = TRUE;
				}
			}

			//Update the drift, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift)
			{
				avsync_lib_update_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib, pCurrentInPort->ullIncomingBufferTS, (uint32_t)unDataRcvdInUsec);
				pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift = TRUE;
			}

			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = FALSE;
			pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = FALSE;
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
			pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = TRUE;
			pCurrentInPort->ullTimeStampForStcBaseInUsec.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampForStcBaseInUsec.frac_part = 0;
			//Since we are maintaining a base value for STC, the num samples for update
			//should also be reset when the RUN command is issued.
			pCurrentInPort->ullNumSamplesForStcUpdate = 0;
			avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
			uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
			avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));
			return ADSP_EOK;
		}

		//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
		if(HOLD == rendering_decision)
		{
			pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part = (-delta);
			pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.frac_part = 0;

			//If hold duration is less than the 1ms, treat it as immediate.
			if(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
			{
				(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
				delta = 0;
				rendering_decision = RENDER;
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
			}
		}

		//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
		if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats)
		{
			avsync_lib_update_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib, delta, unDataRcvdInUsec);
			pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = TRUE;
		}

		if (RENDER == rendering_decision)
		{
			uint64_t proposed_expected_session_clock;
			avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock);
			proposed_expected_session_clock += unDataRcvdInUsec;
			avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));
			if(0 != delta && pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk == FALSE)
			{
				avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
			}

			//Update the drift, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift)
			{
				avsync_lib_update_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
				pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift = TRUE;
			}
			pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = TRUE;

			return ADSP_EOK;
		}
		else if (DROP == rendering_decision)
		{
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

			//Update timestamps
			pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
			pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
			pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

			MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
					(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

			//Drop the buffer
			if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
			{
				MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
			}
			pCurrentInPort->numBufReturned++;
			pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;

			//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
			ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib);
			if (ADSP_FAILED(commit_result) && ADSP_EBADPARAM != commit_result)
			{
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in i/p port %lu context, failed to commit AVSync stats with result=%d", me->mtMxID, unInPortID, commit_result);
			}

			//No holding
			(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
			return ADSP_EFAILED;
		}
		else
		{
			MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for MSW: %lu LSW: %lu usec",
					me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

			//If hold duration is less than the 1ms, treat it as immediate.
			MtMx_RemoveInputPortFromWaitMask(me, unInPortID);
			pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
			pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
			return ADSP_EFAILED;
		}
	}
	else
	{
		//ASM_SESSION_CMD_RUN_START_TIME_RUN_AT_ABSOLUTE_TIME
		//ASM_SESSION_CMD_RUN_START_TIME_RUN_WITH_DELAY
		//IMPORTANT: Start time has already been verified to be positive at this point.
		//If client is using relative TS, the first buffer will have 0 TS.
		//If client is using absolute TS, the first buffer may have non 0 absolute TS.

		//Account for any partly filled input buffer at this point.
		uint32_t unSamplesAlreadyFilled = pCurrentInPort->unInPortPerChBufSize - pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
		uint64_t ullTimeOffsetUsec = 0;

		if(0 != pCurrentInPort->unSamplesPer1Msec)
		{
			ullTimeOffsetUsec = MT_MX_SAMPLES_TO_USEC(unSamplesAlreadyFilled, pCurrentInPort->unSampleRate);
		}
		else
		{
			ullTimeOffsetUsec = MT_MX_SAMPLES_TO_USEC(unSamplesAlreadyFilled, MT_MX_SAMPLING_RATE_48000);
		}

		if(ullTimeOffsetUsec)
		{
			MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: OffsetUsec: [%lu %lu], unSamplesAlreadyFilled: %lu",
					me->mtMxID, unInPortID, (uint32_t)(ullTimeOffsetUsec >> 32), (uint32_t)(ullTimeOffsetUsec), unSamplesAlreadyFilled);
		}

		//Special case handling when SSLib was introduced
		if(unSamplesAlreadyFilled == pCurrentInPort->unInPortPerChBufSize && NULL != pSampleSlip->pSampleSlipAppi)
		{
			/*MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: unSamplesAlreadyFilled: %lu matches i/p port sample size. This will be processed later.",
					me->mtMxID, unInPortID, unSamplesAlreadyFilled);*/
			pCurrentInPort->pInPortAvSync->bShouldActiveInputPortBeReProcessed = TRUE;
			(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync;

			//Stop further processing from this port.
			MtMx_RemoveInputPortFromWaitMask(me, unInPortID);
			return ADSP_EFAILED;
		}

		avsync_lib_update_stc_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib);

		if(TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS)
		{
			//For local STC based rendering, this mode will be used at all times.
			//That means, each buffer will be treated as the first buffer.
			//Therefore, bShouldSessionTimeBeDerivedFromNextTS will always be TRUE for local STC.
			//For default rendering, this mode will be used only for the 1st buffer after a RUN command.
			avsync_lib_make_rendering_decision(pCurrentInPort->pInPortAvSync->pAVSyncLib,
					pInputBuf->ullTimeStamp,
					(uint64_t)ullCurrentDevicePathDelayComp+ullTimeOffsetUsec,
					FALSE,
					&delta,
					&rendering_decision);

			if(HOLD == rendering_decision)
			{
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part = (-delta);
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.frac_part = 0;

				//If hold duration is less than the 1ms, treat it as immediate.
				if(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
					delta = 0;
					rendering_decision = RENDER;
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
				}
			}

			//Compare Wall clock (+ Device latency) <==> Start time + TS
			if((RENDER == rendering_decision))
			{

				if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats)
				{
					avsync_lib_update_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib, delta, unDataRcvdInUsec);
					pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = TRUE;
				}

				//Update the drift, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift)
				{
					avsync_lib_update_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
					pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift = TRUE;
				}

				//Update [session time, absolute time] pair.
				avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
				uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
				avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, EXPECTED_SESSION_CLOCK, &proposed_expected_session_clock, sizeof(proposed_expected_session_clock));
				avsync_lib_update_absolute_time(pCurrentInPort->pInPortAvSync->pAVSyncLib,ullCurrentDevicePathDelay+ullTimeOffsetUsec,FALSE);

				//No holding required.
				(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
				uint32_t current_avsync_rendering_decision =ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT;
				avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,RENDERING_DECISION_TYPE,&current_avsync_rendering_decision);
				//Sync done.
				if(ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT == current_avsync_rendering_decision)
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				}
				else
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  TRUE;
				}
				pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
				pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = TRUE;

				return ADSP_EOK;
			}
			else if(DROP == rendering_decision)
			{
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

				//Update timestamps
				pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
				pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
				pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
						(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

				//No holding required.
				(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
				//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats)
				{
					avsync_lib_update_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib, delta, unDataRcvdInUsec);
					pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = TRUE;
				}

				//Past TS, Drop the buffer.
				if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
				}

				pCurrentInPort->numBufReturned++;
				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
				return ADSP_EFAILED;
			}
			else //(HOLD == rendering_decision)
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));
				//If hold duration is less than the rounding < 1ms, treat it as immediate.
				//i/p port is in a held state.
				pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;

				//Update Expected ST for the next buffer.
				uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
				avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));

				//Stop further processing from this port.
				MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

				//Sync (partially) done.
				uint32_t avsync_rendering_decision_type =ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT;
				avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,RENDERING_DECISION_TYPE,&avsync_rendering_decision_type);
				if(ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT == avsync_rendering_decision_type)
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				}
				else
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  TRUE;
				}
				pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless =  FALSE;

				return ADSP_EFAILED;
			}
		}
		else
		{
			//Steady state operation (for default rendering decision mode)
			//For local STC based rendering, it will treat every buffer as the first buffer. Therefore, it will never hit this logic.
			avsync_lib_make_rendering_decision(pCurrentInPort->pInPortAvSync->pAVSyncLib,
					pInputBuf->ullTimeStamp,
					(uint64_t)0,
					(bool_t)TRUE,
					&delta,
					&rendering_decision);

			if(HOLD == rendering_decision)
			{
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part = (-delta);
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.frac_part = 0;

				//If hold duration is less than the 1ms, treat it as immediate.
				if(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
					delta = 0;
					rendering_decision = RENDER;
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
				}
			}

			//If the stat window ends are setup, call an update on the AVSync stat library, only in steady state operation
			if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats)
			{
				avsync_lib_update_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib, delta, unDataRcvdInUsec);
				pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = TRUE;
			}
			//If past by less than or equal to llRenderWindowEnd, process immediately
			if(RENDER == rendering_decision)
			{
				//Update [session time, absolute time] pair.
				avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
				avsync_lib_increment_expected_session_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib,unDataRcvdInUsec);

				//No holding required.
				(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync

				//Sync done.
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
				pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = TRUE;

				//Update the drift, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift)
				{
					avsync_lib_update_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
					pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift = TRUE;
				}

				return ADSP_EOK;
			}
			else if(DROP == rendering_decision)
			{
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

				//Update timestamps
				pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
				pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
				pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
						(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

				//No holding required.
				(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync

				//Past TS, Drop the buffer.
				if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
				}

				pCurrentInPort->numBufReturned++;
				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;

				//Commit the AVSync update changes for this i/p port for this dropped buffer immediately.
				ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib);
				if (ADSP_FAILED(commit_result) && ADSP_EBADPARAM != commit_result)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in i/p port %lu context, failed to commit AVSync stats with result=%d", me->mtMxID, unInPortID, commit_result);
				}

				return ADSP_EFAILED;
			}
			else// if HOLD ==  rendering decision
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for MSW: %lu LSW: %lu usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));
				//If hold duration is less than the 1ms, treat it as immediate.
				//Update expected ST.
				avsync_lib_increment_expected_session_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib,unDataRcvdInUsec);
				//i/p port is in a held state.
				pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;

				//Stop further processing from this port.
				MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

				//Out of sync.
				(void)MtMx_ResetStrClkSync(me, unInPortID); //AV-Sync

				return ADSP_EFAILED;
			}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_AddInterpolatedSamples(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];

	// Pointer to current sample in upstream peer output buffer and write location in local buffer
	int8_t *pRdLoc = pCurrentInPort->pCurrentSample;
	int8_t *pWrLoc = pCurrentInPort->pWrLoc;

	uint32_t num_bytes_to_interpolate_per_ch = (MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER * pCurrentInPort->unBytesPerSample);
	uint32_t num_bytes_per_ch_in_port_buf = (pCurrentInPort->unInPortPerChBufSize * pCurrentInPort->unBytesPerSample);
	uint32_t in_buf_ch_spacing_in_bytes = (pCurrentInPort->nNumRemainingSamplesPerCh + pCurrentInPort->nNumSamplesUsedPerCh) \
			* pCurrentInPort->unBytesPerSample;

	for (uint16_t j = 0; j < pCurrentInPort->unNumChannels; j++)
	{
		memscpy((void*)pWrLoc, num_bytes_to_interpolate_per_ch,(void*)pRdLoc, num_bytes_to_interpolate_per_ch);
		pRdLoc += in_buf_ch_spacing_in_bytes;
		pWrLoc += num_bytes_per_ch_in_port_buf;
	}

	pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown = TRUE;
	pCurrentInPort->pWrLoc += num_bytes_to_interpolate_per_ch;

	return ADSP_EOK;
}

ADSPResult MtMx_DropSamples(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->pCurrentSample += (MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER * pCurrentInPort->unBytesPerSample);
	pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->nNumRemainingSamplesPerCh -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->nNumSamplesUsedPerCh += MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown = TRUE;
	return ADSP_EOK;
}

ADSPResult MxAr_OutPortToHonorInPortsTimestamps(This_t *me, uint32_t unOutPortID)
{
	int32_t                 unInPortID;
	MatrixInPortInfoType    *pCurrentInPort;
	MatrixOutPortInfoType   *pCurrentOutPort = me->outPortParams[unOutPortID];
	uint32_t                inPortsMask = pCurrentOutPort->inPortsMask;
	elite_msg_data_buffer_t*   pInputBuf;

	uint32_t bufDelay = pCurrentOutPort->unFrameDurationInUsec.int_part * pCurrentOutPort->unNumOutputBufs;
	uint32_t ullCurrentDownstreamDelay = *(pCurrentOutPort->punAFEDelay) + *(pCurrentOutPort->punCoppBufDelay) + *(pCurrentOutPort->punCoppAlgDelay) + bufDelay;

	while (inPortsMask)
	{
		unInPortID = Q6_R_ct0_R(inPortsMask);
		inPortsMask ^= 1 << unInPortID;
		pCurrentInPort = me->inPortParams[unInPortID];

		if (unOutPortID == pCurrentInPort->unTopPrioOutPort)
		{
			avsync_lib_update_stc_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib);

			if((FALSE == pCurrentInPort->bHasFirstSampleAccumulated) &&
					(INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER != pCurrentInPort->inPortState) &&
					(INPUT_PORT_STATE_WAITING_TO_BE_READY != pCurrentInPort->inPortState) &&
					(INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE != pCurrentInPort->inPortState))
			{
				continue;
			}

			avsync_rendering_decision_t rendering_decision = RENDER;
			avsync_lib_make_simple_rendering_decision(pCurrentInPort->pInPortAvSync->pAVSyncLib, pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part, &rendering_decision);

			switch (pCurrentInPort->inPortState)
			{
			case INPUT_PORT_STATE_ACTIVE:
			{
				// if i/p buffer timestamp is not valid, and if i/p port is starving for data, do not
				// increment the session clock when silence is inserted
				if (!pCurrentInPort->bIsTimeStampValid && !(pCurrentOutPort->accInPortsMask & (1 << unInPortID)))
				{
					break;
				}
				// if an EOS was sent down, no longer increment i/p port's session time
				if (FALSE == pCurrentInPort->bIsSessionUnderEOS)
				{
					//Update session time and absolute time
					MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

					if(pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown)
					{
						pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj--;
						if (0 == pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj)
						{
							pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled = FALSE;
							pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown = FALSE;
						}
					}

					//Update timestamps
					if(pCurrentInPort->bIsTimeStampValid)
					{
						mt_mx_copy_time_value(&pCurrentInPort->ullTimeStampProcessed, pCurrentInPort->ullTimeStampAtAccumulation);
					}

#ifdef MT_MX_EXTRA_DEBUG
					MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu ProcessedTS [%lu, %lu] samples, TSValidity: %d", me->mtMxID, unInPortID,
							(uint32_t)(pCurrentInPort->ullTimeStampProcessed.int_part>>32),(uint32_t)(pCurrentInPort->ullTimeStampProcessed.int_part),
							pCurrentInPort->bIsTimeStampValid);
#endif

					//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
					ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib);
					if (ADSP_FAILED(commit_result) && (ADSP_EBADPARAM != commit_result))
					{
						MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
					}
				}

				//For cases where stream clk is not yet sync'd with session clk, and i/p buffers are being dropped because their TS's are in the past,
				//increment the expected str_time by the amount of samples rendered down in the form of silence
				// if session time > expected stream time, expected stream time = session time
				if (!pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk)
				{
					uint64_t ullStaleEOSSamplesuSec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->ullStaleEosSamples, pCurrentInPort->unSampleRate);
					avsync_lib_increment_expected_session_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib, (pCurrentInPort->unFrameDurationInUsec.int_part - ullStaleEOSSamplesuSec));
				}

				pCurrentInPort->ullStaleEosSamples = 0;
				break;
			}
			case INPUT_PORT_STATE_WAITING_TO_BE_READY:
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

				if (RENDER == rendering_decision)
				{
					pCurrentInPort->inPortState = INPUT_PORT_STATE_READY;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_READY;
					MtMx_AddInputPortToWaitMask(me, unInPortID);
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: o/p port %lu moving i/p port %lu to ready state", me->mtMxID, unOutPortID, unInPortID);
				}
				else
				{
					// Need to use output port frame duration as input port media type is not received yet
					mt_mx_decrement_time(&pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

				break;
			}

			case INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE:
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

				if (RENDER == rendering_decision)
				{
					pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
					MtMx_InPortToUpdateWaitMask(me, unInPortID);
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: o/p port %lu moving i/p port %lu to active state", me->mtMxID, unOutPortID, unInPortID);
				}
				else
				{
					mt_mx_decrement_time(&pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

				break;
			}

			case INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER:
			{
				//Update session time and absolute time
				MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

				pInputBuf = pCurrentInPort->pInputBuf;
				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Entering Hold Dur [%lu %lu],frame duration %lu usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part),
						(uint32_t)pCurrentInPort->unFrameDurationInUsec.int_part);

				//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
				ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib);
				if (ADSP_FAILED(commit_result) && NULL!=pCurrentInPort->pInPortAvSync->pAVSyncLib)
				{
					MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
				}

				//Adjust ullInBufHoldDuration if less than 1ms
				if(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
				}

				if (pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part <= pCurrentInPort->unFrameDurationInUsec.int_part)
				{
					//Update session time and expected stream time
					avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
					avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,EXPECTED_SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));

					//I/p port transition back to ACTIVE
					pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
					pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
					MtMx_InPortToUpdateWaitMask(me, unInPortID);

					//Trigger i/p port processing by honoring i/p TS
					MtMx_InPortToHonorTimestamp(me, unInPortID);

					if(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part > 0)
					{
						MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBufferHold from o/p port", me->mtMxID, unInPortID);
						(void)MtMx_MsgDataBufferHold(me, unInPortID, pCurrentInPort);
					}
					else
					{
						//Explictly set the hold duration to 0
						(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
						MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBuffer from o/p port", me->mtMxID, unInPortID);
						MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);
					}

					//Mark this input port as just moved to ACTIVE (to skip the EOS and Underflow handling, that follow this function on the outporthandler)
					pCurrentInPort->bInputPortJustMovedFromHeldToActive = TRUE;

					//Now, sync is complete
					pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = TRUE;
				}
				else if(FALSE == pCurrentInPort->bIsLocalBufEmpty)
				{
					//if(pCurrentInPort->ullInBufHoldDuration >= pCurrentInPort->unFrameDurationInUsec) && (FALSE == pCurrentInPort->bIsLocalBufEmpty)

					//In the normal hold case, there maybe some residual data from previous buffer sitting in the internal buffer
					//of the audio matrix input port. In such a case, it would be better to send the rest of the previous data
					//out before proceeding with the hold duration for the current buffer, to avoid an "island" type glitch.
					(void)MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);
				}
				else //if(pCurrentInPort->ullInBufHoldDurationInSamples >= pCurrentInPort->unInPortPerChBufSize)
				{
					mt_mx_decrement_time(&pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec, pCurrentInPort->unFrameDurationInUsec);
				}

				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Exiting Hold Dur [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));

				break;
			}
			case INPUT_PORT_STATE_PAUSED:
			{
				// During paused state, increment session time ONLY for as long as i/p port receives i/p buffers.
				// When it doesn't receive i/p buffer anymore, stop incrementing session clock.
				if (pCurrentOutPort->accInPortsMask & (1 << unInPortID))
				{
					//Update session time and absolute time
					MxArUpdateInputPortSessionTime(me, unInPortID, pCurrentInPort, pCurrentOutPort, ullCurrentDownstreamDelay);

					if(pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown)
					{
						pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj--;
						if (0 == pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj)
						{
							pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled = FALSE;
							// session clock adjustment has completed, output port can go back to incrementing session clock normally
							pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown = FALSE;
						}
					}

					//Update timestamps
					if(pCurrentInPort->bIsTimeStampValid)
					{
						mt_mx_copy_time_value(&pCurrentInPort->ullTimeStampProcessed, pCurrentInPort->ullTimeStampAtAccumulation);
					}

					//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
					ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib);
					if (ADSP_FAILED(commit_result) && NULL!=pCurrentInPort->pInPortAvSync->pAVSyncLib)
					{
						MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in o/p port %lu context, i/p port %lu failed to commit AVSync stats with result=%d", me->mtMxID, unOutPortID, unInPortID, commit_result);
					}
				}
				pCurrentInPort->ullStaleEosSamples = 0;
				break;
			}
			}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_InPortToCheckReInitSampleSlipLibrary(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t      *pSampleSlip     = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	uint32_t                        unSampleSlipLibSize = 0;
	ADSPResult                  result                        = ADSP_EOK;

	if (NULL == pSampleSlip->pSampleSlipAppi)
	{
		//First time allocation. i.e Init. Get the size of SampleSlip lib.
		result = appi_ss_getsize(NULL, &unSampleSlipLibSize);

		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, APPI SampleSlip failed to get size w/c %d. Cont w/o lib.",
					me->mtMxID, unInPortID, result);
			return result;
		}

		pSampleSlip->pSampleSlipAppi = (appi_t *)qurt_elite_memory_malloc(unSampleSlipLibSize, QURT_ELITE_HEAP_DEFAULT);
		if (NULL == pSampleSlip->pSampleSlipAppi)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, SampleSlip malloc failed. Size: %lu. Cont w/o lib.",
					me->mtMxID, unInPortID, unSampleSlipLibSize);
			result = ADSP_ENOMEMORY;
			return result;
		}
		else
		{
			MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, SampleSlip malloc success. Ptr: 0x%p, Size: %lu",
					me->mtMxID, unInPortID, pSampleSlip->pSampleSlipAppi, unSampleSlipLibSize);
		}

		//Call the Init function of SampleSlip module.
		bool_t is_in_place = 0;
		appi_format_t in_format = {0}, out_format = {0};

		in_format.bits_per_sample = pCurrentInPort->unBitwidth;
		in_format.num_channels = pCurrentInPort->unNumChannels;
		MtMx_CopyChannelMap(pCurrentInPort->unChannelMapping, in_format.channel_type, pCurrentInPort->unNumChannels);
		in_format.data_is_signed = 1;
		in_format.data_is_interleaved = 0;
		in_format.sampling_rate = pCurrentInPort->unSampleRate;

		result = appi_ss_init(pSampleSlip->pSampleSlipAppi, &is_in_place, &in_format, &out_format, NULL);
		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, SampleSlip module init failed with result: %x. Cont w/o lib.",
					me->mtMxID, unInPortID, result);
			MTMX_FREE(pSampleSlip->pSampleSlipAppi);
			return result;
		}
	}
	else
	{
		//pSampleSlip->pSampleSlipAppi is non-NULL. i.e, Re-init scenario. Call the re-init function of SampleSlip module.
		appi_format_t in_format = {0}, out_format = {0};

		in_format.bits_per_sample = pCurrentInPort->unBitwidth;
		in_format.num_channels = pCurrentInPort->unNumChannels;
		MtMx_CopyChannelMap(pCurrentInPort->unChannelMapping, in_format.channel_type, pCurrentInPort->unNumChannels);
		in_format.data_is_signed = 1;
		in_format.data_is_interleaved = 0;
		in_format.sampling_rate = pCurrentInPort->unSampleRate;

		result = pSampleSlip->pSampleSlipAppi->vtbl_ptr->reinit(pSampleSlip->pSampleSlipAppi, &in_format, &out_format, NULL);
		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, SampleSlip module re-init failed with result: %x. Cont w/o lib.",
					me->mtMxID, unInPortID, result);
			MTMX_FREE(pSampleSlip->pSampleSlipAppi);
			return result;
		}
	}
	return result;
}

ADSPResult MtMx_InPortSetS2DPtrSampleSlipLibrary(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t      *pSampleSlip     = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	ADSPResult              result           = ADSP_EOK;

	if(NULL != pSampleSlip->pSampleSlipAppi)
	{
		appi_buf_t param_buf;

		audproc_sampleslip_drift_pointer_t sampleslip_device_drift_ptr = {0};
		param_buf.data_ptr = (int8_t *)&sampleslip_device_drift_ptr;
		param_buf.actual_data_len = param_buf.max_data_len = sizeof(sampleslip_device_drift_ptr);

		//Obtain the stream to primary device drift pointer
		sd_dc_t *s2dLibPtr = NULL;
		avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,DEVICE_DRIFT_PTR,&s2dLibPtr);
		const volatile int32_t* pnStreamToDeviceDriftPtr = sd_drift_calc_get_drift_pointer(s2dLibPtr);


		sampleslip_device_drift_ptr.primary_drift_info_ptr = NULL;
		sampleslip_device_drift_ptr.current_drift_info_ptr = NULL;
		sampleslip_device_drift_ptr.stream_to_device_drift_info_ptr = pnStreamToDeviceDriftPtr;

		result = pSampleSlip->pSampleSlipAppi->vtbl_ptr->set_param(pSampleSlip->pSampleSlipAppi, AUDPROC_PARAM_ID_SAMPLESLIP_DRIFT_POINTER, &param_buf);

		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu SetParam on SampleSlip library failed with result: %x. Cont w/o lib.",
					me->mtMxID, unInPortID, result);
			MTMX_FREE(pSampleSlip->pSampleSlipAppi);
			return result;
		}

		audproc_sampleslip_direction_t sampleslip_direction;
		param_buf.data_ptr = (int8_t *)&sampleslip_direction;
		param_buf.actual_data_len = param_buf.max_data_len = sizeof(sampleslip_direction);

		sampleslip_direction.direction = Playback;
		result = pSampleSlip->pSampleSlipAppi->vtbl_ptr->set_param(pSampleSlip->pSampleSlipAppi, AUDPROC_PARAM_ID_SAMPLESLIP_DIRECTION, &param_buf);

		if (ADSP_EOK != result)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu SetParam on SampleSlip library failed with result: %x. Cont w/o lib.",
					me->mtMxID, unInPortID, result);
			MTMX_FREE(pSampleSlip->pSampleSlipAppi);
			return result;
		}
	}
	return result;
}

ADSPResult MtMx_InPortResetSampleSlipLibrary(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t      *pSampleSlip     = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	ADSPResult              result           = ADSP_EOK;

	if(NULL != pSampleSlip->pSampleSlipAppi)
	{
		//Reset the sampleslip library
		//reset algorithm
		appi_buf_t payload;//dummy payload as flush do not need any payload
		payload.data_ptr = NULL;
		payload.actual_data_len = 0;
		payload.max_data_len = 0;

		result = pSampleSlip->pSampleSlipAppi->vtbl_ptr->set_param(pSampleSlip->pSampleSlipAppi, APPI_PARAM_ID_ALGORITHMIC_RESET, &payload);
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Reconfing Input port: Sampleslip module reset cmd returned with status %d", me->mtMxID, (int)result);				
	}
	return result;
}

ADSPResult MtMx_CheckSSLibStatus_1(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t *pSampleSlip = &(pCurrentInPort->pInPortAvSync->structSampleSlip);

	if(TRUE == pCurrentInPort->pInPortAvSync->bShouldActiveInputPortBeReProcessed && NULL != pSampleSlip->pSampleSlipAppi)
	{
		//Special case handling when SSLib was introduced
		//MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu MtMx_MsgDataBuffer will be called from o/p port. Exiting now", me->mtMxID, unInPortID);
		return ADSP_EOK;
	}
	else
	{
		return ADSP_EFAILED;
	}
}

ADSPResult MtMx_CheckSSLibStatus_2(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t *pSampleSlip = &(pCurrentInPort->pInPortAvSync->structSampleSlip);

	if(TRUE == pCurrentInPort->pInPortAvSync->bShouldActiveInputPortBeReProcessed &&  NULL != pSampleSlip->pSampleSlipAppi)
	{
		//Special case handling when SSLib was introduced
		//MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBuffer from o/p port %lu", me->mtMxID, unInPortID, unOutPortID);
		MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);
		pCurrentInPort->pInPortAvSync->bShouldActiveInputPortBeReProcessed = FALSE;
	}

	return ADSP_EOK;
}

ADSPResult MtMx_CheckSSLibStatus_3(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];

	//Enable the stream-to-device ratematching using SampleSlip module if non-default clock recovery mode is set (the input media type is available at this point).
	uint32_t current_clk_recovery_mechanism = ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT;
	avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, CLOCK_RECOVERY_TYPE, &current_clk_recovery_mechanism);
	if((ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT != current_clk_recovery_mechanism) && (NULL != pCurrentInPort->pStartLoc))
	{
		(void)MtMx_InPortToCheckReInitSampleSlipLibrary(me, unInPortID); //AV-Sync
		(void)MtMx_InPortSetS2DPtrSampleSlipLibrary(me, unInPortID);
	}

	return ADSP_EOK;
}

ADSPResult MtMx_CheckSSLibStatus_4(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t *pSampleSlip = &(pCurrentInPort->pInPortAvSync->structSampleSlip);

	if(NULL != pSampleSlip->pSampleSlipAppi)
	{
		return ADSP_EOK;
	}
	else
	{
		return ADSP_EFAILED;
	}
}

ADSPResult MxAr_AVSyncSetAFEDriftPtrInputDerivedFromOutput(This_t *me)
{
	uint32_t unInPortID;
	MatrixInPortInfoType *pCurrentInPort;
	MatrixOutPortInfoType *pCurrentOutPort;

	for (unInPortID = 0; unInPortID <= me->maxInPortID; unInPortID++)
	{
		if((NULL != me->inPortParams[unInPortID]) && (INPUT_PORT_STATE_INACTIVE != me->inPortParams[unInPortID]->inPortState))
		{
			pCurrentInPort = me->inPortParams[unInPortID];

			//Get top priority port
			uint32_t unOutPortID = pCurrentInPort->unTopPrioOutPort;
			if((unOutPortID < MT_MX_MAX_OUTPUT_PORTS) && (NULL != me->outPortParams[unOutPortID]))
			{
				pCurrentOutPort = me->outPortParams[unOutPortID];

				//Set the s2pd drift pointer here
				if(NULL == pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"MtMx #%lu: i/p port %lu, Top prio o/p port %lu AFE Drift Ptr is NULL", me->mtMxID, unInPortID, unOutPortID);
				}
				else
				{
					MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"MtMx #%lu: i/p port %lu, Top prio o/p port %lu AFE Drift Ptr %p", me->mtMxID, unInPortID, unOutPortID, pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr);
					avsync_lib_set_device_drift_pointer(pCurrentInPort->pInPortAvSync->pAVSyncLib, &(pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr->avt_drift_info));
				}
				//Since the s2pd drift pointer got potentially re-set here, call the re-init on the SampleSlip library to set this, if applicable.
				uint32_t current_clk_recovery_mechanism = ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT;
				avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, CLOCK_RECOVERY_TYPE, &current_clk_recovery_mechanism);
				//Since the s2pd drift pointer got potentially re-set here, call the re-init on the SampleSlip library to set this, if applicable.
				if((ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT != current_clk_recovery_mechanism) &&
						(NULL != pCurrentInPort->pStartLoc))
				{
					(void)MtMx_InPortToCheckReInitSampleSlipLibrary(me, unInPortID); //AV-Sync
					(void)MtMx_InPortSetS2DPtrSampleSlipLibrary(me, unInPortID);
				}
			}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_HandleSampleAddDrops_1(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];

	if (pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled && MT_MX_INPUT_PORT_DROP_SAMPLES == pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
	{
		pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf = (pCurrentInPort->unInPortPerChBufSize + MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER);
		pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = FALSE;
	}
	else if (pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled && MT_MX_INPUT_PORT_ADD_SAMPLES == pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
	{
		pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf = (pCurrentInPort->unInPortPerChBufSize - MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER);
		pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = FALSE;
	}
	else
	{
		pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf = pCurrentInPort->unInPortPerChBufSize;
	}

	return ADSP_EOK;
}

ADSPResult MtMx_HandleSampleAddDrops_2(This_t *me, uint32_t unInPortID)
{
	// If Adjust Session Clk is enabled, change the number of samples that
	// should be read from the input buffer to fill the local buffer.
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	if (pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled)
	{
		if (MT_MX_INPUT_PORT_ADD_SAMPLES == pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
		{
			if (TRUE == pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop)
			{
				pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
				pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = FALSE;
			}
		}
		else if(MT_MX_INPUT_PORT_DROP_SAMPLES == pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
		{
			if (TRUE == pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop)
			{
				pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf += MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
				pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = FALSE;
			}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_HandleSampleAddDrops_3(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];

	if(TRUE == pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled)
	{
		switch(pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
		{
		case MT_MX_INPUT_PORT_ADD_SAMPLES:
		{
			(void)MtMx_AddInterpolatedSamples(me, unInPortID);
			break;
		}
		case MT_MX_INPUT_PORT_DROP_SAMPLES:
		{
			(void)MtMx_DropSamples(me, unInPortID);
			break;
		}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_FillInPortLocalBuf(This_t *me, uint32_t unInPortID, uint32_t numSamplesReqPerCh)
{
	ADSPResult              result = ADSP_EOK;
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_sampleslip_t	    *pSampleSlip        = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	int8_t                  *pWrLocation = pCurrentInPort->pWrLoc;
	int8_t                  *pCurrentSample = pCurrentInPort->pCurrentSample;

	//Convert from samples to bytes.
	uint32_t  unInPortPerChBufSizeBytes = (pCurrentInPort->unBytesPerSample) * (pCurrentInPort->unInPortPerChBufSize);
	uint32_t  numBytesReqPerCh             = (pCurrentInPort->unBytesPerSample) * (numSamplesReqPerCh);
	uint32_t  unInpQNumBytesPerCh        = (pCurrentInPort->unBytesPerSample) * (pCurrentInPort->nNumRemainingSamplesPerCh + pCurrentInPort->nNumSamplesUsedPerCh);

	if (NULL != pSampleSlip->pSampleSlipAppi)
	{
		uint32_t i = 0;
		uint32_t unNumInputSamplesReqForSSLib = 0;

		//Call GetInputReq on the SampleSlip library. Always pass the pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf as argument.
		result = pSampleSlip->pSampleSlipAppi->vtbl_ptr->get_input_req(pSampleSlip->pSampleSlipAppi,
				pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf, &unNumInputSamplesReqForSSLib);
		if(ADSP_FAILED(result))
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu SampleSlip GetInputReq failed. Res=%d. Cont. w/o lib.", me->mtMxID, unInPortID, result);
			MTMX_FREE(pSampleSlip->pSampleSlipAppi);
			result = ADSP_EOK;
		}
		else
		{
			uint32_t unMinSamples = ((int32_t)unNumInputSamplesReqForSSLib <= pCurrentInPort->nNumRemainingSamplesPerCh) ?
					(unNumInputSamplesReqForSSLib) : ((uint32_t)pCurrentInPort->nNumRemainingSamplesPerCh);

			for (i = 0; i < pCurrentInPort->unNumChannels; i++)
			{
				pSampleSlip->inBufs[i].data_ptr              = pCurrentSample;
				pSampleSlip->inBufs[i].actual_data_len   = pCurrentInPort->unBytesPerSample * unMinSamples;
				pSampleSlip->inBufs[i].max_data_len      = unInpQNumBytesPerCh;

				pSampleSlip->outBufs[i].data_ptr            = pWrLocation;
				pSampleSlip->outBufs[i].actual_data_len = 0;
				pSampleSlip->outBufs[i].max_data_len    = pCurrentInPort->unBytesPerSample * numSamplesReqPerCh;

				pCurrentSample += unInpQNumBytesPerCh;
				pWrLocation       += unInPortPerChBufSizeBytes;
			}

			//Copy the data from input buffer into the local buffer based on the stream-to-device drift.
			appi_buflist_t in_buf_list, out_buf_list;

			in_buf_list.bufs_num    = pCurrentInPort->unNumChannels;
			in_buf_list.buf_ptr        = pSampleSlip->inBufs;
			out_buf_list.bufs_num = pCurrentInPort->unNumChannels;
			out_buf_list.buf_ptr     = pSampleSlip->outBufs;

			//Call Process function
			(void)pSampleSlip->pSampleSlipAppi->vtbl_ptr->process(pSampleSlip->pSampleSlipAppi, &in_buf_list, &out_buf_list, NULL);

			//Increment read and write pointers
			pCurrentInPort->pCurrentSample += pSampleSlip->inBufs[0].actual_data_len;
			pCurrentInPort->pWrLoc += pSampleSlip->outBufs[0].actual_data_len;

			//Update state variables
			pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= ((pSampleSlip->outBufs[0].actual_data_len)/(pCurrentInPort->unBytesPerSample));
			pCurrentInPort->nNumRemainingSamplesPerCh -= ((pSampleSlip->inBufs[0].actual_data_len)/(pCurrentInPort->unBytesPerSample));
			pCurrentInPort->nNumSamplesUsedPerCh += ((pSampleSlip->inBufs[0].actual_data_len)/(pCurrentInPort->unBytesPerSample));

			//Update local buffer fullness and emptiness
			if (0 == pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
			{
				pCurrentInPort->bIsLocalBufFull = TRUE;
			}
			else
			{
				pCurrentInPort->bIsLocalBufFull = FALSE;
			}
			pCurrentInPort->bIsLocalBufEmpty = FALSE;

			return result;
		}
	}

	//Normal operation. This is used for 1) Non-SampleSlip use case OR 2) Erroneous condition in SampleSlip usages.
	//Copy the samples from Q to inport local buffer
	for (uint32_t k = 0; k < pCurrentInPort->unNumChannels; k++)
	{
		//Memcpy and increment read and write pointers to next channel
		memscpy((void*)pWrLocation, numBytesReqPerCh, (void*)pCurrentSample, numBytesReqPerCh);
		pCurrentSample += unInpQNumBytesPerCh;
		pWrLocation       += unInPortPerChBufSizeBytes;
	}

	//Increment read and write pointers
	pCurrentInPort->pWrLoc += numBytesReqPerCh;
	pCurrentInPort->pCurrentSample += numBytesReqPerCh;

	//Update state variables
	pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= numSamplesReqPerCh;
	pCurrentInPort->nNumRemainingSamplesPerCh -= numSamplesReqPerCh;
	pCurrentInPort->nNumSamplesUsedPerCh += numSamplesReqPerCh;

	//Update local buffer fullness and emptiness
	if (0 == pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
	{
		pCurrentInPort->bIsLocalBufFull = TRUE;
	}
	else
	{
		pCurrentInPort->bIsLocalBufFull = FALSE;
	}
	pCurrentInPort->bIsLocalBufEmpty = FALSE;

	return result;
}

ADSPResult MtMx_ClearHoldDuration(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_clear_time(&pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec);
	return ADSP_EOK;
}

ADSPResult MtMx_MsgDataBufferHold(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	elite_msg_data_buffer_t*     pInputBuf;
	pCurrentInPort->pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);

	pInputBuf = pCurrentInPort->pInputBuf;
	pCurrentInPort->nNumRemainingSamplesPerCh = (pInputBuf->nActualSize /
			(pCurrentInPort->unNumChannels * pCurrentInPort->unBytesPerSample));
	pCurrentInPort->nNumSamplesUsedPerCh = 0;
	pCurrentInPort->pCurrentSample = (int8_t *)(&(pInputBuf->nDataBuf));

	// Logging the input PCM data to Matrix mixer service
	MtMx_LogPcmData(me, unInPortID, pCurrentInPort->pCurrentSample);

	//Update timestamps
	if(pCurrentInPort->bIsTimeStampValid)
	{
		pCurrentInPort->ullTimeStampAtCopy.int_part = pCurrentInPort->ullIncomingBufferTS;
		pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
	}

	//Now, call the common hold routine
	(void)MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);

	return ADSP_EOK;
}

ADSPResult MtMx_HoldCommonRoutine(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	uint32_t unNumSampPerChZerosToPreFill = 0;
	uint32_t numSamplesReqPerCh;

	if (pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part > 0)
	{
		//Calculate the number of samples/ch of zeros to pre-fill
		//Hold Dur (uS) / 1000 = Hold Dur in ms.
		//Hold Dur (ms) * #samp/ms/ch = #samp/ch
		unNumSampPerChZerosToPreFill = (pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part * pCurrentInPort->unSamplesPer1Msec)/(1000);

		MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu Hold Dur [%lu %lu], ZerosToFill %lu sam/ch",
				me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part>>32),
				(uint32_t)(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part), unNumSampPerChZerosToPreFill);

		//Pre-fill with hold duration worth of zeros (This will guaranteed to be < 1 i/p frame size worth by the caller).
		if(unNumSampPerChZerosToPreFill <= pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
		{
			//Most common use case. There is room in the existing internal buffer to accommodate all the hold zeros.
			(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
			numSamplesReqPerCh = unNumSampPerChZerosToPreFill;
			MtMx_FillInPortLocalBufHold(me, unInPortID, numSamplesReqPerCh);
		}
		else
		{
			//Rare use case. The number of 'hold zeros' to fill is more than what the internal buffer can hold at this point.
			if(0 != pCurrentInPort->unSamplesPer1Msec)
			{
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part -= ((pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf * 1000) / (pCurrentInPort->unSamplesPer1Msec));
			}
			else
			{
				//This use case should never occur.
				// Assume 48kHz sampling rate which gives 48 samples per 1ms
				pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part -= ((pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf * 1000) / (48));
			}
			numSamplesReqPerCh = pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
			MtMx_FillInPortLocalBufHold(me, unInPortID, numSamplesReqPerCh);
			goto __cmnroutineafterfill;
		}
	}

	//Now, fill the remainder of internal buffer as usual.
	if (pCurrentInPort->nNumRemainingSamplesPerCh >= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
	{
		numSamplesReqPerCh = pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
	}
	else
	{
		//i/p buffer contains lesser samples than the size of local buffer (short buffer)
		numSamplesReqPerCh = pCurrentInPort->nNumRemainingSamplesPerCh;
		pCurrentInPort->bIsInBufShort = TRUE;
	}

	//Copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
	MtMx_FillInPortLocalBuf(me, unInPortID, numSamplesReqPerCh);

	__cmnroutineafterfill:
	if (FALSE == pCurrentInPort->bIsLocalBufFull && TRUE == pCurrentInPort->bIsInBufShort)
	{
		// The incoming buffer did not contain sufficient samples to fill local buffer
		// return the buffer and add the input port to the wait mask
		ADSPResult result;

		if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
		}

		pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
		pCurrentInPort->numBufReturned++;
		MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
				me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

	}

	return ADSP_EOK;
}

ADSPResult MtMx_ProcessMsgDataBuffer(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];

	//If it has been determined that a Hold is required (< 1 i/p frame size), then handle appropriately
	if((pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part > 0) && (pCurrentInPort->unNewNumAfeFrames > MT_MX_LL_NUM_AFE_FRAMES))
	{
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBufferHold from i/p port", me->mtMxID, unInPortID);
		(void)MtMx_MsgDataBufferHold(me, unInPortID, pCurrentInPort);
	}
	else
	{
		(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
		MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort); //Process the input buffer normally
	}

	return ADSP_EOK;
}

ADSPResult MxAr_CommonInPortProcessingRoutinePostSteps(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	ADSPResult result = ADSP_EOK;

	//At this point, check if there are any hold-zeros that are remaining to be filled.
	if(0 == pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part)
	{
		//Try refilling the i/p port's local buffer with new samples from upstream peer's o/p buffer
		if (pCurrentInPort->nNumRemainingSamplesPerCh <= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
		{
			//i/p buffer has insufficient samples to fill local buffer
			//Because of sample slip, there may be a use case where there might be a small # of samples still left over in the i/p Q, that needs to be copied over
			//i/p buffer has insufficient samples to fill local buffer, however call the fill local buffer routine again to exhaust any remainder samples in the i/p Q.
			while (pCurrentInPort->nNumRemainingSamplesPerCh > 0 && FALSE == pCurrentInPort->bIsLocalBufFull)
			{
				MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->nNumRemainingSamplesPerCh);
			}

			//Update timestamps
			if(pCurrentInPort->bIsTimeStampValid)
			{
				mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
			}

			//Now it is OK to return the buffer, as it has been established that the i/p Q buffer has truly been emptied, at this point.
			if (INPUT_BUFFER_HELD == pCurrentInPort->bInBufStatus)
			{
				if (ADSP_FAILED(elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
				}

				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
				pCurrentInPort->numBufReturned++;
			}
			MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
					me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

		}
		else
		{
			//i/p buffer has sufficient samples to fill local buffer
			if(pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled)
			{
				//AV-Sync
				switch(pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
				{
				case MT_MX_INPUT_PORT_ADD_SAMPLES:
				{
					(void)MtMx_AddInterpolatedSamples(me, unInPortID);
					break;
				}
				case MT_MX_INPUT_PORT_DROP_SAMPLES:
				{
					(void)MtMx_DropSamples(me, unInPortID);
					break;
				}
				}
			}

			//Update timestamps
			if(pCurrentInPort->bIsTimeStampValid)
			{
				mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
			}

			//copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
			MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf);
		}
	}
	else
	{
		//If hold duration is less than the rounding < 1ms, treat it as immediate.
		if (pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
		{
			(void)MtMx_ClearHoldDuration(me, unInPortID); //AV-Sync
		}

		//Update timestamps
		if (pCurrentInPort->bIsTimeStampValid)
		{
			mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
		}

		//Now, call the common hold routine
		(void)MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);
	}

	return result;
}

ADSPResult MtMx_ResetAvSyncStatsDrift(This_t *me, uint32_t unInPortID)
{
	//This buffer is yet to update AVSync Stats and Drift
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedStats = FALSE;
	pCurrentInPort->pInPortAvSync->bHasInputBufferUpdatedDrift = FALSE;
	return ADSP_EOK;
}

ADSPResult MtMx_ResetAvSyncSampleStats(This_t *me, uint32_t unInPortID)
{
	//Clear sample stats
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->unDataRcvdInSamples = 0;
	pCurrentInPort->unDataRcvdInUsec.int_part = 0;
	pCurrentInPort->unDataRcvdInUsec.frac_part = 0;
	return ADSP_EOK;
}

ADSPResult MtMx_ResetStrClkSync(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->pInPortAvSync->bIsStrClkSyncdWithSessClk = FALSE;
	return ADSP_EOK;
}

ADSPResult MtMx_SetupDefaultRenderWindow(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->pInPortAvSync->bUseDefaultWindowForRendering = TRUE;
	return ADSP_EOK;
}

ADSPResult MtMx_SetDefaultRenderWindow(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	avsync_lib_check_set_default_rendering_window(pCurrentInPort->pInPortAvSync->pAVSyncLib, MT_MX_FRAME_DURATION_5000US);
	MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Render window and Default window changed to 5ms");
	return ADSP_EOK;
}

ADSPResult MtMx_SetDefaultRenderWindows(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	int64_t default_render_window = -MT_MX_FRAME_DURATION_5000US;
	avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, RENDER_WINDOW_START, &default_render_window, sizeof(int64_t));
	default_render_window = -default_render_window;
	avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, RENDER_WINDOW_END, &default_render_window, sizeof(int64_t));
	return ADSP_EOK;
}

ADSPResult MtMx_AdjustSessionClk(This_t *me, elite_msg_any_t *pMsg)
{
	ADSPResult                                   result = ADSP_EOK;
	uint32_t                                     unPort2AdjSessionClk;
	EliteMsg_CustomMtMxAdjustSessionClkType*     pPayload = (EliteMsg_CustomMtMxAdjustSessionClkType*)pMsg->pPayload;

	unPort2AdjSessionClk = pPayload->unPortID;
	MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Processing cmd %lu (AdjSessClk) [port ID, port dir, adj time msw, lsw] = [%lu, %lu, %lu, %lu]",
			me->mtMxID, pPayload->unSecOpCode, pPayload->unPortID, pPayload->unPortDirection,
			(uint32_t)(pPayload->llAdjustTime >> 32), (uint32_t)(pPayload->llAdjustTime));

	if (ADM_MATRIX_ID_AUDIO_TX == me->mtMxID)
	{
		result = ADSP_EUNSUPPORTED;
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: Tx audio matrix does not support this cmd",
				me->mtMxID, pPayload->unSecOpCode);
		goto __bailoutCmdAdjustSessionClk;
	}

	if (MATRIX_MIXER_PORT_DIR_INPUT == pPayload->unPortDirection)
	{
		MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unPort2AdjSessionClk];
		if ((NULL == pCurrentInPort) || (me->maxInPortID < unPort2AdjSessionClk || INPUT_PORT_STATE_INACTIVE == pCurrentInPort->inPortState))
		{
			result = ADSP_EBADPARAM;
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: rcvd invalid/inactive port ID [%lu]",
					me->mtMxID, pPayload->unSecOpCode, pPayload->unPortID);
			goto __bailoutCmdAdjustSessionClk;
		}
		if (0  == pPayload->llAdjustTime)
		{
			result = ADSP_EOK;
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: rcvd zero adjust time",
					me->mtMxID, pPayload->unSecOpCode);
			pPayload->llActualAdjustTime = 0;
			pPayload->ullCmdLatency = 0;
			goto __bailoutCmdAdjustSessionClk;
		}
		else if (0 > pPayload->llAdjustTime)
		{
			int64_t tempVar = -(pPayload->llAdjustTime) * (pCurrentInPort->unSamplesPer1Msec);
			pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj = (uint32_t)(tempVar / 1000);
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: Add %lu samples",
					me->mtMxID, pPayload->unSecOpCode, pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj);
			if (0 < pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj)
			{
				//Delaying the session clock is implemented by adding interpolated samples
				pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled = TRUE;
				pCurrentInPort->pInPortAvSync->samplesAddOrDropMask = MT_MX_INPUT_PORT_ADD_SAMPLES;
				pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = TRUE;
			}
			tempVar = pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj;
			pPayload->llActualAdjustTime = -((tempVar * 1000) / (pCurrentInPort->unSamplesPer1Msec));
			pPayload->ullCmdLatency = (pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj * pCurrentInPort->unFrameDurationInUsec.int_part);
		}
		else
		{
			int64_t tempVar = pPayload->llAdjustTime * (pCurrentInPort->unSamplesPer1Msec);
			pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj = (uint32_t)(tempVar / 1000);
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: Drop %lu samples",
					me->mtMxID, pPayload->unSecOpCode, pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj);
			if(0 < pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj)
			{
				//Advancing the session clock is implemented by dropping samples
				pCurrentInPort->pInPortAvSync->bIsSampleAddDropEnabled = TRUE;
				pCurrentInPort->pInPortAvSync->samplesAddOrDropMask = MT_MX_INPUT_PORT_DROP_SAMPLES;
				pCurrentInPort->pInPortAvSync->bIsThisFirstSampleAddOrDrop = TRUE;
			}
			pPayload->llActualAdjustTime = ((pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj * 1000) / (pCurrentInPort->unSamplesPer1Msec));
			pPayload->ullCmdLatency = (pCurrentInPort->pInPortAvSync->unNumRemSamplesAdj * pCurrentInPort->unFrameDurationInUsec.int_part);
		}
		MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: actual adj time MSW: %lu, LSW: %lu, cmd latency MSW: %lu, LSW: %lu",
				me->mtMxID, pPayload->unSecOpCode, (uint32_t)(pPayload->llActualAdjustTime >> 32), (uint32_t)(pPayload->llActualAdjustTime),
				(uint32_t)(pPayload->ullCmdLatency >> 32), (uint32_t)(pPayload->ullCmdLatency));
	}
	else
	{
		result = ADSP_EBADPARAM;
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (AdjSessClk) handler: rcvd invalid port dir [%lu]",
				me->mtMxID, pPayload->unSecOpCode, pPayload->unPortDirection);
		goto __bailoutCmdAdjustSessionClk;
	}

	__bailoutCmdAdjustSessionClk:
	elite_msg_finish_msg(pMsg, result);
	MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Leaving cmd [%lu] (AdjSessClk) handler with status %d", me->mtMxID, pPayload->unSecOpCode, (int)result);
	return result;
}

ADSPResult MtMx_SetRateMatchingParamHandler(This_t *me, elite_msg_any_t *pMsg)
{
	ADSPResult                          result = ADSP_EOK;
	EliteMsg_CustomMtMxSetRateMatchingParamType*  pPayload = (EliteMsg_CustomMtMxSetRateMatchingParamType* )pMsg->pPayload;
	mtmx_ratematching_set_params RenderingDecisionParamPayload;
	EliteMsg_CustomMtMxGetSetParamType RateMatchingPayload;

	uint32_t unPort2SetParam = pPayload->unPortID;
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Processing cmd %lu (SetRateMatchingParam) [port ID] = [%d, %d]",
			me->mtMxID, pPayload->unSecOpCode, (int)pPayload->unPortID, (int)pPayload->unPortDirection);

	if (MATRIX_MIXER_PORT_DIR_INPUT == pPayload->unPortDirection)
	{
		MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unPort2SetParam];

		//Port ID validity check
		if ((NULL == pCurrentInPort) || (me->maxInPortID < unPort2SetParam || INPUT_PORT_STATE_INACTIVE == pCurrentInPort->inPortState))
		{
			result = ADSP_EBADPARAM;
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (SetRateMatchingParam) handler: rcvd invalid/inactive port ID [%d]",
					me->mtMxID, pPayload->unSecOpCode, (int)pPayload->unPortID);
			goto __bailoutCmdSetRateMatchingParamHandler;
		}

		RenderingDecisionParamPayload.pAddr= NULL;

		//The payload size is the size of the entire structure minus the size of pAddr and data_payload_size
		//4+4+2+2+sizeof(struct asm_session_mtmx_strtr_param_render_decision_t)+4+4+2+2+sizeof(struct asm_session_mtmx_strtr_param_clock_recovery_t);
		RenderingDecisionParamPayload.data_payload_size = sizeof(mtmx_ratematching_set_params) - sizeof(uint32 *) - sizeof(uint32);

		RenderingDecisionParamPayload.unModuleId1 = ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC;
		RenderingDecisionParamPayload.unParamId1 = ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION;
		RenderingDecisionParamPayload.usParamSize1 = sizeof(asm_session_mtmx_strtr_param_render_decision_t);
		RenderingDecisionParamPayload.usReserved1 = 0;
		RenderingDecisionParamPayload.sMtMxRenderingDecision.flags = pPayload->renderingDecison;

		RenderingDecisionParamPayload.unModuleId2 = ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC;
		RenderingDecisionParamPayload.unParamId2 = ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY;
		RenderingDecisionParamPayload.usParamSize2 = sizeof(asm_session_mtmx_strtr_param_clock_recovery_t);
		RenderingDecisionParamPayload.usReserved2 = 0;
		RenderingDecisionParamPayload.sMtMxClkRecoveryMethod.flags = pPayload->ClkRecoveryMode;

		RateMatchingPayload.pnParamData = (int32_t *)&RenderingDecisionParamPayload.unModuleId1;
		RateMatchingPayload.unSize = RenderingDecisionParamPayload.data_payload_size;

		//Route to the appropriate handler
		if(ADM_MATRIX_ID_AUDIO_RX == me->mtMxID)
		{
			result = MxAr_InPortSetParamHandler(me, pMsg, &RateMatchingPayload,  unPort2SetParam, pCurrentInPort);
		}
		else
		{
			result = ADSP_EBADPARAM;
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (SetRateMatchingParam) handler: rcvd on Tx Matrix. Not supported",
					me->mtMxID, pPayload->unSecOpCode);
			goto __bailoutCmdSetRateMatchingParamHandler;
		}
	}
	else
	{
		result = ADSP_EBADPARAM;
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] (SetRateMatchingParam) handler: rcvd invalid port direction[%d]",
				me->mtMxID, pPayload->unSecOpCode, (int)pPayload->unPortDirection);
	}

	__bailoutCmdSetRateMatchingParamHandler:
	elite_msg_finish_msg(pMsg, result);
	MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: Leaving cmd [%lu] (SetRateMatchingParam) handler with status %d", me->mtMxID, pPayload->unSecOpCode, (int)result);
	return result;
}

ADSPResult MxAr_Run(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxRunType *pPayload)
{
	uint32_t unPort2Run = pPayload->unPortID;
	ADSPResult result = ADSP_EOK;
	if (MATRIX_MIXER_PORT_DIR_INPUT == pPayload->unPortDirection)
	{
		MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unPort2Run];
		if (NULL == pCurrentInPort)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd on invalid port [%lu]", me->mtMxID, pPayload->unSecOpCode, unPort2Run);
			return ADSP_EUNEXPECTED;
		}

		if(ADSP_EOK != (result = MtMx_AvSyncRequestHW(me, unPort2Run)))
		{
			return result;
		}

		uint64_t llStartTime = pPayload->ullStartTime;
		avsync_rendering_decision_t rendering_decision = RENDER;
		avsync_lib_process_run(pCurrentInPort->pInPortAvSync->pAVSyncLib, pPayload->unStartFlag);
		pCurrentInPort->pInPortAvSync->unStartFlag = pPayload->unStartFlag;
		avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, START_TIME, &(pPayload->ullStartTime), sizeof(pPayload->ullStartTime));
		MtMx_ResetStrClkSync(me, unPort2Run); //AV-Sync
		avsync_lib_make_simple_rendering_decision(pCurrentInPort->pInPortAvSync->pAVSyncLib,llStartTime,&rendering_decision);

		if(llStartTime > 0)
		{
			pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part = llStartTime;
		}

		switch (pCurrentInPort->inPortState)
		{
		case INPUT_PORT_STATE_INIT:
		case INPUT_PORT_STATE_WAITING_TO_BE_READY:
		case INPUT_PORT_STATE_READY:
		{
			if (HOLD != rendering_decision)
			{
				pCurrentInPort->inPortState = INPUT_PORT_STATE_READY;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_READY;
				MtMx_AddInputPortToWaitMask(me, unPort2Run);
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
				MtMx_SetupDefaultRenderWindow(me, unPort2Run); //AV-Sync
				MtMx_ClearHoldDuration(me, unPort2Run); //AV-Sync
				MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "relative start time good for rendering, moving i/p port %lu to READY", unPort2Run);
			}
			else
			{
				//if relative start time is in the future by at least (-1*pCurrentInPort->llRenderWindowStart):
				//remove i/p port from wait mask, move to waiting_to_be_ready state, wait duration already calculated above
				pCurrentInPort->inPortState = INPUT_PORT_STATE_WAITING_TO_BE_READY;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_WAITING_TO_BE_READY;
				MtMx_RemoveInputPortFromWaitMask(me, unPort2Run);
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
				MtMx_SetupDefaultRenderWindow(me, unPort2Run); //AV-Sync
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "i/p port %lu being held for [MSW: %lu, LSW: %lu] usec",
						unPort2Run, (uint32_t )(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t )(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));
			}
			break;
			return ADSP_EOK;
		}
		case INPUT_PORT_STATE_PAUSED:
		case INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE:
		case INPUT_PORT_STATE_ACTIVE:
		case INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER:
		{

			if (HOLD != rendering_decision)
			{
				pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
				MtMx_InPortToUpdateWaitMask(me, unPort2Run);
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
				MtMx_SetupDefaultRenderWindow(me, unPort2Run); //AV-Sync
				MtMx_ClearHoldDuration(me, unPort2Run); //AV-Sync
				MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "relative start time slightly > 0, moving i/p port %lu to ACTIVE", unPort2Run);
			}
			else
			{
				//if relative start time is in the future by at least  (-1*pCurrentInPort->llRenderWindowStart):
				//remove i/p port from wait mask, move to waiting_to_be_active state, wait duration already calculated above
				pCurrentInPort->inPortState = INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE;
				MtMx_RemoveInputPortFromWaitMask(me, unPort2Run);
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
				MtMx_SetupDefaultRenderWindow(me, unPort2Run); //AV-Sync
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "i/p port %lu being held for [MSW: %lu, LSW: %lu] usec",
						unPort2Run, (uint32_t )(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t )(pCurrentInPort->pInPortAvSync->ullInBufHoldDurationInUsec.int_part));
			}
			break;

			return ADSP_EOK;
		}
		}
	}
	else
	{
		//RX o/p port
		MatrixOutPortInfoType *pCurrentOutPort = me->outPortParams[unPort2Run];
		if (NULL == pCurrentOutPort)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd on invalid port [%lu]",
					me->mtMxID, pPayload->unSecOpCode, unPort2Run);
			return ADSP_EUNEXPECTED;
		}

		switch (pCurrentOutPort->outPortState)
		{
		case OUTPUT_PORT_STATE_INIT:
		{
			MtMx_MoveOutportToActiveState(me, unPort2Run,pCurrentOutPort);
			MtMx_OutPortToUpdateActiveOutPortsMasks(me, unPort2Run);
			MtMx_OutPortToUpdateWaitMask(me, unPort2Run);
			return ADSP_EOK;
		}
		default:
		{
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd in unexpected state [%d], discarding",
					me->mtMxID, pPayload->unSecOpCode, pCurrentOutPort->outPortState);
			return ADSP_EUNEXPECTED;
		}
		}
	}
	return ADSP_EOK;
}

ADSPResult MxAt_Run(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxRunType *pPayload)
{
	uint32_t unPort2Run = pPayload->unPortID;

	if (MATRIX_MIXER_PORT_DIR_INPUT == pPayload->unPortDirection)
	{
		//TX i/p port
		MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unPort2Run];
		if (NULL == pCurrentInPort)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd on invalid port [%lu]",
					me->mtMxID, pPayload->unSecOpCode, unPort2Run);
			return ADSP_EUNEXPECTED;
		}

		switch (pCurrentInPort->inPortState)
		{
		case INPUT_PORT_STATE_INIT:
		{
			int k = qurt_elite_queue_get_channel_bit(pCurrentInPort->inPortHandle.portHandle.dataQ);
			int t = 31 - (Q6_R_cl0_R( (k) ));
			me->inPortIDMap[t] = unPort2Run;
			me->unDataBitfield = me->unDataBitfield | k;
			pCurrentInPort->inPortState = INPUT_PORT_STATE_READY;
			pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_READY;
			MtMx_AddInputPortToWaitMask(me, unPort2Run);
			return ADSP_EOK;
		}
		default:
		{
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd in unexpected state [%ld], discarding",
					me->mtMxID, pPayload->unSecOpCode, pCurrentInPort->inPortState);
			return ADSP_EUNEXPECTED;
		}
		}
	}
	else
	{
		//TX o/p port
		MatrixOutPortInfoType *pCurrentOutPort = me->outPortParams[unPort2Run];
		if (NULL == pCurrentOutPort)
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd on invalid port [%lu]",
					me->mtMxID, pPayload->unSecOpCode, unPort2Run);
			return ADSP_EUNEXPECTED;
		}

		if(NULL == pCurrentOutPort->avt_drv_handle)
		{
			// Open AV timer driver (only needed on the TX matrix o/p port)
			avtimer_open_param_t open_param;
			char client_name[] = "MtMxOp";
			ADSPResult result = ADSP_EOK;
			open_param.client_name = client_name;
			open_param.flag = 0;
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Opening AVTimer Driver for o/p port %lu",me->mtMxID, unPort2Run);
			if (ADSP_EOK != (result = avtimer_drv_hw_open(&(pCurrentOutPort->avt_drv_handle), &open_param)))
			{
				MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to Open AVTimer Driver for o/p port %lu with result: %d",unPort2Run,result);
				pCurrentOutPort->avt_drv_handle = NULL;
				return result;
			}
		}

		switch (pCurrentOutPort->outPortState)
		{
		case OUTPUT_PORT_STATE_INIT:
		case OUTPUT_PORT_STATE_PAUSED:
		case OUTPUT_PORT_STATE_WAITING_TO_BE_ACTIVE:
		{
			/* Mark the next output buffer that is sent downstream as the first buffer
            following a RUN cmd, so that the timestamp is calculated in the following way:
            a. For PAUSE/RUN sequence: Timestamp should increment from where it stopped
            b. For PAUSE/FLUSH/RUN sequence: Timestamp should increment from 0 (flush cmd handler
            resets pCurrentOutPort->llSessionTimeInSamples to 0. */
			pCurrentOutPort->bIsFirstOutBufYetToBeSent = TRUE;

			switch (pPayload->unStartFlag)
			{
			case ASM_SESSION_CMD_RUN_START_TIME_RUN_IMMEDIATE:
			{
				MtMx_RunOutputPort(me, unPort2Run);
				break;
			}
			case ASM_SESSION_CMD_RUN_START_TIME_RUN_AT_RELATIVE_TIME:
			{
				if (0 >= pPayload->ullStartTime)
				{
					//start immediately or start in the past
					MtMx_RunOutputPort(me, unPort2Run);
				}
				else
				{
					//start in the future
					pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.int_part = pPayload->ullStartTime;
					pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.frac_part = 0;
					if (pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.int_part < pCurrentOutPort->unFrameDurationInUsec.int_part)
					{
						//if relative start time is in the future by less than pCurrentOutPort->unOutPortPerChBufSize
						MtMx_RunOutputPort(me, unPort2Run);
					}
					else
					{
						//if relative start time is in the future by at least pCurrentOutPort->unOutPortPerChBufSize,
						//a. do not add o/p port's bufQ to the waitmask, b. move o/p port state to waiting_to_be_active state and
						//c. do not initialize session time to 0. */
						pCurrentOutPort->outPortState = OUTPUT_PORT_STATE_WAITING_TO_BE_ACTIVE;
						MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu cmd [%lu] Run handler: relative start time [MSW: %lu, LSW: %lu] is in future.",
								me->mtMxID, pPayload->unSecOpCode, (uint32_t)(pPayload->ullStartTime >> 32), (uint32_t)(pPayload->ullStartTime));
						MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "i/p port %lu being held for [MSW: %lu, LSW: %lu] samples",
								unPort2Run, (uint32_t)(pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.int_part >> 32),
								(uint32_t)(pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.int_part));
					}
				}
				break;
			}
			default:
			{
				MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd invalid start flag [%d]",
						me->mtMxID, pPayload->unSecOpCode, (int)pPayload->unStartFlag);
				return ADSP_EBADPARAM;
			}
			}
			return ADSP_EOK;
		}
		default:
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu cmd [%lu] Run handler: rcvd cmd in unexpected state [%d], discarding",
					me->mtMxID, pPayload->unSecOpCode, pCurrentOutPort->outPortState);
			return ADSP_EUNEXPECTED;
		}
		}
	}
	return ADSP_EOK;
}

ADSPResult MtMx_AvSyncReleaseHW(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	ADSPResult result = ADSP_EOK;
	MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Releasing HW Resources for i/p port %lu",me->mtMxID, unInPortID);
	if (ADSP_EOK != (result = avsync_lib_close_hw_resources(pCurrentInPort->pInPortAvSync->pAVSyncLib)))
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to close HW Resources for i/p port %lu with result: %d", unInPortID, result);
	}
	return result;
}

ADSPResult MtMx_AvSyncRequestHW(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	ADSPResult result = ADSP_EOK;
	MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Requesting HW Resources for i/p port %lu",me->mtMxID, unInPortID);
	if (ADSP_EOK != (result = avsync_lib_open_hw_resources(pCurrentInPort->pInPortAvSync->pAVSyncLib)))
	{
		MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to open HW Resources for i/p port %lu with result: %d", unInPortID, result);
	}
	return result;
}

ADSPResult MxAr_InPortSetParamHandler(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxGetSetParamType *pPayload,
		uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	ADSPResult result = ADSP_EOK;
	void *pDataStart;

	MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu received SetParam", me->mtMxID, unInPortID);

	uint16_t unTotalPayloadSize = pPayload->unSize;

	asm_session_param_data_v2_t *pDataPayload = (asm_session_param_data_v2_t *)(pPayload->pnParamData);

	//Code should be capable of handling multiple Set Params in the same command
	//Until Total Payload Size becomes zero, keep reading params
	while(unTotalPayloadSize > 0)
	{
		//If we reach the end or very close to the end, break out
		if(unTotalPayloadSize < sizeof(asm_session_param_data_v2_t))
		{
			break;
		}

		//If the total payload size is incorrect, return an error
		if(unTotalPayloadSize < (sizeof(asm_session_param_data_v2_t) + (pDataPayload->param_size)))
		{
			result = ADSP_ENEEDMORE;
			MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu Total Payload Size: %d < Reqd Size: %d",
					me->mtMxID, unInPortID, unTotalPayloadSize,
					(sizeof(asm_session_param_data_v2_t) + (pDataPayload->param_size)));
			break;
		}

		//Offset by the struct size to get to the start of the data
		pDataStart = (void*)(pDataPayload + 1);

		switch(pDataPayload->module_id)
		{
		case ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC:
		{
			ADSPResult local_result = ADSP_EOK;
			switch(pDataPayload->param_id)
			{

			case ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION:
			{
				//Make sure that the Run Command has not been issued yet. If port is already in run state, reject this Set Param. Default mode will be used.
				if(INPUT_PORT_STATE_INIT != pCurrentInPort->inPortState && INPUT_PORT_STATE_PAUSED != pCurrentInPort->inPortState)
				{
					local_result = ADSP_EUNEXPECTED;
					MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu is in %ld state and cannot accept this SetParam",
							me->mtMxID, unInPortID, pCurrentInPort->inPortState);
				}
				break;
			}
			default:
			{
				//Do nothing, let avsync lib handle.
			}
			}
			if (ADSP_SUCCEEDED(local_result))
			{
				local_result = avsync_lib_set_param(pCurrentInPort->pInPortAvSync->pAVSyncLib,pDataPayload->param_id,pDataStart,pDataPayload->param_size);
			}
			if (ADSP_FAILED(local_result))
			{
				result = local_result;
			}
			break;
		}
		default:
		{
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu Invalid Module ID: %lu",
					me->mtMxID, unInPortID, pDataPayload->module_id);
		}
		}

		//Now, move to the next SetParam and update the total payloadsize
		unTotalPayloadSize -= (sizeof(asm_session_param_data_v2_t) + pDataPayload->param_size);
		pDataPayload = (asm_session_param_data_v2_t*) ( (uint32_t*)pDataStart + (pDataPayload->param_size >> 2) );

		if (((pDataPayload->param_size % 4) != 0) && (unTotalPayloadSize > 0))
		{
			MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu: Param size for set param module ID 0x%lx, param ID %lx is %lu which is not a multiple of 4. Aborting further processing.", me->mtMxID, unInPortID, pDataPayload->module_id, pDataPayload->param_id, pDataPayload->param_size);
			result = ADSP_EFAILED;
			break;
		}
	}

	return result;
}

ADSPResult MxAt_InPortSetParamHandler(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxGetSetParamType *pPayload,
		uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	ADSPResult result = ADSP_EUNSUPPORTED;
	MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu received SetParam", me->mtMxID, unInPortID);
	return result;
}

ADSPResult MxAr_OutPortSetParamHandler(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxGetSetParamType *pPayload,
		uint32_t unOutPortID, MatrixOutPortInfoType *pCurrentOutPort)
{
	ADSPResult result = ADSP_EUNSUPPORTED;
	MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: o/p port %lu received SetParam", me->mtMxID, unOutPortID);
	return result;
}

ADSPResult MxAt_OutPortSetParamHandler(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxGetSetParamType *pPayload,
		uint32_t unOutPortID, MatrixOutPortInfoType *pCurrentOutPort)
{
	ADSPResult result = ADSP_EUNSUPPORTED;
	MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: o/p port %lu received SetParam", me->mtMxID, unOutPortID);
	return result;
}

ADSPResult MxAr_InPortGetParamHandler(This_t *me, elite_msg_any_t *pMsg, EliteMsg_CustomMtMxGetSetParamType *pPayload,
		uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	ADSPResult result = ADSP_EOK;
	void *pDataStart;

	uint16_t unTotalPayloadSize = pPayload->unSize;

	asm_session_param_data_v2_t *pDataPayload = (asm_session_param_data_v2_t *)(pPayload->pnParamData);

	//Code needs to handle only one Get Param command.
	//Multiple Get Params in the same command will not be serviced/supported.
	if(unTotalPayloadSize > 0)
	{
		//Offset by the struct size to get to the start of the data
		pDataStart = (void*)(pDataPayload + 1);

		switch(pDataPayload->module_id)
		{
		case ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC:
		{
			switch(pDataPayload->param_id)
			{
			case ASM_SESSION_MTMX_STRTR_PARAM_SESSION_TIME_V3:
			{
				//Make sure that the payload size is valid.
				if(unTotalPayloadSize < (sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_param_session_time_v3_t)))
				{
					result = ADSP_ENEEDMORE;
					MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu, Payload Size: %d < Reqd Size: %d",
							me->mtMxID, unInPortID, unTotalPayloadSize,
							(sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_param_session_time_v3_t)));
					return result;
				}

				//Access the structure to get hold of the data
				asm_session_mtmx_strtr_param_session_time_v3_t *pData = (asm_session_mtmx_strtr_param_session_time_v3_t*)pDataStart;
				//Get Session Clock from AvSync Lib
				uint64_t ullSessionTime;
				uint64_t ullAbsoluteTime;
				uint64_t ullTimeStamp = pCurrentInPort->ullTimeStampProcessed.int_part;
				avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, SESSION_CLOCK, &(ullSessionTime));
				avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, ABSOLUTE_TS, &(ullAbsoluteTime));
				//Fill up data
				pData->session_time_msw = (uint32_t)(ullSessionTime >> 32);
				pData->session_time_lsw = (uint32_t)(ullSessionTime);
				pData->absolute_time_msw = (uint32_t)(ullAbsoluteTime >> 32);
				pData->absolute_time_lsw = (uint32_t)(ullAbsoluteTime);
				pData->time_stamp_msw = (uint32_t)(ullTimeStamp>>32);
				pData->time_stamp_lsw = (uint32_t)(ullTimeStamp);
				if(pCurrentInPort->bIsTimeStampValid && TRUE == pCurrentInPort->bHasFirstSampleAccumulated)
				{
					pData->flags = ASM_SESSION_MTMX_STRTR_PARAM_SESSION_TIME_TIMESTAMP_VALID_FLAG_BIT_MASK;
				}
				else
				{
					pData->flags = 0;
				}

				//Update the param_size so that the caller is aware of the actual size filled by this get param
				pDataPayload->param_size = sizeof(asm_session_mtmx_strtr_param_session_time_v3_t);
				pPayload->unSize = sizeof(asm_session_mtmx_strtr_param_session_time_v3_t);

				//Debug messages
				MSG_9(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu (GetSessionTime): i/p port id %lu: [session time msw, session time lsw] = [%lu, %lu] : "
						"[absolute time msw, absolute time lsw] = [%lu, %lu]:"
						"[time stamp msw, time stamp lsw] = [%lu, %lu]:"
						"Flags %lu",
						me->mtMxID, unInPortID,
						pData->session_time_msw, pData->session_time_lsw,
						pData->absolute_time_msw, pData->absolute_time_lsw,
						pData->time_stamp_msw, pData->time_stamp_lsw,
						pData->flags);

				break;
			}
			case ASM_SESSION_MTMX_STRTR_PARAM_SESSION_INST_STATISTICS_V2:
			{
				//Make sure that the payload size is valid.
				if(unTotalPayloadSize < (sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_session_inst_statistics_v2_t)))
				{
					result = ADSP_ENEEDMORE;
					MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu, Payload Size: %d < Reqd Size: %d",
							me->mtMxID, unInPortID, unTotalPayloadSize,
							(sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_session_inst_statistics_v2_t)));
					return result;
				}

				//Call a query on the instantaneous stats if both the ends of the stat window has been set properly
				result = avsync_lib_query_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib,
						ASM_SESSION_MTMX_STRTR_PARAM_SESSION_INST_STATISTICS_V2,
						pDataStart);
				if(ADSP_FAILED(result))
				{
					MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu, Query for Inst. stats failed, Res = %d",
							me->mtMxID, unInPortID, result);
					return result;
				}

				//Update the param_size so that the caller is aware of the actual size filled by this get param
				pDataPayload->param_size = sizeof(asm_session_mtmx_strtr_session_inst_statistics_v2_t);
				pPayload->unSize = sizeof(asm_session_mtmx_strtr_session_inst_statistics_v2_t);

				break;
			}
			case ASM_SESSION_MTMX_STRTR_PARAM_SESSION_CUMU_STATISTICS_V2:
			{
				//Make sure that the payload size is valid.
				if(unTotalPayloadSize < (sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_session_cumu_statistics_v2_t)))
				{
					result = ADSP_ENEEDMORE;
					MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu, Payload Size: %d < Reqd Size: %d",
							me->mtMxID, unInPortID, unTotalPayloadSize,
							(sizeof(asm_session_param_data_v2_t) + sizeof(asm_session_mtmx_strtr_session_cumu_statistics_v2_t)));
					return result;
				}

				//Call a query on the instantaneous stats if both the ends of the stat window has been set properly

				result = avsync_lib_query_stat(pCurrentInPort->pInPortAvSync->pAVSyncLib,
						ASM_SESSION_MTMX_STRTR_PARAM_SESSION_CUMU_STATISTICS_V2,
						pDataStart);
				if(ADSP_FAILED(result))
				{
					MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu, Query for Cumm. stats failed, Res = %d",
							me->mtMxID, unInPortID, result);
					return result;
				}


				//Update the param_size so that the caller is aware of the actual size filled by this get param
				pDataPayload->param_size = sizeof(asm_session_mtmx_strtr_session_cumu_statistics_v2_t);
				pPayload->unSize = sizeof(asm_session_mtmx_strtr_session_cumu_statistics_v2_t);

				break;
			}
			default:
			{
				result = ADSP_EBADPARAM;
				MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu Invalid Param ID: %lu",
						me->mtMxID, unInPortID, pDataPayload->param_id);
				return result;
			}
			}
			break;
		}
		default:
		{
			result = ADSP_EBADPARAM;
			MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu Invalid Module ID: %lu",
					me->mtMxID, unInPortID, pDataPayload->module_id);
			return result;
		}
		}
	}
	else
	{
		result = ADSP_EBADPARAM;
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port %lu Invalid unTotalPayloadSize: %d",
				me->mtMxID, unInPortID, unTotalPayloadSize);
		return result;
	}

	return result;
}

ADSPResult MtMx_SetInOutDriftPtr(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	MatrixOutPortInfoType    *pCurrentOutPort = me->outPortParams[unOutPortID];

	if(NULL == pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"MtMx #%lu: i/p port %u, Top prio o/p port %lu AFE Drift Ptr is NULL", me->mtMxID, unInPortID, unOutPortID);
	}
	else
	{
		MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"MtMx #%lu: i/p port %u, Top prio o/p port %lu AFE Drift Ptr %p",
				me->mtMxID, unInPortID, unOutPortID, pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr);
		avsync_lib_set_device_drift_pointer(pCurrentInPort->pInPortAvSync->pAVSyncLib, &(pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr->avt_drift_info));
	}

	return ADSP_EOK;
}

ADSPResult  MtMx_InitInputAvSync(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	ADSPResult result = ADSP_EOK;

	//Allocate memory for pInPortAvSync
	pCurrentInPort->pInPortAvSync = (mt_mx_inport_avsync_t *)qurt_elite_memory_malloc(sizeof(mt_mx_inport_avsync_t), QURT_ELITE_HEAP_DEFAULT);
	if(NULL == pCurrentInPort->pInPortAvSync)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu failed to alloc %lu bytes mem for input port AV-Sync structure.",
				me->mtMxID, unInPortID, sizeof(mt_mx_inport_avsync_t));
		result = ADSP_ENOMEMORY;
		return result;
	}
	else
	{
		MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu alloc %lu bytes mem for input port AV-Sync structure success, Ptr: 0x%p.",
				me->mtMxID, unInPortID, sizeof(mt_mx_inport_avsync_t), pCurrentInPort->pInPortAvSync);
	}
	memset((void*)pCurrentInPort->pInPortAvSync, 0, sizeof(mt_mx_inport_avsync_t));

	//Allocate AV-Sync lib, init it and init s2d drift
	if (ADM_MATRIX_ID_AUDIO_RX == me->mtMxID)
	{
		if(NULL == pCurrentInPort->pInPortAvSync->pAVSyncLib)
		{
			pCurrentInPort->pInPortAvSync->pAVSyncLib = qurt_elite_memory_malloc(avsync_lib_getsize(), QURT_ELITE_HEAP_DEFAULT);
			if(NULL == pCurrentInPort->pInPortAvSync->pAVSyncLib)
			{
				MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu, i/p port %lu failed to create pAvSyncLib", me->mtMxID, unInPortID);
				result = ADSP_ENOMEMORY;
				return result;
			}
		}

		char sInstName[32];
		snprintf(sInstName, 32, "MtMxIp %ld", unInPortID);
		if (ADSP_EOK != (result = avsync_lib_init(pCurrentInPort->pInPortAvSync->pAVSyncLib, sInstName)))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu, i/p port %lu avsync_lib_init failed", me->mtMxID, unInPortID);
			if(pCurrentInPort->pInPortAvSync->pAVSyncLib)
			{
				qurt_elite_memory_free(pCurrentInPort->pInPortAvSync->pAVSyncLib);
				pCurrentInPort->pInPortAvSync->pAVSyncLib = NULL;
			}
			result = ADSP_ENOMEMORY;
			return result;
		}

		if (ADSP_EOK != (result = avsync_lib_init_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib)))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu, i/p port %lu avsync_lib_init_s2d_drift failed", me->mtMxID, unInPortID);
			if(pCurrentInPort->pInPortAvSync->pAVSyncLib)
			{
				qurt_elite_memory_free(pCurrentInPort->pInPortAvSync->pAVSyncLib);
				pCurrentInPort->pInPortAvSync->pAVSyncLib = NULL;
			}
			result = ADSP_ENOMEMORY;
			return result;
		}

		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu, i/p port %lu create pAvSyncLib: 0x%p success", me->mtMxID, unInPortID, pCurrentInPort->pInPortAvSync->pAVSyncLib);
	}

	return result;
}

ADSPResult  MtMx_DeInitInputAvSync(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	ADSPResult result = ADSP_EOK;

	//Free SampleSlip library if it has been created.
	mt_mx_sampleslip_t *pSampleSlip = &(pCurrentInPort->pInPortAvSync->structSampleSlip);
	if (NULL != pSampleSlip->pSampleSlipAppi)
	{
		(void)pSampleSlip->pSampleSlipAppi->vtbl_ptr->end(pSampleSlip->pSampleSlipAppi);
		MTMX_FREE(pSampleSlip->pSampleSlipAppi);
	}
	memset(pSampleSlip->inBufs,0,sizeof(pSampleSlip->inBufs));
	memset(pSampleSlip->outBufs,0,sizeof(pSampleSlip->outBufs));

	if(NULL != pCurrentInPort->pInPortAvSync)
	{
	   //Deinit s2d drift, deinit AV-Sync lib and free its memory
	   if(NULL != pCurrentInPort->pInPortAvSync->pAVSyncLib)
	   {
	  	   avsync_lib_deinit_s2d_drift(pCurrentInPort->pInPortAvSync->pAVSyncLib);
		   avsync_lib_deinit(pCurrentInPort->pInPortAvSync->pAVSyncLib);
		   qurt_elite_memory_free(pCurrentInPort->pInPortAvSync->pAVSyncLib);
		   pCurrentInPort->pInPortAvSync->pAVSyncLib = NULL;
	   }

	   //Free memory allocated for the AV-Sync pointer
	   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu, Freeing pInPortAvSync 0x%p", me->mtMxID, unInPortID, pCurrentInPort->pInPortAvSync);
  	   MTMX_FREE(pCurrentInPort->pInPortAvSync);
	}

	return result;
}

ADSPResult MtMx_InitOutputAvSync(This_t *me, uint32_t unOutPortID)
{
	MatrixOutPortInfoType    *pCurrentOutPort = me->outPortParams[unOutPortID];
	ADSPResult result = ADSP_EOK;

	//Allocate memory for the AV-Sync pointer
	pCurrentOutPort->pOutPortAvSync = (mt_mx_outport_avsync_t *)qurt_elite_memory_malloc(sizeof(mt_mx_outport_avsync_t), QURT_ELITE_HEAP_DEFAULT);
	if(NULL == pCurrentOutPort->pOutPortAvSync)
	{
		result = ADSP_ENOMEMORY;
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error: MtMx #%lu o/p port %lu failed to alloc %lu bytes mem for output port AV-Sync structure.",
				me->mtMxID, unOutPortID, sizeof(mt_mx_outport_avsync_t));
		return result;
	}
	else
	{
		MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu o/p port %lu alloc %lu bytes mem for output port AV-Sync structure success, Ptr: 0x%p.",
				me->mtMxID, unOutPortID, sizeof(mt_mx_outport_avsync_t), pCurrentOutPort->pOutPortAvSync);
	}
	memset((void*)pCurrentOutPort->pOutPortAvSync, 0, sizeof(mt_mx_outport_avsync_t));

	return result;
}

ADSPResult MtMx_DeInitOutputAvSync(This_t *me, uint32_t unOutPortID)
{
	MatrixOutPortInfoType    *pCurrentOutPort = me->outPortParams[unOutPortID];
	ADSPResult result = ADSP_EOK;

	//Free memory allocated for the AV-Sync pointer
	if(NULL != pCurrentOutPort->pOutPortAvSync)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: o/p port %lu, Freeing pOutPortAvSync 0x%p", me->mtMxID, unOutPortID, pCurrentOutPort->pOutPortAvSync);
		MTMX_FREE(pCurrentOutPort->pOutPortAvSync);
	}

	return result;
}

ADSPResult MtMx_SetInputDriftPtr(This_t *me, uint32_t unInPortID, volatile const afe_drift_info_t *pAfeDriftPtr)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	pCurrentInPort->pInPortAvSync->pAfeDriftPtr = pAfeDriftPtr;
	return ADSP_EOK;
}

ADSPResult MtMx_SetOutputDriftPtr(This_t *me, uint32_t unOutPortID, volatile const afe_drift_info_t *pAfeDriftPtr)
{
	MatrixOutPortInfoType    *pCurrentOutPort = me->outPortParams[unOutPortID];
	pCurrentOutPort->pOutPortAvSync->pAfeDriftPtr = pAfeDriftPtr;
	return ADSP_EOK;
}

ADSPResult MtMx_CheckIfInportMovesAnyOutportToActiveState(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType       *pCurrentInPort = me->inPortParams[unInPortID];
	MatrixOutPortInfoType      *pCurrentOutPort;
	uint32                     strMask = pCurrentInPort->strMask;
	uint32                     unOutPortID;
	while (strMask)
	{
		unOutPortID = Q6_R_ct0_R(strMask);
		strMask ^= (1 << unOutPortID);
		pCurrentOutPort = me->outPortParams[unOutPortID];

		if (OUTPUT_PORT_STATE_WAITING_TO_BE_ACTIVE == pCurrentOutPort->outPortState)
		{
			mt_mx_decrement_time(&pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
			// accurate to the nearest 1us
			if (pCurrentOutPort->pOutPortAvSync->ullOutBufHoldDurationInUsec.int_part < pCurrentOutPort->unFrameDurationInUsec.int_part)
			{
				MtMx_RunOutputPort(me, unOutPortID);
				pCurrentOutPort->bIsFirstOutBufYetToBeSent = TRUE;
			}
		}
	}

	return ADSP_EOK;
}

ADSPResult MtMx_UpdateExpectedST(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType    *pCurrentInPort = me->inPortParams[unInPortID];
	uint64_t ullSessionTime = 0;
	avsync_lib_get_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, SESSION_CLOCK, &ullSessionTime);
	uint64_t proposed_expected_session_clock = ullSessionTime + pCurrentInPort->unFrameDurationInUsec.int_part;
	avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, EXPECTED_SESSION_CLOCK, &proposed_expected_session_clock, sizeof(proposed_expected_session_clock));
	return ADSP_EOK;
}

ADSPResult MtMx_ResetSessionTimeClock(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];
	avsync_lib_set_internal_param(pCurrentInPort->pInPortAvSync->pAVSyncLib, SESSION_CLOCK, &AVSYNC_LIB_ZERO_SESSION_CLOCK, sizeof(AVSYNC_LIB_ZERO_SESSION_CLOCK));
	pCurrentInPort->ullNumSamplesForStcUpdate = 0;
	pCurrentInPort->ullTimeStampForStcBaseInUsec.int_part = 0;
	pCurrentInPort->ullTimeStampForStcBaseInUsec.frac_part = 0;
	return ADSP_EOK;
}

ADSPResult MxArUpdateInputPortSessionTime(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort,
		MatrixOutPortInfoType *pCurrentOutPort, uint32_t ullCurrentDevicePathDelay)
{
	uint32_t unSampleRate = pCurrentInPort->unSampleRate;
	uint32_t unPortPerChBufSize = pCurrentInPort->unInPortPerChBufSize;
	uint64_t ullNumSamplesForStcUpdate = pCurrentInPort->ullNumSamplesForStcUpdate;
	uint32_t unSampleIncrement = 0;

	// When adding an additional device (running at a different rate) to an existing session it is
	// possible that that output port will send data downstream before reconfiguring to the new rate.
	// During this transition the input port may have already reconfigured itself and it's state
	// will reflect the new sampling rate; however, the data being sent down is at the old rate.
	// In this case we must use the old rate information to update the STC. The old rate information
	// is the same as the output port's rate.
	if (pCurrentInPort->unSampleRate != pCurrentOutPort->unSampleRate)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port fs %lu does not match o/p port fs %lu. Using old rate for ST update!",
				me->mtMxID, pCurrentInPort->unSampleRate, pCurrentOutPort->unSampleRate);
		unSampleRate = pCurrentOutPort->unSampleRate;
		unPortPerChBufSize = pCurrentOutPort->unOutPortPerChBufSize;

		// Convert the sample count to the old base using the previous sampling rate.
		// sample_base_old = sample_base_new * (fs_prev / fs_new)
		ullNumSamplesForStcUpdate = (pCurrentInPort->ullNumSamplesForStcUpdate * pCurrentOutPort->unSampleRate) / pCurrentInPort->unSampleRate;
	}

	//Default rendering mode
	if(pCurrentInPort->pInPortAvSync->bShouldSessionTimeBeAdjWhenSendingBufDown)
	{
		if (MT_MX_INPUT_PORT_ADD_SAMPLES == pCurrentInPort->pInPortAvSync->samplesAddOrDropMask)
		{
			unSampleIncrement += (unPortPerChBufSize - MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER - pCurrentInPort->ullStaleEosSamples);
		}
		else
		{
			unSampleIncrement += (unPortPerChBufSize + MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER - pCurrentInPort->ullStaleEosSamples);
		}
	}
	else
	{
		unSampleIncrement += unPortPerChBufSize - pCurrentInPort->ullStaleEosSamples;
	}
	ullNumSamplesForStcUpdate += unSampleIncrement;
	pCurrentInPort->ullNumSamplesForStcUpdate += unSampleIncrement;

	// overflow protection logic is unnecessary since the calculation below will overflow after ~1000 days
	// The Proposed Session Clock = Equivalent time for the STC Update + The Base Time for STC
	uint64_t proposed_session_clock = (MT_MX_SAMPLES_TO_USEC(ullNumSamplesForStcUpdate, unSampleRate))+ (pCurrentInPort->ullTimeStampForStcBaseInUsec.int_part);

#ifdef MT_MX_EXTRA_DEBUG
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu updating STC to [%lu, %lu]",
			me->mtMxID, unInPortID, (uint32_t)(proposed_session_clock>>32), (uint32_t)proposed_session_clock);
#endif

	avsync_lib_update_session_clock(pCurrentInPort->pInPortAvSync->pAVSyncLib, proposed_session_clock);
	uint64_t ullStaleEosSamplesuSec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->ullStaleEosSamples, unSampleRate);
	avsync_lib_update_absolute_time(pCurrentInPort->pInPortAvSync->pAVSyncLib, ullCurrentDevicePathDelay-ullStaleEosSamplesuSec, FALSE);
	return ADSP_EOK;
}
