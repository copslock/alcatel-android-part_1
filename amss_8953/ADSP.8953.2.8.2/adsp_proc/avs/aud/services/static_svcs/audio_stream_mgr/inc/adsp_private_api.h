/*========================================================================
adsp_privatedefs.h

This file contains private ADSP APIs

Copyright (c) 2010 Qualcomm Technologies, Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary.
======================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/static_svcs/audio_stream_mgr/inc/adsp_private_api.h#7 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
02/25/11   RP      Created

========================================================================== */
/**
@file adsp_privatedefs.h

@brief This file contains private ADSP APIs
*/

#include "adsp_private_api_ext.h"
#include "adsp_prv_avcs_api.h"


#ifndef _ADSP_PRIVATEAPI_H_
#define _ADSP_PRIVATEAPI_H_

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/** @addtogroup adsp_privatedefs
    @{ */

/** Topology ID for creating the DAK in the POPP.
*/
#define ASM_STREAM_POSTPROC_TOPO_ID_DAK                           0x00010BE6

/** Topology ID for creating the DS1AP in the POPP.
*/
#define ASM_STREAM_POSTPROC_TOPO_ID_DS1AP                         0x00010DDD

/**
  Internal API. Operation code for simulating a malloc failure
  (for testing stability). */
#define AVCS_CMD_SIMULATE_MALLOC_FAILURE    0x0001290F

/**  Internal API. Opcode for exiting the premordial main
 *   thread */
#define AVCS_CMD_EXIT_PREMORDIAL_THREAD     0x00012910

/**  Internal API. Opcode for deleting all the stored licenses. */
#define AVCS_CMD_DELETE_LICENSES             0x00012911

/**  Internal API. Opcode for deleting all the custom topologies. */
#define AVCS_CMD_CLEAR_PP_DBS   0x00012913


/**
    ID of the AVSYNC Test module.for collecting AVSYNC statistics

    This module supports the following parameter IDs:
    - #AFE_PRV_PARAM_ID_AVSYNC_STATS
    - #AFE_PARAM_ID_ENABLE

    Opcode -- AFE_PRV_MODULE_AVSYNC_TEST
  @newpage
*/
#define AFE_PRV_MODULE_AVSYNC_TEST			0x0001021A

#define AFE_PRV_PARAM_ID_AVSYNC_STATS		0x0001021B

typedef struct afe_prv_avsync_stats_param_t afe_prv_avsync_stats_param_t;

#include "adsp_begin_pack.h"
struct afe_prv_avsync_stats_param_t
{
   uint32_t 	  non_zero_ts_lsw;
   /**< LSW of the timestamp of the nonzero sample */

   uint32_t 	  non_zero_ts_msw;
   /**< MSW of the timestamp of the nonzero sample. the 64 bit value formed by LSW and MSW is interpreted as an unsigned number */

   uint16_t       is_valid;
   /**< whether above TS is valid. 1-> valid, 0-> invalid.*/

   uint16_t       reserved;
   /**< must be set to zero */
}
#include "adsp_end_pack.h"
;

/** @ingroup audio_mtmx_strtr_module_ids
    This module be used for DTS down-mixer related parameters.

    This module supports the following parameter IDs:
       - #DOWNMIXER_PARAM_ID_MIX_LFE_TO_FRONT 
       - #DOWNMIXER_PARAM_ID_STEREO_MODE 
*/
#define MTMX_MODULE_ID_DTS_DOWNMIXER                                  0x00010337

/* Structure payload for:
 * DOWNMIXER_PARAM_ID_MIX_LFE_TO_FRONT*/
 
/** If enabled, this parameter configures the down-mixer to mix the LFE channel
       to the front channel during down-mixing.

       By default, it is disabled

    @msgpayload{downmixer_param_id_mix_lfe_to_front_t}
*/
#define DOWNMIXER_PARAM_ID_MIX_LFE_TO_FRONT              0x00010338

typedef struct downmixer_param_id_mix_lfe_to_front_t downmixer_param_id_mix_lfe_to_front_t;

#include "adsp_begin_pack.h"

struct downmixer_param_id_mix_lfe_to_front_t
{
    uint32_t                  mix_lfe_to_front_enable;
	/* 0: Disabled, 1: Enabled*/
}
#include "adsp_end_pack.h"
;

/* Structure payload for:
 * DOWNMIXER_PARAM_ID_STEREO_MODE*/
 
/** This parameter specifies the stereo mode of the down-mixer.

    @msgpayload{downmixer_param_id_stereo_mode_t}
*/
#define DOWNMIXER_PARAM_ID_STEREO_MODE              0x00010339

typedef struct downmixer_param_id_stereo_mode_t downmixer_param_id_stereo_mode_t;

#include "adsp_begin_pack.h"

struct downmixer_param_id_stereo_mode_t
{
    uint32_t                  stereo_mode;
	/* 0: LO, RO mode (left only, right only)
	     1: LT, RT mode (left total, right total)
     */
}
#include "adsp_end_pack.h"
;
/** @endcond */



/** @ingroup asm_svc_register_for_adspp_vote_notif
    Registers with ASM for any ADSPPM votes.

  @apr_hdr_fields
    Opcode -- ASM_CMD_REGISTER_FOR_ADSPPM_VOTES \n
    Dst_port -- 0

  @apr_msgpayload none

  @detdesc
     Registration enables ASM to send track ADSPPM voting & enable query.

  @return
    APRV2_IBASIC_RSP_RESULT (refer to @xhyperref{Q3,[Q3]}).
    The handle is defined by the client as input.

  @dependencies
    None.
*/
#define ASM_CMD_REGISTER_FOR_ADSPPM_VOTES    0x00012F36


/** @ingroup asm_svc_deregister_for_adspp_vote_notif
    Deregisters with ASM for any ADSPPM votes.

  @apr_hdr_fields
    Opcode -- ASM_CMD_DEREGISTER_FROM_ADSPPM_VOTE_EVENT \n
    Dst_port -- 0

  @apr_msgpayload none

  @detdesc
     Deregistration stops ASM from tracking ADSPPM voting.

  @return
    APRV2_IBASIC_RSP_RESULT (refer to @xhyperref{Q3,[Q3]}).
    The handle is defined by the client as input.

  @dependencies
    None.
*/
#define ASM_CMD_DEREGISTER_FROM_ADSPPM_VOTES    0x00012F35

/** @ingroup asm_event_adsppm_vote_done
    This is an cmd to query ADSPPM votes from ADSP

  @apr_hdr_fields
    Opcode -- ASM_CMD_GET_ADSPPM_VOTES \n
    Src_port: - zero

    @apr_msg_payload{asm_event_adsppm_vote_done_t}
    @table{weak_asm_event_adsppm_vote_done_t}

  @return
    ASM_CMDRSP_GET_ADSPPM_VOTES

  @dependencies
    The session/stream must be a valid and opened write or read/write
    session/stream.
*/
#define ASM_CMD_GET_ADSPPM_VOTES                           0x00012F34


/** @ingroup asm_event_adsppm_vote_done
    This is an cmd to query ADSPPM votes from ADSP

  @apr_hdr_fields
    Opcode -- ASM_CMD_GET_ADSPPM_VOTES \n
    Src_port: - zero

    @apr_msg_payload{asm_event_adsppm_vote_done_t}
    @table{weak_asm_event_adsppm_vote_done_t}

  @return
    None

  @dependencies
    The session/stream must be a valid and opened write or read/write
    session/stream.
*/
#define ASM_CMDRSP_GET_ADSPPM_VOTES                        0x00012F33

/* Structure for a ASM_CMD_GET_ADSPPM_VOTES. */
typedef struct asm_cmdrsp_get_adsppm_votes_t asm_cmdrsp_get_adsppm_votes_t;

#define ASM_CMDRSP_ADSPPM_VOTE_MASK_MIPS      (1)
#define ASM_CMDRSP_ADSPPM_VOTE_MASK_BW        (1<<1)
#define ASM_CMDRSP_ADSPPM_VOTE_MASK_LATENCY   (1<<2)

#include "adsp_begin_pack.h"
/** @weakgroup weak_asm_cnd_get_adsppm_votes_t
@{ */
/* Payload of the #ASM_CMD_GET_ADSPPM_VOTES command*/
struct asm_cmdrsp_get_adsppm_votes_t
{
   uint32_t event_mask;
   /** which event is raised. Masks are defined above as
    *  ASM_EVENT_ADSPPM_VOTE_MASK_MIPS etc*/

   uint32_t mips_per_thread;
   /** MIPs per thread */
   uint32_t total_mips;
   /** total MIPs*/

   uint32_t bw_adsp_ddr_Bps_lsw;
   /** LSW of ADSP-DDR bandwidth */
   uint32_t bw_adsp_ddr_Bps_msw;
   /** MSW of ADSP-DDR bandwidth */
   uint32_t bw_adsp_lpm_Bps_lsw;
   /** LSW of ADSP-LPM bandwidth */
   uint32_t bw_adsp_lpm_Bps_msw;
   /** MSW of ADSP-LPM bandwidth */

   uint32_t latency_us;
   /** Latency vote */
   uint32_t latency_request;
   /** Latency is released (0) or requested (1) */
}

#include "adsp_end_pack.h"
;
/** @} */ /* end_weakgroup weak_asm_event_adsppm_vote_done_t */



/* Setparam IDs for Dolby Heaac Decoder*/

/** @cond OEM_only */
/** @addtogroup asmstrm_cmd_set_encdec_params
@{ */
/* The following HEAAC parameters make use of the generic
 *  structure asm_heaac_generic_param_t
 */
/** ID of the Dolby Pulse High Efficiency AAC (HEAAC) Number of Output Channels
    parameter in the #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.

    This parameter applies only to #ASM_MEDIA_FMT_AAC_V2 bitstream decoding. The command is to be sent
    after opening a write or read/write stream and before actual decoding
    starts.

    @msgpayload
    Specifies the flag to enable 5.1 output configuration.
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - 0 -- 5.1 output mode disabled (Default)
    - 6 -- 5.1 output mode enabled
*/
#define ASM_PARAM_ID_HEAAC_NUM_OUTPUT_CHANNELS                    0x00012F20


/** ID of the Dolby Pulse HEAAC Error Conceal parameter in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This parameter specifies whether to enable error concealment.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    Do not send this command during decoding; such a change might introduce a
    blank frame in the middle of playback.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - 0 -- Disable concealment (Default)
    - 1 -- Enable concealment
*/
#define ASM_PARAM_ID_HEAAC_ERROR_CONCEAL                        0x00012F19


/** ID of the Dolby Pulse HEAAC 2-to-1 Resampling parameter in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This parameter specifies whether to enable 2-to-1 resampling.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    Do not send this command during decoding; such a change might introduce
    discontinuities in the middle of playback.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - 0 -- Disable 2-to-1 resampling (Default)
    - 1 -- Enable 2-to-1 resampling
*/
#define ASM_PARAM_ID_HEAAC_2TO1_RESAMPLING                        0x00012F1A


/** ID of the Dolby Pulse HEAAC Replay Gain
    parameter in the #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This parameter specifies whether the Replay Gain value is applied.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    Do not send this command during decoding; such a change might introduce
    discontinuities in the output.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - Range from -72 to 55 (-18.0dB to 13.75dB in steps of 0.25dB)
    - Default value -- 0
*/
#define ASM_PARAM_ID_HEAAC_REPLAY_GAIN                            0x00012F1B


/** ID of the Dolby Pulse HEAAC default Program Reference Level (PRL)
    parameter in the #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This parameter specifies the Default PRL value to be applied.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    Do not send this command during decoding; such a change might introduce
    discontinuities in the middle of playback.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - Range of 0 to 127 (0dB to -31.75dB in steps of 0.25dB)
    - Default value -- 124 @newpage
*/
#define ASM_PARAM_ID_HEAAC_DEFAULT_PRL                            0x00012F1C


/** ID of the Dolby Pulse HEAAC DRC Type and Mode parameter in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This parameter specifies whether Dynamic Range Control and Leveling is
    enabled and how it is configured.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    The lower 16 bits indicate the DRC mode and the higher 16 bits indicate
    the DRC type.

    @msgpayload
    Specifies a value that is applied to the DRC type and mode.
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values{for Type}
    - 0 -- None
    - 1 -- MPEG-DRC
    - 2 -- DVB-compression
    - 3 -- Prefer DRC
    - 4 -- Prefer compression
    - 5 -- Portable mode

    @values{for Mode} Application of DRC/compression and consideration of PRL
    on the PCM output of the decoder.
    - 0 -- None
    - 1 -- DRC/compression
    - 2 -- PRL
    - 3 -- DRC/compression and PRL

    @values{for Default}
    - 0 -- Type
    - 2 -- Default mode @newpage
*/
#define ASM_PARAM_ID_HEAAC_DRC_TYPE_MODE                        0x00012F1D


/** ID of the Dolby Pulse HEAAC External Boost parameter in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This postprocessing parameter specifies whether to apply external boost
    on the decoded PCM samples.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - 0 -- Do not apply (Default)
    - 1 -- Apply external boost
*/
#define ASM_PARAM_ID_HEAAC_EXT_BOOST                            0x00012F1E


/** ID of the Dolby Pulse HEAAC External Metadata parameter in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
    This is a post-processing parameter specifies whether or not to apply
    the external metadata on the decoded PCM samples.

    This parameter is used only for internal testing and ITAF verification.
    The command is to be sent after opening a write or read/write stream and
    before actual decoding starts.

    @msgpayload
    For the payload format, see asm_heaac_generic_param_t.
    @par
    @values
    - 0 -- Do not apply (Default)
    - 1 -- Apply external metadata
*/
#define ASM_PARAM_ID_HEAAC_EXT_METADATA                            0x00012F1F


/* Structure for HEAAC Generic Parameter. */
typedef struct asm_heaac_generic_param_t asm_heaac_generic_param_t;

#include "adsp_begin_pack.h"

/** Payload of the generic HEAAC parameters in the
    #ASM_STREAM_CMD_SET_ENCDEC_PARAM command.
*/
struct asm_heaac_generic_param_t
{
    uint32_t                  generic_parameter;
    /**< Generic parameter field used by the following HEAAC parameter IDs:
         - #ASM_PARAM_ID_HEAAC_NUM_OUTPUT_CHANNELS
         - #ASM_PARAM_ID_HEAAC_ERROR_CONCEAL
         - #ASM_PARAM_ID_HEAAC_2TO1_RESAMPLING
         - #ASM_PARAM_ID_HEAAC_REPLAY_GAIN
         - #ASM_PARAM_ID_HEAAC_DEFAULT_PRL
         - #ASM_PARAM_ID_HEAAC_DRC_TYPE_MODE
         - #ASM_PARAM_ID_HEAAC_EXT_BOOST
         - #ASM_PARAM_ID_HEAAC_EXT_METADATA @tablebulletend @newpagetable */
}
#include "adsp_end_pack.h"
;

/** @} */ /* end_addtogroup asmstrm_cmd_set_encdec_params */

/** @} */ /* end_addtogroup adsp_privatedefs */

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* _ADSP_PRIVATEAPI_H_ */
