/*========================================================================
AudDevMgr_PrivateDefs.h

This file contains ADM commands that are currently not exposed via public APIs.

Copyright (c) 2012 Qualcomm Technologies, Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary.
======================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/static_svcs/audio_dev_mgr/inc/AudDevMgr_PrivateDefs.h#6 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/17/12   KR      Created

========================================================================== */
/**
@file AudDevMgr_PrivateDefs.h

@brief This file contains ADM commands that are currently not exposed via public APIs.
*/

#ifndef _AUDDEVMGR_PRIVATEDEFS_H_
#define _AUDDEVMGR_PRIVATEDEFS_H_

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

//Defining internal topology IDs for default COPP topology in either direction
#define ADM_CMD_COPP_OPEN_TOPOLOGY_ID_DEFAULT_AUDIO_COPP_RX 0xE0010BE3
#define ADM_CMD_COPP_OPEN_TOPOLOGY_ID_DEFAULT_AUDIO_COPP_TX 0xF0010BE3

//Defining internal topology ID for topology containing no modules for uncompressed data
#define AUDPROC_TOPOLOGY_ID_UNCOMPRESSED_NONE                         0xE0010774

/** @addtogroup mtmx_module_delay_latency
@{ */
/** ID of the Delay/Latency module on the LPCM data path.

    This module introduces the specified amount of delay in the PSPD path.
    If the delay is increased, silence is inserted. If the delay is decreased,
    data is dropped. This module can be set through the command 
    ADM_CMD_SET_PSPD_MTMX_STRTR_PARAMS_V5.

    There are no smooth transitions. The resolution of the delay applied is
    limited by the period of a single sample. Qualcomm recommends muting the
    device path when the delay is changed (to avoid glitches).
*/
#define MTMX_MODULE_ID_DELAY                    0x00010347

/** ID of the Delay parameter used by MTMX_MODULE_ID_DELAY. This parameter will be
    set through the command ADM_CMD_SET_PSPD_MTMX_STRTR_PARAMS_V5.
    This parameter ID can be retrived through ADM_CMD_GET_PSPD_MTMX_STRTR_PARAMS_V5.
	
    @msgpayload{mtmx_delay_param_t}
    @table{weak__mtmx__delay__param__t}
*/
#define MTMX_PARAM_ID_DELAY                     0x00010348

/** @} */ /* end_addtogroup mtmx_module_delay_latency */

/* Structure for delay parameter in LPCM data paths. */
typedef struct mtmx_delay_param_t mtmx_delay_param_t;

#include "adsp_begin_pack.h"

/** @weakgroup weak_mtmx_delay_param_t
@{ */
/* Payload of the MTMX_PARAM_ID_DELAY parameter used by
    MTMX_MODULE_ID_DELAY.
*/
struct mtmx_delay_param_t
{
    uint32_t                  delay_us;
    /**< Delay in micro seconds.

         @values 0 to 100000

         The amount of delay must be greater than zero.  If the value is 
         zero, this module is disabled.

         The actual resolution of the delay is limited by the period of a
         single audio sample. If a single audio sample duration is a fraction 
         of a microsecond, then delay in number of samples would be floored to
         integer value*/
}
#include "adsp_end_pack.h"
;
/** @} */ /* end_weakgroup weak_mtmx_delay_param_t */
/** @endcond */

/** @} */ /* end_addtogroup AudDevMgr_PrivateDefs */

/** @} */ /* end_weakgroup weak_asm_event_adsppm_vote_done_t */

/** @} */ /* end_addtogroup adsp_privatedefs */

/** @addtogroup admsvc_cmd_open_device
@{ */
/** Opens a COPP/voice processing Tx module and sets up the device
    session.

    Various configurations are supported (based on the mode_of_operation value):
    -# Opens an Audio COPP and sets up the device session: Rx matrix -> COPP -> AFE
    -# Opens an Audio COPP and sets up the device session: AFE -> COPreP -> Tx matrix (Live mode)
    -# Opens an Audio COPP and sets up the device session: AFE -> COPreP -> Tx matrix (Non-live mode)
    -# Opens an Audio COPP and sets up the device session: Rx matrix -> COPP -> Tx matrix
    -# Sets up the device session: Rx stream router -> AFE
    -# Sets up the device session: AFE -> Tx stream router

  @apr_hdr_fields
    Opcode -- ADM_CMD_DEVICE_OPEN_V6 \n
    Dst_port -- Ignored

  @apr_msgpayload{adm_cmd_device_open_v6_t}
    @table{weak__adm__cmd__device__open__v6__t}

  @return
    #ADM_CMDRSP_DEVICE_OPEN_V6 with the resulting status and COPP ID.

  @dependencies
    None.
*/
#define ADM_CMD_DEVICE_OPEN_V6                                    (0x00010351UL)

/** Definition for the ADM native mode channels bitmask. */
#define ADM_BIT_MASK_NATIVE_MODE                                  (0x000000C0UL)

/** Definition for the ADM native mode channels shift value. */
#define ADM_BIT_SHIFT_NATIVE_MODE_CHANNELS                            7

/** Definition for the ADM native mode bit width shift value. */
#define ADM_BIT_SHIFT_NATIVE_MODE_BIT_WIDTH                           6

/* ADM device open command payload. */
typedef struct adm_cmd_device_open_v6_t adm_cmd_device_open_v6_t;

#include "adsp_begin_pack.h"

/** @weakgroup weak_adm_cmd_device_open_v6_t
@{ */
/* Payload of the ADM_CMD_DEVICE_OPEN_V6 command.
*/
struct adm_cmd_device_open_v6_t
{
    uint16_t                  flags;
    /**< Used to configure the device session based on the bitmasks.

         The device_perf_mode flag (bits 15 to 13) indicates the performance
         mode in which this device session must be opened.

         @values{for bits 15 to 13}
         - #ADM_LEGACY_DEVICE_SESSION -- Opens a legacy device session using
           default buffer sizes
         - #ADM_LOW_LATENCY_DEVICE_SESSION -- Opens a low latency device session
           by using shortened buffers in the matrix and COPP
            - Recommendation: Do not enable high latency algorithms; they might
              negate the benefits of opening a low latency device, and they
              might also suffer quality degradation from unexpected jitter.
            - The aDSP data path input latency contributed from the device side
              only can vary from 1 to 2 ms. The aDSP data path output latency
              contributed from the device side only can vary from 2 to 4 ms.
              In a steady state playback, latency is measured as the average
              sample time difference between the sample that is currently being
              read from shared memory and the sample that is currently rendered
              to the DMA.

         @contcell
         @values{for bits 15 to 13 (cont.)}
         - #ADM_ULTRA_LOW_LATENCY_DEVICE_SESSION -- Opens an ULL
           COPP session. There is no COPP or Matrix processing involved.
           - Recommendation: Open the COPP whose bits per sample and
             number of channels match the device to avoid processing
             in the aDSP.
           - Only PCM, 48 kHz, 16-bit/24-bit, mono/stereo data are supported.
           - The aDSP data path output latency contributed from the COPP session
             is 0 ms. The end-to-end aDSP data path output latency is 2 ms,
             which is the AFE contribution.
         - Use #ADM_BIT_MASK_DEVICE_PERF_MODE_FLAG and
           #ADM_BIT_SHIFT_DEVICE_PERF_MODE_FLAG to configure this subfield.

         The native_mode flags (bits 6 and 7) configures Native Mode operation
         for this device session. Native Mode operation is only applicable for
         PCM playback use cases and will be ignored for compressed and PCM record
         use cases. One or more types of native mode may be enabled simultaneously.
         Native mode is disabled if all bits are clear.

         @values{for bit 7}

         - Channel Nativity -- All streams in the session will have their channel
         content/mapping unified. The following rules apply:
            - There are a maximum of 8 channel mappings allowed at any one time in
              a given session.
            - The stream with the largest number of channels is considered high
              priority and will have their content mapped first followed by the
              stream with the next largest number of channels until all 8 possible
              channel mappings are used.
            - If two streams have the same number of channels priority is given to
              the stream connected to the input port with the lowest index.
            - Once all the channel mapping slots are exhausted any remaining lower
              priority streams will have their content added to the appropriate
              common channel mappings set by the higher priority stream. If all 8
              channel mappings are used and a lower priority stream does not share a
              common mapping with a higher priority stream the content for that channel
              is dropped from the output.

         - Use #ADM_BIT_MASK_NATIVE_MODE and #ADM_BIT_SHIFT_NATIVE_MODE_CHANNELS
           to configure this subfield.

         @values{for bit 6}
         - Bit Width Nativity -- All streams in a given session will be converted
         to the same bit-width. The bit-width is set by the stream with the highest
         bit-width. All streams with a smaller bit-width are upconverted.

         - Use #ADM_BIT_MASK_NATIVE_MODE and #ADM_BIT_SHIFT_NATIVE_MODE_BIT_WIDTH
           to configure this subfield.

         All other bits are reserved; clients must set them to zero. */

    uint16_t                  mode_of_operation;
    /**< Specifies whether the COPP is opened on the Tx or Rx path.

         @values
         - #ADM_CMD_COPP_OPEN_MODE_OF_OPERATION_RX_PATH_COPP
         - #ADM_CMD_COPP_OPEN_MODE_OF_OPERATION_TX_PATH_LIVE_COPP
         - #ADM_CMD_COPP_OPEN_MODE_OF_OPERATION_TX_PATH_NON_LIVE_COPP
         - #ADM_CMD_COPP_OPEN_MODE_OF_OPERATION_LOOPBACK_COPP
         - #ADM_CMD_DEVICE_OPEN_MODE_OF_OPERATION_COMPRESSED_RX
         - #ADM_CMD_DEVICE_OPEN_MODE_OF_OPERATION_COMPRESSED_TX

         Live connections cause sample discarding in the Tx device matrix if
         the destination output ports do not pull them fast enough. Non-live
         connections queue the samples indefinitely. */

    uint16_t                  endpoint_id_1;
    /**< Logical and physical endpoint ID of the audio path. If the ID is a
         voice processor Tx block, it receives near samples.

         @values Any pseudoport, AFE Rx port, or AFE Tx port (for a list of
                 valid IDs, refer to @xhyperref{Q6,[Q6]})

         This value is ignored for mode_of_operation = 4 (loopback use case). */
    /* Q6 = Hex MM ADSP.BF.2.0: Audio Front End API Interface Spec (TBD) */

    uint16_t                  endpoint_id_2;
    /**< Second logical and physical endpoint ID for a voice processor Tx
         block. This is not applicable to audio COPP.

         @values
         - AFE Rx port (for a list of valid IDs, refer to @xhyperref{Q6,[Q6]})
         - 0xFFFF -- Endpoint 2 is unavailable and the voice processor Tx
           block ignores this endpoint

         When the voice processor Tx block is created on the audio record path,
         it can receive far-end samples from an AFE Rx port if the voice call
         is active. The ID of the AFE port is provided in this field.

         This value is ignored for:
         - mode_of_operation = 4 (loopback use case)
         - mode_of_operation = 5 and 6 (compressed use cases) @tablebulletend */

    uint32_t                  topology_id;
    /**< Audio COPP or voice processor Tx topology ID.

       @if OEM_only
         @values
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_DEFAULT_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_NONE_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_MONO_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_STEREO_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_MIC_MONO_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_MIC_STEREO_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_STEREO_IIR_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_MCH_PEAK_VOL
         - #AUDPROC_TOPOLOGY_ID_COMPRESSED_DEFAULT
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_MONO_AUDIO_COPP_MBDRCV2
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_STEREO_AUDIO_COPP_MBDRCV2
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SPEAKER_STEREO_IIR_AUDIO_COPP_MBDRCV2
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_PEAKMETER_AUDIO_COPP
         - #ADM_CMD_COPP_OPEN_TOPOLOGY_ID_SRS_TRUMEDIA_TOPOLOGY
       @endif

         For a complete list of audio topology IDs, see Section
         @xref{hdr:AudioPostProcTopos}. For a complete list of voice topology
         IDs, refer to @xhyperref{Q7,[Q7]}.

         Topologies added through #ADM_CMD_ADD_TOPOLOGIES_V5 are permitted.

       @if OEM_only
         The SPEAKER_STEREO_IIR topology is the same as the SPEAKER_STEREO
         topology, except in the SPEAKER_STEREO_IIR, the IIR filter module is
         replaced by two independent per-channel IIR tuning filter modules.
         This topology is to be used on the Rx path only.
       @endif

         For compressed use cases (mode_of_operation = 5 and 6), the client
         must set this field to #AUDPROC_TOPOLOGY_ID_COMPRESSED_DEFAULT.

         Tx voice processing topology IDs are not supported in loopback mode
         (mode_of_operation = 4).

         AUDPROC_TOPOLOGY_ID_COMPRESSED_DEFAULT is not supported in any mode
         other than compressed audio input and compressed audio output. */
      /* Q7 = Hex MM ADSP.BF.2.0: Voice Interface Specification */

    uint16_t                  dev_num_channel;
    /**< Number of channels in the data.

         If channel mode nativity is set this value is not used.

         @values
         - For voice processing Tx block (topology_id) -- 1, 2, 4
         - For audio COPP (topology_id) -- 1 to 8
         - For compressed use case (mode_of_operation) -- 2, 8 @tablebulletend */

     uint16_t                  bit_width;
     /**< Bit width of the data.

          If bit width mode nativity is set this value is ignored.

         @values
         - For voice processing Tx block (topology_id) -- 16 bits
         - For audio COPP (topology_id) -- 16 bits or 24 bits
         - For compressed use case (mode_of_operation) -- 16 bits @tablebulletend */

    uint32_t                  sample_rate;
    /**<  Sampling rate in Hertz of the data.

         @values
         - For voice processing Tx block (topology_id) -- 8K, 16K and 48K
         - For audio COPP (topology_id) -- > 0 and @le 192K
         - For compressed use case (mode_of_operation) -- 32K, 44.1K, 48K, 88.2K,
           96K, 176.4K, and 192K @tablebulletend */

    uint8_t                   dev_channel_mapping[8];
    /**< Channel mapping array of buffers that the audio COPP sends to the
         endpoint. Channel[i] mapping describes channel i inside the buffer,
         where 0 @le i < dev_num_channel.

         @values See Section @xref{hdr:PcmChannelDefs}

         This value is relevent only for an audio COPP.

         For compressed use cases (mode_of_operation = 5 and 6), the client has to set this to zero.
         This value will be ignored if device performance mode is
         ADM_ULTRA_LOW_LATENCY_DEVICE_SESSION.
    */

    uint32_t                  cmd_opcode;
    /**< A command opcode to execute during open. The corresponding parameter payload must
         immediately follow the open command structure in memory.

         This parameter is optional.

         @values
         - #ADM_CMD_SET_PP_PARAMS_V5
           Note: only out-of-band set parameters are accepted for ADM_CMD_SET_PP_PARAMS_V5
           during open.*/
};
#include "adsp_end_pack.h"
/** @} */ /* end_weakgroup weak_adm_cmd_device_open_v6_t */

/** @ingroup admsvc_resp_get_device_status
    Returns the status and COPP ID/Device ID to an #ADM_CMD_DEVICE_OPEN_V6 command.

  @apr_hdr_fields
    Opcode -- ADM_CMDRSP_DEVICE_OPEN_V6 \n
    Dst_port -- Ignored

  @apr_msgpayload{adm_cmd_rsp_device_open_v6_t}
    @table{weak__adm__cmd__rsp__device__open__v6__t}

  @return
    None.

  @dependencies
    None.
*/
#define ADM_CMDRSP_DEVICE_OPEN_V6                                     0x00010352

/* ADM command response to a Device open command. */
typedef struct adm_cmd_rsp_device_open_v6_t adm_cmd_rsp_device_open_v6_t;

#include "adsp_begin_pack.h"

/** @weakgroup weak_adm_cmd_rsp_device_open_v6_t
@{ */
/* Payload of the ADM_CMDRSP_DEVICE_OPEN_V6 message, which returns the
    status and COPP ID to an ADM_CMD_DEVICE_OPEN_V6 command.
*/
struct adm_cmd_rsp_device_open_v6_t
{
    uint32_t                  status;
    /**< Status message (error code).

         @values Refer to @xhyperref{Q3,[Q3]} */
    /* Q3 = Async Packet Router API Interface Specification (80-N1463-2) */

    uint16_t                  copp_id;
    /**< Handle for the created COPP.

         @values
         - mode_of_operation = 1 to 4 (LPCM use cases)} -- 0 @le copp_id < 25
         - mode_of_operation = 5 and 6 (compressed use cases)} -- 100
                 @le copp_id @le 113

         If mode_of_operation = 5 and 6, the COPP ID is to be interpreted as
         the Device ID. */

    uint16_t                  reserved;
    /**< This field must be set to zero. */
};
#include "adsp_end_pack.h"
/** @} */ /* end_weakgroup weak_adm_cmd_rsp_device_open_v6_t */

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* _AUDDEVMGR_PRIVATEDEFS_H_ */
