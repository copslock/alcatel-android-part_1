/*========================================================================

 *//** @file AudioDecSvc_Util.cpp
This file contains utility functions for Elite Audio Decoder service.

Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
  *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_Util.cpp#27 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
07/12/10   Wen Jin      Created file.

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"
#include "EliteMsg_Custom.h"
#include "AudioStreamMgr_AprIf.h"
#include "AdspCoreSvc.h"
#include "AudioDecSvc.h"
#include "AudioDecSvc_Util.h"
#include "AudioDecSvc_MiMoUtil.h"
#include "audio_basic_op.h"
#include <audio_basic_op_ext.h>
#include "adsp_asm_api.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "AudioDecSvc_PullMode.h"
#include "AudioDecSvc_CapiV1Util.h"
#include "AudioDecSvc_CapiV2CallbackHandler.h"
#include "AudioDecSvc_CapiV2Util.h"
#include "AFEInterface.h"


extern qurt_elite_globalstate_t qurt_elite_globalstate;

static const uint32_t NUM_US_PER_SEC = 1000000;
/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */


/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
#define LAST_CAPI       TRUE
#define NOT_LAST_CAPI   FALSE

/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */
static ADSPResult AudioDecSvc_CreateDecCAPI(AudioDecSvc_t* pMe, Dec_CAPI_Type capi_type, uint32_t uMediaFmt, uint32_t id2,
      AudioDecSvcInitParams_t *pInitParams, bool_t is_last_capi, dec_init_time_get_params_t *dec_params);
static void AudioDecSvc_DestroyInternalBufs(AudioDecSvc_t* pMe);
static void AudioDecSvc_DestroyScratchBufs(AudioDecSvc_t *pMe);
static void AudioDecSvc_DestroyExternalBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t* pOutStrm);
static void AudioDecSvc_DestroyCircularBufs(AudioDecSvc_t *pMe , AudioDecSvc_OutStream_t* pOutStrm);


static ADSPResult AudioDecSvc_RecreateBuffers(AudioDecSvc_t *pMe,
      dec_capi_port_index_t in_port_index, dec_capi_port_index_t out_port_index,
      uint8_t capi_index);
static void AudioDecSvc_DetermineSvcBw(AudioDecSvc_t* pMe, elite_multi_channel_pcm_fmt_blk_t *pPcmFormatBlock, DecPcmFmt_t *pcmInFmt);
static ADSPResult AudioDecSvc_AggregateKppsRequired(AudioDecSvc_t *pMe, uint32_t *decoder_kpps);
/* =======================================================================
 **                          Function Definitions
 ** ======================================================================= */
static ADSPResult AudioDecSvc_DetermineConverterOutputFormat(uint32_t media_fmt_id, uint32_t dec_conv_mode, uint32_t *output_media_fmt)
{
   *output_media_fmt = 0;

   switch (media_fmt_id)
   {
   case ASM_MEDIA_FMT_EAC3:
   {
      if (dec_conv_mode == ASM_DDP_DD_CONVERTER_MODE)
      {
         *output_media_fmt = ASM_MEDIA_FMT_AC3;
      }
      else
      {
         return ADSP_EUNSUPPORTED;
      }
      break;
   }
   default:
      return ADSP_EUNSUPPORTED;
   }
   return ADSP_EOK;
}

ADSPResult AudioDecSvc_CreateCAPIs(AudioDecSvc_t *pMe, AudioDecSvcInitParams_t *pInitParams,
      dec_init_time_get_params_t *dec_params)
{
   uint32_t media_fmt_id ;
   eDecoderIOFormatConvType io_fmt_conv = DEC_SVC_IO_FORMAT_CONV_TYPE_AUTO;
   ADSPResult result = ADSP_EOK;

   //TODO: clean checks
   if ( pInitParams->io_param.io_type == AUDIO_DEC_SVC_MIMO_INPUT_STREAM )
   {
      if ( (pInitParams->io_param.in.io_fmt_conv != DEC_SVC_IO_FORMAT_CONV_TYPE_AUTO) &&
            (pInitParams->io_param.in.io_fmt_conv != DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_MULTI))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MIMO Capi In cannot support io_fmt_conv = %d",
               pInitParams->io_param.in.io_fmt_conv);
         return ADSP_EFAILED;
      }
      io_fmt_conv = pInitParams->io_param.in.io_fmt_conv;
      media_fmt_id = pInitParams->io_param.mimo_dec_fmt_id;
   }
   else if (pInitParams->io_param.io_type == AUDIO_DEC_SVC_MIMO_OUTPUT_STREAM)
   {
      media_fmt_id = pInitParams->io_param.mimo_dec_fmt_id;
   }
   else
   {
      media_fmt_id = pInitParams->io_param.format_id;
      io_fmt_conv = pInitParams->io_param.in.io_fmt_conv;
   }

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Creating CAPIs. io fmt conv mode %d. format id 0x%lx. converter mode %lu",
         io_fmt_conv, media_fmt_id, pInitParams->io_param.decoderConverterMode);

   switch(io_fmt_conv)
   {
   case DEC_SVC_IO_FORMAT_CONV_TYPE_AUTO:               //decode raw to PCM
   {
      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_AUTO,
            media_fmt_id, 0, pInitParams, LAST_CAPI, dec_params);
      pMe->mainCapiContainer = pMe->capiContainer[0];
      break;
   }
   case DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_61937:     //pass through
   {
      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_PASS_THROUGH,
            media_fmt_id, 0, pInitParams, LAST_CAPI, dec_params);
      pMe->mainCapiContainer = pMe->capiContainer[0];
      break;
   }
   case DEC_SVC_IO_FORMAT_CONV_TYPE_RAW_TO_61937:       //packetize
   {
      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_PACKETIZER,
            media_fmt_id, 0, pInitParams,LAST_CAPI, dec_params);
      pMe->mainCapiContainer = pMe->capiContainer[0];
      break;
   }
   case DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_PCM:         //depacketizer->decodes
   case DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_MULTI:       //depacketize & use MIMO
   {
      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_DEPACKETIZER,
            media_fmt_id, 0, pInitParams, NOT_LAST_CAPI, dec_params); //media fmt is ignored.
      if (ADSP_FAILED(result)) return result;

      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_AUTO,
            media_fmt_id, 0, pInitParams, LAST_CAPI, dec_params);
      pMe->mainCapiContainer = pMe->capiContainer[1];
      break;
   }
   case DEC_SVC_IO_FORMAT_CONV_TYPE_61937_CONV_61937:   //depacketizer->conv->packetize
   {
      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_DEPACKETIZER,
            media_fmt_id, 0, pInitParams, NOT_LAST_CAPI, dec_params); //media_fmt_id ignored
      if (ADSP_FAILED(result)) return result;

      uint32_t output_fmt_id;
      result = AudioDecSvc_DetermineConverterOutputFormat(media_fmt_id,
            pInitParams->io_param.decoderConverterMode, &output_fmt_id); //new fmt id for packetizer
      if (ADSP_FAILED(result)) return result;

      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_CONVERTER,
            media_fmt_id, output_fmt_id, pInitParams, NOT_LAST_CAPI, dec_params);  //media fmt id denotes the input format. DDP (media fmt id)->DP
      if (ADSP_FAILED(result)) return result;


      result = AudioDecSvc_CreateDecCAPI(pMe, DEC_CAPI_TYPE_PACKETIZER,
            output_fmt_id, 0, pInitParams, LAST_CAPI, dec_params);

      pMe->mainCapiContainer = pMe->capiContainer[1];

      break;
   }
   case DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_RAW:       //depacketize
   case DEC_SVC_IO_FORMAT_CONV_TYPE_PCM_TO_61937:       //encode->packetize.
   default:
      result = ADSP_EUNSUPPORTED;
   }

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create the decoder CAPI instances");
      return ADSP_EFAILED;
   }

   return ADSP_EOK;
}

static ADSPResult AudioDecSvc_CreatePcmDec(AudioDecSvc_t* pMe, dec_CAPI_container_t *capi_container,
      AudioDecSvcInitParams_t *pInitParams, bool_t is_last_capi, dec_init_time_get_params_t *dec_params)
{
   ADSPResult result = ADSP_EOK;

   uint32_t unDecOutBufSizeBytes = AUD_DEC_PCM_OUT_BUF_SIZE;

   if ((ASM_LOW_LATENCY_STREAM_SESSION == pInitParams->io_param.perf_mode ) ||
         (ASM_LOW_LATENCY_NO_PROC_STREAM_SESSION == pInitParams->io_param.perf_mode) ||
         (ASM_ULTRA_LOW_LATENCY_STREAM_SESSION == pInitParams->io_param.perf_mode ))
   {
      //Derive size in terms of AFE frame size for 48000, stereo, 16 bit
      //TBD: Remove the hardcoding to 48000 and use the sample rate from media format
      uint32_t unAfeFrameSize;
      elite_svc_get_frame_size(48000,&unAfeFrameSize);
      unDecOutBufSizeBytes = unAfeFrameSize * 2 * 2;

      if( pInitParams->io_param.bits_per_sample > 16 )
      {
         unDecOutBufSizeBytes *= 2;
      }
   }

   if (DEC_SVC_PULL_MODE == pInitParams->io_param.in.ulMode)
   {
      AudioDecSvc_InpStream_t *pInpStream = AudioDecSvc_GetInputStreamById(pMe, pInitParams->io_param.stream_id);
      if (NULL != pInpStream)
      {
    	  result = AudioDecSvc_CopyPcmOr61937MediaFmt(pMe, pInpStream, (void*)&pInitParams->io_param.in.pull_mode.MediaFmt.pcm);
    	  if(ADSP_FAILED(result))
    	  {
    		  return result;
    	  }
    	  uint32_t bytes_per_smpl = pInitParams->io_param.bits_per_sample == 16 ? 2: 4; //output bytes per ch
    	  unDecOutBufSizeBytes = AudioDecSvc_GetPcmOutBufSize(pInitParams->io_param.perf_mode,
    			  pInpStream->pcmFmt.ulPCMSampleRate, bytes_per_smpl, pInpStream->pcmFmt.chan_map.nChannels);
      }

   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Pcm Decoder perf mode is %d, Output buf size is %lu",pInitParams->io_param.perf_mode, unDecOutBufSizeBytes);

   //now create the internal input and output buffers. for last CAPI out buf is set to zero so that only int bufs are created and ext bufs are not created
   //no need of creating input buf for PCM as output buffer from previous module or client buf can be used.
   //output buf is needed & we cannot use inp buf of next module because it may not be fully free.
   if (is_last_capi)
   {
      AudioDecSvc_OutStream_t *pOutStream = AudioDecSvc_GetOutputStreamById(pMe, pInitParams->io_param.stream_id);
      if (NULL != pOutStream)
      {
         result |= AudioDecSvc_CreateExternalBufs(pMe, pOutStream, capi_container, unDecOutBufSizeBytes, DEFAULT_NUM_PCM_DEC_OUTPUT_BUFFERS);
         result |= AudioDecSvc_CheckCreateScratchBufs(pMe, capi_container, unDecOutBufSizeBytes);
      }
   }
   else
   {
      dec_capi_port_index_t out_capi_port_index;
      out_capi_port_index.index = 0; out_capi_port_index.valid = true;
      result |= AudioDecSvc_CreateInternalOutputBufs(pMe, capi_container,unDecOutBufSizeBytes,out_capi_port_index);
      result |= AudioDecSvc_CheckCreateScratchBufs(pMe, capi_container, unDecOutBufSizeBytes);
   }

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create the internal buffers.");
      return result;
   }

   dec_params->max_stack_size = PCM_STACK_SIZE;

   return result;
}

//id2 is valid only for converter case.
static ADSPResult AudioDecSvc_CreateDecCAPI(AudioDecSvc_t* pMe, Dec_CAPI_Type capi_type, uint32_t uMediaFmt, uint32_t id2,
      AudioDecSvcInitParams_t *pInitParams, bool_t is_last_capi, dec_init_time_get_params_t *dec_params)
{
   ADSPResult result = ADSP_EFAILED;
   uint8_t    capi_index;

   dec_CAPI_init_time_get_params_t init_time_get_params;
   memset(&init_time_get_params, 0, sizeof(dec_CAPI_init_time_get_params_t));

   //find a slot in CAPI container array
   for (capi_index = 0 ; capi_index < DEC_SVC_MAX_CAPI; capi_index++)
   {
      if (NULL == pMe->capiContainer[capi_index])       break;
   }
   if(DEC_SVC_MAX_CAPI == capi_index)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "All CAPIs occupied, cannot create any new CAPI!");
      return ADSP_EFAILED;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Creating CAPI %u: type %u",capi_index, capi_type);

   pMe->capiContainer[capi_index] = (dec_CAPI_container_t*) qurt_elite_memory_malloc(sizeof(dec_CAPI_container_t), QURT_ELITE_HEAP_DEFAULT);

   if (NULL == pMe->capiContainer[capi_index])
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "No memory creating container for decoder");
      return ADSP_ENOMEMORY;
   }
   memset(pMe->capiContainer[capi_index], 0, sizeof(dec_CAPI_container_t));

   pMe->capiContainer[capi_index]->dec_fmt_id = uMediaFmt;

   if (AudioDecSvc_IsPcmFmt(uMediaFmt))
   {
      return AudioDecSvc_CreatePcmDec(pMe, pMe->capiContainer[capi_index], pInitParams, is_last_capi, dec_params);
   }


   AudioDecSvc_InpStream_t *pInpStream = AudioDecSvc_GetInputStreamById(pMe, pInitParams->io_param.stream_id);
   AudioDecSvc_OutStream_t *pOutStream = AudioDecSvc_GetOutputStreamById(pMe, pInitParams->io_param.stream_id);

   dec_CAPI_init_params_t capi_init_param;
   capi_init_param.uMediaFmt = uMediaFmt;
   capi_init_param.id2 = id2;
   capi_init_param.cb_info = audio_dec_svc_get_capi_v2_callback_handler(pMe, capi_index);

   //MIMO is always the last CAPI. Other CAPIs are only SISO.
   //Both in & out indices are valid for non-last CAPIs (since non-last CAPIs are always SISO).
   //since this function is called at beginning, both indices can be zero.
   //For last CAPI, for SISO - both indices are zero. For MIMO, either in or out is valid depending on stream's NULLness

   capi_init_param.input_port_index.index = 0; //use 0 since this is function is called only during creation.
   capi_init_param.output_port_index.index = 0; //use 0 since this is function is called only during creation.
   capi_init_param.input_port_index.valid = true;
   capi_init_param.output_port_index.valid = true;
   capi_init_param.dec_destroy_fn = NULL;

   if (is_last_capi)
   {
      capi_init_param.input_port_index.valid = (NULL != pInpStream); // if In stream or IO stream is created, then in stream index is valid.
      capi_init_param.output_port_index.valid = (NULL != pOutStream); // if out stream or IO stream is created, then out stream index is valid.
   }

   //Create the instance
   switch (capi_type)
   {
   case DEC_CAPI_TYPE_AUTO:
   {
      //check if CAPI is present in AMDB.
      dec_AMDB_presence amdb_presence = audio_dec_svc_get_amdb_presence(capi_type, capi_init_param.uMediaFmt, 0, &capi_init_param.amdb_handle);

      switch(amdb_presence)
      {
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V2 found for fmt 0x%lx. Trying to create CAPI V2", uMediaFmt);
         result = audio_dec_svc_create_init_auto_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V1 found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
         result = audio_dec_svc_create_init_auto_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_STUB:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "fmt 0x%lx stubbed in AMDB. Dec svc doesnot support stubs.", uMediaFmt);
         result = ADSP_EFAILED;
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V2 for fmt 0x%lx.", uMediaFmt);
         result = audio_dec_svc_create_init_auto_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);

         //If CAPI V2 is not present, then the uMediaFmt needs to be checked for CAPI V2
         if (capi_init_param.capi_v2_presence == DEC_CAPI_V2_PRESENCE_NOT_PRESENT)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "CAPI V2 not found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
            result = audio_dec_svc_create_init_auto_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         }
      }
      }
      break;
   }
   case DEC_CAPI_TYPE_DEPACKETIZER:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V2 for fmt 0x%lx.", uMediaFmt);
      result = audio_dec_svc_create_init_depack_capi_v2(&pMe->capiContainer[capi_index]->capi_ptr, pInitParams, &capi_init_param);

      //If CAPI V2 is not present, then the uMediaFmt needs to be checked for CAPI V2
      if (capi_init_param.capi_v2_presence == DEC_CAPI_V2_PRESENCE_NOT_PRESENT)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V1 for fmt 0x%lx as CAPI V2 not found.", uMediaFmt);
         result = audio_dec_svc_create_init_depack_capi_v1(&pMe->capiContainer[capi_index]->capi_ptr, pInitParams, &capi_init_param);
      }
      break;
   }
   case DEC_CAPI_TYPE_CONVERTER:
   {
      //check if CAPI is present in AMDB.
      dec_AMDB_presence amdb_presence = audio_dec_svc_get_amdb_presence(capi_type, capi_init_param.uMediaFmt,
            capi_init_param.id2, &capi_init_param.amdb_handle);

      switch(amdb_presence)
      {
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V2 found for fmt 0x%lx. Trying to create CAPI V2", uMediaFmt);
         result = audio_dec_svc_create_init_conv_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V1 found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
         result = audio_dec_svc_create_init_conv_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_STUB:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "fmt 0x%lx stubbed in AMDB. Dec svc doesnot support stubs.", uMediaFmt);
         result = ADSP_EFAILED;
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V2 for fmt 0x%lx.", uMediaFmt);
         result = audio_dec_svc_create_init_conv_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);

         //If CAPI V2 is not present, then the uMediaFmt needs to be checked for CAPI V2
         if (capi_init_param.capi_v2_presence == DEC_CAPI_V2_PRESENCE_NOT_PRESENT)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "CAPI V2 not found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
            result = audio_dec_svc_create_init_conv_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         }
      }
      }
      break;
   }
   case DEC_CAPI_TYPE_PASS_THROUGH:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V2 for fmt 0x%lx.", uMediaFmt);
      result = audio_dec_svc_create_init_passthru_capi_v2(&pMe->capiContainer[capi_index]->capi_ptr, pInitParams, &capi_init_param);

      //If CAPI V2 is not present, then the uMediaFmt needs to be checked for CAPI V2
      if (capi_init_param.capi_v2_presence == DEC_CAPI_V2_PRESENCE_NOT_PRESENT)
      {
         result = audio_dec_svc_create_init_passthru_capi_v1(&pMe->capiContainer[capi_index]->capi_ptr, pInitParams, &capi_init_param);
      }

      break;
   }
   case DEC_CAPI_TYPE_PACKETIZER: //can be converter as well.
   {
      //check if CAPI is present in AMDB.
      dec_AMDB_presence amdb_presence = audio_dec_svc_get_amdb_presence(capi_type, capi_init_param.uMediaFmt, 0, &capi_init_param.amdb_handle);

      switch(amdb_presence)
      {
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V2 found for fmt 0x%lx. Trying to create CAPI V2", uMediaFmt);
         result = audio_dec_svc_create_init_pack_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AMDB CAPI V1 found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
         result = audio_dec_svc_create_init_pack_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         break;
      }
      case DEC_AMDB_PRESENCE_PRESENT_AS_STUB:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "fmt 0x%lx stubbed in AMDB. Dec svc doesnot support stubs.", uMediaFmt);
         result = ADSP_EFAILED;
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Trying to create CAPI V2 for fmt 0x%lx.", uMediaFmt);
         result = audio_dec_svc_create_init_pack_capi_v2(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);

         //If CAPI V2 is not present, then the uMediaFmt needs to be checked for CAPI V2
         if (capi_init_param.capi_v2_presence == DEC_CAPI_V2_PRESENCE_NOT_PRESENT)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "CAPI V2 not found for fmt 0x%lx. Trying to create CAPI V1", uMediaFmt);
            result = audio_dec_svc_create_init_pack_capi_v1(&(pMe->capiContainer[capi_index]->capi_ptr), pInitParams, &capi_init_param);
         }
      }
      }
      break;
   }
   default:
      return ADSP_EBADPARAM;
   }

   if (ADSP_FAILED(result) || (NULL == (pMe->capiContainer[capi_index]->capi_ptr)))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI at %u or no memory. result %d", capi_index, result);
      return result;
   }

   //store the capi destroy function
   pMe->capiContainer[capi_index]->dec_destroy_fn = capi_init_param.dec_destroy_fn;

   init_time_get_params.input_port_index = capi_init_param.input_port_index;
   init_time_get_params.output_port_index = capi_init_param.output_port_index;

   result = audio_dec_svc_get_init_time_properties(pMe->capiContainer[capi_index]->capi_ptr, &init_time_get_params);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc error in getting properties");
      return result;
   }

   //keep the largest stack size needed.
   if (init_time_get_params.stack_size.size_in_bytes > dec_params->max_stack_size)
   {
      dec_params->max_stack_size = init_time_get_params.stack_size.size_in_bytes;
   }

   uint32_t numOutBufs, outSize;
   AudioDecSvc_GetRequiredOutBufSizeNum(pMe, &numOutBufs, &outSize, init_time_get_params.out_port_thresh.threshold_in_bytes,
         init_time_get_params.metadata_size.size_in_bytes);

   // CAPI V2 is assumed to return proper size for 16 and 32 bit outputs.
   // now create the internal input and output buffers. for last CAPI out buf are not required as ext bufs are used.
   // at this point either one input stream or one output stream or both is opened.
   if (is_last_capi)
   {
      if (NULL != pInpStream)
      {
         // if input stream is created then allocate capi input buffer,
         // this input stream will be the first input stream at this point therefore buffer will be allocated to capi at 0th index
         result |= AudioDecSvc_CreateInternalInputBufs(pMe, pMe->capiContainer[capi_index],
               init_time_get_params.in_port_thresh.threshold_in_bytes, init_time_get_params.input_port_index);
      }
      if (NULL != pOutStream)
      {
         // if output stream is created then allocate circular list buffer and queue buffer
         //result |= AudioDecSvc_CreateCirBufs(pMe,pOutStream, outSize, DEC_SVC_MAX_CIRC_BUFFER); TODO: we need circ buf here too
         result |= AudioDecSvc_CreateExternalBufs(pMe, pOutStream, pMe->capiContainer[capi_index],
               outSize, numOutBufs);
         result |= AudioDecSvc_CheckCreateScratchBufs(pMe, pMe->capiContainer[capi_index], outSize);
      }
   }
   else
   {
      // multiple capi case : need to allocate capi input and output buffer
      result |= AudioDecSvc_CreateInternalInputBufs(pMe, pMe->capiContainer[capi_index],
            init_time_get_params.in_port_thresh.threshold_in_bytes, init_time_get_params.input_port_index);
      result |= AudioDecSvc_CreateInternalOutputBufs(pMe, pMe->capiContainer[capi_index],
            outSize, init_time_get_params.output_port_index);
      result |= AudioDecSvc_CheckCreateScratchBufs(pMe, pMe->capiContainer[capi_index], outSize);
   }

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc error in creating buffers");
      return ADSP_EFAILED;
   }
   return result;
}

/**
 * ensures that the buffers allocated for PCM are always afe-frame aligned.
 */
ADSPResult AudioDecSvc_RecreatePcmBufsBasedOnMediaFmt(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   dec_CAPI_container_t * last_capi = AudioDecSvc_GetLastCapi(pMe);  //check if the last decoder is PCM (this func is for changing output buf of last decoder).

   if (NULL == last_capi) return ADSP_EFAILED;

   if (!AudioDecSvc_IsPcmFmt(last_capi->dec_fmt_id)) return ADSP_EOK;

   ADSPResult result = ADSP_EOK;
   dec_capi_port_index_t out_capi_port_index;
   dec_capi_port_index_t in_capi_port_index;

   in_capi_port_index.index = 0;
   in_capi_port_index.valid = FALSE;

   //no need to consider metadata size as PCM doesnt have

   for (uint8_t capi_index = 0; capi_index < DEC_SVC_MAX_CAPI; capi_index++)
   {
      if(NULL == pMe->capiContainer[capi_index])
      {
         break;
      }
      for(uint32_t out_capi_index = 0; out_capi_index < DEC_SVC_MAX_OUTPUT_STREAMS; out_capi_index++)
      {
         out_capi_port_index.index = out_capi_index;
         out_capi_port_index.valid = TRUE;

         //output ch mapping API may cause more output num ch than input. so more space needed.
         //allocate out buf based on output_bytes_per_sample , not input bytes per sample (Eg. input is 16 bit output is 24 bit)
         uint32_t num_ch = pMe->out_streams_ptr[out_capi_index]->out_chan_map.is_out_chan_map_received ? PCM_FORMAT_MAX_NUM_CHANNEL: pInpStrm->pcmFmt.chan_map.nChannels;
         uint32_t unDecOutBufSizeBytes = AudioDecSvc_GetPcmOutBufSize(pInpStrm->common.perf_mode,
               pInpStrm->pcmFmt.ulPCMSampleRate, pMe->out_streams_ptr[out_capi_index]->output_bytes_per_sample, num_ch);

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"AudioDecSvc output buffer size after media format %ld", unDecOutBufSizeBytes);

         pMe->capiContainer[capi_index]->out_port_event_new_size[out_capi_index] = unDecOutBufSizeBytes;
         result |= AudioDecSvc_RecreateBuffers(pMe, in_capi_port_index, out_capi_port_index, capi_index);
         pMe->capiContainer[capi_index]->out_port_event_new_size[out_capi_index] = 0;

      }
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to re-create the internal buffers.");
         return result;
      }
   }
   return result;
}

ADSPResult AudioDecSvc_CopyPcmOr61937MediaFmt(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, void *pFmtBlk )
{
   //typecast to v3 multi-ch pcm fmt blk since it's
   //an extension of v2
   asm_multi_channel_pcm_fmt_blk_v3_t* pPcmFmtBlk =
         (asm_multi_channel_pcm_fmt_blk_v3_t*) ( pFmtBlk );

   MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
         "PCM/61937 Media Format 0x%lx. ch=%u,SR=%lu,bps=%u, word size=%u",pMe->capiContainer[0]->dec_fmt_id,
         pPcmFmtBlk->num_channels,
         pPcmFmtBlk->sample_rate,
         pPcmFmtBlk->bits_per_sample, pPcmFmtBlk->sample_word_size);
   MSG_8(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
         "PCM/61937 Media Format channel mapping %u,%u,%u,%u,%u,%u,%u,%u",pPcmFmtBlk->channel_mapping[0],
         pPcmFmtBlk->channel_mapping[1],pPcmFmtBlk->channel_mapping[2],
         pPcmFmtBlk->channel_mapping[3],pPcmFmtBlk->channel_mapping[4],
         pPcmFmtBlk->channel_mapping[5],pPcmFmtBlk->channel_mapping[6],
         pPcmFmtBlk->channel_mapping[7]);

   if( ( (16 != pPcmFmtBlk->bits_per_sample) && (24 != pPcmFmtBlk->bits_per_sample) ) ||
         (!pPcmFmtBlk->is_signed) ||
         (pPcmFmtBlk->num_channels == 0) || (pPcmFmtBlk->num_channels > PCM_FORMAT_MAX_NUM_CHANNEL))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Received invalid parameters in PCM media format");
      return ADSP_EBADPARAM;
   }

   if ((0 == pPcmFmtBlk->sample_rate) || (PCM_FORMAT_MAX_SAMPLING_RATE_HZ < pPcmFmtBlk->sample_rate))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error: Aud dec rcvd invalid sample_rate [%lu]Hz.",
            pPcmFmtBlk->sample_rate);
      return ADSP_EBADPARAM;
   }

   //store PCM parameters
   pInpStrm->pcmFmt.chan_map.nChannels = pPcmFmtBlk->num_channels;
   pInpStrm->pcmFmt.ulPCMSampleRate = pPcmFmtBlk->sample_rate;
   pInpStrm->pcmFmt.usIsInterleaved = TRUE;
   pInpStrm->pcmFmt.usBitsPerSample = pPcmFmtBlk->bits_per_sample;

   if (ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2 == pMe->capiContainer[0]->dec_fmt_id) //PCM supports only one CAPI
   {
      pInpStrm->pcmFmt.usBytesPerSample = (pInpStrm->pcmFmt.usBitsPerSample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;
   }
   else if (ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3 == pMe->capiContainer[0]->dec_fmt_id)
   {
      uint32_t bytesPerSample = pPcmFmtBlk->sample_word_size/8;
      if ( (bytesPerSample*8 != pPcmFmtBlk->sample_word_size) ||
            (bytesPerSample > 4))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "sample word size %u not byte aligned or is > 32.",
               pPcmFmtBlk->sample_word_size);
         return ADSP_EBADPARAM;
      }
      pInpStrm->pcmFmt.usBytesPerSample = bytesPerSample;
   }
   else //61937
   {
      pInpStrm->pcmFmt.usBytesPerSample = BYTES_PER_SAMPLE_TWO;
   }

   memset(pInpStrm->pcmFmt.chan_map.nChannelMap, 0, PCM_FORMAT_MAX_NUM_CHANNEL);

   memscpy(pInpStrm->pcmFmt.chan_map.nChannelMap, sizeof(pInpStrm->pcmFmt.chan_map.nChannelMap),
         pPcmFmtBlk->channel_mapping, pPcmFmtBlk->num_channels);


   return ADSP_EOK;
}

ADSPResult AudioDecSvc_UpdateMediaFmt_PeerService(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   ADSPResult result;

   //get media format update cmd payload
   //get the mediaFormat structure
   elite_msg_data_media_type_apr_t *pMediaFmt =
         (elite_msg_data_media_type_apr_t*) (pInpStrm->inpDataQMsg.pPayload);

   /* sanity checks */
   if ( ELITEMSG_DATA_MEDIA_TYPE_APR != pMediaFmt->unMediaTypeFormat )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Unsupported MEDIA FORMAT 0x%8lx",(pMediaFmt->unMediaTypeFormat));
      return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EUNSUPPORTED);
   }

   switch(pMediaFmt->unMediaFormatID)
   {
   case ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM :
   {
      asm_multi_channel_pcm_fmt_blk_v3_t pcmBlk;

      elite_multi_channel_pcm_fmt_blk_t *pInPcmFmtBlk =
            (elite_multi_channel_pcm_fmt_blk_t*) elite_msg_get_media_fmt_blk(pMediaFmt);

      pcmBlk.bits_per_sample = pInPcmFmtBlk->bits_per_sample;
      pcmBlk.is_signed = pInPcmFmtBlk->is_signed;
      pcmBlk.num_channels = pInPcmFmtBlk->num_channels;
      pcmBlk.sample_rate = pInPcmFmtBlk->sample_rate;
      pcmBlk.sample_word_size = (pcmBlk.bits_per_sample == 16) ? 16 : 32; //internally it's always 16 or 32.

      memscpy(pcmBlk.channel_mapping, sizeof(pcmBlk.channel_mapping), pInPcmFmtBlk->channel_mapping, sizeof(pcmBlk.channel_mapping));

      result = AudioDecSvc_CopyPcmOr61937MediaFmt(pMe, pInpStrm, (void*)&pcmBlk);
      if (ADSP_SUCCEEDED(result))
      {
         result = AudioDecSvc_RecreatePcmBufsBasedOnMediaFmt(pMe, pInpStrm);
      }

      break;
   }
   case ELITEMSG_MEDIA_FMT_COMPRESSED:
   {
      //use this struct intermediately
      asm_multi_channel_pcm_fmt_blk_v3_t pcmBlk;

      elite_compressed_fmt_blk_t *pComprFmtBlk = (elite_compressed_fmt_blk_t*) elite_msg_get_media_fmt_blk(pMediaFmt);
      pcmBlk.bits_per_sample = pComprFmtBlk->bits_per_sample;
      pcmBlk.is_signed = TRUE; //to be ignored.
      pcmBlk.num_channels = pComprFmtBlk->num_channels;
      pcmBlk.sample_rate = pComprFmtBlk->sample_rate;
      pcmBlk.sample_word_size = (pcmBlk.bits_per_sample == 16) ? 16 : 32; //internally it's always 16 or 32.
      //pComprFmtBlk->media_format; //decoder acts as decoder OR depacketizer -> decoder OR depacketizer -> converter -> packetizer. This media fmt is not required.

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
            "Compressed media fmt message: fmt = %lx",(pComprFmtBlk->media_format));

      result = AudioDecSvc_CopyPcmOr61937MediaFmt(pMe, pInpStrm, (void*)&pcmBlk);
      if (ADSP_SUCCEEDED(result))
      {
         result = AudioDecSvc_RecreatePcmBufsBasedOnMediaFmt(pMe, pInpStrm);
      }

      break;
   }
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Unsupported MEDIA FORMAT ID 0x%8lx",(pMediaFmt->unMediaFormatID));
      result = ADSP_EUNSUPPORTED;
   }
   }

   return AudioDecSvc_FreeInputDataCmd(pMe,pInpStrm, result);
}

ADSPResult AudioDecSvc_UpdateMediaFmt(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, uint8_t *pFmtBlk, uint32_t ulFmtBlkSize)
{
   ADSPResult result = ADSP_EOK;

   uint16_t input_port_index = AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm);

   dec_CAPI_container_t * first_capi = AudioDecSvc_GetFirstCapi(pMe);  //since only the first decoder receives the media fmt

   switch( first_capi->dec_fmt_id )
   {
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   {
      if (ADSP_FAILED(result = AudioDecSvc_CopyPcmOr61937MediaFmt(pMe, pInpStrm, (void*)pFmtBlk)))
      {
         return result;
      }
      result = AudioDecSvc_RecreatePcmBufsBasedOnMediaFmt(pMe, pInpStrm);
      break;
   }
   default:
   {
      result = aud_dec_svc_set_in_media_fmt_raw_compr(first_capi->capi_ptr, input_port_index,
            pFmtBlk, ulFmtBlkSize, first_capi->dec_fmt_id,
            (int8_t*)first_capi->scratch_out_buf,
            first_capi->scratch_buf_size);
   }
   }

   //TODO: to remove special condition
   if (first_capi->dec_fmt_id == ASM_MEDIA_FMT_ADPCM)
   {
      pInpStrm->pcmFmt.usIsInterleaved = 1;
   }

   //Note: for AAC port thresh might change (dolby vs. ETSI aac case)
   if (ADSP_SUCCEEDED(result))
   {
      result = AudioDecSvc_HandlePortDataThreshChangeEvent(pMe);
   }

   return result;
}

void AudioDecSvc_InitOutDataBuf(elite_msg_data_buffer_t *pBuf, AudioDecSvc_OutStream_t *pOutStrm)
{
   if(!pBuf)
   {
      return;
   }

   pBuf->nFlag                = 0;
   pBuf->ullTimeStamp         = 0;
   pBuf->nOffset              = 0;
   //don't change max size.
   pBuf->pResponseQ           = NULL;
   pBuf->unClientToken        = 0;
   pBuf->unResponseResult     = 0;
   pBuf->nMaxSize             = pOutStrm->maxExtBufSize;
}

ADSPResult AudioDecSvc_CheckCreateScratchBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container, uint32_t outBufSize)
{
   if (outBufSize)
   {
      //create a scratch buffer of the same size which is used in output mapping
      // memory allocated to scratch buffer should be maximum
      //TODO: note that even if capi v2 raises event to decrease, scratch buf size cannot be decreased due to this logic.
      //       this logic is necessary because scratch buf is common to few all IO of a capi.
      if(capi_container->scratch_buf_size < outBufSize)
      {
         if(capi_container->scratch_out_buf)
         {
            qurt_elite_memory_free(capi_container->scratch_out_buf);
            capi_container->scratch_out_buf = NULL;
            capi_container->scratch_buf_size = 0;
         }
      }

      if(NULL == capi_container->scratch_out_buf)
      {
         capi_container->scratch_out_buf = qurt_elite_memory_malloc(outBufSize, QURT_ELITE_HEAP_DEFAULT);
         if(NULL == capi_container->scratch_out_buf)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Insufficient memory to create scratch out-buf for AudioDecSvc.It requires %lu bytes", outBufSize);
            return ADSP_ENOMEMORY;
         }
         capi_container->scratch_buf_size = outBufSize;
      }
   }

   return ADSP_EOK;
}

ADSPResult AudioDecSvc_CreateInternalInputBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container,
      uint32_t inBufSize, dec_capi_port_index_t in_capi_port_index)
{
   if (inBufSize && in_capi_port_index.valid)
   {
      // create internal input bitstream buffer
      uint8_t *int_buf = (uint8_t *) qurt_elite_memory_malloc(inBufSize, ADEC_SVC_INTERNAL_BIT_STREAM_BUF);
      if ( NULL==int_buf)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Insufficient memory to create internal input buffer for AudioDecSvc.It requires %lu bytes", inBufSize);
         return ADSP_ENOMEMORY;
      }

#ifdef DBG_BUFFER_ADDRESSES
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "BUFFERADDR Dec internal bitstream buffer: 0x%8x, size %d",
            int_buf, inBufSize);
#endif /* DBG_BUFFER_ADDRESSES */

      capi_container->in_buf[in_capi_port_index.index].actual_data_len = 0;
      //Set DataLenReq fields here, as they are constant
      capi_container->in_buf[in_capi_port_index.index].max_data_len = inBufSize;
      //Set the data fields for input node of CAPI
      capi_container->in_buf[in_capi_port_index.index].data_ptr    = (int8_t *) (int_buf);

      capi_container->inputs[in_capi_port_index.index].buf_ptr = &capi_container->in_buf[in_capi_port_index.index];
      capi_container->inputs[in_capi_port_index.index].bufs_num = 1;
      capi_container->inputs[in_capi_port_index.index].flags.end_of_frame = false;
      capi_container->inputs[in_capi_port_index.index].flags.is_timestamp_valid = false;
      capi_container->inputs[in_capi_port_index.index].flags.marker_eos = false;
      capi_container->inputs[in_capi_port_index.index].flags.marker_1 = 0;
      capi_container->inputs[in_capi_port_index.index].flags.marker_2 = 0;
      capi_container->inputs[in_capi_port_index.index].flags.marker_3 = 0;
      capi_container->inputs[in_capi_port_index.index].flags.reserved = 0;
      capi_container->inputs[in_capi_port_index.index].timestamp = 0;

      capi_container->inputs_ptr[in_capi_port_index.index] = &capi_container->inputs[in_capi_port_index.index];

      //max in size is not stored elsewhere as capi_container->in_buf[in_capi_port_index.index].max_data_len is not changed.

   }

   return ADSP_EOK;
}

ADSPResult AudioDecSvc_CreateInternalOutputBufs(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container,
      uint32_t outBufSize, dec_capi_port_index_t out_capi_port_index)
{
   if (outBufSize && out_capi_port_index.valid)
   {
      uint32_t reqSize = GET_ELITEMSG_DATABUF_REQ_SIZE(outBufSize);

      //allocate the databuffer payload (metadata + pcm buffer size)
      void *psPcmBuf =  qurt_elite_memory_malloc(reqSize, ADEC_SVC_OUT_BUF);

      if (!psPcmBuf)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Insufficient memory to create internal out-buf for AudDecSvc.It requires %lu bytes", reqSize);
         return ADSP_ENOMEMORY;
      }

#ifdef DBG_BUFFER_ADDRESSES
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "BUFFERADDR decoder output buffer: 0x%8x, size %d", psPcmBuf, reqSize);
#endif /* DBG_BUFFER_ADDRESSES */

      memset(psPcmBuf,0,reqSize);

      elite_msg_data_buffer_t *pcmBuf = (elite_msg_data_buffer_t*) (psPcmBuf);
      pcmBuf->nMaxSize = outBufSize;

      capi_container->out_buf[out_capi_port_index.index].data_ptr = (int8_t*)pcmBuf;
      capi_container->out_buf[out_capi_port_index.index].actual_data_len = 0;
      capi_container->out_buf[out_capi_port_index.index].max_data_len = outBufSize;

      capi_container->outputs[out_capi_port_index.index].buf_ptr = &capi_container->out_buf[out_capi_port_index.index];
      capi_container->outputs[out_capi_port_index.index].bufs_num = 1;
      capi_container->outputs[out_capi_port_index.index].flags.end_of_frame = 0;
      capi_container->outputs[out_capi_port_index.index].flags.is_timestamp_valid = false;
      capi_container->outputs[out_capi_port_index.index].flags.marker_eos = 0;
      capi_container->outputs[out_capi_port_index.index].flags.marker_1 = 0;
      capi_container->outputs[out_capi_port_index.index].flags.marker_2 = 0;
      capi_container->outputs[out_capi_port_index.index].flags.marker_3 = 0;
      capi_container->outputs[out_capi_port_index.index].flags.reserved = 0;
      capi_container->outputs[out_capi_port_index.index].timestamp = 0;

      capi_container->outputs_ptr[out_capi_port_index.index] = &capi_container->outputs[out_capi_port_index.index];

      //max out size is not stored elsewhere as capi_container->out_buf[out_capi_port_index.index].max_data_len is not changed.
   }

   return ADSP_EOK;
}

// Generate buffers for circular buf list
ADSPResult AudioDecSvc_CreateCirBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t* pOutStrm, uint32_t BufSize, uint32_t numBufs)
{
   ADSPResult result = ADSP_EOK;
   Circular_Buffer_List* pCirBufList = &(pOutStrm->CirBufList);
   uint32_t reqSize = GET_ELITEMSG_DATABUF_REQ_SIZE(BufSize);
   uint32_t i = 0;

   pCirBufList->numBufs = 0;
   pCirBufList->readI = 0;
   pCirBufList->writeI = 0;

   // Allocate Buffers and assign it to circular buffer list
   for (i = 0; i < numBufs; i++)
   {
      //allocate the buffer
      void *psPcmBuf =  qurt_elite_memory_malloc(reqSize, ADEC_SVC_OUT_BUF);

      if (!psPcmBuf)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Insufficient memory to create buffer required for circ-buf list for AudDecSvc.It requires %lu bytes", reqSize);
         result = ADSP_ENOMEMORY;
         break;
      }

      memset(psPcmBuf,0,reqSize);

      pCirBufList->buflist[i] = (elite_msg_data_buffer_t*) (psPcmBuf);
      pCirBufList->buflist[i]->nMaxSize = BufSize;
      pCirBufList->numBufs++;
   }

   if(ADSP_FAILED(result))
   {
      AudioDecSvc_DestroyCircularBufs(pMe, pOutStrm);
   }
   return result;
}

/**
 * for last capi both scratch buf and out buf are created here.
 */
ADSPResult AudioDecSvc_CreateExternalBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStream,
      dec_CAPI_container_t *capi_container, uint32_t outBufSize, uint32_t numOutBufs)
{
   if ( numOutBufs > MAX_OUT_DATA_Q_ELEMENTS)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"not enough dataQ elements");
      return ADSP_ENORESOURCE;
   }
   uint32_t reqSize = GET_ELITEMSG_DATABUF_REQ_SIZE(outBufSize);

   // Allocate and queue up the output buffers.
   while ((uint32_t)pOutStream->nBufsAllocated < numOutBufs)
   {
      //allocate the databuffer payload (metadata + pcm buffer size)
      void *psPcmBuf =  qurt_elite_memory_malloc(reqSize, ADEC_SVC_OUT_BUF);

      if (!psPcmBuf)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Insufficient memory to create external out-buf for AudDecSvc.It requires %lu bytes", reqSize);
         return ADSP_ENOMEMORY;
      }

#ifdef DBG_BUFFER_ADDRESSES
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "BUFFERADDR decoder output buffer: 0x%8x, size %d", psPcmBuf, reqSize);
#endif /* DBG_BUFFER_ADDRESSES */

      memset(psPcmBuf,0,reqSize);

      elite_msg_data_buffer_t *pcmBuf = (elite_msg_data_buffer_t*) (psPcmBuf);
      pcmBuf->nMaxSize = outBufSize;

      ADSPResult result;
      if (ADSP_FAILED(result = elite_msg_push_payload_to_returnq(pOutStream->pOutBufQ, (elite_msg_any_payload_t*) psPcmBuf)))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to fill Decoder Svc buffer queue!! \n");
         //free this PCM buffer here because main destroy function doesn't have the ptr, since push to returnq failed.
         qurt_elite_memory_free(psPcmBuf);
         return result;
      }
      pOutStream->nBufsAllocated++;
   }
   uint8_t index = AudioDecSvc_GetOutputStreamIndex(pMe,pOutStream);

   capi_container->out_buf[index].data_ptr = NULL; //will be popped off the queue
   capi_container->out_buf[index].actual_data_len = 0;
   capi_container->out_buf[index].max_data_len = outBufSize;

   capi_container->outputs[index].buf_ptr = &capi_container->out_buf[index];
   capi_container->outputs[index].bufs_num = 1;
   capi_container->outputs[index].flags.end_of_frame = 0;
   capi_container->outputs[index].flags.is_timestamp_valid = false;
   capi_container->outputs[index].flags.marker_eos = 0;
   capi_container->outputs[index].flags.marker_1 = 0;
   capi_container->outputs[index].flags.marker_2 = 0;
   capi_container->outputs[index].flags.marker_3 = 0;
   capi_container->outputs[index].flags.reserved = 0;
   capi_container->outputs[index].timestamp = 0;

   capi_container->outputs_ptr[index] = &capi_container->outputs[index];

   //max out size is stored because, it's changed, esp  in metadata handling etc
   pOutStream->maxExtBufSize = outBufSize;

   return ADSP_EOK;
}

ADSPResult AudioDecSvc_HandlePortDataThreshChangeEvent(AudioDecSvc_t *pMe)
{
   ADSPResult result = ADSP_EOK;

   if (!(pMe->event_mask|AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK))
   {
      return ADSP_EOK;
   }

   dec_capi_port_index_t in_capi_port_index;
   dec_capi_port_index_t out_capi_port_index;
   in_capi_port_index.index = 0; in_capi_port_index.valid = false;
   out_capi_port_index.index = 0; out_capi_port_index.valid = false;

   for (uint8_t capi_index=0; capi_index<DEC_SVC_MAX_CAPI; capi_index++)
   {
      if(NULL == pMe->capiContainer[capi_index]) break;

      out_capi_port_index.index = 0; out_capi_port_index.valid = false;

      //for input streams
      for (uint8_t in_stream_index=0; in_stream_index<DEC_SVC_MAX_INPUT_STREAMS; in_stream_index++)
      {
         //assume that event would be raised only if port is created(which is generally true)
         if (pMe->capiContainer[capi_index]->in_port_event_new_size[in_stream_index])
         {
            in_capi_port_index.index = in_stream_index; in_capi_port_index.valid = true;

            result |= AudioDecSvc_RecreateBuffers(pMe, in_capi_port_index, out_capi_port_index, capi_index);

            //clear the size
            pMe->capiContainer[capi_index]->in_port_event_new_size[in_stream_index] = 0;
         }
      }

      in_capi_port_index.index = 0; in_capi_port_index.valid = false;

      //for output streams
      for (uint8_t out_stream_index=0; out_stream_index<DEC_SVC_MAX_OUTPUT_STREAMS; out_stream_index++)
      {
         if (pMe->capiContainer[capi_index]->out_port_event_new_size[out_stream_index])
         {
            out_capi_port_index.index = out_stream_index; out_capi_port_index.valid = true;

            result |= AudioDecSvc_RecreateBuffers(pMe, in_capi_port_index, out_capi_port_index, capi_index);

            //clear the size
            pMe->capiContainer[capi_index]->out_port_event_new_size[out_stream_index] = 0;
         }
      }
   }

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"failed to recreate buffers");
      result = ADSP_EFAILED;
   }

   //clear all events (even if error)
   pMe->event_mask &= (~AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK);

   return result;
}

static ADSPResult AudioDecSvc_RecreateBuffers(AudioDecSvc_t *pMe,
      dec_capi_port_index_t in_port_index, dec_capi_port_index_t out_port_index,
      uint8_t capi_index)
{
   ADSPResult result = ADSP_EOK;

   uint32_t in_size = 0, out_size = 0, num_out_bufs = 0;

   if (in_port_index.valid)
   {
      in_size = pMe->capiContainer[capi_index]->in_port_event_new_size[in_port_index.index];

      //delete the internal buffers and allocate new ones
      //delete only input buf size based buffers.
      if( pMe->capiContainer[capi_index]->in_buf[in_port_index.index].data_ptr != NULL)
      {
         qurt_elite_memory_free( pMe->capiContainer[capi_index]->in_buf[in_port_index.index].data_ptr);
         pMe->capiContainer[capi_index]->in_buf[in_port_index.index].data_ptr = NULL;

         pMe->capiContainer[capi_index]->inputs[in_port_index.index].buf_ptr = NULL;
         pMe->capiContainer[capi_index]->inputs_ptr[in_port_index.index] = NULL;
      }

      //TODO: to fix allocation based on sum of prev+current CAPIs
      //if last CAPI create ext output buffers as well.
      result = AudioDecSvc_CreateInternalInputBufs(pMe, pMe->capiContainer[capi_index], in_size, in_port_index);
   }

   if (out_port_index.valid)
   {
      uint32_t max_metadata_size = 0;
      //In case PCM this pointer is NULL. So do not get metadata from CAPI
      if (pMe->capiContainer[capi_index]->capi_ptr)
      {
         result = aud_dec_svc_get_max_metadata_size(pMe->capiContainer[capi_index]->capi_ptr,
               &max_metadata_size);
      }

      if (ADSP_FAILED(result)) return result;

      uint32_t index = out_port_index.index;
      out_size = pMe->capiContainer[capi_index]->out_port_event_new_size[index];

      AudioDecSvc_GetRequiredOutBufSizeNum(pMe, &num_out_bufs, &out_size,
            pMe->capiContainer[capi_index]->out_port_event_new_size[index],
            max_metadata_size);

      //delete the internal buffers and allocate new ones
      //delete only output buf size based buffers.

      if (AudioDecSvc_IsLastCapiIndex(pMe, capi_index) )
      {
         AudioDecSvc_DestroyExternalBufs(pMe, pMe->out_streams_ptr[out_port_index.index]);

         pMe->capiContainer[capi_index]->out_buf[out_port_index.index].data_ptr = NULL;
         pMe->capiContainer[capi_index]->outputs[out_port_index.index].buf_ptr = NULL;
         pMe->capiContainer[capi_index]->outputs_ptr[out_port_index.index] = NULL;

         //TODO: to fix allocation based on sum of prev+current CAPIs
         //if last CAPI create ext output buffers as well.
         result = AudioDecSvc_CreateExternalBufs(pMe, pMe->out_streams_ptr[out_port_index.index],
               pMe->capiContainer[capi_index], out_size, num_out_bufs);
         result |= AudioDecSvc_CheckCreateScratchBufs(pMe, pMe->capiContainer[capi_index], out_size);
      }
      else
      {
         //for non-last CAPIs free the internal buffer
         if (NULL != pMe->capiContainer[capi_index]->out_buf[out_port_index.index].data_ptr)
         {
            qurt_elite_memory_free( pMe->capiContainer[capi_index]->out_buf[out_port_index.index].data_ptr);
            pMe->capiContainer[capi_index]->out_buf[out_port_index.index].data_ptr = NULL;

            pMe->capiContainer[capi_index]->outputs[out_port_index.index].buf_ptr = NULL;
            pMe->capiContainer[capi_index]->outputs_ptr[out_port_index.index] = NULL;
         }

         //scratch buf is automatically recreated in createExt/int buffers.

         /* Will create the o/p buffer wrt 4 bps if above conditional is true */
         result = AudioDecSvc_CreateInternalOutputBufs(pMe, pMe->capiContainer[capi_index], out_size, out_port_index);
         result |= AudioDecSvc_CheckCreateScratchBufs(pMe, pMe->capiContainer[capi_index], out_size);
      }
   }
   return result;
}

void AudioDecSvc_DestroyBuffers(AudioDecSvc_t *pMe)
{
   AudioDecSvc_DestroyInternalBufs(pMe); //destroy both in and out size based bufs

   AudioDecSvc_DestroyScratchBufs(pMe);

   for (uint16_t i=0; i<DEC_SVC_MAX_OUTPUT_STREAMS; i++)
   {
      if (pMe->out_streams_ptr[i])
      {
    	  while (0 < pMe->out_streams_ptr[i]->nBufsAllocated) //Destroy all the allocated buffers.
    	  {
    		  (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(pMe->out_streams_ptr[i]->pOutBufQ),
    		      			  qurt_elite_queue_get_channel_bit(pMe->out_streams_ptr[i]->pOutBufQ));

    		  AudioDecSvc_DestroyExternalBufs(pMe, pMe->out_streams_ptr[i]);
    	  }

         AudioDecSvc_DestroyCircularBufs(pMe, pMe->out_streams_ptr[i]);
      }
   }
}

static void AudioDecSvc_DestroyScratchBufs(AudioDecSvc_t *pMe)
{
   for (uint8_t i=0; i < DEC_SVC_MAX_CAPI; i++)
   {
      if (pMe->capiContainer[i] )
      {
         //destroy scratch buffer
         if(pMe->capiContainer[i]->scratch_out_buf != NULL)
         {
            qurt_elite_memory_free(pMe->capiContainer[i]->scratch_out_buf);
            pMe->capiContainer[i]->scratch_out_buf = NULL;
         }
      }
   }
}

/**
 * for last CAPI, the scratch buf is destroyed here itself
 */
static void AudioDecSvc_DestroyInternalBufs(AudioDecSvc_t *pMe)
{
   uint8_t capi_index = 0;
   for (uint8_t i=0; i < DEC_SVC_MAX_CAPI; i++)
   {
      if (pMe->capiContainer[i] )
      {
         for (uint16_t j = 0; j < DEC_SVC_MAX_INPUT_STREAMS; j++)
         {
            //destroy the input buffer
            if( pMe->capiContainer[i]->in_buf[j].data_ptr != NULL)
            {
               qurt_elite_memory_free( pMe->capiContainer[i]->in_buf[j].data_ptr);
               pMe->capiContainer[i]->in_buf[j].data_ptr = NULL;

               pMe->capiContainer[i]->inputs[j].buf_ptr = NULL;
               pMe->capiContainer[i]->inputs_ptr[j] = NULL;
            }
         }
         capi_index++;
      }
   }

   //except for last CAPI destroy out bufs, for last CAPI out bufs are created as dec svc out bufs.
   for (uint8_t i=0; i < capi_index-1; i++)
   {
      //destroy output buffers
      if ( pMe->capiContainer[i])
      {
         for (uint16_t j = 0; j < DEC_SVC_MAX_OUTPUT_STREAMS; j++)
         {
            if( pMe->capiContainer[i]->out_buf[j].data_ptr != NULL)
            {
               qurt_elite_memory_free( pMe->capiContainer[i]->out_buf[j].data_ptr);
               pMe->capiContainer[i]->out_buf[j].data_ptr = NULL;

               pMe->capiContainer[i]->outputs[j].buf_ptr = NULL;
               pMe->capiContainer[i]->outputs_ptr[j] = NULL;
            }
         }
      }
   }

   //for the last CAPI, assign the ptrs as NULL
   for (uint16_t j = 0; j < DEC_SVC_MAX_OUTPUT_STREAMS; j++)
   {
      if (pMe->capiContainer[capi_index])
      {
         pMe->capiContainer[capi_index]->out_buf[j].data_ptr = NULL;
         pMe->capiContainer[capi_index]->outputs[j].buf_ptr = NULL;
         pMe->capiContainer[capi_index]->outputs_ptr[j] = NULL;
      }
   }
}

bool_t AudioDecSvc_IsReallocateExternalBuffer(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm, ADSPResult *result)
{
	elite_msg_data_buffer_t *pBuffer = (elite_msg_data_buffer_t*)pOutStrm->outDataBufferNode.pBuffer;
	dec_CAPI_container_t *lastCapi = AudioDecSvc_GetLastCapi(pMe);

	if( (pBuffer != NULL) && ((uint32_t)pBuffer->nMaxSize != pOutStrm->maxExtBufSize) )
	{
        // Free the buffer
        qurt_elite_memory_free(pBuffer);
        pOutStrm->nBufsAllocated--;

        pOutStrm->outDataBufferNode.pBuffer = NULL;

        if( NULL != lastCapi)
        {
			//nBufsAllocated buffers are already allocated, need to allocate one more.
		   *result |= AudioDecSvc_CreateExternalBufs(pMe, pOutStrm,
				   lastCapi, pOutStrm->maxExtBufSize, (uint32_t)pOutStrm->nBufsAllocated+1);
        }
        else
        {
        	*result |= ADSP_EFAILED;
        }

        return TRUE;
	}
	return FALSE;
}

static void AudioDecSvc_DestroyExternalBufs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t* pOutStrm)
{
   ADSPResult result;
   qurt_elite_bufmgr_node_t bufNode;

   if (pOutStrm->outDataBufferNode.pBuffer)
   {
      (void) elite_msg_push_payload_to_returnq(pOutStrm->pOutBufQ, (elite_msg_any_payload_t*)(pOutStrm->outDataBufferNode.pBuffer));
      pOutStrm->outDataBufferNode.pBuffer = NULL;
   }

   // Drain the buffers
   while (qurt_elite_channel_poll(qurt_elite_queue_get_channel(pOutStrm->pOutBufQ),
           qurt_elite_queue_get_channel_bit(pOutStrm->pOutBufQ)))
   {
      // retrieve the buffer
      result = qurt_elite_queue_pop_front(pOutStrm->pOutBufQ, (uint64_t*)&bufNode );

      if (ADSP_EOK == result){
         // Free the buffer
         qurt_elite_memory_free(bufNode.pBuffer);
         pOutStrm->nBufsAllocated--;
      }
      // shouldn't reach this point.
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error %d in destroying buffers in Decoder svc!!\n", result);
      }
   }
}

static void AudioDecSvc_DestroyCircularBufs(AudioDecSvc_t *pMe , AudioDecSvc_OutStream_t* pOutStrm)
{
   while((pOutStrm->CirBufList.numBufs > 0) && (pOutStrm->CirBufList.numBufs < DEC_SVC_MAX_CIRC_BUFFER))
   {
      qurt_elite_memory_free(pOutStrm->CirBufList.buflist[pOutStrm->CirBufList.numBufs - 1]);
      pOutStrm->CirBufList.numBufs--;
   }
}

ADSPResult AudioDecSvc_GetInputDataCmd(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   ADSPResult result;
   uint32_t uASMOpCode, uAprOpCode;
   //check if we already hold on to an input data cmd..
   //if so return error
   if(pInpStrm->inpDataQMsg.pPayload)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Already holding on to an input data command. \
                                          Cannot get another command until this is freed");
      return ADSP_EUNEXPECTED;
   }

   result = qurt_elite_queue_pop_front(pInpStrm->common.serviceHandle.dataQ, (uint64_t*)&(pInpStrm->inpDataQMsg) );
   if(ADSP_EOK != result)
   {
      pInpStrm->inpDataQMsg.pPayload = NULL;
      return result;
   }

   uASMOpCode = pInpStrm->inpDataQMsg.unOpCode;
   //If msg is not APR packet, return
   if (ELITE_APR_PACKET == uASMOpCode)
   {
      elite_apr_packet_t* pAprPacket = (elite_apr_packet_t*) (pInpStrm->inpDataQMsg.pPayload);
      uAprOpCode = elite_apr_if_get_opcode( pAprPacket ) ;
      if (ASM_DATA_CMD_WRITE_V2 == uAprOpCode)
      {
         pInpStrm->buf_recv_cnt++;
      }
   }
   else //assume opcode is one of the elite data messages
   {
      if (ELITE_DATA_BUFFER == pInpStrm->inpDataQMsg.unOpCode)
      {
         pInpStrm->buf_recv_cnt++;
      }
   }

   return result;
}

ADSPResult AudioDecSvc_FreeInputDataCmd(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, ADSPResult status)
{
   if(!pInpStrm->inpDataQMsg.pPayload)
   {
      return ADSP_EOK;
   }
   ADSPResult res = ADSP_EOK;
   if(ELITE_APR_PACKET == pInpStrm->inpDataQMsg.unOpCode)
   {
      elite_apr_packet_t *pAprPacket = ( elite_apr_packet_t *) (pInpStrm->inpDataQMsg.pPayload);

      if (ASM_DATA_CMD_WRITE_V2 == pAprPacket->opcode) {
         pInpStrm->buf_done_cnt++;
         //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "buf_done_cnt is %d",pMe->buf_done_cnt);
      }

      if(ASM_DATA_CMD_EOS == pAprPacket->opcode ||
            ASM_DATA_CMD_MARK_BUFFER_V2 == pAprPacket->opcode)
      {


         //this means that we have not processed the EOS command.
         //cant send CMDRSP_EOS since this is not successfully handled
         //so just discard the EOS cmd
         res = AudioStreamMgr_FreeAprPkt(pAprPacket);
      }
      else
      {
         if (0 != pInpStrm->inp_buf_params_backup.unVirtAddr)
         {
            //Invalidating the cache before sending the ACK
            elite_mem_invalidate_cache( &(pInpStrm->inp_buf_params_backup) );

            elite_mem_map_release_shm(&(pInpStrm->inp_buf_params_backup));

            memset(&(pInpStrm->inp_buf_params_backup), 0, sizeof(elite_mem_shared_memory_map_t));
         }

         res = AudioStreamMgr_GenerateAck(pAprPacket, status, NULL, 0,0 );

         //reset input buffer params
         pInpStrm->inp_buf_params.unMemSize = 0;
         pInpStrm->inp_buf_params.unPhysAddrLsw = 0;
         pInpStrm->inp_buf_params.unPhysAddrMsw = 0;
         pInpStrm->inp_buf_params.unVirtAddr = 0;
      }

   }
   else
   {
      if (ELITE_DATA_BUFFER == pInpStrm->inpDataQMsg.unOpCode)
      {
         pInpStrm->buf_done_cnt++;
      }

      res = elite_msg_finish_msg(&pInpStrm->inpDataQMsg, status);
   }

   //set payload to NULL to indicate we are not holding on to any input data msg
   pInpStrm->inpDataQMsg.pPayload = NULL;

   return res;
}

ADSPResult AudioDecSvc_ResetDecoder(AudioDecSvc_t *pMe)
{
   pMe->need_to_send_eos = FALSE;

   //TODO: introduce stream_id in flush cmd sent from ASM.
   AudioDecSvc_InpStream_t *pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe);
   //increment log id so that after every reset(flush,EoS), fresh log file is created.
   pInpStrm->common.ulDataLogId++;

   //reset internal buffer params
   for (uint8_t i=0; i<DEC_SVC_MAX_CAPI; i++)
   {
      if (pMe->capiContainer[i])
      {
         for (uint8_t j=0; j<DEC_SVC_MAX_INPUT_STREAMS;j++)
         {
            if (pMe->capiContainer[i]->in_buf[j].data_ptr)
            {
               pMe->capiContainer[i]->in_buf[j].actual_data_len = 0;
               pMe->capiContainer[i]->inputs[j].flags.marker_eos = false;
               pMe->capiContainer[i]->inputs[j].flags.is_timestamp_valid = false;
               pMe->capiContainer[i]->inputs[j].flags.end_of_frame = false;
               memset(pMe->capiContainer[i]->in_buf[j].data_ptr, 0, pMe->capiContainer[i]->in_buf[j].max_data_len);
            }
         }

         for (uint8_t j=0; j<DEC_SVC_MAX_OUTPUT_STREAMS;j++)
         {
            if (pMe->capiContainer[i]->out_buf[j].data_ptr)
            {
               pMe->capiContainer[i]->outputs[j].flags.marker_eos = false;
               pMe->capiContainer[i]->outputs[j].flags.is_timestamp_valid = false;
               pMe->capiContainer[i]->outputs[j].flags.end_of_frame = false;
               pMe->capiContainer[i]->out_buf[j].actual_data_len = 0;
            }
         }

         //reset decoder CAPI
         if (pMe->capiContainer[i]->capi_ptr)
         {
            aud_dec_svc_algorithmic_reset(pMe->capiContainer[i]->capi_ptr);
         }

         pMe->capiContainer[i]->bytes_logged = 0;
      }
   }

   for (uint16_t i=0; i<DEC_SVC_MAX_INPUT_STREAMS; i++)
   {
      if (pMe->in_streams_ptr[i])
      {
         pMe->in_streams_ptr[i]->bEndOfStream = FALSE;

         AudioDecSvc_DecErrorEvent_Reset(&pMe->in_streams_ptr[i]->dec_err_event);

         //reset timestamp state
         (void) AudioDecSvc_InitTsState(&pMe->in_streams_ptr[i]->TsState);

         AudioDecSvc_ResetPullMode(pMe, pMe->in_streams_ptr[i]);

         pMe->in_streams_ptr[i]->WasPrevDecResNeedMore = false;
      }
   }

   for (uint16_t i=0; i<DEC_SVC_MAX_OUTPUT_STREAMS; i++)
   {
      if (pMe->out_streams_ptr[i])
      {
         //if holding a buf, drop it, return to queue.
         if (pMe->out_streams_ptr[i]->outDataBufferNode.pBuffer)
         {
            (void) elite_msg_push_payload_to_returnq(pMe->out_streams_ptr[i]->pOutBufQ, (elite_msg_any_payload_t*)(pMe->out_streams_ptr[i]->outDataBufferNode.pBuffer));
            pMe->out_streams_ptr[i]->outDataBufferNode.pBuffer = NULL;
         }

         memset(&pMe->out_streams_ptr[i]->metadata_xfr, 0, sizeof(dec_metadata_xfer_t));

      }
   }

   return ADSP_EOK;
}
ADSPResult AudioDecSvc_DiscardMarkBuffer(AudioDecSvc_t* pMe,AudioDecSvc_InpStream_t *pInpStrm)
{
   ADSPResult nResult = ADSP_EOK;
   uint32_t uAprOpCode;
   elite_apr_packet_t* pAprPacket;
   pAprPacket = (elite_apr_packet_t*) (pInpStrm->inpDataQMsg.pPayload);
   if(NULL == pInpStrm->inpDataQMsg.pPayload)
   {
      return ADSP_EOK;
   }

   uAprOpCode = elite_apr_if_get_opcode( pAprPacket ) ;
   if( uAprOpCode == ASM_DATA_CMD_MARK_BUFFER_V2)
   {
      asm_data_cmd_mark_buffer_v2_t *apr_payload;
      elite_apr_if_get_payload((void **)&apr_payload, pAprPacket);
      asm_data_event_mark_buffer_v2_t event;
      event.token_lsw = apr_payload->token_lsw;
      event.token_msw = apr_payload->token_msw;
      event.result = ASM_DATA_EVENT_MARK_BUFFER_DISCARDED;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc discarding mark buffer %lu",event.token_lsw);
      nResult = AudioStreamMgr_GenerateClientEventFromCb(&pInpStrm->common.CallbackHandle,
            ASM_DATA_EVENT_MARK_BUFFER_V2,
            pInpStrm->common.CallbackHandle.unAsynClientToken,
            &event,
            sizeof(asm_data_event_mark_buffer_v2_t));
   }
   return nResult;
}
ADSPResult AudioDecSvc_FlushInputDataQ(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   do
   {
      AudioDecSvc_DiscardMarkBuffer(pMe, pInpStrm);
      //first free up any data q msgs that we are already holding
      AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);

      // Drain any queued buffers while there are input data messages.
      AudioDecSvc_GetInputDataCmd(pMe, pInpStrm);

   } while (pInpStrm->inpDataQMsg.pPayload);

   return ADSP_EOK;
}

void AudioDecSvc_UpdatePrevFmtBlk(AudioDecSvc_t *pMe,
      AudioDecSvc_InpStream_t *pInpStrm,
      dec_CAPI_container_t *capi_cont,
      int32_t nSampleRateAfter,
      int32_t nChannelsAfter,
      uint8_t *ucChannelMappingAfter,
      uint16_t bits_per_sample,
      uint8_t capi_out_index)
{
   capi_cont->PrevFmtBlk[capi_out_index].num_channels   = nChannelsAfter;
   if(16 < bits_per_sample)
   {
      capi_cont->PrevFmtBlk[capi_out_index].bits_per_sample = 32;
   }
   else
   {
      capi_cont->PrevFmtBlk[capi_out_index].bits_per_sample = 16;
   }
   capi_cont->PrevFmtBlk[capi_out_index].sample_rate = nSampleRateAfter;
   capi_cont->PrevFmtBlk[capi_out_index].is_signed = 1;
   capi_cont->PrevFmtBlk[capi_out_index].is_interleaved = AUD_DEC_DEINTERLEAVED;
   if( ASM_MEDIA_FMT_ADPCM == capi_cont->dec_fmt_id )

   {
      //for adpcm format, dec doesnt do any de-interleaving,
      //so handle this as a special case
      capi_cont->PrevFmtBlk[capi_out_index].is_interleaved = pInpStrm->pcmFmt.usIsInterleaved;
   }

   memscpy(capi_cont->PrevFmtBlk[capi_out_index].channel_mapping, sizeof(capi_cont->PrevFmtBlk[capi_out_index].channel_mapping), ucChannelMappingAfter, nChannelsAfter);
}

ADSPResult AudioDecSvc_NotifyPeerSvcWithMediaFmtUpdate (AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm,
      int32_t nSampleRateAfter,
      int32_t nChannelsAfter,
      uint8_t *ucChannelMappingAfter,
      uint16_t output_bits_per_sample)
{
   elite_msg_any_t msg;
   ADSPResult nResult=ADSP_EOK;

   uint32_t nPayloadSize = sizeof(elite_msg_data_media_type_apr_t) + sizeof(elite_multi_channel_pcm_fmt_blk_t);
   if ( ADSP_FAILED( elite_msg_create_msg(&msg,&nPayloadSize,
         ELITE_DATA_MEDIA_TYPE,
         NULL, 0,0 ) ))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Fail to create media fmt message");
      return ADSP_EFAILED;
   }

   elite_msg_data_media_type_apr_t *pCmdMsgPayload = (elite_msg_data_media_type_apr_t*) (msg.pPayload);
   pCmdMsgPayload->unMediaTypeFormat = ELITEMSG_DATA_MEDIA_TYPE_APR;
   pCmdMsgPayload->unMediaFormatID   = ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM;

   elite_multi_channel_pcm_fmt_blk_t *pPcmFormatBlock =
         (elite_multi_channel_pcm_fmt_blk_t*) ((uint8_t*)(pCmdMsgPayload)+sizeof(elite_msg_data_media_type_apr_t));
   pPcmFormatBlock->num_channels = nChannelsAfter;

   if(16 < output_bits_per_sample)
   {
      pPcmFormatBlock->bits_per_sample = 32;
   }
   else
   {
      pPcmFormatBlock->bits_per_sample = 16;
   }

   pPcmFormatBlock->sample_rate = nSampleRateAfter;
   pPcmFormatBlock->is_signed = 1;
   pPcmFormatBlock->is_interleaved = AUD_DEC_DEINTERLEAVED;

   dec_CAPI_container_t *last_capi = AudioDecSvc_GetLastCapi(pMe);
   if(NULL == last_capi)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Failed to get last capi");
      return ADSP_EFAILED;
   }
   if( ASM_MEDIA_FMT_ADPCM == last_capi->dec_fmt_id )

   {
      //for pcm format, pcm dec doesnt do any de-interleaving,
      //same is the case with adpcm decoding
      //so handle this as a special case
      pPcmFormatBlock->is_interleaved = pInpStrm->pcmFmt.usIsInterleaved;
   }

   /*******************************************************************/
   /***MULTI-CHANNEL-TO-DO: CAPI shall provide appropriate channel mapping****/
   /** For now, work around to provide necessary information: */
   /*   Mono: center */
   /* Stero: Left, right */
   /* 5.1: Left, Right, Center, LFE, LS, RS */
   /***************************************************************/
   memscpy( pPcmFormatBlock->channel_mapping, sizeof(pPcmFormatBlock->channel_mapping), ucChannelMappingAfter, nChannelsAfter);

   nResult = qurt_elite_queue_push_back(pOutStrm->pDownStreamSvc->dataQ, (uint64_t*)&msg );

   MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,
         "Reconfig media format: FMTID 0x%lx, CH %d, BPS %d, SR %lu, isSigned %d, isInterleaved %d, Result %d",
         pCmdMsgPayload->unMediaFormatID,pPcmFormatBlock->num_channels,
         pPcmFormatBlock->bits_per_sample,pPcmFormatBlock->sample_rate,
         pPcmFormatBlock->is_signed,pPcmFormatBlock->is_interleaved,nResult);


   AudioDecSvc_DetermineSvcBw(pMe, pPcmFormatBlock, &pInpStrm->pcmFmt);
   //force vote since svc vote might have changed.
   nResult = AudioDecSvc_ProcessKppsBw(pMe, FALSE, TRUE);

   return nResult;
}
//#define AUDENCDEC_TIMESTAMP_LOGS
ADSPResult AudioDecSvc_SendPcmToPeerSvc (AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, elite_msg_data_buffer_t *pOutBuf )
{
   ADSPResult nResult=ADSP_EOK;

   // send the output buffer downstream
   AudioDecSvc_InitOutDataBuf(pOutBuf, pOutStrm);

   //put timestamp in output buffer
   asm_set_timestamp_valid_flag(&pOutBuf->nFlag, pInpStrm->TsState.bNextOutbufTSValid);
   pOutBuf->ullTimeStamp = pInpStrm->TsState.ullNextOutBufTS;
#ifdef AUDENCDEC_TIMESTAMP_LOGS
   MSG_4(MSG_SSID_QDSP6,  DBG_HIGH_PRIO, "outgoing buffer TS: %lu %lu, flag=%lx, size=%ld",
         (uint32_t)(pOutBuf->ullTimeStamp>>32), (uint32_t)pOutBuf->ullTimeStamp, pOutBuf->nFlag, pOutBuf->nActualSize);
#endif
   //update next output time stamp
   dec_CAPI_container_t *last_capi = AudioDecSvc_GetLastCapi(pMe);
   if(NULL == last_capi)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Failed to get last capi");
      return ADSP_EFAILED;
   }
   //TODO: since this function is called for each out stream, pInpStrm shouldn't be used here
   (void)AudioDecSvc_UpdateNextOutbufTS(pOutBuf->nActualSize, &last_capi->PrevFmtBlk[0], &pInpStrm->TsState,
         pMe->mainCapiContainer->dec_fmt_id); //always we use dec fmt of main dec here.

   uint32_t client_token = AudioDecSvc_AddBufDelayAndGetClientToken(pOutStrm, pOutBuf->nActualSize,
         last_capi->PrevFmtBlk[0].sample_rate, last_capi->PrevFmtBlk[0].num_channels, last_capi->PrevFmtBlk[0].bits_per_sample/8);

   qurt_elite_bufmgr_node_t outBufMgrNode = pOutStrm->outDataBufferNode;
   elite_msg_any_t *pPeerDataQMsg = elite_msg_convt_buf_node_to_msg(
         &outBufMgrNode,
         ELITE_DATA_BUFFER,
         NULL, /* do not need response */
         client_token,   /* token : store delay in the token */
         0     /* do not care response result*/
   );
   nResult = AudioDecSvc_PushOutBuf(pMe, pOutStrm, (uint64_t*)pPeerDataQMsg);

   return nResult;
}

/*
 *Function Name: AudioDecSvc_InitTsState
 *
 *Parameters: DecTimeStampState_t *pTsState: Instance of timestamp structure
 *
 *Description: Initialize timestamp parameters
 *
 *Returns: ADSP_EOK/ADSP_EBADPARAM
 *
 */
ADSPResult AudioDecSvc_InitTsState(DecTimeStampState_t *pTsState)
{
   if(!pTsState)
   {
      return ADSP_EBADPARAM;
   }

   pTsState->ullInbufTS = 0;
   pTsState->bInbufTSValid = false;
   pTsState->ullNextOutBufTS = 0;
   pTsState->ullNextOutBufSampleCount = 0;
   pTsState->ullLastSyncedInBufTS = 0;
   pTsState->bNextOutbufTSValid = false;
   pTsState->bNextOutbufTsOld = false;

   return ADSP_EOK;
}

/*
 *Function Name: AudioDecSvc_SyncToInputTS
 *
 *Parameters: DecTimeStampState_t *pTsState: Instance of timestamp structure
 *
 *Description: Set the next output buffer timestamp equal to the input
 *             buffer's timestamp. And mark that we are synced to the
 *             input buffer's timestamp.
 *
 *Returns: None
 *
 */
void AudioDecSvc_SyncToInputTS(DecTimeStampState_t *pTsState)
{
   pTsState->ullLastSyncedInBufTS = pTsState->ullInbufTS;
   pTsState->ullNextOutBufTS = pTsState->ullInbufTS;
   pTsState->ullNextOutBufSampleCount = 0;
   pTsState->bNextOutbufTSValid = pTsState->bInbufTSValid;
   pTsState->bNextOutbufTsOld = false;
}

/*
 *Function Name: AudioDecSvc_FillTsStateFromInBuf
 *
 *Parameters: DecTimeStampState_t *pTsState: Instance of timestamp structure
 *             asm_data_cmd_write_v2_t *pInBuf: Pointer to data write cmd
 *
 *Description: Fills time stamp struct from the input data write command
 *
 *Returns: ADSP_EOK/ADSP_EBADPARAM
 *
 * TODO: dec_fmt for this is taken from main decoder, ideally we shouldn't need these TS related functions to depend
 * on time-stamp.
 */
ADSPResult AudioDecSvc_FillTsStateFromInBuf(DecTimeStampState_t *pTsState,
      uint32_t dec_fmt,
      uint64_t ts,
      bool_t ts_valid,
      bool_t ts_continue,
      bool_t eof_flag, bool_t WasPrevDecResNeedMore)
{
   //copy input buffer time stamp info
   pTsState->ullInbufTS = ts;
   pTsState->bInbufTSValid = ts_valid;
   bool_t bTsContinue = ts_continue;

#ifdef AUDENCDEC_TIMESTAMP_LOGS
   MSG_5(MSG_SSID_QDSP6,  DBG_HIGH_PRIO, "incoming buffer TS: %lu %lu, valid=%d,continue=%d,EoF=%u",
         (uint32_t)(pTsState->ullInbufTS>>32), (uint32_t)pTsState->ullInbufTS, pTsState->bInbufTSValid,bTsContinue, (unsigned int)eof_flag);
#endif

   if(!pTsState->bInbufTSValid && bTsContinue)
   {
      // Current buffer TS is invalid and TS continue flag is set to true.
      // so next output buffer TS and TS valid flag
      // continue to hold; just mark that next output
      // buffer TS is not stale and return.
      pTsState->bNextOutbufTsOld = false;
      return ADSP_EOK;
   }

   /* if the last decoding attempt did not require more data, it means
   that there are no more bytes left unconsumed in the last input
   buffer, so next output buffer TS should be the same as that of
   the new input buffer.
    */
   if(!WasPrevDecResNeedMore)
   {
      AudioDecSvc_SyncToInputTS(pTsState);
   }
   else
   {
      //we continue to use output time stamp based on prev input buffer
      //so set this flag to true
      pTsState->bNextOutbufTsOld = true;

      if(ASM_MEDIA_FMT_WMA_V9_V2 == dec_fmt ||
            ASM_MEDIA_FMT_WMA_V10PRO_V2 == dec_fmt)
      {
         /* An exception for WMA codecs until ASF packet TS
            are properly understood.
            If the incoming TS is very close (< 2 ms) to
            the next output buffer TS, treat the incoming TS
            as the TS for the next output buffer, i.e.,
            sync to the incoming TS immediately.
            Rather than delay it by one
            output buffer and cause TS discontinuity*/

         int64_t ts_diff = ((int64_t) pTsState->ullNextOutBufTS) - ((int64_t) pTsState->ullInbufTS);

         if(pTsState->bNextOutbufTSValid &&
               (ts_diff < 2000) &&
               (ts_diff > -2000))
         {
            AudioDecSvc_SyncToInputTS(pTsState);
         }
      }
   }

   return ADSP_EOK;
}

/*
 *Function Name: AudioDecSvc_UpdateNextOutbufTS
 *
 *Parameters: uint32_t ulOutbufSize: Output buffer size in bytes
 *            elite_multi_channel_pcm_fmt_blk_t* pOutputFmt : Output format blk
 *            DecTimeStampState_t* pTsState: Timestamp state
 *
 *Description: Updates next outbuf TS by output frame duration
 *
 *Returns: ADSP_EOK/ADSP_EBADPARAM
 *
 * TODO: dec_fmt for this is taken from main decoder, ideally we shouldn't need these TS related functions to depend
 * on time-stamp.
 *
 */
ADSPResult AudioDecSvc_UpdateNextOutbufTS(uint32_t ulOutbufSize,
      elite_multi_channel_pcm_fmt_blk_t* pOutputFmt,
      DecTimeStampState_t* pTsState,
      uint32_t dec_fmt)
{
   if(!pOutputFmt || !pTsState)
   {
      return ADSP_EBADPARAM;
   }

   uint32_t bytes_per_sample = (pOutputFmt->bits_per_sample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;

   //increment TS based on # of samples and sample rate
   uint32_t ulNumSampPerCh = (ulOutbufSize) / (pOutputFmt->num_channels * bytes_per_sample);
   uint32_t ulSampleRate = pOutputFmt->sample_rate;

   //get output frame duration in microsec
   pTsState->ullNextOutBufSampleCount += ulNumSampPerCh;

   pTsState->ullNextOutBufTS = pTsState->ullLastSyncedInBufTS +
         ((pTsState->ullNextOutBufSampleCount * (uint64_t)NUM_US_PER_SEC ) / ulSampleRate );

   //if the output TS is based on previous input buffer's TS,
   //update to the current input buffer's TS

   if(pTsState->bNextOutbufTsOld)
   {
      if(ASM_MEDIA_FMT_WMA_V10PRO_V2 != dec_fmt &&
            ASM_MEDIA_FMT_WMA_V9_V2 != dec_fmt)
      {
         AudioDecSvc_SyncToInputTS(pTsState);
      }
      else
      {
         //check if the input TS is still in the future
         //hold up syncing until we reach there
         int64_t ts_diff = ((int64_t) pTsState->ullInbufTS) - ((int64_t) pTsState->ullNextOutBufTS);
         if(pTsState->bInbufTSValid &&
               (ts_diff < 2000) &&
               (ts_diff >=0 ))
         {
            AudioDecSvc_SyncToInputTS(pTsState);
         }

      }

   }

   return ADSP_EOK;
}

/*
 *Function Name: AudioDecSvc_SendSrCmEvent
 *
 *Parameters: pMe: Decoder Service state structure pointer
 *            lSamplerate: new sample rate
 *            lNumChannels: new # of channels
 *
 *Description: Sends SR/CM change notification event (if enabled)
 *
 *Returns: ADSP_EOK/ADSP_EBADPARAM
 *
 */
ADSPResult AudioDecSvc_SendSrCmEvent(AudioDecSvc_t *pMe,AudioDecSvc_InpStream_t *pInpStrm,
      uint32_t sample_rate,
      DecChannelMap_t *chan_map)
{
   //raise event only if it is enabled
   if(!pInpStrm->bEnableSrCmEvent)
   {
      return ADSP_EOK;
   }

   //form the payload for the event
   asm_data_event_sr_cm_change_notify_t SrCmEvent;
   SrCmEvent.sample_rate = sample_rate;
   SrCmEvent.num_channels = (uint16_t) chan_map->nChannels;
   SrCmEvent.reserved = 0;

   memscpy(SrCmEvent.channel_mapping, sizeof(SrCmEvent.channel_mapping), chan_map->nChannelMap, chan_map->nChannels);

   return AudioStreamMgr_GenerateClientEventFromCb(&pInpStrm->common.CallbackHandle,
         ASM_DATA_EVENT_SR_CM_CHANGE_NOTIFY,
         pInpStrm->common.CallbackHandle.unAsynClientToken,
         &SrCmEvent,
         sizeof(SrCmEvent));
}

/*
 *Function Name: AudioDecSvc_RemoveIinitialSamples
 *
 *Parameters:samples_to_remove-samples to remove at the beginning of stream
 *           valid_bytes_in_buffer-valid byted in output buffer
 *           buffer_start- starting address of buffer
 *           num_channels- number of channels
 *Description: Removed the required number of samples from beginning of output buffer. If this
 *             is greater than output buffer size, removes the whole buffer and decrements
 *             the value accordingly
 *
 *Returns: none
 *
 */
void AudioDecSvc_RemoveInitialSamples(uint32_t *samples_to_remove,
      uint32_t *valid_bytes_in_buffer,
      uint8_t * buffer_start,
      uint16_t num_channels,
      uint32_t sampling_rate,
      uint64_t *next_output_buf_ts,
      uint64_t *next_out_buf_sample_count,
      uint32_t bytes_per_sample
)
{
   uint32_t bytes_to_remove, samples_removed;
   uint32_t valid_bytes_per_channel, bytes_to_remove_per_channel;

   /* per channel address to read from and write to */
   uint8_t *addr_read, *addr_write;

   bytes_to_remove = (*samples_to_remove) * num_channels * bytes_per_sample;

   valid_bytes_per_channel = (*valid_bytes_in_buffer)/num_channels;


   if(bytes_to_remove > *valid_bytes_in_buffer)
   {
      bytes_to_remove = *valid_bytes_in_buffer;
      samples_removed = ((bytes_to_remove / bytes_per_sample)/ num_channels);
      *samples_to_remove -= samples_removed;
   }
   else
   {
      samples_removed = *samples_to_remove;
      *samples_to_remove = 0;
   }

   bytes_to_remove_per_channel = bytes_to_remove / num_channels;

   addr_read = buffer_start;
   addr_write = buffer_start;

   for (uint16_t i=0; i < num_channels; i++)
   {
      memsmove(addr_write,valid_bytes_per_channel-bytes_to_remove_per_channel,
            addr_read+bytes_to_remove_per_channel,valid_bytes_per_channel -
            bytes_to_remove_per_channel);
      addr_read += valid_bytes_per_channel;
      addr_write += (valid_bytes_per_channel - bytes_to_remove_per_channel);
   }


   //memcpy(buffer_start, buffer_start + bytes_to_remove, *valid_bytes_in_buffer - bytes_to_remove);
   *valid_bytes_in_buffer -= bytes_to_remove;

   /*We need to increment the timestamp accordingly*/
   *next_output_buf_ts += (((uint64_t)samples_removed * NUM_US_PER_SEC)/sampling_rate);
   *next_out_buf_sample_count += samples_removed;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: Initial bytes removed:%lu", bytes_to_remove);
}


/*
 *Function Name: AudioDecSvc_RemoveTrailingSamples
 *
 *Parameters:samples_to_remove-samples to remove at the end of stream
 *           valid_bytes_in_buffer-valid byted in output buffer *
 *           num_channels- number of channels
 *Description: Removed the required number of samples from the end
 *             of output buffer.
 *
 *Returns: none*
 */
void AudioDecSvc_RemoveTrailingSamples(uint32_t samples_to_remove,
      uint32_t *valid_bytes_in_buffer,
      uint8_t *buffer_start,
      uint16_t num_channels,
      uint32_t bytes_per_sample)
{
   uint32_t bytes_to_remove;
   uint32_t valid_bytes_per_channel, bytes_to_remove_per_channel;


   /* per channel address to read from and write to */
   uint8_t *addr_read, *addr_write;


   valid_bytes_per_channel = (*valid_bytes_in_buffer)/num_channels;

   bytes_to_remove = samples_to_remove * num_channels * bytes_per_sample;
   if(bytes_to_remove > *valid_bytes_in_buffer)
   {
      bytes_to_remove = *valid_bytes_in_buffer;
   }

   bytes_to_remove_per_channel = bytes_to_remove / num_channels;

   addr_read = buffer_start;
   addr_write = buffer_start;

   for (uint16_t i=0; i < num_channels-1; i++)
   {
      addr_read += valid_bytes_per_channel;
      addr_write += (valid_bytes_per_channel - bytes_to_remove_per_channel);

      memsmove(addr_write,valid_bytes_per_channel- bytes_to_remove_per_channel,
            addr_read, valid_bytes_per_channel - bytes_to_remove_per_channel);

   }


   *valid_bytes_in_buffer -= bytes_to_remove;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: Trailing bytes removed:%lu", bytes_to_remove);
}

/*
 *Function Name: AudioDecSvc_LogBitStreamData
 *
 *Parameters: buf_addr : pointer to buffer to be logged
 *            buf_size : buffer size in bytes
 *            session_id : session id for the log
 *            ulDecFormatId : format id for decoder
 *
 *Description: Logs the encoded bistream data
 *
 *Returns: ADSPResult
 *
 */
ADSPResult AudioDecSvc_LogBitStreamData(int8_t *buf_addr, uint32_t buf_size,
      uint32_t session_id, uint32_t ulDecFormatId)
{
   ADSPResult result = ADSP_EOK;
   elite_log_info log_info_obj;

   /* Populate the packet to be sent to logging utility */
   log_info_obj.qxdm_log_code = QXDM_LOG_CODE_AUD_DEC_IN;
   log_info_obj.buf_ptr = (uint8_t *)buf_addr;
   log_info_obj.buf_size = buf_size;
   log_info_obj.session_id = session_id;
   log_info_obj.log_tap_id = AUDIOLOG_AUD_DEC_BS_IN_TAP_ID;
   log_info_obj.log_time_stamp = qurt_elite_timer_get_time();
   log_info_obj.data_fmt = ELITE_LOG_DATA_FMT_BITSTREAM;

   log_info_obj.data_info.media_fmt_id = ulDecFormatId;

   /* Allocate the log buffer and log the packet
      If log code is disabled, log buffer allocation returns NULL
    */
   result = elite_allocbuf_and_log_data_pkt(&log_info_obj);

   return result;
}

/*
 *Function Name: AudioDecSvc_LogPcmData
 *
 *Parameters: AudioDecSvc_t* pMe: Instance of enc svc
 *            buf_addr: Addr of buffer to be logged
 *            buf_size: Size in bytes of buffer to be logged
 *
 *Description: Logs the input PCM data to encoder service
 *
 *Returns: ADSPResult
 *
 */
ADSPResult AudioDecSvc_LogPcmData(AudioDecSvc_t* pMe, AudioDecSvc_InpStream_t *pInpStrm, int8_t * buf_addr, uint32_t buf_size)
{
   ADSPResult result = ADSP_EOK;

   elite_log_info log_info_obj;
   pcm_data_info *pcm_data = &(log_info_obj.data_info.pcm_data_fmt);

   /* Populate the packet to be sent to logging utility */
   log_info_obj.qxdm_log_code = QXDM_LOG_CODE_AUD_DEC_IN;
   log_info_obj.buf_ptr = (uint8_t *)buf_addr;
   log_info_obj.buf_size = buf_size;
   log_info_obj.session_id = pInpStrm->common.ulDataLogId;
   log_info_obj.log_tap_id = AUDIOLOG_AUD_DEC_PCM_IN_TAP_ID;
   log_info_obj.log_time_stamp = qurt_elite_timer_get_time();
   log_info_obj.data_fmt = ELITE_LOG_DATA_FMT_PCM;

   pcm_data->num_channels = pInpStrm->pcmFmt.chan_map.nChannels;
   pcm_data->sampling_rate = pInpStrm->pcmFmt.ulPCMSampleRate;
   pcm_data->bits_per_sample = pInpStrm->pcmFmt.usBytesPerSample*8;
   pcm_data->interleaved = pInpStrm->pcmFmt.usIsInterleaved;
   pcm_data->channel_mapping = pInpStrm->pcmFmt.chan_map.nChannelMap;

   /* Allocate the log buffer and log the packet
      If log code is disabled, log buffer allocation returns NULL
    */
   result = elite_allocbuf_and_log_data_pkt(&log_info_obj);

   return result;
}

/**
 * total BW = sum of CAPI BW + svc contribution.
 */
static uint32_t AudioDecSvc_DetermineTotalBw(AudioDecSvc_t* pMe)
{
   uint32_t bw = 0;

   for (uint8_t capi_i = 0; capi_i < DEC_SVC_MAX_CAPI; capi_i++)
   {
      if (!pMe->capiContainer[capi_i]) break;

      bw += (pMe->capiContainer[capi_i]->data_bw + pMe->capiContainer[capi_i]->code_bw);
   }
   return bw + pMe->svc_bw;
}

/*
 * svc BW = input BW + output BW. Input BW is considered only for PCM.
 *
 * Also this BW should ideally be InpStrm, OutStrm property. But for now, its property of AudioDecSvc_t
 */
static void AudioDecSvc_DetermineSvcBw(AudioDecSvc_t* pMe, elite_multi_channel_pcm_fmt_blk_t *pPcmFormatBlock, DecPcmFmt_t *pcmInFmt)
{
   uint32_t bw = 0;
   if (NULL != pPcmFormatBlock)
   {
      bw += pPcmFormatBlock->sample_rate * \
            pPcmFormatBlock->num_channels * \
            pPcmFormatBlock->bits_per_sample/8; //this works for compressed as well since 61937 is as good as pcm.
   }

   for (uint8_t capi_i = 0; capi_i < DEC_SVC_MAX_CAPI; capi_i++)
   {
      if (!pMe->capiContainer[capi_i]) break;

      switch(pMe->capiContainer[capi_i]->dec_fmt_id)
      {
      case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
      case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
      {
         if (NULL != pcmInFmt)
         {
            bw += pcmInFmt->ulPCMSampleRate * pcmInFmt->usBytesPerSample * pcmInFmt->chan_map.nChannels;
            if (pcmInFmt->ulPCMSampleRate > 48000)
            {
               bw += 1*1024*1024;
            }
            if (pcmInFmt->chan_map.nChannels > 2)
            {
               bw += 1*1024*1024;
            }
         }
      }
      break;
      default:
         break;
      }
   }

   pMe->svc_bw = bw;
}

/**
 * force_vote doesn't matter if is_release=TRUE
 *
 * is_release helps in releasing BW even when aggregated BW is nonzero, useful for suspend.
 *
 * force_vote helps in voting BW due to changes in svc & not due to CAPI V2 events.
 */
ADSPResult AudioDecSvc_ProcessKppsBw(AudioDecSvc_t *pMe, bool_t is_release, bool_t force_vote)
{
   ADSPResult result = ADSP_EOK;

   uint32_t decoder_kpps=0, gen_bw = 0;

   if ( !qurt_elite_adsppm_wrapper_is_registered(pMe->ulAdsppmClientId) )
   {
       return ADSP_EFAILED;
   }

   if (!is_release)
   {
      if ( (pMe->event_mask & AUD_DEC_SVC_EVENT__KPPS_MASK) || force_vote)
      {
         result = AudioDecSvc_AggregateKppsRequired(pMe, &decoder_kpps);
         if(ADSP_FAILED(result))
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to aggregate kpps");
         }

         pMe->event_mask &= ~AUD_DEC_SVC_EVENT__KPPS_MASK;
      }
      else
      {
         decoder_kpps = pMe->prev_kpps_vote;
      }

      if ( (pMe->event_mask & AUD_DEC_SVC_EVENT__BW_MASK) || force_vote)
      {
         gen_bw = AudioDecSvc_DetermineTotalBw(pMe);

         //clear the event
         pMe->event_mask &= ~AUD_DEC_SVC_EVENT__BW_MASK;

      }
      else
      {
         gen_bw = pMe->prev_bw_vote;
      }
   }

   //If there was no event or no release-call, or no foce vote or there was no change, return.
   if (!(is_release || (pMe->prev_kpps_vote != decoder_kpps) || (pMe->prev_bw_vote != gen_bw)))
   {
      return ADSP_EOK;
   }

#if (ADSPPM_INTEGRATION==1)
   static const uint8_t NUM_REQUEST=2;
   MmpmRscParamType rscParam[NUM_REQUEST];
   MMPM_STATUS      retStats[NUM_REQUEST];
   MmpmRscExtParamType reqParam;
   uint8_t req_num=0;
   MmpmMppsReqType mmpmMppsParam;
   MmpmGenBwValType bwReqVal;
   MmpmGenBwReqType bwReq;
   uint32_t mpps=0;
   reqParam.apiType                    = MMPM_API_TYPE_SYNC;
   reqParam.pExt                       = NULL;       //for future
   reqParam.pReqArray                  = rscParam;
   reqParam.pStsArray                  = retStats;   //for most cases mmpmRes is good enough, need not check this array.
   reqParam.reqTag                     = 0;          //for async only

   /** check if mips and bw are both releases or requests. both_diff => each != request or release. */
   bool_t both_diff = ( (decoder_kpps > 0) ^ (gen_bw > 0) );
   //whether mips req is made
   bool_t is_mips_req = ( (pMe->prev_kpps_vote != decoder_kpps) || is_release);
   //whether bw req is made
   bool_t is_bw_req = ( (pMe->prev_bw_vote != gen_bw) || is_release );

   bool_t req_done = false;

   if ( is_mips_req )
   {
      pMe->prev_kpps_vote = decoder_kpps;

      mpps = (decoder_kpps+999)/1000;

      //Requesting 0 will be equivalent to releasing particular resource
      mmpmMppsParam.mppsTotal                  = mpps;
      mmpmMppsParam.adspFloorClock             = 0;
      rscParam[req_num].rscId                   = MMPM_RSC_ID_MPPS;
      rscParam[req_num].rscParam.pMppsReq       = &mmpmMppsParam;

      req_num++;
      if (both_diff || !is_bw_req) //request separately if either bw and mips are differet types (req/rel), or if there's no BW req.
      {
         reqParam.numOfReq                   = req_num;

         if (decoder_kpps == 0)
         {
            result = qurt_elite_adsppm_wrapper_release(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM release MPPS by Dec (%lu). Result %lu",pMe->ulAdsppmClientId, result);
         }
         else
         {
            result = qurt_elite_adsppm_wrapper_request(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM request MPPS %lu by Dec (%lu). Result %lu", mpps, pMe->ulAdsppmClientId,result);
         }
         req_done = true;
         req_num = 0; //reset req_num as req is already made.
      }
   }

   if ( is_bw_req )
   {
      pMe->prev_bw_vote = gen_bw;

      bwReqVal.busRoute.masterPort                 = MMPM_BW_PORT_ID_ADSP_MASTER;
      bwReqVal.busRoute.slavePort                  = MMPM_BW_PORT_ID_DDR_SLAVE;
      bwReqVal.bwValue.busBwValue.bwBytePerSec     = gen_bw;
      bwReqVal.bwValue.busBwValue.usagePercentage  = 100;
      bwReqVal.bwValue.busBwValue.usageType        = MMPM_BW_USAGE_LPASS_DSP;

      bwReq.numOfBw            = 1;
      bwReq.pBandWidthArray    = &bwReqVal;

      rscParam[req_num].rscId                   = MMPM_RSC_ID_GENERIC_BW_EXT;
      rscParam[req_num].rscParam.pGenBwReq      = &bwReq;

      req_num++;
      if (both_diff || !is_mips_req) //request separately if either bw and mips are differet types (req/rel), or if there's no mips req.
      {
         reqParam.numOfReq                   = req_num;

         if (gen_bw==0)
         {
            result = qurt_elite_adsppm_wrapper_release(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM release BW by Dec (%lu). Result %lu", pMe->ulAdsppmClientId, result);
         }
         else
         {
            result = qurt_elite_adsppm_wrapper_request(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM request BW %lu KBPS by Dec (%lu). Result %lu", gen_bw/1024, pMe->ulAdsppmClientId, result);
         }
         req_done = true;
         req_num = 0; //reset req_num as req is already made.
      }
   }

   if (req_num && !req_done)
   {
      reqParam.numOfReq                   = req_num;

      if ( (decoder_kpps == 0) && (gen_bw == 0) )
      {
         result = qurt_elite_adsppm_wrapper_release(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM release BW and MPPS by Dec (%lu). Result %lu", pMe->ulAdsppmClientId,result);
      }
      else
      {
         result = qurt_elite_adsppm_wrapper_request(pMe->ulAdsppmClientId, &pMe->adsppmClientPtr, &reqParam);
         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM request MPPS %lu and BW %lu KBPS by Dec (%lu). Result %lu", mpps, gen_bw/1024, pMe->ulAdsppmClientId, result);
      }
   }

#endif //#if (ADSPPM_INTEGRATION==1)
   return result;
}

/** gives for all CAPIs. but not per IO streams. */
static ADSPResult AudioDecSvc_AggregateKppsRequired(AudioDecSvc_t *pMe, uint32_t *decoder_kpps)
{
   int kpps,num_chans=0,samp_rate=0,bytes_per_samp=0;
   *decoder_kpps = 0;

   for (uint8_t i=0; i<DEC_SVC_MAX_CAPI; i++)
   {
      if (!pMe->capiContainer[i]) break;

      if (pMe->capiContainer[i] && pMe->capiContainer[i]->capi_ptr)
      {
         kpps = pMe->capiContainer[i]->kpps;
         if (0==kpps) kpps = GENERIC_DEC_KPPS;
      }
      else
      {
         /* Aggregating MPPS votes for PCM decoder using media format */
         /* scaling to a minimum of 4 MPPS to avoid very less vote to */
         /* ensure existing usecases are not disturbed much           */
		   num_chans =  pMe->in_streams_ptr[0]->pcmFmt.chan_map.nChannels;
		   samp_rate = pMe->in_streams_ptr[0]->pcmFmt.ulPCMSampleRate;
		   bytes_per_samp = pMe->in_streams_ptr[0]->pcmFmt.usBytesPerSample;
         kpps = (PCM_DEC_KPPS_FACTOR * num_chans * samp_rate * bytes_per_samp)/1000;   //default mips (PCM)
         kpps = (kpps > PCM_DEC_MIN_KPPS) ? kpps : PCM_DEC_MIN_KPPS;
      }

      *decoder_kpps += kpps;  //aggregate kpps.
   }

   return ADSP_EOK;
}

ADSPResult AudioDecSvc_GetRequiredThreadStackSize(AudioDecSvc_t *pMe, uint32_t *threadStackSize)
{
   if (*threadStackSize == 0 ) *threadStackSize = PCM_STACK_SIZE;

   *threadStackSize += STACK_SIZE_DEC_PROCESS;
   if (MIN_AUDEC_THREAD_STACK_SIZE > *threadStackSize)
   {
      //minimum cumulative stack required excluding decoder process
      *threadStackSize = MIN_AUDEC_THREAD_STACK_SIZE;
   }

   return ADSP_EOK;
}

bool_t AudioDecSvc_IsLastCapiIndex(AudioDecSvc_t *pMe, uint32_t ind)
{
   return ( (ind == (DEC_SVC_MAX_CAPI-1)) || (pMe->capiContainer[ind+1] == NULL) );
}

bool_t AudioDecSvc_IsLastCapi(AudioDecSvc_t *pMe, dec_CAPI_container_t *capi_container)
{
   for (uint8_t i=0; i<DEC_SVC_MAX_CAPI; i++)
   {
      if (capi_container == pMe->capiContainer[i])
      {
         if ( AudioDecSvc_IsLastCapiIndex (pMe, i) )
         {
            return TRUE;
         }
      }
   }
   return FALSE;
}

dec_CAPI_container_t *AudioDecSvc_GetFirstCapi(AudioDecSvc_t *pMe)
{
   return pMe->capiContainer[0];
}

//There can be only one MIMO CAPI
dec_CAPI_container_t *AudioDecSvc_GetMimoCapi(AudioDecSvc_t *pMe)
{
   //currently MIMO CAPI is always the last CAPI
   return AudioDecSvc_GetLastCapi(pMe);
}

dec_CAPI_container_t *AudioDecSvc_GetLastCapi(AudioDecSvc_t *pMe)
{
   uint8_t i;
   for (i = 0; i<DEC_SVC_MAX_CAPI; i++)
   {
      if (!pMe->capiContainer[i]) break;
   }

   if(0 == i)
   {
      return NULL;
   }
   else
   {
      return pMe->capiContainer[i-1];
   }
}

void AudioDecSvc_DecErrorEvent_Clear(dec_err_event_t *dec_err_event)
{
   if (!dec_err_event) return;

   dec_err_event->nConsecutiveDecErr = 0;
}

/**
 * called during flush, shouldn't disable events if enabled at that time.
 */
void AudioDecSvc_DecErrorEvent_Reset(dec_err_event_t *dec_err_event)
{
   if (!dec_err_event) return;

   AudioDecSvc_DecErrorEvent_Clear(dec_err_event);
}

void AudioDecSvc_DecErrorEvent_RecordError(dec_err_event_t *dec_err_event)
{
   if (!dec_err_event) return;

   //Mark start time of the first failure
   if(dec_err_event->nConsecutiveDecErr == 0)
   {
      dec_err_event->errorTime =  qurt_elite_timer_get_time_in_msec();
   }

   dec_err_event->nConsecutiveDecErr++;
}

static inline bool_t AudioDecSvc_DecErrEvent_NeedToRaise(uint16_t currError, uint16_t threshError, bool_t timeOut)
{
   return ((currError > 1) && timeOut) || ((currError >= threshError) && (threshError > 0));
}

void AudioDecSvc_DecErrorEvent_CheckRaise(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, dec_err_event_t *dec_err_event)
{
   if (!dec_err_event) return;

   if (!dec_err_event->bEnableDecErrEvent) return;

   if (dec_err_event->nConsecutiveDecErr == 0) return;

   uint64_t currentTime = qurt_elite_timer_get_time_in_msec();

   //If the time between now and the first consecutive failure is greater than timeout value, raise event.
   bool_t bTimeOut = ((currentTime - dec_err_event->errorTime) > dec_err_event->msThresholdTimeout);

#ifdef DBG_DEC_ERR_EVENT
   //debugging msg
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Decoder Error count : %d, threshold %d, time %ld",
         dec_err_event->nConsecutiveDecErr,dec_err_event->nThresholdDecError,
         (int32_t)(currentTime - dec_err_event->errorTime));
#endif//DBG_DEC_ERR_EVENT


   if(AudioDecSvc_DecErrEvent_NeedToRaise(dec_err_event->nConsecutiveDecErr, dec_err_event->nThresholdDecError, bTimeOut))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Consecutive error boudanry of size %u hit", dec_err_event->nThresholdDecError);

      //Raise dec err event.
      AudioStreamMgr_GenerateClientEventFromCb(&pInpStrm->common.CallbackHandle,
            ASM_DATA_EVENT_DEC_ERR_NOTIFY,
            pInpStrm->common.CallbackHandle.unAsynClientToken,
            NULL, 0);

      AudioDecSvc_DecErrorEvent_Clear(dec_err_event);

   }
}

void AudioDecSvc_DecErrEvent_SetParam(dec_err_event_t *dec_err_event, uint8_t *pSetParamPayload)
{
   asm_dec_err_param_t *pDecErrMsg =
         (asm_dec_err_param_t *)pSetParamPayload;

   if (0 == pDecErrMsg->n_dec_err_threshold)
   {
      dec_err_event->bEnableDecErrEvent = FALSE;
      return;
   }

   dec_err_event->bEnableDecErrEvent = TRUE;
   dec_err_event->nThresholdDecError = pDecErrMsg->n_dec_err_threshold;
   dec_err_event->msThresholdTimeout = pDecErrMsg->timeout_ms;
}

ADSPResult AudioDecSvc_RegisterWithAdsppm(AudioDecSvc_t* pMe)
{
   ADSPResult result = ADSP_EOK;
#if (ADSPPM_INTEGRATION==1)

   MmpmRegParamType regParam;
   char threadname[16];
   qurt_thread_get_name(threadname, 16-1);

   regParam.rev             = MMPM_REVISION;
   regParam.instanceId      = MMPM_CORE_INSTANCE_0;
   regParam.pwrCtrlFlag     = PWR_CTRL_NONE; //PWR_CTRL_STATIC_DISPLAY, PWR_CTRL_THERMAL
   regParam.callBackFlag    = CALLBACK_NONE; //CALLBACK_STATIC_DISPLAY, CALLBACK_THERMAL, CALLBACK_REQUEST_COMPLETE
   regParam.MMPM_Callback   = NULL;
   regParam.cbFcnStackSize  = 0;

   regParam.coreId          = MMPM_CORE_ID_LPASS_ADSP; //no need to request power, register access.
   regParam.pClientName     = threadname;

   result = qurt_elite_adsppm_wrapper_register(&regParam, &pMe->ulAdsppmClientId, &pMe->adsppmClientPtr);

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM register by Dec. Result %lu. Client id %lu", result, pMe->ulAdsppmClientId);

#endif
   return result;
}

ADSPResult AudioDecSvc_DeregisterWithAdsppm(AudioDecSvc_t* pMe)
{
   ADSPResult result = ADSP_EOK;

   if ( !qurt_elite_adsppm_wrapper_is_registered(pMe->ulAdsppmClientId) )
   {
       return ADSP_EOK;
   }

   uint32_t client_id = pMe->ulAdsppmClientId;
   result = qurt_elite_adsppm_wrapper_deregister(&pMe->ulAdsppmClientId, &pMe->adsppmClientPtr);

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM deregister by Dec. Result %lu. Client id %lu", result, client_id);
   return result;
}


#ifdef ELITE_CAPI_H
#error "Do not include CAPI V1 in this file"
#endif
