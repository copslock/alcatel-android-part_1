/** @file AudioDecSvc_CapiV1Util.cpp
This file contains functions for Elite Decoder service.

Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Technologies, Incorporated Proprietary.
Export of this technology or software is regulated by the U.S. Government,
Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_CapiV1Util.cpp#18 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/04/14    rbhatnk      Created file.

========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"

#include "AudioDecSvc_CapiV1Wrapper.h"
#include "AudioDecSvc_CapiV1Util.h"

#include "Elite_CAPI.h"
#include "audio_basic_op.h"
#include <audio_basic_op_ext.h>
#include "adsp_asm_api.h"
#include "CVocoderCapiLib.h"
#include "CHpMp2DecoderLib.h"
#include "CADPCMDecoderLib.h"
#include "CAc3PacketizerLib.h"
#include "CeAc3PacketizerLib.h"
#include "CMatPacketizerLib.h"
#include "CDtmfGeneratorLib.h"
#include "CPassthruFormatterLib.h"
#include "CDtshdPacketizerLib.h"
#include "CDepacketizerLib.h"
#include "CDTS_BCDecLib.h"
#include "CDTS_LBRDecLib.h"

//secret hack: ideally the service shouldn't be aware of capi v1. however, for create we need to know.
//in order to hide details from the service structs, this layer is written.
//wrapper cannot do this because wrapper only complies to CAPI V2 APIs
#include "AudioDecSvc_CapiV1WrapperPrivate.h"

static void init_media_fmt(capi_v2_standard_data_format_t *std_media_fmt)
{
   std_media_fmt->bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->bitstream_format = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->data_interleaving = CAPI_V2_INVALID_INTERLEAVING;
   std_media_fmt->data_is_signed = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;

   for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
   {
      std_media_fmt->channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
   }
}

static ADSPResult validate_input(dec_CAPI_init_params_t *pCapiInitParams)
{
   //Since CAPI V1 only supports SISO (not MIMO), both input and output ports must be created at once.
   //If either is invalid, then error out.
   if (!pCapiInitParams->input_port_index.valid || !pCapiInitParams->output_port_index.valid)
   {
      return ADSP_EUNSUPPORTED;
   }
   if ( (pCapiInitParams->input_port_index.index >= AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_IN_PORTS) ||
         (pCapiInitParams->output_port_index.index >= AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_OUT_PORTS) )
   {
      return ADSP_EUNSUPPORTED;
   }
   return ADSP_EOK;
}

static ADSPResult allocate_memory(capi_v2_t **capi_v2_ptr_ptr)
{
   ADSPResult result = ADSP_EOK;
   *capi_v2_ptr_ptr = NULL;

   //Allocate memory for the wrapper
   capi_v2_proplist_t props_list;
   capi_v2_prop_t prop[1];

   capi_v2_init_memory_requirement_t init_mem;
   uint32_t i=0;
   prop[i].id = CAPI_V2_INIT_MEMORY_REQUIREMENT;
   prop[i].payload.actual_data_len = 0;
   prop[i].payload.max_data_len = sizeof(capi_v2_init_memory_requirement_t);
   prop[i].payload.data_ptr = (int8_t*)&init_mem;
   prop[i].port_info.is_valid = false;
   i++;

   props_list.prop_ptr = prop;
   props_list.props_num = i;

   result = audio_dec_svc_capi_v1_wrapper_get_static_properties(NULL, &props_list); //only init_mem can be queried for CAPI v1 wrapper as other things need instance
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Get Static Properties error");
      return result;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"AudioDecSvc: Get Static Properties done with init_mem_req=%lu", init_mem.size_in_bytes);

   capi_v2_t *capi_v2_ptr = (capi_v2_t*)qurt_elite_memory_malloc(init_mem.size_in_bytes, QURT_ELITE_HEAP_DEFAULT);
   if (!capi_v2_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Insufficient memory to allocate to CapiV2 Module.It requires %lu bytes",init_mem.size_in_bytes);
      return ADSP_ENOMEMORY;
   }
   memset(capi_v2_ptr, 0, init_mem.size_in_bytes);

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: Module allocated for %lu bytes of memory at location 0x%p.",
         init_mem.size_in_bytes, capi_v2_ptr);

   *capi_v2_ptr_ptr = capi_v2_ptr;

   return result;
}

static ADSPResult audio_dec_svc_destroy_capi_v1_amdb(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      wrapper->capi_v1_ptr->~ICAPI();
      wrapper->capi_v1_ptr = NULL;
   }
   if (wrapper->amdb_capi_ptr)
   {
      adsp_amdb_module_handle_info_t module_handle_info;
      module_handle_info.interface_type = CAPI;
      module_handle_info.type = 0; // Ignored
      module_handle_info.id1 = 0; // Ignored
      module_handle_info.id2 = 0; // Ignored
      module_handle_info.h.capi_handle = wrapper->amdb_capi_ptr;
      module_handle_info.result = ADSP_EOK;
      adsp_amdb_release_handles(&module_handle_info, 1);
      wrapper->amdb_capi_ptr = NULL;
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

static ADSPResult audio_dec_svc_destroy_capi_v1(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      qurt_elite_memory_delete(wrapper->capi_v1_ptr,ICAPI);
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

static ADSPResult audio_dec_svc_destroy_capi_v1_voice(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      qurt_elite_memory_free(wrapper->capi_v1_ptr);
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

ADSPResult audio_dec_svc_create_init_auto_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_RAW_COMPRESSED;
   wrapper->out_data_fmt = CAPI_V2_FIXED_POINT;

   //CAPI presence in AMDB is already checked.
   //AMDB capi can only convert raw to PCM (cannot do packetization, depacketization, or conversion from DDP->DP etc.
   //this is because it only works on media fmt which alone is not sufficient to identify the other cases.
   adsp_amdb_capi_t *pCapi = (adsp_amdb_capi_t*)pCapiInitParams->amdb_handle;
   if (pCapi)
   {
      result = adsp_amdb_capi_new_f(pCapi, pCapiInitParams->uMediaFmt, pInitParams->io_param.bits_per_sample, &wrapper->capi_v1_ptr);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed in adsp_amdb_capi_new_f");
      }
      wrapper->amdb_capi_ptr = pCapi;
      pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1_amdb;
   }
   else
   {
      switch(pCapiInitParams->uMediaFmt)
      {
      case ASM_MEDIA_FMT_MP2:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CHpMp2DecoderLib, QURT_ELITE_HEAP_DEFAULT, pInitParams->io_param.bits_per_sample, result);
         break;
      }
      case ASM_MEDIA_FMT_ADPCM:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CADPCMDecoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }

      case ASM_MEDIA_FMT_EVRC_FS:
      case ASM_MEDIA_FMT_EVRCB_FS:
      case ASM_MEDIA_FMT_V13K_FS:
      case ASM_MEDIA_FMT_AMRNB_FS:
      case ASM_MEDIA_FMT_AMRWB_FS:
      case ASM_MEDIA_FMT_EVRCWB_FS:
      case ASM_MEDIA_FMT_G711_ALAW_FS:
      case ASM_MEDIA_FMT_G711_MLAW_FS:
      case ASM_MEDIA_FMT_FR_FS:
      {
         result = Voc_Create_Decoder_CAPI(&wrapper->capi_v1_ptr);
         if (result != ADSP_EOK)
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Failed to create voice decoder CAPI");
            return result; //return is necessary because of "For non-voice CAPI, arrive at this place only if.." found below
         }
         pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1_voice;
         break;
      }
      case ASM_MEDIA_FMT_DTMF:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CDtmfGeneratorLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      case ASM_MEDIA_FMT_DTS:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CDTS_BCDecoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      case ASM_MEDIA_FMT_DTS_LBR:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CDTS_LBRDecoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }

      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported CAPI V1 media fmt 0x%lx", pCapiInitParams->uMediaFmt);
         result = ADSP_EBADPARAM;
      }
      }
   }

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI. result %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memory allocation is successful but
      // the constructor fail. The constructor is responsible for
      // cleaning up its own memory. Since constructor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->media_fmt = pCapiInitParams->uMediaFmt;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      audio_dec_svc_capi_v2_media_fmt_t media_fmt;
      init_media_fmt(&media_fmt.std);

      media_fmt.main.format_header.data_format = CAPI_V2_FIXED_POINT;
      media_fmt.std.bits_per_sample = pInitParams->io_param.bits_per_sample;
      props[1].id = CAPI_V2_OUTPUT_MEDIA_FORMAT;
      props[1].payload.actual_data_len = props[1].payload.max_data_len = sizeof(media_fmt);
      props[1].payload.data_ptr = reinterpret_cast<int8_t*>(&media_fmt);
      props[1].port_info.is_valid = true;
      props[1].port_info.is_input_port = false;
      props[1].port_info.port_index = 0; //CAPI v1 only supports one port.

      init_proplist.props_num = 2;
      init_proplist.prop_ptr = props;

      result = audio_dec_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->dec_destroy_fn)
      {
         pCapiInitParams->dec_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->dec_destroy_fn = NULL;
      }
      else
      {
         if (*capi_v2_ptr_ptr)
         {
            qurt_elite_memory_free(*capi_v2_ptr_ptr);
            *capi_v2_ptr_ptr = NULL;
         }
      }
   }

   return result;
}

ADSPResult audio_dec_svc_create_init_depack_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   //doesn't depend on format id
   qurt_elite_memory_new(wrapper->capi_v1_ptr, CDepacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_IEC61937_PACKETIZED;
   wrapper->out_data_fmt = CAPI_V2_RAW_COMPRESSED;

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->media_fmt = pCapiInitParams->uMediaFmt;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample not required for depacketizer

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_dec_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->dec_destroy_fn)
      {
         pCapiInitParams->dec_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->dec_destroy_fn = NULL;
      }
   }


   return result;
}

ADSPResult audio_dec_svc_create_init_conv_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_RAW_COMPRESSED;
   wrapper->out_data_fmt = CAPI_V2_RAW_COMPRESSED;

   switch(pCapiInitParams->uMediaFmt)
   {
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported media fmt 0x%x\n in compressed playback mode (packetization)",
            (int)pCapiInitParams->uMediaFmt);
      result = ADSP_EBADPARAM;
   }

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->media_fmt = pCapiInitParams->uMediaFmt;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample need not be set on converter capi

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_dec_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Initialization error");
      }

      //set any specific parameters

      switch(pCapiInitParams->uMediaFmt)
      {
      default:
         result = ADSP_EBADPARAM;
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->dec_destroy_fn)
      {
         pCapiInitParams->dec_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->dec_destroy_fn = NULL;
      }
   }

   return result;
}

ADSPResult audio_dec_svc_create_init_passthru_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_IEC61937_PACKETIZED;
   wrapper->out_data_fmt = CAPI_V2_IEC61937_PACKETIZED;

   switch(pCapiInitParams->uMediaFmt)
   {
   case ASM_MEDIA_FMT_AC3:
   case ASM_MEDIA_FMT_EAC3:
   case ASM_MEDIA_FMT_MP3:
   case ASM_MEDIA_FMT_MP2:
   case ASM_MEDIA_FMT_DTS:
   case ASM_MEDIA_FMT_AAC_V2:
   case ASM_MEDIA_FMT_ATRAC:
   case ASM_MEDIA_FMT_MAT:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CPassthruFormatterLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported media fmt 0x%x\n in compressed playback mode (pass-through)",
            (int)pCapiInitParams->uMediaFmt);
      result = ADSP_EBADPARAM;
   }

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->media_fmt = pCapiInitParams->uMediaFmt;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample need not be set on pass through capi

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_dec_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->dec_destroy_fn)
      {
         pCapiInitParams->dec_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->dec_destroy_fn = NULL;
      }
   }


   return result;
}

ADSPResult audio_dec_svc_create_init_pack_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_dec_svc_capi_v1_wrapper_t *wrapper = (audio_dec_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->dec_destroy_fn = audio_dec_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_RAW_COMPRESSED;
   wrapper->out_data_fmt = CAPI_V2_IEC61937_PACKETIZED;

   //for compressed playback create packetizer instead of decoder
   switch(pCapiInitParams->uMediaFmt)
   {
   case ASM_MEDIA_FMT_AC3:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CAc3PacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_EAC3:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CeAc3PacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_DTS:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CDtshdPacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_MAT:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CMatPacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported media fmt 0x%x\n in compressed playback mode (packetization)",
            (int)pCapiInitParams->uMediaFmt);
      result = ADSP_EBADPARAM;
   }

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create decoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->media_fmt = pCapiInitParams->uMediaFmt;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample need not be set on packetizer capi

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_dec_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->dec_destroy_fn)
      {
         pCapiInitParams->dec_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->dec_destroy_fn = NULL;
      }
   }


   return result;
}

/**
 * this is CAPI V1 util (instead of CAPI V2 util) because once we have only CAPI V2, we can remove this and have this code directly in create_init functions.
 */
dec_AMDB_presence audio_dec_svc_get_amdb_presence(Dec_CAPI_Type type, uint32_t id1, uint32_t id2, void **amdb_capi_handle_ptr)
{
   ADSPResult result = ADSP_EOK;
   dec_AMDB_presence ret = DEC_AMDB_PRESENCE_NOT_PRESENT;
   *amdb_capi_handle_ptr = NULL;

   uint32_t amdb_type;

   switch(type)
   {
   case DEC_CAPI_TYPE_AUTO:
      amdb_type = AMDB_MODULE_TYPE_DECODER;
      break;
   case DEC_CAPI_TYPE_DEPACKETIZER: //depacketizer won't be in AMDB
      return ret;
   case DEC_CAPI_TYPE_CONVERTER:
      amdb_type = AMDB_MODULE_TYPE_CONVERTER;
      break;
   case DEC_CAPI_TYPE_PASS_THROUGH: //passthrough won't be in AMDB
      return ret;
   case DEC_CAPI_TYPE_PACKETIZER:
      amdb_type = AMDB_MODULE_TYPE_PACKETIZER;
      break;
   default:
      return ret;
   }

   //first check CAPI V2 (like PP)
   adsp_amdb_module_handle_info_t module_handle_info;
   module_handle_info.interface_type = CAPI_V2;
   module_handle_info.type = amdb_type;
   module_handle_info.id1 = id1;
   module_handle_info.id2 = id2;
   module_handle_info.h.capi_v2_handle = NULL;
   module_handle_info.result = ADSP_EFAILED;

   /*
    * Note: This call will block till all modules with 'preload = 0' are loaded by the AMDB. This loading
    * happens using a thread pool using threads of very low priority. This can cause the current thread
    * to be blocked because of a low priority thread. If this is not desired, a callback function
    * should be provided that can be used by the AMDB to signal when the modules are loaded. The current
    * thread can then handle other tasks in parallel.
    */
   adsp_amdb_get_modules_request(&module_handle_info, 1, NULL, NULL);

   result = module_handle_info.result;
   if (ADSP_SUCCEEDED(result))
   {
      switch(module_handle_info.interface_type)
      {
      case CAPI_V2:
         ret = DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2;
         *amdb_capi_handle_ptr = (void*)module_handle_info.h.capi_v2_handle;
         break;
      case CAPI:
         if (DEC_CAPI_TYPE_AUTO == type) // Only AUTO CAPIv1 modules are supported.
         {
            *amdb_capi_handle_ptr = (void*)module_handle_info.h.capi_handle;
            ret = DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1;
         }
         else
         {
            adsp_amdb_release_handles(&module_handle_info, 1);
         }
         break;
      case STUB:
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioDecSvc: module present as stub.");
            adsp_amdb_release_handles(&module_handle_info, 1);
            ret = DEC_AMDB_PRESENCE_PRESENT_AS_STUB;
            break;
         }
      default:
         adsp_amdb_release_handles(&module_handle_info, 1);
         break;
      }
   }

   return ret;
}
