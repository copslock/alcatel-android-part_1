
/*========================================================================

This file contains structure definitions for Audio Decoder Service.

Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_Structs.h#16 $

when       who     what, where, why
--------   ---     --------------------------   -----------------------------
12/16/09    SJ      Created file.

========================================================================== */
#ifndef AUDIODECSVC_STRUCT_H
#define AUDIODECSVC_STRUCT_H

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "AudioDecSvc.h"
#include "AudioStreamMgr.h"
#include "adsp_private_api_ext.h"
#include "Elite_CAPI_V2.h"
#include "AudioDecSvc_CapiV2CallbackHandler.h"
#include "Elite_CAPI_V2_private_params.h"
#include "capi_v2_adsp_error_code_converter.h"
#include "qurt_elite_adsppm_wrapper.h"

#define  AUD_DEC_CMD_SIG   (0x80000000)
#define  AUD_DEC_RESP_SIG  (0x20000000)

//Maximum buffer in circular buffer list
#define DEC_SVC_MAX_CIRC_BUFFER   5

//maximum number of input streams (input signal mask should be changed when this is changed)
#define DEC_SVC_MAX_INPUT_STREAMS   1

//maximum number of output streams (output signal mask should be changed when this is changed)
#define DEC_SVC_MAX_OUTPUT_STREAMS  1

//pDecQHandler changes according to changes here.
#define  AUD_DEC_INP_DATA_SIG_MASK    (0x08000000)
#define  AUD_DEC_OUT_DATA_SIG_MASK    (0x00800000)

#define AUD_DEC_INP_DATA_SIG_SHIFT     27 //27 for input (24,25,26 are left vacant now)
#define AUD_DEC_OUT_DATA_SIG_SHIFT     18 //18 for output (19,20,21,22,23 are vacant now)

#define AUD_DEC_INP_INDEX_TO_DATA_SIG(ind)    ( ((uint32_t)0x1) << (AUD_DEC_INP_DATA_SIG_SHIFT+ind))
#define AUD_DEC_OUT_INDEX_TO_DATA_SIG(ind)    ( ((uint32_t)0x1) << (AUD_DEC_OUT_DATA_SIG_SHIFT+ind))

#define AUD_DEC_INP_SIG_BIT_IND_TO_INDEX(sigBitInd) ( sigBitInd - AUD_DEC_INP_DATA_SIG_SHIFT )
#define AUD_DEC_OUT_SIG_BIT_IND_TO_INDEX(sigBitInd)  ( sigBitInd - AUD_DEC_OUT_DATA_SIG_SHIFT )

#define  PCM_FORMAT_MAX_NUM_CHANNEL  8
#define  PCM_FORMAT_MAX_SAMPLING_RATE_HZ  384000

#define SECTION(a) __attribute__((__section__(a)))

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */
static const uint32_t BITS_IN_BYTE = 8;
static const uint32_t BITS_PER_SAMPLE = 16;
static const uint32_t AUD_DEC_INTERLEAVED = 1;
static const uint32_t AUD_DEC_DEINTERLEAVED = 0;
static const int64_t  SAMPLES_PER_FRAME_X_1000000 = 1000000L;

/// Round a number to the nearest multiple of 4 towards the direction of infinity.
#define ROUNDUP_MULTIPLE4(x)    ( ( ( (x)+3)>>2 ) << 2 )

// maximum data messages in data queue. Must consider data buffers + EOS messages.
static const uint32_t MAX_INP_DATA_Q_ELEMENTS = 16;

// maximum number of commands expected ever in command queue.
static const uint32_t MAX_CMD_Q_ELEMENTS = 8;

// How many buffers in output buffer queue? May need to make this configurable...
static const uint32_t MAX_OUT_DATA_Q_ELEMENTS = 4;
// number of output buffers (default).
static const uint32_t DEFAULT_NUM_OUTPUT_BUFFERS = 2;
// number of output buffers for PCM decoder.
static const uint32_t DEFAULT_NUM_PCM_DEC_OUTPUT_BUFFERS = 1;
// number of output buffers with metadata. this is high because metadata is transferred from dec -> PP -> matrix.
// we need 2 buffers on dec (pcm and metadata), and a buffer each on PP and matrix.
static const uint32_t NUM_OUTPUT_BUFFERS_WITH_METADATA = 4;
// queue must be sized to be larger than max # of msgs, and a power of 2.
static const uint32_t BUF_Q_ELEMENTS_SIZE = 8;

static const uint32_t BYTES_PER_SAMPLE_TWO = 2;
static const uint32_t BYTES_PER_SAMPLE_FOUR = 4;

// number of Response queue elements.
static const uint32_t MAX_RESP_Q_ELEMENTS = 2;

// buffer size. Assumed that service knows what size buffer it needs to deliver.
// Based on the format the buffer size will be allocated using below constants
static const uint32_t AUD_DEC_DEFAULT_OUT_BUF_SIZE = 1024;

static const uint32_t AUD_DEC_PCM_OUT_BUF_SIZE = 840*4*2; // 840 * 4
// 840 is the least common multiple of 1,2,3,4,5,6,7,8.
// The factor 2 is chose to make sure that a reasonable amount
// of samples are delivered. A multiple of # of bytes * max number of
// channels
// now with run-time buffer creation  this allocation is freed as soon as media-fmt comes.
static const uint32_t MASK_32BYTE_ALIGNMENT = 0x1F;

#define PCM_DEC_KPPS_FACTOR    6 /* Number of transactions of pcm data(in->scratch, scratch->out)+2 */
#define PCM_DEC_MIN_KPPS    4000 /* Minimum KPPS for PCM decoder */

#define GENERIC_DEC_KPPS    13000

//No. of possible speaker positions
#define MAX_SPKR_POS             (16)

//buffer size in AFE-frames for regular latency
#define BUF_SIZE_REG_LATENCY   5

//max number of concurrent CAPIs
#define DEC_SVC_MAX_CAPI   3

/**
 * bandwidth required for single stream decode
 * values are so due to legacy implementation.
 *
 * assuming 48kHz sampling, 2 channels, 2 bytes per channel.
 * in bytes per second.
 */
#define SINGLE_STREAM_DECODED_BW  (48000 * 2 * 2)

#define PCM_STACK_SIZE 2048
#define MIN_AUDEC_THREAD_STACK_SIZE 5084 // minimum audio decoder thread stack size excluding decoder stack
#define STACK_SIZE_DEC_PROCESS 512   //decoder stack size excluding core process function stack

/** Event mask is useful in 2  cases
 * 1. when event is not per port
 * 2. when event is per port, but the event handling is done for all ports at once.
 *
 * It's not useful when event is not per port and event cannot be handled at once, because
 * it's not known when to clear the mask*/
#define AUD_DEC_SVC_EVENT__KPPS_MASK   (0x80000000)
#define AUD_DEC_SVC_EVENT__KPPS_SHIFT  (31)
#define AUD_DEC_SVC_EVENT__BW_MASK     (0x40000000)
#define AUD_DEC_SVC_EVENT__BW_SHIFT    (30)
#define AUD_DEC_SVC_EVENT__METADATA_AVAILABLE_MASK   (0x20000000)
#define AUD_DEC_SVC_EVENT__METADATA_AVAILABLE_SHIFT  (29)
#define AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK   (0x10000000)
#define AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_SHIFT  (28)
/* -----------------------------------------------------------------------
 ** Global definitions/forward declarations
 ** ----------------------------------------------------------------------- */
typedef enum Dec_CAPI_Type
{
   DEC_CAPI_TYPE_AUTO,
   DEC_CAPI_TYPE_DEPACKETIZER,
   DEC_CAPI_TYPE_CONVERTER,
   DEC_CAPI_TYPE_PASS_THROUGH,
   DEC_CAPI_TYPE_PACKETIZER,
} Dec_CAPI_Type;

typedef struct AudioDecSvc_t AudioDecSvc_t;
typedef struct AudioDecSvc_InpStream_t AudioDecSvc_InpStream_t;

struct DecTimeStampState_t
{
   uint64_t        ullInbufTS;         /**< Time stamp of the input buffer that we
                                            are currently holding on to */
   bool_t          bInbufTSValid;      /**< Time stamp valid flag of the input buffer
                                            that we are currently holding on to */
   uint64_t        ullNextOutBufTS;    /**< Time stamp of the next output buffer
                                            to be sent out to PP */
   uint64_t        ullNextOutBufSampleCount;
   /**< Sample count corresponding to ullNextOutBufTS
    *   this value resets to zero every time
    *   AudioDecSvc_SyncToInputTS is called.*/
   uint64_t        ullLastSyncedInBufTS;
   /**< TS of the input buffer that was last sync'ed in
    * AudioDecSvc_SyncToInputTS*/
   bool_t          bNextOutbufTSValid; /**< Time stamp valid flag of the next
                                            output buffer to be sent out to PP */
   bool_t          bNextOutbufTsOld;   /**< This flag indicates that the TS used for
                                            next output buffer is based on the last
                                            input buffer, not the current one */
} ;

struct silence_removal_t
{
   uint32_t        initial_samples_to_remove;      /**< Number of silence samples to remove
                                                         at the beginning of file*/
   uint32_t        trailing_samples_to_remove;     /**< Number of silence samples to remove
                                                         at the end of file*/
   bool_t          last_buffer;                    /**< Flag indicating if the current client buffer is the last buffer*/

} ;

struct DecChannelMap_t
{
   uint32_t  nChannels;                 /** Number of channels mapped */
   uint8_t   nChannelMap[CAPI_V2_MAX_CHANNELS];          /** channel positions mapped */
};

struct DecPcmFmt_t
{
   uint32_t                             ulPCMSampleRate;
   uint16_t                             usBitsPerSample;
   uint16_t                             usIsInterleaved;
   uint32_t                             usBytesPerSample;
   DecChannelMap_t                      chan_map;
} ;

/** Describes the input channels that get routed to a given
 *  output channel */
struct dec_in_chan_map_t
{
   //# of input channels that route to this output channel
   uint32_t                             num_in_chan;
   //list of input channel positions that route to this output channel
   uint8_t                              in_chan[PCM_FORMAT_MAX_NUM_CHANNEL];
} ;

struct dec_out_chan_map_t
{
   //client given output channel mapping
   DecChannelMap_t                      max_map;
   //# of output channels actually going to downstream svc
   uint32_t                             num_out_chan;
   //list of output channel positions
   uint8_t                              out_chan[PCM_FORMAT_MAX_NUM_CHANNEL];
   //details of input channels routed to each of the output channels above
   dec_in_chan_map_t                    in_chan_map[PCM_FORMAT_MAX_NUM_CHANNEL];
   //flag to indicate whether downmixing of input is required
   bool_t                               downmix_required;
   //flag to indicate whether output chan map command was received.
   bool_t                               is_out_chan_map_received;
} ;

/** metadata transfer related */
struct dec_metadata_xfer_t
{
   uint8_t                              is_waiting_to_push_metadata; /**< after waking up from wait, whether decoder is waiting to push meta-data down.
                                                                           true after an attempt to send meta-data but out buf was not available. */
   uint8_t                              need_to_send_eos;            /**< whether EoS has to be sent after a process call */
   uint32_t                             curr_wait_mask_backup;       /**< back up copy of the mask to be restored after sending meta-data */
};
/*decoder error event related*/
struct dec_err_event_t
{
   uint64_t                             errorTime;
   //flag to enable/disable  DEC_ERROR notification event
   bool_t                               bEnableDecErrEvent;
   //Number of consecutive errors from decoder process.
   uint16_t                             nConsecutiveDecErr;
   //Maximum number of consecutive errors before raising event, if enabled
   uint16_t                             nThresholdDecError;
   //Maximum miliseconds of timeout
   uint16_t                             msThresholdTimeout;
};

typedef ADSPResult (*dec_capi_destroy_fn)(capi_v2_t **capi_ptr_ptr);

/** parameters applicable to the whole decoder svc. */
typedef struct dec_init_time_get_params_t
{
   uint32_t max_stack_size;
} dec_init_time_get_params_t;

typedef struct dec_capi_port_index_t
{
   uint16_t index;
   bool_t   valid;

} dec_capi_port_index_t;
/** parameters applicable to the each CAPI. */
typedef struct dec_CAPI_init_time_get_params_t
{
   // ------ input
   dec_capi_port_index_t input_port_index;
   dec_capi_port_index_t output_port_index;

   // ------ output
   capi_v2_port_data_threshold_t in_port_thresh;
   capi_v2_port_data_threshold_t out_port_thresh;
   capi_v2_stack_size_t stack_size;
   capi_v2_max_metadata_size_t metadata_size;
   capi_v2_output_media_format_size_t out_med_fmt_size;

} dec_CAPI_init_time_get_params_t;

typedef enum dec_CAPI_V2_presence
{
   DEC_CAPI_V2_PRESENCE_UNKNOWN=0,
   DEC_CAPI_V2_PRESENCE_NOT_PRESENT,
   DEC_CAPI_V2_PRESENCE_PRESENT
}dec_CAPI_V1_presence;

typedef enum dec_AMDB_presence
{
   DEC_AMDB_PRESENCE_NOT_PRESENT = 0,
   DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1,
   DEC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2,
   DEC_AMDB_PRESENCE_PRESENT_AS_STUB,

}dec_AMDB_presence;

typedef struct dec_CAPI_init_params_t
{
   // ------ input
   uint32_t uMediaFmt;
   uint32_t id2; //valid for converter case only. zero for other cases.
   capi_v2_event_callback_info_t cb_info;
   dec_capi_port_index_t input_port_index;
   dec_capi_port_index_t output_port_index;
   void *amdb_handle;

   // ------ output
   dec_capi_destroy_fn  dec_destroy_fn;
   dec_CAPI_V2_presence  capi_v2_presence;

} dec_CAPI_init_params_t;

typedef struct dec_pull_mode_t dec_pull_mode_t;

struct dec_CAPI_container_t
{
   capi_v2_t                           *capi_ptr;
   capi_v2_buf_t                       in_buf[DEC_SVC_MAX_INPUT_STREAMS];
   capi_v2_buf_t                       out_buf[DEC_SVC_MAX_OUTPUT_STREAMS];

   capi_v2_stream_data_t               inputs[DEC_SVC_MAX_INPUT_STREAMS];
   capi_v2_stream_data_t               outputs[DEC_SVC_MAX_OUTPUT_STREAMS];

   capi_v2_stream_data_t               *inputs_ptr[DEC_SVC_MAX_INPUT_STREAMS];
   capi_v2_stream_data_t               *outputs_ptr[DEC_SVC_MAX_OUTPUT_STREAMS];

   int32_t                             bytes_logged;        //num of bytes from current CAPI buffer that are already logged.
   uint32_t                            dec_fmt_id;          //format id:
   void                                *scratch_out_buf;    //scratch output buffer: is allocated as one buf for all CAPI outputs & is of max out size.
   uint32_t                            scratch_buf_size; //could be diff from maxExtBufSize as this is max of out sizes
   elite_multi_channel_pcm_fmt_blk_t   PrevFmtBlk[DEC_SVC_MAX_OUTPUT_STREAMS];       //store the previous format blk for each output stream
   dec_CAPI_callback_obj_t             capi_callback_obj;
   dec_capi_destroy_fn                 dec_destroy_fn;
   uint32_t                            kpps; //total kpps of this capi for all its ports. needed to check for change.
   uint32_t                            code_bw;
   uint32_t                            data_bw;

   uint32_t                            in_port_event_new_size[DEC_SVC_MAX_INPUT_STREAMS];
   uint32_t                            out_port_event_new_size[DEC_SVC_MAX_OUTPUT_STREAMS];
   bool_t                              metadata_available_event[DEC_SVC_MAX_OUTPUT_STREAMS];
   bool_t                              media_fmt_event[DEC_SVC_MAX_OUTPUT_STREAMS];
};

struct AudioDecSvc_CommonStream_t
{
   elite_svc_handle_t                     serviceHandle;
   uint16_t                               stream_type;  //primary/secondary etc
   uint8_t                                stream_id; //0 is invalid
   AudioDecSvcIOStreamType                io_type;

   AudioStreamMgr_CallBackHandleType      CallbackHandle;

   volatile uint32_t                    *delay_ptr;         //delay ptr shared by ASM. for input streams delay will be zero.
   uint32_t                             ulDataLogId;       // data logging id: <12 bit int. apr port id ><12 bit seq num><8 bit for flush etc>
   uint16_t                             perf_mode;        /**< performance mode in which the decoder is opened
                                                             Supported values are ASM_LEGACY_STREAM_SESSION &
                                                             ASM_LOW_LATENCY_STREAM_SESSION. This mode is
                                                             currently only applicable to PCM playback */
};

/* Rules
 * if readI is equals to writeI ; there is no valid buffer in list
 * if readI is not equals to writeI ; buffer presents at readI will swapped with decoder output buffer and readI will be circularly increamented
 * if process is to be called and no decoder output buffer is available then use buffer present at writeI in list and circularly increament the writeI
 *after increamenting the writeI if writeI is equals to readI then increament the readI (dropping the oldest buffer)
 */
struct Circular_Buffer_List
{
   elite_msg_data_buffer_t    *buflist[DEC_SVC_MAX_CIRC_BUFFER];
   uint8_t                    numBufs;
   uint8_t                    readI;
   uint8_t                    writeI;
};

struct AudioDecSvc_InpStream_t
{
   AudioDecSvc_CommonStream_t           common;              //keep this on top so that handle will be the first element.

   elite_msg_any_t                      inpDataQMsg;

   elite_mem_shared_memory_map_t        inp_buf_params;
   elite_mem_shared_memory_map_t        inp_buf_params_backup;  /* Backup inp_buf_params for cache invalidation in FreeInputDataCmd */

   bool_t                               bEndOfFrame;
   bool_t                               bEndOfStream;
   //flag to enable/disable  SR/CM change notification event
   bool_t                               bEnableSrCmEvent;
   //info about decoder error event
   dec_err_event_t                      dec_err_event;
   bool_t                               is_frame_rate_set;

   DecPcmFmt_t                          pcmFmt;    //PCM CAPI does not take #channel or sampling rate as inputs

   bool_t                               WasPrevDecResNeedMore;

   // saving this information here in service
   DecTimeStampState_t                  TsState; /**< time stamp state structure */
   silence_removal_t                    silence_removal; /**< silence removal state structure*/

   uint32_t                             buf_recv_cnt;    // write buffer recvd count
   uint32_t                             buf_done_cnt;    // write buffer done count
   elite_msg_data_eos_info_t            DtmfEndToneInfo;   //store the client and src address to send the tone ended event
   dec_pull_mode_t                      *pull_mode_ptr; //pull_mode related parameters
   bool_t                               no_apr_dec;    /** indicates that this decoder doesn't have an APR client but an internal client to provide input data */
   eDecoderIOFormatConvType             io_fmt_conv;
};

struct AudioDecSvc_OutStream_t
{
   AudioDecSvc_CommonStream_t           common;

   qurt_elite_queue_t                   *pOutBufQ;
   Circular_Buffer_List                 CirBufList;
   int                                  nBufsAllocated;         /**< Keep track of number of output bufs in circulation */
   uint32_t                             maxExtBufSize;          /**< ext buf or out buf. (could be diff from scratch_buf_size) */
   elite_svc_handle_t                   *pDownStreamSvc;        /**< down stream peer service handle */

   qurt_elite_bufmgr_node_t             outDataBufferNode;

   uint32_t                             decoderConverterMode;   /**< decoder converter mode
                                                                     0x1- ddp converter
                                                                     0x2-0xF: reserved for future use */
   dec_out_chan_map_t                   out_chan_map;

   uint16_t                             output_bits_per_sample; /**< bits per sample as received from ASM Read/Write open CMDs */
   uint32_t                             output_bytes_per_sample;

   dec_metadata_xfer_t                  metadata_xfr;           /**< helper struct for metadata transfer */
};

//TODO: error check to make sure when multi-capi is used, only one input stream possible currently.
//TODO: whenever MIMO dec is opened, check whether the fmt is compatible with existing fmt_ids
//TODO: there can be only one MIMO dec & it has to be the last CAPI: error check
//TODO: if capi chaining with MIMO dec, then it can support only one input stream.

struct AudioDecSvc_t
{
   AudioDecSvc_InpStream_t              *in_streams_ptr[DEC_SVC_MAX_INPUT_STREAMS];   //even in default mode (SISO), 2 streams are assumed, however, both have same stream_id.
   AudioDecSvc_OutStream_t              *out_streams_ptr[DEC_SVC_MAX_OUTPUT_STREAMS]; //and in such cases more than 2 streams are assumed to be not created.

   qurt_elite_thread_t                  thread_id;
   qurt_elite_queue_t                   *pCmdQ;
   qurt_elite_queue_t                   *pRespQ;        //Response queue

   dec_CAPI_container_t                 *capiContainer[DEC_SVC_MAX_CAPI];
   dec_CAPI_container_t                 *mainCapiContainer;  //TODO: Currently set-param doesn't have a way of knowing which of the multiple CAPIs the it's supposed to be set-to,
   //in future, we need a way to identify the decoder on which it has to be set.
   //Until then set-enc-dec will be on main CAPI container.
   //similarly, other features like metadata transfer,
   qurt_elite_channel_t                 channel;             //hold MQ's owned by this instance
   uint32_t                             unCurrentBitfield;
   elite_msg_any_t                      cmdMsg;
   qurt_elite_queue_t                   *pEventQ;   // Messages will be queued here to raise asynchronous events with the static service.
   uint32_t                             unPreviousBitfield; //useful to handle connect->pause->suspend->flush->run & its combinations
   bool_t                               isPrevBitfieldValid;
   bool_t                               bIsSuspended;       // flag to confirm whether decoder is in suspend state

   bool_t                               need_to_send_eos;   //need to send eos to all connected down-stream services

   uint32_t                             prev_kpps_vote;     //total KPPS of the decoder.
   uint32_t                             svc_bw;             //BW from decoder svc.
   uint32_t                             prev_bw_vote;       //previous bw vote inclu capi vote

   eDecoderMode                         ulMode;
   uint32_t                             event_mask;   //event mask indicating which event was raised from CAPI (see description above AUD_DEC_SVC_EVENT__KPPS_MASK)

   uint32_t                             ulAdsppmClientId;
   qurt_elite_adsppm_client_t           *adsppmClientPtr;

};

static inline void AudioDecSvc_SetPrevBitField(AudioDecSvc_t *pMe)
{
   pMe->isPrevBitfieldValid = true;
   pMe->unPreviousBitfield = pMe->unCurrentBitfield;
}


static inline bool_t AudioDecSvc_IsPcmFmt(uint32_t fmt_id)
{
   return ( (ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2 == fmt_id) ||
         (ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3 == fmt_id) );
}

ADSPResult AudioDecSvc_ProcessPCM(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInStrm, AudioDecSvc_OutStream_t *pOutStrm,
      uint8_t **ppucIn, uint32_t *pRemBytesInSrcBuf,
      uint32_t *numBytesProcessed, bool_t isPackedChSpacing, uint32_t usableOutBufSize,bool_t isInvalidateCache);
ADSPResult AudioDecSvc_FinishSuspend(AudioDecSvc_t* pMe);

bool_t AudioDecSvc_IsReallocateExternalBuffer(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm, ADSPResult *result);

//Returns primary or primary_secondary stream in mimo case otherwise first input stream.
static inline AudioDecSvc_InpStream_t *AudioDecSvc_GetDefaultInputStream(AudioDecSvc_t *pMe)
{
   for(uint8_t ii =0 ; ii < DEC_SVC_MAX_INPUT_STREAMS ; ii++)
   {
      if(NULL != pMe->in_streams_ptr[ii])
      {
         if(ASM_STREAM_TYPE_SECONDARY != pMe->in_streams_ptr[ii]->common.stream_type) return pMe->in_streams_ptr[ii];
      }
   }
   return pMe->in_streams_ptr[0];
}

static inline AudioDecSvc_OutStream_t *AudioDecSvc_GetDefaultOutputStream(AudioDecSvc_t *pMe)
{
   return pMe->out_streams_ptr[0];
}

static inline uint8_t AudioDecSvc_GetInputStreamIndex(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   for(uint8_t ii =0 ; ii < DEC_SVC_MAX_INPUT_STREAMS ; ii++)
   {
      if(pInpStrm == pMe->in_streams_ptr[ii])
      {
         return ii;
      }
   }
   return 0;
}

static inline uint8_t AudioDecSvc_GetOutputStreamIndex(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   for(uint8_t ii =0 ; ii < DEC_SVC_MAX_OUTPUT_STREAMS ; ii++)
   {
      if(pOutStrm == pMe->out_streams_ptr[ii])
      {
         return ii;
      }
   }
   return 0;
}

static inline uint32_t AudioDecSvc_AddBufDelayAndGetClientToken(AudioDecSvc_OutStream_t *pOutStrm, uint32_t buf_size,
      uint32_t sample_rate, uint32_t num_channels, uint32_t bytes_per_sample)
{
   uint32_t bytes_p_sec = (sample_rate * num_channels * bytes_per_sample);

   if (bytes_p_sec == 0)
   {
      return 0;
   }

   uint32_t delay = ((uint64_t)buf_size*1000000L)/(bytes_p_sec);

   *pOutStrm->common.delay_ptr += delay; //add delay when the buf goes down & also store as client token.

   uint32_t client_token = delay;

   return client_token;

}

static inline void AudioDecSvc_SubtractBufDelayUsingClientToken(AudioDecSvc_OutStream_t *pOutStrm, uint32_t *client_token_ptr)
{
   uint32_t delay = *client_token_ptr;

   *pOutStrm->common.delay_ptr = (uint32_t) ((int64_t)(*pOutStrm->common.delay_ptr) - (int64_t)delay);

   *client_token_ptr = 0;
}

int AudioDecSvc_ConnectToService(AudioDecSvc_t* pMe);
int AudioDecSvc_DisconnectFromService(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_ReturnUnsupported (AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_Flush(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_Run(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_Pause(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_StartSuspend(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_SetParam(AudioDecSvc_t* pMe);
ADSPResult AudioDecSvc_DtmfHandler(AudioDecSvc_t *pMe);
void AudioDecSvc_SendDtmfToneEndedEvent(AudioDecSvc_t *pMe);

ADSPResult AudioDecSvc_ProcessInputDataQ(AudioDecSvc_t* pMe, uint32_t channelBitIndex);
ADSPResult AudioDecSvc_ProcessOutputDataQ(AudioDecSvc_t* pMe, uint32_t channelBitIndex);

static inline uint32_t AudioDecSvc_GetPcmOutBufSize(uint16_t perf_mode, uint32_t sample_rate, uint16_t bytes_per_sample, uint16_t num_ch)
{
   //Calculate size based on the current Format Sample Rate

   uint32_t unAfeFrameSizeInSamples;
   elite_svc_get_frame_size(sample_rate,&unAfeFrameSizeInSamples);
   uint32_t unDecOutBufSizeBytes = bytes_per_sample * num_ch * (unAfeFrameSizeInSamples);
   unDecOutBufSizeBytes = (ASM_LEGACY_STREAM_SESSION == perf_mode)? (BUF_SIZE_REG_LATENCY*unDecOutBufSizeBytes) : unDecOutBufSizeBytes; //5 AFE-frames for regular latency
   return unDecOutBufSizeBytes;
}
#ifdef ELITE_CAPI_H
#error "Do not include CAPI V1 in this file"
#endif

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef AUDCMNUTIL_H
