#ifndef ADSP_AMDB_H
#define ADSP_AMDB_H
/*==============================================================================

Copyright (c) 2012-2013 Qualcomm Technologies, Incorporated.  All Rights Reserved. 
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include "Elite_APPI.h"
#include "Elite_CAPI.h"
#include "Elite_CAPI_new.h"
#include "Elite_CAPI_V2.h"

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

#include "adsp_amdb.h"

/******************************************************************************* 
 ADSP Audio Module DataBase (adsp_amdb) API Overview

 AMDB is a repository for APPI and CAPI based audio modules.  It allows both
 static and dynamic modules to be added, queried, and removed.
 
 Modules added to the database are tracked using numeric reference counting.
 The reference is incremented by one when the module is added to the database
 and for each time the module is successfully queried.  upon a successful query
 a pointer to the module is returned, the reference count can be incremented or
 decremented using the addref and release functions.  When no longer needed the
 reference count added when the module is queried must be released.
 
 For example:
 
 adsp_amdb_add_appi - sets ref count to 1
 adsp_amdb_get_appi() - increments reference count and returns pointer to module
 adsp_amdb_appi_addref - increments ref count
 adsp_amdb_appi_release - decrements ref count
 adsp_amdb_appi_release - decrements ref count
 adsp_amdb_remove_appi - decreement ref count, count is no zero and module is
                         freed
 
 As long as a module is being used a reference count must be non-zero.
 
 Once a module is queried, that module can be used to create instances of
 the APPI or CAPI objects.  The modules reference count is incremented each time
 an APPI or CAPI object is created and decremented when the APPI or CAPI End()
 method is called.
 
 To create an APPI object:
 
   call adsp_amdb_appi_getsize_f()
   allocate memory
   call adsp_amdb_appi_init_f() and pass the pointer allocated
 
 To create a CAPI object:
 
   call adsp_amdb_capi_new_f()
 
 Once an APPI or CAPI object is created that interface's End() method must be
 called to decrement the reference count.  If it is not then that object will
 never be freed.
 
 When dealing with dynamic audio modules the module will only be unloaded when
 all objects using it are freed (ref count goes to zero).
 
*******************************************************************************/

/******************************************************************************* 
  init/deinit, operates on a global instance of adsp_amdb
*******************************************************************************/
ADSPResult adsp_amdb_init(void);
void adsp_amdb_deinit(void);

/******************************************************************************* 
  amdb modules types
*******************************************************************************/
typedef struct adsp_amdb_capi_t adsp_amdb_capi_t;
typedef struct adsp_amdb_appi_t adsp_amdb_appi_t;
typedef struct adsp_amdb_capi_v2_t adsp_amdb_capi_v2_t;
typedef struct adsp_amdb_stub_t adsp_amdb_stub_t;
/******************************************************************************* 
  creates new CAPI object
*******************************************************************************/
ADSPResult adsp_amdb_capi_new_f(adsp_amdb_capi_t* me, uint32_t format,
                                uint32_t bps, ICAPI** capi_ptr_ptr);

capi_v2_err_t adsp_amdb_capi_v2_get_static_properties_f(adsp_amdb_capi_v2_t* me,
                                                     capi_v2_proplist_t* init_set_properties,
                                                     capi_v2_proplist_t* static_properties);
capi_v2_err_t adsp_amdb_capi_v2_init_f(adsp_amdb_capi_v2_t* me,
                                    capi_v2_t* _pif,
                                    capi_v2_proplist_t* init_set_properties);

/******************************************************************************* 
  queries size and then initializes an APPI object
*******************************************************************************/
ADSPResult adsp_amdb_appi_getsize_f(adsp_amdb_appi_t* me,
                                    const appi_buf_t* params_ptr,
                                    uint32_t* size_ptr);
ADSPResult adsp_amdb_appi_init_f(adsp_amdb_appi_t* me, appi_t* appi_ptr,
                                 bool_t* is_inplace_ptr,
                                 const appi_format_t* in_format_ptr,
                                 appi_format_t* out_format_ptr, appi_buf_t* info_ptr);

/******************************************************************************* 
  Add a static module to the database
*******************************************************************************/
ADSPResult adsp_amdb_add_static_capi(uint32_t entry_type, int id, capi_getsize_f getsize_f,
                                     capi_ctor_f ctor_f);
ADSPResult adsp_amdb_add_static_capi_ex(uint32_t entry_type, int type, int id1, int id2,
                                        capi_getsize_f getsize_f,
                                        capi_ctor_f ctor_f);
ADSPResult adsp_amdb_add_static_capi_v2(uint32_t entry_type, int type, int id1, int id2,
                                        capi_v2_get_static_properties_f get_static_properties_f,
                                        capi_v2_init_f init_f);
ADSPResult adsp_amdb_add_static_appi(uint32_t entry_type, int id, appi_getsize_f getsize_f,
                                     appi_init_f init_f);
                                     
/******************************************************************************
 * Addition of special types of module entries for CAPIv2 modules
******************************************************************************/
// The entry type with the lowest value has the highest priority
static const uint32_t ADSP_AMDB_ENTRY_TYPE_LEGACY = 0x10; // Entries made using the old API.
static const uint32_t ADSP_AMDB_ENTRY_TYPE_CLIENT = 0x20; // Entries set by the client processor.
static const uint32_t ADSP_AMDB_ENTRY_TYPE_INIT   = 0x30; // Entries set during DSP boot.
static const uint32_t ADSP_AMDB_ENTRY_TYPE_STATIC = 0x40; // Entries set before DSP boot.

// All modules of a particular entry type can be disabled. This will prevent any future calls of adsp_amdb_get_modules_request
// from getting modules of this entry type.
void adsp_amdb_disable_entry_type(uint32_t entry_type);
void adsp_amdb_enable_entry_type(uint32_t entry_type);

// In the calls below, calls with the same client id are not thread-safe.

ADSPResult adsp_amdb_add_capi_with_tag(uint32_t entry_type, int type, int id1, int id2, boolean preload, const char* filename_str, const char* tag_str, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_add_capi_with_entry_type(uint32_t entry_type, int type, int id1, int id2,
      boolean preload, const char* filename_str, const char* getsize_str, const char* ctor_str, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_remove_capi_with_entry_type(uint32_t entry_type, int type, int id1, int id2);

ADSPResult adsp_amdb_add_appi_with_tag(uint32_t entry_type, int id, boolean preload, const char* filename_str, const char* tag_str, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_add_appi_with_entry_type(uint32_t entry_type, int id,
      boolean preload, const char* filename_str, const char* getsize_str, const char* init_str, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_remove_appi_with_entry_type(uint32_t entry_type, int id);

ADSPResult adsp_amdb_add_capi_v2_with_entry_type(uint32_t entry_type, int type, int id1, int id2,
      boolean preload, const char* filename_str, const char* tag_str, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_remove_capi_v2_with_entry_type(uint32_t entry_type, int type, int id1, int id2);

ADSPResult adsp_amdb_add_stub_with_entry_type(uint32_t entry_type, int type, int id1, int id2, ADSPResult *dynamic_loading_err_ptr = NULL, uint32_t client_id = 0);
ADSPResult adsp_amdb_remove_stub_with_entry_type(uint32_t entry_type, int type, int id1, int id2);
ADSPResult adsp_amdb_remove_with_entry_type(uint32_t entry_type, int type, int id1, int id2);

// This function returns the aggregate error code for all the module additions for this client since the last time this function was called.
// Per module error codes can be obtained by providing the dynamic_loading_err_ptr values in the add APIs.
ADSPResult adsp_amdb_wait_for_module_additions(uint32_t client_id = 0);

/***************************************************************************
 API for querying for module handles.
 ***************************************************************************/

// Interface type of the module present in the AMDB
enum adsp_amdb_interface_type
{
   APPI,
   CAPI,
   CAPI_V2,
   STUB,
   INVALID = 0x7FFFFFFF
};

// Handle information that is provided by the AMDB per module.
typedef struct adsp_amdb_module_handle_info_t
{
   adsp_amdb_interface_type interface_type; // To be filled by AMDB. AMDB will return the interface type of the module if it returns a valid handle.
   int type; // To be filled by the caller for CAPIv2 modules. Should be set to 0 for all other types.
   int id1; // To be filled by the caller for APPI, CAPI and CAPIv2 modules.
   int id2; // To be filled by the caller for CAPIv2 modules. Should be set to 0 for all other types.
   union adsp_amdb_handle_union
   {
      // To be filled by AMDB. Only one of the following values will be valid, based on the interface type.
      adsp_amdb_appi_t *appi_handle;
      adsp_amdb_capi_t *capi_handle;
      adsp_amdb_capi_v2_t *capi_v2_handle;
      adsp_amdb_stub_t *stub_handle;
   } h;
   ADSPResult result; // To be filled by AMDB. If result is not ADSP_EOK, the handle is not valid.
} adsp_amdb_module_handle_info_t;

// Function signature for the callback function that is called when all the module handles are ready.
typedef void (*adsp_amdb_get_modules_callback_f)(void *callback_context);

void adsp_amdb_get_modules_request(
      adsp_amdb_module_handle_info_t module_handle_info[], // An array of num_modules elements, for the modules whose handles are required.
      uint32_t num_modules, // Number of modules for which handles are being queried.
      adsp_amdb_get_modules_callback_f callback_function, // The callback function that is called when the handles are available for all modules. If NULL, this function will be blocking.
      void *callback_context); // Context to be passed to the callback function.

void adsp_amdb_release_handles(
      adsp_amdb_module_handle_info_t module_handle_info[], // Array of num_modules_elements, whose handles are to be released. If the handle value is NULL or if the 'result' value is not EOK, the corresponding entry will be ignored.
      uint32_t num_modules // Number of modules whose handles are to be released.
      );

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // ADSP_AMDB_H
