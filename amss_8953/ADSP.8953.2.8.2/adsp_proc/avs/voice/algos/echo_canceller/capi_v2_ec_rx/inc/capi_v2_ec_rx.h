#ifndef CAPI_V2_EC_RX_H_
#define CAPI_V2_EC_RX_H_

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "Elite_CAPI_V2.h"

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

/*========================================================================
$Header: //components/rel/avs.adsp/2.7/voice/algos/echo_canceller/capi_v2_ec_rx/inc/capi_v2_ec_rx.h#1 $

Edit History

when       who            what, where, why
--------   ---             -----------------------------------------------
3/25/2015  sabdagir       Created
==========================================================================*/

/*----------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/

capi_v2_err_t capi_v2_voice_ec_rx_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_voice_ec_rx_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);


#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPI_V2_EC_RX_H_ */
