/*
 * AFE_private_defs_i.h
 *
 *  Created on: Aug 11, 2013
 *      Author: rajkrish
 */

#ifndef AFE_PRIVATE_DEFS_I_H_
#define AFE_PRIVATE_DEFS_I_H_

#define AFE_PORT_GET_DRIFT_PARAM_AND_VERIFY              0xFFFF0000

#define AFE_MODULE_ID_DRIFT                              0xFFFF0001

#define AFE_PARAM_ID_AVT_DRFT                            0xFFFF0002

typedef struct afe_avt_drift_param_t  afe_avt_drift_param_t;

struct afe_avt_drift_param_t
{
   uint32_t frame_counter;

   int32_t drift_value;
};

#define AFE_PARAM_ID_VFR_DRFT                            0xFFFF0003

typedef struct afe_vfr_drift_param_t  afe_vfr_drift_param_t;

struct afe_vfr_drift_param_t
{
   uint32_t frame_counter[NUM_MAX_VFR_SRC];

   uint32_t drift_value[NUM_MAX_VFR_SRC];
};

#endif /* AFE_PRIVATE_DEFS_I_H_ */
