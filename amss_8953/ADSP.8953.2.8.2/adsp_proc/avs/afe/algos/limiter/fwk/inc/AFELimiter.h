/*========================================================================
 This file contains limiter high level apis

 Copyright (c) 2015 QUALCOMM Technologies, Inc. (QTI).  All Rights Reserved.
 QUALCOMM Proprietary.  Export of this technology or software is regulated
 by the U.S. Government, Diversion contrary to U.S. law prohibited.

 $Header: //components/rel/avs.adsp/2.7/afe/algos/limiter/fwk/inc/AFELimiter.h#1 $

 Edit History

 when       who     what, where, why
 --------   ---     -------------------------------------------------------
 8/7/2015   rv       Created
 ====================================================================== */
#ifndef _AFE_LIMITER_H_
#define _AFE_LIMITER_H_

/*==========================================================================
 File includes
 ========================================================================== */
#include "AFEInternal.h"

#ifdef __cplusplus
extern "C"
{
#endif /*__cplusplus*/

/*==========================================================================
 Function Defines
 ========================================================================== */

/* Initializes the limiter for the port.
 */
ADSPResult afe_port_init_limiter(afe_dev_port_t *pDevPort);

/* De-initializes the limiter for the port.
 */
ADSPResult afe_port_deinit_limiter(afe_dev_port_t *pDevPort);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif // _AFE_LIMITER_H_
