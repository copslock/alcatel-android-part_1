/*==============================================================================
$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/dma_manager/common/src/AFEDmaManager.cpp#5 $
$DateTime: 2015/11/23 06:08:02 $
$Author: pwbldsvc $
$Change: 9466407 $
$Revision: #5 $

FILE:     hal_dma_manager.cpp

DESCRIPTION: Common DMA Manager

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright 2013 Qualcomm Technologies, Inc. (QTI).
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/dma_manager/common/src/AFEDmaManager.cpp#5 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
7/25/13    RP      created

============================================================================*/

#include "qurt_elite.h"
#include "AFEInternal.h"
#include "AFEInterface.h"
#include "AFEDmaManager.h"
#include "AFEDmaManager_i.h"
#include "AFEHalDmaManager_i.h"
#include "AFEDeviceDriver.h"
#include "AFEHalDmaManagerCommon.h"
#include "AFESsrDrv.h"


/**
  initialize afe dma manager
      initialize the dma state data including the dma resource state and interrupt registeration

  @return
  aDSP error code

  @dependencies
  None.
 */
ADSPResult afe_dma_mgr_init(void)
{
	ADSPResult   result = ADSP_EOK;

   if(ADSP_EOK != (result = afe_dma_mgr_resource_state_init()))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe_dma_mgr_init is failed");
   }

   return result;
}

/**
  deinitialize afe dma manager
      free all the allocated resources.

  @return
  aDSP error code

  @dependencies
  None.
 */

ADSPResult afe_dma_mgr_deinit(void)
{
	ADSPResult   result = ADSP_EOK;

   if(ADSP_EOK != (result = afe_dma_mgr_resource_state_deinit()))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe_dma_mgr_deinit is failed");
   }

   return result;
}

/**
  get the allocated DMA index
  
  @param[in]  h_dma_ptr                Pointer to the DMA manger handle
  @param[in]  uint32_t *dma_index_ptr   Parameters to get  the DMA index  
  
  @return 
  aDSP error code
  
  @dependencies 
  None.
 */
uint32_t afe_dma_mgr_get_dma_index(dma_device_handle h_dma_ptr)
{
	dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

	if((NULL == dma_mgr_state_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   return dma_mgr_state_ptr->dma_idx;
}


/**
  @brief Register with DMA manager

  @param[in] h_dma_ptr pointer to DMA manager handle
  @param[in] open_params_ptr pointer to the parameter list DMA Manager registration

  @return  ADSP_EOK on success, an error code on error

*/
ADSPResult afe_dma_mgr_open(dma_device_handle *h_dma_ptr, dma_mgr_open_params_t *open_params_ptr)
{

	ADSPResult result = ADSP_EOK;

   if((NULL == h_dma_ptr) || (NULL == open_params_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle / Open Params Null");
      return ADSP_EFAILED;
   }

   switch(open_params_ptr->hardware_type)
   {
      case LPASS_HW_HDMI_INPUT:
      case LPASS_HW_HDMI_OUTPUT:
      case LPASS_HW_I2S:
      case LPASS_HW_PCM:
      case LPASS_HW_TDM:
      case LPASS_HW_HDMI14:   
         break;
      default:
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Incorrect hardware type");
         return ADSP_EUNSUPPORTED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)qurt_elite_memory_malloc(sizeof(dma_manager_state_t),
		   QURT_ELITE_HEAP_DEFAULT);
   if(NULL == dma_mgr_state_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error in allocating memory for DMA Manager state structure");
      return ADSP_ENORESOURCE;
   }
   memset(dma_mgr_state_ptr, 0, sizeof(dma_manager_state_t));

	//copy over the parameters to DMA state structure
   dma_mgr_state_ptr->hardware_type = open_params_ptr->hardware_type;
	dma_mgr_state_ptr->num_channels = open_params_ptr->num_channels;
	dma_mgr_state_ptr->sampling_rate = open_params_ptr->sample_rate;
	dma_mgr_state_ptr->int_samples_per_period = open_params_ptr->int_samples_per_period;
	dma_mgr_state_ptr->input_bytes_per_sample = open_params_ptr->input_bytes_per_sample;
	dma_mgr_state_ptr->output_bytes_per_sample = open_params_ptr->output_bytes_per_sample;
	dma_mgr_state_ptr->dma_read_func_ptr = open_params_ptr->dma_read_func_ptr;
   dma_mgr_state_ptr->dma_write_func_ptr = open_params_ptr->dma_write_func_ptr;
	dma_mgr_state_ptr->buff_mem_type = open_params_ptr->buff_mem_type;
	dma_mgr_state_ptr->watermark = open_params_ptr->watermark;
	dma_mgr_state_ptr->num_buffers = open_params_ptr->num_buffers;
   dma_mgr_state_ptr->dma_type = open_params_ptr->dma_type;
   dma_mgr_state_ptr->dma_dir = open_params_ptr->dma_dir;

   result = afe_dma_mgr_alloc_dma(dma_mgr_state_ptr);
   dma_mgr_state_ptr->ifconfig_dma_control = open_params_ptr->ifconfig_dma_control;
   dma_mgr_state_ptr->dma_int_cb = open_params_ptr->dma_int_cb;
   dma_mgr_state_ptr->afe_driver_context = open_params_ptr->afe_driver_context;

	if(ADSP_EOK != result)
	{
		goto __bailout;
	}
	if(ADSP_EOK != (result = afe_dma_manager_get_dma_buffer_size(dma_mgr_state_ptr)))
	{
		goto __bailout;
	}

	//allocate the DMA buffer and zero initialize it
	result = afe_dma_manager_alloc_dma_buffer(dma_mgr_state_ptr);
	memset(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr, 0, dma_mgr_state_ptr->dma_buffer_size);

	//flush the cache associated with the DMA memory
	result = qurt_elite_memorymap_cache_flush((uint32_t)dma_mgr_state_ptr->dma_buffer_virt_addr_ptr,
			dma_mgr_state_ptr->dma_buffer_size);
	if(ADSP_EOK != result)
	{
		MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to flush cache associated with DMA buffer");
		goto __bailout;
	}

	//SW starts reading from the beginning of DMA buffer
	dma_mgr_state_ptr->dma_buffer_next_avail_addr = dma_mgr_state_ptr->dma_buffer_virt_addr_ptr;

   //store the handle
   *h_dma_ptr = (dma_device_handle *)dma_mgr_state_ptr;

   return ADSP_EOK;

__bailout:
	//If allocated free the DMA buffer
	if(NULL != dma_mgr_state_ptr->dma_buffer_virt_addr_ptr)
	{
		if(DMA_MGR_DDR_BUFFER == dma_mgr_state_ptr->buff_mem_type)
		{
			qurt_elite_memory_aligned_free(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);
		}
		else
		{
			afe_dev_lpa_aligned_free(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);
		}
		dma_mgr_state_ptr->dma_buffer_virt_addr_ptr = NULL;
	}

	if(NULL != dma_mgr_state_ptr)
	{
		//free the DMA manager state
		qurt_elite_memory_free(dma_mgr_state_ptr);
		dma_mgr_state_ptr = NULL;
	}

	return result;
}


/**
  Deregisters DMA manager client

  @param[in]  h_dma_ptr                DMA manger handle

  @return
  aDSP error code

  @dependencies
  None.
 */
ADSPResult afe_dma_mgr_close(dma_device_handle h_dma_ptr)
{
	ADSPResult result = ADSP_EOK;
	dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

	if((NULL == dma_mgr_state_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   result = afe_dma_mgr_free_dma(dma_mgr_state_ptr);

	//If allocated free the DMA buffer
	if(NULL != dma_mgr_state_ptr->dma_buffer_virt_addr_ptr)
	{
		if(DMA_MGR_DDR_BUFFER == dma_mgr_state_ptr->buff_mem_type)
		{
			qurt_elite_memory_aligned_free(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);
		}
		else
		{
			afe_dev_lpa_aligned_free(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);
		}
		dma_mgr_state_ptr->dma_buffer_virt_addr_ptr = NULL;
	}

   //free the DMA manager state
	qurt_elite_memory_free(dma_mgr_state_ptr);
	dma_mgr_state_ptr = NULL;

   return result;
}


/**
  @brief Starts the DMA Engine

  @param[in] h_dma_ptr pointer to DMA manager handle
  @param[in] start_param_ptr pointer to the parameter list for setting up DMA

  @return  ADSP_EOK on success, an error code on error

*/
ADSPResult afe_dma_mgr_start(dma_device_handle h_dma_ptr)
{
   uint32_t wps_count;
   hal_dma_config_t dma_config;
   ADSPResult   result = ADSP_EOK;

   if(NULL == h_dma_ptr)
   {
	   MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DMA Manager handle / Open Params Null");
	   return ADSP_EFAILED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   //make sure DMA is disabled before enabling it
   if(ADSP_EOK != (result = afe_hal_dma_disable_dma_channel(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to disable dma channel [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
      return result;
   }

   //wps count is the number of 32 bit words per sample element (sample element = bytes per sample * number of channels)
   // if the number of 32 bits words per sample element is less than 1 Dword, then set it to 1. 
   wps_count = (dma_mgr_state_ptr->num_channels * dma_mgr_state_ptr->input_bytes_per_sample) >> 2;
   if(0 == wps_count)
   {
      wps_count = 1;
   }

   //init and fill the dma config args.
   memset(&dma_config, 0, sizeof(hal_dma_config_t));

   //calculate burst size depending on the period length.
   //Period count should be a multiple of burst size
    if(0 == (dma_mgr_state_ptr->period_count_in_word32 & DMA_BUFFER_MGR_16_BYTE_ALIGNMENT))
   {
	   dma_config.burst_size = 16;
   }
   else if(0 == (dma_mgr_state_ptr->period_count_in_word32 & DMA_BUFFER_MGR_8_BYTE_ALIGNMENT))
   {
	   dma_config.burst_size = 8;
   }
   else if(0 == (dma_mgr_state_ptr->period_count_in_word32 & DMA_BUFFER_MGR_4_BYTE_ALIGNMENT))
   {
	   dma_config.burst_size = 4;
   }
   else
   {
	   dma_config.burst_size = 1;
   }

   //configure the DMA channel
   dma_config.buffer_len = dma_mgr_state_ptr->dma_buffer_size/sizeof(uint32_t); //buffer length in terms of 32 bit words
   dma_config.buffer_start_addr = dma_mgr_state_ptr->dma_buffer_phy_addr_ptr;
   dma_config.dma_int_per_cnt = dma_mgr_state_ptr->period_count_in_word32;
   dma_config.wps_count = wps_count;
   dma_config.watermark = dma_mgr_state_ptr->watermark;
   dma_config.ifconfig_dma_control = dma_mgr_state_ptr->ifconfig_dma_control;

   //NOTE: if burst size is 1 or dma_int_per_cnt is smaller than dma fifo size, then the burst is supposed to be disabled.

   //config the DMA channel
   if(ADSP_EOK != (result = afe_hal_dma_config_dma_channel(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, &dma_config)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to config dma channel [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   } //clear only the DMA interrupt status
   else if(ADSP_EOK != (result = afe_hal_dma_clear_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to cliar dma interrupt [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   } //enable the interrupt
   else if(ADSP_EOK != (result = afe_hal_dma_enable_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
      {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to enable dma interrupt [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }//enable the DMA channel
   else if(ADSP_EOK != (result = afe_hal_dma_enable_dma_channel(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to enable dma channel [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }

   afe_ssr_update_lpass_dma_status(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, TRUE);
   
   return result;
}

/**
  De-initializes DMA engine

  @param[in]  h_dma_ptr                DMA manger handle

  @return
  aDSP error code

  @dependencies
  None.
 */
ADSPResult afe_dma_mgr_stop(dma_device_handle h_dma_ptr)
{
   ADSPResult   result = ADSP_EOK;

   if((NULL == h_dma_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   //disable the interrupt
   if(ADSP_EOK != (result = afe_hal_dma_disable_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to disable dma interrupt [dma type:%d, dir:%d, idx:%d]", 
             dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   } //clear the interrupt
   else if(ADSP_EOK != (result = afe_hal_dma_clear_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to clear dma interrupt [dma type:%d, dir:%d, idx:%d]", 
             dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);

   }//disable the DMA channel
   else if(ADSP_EOK != (result = afe_hal_dma_disable_dma_channel(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to disable dma channel [dma type:%d, dir:%d, idx:%d]", 
          dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }

   afe_ssr_update_lpass_dma_status(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, FALSE);
   
   return result;
}

/**
  disable and clear DMA interrupt

  @param[in]  h_dma_ptr                DMA manger handle

  @return
  aDSP error code

  @dependencies
  None.
 */
ADSPResult afe_dma_mgr_disable_clear_dma_int(dma_device_handle h_dma_ptr)
{
   ADSPResult   result = ADSP_EOK;

   if((NULL == h_dma_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   //disable the interrupt
   if(ADSP_EOK != (result = afe_hal_dma_disable_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to disable dma interrupt [dma type:%d, dir:%d, idx:%d]", 
             dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   } //clear the interrupt
   else if(ADSP_EOK != (result = afe_hal_dma_clear_dma_interrupt(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to clear dma interrupt [dma type:%d, dir:%d, idx:%d]", 
             dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }
   else if(LPASS_HW_DMA_SINK == dma_mgr_state_ptr->dma_dir)
   {
     /**<Below change has been done as a s/w workaround for h/w (digital codec) limitation
      * If i2s/DMA is stopped first, and codec Rx is still running, codec may read
      * the last non-zero sample in its buffer continuously, resulting in DC pulse
      * generation at codec output, which is perceived as pop noise when high->zero->high
      * transition happens in this DC pulse.
      *
      * Below workaround zero-fills the DMA FIFO, i2s data lines before i2s/DMA disable. This
      * ensures that codec is not stuck at reading non-zero value after DMA disable
      * */

     if(NULL != dma_mgr_state_ptr->dma_buffer_virt_addr_ptr)
     {
       /* Zero out the DMA buffer */
       memset(dma_mgr_state_ptr->dma_buffer_virt_addr_ptr, 0, dma_mgr_state_ptr->dma_buffer_size);

       /**<flush the cache associated with the DMA memory.*/
       if(ADSP_EOK != (result = qurt_elite_memorymap_cache_flush((uint32_t)dma_mgr_state_ptr->dma_buffer_virt_addr_ptr,
                                                 dma_mgr_state_ptr->dma_buffer_size)) )
       {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to flush cache associated with DMA buffer");
       }
       else
       {
         /**< Sleep for 625 us to let the DMA FIFO and i2s buffers drained out
          * For 8 KHz, FIFO watermark is set to 1 DWORD + 3 samples in i2s h/w
          * buffer.
          * Total worst case delay = 2+3 = 5 samples @ 16-bit 8 KHz = 625 us*/
         avtimer_drv_sleep(625);
       }
     }
   }
   return result;
}


/**
  Read from the DMA buffers

  @param[in]  h_dma_ptr                DMA manger handle
  @param[in]  buffer_ptr               Destination buffer pointer

  @return
  aDSP error code

  @dependencies
  None.
 */
ADSPResult afe_dma_mgr_read(dma_device_handle h_dma_ptr, uint8_t *buffer_ptr)
{
   ADSPResult result = ADSP_EOK;

   if((NULL == h_dma_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   uint32_t *curr_addr;
   
   // This is also need to move to driver specific functions.
   //read the current address that DMA Is writing to
   (void) afe_hal_dma_get_dma_curr_addr(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, (uint32_t *)&curr_addr);
   uint32_t *next_read_addr = dma_mgr_state_ptr->dma_buffer_next_avail_addr;

   //check to see if there is enough data in DMA buffer to read
   uint32_t hw_offset = (uint32_t)(curr_addr - dma_mgr_state_ptr->dma_buffer_phy_addr_ptr);
   uint32_t sw_offset = (uint32_t)(next_read_addr - dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);
   int32_t diff = hw_offset - sw_offset;
   uint32_t dma_buff_size_in_word32 = dma_mgr_state_ptr->dma_buffer_size >> 2;

   if(diff < 0)
   {
      diff += dma_buff_size_in_word32;
   }

   if((uint32_t)diff < dma_mgr_state_ptr->period_count_in_word32)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
         "Warning: Not enough 32 bit words available to read from DMA buffer, Available words = %lu",
         (uint32_t)diff);
      //set back the next available address to read
      next_read_addr += dma_mgr_state_ptr->period_count_in_word32;
      if(next_read_addr >= dma_mgr_state_ptr->dma_buffer_virt_addr_ptr + dma_buff_size_in_word32)
      {
         //wrap around
         next_read_addr = dma_mgr_state_ptr->dma_buffer_virt_addr_ptr;
      }
   }

   //invalidate cache before reading, function internally takes care of alignment to cache line boundary
   result = afe_dma_buff_cache_invalidate((uint32_t)next_read_addr,
                                    dma_mgr_state_ptr->period_count_in_word32 * sizeof(int32));

   //copy from DMA buffer to client buffer
   dma_mgr_state_ptr->dma_read_func_ptr(dma_mgr_state_ptr->afe_driver_context, next_read_addr, buffer_ptr);

   //move ahead the read pointer
   next_read_addr += dma_mgr_state_ptr->period_count_in_word32;
   if(next_read_addr >= dma_mgr_state_ptr->dma_buffer_virt_addr_ptr + dma_buff_size_in_word32)
   {
      //wrap around
      next_read_addr = dma_mgr_state_ptr->dma_buffer_virt_addr_ptr;
   }
   dma_mgr_state_ptr->dma_buffer_next_avail_addr = next_read_addr;

   return result;
}

ADSPResult afe_dma_mgr_write(dma_device_handle h_dma_ptr, uint8_t *buffer_ptr)
{
    ADSPResult result = ADSP_EOK;

   if((NULL == h_dma_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Pointer to DMA Manager handle Null");
      return ADSP_EFAILED;
   }

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   uint32_t dma_buff_size_in_word32 = dma_mgr_state_ptr->dma_buffer_size >> 2;
   uint32_t *next_write_addr = dma_mgr_state_ptr->dma_buffer_next_avail_addr;

   uint32_t *curr_addr;
   uint32_t hw_offset;
   uint32_t sw_offset;;
   int32_t diff;
   uint32_t tail_length;

   tail_length = dma_buff_size_in_word32 - (next_write_addr - dma_mgr_state_ptr->dma_buffer_virt_addr_ptr);

   if(tail_length >= dma_mgr_state_ptr->period_count_in_word32)
   {
      //copy from client buffer to DMA buffer
      if(dma_mgr_state_ptr->dma_write_func_ptr)
      {
         dma_mgr_state_ptr->dma_write_func_ptr(dma_mgr_state_ptr->afe_driver_context, buffer_ptr, next_write_addr);
      }

      //cache flush
      result = afe_dma_buff_cache_flush((uint32_t)next_write_addr,
                                       dma_mgr_state_ptr->period_count_in_word32 * sizeof(int32));

       ///< update the address for software processing next time.
      if (tail_length > dma_mgr_state_ptr->period_count_in_word32)
      {
         next_write_addr += dma_mgr_state_ptr->period_count_in_word32;
      }
      else
      {
         next_write_addr = dma_mgr_state_ptr->dma_buffer_virt_addr_ptr;
      }

      // This is also need to move to driver specific functions.
      (void) afe_hal_dma_get_dma_curr_addr(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, 
                                   (uint32_t *)&curr_addr);
          
      hw_offset  = curr_addr - dma_mgr_state_ptr->dma_buffer_phy_addr_ptr;
      sw_offset = next_write_addr - dma_mgr_state_ptr->dma_buffer_virt_addr_ptr;
      diff = sw_offset - hw_offset;
      if (diff<0)
      {
         diff = diff + dma_buff_size_in_word32;
      }

      if ((diff < (int32_t)dma_mgr_state_ptr->period_count_in_word32) && (0 != diff)) 
      {
         //pAfeDevAudioChInfo->dmaStateInfo.diag++;
         //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AFEDal: Warning Write diag [%d] \n", pAfeDevAudioChInfo->dmaStateInfo.diag);
         //MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AFEDal: curAddr[0x%x] nextAvlAddr [0x%x] \n", (uint32)currAddr,(uint32)nextAvlAddr);

         next_write_addr = next_write_addr + dma_mgr_state_ptr->period_count_in_word32 ;
         if ( next_write_addr >= dma_mgr_state_ptr->dma_buffer_virt_addr_ptr + dma_buff_size_in_word32)
         {
            next_write_addr -= dma_buff_size_in_word32;
         }
      }
   
      ///< Store next available addr for processing the subsequent iteration.
      dma_mgr_state_ptr->dma_buffer_next_avail_addr = next_write_addr;
   }

   return result;
}

ADSPResult afe_dma_mgr_read_timestamp_fifo_count(dma_device_handle h_dma_ptr, uint64_t *dma_int_timestamp_ptr, uint32_t *fifo_count_ptr)
{
   ADSPResult   result = ADSP_EOK;

   dma_manager_state_t *dma_mgr_state_ptr = (dma_manager_state_t *)h_dma_ptr;

   if(ADSP_EOK != (result = afe_hal_dma_get_dma_int_stc(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, \
             dma_int_timestamp_ptr)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to read STC [dma type:%d, dir:%d, idx:%d]", 
            dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }
   else if(ADSP_EOK != (result = afe_hal_dma_get_dma_fifo_count(dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx, \
            fifo_count_ptr)))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Dma Manager failed to read fifo count[dma type:%d, dir:%d, idx:%d]",
                     dma_mgr_state_ptr->dma_type, dma_mgr_state_ptr->dma_dir, dma_mgr_state_ptr->dma_idx);
   }
    
   return result;
}

