/*
  Copyright (C) 2014-2015 QUALCOMM Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/cvd_vfr/src/cvd_vfr_module.c#5 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include "msg.h"
#include "err.h"
#ifndef WINSIM
#include "Elite.h"
#include "qurt_elite.h"
#include "VoiceTimerMsgs.h"
#endif /* WINSIM */

#include "mmstd.h"
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"
#include "apr_log.h"
#include "aprv2_api_inline.h"
#include "aprv2_msg_if.h"
#include "vss_private_if.h"
#include "cvd_task.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

static char_t cvd_vfr_my_dns[] = "qcom.audio.cvdvfr";
  /**< The CVD VFR is a dynamic APR service that is used only within CVD. */
static uint16_t cvd_vfr_my_addr;
  /**< CVD VFR address is set at initialization. */

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define CVD_VFR_HEAP_SIZE_V ( 256 )

#define CVD_VFR_NUM_COMMANDS_V ( 10 )

#define CVD_VFR_HANDLE_TOTAL_BITS_V ( 16 )
#define CVD_VFR_HANDLE_INDEX_BITS_V ( 4 ) /* 4 bits = 16 handles. */

#define CVD_VFR_MAX_OBJECTS_V ( 1 << CVD_VFR_HANDLE_INDEX_BITS_V )

#define CVD_VFR_EXPIRY_MASK ( 1 )
#define CVD_VFR_STOP_MASK ( 2 )

#define CVD_VFR_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

typedef enum cvd_vfr_thread_state_enum_t
{
  CVD_VFR_THREAD_STATE_ENUM_INIT,
  CVD_VFR_THREAD_STATE_ENUM_READY,
  CVD_VFR_THREAD_STATE_ENUM_EXIT
}
  cvd_vfr_thread_state_enum_t;

/****************************************************************************
 * WORK QUEUE DEFINITIONS                                                   *
 ****************************************************************************/

typedef struct cvd_vfr_work_item_t
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
}
  cvd_vfr_work_item_t;

/****************************************************************************
 * CVD VFR HANDLE OBJECT DEFINITIONS                                        *
 ****************************************************************************/

typedef struct cvd_vfr_handle_object_t
{
  uint32_t handle;
  uint16_t client_addr;
  uint16_t client_port;
}
  cvd_vfr_handle_object_t;

/****************************************************************************
 * PENDING COMMAND CONTROL DEFINITIONS                                      *
 ****************************************************************************/

typedef enum cvd_vfr_pending_cmd_state_enum_t
{
  CVD_VFR_PENDING_CMD_STATE_ENUM_FETCH,
    /**< Fetch the next pending command to execute. */
  CVD_VFR_PENDING_CMD_STATE_ENUM_EXECUTE,
    /**< Execute the current pending command for the first time. */
  CVD_VFR_PENDING_CMD_STATE_ENUM_CONTINUE
    /**< Continue executing the current pending command. */
}
  cvd_vfr_pending_cmd_state_enum_t;

/**
 * The pending command control structure stores information used to process
 * pending commands serially.
 *
 * Pending commands have state and are executed one at a time until
 * completion. A gated pending command gates all the pending commands that
 * follows it. A gated pending command should employ abort timers to allow the
 * system to make progress.
 *
 * Aborting critical pending commands can leave the system in a bad state. It
 * is recommended that (at the very least) the configurations of the aborted
 * critical pending commands be saved so that on error recovery the proper
 * configurations could be restored.
 */
typedef struct cvd_vfr_pending_control_t
{
  apr_list_t cmd_q;
    /**< The pending (cvd_vfr_work_item_t) command queue. */
  cvd_vfr_pending_cmd_state_enum_t state;
    /**<
     * The current state of the pending command control.
     *
     * This variable is managed by the pending command processor. The
     * individual pending command controls indicates to the pending command
     * processor to complete or to delay the completion of the current
     * pending command.
     */
  aprv2_packet_t* packet;
    /**<
     * The current (command) packet being processed.
     */
}
  cvd_vfr_pending_control_t;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Lock Management */
static apr_lock_t cvd_vfr_int_lock;

/* Heap Management */
static uint8_t cvd_vfr_heap_pool[ CVD_VFR_HEAP_SIZE_V ];
static apr_memmgr_type cvd_vfr_heapmgr;

/* Object Management */
static apr_objmgr_object_t cvd_vfr_object_table[ CVD_VFR_MAX_OBJECTS_V ];
static apr_objmgr_t cvd_vfr_objmgr;

/* Command Queue Management */
static cvd_vfr_work_item_t cvd_vfr_cmd_pool[ CVD_VFR_NUM_COMMANDS_V ];
static apr_list_t cvd_vfr_free_cmd_q;

/* Task Management */
static apr_event_t cvd_vfr_task_event;
static apr_event_t cvd_vfr_thread_event;
static apr_thread_t cvd_vfr_task_handle;
static cvd_vfr_thread_state_enum_t cvd_vfr_task_state = CVD_VFR_THREAD_STATE_ENUM_INIT;
static uint8_t cvd_vfr_task_stack[ CVD_VFR_TASK_STACK_SIZE ];

/* Pending Command Management */
static cvd_vfr_pending_control_t cvd_vfr_pending_ctrl;

/* APR Resource */
static uint32_t cvd_vfr_apr_handle;

static uint32_t cvd_vfr_soft_vfr_client_count;
static uint64_t cvd_vfr_soft_vfr_ref_timestamp_us;

#ifndef WINSIM
static Vtm_SubUnsubMsg_t cvd_vfr_vtm_sub_unsub_msg;
static elite_msg_any_t cvd_vfr_vtm_sub_elite_msg;
static elite_msg_any_t cvd_vfr_vtm_unsub_elite_msg;
static qurt_elite_signal_t cvd_vfr_expiry_signal;
static qurt_elite_signal_t cvd_vfr_stop_signal;
static qurt_elite_channel_t cvd_vfr_elite_channel;
#endif /* WINSIM */

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void cvd_vfr_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvd_vfr_int_lock );
}

static void cvd_vfr_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvd_vfr_int_lock );
}

static void cvd_vfr_signal_run ( void )
{
  apr_event_signal( cvd_vfr_task_event );
}

static int32_t cvd_vfr_create_handle_object (
  cvd_vfr_handle_object_t** ret_handle_obj
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  cvd_vfr_handle_object_t* handle_obj;
  apr_objmgr_object_t* objmgr_obj;

  for ( ;; )
  {
    if ( ret_handle_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    { /* Allocate memory for the new handle object. */
      handle_obj = apr_memmgr_malloc( &cvd_vfr_heapmgr, 
                                      sizeof( cvd_vfr_handle_object_t ) );
      if ( handle_obj == NULL )
      {
        rc = APR_ENORESOURCE;
        break;
      }
      checkpoint = 1;
    }

    { /* Allocate a new handle for the region object. */
      rc = apr_objmgr_alloc_object( &cvd_vfr_objmgr, &objmgr_obj );
      if ( rc ) break;
      checkpoint = 2;
    }

    /* Link the handle object to the handle. */
    objmgr_obj->any.ptr = handle_obj;

    { /* Initialize the handle object. */
      handle_obj->handle = objmgr_obj->handle;
      handle_obj->client_addr = APRV2_PKT_INIT_ADDR_V;
      handle_obj->client_port = APR_NULL_V;
    }

    *ret_handle_obj = handle_obj;

    return APR_EOK;
  }
  
  switch ( checkpoint )
  {
  case 2:
    ( void ) apr_objmgr_free_object( &cvd_vfr_objmgr, objmgr_obj->handle );
    /*-fallthru */

  case 1:
    apr_memmgr_free( &cvd_vfr_heapmgr, handle_obj );
    /*-fallthru */

  default:
    break;
  }

  return rc;
}

static int32_t cvd_vfr_free_handle_object (
  cvd_vfr_handle_object_t* handle_obj
)
{
  if ( handle_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &cvd_vfr_objmgr, handle_obj->handle );
  apr_memmgr_free( &cvd_vfr_heapmgr, handle_obj );

  return APR_EOK;
}

static int32_t cvd_vfr_get_handle_object (
  uint32_t handle,
  cvd_vfr_handle_object_t** ret_handle_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;
  cvd_vfr_handle_object_t* handle_obj;

  for ( ;; )
  {
    if ( ret_handle_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    rc = apr_objmgr_find_object( &cvd_vfr_objmgr, handle, &objmgr_obj );
    if ( rc ) break;

    handle_obj = ( ( cvd_vfr_handle_object_t* ) objmgr_obj->any.ptr );
    if ( handle_obj == NULL )
    {
      rc = APR_EFAILED;
      break;
    }

    *ret_handle_obj = handle_obj;

    return APR_EOK;
  }

  return rc;
}

#ifndef WINSIM
static int32_t cvd_vfr_alloc_send_msg_to_vtm (
  elite_msg_any_t* elite_msg,
  uint32_t opcode
)
{
  int32_t rc;
  ADSPResult result;
  uint32_t size;
  EliteMsg_CustomVtSubUnsubHeaderType* payload;

  for ( ;; )
  {
    if ( qurt_elite_globalstate.pVoiceTimerCmdQ == NULL )
    {
#ifndef SIM_DEFINED
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_alloc_send_msg_to_vtm(): \
           qurt_elite_globalstate.pVoiceTimerCmdQ is NULL" );
#endif /* SIM_DEFINED */
      rc = APR_EFAILED;
      break;
    }

    size = sizeof( EliteMsg_CustomVtSubUnsubHeaderType );

    /* Create an elite message. The message payload ownership will be
     * transferred to VTM who will be responsible to free the payload.
     */
    result = elite_msg_create_msg(
               elite_msg, &size, ELITE_CUSTOM_MSG, NULL, 0, ADSP_EOK );
    if( ADSP_FAILED( result ) )
    {
#ifndef SIM_DEFINED
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_alloc_send_msg_to_vtm(): \
             Failed to create elite msg, result = 0x%08X", result );
#endif /* SIM_DEFINED */
      rc = APR_EFAILED;
      break;
    }

    payload = ( ( EliteMsg_CustomVtSubUnsubHeaderType* ) elite_msg->pPayload );
    payload->unSecOpCode = opcode;
    payload->vtm_sub_unsub_payload_ptr = &cvd_vfr_vtm_sub_unsub_msg;

    /* Note that according to VCP team, the VTM design is kept simple such that
     * VTM will not report any failure to any of the modules sub/unsub to VTM.
     * If there is any failure, voice call will break and such failure is not
     * recoverable (requires debugging). We rely on logs to debug any VTM
     * failure.
     */
    result = qurt_elite_queue_push_back(
               qurt_elite_globalstate.pVoiceTimerCmdQ, ( ( uint64* ) elite_msg ) );
    if ( ADSP_FAILED( result ) )
    {
#ifndef SIM_DEFINED
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_alloc_send_msg_to_vtm(): \
             Failed to send elite msg, result = 0x%08X", result );
#endif /* SIM_DEFINED */

      /* Free the payload buffer if failed to push message to VTM. */
      ( void ) elite_msg_return_payload_buffer( elite_msg );

      rc = APR_EFAILED;
      break;
    }

    return APR_EOK;
  }

  return rc;
}
#endif /* !WINSIM */

/****************************************************************************
 * PENDING COMMAND ROUTINES                                                 *
 ****************************************************************************/

static int32_t cvd_vfr_pending_control_init (
  cvd_vfr_pending_control_t* ctrl
)
{
  int32_t rc;

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2(
         &ctrl->cmd_q, cvd_vfr_int_lock_fn, cvd_vfr_int_unlock_fn );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVD_VFR_PENDING_CMD_STATE_ENUM_FETCH;
  ctrl->packet = NULL;

  return APR_EOK;
}

static int32_t cvd_vfr_pending_control_destroy (
  cvd_vfr_pending_control_t* ctrl
)
{
#if 0 /* There's nothing to destroy if everything is in a good state. */
  int32_t rc;
#endif /* 0 */

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

#if 0 /* There's nothing to destroy if everything is in a good state. */
  rc = apr_list_destroy( &ctrl->cmd_q );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVD_VFR_PENDING_CMD_STATE_ENUM_UNINITIALIZED;
  ctrl->packet = NULL;
#endif /* 0 */

  return APR_EOK;
}

/****************************************************************************
 * CVD VFR PENDING COMMAND ROUTINES                                        *
 ****************************************************************************/

static int32_t cvd_vfr_open_cmd_control (
  cvd_vfr_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_ivfr_rsp_open_t open_rsp;
  uint32_t payload_size;
  uint32_t checkpoint = 0;
  vss_ivfr_cmd_open_t* in_args;
  cvd_vfr_handle_object_t* handle_obj;

  for ( ;; )
  {
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
    if ( payload_size != sizeof( vss_ivfr_cmd_open_t ) )
    {
#ifndef SIM_DEFINED
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_open_cmd_control(): \
             Invalid payload size %d != valid size %d",
             payload_size, sizeof( vss_ivfr_cmd_open_t ) );
#endif /* SIM_DEFINED */
      rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVD_VFR_PANIC_ON_ERROR( rc );

      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ivfr_cmd_open_t, ctrl->packet );

    if ( in_args->mode != VSS_ICOMMON_VFR_MODE_SOFT )
    {
#ifndef SIM_DEFINED
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_open_cmd_control(): \
             Invalid vfr mode %d.", in_args->mode );
#endif /* SIM_DEFINED */
      rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVD_VFR_PANIC_ON_ERROR( rc );

      break;
    }

    rc = cvd_vfr_create_handle_object( &handle_obj );
    if ( rc )
    {
#ifndef SIM_DEFINED
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_open_cmd_control(): \
             Failed to create handle obj: result=0x%08X", rc );
#endif /* SIM_DEFINED */
      rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_ENORESOURCE );
      CVD_VFR_PANIC_ON_ERROR( rc );

      break;
    }
    checkpoint = 1;

    handle_obj->client_addr = ctrl->packet->src_addr;
    handle_obj->client_port = ctrl->packet->src_port;

    if ( cvd_vfr_soft_vfr_client_count == 0 )
    {
      /* Subscribe to the voice timer service if this is the first time that
       * any client is opening the VFR driver for soft VFR.
       */
#ifdef WINSIM
      cvd_vfr_soft_vfr_ref_timestamp_us = 0;
#else
      /* Clear residule signals if there is any. */
      qurt_elite_signal_clear( &cvd_vfr_expiry_signal );
      cvd_vfr_vtm_sub_unsub_msg.signal_enable = 1;
      cvd_vfr_vtm_sub_unsub_msg.avtimer_timestamp_us = 0;

      rc = cvd_vfr_alloc_send_msg_to_vtm(
             &cvd_vfr_vtm_sub_elite_msg, VOICE_TIMER_SUBSCRIBE );
      if ( rc )
      {
#ifndef SIM_DEFINED
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_open_cmd_control(): \
               Failed to subscribe to VTM, rc = 0x%08X", rc );
#endif /* SIM_DEFINED */
        rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, rc );
        CVD_VFR_PANIC_ON_ERROR( rc );

        break;
      }

      /* Wait for the soft VFR timer expiry signal. */
      ( void ) qurt_elite_channel_wait(
        &cvd_vfr_elite_channel, CVD_VFR_EXPIRY_MASK );

      cvd_vfr_vtm_sub_unsub_msg.signal_enable = 0;
      qurt_elite_signal_clear( &cvd_vfr_expiry_signal );

      /* Read the soft VFR timestamp populated by the voice timer. */
      cvd_vfr_soft_vfr_ref_timestamp_us = cvd_vfr_vtm_sub_unsub_msg.avtimer_timestamp_us;
#endif /* WINSIM */
    }

    cvd_vfr_soft_vfr_client_count++;

    open_rsp.ref_timestamp_us = cvd_vfr_soft_vfr_ref_timestamp_us;

    rc = __aprv2_cmd_alloc_send(
           cvd_vfr_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvd_vfr_my_addr, ( ( uint16_t ) handle_obj->handle ),
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_IVFR_RSP_OPEN,
           &open_rsp, sizeof( open_rsp ) );
    CVD_VFR_PANIC_ON_ERROR( rc );

    ( void ) __aprv2_cmd_free( cvd_vfr_apr_handle, ctrl->packet );

    return APR_EOK;
  }

  /* Clean up. */
  switch ( checkpoint )
  {
  case 1:
    {
      ( void ) cvd_vfr_free_handle_object( handle_obj );
    }
    /*-fallthru */

  default:
    break;
  }

  return APR_EOK;
}

static int32_t cvd_vfr_close_cmd_control (
  cvd_vfr_pending_control_t* ctrl
)
{
  int32_t rc;
  cvd_vfr_handle_object_t* handle_obj;

  for ( ;; )
  {
    rc = cvd_vfr_get_handle_object( ctrl->packet->dst_port, &handle_obj );
    if ( rc )
    {
#ifndef SIM_DEFINED
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_close_cmd_control(): \
             Invalid handle=0x%04X", ctrl->packet->dst_port );
#endif /* SIM_DEFINED */
      rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_EHANDLE );
      CVD_VFR_PANIC_ON_ERROR( rc );

      break;
    }

    if ( ( ctrl->packet->src_addr != handle_obj->client_addr ) ||
         ( ctrl->packet->src_port != handle_obj->client_port ) )
    {
#ifndef SIM_DEFINED
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_close_cmd_control(): \
             Invalid client address 0x%08X or port 0x%04X",
             ctrl->packet->src_addr, ctrl->packet->src_port );
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_close_cmd_control(): \
             Valid client address is 0x%08X and port is 0x%04X",
             handle_obj->client_addr, handle_obj->client_port );
#endif /* SIM_DEFINED */
      rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_EFAILED );
      CVD_VFR_PANIC_ON_ERROR( rc );

      break;
    }

    if ( cvd_vfr_soft_vfr_client_count == 1 )
    { /* Unsubscribe to voice timer service. */
#ifndef WINSIM
      rc = cvd_vfr_alloc_send_msg_to_vtm(
             &cvd_vfr_vtm_unsub_elite_msg, VOICE_TIMER_UNSUBSCRIBE );
      if ( rc )
      {
#ifndef SIM_DEFINED
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_vfr_close_cmd_control(): \
               Failed to unsubscribe to VTM, rc = 0x%08X", rc );
#endif /* SIM_DEFINED */
        rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, rc );
        CVD_VFR_PANIC_ON_ERROR( rc );

        break;
      }

      ( void) qurt_elite_channel_wait(
                &cvd_vfr_elite_channel, CVD_VFR_STOP_MASK );

      qurt_elite_signal_clear( &cvd_vfr_stop_signal );
#endif /* WINSIM */
    }

    ( void ) cvd_vfr_free_handle_object( handle_obj );

    cvd_vfr_soft_vfr_client_count--;

    rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, ctrl->packet, APR_EOK );
    CVD_VFR_PANIC_ON_ERROR( rc );

    break;
  }

  return APR_EOK;
}

/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/

APR_INTERNAL int32_t cvd_vfr_queue_pending_packet (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvd_vfr_work_item_t* work_item;

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return APR_EFAILED;
  }

  for ( ;; )
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &cvd_vfr_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Queue the command to the pending command queue. */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( &cvd_vfr_pending_ctrl.cmd_q, &work_item->link );
      cvd_vfr_signal_run( );
    }

    return APR_EOK;
  }

  { /* Try reporting the error. */
    rc = __aprv2_cmd_end_command( cvd_vfr_apr_handle, packet, rc );
    CVD_VFR_PANIC_ON_ERROR( rc );
  }

  return APR_EFAILED;
}

static int32_t cvd_vfr_isr_dispatch_fn (
  aprv2_packet_t* packet,
  void* dispatch_data
)
{
  cvd_vfr_queue_pending_packet( packet );
  return APR_EOK;
}

static void cvd_vfr_thread_process_gating_commands ( void )
{
  int32_t rc;
  cvd_vfr_pending_control_t* ctrl = &cvd_vfr_pending_ctrl;
  cvd_vfr_work_item_t* work_item;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case CVD_VFR_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &cvd_vfr_free_cmd_q, &work_item->link );
        }
        ctrl->state = CVD_VFR_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case CVD_VFR_PENDING_CMD_STATE_ENUM_EXECUTE:
    case CVD_VFR_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_IVFR_CMD_OPEN:
          rc = cvd_vfr_open_cmd_control( ctrl );
          break;

        case VSS_IVFR_CMD_CLOSE:
          rc = cvd_vfr_close_cmd_control( ctrl );
          break;

        default:
          { /* Handle error. */
            rc = __aprv2_cmd_end_command( 
                   cvd_vfr_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            CVD_VFR_PANIC_ON_ERROR( rc );
            rc = APR_EOK;
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = CVD_VFR_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = CVD_VFR_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          CVD_VFR_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      CVD_VFR_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

/****************************************************************************
 * TASK ROUTINES                                                            *
 ****************************************************************************/

static int32_t cvd_vfr_run ( void )
{
  cvd_vfr_thread_process_gating_commands( );

  return APR_EOK;
}

static int32_t cvd_vfr_task ( void* param )
{
  int32_t rc;
  rc = apr_event_create( &cvd_vfr_task_event );
  CVD_VFR_PANIC_ON_ERROR( rc );

#ifndef WINSIM
  qurt_elite_channel_init( &cvd_vfr_elite_channel );
  ( void ) qurt_elite_signal_init( &cvd_vfr_expiry_signal );
  ( void ) qurt_elite_signal_init( &cvd_vfr_stop_signal );

  ( void ) qurt_elite_channel_add_signal(
             &cvd_vfr_elite_channel, &cvd_vfr_expiry_signal, CVD_VFR_EXPIRY_MASK );
  ( void ) qurt_elite_channel_add_signal(
             &cvd_vfr_elite_channel, &cvd_vfr_stop_signal, CVD_VFR_STOP_MASK );

  cvd_vfr_vtm_sub_unsub_msg.signal_ptr = &cvd_vfr_expiry_signal;
  cvd_vfr_vtm_sub_unsub_msg.signal_end_ptr = &cvd_vfr_stop_signal;
  cvd_vfr_vtm_sub_unsub_msg.resync_signal_ptr = NULL;
  cvd_vfr_vtm_sub_unsub_msg.offset = 0; /* To receive VFR signal right on when VFR fires. */
  cvd_vfr_vtm_sub_unsub_msg.signal_enable = 1;
  cvd_vfr_vtm_sub_unsub_msg.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
  cvd_vfr_vtm_sub_unsub_msg.client_id = VOICE_DRIVER;
  cvd_vfr_vtm_sub_unsub_msg.vsid = 0;
  cvd_vfr_vtm_sub_unsub_msg.avtimer_timestamp_us = 0;
  cvd_vfr_vtm_sub_unsub_msg.timing_ver = 0; /* Ignored for SOFT VFR, shall set to 0 */
#endif /* WINSIM */

  cvd_vfr_task_state = CVD_VFR_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvd_vfr_thread_event );

  do
  {
    rc = apr_event_wait( cvd_vfr_task_event );
   ( void ) cvd_vfr_run( );
  }
  while ( rc == APR_EOK );

#ifndef WINSIM
  qurt_elite_signal_deinit( &cvd_vfr_stop_signal );
  qurt_elite_signal_deinit( &cvd_vfr_expiry_signal );
  qurt_elite_channel_destroy( &cvd_vfr_elite_channel );
#endif /* WINSIM */

  rc = apr_event_destroy( cvd_vfr_task_event );
  CVD_VFR_PANIC_ON_ERROR( rc );

  cvd_vfr_task_state = CVD_VFR_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static int32_t cvd_vfr_init ( void )
{
  uint32_t rc;
  uint32_t i;

#ifndef SIM_DEFINED
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvd_vfr_init()======" );
#endif /* SIM_DEFINED */

  cvd_vfr_soft_vfr_client_count = 0;
  cvd_vfr_soft_vfr_ref_timestamp_us = 0;

  { /* Initialize the lock. */
    ( void ) apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &cvd_vfr_int_lock );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &cvd_vfr_heapmgr, ( ( void* ) &cvd_vfr_heap_pool ),
                          sizeof( cvd_vfr_heap_pool ), NULL, NULL );
      /* memheap mustn't be called from interrupt context. No locking is
       * required in task context because all commands are serialized.
       */
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = cvd_vfr_object_table;
    params.total_bits = CVD_VFR_HANDLE_TOTAL_BITS_V;
    params.index_bits = CVD_VFR_HANDLE_INDEX_BITS_V;
    params.lock_fn = cvd_vfr_int_lock_fn;
    params.unlock_fn = cvd_vfr_int_unlock_fn;
    ( void ) apr_objmgr_construct( &cvd_vfr_objmgr, &params );
      /* TODO: When the ISR and task context contentions becomes a problem
       *       the objmgr could be split into ISR/task and task-only stores
       *       to reduce the bottleneck.
       */
  }

  { /* Initialize the pending command control. */
    ( void ) cvd_vfr_pending_control_init( &cvd_vfr_pending_ctrl );
  }

  { /* Initialize the command queue management. */
    { /* Populate the free command structures */
      ( void ) apr_list_init_v2( &cvd_vfr_free_cmd_q,
                                 cvd_vfr_int_lock_fn, cvd_vfr_int_unlock_fn );
      for ( i = 0; i < CVD_VFR_NUM_COMMANDS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &cvd_vfr_cmd_pool[ i ] );
        ( void ) apr_list_add_tail( &cvd_vfr_free_cmd_q,
                                    ( ( apr_list_node_t* ) &cvd_vfr_cmd_pool[ i ] ) );
      }
    }
  }

  { /* Create the CVD VFR task. */
    rc = apr_event_create( &cvd_vfr_thread_event );
    CVD_VFR_PANIC_ON_ERROR( rc );

    ( void ) apr_thread_create( &cvd_vfr_task_handle, CVD_VFR_TASK_NAME,
                                CVD_VFR_TASK_PRIORITY, cvd_vfr_task_stack, CVD_VFR_TASK_STACK_SIZE, 
                                cvd_vfr_task, NULL );

    /* Wait for the thread to be created. */
    ( void ) apr_event_wait( cvd_vfr_thread_event );

    rc = apr_event_destroy( cvd_vfr_thread_event );
    CVD_VFR_PANIC_ON_ERROR( rc );
  }

  { /* Initialize the APR resource. */
    /* Note that CVD VFR is a dynamic APR service whose address can only be
     * queried by client who resides on the same APR domain as CVD VFR
     * service. CVD VFR is a private module which can only be used internal
     * within CVD.
     */
    rc = __aprv2_cmd_register2(
           &cvd_vfr_apr_handle, cvd_vfr_my_dns, sizeof( cvd_vfr_my_dns ), 0,
           cvd_vfr_isr_dispatch_fn, NULL, &cvd_vfr_my_addr );
#ifndef SIM_DEFINED
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_vfr_init(): registered with APR rc=0x%08X",
           rc );
#endif /* SIM_DEFINED */
  }

  return APR_EOK;
}

static int32_t cvd_vfr_post_init ( void )
{
  return APR_EOK;
}

static int32_t cvd_vfr_pre_deinit ( void )
{
  return APR_EOK;
}

static int32_t cvd_vfr_deinit ( void )
{
#ifndef SIM_DEFINED
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvd_vfr_deinit()======" );
#endif /* SIM_DEFINED */

  { /* Release the APR resource. */
    ( void ) __aprv2_cmd_deregister( cvd_vfr_apr_handle );
  }

  { /* Destroy the CVD VFR task. */
    ( void ) apr_event_signal_abortall( cvd_vfr_task_event );

    while ( cvd_vfr_task_state != CVD_VFR_THREAD_STATE_ENUM_EXIT )
    {
      ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
    }

    ( void ) apr_thread_destroy( cvd_vfr_task_handle );
  }

  { /* Release the command queue management. */
    ( void ) apr_list_destroy( &cvd_vfr_free_cmd_q );
  }

  { /* Release the pending command control. */
    ( void ) cvd_vfr_pending_control_destroy( &cvd_vfr_pending_ctrl );
  }

  { /* Release the object management. */
    ( void ) apr_objmgr_destruct( &cvd_vfr_objmgr );
  }

  { /* Release the locks. */
    ( void ) apr_lock_destroy( cvd_vfr_int_lock );
  }

  { /* Print out debug info. on object sizes. */
#ifndef SIM_DEFINED
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "apr_objmgr_object_t size = %d",
           sizeof( apr_objmgr_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvd_vfr_handle_object_t size = %d",
           sizeof( cvd_vfr_handle_object_t ) );
#endif /* SIM_DEFINED */
  }

  return APR_EOK;
}

APR_EXTERNAL int32_t cvd_vfr_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  int32_t rc;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = cvd_vfr_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = cvd_vfr_post_init( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = cvd_vfr_pre_deinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = cvd_vfr_deinit( );
    break;

  case DRV_CMDID_RUN:
    rc = cvd_vfr_run( );
    break;

  default:
#ifndef SIM_DEFINED
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unsupported cmd_id (0x%08X)", cmd_id );
#endif /* SIM_DEFINED */
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

