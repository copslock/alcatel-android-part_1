/*
  Copyright (C) 2009-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/mvm/src/mvm_module.c#35 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include "msg.h"
#include "err.h"

#ifndef WINSIM
#include "sys_m_messages.h"
#include "rcecb.h"
#endif /* WINSIM */

#include "mmstd.h"
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"
#include "apr_log.h"
#include "aprv2_ids_domains.h"
#include "aprv2_api_inline.h"
#include "aprv2_msg_if.h"
#include "adsp_vcmn_api.h"
#include "adsp_vparams_api.h"
#include "vocsvc_avtimer_api.h"
#include "vss_public_if.h"
#include "vss_private_if.h"
#include "cvd_task.h"
#include "mvm_ccm_api_i.h"
#include "mvm_api_i.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

static char_t mvm_my_dns[] = "qcom.audio.mvm";
static uint16_t mvm_my_addr;
  /**< MVM address is set at initialization. */
static char_t mvm_cvs_dns[] = "qcom.audio.cvs";
static uint16_t mvm_cvs_addr;
  /**< CVS address is set at initialization. */
static char_t mvm_cvp_dns[] = "qcom.audio.cvp";
static uint16_t mvm_cvp_addr;
  /**< CVP address is set at initialization. */
static char_t mvm_vpm_dns[] = "qcom.audio.vpm";
static uint16_t mvm_vpm_addr;
  /**< VPM address is set at initialization. */
static char_t mvm_vsm_dns[] = "qcom.audio.vsm";
static uint16_t mvm_vsm_addr;
  /**< VSM address is set at initialization. */
static char_t mvm_cvd_vfr_dns[] = "qcom.audio.cvdvfr";
static uint16_t mvm_cvd_vfr_addr;
  /**< CVD VFR address is set at initialization. */

static char_t mvm_default_modem_session_name[] = "default modem voice";
  /**< To track handle of the circuit switched default modem stream. */

static char_t mvm_version[] = "2.2";
  /**<
   * The current MVM (CVD) version string. The format of the string is x.y,
   * where x is the major (architecture) version and y is the minor
   * (feature-set) version. This version string will be returned to the client
   * in response to the VSS_IVERSION_CMD_GET command.
   *
   * Version Information - New feature support
   * Version 2.2: Voice custom topology support & custom topology clock control.
   */

static uint32_t mvm_debug_call_num = 0;
/**< To track the number of call made. */

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define MVM_HEAP_SIZE_V ( 10240 )

#define MVM_NUM_COMMANDS_V ( 30 )

#define MVM_HANDLE_TOTAL_BITS_V ( 16 )
#define MVM_HANDLE_INDEX_BITS_V ( 5 ) /* 5 bits = 32 handles. */

#define MVM_MAX_OBJECTS_V ( 1 << MVM_HANDLE_INDEX_BITS_V )

#define MVM_SSR_MAX_TRACKED_MEM_HANDLES ( 32 )

#define MVM_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

/* This macro is to be used to inspect the APR return code whenever MVM
 * calls APR to send a packet.
 *
 * When MVM encounters an APR communication error for sending a packet to an
 * APR service who resides in a different domain than MVM, an error message
 * will be printed and the error is ignored. This is the phase one SSR
 * implementation for handling APR communication errors due to that MVM's
 * client's subsystem encounters SSR. In the future, MVM may need to use a
 * timer based retry mechanism to keep on trying to communicate with the remote
 * client until MVM knows that the remote client is no longer available.
 *
 * When MVM encounters an APR communication error for sending a packet to an
 * APR service who resides in the same domain as MVM, MVM_PANIC_ON_ERROR will
 * be called.
 */
#define MVM_COMM_ERROR( rc, dst_addr ) \
  { if ( ( rc ) && \
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, dst_addr ) != \
           APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, mvm_my_addr ) ) ) \
    { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,"MVM APR comm error 0x%08X, dst_addr = 0x%04X", rc, dst_addr ); } \
    else \
    { MVM_PANIC_ON_ERROR( rc ); } \
  }

#define MVM_MAX_GENERIC_OBJECTS_PER_SESSION_V ( 32 )

#define MVM_MAX_SESSION_NAME_SIZE ( 31 )

#define MVM_MAX_SESSIONS ( 5 )
  /**< Maximum number of full control MVM sessions that can be created. */

#define MVM_NB_SR ( 8000 )
  /**< Narrowband stream/vocproc sample rate. */

#define MVM_WB_SR ( 16000 )
  /**< Wide-band stream/vocproc sample rate. */

#define MVM_SWB_SR ( 32000 )
  /**< Super wide-band stream/vocproc sample rate. */

#define MVM_DEFAULT_DEC_SR ( 8000 ) /* Default decoder sample rate. */
#define MVM_DEFAULT_ENC_SR ( 8000 ) /* Default encoder sample rate. */

#define MVM_DEFAULT_VAR_VOC_DEC_SAMPLING_RATE ( 48000 ) /* Default decoder sample rate for variable vocoders. */
#define MVM_DEFAULT_VAR_VOC_ENC_SAMPLING_RATE ( 48000 ) /* Default encoder sample rate variable vocoders. */

#define MVM_DEFAULT_RX_PP_SR ( 8000 ) /* Default post-proc sample rate. */
#define MVM_DEFAULT_TX_PP_SR ( 8000 ) /* Default pre-proc sample rate. */

#define MVM_VOICE_FRAME_SIZE_US ( 20000 )
  /**<
   * The frame size in microseconds that each voice processing threads (vptx,
   * vprx, encoder, decoder, decoder pp) operates on.
   */

#define MVM_MAX_VOICE_TIMING_OFFSET_US ( 19500 )
  /**<
   * The maximum voice timing offset in microseconds that can be set on VCP for
   * each of the timing offsets (vp_tx_start_offset, vp_tx_delivery_offset,
   * enc_offset, dec_req_offset, dec_offset, dec_pp_start_offset,
   * vp_rx_delivery_offset).
   *
   * This is used by MVM to solve the following problem:
   * On each VFR interrupt (every 20ms), VCP sets up a number of one-shot
   * relative timers corresponding to each of the voice timing offsets set by
   * CVD. If a particular voice timing offset is high such that it is close to
   * 20ms, the corresponding timer may not fire before the next VFR interrupt
   * occurs, due to system jitter (i.e. VFR interrupt doesn't occur at exact
   * 20ms boundaries). This will cause the timer to be reset before it can
   * fire, which results in that the voice processing chain not operating as
   * expected, and leads to problems such as delivery misses for vocoder
   * packets.
   *
   * The current solution to solve the problem is to ensure a safety margin
   * of 500us for each voice timing offset that CVD set on VCP, i.e. each
   * voice timing offset can not exceed MVM_MAX_VOICE_TIMING_OFFSET_US. More
   * specifically:
   * On the uplink, offsets are moved backward if they exceed
   * MVM_MAX_VOICE_TIMING_OFFSET_US:
   * 1. If enc_offset is greater than MVM_MAX_VOICE_TIMING_OFFSET_US, it will
   *    be adjusted to MVM_MAX_VOICE_TIMING_OFFSET_US. The adjusted enc_offset
   *    will be used to derive vp_tx_delivery_offset.
   * 2. If vp_tx_delivery_offset is greater than
   *    MVM_MAX_VOICE_TIMING_OFFSET_US, it will be adjusted to
   *    MVM_MAX_VOICE_TIMING_OFFSET_US. The adjusted vp_tx_delivery_offset will
   *    be used to derive vp_tx_start_offset.
   * 3. If vp_tx_start_offset is greater than MVM_MAX_VOICE_TIMING_OFFSET_US,
   *    it will be adjusted to MVM_MAX_VOICE_TIMING_OFFSET_US.
   * On the downlink, offsets are moved forward if they exceed
   * MVM_MAX_VOICE_TIMING_OFFSET_US:
   * 1. If dec_req_offset is greater than MVM_MAX_VOICE_TIMING_OFFSET_US, it
   *    will be adjusted to 0us. The amount of adjustment made to
   *    dec_req_offset will be added to dec_offset.
   * 2. If dec_offset is greater than MVM_MAX_VOICE_TIMING_OFFSET_US, it will
   *    be adjusted to 0us. The adjusted dec_offset will be used to derive
   *    dec_pp_start_offset.
   * 3. If dec_pp_start_offset is greater than MVM_MAX_VOICE_TIMING_OFFSET_US,
   *    it will be adjusted to 0us. The adjusted dec_pp_start_offset will be
   *    used to derive vp_rx_delivery_offset.
   * 4. If vp_rx_delivery_offset is greater than
   *    MVM_MAX_VOICE_TIMING_OFFSET_US, it will be adjusted to 0us.
   *
   * TODO: VCP team will discuss if there are any better alternative solutions
   * to the problem, for future PLs. VSD team will update the code accordingly.
   */
#define MVM_VOCPROC_PROCESSING_TIME_SAFETY_MARGIN_US ( 1000 )
  /**<
   * Additional 1ms safety margin in the VpTx and VpRx timing offset that CVD
   * sets on VCP. This is needed to compensate for sample slipping/stuffing,
   * where VpTx/VpRx processing start time can be delayed by 1ms.
   */

#define MVM_TX_PROCESSING_TIME_SAFETY_MARGIN_US ( 500 )
  /**<
   * Additional 0.5ms safety margin in the VpTx and Venc timing offset that CVD
   * sets on VCP. This is needed to take care of thread preemption delay.
   */

#define MVM_EVS_ENCODER_PROCESSING_TIME_SAFETY_MARGIN_US ( 1000 )
  /**<
   * Additional 1ms safety margin in the Venc timing offset that CVD sets on VCP for 
   * EVS vocoder. This is needed to take care of any delays in processing time 
   * due to insufficient bandwidth. 
   */

#define MVM_HDVOICE_UI_DISABLED (0) /* VSS_IHDVOICE_CMD_DISABLE. */
#define MVM_HDVOICE_UI_ENABLED (1)  /* VSS_IHDVOICE_CMD_ENABLE. */
#define MVM_HDVOICE_UI_UNUSED (2)   /* VSS_IHDVOICE_CMD not used in the session. */
  /**< Controls the state of the HD Voice UI enablement for MVM. */

/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

typedef enum mvm_thread_state_enum_t
{
  MVM_THREAD_STATE_ENUM_INIT,
  MVM_THREAD_STATE_ENUM_READY,
  MVM_THREAD_STATE_ENUM_EXIT
}
  mvm_thread_state_enum_t;

typedef enum mvm_thread_priority_enum_t
{
  MVM_THREAD_PRIORITY_ENUM_HIGH,
  MVM_THREAD_PRIORITY_ENUM_MED,
  MVM_THREAD_PRIORITY_ENUM_LOW
}
  mvm_thread_priority_enum_t;

typedef enum mvm_mem_mapping_mode_t
{
  MVM_MEM_MAPPING_MODE_ENUM_VIRTUAL,
  MVM_MEM_MAPPING_MODE_ENUM_PHYSICAL
}
  mvm_mem_mapping_mode_t;

typedef struct mvm_stream_vocproc_processing_time_us_t
{
  uint16_t enc;
  uint16_t dec;
  uint16_t dec_pp;
  uint16_t vp_tx;
  uint16_t vp_rx;
}
  mvm_stream_vocproc_processing_time_us_t;

/****************************************************************************
 * WORK QUEUE DEFINITIONS                                                   *
 ****************************************************************************/

typedef struct mvm_work_item_t
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
}
  mvm_work_item_t;

/****************************************************************************
 * COMMAND RESPONSE FUNCTION TABLE                                          *
 ****************************************************************************/

typedef void ( *mvm_event_handler_fn_t ) ( aprv2_packet_t* packet );

typedef enum mvm_response_fn_enum_t
{
  MVM_RESPONSE_FN_ENUM_ACCEPTED,
  MVM_RESPONSE_FN_ENUM_RESULT,
  MVM_RESPONSE_FN_ENUM_MAP_MEMORY,
  MVM_RESPONSE_FN_ENUM_SET_SYSTEM_CONFIG,
  MVM_RESPONSE_FN_ENUM_GET_AVSYNC_DELAYS,
  MVM_RESPONSE_FN_ENUM_GET_PKTEXG_MODE,
  MVM_RESPONSE_FN_ENUM_GET_MAILBOX_PKTEXG_TIME_REFERENCE,
  MVM_RESPONSE_FN_ENUM_GET_HDVOICE_CONFIG,
  MVM_RESPONSE_FN_ENUM_SET_HDVOICE_CONFIG,
  MVM_RESPONSE_FN_ENUM_OPEN_SOFT_VFR,
  MVM_RESPONSE_FN_ENUM_INVALID,
  MVM_RESPONSE_FN_ENUM_MAX = MVM_RESPONSE_FN_ENUM_INVALID
}
  mvm_response_fn_enum_t;

/**
 * Pending commands may load different sets of response and event handlers to
 * complete each job. The response function table is equivalent to the state
 * design pattern. The state context is stored in the pending command control.
 * Pending commands can be as simple or as complex as required.
 */
typedef mvm_event_handler_fn_t mvm_response_fn_table_t[ MVM_RESPONSE_FN_ENUM_MAX ];

/****************************************************************************
 * SESSION CONTROL DEFINITIONS                                              *
 ****************************************************************************/

typedef enum mvm_state_enum_t
{
  MVM_STATE_ENUM_UNINITIALIZED,
    /**< Reserved. */
  MVM_STATE_ENUM_RESET_ENTRY,
    /**< Move into or out of reset. */
  MVM_STATE_ENUM_RESET,
    /**< The session resource is not acquired. */
  MVM_STATE_ENUM_INIT_ENTRY,
    /**< Move into or out of init. */
  MVM_STATE_ENUM_INIT,
    /**< The session resource is not brought up. */
  MVM_STATE_ENUM_IDLE_ENTRY,
    /**< Move into or out of idle. */
  MVM_STATE_ENUM_IDLE,
    /**< The session resource is ready to run. */
  MVM_STATE_ENUM_RUN_ENTRY,
    /**< Move into or out of run. */
  MVM_STATE_ENUM_RUN,
    /**< The session resource is running. */
  MVM_STATE_ENUM_ERROR_ENTRY,
    /**< Performing error recovery. */
  MVM_STATE_ENUM_ERROR,
    /**< The session resource is unusable and should be destroyed. */
  MVM_STATE_ENUM_INVALID
    /**< Reserved. */
}
  mvm_state_enum_t;

typedef enum mvm_goal_enum_t
{
  MVM_GOAL_ENUM_UNINITIALIZED,
  MVM_GOAL_ENUM_NONE,
  MVM_GOAL_ENUM_CREATE,
  MVM_GOAL_ENUM_DESTROY,
  MVM_GOAL_ENUM_STOP,
  MVM_GOAL_ENUM_PAUSE,
  MVM_GOAL_ENUM_STANDBY,
  MVM_GOAL_ENUM_START,
  MVM_GOAL_ENUM_INVALID,
}
  mvm_goal_enum_t;

typedef enum mvm_action_enum_t
{
  MVM_ACTION_ENUM_UNINITIALIZED,
    /* Common actions. */
  MVM_ACTION_ENUM_NONE,
    /**< The first action has not started for a goal from any state. */
  MVM_ACTION_ENUM_COMPLETE,
    /**<
     * Reached the last action for a goal from a state. A multi-action goal
     * that starts from and ends in the same state may require a COMPLETE
     * action to properly differentiate a terminate signal.
     */
  MVM_ACTION_ENUM_CONTINUE,
    /**<
     * For multi-state goals, the last action from each state should set to
     * CONINTUE. This indicates to the next state that a goal is continuing
     * its operation from a previous state. Usually the previous state is
     * known given the current state and the continued goal. New actions can
     * be created to help discriminate the direction from where goals come
     * from as required.
     */
  /* RESET -> INIT actions. */
  MVM_ACTION_ENUM_STREAM_CREATE_SESSION, /* BACKWARD COMPATIBILITY */
  /* INIT -> IDLE actions. */
  MVM_ACTION_ENUM_REQUEST_HIGH_CLOCK,
  MVM_ACTION_ENUM_RESET_STREAM_VOCPROC_LOAD_COUNT,
  MVM_ACTION_ENUM_HDVOICE_CONFIG,
  MVM_ACTION_ENUM_VOCPROC_RECONFIG,
  MVM_ACTION_ENUM_VAR_VOC_STREAM_SAMP_RATE_CONFIG,
  MVM_ACTION_ENUM_STREAM_RECONFIG,
  MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE,
  MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE,
  MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE,
  MVM_ACTION_ENUM_OPEN_SOFT_VFR_FOR_MAILBOX_PKTEXG,
  MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS,
  MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS,
  MVM_ACTION_ENUM_VOCPROC_SET_TIMING,
  MVM_ACTION_ENUM_STREAM_SET_TIMING,
  MVM_ACTION_ENUM_UPDATE_ACTIVE_SET,
  MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS,
  MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS,
  MVM_ACTION_ENUM_STREAM_SET_AVSYNC_DELAYS,
  MVM_ACTION_ENUM_VOCPROC_ENABLE,
  /* INIT -> IDLE and IDLE -> RUN actions. */
  MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE,
  MVM_ACTION_ENUM_REVERT_HIGH_CLOCK,
  /* IDLE -> RUN actions. */
  MVM_ACTION_ENUM_STREAM_RUN,
  /* RUN -> IDLE actions. */
  MVM_ACTION_ENUM_STREAM_STOP,
  /* IDLE -> INIT actions. */
  MVM_ACTION_ENUM_VOCPROC_DISABLE,
  /* IDLE -> INIT and INIT -> RESET actions. */
  MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE,
  MVM_ACTION_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING,
  MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG,
  /* INIT -> RESET actions. */
  MVM_ACTION_ENUM_VOCPROC_DETACH,
  MVM_ACTION_ENUM_STREAM_DESTROY_SESSION, /* BACKWARD COMPATIBILITY */
  /* No transition actions. */
  MVM_ACTION_ENUM_STREAM_REINIT,
  /* RUN -> IDLE actions*/
  MVM_ACTION_ENUM_VOCPROC_REINIT,
  /* RUN -> IDLE actions*/
  MVM_ACTION_ENUM_INVALID
}
  mvm_action_enum_t;

typedef struct mvm_control_t
{
  uint32_t statejob_handle;

  mvm_state_enum_t state;
  mvm_goal_enum_t goal;
  mvm_action_enum_t action;
  uint32_t status;
}
  mvm_control_t;

/****************************************************************************
 * MVM OBJECT DEFINITIONS                                                   *
 ****************************************************************************/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE COMMON OBJECT DEFINITIONS                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef enum mvm_object_type_enum_t
{
  MVM_OBJECT_TYPE_ENUM_UNINITIALIZED,
  MVM_OBJECT_TYPE_ENUM_SESSION,
  MVM_OBJECT_TYPE_ENUM_INDIRECTION,
  MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
  MVM_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
  MVM_OBJECT_TYPE_ENUM_INVALID
}
  mvm_object_type_enum_t;

typedef struct mvm_object_header_t
{
  uint32_t handle;
    /**< The handle to the associated apr_objmgr_object_t instance. */
  mvm_object_type_enum_t type;
    /**<
     * The object type defines the actual derived object.
     *
     * The derived object can be any custom object type. A session or a
     * command are two such custom object types. A free object entry is set
     * to MVM_OBJECT_TYPE_ENUM_FREE.
     */
}
  mvm_object_header_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SESSION OBJECT                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * mvm_generic_item_t is a generic linked list node data structure whose
 * intent is to maximize recycling of linked list node storage space.
 * mvm_generic_item_t linked list nodes contain a handle. The handle is
 * dynamically re-purposed based on the use case.
 *
 * An example of this is in the session object where there is a free_items_q
 * of generic items. The free_item_q are dequeued and enqueued into the
 * indirection_q (storing MVM indirection objects) or the vocproc_q (storing
 * CVP handles). Once the item storage is no longer needed in the respective
 * list, the free item is moved back into the free_item_q.
 */
typedef struct mvm_generic_item_t
{
  apr_list_node_t link;

  uint32_t handle;
    /**< Any custom handle. */
}
  mvm_generic_item_t;

typedef struct mvm_voice_timing_t
{
  uint16_t enc_offset;
  uint16_t dec_req_offset;
  uint16_t dec_offset;
  uint16_t dec_pp_start_offset;
  uint16_t vp_rx_start_offset;
  uint16_t vp_rx_delivery_offset;
  uint16_t vp_tx_start_offset;
  uint16_t vp_tx_delivery_offset;
}
  mvm_voice_timing_t;

typedef struct mvm_clock_and_concurrency_config_t
{
  bool_t is_multi_session;
    /**<
     * Indicates whether the voice system contains more than one voice session
     * or more than one stream or more than one vocproc active.
     */
  uint32_t total_core_kpps;
    /**< VDSP current core KPPS requested for voice use cases. */
}
  mvm_clock_and_concurrency_config_t;

typedef struct mvm_stream_vocproc_load_t
{
  uint16_t num_nb_streams;
    /**< Number of narrow-band (8 KHz) streams attached to an MVM session. */
  uint16_t num_wb_streams;
    /**< Number of wide-band (16 KHz) streams attached to an MVM session. */
  uint16_t num_swb_streams;
    /**< Number of super wide-band (32 KHz) streams attached to an MVM session. */
  uint16_t num_fb_plus_streams;
    /**< Number of full-band (48 KHz) or higher streams attached to an MVM session. */
  uint16_t num_nb_vocprocs;
    /**<  Number of narrow-band (8 KHz) vocprocs attached to an MVM session. */
  uint16_t num_wb_vocprocs;
    /**< Number of wide-band (16 Khz) vocprocs attached to an MVM session. */
  uint16_t num_swb_vocprocs;
    /**< Number of super wide-band (32 Khz) vocprocs attached to an MVM session. */
  uint16_t num_fb_plus_vocprocs;
    /**< Number of full-band (48 Khz) or higher vocprocs attached to an MVM session. */
  uint32_t total_kpps;
    /**< Total KPPS for all the streams and vocprocs attached to an MVM session. */
  uint32_t vocproc_rx_topology_id;
    /**< Vocproc Rx path topology ID. Applicable only if there is a single vocproc
         attached to this MVM session. It is ignored otherwise. */
  uint32_t vocproc_tx_topology_id;
    /**< Vocproc Tx path topology ID. Applicable only if there is a single vocproc
         attached to this MVM session. It is ignored otherwise. */
  uint32_t stream_media_id;
    /**< Stream Media ID.  Applicable only if there is a single stream
         attached to this MVM session. It is ignored otherwise. */
  uint32_t enc_kpps;
    /**< Encoder KPPS requirements. Applicable only if there is a single stream
         attached to this MVM session. It is ignored otherwise. */
  uint32_t dec_kpps;
    /**< Decoder KPPS requirements. Applicable only if there is a single stream
         attached to this MVM session. It is ignored otherwise. */
  uint32_t dec_pp_kpps;
    /**< Stream RX post-processing block KPPS requirements. Applicable only if
         there is a single stream attached to this MVM session. It is ignored
         otherwise. */
  uint32_t vp_rx_kpps;
    /**< Vocproc RX KPPS requirements. Applicable only if there is a single vocproc
         attached to this MVM session. It is ignored otherwise. */
  uint32_t vp_tx_kpps;
    /**< Vocproc TX KPPS requirements. Applicable only if there is a single vocproc
         attached to this MVM session. It is ignored otherwise. */
  uint32_t tx_num_channels;
    /**< Tx Number of channels. Applicable only if there is a single active
         voice session which has a single vocproc attached to it. It is ignored
         otherwise. */
  uint32_t tx_mpps_scale_factor;
    /** Tx MPPS scale factor. */
  uint32_t tx_bw_scale_factor;
    /** Tx BW scale factor. */
  uint32_t rx_mpps_scale_factor;
    /** Rx MPPS scale factor. */
  uint32_t rx_bw_scale_factor;
    /** Rx BW scale factor. */
}
  mvm_stream_vocproc_load_t;

typedef struct mvm_avsync_vocproc_delays_info_t
{
  uint32_t max_vp_rx_algorithmic_delay;
    /**< Max vocproc rx path algorithmic delay. */
  uint32_t max_vp_tx_algorithmic_delay;
    /**< Max vocproc tx path algorithmic delay. */
  uint32_t vp_rx_normalized_total_delay;
    /**< vocproc rx path algorithmic + processing delay. */
  uint32_t vp_tx_normalized_total_delay;
    /**< vocproc tx path algorithmic + processing delay. */
}
  mvm_avsync_vocproc_delays_info_t;

typedef struct mvm_mailbox_pktexg_stream_info_t
{
  bool_t is_ready_to_run;
    /**< Flag indicates whether the mailbox streams attached to this MVM
         session are ready to run. The streams are ready to run once all of the
         streams have received the packet exchagne time reference from the
         client. */
  bool_t is_clear_time_ref_needed;
    /**< Flag indicates whether the mailbox streams' packet exchange time
         reference needs to be cleared. This flag is set to TRUE whenever MVM
         receive a VSS_IMVM_CMD_START_VOICE or VSS_IMVM_CMD_MODEM_START_VOICE
         command from the full control client (i.e. the client who provides
         packet exchange time reference). */
  bool_t is_soft_vfr_opened;
    /**< Flag indicates whether the soft VFR driver has been opened. */
  bool_t is_disable_rx_expiry_processing;
    /**< Flag indicates whether the Rx expiry processing need to be disabled.
         This flag is set to TRUE whenever MVM receive a
         VSS_IMVM_CMD_STOP_VOICE or VSS_IMVM_CMD_MODEM_STOP_VOICE command from
         the full control client. */
  uint16_t stream_cnt;
    /**< The number of mailbox streams attached to this MVM session. */
  uint16_t get_time_ref_cnt;
    /**< The number of responses to the
         VSS_IPKTEXG_CMD_MAILBOX_GET_TIME_REFERENCE command has been
         received from all the streams attached to this MVM session. */
  vss_ipktexg_rsp_mailbox_get_time_reference_t time_ref;
    /**< The time reference to be used for mailbox packet exchange. Note that
         there is no use case that require multiple streams running in a single
         voice call. If there is such use case in the future, all the streams
         in the same voice call will use the same packet exchange time
         reference. One of the stream will have optimal RTD, the rest of the
         streams will have non-optimal RTD. */
  mvm_voice_timing_t timing_params;
    /**< The voice timing parameters to be used for mailbox packet exchange. */
  uint16_t soft_vfr_handle;
    /**< The soft VFR driver open handle. */
  uint64_t soft_vfr_ref_timestamp_us;
    /**< The soft VFR reference timestamp in microseconds. */
}
  mvm_mailbox_pktexg_stream_info_t;

typedef struct mvm_active_settings_t
{
  vss_icommon_cmd_set_system_config_t system_config;
  mvm_voice_timing_t voice_timing;
  mvm_clock_and_concurrency_config_t clock_and_concurrency_config;
  mvm_stream_vocproc_load_t stream_vocproc_load;
  uint32_t tty_mode;
  uint32_t widevoice_enable; /* BACKWARD COMPATIBILITY */
}
  mvm_active_settings_t;

typedef struct mvm_target_settings_t
{
  vss_icommon_cmd_set_system_config_t system_config;
  mvm_voice_timing_t voice_timing;
  mvm_clock_and_concurrency_config_t clock_and_concurrency_config;
  mvm_stream_vocproc_load_t stream_vocproc_load;
  uint32_t tty_mode;
  uint32_t widevoice_enable; /* BACKWARD COMPATIBILITY */
}
  mvm_target_settings_t;

typedef struct mvm_session_object_t
{
  mvm_object_header_t header;

  /* Private housekeeping variables. */
  mvm_generic_item_t generic_pool[ MVM_MAX_GENERIC_OBJECTS_PER_SESSION_V ];
  apr_list_t free_item_q;
    /**< A list of free mvm_generic_item_t entries. The handle is undefined. */

  /* Internal variables. */
  char_t session_name[ MVM_MAX_SESSION_NAME_SIZE ];

  uint16_t self_addr;

  bool_t is_dual_control;
    /* Indicates that both the evaluation of the apps and modem state are needed. */

  /* Full control client. */
  uint16_t master_addr;
  uint16_t master_port;

  /* BACKWARD COMPATIBILITY */
  /* MVS v1.0 backward compatibility policy management. */
  bool_t is_mvs_v1_policy;
    /**<
     * The method by which we distinguish between MVS 1.0 vs MVS 2.0+ behavior is
     * based on whether MVM receives a VSS_ISTREAM_CMD_SET_MEDIA_TYPE command
     * from MVS.
     * In MVS 1.0 behavior, the media type is set by MVS on MVM and propagated
     * by MVM to the "default modem voice" stream based on MVM's own passive control
     * handle to the stream.
     * In MVS 2.0+ behavior, the media type is set by MVS directly on stream
     * and MVM does not create a passive handle to the stream.
     */

  uint16_t default_modem_stream_addr; /* BACKWARD COMPATIBILITY */
  uint16_t default_modem_stream_handle; /* BACKWARD COMPATIBILITY */
    /**<
     * The address and handle of the passive control session to CVS. Required for
     * setting media type.
     */

  bool_t is_default_modem_stream_attached; /* BACKWARD COMPATIBILITY */
    /**<
     * Indicates whether the default_modem_stream passive handle has been
     * inserted into the MVM's attached stream_q. The operation of inserting
     * it into the stream_q is only performed once for the lifetime of the MVM
     * session (on the first START command from modem). Hence, this flag.
     * Read comments in mvm_stream_create_session_transition_result_rsp_fn()
     * and mvm_start_voice_cmd_control() for further details.
     */

  bool_t is_wv_set_by_ui; /* BACKWARD COMPATIBILITY */
    /**<
     * Indicates whether the WideVoice is set by UI setting, which is
     * deprecated behavior. This flag will be set to TRUE if the MVM receives
     * the VSS_IWIDEVOICE_CMD_SET_WIDEVOICE command. Only when this flag is
     * set to TRUE, will MVM forward the WideVoice setting to CVS during stream
     * reconfiguration.
     */

  /* Indirection queue. */
  apr_list_t indirection_q;
    /**<
     * A list of mvm_generic_item_t entries. The handle is an MVM indirection
     * object handle.
     */

  /* Stream management. */
  apr_list_t stream_q;
    /**< A list of mvm_generic_item_t entries. The handle is a CVS handle. */
  uint32_t stream_rsp_cnt;
    /**< CVS group operation response counter. */
  uint32_t stream_attach_detach_vocproc_rsp_cnt;
    /**< CVS multiple attach/detach vocproc commands response counter. */
  mvm_mailbox_pktexg_stream_info_t mailbox_pktexg_stream_info;
    /**<
     * Stores the infomation of the mailbox packet exchange streams attached
     * to this MVM session.
     */

  /* Vocproc management. */
  apr_list_t vocproc_q;
    /**< A list of mvm_generic_item_t entries. The handle is a CVP handle. */
  uint32_t vocproc_rsp_cnt;
    /**< CVP group operation response counter. */

  uint32_t detach_all_rsp_cnt;
    /**< Detach all vocprocs from all streams response counter. */

  uint32_t requested_var_voc_rx_sampling_rate;
  uint32_t requested_var_voc_tx_sampling_rate;
    /**<
     * Variable vocoder sampling rates requested by client. Upon
     * VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE we cache them here. On
     * every VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE or
     * VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE, we determine the target_set enc,
     * dec and pp sample rates based on the media type and the requested
     * sample rates.
     */

  bool_t is_voc_op_mode_evt_received;
    /**<
     * Indicates whether the VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event
     * notification has been received at least once from at least one of the
     * streams attached to this MVM session. The stream sends the
     * VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event to MVM:
     * 1. upon the stream is attached to MVM.
     * 2. upon the stream receives a set_media_type command.
     * 3. upon the stream detects that the vocoder operating mode has changed
     *    for the current media ID configured on the stream.
     *
     * The stream's vocoder operating mode will be used to calibrate the
     * streams and the vocprocs attached to this MVM session.
     *
     * If there are multiple streams attached to this MVM session, MVM will use
     * the vocoder operating mode received via the lastest
     * VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event to calibrate the streams
     * and the vocprocs attached to this MVM session.
     */

  /* Policy management. */
  mvm_control_t session_ctrl;

  bool_t is_active;
    /**<
     * Indicates whether this session is active. Specifically, this flag is set
     * to TRUE once VSS_IMVM_EVT_VOICE_SESSION_ACTIVE has been sent to CCM.
     *
     * This flag is set to FALSE:
     *
     *  - upon session creation.
     *
     *  - upon VSS_IMVM_EVT_VOICE_SESSION_INACTIVE has been sent to CCM.
     */

  uint16_t is_hdvoice_ui_enabled;
    /**< TRUE: client has enabled HD Voice on UI via VSS_IHDVOICE_CMD_ENABLE */

  bool_t is_wv2_enabled;
    /**< FALSE: client has disabled HD Voice on UI via VSS_IHDVOICE_CMD_WV2_DISABLE */

  bool_t is_beamr_enabled;
    /**< FALSE: client has disabled HD Voice on UI via VSS_IHDVOICE_CMD_BEAMR_DISABLE */

  bool_t is_kpps_changed_by_hdvoice;
    /**< TRUE: From VSS_IHDVOICE_RSP_SET_CONFIG if response specifies KPPS changes */

  mvm_avsync_vocproc_delays_info_t avsync_vocproc_delay;
  bool_t is_avtimer_handle_open;
    /**< Facilitate per Session reference count for avtimer_drv_handle
     * in vocsvc_avtimer.c.
     */

  mvm_active_settings_t active_set;
  mvm_target_settings_t target_set;

  mvm_goal_enum_t modem_state;
    /**< The valid values are: STOP, STANDBY, and START. */
  mvm_goal_enum_t apps_state;
    /**< The valid values are: STOP, PAUSE, STANDBY, and START. */

  bool_t is_stream_reinit_required;
    /**< Indicates whether a stream re-init is required. */

  bool_t is_vocproc_reinit_required;
    /**< Indicates whether a vocproc re-init is required. */

  apr_lock_t lock;
    /* Per session object lock. To guard multi-thread access of session object. */
}
  mvm_session_object_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SESSION INDIRECTION OBJECT                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define MVM_INDIRECTION_ACCESS_FULL_CONTROL_MASK ( 0x00000001 )
#define MVM_INDIRECTION_ACCESS_FULL_CONTROL_SHFT ( 0 )

typedef struct mvm_indirection_object_t
{
  mvm_object_header_t header;

  uint16_t client_addr;
    /**<
     * The address of the MVM client that this indirection object is created
     * for. When MVM client creates either a full or passive session control,
     * MVM creates a session indirection object whose handle is provided
     * to the client for controling the session.
     */

  uint32_t session_handle;
    /**< The handle to the named session mvm_session_object_t. */

  uint32_t access_bits;
    /**<
     * The indirection access bit-flags:
     *
     * Bit 0: 0: Passive control.
     *        1: Full control.
     */
}
  mvm_indirection_object_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SIMPLE JOB OBJECT                                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct mvm_simple_job_object_t
{
  mvm_object_header_t header;

  uint32_t context_handle;
    /**<
     * The context handle provides additional information or storage for the
     * simple job to complete.
     *
     * The context can be anything; a session, a parent-job, etc. The
     * context is especially useful to applications that override the
     * default simple job response function table to perform additional
     * processing such as state transitions.
     *
     * Set this value to -1 when it is unused or when there is no parent.
     */
  mvm_response_fn_table_t fn_table;
    /**<
     * This is the response function v-table. The response table can store
     * custom response routines for all possible responses directed to this
     * specific job.
     */
  bool_t is_accepted;
    /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
    /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
    /**< The status returned by the command completion. */
}
  mvm_simple_job_object_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SEQUENCER JOB OBJECT                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct mvm_sequencer_attach_stream_struct_t
{
  uint16_t cvs_handle;
}
  mvm_sequencer_attach_stream_struct_t;

typedef struct mvm_sequencer_detach_stream_struct_t
{
  uint16_t cvs_handle;
}
  mvm_sequencer_detach_stream_struct_t;

typedef struct mvm_sequencer_attach_vocproc_struct_t
{
  uint16_t cvp_handle;
}
  mvm_sequencer_attach_vocproc_struct_t;

typedef struct mvm_sequencer_detach_vocproc_struct_t
{
  uint16_t cvp_handle;
}
  mvm_sequencer_detach_vocproc_struct_t;

typedef struct mvm_sequencer_map_unmap_memory_struct_t
{
  uint32_t mem_handle;
  vss_imemory_cmd_map_virtual_t* map_virtual_arg;
  vss_imemory_cmd_map_physical_t* map_physical_arg;
}
  mvm_sequencer_map_unmap_memory_struct_t;

typedef struct mvm_sequencer_ssr_cleanup_struct_t
{
  uint8_t domain_id;
}
  mvm_sequencer_ssr_cleanup_struct_t;

typedef union mvm_sequencer_use_case_union_t
{
  mvm_sequencer_attach_stream_struct_t attach_stream;
  mvm_sequencer_detach_stream_struct_t detach_stream;
  mvm_sequencer_attach_vocproc_struct_t attach_vocproc;
  mvm_sequencer_detach_vocproc_struct_t detach_vocproc;
  mvm_sequencer_map_unmap_memory_struct_t map_unmap_memory;
  mvm_sequencer_ssr_cleanup_struct_t ssr_cleanup;
}
  mvm_sequencer_use_case_union_t;

typedef struct mvm_sequencer_job_object_t
{
  mvm_object_header_t header;

  uint32_t state;
    /**< The generic state variable. */
  union mvm_object_t* subjob_obj;
    /**< The current sub-job object. */
  uint32_t status;
    /**< A status value. */

  mvm_sequencer_use_case_union_t use_case;
}
  mvm_sequencer_job_object_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE GENERIC CVD OBJECT                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef union mvm_object_t
{
  mvm_object_header_t header;
  mvm_session_object_t session;
  mvm_indirection_object_t indirection;
  mvm_simple_job_object_t simple_job;
  mvm_sequencer_job_object_t sequencer_job;
}
  mvm_object_t;

/****************************************************************************
 * PENDING COMMAND CONTROL DEFINITIONS                                      *
 ****************************************************************************/

typedef enum mvm_pending_cmd_state_enum_t
{
  MVM_PENDING_CMD_STATE_ENUM_FETCH,
    /**< Fetch the next pending command to execute. */
  MVM_PENDING_CMD_STATE_ENUM_EXECUTE,
    /**< Execute the current pending command for the first time. */
  MVM_PENDING_CMD_STATE_ENUM_CONTINUE
    /**< Continue executing the current pending command. */
}
  mvm_pending_cmd_state_enum_t;

/**
 * The pending command control structure stores information used to process
 * pending commands serially.
 *
 * Pending commands have state and are executed one at a time until
 * completion. A gated pending command gates all the pending commands that
 * follows it. A gated pending command should employ abort timers to allow the
 * system to make progress.
 *
 * Aborting critical pending commands can leave the system in a bad state. It
 * is recommended that (at the very least) the configurations of the aborted
 * critical pending commands be saved so that on error recovery the proper
 * configurations could be restored.
 */
typedef struct mvm_pending_control_t
{
  apr_list_t cmd_q;
    /**< The pending (mvm_work_item_t) command queue. */
  mvm_pending_cmd_state_enum_t state;
    /**<
     * The current state of the pending command control.
     *
     * This variable is managed by the pending command processor. The
     * individual pending command controls indicates to the pending command
     * processor to complete or to delay the completion of the current
     * pending command.
     */
  aprv2_packet_t* packet;
    /**<
     * The current (command) packet being processed.
     */
  mvm_object_t* pendjob_obj;
    /**<
     * The pendjob_obj is a temporary storage for the current pending
     * command.
     */
}
  mvm_pending_control_t;

/****************************************************************************
 * SUBSYSTEM RESTART (SSR) RELATED TRACKING                                 *
 ****************************************************************************/

typedef struct mvm_ssr_cleanup_cmd_tracking_t
{
  uint32_t num_cmd_issued;
    /**<
     * Number of the commands issued by MVM to itself in each of the cleanup
     * sequencer state, when doing cleanup due to the subsystem where MVM
     * clients reside is being restarted.
     */
  uint32_t rsp_cnt;
    /**<
     * Response counter for the commands issued by MVM to itself in each of the
     * cleanup sequencer state, when doing cleanup due to the subsystem where
     * MVM clients reside is being restarted.
     */
}
  mvm_ssr_cleanup_cmd_tracking_t;

typedef struct mvm_ssr_mem_handle_tracking_item_t
{
  apr_list_node_t link;

  uint16_t client_addr;
    /**< The address of the client who mapped the memory. */
  uint32_t mem_handle;
    /**< The handle to the mapped memory. */
}
  mvm_ssr_mem_handle_tracking_item_t;

typedef struct mvm_ssr_mem_handle_tracking_t
{
  mvm_ssr_mem_handle_tracking_item_t pool[ MVM_SSR_MAX_TRACKED_MEM_HANDLES ];
  apr_list_t free_q;
  apr_list_t used_q;
}
  mvm_ssr_mem_handle_tracking_t;

typedef struct mvm_voice_activity_client_info_t
{
  bool_t is_enabled;
    /**< Indicatea whether voice acitvity update notification is enabled. */

  uint16_t client_addr;
    /**< Address of the client who listens to the voice activty update 
         notification. */

  uint16_t client_port;
    /**< Port of the client who listens to the voice activty update 
         notification. */
}
  mvm_voice_activity_client_info_t;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Lock Management */
static apr_lock_t mvm_int_lock;
static apr_lock_t mvm_med_task_lock;
static apr_lock_t mvm_low_task_lock;
static apr_lock_t mvm_ref_cnt_lock;

/* Heap Management */
static uint8_t mvm_heap_pool[ MVM_HEAP_SIZE_V ];
static apr_memmgr_type mvm_heapmgr;

/* Object Management */
static apr_objmgr_object_t mvm_object_table[ MVM_MAX_OBJECTS_V ];
static apr_objmgr_t mvm_objmgr;

/* Command Queue Management */
static mvm_work_item_t mvm_cmd_pool[ MVM_NUM_COMMANDS_V ];
static apr_list_t mvm_free_cmd_q;
static apr_list_t mvm_high_task_incoming_cmd_q;
static apr_list_t mvm_med_task_incoming_cmd_q;
static apr_list_t mvm_low_task_incoming_cmd_q;

/* Task Management */
static apr_event_t mvm_thread_event;
/* For high priority task. */
static apr_thread_t mvm_high_task_handle;
static apr_event_t mvm_high_task_event;
static uint8_t mvm_high_task_stack[ MVM_HIGH_TASK_STACK_SIZE ];
/* For medium priority task. */
static apr_thread_t mvm_med_task_handle;
static apr_event_t mvm_med_task_event;
static uint8_t mvm_med_task_stack[ MVM_MED_TASK_STACK_SIZE ];
/* For low priority task. */
static apr_thread_t mvm_low_task_handle;
static apr_event_t mvm_low_task_event;
static uint8_t mvm_low_task_stack[ MVM_LOW_TASK_STACK_SIZE ];

/* For creating and destroying tasks. */
static mvm_thread_state_enum_t mvm_task_state = MVM_THREAD_STATE_ENUM_INIT;

/* Session Management */
/* For high priority task. */
static mvm_pending_control_t mvm_high_task_pending_ctrl;
/* For medium priority task. */
static mvm_pending_control_t mvm_med_task_pending_ctrl;
/* For low priority task. */
static mvm_pending_control_t mvm_low_task_pending_ctrl;

/* Subsystem Restart (SSR) Management */
static mvm_pending_control_t mvm_ssr_pending_ctrl;
static mvm_ssr_cleanup_cmd_tracking_t mvm_ssr_cleanup_cmd_tracking;
static mvm_ssr_mem_handle_tracking_t mvm_ssr_mem_handle_tracking;

/* APR Resource */
static uint32_t mvm_apr_handle;

/* Session tracking */
static mvm_generic_item_t mvm_session_list_pool[ MVM_MAX_SESSIONS ];
static apr_list_t mvm_session_list_free_q;
static apr_list_t mvm_session_q;

/* Global client info who listen for voice acitity update events. */
mvm_voice_activity_client_info_t voice_activity_client = { FALSE,
                                                           APR_NULL_V,
                                                           APR_NULL_V };

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

static int32_t mvm_free_object (
  mvm_object_t* object
);

static bool_t mvm_voc_op_mode_is_valid (
  uint32_t voc_op_mode
);

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void mvm_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( mvm_int_lock );
}

static void mvm_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( mvm_int_lock );
}

static void mvm_med_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( mvm_med_task_lock );
}

static void mvm_med_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( mvm_med_task_lock );
}

static void mvm_low_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( mvm_low_task_lock );
}

static void mvm_low_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( mvm_low_task_lock );
}

static void mvm_signal_run (
  mvm_thread_priority_enum_t priority
)
{
  switch( priority )
  {
  case MVM_THREAD_PRIORITY_ENUM_HIGH:
    apr_event_signal( mvm_high_task_event );
    break;

  case MVM_THREAD_PRIORITY_ENUM_MED:
    apr_event_signal( mvm_med_task_event );
    break;

  case MVM_THREAD_PRIORITY_ENUM_LOW:
    apr_event_signal( mvm_low_task_event );
    break;

  default:
    break;
  }

  return;
}

static int32_t mvm_get_object
(
  uint32_t handle,
  mvm_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &mvm_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( mvm_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static int32_t mvm_typecast_object (
  apr_objmgr_object_t* store,
  mvm_object_type_enum_t type,
  mvm_object_t** ret_obj
)
{
  mvm_object_t* obj;

  if ( ( store == NULL ) || ( ret_obj == NULL ) )
  {
    return APR_EBADPARAM;
  }

  obj = ( ( mvm_object_t* ) store->any.ptr );

  if ( ( obj == NULL ) || ( obj->header.type != type ) )
  {
    return APR_EFAILED;
  }

  *ret_obj = obj;

  return APR_EOK;
}

static int32_t mvm_get_typed_object (
  uint32_t handle,
  mvm_object_type_enum_t type,
  mvm_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* store;

  rc = apr_objmgr_find_object( &mvm_objmgr, handle, &store );
  if ( rc ) return rc;

  rc = mvm_typecast_object( store, type, ret_obj );
  if ( rc ) return rc;

  return APR_EOK;
}

/****************************************************************************
 * PENDING COMMAND ROUTINES                                                 *
 ****************************************************************************/

static int32_t mvm_pending_control_init (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2( &ctrl->cmd_q, NULL, NULL );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = MVM_PENDING_CMD_STATE_ENUM_FETCH;
  ctrl->packet = NULL;
  ctrl->pendjob_obj = NULL;

  return APR_EOK;
}

static int32_t mvm_pending_control_destroy (
  mvm_pending_control_t* ctrl
)
{
#if 0 /* There's nothing to destroy if everything is in a good state. */
  int32_t rc;
#endif /* 0 */

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

#if 0 /* There's nothing to destroy if everything is in a good state. */
  rc = apr_list_destroy( &ctrl->cmd_q );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = MVM_PENDING_CMD_STATE_ENUM_UNINITIALIZED;
  ctrl->packet = NULL;
  ctrl->pendjob_obj = NULL;
#endif /* 0 */

  return APR_EOK;
}

/****************************************************************************
 * CVD WORK QUEUE ROUTINES                                                  *
 ****************************************************************************/

static void mvm_queue_pending_packet (
  apr_list_t* pending_cmd_q,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_work_item_t* work_item;
  uint16_t client_addr;

  if ( pending_cmd_q == NULL )
  {
    return;
  }

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }

  for ( ;; )
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &mvm_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Queue the incoming command to the pending command queue. We don't
       * need to signal do work because after the incoming command
       * handler is done the pending command handler routine will be
       * called.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( pending_cmd_q, &work_item->link );
    }

    return;
  }

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, packet, rc );
    MVM_COMM_ERROR( rc, client_addr );
  }
}

static void mvm_queue_incoming_packet (
  mvm_thread_priority_enum_t priority,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_work_item_t* work_item;
  apr_list_t* incoming_cmd_q;
  uint16_t client_addr;

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }

  switch ( priority )
  {
  case MVM_THREAD_PRIORITY_ENUM_HIGH:
    incoming_cmd_q = &mvm_high_task_incoming_cmd_q;
    break;

  case MVM_THREAD_PRIORITY_ENUM_MED:
    incoming_cmd_q = &mvm_med_task_incoming_cmd_q;
    break;

  case MVM_THREAD_PRIORITY_ENUM_LOW:
    incoming_cmd_q = &mvm_low_task_incoming_cmd_q;
    break;

  default:
    return;
  }

  for ( ;; )
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &mvm_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Report command acceptance when requested. */
      if ( priority == MVM_THREAD_PRIORITY_ENUM_HIGH )
      {
        rc = __aprv2_cmd_accept_command( mvm_apr_handle, packet );
        if ( rc )
        { /* Can't report so abort the command. */
          ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );
          break;
        }
      }
    }

    { /* Queue the new command to the incoming command queue and signal do
       * work.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( incoming_cmd_q, &work_item->link );

      mvm_signal_run( priority );
    }

    return;
  }

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, packet, rc );
    MVM_COMM_ERROR( rc, client_addr );
  }
}

/****************************************************************************
 * DEFAULT RESPONSE PROCESSING ROUTINES                                     *
 ****************************************************************************/

static void mvm_default_event_rsp_fn (
  aprv2_packet_t* packet
)
{
  /* The default event handler just drops the packet. A specific event
   * handler routine should be written to do something more useful.
   */
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_set_default_response_table (
  mvm_response_fn_table_t table
)
{
  int i;

  if ( table == NULL )
  {
    return;
  }

  /* Initialize the state response handler function table. */
  for ( i = 0; i < MVM_RESPONSE_FN_ENUM_MAX; ++i )
  {
    table[ i ] = mvm_default_event_rsp_fn;
  }
}

static void mvm_simple_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

    /* If completed command failed, log the error. */
    if ( obj->status != APR_EOK )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_simple_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                              APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                              obj->status );
    }
  }

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/* Destroy the simple job object once done. This is primarily useful for
 * analyzing/debugging the flow of jobs in logs.
 */
static void mvm_simple_self_destruct_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* obj;
  uint32_t status;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    /* This rsp function should NOT be used for commands returning anything
     * other than APRV2_IBASIC_RSP_RESULT (i.e. custom responses). However,
     * for robustness' sake, we do this check before interpreting the payload.
     */
    if ( packet->opcode == APRV2_IBASIC_RSP_RESULT )
    {
      status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

      /* If completed command failed, log the error. */
      if ( status != APR_EOK )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_simple_self_destruct_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                                APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                                status );
      }
    }

    ( void ) mvm_free_object( ( mvm_object_t* ) obj );
  }

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/****************************************************************************
 * CUSTOM RESPONSE PROCESSING ROUTINES (NOT FOR STATE MACHINE TRANSITION)   *
 ****************************************************************************/

static void mvm_stream_attach_detach_vocproc_result_count_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;
  uint32_t status;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* If the last attach/detach operation failed, log the error. */
  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_stream_attach_detach_vocproc_result_count_rsp_fn(): Last attach/detach failed with 0x%08X",
                                            status );
  }

  /* Count the number of attach_vocproc responses received from the stream
   * (or the nubmer of streams that have responded).
   */
  session_obj->stream_attach_detach_vocproc_rsp_cnt++;

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_vocproc_dynamic_reconfig_result_count_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;
  uint32_t status;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* If the last vocproc dynamic reconfig failed, log the error. */
  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_vocproc_dynamic_reconfig_result_count_rsp_fn(): Last reconfig failed with 0x%08X",
                                            status );
  }

  session_obj->vocproc_rsp_cnt++;

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_map_memory_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  voice_rsp_shared_mem_map_regions_t* mem_map_rsp;
  mvm_simple_job_object_t* obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APR_EOK;
    mem_map_rsp = APRV2_PKT_GET_PAYLOAD( voice_rsp_shared_mem_map_regions_t, packet );
    obj->context_handle = mem_map_rsp->mem_map_handle;
  }

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/* Response function to count the responses for the commands issued by MVM to
 * itself in each of the cleanup sequencer state, when doing cleanup due to the
 * subsystem where MVM clients reside is being restarted.
 */
static void mvm_ssr_cleanup_cmd_result_count_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  uint32_t command;
  uint32_t status;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* If the last operation failed, log the error. */
  command = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->opcode;
  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ssr_cleanup_cmd_result_count_rsp_fn(): Command 0x%08X failed with 0x%08X",
                                            command, status );
  }

  /* Count the number of command responses received. */
  mvm_ssr_cleanup_cmd_tracking.rsp_cnt++;

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/****************************************************************************
 * CVD OBJECT CREATION AND DESTRUCTION ROUTINES                             *
 ****************************************************************************/

static int32_t mvm_mem_alloc_object (
  uint32_t size,
  mvm_object_t** ret_object
)
{
  int32_t rc;
  mvm_object_t* ctrl;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the new CVD object. */
    ctrl = apr_memmgr_malloc( &mvm_heapmgr, size );
    if ( ctrl == NULL )
    {
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the CVD object. */
    rc = apr_objmgr_alloc_object( &mvm_objmgr, &objmgr_obj );
    if ( rc )
    {
      apr_memmgr_free( &mvm_heapmgr, ctrl );
      return APR_ENORESOURCE;
    }

    /* Link the CVD object to the handle. */
    objmgr_obj->any.ptr = ctrl;

    ( void ) mmstd_memset( ctrl, 0xFD, size );

    /* Initialize the base CVD object header. */
    ctrl->header.handle = objmgr_obj->handle;
    ctrl->header.type = MVM_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = ctrl;

  return APR_EOK;
}

static int32_t mvm_mem_free_object (
  mvm_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &mvm_objmgr, object->header.handle );
  apr_memmgr_free( &mvm_heapmgr, object );

  return APR_EOK;
}

static int32_t mvm_create_session_object (
  mvm_session_object_t** ret_session_obj
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  uint32_t i;

  if ( ret_session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvm_mem_alloc_object( sizeof( mvm_session_object_t ),
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the session object. */
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->lock );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_create_session_object(): " \
                                              "Failed to create lock, rc = (0x%08x)", rc );
      ( void ) mvm_free_object( ( mvm_object_t* )session_obj );
      return APR_ENORESOURCE;
    }

    session_obj->header.type = MVM_OBJECT_TYPE_ENUM_SESSION;

    ( void ) mmstd_memset( session_obj->generic_pool, 0,
                           sizeof( session_obj->generic_pool ) );

    /* Initialize free generic objects queue. */
    ( void ) apr_list_init_v2( &session_obj->free_item_q, NULL, NULL );
    for ( i = 0; i < MVM_MAX_GENERIC_OBJECTS_PER_SESSION_V; ++i )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &session_obj->generic_pool[ i ] );
      ( void ) apr_list_add_tail(
                 &session_obj->free_item_q,
                 ( ( apr_list_node_t* ) &session_obj->generic_pool[i] ) );
    }

    session_obj->self_addr = mvm_my_addr;

    ( void ) mmstd_memset( session_obj->session_name, 0,
                           sizeof( session_obj->session_name ) );

    session_obj->is_dual_control = FALSE;
    session_obj->is_active = FALSE;
    session_obj->is_hdvoice_ui_enabled = MVM_HDVOICE_UI_UNUSED;
    session_obj->is_beamr_enabled = TRUE;
    session_obj->is_wv2_enabled = TRUE;
    session_obj->is_kpps_changed_by_hdvoice = FALSE;

    session_obj->master_addr = APRV2_PKT_INIT_ADDR_V;
    session_obj->master_port = APR_NULL_V;

    /* BACKWARD COMPATIBILITY */
    /* Initialize MVS v1.0 backward compatibility variables. */
    session_obj->is_mvs_v1_policy = FALSE;
    session_obj->default_modem_stream_addr = APR_NULL_V;
    session_obj->default_modem_stream_handle = 0;
    session_obj->is_default_modem_stream_attached = FALSE;

    ( void ) apr_list_init_v2( &session_obj->indirection_q, NULL, NULL );

    /* Initialize the stream list. */
    ( void ) apr_list_init_v2( &session_obj->stream_q, NULL, NULL );
    session_obj->stream_rsp_cnt = 0;

    /* Initialize the vocproc list. */
    ( void ) apr_list_init_v2( &session_obj->vocproc_q, NULL, NULL );
    session_obj->vocproc_rsp_cnt = 0;

    session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;
    session_obj->detach_all_rsp_cnt = 0;

    session_obj->requested_var_voc_rx_sampling_rate = MVM_DEFAULT_VAR_VOC_DEC_SAMPLING_RATE;
    session_obj->requested_var_voc_tx_sampling_rate = MVM_DEFAULT_VAR_VOC_ENC_SAMPLING_RATE;

    session_obj->is_voc_op_mode_evt_received = FALSE;

    session_obj->is_wv_set_by_ui = FALSE;

    /* Initialize the session and stream state machine control variables. */
    session_obj->session_ctrl.statejob_handle = APR_NULL_V;

    session_obj->session_ctrl.state = MVM_STATE_ENUM_RESET;
    session_obj->session_ctrl.goal = MVM_GOAL_ENUM_NONE;
    session_obj->session_ctrl.action = MVM_ACTION_ENUM_NONE;
    session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

    session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay = 0;
    session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay = 0;
    session_obj->avsync_vocproc_delay.max_vp_rx_algorithmic_delay = 0;
    session_obj->avsync_vocproc_delay.max_vp_tx_algorithmic_delay = 0;
    session_obj->is_avtimer_handle_open = FALSE;

    session_obj->mailbox_pktexg_stream_info.is_ready_to_run = FALSE;
    session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed = FALSE;
    session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened = FALSE;
    session_obj->mailbox_pktexg_stream_info.is_disable_rx_expiry_processing = FALSE;
    session_obj->mailbox_pktexg_stream_info.stream_cnt = 0;
    session_obj->mailbox_pktexg_stream_info.get_time_ref_cnt = 0;
    session_obj->mailbox_pktexg_stream_info.time_ref.tx_timstamp_us = 0;
    session_obj->mailbox_pktexg_stream_info.time_ref.rx_timstamp_us = 0;
    session_obj->mailbox_pktexg_stream_info.time_ref.enc_offset_margin_us = 0;
    session_obj->mailbox_pktexg_stream_info.time_ref.dec_req_offset_margin_us = 0;
    session_obj->mailbox_pktexg_stream_info.time_ref.dec_offset_margin_us = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.enc_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.dec_req_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.dec_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.dec_pp_start_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.vp_rx_start_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.vp_rx_delivery_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.vp_tx_start_offset = 0;
    session_obj->mailbox_pktexg_stream_info.timing_params.vp_tx_delivery_offset = 0;
    session_obj->mailbox_pktexg_stream_info.soft_vfr_handle = APR_NULL_V;
    session_obj->mailbox_pktexg_stream_info.soft_vfr_ref_timestamp_us = 0;

    session_obj->active_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
    session_obj->active_set.system_config.media_id = VSS_MEDIA_ID_NONE;
    session_obj->active_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
    session_obj->active_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
    session_obj->active_set.system_config.dec_sr = MVM_DEFAULT_DEC_SR;
    session_obj->active_set.system_config.enc_sr = MVM_DEFAULT_ENC_SR;
    session_obj->active_set.system_config.rx_pp_sr = MVM_DEFAULT_RX_PP_SR;
    session_obj->active_set.system_config.tx_pp_sr = MVM_DEFAULT_TX_PP_SR;
    session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
    session_obj->active_set.system_config.vsid = 0;
    session_obj->active_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
    session_obj->active_set.voice_timing.enc_offset = 0;
    session_obj->active_set.voice_timing.dec_req_offset = 0;
    session_obj->active_set.voice_timing.dec_offset = 0;
    session_obj->active_set.voice_timing.dec_pp_start_offset = 0;
    session_obj->active_set.voice_timing.vp_rx_start_offset = 0;
    session_obj->active_set.voice_timing.vp_rx_delivery_offset = 0;
    session_obj->active_set.voice_timing.vp_tx_start_offset = 0;
    session_obj->active_set.voice_timing.vp_tx_delivery_offset = 0;
    session_obj->active_set.clock_and_concurrency_config.is_multi_session = FALSE;
    session_obj->active_set.clock_and_concurrency_config.total_core_kpps = 0;
    session_obj->active_set.stream_vocproc_load.num_nb_streams = 0;
    session_obj->active_set.stream_vocproc_load.num_wb_streams = 0;
    session_obj->active_set.stream_vocproc_load.num_swb_streams = 0;
    session_obj->active_set.stream_vocproc_load.num_fb_plus_streams = 0;
    session_obj->active_set.stream_vocproc_load.num_nb_vocprocs = 0;
    session_obj->active_set.stream_vocproc_load.num_wb_vocprocs = 0;
    session_obj->active_set.stream_vocproc_load.num_swb_vocprocs = 0;
    session_obj->active_set.stream_vocproc_load.num_fb_plus_vocprocs = 0;
    session_obj->active_set.stream_vocproc_load.total_kpps = 0;
    session_obj->active_set.stream_vocproc_load.vocproc_rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    session_obj->active_set.stream_vocproc_load.vocproc_tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    session_obj->active_set.stream_vocproc_load.stream_media_id = VSS_MEDIA_ID_NONE;
    session_obj->active_set.stream_vocproc_load.enc_kpps = 0;
    session_obj->active_set.stream_vocproc_load.dec_kpps = 0;
    session_obj->active_set.stream_vocproc_load.dec_pp_kpps = 0;
    session_obj->active_set.stream_vocproc_load.vp_rx_kpps = 0;
    session_obj->active_set.stream_vocproc_load.vp_tx_kpps = 0;
    session_obj->active_set.stream_vocproc_load.tx_num_channels = 0;
    session_obj->active_set.stream_vocproc_load.tx_mpps_scale_factor = 0;
    session_obj->active_set.stream_vocproc_load.tx_bw_scale_factor = 0;
    session_obj->active_set.stream_vocproc_load.rx_mpps_scale_factor = 0;
    session_obj->active_set.stream_vocproc_load.rx_bw_scale_factor = 0;
    session_obj->active_set.tty_mode = 0; /* 0 = disable TTY. */
    session_obj->active_set.widevoice_enable = 0; /* 0 = WideVoice disabled. */

    session_obj->target_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
    session_obj->target_set.system_config.media_id = VSS_MEDIA_ID_NONE;
    session_obj->target_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
    session_obj->target_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
    session_obj->target_set.system_config.dec_sr = MVM_DEFAULT_DEC_SR;
    session_obj->target_set.system_config.enc_sr = MVM_DEFAULT_ENC_SR;
    session_obj->target_set.system_config.rx_pp_sr = MVM_DEFAULT_RX_PP_SR;
    session_obj->target_set.system_config.tx_pp_sr = MVM_DEFAULT_TX_PP_SR;
    session_obj->target_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
    session_obj->target_set.system_config.vsid = 0;
    session_obj->target_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
    session_obj->target_set.system_config.call_num = 0;
    session_obj->target_set.voice_timing.enc_offset = 0;
    session_obj->target_set.voice_timing.dec_req_offset = 0;
    session_obj->target_set.voice_timing.dec_offset = 0;
    session_obj->target_set.voice_timing.dec_pp_start_offset = 0;
    session_obj->target_set.voice_timing.vp_rx_start_offset = 0;
    session_obj->target_set.voice_timing.vp_rx_delivery_offset = 0;
    session_obj->target_set.voice_timing.vp_tx_start_offset = 0;
    session_obj->target_set.voice_timing.vp_tx_delivery_offset = 0;
    session_obj->target_set.clock_and_concurrency_config.is_multi_session = FALSE;
    session_obj->target_set.clock_and_concurrency_config.total_core_kpps = 0;
    session_obj->target_set.stream_vocproc_load.num_nb_streams = 0;
    session_obj->target_set.stream_vocproc_load.num_wb_streams = 0;
    session_obj->target_set.stream_vocproc_load.num_swb_streams = 0;
    session_obj->target_set.stream_vocproc_load.num_fb_plus_streams = 0;
    session_obj->target_set.stream_vocproc_load.num_nb_vocprocs = 0;
    session_obj->target_set.stream_vocproc_load.num_wb_vocprocs = 0;
    session_obj->target_set.stream_vocproc_load.num_swb_vocprocs = 0;
    session_obj->target_set.stream_vocproc_load.num_fb_plus_vocprocs = 0;
    session_obj->target_set.stream_vocproc_load.total_kpps = 0;
    session_obj->target_set.stream_vocproc_load.vocproc_rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    session_obj->target_set.stream_vocproc_load.vocproc_tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    session_obj->target_set.stream_vocproc_load.stream_media_id = VSS_MEDIA_ID_NONE;
    session_obj->target_set.stream_vocproc_load.enc_kpps = 0;
    session_obj->target_set.stream_vocproc_load.dec_kpps = 0;
    session_obj->target_set.stream_vocproc_load.dec_pp_kpps = 0;
    session_obj->target_set.stream_vocproc_load.vp_rx_kpps = 0;
    session_obj->target_set.stream_vocproc_load.vp_tx_kpps = 0;
    session_obj->target_set.stream_vocproc_load.tx_num_channels = 0;
    session_obj->target_set.stream_vocproc_load.tx_mpps_scale_factor = 0;
    session_obj->target_set.stream_vocproc_load.tx_bw_scale_factor = 0;
    session_obj->target_set.stream_vocproc_load.rx_mpps_scale_factor = 0;
    session_obj->target_set.stream_vocproc_load.rx_bw_scale_factor = 0;
    session_obj->target_set.tty_mode = 0; /* 0 = disable TTY. */
    session_obj->target_set.widevoice_enable = 0; /* 0 = WideVoice disabled. */

    session_obj->modem_state = MVM_GOAL_ENUM_STOP;
    session_obj->apps_state = MVM_GOAL_ENUM_STOP;

    session_obj->is_stream_reinit_required = FALSE;
    session_obj->is_vocproc_reinit_required = FALSE;
  }

  *ret_session_obj = session_obj;

  return APR_EOK;
}

static int32_t mvm_create_indirection_object (
  uint16_t client_addr,
  char_t* req_session_name,
  uint32_t req_session_name_size,
  uint32_t access_bits,
  mvm_indirection_object_t** ret_indirect_obj,
  mvm_session_object_t** ret_session_obj
)
{
  int32_t rc;
  uint32_t result;
  mvm_indirection_object_t* indirect_obj;
  mvm_session_object_t* session_obj = NULL;
  mvm_generic_item_t* generic_item;

  if ( ( ret_indirect_obj == NULL ) ||
       ( ret_session_obj == NULL )  ||
       ( req_session_name_size > MVM_MAX_SESSION_NAME_SIZE ) )
  {
    return APR_EBADPARAM;
  }

  /* See if a session with this name has already been created. */
  if ( req_session_name_size > 0 )
  {
    generic_item = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &mvm_session_q,
                              ( ( apr_list_node_t* ) generic_item ),
                              ( ( apr_list_node_t** ) &generic_item ) );
      if ( rc ) break;

      rc = mvm_get_typed_object( generic_item->handle,
                                 MVM_OBJECT_TYPE_ENUM_SESSION,
                                 ( ( mvm_object_t** ) &session_obj ) );
      MVM_PANIC_ON_ERROR( rc );

      if ( mmstd_strncmp( session_obj->session_name,
                          sizeof( session_obj->session_name ),
                          req_session_name,
                          req_session_name_size )
           == 0 )
      {
        /* Found a session with the requested name. */
        break;
      }
      else
      {
        /* Keep looking. */
        session_obj = NULL;
      }
    }
  }

  rc = mvm_mem_alloc_object( sizeof( mvm_indirection_object_t ),
                             ( ( mvm_object_t** ) &indirect_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  if ( session_obj == NULL )
  { /* Didn't find a session with the requested name, so create a new session. */
    if ( mvm_session_q.size >= MVM_MAX_SESSIONS )
    {
      rc = mvm_free_object( ( mvm_object_t* ) indirect_obj );
      MVM_PANIC_ON_ERROR( rc );

      return APR_ENORESOURCE;
    }

    rc = mvm_create_session_object( &session_obj );
    if ( rc )
    {
      rc = mvm_free_object( ( mvm_object_t* ) indirect_obj );
      MVM_PANIC_ON_ERROR( rc );

      return APR_ENORESOURCE;
    }

    if ( req_session_name != NULL )
    {
      /* Save the session name. */
      uint32_t dst_len = MMSTD_MIN( sizeof( session_obj->session_name ),
                                    req_session_name_size );
      result = mmstd_strlcpy( session_obj->session_name, req_session_name,
                              dst_len );

      if ( result >= dst_len )
      { /* Truncation happened. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_create_indirection_object: ERROR, input truncation" );

        rc = mvm_free_object( ( mvm_object_t* ) session_obj );
        MVM_PANIC_ON_ERROR( rc );
        rc = mvm_free_object( ( mvm_object_t* ) indirect_obj );
        MVM_PANIC_ON_ERROR( rc );

        return APR_EBADPARAM;
      }
    }

    /* Add the new session to the session tracking list. */
    rc = apr_list_remove_head( &mvm_session_list_free_q,
                               ( ( apr_list_node_t** ) &generic_item ) );

    generic_item->handle = session_obj->header.handle;

    ( void ) apr_list_add_tail( &mvm_session_q, &generic_item->link );
  }

  { /* Initialize the indirection object. */
    indirect_obj->header.type = MVM_OBJECT_TYPE_ENUM_INDIRECTION;

    indirect_obj->client_addr = client_addr;
    indirect_obj->session_handle = session_obj->header.handle;
    indirect_obj->access_bits = access_bits;
  }

  { /* Associate the indirection object to the session object. */
    rc = apr_list_remove_head( &session_obj->free_item_q,
                               ( ( apr_list_node_t** ) &generic_item ) );
    MVM_PANIC_ON_ERROR( rc );

    generic_item->handle = indirect_obj->header.handle;

    ( void ) apr_list_add_tail( &session_obj->indirection_q, &generic_item->link );
  }

  *ret_indirect_obj = indirect_obj;
  *ret_session_obj = session_obj;

  return APR_EOK;
}

/* This function assumes the input parameters are good. */
static int32_t mvm_find_object_from_session (
  apr_list_t* list,
  uint32_t handle,
  mvm_generic_item_t** ret_generic_item
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item;

  generic_item = ( ( mvm_generic_item_t* ) &list->dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( list,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc )
    {
      return APR_EFAILED;
    }

    if ( generic_item->handle == handle )
    {
      break;
    }
  }

  *ret_generic_item = generic_item;

  return APR_EOK;
}

static int32_t mvm_destroy_indirection_object (
  mvm_indirection_object_t* indirect_obj
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_generic_item_t* generic_item;

  rc = mvm_get_typed_object( indirect_obj->session_handle,
                             MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

 /* Disassociate the indirection object from the session object. */
  rc = mvm_find_object_from_session( &session_obj->indirection_q,
                                     indirect_obj->header.handle,
                                     &generic_item );
  MVM_PANIC_ON_ERROR( rc );

  ( void ) apr_list_delete( &session_obj->indirection_q, &generic_item->link );
  ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );

  if ( session_obj->indirection_q.size == 0 )
  { /* Free the session object when there are no more references and
     * an remove from global sessions list.
     */
    generic_item = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );
    for ( ;; )
    {
      rc = apr_list_get_next( &mvm_session_q,
                              ( ( apr_list_node_t* ) generic_item ),
                              ( ( apr_list_node_t** ) &generic_item ) );
      if ( rc )
        break;

      if ( generic_item->handle == session_obj->header.handle )
      {
        apr_list_delete ( &mvm_session_q, ( apr_list_node_t* ) generic_item );
        apr_list_add_tail( &mvm_session_list_free_q, &generic_item->link );
        break;
      }
    }

    ( void ) mvm_free_object( ( mvm_object_t* ) session_obj );
  }

  return APR_EOK;
}

static int32_t mvm_destroy_session_object (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item;
  mvm_indirection_object_t* indirect_object;

  /* Remove session object. */
  generic_item = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &mvm_session_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc )
      break;

    if ( generic_item->handle == session_obj->header.handle )
    {
      apr_list_delete ( &mvm_session_q, ( apr_list_node_t* ) generic_item );
      apr_list_add_tail( &mvm_session_list_free_q, &generic_item->link );
      break;
    }
  }

  /* Remove indirect object. */
  generic_item = ( ( mvm_generic_item_t* )&session_obj->indirection_q.dummy );
  rc = apr_list_get_next( &session_obj->indirection_q,
                          ( ( apr_list_node_t* ) generic_item ),
                          ( ( apr_list_node_t** ) &generic_item ) );
  if ( rc )
  {
    /* At this point, we should have exact one item here. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_destroy_session_object(): " \
           "failed to get indirect object, rc = (0x%08x)", rc );
  }
  else
  {
    ( void ) mvm_get_typed_object( generic_item->handle,
                                   MVM_OBJECT_TYPE_ENUM_INDIRECTION,
                                  ( ( mvm_object_t** ) &indirect_object ) );
    ( void ) apr_list_delete( &session_obj->indirection_q, &generic_item->link );
    ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
    ( void ) mvm_mem_free_object( ( mvm_object_t* ) indirect_object );
  }

  ( void )apr_lock_destroy( session_obj->lock );

  ( void )mvm_free_object( ( mvm_object_t* ) session_obj );

  return APR_EOK;
}

static int32_t mvm_create_simple_job_object (
  uint32_t context_handle,
  mvm_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvm_mem_alloc_object( sizeof( mvm_simple_job_object_t ),
                             ( ( mvm_object_t** ) &job_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    job_obj->header.type = MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB;

    job_obj->context_handle = context_handle;
    mvm_set_default_response_table( job_obj->fn_table );
    job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] = mvm_simple_result_rsp_fn;
    job_obj->is_accepted = FALSE;
    job_obj->is_completed = FALSE;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t mvm_create_sequencer_job_object (
  mvm_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  mvm_sequencer_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = mvm_mem_alloc_object( sizeof( mvm_sequencer_job_object_t ),
                             ( ( mvm_object_t** ) &job_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = MVM_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t mvm_free_object (
  mvm_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Perform object-specific clean-up necessary. */
  switch ( object->header.type )
  {
  case MVM_OBJECT_TYPE_ENUM_UNINITIALIZED:
    break;

  case MVM_OBJECT_TYPE_ENUM_SESSION:
    break;

  case MVM_OBJECT_TYPE_ENUM_INDIRECTION:
    ( void ) mvm_destroy_indirection_object( &object->indirection );
    break;

  case MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB:
    break;

  case MVM_OBJECT_TYPE_ENUM_SEQUENCER_JOB:
    break;

  case MVM_OBJECT_TYPE_ENUM_INVALID:
    break;

  default:
    break;
  }

  /* Free the object memory and object handle. */
  ( void ) mvm_mem_free_object( object );

  return APR_EOK;
}

/****************************************************************************
 * MVM SUBSYSTEM RESTART (SSR) HELPER FUNCTIONS                             *
 ****************************************************************************/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CLEANUP HELPER FUNCTIONS                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* Stops each of the MVM session on behalf of MVM clients who reside in a
 * subsystem that is being restarted.
 *
 * TODO: Current implementation only handles Modem SSR properly and it issues
 * VSS_IMVM_CMD_MODEM_STOP_VOICE on behalf of Modem full control clients. In
 * the future, we need to save the client address whenever the client
 * "enables"/"starts" a voice feature, and "disable"/"stop" the feature on
 * behalf of the client, once we know that the client's subsystem is being
 * restarted.
 */
static int32_t mvm_ssr_stop_voice (
  uint8_t domain_id
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item_1;
  mvm_generic_item_t* generic_item_2;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirection_obj;
  mvm_simple_job_object_t* job_obj;

  mvm_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  mvm_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &mvm_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = mvm_get_typed_object( generic_item_1->handle,
                               MVM_OBJECT_TYPE_ENUM_SESSION,
                               ( ( mvm_object_t** ) &session_obj ) );
    MVM_PANIC_ON_ERROR( rc );

    generic_item_2 = ( ( mvm_generic_item_t* ) &session_obj->indirection_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->indirection_q,
                              ( ( apr_list_node_t* ) generic_item_2 ),
                              ( ( apr_list_node_t** ) &generic_item_2 ) );
      if ( rc ) break;

      rc = mvm_get_typed_object( generic_item_2->handle,
                                 MVM_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( mvm_object_t** ) &indirection_obj ) );

      if ( ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                            indirection_obj->client_addr ) == domain_id ) &&
           ( APR_GET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                            indirection_obj->access_bits ) ) )
      {
        rc = mvm_create_simple_job_object( APR_NULL_V, &job_obj );
        MVM_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
          mvm_ssr_cleanup_cmd_result_count_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_my_addr, ( ( uint16_t ) indirection_obj->header.handle ),
               job_obj->header.handle,
               VSS_IMVM_CMD_MODEM_STOP_VOICE,
               NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_my_addr );

        mvm_ssr_cleanup_cmd_tracking.num_cmd_issued++;

        break;
      }
    }
  }

  return APR_EOK;
}

/* For each of the MVM sessions, detach the streams currently attached to it on
 * behalf of MVM clients who reside in a subsystem that is being restarted.
 *
 * TODO: Current implementation only handles Modem SSR properly. It assumes
 * that for dual controlled voice use case, only MVM full control Modem clients
 * will attach streams to MVM. Based on this assumption, the current
 * implementation detaches all the streams attached to each MVM session on
 * behalf of full control MVM clients who reside in a subsystem that is being
 * restarted. In the future, whenever the client attaches a stream, we need to
 * save the client address, and detach the stream once we know that the
 * client's subsystem is being restarted.
 */
static int32_t mvm_ssr_detach_stream (
  uint8_t domain_id
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item_1;
  mvm_generic_item_t* generic_item_2;
  mvm_generic_item_t* generic_item_3;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirection_obj;
  mvm_simple_job_object_t* job_obj;
  vss_imvm_cmd_detach_stream_t detach_stream;

  mvm_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  mvm_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &mvm_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = mvm_get_typed_object( generic_item_1->handle,
                               MVM_OBJECT_TYPE_ENUM_SESSION,
                               ( ( mvm_object_t** ) &session_obj ) );
    MVM_PANIC_ON_ERROR( rc );

    generic_item_2 = ( ( mvm_generic_item_t* ) &session_obj->indirection_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->indirection_q,
                              ( ( apr_list_node_t* ) generic_item_2 ),
                              ( ( apr_list_node_t** ) &generic_item_2 ) );
      if ( rc ) break;

      rc = mvm_get_typed_object( generic_item_2->handle,
                                 MVM_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( mvm_object_t** ) &indirection_obj ) );

      if ( ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                            indirection_obj->client_addr ) == domain_id ) &&
           ( APR_GET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                            indirection_obj->access_bits ) ) )
      {
        generic_item_3 = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );

        for ( ;; )
        {
          rc = apr_list_get_next( &session_obj->stream_q,
                                  ( ( apr_list_node_t* ) generic_item_3 ),
                                  ( ( apr_list_node_t** ) &generic_item_3 ) );
          if ( rc ) break;

          detach_stream.handle = ( ( uint16_t ) generic_item_3->handle );

          rc = mvm_create_simple_job_object( APR_NULL_V, &job_obj );
          MVM_PANIC_ON_ERROR( rc );
          job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
            mvm_ssr_cleanup_cmd_result_count_rsp_fn;

          rc = __aprv2_cmd_alloc_send(
                 mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvm_my_addr, APR_NULL_V,
                 mvm_my_addr, ( ( uint16_t ) indirection_obj->header.handle ),
                 job_obj->header.handle,
                 VSS_IMVM_CMD_DETACH_STREAM,
                 &detach_stream, sizeof( detach_stream ) );
          MVM_COMM_ERROR( rc, mvm_my_addr );

          mvm_ssr_cleanup_cmd_tracking.num_cmd_issued++;
        }

        break;
      }
    }
  }

  return APR_EOK;
}

/* Destroy the MVM session control handles (indirection handles) on behalf of
 * MVM clients who reside in a subsystem that is being restarted.
 */
static int32_t mvm_ssr_destroy_session (
  uint8_t domain_id
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item_1;
  mvm_generic_item_t* generic_item_2;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirection_obj;
  mvm_simple_job_object_t* job_obj;

  mvm_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  mvm_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  generic_item_1 = ( ( mvm_generic_item_t* ) &mvm_session_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &mvm_session_q,
                            ( ( apr_list_node_t* ) generic_item_1 ),
                            ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    rc = mvm_get_typed_object( generic_item_1->handle,
                               MVM_OBJECT_TYPE_ENUM_SESSION,
                               ( ( mvm_object_t** ) &session_obj ) );
    MVM_PANIC_ON_ERROR( rc );

    generic_item_2 = ( ( mvm_generic_item_t* ) &session_obj->indirection_q.dummy );

    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->indirection_q,
                              ( ( apr_list_node_t* ) generic_item_2 ),
                              ( ( apr_list_node_t** ) &generic_item_2 ) );
      if ( rc ) break;

      rc = mvm_get_typed_object( generic_item_2->handle,
                                 MVM_OBJECT_TYPE_ENUM_INDIRECTION,
                                 ( ( mvm_object_t** ) &indirection_obj ) );

      if ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                          indirection_obj->client_addr ) == domain_id )
      {
        rc = mvm_create_simple_job_object( APR_NULL_V, &job_obj );
        MVM_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
          mvm_ssr_cleanup_cmd_result_count_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_my_addr, ( ( uint16_t ) indirection_obj->header.handle ),
               job_obj->header.handle,
               APRV2_IBASIC_CMD_DESTROY_SESSION,
               NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_my_addr );

        mvm_ssr_cleanup_cmd_tracking.num_cmd_issued++;
      }
    }
  }

  return APR_EOK;
}

/* Unmap memory on behalf of MVM clients who reside in a subsystem that is
 * being restarted.
 */
static int32_t mvm_ssr_unmap_memory (
  uint8_t domain_id
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_ssr_mem_handle_tracking_item_t* mem_handle_tracking_item;
  vss_imemory_cmd_unmap_t mem_unmap;

  mvm_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  mvm_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

  mem_handle_tracking_item =
    ( ( mvm_ssr_mem_handle_tracking_item_t* )
      &mvm_ssr_mem_handle_tracking.used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next(
           &mvm_ssr_mem_handle_tracking.used_q,
           ( ( apr_list_node_t* ) mem_handle_tracking_item ),
           ( ( apr_list_node_t** ) &mem_handle_tracking_item ) );
    if ( rc ) break;

    if ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                        mem_handle_tracking_item->client_addr ) == domain_id )
    {
      rc = mvm_create_simple_job_object( APR_NULL_V, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_ssr_cleanup_cmd_result_count_rsp_fn;

      mem_unmap.mem_handle = mem_handle_tracking_item->mem_handle;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvm_my_addr, APR_NULL_V,
             mvm_my_addr, APR_NULL_V,
             job_obj->header.handle,
             VSS_IMEMORY_CMD_UNMAP,
             &mem_unmap, sizeof( mem_unmap ) );
      MVM_COMM_ERROR( rc, mvm_my_addr );

      mvm_ssr_cleanup_cmd_tracking.num_cmd_issued++;
    }
  }

  return APR_EOK;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * SSR CALLBACKS                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static void mvm_ssr_modem_after_shutdown_handler ( void )
{
  int32_t rc;
  vss_issr_cmd_cleanup_t cleanup_arg;
  mvm_simple_job_object_t* job_obj;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ssr_modem_after_shutdown_handler is called." );

  rc = mvm_create_simple_job_object( APR_NULL_V, &job_obj );
  MVM_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_self_destruct_result_rsp_fn;

  cleanup_arg.domain_id = APRV2_IDS_DOMAIN_ID_MODEM_V;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvm_my_addr, APR_NULL_V,
         mvm_my_addr, APR_NULL_V,
         job_obj->header.handle,
         VSS_ISSR_CMD_CLEANUP,
         &cleanup_arg, sizeof( cleanup_arg ) );
  MVM_COMM_ERROR( rc, mvm_my_addr );
}

static void mvm_ssr_modem_after_powerup_handler ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_ssr_modem_after_powerup_handler() is called." );
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * MEM HANDLE TRACKING FUNCTIONS                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static int32_t mvm_ssr_track_mem_handle (
  uint16_t client_addr,
  uint32_t mem_handle
)
{
  int32_t rc;
  mvm_ssr_mem_handle_tracking_item_t* mem_handle_tracking_item;

  for ( ;; )
  {
    rc = apr_list_remove_head( &mvm_ssr_mem_handle_tracking.free_q,
                               ( ( apr_list_node_t** ) &mem_handle_tracking_item ) );
    if ( rc ) break;

    mem_handle_tracking_item->client_addr = client_addr;
    mem_handle_tracking_item->mem_handle = mem_handle;

    ( void ) apr_list_add_tail( &mvm_ssr_mem_handle_tracking.used_q,
                                &mem_handle_tracking_item->link );

    break;
  }

  return rc;
}

static int32_t mvm_ssr_untrack_mem_handle (
  uint32_t mem_handle
)
{
  int32_t rc;
  mvm_ssr_mem_handle_tracking_item_t* mem_handle_tracking_item;

  mem_handle_tracking_item =
    ( ( mvm_ssr_mem_handle_tracking_item_t* )
      &mvm_ssr_mem_handle_tracking.used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next(
           &mvm_ssr_mem_handle_tracking.used_q,
           ( ( apr_list_node_t* ) mem_handle_tracking_item ),
           ( ( apr_list_node_t** ) &mem_handle_tracking_item ) );
    if ( rc ) break;

    if ( mem_handle_tracking_item->mem_handle == mem_handle )
    {
      ( void ) apr_list_delete(
                 &mvm_ssr_mem_handle_tracking.used_q,
                 &mem_handle_tracking_item->link );
      ( void ) apr_list_add_tail(
                 &mvm_ssr_mem_handle_tracking.free_q,
                 &mem_handle_tracking_item->link );
      break;
    }
  }

  return rc;
}

/****************************************************************************
 * MVM SYSTEM CONFIGURATION-RELATED HELPER FUNCTIONS                        *
 ****************************************************************************/

static bool_t mvm_network_id_is_valid (
  uint32_t network_id
)
{
  bool_t rc;

  switch ( network_id )
  {
  case VSS_ICOMMON_CAL_NETWORK_ID_NONE:
  case VSS_ICOMMON_CAL_NETWORK_ID_CDMA:
  case VSS_ICOMMON_CAL_NETWORK_ID_GSM:
  case VSS_ICOMMON_CAL_NETWORK_ID_WCDMA:
  case VSS_ICOMMON_CAL_NETWORK_ID_VOIP:
  case VSS_ICOMMON_CAL_NETWORK_ID_LTE:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_NETWORK_ID_NONE is considered a valid parameter because system can
   * run without a network id.
   */

  return rc;
}

static bool_t mvm_media_id_is_valid (
  uint32_t media_id
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_13K:
  case VSS_MEDIA_ID_EVRC:
  case VSS_MEDIA_ID_4GV_NB:
  case VSS_MEDIA_ID_4GV_WB:
  case VSS_MEDIA_ID_4GV_NW:
  case VSS_MEDIA_ID_4GV_NW2K:
  case VSS_MEDIA_ID_AMR_NB:
  case VSS_MEDIA_ID_AMR_WB:
  case VSS_MEDIA_ID_EAMR:
  case VSS_MEDIA_ID_EFR:
  case VSS_MEDIA_ID_FR:
  case VSS_MEDIA_ID_HR:
  case VSS_MEDIA_ID_PCM_8_KHZ:
  case VSS_MEDIA_ID_PCM_16_KHZ:
  case VSS_MEDIA_ID_PCM_32_KHZ:
  case VSS_MEDIA_ID_PCM_48_KHZ:
  case VSS_MEDIA_ID_G711_ALAW:
  case VSS_MEDIA_ID_G711_MULAW:
  case VSS_MEDIA_ID_G711_LINEAR:
  case VSS_MEDIA_ID_G729:
  case VSS_MEDIA_ID_G722:
  case VSS_MEDIA_ID_EVS:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_MEDIA_ID_NONE is not considered a valid parameter because
   * system cannot run without a media type.
   */

  return rc;
}

static bool_t mvm_media_id_is_nb_sr (
  uint32_t media_id
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_13K:
  case VSS_MEDIA_ID_EVRC:
  case VSS_MEDIA_ID_4GV_NB:
  case VSS_MEDIA_ID_AMR_NB:
  case VSS_MEDIA_ID_EFR:
  case VSS_MEDIA_ID_FR:
  case VSS_MEDIA_ID_HR:
  case VSS_MEDIA_ID_PCM_8_KHZ:
  case VSS_MEDIA_ID_G711_ALAW:
  case VSS_MEDIA_ID_G711_MULAW:
  case VSS_MEDIA_ID_G711_LINEAR:
  case VSS_MEDIA_ID_G729:
  case VSS_MEDIA_ID_G722:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_MEDIA_ID_NONE is not considered a valid parameter because
   * system cannot run without a media type.
   */

  return rc;
}

static bool_t mvm_media_id_is_var_sr (
  uint32_t media_id /* Assumes the media_id is valid. */
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_EVS:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t mvm_var_voc_sr_is_valid (
  uint32_t sr
)
{
  bool_t rc;

  switch ( sr )
  {
  case 0: /* 0 is Defaulted to 48 kHz for EVS */
  case 8000:
  case 16000:
  case 32000:
  case 41000:
  case 48000:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t mvm_voc_op_mode_is_valid (
  uint32_t voc_op_mode
)
{
  bool_t rc;

  switch ( voc_op_mode )
  {
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_SWB:
  case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_FB:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY */
/* Convert legacy network id to new network id + sample rates.
 * (Also validate legacy network id in the process.)
 */
static int32_t mvm_convert_legacy_network_id (
  uint32_t legacy_network_id,
  uint32_t* ret_network_id,
  uint32_t* ret_rx_pp_sr,
  uint32_t* ret_tx_pp_sr
)
{
  int32_t rc;

  if ( ret_network_id == NULL )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  switch ( legacy_network_id )
  {
  case VSS_NETWORK_ID_DEFAULT:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
      *ret_rx_pp_sr = MVM_DEFAULT_RX_PP_SR;
      *ret_tx_pp_sr = MVM_DEFAULT_TX_PP_SR;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  default:
    rc = APR_EBADPARAM;
    break;
  }

  return rc;
}

static int32_t mvm_determine_closest_supported_pp_sr (
  uint32_t enc_dec_sr,
  uint32_t* ret_pp_sr
)
{
  switch ( enc_dec_sr )
  {
  case 8000:
    *ret_pp_sr = 8000;
    break;

  case 16000:
    *ret_pp_sr = 16000;
    break;

  case 32000:
    *ret_pp_sr = 32000;
    break;  
  
  case 41000:
  case 48000:
    *ret_pp_sr = 48000;
    break;

  default:
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  return APR_EOK;
}

/* Determine the target_set enc, dec sampling rates for the EVS media
type. Parameters ret_dec_sampling_rate and ret_enc_sampling_rate should
be non-NULL*/
static int32_t mvm_evs_sanitize_sampling_rates (
  uint32_t requested_rx_sampling_rate,
  uint32_t requested_tx_sampling_rate,
  uint32_t* ret_dec_sampling_rate,
  uint32_t* ret_enc_sampling_rate
)
{
  int32_t rc = APR_EOK;

  if ( ( ret_dec_sampling_rate == NULL ) || ( ret_enc_sampling_rate == NULL ) )
  {
    return APR_EBADPARAM;
  }

  switch ( requested_rx_sampling_rate )
  {
    case 8000:
      {
        *ret_dec_sampling_rate = 8000;
        break;
      }
    case 16000:
      {
        *ret_dec_sampling_rate = 16000;
        break;
      }
    case 32000:
      {
        *ret_dec_sampling_rate = 32000;
        break;
      }    
    case 0:
    case 48000:
      {
        *ret_dec_sampling_rate = 48000;
        break;
      }
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_evs_sanitize_sampling_rates: " \
                                                "Invalid Rx Sampling Rate %d",
          requested_rx_sampling_rate );
        rc = APR_EBADPARAM;
        break;
      }
  }

  switch ( requested_tx_sampling_rate )
  {
    case 8000:
      {
        *ret_enc_sampling_rate = 8000;
        break;
      }
    case 16000:
      {
        *ret_enc_sampling_rate = 16000;
        break;
      }
    case 32000:
      {
        *ret_enc_sampling_rate = 32000;
        break;
      }    
    case 0:
    case 48000:
      {
        *ret_enc_sampling_rate = 48000;
        break;
      }
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_evs_sanitize_sampling_rates: " \
                                                "Invalid Tx Sampling Rate %d",
          requested_tx_sampling_rate );
        rc = APR_EBADPARAM;
        break;
      }
  }

  return rc;
}

/* Determine the target_set enc, dec and pp sampling rates based on the
 * media type and the client requested variable vocoder sampling rates.
 */
static int32_t mvm_determine_target_sampling_rates (
  uint32_t media_id,
  uint32_t requested_var_voc_rx_sampling_rate,
  uint32_t requested_var_voc_tx_sampling_rate,
  uint32_t* ret_dec_sr,
  uint32_t* ret_enc_sr,
  uint32_t* ret_rx_pp_sr,
  uint32_t* ret_tx_pp_sr
)
{
  int32_t rc = APR_EOK;

  if ( ( ret_dec_sr == NULL ) || ( ret_enc_sr == NULL ) ||
       ( ret_rx_pp_sr == NULL ) || ( ret_tx_pp_sr == NULL ) )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  for ( ;; )
  {
    if ( mvm_media_id_is_valid( media_id ) == FALSE ||
         mvm_var_voc_sr_is_valid( requested_var_voc_rx_sampling_rate ) == FALSE ||
         mvm_var_voc_sr_is_valid( requested_var_voc_tx_sampling_rate ) == FALSE )
    {
      rc = APR_EBADPARAM;
      break;
    }

    /* First determine the enc/dec sampling rates. */
    switch ( media_id )
    {
    case VSS_MEDIA_ID_13K:
    case VSS_MEDIA_ID_EVRC:
    case VSS_MEDIA_ID_4GV_NB:
    case VSS_MEDIA_ID_AMR_NB:
    case VSS_MEDIA_ID_EFR:
    case VSS_MEDIA_ID_FR:
    case VSS_MEDIA_ID_HR:
    case VSS_MEDIA_ID_PCM_8_KHZ:
    case VSS_MEDIA_ID_G711_ALAW:
    case VSS_MEDIA_ID_G711_MULAW:
    case VSS_MEDIA_ID_G711_LINEAR:
    case VSS_MEDIA_ID_G729:
    case VSS_MEDIA_ID_G722:
      {
        *ret_dec_sr = 8000;
        *ret_enc_sr = 8000;
      }
      break;

    case VSS_MEDIA_ID_4GV_WB:
    case VSS_MEDIA_ID_4GV_NW:
    case VSS_MEDIA_ID_4GV_NW2K:
    case VSS_MEDIA_ID_AMR_WB:
    case VSS_MEDIA_ID_EAMR:
    case VSS_MEDIA_ID_PCM_16_KHZ:
      {
        *ret_dec_sr = 16000;
        *ret_enc_sr = 16000;
      }
      break;

    case VSS_MEDIA_ID_PCM_32_KHZ:
      {
        *ret_dec_sr = 32000;
        *ret_enc_sr = 32000;
      }
      break;
      
    case VSS_MEDIA_ID_PCM_48_KHZ:
      {
        *ret_dec_sr = 48000;
        *ret_enc_sr = 48000;
      }
      break;

    case VSS_MEDIA_ID_EVS:
      {
        rc = mvm_evs_sanitize_sampling_rates( requested_var_voc_rx_sampling_rate,
                                              requested_var_voc_tx_sampling_rate,
                                                                  ret_dec_sr, ret_enc_sr );
      }
      break;

    default:
      {
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        rc = APR_EBADPARAM;
      }
      break;
    }

    if ( rc )
      break;

    /* Now determine the supported closest mataching higher sample rates
     * for the pp based on the enc/dec sample rates.
     */
    rc = mvm_determine_closest_supported_pp_sr( *ret_dec_sr, ret_rx_pp_sr );
    if ( rc )
    {
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    rc = mvm_determine_closest_supported_pp_sr( *ret_enc_sr, ret_tx_pp_sr );
    if ( rc )
    {
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return rc;
}

/****************************************************************************
 * MVM STATE MACHINE HELPER FUNCTIONS                                       *
 ****************************************************************************/

static int32_t mvm_do_complete_goal (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  aprv2_ibasic_rsp_result_t res;

  /* Complete the pending command and stay in the same state. */
  res.opcode = APR_UNDEFINED_ID_V;
  res.status = session_obj->session_ctrl.status;
  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ), /* This src_port is used for debugging only. */
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ), /* This dst_port is used for debugging only. */
         session_obj->session_ctrl.statejob_handle,
         APRV2_IBASIC_RSP_RESULT, &res, sizeof( res ) );
  MVM_COMM_ERROR( rc, session_obj->self_addr );

  session_obj->session_ctrl.statejob_handle = APR_NULL_V;
  session_obj->session_ctrl.goal = MVM_GOAL_ENUM_NONE;
  session_obj->session_ctrl.action = MVM_ACTION_ENUM_NONE;
  session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

  return APR_EOK;
}

static void mvm_log_state_info (
  mvm_session_object_t* session_obj
)
{
  static mvm_state_enum_t prev_state = MVM_STATE_ENUM_UNINITIALIZED;
  static mvm_goal_enum_t prev_goal = MVM_GOAL_ENUM_UNINITIALIZED;
  static mvm_action_enum_t prev_action = MVM_ACTION_ENUM_UNINITIALIZED;

  if ( session_obj == NULL )
    return;

  /* Show new state information. */
  if ( ( prev_state != session_obj->session_ctrl.state ) ||
       ( prev_goal != session_obj->session_ctrl.goal ) ||
       ( prev_action != session_obj->session_ctrl.action ) )
  {
    /* Update information. */
    prev_state = session_obj->session_ctrl.state;
    prev_goal = session_obj->session_ctrl.goal;
    prev_action = session_obj->session_ctrl.action;

    /* Log goal. */
    switch ( session_obj->session_ctrl.goal )
    {
    case MVM_GOAL_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_NONE", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_CREATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_CREATE", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_DESTROY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_DESTROY", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_STOP:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_STOP", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_PAUSE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_PAUSE", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_STANDBY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_STANDBY", session_obj->header.handle );
      break;

    case MVM_GOAL_ENUM_START:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_GOAL_ENUM_START", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, MVM_GOAL_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log state. */
    switch ( session_obj->session_ctrl.state )
    {
    case MVM_STATE_ENUM_RESET_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_RESET_ENTRY", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_RESET:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_RESET", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_INIT_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_INIT_ENTRY", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_INIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_INIT", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_IDLE_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_IDLE_ENTRY", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_IDLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_IDLE", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_RUN_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_RUN_ENTRY", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_RUN:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_RUN", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_ERROR_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_ERROR_ENTRY", session_obj->header.handle );
      break;

    case MVM_STATE_ENUM_ERROR:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_STATE_ENUM_ERROR", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, MVM_STATE_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log action. */
    switch ( session_obj->session_ctrl.action )
    {
    case MVM_ACTION_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_NONE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_COMPLETE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_COMPLETE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_CONTINUE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_CONTINUE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_CREATE_SESSION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_CREATE_SESSION", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_RECONFIG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_RECONFIG", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_ENABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_ENABLE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_RECONFIG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_RECONFIG", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_RUN:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_RUN", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_STOP:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_STOP", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_DISABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_DISABLE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_DETACH:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_DETACH", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_DESTROY_SESSION:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_DESTROY_SESSION", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_REINIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_REINIT", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_REINIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_REINIT", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_RESET_STREAM_VOCPROC_LOAD_COUNT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_RESET_STREAM_VOCPROC_LOAD_COUNT", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_SET_TIMING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_SET_TIMING", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_SET_TIMING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_SET_TIMING", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_UPDATE_ACTIVE_SET:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_UPDATE_ACTIVE_SET", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_SET_AVSYNC_DELAYS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_SET_AVSYNC_DELAYS", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_OPEN_SOFT_VFR_FOR_MAILBOX_PKTEXG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_OPEN_SOFT_VFR_FOR_MAILBOX_PKTEXG", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_REQUEST_HIGH_CLOCK:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_REQUEST_HIGH_CLOCK", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_REVERT_HIGH_CLOCK:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_REVERT_HIGH_CLOCK", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_HDVOICE_CONFIG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_HDVOICE_CONFIG", session_obj->header.handle );
      break;

    case MVM_ACTION_ENUM_VAR_VOC_STREAM_SAMP_RATE_CONFIG:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, MVM_ACTION_ENUM_VAR_VOC_STREAM_SAMP_RATE_CONFIG", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, MVM_ACTION_ENUM_INVALID", session_obj->header.handle );
      break;
    }
  }
}

static bool_t mvm_is_stream_vocproc_reconfig_needed (
  mvm_session_object_t* session_obj
)
{
  return
    (
      ( session_obj->active_set.system_config.network_id !=
        session_obj->target_set.system_config.network_id ) ||

      ( session_obj->active_set.system_config.media_id !=
        session_obj->target_set.system_config.media_id ) ||

      ( session_obj->active_set.system_config.rx_voc_op_mode !=
        session_obj->target_set.system_config.rx_voc_op_mode ) ||

      ( session_obj->active_set.system_config.tx_voc_op_mode !=
        session_obj->target_set.system_config.tx_voc_op_mode ) ||

      ( session_obj->active_set.system_config.rx_pp_sr !=
        session_obj->target_set.system_config.rx_pp_sr ) ||

      ( session_obj->active_set.system_config.tx_pp_sr !=
        session_obj->target_set.system_config.tx_pp_sr ) ||

      ( session_obj->active_set.system_config.feature !=
        session_obj->target_set.system_config.feature ) ||

      ( session_obj->active_set.system_config.dec_sr !=
        session_obj->target_set.system_config.dec_sr ) ||

      ( session_obj->active_set.system_config.enc_sr !=
        session_obj->target_set.system_config.enc_sr ) ||

      ( session_obj->active_set.system_config.vfr_mode !=
        session_obj->target_set.system_config.vfr_mode ) ||

      ( session_obj->active_set.system_config.vsid !=
        session_obj->target_set.system_config.vsid ) ||

      ( session_obj->active_set.voice_timing.enc_offset !=
        session_obj->target_set.voice_timing.enc_offset ) ||

      ( session_obj->active_set.voice_timing.dec_req_offset !=
        session_obj->target_set.voice_timing.dec_req_offset ) ||

      ( session_obj->active_set.voice_timing.dec_offset !=
        session_obj->target_set.voice_timing.dec_offset ) ||

      ( session_obj->active_set.widevoice_enable !=
        session_obj->target_set.widevoice_enable ) ||

      ( session_obj->active_set.tty_mode !=
        session_obj->target_set.tty_mode )
    );
}

static bool_t mvm_is_vocproc_dynamic_reconfig_needed (
  mvm_session_object_t* session_obj
)
{
  return
    (
      ( session_obj->active_set.system_config.rx_voc_op_mode !=
        session_obj->target_set.system_config.rx_voc_op_mode ) ||

      ( session_obj->active_set.system_config.tx_voc_op_mode !=
        session_obj->target_set.system_config.tx_voc_op_mode )
    );
}

static bool_t mvm_is_notify_ccm_session_active_needed (
  mvm_session_object_t* session_obj
)
{
  return
    (
      ( session_obj->is_active == FALSE ) ||

      ( session_obj->active_set.stream_vocproc_load.num_nb_streams !=
        session_obj->target_set.stream_vocproc_load.num_nb_streams ) ||

      ( session_obj->active_set.stream_vocproc_load.num_wb_streams !=
        session_obj->target_set.stream_vocproc_load.num_wb_streams ) ||

      ( session_obj->active_set.stream_vocproc_load.num_swb_streams !=
        session_obj->target_set.stream_vocproc_load.num_swb_streams ) ||

      ( session_obj->active_set.stream_vocproc_load.num_fb_plus_streams !=
        session_obj->target_set.stream_vocproc_load.num_fb_plus_streams ) ||

      ( session_obj->active_set.stream_vocproc_load.num_nb_vocprocs !=
        session_obj->target_set.stream_vocproc_load.num_nb_vocprocs ) ||

      ( session_obj->active_set.stream_vocproc_load.num_wb_vocprocs !=
        session_obj->target_set.stream_vocproc_load.num_wb_vocprocs ) ||

      ( session_obj->active_set.stream_vocproc_load.num_swb_vocprocs !=
        session_obj->target_set.stream_vocproc_load.num_swb_vocprocs ) ||

      ( session_obj->active_set.stream_vocproc_load.num_fb_plus_vocprocs !=
        session_obj->target_set.stream_vocproc_load.num_fb_plus_vocprocs ) ||

      ( session_obj->active_set.stream_vocproc_load.total_kpps !=
        session_obj->target_set.stream_vocproc_load.total_kpps ) ||

      ( session_obj->active_set.stream_vocproc_load.vocproc_tx_topology_id !=
        session_obj->target_set.stream_vocproc_load.vocproc_tx_topology_id ) ||

      ( session_obj->active_set.stream_vocproc_load.vocproc_rx_topology_id !=
        session_obj->target_set.stream_vocproc_load.vocproc_rx_topology_id ) ||

      ( session_obj->active_set.stream_vocproc_load.stream_media_id !=
        session_obj->target_set.stream_vocproc_load.stream_media_id ) ||

      ( session_obj->active_set.system_config.vfr_mode !=
        session_obj->target_set.system_config.vfr_mode )
    );
}

static int32_t mvm_calculate_processing_times (
  mvm_session_object_t* session_obj,
  mvm_stream_vocproc_processing_time_us_t* ret_processing_times
)
{
  int32_t rc;
  uint32_t total_core_kpps = 0;
  mvm_stream_vocproc_processing_time_us_t processing_times;

  for ( ;; )
  {
    if ( ( session_obj == NULL ) || ( ret_processing_times == NULL ) )
    {
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    }

    if ( session_obj->target_set.clock_and_concurrency_config.is_multi_session )
    {
      /* For multi-session (multiple calls or multiple streams or multiple
       * vocprocs) use case, give each processing stage (encoding, decoding,
       * stream/vocproc pre/post-processing) MVM_VOICE_FRAME_SIZE_US.
       */
      processing_times.enc = MVM_VOICE_FRAME_SIZE_US;
      processing_times.dec = MVM_VOICE_FRAME_SIZE_US;
      processing_times.dec_pp = MVM_VOICE_FRAME_SIZE_US;
      processing_times.vp_tx = MVM_VOICE_FRAME_SIZE_US;
      processing_times.vp_rx = MVM_VOICE_FRAME_SIZE_US;
    }
    else
    {
      /* For single voice call with single stream and single vocproc use case,
       * calculate the processing time based on the required processing KPPS and
       * the available Q6 core KPPS to achieve the best RTD.
       */
      total_core_kpps =
        session_obj->target_set.clock_and_concurrency_config.total_core_kpps;

      processing_times.enc =
        ( ( uint16_t ) ( ( session_obj->target_set.stream_vocproc_load.enc_kpps *
                           MVM_VOICE_FRAME_SIZE_US ) / total_core_kpps ) );

      processing_times.dec =
        ( ( uint16_t ) ( ( session_obj->target_set.stream_vocproc_load.dec_kpps *
                           MVM_VOICE_FRAME_SIZE_US ) / total_core_kpps ) );

      processing_times.dec_pp =
        ( ( uint16_t ) ( ( session_obj->target_set.stream_vocproc_load.dec_pp_kpps *
                           MVM_VOICE_FRAME_SIZE_US ) / total_core_kpps ) );

      processing_times.vp_tx =
        ( ( uint16_t ) ( ( session_obj->target_set.stream_vocproc_load.vp_tx_kpps *
                           MVM_VOICE_FRAME_SIZE_US ) / total_core_kpps ) );

      processing_times.vp_rx =
        ( ( uint16_t ) ( ( session_obj->target_set.stream_vocproc_load.vp_rx_kpps *
                           MVM_VOICE_FRAME_SIZE_US ) / total_core_kpps ) );

      /* Additional 1ms safety margin in vptx/vprx timing offsets to compensate sample
       * slippling/stuffing.
       */
      processing_times.vp_tx += MVM_VOCPROC_PROCESSING_TIME_SAFETY_MARGIN_US;
      processing_times.vp_rx += MVM_VOCPROC_PROCESSING_TIME_SAFETY_MARGIN_US;

      /* Additional 0.5ms safety margin in the VpTx and Venc timing offsets
      * to take care of thread preemption delay.
      */
      processing_times.vp_tx += MVM_TX_PROCESSING_TIME_SAFETY_MARGIN_US;
      processing_times.enc += MVM_TX_PROCESSING_TIME_SAFETY_MARGIN_US;

      if ( VSS_MEDIA_ID_EVS == session_obj->target_set.system_config.media_id )
      {
       /* Additional 1ms safety margin in the Venc timing offsets.
        * Refer to comments under MVM_EVS_ENCODER_PROCESSING_TIME_SAFETY_MARGIN_US.
        */
        processing_times.enc += MVM_EVS_ENCODER_PROCESSING_TIME_SAFETY_MARGIN_US;
      }

      if ( processing_times.enc > MVM_VOICE_FRAME_SIZE_US )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_calculate_processing_times(): enc processing time %d us\
                                                exceeds 20ms",
                                                processing_times.enc );
        processing_times.enc = MVM_VOICE_FRAME_SIZE_US;
      }
      if ( processing_times.dec > MVM_VOICE_FRAME_SIZE_US )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_calculate_processing_times(): dec processing time %d us\
                                                 exceeds 20ms",
                                                processing_times.dec );
        processing_times.dec = MVM_VOICE_FRAME_SIZE_US;
      }
      if ( processing_times.dec_pp > MVM_VOICE_FRAME_SIZE_US )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_calculate_processing_times(): dec_pp processing time %d us\
                                                exceeds 20ms",
                                                processing_times.dec_pp );
        processing_times.dec_pp = MVM_VOICE_FRAME_SIZE_US;
      }
      if ( processing_times.vp_tx > MVM_VOICE_FRAME_SIZE_US )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_calculate_processing_times(): vp_tx processing time %d us\
                                                exceeds 20ms",
                                                processing_times.vp_tx );
        processing_times.vp_tx = MVM_VOICE_FRAME_SIZE_US;
      }
      if ( processing_times.vp_rx > MVM_VOICE_FRAME_SIZE_US )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_calculate_processing_times(): vp_rx processing time %d us\
                                                exceeds 20ms",
                                                processing_times.vp_rx );
        processing_times.vp_rx = MVM_VOICE_FRAME_SIZE_US;
      }
    }

    rc = APR_EOK;
    *ret_processing_times = processing_times;

    break;
  }

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_calculate_processing_times(): enc = %d us, dec = %d us, dec_pp = %d us",
                                        processing_times.enc,
                                        processing_times.dec,
                                        processing_times.dec_pp );
  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_calculate_processing_times(): vp_tx = %d us, vp_rx = %d us total_core_kpps = %d",
                                        processing_times.vp_tx,
                                        processing_times.vp_rx,
                                        total_core_kpps );

  return rc;
}

static int32_t mvm_calculate_timing_offsets (
  mvm_session_object_t* session_obj,
  mvm_stream_vocproc_processing_time_us_t* processing_times
)
{
  mvm_voice_timing_t* target_timing;

  if ( ( session_obj == NULL ) || ( processing_times == NULL ) )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == TRUE )
  {
    target_timing = &session_obj->mailbox_pktexg_stream_info.timing_params;
  }
  else
  {
    target_timing = &session_obj->target_set.voice_timing;
  }

  /* All voice timing offsets will be adjusted such that they don't exceed
   * MVM_MAX_VOICE_TIMING_OFFSET_US. Refer to the comments under
   * MVM_MAX_VOICE_TIMING_OFFSET_US for details.
   */

  { /* Uplink timing offsets calculations. */
    { /* Adjust the enc_offset base on MVM_MAX_VOICE_TIMING_OFFSET_US. */
      if ( target_timing->enc_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->enc_offset = MVM_MAX_VOICE_TIMING_OFFSET_US;
      }
    }
    {
      /* Calculate vp_tx_delivery_offset and adjust it base on
       * MVM_MAX_VOICE_TIMING_OFFSET_US.
       */
      if ( target_timing->enc_offset > processing_times->enc )
      {
        target_timing->vp_tx_delivery_offset =
          ( target_timing->enc_offset - processing_times->enc );
      }
      else
      {
        target_timing->vp_tx_delivery_offset =
          ( MVM_VOICE_FRAME_SIZE_US + target_timing->enc_offset -
            processing_times->enc );
      }

      if ( target_timing->vp_tx_delivery_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->vp_tx_delivery_offset = MVM_MAX_VOICE_TIMING_OFFSET_US;
      }
    }
    {
      /* Calculate vp_tx_start_offset and adjust it base on
       * MVM_MAX_VOICE_TIMING_OFFSET_US.
       */
      if ( target_timing->vp_tx_delivery_offset > processing_times->vp_tx )
      {
        target_timing->vp_tx_start_offset =
          ( target_timing->vp_tx_delivery_offset - processing_times->vp_tx );
      }
      else
      {
        target_timing->vp_tx_start_offset =
          ( MVM_VOICE_FRAME_SIZE_US + target_timing->vp_tx_delivery_offset -
            processing_times->vp_tx );
      }

      if ( target_timing->vp_tx_start_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->vp_tx_start_offset = MVM_MAX_VOICE_TIMING_OFFSET_US;
      }
    }
  }
  { /* Downlink timing offsets calculations. */
    {
      /* Adjust the dec_req_offset and dec_offset base on
       * MVM_MAX_VOICE_TIMING_OFFSET_US.
       */
      if ( target_timing->dec_req_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        {
          /* Note that if we need to adjust the dec_req_offset, we must also
           * adjust the dec_offset base on the amount of adjustment to be made
           * to dec_req_offset.
           */
          target_timing->dec_offset +=
            ( MVM_VOICE_FRAME_SIZE_US - target_timing->dec_req_offset );

          if ( target_timing->dec_offset > MVM_VOICE_FRAME_SIZE_US )
          {
            target_timing->dec_offset -= MVM_VOICE_FRAME_SIZE_US;
          }
        }

        target_timing->dec_req_offset = 0;
      }

      if ( target_timing->dec_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->dec_offset = 0;
      }
    }
    {
      /* Calculate dec_pp_start_offset and adjust it base on
       * MVM_MAX_VOICE_TIMING_OFFSET_US.
       */
      target_timing->dec_pp_start_offset =
        ( target_timing->dec_offset + processing_times->dec );
      if ( target_timing->dec_pp_start_offset > MVM_VOICE_FRAME_SIZE_US )
      {
        target_timing->dec_pp_start_offset -= MVM_VOICE_FRAME_SIZE_US;
      }

      if ( target_timing->dec_pp_start_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->dec_pp_start_offset = 0;
      }
    }
    {
      /* Calculate vp_rx_delivery_offset and adjust it base on
       * MVM_MAX_VOICE_TIMING_OFFSET_US.
       */
      target_timing->vp_rx_delivery_offset =
        ( target_timing->dec_pp_start_offset + processing_times->dec_pp +
          processing_times->vp_rx );
      if ( target_timing->vp_rx_delivery_offset > MVM_VOICE_FRAME_SIZE_US )
      {
        target_timing->vp_rx_delivery_offset -= MVM_VOICE_FRAME_SIZE_US;

        /* Note that we must check if the vprx_delivery_offset is greater than
         * MVM_VOICE_FRAME_SIZE_US again, because both the dec_pp and vp_rx
         * processing can take up to MVM_VOICE_FRAME_SIZE_US.
         */
        if ( target_timing->vp_rx_delivery_offset > MVM_VOICE_FRAME_SIZE_US )
        {
          target_timing->vp_rx_delivery_offset -= MVM_VOICE_FRAME_SIZE_US;
        }
      }

      if ( target_timing->vp_rx_delivery_offset > MVM_MAX_VOICE_TIMING_OFFSET_US )
      {
        target_timing->vp_rx_delivery_offset = 0;
      }
    }
    {
      /* Calculate vp_rx_start_offset. This is required to reverse caluate the
       * processing times for stream & vocproc both over tx & rx.
       */
      if( target_timing->vp_rx_delivery_offset > processing_times->vp_rx )
      {
        target_timing->vp_rx_start_offset =
          ( target_timing->vp_rx_delivery_offset - processing_times->vp_rx );
      }
      else
      {
        target_timing->vp_rx_start_offset =
          ( MVM_VOICE_FRAME_SIZE_US - processing_times->vp_rx +
            target_timing->vp_rx_delivery_offset );
      }
    }
  }

  return APR_EOK;
}

/****************************************************************************
 * MVM STATE MACHINE RESPONSE FUNCTIONS                                     *
 ****************************************************************************/

static void mvm_simple_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* Complete the current action. */
  session_obj->session_ctrl.status =
    APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* If completed action failed, log the error. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_simple_transition_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                            APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                            session_obj->session_ctrl.status );
  }

  mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_streams_group_wait_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* Complete the current action once all streams have responded. */
  session_obj->stream_rsp_cnt += 1;

  if ( session_obj->stream_rsp_cnt == session_obj->stream_q.size )
  {
    /* Individual stream results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_streams_set_hdvoice_group_wait_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;
  vss_ihdvoice_rsp_set_config_t* set_hdvoice_config_rsp;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_IHDVOICE_RSP_SET_CONFIG )
  {
    set_hdvoice_config_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_ihdvoice_rsp_set_config_t, packet );
    /** Check if KPPS changed from the module **/
    session_obj->is_kpps_changed_by_hdvoice = set_hdvoice_config_rsp->is_kpps_changed;
  }

  /* Complete the current action once all streams have responded. */
  session_obj->stream_rsp_cnt += 1;

  if ( session_obj->stream_rsp_cnt == session_obj->stream_q.size )
  {
    /* Individual stream results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_streams_get_pktexg_mode_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ipktexg_rsp_get_mode_t* get_pktexg_mode_rsp;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_IPKTEXG_RSP_GET_MODE )
  {
    get_pktexg_mode_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_ipktexg_rsp_get_mode_t, packet );

    if ( get_pktexg_mode_rsp->mode == VSS_IPKTEXG_MODE_MAILBOX )
    {
      session_obj->mailbox_pktexg_stream_info.stream_cnt += 1;
    }
  }

  /* Complete the current action once all streams have responded. */
  session_obj->stream_rsp_cnt += 1;

  if ( session_obj->stream_rsp_cnt == session_obj->stream_q.size )
  {
    /* Individual stream results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_streams_get_mailbox_pktexg_time_ref_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ipktexg_rsp_mailbox_get_time_reference_t* get_time_ref_rsp;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_IPKTEXG_RSP_MAILBOX_GET_TIME_REFERENCE )
  {
    get_time_ref_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_ipktexg_rsp_mailbox_get_time_reference_t, packet );

    { /* Update the mailbox time reference. */
      /* Note that there is no use case that require multiple streams running
       * in a single voice call. If there is such use case in the future, all
       * mailbox streams in the same voice call will use the same packet
       * exchange time reference. One of the stream will have optimal RTD,
       * the rest of the streams will have non-optimal RTD.
       */
      session_obj->mailbox_pktexg_stream_info.time_ref.tx_timstamp_us =
        get_time_ref_rsp->tx_timstamp_us;

      session_obj->mailbox_pktexg_stream_info.time_ref.rx_timstamp_us =
        get_time_ref_rsp->rx_timstamp_us;

      session_obj->mailbox_pktexg_stream_info.time_ref.enc_offset_margin_us =
        get_time_ref_rsp->enc_offset_margin_us;

      session_obj->mailbox_pktexg_stream_info.time_ref.dec_req_offset_margin_us =
        get_time_ref_rsp->dec_req_offset_margin_us;

      session_obj->mailbox_pktexg_stream_info.time_ref.dec_offset_margin_us =
        get_time_ref_rsp->dec_offset_margin_us;
    }

    session_obj->mailbox_pktexg_stream_info.get_time_ref_cnt += 1;

    if ( session_obj->mailbox_pktexg_stream_info.stream_cnt ==
         session_obj->mailbox_pktexg_stream_info.get_time_ref_cnt )
    {
      session_obj->mailbox_pktexg_stream_info.is_ready_to_run = TRUE;
    }
  }

  /* Complete the current action once all streams have responded. */
  session_obj->stream_rsp_cnt += 1;

  if ( session_obj->stream_rsp_cnt == session_obj->stream_q.size )
  {
    /* Individual stream results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_streams_set_system_config_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint32_t max_sr;
  vss_icommon_rsp_set_system_config_t* set_system_config_rsp;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG )
  {
    set_system_config_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_icommon_rsp_set_system_config_t, packet );

    { /* Find the Max enc/dec sampling rate */
     if ( set_system_config_rsp->dec_sr >= set_system_config_rsp->enc_sr )
     { 
      max_sr = set_system_config_rsp->dec_sr;
     }
     else
     {
      max_sr = set_system_config_rsp->enc_sr;
     }
     /* Update the stream load management variables. */
     if ( max_sr == MVM_NB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_nb_streams += 1;
      }
     else if ( max_sr == MVM_WB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_wb_streams += 1;
      }
     else if ( max_sr == MVM_SWB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_swb_streams += 1;
      }
      else
      {
        session_obj->target_set.stream_vocproc_load.num_fb_plus_streams += 1;
      }

      session_obj->target_set.stream_vocproc_load.total_kpps +=
        ( set_system_config_rsp->dec_kpps +
          set_system_config_rsp->dec_pp_kpps +
          set_system_config_rsp->enc_kpps );
    }

    if ( session_obj->stream_q.size == 1 )
    { /* Store the stream properties if there is a single stream attached to MVM. */
      session_obj->target_set.stream_vocproc_load.dec_kpps = set_system_config_rsp->dec_kpps;
      session_obj->target_set.stream_vocproc_load.dec_pp_kpps = set_system_config_rsp->dec_pp_kpps;
      session_obj->target_set.stream_vocproc_load.enc_kpps = set_system_config_rsp->enc_kpps;
      session_obj->target_set.stream_vocproc_load.stream_media_id = set_system_config_rsp->stream_media_id;
    }
  }

  /* Complete the current action once all streams have responded. */
  session_obj->stream_rsp_cnt += 1;

  if ( session_obj->stream_rsp_cnt == session_obj->stream_q.size )
  {
    /* Individual stream results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_vocprocs_group_wait_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* Complete the current action once all VOCPROCs have responded. */
  session_obj->vocproc_rsp_cnt += 1;

  if ( session_obj->vocproc_rsp_cnt == session_obj->vocproc_q.size )
  {
    /* Individual VOCPROC results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_vocprocs_get_avsync_delays_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;
  vss_ivocproc_rsp_get_avsync_delays_t* avsync_vocproc_delays;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  avsync_vocproc_delays = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_rsp_get_avsync_delays_t, packet );

  /* Extract the vocproc delays from response of a particular vocproc,
   * to normalize these delays cache them to mvm _session_object_t
   * in case they exceed the already cached delays.
   */

  if ( avsync_vocproc_delays->vp_rx_algorithmic_delay >
  	    session_obj->avsync_vocproc_delay.max_vp_rx_algorithmic_delay )
  {
    session_obj->avsync_vocproc_delay.max_vp_rx_algorithmic_delay =
		avsync_vocproc_delays->vp_rx_algorithmic_delay;
  }

  if ( avsync_vocproc_delays->vp_tx_algorithmic_delay >
  	    session_obj->avsync_vocproc_delay.max_vp_tx_algorithmic_delay )
  {
    session_obj->avsync_vocproc_delay.max_vp_tx_algorithmic_delay =
		avsync_vocproc_delays->vp_tx_algorithmic_delay;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_vocprocs_get_avsync_delays_group_wait_transition_rsp_fn(): " \
                                        "vp_rx_algorithmic_delay = %d, vp_tx_algorithmic_delay = %d ",
                                        avsync_vocproc_delays->vp_rx_algorithmic_delay,
                                        avsync_vocproc_delays->vp_tx_algorithmic_delay );

  /* Complete the current action once all VOCPROCs have responded. */
  session_obj->vocproc_rsp_cnt += 1;

  if ( session_obj->vocproc_rsp_cnt == session_obj->vocproc_q.size )
  {
    /* Individual VOCPROC results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }


  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_vocprocs_set_system_config_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint32_t max_pp_sr;
  vss_icommon_rsp_set_system_config_t* set_system_config_rsp;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG )
  {
    set_system_config_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_icommon_rsp_set_system_config_t, packet );

    { /* Find the Max PP sampling rate */
     if ( set_system_config_rsp->rx_pp_sr >= set_system_config_rsp->tx_pp_sr )
      {
        max_pp_sr = set_system_config_rsp->rx_pp_sr;
      }
     else
      {
        max_pp_sr = set_system_config_rsp->tx_pp_sr;
      }
     /* Update the vocproc load management variables. */
     if ( max_pp_sr == MVM_NB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_nb_vocprocs += 1;
      }
     else if ( max_pp_sr == MVM_WB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_wb_vocprocs += 1;
      }
     else if ( max_pp_sr == MVM_SWB_SR )
      {
        session_obj->target_set.stream_vocproc_load.num_swb_vocprocs += 1;
      }
      else
      {
        session_obj->target_set.stream_vocproc_load.num_fb_plus_vocprocs += 1;
      }

      session_obj->target_set.stream_vocproc_load.total_kpps +=
        ( set_system_config_rsp->vp_rx_kpps + set_system_config_rsp->vp_tx_kpps );
    }

    if ( session_obj->vocproc_q.size == 1 )
    { /* Store the vocproc properties if there is a single vocproc attached to MVM. */
      session_obj->target_set.stream_vocproc_load.vocproc_rx_topology_id = set_system_config_rsp->vocproc_rx_topology_id;
      session_obj->target_set.stream_vocproc_load.vocproc_tx_topology_id = set_system_config_rsp->vocproc_tx_topology_id;
      session_obj->target_set.stream_vocproc_load.vp_rx_kpps = set_system_config_rsp->vp_rx_kpps;
      session_obj->target_set.stream_vocproc_load.vp_tx_kpps = set_system_config_rsp->vp_tx_kpps;
      session_obj->target_set.stream_vocproc_load.tx_num_channels = set_system_config_rsp->tx_num_channels;
      session_obj->target_set.stream_vocproc_load.tx_mpps_scale_factor = set_system_config_rsp->tx_mpps_scale_factor;
      session_obj->target_set.stream_vocproc_load.tx_bw_scale_factor = set_system_config_rsp->tx_bw_scale_factor;
      session_obj->target_set.stream_vocproc_load.rx_mpps_scale_factor = set_system_config_rsp->rx_mpps_scale_factor;
      session_obj->target_set.stream_vocproc_load.rx_bw_scale_factor = set_system_config_rsp->rx_bw_scale_factor;
    }
  }

  /* Complete the current action once all VOCPROCs have responded. */
  session_obj->vocproc_rsp_cnt += 1;

  if ( session_obj->vocproc_rsp_cnt == session_obj->vocproc_q.size )
  {
    /* Individual VOCPROC results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  vss_ihdvoice_rsp_get_config_t* get_hdvoice_config_rsp;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  if ( packet->opcode == VSS_IHDVOICE_RSP_GET_CONFIG )
  {
    get_hdvoice_config_rsp =
      APRV2_PKT_GET_PAYLOAD( vss_ihdvoice_rsp_get_config_t, packet );

    { /* Evaluate the HD Voice config response. */
      if ( ( ( get_hdvoice_config_rsp->enable_mode == VSS_PARAM_FEATURE_FORCED_ON ) ||
           ( ( get_hdvoice_config_rsp->enable_mode == VSS_PARAM_FEATURE_DEFAULT_ON ) && ( session_obj->is_hdvoice_ui_enabled == MVM_HDVOICE_UI_UNUSED ) ) ||
           ( ( get_hdvoice_config_rsp->enable_mode == VSS_PARAM_FEATURE_DEFAULT_ON ) && ( session_obj->is_hdvoice_ui_enabled == MVM_HDVOICE_UI_ENABLED ) ) ||
           ( ( get_hdvoice_config_rsp->enable_mode == VSS_PARAM_FEATURE_DEFAULT_OFF ) && ( session_obj->is_hdvoice_ui_enabled == MVM_HDVOICE_UI_ENABLED ) ) ) &&
           ( ( ( get_hdvoice_config_rsp->feature_id == VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2 ) && ( session_obj->is_wv2_enabled ) ) ||
           ( ( get_hdvoice_config_rsp->feature_id == VSS_ICOMMON_CAL_FEATURE_BEAMR ) && ( session_obj->is_beamr_enabled ) ) ||
           ( ( get_hdvoice_config_rsp->feature_id == VSS_ICOMMON_CAL_FEATURE_WIDEVOICE ) ) ) )
      {
        session_obj->target_set.system_config.feature = get_hdvoice_config_rsp->feature_id;

        if ( get_hdvoice_config_rsp->rx_pp_sr > session_obj->target_set.system_config.rx_pp_sr )
        {
          session_obj->target_set.system_config.rx_pp_sr = get_hdvoice_config_rsp->rx_pp_sr;
        }
      }
      else
      {
        session_obj->target_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
        rc = mvm_determine_target_sampling_rates( session_obj->target_set.system_config.media_id,
                                                  session_obj->requested_var_voc_rx_sampling_rate,
                                                  session_obj->requested_var_voc_tx_sampling_rate,
                                                  &session_obj->target_set.system_config.dec_sr,
                                                  &session_obj->target_set.system_config.enc_sr,
                                                  &session_obj->target_set.system_config.rx_pp_sr,
                                                  &session_obj->target_set.system_config.tx_pp_sr );
        MVM_PANIC_ON_ERROR( rc );
      }
    }
  }

  /* Complete the current action once all VOCPROCs have responded. */
  session_obj->vocproc_rsp_cnt += 1;

  if ( (session_obj->vocproc_rsp_cnt == session_obj->vocproc_q.size) || (session_obj->vocproc_q.size == 0) )
  {
    /* Individual VOCPROC results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_detach_all_wait_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* Complete the current action once all VOCPROCs have responded. */
  session_obj->detach_all_rsp_cnt++;

  if ( session_obj->detach_all_rsp_cnt ==
       ( session_obj->stream_q.size * session_obj->vocproc_q.size ) )
  {
    /* Individual results are ignored. We depend on logs to detect
     * errors.
     */
    session_obj->session_ctrl.status = APR_EOK;
    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */
  }

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY */
static void mvm_stream_create_session_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  /* Save the stream session handle and complete the current action. */
  session_obj->default_modem_stream_addr = packet->src_addr;
  session_obj->default_modem_stream_handle = packet->src_port;

  /* NOTE that we are not inserting the default modem stream handle into the
   * session_obj->stream_q at this point. We postpone inserting it until the
   * first START command from MODEM. Also we must insert it in the stream_q
   * ONLY in the case of MVS 1.0, otherwise in the case of MVS 2.0+ MVM would
   * end up with two handles to the same stream in the stream_q (one from here
   * and another one inserted when MVS attaches its stream handle to MVM),
   * which would cause all the stream commands issued by the state machine to
   * CVS and all the attach/detach vocproc commands to be issued twice to the
   * same stream (once for each handle) with the echo for each command
   * appearing as a failure in the logs (E_ALREADY).
   */

  session_obj->session_ctrl.status =
    APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

static void mvm_open_soft_vfr_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_session_object_t* session_obj;
  vss_ivfr_rsp_open_t* vfr_open_rsp;

  rc = mvm_get_typed_object( packet->token, MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( mvm_object_t** ) &job_obj ) );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_get_typed_object( job_obj->context_handle, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  MVM_PANIC_ON_ERROR( rc );

  vfr_open_rsp =
    APRV2_PKT_GET_PAYLOAD( vss_ivfr_rsp_open_t, packet );

  session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened = TRUE;
  session_obj->mailbox_pktexg_stream_info.soft_vfr_ref_timestamp_us =
    vfr_open_rsp->ref_timestamp_us;
  session_obj->mailbox_pktexg_stream_info.soft_vfr_handle =
    packet->src_port;

  session_obj->session_ctrl.status = APR_EOK;

  mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */

  ( void ) mvm_free_object( ( mvm_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/****************************************************************************
 * MVM STATE MACHINE TRANSITION ACTION FUNCTIONS                            *
 ****************************************************************************/

/* BACKWARD COMPATIBILITY */
static int32_t mvm_action_create_passive_control_stream (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] = mvm_stream_create_session_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         mvm_cvs_addr, APR_NULL_V,
         job_obj->header.handle,
         VSS_ISTREAM_CMD_CREATE_PASSIVE_CONTROL_SESSION,
         mvm_default_modem_session_name, sizeof( mvm_default_modem_session_name ) );
  MVM_COMM_ERROR( rc, mvm_cvs_addr );

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t mvm_action_destroy_passive_control_stream (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->default_modem_stream_addr, session_obj->default_modem_stream_handle,
         job_obj->header.handle,
         APRV2_IBASIC_CMD_DESTROY_SESSION, NULL, 0 );
  MVM_COMM_ERROR( rc, session_obj->default_modem_stream_addr );

  return APR_EOK;
}

static int32_t mvm_action_reinit_streams (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send re-init command to all the attached streams. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ISTREAM_CMD_REINIT, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_disable_mailbox_pktexg_rx_expiry_processing (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle,
             VSS_IPKTEXG_CMD_MAILBOX_DISABLE_RX_EXPIRY_PROCESSING, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_reconfigure_streams (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  typedef struct mod_enable_param_data_t
  {
    uint16_t enable;
    uint16_t reserved;
  } mod_enable_param_data_t;
  struct mod_enable_args_t
  {
    vss_icommon_cmd_set_ui_property_t header;
    mod_enable_param_data_t param_data;
  } mod_enable_args;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  mod_enable_args.header.module_id = VOICE_MODULE_WV;
  mod_enable_args.header.param_id = VOICE_PARAM_MOD_ENABLE;
  mod_enable_args.header.param_size = sizeof( mod_enable_args.param_data );
  mod_enable_args.header.reserved = 0;

  mod_enable_args.param_data.enable = ( ( uint16_t ) session_obj->target_set.widevoice_enable );
  mod_enable_args.param_data.reserved = 0;

  /* Restart the response counter for set_system_config. */
  session_obj->stream_rsp_cnt = 0;

  /* Set Call Number */
  session_obj->target_set.system_config.call_num = mvm_debug_call_num;

  /* Reconfigure all attached streams. */
  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    /* Note that the TTY mode and WideVoice enable/disable param must be set on
     * CVS before calling set_system_config on CVS. As part of the handling for
     * set_system_config, CVS will query KPPS from VSM. Both the TTY mode and
     * WideVoice enable/disable param affect KPPS. Therefore they must be set
     * first, in order to get an accurate KPPS value.
     */

    { /* Set TTY mode on CVS. */
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_simple_self_destruct_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ITTY_CMD_SET_TTY_MODE,
             &session_obj->target_set.tty_mode,
             sizeof( session_obj->target_set.tty_mode ) );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
    {
      /* Set WideVoice on CVS. */
      if ( session_obj->is_wv_set_by_ui == TRUE )
      {
        rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
        MVM_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
          mvm_simple_self_destruct_result_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
               job_obj->header.handle, VSS_ICOMMON_CMD_SET_UI_PROPERTY,
               &mod_enable_args, sizeof( mod_enable_args ) );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
    }
    {
      /* Apply current system configuration settings (network id, voc class,
       * sample rates, vsid, vfr_mode) on all attached streams. Create a new
       * result response function to count the number of responses. The result
       * response function completes the current action when the number of
       * responses matches the number of attached streams. This method has
       * maximum parallelization. The solution doesn't keep track of individual
       * stream's responses so it's required search through the logs when stuck
       * in the same action for a long time.
       */
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_set_system_config_group_wait_transition_rsp_fn;

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_SET_SYSTEM_CONFIG ] =
        mvm_streams_set_system_config_group_wait_transition_rsp_fn;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVS to new settings. mvm_action_reconfigure_streams(): Network 0x%08X, Tx PP SR %d, Rx PP SR %d",
                                            session_obj->target_set.system_config.network_id,
                                            session_obj->target_set.system_config.tx_pp_sr,
                                            session_obj->target_set.system_config.rx_pp_sr );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVS to new settings. mvm_action_reconfigure_streams(): Tx voc op mode 0x%08X, Rx voc op mode 0x%08X, Media ID 0x%08X",
                                            session_obj->target_set.system_config.tx_voc_op_mode,
                                            session_obj->target_set.system_config.rx_voc_op_mode,
                                            session_obj->target_set.system_config.media_id );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVS to new settings. mvm_action_reconfigure_streams(): Feature 0x%08X.",
                                            session_obj->target_set.system_config.feature );

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG,
             &session_obj->target_set.system_config,
             sizeof( session_obj->target_set.system_config ) );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_enable_streams (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send enable command to the attached streamss. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ISTREAM_CMD_ENABLE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_disable_streams (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send disable command to all the attached streams. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ISTREAM_CMD_DISABLE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_detach_vocprocs_from_streams (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item_1;
  mvm_generic_item_t* generic_item_2;
  vss_istream_cmd_detach_vocproc_t stream_detach_vocproc;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* For every stream detach all vocprocs from it. */
  session_obj->detach_all_rsp_cnt = 0; /* Restart the response counter. */

  generic_item_1 = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                              ( ( apr_list_node_t* ) generic_item_1 ),
                              ( ( apr_list_node_t** ) &generic_item_1 ) );
    if ( rc ) break;

    generic_item_2 = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->vocproc_q,
                              ( ( apr_list_node_t* ) generic_item_2 ),
                              ( ( apr_list_node_t** ) &generic_item_2 ) );
      if ( rc ) break;

      {
        rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
        MVM_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
          mvm_detach_all_wait_transition_result_rsp_fn;

        stream_detach_vocproc.handle = ( uint16_t ) generic_item_2->handle;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, ( ( uint16_t ) generic_item_1->handle ),
               job_obj->header.handle,
               VSS_ISTREAM_CMD_DETACH_VOCPROC,
               &stream_detach_vocproc, sizeof( stream_detach_vocproc ) );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_reinit_vocprocs (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send re-init command to all the attached vocprocs. */
  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_vocprocs_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_IVOCPROC_CMD_REINIT, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_reconfigure_vocprocs (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  vss_icommon_cmd_set_system_config_t system_config; /* BACKWARD COMPATIBILITY */

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Set Call Number */
  session_obj->target_set.system_config.call_num = mvm_debug_call_num;

  /* BACKWARD COMPATIBILITY */
  /* If widevoice is enabled (i.e. via the deprecated VSS_IWIDEVOICE_CMD_SET_WIDEVOICE
   * command, then we override the rx_pp_sr with 16 KHz.
   */
  system_config = session_obj->target_set.system_config;
  if ( session_obj->target_set.widevoice_enable == 1 )
  {
    system_config.rx_pp_sr = 16000;
  }

  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  /* Apply current system configuration settings (network id, voc class,
   * sample rates, vsid, vfr_mode) on all attached VOCPROCs. Create a new
   * result response function to count the number of responses. The result
   * response function completes the current action when the number of
   * responses matches the number of attached VOCPROCs. This method has
   * maximum parallelization. The solution doesn't keep track of individual
   * VOCPROC's responses so it's required search through the logs when stuck
   * in the same action for a long time.
   */
  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
    MVM_PANIC_ON_ERROR( rc );

    job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
      mvm_vocprocs_set_system_config_group_wait_transition_rsp_fn;

    job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_SET_SYSTEM_CONFIG ] =
      mvm_vocprocs_set_system_config_group_wait_transition_rsp_fn;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVP to new settings. mvm_action_reconfigure_vocprocs(): Network 0x%08X, Tx PP SR %d, Rx PP SR %d",
                                          session_obj->target_set.system_config.network_id,
                                          session_obj->target_set.system_config.tx_pp_sr,
                                          session_obj->target_set.system_config.rx_pp_sr );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVP to new settings. mvm_action_reconfigure_vocprocs(): Tx voc op mode 0x%08X, Rx voc op mode 0x%08X, Media ID 0x%08X",
                                          session_obj->target_set.system_config.tx_voc_op_mode,
                                          session_obj->target_set.system_config.rx_voc_op_mode,
                                          session_obj->target_set.system_config.media_id );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: MVM Configuring CVP to new settings.  mvm_action_reconfigure_vocprocs(): Feature 0x%08X.",
                                          session_obj->target_set.system_config.feature );

    rc = __aprv2_cmd_alloc_send(
           mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
           job_obj->header.handle, VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG,
           &system_config, sizeof( system_config ) );
    MVM_COMM_ERROR( rc, mvm_cvp_addr );
  }

  return APR_EOK;
}

static int32_t mvm_action_enable_vocprocs (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send enable command to the attached VOCPROCs. Create a new result
   * response function to count the number of responses. The result
   * response function completes the current action when the number of
   * responses matches the number of attached VOCPROCs. This method
   * has maximum parallelization. The solution doesn't keep track of
   * individual VOCPROCs responses so it's required search through the
   * logs when stuck in the same action for a long time.
   */
  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_vocprocs_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle,
             VSS_IVOCPROC_CMD_MVM_ENABLE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_disable_vocprocs (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send disable command to the attached VOCPROCs. Create a new result
   * response function to count the number of responses. The result
   * response function completes the current action when the number of
   * responses matches the number of attached VOCPROCs. This method
   * has maximum parallelization. The solution doesn't keep track of
   * individual VOCPROCs responses so it's required search through the
   * logs when stuck in the same action for a long time.
   */
  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_vocprocs_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle,
             VSS_IVOCPROC_CMD_MVM_DISABLE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_vocproc_get_avsync_delays (
  mvm_session_object_t *session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  /* Clear the vocproc delays. */
  session_obj->avsync_vocproc_delay.max_vp_tx_algorithmic_delay = 0;
  session_obj->avsync_vocproc_delay.max_vp_rx_algorithmic_delay = 0;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_vocprocs_group_wait_transition_result_rsp_fn;
	  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_AVSYNC_DELAYS ] =
        mvm_vocprocs_get_avsync_delays_group_wait_transition_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_IVOCPROC_CMD_GET_AVSYNC_DELAYS,
             NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_calculate_avsync_delays (
  mvm_session_object_t * session_obj
)
{
  mvm_voice_timing_t* target_timing;

  if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == TRUE )
  {
    target_timing = &session_obj->mailbox_pktexg_stream_info.timing_params;
  }
  else
  {
    target_timing = &session_obj->target_set.voice_timing;
  }

  for ( ;; )
  {
    /* Reverse calculating the normalized ( processing + algorithmic ) vocproc
     * rx delay from the timing offset.
     */
    if( target_timing->vp_rx_delivery_offset >
        target_timing->vp_rx_start_offset )
    {
      session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay =
        target_timing->vp_rx_delivery_offset -
        target_timing->vp_rx_start_offset;
    }
    else
    {
      session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay =
        ( MVM_VOICE_FRAME_SIZE_US + target_timing->vp_rx_delivery_offset -
          target_timing->vp_rx_start_offset );
    }
    session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay +=
      session_obj->avsync_vocproc_delay.max_vp_rx_algorithmic_delay;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_calculate_avsync_delays(): vp_rx_normalized_total_delay = %d: ",
                                          session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay );

    /* Reverse calculating the  processing  stream rx delay from the timing offset. */
    if( target_timing->vp_rx_start_offset > target_timing->dec_offset )
    {
      session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay +=
        ( target_timing->vp_rx_start_offset -
          target_timing->dec_offset );
    }
    else
    {
      session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay +=
        ( MVM_VOICE_FRAME_SIZE_US + target_timing->vp_rx_start_offset -
          target_timing->dec_offset );
    }

    if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == TRUE )
    {
      /* Add dec_req_margin_us (safety margin between client reference and dec
       * req offset) + dec_margin_us (safety margin between dec req and dec
       * offset.
       */
      session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay +=
        session_obj->mailbox_pktexg_stream_info.time_ref.dec_req_offset_margin_us +
        session_obj->mailbox_pktexg_stream_info.time_ref.dec_offset_margin_us;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_calculate_avsync_delays(): vp_rx_normalized_total_delay"
                                          "with dec proc = %d: ",
                                          session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay );

    /* Reverse calculating the normalized ( processing + algorithmic ) vocproc
     * tx delay from the timing offset.
     */

    if( target_timing->vp_tx_delivery_offset >
        target_timing->vp_tx_start_offset )
    {
      session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay =
        target_timing->vp_tx_delivery_offset -
        target_timing->vp_tx_start_offset;
    }
    else
    {
      session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay =
        ( MVM_VOICE_FRAME_SIZE_US + target_timing->vp_tx_delivery_offset -
          target_timing->vp_tx_start_offset );
    }
    session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay +=
      session_obj->avsync_vocproc_delay.max_vp_tx_algorithmic_delay;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_calculate_avsync_delays(): vp_tx_normalized_total_delay = %d: ",
                                          session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay );

    /* Reverse calculating the processing  stream tx delay from the timing offset. */
    if( target_timing->enc_offset > target_timing->vp_tx_delivery_offset )
    {
      session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay +=
        ( target_timing->enc_offset -
          target_timing->vp_tx_delivery_offset );
    }
    else
    {
      session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay +=
        ( MVM_VOICE_FRAME_SIZE_US + target_timing->enc_offset -
          target_timing->vp_tx_delivery_offset );
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_calculate_avsync_delays(): vp_tx_normalized_total_delay"
                                          " with ecc proc = %d: ",
                                          session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay );
    break;
  }

  return APR_EOK;
}

static int32_t mvm_action_stream_set_avsync_delays (
  mvm_session_object_t *session_obj
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item;
  mvm_simple_job_object_t *job_obj;
  vss_istream_cmd_set_vocproc_avsync_delays_t avsync_delays;

  /* Get the normalised delays stored in MVM. */
  avsync_delays.vp_rx_normalized_total_delay = session_obj->avsync_vocproc_delay.vp_rx_normalized_total_delay;
  avsync_delays.vp_tx_normalized_total_delay = session_obj->avsync_vocproc_delay.vp_tx_normalized_total_delay;

  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */
  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
	                    ( ( apr_list_node_t* ) generic_item ),
	                    ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      /* Send the normalized vocproc delays to all attached streams. */
      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ISTREAM_CMD_SET_VOCPROC_AVSYNC_DELAYS,
             &avsync_delays, sizeof( avsync_delays ) );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_clean_up_stream_vocproc_lists (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Clean up the stream and vocproc lists. */
  while ( session_obj->stream_q.size > 0 )
  {
    rc = apr_list_remove_head( &session_obj->stream_q,
                                   ( ( apr_list_node_t** ) &generic_item ) );
    MVM_PANIC_ON_ERROR( rc );
    ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
  }

  while ( session_obj->vocproc_q.size > 0 )
  {
    rc = apr_list_remove_head( &session_obj->vocproc_q,
                               ( ( apr_list_node_t** ) &generic_item ) );
    MVM_PANIC_ON_ERROR( rc );
    ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
  }

  return APR_EOK;
}

static int32_t mvm_action_reset_stream_vocproc_load_count (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_obj->target_set.stream_vocproc_load.num_nb_streams = 0;
  session_obj->target_set.stream_vocproc_load.num_wb_streams = 0;
  session_obj->target_set.stream_vocproc_load.num_swb_streams = 0;
  session_obj->target_set.stream_vocproc_load.num_fb_plus_streams = 0;
  session_obj->target_set.stream_vocproc_load.num_nb_vocprocs = 0;
  session_obj->target_set.stream_vocproc_load.num_wb_vocprocs = 0;
  session_obj->target_set.stream_vocproc_load.num_swb_vocprocs = 0;
  session_obj->target_set.stream_vocproc_load.num_fb_plus_vocprocs = 0;
  session_obj->target_set.stream_vocproc_load.total_kpps = 0;
  session_obj->target_set.stream_vocproc_load.tx_num_channels = 0;
  session_obj->target_set.stream_vocproc_load.tx_mpps_scale_factor = 0;
  session_obj->target_set.stream_vocproc_load.tx_bw_scale_factor = 0;
  session_obj->target_set.stream_vocproc_load.rx_mpps_scale_factor = 0;
  session_obj->target_set.stream_vocproc_load.rx_bw_scale_factor = 0;

  /* Note that the rest of the properties in stream_vocproc_load do not need
   * to be reseted. Because they are not counters and they get assigned a
   * valid value upon MVM receives VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG from the
   * stream and vocproc, if there is only a single stream and/or a single
   * vocproc attached to this MVM session.
   */

  return APR_EOK;
}

static int32_t mvm_action_notify_ccm_session_active (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  vss_imvm_evt_voice_session_active_t evt_voice_session_active;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  evt_voice_session_active.num_nb_streams =
    session_obj->target_set.stream_vocproc_load.num_nb_streams;

  evt_voice_session_active.num_wb_streams =
    session_obj->target_set.stream_vocproc_load.num_wb_streams;

  evt_voice_session_active.num_swb_streams =
    session_obj->target_set.stream_vocproc_load.num_swb_streams;

  evt_voice_session_active.num_fb_plus_streams =
    session_obj->target_set.stream_vocproc_load.num_fb_plus_streams;

  evt_voice_session_active.num_nb_vocprocs =
    session_obj->target_set.stream_vocproc_load.num_nb_vocprocs;

  evt_voice_session_active.num_wb_vocprocs =
    session_obj->target_set.stream_vocproc_load.num_wb_vocprocs;

  evt_voice_session_active.num_swb_vocprocs =
    session_obj->target_set.stream_vocproc_load.num_swb_vocprocs;

  evt_voice_session_active.num_fb_plus_vocprocs =
    session_obj->target_set.stream_vocproc_load.num_fb_plus_vocprocs;

  evt_voice_session_active.total_kpps =
    session_obj->target_set.stream_vocproc_load.total_kpps;

  evt_voice_session_active.tx_topology_id =
    session_obj->target_set.stream_vocproc_load.vocproc_tx_topology_id;

  evt_voice_session_active.rx_topology_id =
    session_obj->target_set.stream_vocproc_load.vocproc_rx_topology_id;

  evt_voice_session_active.media_id =
    session_obj->target_set.stream_vocproc_load.stream_media_id;

  evt_voice_session_active.vfr_mode =
    session_obj->target_set.system_config.vfr_mode;

  evt_voice_session_active.tx_num_channels =
    session_obj->target_set.stream_vocproc_load.tx_num_channels;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_notify_ccm_session_active(): vptx_kpps = %d, rx_kpps = %d ",
                                        session_obj->target_set.stream_vocproc_load.vp_tx_kpps,
                                        session_obj->target_set.stream_vocproc_load.vp_rx_kpps);
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_action_notify_ccm_session_active(): enc_kpps = %d, dec_kpps = %d ",
                                        session_obj->target_set.stream_vocproc_load.enc_kpps,
                                        ( session_obj->target_set.stream_vocproc_load.dec_kpps + session_obj->target_set.stream_vocproc_load.dec_pp_kpps ) );

  evt_voice_session_active.tx_mpps_scale_factor =
    session_obj->target_set.stream_vocproc_load.tx_mpps_scale_factor;
  evt_voice_session_active.tx_bw_scale_factor =
    session_obj->target_set.stream_vocproc_load.tx_bw_scale_factor;
  evt_voice_session_active.rx_mpps_scale_factor =
    session_obj->target_set.stream_vocproc_load.rx_mpps_scale_factor;
  evt_voice_session_active.rx_bw_scale_factor =
    session_obj->target_set.stream_vocproc_load.rx_bw_scale_factor;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         mvm_my_addr, APR_NULL_V,
         0, VSS_IMVM_EVT_VOICE_SESSION_ACTIVE,
         &evt_voice_session_active,
         sizeof( evt_voice_session_active ) );
  MVM_COMM_ERROR( rc, mvm_my_addr );

  return APR_EOK;
}

static int32_t mvm_action_notify_ccm_session_inactive (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
         mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         mvm_my_addr, APR_NULL_V,
         0, VSS_IMVM_EVT_VOICE_SESSION_INACTIVE,
         NULL, 0 );
  MVM_COMM_ERROR( rc, mvm_my_addr );

  return APR_EOK;
}

static int32_t mvm_action_get_stream_pktexg_mode (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Get packet exchange mode from all the attached streams. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */
  session_obj->mailbox_pktexg_stream_info.stream_cnt = 0;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_get_pktexg_mode_group_wait_transition_rsp_fn;

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_PKTEXG_MODE ] =
        mvm_streams_get_pktexg_mode_group_wait_transition_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_IPKTEXG_CMD_GET_MODE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_clear_mailbox_stream_pktexg_time_reference (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send clear time reference command to the attached streams. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle,
             VSS_IPKTEXG_CMD_MAILBOX_CLEAR_TIME_REFERENCE, NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_get_mailbox_stream_pktexg_time_reference (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Send mailbox_get_time_reference command to the attached streams. */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */
  session_obj->mailbox_pktexg_stream_info.get_time_ref_cnt = 0;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_get_mailbox_pktexg_time_ref_group_wait_transition_rsp_fn;

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_MAILBOX_PKTEXG_TIME_REFERENCE ] =
        mvm_streams_get_mailbox_pktexg_time_ref_group_wait_transition_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_IPKTEXG_CMD_MAILBOX_GET_TIME_REFERENCE,
             NULL, 0 );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_calculate_timing_offsets (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_stream_vocproc_processing_time_us_t processing_times;
  ( void ) mmstd_memset(&processing_times, 0,sizeof( mvm_stream_vocproc_processing_time_us_t));
  for ( ;; )
  {
    rc = mvm_calculate_processing_times( session_obj, &processing_times );
    if ( rc )
    {
      session_obj->session_ctrl.status = rc;
      break;
    }

    ( void ) mvm_calculate_timing_offsets( session_obj, &processing_times );

    break;
  }

  return APR_EOK;
}

static int32_t mvm_action_set_vocproc_timing (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  vss_ivocproc_cmd_set_voice_timing_t set_voice_timing;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  mvm_voice_timing_t* target_timing;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == TRUE )
  {
    target_timing = &session_obj->mailbox_pktexg_stream_info.timing_params;
  }
  else
  {
    target_timing = &session_obj->target_set.voice_timing;
  }

  /* Send set_timing command to the attached VOCPROCs. Create a new result
   * response function to count the number of responses. The result
   * response function completes the current action when the number of
   * responses matches the number of attached VOCPROCs. This method
   * has maximum parallelization. The solution doesn't keep track of
   * individual VOCPROC's response so it's required search through the
   * logs when stuck in the same action for a long time.
   */
  session_obj->vocproc_rsp_cnt = 0; /* Restart the response counter. */

  set_voice_timing.vp_rx_delivery_offset = target_timing->vp_rx_delivery_offset;

  set_voice_timing.vp_tx_start_offset = target_timing->vp_tx_start_offset;

  set_voice_timing.vp_tx_delivery_offset = target_timing->vp_tx_delivery_offset;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_vocprocs_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_IVOCPROC_CMD_SET_VOICE_TIMING,
             &set_voice_timing, sizeof( set_voice_timing ) );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_set_stream_timing (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  vss_istream_cmd_set_voice_timing_t set_voice_timing;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  mvm_voice_timing_t* target_timing;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == TRUE )
  {
    target_timing = &session_obj->mailbox_pktexg_stream_info.timing_params;
  }
  else
  {
    target_timing = &session_obj->target_set.voice_timing;
  }

  /* Send set_timing command to the attached streams. Create a new result
   * response function to count the number of responses. The result
   * response function completes the current action when the number of
   * responses matches the number of attached streams. This method
   * has maximum parallelization. The solution doesn't keep track of
   * individual stream's response so it's required search through the
   * logs when stuck in the same action for a long time.
   */
  session_obj->stream_rsp_cnt = 0; /* Restart the response counter. */

  set_voice_timing.enc_offset = target_timing->enc_offset;

  set_voice_timing.dec_req_offset = target_timing->dec_req_offset;

  set_voice_timing.dec_offset = target_timing->dec_offset;

  set_voice_timing.dec_pp_start_offset = target_timing->dec_pp_start_offset;

  set_voice_timing.vp_tx_delivery_offset = target_timing->vp_tx_delivery_offset;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->stream_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    {
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
             job_obj->header.handle, VSS_ISTREAM_CMD_SET_VOICE_TIMING,
             &set_voice_timing, sizeof( set_voice_timing ) );
      MVM_COMM_ERROR( rc, mvm_cvs_addr );
    }
  }

  return APR_EOK;
}

static int32_t mvm_action_update_active_set (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  session_obj->active_set.system_config = session_obj->target_set.system_config;
  session_obj->active_set.widevoice_enable = session_obj->target_set.widevoice_enable;
  session_obj->active_set.tty_mode = session_obj->target_set.tty_mode;
  session_obj->active_set.voice_timing = session_obj->target_set.voice_timing;
  session_obj->active_set.clock_and_concurrency_config =
    session_obj->target_set.clock_and_concurrency_config;
  session_obj->active_set.stream_vocproc_load =
    session_obj->target_set.stream_vocproc_load;

  return APR_EOK;
}

static int32_t mvm_action_open_soft_vfr_for_mailbox_pktexg (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  vss_ivfr_cmd_open_t vfr_open;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );

  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_transition_result_rsp_fn;

  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_OPEN_SOFT_VFR ] =
    mvm_open_soft_vfr_transition_rsp_fn;

  vfr_open.mode = VSS_ICOMMON_VFR_MODE_SOFT;

  /* Open the VFR driver for soft VFR. */
  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         mvm_cvd_vfr_addr, APR_NULL_V,
         job_obj->header.handle, VSS_IVFR_CMD_OPEN,
         &vfr_open, sizeof( vfr_open ) );
  MVM_COMM_ERROR( rc, mvm_cvd_vfr_addr );

  return APR_EOK;
}

static int32_t mvm_action_close_soft_vfr_for_mailbox_pktexg (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );

  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_transition_result_rsp_fn;

  /* Close the VFR driver for soft VFR. */
  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         mvm_cvd_vfr_addr, session_obj->mailbox_pktexg_stream_info.soft_vfr_handle,
         job_obj->header.handle, VSS_IVFR_CMD_CLOSE,
         NULL, 0 );
  MVM_COMM_ERROR( rc, mvm_cvd_vfr_addr );

  return APR_EOK;
}

static int32_t mvm_action_calculate_mailbox_pktexg_timing_params (
  mvm_session_object_t* session_obj
)
{
  uint16_t enc_offset;
  uint16_t dec_req_offset;
  uint16_t dec_offset;
  uint16_t client_tx_offset;
  uint16_t client_rx_offset;
  uint16_t vfr_offset;
  vss_ipktexg_rsp_mailbox_get_time_reference_t* time_ref;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  time_ref = &session_obj->mailbox_pktexg_stream_info.time_ref;

  vfr_offset =
    ( session_obj->mailbox_pktexg_stream_info.soft_vfr_ref_timestamp_us %
      MVM_VOICE_FRAME_SIZE_US );

  client_tx_offset = ( time_ref->tx_timstamp_us % MVM_VOICE_FRAME_SIZE_US );

  client_rx_offset = ( time_ref->rx_timstamp_us % MVM_VOICE_FRAME_SIZE_US );

  { /* Encoder offset calculation. */
    if ( client_tx_offset >= vfr_offset )
    {
      enc_offset = ( client_tx_offset - vfr_offset );
    }
    else
    {
      enc_offset =
        ( MVM_VOICE_FRAME_SIZE_US + client_tx_offset - vfr_offset );
    }

    if ( enc_offset >= time_ref->enc_offset_margin_us )
    {
      enc_offset -= time_ref->enc_offset_margin_us;
    }
    else
    {
      enc_offset =
        ( MVM_VOICE_FRAME_SIZE_US + enc_offset - time_ref->enc_offset_margin_us );
    }
  }

  { /* Decoder request offset calculation. */
    if ( client_rx_offset >= vfr_offset )
    {
      dec_req_offset = ( client_rx_offset - vfr_offset );
    }
    else
    {
      dec_req_offset =
        ( MVM_VOICE_FRAME_SIZE_US + client_rx_offset - vfr_offset );
    }

    dec_req_offset += time_ref->dec_req_offset_margin_us;
    if ( dec_req_offset >= MVM_VOICE_FRAME_SIZE_US )
    {
      dec_req_offset -= MVM_VOICE_FRAME_SIZE_US;
    }
  }

  { /* Decoder offset calculation. */
    dec_offset = ( dec_req_offset + time_ref->dec_offset_margin_us );
    if ( dec_offset >= MVM_VOICE_FRAME_SIZE_US )
    {
      dec_offset -= MVM_VOICE_FRAME_SIZE_US;
    }
  }

  session_obj->mailbox_pktexg_stream_info.timing_params.enc_offset = enc_offset;
  session_obj->mailbox_pktexg_stream_info.timing_params.dec_req_offset = dec_req_offset;
  session_obj->mailbox_pktexg_stream_info.timing_params.dec_offset = dec_offset;

  return APR_EOK;
}

static int32_t mvm_action_request_high_clk(
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );

  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             mvm_my_addr, APR_NULL_V,
             job_obj->header.handle, VSS_IMVM_CMD_SET_MAX_CLOCK,
             NULL, 0 );
  MVM_COMM_ERROR( rc, mvm_my_addr );

  return APR_EOK;
}

static int32_t mvm_action_revert_high_clk(
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );

  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_transition_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
           mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           mvm_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           mvm_my_addr, APR_NULL_V,
           job_obj->header.handle, VSS_IMVM_CMD_RESET_MAX_CLOCK,
           NULL, 0 );
  MVM_COMM_ERROR( rc, mvm_my_addr );

  return APR_EOK;
}

static int32_t mvm_action_hdvoice_config(
  mvm_session_object_t* session_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  vss_ihdvoice_cmd_get_config_t get_hdvoice_config;

  if ( session_obj == NULL )
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );

  get_hdvoice_config.media_id = session_obj->target_set.system_config.media_id;
  get_hdvoice_config.network_id = session_obj->target_set.system_config.network_id;
  get_hdvoice_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;
  get_hdvoice_config.rx_voc_op_mode = session_obj->target_set.system_config.rx_voc_op_mode;
  get_hdvoice_config.tx_pp_sr = session_obj->target_set.system_config.tx_pp_sr;
  get_hdvoice_config.tx_voc_op_mode = session_obj->target_set.system_config.tx_voc_op_mode;
  get_hdvoice_config.feature_id = session_obj->target_set.system_config.feature;

  generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );
  session_obj->vocproc_rsp_cnt = 0;

  for ( ;; )
  {
    rc = apr_list_get_next( &session_obj->vocproc_q,
                            ( ( apr_list_node_t* ) generic_item ),
                            ( ( apr_list_node_t** ) &generic_item ) );
    if ( rc ) break;

    rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
    MVM_PANIC_ON_ERROR( rc );

    job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
      mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

    job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_HDVOICE_CONFIG ] =
      mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

    rc = __aprv2_cmd_alloc_send(
            mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
            session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
            mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
            job_obj->header.handle, VSS_IHDVOICE_CMD_GET_CONFIG,
            &get_hdvoice_config, sizeof( get_hdvoice_config ) );
    MVM_COMM_ERROR( rc, mvm_cvp_addr );
  }

  return APR_EOK;
}

/****************************************************************************
 * MVM STATE MACHINE                                                        *
 ****************************************************************************/

static int32_t mvm_state_reset_entry (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_state_reset_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = MVM_STATE_ENUM_RESET;
  return APR_EIMMEDIATE;
}

static int32_t mvm_state_reset (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case MVM_GOAL_ENUM_NONE:
    break;

  case MVM_GOAL_ENUM_CREATE:
  case MVM_GOAL_ENUM_STOP:
  case MVM_GOAL_ENUM_STANDBY:
  case MVM_GOAL_ENUM_START:
    { /* (RESET to INIT transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_STREAM_CREATE_SESSION:
        { /* BACKWARD COMPATIBILITY */ /* Create stream session if this is default modem MVM. */

          /* Check if we need to create a passive control CVS stream in order
           * to handle MVS v1.0 backward compatibility.
           */
          if ( mmstd_strncmp( session_obj->session_name,
                              sizeof( session_obj->session_name ),
                              mvm_default_modem_session_name,
                              sizeof( mvm_default_modem_session_name ) )
               == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            rc = mvm_action_create_passive_control_stream( session_obj );
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        rc  = APR_EOK;
        return rc;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_DESTROY:
    { /* Complete the pending command and stay in the same state. */
      rc = mvm_action_clean_up_stream_vocproc_lists( session_obj );

      session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
      rc = mvm_do_complete_goal( session_obj );
    }
    break;

  default:
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t mvm_state_init_entry (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_state_init_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT;
  return APR_EIMMEDIATE;
}

static int32_t mvm_state_init (
  mvm_session_object_t* session_obj
)
{
  switch ( session_obj->session_ctrl.goal )
  {
  case MVM_GOAL_ENUM_NONE:
    break;

  case MVM_GOAL_ENUM_CREATE:
  case MVM_GOAL_ENUM_PAUSE:
    { /* Complete the pending command and stay in the same state. */
      session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
      ( void )mvm_do_complete_goal( session_obj );
    }
    break;

  case MVM_GOAL_ENUM_STOP:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG;

          if ( session_obj->mailbox_pktexg_stream_info.is_disable_rx_expiry_processing == FALSE )
          {
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }

          session_obj->mailbox_pktexg_stream_info.is_disable_rx_expiry_processing = FALSE;

          if ( session_obj->stream_q.size == 0 )
          {
            /* If there are no streams attached, nothing to do, continue with
             * next action.
             */
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }

          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          ( void )mvm_action_disable_mailbox_pktexg_rx_expiry_processing( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE;

          if ( session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened == FALSE )
          {
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }

          if ( ( session_obj->apps_state == MVM_GOAL_ENUM_STOP ) &&
               ( session_obj->modem_state == MVM_GOAL_ENUM_STOP ) )
          { /* Close soft VFR only when both APPS and MODEM called STOP_VOICE. */
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_close_soft_vfr_for_mailbox_pktexg( session_obj );

            session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened = FALSE;
            session_obj->mailbox_pktexg_stream_info.soft_vfr_handle = APR_NULL_V;
          }
          else
          {
            /* If there is no STOP_VOICE from both APPS and MODEM, continue
             * with next action.
             */
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_COMPLETE;
          session_obj->session_ctrl.status = APR_EOK;

          if ( ( session_obj->apps_state == MVM_GOAL_ENUM_STOP ) &&
               ( session_obj->modem_state == MVM_GOAL_ENUM_STOP ) &&
               ( session_obj->is_active == TRUE ) )
          {
            /* Notify MVM-CCM that this MVM session is becoming inactive, when
             * both APPS and MODEM called STOP_VOICE.
             */
            ( void )mvm_action_notify_ccm_session_inactive( session_obj );
            session_obj->is_active = FALSE;
          }
        }
        return APR_EIMMEDIATE;

      case MVM_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK;
          ( void )mvm_do_complete_goal( session_obj );
        }
        break;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_DESTROY:
    { /* New action: Release resource handles. (INIT to RESET transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_VOCPROC_DETACH:
        { /* Detach every vocproc from every stream. */
          if ( session_obj->stream_q.size == 0 ||
               session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_DESTROY_SESSION;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_DESTROY_SESSION;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_detach_vocprocs_from_streams( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_DESTROY_SESSION:
        {
          /* BACKWARD COMPATIBILITY */
          /* Destroy default modem stream session (if this is default modem MVM). */
          if ( session_obj->default_modem_stream_handle != 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_destroy_passive_control_stream( session_obj );
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_CLOSE_SOFT_VFR_FOR_MAILBOX_PKTEXG:
        {
          if ( session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened == TRUE )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_close_soft_vfr_for_mailbox_pktexg( session_obj );

            session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened = FALSE;
            session_obj->mailbox_pktexg_stream_info.soft_vfr_handle = APR_NULL_V;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_INACTIVE:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_RESET_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_EOK;

          if ( session_obj->is_active == TRUE )
          {
            ( void )mvm_action_notify_ccm_session_inactive( session_obj );
            session_obj->is_active = FALSE;
          }
        }
        return APR_EIMMEDIATE;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_STANDBY:
  case MVM_GOAL_ENUM_START:
    {
      /* New action: Configure the streams and vocprocs as well as bring up the
       * vocprocs. (INIT to IDLE transition).
       */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_REQUEST_HIGH_CLOCK:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_REINIT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          ( void )mvm_action_request_high_clk( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_REINIT:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_REINIT;

          if ( session_obj->stream_q.size == 0 || session_obj->is_stream_reinit_required == FALSE )
          {
            /* If there are no streams attached, nothing to do, continue with
             * next action.
             */
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
            ( void )mvm_action_reinit_streams( session_obj );
            session_obj->is_stream_reinit_required = FALSE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_VOCPROC_REINIT:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_RESET_STREAM_VOCPROC_LOAD_COUNT;

          if ( session_obj->vocproc_q.size == 0 || session_obj->is_vocproc_reinit_required == FALSE )
          {
            /* If there are no vocprocs attached, nothing to do, continue with
             * next action.
             */
            session_obj->session_ctrl.status = APR_EOK;
            return APR_EIMMEDIATE;
          }
          else
          {
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
            ( void )mvm_action_reinit_vocprocs( session_obj );
            session_obj->is_vocproc_reinit_required = FALSE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_RESET_STREAM_VOCPROC_LOAD_COUNT:
        { /* Reset the stream and vocproc load counters. */
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
              session_obj->session_ctrl.action = MVM_ACTION_ENUM_HDVOICE_CONFIG;
              session_obj->session_ctrl.status = APR_EOK;

          ( void )mvm_action_reset_stream_vocproc_load_count( session_obj );
          }
            return APR_EIMMEDIATE;

      case MVM_ACTION_ENUM_HDVOICE_CONFIG:
        {
          /* Cannot search for HD Voice feature that does not matter for Media ID.
           * HD voice requires a Media ID. And also, atleast one session is needed to be attached.
           */
          if ( ( session_obj->target_set.system_config.media_id == VSS_MEDIA_ID_NONE ) ||
               ( session_obj->vocproc_q.size == 0) )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_RECONFIG;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_RECONFIG;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_hdvoice_config( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_VOCPROC_RECONFIG:
        { /* Re-configure all the attached VOCPROCs. */
          if ( session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_RECONFIG;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_RECONFIG;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_reconfigure_vocprocs( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_RECONFIG:
        { /* Re-configure all the attached streams. */
          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_reconfigure_streams( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_NOTIFY_CCM_SESSION_ACTIVE:
        { /* Notify MVM-CCM this session's configurations, if required. */
          if ( mvm_is_notify_ccm_session_active_needed( session_obj ) == TRUE )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_notify_ccm_session_active( session_obj );

            session_obj->is_active = TRUE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_GET_PKTEXG_MODE:
        { /* Gets the packet exchange mode from all attached streams. */
          session_obj->mailbox_pktexg_stream_info.is_ready_to_run = FALSE;

          if ( ( session_obj->session_ctrl.goal == MVM_GOAL_ENUM_START ) &&
               ( session_obj->stream_q.size > 0 ) )
          {
            /* Gets the packet exchange mode only if the current goal is start
             * and there is at least one stream attached to MVM.
             */
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_get_stream_pktexg_mode( session_obj );
          }
          else
          {
            /* Skip all the state actions specific to mailbox packet exchange
             * if either APPS or modem has not started the call or if there is
             * no stream attached to MVM.
             */
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE:
        {
          if ( session_obj->mailbox_pktexg_stream_info.stream_cnt == 0 )
          {
            /* Skip all the state actions specific to mailbox packet exchange if
             * the streams attached to MVM are not in mailbox packet exchange
             * mode.
             */
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }

          if ( session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed == FALSE )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed = FALSE;
            ( void )mvm_action_clear_mailbox_stream_pktexg_time_reference( session_obj );
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_GET_MAILBOX_PKTEXG_TIME_REFERENCE:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_OPEN_SOFT_VFR_FOR_MAILBOX_PKTEXG;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          ( void )mvm_action_get_mailbox_stream_pktexg_time_reference( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_OPEN_SOFT_VFR_FOR_MAILBOX_PKTEXG:
        {
          if ( session_obj->mailbox_pktexg_stream_info.is_ready_to_run == FALSE )
          {
            /* Complete the current goal and stay in the same state. The MVM
             * state machine will run again when the mailbox stream is ready to
             * run (i.e. after the stream received the time reference from its
             * client), at which time the stream will send the
             * VSS_IPKTEXG_EVT_MAILBOX_TIMING_RECONFIG event to MVM. */
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_COMPLETE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }

          if ( session_obj->mailbox_pktexg_stream_info.is_soft_vfr_opened == TRUE )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            ( void )mvm_action_open_soft_vfr_for_mailbox_pktexg( session_obj );
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_CALCULATE_MAILBOX_PKTEXG_TIMING_PARAMS:
        { /* Calculate the voice timing parameters for mailbox packet exchange. */
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS;
          session_obj->session_ctrl.status = APR_EOK;

          ( void )mvm_action_calculate_mailbox_pktexg_timing_params( session_obj );
        }
        return APR_EIMMEDIATE;

      case MVM_ACTION_ENUM_CALCULATE_TIMING_OFFSETS:
        {
          /* Calculate the timing offsets at each voice processing stage
           * (encoding, decoding, stream/vocproc pre/post-processing).
           */
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_SET_TIMING;
          session_obj->session_ctrl.status = APR_EOK;

          ( void )mvm_action_calculate_timing_offsets( session_obj );
        }
        return APR_EIMMEDIATE;

      case MVM_ACTION_ENUM_VOCPROC_SET_TIMING:
        { /* Set timing on all the attached VOCPROCs. */
          if ( session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_SET_TIMING;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_SET_TIMING;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_set_vocproc_timing( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_SET_TIMING:
        { /* Set timing on all the attached streams. */
          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_UPDATE_ACTIVE_SET;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_UPDATE_ACTIVE_SET;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_set_stream_timing( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_UPDATE_ACTIVE_SET:
        { /* Update the active set. */
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_ENABLE;
          session_obj->session_ctrl.status = APR_EOK;

          ( void )mvm_action_update_active_set( session_obj );
        }
        return APR_EIMMEDIATE;

      case MVM_ACTION_ENUM_VOCPROC_ENABLE:
        { /* Enable the attached VOCPROCs. */
          if ( session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_enable_vocprocs( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_VOCPROC_GET_AVSYNC_DELAYS:
        {
          if ( session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_vocproc_get_avsync_delays( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_CALCULATE_AVSYNC_DELAYS:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_SET_AVSYNC_DELAYS;
          session_obj->session_ctrl.status = APR_EOK;

          ( void )mvm_action_calculate_avsync_delays( session_obj );
        }
        return APR_EIMMEDIATE;


      case MVM_ACTION_ENUM_STREAM_SET_AVSYNC_DELAYS:
      	{
          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_REVERT_HIGH_CLOCK;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_REVERT_HIGH_CLOCK;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          ( void )mvm_action_stream_set_avsync_delays( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_REVERT_HIGH_CLOCK:
        {
          session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          ( void )mvm_action_revert_high_clk( session_obj );
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK;
          ( void )mvm_do_complete_goal( session_obj );
        }
        break;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t mvm_state_idle_entry (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_state_idle_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE;
  return APR_EIMMEDIATE;
}

static int32_t mvm_state_idle (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case MVM_GOAL_ENUM_NONE:
    break;

  case MVM_GOAL_ENUM_DESTROY:
  case MVM_GOAL_ENUM_STOP:
  case MVM_GOAL_ENUM_PAUSE:
    { /* New action: Stop the device group. (IDLE to INIT transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_VOCPROC_DISABLE:
        { /* Disable the attached VOCPROCs. */
          if ( session_obj->vocproc_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          rc = mvm_action_disable_vocprocs( session_obj );
        }
        rc = APR_EOK;
        return rc;

      case MVM_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
          rc = mvm_do_complete_goal( session_obj );
        }
        break;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_STANDBY:
    {
      /* New action: Re-configure the CVPs and CVSs when needed. (IDLE to IDLE
       * transition).
       */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_VOCPROC_DISABLE:
        {
          if ( mvm_is_stream_vocproc_reconfig_needed( session_obj ) == TRUE )
          {
            /* Disable all attached VOCPROCS to ensure that voice timing params
             * will be set properly.
             */
            if ( session_obj->vocproc_q.size == 0 )
            {
              session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
              session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
              session_obj->session_ctrl.status = APR_EOK;

              return APR_EIMMEDIATE;
            }
            else
            {
              session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
              session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
              session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
            }

            rc = mvm_action_disable_vocprocs( session_obj );
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_COMPLETE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
          rc = mvm_do_complete_goal( session_obj );
        }
        break;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_START:
    { /* New action: Run the voice stream. (IDLE to RUN transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_CONTINUE:
      case MVM_ACTION_ENUM_VOCPROC_DISABLE:
        {
          if ( mvm_is_stream_vocproc_reconfig_needed( session_obj ) == TRUE )
          {
            /* Disable all attached VOCPROCS to ensure that voice timing params
             * will be set properly.
             */
            if ( session_obj->vocproc_q.size == 0 )
            {
              session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
              session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
              session_obj->session_ctrl.status = APR_EOK;

              return APR_EIMMEDIATE;
            }
            else
            {
              session_obj->session_ctrl.state = MVM_STATE_ENUM_INIT_ENTRY;
              session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
              session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
            }

            rc = mvm_action_disable_vocprocs( session_obj );
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_CLEAR_MAILBOX_PKTEXG_TIME_REFERENCE:
        {
          /* If the streams attached to MVM are configured for mailbox packet
           * exchange and their packet exchange time reference is cleared
           * by this action, the stream will send the
           * VSS_IPKTEXG_EVT_MAILBOX_TIMING_RECONFIG upon its IDLE to RUN
           * transition, to trigger MVM to transition back to INIT and
           * configure the packet exchange timing parameters, then run the
           * streams with proper packet exchange timing parameters.
           */
          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_RUN;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }

          if ( session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed == FALSE )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_RUN;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_STREAM_RUN;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

            session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed = FALSE;

            rc = mvm_action_clear_mailbox_stream_pktexg_time_reference( session_obj );
          }
        }
        return APR_EOK;

      case MVM_ACTION_ENUM_STREAM_RUN:
        { /* Run all the voice streams currently attached to the MVM. */
          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_RUN_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_RUN_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          rc = mvm_action_enable_streams( session_obj );
        }
        return APR_EOK;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t mvm_state_run_entry (
  mvm_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_state_run_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = MVM_STATE_ENUM_RUN;
  return APR_EIMMEDIATE;
}

static int32_t mvm_state_run (
  mvm_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case MVM_GOAL_ENUM_NONE:
    break;

  case MVM_GOAL_ENUM_DESTROY:
  case MVM_GOAL_ENUM_STOP:
  case MVM_GOAL_ENUM_PAUSE:
  case MVM_GOAL_ENUM_STANDBY:
    { /* New action: Stop the voice stream. (RUN to IDLE transition). */
      switch ( session_obj->session_ctrl.action )
      {
      case MVM_ACTION_ENUM_NONE:
      case MVM_ACTION_ENUM_STREAM_STOP:
        {
          /* Stop all the voice streams currently attached to the MVM. */

          if ( session_obj->stream_q.size == 0 )
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }
          else
          {
            session_obj->session_ctrl.state = MVM_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = MVM_ACTION_ENUM_CONTINUE;
            session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
          }

          rc = mvm_action_disable_streams( session_obj );
        }
        rc = APR_EOK;
        return rc;

      default:
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case MVM_GOAL_ENUM_START:
    { /* Complete the pending command and stay in the same state. */
      session_obj->session_ctrl.status = APR_EOK; /* TODO: Handle errors. */
      rc = mvm_do_complete_goal( session_obj );
    }
    break;

  default:
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t mvm_state_control (
  mvm_session_object_t* session_obj
)
{
  int32_t rc = APR_EOK;

  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  do
  {
    mvm_log_state_info( session_obj );

    switch ( session_obj->session_ctrl.state )
    {
    case MVM_STATE_ENUM_RESET_ENTRY:
      rc = mvm_state_reset_entry( session_obj );
      break;

    case MVM_STATE_ENUM_RESET:
      rc = mvm_state_reset( session_obj );
      break;

    case MVM_STATE_ENUM_INIT_ENTRY:
      rc = mvm_state_init_entry( session_obj );
      break;

    case MVM_STATE_ENUM_INIT:
      rc = mvm_state_init( session_obj );
      break;

    case MVM_STATE_ENUM_IDLE_ENTRY:
      rc = mvm_state_idle_entry( session_obj );
      break;

    case MVM_STATE_ENUM_IDLE:
      rc = mvm_state_idle( session_obj );
      break;

    case MVM_STATE_ENUM_RUN_ENTRY:
      rc = mvm_state_run_entry( session_obj );
      break;

    case MVM_STATE_ENUM_RUN:
      rc = mvm_state_run( session_obj );
      break;

    default:
      rc = APR_EUNEXPECTED;
      break;
    }
  }
  while ( rc == APR_EIMMEDIATE );

  mvm_log_state_info( session_obj );

  if ( rc == APR_ECONTINUE )
  { /* TODO: Use a timer to trigger the state machine to run after 5ms. */
  }

  return rc;
}

/****************************************************************************
 * MVM HELPER FUNCTIONS                                                     *
 ****************************************************************************/

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t mvm_helper_open_session_control (
  mvm_pending_control_t* ctrl,
  mvm_indirection_object_t** ret_indirect_obj,
  mvm_session_object_t** ret_session_obj
)
{
  int32_t rc;
  mvm_indirection_object_t* indirect_obj;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  client_addr = ctrl->packet->src_addr;

  if ( ret_session_obj == NULL )
  {
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EFAILED );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EBADPARAM;
  }

  rc = mvm_get_typed_object( ctrl->packet->dst_port,
                             MVM_OBJECT_TYPE_ENUM_INDIRECTION,
                             ( ( mvm_object_t** ) &indirect_obj ) );
  if ( rc )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_helper_open_session_control(): Invalid handle: cmd_opcode=0x%08X src_addr=0x%08X req_handle=0x%08X",
                                            ctrl->packet->opcode,
                                            ctrl->packet->src_addr,
                                            ctrl->packet->dst_port );
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EHANDLE;
  }

  rc = mvm_get_typed_object( indirect_obj->session_handle,
                             MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) ret_session_obj ) );
  if ( rc )
  {
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EHANDLE;
  }

  if ( ret_indirect_obj != NULL )
  {
    *ret_indirect_obj = indirect_obj;
  }

  return APR_EOK;
}

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t mvm_helper_validate_payload_size_control (
  mvm_pending_control_t* ctrl,
  uint32_t valid_size
)
{
  int32_t rc;
  uint32_t size;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
  if ( size != valid_size )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_helper_validate_payload_size_control(): Invalid payload size: opcode=0x%08X valid_size=%d actual_size=%d",
                                            ctrl->packet->opcode,
                                            valid_size,
                                            size );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EBADPARAM;
  }

  return APR_EOK;
}

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t mvm_helper_create_new_goal_control (
  mvm_goal_enum_t new_goal,
  mvm_session_object_t* session_obj,
  mvm_object_t** ret_job_obj
)
{
  int32_t rc;
  mvm_simple_job_object_t* job_obj;

  if ( ( session_obj == NULL ) || ( ret_job_obj == NULL ) )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( session_obj->session_ctrl.goal != MVM_GOAL_ENUM_NONE )
  { /* The session shouldn't be doing anything. */
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );

  session_obj->session_ctrl.goal = new_goal;
    /* The goal is reset by the state machine on completion. */
  session_obj->session_ctrl.statejob_handle = job_obj->header.handle;
    /* The statejob_handle will signal completion. The statejob_handle is
     * reset by the state machine on completion.
     */
  *ret_job_obj = ( ( mvm_object_t* ) job_obj );
    /* ret_job_obj stores the job_obj to be checked for completion and to be
     * freed by the current pending command control.
     */

  return APR_EOK;
}

/* Returns APR_EOK when done and packet completed, else APR_EPENDING. */
static int32_t mvm_helper_simple_wait_for_goal_completion_control (
  aprv2_packet_t* packet,
  mvm_object_t* job_obj
)
{
  int32_t rc;
  int32_t status;
  uint16_t client_addr;

  if ( ( packet == NULL ) || ( job_obj == NULL ) )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  /* Wait until the job is done. */
  if ( job_obj->simple_job.is_completed )
  {
    status = job_obj->simple_job.status;
    ( void ) mvm_free_object( job_obj );

    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, packet, status );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  return APR_EPENDING;
}

static bool_t mvm_helper_verify_full_control (
  mvm_pending_control_t* ctrl,
  mvm_indirection_object_t* indirect_obj
)
{
  int32_t rc;
  uint16_t client_addr;

  if ( ctrl == NULL || indirect_obj == NULL )
  {
    MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return FALSE;
  }

  if ( APR_GET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                      indirect_obj->access_bits ) )
  {
    return TRUE;
  }
  else
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_helper_verify_full_control(): Access denied: opcode=0x%08X src_addr=0x%08X src_port=0x%08X",
                                            ctrl->packet->opcode,
                                            ctrl->packet->src_addr,
                                            ctrl->packet->src_port );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EDENIED );
    MVM_COMM_ERROR( rc, client_addr );
    return FALSE;
  }
}

/****************************************************************************
 * MVM SESSION NON-GATIMG EVENT PROCESSING ROUTINES                         *
 ****************************************************************************/

static int32_t mvm_core_trigger_reconfig (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_simple_job_object_t* job_obj;

  rc = mvm_get_typed_object( packet->dst_port, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_core_trigger_reconfig(): Failed to get mvm session object. " \
                                          "Dropping event packet." );
    ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

    return rc;
  }

  rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
  MVM_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
    mvm_simple_self_destruct_result_rsp_fn;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         job_obj->header.handle, VSS_IMVM_CMD_RECONFIG,
         NULL, 0 );
  MVM_COMM_ERROR( rc, session_obj->self_addr );

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

  return APR_EOK;
}

static int32_t mvm_ccm_active_sessions_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  vss_iccm_evt_active_sessions_t* in_args;
  uint32_t payload_size;
  uint16_t total_num_streams;
  uint16_t total_num_vocprocs;

  rc = mvm_get_typed_object( packet->dst_port, MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_active_sessions_evt_processing(): Failed to get mvm " \
                                          "session object. Dropping event packet." );
    ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

    return rc;
  }

  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
  if ( payload_size != sizeof( vss_iccm_evt_active_sessions_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_ccm_active_sessions_evt_processing(): Unexpected payload " \
                                            "size, %d != %d, Dropping event packet.",
                                            payload_size,
                                            sizeof( vss_iccm_evt_active_sessions_t ) );
    ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

    return APR_EBADPARAM;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_iccm_evt_active_sessions_t, packet );

  total_num_streams = ( in_args->num_nb_streams + in_args->num_wb_streams +
                        in_args->num_swb_streams + in_args->num_fb_plus_streams );
  total_num_vocprocs = ( in_args->num_nb_vocprocs + in_args->num_wb_vocprocs +
                         in_args->num_swb_vocprocs + in_args->num_fb_plus_vocprocs );

  if ( ( in_args->num_voice_sessions == 1 ) && ( total_num_streams <= 1 ) &&
       ( total_num_vocprocs <= 1 ) )
  {
    session_obj->target_set.clock_and_concurrency_config.is_multi_session = FALSE;
  }
  else
  {
    session_obj->target_set.clock_and_concurrency_config.is_multi_session = TRUE;
  }

  session_obj->target_set.clock_and_concurrency_config.total_core_kpps =
    in_args->total_core_kpps;

  /* An MVM session can receive the VSS_ICCM_EVT_ACTIVE_SESSIONS event under two
   * scenerios:
   * 1. This MVM session is transitioning from INIT to IDLE state, where it
   *    sends the VSS_IMVM_EVT_VOICE_SESSION_ACTIVE event to the CCM. Upon CCM
   *    receives the event from this MVM session, it will send
   *    VSS_ICCM_EVT_ACTIVE_SESSIONS to this MVM session and all the other
   *    active MVM sessions in the system.
   * 2. This MVM session is in IDLE or RUN state, and another MVM session sends
   *    the VSS_IMVM_EVT_VOICE_SESSION_ACTIVE or
   *    VSS_IMVM_EVT_VOICE_SESSION_INACTIVE event to the CCM. Upon CCM receives
   *    the event from an MVM session, it will broadcast the
   *    VSS_ICCM_EVT_ACTIVE_SESSIONS to all the active MVM sessions.
   *
   * In scenerio 1, this MVM session's state machine is gated in the
   * MVM_STATE_ENUM_INIT_ENTRY state where it is wating for the
   * VSS_ICCM_EVT_ACTIVE_SESSIONS event to know the current system concurrency
   * configurations and VDSP's per hw thread KPPS in order to continue the INIT
   * to IDLE state transition.
   *
   * In scenerio 2, if the clock or concurrency configurations have changed,
   * then this MVM session's state machine must transition to INIT state first
   * and back to the state where it was in prior to receiving the
   * VSS_ICCM_EVT_ACTIVE_SESSIONS, in order to re-calculate and re-apply the
   * voice timing offsets based on the new system concurrency configurations
   * and VDSP's per hw thread KPPS.
   */

  if ( session_obj->session_ctrl.state == MVM_STATE_ENUM_INIT_ENTRY )
  {
    session_obj->session_ctrl.status = APR_EOK;

    mvm_signal_run( MVM_THREAD_PRIORITY_ENUM_MED ); /* Trigger the session state control to run. */

    ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
  }
  else
  {
    /* If the clock or concurrency configurations have changed, trigger MVM to
     * reconfig, in order to go through current state -> INIT -> current state
     * transition. Otherwise, drop the event.
     */
    if ( ( session_obj->active_set.clock_and_concurrency_config.is_multi_session !=
           session_obj->target_set.clock_and_concurrency_config.is_multi_session ) ||
         ( session_obj->active_set.clock_and_concurrency_config.total_core_kpps !=
           session_obj->target_set.clock_and_concurrency_config.total_core_kpps ) )
    {
      ( void ) mvm_core_trigger_reconfig( packet );
    }
    else
    {
      ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
    }
  }

  return APR_EOK;
}

static int32_t mvm_stream_voc_operating_mode_update_evt_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_simple_job_object_t* job_obj;
  vss_istream_evt_voc_operating_mode_update_t* payload;
  uint32_t payload_size;
  vss_imvm_cmd_dynamic_reconfig_t dynamic_reconfig;

  for ( ;; )
  {
    rc = mvm_get_typed_object( packet->dst_port, MVM_OBJECT_TYPE_ENUM_SESSION,
                               ( ( mvm_object_t** ) &session_obj ) );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_stream_voc_operating_mode_update_evt_processing(): " \
                                            "Failed to get mvm session object. Dropping event packet." );

      break;
    }

    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
    if ( payload_size != sizeof( vss_istream_evt_voc_operating_mode_update_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_stream_voc_operating_mode_update_evt_processing(): " \
                                              "Unexpected payload size, %d != %d, Dropping event packet.",
                                              payload_size,
                                              sizeof( vss_istream_evt_voc_operating_mode_update_t ) );

      rc = APR_EBADPARAM;
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_voc_operating_mode_update_t,
                                     packet );

    if ( ( mvm_voc_op_mode_is_valid( payload->rx_mode ) == FALSE ) ||
         ( mvm_voc_op_mode_is_valid( payload->tx_mode ) == FALSE ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_stream_voc_operating_mode_update_evt_processing(): " \
             "Invalid Rx vocoder op mode 0x%08X or Tx vocoder op mode 0x%08X. Dropping event packet.",
             payload->rx_mode, payload->tx_mode );

      rc = APR_EBADPARAM;
      break;
    }

    if ( session_obj->is_voc_op_mode_evt_received == FALSE )
    {
      /* The VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event is received due to
       * the first stream being attached to MVM. Update the vocoder operating
       * mode book-keeping variables.
       */
      session_obj->is_voc_op_mode_evt_received = TRUE;

      session_obj->target_set.system_config.rx_voc_op_mode = payload->rx_mode;
      session_obj->target_set.system_config.tx_voc_op_mode = payload->tx_mode;
    }
    else
    {
      /* The VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event is received after
       * a stream has been attached to MVM. Queue the operating mode update to
       * pending command queue via the VSS_IMVM_CMD_DYNAMIC_RECONFIG command
       * for further processing.
       */
      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_simple_self_destruct_result_rsp_fn;

      dynamic_reconfig.rx_voc_op_mode = payload->rx_mode;
      dynamic_reconfig.tx_voc_op_mode = payload->tx_mode;

      rc = __aprv2_cmd_alloc_send(
             mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             job_obj->header.handle, VSS_IMVM_CMD_DYNAMIC_RECONFIG,
             &dynamic_reconfig, sizeof( dynamic_reconfig ) );
      MVM_COMM_ERROR( rc, session_obj->self_addr );
    }

    break;
  }

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

  return rc;
}


static int32_t mvm_broadcast_voice_activity_event (
  aprv2_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  uint32_t payload_size;
  mvm_session_object_t* session_obj;

  for ( ;; )
  {
    rc = mvm_get_typed_object( packet->dst_port, MVM_OBJECT_TYPE_ENUM_SESSION,
                               ( ( mvm_object_t** ) &session_obj ) );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "mvm_broadcast_voice_activity_event(): Failed to get mvm session "
           "object. Dropping event packet." );
      ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
      break;
    }

    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
    if ( payload_size != sizeof( vss_icommon_evt_voice_activity_update_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "mvm_broadcast_voice_activity_event(): Unexpected payload "
             "size, %d != %d, Dropping event packet.", payload_size,
             sizeof( vss_istream_evt_voc_operating_mode_update_t ) );
      ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
      break;
    }

    if( ( TRUE == voice_activity_client.is_enabled ) &&
        ( APR_NULL_V != voice_activity_client.client_addr ) )
    {
      /* Broadcast the voice activity event recieved from CVP/CVS to client 
       * listening for the same.
       */
      packet->src_addr = mvm_my_addr;
      packet->src_port = APR_NULL_V;
      packet->dst_addr = voice_activity_client.client_addr;
      packet->dst_port = voice_activity_client.client_port;

      rc = __aprv2_cmd_forward( mvm_apr_handle, packet );
      MVM_COMM_ERROR( rc, voice_activity_client.client_addr );
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "mvm_broadcast_voice_activity_event(): Dropping voice activity event "
           "Packet as no client is listening for the same");
      ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
    }

    break;
  }

  return APR_EOK;
}


static int32_t mvm_avtimer_get_time_cmd (
  aprv2_packet_t* packet
)
{
  int32_t rc = APR_EFAILED;
  uint16_t client_addr;
  vss_iavtimer_rsp_get_time_t avtimer_timestamp;
  vocsvc_avtimer_timestamp_t time;

  client_addr = packet->src_addr;

#ifndef WINSIM
  rc = vocsvc_avtimer_get_time( &time );
  avtimer_timestamp.timestamp_us = time.timestamp_us;
#endif

  if ( rc )
  {
    /* Send APR_EFAILED command response to client.
       provided the vocsvc_avimer_get_time() return error. */
    rc = __aprv2_cmd_end_command( mvm_apr_handle, packet, APR_EFAILED );
    MVM_COMM_ERROR( rc, client_addr);
  }
  else
  {
    /* Send VSS_IAVTIMER_RSP_GET_TIME command response to
       client provided the vocsvc_avimer_call() return error. */
    rc = __aprv2_cmd_alloc_send(
           mvm_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           mvm_my_addr, APR_NULL_V,
           client_addr, packet->src_port,
           packet->token, VSS_IAVTIMER_RSP_GET_TIME,
           &avtimer_timestamp, sizeof( avtimer_timestamp ) );
    MVM_COMM_ERROR( rc, client_addr);
  }

  /* Free the incoming command packet. */
  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );

  return APR_EOK;
}

/****************************************************************************
 * MVM SESSION PENDING COMMAND ROUTINES                                     *
 ****************************************************************************/

static int32_t mvm_core_create_session_cmd_control (
  mvm_pending_control_t* ctrl,
  bool_t is_full_ctrl
)
{
  int32_t rc;
  uint32_t access_bits;
  mvm_indirection_object_t* indirect_obj;
  mvm_session_object_t* session_obj;
  char_t* req_session_name;
  uint32_t req_session_name_size;
  uint16_t client_addr;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    access_bits = APR_SET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                                 ( ( is_full_ctrl ) ? 1 : 0 ) );

    req_session_name = APRV2_PKT_GET_PAYLOAD( char_t, ctrl->packet );
    req_session_name_size = ( ( uint16_t ) APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header ) );

    if ( req_session_name_size > 0 )
    {
    	 MSG_SPRINTF_1(MSG_SSID_QDSP6, MSG_LEGACY_HIGH,"mvm_core_create_session_cmd_control(): Session name = %s",
                                            req_session_name );
    }

    rc = mvm_create_indirection_object( client_addr,
                                        req_session_name,
                                        req_session_name_size,
                                        access_bits,
                                        &indirect_obj,
                                        &session_obj );
    if ( rc )
    {
      rc =  __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, rc );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    { /* Initialize the session object. */

      /* Setup for the first full control client. */
      if ( is_full_ctrl )
      {
        if ( session_obj->master_addr == APRV2_PKT_INIT_ADDR_V )
        {
          session_obj->master_addr = ctrl->packet->src_addr;
          session_obj->master_port = ctrl->packet->src_port;
        }
        else
        { /* Allow only one full control session per named session. */
          ( void ) mvm_free_object( ( mvm_object_t* ) indirect_obj );
          rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EALREADY );
          MVM_COMM_ERROR( rc, client_addr );
          return APR_EOK;
        }
      }
    }

    { /* Update the packet->dst_port to the new session handle. */
      ctrl->packet->dst_port = ( ( uint16_t ) indirect_obj->header.handle );
    }

    /* If this is not the first handle being opened for this session then
     * state machine is already out of reset and there's no need to run it.
     */
    if ( session_obj->indirection_q.size >= 2 )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
    else
    {
      rc = mvm_helper_create_new_goal_control(
             MVM_GOAL_ENUM_CREATE, session_obj, &ctrl->pendjob_obj );
      if ( rc ) return APR_EOK;
    }
  }
  else
  {
    /* Wait until the job is done. */
    if ( ctrl->pendjob_obj->simple_job.is_completed )
    {
      if ( ctrl->pendjob_obj->simple_job.status )
      { /* Destroy the session and respond a failure. */
        MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
        /* TODO: Need to handle error recovery on session setup failure. */
      }
      else
      { /* Respond success. */
        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
        MVM_COMM_ERROR( rc, client_addr );
      }

      ( void ) mvm_free_object( ctrl->pendjob_obj );
      return APR_EOK;
    }
  }

  /* Run the CVD session state machine. The CVD session state machine should
   * only need to run when there are pending commands to process.
   */
  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  MVM_PANIC_ON_ERROR( rc );
  rc = mvm_state_control( session_obj );

  return APR_EPENDING;
}


static int32_t mvm_core_destroy_session_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_indirection_object_t* indirect_obj;
  mvm_session_object_t* session_obj;
  uint16_t client_addr;

  client_addr = ctrl->packet->src_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc )
  {
    return APR_EOK;
  }

  if( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* Do not run state machine when there are
     * multiple indirections still alive. */
    if ( session_obj->indirection_q.size >= 2 )
    {
      if ( APR_GET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                        indirect_obj->access_bits ) )
      {
        session_obj->master_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->master_port = APR_NULL_V;
      }

      ( void ) mvm_free_object( ( mvm_object_t* ) indirect_obj );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
    else /* No multiple indirections alive */
    {
      rc = mvm_helper_create_new_goal_control( MVM_GOAL_ENUM_DESTROY, session_obj,
                                               &ctrl->pendjob_obj );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_core_destroy_session_cmd_control(): Failed to create new goal MVM_GOAL_ENUM_DESTROY, "
               "result: 0x%08X", rc );
        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EFAILED );
        return APR_EOK;
      }
    }
  }

  rc = mvm_helper_simple_wait_for_goal_completion_control(
       ctrl->packet, ctrl->pendjob_obj );
  if ( rc == APR_EOK )
  {
    ( void )mvm_destroy_session_object( session_obj );
    return APR_EOK;
  }

  /* TODO: On destroy session failure we need to keep the session object
           around because we need to continue the reset process. Aborted
           CVD session objects need to be queued somewhere for later
           destruction. We still have to run the session state control on the
           session object until the RESET is complete.

           The current handling assumes the destroy operation from the
           lower layers are always successful.
  */

  /* Run the CVD session state machine. The CVD session state machine should
   * only need to run when there are pending commands to process.
   */
  rc = mvm_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t mvm_eval_voice_state_decision (
  mvm_goal_enum_t modem_state,
  mvm_goal_enum_t apps_state,
  bool_t is_dual_control,
  mvm_goal_enum_t* ret_next_state
)
{
  mvm_goal_enum_t next_state;

  if ( ret_next_state == NULL )
  {
    return APR_EBADPARAM;
  }

  if ( is_dual_control == FALSE )
  {
    /* If this is not a "default modem voice" MVM then there is only
       one controller: Apps. Therefore we go to whichever state Apps
       is requesting.
    */
    next_state = apps_state;
  }
  else
  {
    /*
      Problem:
        There are 2 clients, modem and Apps. Apps start and modem start
        are two different requests. The goal performed is based off of
        the highest operation priority.

        STOP > PAUSE > STANDBY > START

      Strategy:
        The evaluation of apps_state and modem_state be evaluated into
        integers which recommends the goal to perform on the MVM state
        machine.

        Decision matrix:

                \ apps
           modem \  stop  |  pause  | standby  | start
        ------------------------------------------------
        stop     |  stop  |  stop   | stop     | stop
        ---------|--------------------------------------
        standby  |  stop  |  pause  | standby  | standby
        ---------|--------------------------------------
        start    |  stop  |  pause  | standby  | start
    */

    if ( ( modem_state == MVM_GOAL_ENUM_STOP ) ||
         ( apps_state == MVM_GOAL_ENUM_STOP ) )
    {
      next_state = MVM_GOAL_ENUM_STOP;
    }
    else
    if ( ( modem_state == MVM_GOAL_ENUM_START ) &&
         ( apps_state == MVM_GOAL_ENUM_START ) )
    {
      next_state = MVM_GOAL_ENUM_START;
    }
    else
    if ( apps_state == MVM_GOAL_ENUM_PAUSE )
    {
      next_state = MVM_GOAL_ENUM_PAUSE;
    }
    else
    {
      next_state = MVM_GOAL_ENUM_STANDBY;
    }
  }

  *ret_next_state = next_state;

  return APR_EOK;
}

static int32_t mvm_start_voice_cmd_control (
  mvm_pending_control_t* ctrl,
  bool_t is_modem
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  mvm_sequencer_job_object_t* seqjob_obj; /* BACKWARD COMPATIBILITY */
  mvm_simple_job_object_t* subjob_obj; /* BACKWARD COMPATIBILITY */
  mvm_goal_enum_t next_goal;
  vss_istream_cmd_attach_vocproc_t stream_attach_vocproc; /* BACKWARD COMPATIBILITY */
  mvm_generic_item_t* generic_item; /* BACKWARD COMPATIBILITY */
  uint16_t client_addr;
  enum { /* BACKWARD COMPATIBILITY */
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_ADD_DEFAULT_MODEM_STREAM_TO_LIST,
    MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_DEFAULT_MODEM_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_SEND_START,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* The modem command is allowed only with full control handle. */
    if ( is_modem && ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE ) )
    {
      return APR_EOK;
    }

    /* BACKWARD COMPATIBILITY */
    /* In order to maintain compatibility with MVS 1.0 we can
     * no longer rely on MVS explicity setting dual control with the
     * VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL command and must therefore
     * use the name "default modem voice" as in MVS 1.0.
     */
    if ( mmstd_strncmp( session_obj->session_name,
                        sizeof( session_obj->session_name ),
                        mvm_default_modem_session_name,
                        sizeof( mvm_default_modem_session_name ) )
         == 0 )
    {
      session_obj->is_dual_control = TRUE;
    }

    if ( session_obj->is_avtimer_handle_open == FALSE )
    {
    /* Open the AVTIMER_HANDLE for providng support for IAVTIMER Interface. */
#ifndef WINSIM
    rc = vocsvc_avtimer_open_handle();
#endif
      session_obj->is_avtimer_handle_open = TRUE;
    if ( rc )
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_start_voice_cmd_control(): vocsvc_avtimer_open_handle"
                                            "Failed with error: rc = %d", rc );
    }

    /* Run the sequencer. */ /* BACKWARD COMPATIBILITY */
    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_ADD_DEFAULT_MODEM_STREAM_TO_LIST;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  /* BACKWARD COMPATIBILITY
   *
   * This sequencer is needed for MVS 1.0 backward comaptibility reasons only.
   * For MVS 2.0+, commanding the state machine to start is sufficient.
   *
   * Background:
   *
   *   In MVS 1.0 behavior, the media type is set by MVS on MVM and propagated
   *   by MVM to the "default modem voice" stream based on MVM's own passive control
   *   handle to the stream.
   *   In MVS 2.0+ behavior, the media type is set by MVS directly on stream
   *   and MVM does not create a passive handle to the stream.
   *
   * Behavior to handle backward compatibility:
   *
   *   Common to MVS 1.0 and MVS 2.0+:
   *
   *     Regardless of MVS version, MVM creates a passive control session
   *     for the "default modem voice" stream in reset state and stores it in
   *     session_obj->default_modem_stream_handle.
   *     See comments in mvm_stream_create_session_transition_result_rsp_fn().
   *
   *   Specific to MVS 1.0:
   *
   *     In the case of MVS 1.0 we must insert the default_modem_stream_handle
   *     into the MVM's stream_q, such that MVM state machine commands can be
   *     issued from MVM to the "default modem voice" stream. We do this on
   *     the first START command from MODEM (and only once).
   *     The reason we do it then, is because it is only at that time that we
   *     know for sure whether we are operating in MVS 1.0 or MVS 2.0 mode
   *     (i.e. the VSS_ISTREAM_CMD_SET_MEDIA_TYPE command has been issued).
   *     The reason we don't do it before, is because of the use case where
   *     Apps is brought up before modem which would cause the attach vocproc
   *     by Apps on MVM to be sent to the "default modem voice" stream twice
   *     (with the scond one failing with E_ALREADY). Because of the same use
   *     case we must also attach the stream to any vocprocs already attached
   *     to MVM at this point, as seen in the sequencer below.
   *
   *   Specific to MVS 2.0+:
   *
   *     We do nothing and skip directly to running the state machine.
   *     The "default modem voice" stream handle is already in the MVM's
   *     stream_q, because it was attached by MVS.
   */
  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_ADD_DEFAULT_MODEM_STREAM_TO_LIST:
      {
        if ( ( is_modem == TRUE ) &&
             ( session_obj->is_dual_control == TRUE ) &&
             ( session_obj->is_mvs_v1_policy == TRUE ) &&
             ( session_obj->is_default_modem_stream_attached == FALSE ) )
        { /* Insert the default modem stream into the attached stream list, attach it to all the
           * vocprocs currently attached to MVM and reconfigure all the streams and vocprocs
           * attached to MVM and then command the state machine to START.
           */
          rc = apr_list_remove_head( &session_obj->free_item_q,
                                     ( ( apr_list_node_t** ) &generic_item ) );
          if ( rc )
          {
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_start_voice_cmd_control(): MVS 1.0 BACKWARD COMPATIBILITY: Failed to add " \
                                                  "default modem stream to the list due to empty q" );
            seqjob_obj->state = MVM_SEQUENCER_ENUM_SEND_START;
            seqjob_obj->status = APR_EOK;
            continue;
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_DEFAULT_MODEM_STREAM;
            seqjob_obj->status = APR_EOK;
          }

          generic_item->handle = session_obj->default_modem_stream_handle;
          ( void ) apr_list_add_tail( &session_obj->stream_q, &generic_item->link );
        }
        else
        { /* Go directly to commanding the sate machine to START. */
          seqjob_obj->state = MVM_SEQUENCER_ENUM_SEND_START;
          seqjob_obj->status = APR_EOK;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_DEFAULT_MODEM_STREAM:
      { /* Attach the default modem stream to all vocprocs attached to MVM. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
        session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;

        generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

        for ( ;; )
        {
          rc = apr_list_get_next( &session_obj->vocproc_q,
                                  ( ( apr_list_node_t* ) generic_item ),
                                  ( ( apr_list_node_t** ) &generic_item ) );
          if ( rc ) break;

          {
            rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
            MVM_PANIC_ON_ERROR( rc );
            subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
              mvm_stream_attach_detach_vocproc_result_count_rsp_fn;

            stream_attach_vocproc.handle = ( uint16_t ) generic_item->handle;

            rc = __aprv2_cmd_alloc_send(
                   mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                   session_obj->default_modem_stream_addr,
                   session_obj->default_modem_stream_handle,
                   subjob_obj->header.handle,
                   VSS_ISTREAM_CMD_ATTACH_VOCPROC,
                   &stream_attach_vocproc, sizeof( stream_attach_vocproc ) );
            MVM_COMM_ERROR( rc, session_obj->default_modem_stream_addr );
          }
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for CVS to respond for each attach_vocproc command. */
        if ( session_obj->stream_attach_detach_vocproc_rsp_cnt < session_obj->vocproc_q.size )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED;

        session_obj->is_default_modem_stream_attached = TRUE;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED:
      { /* Notify the stream that it is being attached to an MVM session. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->default_modem_stream_addr,
               session_obj->default_modem_stream_handle,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_MVM_ATTACH, NULL, 0 );
        MVM_COMM_ERROR( rc, session_obj->default_modem_stream_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for CVS to respond to the VSS_ISTREAM_CMD_MVM_ATTACH command. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
        {
          /* If the session state machine is not in INIT state, we must
           * transition to INIT state before proceeding to
           * MVM_SEQUENCER_ENUM_SEND_START, so that all the actions required
           * for INIT to IDLE transition will be performed when the MVM session
           * wants to RUN, in order to:
           * 1. Configure the newly attached stream.
           * 2. Report the new session configurations (number of nb and wb
           *    streams/vocprocs and total KPPS requirement) to CCM.
           * 3. Re-calculate and re-apply the stream/vocproc timing offsets.
           */

          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

          rc = mvm_helper_create_new_goal_control(
                 MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
          /* TODO: HACK: Need fix the create new goal to return an error rather
           *             than completing the command packet immediately on
           *             error. The sequencer should only have one exit point
           *             for completion to prevent sequences from failing to
           *             deallocate resources on exit.
           */
          if ( rc ) return APR_EOK;
#else
          if ( rc )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = rc;
            continue;
          }
#endif /* 0 */
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_SEND_START;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_SEND_START;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_SEND_START:
      { /* Command the state machine to START. */
        if ( is_modem == TRUE )
        {
          session_obj->modem_state = MVM_GOAL_ENUM_START;
        }
        else
        {
          session_obj->apps_state = MVM_GOAL_ENUM_START;
        }
        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &next_goal );

        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

        if ( APR_GET_FIELD( MVM_INDIRECTION_ACCESS_FULL_CONTROL,
                            indirect_obj->access_bits ) )
        {
          /* If a full control client issue start to the session, the stale
           * mailbox packet exchange time reference provided by the full
           * control client on the stream will need to be cleared when the
           * session needs to run.
           */
          session_obj->mailbox_pktexg_stream_info.is_clear_time_ref_needed = TRUE;
        }

        rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_standby_voice_cmd_control (
  mvm_pending_control_t* ctrl,
  bool_t is_modem
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  mvm_goal_enum_t next_goal;
  vss_istream_cmd_set_media_type_t stream_media_type;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_SEND_STANDBY,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_STREAM_SET_MEDIA_TYPE,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* The modem command is allowed only with full control handle. */
    if ( is_modem && ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE ) )
    {
      return APR_EOK;
    }

    /* BACKWARD COMPATIBILITY */
    /* In order to maintain compatibility with MVS 1.0 and 2.0 we can
     * no longer rely on MVS explicity setting dual control with the
     * VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL command and must therefore
     * use the name 'default modem voice' as in MVS 1.0.
     */
    if ( mmstd_strncmp( session_obj->session_name,
                        sizeof( session_obj->session_name ),
                        mvm_default_modem_session_name,
                        sizeof( mvm_default_modem_session_name ) )
         == 0 )
    {
      session_obj->is_dual_control = TRUE;
    }

    /* Run the sequencer. */
    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_SEND_STANDBY;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_SEND_STANDBY:
      { /* Command the MVM state machine to go to standby. */
        if ( is_modem == TRUE )
        {
          session_obj->modem_state = MVM_GOAL_ENUM_STANDBY;
        }
        else
        {
          session_obj->apps_state = MVM_GOAL_ENUM_STANDBY;
        }
        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &next_goal );

        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                                 &seqjob_obj->subjob_obj );
#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_STREAM_SET_MEDIA_TYPE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_STREAM_SET_MEDIA_TYPE:
      { /* BACKWARD COMPATIBILITY */ /* Command the new media-type on the CVS. */
        if ( session_obj->is_mvs_v1_policy == TRUE )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        stream_media_type.rx_media_id = session_obj->target_set.system_config.media_id;
        stream_media_type.tx_media_id = session_obj->target_set.system_config.media_id;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->default_modem_stream_addr, session_obj->default_modem_stream_handle,
               subjob_obj->header.handle, VSS_ISTREAM_CMD_SET_MEDIA_TYPE,
               &stream_media_type, sizeof( stream_media_type ) );
        MVM_COMM_ERROR( rc, session_obj->default_modem_stream_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the CVS to set the media-type. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_pause_voice_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_goal_enum_t next_goal;

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* BACKWARD COMPATIBILITY */
    /* In order to maintain compatibility with MVS 1.0 and 2.0 we can
     * no longer rely on MVS explicity setting dual control with the
     * VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL command and must therefore
     * use the name 'default modem voice' as in MVS 1.0.
     */
    if ( mmstd_strncmp( session_obj->session_name,
                        sizeof( session_obj->session_name ),
                        mvm_default_modem_session_name,
                        sizeof( mvm_default_modem_session_name ) )
         == 0 )
    {
      session_obj->is_dual_control = TRUE;
    }

    session_obj->apps_state = MVM_GOAL_ENUM_PAUSE;

    ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                            session_obj->apps_state,
                                            session_obj->is_dual_control,
                                            &next_goal );

    rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                             &ctrl->pendjob_obj );
    if ( rc ) return APR_EOK;
  }
  else
  {
    rc = mvm_helper_simple_wait_for_goal_completion_control(
           ctrl->packet, ctrl->pendjob_obj );
    if ( rc == APR_EOK ) return APR_EOK;
  }

  rc = mvm_state_control( session_obj );

  return APR_EPENDING;
}

/* TODO: Modify mvm_stream_stop_voice_cmd_control() so that it handles the
         disable sequence. We need to stop the voice stream because
         DISABLE is a type of tear down.

         Then do STANDBY!  Just mute the voice stream.
*/
static int32_t mvm_stop_voice_cmd_control (
  mvm_pending_control_t* ctrl,
  bool_t is_modem
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  mvm_goal_enum_t next_goal;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* The modem command is allowed only with full control handle. */
    if ( is_modem && ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE ) )
    {
      return APR_EOK;
    }

    session_obj->mailbox_pktexg_stream_info.is_disable_rx_expiry_processing = TRUE;

    if ( is_modem == TRUE )
    {
      session_obj->modem_state = MVM_GOAL_ENUM_STOP;
    }
    else
    {
      session_obj->apps_state = MVM_GOAL_ENUM_STOP;
      session_obj->is_stream_reinit_required = TRUE;
      session_obj->is_vocproc_reinit_required = TRUE;
    }

    ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                            session_obj->apps_state,
                                            session_obj->is_dual_control,
                                            &next_goal );

    rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                             &ctrl->pendjob_obj );
    if ( rc ) return APR_EOK;
  }
  else
  {
    rc = mvm_helper_simple_wait_for_goal_completion_control(
           ctrl->packet, ctrl->pendjob_obj );
    if ( rc == APR_EOK )
    {
      if ( session_obj->is_avtimer_handle_open == TRUE )
      {
        /* Close the AVTIMER_HANDLE required for IAVTIMER Interface. */
#ifndef WINSIM
        rc = vocsvc_avtimer_close_handle();
#endif
        session_obj->is_avtimer_handle_open = FALSE;

        if ( rc )
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_stop_voice_cmd_control():"
               " vocsvc_avtimer_close_handle Failed with error: rc = %d", rc );
      }

      return APR_EOK;
    }
  }

  /* Assuming that HLOS will call STOP_VOICE only when the call ends.*/
  if ( is_modem == FALSE )
  {
    mvm_debug_call_num++;
  }

  rc = mvm_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t mvm_attach_stream_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imvm_cmd_attach_stream_t* in_args;
  vss_istream_cmd_attach_vocproc_t stream_attach_vocproc;
  mvm_goal_enum_t eval_state;
  mvm_generic_item_t* generic_item;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_DISABLE_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_ADD_STREAM_TO_LIST,
    MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE,
    MVM_SEQUENCER_ENUM_WAIT_5,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_imvm_cmd_attach_stream_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_attach_stream_t,
                                     ctrl->packet );

    client_addr = ctrl->packet->src_addr;

    if ( NULL == in_args->handle )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_attach_stream_cmd_control(): NULL Stream handle" );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.attach_stream.cvs_handle = in_args->handle;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_DISABLE_STREAM;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_DISABLE_STREAM:
      {
        /* Report error if the client attaches the same stream twice. */
        /* Check if stream is already attached. */
        rc = mvm_find_object_from_session(
               &session_obj->stream_q,
               seqjob_obj->use_case.attach_stream.cvs_handle,
               &generic_item );
        if ( rc == APR_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_attach_stream_cmd_control(): stream[0x%08X] already attached",
            seqjob_obj->use_case.attach_stream.cvs_handle );
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EALREADY;
          continue;
        }

        /* Disable the CVS. (Currently we do not expose stream enable/disable
         * publicly so the CVS must necessarily be disabled at this stage
         * and this operation is not really necessary. But doing it just in
         * case to be future proof).
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, seqjob_obj->use_case.attach_stream.cvs_handle,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_DISABLE, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for CVS to disable. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        if ( subjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_STREAM;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
        }
        else
        { /* Complete and return error to client if disable CVS failed. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "mvm_attach_stream_cmd_control(): stream[0x%08X] disable failed, rc = (0x%08X)",
                 seqjob_obj->use_case.attach_stream.cvs_handle, subjob_obj->status );
          seqjob_obj->status = subjob_obj->status;
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }

      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_ATTACH_VOCPROCS_TO_STREAM:
      {
        /* Attach all the vocprocs that are currently under MVM's jurisdiction
         * to this stream.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;
        session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;

        generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

        for ( ;; )
        {
          rc = apr_list_get_next( &session_obj->vocproc_q,
                                  ( ( apr_list_node_t* ) generic_item ),
                                  ( ( apr_list_node_t** ) &generic_item ) );
          if ( rc ) break;

          {
            rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
            MVM_PANIC_ON_ERROR( rc );
            subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
              mvm_stream_attach_detach_vocproc_result_count_rsp_fn;

            stream_attach_vocproc.handle = ( uint16_t ) generic_item->handle;

            rc = __aprv2_cmd_alloc_send(
                   mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                   mvm_cvs_addr, seqjob_obj->use_case.attach_stream.cvs_handle,
                   subjob_obj->header.handle,
                   VSS_ISTREAM_CMD_ATTACH_VOCPROC,
                   &stream_attach_vocproc, sizeof( stream_attach_vocproc ) );
            MVM_COMM_ERROR( rc, mvm_cvs_addr );
          }
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for CVS to respond for each attach_vocproc command. */
        if ( session_obj->stream_attach_detach_vocproc_rsp_cnt < session_obj->vocproc_q.size )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_ADD_STREAM_TO_LIST;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_ADD_STREAM_TO_LIST:
      { /* Add the CVS into the active stream list. */
        rc = apr_list_remove_head( &session_obj->free_item_q,
                                   ( ( apr_list_node_t** ) &generic_item ) );
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_ENORESOURCE;
          continue;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED;
        }

        generic_item->handle = seqjob_obj->use_case.attach_stream.cvs_handle;

        ( void ) apr_list_add_tail( &session_obj->stream_q, &generic_item->link );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_NOTIFY_STREAM_ATTACHED:
      { /* Notify the stream that it is being attached to an MVM session. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, seqjob_obj->use_case.attach_stream.cvs_handle,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_MVM_ATTACH, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_3:
      {
        /* Wait for CVS to respond to the VSS_ISTREAM_CMD_MVM_ATTACH command;
         * as well as the VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event from
         * the stream.
         */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        if ( subjob_obj->status != APR_EOK )
        { /* Complete and return error to client if attach MVM to CVS failed. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "mvm_attach_stream_cmd_control(): stream[0x%08X] MVM attach failed, rc = (0x%08X)",
                 seqjob_obj->use_case.attach_stream.cvs_handle, subjob_obj->status );
          seqjob_obj->status = subjob_obj->status;
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }

        /* Wait for VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event if this
         * event has never been received from any of the attached streams. The
         * stream that is being attached to MVM in this sequencer will send the
         * VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE event upon receiving the
         * VSS_ISTREAM_CMD_MVM_ATTACH command.
         */
        if ( session_obj->is_voc_op_mode_evt_received == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
        {
          /* If the session state machine is not in INIT state, we must
           * transition to INIT state first and then transition back to the
           * state that this session was in prior to receiving ths command, in
           * order to:
           * 1. Configure the newly attached stream.
           * 2. Report the new session configurations (number of nb and wb
           *    streams/vocprocs and total KPPS requirement) to CCM.
           * 3. Re-calculate and re-apply the stream/vocproc timing offsets.
           */

          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

          rc = mvm_helper_create_new_goal_control(
                 MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
          /* TODO: HACK: Need fix the create new goal to return an error rather
           *             than completing the command packet immediately on
           *             error. The sequencer should only have one exit point
           *             for completion to prevent sequences from failing to
           *             deallocate resources on exit.
           */
          if ( rc ) return APR_EOK;
#else
          if ( rc )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = rc;
            continue;
          }
#endif /* 0 */
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EOK;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE:
      { /* Command the state machine to transition back to the state it was. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_5;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &eval_state );

        rc = mvm_helper_create_new_goal_control( eval_state, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_detach_stream_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imvm_cmd_detach_stream_t* in_args;
  mvm_goal_enum_t eval_state;
  mvm_generic_item_t* generic_item;
  vss_istream_cmd_detach_vocproc_t stream_detach_vocproc;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_REMOVE_STREAM_FROM_LIST,
    MVM_SEQUENCER_ENUM_DETACH_VOCPROCS_FROM_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_STREAM_DISABLE,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_NOTIFY_STREAM_DETACHED,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_5,
    MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE,
    MVM_SEQUENCER_ENUM_WAIT_6,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_imvm_cmd_detach_stream_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_detach_stream_t,
                                     ctrl->packet );

    client_addr = ctrl->packet->src_addr;
    if ( NULL == in_args->handle )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_detach_stream_cmd_control(): NULL Stream handle" );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.detach_stream.cvs_handle = in_args->handle;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_REMOVE_STREAM_FROM_LIST;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_REMOVE_STREAM_FROM_LIST:
      { /* Remove the CVS from the active stream list. */
        rc = mvm_find_object_from_session(
               &session_obj->stream_q,
               seqjob_obj->use_case.detach_stream.cvs_handle,
               &generic_item );
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EFAILED;
          continue;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_DETACH_VOCPROCS_FROM_STREAM;
        }

        ( void ) apr_list_delete( &session_obj->stream_q, &generic_item->link );
        ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_DETACH_VOCPROCS_FROM_STREAM:
      {
        /* Detach all the vocprocs that are currently under MVM's jurisdiction
         * from this stream.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
        session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;

        generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

        for ( ;; )
        {
          rc = apr_list_get_next( &session_obj->vocproc_q,
                                  ( ( apr_list_node_t* ) generic_item ),
                                  ( ( apr_list_node_t** ) &generic_item ) );
          if ( rc ) break;

          {
            rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
            MVM_PANIC_ON_ERROR( rc );
            subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
              mvm_stream_attach_detach_vocproc_result_count_rsp_fn;

            stream_detach_vocproc.handle = ( uint16_t ) generic_item->handle;

            rc = __aprv2_cmd_alloc_send(
                   mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                   mvm_cvs_addr, seqjob_obj->use_case.detach_stream.cvs_handle,
                   subjob_obj->header.handle,
                   VSS_ISTREAM_CMD_DETACH_VOCPROC,
                   &stream_detach_vocproc, sizeof( stream_detach_vocproc ) );
            MVM_COMM_ERROR( rc, mvm_cvs_addr );
          }
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for CVS to respond for each detach_vocproc command. */
        if ( session_obj->stream_attach_detach_vocproc_rsp_cnt < session_obj->vocproc_q.size )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_STREAM_DISABLE;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_STREAM_DISABLE:
      { /* This stream is being taken out of the MVM's jurisdiction so disable it. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, seqjob_obj->use_case.detach_stream.cvs_handle,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_DISABLE, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
      /*-fallthru*/

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the CVS to disable. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_DISABLE_MAILBOX_PKTEXG_RX_EXPIRY_PROCESSING:
      { /* Cancel mailbox packet exchange Rx expiry processing. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, seqjob_obj->use_case.detach_stream.cvs_handle,
               subjob_obj->header.handle,
               VSS_IPKTEXG_CMD_MAILBOX_DISABLE_RX_EXPIRY_PROCESSING, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }

    case MVM_SEQUENCER_ENUM_WAIT_3:
      {
        /* Wait for CVS to respond to the
         * VSS_IPKTEXG_CMD_MAILBOX_DISABLE_RX_EXPIRY_PROCESSING command. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_NOTIFY_STREAM_DETACHED;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_NOTIFY_STREAM_DETACHED:
      { /* Notify the stream that it is being detached to an MVM session. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvs_addr, seqjob_obj->use_case.detach_stream.cvs_handle,
               subjob_obj->header.handle,
               VSS_ISTREAM_CMD_MVM_DETACH, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for CVS to respond to the VSS_ISTREAM_CMD_MVM_DEATTACH command. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
        {
          /* If the session state machine is not in INIT state, we must
           * transition to INIT state first and then transition back to the
           * state that this session was in prior to receiving ths command, in
           * order to:
           * 1. Report the new session configurations (number of nb and wb
           *    streams/vocprocs and total KPPS requirement) to CCM.
           * 2. Re-calculate and re-apply the stream/vocproc timing offsets.
           */

          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_5;

          rc = mvm_helper_create_new_goal_control(
                 MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
          /* TODO: HACK: Need fix the create new goal to return an error rather
           *             than completing the command packet immediately on
           *             error. The sequencer should only have one exit point
           *             for completion to prevent sequences from failing to
           *             deallocate resources on exit.
           */
          if ( rc ) return APR_EOK;
#else
          if ( rc )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = rc;
            continue;
          }
#endif /* 0 */
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EOK;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE:
      { /* Command the state machine to transition back to the state it was. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_6;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &eval_state );

        rc = mvm_helper_create_new_goal_control( eval_state, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_6:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        if ( session_obj->stream_q.size == 0 )
        {
          /* If there is no stream attached to MVM, reset the vocoder operating
           * mode book-keeping variables.
           */
          session_obj->is_voc_op_mode_evt_received = FALSE;

          session_obj->target_set.system_config.rx_voc_op_mode =
            VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;

          session_obj->target_set.system_config.tx_voc_op_mode =
            VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
        }

        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_attach_vocproc_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imvm_cmd_attach_vocproc_t* in_args;
  mvm_goal_enum_t eval_state;
  vss_istream_cmd_attach_vocproc_t stream_attach_vocproc;
  mvm_generic_item_t* generic_item;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_DISABLE_VOCPROC,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_ATTACH_VOCPROC_TO_STREAMS,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_ADD_VOCPROC_TO_LIST,
    MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_ATTACHED,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE,
    MVM_SEQUENCER_ENUM_WAIT_5,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_imvm_cmd_attach_vocproc_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_attach_vocproc_t,
                                     ctrl->packet );

    client_addr = ctrl->packet->src_addr;
    if ( NULL == in_args->handle )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_attach_vocproc_cmd_control(): NULL Vocproc handle" );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.attach_vocproc.cvp_handle = in_args->handle;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_DISABLE_VOCPROC;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_DISABLE_VOCPROC:
      {
        /* Report error if the client attaches the same vocproc twice. */
        /* Check if vocproc is already attached. */
        rc = mvm_find_object_from_session(
               &session_obj->vocproc_q,
               seqjob_obj->use_case.attach_vocproc.cvp_handle,
               &generic_item );
        if ( rc == APR_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_attach_vocproc_cmd_control(): vocproc[0x%08X] already attached",
            seqjob_obj->use_case.attach_vocproc.cvp_handle );
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EALREADY;
          continue;
        }

        /* Command the CVP to disable. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvp_addr, seqjob_obj->use_case.attach_vocproc.cvp_handle,
               subjob_obj->header.handle,
               VSS_IVOCPROC_CMD_MVM_DISABLE, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvp_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the CVP to disable. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        if ( subjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_ATTACH_VOCPROC_TO_STREAMS;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
        }
        else
        { /* Complete and return error to client if disable CVP failed. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "mvm_attach_vocproc_cmd_control(): vocproc[0x%08X] disable failed, rc = (0x%08X)",
                 seqjob_obj->use_case.attach_vocproc.cvp_handle, subjob_obj->status );
          seqjob_obj->status = subjob_obj->status;
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }

      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_ATTACH_VOCPROC_TO_STREAMS:
      {
        /* Attach this vocproc to all the streams that are currently under
         * MVM's jurisdiction.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;
        session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;

        stream_attach_vocproc.handle = seqjob_obj->use_case.attach_vocproc.cvp_handle;

        generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );

        for ( ;; )
          {
            rc = apr_list_get_next( &session_obj->stream_q,
                                    ( ( apr_list_node_t* ) generic_item ),
                                    ( ( apr_list_node_t** ) &generic_item ) );
            if ( rc ) break;

            {
              rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              MVM_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_stream_attach_detach_vocproc_result_count_rsp_fn;

              rc = __aprv2_cmd_alloc_send(
                     mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                     session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                     mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
                     subjob_obj->header.handle,
                     VSS_ISTREAM_CMD_ATTACH_VOCPROC,
                     &stream_attach_vocproc, sizeof( stream_attach_vocproc ) );
              MVM_COMM_ERROR( rc, mvm_cvs_addr );
            }
          }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for all the streams to respond to the attach_vocproc command. */
        if ( session_obj->stream_attach_detach_vocproc_rsp_cnt < session_obj->stream_q.size )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_ADD_VOCPROC_TO_LIST;
      }
      /*-fallthru*/

    case MVM_SEQUENCER_ENUM_ADD_VOCPROC_TO_LIST:
      { /* Add the CVP into the active vocproc list. */
        rc = apr_list_remove_head( &session_obj->free_item_q,
                                   ( ( apr_list_node_t** ) &generic_item ) );
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_ENORESOURCE;
          continue;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_ATTACHED;
        }

        generic_item->handle = seqjob_obj->use_case.attach_vocproc.cvp_handle;

        ( void ) apr_list_add_tail( &session_obj->vocproc_q, &generic_item->link );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_ATTACHED:
      { /* Notify CVP that it is being attached to an MVM session. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvp_addr, seqjob_obj->use_case.attach_vocproc.cvp_handle,
               subjob_obj->header.handle,
               VSS_IVOCPROC_CMD_MVM_ATTACH, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvp_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for CVP to respond to the VSS_IVOCPROC_CMD_MVM_ATTACH command. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }
        
        if ( subjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
        }
        else
        { /* Complete and return error to client if attach MVM to CVP failed. */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "mvm_attach_vocproc_cmd_control(): vocproc[0x%08X] attach MVM failed, rc = (0x%08X)",
                 seqjob_obj->use_case.attach_vocproc.cvp_handle, subjob_obj->status );
          seqjob_obj->status = subjob_obj->status;
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }

      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
        {
          /* If the session state machine is not in INIT state, we must
           * transition to INIT state first and then transition back to the
           * state that this session was in prior to receiving ths command, in
           * order to:
           * 1. Configure the newly attached vocproc.
           * 2. Report the new session configurations (number of nb and wb
           *    streams/vocprocs and total KPPS requirement) to CCM.
           * 3. Re-calculate and re-apply the stream/vocproc timing offsets.
           */

          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

          rc = mvm_helper_create_new_goal_control(
                 MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
          /* TODO: HACK: Need fix the create new goal to return an error rather
           *             than completing the command packet immediately on
           *             error. The sequencer should only have one exit point
           *             for completion to prevent sequences from failing to
           *             deallocate resources on exit.
           */
          if ( rc ) return APR_EOK;
#else
          if ( rc )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = rc;
            continue;
          }
#endif /* 0 */
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EOK;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE:
      { /* Command the state machine to transition back to the state it was. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_5;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &eval_state );

        rc = mvm_helper_create_new_goal_control( eval_state, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_detach_vocproc_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imvm_cmd_detach_vocproc_t* in_args;
  mvm_goal_enum_t eval_state;
  mvm_generic_item_t* generic_item;
  vss_istream_cmd_detach_vocproc_t stream_detach_vocproc;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_REMOVE_VOCPROC_FROM_LIST,
    MVM_SEQUENCER_ENUM_DETACH_VOCPROC_FROM_STREAMS,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_DETACHED,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_imvm_cmd_detach_vocproc_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_detach_vocproc_t,
                                     ctrl->packet );

    client_addr = ctrl->packet->src_addr;
    if ( NULL == in_args->handle )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "mvm_detach_vocproc_cmd_control(): NULL Vocproc handle" );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.detach_vocproc.cvp_handle = in_args->handle;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_REMOVE_VOCPROC_FROM_LIST;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_REMOVE_VOCPROC_FROM_LIST:
      { /* Remove the CVP from the active vocproc list. */
        rc = mvm_find_object_from_session(
               &session_obj->vocproc_q,
               seqjob_obj->use_case.detach_vocproc.cvp_handle,
               &generic_item );
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EFAILED;
          continue;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_DETACH_VOCPROC_FROM_STREAMS;
        }

        ( void ) apr_list_delete( &session_obj->vocproc_q, &generic_item->link );
        ( void ) apr_list_add_tail( &session_obj->free_item_q, &generic_item->link );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_DETACH_VOCPROC_FROM_STREAMS:
      {
        /* Detach this vocproc from all the streams that are currently under
         * MVM's jurisdiction.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
        session_obj->stream_attach_detach_vocproc_rsp_cnt = 0;

        stream_detach_vocproc.handle = seqjob_obj->use_case.detach_vocproc.cvp_handle;

        generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );

        for ( ;; )
        {
          rc = apr_list_get_next( &session_obj->stream_q,
                                  ( ( apr_list_node_t* ) generic_item ),
                                  ( ( apr_list_node_t** ) &generic_item ) );
          if ( rc ) break;

          {
            rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
            MVM_PANIC_ON_ERROR( rc );
            subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
              mvm_stream_attach_detach_vocproc_result_count_rsp_fn;

            rc = __aprv2_cmd_alloc_send(
                   mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                   session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                   mvm_cvs_addr, ( (uint16_t) generic_item->handle ),
                   subjob_obj->header.handle,
                   VSS_ISTREAM_CMD_DETACH_VOCPROC,
                   &stream_detach_vocproc, sizeof( stream_detach_vocproc ) );
            MVM_COMM_ERROR( rc, mvm_cvs_addr );
          }
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for all the streams to respond to the detach_vocproc command. */
        if ( session_obj->stream_attach_detach_vocproc_rsp_cnt < session_obj->stream_q.size )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_DETACHED;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_NOTIFY_VOCPROC_DETACHED:
      { /* Notify the CVP that it is being detached to an MVM session. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               mvm_cvp_addr, seqjob_obj->use_case.detach_vocproc.cvp_handle,
               subjob_obj->header.handle,
               VSS_IVOCPROC_CMD_MVM_DETACH, NULL, 0 );
        MVM_COMM_ERROR( rc, mvm_cvp_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for CVP to respond to the VSS_IVOCPROC_CMD_MVM_DETACH command. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
        {
          /* If the session state machine is not in INIT state, we must
           * transition to INIT state first and then transition back to the
           * state that this session was in prior to receiving ths command, in
           * order to:
           * 1. Report the new session configurations (number of nb and wb
           *    streams/vocprocs and total KPPS requirement) to CCM.
           * 2. Re-calculate and re-apply the stream/vocproc timing offsets.
           */

          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

          rc = mvm_helper_create_new_goal_control(
                 MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
          /* TODO: HACK: Need fix the create new goal to return an error rather
           *             than completing the command packet immediately on
           *             error. The sequencer should only have one exit point
           *             for completion to prevent sequences from failing to
           *             deallocate resources on exit.
           */
          if ( rc ) return APR_EOK;
#else
          if ( rc )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            seqjob_obj->status = rc;
            continue;
          }
#endif /* 0 */
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = APR_EOK;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE:
      { /* Command the state machine to transition back to the state it was. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &eval_state );

        rc = mvm_helper_create_new_goal_control( eval_state, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

/* BACKWARD COMPATIBILITY */
static int32_t mvm_stream_set_media_type_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_istream_cmd_set_media_type_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_istream_cmd_set_media_type_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_istream_cmd_set_media_type_t, ctrl->packet );

    /* ADSP currently does not support independent Rx and Tx media types. */
    if ( in_args->rx_media_id != in_args->tx_media_id ||
         mvm_media_id_is_valid( in_args->rx_media_id ) )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->target_set.system_config.media_id = in_args->rx_media_id;

    /* BACKWARD COMPATIBILITY */
    /* The method by which we distinguish between MVS 1.0 vs MVS 2.0+ behavior is
     * based on whether MVM receives a VSS_ISTREAM_CMD_SET_MEDIA_TYPE command
     * from MVS.
     * In MVS 1.0 behavior, the media type is set by MVS on MVM and propagated
     * by MVM to the "default modem voice" stream based on MVM's own passive control
     * handle to the stream.
     * In MVS 2.0+ behavior, the media type is set by MVS directly on stream
     * and MVM does not create a passive handle to the stream.
     */
    session_obj->is_mvs_v1_policy = TRUE;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_set_cal_network_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_imvm_cmd_set_cal_network_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_imvm_cmd_set_cal_network_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_set_cal_network_t, ctrl->packet );

    /* Validate parameters. */
    if ( mvm_network_id_is_valid( in_args->network_id ) == FALSE )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.network_id = in_args->network_id;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t mvm_common_set_network_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_icommon_cmd_set_network_t* in_args;
  uint32_t network_id;
  uint32_t rx_pp_sr;
  uint32_t tx_pp_sr;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_icommon_cmd_set_network_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_network_t, ctrl->packet );

    /* Convert the legacy network id to the new network_id.
     * (And validate the parameter in the process.)
     */
    rc = mvm_convert_legacy_network_id( in_args->network_id,
                                        &network_id,
                                        &rx_pp_sr,
                                        &tx_pp_sr );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.network_id = network_id;
    session_obj->target_set.system_config.rx_pp_sr = rx_pp_sr;
    session_obj->target_set.system_config.tx_pp_sr = tx_pp_sr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_set_cal_media_type_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_imvm_cmd_set_cal_media_type_t* in_args;
  uint32_t target_dec_sr;
  uint32_t target_enc_sr;
  uint32_t target_rx_pp_sr;
  uint32_t target_tx_pp_sr;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_imvm_cmd_set_cal_media_type_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_set_cal_media_type_t, ctrl->packet );

    /* Determine the target_set enc, dec and pp sampling rates based on the
     * media type and the client requested variable vocoder sampling rates.
     * (And validate paramters in the process.)
     */
    rc = mvm_determine_target_sampling_rates(
                in_args->media_id,
                session_obj->requested_var_voc_rx_sampling_rate,
                session_obj->requested_var_voc_tx_sampling_rate,
                &target_dec_sr,
                &target_enc_sr,
                &target_rx_pp_sr,
                &target_tx_pp_sr );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.media_id = in_args->media_id;
    session_obj->target_set.system_config.dec_sr = target_dec_sr;
    session_obj->target_set.system_config.enc_sr = target_enc_sr;
    session_obj->target_set.system_config.rx_pp_sr = target_rx_pp_sr;
    session_obj->target_set.system_config.tx_pp_sr = target_tx_pp_sr;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_set_max_var_voc_sampling_rate_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  vss_imvm_cmd_set_max_var_voc_sampling_rate_t* in_args;
  uint32_t target_dec_sr;
  uint32_t target_enc_sr;
  uint32_t target_rx_pp_sr;
  uint32_t target_tx_pp_sr;
  uint16_t client_addr;
  mvm_simple_job_object_t* job_obj;
  mvm_generic_item_t* generic_item;
  vss_icommon_cmd_set_var_voc_sampling_rate_t set_var_voc_sampling_rate;


  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_imvm_cmd_set_max_var_voc_sampling_rate_t ) );
  if ( rc ) return APR_EOK;

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_set_max_var_voc_sampling_rate_t, ctrl->packet );

    /* If no media type has been set, reject this command. */
    if ( FALSE == mvm_media_id_is_valid( session_obj->target_set.system_config.media_id ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_set_max_var_voc_sampling_rate_cmd_control(): Media type has not been set yet" );
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EFAILED );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* If media type is not variable sample rate vocoder, reject this command. */
    if ( FALSE == mvm_media_id_is_var_sr( session_obj->target_set.system_config.media_id ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_set_max_var_voc_sampling_rate_cmd_control(): Media type does not support variable sample rates" );
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EFAILED );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Determine the target_set enc, dec and pp sampling rates based on the
     * media type and the client requested variable vocoder sampling rates.
     */
    rc = mvm_determine_target_sampling_rates(
                session_obj->target_set.system_config.media_id,
                in_args->rx,
                in_args->tx,
                &target_dec_sr,
                &target_enc_sr,
                &target_rx_pp_sr,
                &target_tx_pp_sr );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Cache the client requested variable vocoder sample rates.
     * The reason for caching these here is the following scenario:
     *   1. Client sets EVS.
     *   2. Client sets var voc sample rates to 16, 32.
     *   3. System runs at 16, 32.
     *   4. Client sets AMR-NB.
     *   5. System runs at 8, 8.
     *   6. Client goes back to EVS.
     *   7. Now we run at the sample rates that the client requested earlier (16, 32)
     *      instead of just going back to the defaults (8, 8).
     */
    /* The Tx/Rx Sampling Rates determined by the policy are the session's variable vocoder
     * sampling rates.
     */

    if ( in_args->rx == 0 )
    {
      session_obj->requested_var_voc_rx_sampling_rate = MVM_DEFAULT_VAR_VOC_DEC_SAMPLING_RATE;
    }
    else
    {
      session_obj->requested_var_voc_rx_sampling_rate = in_args->rx;
    }

    if ( in_args->tx == 0 )
    {
      session_obj->requested_var_voc_tx_sampling_rate = MVM_DEFAULT_VAR_VOC_ENC_SAMPLING_RATE;
    }
    else
    {
      session_obj->requested_var_voc_tx_sampling_rate = in_args->tx;
    }

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.dec_sr = target_dec_sr;
    session_obj->target_set.system_config.enc_sr = target_enc_sr;
    session_obj->target_set.system_config.rx_pp_sr = target_rx_pp_sr;
    session_obj->target_set.system_config.tx_pp_sr = target_tx_pp_sr;

    set_var_voc_sampling_rate.rx_pp_sr = session_obj->requested_var_voc_rx_sampling_rate;
    set_var_voc_sampling_rate.tx_pp_sr = session_obj->requested_var_voc_tx_sampling_rate;

    generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
    session_obj->stream_rsp_cnt = 0;

    for ( ;; )
    {
      rc = apr_list_get_next( &session_obj->stream_q,
                              ( ( apr_list_node_t* ) generic_item ),
                              ( ( apr_list_node_t** ) &generic_item ) );
      if ( rc ) break;

      rc = mvm_create_simple_job_object( session_obj->header.handle, &job_obj );
      MVM_PANIC_ON_ERROR( rc );

      job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
        mvm_streams_group_wait_transition_result_rsp_fn;

      rc = __aprv2_cmd_alloc_send(
              mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
              session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
              mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
              job_obj->header.handle, VSS_ICOMMON_CMD_SET_VAR_VOC_SAMPLING_RATE,
              &set_var_voc_sampling_rate, sizeof( vss_icommon_cmd_set_var_voc_sampling_rate_t ) );
      MVM_COMM_ERROR( rc, mvm_cvp_addr );
    }

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_common_set_voice_timing_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_icommon_cmd_set_voice_timing_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_icommon_cmd_set_voice_timing_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_voice_timing_t, ctrl->packet );

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.vsid = 0;
    session_obj->target_set.system_config.vfr_mode = in_args->mode;
    session_obj->target_set.voice_timing.enc_offset = in_args->enc_offset;
    session_obj->target_set.voice_timing.dec_req_offset = in_args->dec_req_offset;
    session_obj->target_set.voice_timing.dec_offset = in_args->dec_offset;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_common_set_voice_timing_v2_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  vss_icommon_cmd_set_voice_timing_v2_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_icommon_cmd_set_voice_timing_v2_t ) );
  if ( rc ) return APR_EOK;

  /* This command is allowed only with full control handle. */
  if ( mvm_helper_verify_full_control( ctrl, indirect_obj ) == FALSE )
  {
    return APR_EOK;
  }

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_voice_timing_v2_t, ctrl->packet );

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.vsid = in_args->vsid;
    session_obj->target_set.system_config.vfr_mode = in_args->mode;
    session_obj->target_set.voice_timing.enc_offset = in_args->enc_offset;
    session_obj->target_set.voice_timing.dec_req_offset = in_args->dec_req_offset;
    session_obj->target_set.voice_timing.dec_offset = in_args->dec_offset;

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_tty_set_tty_mode_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  vss_itty_cmd_set_tty_mode_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_itty_cmd_set_tty_mode_t ) );
  if ( rc ) return APR_EOK;

  in_args = APRV2_PKT_GET_PAYLOAD( vss_itty_cmd_set_tty_mode_t, ctrl->packet );

  /* Save parameters in target set to be picked up by state machine. */
  session_obj->target_set.tty_mode = in_args->mode;

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
  MVM_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t mvm_widevoice_set_widevoice_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  vss_iwidevoice_cmd_set_widevoice_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, NULL, &session_obj );
  if ( rc ) return APR_EOK;

  rc = mvm_helper_validate_payload_size_control(
         ctrl, sizeof( vss_iwidevoice_cmd_set_widevoice_t ) );
  if ( rc ) return APR_EOK;

  { /* Do work. */
    client_addr = ctrl->packet->src_addr;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_iwidevoice_cmd_set_widevoice_t, ctrl->packet );

    if ( in_args->enable == 1 ||
         in_args->enable == 0 )
    {
      /* Save parameters in target set to be picked up by state machine. */
      session_obj->is_wv_set_by_ui = TRUE;
      session_obj->target_set.widevoice_enable = in_args->enable;

      /* BACKWARD COMPATIBILITY */
      /* If widevoice is enabled, when configuring the vocproc in the
       * state machine, we overrride the rx sample rate with 16 KHz.
       */
    }
    else
    {
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
    MVM_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t mvm_mvm_set_policy_dual_control_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_indirection_object_t* indirect_obj;
  mvm_session_object_t* session_obj;
  vss_imvm_cmd_set_policy_dual_control_t* in_args;
  uint16_t client_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_set_policy_dual_control_t,
                                   ctrl->packet );

  session_obj->is_dual_control = in_args->enable_flag;

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
  MVM_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t mvm_memory_map_cmd_control (
  mvm_pending_control_t* ctrl,
  mvm_mem_mapping_mode_t mapping_mode
)
{
  int32_t rc;
  uint32_t cvd_mem_handle;
  uint32_t vpm_mem_handle;
  uint64_t region_base_virt_addr;
  uint32_t region_size;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imemory_cmd_map_virtual_t* in_args_virtual = NULL;
  vss_imemory_cmd_map_physical_t* in_args_physical = NULL;
  vss_imemory_rsp_map_t map_memory_rsp;
  struct voice_map_args_t {
    voice_cmd_shared_mem_map_regions_t voice_map_memory;
    voice_shared_map_region_payload_t voice_mem_region;
  } voice_map_args;
  voice_cmd_shared_mem_unmap_regions_t voice_unmap_memory;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_CVD,
    MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VPM,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VSM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR,
    MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR_DONE,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( mapping_mode == MVM_MEM_MAPPING_MODE_ENUM_VIRTUAL )
    {
      rc = mvm_helper_validate_payload_size_control(
             ctrl, sizeof( vss_imemory_cmd_map_virtual_t ) );
      if ( rc ) return APR_EOK;

      in_args_virtual = APRV2_PKT_GET_PAYLOAD( vss_imemory_cmd_map_virtual_t,
                                               ctrl->packet );
    }
    else
    if ( mapping_mode == MVM_MEM_MAPPING_MODE_ENUM_PHYSICAL )
    {
      rc = mvm_helper_validate_payload_size_control(
             ctrl, sizeof( vss_imemory_cmd_map_physical_t ) );
      if ( rc ) return APR_EOK;

      in_args_physical = APRV2_PKT_GET_PAYLOAD( vss_imemory_cmd_map_physical_t,
                                                ctrl->packet );
    }
    else
    {
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

    if ( mapping_mode == MVM_MEM_MAPPING_MODE_ENUM_VIRTUAL )
    {
      seqjob_obj->use_case.map_unmap_memory.map_virtual_arg = in_args_virtual;
    }
    else
    {
      seqjob_obj->use_case.map_unmap_memory.map_physical_arg = in_args_physical;
    }

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_CVD;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_CVD:
      { /* Map memory with CVD. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VPM;

        if ( mapping_mode == MVM_MEM_MAPPING_MODE_ENUM_VIRTUAL )
        {
          rc = cvd_mem_mapper_map_virtual(
                 seqjob_obj->use_case.map_unmap_memory.map_virtual_arg,
                 &cvd_mem_handle );
        }
        else
        {
          rc = cvd_mem_mapper_map_physical(
                 seqjob_obj->use_case.map_unmap_memory.map_physical_arg,
                 &cvd_mem_handle );
        }

        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_memory_map_cmd_control(): Failed to map memory: mode=%d result=0x%08X",
                                                  mapping_mode, rc );
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }

        seqjob_obj->use_case.map_unmap_memory.mem_handle = cvd_mem_handle;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VPM:
      { /* Map virtual memory with VPM. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );

        subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_MAP_MEMORY ] =
          mvm_map_memory_rsp_fn;
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;

        rc = cvd_mem_mapper_get_region_base_virt_addr(
               cvd_mem_handle, &region_base_virt_addr );
        MVM_PANIC_ON_ERROR( rc );

        rc = cvd_mem_mapper_get_region_size( cvd_mem_handle, &region_size );
        MVM_PANIC_ON_ERROR( rc );

        voice_map_args.voice_map_memory.mem_pool_id = VOICE_MAP_MEMORY_SHMEM8_4K_POOL;
        voice_map_args.voice_map_memory.num_regions = 1;
        voice_map_args.voice_map_memory.property_flag = 1; /* Indicates mapping virtual memory. */
        voice_map_args.voice_mem_region.shm_addr_lsw = ( ( uint32_t ) region_base_virt_addr );
        voice_map_args.voice_mem_region.shm_addr_msw = ( ( uint32_t ) ( region_base_virt_addr >> 32 ) );
        voice_map_args.voice_mem_region.mem_size_bytes = region_size;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_vpm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_MAP_REGIONS,
               &voice_map_args, sizeof( voice_map_args ) );
        MVM_COMM_ERROR( rc, mvm_vpm_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for VPM memory mapping to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        if ( seqjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VSM;
          cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;
          rc = cvd_mem_mapper_set_vpm_mem_handle( cvd_mem_handle,
                                                  subjob_obj->context_handle );
          MVM_PANIC_ON_ERROR( rc );
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_memory_map_cmd_control(): Failed to map memory with VPM: result=0x%08X",
                                                  seqjob_obj->status );
          seqjob_obj->state = MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_MAP_MEMORY_WITH_VSM:
      { /* Map virtual memory with VSM. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );

        subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_MAP_MEMORY ] =
          mvm_map_memory_rsp_fn;
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;

        rc = cvd_mem_mapper_get_region_base_virt_addr(
               cvd_mem_handle, &region_base_virt_addr );
        MVM_PANIC_ON_ERROR( rc );

        rc = cvd_mem_mapper_get_region_size( cvd_mem_handle, &region_size );
        MVM_PANIC_ON_ERROR( rc );

        voice_map_args.voice_map_memory.mem_pool_id = VOICE_MAP_MEMORY_SHMEM8_4K_POOL;
        voice_map_args.voice_map_memory.num_regions = 1;
        voice_map_args.voice_map_memory.property_flag = 1; /* Indicates mapping virtual memory. */
        voice_map_args.voice_mem_region.shm_addr_lsw = ( ( uint32_t ) region_base_virt_addr );
        voice_map_args.voice_mem_region.shm_addr_msw = ( ( uint32_t ) ( region_base_virt_addr >> 32 ) );
        voice_map_args.voice_mem_region.mem_size_bytes = region_size;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_vsm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_MAP_REGIONS,
               &voice_map_args, sizeof( voice_map_args ) );
        MVM_COMM_ERROR( rc, mvm_vsm_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for VSM memory mapping to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        if ( seqjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;
          rc = cvd_mem_mapper_set_vsm_mem_handle( cvd_mem_handle,
                                                  subjob_obj->context_handle );
          MVM_PANIC_ON_ERROR( rc );
         ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          continue;
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_memory_map_cmd_control(): Failed to map memory with VSM: result=0x%08X",
                                                  seqjob_obj->status );
          seqjob_obj->state = MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR;
          ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR:
      { /* Clean up on error. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR_DONE;

        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;

        rc = cvd_mem_mapper_get_vpm_mem_handle( cvd_mem_handle, &vpm_mem_handle );
        if ( rc == APR_ENOTEXIST )
        { /* VPM mem handle doesn't exist. */
          subjob_obj->is_completed = TRUE;
          seqjob_obj->status = APR_EOK;
        }
        else
        {
          voice_unmap_memory.mem_map_handle = vpm_mem_handle;

          rc = __aprv2_cmd_alloc_send(
                 mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 mvm_my_addr, APR_NULL_V,
                 mvm_vpm_addr, APR_NULL_V,
                 subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
                 &voice_unmap_memory, sizeof( voice_unmap_memory ) );
          MVM_COMM_ERROR( rc, mvm_vpm_addr );
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_CLEAN_UP_ON_ERROR_DONE:
      { /* Finish clean up on error. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;

        if ( subjob_obj->status != APR_EOK )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_memory_map_cmd_control(): Clean up error=0x%08X",
                                                  subjob_obj->status );
        }

        cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;
        rc = cvd_mem_mapper_unmap( cvd_mem_handle );
        MVM_PANIC_ON_ERROR( rc );

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        if ( seqjob_obj->status != APR_EOK )
        {
          rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                        seqjob_obj->status );
          MVM_COMM_ERROR( rc, client_addr );
        }
        else
        {
          map_memory_rsp.mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;

          rc = __aprv2_cmd_alloc_send(
                 mvm_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
                 mvm_my_addr, APR_NULL_V,
                 ctrl->packet->src_addr, ctrl->packet->src_port,
                 ctrl->packet->token, VSS_IMEMORY_RSP_MAP,
                 &map_memory_rsp, sizeof( map_memory_rsp ) );
          MVM_COMM_ERROR( rc, client_addr );

          /* Track the mem_handle and the client address who maps the memory,
           * in order to unmap the memory in case of SSR for the subsystem
           * where the client resides.
           */
          rc = mvm_ssr_track_mem_handle( client_addr,
                                         map_memory_rsp.mem_handle );
          if ( rc )
          {
            MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_memory_map_cmd_control(): " \
                                                    "Unable to track mem_handle 0x%08X, client_addr 0x%04X, rc = 0x%08X",
                                                    map_memory_rsp.mem_handle,
                                                    client_addr,
                                                    rc );
          }

          ( void ) __aprv2_cmd_free( mvm_apr_handle, ctrl->packet );
        }

        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }

      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_memory_unmap_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  uint32_t cvd_mem_handle;
  uint32_t vpm_mem_handle;
  uint32_t vsm_mem_handle;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imemory_cmd_unmap_t* in_args;
  voice_cmd_shared_mem_unmap_regions_t voice_unmap_memory;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VPM,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VSM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_CVD,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_imemory_cmd_unmap_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imemory_cmd_unmap_t,
                                     ctrl->packet );

    rc = cvd_mem_mapper_validate_handle( in_args->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,"mvm_memory_unmap_cmd_control(): Invalid mem_handle 0x%08X.",
                                             in_args->mem_handle );
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
      MVM_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.map_unmap_memory.mem_handle = in_args->mem_handle;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VPM;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
  cvd_mem_handle = seqjob_obj->use_case.map_unmap_memory.mem_handle;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VPM:
      { /* Unmap virtual memory with VPM. */
        ( void ) cvd_mem_mapper_get_vpm_mem_handle( cvd_mem_handle, &vpm_mem_handle );

        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );

        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        voice_unmap_memory.mem_map_handle = vpm_mem_handle;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_vpm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
               &voice_unmap_memory, sizeof( voice_unmap_memory ) );
        MVM_COMM_ERROR( rc, mvm_vpm_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for VPM memory unmapping to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );

        if ( seqjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VSM;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_VSM:
      { /* Unmap virtual memory with VSM. */
        ( void ) cvd_mem_mapper_get_vsm_mem_handle( cvd_mem_handle, &vsm_mem_handle );

        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;
        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );

        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        voice_unmap_memory.mem_map_handle = vsm_mem_handle;

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_vsm_addr, APR_NULL_V,
               subjob_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
               &voice_unmap_memory, sizeof( voice_unmap_memory ) );
        MVM_COMM_ERROR( rc, mvm_vsm_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for VSM memory unmapping to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->status = subjob_obj->status;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );

        if ( seqjob_obj->status == APR_EOK )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_CVD;
        }
        else
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          continue;
        }
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_UNMAP_MEMORY_WITH_CVD:
      { /* Unmap memory with CVD. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;

        rc = cvd_mem_mapper_unmap( cvd_mem_handle );
        seqjob_obj->status = rc;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        if ( seqjob_obj->status == APR_EOK )
        { /* Untrack the mem_handle after memory unmap. */
          ( void ) mvm_ssr_untrack_mem_handle( cvd_mem_handle );
        }

        rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet,
                                      seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );
        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }

      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_reconfig_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  mvm_goal_enum_t next_goal;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  rc = mvm_get_typed_object( ctrl->packet->dst_port,
                             MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_reconfig_cmd_control(): Invalid handle: "
           "src_addr=0x%08X, req_handle=0x%08X",
           ctrl->packet->src_addr,
           ctrl->packet->dst_port );
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  /* If the MVM session state machine is currently not in INIT state,
   * trigger the session state machine to transition back to INIT state and
   * then back to the state it was prior to receiving this event, in order to:
   * 1. Stop the streams and vocprocs attached to this MVM session.
   * 2. Report to the CCM of the new session configurations and/or KPPS
   *    requirement.
   * 3. Re-configure the streams and vocprocs attached to this MVM session.
   * 4. Re-calculate and re-apply the timing offsets to all the streams and
   *    vocprocs attached to this MVM session, based on the new overall system
   *    configurations indicated by the CCM.
   * 5. Re-start the streams and vocprocs.
   */
  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_INIT )
    { /* Run the sequencer. */
      rc = mvm_create_sequencer_job_object(
             ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
      MVM_PANIC_ON_ERROR( rc );

      ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;
    }
    else
    { /* Nothing to do if we are in INIT state. */
      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_helper_create_new_goal_control(
               MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_RESTORE_ORIGINAL_STATE:
      { /* Command the state machine to transition back to the state it was. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &next_goal );

        rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        rc = __aprv2_cmd_end_command(
               mvm_apr_handle, ctrl->packet, seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );

        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_ssr_cleanup_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_issr_cmd_cleanup_t* in_args;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  uint16_t client_addr;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_STOP_VOICE,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_DETACH_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_DESTROY_SESSION,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_CLEANUP_CVS,
    MVM_SEQUENCER_ENUM_WAIT_4,
    MVM_SEQUENCER_ENUM_CLEANUP_CVP,
    MVM_SEQUENCER_ENUM_WAIT_5,
    MVM_SEQUENCER_ENUM_UNMAP_MEMORY,
    MVM_SEQUENCER_ENUM_WAIT_6,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
           ctrl, sizeof( vss_issr_cmd_cleanup_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_issr_cmd_cleanup_t, ctrl->packet );

    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.ssr_cleanup.domain_id = in_args->domain_id;

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_STOP_VOICE;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_STOP_VOICE:
      {
        /* Stop each of the MVM session on behalf of MVM clients who reside in
         * a subsystem that is being restarted.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        ( void ) mvm_ssr_stop_voice(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for MVM to respond for each stop voice command. */
        if ( mvm_ssr_cleanup_cmd_tracking.rsp_cnt <
             mvm_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_DETACH_STREAM;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_DETACH_STREAM:
      {
        /* For each of the MVM sessions, detach the streams currently attached
         * to it on behalf of MVM clients who reside in a subsystem that is
         * being restarted.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        ( void ) mvm_ssr_detach_stream(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for MVM to respond for each detach stream command. */
        if ( mvm_ssr_cleanup_cmd_tracking.rsp_cnt <
             mvm_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_DESTROY_SESSION;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_DESTROY_SESSION:
      {
        /* Destroy the MVM session control handles (indirection handles) on
         * behalf of MVM clients who reside in a subsystem that is being
         * restarted.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

        ( void ) mvm_ssr_destroy_session(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_3:
      { /* Wait for MVM to respond for each destroy session command. */
        if ( mvm_ssr_cleanup_cmd_tracking.rsp_cnt <
             mvm_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_CLEANUP_CVS;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_CLEANUP_CVS:
      { /* Request CVS to cleanup for SSR. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_4;

        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_cvs_addr, APR_NULL_V,
               subjob_obj->header.handle,
               VSS_ISSR_CMD_CLEANUP,
               APRV2_PKT_GET_PAYLOAD( void, ctrl->packet ),
               APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header ) );
        MVM_COMM_ERROR( rc, mvm_cvs_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_4:
      { /* Wait for CVS to finish cleanup. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_CLEANUP_CVP;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_CLEANUP_CVP:
      { /* Request CVP to cleanup for SSR. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_5;

        rc = mvm_create_simple_job_object( APR_NULL_V, &subjob_obj );
        MVM_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( mvm_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               mvm_my_addr, APR_NULL_V,
               mvm_cvp_addr, APR_NULL_V,
               subjob_obj->header.handle,
               VSS_ISSR_CMD_CLEANUP,
               APRV2_PKT_GET_PAYLOAD( void, ctrl->packet ),
               APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header ) );
        MVM_COMM_ERROR( rc, mvm_cvp_addr );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for CVP to finish cleanup. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_UNMAP_MEMORY;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_UNMAP_MEMORY:
      {
        /* Unmap memory on behalf of MVM clients who reside in a subsystem
         * that is being restarted.
         */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_6;

        ( void ) mvm_ssr_unmap_memory(
                   seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_6:
      { /* Wait for MVM to respond to each memory unmap command. */
        if ( mvm_ssr_cleanup_cmd_tracking.rsp_cnt <
             mvm_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command(
               mvm_apr_handle, ctrl->packet, seqjob_obj->status );
        MVM_COMM_ERROR( rc, client_addr );

        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_pktexg_mailbox_timing_reconfig_evt_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  mvm_goal_enum_t next_goal;
  uint16_t client_addr;

  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_TRANSITION_TO_PROPER_STATE,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  rc = mvm_get_typed_object( ctrl->packet->dst_port,
                             MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_pktexg_mailbox_timing_reconfig_evt_control(): Invalid handle: "
           "src_addr=0x%08X, req_handle=0x%08X",
           ctrl->packet->src_addr,
           ctrl->packet->dst_port );
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  /* The MVM session state machine will be transitioned to INIT state and then
   * to the state that it should be in based on the APPS and Modem states, in
   * order to program the proper voice timing parameters for mailbox vocoder
   * packet exchange to align the packet exchange timeline to mailbox client's
   * timeline.
   */
  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* Run the sequencer. */
    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_TRANSITION_TO_INIT_STATE:
      {
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;

        rc = mvm_helper_create_new_goal_control(
               MVM_GOAL_ENUM_STOP, session_obj, &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_TRANSITION_TO_PROPER_STATE;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_TRANSITION_TO_PROPER_STATE:
      { /* Command the state machine to transition to the state it should be. */
        seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

        ( void ) mvm_eval_voice_state_decision( session_obj->modem_state,
                                                session_obj->apps_state,
                                                session_obj->is_dual_control,
                                                &next_goal );

        rc = mvm_helper_create_new_goal_control( next_goal, session_obj,
                                                 &seqjob_obj->subjob_obj );

#if 1 /* HACK */
        /* TODO: HACK: Need fix the create new goal to return an error rather
         *             than completing the command packet immediately on
         *             error. The sequencer should only have one exit point
         *             for completion to prevent sequences from failing to
         *             deallocate resources on exit.
         */
        if ( rc ) return APR_EOK;
#else
        if ( rc )
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
          seqjob_obj->status = rc;
          continue;
        }
#endif /* 0 */
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the MVM state machine to finish. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          ( void ) mvm_state_control( session_obj );
          return APR_EPENDING;
        }

        seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;

        ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case MVM_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        ( void ) __aprv2_cmd_free( mvm_apr_handle, ctrl->packet );

        ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t mvm_dynamic_reconfig_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  mvm_session_object_t* session_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  vss_imvm_cmd_dynamic_reconfig_t* in_args;
  mvm_generic_item_t* generic_item;
  vss_icommon_cmd_set_dynamic_system_config_t dynamic_system_config;
  vss_ihdvoice_cmd_get_config_t get_hdvoice_config;
  vss_ihdvoice_cmd_set_config_t set_hdvoice_config;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  rc = mvm_get_typed_object( ctrl->packet->dst_port,
                             MVM_OBJECT_TYPE_ENUM_SESSION,
                             ( ( mvm_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_dynamic_reconfig_cmd_control(): Invalid handle: "
           "src_addr=0x%08X, req_handle=0x%08X",
           ctrl->packet->src_addr,
           ctrl->packet->dst_port );
    rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EHANDLE );
    MVM_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = mvm_helper_validate_payload_size_control(
            ctrl, sizeof( vss_imvm_cmd_dynamic_reconfig_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_imvm_cmd_dynamic_reconfig_t,
                                      ctrl->packet );

    if ( ( mvm_voc_op_mode_is_valid( in_args->rx_voc_op_mode ) == FALSE ) ||
          ( mvm_voc_op_mode_is_valid( in_args->tx_voc_op_mode ) == FALSE ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "mvm_dynamic_reconfig_cmd_control(): " \
             "Invalid Rx vocoder op mode 0x%08X or Tx vocoder op mode 0x%08X.",
             in_args->rx_voc_op_mode, in_args->tx_voc_op_mode );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EBADPARAM );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    session_obj->target_set.system_config.rx_voc_op_mode = in_args->rx_voc_op_mode;
    session_obj->target_set.system_config.tx_voc_op_mode = in_args->tx_voc_op_mode;

    if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_RUN )
    {
      /* Cache the vocoder operating mode if not in RUN state. The vocoder
        * operating mode will be propagated to all streams and vocprocs when
        * MVM goes to RUN.
        */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_dynamic_reconfig_cmd_control(): " \
             "Caching Tx vocoder op mode 0x%08X and Rx vocoder op mode 0x%08X.",
              in_args->tx_voc_op_mode, in_args->rx_voc_op_mode );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    if ( mvm_is_vocproc_dynamic_reconfig_needed( session_obj ) == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_dynamic_reconfig_cmd_control(): " \
           "Stream and vocproc reconfiguration is not required." );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    /* Run the sequencer. */
    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    session_obj->stream_rsp_cnt = 0;
    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG:
        {
          /* Cannot search for HD Voice configuration when Media ID is None */
          if ( session_obj->target_set.system_config.media_id != VSS_MEDIA_ID_NONE )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG;
            continue;
          }

          session_obj->vocproc_rsp_cnt = 0;
          generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

          for ( ;; )
          {
            rc = apr_list_get_next( &session_obj->vocproc_q,
                                    ( ( apr_list_node_t* ) generic_item ),
                                    ( ( apr_list_node_t** ) &generic_item ) );
            if ( rc ) break;

            {
              rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              MVM_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_HDVOICE_CONFIG ] =
                mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

              get_hdvoice_config.media_id = session_obj->target_set.system_config.media_id;
              get_hdvoice_config.network_id = session_obj->target_set.system_config.network_id;
              get_hdvoice_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;
              get_hdvoice_config.rx_voc_op_mode = session_obj->target_set.system_config.rx_voc_op_mode;
              get_hdvoice_config.tx_pp_sr = session_obj->target_set.system_config.tx_pp_sr;
              get_hdvoice_config.tx_voc_op_mode = session_obj->target_set.system_config.tx_voc_op_mode;
              get_hdvoice_config.feature_id = session_obj->target_set.system_config.feature;

              rc = __aprv2_cmd_alloc_send(
                      mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                      session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                      mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
                      subjob_obj->header.handle, VSS_IHDVOICE_CMD_GET_CONFIG,
                      &get_hdvoice_config, sizeof( get_hdvoice_config ) );
              MVM_COMM_ERROR( rc, mvm_cvp_addr );
            }
          }
        }

      case MVM_SEQUENCER_ENUM_WAIT_1:
        {
          subjob_obj = &seqjob_obj->subjob_obj->simple_job;

          if ( session_obj->vocproc_rsp_cnt < session_obj->vocproc_q.size )
          {
            return APR_EPENDING;
          }

          /** If feature has changed,  configure the Stream **/
          if ( session_obj->target_set.system_config.feature != session_obj->active_set.system_config.feature )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
            continue;
          }
        }

        case MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM:
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

            session_obj->stream_rsp_cnt = 0;
            generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
            for ( ;; )
            {
              rc = apr_list_get_next( &session_obj->stream_q,
                                      ( ( apr_list_node_t* ) generic_item ),
                                      ( ( apr_list_node_t** ) &generic_item ) );
              if ( rc ) break;
              {
                rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
                MVM_PANIC_ON_ERROR( rc );

                set_hdvoice_config.feature_id = session_obj->target_set.system_config.feature;

                subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                  mvm_streams_set_hdvoice_group_wait_transition_result_rsp_fn;

                subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_SET_HDVOICE_CONFIG ] =
                  mvm_streams_set_hdvoice_group_wait_transition_result_rsp_fn;

                /** Stream will configure its Rx PP if it's using WV or WV2 **/
                rc = __aprv2_cmd_alloc_send(
                        mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                        session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                        mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
                        subjob_obj->header.handle, VSS_IHDVOICE_CMD_SET_CONFIG,
                        &set_hdvoice_config, sizeof( set_hdvoice_config ) );
                MVM_COMM_ERROR( rc, mvm_cvs_addr );
              }
            }
          }

        case MVM_SEQUENCER_ENUM_WAIT_2:
        {
          subjob_obj = &seqjob_obj->subjob_obj->simple_job;

          if ( session_obj->stream_rsp_cnt < session_obj->stream_q.size )
          {
            return APR_EPENDING;
          }

          /** If KPPS changed, CVS should have sent MVM Reconfig **/
          if ( session_obj->is_kpps_changed_by_hdvoice )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
            continue;
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          }
        }

      case MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG:
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

          session_obj->vocproc_rsp_cnt = 0;
          generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

          for ( ;; )
          {
            rc = apr_list_get_next( &session_obj->vocproc_q,
                                    ( ( apr_list_node_t* ) generic_item ),
                                    ( ( apr_list_node_t** ) &generic_item ) );
            if ( rc ) break;

            {
              rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              MVM_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_vocproc_dynamic_reconfig_result_count_rsp_fn;

              dynamic_system_config.rx_voc_op_mode = session_obj->target_set.system_config.rx_voc_op_mode;
              dynamic_system_config.tx_voc_op_mode = session_obj->target_set.system_config.tx_voc_op_mode;
              dynamic_system_config.feature_id = session_obj->target_set.system_config.feature;
              dynamic_system_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;

              rc = __aprv2_cmd_alloc_send(
                     mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                     session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                     mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
                     subjob_obj->header.handle, VSS_ICOMMON_CMD_SET_DYNAMIC_SYSTEM_CONFIG,
                     &dynamic_system_config, sizeof( dynamic_system_config ) );
              MVM_COMM_ERROR( rc, mvm_cvp_addr );
            }
          }
        }

        case MVM_SEQUENCER_ENUM_WAIT_3:
          { /* Wait for CVS to finish setting HD Voice on VSM. */
            subjob_obj = &seqjob_obj->subjob_obj->simple_job;

            if ( session_obj->vocproc_rsp_cnt < session_obj->vocproc_q.size )
            {
              return APR_EPENDING;
            }

            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;

            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          }

        case MVM_SEQUENCER_ENUM_COMPLETE:
          {

            session_obj->active_set.system_config.rx_voc_op_mode =
              session_obj->target_set.system_config.rx_voc_op_mode;

            session_obj->active_set.system_config.tx_voc_op_mode =
              session_obj->target_set.system_config.tx_voc_op_mode;

            session_obj->active_set.system_config.feature =
              session_obj->target_set.system_config.feature;

            rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
            MVM_COMM_ERROR( rc, client_addr );

            ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
            ctrl->pendjob_obj = NULL;
          }
          return APR_EOK;

        default:
          MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
    }
    break;
  }

  return APR_EPENDING;;
}

static int32_t mvm_hdvoice_ui_cmd_control (
  mvm_pending_control_t* ctrl,
  uint32_t hdvoice_cmd
)
{
  int32_t rc;
  bool_t is_hdvoice_specific_cmd = FALSE;
  uint16_t client_addr;
  mvm_session_object_t* session_obj;
  mvm_indirection_object_t* indirect_obj;
  mvm_sequencer_job_object_t* seqjob_obj;
  mvm_simple_job_object_t* subjob_obj;
  mvm_generic_item_t* generic_item;
  vss_icommon_cmd_set_dynamic_system_config_t dynamic_system_config;
  vss_ihdvoice_cmd_get_config_t get_hdvoice_config;
  vss_ihdvoice_cmd_set_config_t set_hdvoice_config;
  enum {
    MVM_SEQUENCER_ENUM_UNINITIALIZED,
    MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG,
    MVM_SEQUENCER_ENUM_WAIT_1,
    MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM,
    MVM_SEQUENCER_ENUM_WAIT_2,
    MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG,
    MVM_SEQUENCER_ENUM_WAIT_3,
    MVM_SEQUENCER_ENUM_COMPLETE,
    MVM_SEQUENCER_ENUM_INVALID
  };

  client_addr = ctrl->packet->src_addr;

  rc = mvm_helper_open_session_control( ctrl, &indirect_obj, &session_obj );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == MVM_PENDING_CMD_STATE_ENUM_EXECUTE )
  {

    switch ( hdvoice_cmd )
    {
      case VSS_IHDVOICE_CMD_ENABLE:
        {
          session_obj->is_hdvoice_ui_enabled = MVM_HDVOICE_UI_ENABLED;
          break;
        }
      case VSS_IHDVOICE_CMD_DISABLE:
        {
          session_obj->is_hdvoice_ui_enabled = MVM_HDVOICE_UI_DISABLED;
          break;
        }
      case VSS_IHDVOICE_CMD_BEAMR_DISABLE:
        {
          session_obj->is_beamr_enabled = FALSE;
          is_hdvoice_specific_cmd = TRUE;
          break;
        }
      case VSS_IHDVOICE_CMD_WV2_DISABLE:
        {
          session_obj->is_wv2_enabled = FALSE;
          is_hdvoice_specific_cmd = TRUE;
          break;
        }
      case VSS_IHDVOICE_CMD_BEAMR_ENABLE:
        {
          session_obj->is_beamr_enabled = TRUE;
          is_hdvoice_specific_cmd = TRUE;
          break;
        }
      case VSS_IHDVOICE_CMD_WV2_ENABLE:
        {
          session_obj->is_wv2_enabled = TRUE;
          is_hdvoice_specific_cmd = TRUE;
          break;
        }
      default:
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_hdvoice_ui_cmd_control(): " \
                 "Invalid HD Voice command 0x%08X.",
                 hdvoice_cmd );
          break;
        }

    }

    if ( is_hdvoice_specific_cmd )
    {
      /* Cache the vocoder operating mode if not in RUN state. The vocoder
        * operating mode will be propagated to all streams and vocprocs when
        * MVM goes to RUN.
        */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_hdvoice_ui_cmd_control(): " \
             "BBWE-specific command 0x%08X. Caching command for next sessions.",
             hdvoice_cmd );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    if ( session_obj->session_ctrl.state != MVM_STATE_ENUM_RUN )
    {
      /* Cache the vocoder operating mode if not in RUN state. The vocoder
        * operating mode will be propagated to all streams and vocprocs when
        * MVM goes to RUN.
        */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "mvm_hdvoice_ui_cmd_control(): " \
             "Caching HD Voice UI enablement 0x%08X.",
             session_obj->is_hdvoice_ui_enabled );

      rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
      MVM_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    /* Run the sequencer. */
    rc = mvm_create_sequencer_job_object(
           ( mvm_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    MVM_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case MVM_SEQUENCER_ENUM_GET_HDVOICE_CONFIG:
        {
          /* Cannot search for HD Voice configuration when Media ID is None */
          if ( session_obj->target_set.system_config.media_id != VSS_MEDIA_ID_NONE )
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_1;
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG;
            continue;
          }

          session_obj->vocproc_rsp_cnt = 0;
          generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

          for ( ;; )
          {
            rc = apr_list_get_next( &session_obj->vocproc_q,
                                    ( ( apr_list_node_t* ) generic_item ),
                                    ( ( apr_list_node_t** ) &generic_item ) );
            if ( rc ) break;

            {
              rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              MVM_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_GET_HDVOICE_CONFIG ] =
                mvm_vocprocs_hdvoice_config_group_wait_transition_rsp_fn;

              get_hdvoice_config.media_id = session_obj->active_set.system_config.media_id;
              get_hdvoice_config.network_id = session_obj->active_set.system_config.network_id;
              get_hdvoice_config.rx_pp_sr = session_obj->active_set.system_config.rx_pp_sr;
              get_hdvoice_config.rx_voc_op_mode = session_obj->active_set.system_config.rx_voc_op_mode;
              get_hdvoice_config.tx_pp_sr = session_obj->active_set.system_config.tx_pp_sr;
              get_hdvoice_config.tx_voc_op_mode = session_obj->active_set.system_config.tx_voc_op_mode;
              get_hdvoice_config.feature_id = session_obj->active_set.system_config.feature;

              rc = __aprv2_cmd_alloc_send(
                      mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                      session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                      mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
                      subjob_obj->header.handle, VSS_IHDVOICE_CMD_GET_CONFIG,
                      &get_hdvoice_config, sizeof( get_hdvoice_config ) );
              MVM_COMM_ERROR( rc, mvm_cvp_addr );
            }
          }
        }

      case MVM_SEQUENCER_ENUM_WAIT_1:
        {
          subjob_obj = &seqjob_obj->subjob_obj->simple_job;

          if ( session_obj->vocproc_rsp_cnt < session_obj->vocproc_q.size )
          {
            return APR_EPENDING;
          }

          if ( session_obj->active_set.system_config.feature != session_obj->target_set.system_config.feature )
          {
            /** If the media ID is NB, reconfig to apply feature **/
            if ( mvm_media_id_is_nb_sr( session_obj->active_set.system_config.media_id ) )
            {
              mvm_simple_job_object_t* reconfig_job_obj;
              seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;

              rc = mvm_create_simple_job_object( session_obj->header.handle, &reconfig_job_obj );
              MVM_PANIC_ON_ERROR( rc );
              reconfig_job_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_simple_self_destruct_result_rsp_fn;

              rc = __aprv2_cmd_alloc_send(
                     mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                     session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                     session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                     reconfig_job_obj->header.handle, VSS_IMVM_CMD_RECONFIG,
                     NULL, 0 );
              MVM_COMM_ERROR( rc, session_obj->self_addr );

              ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
              continue;
            }
            else
            {
              /** If the media ID is WB, the feature can be applied dynamically **/
              seqjob_obj->state = MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM;
              ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
            }
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
            continue;
          }
        }

        case MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM:
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_2;

            session_obj->stream_rsp_cnt = 0;
            generic_item = ( ( mvm_generic_item_t* ) &session_obj->stream_q.dummy );
            for ( ;; )
            {
              rc = apr_list_get_next( &session_obj->stream_q,
                                      ( ( apr_list_node_t* ) generic_item ),
                                      ( ( apr_list_node_t** ) &generic_item ) );
              if ( rc ) break;
              {
                rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
                MVM_PANIC_ON_ERROR( rc );

                set_hdvoice_config.feature_id = session_obj->target_set.system_config.feature;

                subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                  mvm_streams_set_hdvoice_group_wait_transition_result_rsp_fn;

                subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_SET_HDVOICE_CONFIG ] =
                  mvm_streams_set_hdvoice_group_wait_transition_result_rsp_fn;

                /** Stream will configure its Rx PP if it's using WV or WV2 **/
                rc = __aprv2_cmd_alloc_send(
                        mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                        session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                        mvm_cvs_addr, ( ( uint16_t ) generic_item->handle ),
                        subjob_obj->header.handle, VSS_IHDVOICE_CMD_SET_CONFIG,
                        &set_hdvoice_config, sizeof( set_hdvoice_config ) );
                MVM_COMM_ERROR( rc, mvm_cvs_addr );
              }
            }
          }

        case MVM_SEQUENCER_ENUM_WAIT_2:
        {
          subjob_obj = &seqjob_obj->subjob_obj->simple_job;

          if ( session_obj->stream_rsp_cnt < session_obj->stream_q.size )
          {
            return APR_EPENDING;
          }

          if ( session_obj->is_kpps_changed_by_hdvoice )
          {
            /* Reconfig should be triggered by CVS if KPPS changed */
            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
            continue;
          }
          else
          {
            seqjob_obj->state = MVM_SEQUENCER_ENUM_CONFIG_HDVOICE_ON_STREAM;
            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          }
        }

      case MVM_SEQUENCER_ENUM_SET_DYNAMIC_VOCPROC_SYSTEM_CONFIG:
        {
          seqjob_obj->state = MVM_SEQUENCER_ENUM_WAIT_3;

          session_obj->vocproc_rsp_cnt = 0;
          generic_item = ( ( mvm_generic_item_t* ) &session_obj->vocproc_q.dummy );

          for ( ;; )
          {
            rc = apr_list_get_next( &session_obj->vocproc_q,
                                    ( ( apr_list_node_t* ) generic_item ),
                                    ( ( apr_list_node_t** ) &generic_item ) );
            if ( rc ) break;

            {
              rc = mvm_create_simple_job_object( session_obj->header.handle, &subjob_obj );
              MVM_PANIC_ON_ERROR( rc );
              subjob_obj->fn_table[ MVM_RESPONSE_FN_ENUM_RESULT ] =
                mvm_vocproc_dynamic_reconfig_result_count_rsp_fn;

              /** Vocoder Operating mode should not be changing here. **/
              dynamic_system_config.rx_voc_op_mode = session_obj->active_set.system_config.rx_voc_op_mode;
              dynamic_system_config.tx_voc_op_mode = session_obj->active_set.system_config.tx_voc_op_mode;
              dynamic_system_config.feature_id = session_obj->target_set.system_config.feature;
              dynamic_system_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;

              rc = __aprv2_cmd_alloc_send(
                     mvm_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                     session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                     mvm_cvp_addr, ( ( uint16_t ) generic_item->handle ),
                     subjob_obj->header.handle, VSS_ICOMMON_CMD_SET_DYNAMIC_SYSTEM_CONFIG,
                     &dynamic_system_config, sizeof( dynamic_system_config ) );
              MVM_COMM_ERROR( rc, mvm_cvp_addr );
            }
          }
        }

        case MVM_SEQUENCER_ENUM_WAIT_3:
          { /* Wait for CVS to finish setting HD Voice on VSM. */
            subjob_obj = &seqjob_obj->subjob_obj->simple_job;

            if ( session_obj->vocproc_rsp_cnt < session_obj->vocproc_q.size )
            {
              return APR_EPENDING;
            }

            session_obj->active_set.system_config.feature =
              session_obj->target_set.system_config.feature;

            seqjob_obj->state = MVM_SEQUENCER_ENUM_COMPLETE;

            ( void ) mvm_free_object( ( mvm_object_t* ) subjob_obj );
          }

        case MVM_SEQUENCER_ENUM_COMPLETE:
          {
            rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EOK );
            MVM_COMM_ERROR( rc, client_addr );

            ( void ) mvm_free_object( ( mvm_object_t* ) seqjob_obj );
            ctrl->pendjob_obj = NULL;
          }
          return APR_EOK;

        default:
          MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
    }
    break;
  }

  return APR_EPENDING;;
}

static int32_t mvm_version_get_cmd_control (
  mvm_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_alloc_send(
         mvm_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         mvm_my_addr, APR_NULL_V,
         client_addr, ctrl->packet->src_port,
         ctrl->packet->token, VSS_IVERSION_RSP_GET,
         mvm_version, sizeof( mvm_version ) );
  MVM_COMM_ERROR( rc, client_addr );

  /* Free the incoming command packet. */
  ( void ) __aprv2_cmd_free( mvm_apr_handle, ctrl->packet );

  return APR_EOK;
}


static uint32_t mvm_listen_for_event_class_cmd_ctrl (
  mvm_pending_control_t* ctrl
)
{
  uint32_t rc = APR_EOK;
  uint32_t status = APR_EOK;
  uint16_t client_addr;
  vss_inotify_cmd_listen_for_event_class_t* in_args;

  client_addr = ctrl->packet->src_addr;

  in_args = APRV2_PKT_GET_PAYLOAD( vss_inotify_cmd_listen_for_event_class_t,
                                   ctrl->packet );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "mvm_listen_for_event_class_cmd_ctrl(): client request to listen for."
         "class=(0x%8x)", in_args->class_id );

  switch( in_args->class_id )
  {
    case VSS_ICOMMON_EVENT_CLASS_VOICE_ACTIVITY_UPDATE:
      {
        /* Cache client's address and client's port.
           This is a service level support to broadcast voice activity events
           during a voice call. */

        if( TRUE == voice_activity_client.is_enabled )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_listen_for_event_class_cmd_ctrl(): Client already listening." );
          status = APR_EALREADY;
          break;
        }

        voice_activity_client.is_enabled = TRUE;
        voice_activity_client.client_addr = ctrl->packet->src_addr;
        voice_activity_client.client_port = ctrl->packet->src_port;

        status = APR_EOK;
      }
      break;

    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_listen_for_event_class_cmd_ctrl(): Unsupported "
               "Event Class: (0x%08X)", in_args->class_id );

        status = APR_EUNSUPPORTED;
     }
     break;
  }

  rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, status );
  MVM_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}


static int32_t mvm_cancel_event_class_cmd_ctrl (
  mvm_pending_control_t* ctrl
)
{
  uint32_t rc = APR_EOK;
  uint32_t status = APR_EOK;
  uint16_t client_addr;
  vss_inotify_cmd_cancel_event_class_t* in_args;
  
  client_addr = ctrl->packet->src_addr;
  
  in_args = APRV2_PKT_GET_PAYLOAD( vss_inotify_cmd_cancel_event_class_t,
                                   ctrl->packet );
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "mvm_cancel_event_class_cmd_ctrl(): client request to cancel for."
         "class=(0x%8x)", in_args->class_id );

  switch( in_args->class_id )
  {
    case VSS_ICOMMON_EVENT_CLASS_VOICE_ACTIVITY_UPDATE:
      {
        /* Clear cached client's address and client's port.
           This is a service level support to broadcast voice activity events
           during a voice call. */
         if( FALSE == voice_activity_client.is_enabled )
         {
           MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "mvm_cancel_event_class_cmd_ctrl(): Client not listening." );

           status = APR_EALREADY;
           break;
         }

        voice_activity_client.is_enabled = FALSE;
        voice_activity_client.client_addr = APR_NULL_V;
        voice_activity_client.client_port = APR_NULL_V;

        status = APR_EOK;
      }
      break;
  
    default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "mvm_cancel_event_class_cmd_ctrl(): Unsupported "
               "Event Class: (0x%08X)", in_args->class_id );
  
        status = APR_EUNSUPPORTED;
     }
     break;
  }

  rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, status );
  MVM_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}


/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/

static int32_t mvm_isr_dispatch_fn (
  aprv2_packet_t* packet,
  void* dispatch_data
)
{
  /* TODO: Queue our client and our server messages into separate lists. */

  mvm_queue_incoming_packet( MVM_THREAD_PRIORITY_ENUM_HIGH, packet );

  return APR_EOK;
}

static void mvm_response_fn_trampoline (
  uint32_t fn_index,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  mvm_object_t* object;
  uint32_t msg_type;

  msg_type = APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header );

  if ( msg_type == APRV2_PKT_MSGTYPE_EVENT_V )
  {
    rc = mvm_get_object( packet->dst_port, &object );
  }
  else
  {
    rc = mvm_get_object( packet->token, &object );
  }

  if ( rc == APR_EOK )
  {
    switch ( object->header.type )
    {
    case MVM_OBJECT_TYPE_ENUM_SIMPLE_JOB:
      object->simple_job.fn_table[ fn_index ]( packet );
      return;

    default:
      break;
    }
  }

  ( void ) __aprv2_cmd_free( mvm_apr_handle, packet );
}

/* Process high priority events, responses, and non-gating commands in task context. */
static void mvm_high_task_process_nongating_commands ( void )
{
  mvm_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &mvm_high_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    case VSS_IAVTIMER_CMD_GET_TIME:
      ( void )mvm_avtimer_get_time_cmd( packet );
      break;

    default:
      mvm_queue_incoming_packet( MVM_THREAD_PRIORITY_ENUM_MED, packet );
      break;
    }
  }
}

static void mvm_high_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void mvm_med_task_process_nongating_commands ( void )
{
  mvm_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &mvm_med_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    case APRV2_IBASIC_EVT_ACCEPTED:
      /* TODO: Events use dst_port, but trampoline use token for results. */
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_ACCEPTED, packet );
      break;

    case APRV2_IBASIC_RSP_RESULT:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_RESULT, packet );
      break;

    case VOICE_RSP_SHARED_MEM_MAP_REGIONS:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_MAP_MEMORY, packet );
      break;

    case VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_SET_SYSTEM_CONFIG, packet );
      break;

    case VSS_IVOCPROC_RSP_GET_AVSYNC_DELAYS:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_GET_AVSYNC_DELAYS, packet );
      break;

    case VSS_IPKTEXG_RSP_GET_MODE:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_GET_PKTEXG_MODE, packet );
      break;

    case VSS_IPKTEXG_RSP_MAILBOX_GET_TIME_REFERENCE:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_GET_MAILBOX_PKTEXG_TIME_REFERENCE, packet );
      break;

    case VSS_IHDVOICE_RSP_GET_CONFIG:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_GET_HDVOICE_CONFIG, packet );
      break;

    case VSS_IHDVOICE_RSP_SET_CONFIG:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_SET_HDVOICE_CONFIG, packet );
      break;

    case VSS_IVFR_RSP_OPEN:
      mvm_response_fn_trampoline( MVM_RESPONSE_FN_ENUM_OPEN_SOFT_VFR, packet );
      break;

    case VSS_IVOCPROC_EVT_RECONFIG:
    case VSS_ISTREAM_EVT_RECONFIG:
      ( void ) mvm_core_trigger_reconfig( packet );
       break;

    case VSS_ISTREAM_EVT_VOC_OPERATING_MODE_UPDATE:
      ( void ) mvm_stream_voc_operating_mode_update_evt_processing( packet );
      break;

    case VSS_ICCM_EVT_ACTIVE_SESSIONS: /* Event from CCM to an MVM session. */
      ( void ) mvm_ccm_active_sessions_evt_processing( packet );
      break;

    case VSS_IMVM_EVT_VOICE_SESSION_ACTIVE: /* Events from an MVM session to CCM. */
    case VSS_IMVM_EVT_VOICE_SESSION_INACTIVE:
      ( void ) mvm_ccm_process_evt( packet );
      break;

    case VSS_IMVM_CMD_SET_MAX_CLOCK:
    case VSS_IMVM_CMD_RESET_MAX_CLOCK:
      ( void ) mvm_ccm_process_cmd( packet );
      break;

    case VSS_ICOMMON_EVT_VOICE_ACTIVITY_UPDATE:
      ( void ) mvm_broadcast_voice_activity_event( packet );
      break;
     
    case VSS_ISSR_CMD_CLEANUP:
      mvm_queue_pending_packet( &mvm_ssr_pending_ctrl.cmd_q, packet );
      break;

    default:
      mvm_queue_pending_packet( &mvm_med_task_pending_ctrl.cmd_q, packet );
      break;
    }
  }
}

static void mvm_med_task_process_gating_commands ( void )
{
  int32_t rc;
  mvm_pending_control_t* ctrl = &mvm_med_task_pending_ctrl;
  mvm_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case MVM_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );
        }
        ctrl->state = MVM_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case MVM_PENDING_CMD_STATE_ENUM_EXECUTE:
    case MVM_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION:
          rc = mvm_core_create_session_cmd_control( ctrl, TRUE );
          break;

        case VSS_IMVM_CMD_CREATE_PASSIVE_CONTROL_SESSION:
          rc = mvm_core_create_session_cmd_control( ctrl, FALSE );
          break;

        case APRV2_IBASIC_CMD_DESTROY_SESSION:
          rc = mvm_core_destroy_session_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_MODEM_START_VOICE:
          rc = mvm_start_voice_cmd_control( ctrl, TRUE );
          break;

        case VSS_IMVM_CMD_START_VOICE:
          rc = mvm_start_voice_cmd_control( ctrl, FALSE );
          break;

        case VSS_IMVM_CMD_MODEM_STANDBY_VOICE:
          rc = mvm_standby_voice_cmd_control( ctrl, TRUE );
          break;

        case VSS_IMVM_CMD_STANDBY_VOICE:
          rc = mvm_standby_voice_cmd_control( ctrl, FALSE );
          break;

        case VSS_IMVM_CMD_PAUSE_VOICE:
          rc = mvm_pause_voice_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_MODEM_STOP_VOICE:
          rc = mvm_stop_voice_cmd_control( ctrl, TRUE );
          break;

        case VSS_IMVM_CMD_STOP_VOICE:
          rc = mvm_stop_voice_cmd_control( ctrl, FALSE );
          break;

        case VSS_IMVM_CMD_ATTACH_STREAM:
          rc = mvm_attach_stream_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_DETACH_STREAM:
          rc = mvm_detach_stream_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_ATTACH_VOCPROC:
        case VSS_ISTREAM_CMD_ATTACH_VOCPROC: /* BACKWARD COMPATIBILITY */
          rc = mvm_attach_vocproc_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_DETACH_VOCPROC:
        case VSS_ISTREAM_CMD_DETACH_VOCPROC: /* BACKWARD COMPATIBILITY */
          rc = mvm_detach_vocproc_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE:
          rc = mvm_set_cal_media_type_cmd_control( ctrl );
          break;

        case VSS_ISTREAM_CMD_SET_MEDIA_TYPE: /* BACKWARD COMPATIBILITY */
          rc = mvm_stream_set_media_type_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE:
          rc = mvm_set_max_var_voc_sampling_rate_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_SET_CAL_NETWORK:
          rc = mvm_set_cal_network_cmd_control( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_NETWORK: /* BACKWARD COMPATIBILITY */
          rc = mvm_common_set_network_cmd_control( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_VOICE_TIMING: /* BACKWARD COMPATIBILITY */
          rc = mvm_common_set_voice_timing_cmd_control( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_VOICE_TIMING_V2:
          rc = mvm_common_set_voice_timing_v2_cmd_control( ctrl );
          break;

        case VSS_ITTY_CMD_SET_TTY_MODE:
          rc = mvm_tty_set_tty_mode_cmd_control( ctrl );
          break;

        case VSS_IWIDEVOICE_CMD_SET_WIDEVOICE: /* BACKWARD COMPATIBILITY */
          rc = mvm_widevoice_set_widevoice_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_SET_POLICY_DUAL_CONTROL:
        case VSS_IMVM_CMD_SET_MODEM_VOICE_CONTROL: /* BACKWARD COMPATIBILITY */
          rc = mvm_mvm_set_policy_dual_control_cmd_control( ctrl );
          break;

        case VSS_IMEMORY_CMD_MAP_VIRTUAL:
          rc = mvm_memory_map_cmd_control( ctrl, MVM_MEM_MAPPING_MODE_ENUM_VIRTUAL );
          break;

        case VSS_IMEMORY_CMD_MAP_PHYSICAL:
          rc = mvm_memory_map_cmd_control( ctrl, MVM_MEM_MAPPING_MODE_ENUM_PHYSICAL );
          break;

        case VSS_IMEMORY_CMD_UNMAP:
          rc = mvm_memory_unmap_cmd_control( ctrl );
          break;

        case VSS_IMVM_CMD_RECONFIG:
          rc = mvm_reconfig_cmd_control( ctrl );
          break;

        case VSS_IPKTEXG_EVT_MAILBOX_TIMING_RECONFIG:
          rc = mvm_pktexg_mailbox_timing_reconfig_evt_control( ctrl );
          break;

        case VSS_IMVM_CMD_DYNAMIC_RECONFIG:
          rc = mvm_dynamic_reconfig_cmd_control( ctrl );
          break;

        case VSS_IVERSION_CMD_GET:
          rc = mvm_version_get_cmd_control( ctrl );
          break;

        case VSS_IHDVOICE_CMD_ENABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_ENABLE );
          break;

        case VSS_IHDVOICE_CMD_DISABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_DISABLE );
          break;

        case VSS_IHDVOICE_CMD_BEAMR_DISABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_BEAMR_DISABLE );
          break;

        case VSS_IHDVOICE_CMD_WV2_DISABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_WV2_DISABLE );
          break;

        case VSS_IHDVOICE_CMD_BEAMR_ENABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_BEAMR_ENABLE );
          break;

        case VSS_IHDVOICE_CMD_WV2_ENABLE:
          rc = mvm_hdvoice_ui_cmd_control ( ctrl, VSS_IHDVOICE_CMD_WV2_ENABLE );
          break;

        case VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS:
          rc = mvm_listen_for_event_class_cmd_ctrl( ctrl );
          break;

        case VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS:
          rc = mvm_cancel_event_class_cmd_ctrl( ctrl );
          break;

        default:
          { /* Handle error. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            MVM_COMM_ERROR( rc, client_addr );
            rc = APR_EOK;
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = MVM_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = MVM_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

static void mvm_low_task_process_nongating_commands ( void )
{
  mvm_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &mvm_low_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    /* Currently nothing here.
       Add case here if needs to be done in medium priority. */

    default:
      /* We do not have anything to be processed in low priority thread now.
         Report error here. */
      ( void ) __aprv2_cmd_end_command( mvm_apr_handle, packet, APR_EUNSUPPORTED );
      break;
    }
  }
}

static void mvm_low_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void mvm_thread_process_ssr_gating_commands ( void )
{
  int32_t rc;
  mvm_pending_control_t* ctrl = &mvm_ssr_pending_ctrl;
  mvm_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case MVM_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &mvm_free_cmd_q, &work_item->link );
        }
        ctrl->state = MVM_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case MVM_PENDING_CMD_STATE_ENUM_EXECUTE:
    case MVM_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_ISSR_CMD_CLEANUP:
          rc = mvm_ssr_cleanup_cmd_control( ctrl );
          break;

        default:
          { /* Handle error. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( mvm_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            MVM_COMM_ERROR( rc, client_addr );
            rc = APR_EOK;
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = MVM_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = MVM_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      MVM_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

/****************************************************************************
 * TASK ROUTINES                                                            *
 ****************************************************************************/

static int32_t mvm_run_high_task ( void )
{
  mvm_high_task_process_nongating_commands( );
  mvm_high_task_process_gating_commands( );
  /*mvm_thread_process_state_control( );*/

  return APR_EOK;
}

static int32_t mvm_high_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &mvm_high_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_READY;
  apr_event_signal( mvm_thread_event );

  do
  {
    rc = apr_event_wait( mvm_high_task_event );
   ( void ) mvm_run_high_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( mvm_high_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t mvm_run_med_task ( void )
{
  mvm_med_task_process_nongating_commands( );
  mvm_med_task_process_gating_commands( );
  mvm_thread_process_ssr_gating_commands( );

  return APR_EOK;
}

static int32_t mvm_med_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &mvm_med_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_READY;
  apr_event_signal( mvm_thread_event );

  do
  {
    rc = apr_event_wait( mvm_med_task_event );
   ( void ) mvm_run_med_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( mvm_med_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t mvm_run_low_task ( void )
{
  mvm_low_task_process_nongating_commands( );
  mvm_low_task_process_gating_commands( );

  return APR_EOK;
}

static int32_t mvm_low_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &mvm_low_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_READY;
  apr_event_signal( mvm_thread_event );

  do
  {
    rc = apr_event_wait( mvm_low_task_event );
   ( void ) mvm_run_low_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( mvm_low_task_event );
  MVM_PANIC_ON_ERROR( rc );

  mvm_task_state = MVM_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static uint32_t mvm_run ( void )
{
  mvm_run_high_task( );
  mvm_run_med_task( );
  mvm_run_low_task( );

  return APR_EOK;
}

/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static int32_t mvm_init ( void )
{
  uint32_t rc;
#ifndef WINSIM
  RCECB_HANDLE ssr_handle;
#endif /*WINSIM */
  uint32_t i;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== mvm_init()======" );

  { /* Initialize the locks. */
    ( void ) apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &mvm_int_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &mvm_med_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &mvm_low_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &mvm_ref_cnt_lock );
  }
  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &mvm_heapmgr, ( ( void* ) &mvm_heap_pool ),
                          sizeof( mvm_heap_pool ), NULL, NULL );
      /* memheap mustn't be called from interrupt context. No locking is
       * required in task context because all commands are serialized.
       */
  }
  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = mvm_object_table;
    params.total_bits = MVM_HANDLE_TOTAL_BITS_V;
    params.index_bits = MVM_HANDLE_INDEX_BITS_V;
    params.lock_fn = mvm_int_lock_fn;
    params.unlock_fn = mvm_int_unlock_fn;
    ( void ) apr_objmgr_construct( &mvm_objmgr, &params );
      /* TODO: When the ISR and task context contentions becomes a problem
       *       the objmgr could be split into ISR/task and task-only stores
       *       to reduce the bottleneck.
       */
  }
  { /* Initialize the session management. */
    ( void ) mvm_pending_control_init( &mvm_high_task_pending_ctrl );
    ( void ) mvm_pending_control_init( &mvm_med_task_pending_ctrl );
    ( void ) mvm_pending_control_init( &mvm_low_task_pending_ctrl );
  }
  { /* Initialize the SSR management. */
    ( void ) mvm_pending_control_init( &mvm_ssr_pending_ctrl );

    mvm_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
    mvm_ssr_cleanup_cmd_tracking.rsp_cnt = 0;

    ( void ) apr_list_init_v2( &mvm_ssr_mem_handle_tracking.free_q, NULL, NULL );
    for ( i = 0; i < MVM_SSR_MAX_TRACKED_MEM_HANDLES; ++i )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &mvm_ssr_mem_handle_tracking.pool[ i ] );
      ( void ) apr_list_add_tail(
                 &mvm_ssr_mem_handle_tracking.free_q,
                 ( ( apr_list_node_t* ) &mvm_ssr_mem_handle_tracking.pool[ i ] ) );
    }

    ( void ) apr_list_init_v2( &mvm_ssr_mem_handle_tracking.used_q, NULL, NULL );
  }

#ifndef WINSIM
  { /* Register for system monitor SSR callbacks. */
    ssr_handle = rcecb_register_context_name(
                   SYS_M_SSR_MODEM_AFTER_SHUTDOWN,
                   mvm_ssr_modem_after_shutdown_handler );
    if ( ssr_handle == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR , "mvm_init: Failed to register for SYS_M_SSR_MODEM_AFTER_SHUTDOWN callback" );
    }

    ssr_handle = rcecb_register_context_name(
                   SYS_M_SSR_MODEM_AFTER_POWERUP,
                   mvm_ssr_modem_after_powerup_handler );
    if ( ssr_handle == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR , "mvm_init: Failed to register for SYS_M_SSR_MODEM_AFTER_POWERUP callback" );
    }
  }
#endif /* !WINSIM */

  { /* Initialize the command queue management. */
    { /* Populate the free command structures */
      ( void ) apr_list_init_v2( &mvm_free_cmd_q,
                                 mvm_int_lock_fn, mvm_int_unlock_fn );
      for ( i = 0; i < MVM_NUM_COMMANDS_V; ++i )
      {
        ( void ) apr_list_init_node( ( apr_list_node_t* ) &mvm_cmd_pool[ i ] );
        ( void ) apr_list_add_tail( &mvm_free_cmd_q,
                                    ( ( apr_list_node_t* ) &mvm_cmd_pool[i] ) );
      }
    }
    ( void ) apr_list_init_v2( &mvm_high_task_incoming_cmd_q,
                               mvm_int_lock_fn, mvm_int_unlock_fn );
    ( void ) apr_list_init_v2( &mvm_med_task_incoming_cmd_q,
                               mvm_med_task_lock_fn, mvm_med_task_unlock_fn );
    ( void ) apr_list_init_v2( &mvm_low_task_incoming_cmd_q,
                               mvm_low_task_lock_fn, mvm_low_task_unlock_fn );
  }
  { /* Initialize the MVM session tracking list. */
    ( void ) apr_list_init_v2( &mvm_session_list_free_q, NULL, NULL );
    for ( i = 0; i < MVM_MAX_SESSIONS; ++i )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &mvm_session_list_pool[ i ] );
      ( void ) apr_list_add_tail(
                 &mvm_session_list_free_q,
                 ( ( apr_list_node_t* ) &mvm_session_list_pool[i] ) );
    }

    ( void ) apr_list_init_v2( &mvm_session_q, NULL, NULL );
  }
  { /* Create the high/meduim/low MVM tasks, sequentially. */
    rc = apr_event_create( &mvm_thread_event );
    MVM_PANIC_ON_ERROR( rc );

    {
      ( void ) apr_thread_create( &mvm_high_task_handle, MVM_HIGH_TASK_NAME,
                                  MVM_HIGH_TASK_PRIORITY, mvm_high_task_stack, MVM_HIGH_TASK_STACK_SIZE,
                                  mvm_high_task, NULL );
      ( void ) apr_event_wait( mvm_thread_event );
    }
    {
      ( void ) apr_thread_create( &mvm_med_task_handle, MVM_MED_TASK_NAME,
                                  MVM_MED_TASK_PRIORITY, mvm_med_task_stack, MVM_MED_TASK_STACK_SIZE,
                                  mvm_med_task, NULL );
      ( void ) apr_event_wait( mvm_thread_event );
    }
    {
      ( void ) apr_thread_create( &mvm_low_task_handle, MVM_LOW_TASK_NAME,
                                  MVM_LOW_TASK_PRIORITY, mvm_low_task_stack, MVM_LOW_TASK_STACK_SIZE,
                                  mvm_low_task, NULL );
      ( void ) apr_event_wait( mvm_thread_event );
    }

    rc = apr_event_destroy( mvm_thread_event );
    MVM_PANIC_ON_ERROR( rc );
  }
  { /* Initialize the CVD shared memory utility library. */
    ( void ) cvd_mem_mapper_init( );
  }
  { /* Initialize the CVD calibration search utility library. */
    ( void ) cvd_cal_init( );
  }
  { /* Initialize the CVD devcfg parser. */
    ( void ) cvd_devcfg_parser_init( );
  }
  { /* Initialize the APR resource. */
    rc = __aprv2_cmd_register2(
           &mvm_apr_handle, mvm_my_dns, sizeof( mvm_my_dns ), 0,
           mvm_isr_dispatch_fn, NULL, &mvm_my_addr );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "mvm_init(): registered with APR rc=0x%08X",
                                           rc );
  }
  { /* Initialize the MVM-CCM */
    ( void ) mvm_ccm_init( mvm_apr_handle, mvm_my_addr );
  }

  return APR_EOK;
}

static int32_t mvm_post_init ( void )
{
  { /* Perform DNS look-ups now after services have registered in init. */
    ( void ) __aprv2_cmd_local_dns_lookup(
               mvm_cvs_dns, sizeof( mvm_cvs_dns ), &mvm_cvs_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               mvm_cvp_dns, sizeof( mvm_cvp_dns ), &mvm_cvp_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               mvm_vpm_dns, sizeof( mvm_vpm_dns ), &mvm_vpm_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               mvm_vsm_dns, sizeof( mvm_vsm_dns ), &mvm_vsm_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               mvm_cvd_vfr_dns, sizeof( mvm_cvd_vfr_dns ), &mvm_cvd_vfr_addr );
      /**<
        * Note that CVD VFR is a dynamic APR service whose address can only be
        * queried by client who resides on the same APR domain as CVD VFR
        * service. CVD VFR is a private module which can only be used internal
        * within CVD.
        */
  }

  return APR_EOK;
}

static int32_t mvm_pre_deinit ( void )
{
  return APR_EOK;
}

static int32_t mvm_deinit ( void )
{
  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== mvm_deinit()======" );

  /* TODO: Ensure no race conditions on deregister. */

  { /* Deinit the MVM-CCM. */
    ( void ) mvm_ccm_deinit( );
  }
  { /* Release the APR resource. */
    ( void ) __aprv2_cmd_deregister( mvm_apr_handle );
  }
  { /* Deinit the CVD devcfg parser. */
    ( void ) cvd_devcfg_parser_deinit( );
  }
  { /* Deinit the CVD calibration search utility library. */
    ( void ) cvd_cal_deinit( );
  }
  { /* Deinit the CVD shared memory utility library. */
    ( void ) cvd_mem_mapper_deinit( );
  }
  { /* Destroy the low/medium/high MVM tasks, sequentially. */
    {
      ( void ) apr_event_signal_abortall( mvm_low_task_event );

      while ( mvm_task_state != MVM_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }
      ( void ) apr_thread_destroy( mvm_low_task_handle );
    }
    {
      ( void ) apr_event_signal_abortall( mvm_med_task_event );

      while ( mvm_task_state != MVM_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }
      ( void ) apr_thread_destroy( mvm_med_task_handle );
    }
    {
      ( void ) apr_event_signal_abortall( mvm_high_task_event );

      while ( mvm_task_state != MVM_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }
      ( void ) apr_thread_destroy( mvm_high_task_handle );
    }
  }
  { /* Release the session queue management. */
    ( void ) apr_list_destroy( &mvm_session_q );
    ( void ) apr_list_destroy( &mvm_session_list_free_q );
  }
  { /* Release the command queue management. */
    ( void ) apr_lock_destroy( mvm_ref_cnt_lock );
    ( void ) apr_list_destroy( &mvm_low_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &mvm_med_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &mvm_high_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &mvm_free_cmd_q );
  }

#ifndef WINSIM
  { /* Unregister for system monitor SSR callbacks. */
    ( void ) rcecb_unregister_context_name(
               SYS_M_SSR_MODEM_AFTER_POWERUP,
               mvm_ssr_modem_after_powerup_handler );

    ( void ) rcecb_unregister_context_name(
               SYS_M_SSR_MODEM_AFTER_SHUTDOWN,
               mvm_ssr_modem_after_shutdown_handler );
  }
#endif /* !WINSIM */

  { /* Release the SSR management. */
    ( void ) apr_list_destroy( &mvm_ssr_mem_handle_tracking.used_q );
    ( void ) apr_list_destroy( &mvm_ssr_mem_handle_tracking.free_q );

    ( void ) mvm_pending_control_destroy( &mvm_ssr_pending_ctrl );
  }
  { /* Release the session management. */
    ( void ) mvm_pending_control_destroy( &mvm_low_task_pending_ctrl );
    ( void ) mvm_pending_control_destroy( &mvm_med_task_pending_ctrl );
    ( void ) mvm_pending_control_destroy( &mvm_high_task_pending_ctrl );
  }
  { /* Release the object management. */
    ( void ) apr_objmgr_destruct( &mvm_objmgr );
  }
  { /* Release the locks. */
    ( void ) apr_lock_destroy( mvm_low_task_lock );
    ( void ) apr_lock_destroy( mvm_med_task_lock );
    ( void ) apr_lock_destroy( mvm_int_lock );
  }

  { /* Print out debug info. on object sizes. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "apr_objmgr_object_t size = %d",
                                          sizeof( apr_objmgr_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "mvm_object_t size = %d",
                                          sizeof( mvm_object_t ) );
  }

  return APR_EOK;
}

APR_EXTERNAL int32_t mvm_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  int32_t rc;
  vss_imvm_cmd_cal_query_t* cal_query_params;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = mvm_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = mvm_post_init( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = mvm_pre_deinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = mvm_deinit( );
    break;

  case DRV_CMDID_RUN:
    rc = mvm_run( );
    break;

  case MVM_CMDID_CAL_QUERY:
    {
      cal_query_params = ( ( vss_imvm_cmd_cal_query_t* ) params );
      rc = cvd_cal_query(
             cal_query_params->query_handle, cal_query_params->cb_fn,
             ( ( void* ) cal_query_params->client_data ) );
    }
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unsupported callindex (%d)",
                                            cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

