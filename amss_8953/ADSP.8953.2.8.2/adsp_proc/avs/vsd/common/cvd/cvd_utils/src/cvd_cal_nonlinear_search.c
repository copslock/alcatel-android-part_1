/*
  Copyright (C) 2012-2014 QUALCOMM Technologies, Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/cvd_utils/src/cvd_cal_nonlinear_search.c#3 $
  $Author: svutukur $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <string.h>
#include "msg.h"
#include "mmstd.h"
#include "apr_comdef.h"
#include "apr_errcodes.h"
#include "cvd_cal_private_i.h"
#include "cvd_cal_common_i.h"
#include "cvd_cal_nonlinear_search_i.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define CVD_CAL_HASH_INDEX_WIDTH ( 32 )
#define CVD_CAL_HASH_AVERAGE_ITEM_PER_INDEX ( 1 )
#define CVD_CAL_HASH_SHIFT_SIZE ( 4 )
#define CVD_CAL_HASH_F_MULTIPLIER ( 0X9E3779B9U )

/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

typedef struct cvd_cal_hash_table_descriptor_t
{
  uint32_t num_entries;
  uint32_t entry_bit_cnt;

  uint8_t* idx_start_ptr;
  uint8_t* cur_item_ptr;
}
  cvd_cal_hash_table_descriptor_t;

/* TODO: revisit this part for memory usage optimization. */
typedef struct cvd_cal_hash_table_index_t
{
  void* first_addr;
  void* last_addr;

  uint32_t num_items;
}
  cvd_cal_hash_table_index_t;

typedef struct cvd_cal_hash_table_item_t
{
  void* cal_key_addr;
  void* data_addr;
  uint32_t data_size;

  void* next_item_addr;
}
  cvd_cal_hash_table_item_t;

typedef struct cvd_cal_hash_search_t
{
  /* uint8 data type for easier pointer arithmetic without casts everytime */
  uint8_t* user_buf_addr;
  uint8_t* user_buf_end_addr;

  uint32_t num_entries;
  bool_t need_more_flag;

  uint8_t regen_key_buf[ CVD_CAL_KEY_COLUMN_SIZE_IN_BYTES ];
}
  cvd_cal_hash_search_t;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* CVD CAL Heap management. */
extern apr_memmgr_type cvd_cal_heapmgr;


/****************************************************************************
 * INTERNAL ROUTINES                                                        *
 ****************************************************************************/

/* Set up hash table configurations. */
static uint32_t cvd_cal_config_hash_table (
  uint32_t num_cal_entries,
  cvd_cal_hash_table_descriptor_t* table
)
{
  uint32_t num_hash_idx;

  if ( table == NULL )
  {
    return APR_EBADPARAM;
  }

  num_hash_idx = ( num_cal_entries / CVD_CAL_HASH_AVERAGE_ITEM_PER_INDEX );

  if ( num_hash_idx <= 1024 )
  {
    table->num_entries = 1024;
    table->entry_bit_cnt = 10;
  }
  else if ( num_hash_idx <= 4096 )
  {
    table->num_entries = 4096;
    table->entry_bit_cnt = 12;
  }
  else
  {
    table->num_entries = 16384;
    table->entry_bit_cnt = 14;
  }

  return APR_EOK;
}


/* Calculate the memory size of hash index. */
static uint32_t cvd_cal_eval_hash_index_size (
  uint32_t num_hash_entries
)
{
  return ( num_hash_entries * sizeof( cvd_cal_hash_table_index_t ) );
}


/* Calculate the totol indexing memory needed. */
APR_INTERNAL uint32_t cvd_cal_nonlinear_search_eval_index_mem_size (
  uint32_t num_cal_entries,
  uint32_t* ret_size
)
{
  int32_t rc;
  uint32_t index_size;
  uint32_t item_size;
  cvd_cal_hash_table_descriptor_t descriptor;

  for ( ;; )
  {
    if ( ret_size == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    ( void ) cvd_cal_config_hash_table( num_cal_entries, &descriptor );
    index_size = cvd_cal_eval_hash_index_size( descriptor.num_entries );
    item_size = num_cal_entries * sizeof( cvd_cal_hash_table_item_t );
    *ret_size = index_size + item_size;

    rc = APR_EOK;
    break;
  }

  return rc;
}


/* The hash indexing function for building hash table. */
static int32_t cvd_cal_hash_fn (
  cvd_cal_table_descriptor_t* table_descriptor,
  uint8_t* entry_ptr,
  uint32_t meta_size,
  cvd_cal_hash_table_descriptor_t* hash_table,
  uint32_t* ret_hash_idx
)
{
  uint32_t idx;
  uint32_t hash_value;
  uint8_t* column_ptr;
  cvd_cal_column_descriptor_t* column;
  int32_t rc = APR_EOK;

  if ( ( table_descriptor == NULL ) ||
       ( entry_ptr == NULL ) || ( hash_table == NULL ) ||
       ( ret_hash_idx == NULL ) )
  {
    return APR_EBADPARAM;
  }

  hash_value = 0;
  column_ptr = entry_ptr;
  column = table_descriptor->columns;
  for ( idx = 0; idx < table_descriptor->num_columns; ++idx )
  {
    rc = cvd_cal_validate_entry( table_descriptor, column_ptr, 0 );
    if ( rc != APR_EOK )
    {
      return rc;
    }
    hash_value += ( ( *( uint32_t * )( column_ptr ) ) <<
                    ( idx * CVD_CAL_HASH_SHIFT_SIZE ) );
    column_ptr += cvd_cal_get_type_width( column->type );
    ++column;
  }

  *ret_hash_idx = ( ( hash_value * CVD_CAL_HASH_F_MULTIPLIER ) >>
                    ( CVD_CAL_HASH_INDEX_WIDTH - hash_table->entry_bit_cnt ) );

  return APR_EOK;
}


/* Get the hashing index from a query key. */
static uint32_t cvd_cal_hash_query_key (
  cvd_cal_hash_table_descriptor_t* hash_table,
  cvd_cal_key_t* key,
  uint32_t* ret_hash_idx
)
{
  uint32_t idx;
  uint32_t hash_value;
  cvd_cal_column_t* column;

  if ( ( hash_table == NULL ) || ( key == NULL ) ||
       ( ret_hash_idx == NULL ) )
  {
    return APR_EBADPARAM;
  }

  hash_value = 0;
  column = key->columns;
  for ( idx = 0; idx < key->num_columns; ++idx )
  {
    hash_value += ( column->value << ( idx * CVD_CAL_HASH_SHIFT_SIZE ) );
    ++column;
  }

  *ret_hash_idx = ( ( hash_value * CVD_CAL_HASH_F_MULTIPLIER ) >>
                    ( CVD_CAL_HASH_INDEX_WIDTH - hash_table->entry_bit_cnt ) );

  return APR_EOK;
}


/* Get a hash item from indexing memory. */
static uint32_t cvd_cal_hash_get_item (
  cvd_cal_hash_table_descriptor_t* hash_table,
  cvd_cal_hash_table_item_t** ret_item
)
{
  if ( ( hash_table == NULL ) || ( ret_item == NULL ) )
  {
    return APR_EBADPARAM;
  }

  *ret_item = ( cvd_cal_hash_table_item_t* )hash_table->cur_item_ptr;

  /* Update the pointer to item pools. */
  hash_table->cur_item_ptr += sizeof( cvd_cal_hash_table_item_t );

  return APR_EOK;
}


/* Add a hash item to indexed linked list. */
/* TODO: revisit this part for memory usage optimization. */
static int32_t cvd_cal_hash_add_item (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_hash_table_descriptor_t* hash_table,
  uint8_t* entry_ptr,
  uint32_t hash_idx,
  cvd_cal_hash_table_item_t* item_ptr,
  uint32_t meta_size
)
{
  cvd_cal_hash_table_index_t* idx_ptr;
  cvd_cal_hash_table_item_t* last_ptr;
  int32_t rc = APR_EOK;

  if ( ( hash_table == NULL ) || ( entry_ptr == NULL ) ||
       ( item_ptr == NULL ) )
  {
    return APR_EBADPARAM;
  }

  idx_ptr = ( ( cvd_cal_hash_table_index_t* )hash_table->idx_start_ptr
               + hash_idx );
  item_ptr = ( ( cvd_cal_hash_table_item_t* )hash_table->cur_item_ptr );

  item_ptr->cal_key_addr = ( ( void* )entry_ptr );
  item_ptr->data_addr = ( void* )( entry_ptr + meta_size + sizeof( uint32_t ) );
  rc = cvd_cal_validate_entry( table_descriptor, entry_ptr, meta_size + sizeof(uint32_t) );
  if ( rc != APR_EOK )
  {
    return rc;
  }
  item_ptr->data_size = ( *( ( uint32_t* )( entry_ptr + meta_size ) ) );

  rc = cvd_cal_validate_entry( table_descriptor, entry_ptr, item_ptr->data_size + meta_size + sizeof(uint32_t) );
  if ( rc != APR_EOK )
  {
    return rc;
  }

  item_ptr->next_item_addr = NULL;
  if ( idx_ptr->first_addr == NULL )
  {
    idx_ptr->first_addr = ( void* )item_ptr;
  }
  else
  {
    /* Collision, add the item to the tail. */
    last_ptr = ( ( cvd_cal_hash_table_item_t* )idx_ptr->last_addr );
    last_ptr->next_item_addr = ( ( void* )item_ptr );
  }
  idx_ptr->last_addr = ( ( void* )item_ptr );
  ++idx_ptr->num_items;

  return APR_EOK;
}


/* Setup the hash table. */
static uint32_t cvd_cal_setup_hash_table (
  void* indexing_mem_addr,
  uint32_t indexing_mem_size,
  cvd_cal_hash_table_descriptor_t* hash_table
)
{
  uint32_t idx_size;

  if ( ( hash_table == NULL ) || ( indexing_mem_addr == NULL ) )
  {
    return APR_EBADPARAM;
  }

  idx_size = cvd_cal_eval_hash_index_size( hash_table->num_entries );

  hash_table->idx_start_ptr = (uint8_t*)indexing_mem_addr;
  hash_table->cur_item_ptr = ( (uint8_t*)indexing_mem_addr + idx_size );

  mmstd_memset( ( void* )hash_table->idx_start_ptr, 0, idx_size );

  return APR_EOK;
}


/* Parse a calibration table for non-linear search. */
APR_INTERNAL int32_t cvd_cal_nonlinear_search_parse_table (
  void* indexing_mem_ptr,
  uint32_t indexing_mem_size,
  cvd_cal_table_handle_t* table_handle
)
{
  int32_t rc = APR_EOK;
  uint8_t* cur_row;
  uint8_t* table_end;
  uint32_t row_size;
  cvd_cal_entry_t entry_item;
  uint32_t hash_idx = 0;
  uint32_t num_cal_entries;
  cvd_cal_hash_table_item_t* item_ptr;
  cvd_cal_table_descriptor_t* table_descriptor;
  cvd_cal_hash_table_descriptor_t* hash_table_descriptor;

  if ( ( table_handle == NULL ) || ( indexing_mem_ptr == NULL ) )
  {
    return APR_EBADPARAM;
  }

  cur_row = ( ( uint8_t* )table_handle->table_descriptor.start_ptr );
  table_end = ( cur_row + table_handle->table_descriptor.size );

  table_descriptor = &table_handle->table_descriptor;

  rc = cvd_cal_validate_table( table_descriptor );
  if ( rc != APR_EOK )
  {
    return rc;
  }

  rc = cvd_cal_get_num_cal_entries( table_descriptor, &num_cal_entries );
  if ( rc != APR_EOK )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_cal_nonlinear_search_eval_index_mem_size(): Cannot get number of cal entries in table" );
    return APR_EFAILED;
  }

  hash_table_descriptor = ( cvd_cal_hash_table_descriptor_t* )
                          apr_memmgr_malloc( &cvd_cal_heapmgr,
                          sizeof( cvd_cal_hash_table_descriptor_t ) );
  if ( hash_table_descriptor == NULL )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_cal_nonlinear_search_eval_index_mem_size(): no memory for descriptor, \
                                            size %d.",
                                            sizeof( cvd_cal_hash_table_descriptor_t ) );
    return APR_ENORESOURCE;
  }

  //hash_table_descriptor = ( ( cvd_cal_hash_table_descriptor_t* )table_handle->aux_info );

  /* Validate indexing mem handle and address/size */
  rc = cvd_cal_validate_indexing_mem( table_descriptor, indexing_mem_ptr, indexing_mem_size );
  if ( rc != APR_EOK )
  {
    return rc;
  }

  ( void ) cvd_cal_config_hash_table( num_cal_entries, hash_table_descriptor );
  ( void ) cvd_cal_setup_hash_table( indexing_mem_ptr, indexing_mem_size,
                                     hash_table_descriptor );
  table_handle->aux_info = ( void* )hash_table_descriptor;

  while( cur_row < table_end )
  {
    row_size = cvd_cal_get_entry_info( table_handle, cur_row, &entry_item );

    rc = cvd_cal_validate_entry( table_descriptor, cur_row, row_size );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_cal_nonlinear_search_parse_table(): Invalid table" );
      rc = APR_EFAILED;
      break;
    }

    rc = cvd_cal_hash_fn( table_descriptor, cur_row, table_handle->meta_size,
                              hash_table_descriptor, &hash_idx );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_cal_nonlinear_search_parse_table(): Invalid table" );
      rc = APR_EFAILED;
      break;
    }

    ( void ) cvd_cal_hash_get_item( hash_table_descriptor, &item_ptr );
    rc = cvd_cal_hash_add_item( table_descriptor, hash_table_descriptor, cur_row, hash_idx,
                                    item_ptr, table_handle->meta_size );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_cal_nonlinear_search_parse_table(): Invalid table" );
      rc = APR_EFAILED;
      break;
    }

    cur_row += row_size;
  }

  if ( rc == APR_EOK )
  {
    table_handle->is_parsed = TRUE;
  }

  return rc;
}


/* Key value comparison. */
static int32_t cvd_cal_hash_comp_key (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_key_t* key,
  uint8_t* data_addr,
  bool_t* is_matching
)
{
  uint32_t idx;
  uint32_t type_size;
  cvd_cal_column_descriptor_t* column_descriptor;
  cvd_cal_column_t* column;

  if ( ( table_descriptor == NULL ) || ( key == NULL ) ||
       ( ( void* )data_addr == NULL ) || ( is_matching == NULL ) )
  {
    return APR_EBADPARAM;
  }

  *is_matching = TRUE;
  column = key->columns;
  column_descriptor = table_descriptor->columns;
  for( idx = 0; idx < key->num_columns; ++idx )
  {
    type_size = cvd_cal_get_type_width( column_descriptor->type );

    if ( memcmp( &( column->value ), data_addr, type_size ) != 0 )
    {
      *is_matching = FALSE;
      break;
    }

    data_addr += type_size;
    ++column;
  }

  return APR_EOK;
}


/* Search to see if the query key is matched in table. */
static uint32_t cvd_cal_match_keys (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_hash_table_descriptor_t* hash_table,
  cvd_cal_key_t* key,
  bool_t* is_matching,
  cvd_cal_entry_t* ret_item
)
{
  uint32_t hash_idx;
  cvd_cal_hash_table_index_t* table_idx;
  void* next_item;
  cvd_cal_hash_table_item_t* item_ptr;

  if ( ( table_descriptor == NULL ) || ( hash_table == NULL ) ||
       ( key == NULL ) || ( is_matching == NULL ) || ( ret_item == NULL ) )
  {
    return APR_EBADPARAM;
  }

  /* Do hash. */
  ( void ) cvd_cal_hash_query_key( hash_table, key, &hash_idx );

  /* Get address to the first item corresponding to the hash idx. */
  table_idx = ( ( cvd_cal_hash_table_index_t* )hash_table->idx_start_ptr +
                 hash_idx );

  /* Do comparison (traverse all items). */
  next_item = table_idx->first_addr;
  while ( next_item )
  {
    item_ptr = ( cvd_cal_hash_table_item_t* )next_item;

    ( void )cvd_cal_hash_comp_key( table_descriptor, key, (uint8_t*)item_ptr->cal_key_addr, is_matching );
    if ( *is_matching == TRUE )
    {
      ret_item->start_ptr = item_ptr->data_addr;
      ret_item->size = item_ptr->data_size;
      break;
    }

    next_item = item_ptr->next_item_addr;
  }

  return APR_EOK;
}


/* Re-generate query key. Needed since out of order key,
   incomplete key, N/A value etc has to be taken care of. */
static int32_t cvd_cal_hash_regen_key (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_key_t* orig_key,
  uint32_t mask,
  cvd_cal_key_t* ret_key,
  uint8_t* column_buf
 )
{
  uint32_t idx;
  cvd_cal_column_t* key_buf;
  cvd_cal_column_t* key_column;
  cvd_cal_column_descriptor_t* table_column;

  if ( ( table_descriptor == NULL ) || ( orig_key == NULL ) ||
       ( ret_key == NULL ) || ( column_buf == NULL ) )
  {
    return APR_EBADPARAM;
  }

  key_buf = ( ( cvd_cal_column_t* )column_buf );
  ret_key->num_columns = table_descriptor->num_columns;
  ret_key->columns = key_buf;
  table_column = table_descriptor->columns;
  key_column = orig_key->columns;
  for ( idx = 0; idx < table_descriptor->num_columns; ++idx )
  {
    key_buf->id = table_column->id;
    if( ( ( mask >> idx ) & 0x1 ) )
    {
      /* Missing column. Use N/A value. */
      /* Currently supported maxcolumn width is 32 bits. */
      key_buf->value = table_column->na_value.uint32_val;
    }
    else
    { /* Use original value. */
      key_buf->value = key_column->value;
    }

    ++key_column;
    ++table_column;
    ++key_buf;
  }

  return APR_EOK;
}

/* Assumes all pointers are not NULL, dst_left is bigger
   than src_size and that all sizes are correct. */
static void cvd_cal_nonlinear_search_write_segment (
  /* in/out */ void** dst_buffer,
  /* in/out */ uint64_t* dst_left,
  /* in */ void* src_buffer,
  /* in */ uint32_t src_size
)
{
  ( void ) mmstd_memcpy( *dst_buffer, *dst_left, src_buffer, src_size );
  *( ( uint8_t** ) dst_buffer ) += src_size;
  *dst_left -= src_size;

  return;
}

/* Re-cursively search key. Needed since out of order key,
   incomplete key, N/A value etc has to be taken care of. */
static void cvd_cal_recursive_search (
  cvd_cal_table_descriptor_t* table_descriptor,
  cvd_cal_hash_table_descriptor_t* hash_table,
  int32_t level,
  cvd_cal_key_t* key,
  uint32_t mask,
  cvd_cal_hash_search_t* search_result
)
{
  bool_t is_matching;
  int32_t next_level;
  cvd_cal_column_t* columns;
  cvd_cal_column_descriptor_t* table_column;
  uint32_t type_size;
  uint32_t mask_1;
  uint32_t mask_2;
  cvd_cal_key_t new_key;
  cvd_cal_entry_t entry_item;
  const uint32_t item_size = sizeof( cvd_cal_entry_t );
  uint8_t* buffer_ptr;
  uint64_t buffer_len;

  buffer_ptr = ( ( uint8_t* ) &search_result->user_buf_addr );
  buffer_len = search_result->user_buf_end_addr - search_result->user_buf_addr;

  /* Caller cvd_cal_nonlinear_search_query() has taken of
     input parameter validation. Do not do validation here since
     it is a recursive function. */

  mmstd_memset( &new_key, 0, sizeof( cvd_cal_key_t ) );

  if ( level == 0 )
  {
    /* Bottom level, do the actual matching. */
    ( void ) cvd_cal_hash_regen_key( table_descriptor, key, mask, &new_key,
                                     search_result->regen_key_buf );

    ( void )cvd_cal_match_keys( table_descriptor, hash_table,
                             &new_key, &is_matching, &entry_item );
    if ( is_matching == TRUE )
    {
      ++search_result->num_entries;

      /* Copy matched entries to user buffer. */
      if ( ( buffer_ptr + item_size ) >
            search_result->user_buf_end_addr )
      {/* User buffer is too small, stop copying but keep searching. */
        search_result->need_more_flag = TRUE;
      }
      else
      {
        cvd_cal_nonlinear_search_write_segment( ( ( void** ) &buffer_ptr ), &buffer_len, &entry_item, item_size );
      }
    }

    return;
  }
  else
  {
    /* Intermediate level, form the query key and keep on. */
    next_level = level - 1;

    columns = ( key->columns + next_level );
    table_column = ( table_descriptor->columns + next_level );
    type_size = cvd_cal_get_type_width( table_column->type );
    if ( memcmp( &columns->value, &table_column->na_value, type_size ) != 0 )
    {
      /* Key column value is not a N/A value, need to search for N/A value as well. */
      /* Mask tells the key re-generator to pick up the orignal key column value or the N/A value.
         Bit value 0 denotes picking up only N/A value.
         Bit value 1 denotes picking up both original values.
         See cvd_cal_hash_regen_key() how mask is being use. */
      mask_1 = ( mask | ( 0x1 << next_level ) );
      cvd_cal_recursive_search( table_descriptor, hash_table,
                                next_level, key, mask_1, search_result );
      mask_2 = ( mask | ( 0x0 << next_level ) );
      cvd_cal_recursive_search( table_descriptor, hash_table,
                                next_level, key, mask_2, search_result );
    }
    else
    {
      /* Key column value is a N/A value, only need to search for N/A value. */
      mask_2 = ( mask | ( 0x0 << next_level ) );
      cvd_cal_recursive_search( table_descriptor, hash_table,
                                next_level, key, mask_2, search_result );
    }
  }
}


/* Nonlinear search on a table. */
APR_INTERNAL int32_t cvd_cal_nonlinear_search_query (
  cvd_cal_table_handle_t* table_handle,
  cvd_cal_key_t* key,
  uint32_t entries_buf_size,
  cvd_cal_entry_t* ret_entries,
  uint32_t* ret_num_entries
)
{
  int32_t rc;
  cvd_cal_table_descriptor_t* table_descriptor;
  cvd_cal_hash_table_descriptor_t* hash_table_descriptor;
  cvd_cal_key_t new_key;
  cvd_cal_hash_search_t search_result;

  if ( ( table_handle == NULL ) || ( key == NULL ) ||
       ( ret_entries == NULL ) || ( ret_num_entries == NULL ) )
  {
    return APR_EBADPARAM;
  }

  *ret_num_entries = 0;

  table_descriptor = &table_handle->table_descriptor;
  hash_table_descriptor = ( ( cvd_cal_hash_table_descriptor_t* )table_handle->aux_info );

  search_result.need_more_flag = FALSE;
  search_result.num_entries = 0;
  search_result.user_buf_addr = ( ( uint8_t* )ret_entries );
  search_result.user_buf_end_addr = ( search_result.user_buf_addr + entries_buf_size );

  ( void ) cvd_cal_reorder_key( table_descriptor, key, &new_key, table_handle->reorder_key_buf );
  cvd_cal_recursive_search( table_descriptor,
                            hash_table_descriptor,
                            table_descriptor->num_columns,
                            &new_key, 0, &search_result );

  *ret_num_entries = search_result.num_entries;

  if ( search_result.need_more_flag == TRUE )
  {
    rc = APR_ENEEDMORE;
  }
  else
  {
    rc = APR_EOK;
  }

  return rc;
}
