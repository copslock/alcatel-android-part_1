/*
  Copyright (C) 2009-2016 QUALCOMM Technologies, Incorporated.
  All Rights Reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietar

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/cvp/src/cvp_module.c#40 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "mmstd.h"
#include "msg.h"
#include "err.h"

#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"
#include "apr_log.h"
#include "aprv2_api_inline.h"
#include "aprv2_msg_if.h"

#include "adsp_vparams_api.h"
#include "adsp_vcmn_api.h"
#include "adsp_vpm_api.h"
#include "adsp_afe_service_commands.h"
#include "vss_public_if.h"

#include "vccm_api.h"
#include "vss_private_if.h"
#include "cvd_task.h"
#include "cvd_cal_common_i.h"
#include "cvd_cal_log_i.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

static char_t cvp_my_dns[] = "qcom.audio.cvp";
static uint16_t cvp_my_addr;
  /**< CVP address is set at initialization. */
static char_t cvp_cvs_dns[] = "qcom.audio.cvs";
static uint16_t cvp_cvs_addr;
  /**< CVS address is set at initialization. */
static char_t cvp_vpm_dns[] = "qcom.audio.vpm";
static uint16_t cvp_vpm_addr;
  /**< VPM address is set at initialization. */
static char_t cvp_mvm_dns[] = "qcom.audio.mvm";
static uint16_t cvp_mvm_addr;
  /**< MVM address is set at initialization. */

static bool_t cvp_initialized = FALSE;
  /**< Indicates whether CVP module has been initialized. */

/****************************************************************************
 * CVP DEFINES                                                              *
 ****************************************************************************/

#define CVP_VERSION_V ( 0x00000000 )
  /**<
   * TODO: Need to come up with a versioning scheme that will sustain for
   * generations.
   */

#define CVP_HEAP_SIZE_V ( 55 * 1024 )

#define CVP_NUM_COMMANDS_V ( 100 )

#define CVP_HANDLE_TOTAL_BITS_V ( 16 )
#define CVP_HANDLE_INDEX_BITS_V ( 6 ) /* 6 bits = 64 handles. */

#define CVP_MAX_OBJECTS_V ( 1 << CVP_HANDLE_INDEX_BITS_V )

#define CVP_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

/* This macro is to be used to inspect the APR return code whenever CVP
 * calls APR to send a packet.
 *
 * When CVP encounters an APR communication error for sending a packet to an
 * APR service who resides in a different domain than CVP, an error message
 * will be printed and the error is ignored. This is the phase one SSR
 * implementation for handling APR communication errors due to that CVP's
 * client's subsystem encounters SSR. In the future, CVP may need to use a
 * timer based retry mechanism to keep on trying to communicate with the remote
 * client until CVP knows that the remote client is no longer available.
 *
 * When CVP encounters an APR communication error for sending a packet to an
 * APR service who resides in the same domain as CVP, CVP_PANIC_ON_ERROR will
 * be called.
 */
#define CVP_COMM_ERROR( rc, dst_addr ) \
  { if ( ( rc ) && \
         ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, dst_addr ) != \
           APR_GET_FIELD( APRV2_PKT_DOMAIN_ID, cvp_my_addr ) ) ) \
    { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, "CVP APR comm error 0x%08X, \
                                    dst_addr = 0x%04X", rc, dst_addr ); } \
    else \
    { CVP_PANIC_ON_ERROR( rc ); } \
  }

#define CVP_MIN_VOLUME_MB_SUPPORTED ( -10000 )
  /**< Minimum volume supported is -10000 mB. Based on system team library limition. */

#define CVP_MAX_VOLUME_MB_SUPPORTED ( 2000 )
  /**< Maximum volume supported is 2000 mB. Based on system team library limition. */

#define CVP_VOLUME_INTERPOLATION_STEPS ( 100 )
  /**< Number of interval steps between successive mB. */

#define CVP_MAX_VP3_DATA_LEN ( 1024 )
  /**< Maximum length (in bytes) of VP3 data suppported. */

#define CVP_MAX_UI_PROP_DATA_LEN ( 12 )
  /**<
   * Maximum length (in bytes) of a UI property.
   *
   * UI prop data can hold only enable/disable params
   * If UI properties intend to hold more params,
   * this size need to be increased
   */

#define CVP_MAX_UI_PROP ( 20 )
  /**< Maximum number of UI properties that can be cached. */

#define CVP_MUTE_ENABLE ( 1 )
#define CVP_MUTE_DISABLE ( 0 )

#define CVP_VP3_MOD_ID ( 0x00010EF6 )
  /**< Module ID for VP3 data. This is the same irrespective of topology. */
#define CVP_VP3_PARAM_ID ( 0x00010E03 )
  /**< Param ID for VP3 data. This is the same irrespective of topology. */
#define CVP_VP3_SIZE_PARAM_ID ( 0x00010E1A )

#define CVP_DEFAULT_RX_PP_SR ( 8000 ) /* Default post-proc sample rate. */
#define CVP_DEFAULT_TX_PP_SR ( 8000 ) /* Default pre-proc sample rate. */

#define CVP_DEFAULT_DEC_SR ( 8000 ) /* Default dec sample rate. */
#define CVP_DEFAULT_ENC_SR ( 8000 ) /* Default enc sample rate. */

#define CVP_COMMON_NUM_CAL_COLUMNS ( 3 ) /* BACKWARD COMPATIBILITY */
  /**< Number of common cal columns is 3: network ID, Tx PP sampling rate,
       Rx PP sampling rate. */

#define CVP_VOLUME_NUM_CAL_COLUMNS ( 4 ) /* BACKWARD COMPATIBILITY */
  /**< Number of volume cal columns is 4: network ID, Tx PP sampling rate,
       Rx PP sampling rate, and Rx volume index. */

#define CVP_MAX_NUM_MATCHING_COMMON_CAL_ENTRIES ( 1 << CVP_COMMON_NUM_CAL_COLUMNS ) /* BACKWARD COMPATIBILITY */
  /**< Max number of matching common calibration entries. */

#define CVP_MAX_NUM_MATCHING_VOLUME_CAL_ENTRIES ( 1 << CVP_VOLUME_NUM_CAL_COLUMNS ) /* BACKWARD COMPATIBILITY */
  /**< Max number of matching volume calibration entries. */

#define CVP_NUM_STATIC_CAL_COLUMNS ( 7 )
  /**< Number of static cal columns is 7: network ID, Tx PP sampling rate, Rx
       PP sampling rate, Tx vocoder operating mode, Rx vocoder operating mode,
       media ID, and feature. If more columns are added, this value has to be
       increased. */

#define CVP_NUM_DYNAMIC_CAL_COLUMNS ( 8 )
  /**< Number of dynamic cal columns is 8: network ID, Tx PP sampling rate, Rx
       PP sampling rate, Rx volume index, Tx vocoder operating mode, Rx vocoder
       operating mode, media ID, and feature. If more columns are added, this
       value has to be increased. */

#define CVP_NUM_WV_FENS_CAL_FROM_COMMON_CAL_DATA ( 10 )
  /**< Number of WV/FENS/Rx gain cal data that is fetched from common cal 
       Follows the module name and param's supported.  
         MODULE NAME        ->       PARAM
	   VOICE_MODULE_WV      -> VOICE_PARAM_MOD_ENABLE
	   VOICE_MODULE_WV      -> VOICE_PARAM_WV
	   VOICE_MODULE_FNS     -> VOICE_PARAM_MOD_ENABLE
	   VOICE_MODULE_FNS     -> VOICE_PARAM_FNS
	   VOICE_MODULE_FNS     -> VOICE_PARAM_FNS_V2
	   VOICE_MODULE_FNS_V2  -> VOICE_PARAM_MOD_ENABLE
	   VOICE_MODULE_FNS_V2  -> VOICE_PARAM_FNS_V3
	   VOICE_MODULE_WV_V2   -> VOICE_PARAM_MOD_ENABLE
	   VOICE_MODULE_WV_V2   -> VOICE_PARAM_WV_V2
	   VOICE_MODULE_RX_GAIN -> VOICE_PARAM_GAIN */

#define CVP_SOUND_DEVICE_DIRECTION_RX ( VSS_IVOCPROC_DIRECTION_RX )
#define CVP_SOUND_DEVICE_DIRECTION_TX ( VSS_IVOCPROC_DIRECTION_TX )
#define CVP_SOUND_DEVICE_DIRECTION_RX_TX ( VSS_IVOCPROC_DIRECTION_RX_TX )
#define CVP_SOUND_DEVICE_DIRECTION_INVALID ( 0xFFFF )

#define CVP_MAX_NUM_DEVICE_PAIRS_OF_VP3_STORED ( 5 )
  /**< Maximum number of device pairs in which CVP will store the VP3 data
       for. */

#define CVP_MAX_NUM_SESSIONS ( 16 )

#define CVP_SESSION_INVALID ( 0xFFFFFFFF )

#define CVP_MAX_NUM_TOPO_PARAMS ( 1 )
  /**< Maximum number of topology parameters supported in OOB registration. */
#define CVP_MAX_MPPS_SCALE_FACTOR ( 50 )
  /**< Maximum scale factor for MPPS voting. */
#define CVP_MIN_MPPS_SCALE_FACTOR ( 10 )
  /**< Min scale factor for MPPS voting. */
#define CVP_MAX_BW_SCALE_FACTOR ( 50 )
  /**< Maximum scale factor for BW voting. */
#define CVP_MIN_BW_SCALE_FACTOR ( 5 )
  /**< Min scale factor for BW voting. */
/*****************************************************************************
 * DEFINITIONS                                                               *
 ****************************************************************************/

typedef enum cvp_thread_state_enum_t
{
  CVP_THREAD_STATE_ENUM_INIT,
  CVP_THREAD_STATE_ENUM_READY,
  CVP_THREAD_STATE_ENUM_EXIT
}
  cvp_thread_state_enum_t;

typedef enum cvp_thread_priority_enum_t
{
  CVP_THREAD_PRIORITY_ENUM_HIGH,
  CVP_THREAD_PRIORITY_ENUM_MED,
  CVP_THREAD_PRIORITY_ENUM_LOW
}
  cvp_thread_priority_enum_t;

typedef enum cvp_topology_commit_state_enum_t
{
  CVP_TOPOLOGY_COMMIT_STATE_NONE,
  CVP_TOPOLOGY_COMMIT_STATE_CREATE,
  CVP_TOPOLOGY_COMMIT_STATE_SET_DEVICE
}
  cvp_topology_commit_state_enum_t;

/****************************************************************************
 * CVP WORK QUEUE DEFINITIONS                                               *
 ****************************************************************************/

typedef struct cvp_work_item_t cvp_work_item_t;

struct cvp_work_item_t
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
};

/****************************************************************************
 * COMMAND RESPONSE FUNCTION TABLE                                          *
 ****************************************************************************/

typedef void ( *cvp_event_handler_fn_t ) ( aprv2_packet_t* packet );

typedef enum cvp_response_fn_enum_t
{
  CVP_RESPONSE_FN_ENUM_ACCEPTED,
  CVP_RESPONSE_FN_ENUM_RESULT,
  CVP_RESPONSE_FN_ENUM_GET_PARAM,
  CVP_RESPONSE_FN_ENUM_MAP_MEMORY,
  CVP_RESPONSE_FN_ENUM_GET_KPPS,
  CVP_RESPONSE_FN_ENUM_GET_DELAYS,
  CVP_RESPONSE_FN_ENUM_INVALID,
  CVP_RESPONSE_FN_ENUM_MAX = CVP_RESPONSE_FN_ENUM_INVALID
}
  cvp_response_fn_enum_t;

/**
 * Pending commands may load different sets of response and event handlers to
 * complete each job. The response function table is equivalent to the state
 * design pattern. The state context is stored in the pending command control.
 * Pending commands can be as simple or as complex as required.
 */
typedef cvp_event_handler_fn_t cvp_response_fn_table_t[ CVP_RESPONSE_FN_ENUM_MAX ];

/****************************************************************************
 * SESSION CONTROL DEFINITIONS                                              *
 ****************************************************************************/

typedef enum cvp_state_enum_t
{
  CVP_STATE_ENUM_UNINITIALIZED,
    /**< Reserved. */
  CVP_STATE_ENUM_RESET_ENTRY,
    /**< Move into or out of reset. */
  CVP_STATE_ENUM_RESET,
    /**< The session resource is not acquired. */
  CVP_STATE_ENUM_IDLE_ENTRY,
    /**< Move into or out of idle. */
  CVP_STATE_ENUM_IDLE,
    /**< The session resource is acquired but not running. */
  CVP_STATE_ENUM_RUN_ENTRY,
    /**< Move into or out of run. */
  CVP_STATE_ENUM_RUN,
    /**< The session resource is running. */
  CVP_STATE_ENUM_ERROR_ENTRY,
    /**< Performing error recovery. */
  CVP_STATE_ENUM_ERROR,
    /**< The session resource is unusable and should be destroyed. */
  CVP_STATE_ENUM_INVALID
    /**< Reserved. */
}
  cvp_state_enum_t;

typedef enum cvp_goal_enum_t
{
  CVP_GOAL_ENUM_UNINITIALIZED,
  CVP_GOAL_ENUM_NONE,
  CVP_GOAL_ENUM_CREATE,
  CVP_GOAL_ENUM_ENABLE,
  CVP_GOAL_ENUM_SET_MUTE,
  CVP_GOAL_ENUM_DISABLE,
  CVP_GOAL_ENUM_DESTROY,
  CVP_GOAL_ENUM_REINIT,
  CVP_GOAL_ENUM_INVALID,
}
  cvp_goal_enum_t;

typedef enum cvp_action_enum_t
{
  CVP_ACTION_ENUM_UNINITIALIZED,
    /* Common actions. */
  CVP_ACTION_ENUM_NONE,
    /**< The first action has not started for a goal from any state. */
  CVP_ACTION_ENUM_COMPLETE,
    /**<
     * Reached the last action for a goal from a state. A multi-action goal
     * that starts from and ends in the same state may require a COMPLETE
     * action to properly differentiate a terminate signal.
     */
  CVP_ACTION_ENUM_CONTINUE,
    /**<
     * For multi-state goals, the last action from each state should set to
     * CONINTUE. This indicates to the next state that a goal is continuing
     * its operation from a previous state. Usually the previous state is
     * known given the current state and the continued goal. New actions can
     * be created to help discriminate the direction from where goals come
     * from as required.
     */
  CVP_ACTION_ENUM_SET_CLOCKS,
  CVP_ACTION_ENUM_REVERT_CLOCKS,
  CVP_ACTION_ENUM_CREATE_VOCPROC,
  CVP_ACTION_ENUM_REINIT_VOCPROC,
  CVP_ACTION_ENUM_CALIBRATE_COMMON,
  CVP_ACTION_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL,
  CVP_ACTION_ENUM_CALIBRATE_VOLUME,
  CVP_ACTION_ENUM_CLEAR_CACHED_STREAM_CAL,
  CVP_ACTION_ENUM_CALIBRATE_STATIC,
  CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_STATIC_CAL,
  CVP_ACTION_ENUM_CALIBRATE_DYNAMIC,
  CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_DYNAMIC_CAL,
  CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES,
  CVP_ACTION_ENUM_SET_TX_MUTE,
  CVP_ACTION_ENUM_SET_RX_MUTE,
  CVP_ACTION_ENUM_SET_VP3,
  CVP_ACTION_ENUM_SET_UI_PROPERTIES,
  CVP_ACTION_ENUM_SET_SOUNDFOCUS_SECTORS,
  CVP_ACTION_ENUM_ENABLE_VOCPROC,
  CVP_ACTION_ENUM_BROADCAST_READY,
  CVP_ACTION_ENUM_BROADCAST_NOT_READY,
  CVP_ACTION_ENUM_DISABLE_VOCPROC,
  CVP_ACTION_ENUM_GET_VP3,
  CVP_ACTION_ENUM_BROADCAST_GOING_AWAY,
  CVP_ACTION_ENUM_DESTROY_VOCPROC,
  CVP_ACTION_ENUM_MAP_SHARED_MEMORY,
  CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT,
  CVP_ACTION_ENUM_UNMAP_SHARED_MEMORY,
  CVP_ACTION_ENUM_SET_VOICE_TIMING,
  CVP_ACTION_ENUM_TX_DTMF_DETECT,
  CVP_ACTION_ENUM_INVALID
}
  cvp_action_enum_t;

typedef enum cvp_enable_state_enum_t
{
    CVP_DISABLED = 0,
    CVP_ENABLED = 1
}
  cvp_enable_state_enum_t;

typedef struct cvp_control_t cvp_control_t;
struct cvp_control_t
{
  uint32_t goal_status;
  bool_t goal_completed;

  uint32_t transition_job_handle;

  cvp_state_enum_t state;
  cvp_goal_enum_t goal;
  cvp_action_enum_t action;
  uint32_t status;
};

/****************************************************************************
 * CVP VP3 TRACKING                                                         *
 ****************************************************************************/

typedef struct cvp_sound_device_pair_t
{
  uint16_t direction;
    /**<
     * The direction of the sound device pair. The supported values:\n
     * CVP_SOUND_DEVICE_DIRECTION_RX \n
     * CVP_SOUND_DEVICE_DIRECTION_TX \n
     * CVP_SOUND_DEVICE_DIRECTION_RX_TX
     */
  uint32_t tx_device_id;
    /**<
     * Logical Tx sound device ID. This field is ignored if the sound device
     * direction is CVP_SOUND_DEVICE_DIRECTION_RX.
     */
  uint32_t rx_device_id;
    /**<
     * Logical Rx sound device ID. This field is ignored if the sound device
     * direction is CVP_SOUND_DEVICE_DIRECTION_TX.
     */
}
  cvp_sound_device_pair_t;

typedef struct cvp_per_device_pair_vp3_t
{
  cvp_sound_device_pair_t sound_device_pair;
    /**< The sound device pair in which the VP3 data is associated with. */
  uint32_t data_len;
    /**< VP3 data length. */
  uint8_t data [ CVP_MAX_VP3_DATA_LEN ];
    /**< VP3 data. */
}
  cvp_per_device_pair_vp3_t;

typedef struct cvp_global_vp3_list_item_t
{
  apr_list_node_t link;
  cvp_per_device_pair_vp3_t* vp3;
}
  cvp_global_vp3_list_item_t;

/****************************************************************************
 * CVP OBJECT DEFINITIONS                                                   *
 ****************************************************************************/

typedef struct cvp_object_header_t cvp_object_header_t;
typedef struct cvp_session_object_t cvp_session_object_t;
typedef struct cvp_simple_job_object_t cvp_simple_job_object_t;
typedef struct cvp_track_job_object_t cvp_track_job_object_t;
typedef struct cvp_sequencer_job_object_t cvp_sequencer_job_object_t;
typedef union cvp_object_t cvp_object_t;

/****************************************************************************
 * PENDING COMMAND CONTROL DEFINITIONS                                      *
 ****************************************************************************/

typedef enum cvp_pending_cmd_state_enum_t
{
  CVP_PENDING_CMD_STATE_ENUM_FETCH,
    /**< Fetch the next pending command to execute. */
  CVP_PENDING_CMD_STATE_ENUM_EXECUTE,
    /**< Execute the current pending command for the first time. */
  CVP_PENDING_CMD_STATE_ENUM_CONTINUE
    /**< Continue executing the current pending command. */
}
  cvp_pending_cmd_state_enum_t;

/**
 * The pending command control structure stores information used to process
 * pending commands serially.
 *
 * Pending commands have state and are executed one at a time until
 * completion. A gated pending command gates all the pending commands that
 * follows it. A gated pending command should employ abort timers to allow the
 * system to make progress.
 *
 * Aborting critical pending commands can leave the system in a bad state. It
 * is recommended that (at the very least) the configurations of the aborted
 * critical pending commands be saved so that on error recovery the proper
 * configurations could be restored.
 */
typedef struct cvp_pending_control_t
{
  apr_list_t cmd_q;
    /**< The pending (cvp_work_item_t) command queue. */
  cvp_pending_cmd_state_enum_t state;
    /**<
     * The current state of the pending command control.
     *
     * This variable is managed by the pending command processor. The
     * individual pending command controls indicates to the pending command
     * processor to complete or to delay the completion of the current
     * pending command.
     */
  aprv2_packet_t* packet;
    /**<
     * The current (command) packet being processed.
     */
  cvp_session_object_t* session_obj;
    /**<
     * Session whose command is currently pending.
     */

  cvp_object_t* pendjob_obj;
    /**<
     * The pendjob_obj is a temporary storage for the current pending
     * command.
     */
}
  cvp_pending_control_t;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE COMMON OBJECT DEFINITIONS                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef enum cvp_object_type_enum_t
{
  CVP_OBJECT_TYPE_ENUM_UNINITIALIZED,
  CVP_OBJECT_TYPE_ENUM_SESSION,
  CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
  CVP_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
  CVP_OBJECT_TYPE_ENUM_INVALID
}
  cvp_object_type_enum_t;

struct cvp_object_header_t
{
  uint32_t handle;
    /**< The handle to the associated apr_objmgr_object_t instance. */
  cvp_object_type_enum_t type;
    /**<
     * The object type defines the actual derived object.
     *
     * The derived object can be any custom object type. A session or a
     * command are two such custom object types. A free object entry is set
     * to CVP_OBJECT_TYPE_ENUM_FREE.
     */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SESSION OBJECT                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct cvp_vp3_cache_t
{
  uint32_t data_len;
  uint8_t data [ CVP_MAX_VP3_DATA_LEN ];
}
  cvp_vp3_cache_t;

typedef struct cvp_ui_prop_cache_t
{
  uint32_t data_len;
  uint32_t num_ui_prop;
  uint8_t data [ ( ( CVP_MAX_UI_PROP_DATA_LEN +
                     sizeof( vss_icommon_param_data_t ) ) *
                   CVP_MAX_UI_PROP ) ];
    /**<
     * The size of the data is
     * ( UI prop data length + header ) * max number UI props.
     */
}
  cvp_ui_prop_cache_t;

typedef struct cvp_vol_param_data_t cvp_vol_param_data_t;

#include "apr_pack_begin.h"

struct cvp_vol_param_data_t
{
  uint32_t volume;
  uint16_t ramp_duration_ms;
  uint16_t reserved;
}
#include "apr_pack_end.h"
;

typedef struct cvp_vol_param_t cvp_vol_param_t;

#include "apr_pack_begin.h"

struct cvp_vol_param_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
  cvp_vol_param_data_t param_data;
}
#include "apr_pack_end.h"
;

typedef struct cvp_vol_set_param_t cvp_vol_set_param_t;

#include "apr_pack_begin.h"

struct cvp_vol_set_param_t
{
  uint32_t payload_address_lsw;
  uint32_t payload_address_msw;
  uint32_t payload_size;
  uint32_t mem_map_handle;
  cvp_vol_param_t vol_param;
}
#include "apr_pack_end.h"
;

typedef struct cvp_device_config_entry_t cvp_device_config_entry_t;

#include "apr_pack_begin.h"

struct cvp_device_config_entry_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
  uint8_t* param_data;
}
#include "apr_pack_end.h"
;

typedef struct cvp_topology_param_entry_t cvp_topology_param_entry_t;

#include "apr_pack_begin.h"

struct cvp_topology_param_entry_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
#if 0
  uint8_t param_data[param_size];
#endif
}
#include "apr_pack_end.h"
;

typedef struct cvp_device_config_wv_sr_t cvp_device_config_wv_sr_t;

#include "apr_pack_begin.h"

struct cvp_device_config_wv_sr_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
  uint32_t rx_sr;
}
#include "apr_pack_end.h"
;

typedef struct cvp_vol_level_info_t cvp_vol_level_info_t;

#include "apr_pack_begin.h"

struct cvp_vol_level_info_t
{
  bool_t is_vol_level_found;
  uint32_t vol_level;
}
#include "apr_pack_end.h"
;

typedef struct cvp_soundfocus_param_data_t cvp_soundfocus_param_data_t;

#include "apr_pack_begin.h"

struct cvp_soundfocus_param_data_t
{
  uint16_t start_angles[ 8 ];
  uint8_t enables[ 8 ];
  uint16_t gain_step;
  uint16_t reserved;
}
#include "apr_pack_end.h"
;

typedef struct cvp_soundfocus_param_t cvp_soundfocus_param_t;

#include "apr_pack_begin.h"

struct cvp_soundfocus_param_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
  cvp_soundfocus_param_data_t param_data;
}
#include "apr_pack_end.h"
;

typedef struct cvp_soundfocus_set_param_t cvp_soundfocus_set_param_t;

#include "apr_pack_begin.h"

struct cvp_soundfocus_set_param_t
{
  uint32_t payload_address_lsw;
  uint32_t payload_address_msw;
  uint32_t payload_size;
  uint32_t mem_map_handle;
  cvp_soundfocus_param_t param;
}
#include "apr_pack_end.h"
;

typedef struct cvp_sourcetrack_data_t cvp_sourcetrack_data_t;

#include "apr_pack_begin.h"

struct cvp_sourcetrack_data_t
{
  uint32_t module_id;
  uint32_t param_id;
  uint16_t param_size;
  uint16_t reserved;
  vss_isourcetrack_activity_data_t activity_data;
}
#include "apr_pack_end.h"
;

/* CVP heap shared with VPM. This heap stores the cached data:
 * VP3 data, UI property, and source track activity.
 */
typedef struct cvp_shared_heap_t
{
  uint32_t vpm_mem_handle;
  cvp_vp3_cache_t vp3_cache;
  cvp_ui_prop_cache_t ui_prop_cache;
  cvp_sourcetrack_data_t sourcetrack_activity;
}
  cvp_shared_heap_t;

typedef struct cvp_active_settings_t
{
  uint16_t tx_port_id;
  uint16_t rx_port_id;

  uint32_t tx_topology_id;
  uint32_t rx_topology_id;

  uint32_t vol_step;
  uint32_t client_num_vol_steps;

  vss_icommon_cmd_set_system_config_t system_config;

  vss_ivocproc_cmd_set_voice_timing_t voice_timing;

  bool_t mvm_tx_mute;
  bool_t mvm_rx_mute;
  bool_t client_tx_mute;
  bool_t client_rx_mute;
  uint16_t mute_ramp_duration;

  uint32_t vocproc_mode;
  uint16_t ec_ref_port_id;
}
  cvp_active_settings_t;

typedef struct cvp_target_settings_t
{
  uint16_t tx_port_id;
  uint16_t rx_port_id;

  uint32_t tx_topology_id;
  uint32_t rx_topology_id;

  uint32_t vol_step;
  uint32_t client_num_vol_steps;

  vss_icommon_cmd_set_system_config_t system_config;

  vss_ivocproc_cmd_set_voice_timing_t voice_timing;

  vss_isoundfocus_cmd_set_sectors_t soundfocus_sectors;

  bool_t mvm_tx_mute;
  bool_t mvm_rx_mute;
  bool_t client_tx_mute;
  bool_t client_rx_mute;
  uint16_t mute_ramp_duration;

  uint32_t vocproc_mode;
  uint16_t ec_ref_port_id;
}
  cvp_target_settings_t;

typedef struct cvp_attached_stream_item_t
{
  apr_list_node_t link;

  uint16_t addr;
  uint16_t port;
}
  cvp_attached_stream_item_t;

typedef struct cvp_set_tx_dtmf_detect_settings_t
{
  uint16_t client_addr;
  uint16_t client_port;
  cvp_enable_state_enum_t  enable_flag;
}
  cvp_set_tx_dtmf_detect_settings_t;

typedef struct cvp_vpcm_info_t
{
  bool_t is_enabled;
    /* Flag to indicate whether vpcm is enabled or not. */
  uint16_t client_addr;
    /* Address of VPCM client. */
  uint16_t client_handle;
    /* Handle for VPCM client. */
  uint32_t mem_handle;
    /* VPCM Client's memory handle. */
}
  cvp_vpcm_info_t;

typedef struct cvp_common_cal_info_t /* BACKWARD COMPATIBILITY. */
{
  bool_t is_registered;
    /* Indicates whether calibration table is registered. */
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the vocproc common calibration data need to be
      * applied. Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of common calibration data being registered with the
      *     vocproc.
      *
      *   - upon the vocproc has been re-initialized, and there is common calibration
      *     data currently being registered with the vocproc.
      *
      *   - upon the vocproc receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
      *     command and any of the following parameters is different between
      *     the vocproc active_set and target_set: network_id, tx_pp_sr,
      *     rx_pp_sr, and there is common calibration data currently
      *     registered.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon common calibration being deregistered.
      */
  uint32_t required_index_mem_size;
    /* Indexing memory size. */
  cvd_cal_table_handle_t* table_handle;
    /* Handle to parsed calibration table information. */
  uint32_t vpm_mem_handle;
    /* Handle to VPM shared memory. */
  cvd_cal_entry_t matching_entries[ ( CVP_MAX_NUM_MATCHING_COMMON_CAL_ENTRIES *
                                      sizeof( cvd_cal_entry_t ) ) ];
    /* Memory for storing matched entries. */
  uint32_t num_matching_entries;
    /* Number of matched entries. */
  uint32_t num_set_param_to_stream;
    /* Number of set param commands sent to CVS. */
  uint32_t set_param_rsp_cnt;
    /* Number of set param command responses. */
  uint32_t set_param_failed_rsp_cnt;
    /* Number of failed set param command responses. */
}
  cvp_common_cal_info_t;

typedef struct cvp_vol_cal_info_t /* BACKWARD COMPATIBILITY. */
{
  bool_t is_registered;
    /* Indicates whether calibration table is registered. */
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the vocproc volume calibration data need to be
      * applied. Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of volume calibration data being registered with the
      *     vocproc.
      *
      *   - upon the vocproc has been re-initialized, and there is volume calibration
      *     data currently being registered with the vocproc.
      *
      *   - upon the vocproc receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
      *     command and any of the following parameters is different between
      *     the vocproc active_set and target_set: network_id, tx_pp_sr,
      *     rx_pp_sr, and there is volume calibration data currently
      *     registered.
      *
      *   - upon the vocproc receiving a VSS_IVOCPROC_CMD_SET_RX_VOLUME_INDEX
      *     or VSS_IVOLUME_CMD_SET_STEP when the vocproc is in IDLE state such
      *     that it results in the vocproc to have a different volume level than
      *     what is currently configured on the vocproc.
      *
      *   - upon the vocproc receiving a VSS_IVOLUME_CMD_SET_NUMBER_OF_STEPS
      *     such that it results in the vocproc to have a different number of
      *     volume steps than what is currently configured on the vocproc.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon volume calibration being deregistered.
      */
  uint32_t required_index_mem_size;
    /* Indexing memory size. */
  cvd_cal_table_handle_t* table_handle;
    /* Handle to parsed calibration table information. */
  uint32_t vpm_mem_handle;
    /* Handle to VPM shared memory. */
  cvd_cal_entry_t matching_entries[ ( CVP_MAX_NUM_MATCHING_VOLUME_CAL_ENTRIES *
                                      sizeof( cvd_cal_entry_t ) ) ];
    /* Memory for storing matched entries. */
  uint32_t num_matching_entries;
    /* Number of matched entries. */
  uint32_t set_param_rsp_cnt;
    /* Number of set param command responses. */
  uint32_t set_param_failed_rsp_cnt;
    /* Number of failed set param command responses. */
  uint32_t min_vol_index;
    /* Min volume index in table. */
  uint32_t max_vol_index;
    /* Max volume index in table. */
  uint32_t num_vol_indices;
    /* Number of volume indices. */
  bool_t is_v1_format;
    /* Volume table format flag. */
}
  cvp_vol_cal_info_t;

typedef struct cvp_static_cal_info_t
{
  bool_t is_registered;
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the vocproc static calibration data need to be
      * applied. Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of static calibration data being registered with the
      *     vocproc.
      *
      *   - upon the vocproc has been re-initialized, and there is static
      *     calibration data currently being registered with the vocproc.
      *
      *   - upon the vocproc receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
      *     command and any of the following system configurations is different
      *     between the vocproc active_set and target_set: network_id, tx_pp_sr,
      *     rx_pp_sr, tx_voc_op_mode, rx_voc_op_mode, media_id, feature, and
      *     there is static calibration data currently registered.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon static calibration being deregistered.
      */
  uint32_t table_handle;
  uint32_t query_handle;
  uint8_t matching_entries[ CVD_CAL_GET_REQUIRED_MATCHING_ENTRIES_BYTE_SIZE( CVP_NUM_STATIC_CAL_COLUMNS ) ];
    /**<
      * Book-keeping buffer to be provided to CVD CAL utility for maintaining
      * the location (address or offset) of the matching calibration entries in
      * the calibration table; so that multiple calls of cvd_cal_query with the
      * the same query_handle can simply return the calibration values based on
      * the cached matching entries rather than performing a fresh search on
      * every cvd_cal_query call.
      */
  cvd_cal_column_t query_key_columns[ CVP_NUM_STATIC_CAL_COLUMNS ];
}
  cvp_static_cal_info_t;

typedef struct cvp_dynamic_cal_info_t
{
  bool_t is_registered;
  bool_t is_calibrate_needed;
    /**<
      * Indicates whether the vocproc dynamic calibration data need to be
      * applied. Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon a new set of dynamic calibration data being registered with
      *     the vocproc.
      *
      *   - upon the vocproc has been re-initialized, and there is dynamic
      *     calibration data currently being registered with the vocproc.
      *
      *   - upon the vocproc receiving a VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
      *     command and any of the following parameters is different between
      *     the vocproc active_set and target_set: network_id, tx_pp_sr,
      *     rx_pp_sr, rx_vol_index, tx_voc_op_mode, rx_voc_op_mode, media_id,
      *     feature, and there is volume calibration data currently registered.
      *
      *   - upon the vocproc receiving a VSS_IVOCPROC_CMD_SET_RX_VOLUME_INDEX
      *     or VSS_IVOLUME_CMD_SET_STEP when the vocproc is in IDLE state such
      *     that it results in the vocproc to have a different volume level than
      *     what is currently configured on the vocproc.
      *
      *   - upon the vocproc receiving a VSS_IVOLUME_CMD_SET_NUMBER_OF_STEPS
      *     such that it results in the vocproc to have a different number of
      *     volume steps than what is currently configured on the vocproc.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      *
      *   - upon dynamic calibration being deregistered.
      */

  uint32_t min_vol_index;
    /* Min volume index in table. */
  uint32_t max_vol_index;
    /* Max volume index in table. */
  uint32_t num_vol_indices;
    /* Number of volume indices. */
  bool_t is_v1_vol_format;
    /* Volume calibration format flag. */

  uint32_t table_handle;
  uint32_t query_handle;
  uint8_t matching_entries[ CVD_CAL_GET_REQUIRED_MATCHING_ENTRIES_BYTE_SIZE( CVP_NUM_DYNAMIC_CAL_COLUMNS ) ];
    /**<
      * Book-keeping buffer to be provided to CVD CAL utility for maintaining
      * the location (address or offset) of the matching calibration entries in
      * the calibration table; so that multiple calls of cvd_cal_query with the
      * the same query_handle can simply return the calibration values based on
      * the cached matching entries rather than performing a fresh search on
      * every cvd_cal_query call.
      */
  cvd_cal_column_t query_key_columns[ CVP_NUM_DYNAMIC_CAL_COLUMNS ];

  uint32_t num_set_param_issued;
  uint32_t set_param_rsp_cnt;
  uint32_t set_param_failed_rsp_cnt;
}
  cvp_dynamic_cal_info_t;

/* Book keeping structure used for the temporary hack for setting the WV and
 * FENS parameters on the attached streams.
 *
 * The WV/FENS calibration data is in the vocproc common table, however,
 * the WV/FENS modules are in the stream. For RTC to work, the set_param
 * for WV/FENS on the vocproc must be forwarded to all the attached
 * streams.
 *
 * This hack is required until WV/FENS is instantiated per
 * stream<->vocproc connection, and VPM can calibrate the WV/FENS.
 */
typedef struct cvp_set_param_info_t
{
  uint32_t num_set_params;
    /* Number of set_params to issue. */
  uint32_t rsp_cnt;
    /* Number of set_param command responses. */
  int32_t status;
    /* Status of the set_param. */
}
  cvp_set_param_info_t;

/* Book keeping structure to store the WV and FENS calibration found from the
 * static and dynamic calibration data table. This is used for the temporary
 * hack for setting the WV and FENS parameters on the attached streams.
 */
typedef struct cvp_stream_cal_info_t
{
  bool_t is_wv_cal_param_found;
  cvd_cal_param_t wv_cal_param;

  bool_t is_wv_v2_cal_param_found;
  cvd_cal_param_t wv_v2_cal_param;

  bool_t is_fens_enable_param_found;
  cvd_cal_param_t fens_enable_param;

  bool_t is_fens_cal_param_found;
  cvd_cal_param_t fens_cal_param;

  bool_t is_fens_cal_param_v2_found;
  cvd_cal_param_t fens_cal_param_v2;

  bool_t is_fens_v2_enable_param_found;
  cvd_cal_param_t fens_v2_enable_param;

  bool_t is_fens_v2_cal_param_v3_found;
  cvd_cal_param_t fens_v2_cal_param_v3;

  bool_t is_rx_gain_cal_param_found;
  cvd_cal_param_t rx_gain_cal_param;

  uint32_t num_set_param_issued;
  uint32_t set_param_rsp_cnt;
  uint32_t set_param_failed_rsp_cnt;
}
  cvp_stream_cal_info_t;

typedef struct cvp_kpps_info_t
{
  uint32_t vp_rx;
  uint32_t vp_tx;
}
  cvp_kpps_info_t;

typedef struct cvp_hdvoice_config_hdr_t
{
  uint32_t minor_version;
  uint32_t size;
  uint32_t num_columns;
  void* columns;
  uint32_t num_sys_config_entries;
  void* sys_config_list_head;
}
  cvp_hdvoice_config_hdr_t;

typedef struct cvp_hdvoice_config_info_t
{
  cvp_hdvoice_config_hdr_t hdvoice_config_hdr;
  /**< HD Voice config information */
}
  cvp_hdvoice_config_info_t;

typedef struct cvp_hdvoice_config_index_t
{
  uint32_t *index_columns;
  vss_param_hdvoice_config_data_t hdvoice_config_data;
}
  cvp_hdvoice_config_t;

typedef struct cvp_sound_device_info_t
{
  bool_t is_available;
    /**<
      * Flag that indicates if the sound device pair information is available.
      * This flag is set to TRUE upon receiving the
      * VSS_IVOCPROC_CMD_REGISTER_DEVICE_CONFIG command and the sound device
      * information is available in the device configuration data.
      */
  cvp_sound_device_pair_t device_pair;
    /**< The sound device pair. */
}
  cvp_sound_device_info_t;

typedef struct cvp_clk_control_config_t
{
  uint32_t minor_version;
  uint32_t size;
  uint32_t num_columns;
  void* columns;
}
  cvp_clk_control_config_t;

typedef struct cvp_topo_param_info_t
{
  bool_t is_registered;
    /* Topology OOB param registered status flag. */
  void* vir_addr;
    /**< Topology OOB param data virtual address. */
  uint32_t mem_size;
    /**< Topology OOB param size. */
  vss_param_num_dev_channels_t num_dev_chans;
    /**< Vocproc inband topology param - number of device channels. */
  vpm_create_param_t vpm_param[ CVP_MAX_NUM_TOPO_PARAMS ];

  uint32_t vpm_param_size;
}
  cvp_topo_param_info_t;

struct cvp_session_object_t
{
  cvp_object_header_t header;

  uint16_t self_addr;
  uint16_t full_ctrl_client_addr;
  uint16_t full_ctrl_client_port;
  uint16_t vocproc_addr;
  uint16_t vocproc_handle;
  uint16_t attached_mvm_handle;
    /**<
      * The MVM session handle that this vocproc is attached to. A handle with
      * a value of APR_NULL_V indicates that this vocproc is not attached to an
      * MVM session.
      */

  bool_t is_vocproc_config_changed;
    /**<
      * Indicates whether the vocproc configurations have changed since the
      * last time when VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      * Specifically, this flag is set to TRUE under the following
      * circumstances:
      *
      *   - upon the vocproc receiving a VSS_IVOCPROC_CMD_SET_SAMPLING_RATE
      *     command such that it results in the vocproc to have different
      *     sample rates from those currently configured on the vocproc.
      *
      *   - upon the vocproc receiving a VSS_IVOCPROC_CMD_SET_DEVICE or
      *     VSS_IVOCPROC_CMD_SET_DEVICE_V2 command such that it results in
      *     the vocporc being re-initialized.
      *
      *   - upon the vocproc receiving a
      *     VSS_IVOCPROC_CMD_DEREGISTER_DEVICE_CONFIG such that it results in
      *     the vocproc to have a different rx sampling rate from what is
      *     currently configured on the vocproc.
      *
      *   - upon a new set of calibration data (either volume or common) being
      *     registered with the vocproc.
      *
      *   - upon the vocproc receiving a VSS_IVOCPROC_CMD_SET_RX_VOLUME_INDEX
      *     or VSS_IVOLUME_CMD_SET_STEP when the vocproc is in IDLE state such
      *     that it results in the vocproc to have a different volume level than
      *     what is currently configured on the vocproc.
      *
      *   - upon the vocproc receiving a VSS_IVOLUME_CMD_SET_NUMBER_OF_STEPS
      *     such that it results in the vocproc to have a different number of
      *     volume steps than what is currently configured on the vocproc.
      *
      * The first four events have the effect of potentially changing the
      * current KPPS requirements of the vocproc. And the vocproc must be
      * reconfigured.
      *
      * The last two events do not change the current KPPS requirement. Note
      * that changing the volume level or the number of volume steps will not
      * cause additional modules to be enabled/disabled (assuming the volume
      * calibration data has been applied for a different volume level).
      * This is because by design, each volume level in the volume calibration
      * table uses the same set of module enable/disable parameters, and volume
      * level change only changes the gain-dependent parameter data values. But
      * the vocproc must be reconfigured in-order to apply the new volume
      * settings.
      *
      * Upon the vocproc receiving a VSS_IVOCPROC_CMD_ENABLE command, if
      * is_vocproc_config_changed is TRUE, and the vocproc is attached to an MVM
      * session, the vocproc will send a VSS_IVOCPROC_EVT_RECONFIG event to MVM
      * in order to notify MVM to reconfigure this vocproc.
      *
      * This flag is set to FALSE under the following circumstances:
      *
      *   - upon session creation.
      *
      *   - upon VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG has been processed.
      */

  uint32_t direction;

  bool_t public_enable;
  bool_t modem_enable;

  cvp_shared_heap_t shared_heap;
    /**< Cached VP3, UI property, and source track data that to be shared with VPM. */

  cvp_control_t session_ctrl;

  cvp_active_settings_t active_set;
  cvp_target_settings_t target_set;

  apr_list_t attached_stream_list;
    /**< Handles of streams attached to this CVP session. */

  cvp_set_tx_dtmf_detect_settings_t set_tx_dtmf_detect;

  cvp_vpcm_info_t vpcm_info;
  //bool_t is_vol_table_format_v1;

  bool_t is_sr_set_by_client;
    /**< TRUE: Sampling rate is set by client. */

  bool_t is_device_config_registered;
    /**< TRUE: Device config data is registered. */

  uint32_t widevoice_rx_sr;
    /**< Widevoice Rx sampling rate. */

  cvp_common_cal_info_t common_cal; /* BACKWARD COMPATIBILITY */
    /**< Common calibration table information. */

  cvp_vol_cal_info_t volume_cal; /* BACKWARD COMPATIBILITY */
    /**< Volume calibration table information. */

  cvp_static_cal_info_t static_cal;
    /**< Static calibration table information. */

  cvp_dynamic_cal_info_t dynamic_cal;
    /**< Dynamic calibration table information. */

  cvp_stream_cal_info_t stream_cal_info;
    /**<
     * Book keeping structure to store the Stream (WV, FENS, Rx Gain) calibration
     * found from the static and dynamic calibration data table. This is used
     * for the temporary hack for setting the WV and FENS parameters on the attached
     * streams.
     */

  bool_t is_client_set_num_vol_steps;
    /**< TRUE: client has set number of volume steps. */

  cvp_set_param_info_t set_param_info;
    /**<
      * Book keeping structure used for the temporary hack for setting the WV
      * and FENS parameters on the attached streams.
      *
      * The WV/FENS calibration data is in the vocproc common table, however,
      * the WV/FENS modules are in the stream. For RTC to work, the set_param
      * for WV/FENS on the vocproc must be forwarded to all the attached
      * streams.
      *
      * This hack is required until WV/FENS is instantiated per
      * stream<->vocproc connection, and VPM can calibrate the WV/FENS.
      */

  bool_t is_valid_afe_port_provided;
    /**<
      * Flag to indicate if valid AFE port IDs has been provided. This is used
      * for a temporary hack to solve the following problem. and the long-term
      * proper solution is required to be done on the VCP side.
      *
      * Problem: In the current VCP architecture, issuing VPM_CREATE without Rx
      * or Tx AFE port results in the VPM not being created. As a result, any
      * attach of vocproc to stream before providing AFE ports, although it
      * reports success, is actually a no-op. This means following valid
      * sequence does not work:
      * 1. Create stream.
      * 2. Create vocproc with TOPOLOGY_ID_NONE and PORT_ID_NONE for both Tx
      *    and Rx.
      * 3. Attach vocproc to stream.
      * 4. SET_DEVICE on vocproc (with new topologies and port IDs).
      * 5. Run.
      * Although all the steps above will report success, there is no data
      * transfer, because step 3 is actually a no-op.
      *
      * Temoprary hack in CVP:
      *
      * 1. Set the AFE ports to AFE pseudo-ports upon vocproc creation if the
      *    Tx or Rx AFE ports provided by the client is
      *    VSS_IVOCPROC_PORT_ID_NONE.
      * 2. Keep a flag indicating whether the client has provided valid Tx or
      *    Rx AFE ports or not.
      * 3. If client attempts to RUN the vocproc without having provided valid
      *    AFE port, we will fail the RUN. This is to avoid running with some
      *    pseudo-port that perhaps Audio path is actually doing something with
      *    and picking up that data on the voice path.
      */

  bool_t is_client_set_vp3_data;
    /**< Flag to indicate if the client has set the VP3 data via the
         VSS_IVP3_CMD_SET_DATA command and the VP3 data has not been
         applied. */

  cvp_sound_device_info_t sound_device_info;

  cvp_kpps_info_t kpps_info;

  bool_t is_kpps_changed;

  cvp_hdvoice_config_info_t hdvoice_config_info;
  /**< HD Voice config information */

  uint32_t call_num;

  cvd_virt_addr_t sourcetrack_mem_addr;
    /**< Address of client mapped memory for getting
         source track sound activity. */

  bool_t is_client_set_sectors;
    /**< TRUE: client has set sound focus sectors. */

  apr_lock_t lock;
    /**< Per session object lock. To guard multi-thread
       access of session object. */

  uint32_t ref_cnt;
    /**< Reference counter. To safely free session object
       and associated resources. */

  cvp_topo_param_info_t topo_param;
    /**< Topology param information. */

  cvp_topology_commit_state_enum_t topo_commit_state;
    /**< Topology parameters commit state. */

  cvp_clk_control_config_t clk_ctrl_table;
    /**< Clock control table containing per use case MPPS and bus BW scaling factors. */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SIMPLE JOB OBJECT                                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

struct cvp_simple_job_object_t
{
  cvp_object_header_t header;
  uint32_t context_handle;
    /**<
     * The context handle provides additional information or storage for the
     * simple job to complete.
     *
     * The context can be anything; a session, a parent-job, etc. The
     * context is especially useful to applications that override the
     * default simple job response function table to perform additional
     * processing such as state transitions.
     *
     * Set this value to -1 when it is unused or when there is no parent.
     */
  cvp_response_fn_table_t fn_table;
    /**<
     * This is the response function v-table. The response table can store
     * custom response routines for all possible responses directed to this
     * specific job.
     */
  bool_t is_accepted;
    /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
    /**< The command completed response flag. 0 is false and 1 is true. */
  int32_t status;
    /**< The status returned by the command completion. */
};

struct cvp_track_job_object_t
{
  cvp_object_header_t header;
  uint32_t context_handle;
    /**<
     * The context handle provides additional information or storage for the
     * simple job to complete.
     *
     * The context can be anything; a session, a parent-job, etc. The
     * context is especially useful to applications that override the
     * default simple job response function table to perform additional
     * processing such as state transitions.
     *
     * Set this value to -1 when it is unused or when there is no parent.
     */
  cvp_response_fn_table_t fn_table;
    /**<
     * This is the response function v-table. The response table can store
     * custom response routines for all possible responses directed to this
     * specific job.
     */
  bool_t is_accepted;
    /**< The command accepted response flag. 0 is false and 1 is true. */
  bool_t is_completed;
    /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
    /**< The status returned by the command completion. */

    /**< Optional params for forwarding. */
  uint16_t orig_src_service;
    /**< The original destination service id.. */
  uint16_t orig_src_port;
    /**< The original destination port id. */
  uint16_t orig_dst_port;
   /**< The original source port. */
  uint32_t orig_opcode;
    /**< The original opcode used before this comamnd was forwarded. */
  uint32_t orig_token;
    /**< The original token used before this comamnd was forwarded. */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE SEQUENCER JOB OBJECT                                                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct cvp_sequencer_ssr_cleanup_struct_t
{
  uint8_t domain_id;
}
  cvp_sequencer_ssr_cleanup_struct_t;

typedef union cvp_sequencer_use_case_union_t
{
  cvp_sequencer_ssr_cleanup_struct_t ssr_cleanup;
}
  cvp_sequencer_use_case_union_t;

struct cvp_sequencer_job_object_t
{
  cvp_object_header_t header;

  uint32_t state;
    /**< The generic state variable. */
  union cvp_object_t* subjob_obj;
    /**< The current sub-job object. */
  uint32_t status;
    /**< A status value. */

  cvp_sequencer_use_case_union_t use_case;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * THE GENERIC CVP OBJECT                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

union cvp_object_t
{
  cvp_object_header_t header;
  cvp_session_object_t session;
  cvp_simple_job_object_t simple_job;
  cvp_sequencer_job_object_t sequencer_job;
};

/* Overall CVP structure. */
typedef struct cvp_info_t
{
  uint16_t session_cnt;
  cvp_session_object_t* sessions[ CVP_MAX_NUM_SESSIONS ];
    /**< Maintain a list of all sessions. */
}
  cvp_info_t;

/****************************************************************************
 * SUBSYSTEM RESTART (SSR) RELATED TRACKING                                 *
 ****************************************************************************/

typedef struct cvp_ssr_cleanup_cmd_tracking_t
{
  uint32_t num_cmd_issued;
    /**<
     * Number of the commands issued by CVP to itself in each of the cleanup
     * sequencer state, when doing cleanup due to the subsystem where CVP
     * clients reside is being restarted.
     */
  uint32_t rsp_cnt;
    /**<
     * Response counter for the commands issued by CVP to itself in each of the
     * cleanup sequencer state, when doing cleanup due to the subsystem where
     * CVP clients reside is being restarted.
     */
}
  cvp_ssr_cleanup_cmd_tracking_t;

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Lock Management */
static apr_lock_t cvp_int_lock;
static apr_lock_t cvp_low_task_lock;
static apr_lock_t cvp_med_task_lock;
static apr_lock_t cvp_ref_cnt_lock;

/* Heap Management */
static uint8_t cvp_heap_pool[ CVP_HEAP_SIZE_V ];
static apr_memmgr_type cvp_heapmgr;

/* Object Management */
static apr_objmgr_object_t cvp_object_table[ CVP_MAX_OBJECTS_V ];
static apr_objmgr_t cvp_objmgr;

/* Command Queue Management */
static cvp_work_item_t cvp_cmd_pool[ CVP_NUM_COMMANDS_V ];
static apr_list_t cvp_free_cmd_q;
static apr_list_t cvp_high_task_incoming_cmd_q;
static apr_list_t cvp_med_task_incoming_cmd_q;
static apr_list_t cvp_low_task_incoming_cmd_q;


/* Task Management */
static apr_event_t cvp_thread_event;

static apr_event_t cvp_high_task_event;
static apr_thread_t cvp_high_task_handle;
static cvp_thread_state_enum_t cvp_high_task_state = CVP_THREAD_STATE_ENUM_INIT;
static uint8_t cvp_high_task_stack[ CVP_HIGH_TASK_STACK_SIZE ];

static apr_event_t cvp_med_task_event;
static apr_thread_t cvp_med_task_handle;
static cvp_thread_state_enum_t cvp_med_task_state = CVP_THREAD_STATE_ENUM_INIT;
static uint8_t cvp_med_task_stack[ CVP_MED_TASK_STACK_SIZE ];

static apr_event_t cvp_low_task_event;
static apr_thread_t cvp_low_task_handle;
static cvp_thread_state_enum_t cvp_low_task_state = CVP_THREAD_STATE_ENUM_INIT;
static uint8_t cvp_low_task_stack[ CVP_LOW_TASK_STACK_SIZE ];

/* Session Management */
static cvp_pending_control_t cvp_high_task_pending_ctrl;
static cvp_pending_control_t cvp_med_task_pending_ctrl;
static cvp_pending_control_t cvp_low_task_pending_ctrl;


/* Subsystem restart (SSR) management. */
static cvp_pending_control_t cvp_ssr_pending_ctrl;
static cvp_ssr_cleanup_cmd_tracking_t cvp_ssr_cleanup_cmd_tracking;

/* Global VP3 tracking */
static cvp_per_device_pair_vp3_t* cvp_global_vp3_heap;
static cvp_global_vp3_list_item_t cvp_global_vp3_list_item_pool[ CVP_MAX_NUM_DEVICE_PAIRS_OF_VP3_STORED ];
static apr_list_t cvp_global_vp3_list_free_q;
static apr_list_t cvp_global_vp3_list_used_q;

/* APR Resource */
static uint32_t cvp_apr_handle;
static cvp_info_t cvp_info;

/* Table used for volume interpolation. */
static const uint32_t cvp_vol_interpolation_table[] = {
        2684,       3012,       3379,       3792,        4254,
        4774,       5356,       6010,       6743,        7566,
        8489,       9524,      10687,      11991,       13454,
       15095,      16937,      19004,      21323,       23924,
       26844,      30119,      33794,      37918,       42544,
       47735,      53560,      60095,      67428,       75655,
       84887,      95244,     106866,     119906,      134536,
      150952,     169371,     190038,     213226,      239243,
      268435,     301190,     337940,     379175,      425442,
      477353,     535599,     600952,     674279,      756554,
      848867,     952445,    1068661,    1199057,     1345364,
     1509524,    1693713,    1900377,    2132259,     2392434,
     2684355,    3011895,    3379402,    3791752,     4254415,
     4773532,    5355991,    6009521,    6742794,     7565539,
     8488674,    9524449,   10686608,   11990571,    13453642,
    15095235,   16937132,   19003775,   21322586,    23924335,
    26843546,   30118954,   33794022,   37917516,    42544153,
    47735324,   53559915,   60095213,   67427938,    75655391,
    84886745,   95244494,  106866080,  119905714,   134536424,
   150952350,  169371322,  190037749,  213225862,   239243352,
   268435456,  301189535,  337940217,  379175160,   425441527,
   477353244,  535599149,  600952130,  674279380,   756553907,
   848867446,  952444939, 1068660799, 1199057137,  1345364236,
  1509523501, 1693713225, 1900377495, 2132258619, 2392433520u,
  2684354560u
};

/****************************************************************************
 * FORWARD PROTOTYPES                                                       *
 ****************************************************************************/

static int32_t cvp_free_object ( cvp_object_t* object );

static int32_t cvp_extract_topology_params  (
  cvp_session_object_t* session_obj,
  bool_t error_check_only_flag
);

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void cvp_int_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvp_int_lock );
}

static void cvp_int_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvp_int_lock );
}

static void cvp_med_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvp_med_task_lock );
}

static void cvp_med_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvp_med_task_lock );
}

static void cvp_low_task_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvp_low_task_lock );
}

static void cvp_low_task_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvp_low_task_lock );
}


static void cvp_signal_run (
  cvp_thread_priority_enum_t priority
)
{
  switch( priority )
  {
  case CVP_THREAD_PRIORITY_ENUM_HIGH:
    apr_event_signal( cvp_high_task_event );
    break;

  case CVP_THREAD_PRIORITY_ENUM_MED:
    apr_event_signal( cvp_med_task_event );
    break;

  case CVP_THREAD_PRIORITY_ENUM_LOW:
    apr_event_signal( cvp_low_task_event );
    break;

  default:
    break;
  }

  return;
}

static int32_t cvp_get_object
(
  uint32_t handle,
  cvp_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &cvp_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( cvp_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static int32_t cvp_typecast_object (
  apr_objmgr_object_t* store,
  cvp_object_type_enum_t type,
  cvp_object_t** ret_obj
)
{
  cvp_object_t* obj;

  if ( ( store == NULL ) || ( ret_obj == NULL ) )
  {
    return APR_EBADPARAM;
  }

  obj = ( ( cvp_object_t* ) store->any.ptr );

  if ( ( obj == NULL ) || ( obj->header.type != type ) )
  {
    return APR_EFAILED;
  }

  *ret_obj = obj;

  return APR_EOK;
}

static int32_t cvp_get_typed_object (
  uint32_t handle,
  cvp_object_type_enum_t type,
  cvp_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* store;

  if ( handle == 0 )
  {
    return APR_EHANDLE;
  }

  rc = apr_objmgr_find_object( &cvp_objmgr, handle, &store );
  if ( rc ) return rc;

  rc = cvp_typecast_object( store, type, ret_obj );
  if ( rc ) return rc;

  return APR_EOK;
}

/* Get session object from handle.
   Increment reference counter. */
static uint32_t cvp_access_session_obj_start (
  uint32_t handle,
  cvp_session_object_t** ret_session_obj
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  
  ( void ) apr_lock_enter( cvp_ref_cnt_lock );
  for ( ;; )
  {
    if ( handle == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    rc = cvp_get_typed_object( handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvp_object_t** ) &session_obj ) );
    if ( rc )
    {
      rc = APR_EHANDLE;
      break;
    }

    if ( ret_session_obj != NULL )
    {
      *ret_session_obj = ( ( cvp_session_object_t* ) session_obj );
    }

    /* Session is to be destroyed. Reject the acess to the session. */
    if ( session_obj->ref_cnt == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    session_obj->ref_cnt++;

    break;
  }

  ( void ) apr_lock_leave( cvp_ref_cnt_lock );

  return rc;
}

/* Decrement reference counter. */
static uint32_t cvp_access_session_obj_end (
  uint32_t handle
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  
  ( void ) apr_lock_enter( cvp_ref_cnt_lock );
  for ( ;; )
  {
    if ( handle == 0 )
    {
      rc = APR_EHANDLE;
      break;
    }

    rc = cvp_get_typed_object( handle,
                               CVP_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvp_object_t** ) &session_obj ) );
    if ( rc )
    {
      rc = APR_EHANDLE;
      break;
    }

    if ( session_obj->ref_cnt == 0 )
    { /* Session is to be destroyed. No-op. */
      rc = APR_EOK;
      break;
    }
    else
    {
      session_obj->ref_cnt--;
    }

    /* Signal Destroy Command Processing thread after
     * decrementing session_obj reference count.
     * Destroy Command is not processed until session_obj
     * is not accessed by any other command.
     */
    ( void ) apr_event_signal( cvp_low_task_event );

    rc = APR_EOK;
    break;
  }

  ( void ) apr_lock_leave( cvp_ref_cnt_lock );

  return rc;
}


/**
 * TODO: cvp_find_session() should be based on the session name
 * when passive control is implemented
 */
static cvp_session_object_t* cvp_find_session (
  cvp_info_t* p_cvp_info,
  cvp_session_object_t* session_obj
)
{
  uint32_t i;
  cvp_session_object_t* found_session = (cvp_session_object_t *) CVP_SESSION_INVALID;

  for ( i = 0; i < CVP_MAX_NUM_SESSIONS; ++i )
  {
    if ( p_cvp_info->sessions[ i ] == session_obj )
    {
      found_session = session_obj;
      break;
    }
  }

  return found_session;
}

static void cvp_add_session (
  cvp_info_t* p_cvp_info,
  cvp_session_object_t* new_session
)
{
  uint32_t i;

  if ( new_session ) /* Make sure it's not NULL. */
  {
    /* If the session already exists, do nothing. */
    if ((cvp_session_object_t *)CVP_SESSION_INVALID != cvp_find_session( p_cvp_info, new_session ) )
    {
      return;
    }

    for ( i = 0; i < CVP_MAX_NUM_SESSIONS; ++i )
    {
      if ( (cvp_session_object_t *)CVP_SESSION_INVALID == p_cvp_info->sessions[ i ] )
      { /* Found an empty spot. */
        p_cvp_info->sessions[ i ] = new_session;
        p_cvp_info->session_cnt++;
        break;
      }
    }
  }
}

static void cvp_remove_session (
  cvp_info_t* p_cvp_info,
  cvp_session_object_t* obj_session
)
{
  uint32_t i;

  if ( obj_session ) /* NULL check. */
  {
    for ( i = 0; i < CVP_MAX_NUM_SESSIONS; ++i )
    {
      if ( obj_session == p_cvp_info->sessions[ i ] )
      { /* Found it, now remove. */
        p_cvp_info->sessions[ i ] = (cvp_session_object_t *)CVP_SESSION_INVALID;
        p_cvp_info->session_cnt--;
        break;
      }
    }
  }
}

/****************************************************************************
 * PENDING COMMAND ROUTINES                                                 *
 ****************************************************************************/

static int32_t cvp_pending_control_init (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2( &ctrl->cmd_q, NULL, NULL );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVP_PENDING_CMD_STATE_ENUM_FETCH;
  ctrl->packet = NULL;
  ctrl->session_obj = NULL;

  return APR_EOK;
}

static int32_t cvp_pending_control_destroy (
  cvp_pending_control_t* ctrl
)
{
#if 0 /* There's nothing to destroy if everything is in a good state. */
  int32_t rc;
#endif /* 0 */

  if ( ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

#if 0 /* There's nothing to destroy if everything is in a good state. */
  rc = apr_list_destroy( &ctrl->cmd_q );
  if ( rc )
  {
    return APR_EFAILED;
  }

  ctrl->state = CVP_PENDING_CMD_STATE_ENUM_UNINITIALIZED;
  ctrl->packet = NULL;
  ctrl->session_obj = NULL;
#endif /* 0 */

  return APR_EOK;
}

/****************************************************************************
 * WORK QUEUE ROUTINES                                                      *
 ****************************************************************************/

static void cvp_queue_pending_packet (
  apr_list_t* pending_cmd_q,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_work_item_t* work_item;
  int do_once = 0;
  uint16_t client_addr;

  if ( pending_cmd_q == NULL )
  {
    return;
  }

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }

  do
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &cvp_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Queue the incoming command to the pending command queue. We don't
       * need to signal do work because after the incoming command
       * handler is done the pending command handler routine will be
       * called.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( pending_cmd_q, &work_item->link );
    }

    return;
  }
  while ( do_once );

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, rc );
    CVP_COMM_ERROR( rc, client_addr );
  }
}

static void cvp_queue_incoming_packet (
  cvp_thread_priority_enum_t priority,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_work_item_t* work_item;
  apr_list_t* incoming_cmd_q;
  uint16_t client_addr;

  if ( packet == NULL )
  { /* We should assert that the packet can't be NULL. */
    return;
  }

  switch ( priority )
  {
    case CVP_THREAD_PRIORITY_ENUM_HIGH:
      incoming_cmd_q = &cvp_high_task_incoming_cmd_q;
      break;

    case CVP_THREAD_PRIORITY_ENUM_MED:
      incoming_cmd_q = &cvp_med_task_incoming_cmd_q;
      break;

    case CVP_THREAD_PRIORITY_ENUM_LOW:
      incoming_cmd_q = &cvp_low_task_incoming_cmd_q;
      break;

    default:
      return;
  }

  for( ;; )
  {
    { /* Get a free command structure. */
      rc = apr_list_remove_head( &cvp_free_cmd_q,
                                 ( ( apr_list_node_t** ) &work_item ) );
      if ( rc )
      { /* No free command structure is available. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "cvp_queue_incoming_packet(): Free command queue is full" );
        rc = APR_EBUSY;
        break;
      }
    }

    { /* Report command acceptance when requested. */
      if ( priority == CVP_THREAD_PRIORITY_ENUM_HIGH )
      {
        /* Accept the command in APR dispatcher context. */
        rc = __aprv2_cmd_accept_command( cvp_apr_handle, packet );
        if ( rc )
        { /* Can't report so abort the command. */
          ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );
          break;
        }
      }
    }

    { /* Queue the new command to the incoming command queue and signal do
       * work.
       */
      work_item->packet = packet;
      ( void ) apr_list_add_tail( incoming_cmd_q, &work_item->link );
      cvp_signal_run( priority );
    }

    return;
  }

  { /* Try reporting the error. */
    client_addr = packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, rc );
    CVP_COMM_ERROR( rc, client_addr );
  }
}

/****************************************************************************
 * DEFAULT RESPONSE PROCESSING ROUTINES                                     *
 ****************************************************************************/

static void cvp_default_event_rsp_fn (
  aprv2_packet_t* packet
)
{
  /* The default event handler just drops the packet. A specific event
   * handler routine should be written to something more useful.
   */
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_set_default_response_table (
  cvp_response_fn_table_t table
)
{
  int i;

  if ( table == NULL )
  {
    return;
  }

  /* Initialize the state response handler function table. */
  for ( i = 0; i < CVP_RESPONSE_FN_ENUM_MAX; ++i )
  {
    table[ i ] = cvp_default_event_rsp_fn;
  }
}

static void cvp_simple_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    obj->is_completed = TRUE;
    obj->status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

    /* If completed command failed, log the error. */
    if ( obj->status != APR_EOK )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_simple_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
           APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                                              obj->status );
    }
  }

  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_simple_self_destruct_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &obj ) );
  if ( rc == APR_EOK )
  {
    ( void ) cvp_free_object( ( cvp_object_t* ) obj );
  }

  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/****************************************************************************
 * CUSTOM RESPONSE PROCESSING ROUTINES (NOT FOR STATE MACHINE TRANSITION)   *
 ****************************************************************************/

static void cvp_forward_command_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_track_job_object_t* forwarding_job_obj;
  cvp_session_object_t* session_obj;
  aprv2_ibasic_rsp_result_t* p_payload;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* Overwrite the orignal opcode here */
  p_payload  = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet );
  p_payload->opcode = forwarding_job_obj->orig_opcode;

  /* This should copy the original packet's content and create new one */
  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         APRV2_IBASIC_RSP_RESULT,
         APRV2_PKT_GET_PAYLOAD( void, packet ),
         APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  CVP_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_forward_command_vp3_get_size_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_track_job_object_t* forwarding_job_obj;
  cvp_session_object_t* session_obj;
  voice_get_param_ack_t* voice_get_param_ack;
  voice_param_data_t* voice_param_data;
  vss_ivp3_rsp_get_size_t* vss_ivp3_rsp_get_size = NULL;
  aprv2_ibasic_rsp_result_t basic_rsp;
  bool_t get_size_status_ok;
  uint8_t* payload = NULL;
  uint32_t align = CVD_MEM_MAPPER_SUPPORTED_CACHE_LINE_SIZE;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  for ( ;; )
  {
    get_size_status_ok = FALSE;
    if ( ( APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) ) <
         ( sizeof( voice_param_data_t ) + sizeof( voice_get_param_ack_t ) ) )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_forward_command_vp3_get_size_rsp_fn(): Payload length not "
             "proper 0x%X", APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
      rc = APR_EFAILED;
      break;
    }

    /* Extract data from payload */
    payload = APRV2_PKT_GET_PAYLOAD( void, packet );
    voice_get_param_ack = ( ( voice_get_param_ack_t* ) payload );
    payload = payload + sizeof( voice_get_param_ack_t );
    voice_param_data = ( ( voice_param_data_t* )payload );
    payload = payload + sizeof( voice_param_data_t );

    if ( voice_get_param_ack->status != APR_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_forward_command_vp3_get_size_rsp_fn(): Status is not "
             "proper 0x%08X", voice_get_param_ack->status );
      rc = voice_get_param_ack->status;
      break;
    }

    if ( voice_param_data->param_size < sizeof( vss_ivp3_rsp_get_size_t ) )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_forward_command_vp3_get_size_rsp_fn (): Size of parameter "
             "is not proper 0x%04X", voice_param_data->param_size );
      rc = APR_EFAILED;
      break;
    }

    vss_ivp3_rsp_get_size = ( ( vss_ivp3_rsp_get_size_t* ) payload );

    if( vss_ivp3_rsp_get_size->size != 0 )
    {
      vss_ivp3_rsp_get_size->size = ( ( ( vss_ivp3_rsp_get_size->size ) & 0x0000FFFF ) +
                                        sizeof( voice_param_data_t ) );
    }
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "cvp_forward_command_vp3_get_size_rsp_fn(): vp3 size before "
           "alignment 0x%08X", vss_ivp3_rsp_get_size->size );

    vss_ivp3_rsp_get_size->size =
      ( ( vss_ivp3_rsp_get_size->size + align -1 ) & ( ~ ( align - 1 ) ) );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           forwarding_job_obj->orig_src_service ,forwarding_job_obj->orig_src_port,
           forwarding_job_obj->orig_token, VSS_IVP3_RSP_GET_SIZE,
           vss_ivp3_rsp_get_size, sizeof( vss_ivp3_rsp_get_size_t ) );
    CVP_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

    get_size_status_ok = TRUE;
    break;
  }

  if ( get_size_status_ok == FALSE )
  {
    basic_rsp.opcode = VSS_IVP3_CMD_GET_SIZE;
    basic_rsp.status = rc;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
           forwarding_job_obj->orig_token,  APRV2_IBASIC_RSP_RESULT,
           &basic_rsp, sizeof( basic_rsp ) );
    CVP_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );
  }

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_forward_command_get_param_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_track_job_object_t* forwarding_job_obj;
  cvp_session_object_t* session_obj;
  uint32_t payload_len;
  uint8_t* payload;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* If the get param is for GET_UI_PROPERTY, translate the response opcode to RSP_GET_UI_PROP */
  if ( forwarding_job_obj->orig_opcode == VSS_ICOMMON_CMD_GET_UI_PROPERTY )
  {
    packet->opcode = VSS_ICOMMON_RSP_GET_UI_PROPERTY;
  }

  payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_len >= 16 )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "CVD_CAL_MSG_PARAM: cvp_forward_command_get_param_rsp_fn(): "
           "Payload[0-2]=0x%08x, 0x%08x, 0x%08x",
           *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) ),
           *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ),
           *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "CVD_CAL_MSG_PARAM: cvp_forward_command_get_param_rsp_fn(): "
           "Payload[3]=0x%08x",
           *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
  }
  else if ( payload_len >= 4 )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "CVD_CAL_MSG_PARAM: cvp_forward_command_get_param_rsp_fn(): "
           "Payload[0]=0x%08x",
           *( uint32_t* ) ( payload + sizeof( voice_param_data_t ) ) );
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "CVD_CAL_MSG_PARAM: cvp_forward_command_get_param_rsp_fn(): "
         "Get Param Response Token=%d", forwarding_job_obj->orig_token );

  /* This should copy the original packet's content and create new one */
  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token,
         packet->opcode,
         APRV2_PKT_GET_PAYLOAD( void, packet ),
         APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  CVP_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_forward_command_soundfocus_get_sectors_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_track_job_object_t* forwarding_job_obj;
  cvp_session_object_t* session_obj;
  uint32_t payload_len;
  uint8_t* payload;
  cvp_soundfocus_param_t* soundfocus_param;
  vss_isoundfocus_rsp_get_sectors_t get_sectors_rsp;
  uint32_t status;
  bool_t is_param_valid = TRUE;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  /* NOTE: The ACK packet from VCP always contains the buffer for param data, even if
     the status is not APR_EOK, and they do not write to the buffer.
     We will only read the buffer if the status is APR_EOK.
  */
  if ( payload_len == ( sizeof( cvp_soundfocus_param_t ) + sizeof( voice_get_param_ack_t ) ) )
  {
    payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );

    status = ( ( voice_get_param_ack_t* ) payload )->status;
    soundfocus_param = ( cvp_soundfocus_param_t* )( payload + sizeof( voice_get_param_ack_t ) );

    if ( status != APR_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): "
             "failed, status = (0x%08x)", status );
      is_param_valid = FALSE;
    }
    else if ( ( soundfocus_param->module_id != VOICEPROC_MODULE_TX ) ||
         ( soundfocus_param->param_id != VOICE_PARAM_FLUENCE_SOUNDFOCUS ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): "
             "Invalid mid/pid, mid=0x%08x, pid=0x%08x",
             soundfocus_param->module_id, soundfocus_param->param_id );
      is_param_valid = FALSE;
    }
    else
    {
      mmstd_memcpy( get_sectors_rsp.start_angles,
                    sizeof( get_sectors_rsp.start_angles ),
                    soundfocus_param->param_data.start_angles,
                    sizeof( get_sectors_rsp.start_angles ) );
      mmstd_memcpy( get_sectors_rsp.enables,
                    sizeof( get_sectors_rsp.enables ),
                    soundfocus_param->param_data.enables,
                    sizeof( get_sectors_rsp.enables ) );
      get_sectors_rsp.gain_step = soundfocus_param->param_data.gain_step;

      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): enables[0-3] = "
             "[%d, %d, %d, %d]", get_sectors_rsp.enables[ 0 ], get_sectors_rsp.enables[ 1 ],
             get_sectors_rsp.enables[ 2 ], get_sectors_rsp.enables[ 3 ] );

      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): enables[4-7] = "
             "[%d, %d, %d, %d]", get_sectors_rsp.enables[ 4 ], get_sectors_rsp.enables[ 5 ],
             get_sectors_rsp.enables[ 6 ], get_sectors_rsp.enables[ 7 ] );

      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): start_angles[0-3] = "
             "[%d, %d, %d, %d]", get_sectors_rsp.start_angles[ 0 ], get_sectors_rsp.start_angles[ 1 ],
             get_sectors_rsp.start_angles[ 2 ], get_sectors_rsp.start_angles[ 3 ] );

      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): start_angles[4-7] = "
             "[%d, %d, %d, %d]", get_sectors_rsp.start_angles[ 4 ], get_sectors_rsp.start_angles[ 5 ],
             get_sectors_rsp.start_angles[ 6 ], get_sectors_rsp.start_angles[ 7 ] );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): gain_step = %d", get_sectors_rsp.gain_step );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
             forwarding_job_obj->orig_token,
             VSS_ISOUNDFOCUS_RSP_GET_SECTORS,
             &get_sectors_rsp, sizeof( get_sectors_rsp ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_forward_command_soundfocus_get_sectors_rsp_fn(): "
           "Invalid param size (%d)", payload_len );
    is_param_valid = FALSE;
  }

  if ( is_param_valid == FALSE )
  {/* Send generic error code to client. */
    aprv2_ibasic_rsp_result_t rsp;
    rsp.opcode = VSS_ISOUNDFOCUS_CMD_GET_SECTORS;
    rsp.status = APR_EFAILED;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
           forwarding_job_obj->orig_token,
           APRV2_IBASIC_RSP_RESULT,
           &rsp, sizeof( rsp ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );
  }

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_forward_command_sourcetrack_get_activity_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_track_job_object_t* forwarding_job_obj;
  cvp_session_object_t* session_obj;
  uint32_t payload_len;
  voice_get_param_ack_t* payload;
  aprv2_ibasic_rsp_result_t rsp;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  rsp.opcode = VSS_ISOURCETRACK_CMD_GET_ACTIVITY;
  rsp.status = APR_EOK;

  payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
  payload = APRV2_PKT_GET_PAYLOAD( voice_get_param_ack_t, packet );

  if ( payload_len == sizeof( voice_get_param_ack_t ) )
  {
    if ( payload->status == APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_forward_command_sourcetrack_get_activity_rsp_fn(): "
           "get activity successfully" );

      /* Log the sound activity data. */
      {
        log_info_data.table_handle = 0;
        log_info_data.cal_query_handle = 0;
        log_info_data.data_seq_num = 0;

        log_info.instance =  ( ( session_obj->attached_mvm_handle << 16 ) |
                               ( session_obj->header.handle ) );
        log_info.call_num = session_obj->target_set.system_config.call_num;
        log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
        log_info.data_container_header_size = sizeof( log_info_data );
        log_info.data_container_header = &log_info_data;
        log_info.payload_buf = ( void* )&session_obj->shared_heap.sourcetrack_activity.activity_data;
        log_info.payload_size = sizeof( vss_isourcetrack_activity_data_t );

        ( void ) cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_SOUNDFOCUS_ACTIVITY_DATA,
                                ( void* )&log_info, sizeof( log_info ) );
      }

      /* Copy data to client mapped shared memory. */
      mmstd_memcpy( session_obj->sourcetrack_mem_addr.ptr,
                    sizeof( vss_isourcetrack_activity_data_t ),
                    ( ( void* )&session_obj->shared_heap.sourcetrack_activity.activity_data ) ,
                    sizeof( vss_isourcetrack_activity_data_t ) );
      ( void ) cvd_mem_mapper_cache_flush_v2( &session_obj->sourcetrack_mem_addr,
                                              sizeof( vss_isourcetrack_activity_data_t ) );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_forward_command_sourcetrack_get_activity_rsp_fn(): "
             "get activity failed, rc=(0x%08x)", payload->status );
      rsp.status = payload->status;
    }
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_forward_command_sourcetrack_get_activity_rsp_fn(): "
           "Invalid param size (%d)", payload_len );
    rsp.status = APR_EFAILED;
  }

  rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
           forwarding_job_obj->orig_token,
           APRV2_IBASIC_RSP_RESULT,
           &rsp, sizeof( rsp ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_set_param_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  session_obj->set_param_info.rsp_cnt++;

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->set_param_info.status = APR_EFAILED;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_set_param_rsp_fn(): Last set param failed with 0x%08X", status );
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_get_kpps_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;
  vpm_get_kpps_ack_t* get_kpps_ack;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  job_obj->is_completed = TRUE;
  job_obj->status = APR_EOK;

  get_kpps_ack = APRV2_PKT_GET_PAYLOAD( vpm_get_kpps_ack_t, packet );

  if( session_obj->kpps_info.vp_tx != get_kpps_ack->vptx_kpps )
  {
    session_obj->kpps_info.vp_tx = get_kpps_ack->vptx_kpps;
    session_obj->is_kpps_changed = TRUE;
  }

  if( session_obj->kpps_info.vp_rx != get_kpps_ack->vprx_kpps )
  {
    session_obj->kpps_info.vp_rx = get_kpps_ack->vprx_kpps;
    session_obj->is_kpps_changed = TRUE;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_get_kpps_rsp_fn(): vptx_kpps = %d, vprx_kpps = %d",
         get_kpps_ack->vptx_kpps, get_kpps_ack->vprx_kpps );

  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_get_algorithmic_delay_result_rsp_fn (
  aprv2_packet_t *packet
)
{
  int32_t rc;
  cvp_track_job_object_t *job_obj;
  cvp_session_object_t* session_obj;
  vpm_get_delay_ack_t* avsync_delays;
  vss_ivocproc_rsp_get_avsync_delays_t vp_avsync_delay;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  avsync_delays = APRV2_PKT_GET_PAYLOAD( vpm_get_delay_ack_t, packet );

  vp_avsync_delay.vp_rx_algorithmic_delay = avsync_delays->vprx_delay;
  vp_avsync_delay.vp_tx_algorithmic_delay = avsync_delays->vptx_delay;

  /* Send #VSS_IVOCPROC_RSP_GET_AVSYNC_DELAYS back to MVM. */
  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvp_my_addr, job_obj->orig_dst_port,
         job_obj->orig_src_service, job_obj->orig_src_port,
         job_obj->orig_token, VSS_IVOCPROC_RSP_GET_AVSYNC_DELAYS,
         &vp_avsync_delay, sizeof( vp_avsync_delay ) );
  CVP_COMM_ERROR( rc, job_obj->orig_src_service );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_get_algorithmic_delay_result_rsp_fn(): vptx_delay = %d, "
         "vprx_delay = %d ", avsync_delays->vptx_delay,
         avsync_delays->vprx_delay );

  /* Free the cvp_track_job_object_t created. */
  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );

  /* Free the response apr pakcet. */
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY */
static void cvp_calibrate_common_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->common_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_calibrate_common_rsp_fn(): Last set param failed with 0x%08X",
           status );
  }

  session_obj->common_cal.set_param_rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY */
static void cvp_calibrate_wv_fens_from_common_cal_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->common_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_calibrate_wv_fens_from_common_cal_rsp_fn(): Last set param "
           "failed with 0x%08X", status );
  }

  session_obj->common_cal.set_param_rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY */
static void cvp_calibrate_volume_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->volume_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_calibrate_volume_rsp_fn(): Last set param failed with 0x%08X",
           status );
  }

  session_obj->volume_cal.set_param_rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_calibrate_dynamic_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->dynamic_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_calibrate_dynamic_rsp_fn(): Last set param failed with 0x%08X",
           status );
  }

  session_obj->dynamic_cal.set_param_rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

static void cvp_calibrate_stream_modules_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;
  if ( status != APR_EOK )
  {
    session_obj->stream_cal_info.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_calibrate_stream_modules_rsp_fn(): Last set param failed with 0x%08X",
           status );
  }

  session_obj->stream_cal_info.set_param_rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* Response function to count the responses for the commands issued by CVS to
 * itself in each of the cleanup sequencer state, when doing cleanup due to the
 * subsystem where CVS clients reside is being restarted.
 */
static void cvp_ssr_cleanup_cmd_result_count_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  uint32_t command;
  uint32_t status;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* If the last operation failed, log the error. */
  command = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->opcode;
  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_ssr_cleanup_cmd_result_count_rsp_fn(): Command 0x%08X failed "
           "with 0x%08X", command, status );
  }

  /* Count the number of command responses received. */
  cvp_ssr_cleanup_cmd_tracking.rsp_cnt++;

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/****************************************************************************
 * OBJECT CREATION AND DESTRUCTION ROUTINES                                 *
 ****************************************************************************/

static int32_t cvp_mem_alloc_object (
  uint32_t size,
  cvp_object_t** ret_object
)
{
  int32_t rc;
  cvp_object_t* obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the new CVP object. */
    obj = apr_memmgr_malloc( &cvp_heapmgr, size );
    if ( obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mem_alloc_object(): Out of memory, requested size (%d)",
                                              size);
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the CVP object. */
    rc = apr_objmgr_alloc_object( &cvp_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mem_alloc_object(): Out of objects, result: 0x%08X",
                                              rc);
      apr_memmgr_free( &cvp_heapmgr, obj );
      return APR_ENORESOURCE;
    }

    /* Link the CVP object to the handle. */
    objmgr_obj->any.ptr = obj;

    /* Initialize allocated object with null values. */
    ( void )mmstd_memset( obj, 0xFD, size );

    /* Initialize the base CVP object header. */
    obj->header.handle = objmgr_obj->handle;
    obj->header.type = CVP_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = obj;

  return APR_EOK;
}

static int32_t cvp_mem_free_object (
  cvp_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &cvp_objmgr, object->header.handle );
  apr_memmgr_free( &cvp_heapmgr, object );

  return APR_EOK;
}

static int32_t cvp_create_session_object (
  cvp_session_object_t** ret_session_obj
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  cvp_session_object_t* session_obj;
  uint32_t i;

  for ( ;; )
  {
    if ( ret_session_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "cvp_create_session_object(): sizeof( cvp_session_object_t ): %d",
           sizeof( cvp_session_object_t ) );

    /* Allocate the session object. */
    rc = cvp_mem_alloc_object( sizeof( cvp_session_object_t ),
                               ( ( cvp_object_t** ) &session_obj ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_create_session_object(): Failed to allocate session object, "
             " result: 0x%08X", rc );
      break;
    }
    checkpoint = 1;

    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->lock );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_create_session_object(): Failed to create lock, "
             "rc = (0x%08x)", rc );
      break;
    }
    checkpoint = 2;

    { /* Initialize the session object. */
      session_obj->self_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->full_ctrl_client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->full_ctrl_client_port = APR_NULL_V;
      session_obj->vocproc_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->vocproc_handle = APR_NULL_V;
      session_obj->attached_mvm_handle = APR_NULL_V;

      session_obj->is_vocproc_config_changed = FALSE;

      session_obj->direction = APR_UNDEFINED_ID_V;
      session_obj->public_enable = FALSE;
      session_obj->modem_enable = FALSE;

      session_obj->vpcm_info.is_enabled = FALSE;
      session_obj->vpcm_info.client_addr = APRV2_PKT_INIT_ADDR_V;
      session_obj->vpcm_info.client_handle = APR_NULL_V;
      session_obj->vpcm_info.mem_handle = 0;

      session_obj->is_sr_set_by_client = FALSE;
      session_obj->is_device_config_registered = FALSE;

      session_obj->widevoice_rx_sr = CVP_DEFAULT_RX_PP_SR;

      session_obj->set_param_info.num_set_params = 0;
      session_obj->set_param_info.rsp_cnt = 0;
      session_obj->set_param_info.status = APR_EOK;

      session_obj->kpps_info.vp_rx = 0;
      session_obj->kpps_info.vp_tx = 0;
      session_obj->is_kpps_changed = FALSE;

      /* Initialize Feature Configurations for the session object */
      session_obj->hdvoice_config_info.hdvoice_config_hdr.columns = NULL;
      session_obj->hdvoice_config_info.hdvoice_config_hdr.minor_version = 0;
      session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns = 0;
      session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries = 0;
      session_obj->hdvoice_config_info.hdvoice_config_hdr.size = 0;
      session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head = NULL;

      session_obj->is_client_set_vp3_data = FALSE;
      session_obj->sound_device_info.is_available = FALSE;
      session_obj->sound_device_info.device_pair.direction = CVP_SOUND_DEVICE_DIRECTION_INVALID;
      session_obj->sound_device_info.device_pair.rx_device_id = 0;
      session_obj->sound_device_info.device_pair.tx_device_id = 0;

      session_obj->sourcetrack_mem_addr.ptr = NULL;

      { /* Temporary hack. See is_valid_afe_port_provided definition for details. */
        session_obj->is_valid_afe_port_provided = FALSE;
      }

      { /* Initialize the session and stream state machine control variables. */
        session_obj->session_ctrl.goal_completed = FALSE;
        session_obj->session_ctrl.goal_status = APR_UNDEFINED_ID_V;
        session_obj->session_ctrl.transition_job_handle = (uint32_t) NULL;

        session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET;
        session_obj->session_ctrl.goal = CVP_GOAL_ENUM_NONE;
        session_obj->session_ctrl.action = CVP_ACTION_ENUM_NONE;
        session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

        session_obj->active_set.tx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
        session_obj->active_set.rx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
        session_obj->active_set.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
        session_obj->active_set.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
        session_obj->active_set.vol_step = 0;
        session_obj->active_set.client_num_vol_steps = 0;
        session_obj->active_set.mvm_rx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->active_set.client_rx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->active_set.mvm_tx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->active_set.client_tx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->active_set.mute_ramp_duration = 0;
        session_obj->active_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
        session_obj->active_set.system_config.media_id = VSS_MEDIA_ID_NONE;
        session_obj->active_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
        session_obj->active_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
        session_obj->active_set.system_config.dec_sr = CVP_DEFAULT_DEC_SR;
        session_obj->active_set.system_config.enc_sr = CVP_DEFAULT_ENC_SR;
        session_obj->active_set.system_config.rx_pp_sr = CVP_DEFAULT_RX_PP_SR;
        session_obj->active_set.system_config.tx_pp_sr = CVP_DEFAULT_TX_PP_SR;
        session_obj->active_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
        session_obj->active_set.system_config.vsid = 0;
        session_obj->active_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
        session_obj->active_set.voice_timing.vp_rx_delivery_offset = 0;
        session_obj->active_set.voice_timing.vp_tx_start_offset = 0;
        session_obj->active_set.voice_timing.vp_tx_delivery_offset = 0;
        session_obj->active_set.vocproc_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;
        session_obj->active_set.ec_ref_port_id = VSS_IVOCPROC_PORT_ID_NONE;

        session_obj->target_set.tx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
        session_obj->target_set.rx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
        session_obj->target_set.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
        session_obj->target_set.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
        session_obj->target_set.vol_step = 0;
        session_obj->target_set.client_num_vol_steps = 0;
        session_obj->target_set.mvm_rx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->target_set.client_rx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->target_set.mvm_tx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->target_set.client_tx_mute = VSS_IVOLUME_MUTE_OFF;
        session_obj->target_set.mute_ramp_duration = 0;
        session_obj->target_set.system_config.network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
        session_obj->target_set.system_config.media_id = VSS_MEDIA_ID_NONE;
        session_obj->target_set.system_config.rx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
        session_obj->target_set.system_config.tx_voc_op_mode = VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE;
        session_obj->target_set.system_config.dec_sr = CVP_DEFAULT_DEC_SR;
        session_obj->target_set.system_config.enc_sr = CVP_DEFAULT_ENC_SR;
        session_obj->target_set.system_config.rx_pp_sr = CVP_DEFAULT_RX_PP_SR;
        session_obj->target_set.system_config.tx_pp_sr = CVP_DEFAULT_TX_PP_SR;
        session_obj->target_set.system_config.feature = VSS_ICOMMON_CAL_FEATURE_NONE;
        session_obj->target_set.system_config.vsid = 0;
        session_obj->target_set.system_config.vfr_mode = VSS_ICOMMON_VFR_MODE_SOFT;
        session_obj->target_set.system_config.call_num = 0;
        session_obj->target_set.voice_timing.vp_rx_delivery_offset = 0;
        session_obj->target_set.voice_timing.vp_tx_start_offset = 0;
        session_obj->target_set.voice_timing.vp_tx_delivery_offset = 0;
        session_obj->target_set.vocproc_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;
        session_obj->target_set.ec_ref_port_id = VSS_IVOCPROC_PORT_ID_NONE;
        session_obj->is_client_set_sectors = FALSE;

        session_obj->set_tx_dtmf_detect.enable_flag = CVP_DISABLED;
        session_obj->set_tx_dtmf_detect.client_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->set_tx_dtmf_detect.client_port = APR_NULL_V;
        session_obj->is_client_set_num_vol_steps = FALSE;

        { /* BACKWARD COMPATIBILITY */
          /* Initialize common calibration information. */
          session_obj->common_cal.is_registered = FALSE;
          session_obj->common_cal.is_calibrate_needed = FALSE;
          session_obj->common_cal.required_index_mem_size = 0;
          session_obj->common_cal.table_handle = NULL;
          session_obj->common_cal.vpm_mem_handle = 0;
          session_obj->common_cal.num_matching_entries = 0;
          session_obj->common_cal.num_set_param_to_stream = 0;
          session_obj->common_cal.set_param_rsp_cnt = 0;
          session_obj->common_cal.set_param_failed_rsp_cnt = 0;
          ( void )mmstd_memset( &session_obj->common_cal.matching_entries, 0,
                               sizeof( session_obj->common_cal.matching_entries ) );

          /* Initialize volume calibration information. */
          session_obj->volume_cal.is_registered = FALSE;
          session_obj->volume_cal.is_calibrate_needed = FALSE;
          session_obj->volume_cal.required_index_mem_size = 0;
          session_obj->volume_cal.table_handle = NULL;
          session_obj->volume_cal.vpm_mem_handle = 0;
          session_obj->volume_cal.is_v1_format = FALSE;
          session_obj->volume_cal.num_vol_indices = 0;
          session_obj->volume_cal.min_vol_index = 0;
          session_obj->volume_cal.max_vol_index = 0;
          session_obj->volume_cal.num_matching_entries = 0;
          session_obj->volume_cal.set_param_rsp_cnt = 0;
          session_obj->volume_cal.set_param_failed_rsp_cnt = 0;
          ( void )mmstd_memset( session_obj->volume_cal.matching_entries, 0,
                                sizeof( session_obj->volume_cal.matching_entries ) );
        }

        session_obj->static_cal.is_registered = FALSE;
        session_obj->static_cal.is_calibrate_needed = FALSE;
        session_obj->static_cal.table_handle = APR_NULL_V;
        session_obj->static_cal.query_handle = APR_NULL_V;
        ( void )mmstd_memset( &session_obj->static_cal.matching_entries, 0,
                              sizeof( session_obj->static_cal.matching_entries ) );
        ( void )mmstd_memset( &session_obj->static_cal.query_key_columns, 0,
                              sizeof( session_obj->static_cal.query_key_columns ) );

        session_obj->dynamic_cal.is_registered = FALSE;
        session_obj->dynamic_cal.is_calibrate_needed = FALSE;
        session_obj->dynamic_cal.min_vol_index = 0;
        session_obj->dynamic_cal.max_vol_index = 0;
        session_obj->dynamic_cal.num_vol_indices = 0;
        session_obj->dynamic_cal.is_v1_vol_format = FALSE;
        session_obj->dynamic_cal.table_handle = APR_NULL_V;
        session_obj->dynamic_cal.query_handle = APR_NULL_V;
        ( void )mmstd_memset( &session_obj->dynamic_cal.matching_entries, 0,
                              sizeof( session_obj->dynamic_cal.matching_entries ) );
        ( void )mmstd_memset( &session_obj->dynamic_cal.query_key_columns, 0,
                              sizeof( session_obj->dynamic_cal.query_key_columns ) );
        session_obj->dynamic_cal.num_set_param_issued = 0;
        session_obj->dynamic_cal.set_param_rsp_cnt = 0;
        session_obj->dynamic_cal.set_param_failed_rsp_cnt = 0;

        session_obj->stream_cal_info.is_wv_cal_param_found = FALSE;
        session_obj->stream_cal_info.is_wv_v2_cal_param_found = FALSE;
        session_obj->stream_cal_info.is_fens_enable_param_found = FALSE;
        session_obj->stream_cal_info.is_fens_cal_param_found = FALSE;
        session_obj->stream_cal_info.is_fens_cal_param_v2_found = FALSE;
        session_obj->stream_cal_info.is_fens_v2_enable_param_found = FALSE;
        session_obj->stream_cal_info.is_fens_v2_cal_param_v3_found = FALSE;
        session_obj->stream_cal_info.is_rx_gain_cal_param_found = FALSE;
        ( void )mmstd_memset( &session_obj->stream_cal_info.wv_cal_param, 0,
                              sizeof( session_obj->stream_cal_info.wv_cal_param ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.wv_v2_cal_param, 0,
                              sizeof( session_obj->stream_cal_info.wv_v2_cal_param ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.fens_enable_param, 0,
                              sizeof( session_obj->stream_cal_info.fens_enable_param ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.fens_cal_param, 0,
                              sizeof( session_obj->stream_cal_info.fens_cal_param ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.fens_cal_param_v2, 0,
                              sizeof( session_obj->stream_cal_info.fens_cal_param_v2 ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.fens_v2_enable_param, 0,
                              sizeof( session_obj->stream_cal_info.fens_v2_enable_param ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.fens_v2_cal_param_v3, 0,
                              sizeof( session_obj->stream_cal_info.fens_v2_cal_param_v3 ) );
        ( void )mmstd_memset( &session_obj->stream_cal_info.rx_gain_cal_param, 0,
                              sizeof( session_obj->stream_cal_info.rx_gain_cal_param ) );
        session_obj->stream_cal_info.num_set_param_issued = 0;
        session_obj->stream_cal_info.set_param_rsp_cnt = 0;
        session_obj->stream_cal_info.set_param_failed_rsp_cnt = 0;

        session_obj->ref_cnt = 1;
        /* Initialize custom topology params. */
        session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_NONE;
        session_obj->topo_param.is_registered = FALSE;
        session_obj->topo_param.mem_size = 0;
        session_obj->topo_param.vpm_param_size = 0;
        for ( i = 0; i < CVP_MAX_NUM_TOPO_PARAMS; i++)
        {
          session_obj->topo_param.vpm_param[i].param_size = 0;
          session_obj->topo_param.vpm_param[i].param_virt_addr = NULL;
        }
        session_obj->topo_param.num_dev_chans.tx_num_channels = 0;
        session_obj->topo_param.num_dev_chans.rx_num_channels = 0;
      }

      /* Initialize Clock control table for the session object */
      session_obj->clk_ctrl_table.columns = NULL;
      session_obj->clk_ctrl_table.minor_version = 0;
      session_obj->clk_ctrl_table.num_columns = 0;
      session_obj->clk_ctrl_table.size = 0;

      /* Initialize the attached stream list. */
      ( void ) apr_list_init_v2( &(session_obj->attached_stream_list), NULL, NULL );

      ( void ) mmstd_memset( &session_obj->shared_heap, 0,
                             sizeof( session_obj->shared_heap ) );
    }

    /* Complete initialization. */
    session_obj->header.type = CVP_OBJECT_TYPE_ENUM_SESSION;
      /* Mark the actual object type here to indicate that the object has been
       * fully initialized. Otherwise, on an error the destructor called at
       * the checkpoint handler would inadvertently try to free resources in
       * the object that have not been allocated yet or have already been
       * freed by the clean up sequence.
       */

    *ret_session_obj = session_obj;

    return APR_EOK;
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "cvp_create_session_object(): failed, checkpoint=%d", checkpoint );

  switch ( checkpoint )
  {
    case 2:
      ( void ) apr_lock_destroy ( session_obj->lock );
      /*-fallthru */
    case 1:
      ( void ) cvp_free_object( ( cvp_object_t* ) session_obj );
      /*-fallthru */
    default:
      break;
  }

  return rc;
}

static int32_t cvp_create_simple_job_object (
  uint32_t context_handle,
  cvp_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvp_mem_alloc_object( sizeof( cvp_simple_job_object_t ),
                             ( ( cvp_object_t** ) &job_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_create_simple_job_object(): Failed to allocate object, result: 0x%08X",
                                            rc );
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    job_obj->header.type = CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    job_obj->context_handle = context_handle;
    cvp_set_default_response_table( job_obj->fn_table );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_result_rsp_fn;
    job_obj->is_accepted = FALSE;
    job_obj->is_completed = FALSE;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvp_create_track_job_object (
  uint32_t context_handle,
  cvp_track_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvp_track_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvp_mem_alloc_object( sizeof( cvp_track_job_object_t ),
                             ( ( cvp_object_t** ) &job_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_create_track_job_object(): Failed to allocate object, result: 0x%08X",
                                            rc );
    return APR_ENORESOURCE;
  }

  { /* Initialize the track job object. */
    job_obj->header.type = CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    job_obj->context_handle = context_handle;
    cvp_set_default_response_table( job_obj->fn_table );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_result_rsp_fn;
    job_obj->is_accepted = FALSE;
    job_obj->is_completed = FALSE;
    job_obj->status = APR_UNDEFINED_ID_V;
    job_obj->orig_src_port = 0;
    job_obj->orig_src_service = 0;
    job_obj->orig_opcode = APR_UNDEFINED_ID_V;
    job_obj->orig_token = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvp_create_sequencer_job_object (
  cvp_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  cvp_sequencer_job_object_t* job_obj;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = cvp_mem_alloc_object( sizeof( cvp_sequencer_job_object_t ),
                             ( ( cvp_object_t** ) &job_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = CVP_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}

static int32_t cvp_free_object (
  cvp_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Perform object-specific clean-up necessary. */
  switch ( object->header.type )
  {
  case CVP_OBJECT_TYPE_ENUM_UNINITIALIZED:
    break;

  case CVP_OBJECT_TYPE_ENUM_SESSION:
    {
      cvp_session_object_t* session_obj = (cvp_session_object_t*) object;
      apr_list_node_t* node;

      /* Clear attached streams list. */
      while ( session_obj->attached_stream_list.size > 0 )
      {
        ( void ) apr_list_get_next(
                    &(session_obj->attached_stream_list),
                    &(session_obj->attached_stream_list.dummy),
                    &node );

        ( void ) apr_list_delete (
                    &(session_obj->attached_stream_list),
                    node );

        apr_memmgr_free( &cvp_heapmgr, ( ( cvp_attached_stream_item_t* ) node ) );
      }

      ( void ) apr_list_destroy( &session_obj->attached_stream_list );
      ( void ) apr_lock_destroy( session_obj->lock );
    }
    break;

  case CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB:
    break;

  case CVP_OBJECT_TYPE_ENUM_SEQUENCER_JOB:
    break;

  case CVP_OBJECT_TYPE_ENUM_INVALID:
    break;

  default:
    break;
  }

  /* Free the object memory and object handle. */
  ( void ) cvp_mem_free_object( object );

  return APR_EOK;
}

/****************************************************************************
 * CVP VP3 HELPER FUNCTIONS                                                 *
 ****************************************************************************/

static int32_t cvp_find_matching_item_from_global_vp3_heap (
  cvp_session_object_t* session_obj,
  cvp_global_vp3_list_item_t** ret_vp3_list_item
)
{
  int32_t rc;
  cvp_global_vp3_list_item_t* vp3_list_item;
  bool_t is_item_found = FALSE;

  if ( ( session_obj == NULL ) || ( ret_vp3_list_item == NULL ) )
  {
    return APR_EBADPARAM;
  }

  vp3_list_item = ( ( cvp_global_vp3_list_item_t* ) &cvp_global_vp3_list_used_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvp_global_vp3_list_used_q,
                            ( ( apr_list_node_t* ) vp3_list_item ),
                            ( ( apr_list_node_t** ) &vp3_list_item ) );
    if ( rc ) break;

    if ( session_obj->sound_device_info.device_pair.direction ==
         vp3_list_item->vp3->sound_device_pair.direction )
    {
      switch ( session_obj->sound_device_info.device_pair.direction )
      {
      case CVP_SOUND_DEVICE_DIRECTION_RX:
        {
          if ( session_obj->sound_device_info.device_pair.rx_device_id ==
               vp3_list_item->vp3->sound_device_pair.rx_device_id )
          {
            *ret_vp3_list_item = vp3_list_item;
            is_item_found = TRUE;
          }
        }
        break;

      case CVP_SOUND_DEVICE_DIRECTION_TX:
        {
          if ( session_obj->sound_device_info.device_pair.tx_device_id ==
               vp3_list_item->vp3->sound_device_pair.tx_device_id )
          {
            *ret_vp3_list_item = vp3_list_item;
            is_item_found = TRUE;
          }
        }
        break;

      case CVP_SOUND_DEVICE_DIRECTION_RX_TX:
        {
          if ( ( session_obj->sound_device_info.device_pair.rx_device_id ==
                 vp3_list_item->vp3->sound_device_pair.rx_device_id ) &&
               ( session_obj->sound_device_info.device_pair.tx_device_id ==
                 vp3_list_item->vp3->sound_device_pair.tx_device_id ) )
          {
            *ret_vp3_list_item = vp3_list_item;
            is_item_found = TRUE;
          }
        }
        break;

      default:
        break;
      }
    }

    if ( is_item_found == TRUE )
    {
      break;
    }
  }

  return rc;
}

static int32_t cvp_update_global_vp3_heap (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_global_vp3_list_item_t* vp3_list_item;

  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  for ( ;; )
  {
    {
      /* If the VP3 data from the global VP3 heap is already stored for the
       * current sound device pair. Update the VP3 data and update the global VP3
       * tracking list to reflect that the current sound device pair is the most
       * recently (i.e. move it to the head of the list) used sound device pair.
       */
      rc = cvp_find_matching_item_from_global_vp3_heap( session_obj, &vp3_list_item );
      if ( rc == APR_EOK )
      {
        mmstd_memcpy( vp3_list_item->vp3->data, CVP_MAX_VP3_DATA_LEN,
                      session_obj->shared_heap.vp3_cache.data,
                      session_obj->shared_heap.vp3_cache.data_len );
        vp3_list_item->vp3->data_len = session_obj->shared_heap.vp3_cache.data_len;

        /* Move the item to the head (most recently used device pair) of the
         * VP3 tracking list.
         */
        ( void ) apr_list_delete( &cvp_global_vp3_list_used_q, &vp3_list_item->link );
        ( void ) apr_list_add_head( &cvp_global_vp3_list_used_q, &vp3_list_item->link );

        break;
      }
    }

    {
      /* If VP3 data for the current device pair is not found in the global heap,
       * add the VP3 data for the current device into the global heap and update
       * the global VP3 tracking list to reflect that the current sound device
       * pair is the most recently used sound device pair.
       */

      rc = apr_list_remove_head( &cvp_global_vp3_list_free_q,
                                 ( ( apr_list_node_t** ) &vp3_list_item ) );
      if ( rc )
      {
        /* No free VP3 tracking list item is available. Remove the oldest
         * device pair tracking item from the global VP3 heap which will be
         * used to track the new device pair.
         */
        rc = apr_list_remove_tail( &cvp_global_vp3_list_used_q,
                                   ( ( apr_list_node_t** ) &vp3_list_item ) );

        if ( rc ) break;
      }

      mmstd_memcpy( vp3_list_item->vp3->data, CVP_MAX_VP3_DATA_LEN,
                    session_obj->shared_heap.vp3_cache.data,
                    session_obj->shared_heap.vp3_cache.data_len );
      vp3_list_item->vp3->data_len = session_obj->shared_heap.vp3_cache.data_len;

      vp3_list_item->vp3->sound_device_pair.direction =
        session_obj->sound_device_info.device_pair.direction;

      vp3_list_item->vp3->sound_device_pair.rx_device_id =
        session_obj->sound_device_info.device_pair.rx_device_id;

      vp3_list_item->vp3->sound_device_pair.tx_device_id =
        session_obj->sound_device_info.device_pair.tx_device_id;

      ( void ) apr_list_add_head( &cvp_global_vp3_list_used_q, &vp3_list_item->link );
    }

    break;
  }

  return rc;
}

static int32_t cvp_clear_sound_device_info (
  cvp_session_object_t* session_obj
)
{
  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  session_obj->sound_device_info.is_available = FALSE;
  session_obj->sound_device_info.device_pair.direction = CVP_SOUND_DEVICE_DIRECTION_INVALID;
  session_obj->sound_device_info.device_pair.rx_device_id = 0;
  session_obj->sound_device_info.device_pair.tx_device_id = 0;

  return APR_EOK;
}

static int32_t cvp_parse_sound_device_mod(
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* sound_device_mod
)
{
  int32_t rc = APR_EOK;

  if ( ( session_obj == NULL ) || ( sound_device_mod == NULL ) )
  {
    return APR_EBADPARAM;
  }

  switch ( sound_device_mod->param_id )
  {
  case VSS_PARAM_RX_SOUND_DEVICE_ID:
    {
      if ( sound_device_mod->param_size != sizeof( uint32_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Invalid sound device param size %d for param 0x%08X",
               sound_device_mod->param_size, sound_device_mod->param_id );

        rc = APR_EBADPARAM;
        break;
      }

      if ( session_obj->sound_device_info.device_pair.direction ==
           CVP_SOUND_DEVICE_DIRECTION_TX )
      {
        session_obj->sound_device_info.device_pair.direction =
           CVP_SOUND_DEVICE_DIRECTION_RX_TX;
      }
      else
      if ( session_obj->sound_device_info.device_pair.direction ==
             CVP_SOUND_DEVICE_DIRECTION_INVALID )
      {
        session_obj->sound_device_info.device_pair.direction =
           CVP_SOUND_DEVICE_DIRECTION_RX;
      }

      session_obj->sound_device_info.device_pair.rx_device_id
        = *( ( uint32_t* ) ( ( uint8_t* ) sound_device_mod + sizeof( voice_param_data_t ) ) );

      session_obj->sound_device_info.is_available = TRUE;
    }
    break;

  case VSS_PARAM_TX_SOUND_DEVICE_ID:
    {
      if ( sound_device_mod->param_size != sizeof( uint32_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Invalid sound device param size %d for param 0x%08X",
               sound_device_mod->param_size, sound_device_mod->param_id );

        rc = APR_EBADPARAM;
        break;
      }

      if ( session_obj->sound_device_info.device_pair.direction ==
           CVP_SOUND_DEVICE_DIRECTION_RX )
      {
        session_obj->sound_device_info.device_pair.direction =
           CVP_SOUND_DEVICE_DIRECTION_RX_TX;
      }
      else
      if ( session_obj->sound_device_info.device_pair.direction ==
             CVP_SOUND_DEVICE_DIRECTION_INVALID )
      {
        session_obj->sound_device_info.device_pair.direction =
           CVP_SOUND_DEVICE_DIRECTION_TX;
      }

      session_obj->sound_device_info.device_pair.tx_device_id
        = *( ( uint32_t* ) ( ( uint8_t* ) sound_device_mod + sizeof( voice_param_data_t ) ) );

      session_obj->sound_device_info.is_available = TRUE;
    }
    break;

  default:
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "Unrecognized param ID 0x%08X for mod ID 0x%08X",
           sound_device_mod->param_id, sound_device_mod->module_id );
    break;
  }

  if ( rc != APR_EOK )
  {
    ( void ) cvp_clear_sound_device_info( session_obj );
  }

  return rc;
}

/* Verify custom topology clock control parameters.
 */
static int32_t cvp_verify_clk_ctrl_columns (
  uint32_t num_columns,
  void* columns_head,
  uint32_t* column_size
)
{
  int32_t rc = APR_EOK;
  uint32_t i;
  uint32_t advance_in_bytes = 0;
  vss_param_clock_control_params_t* columns = ( ( vss_param_clock_control_params_t * ) columns_head );
  advance_in_bytes = sizeof( vss_param_clock_control_params_t );
  *column_size = 0;

  for ( i = 0; i < num_columns; i++ )
  {
    if ( columns->direction != VSS_IVOCPROC_DIRECTION_RX &&
         columns->direction != VSS_IVOCPROC_DIRECTION_TX &&
         columns->direction != VSS_IVOCPROC_DIRECTION_RX_TX )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_verify_clk_ctrl_columns(): Undefined direction 0x%08X", columns->direction );
      rc = APR_EBADPARAM;
      break;
    }
    if ( columns->sampling_rate != VSS_PARAM_SAMPLING_RATE_ANY &&
         columns->sampling_rate != VSS_PARAM_SAMPLING_RATE_8K &&
         columns->sampling_rate != VSS_PARAM_SAMPLING_RATE_16K &&
         columns->sampling_rate != VSS_PARAM_SAMPLING_RATE_32K &&
         columns->sampling_rate != VSS_PARAM_SAMPLING_RATE_48K )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_verify_clk_ctrl_columns(): Undefined sampling_rate 0x%08X", columns->sampling_rate );
      rc = APR_EBADPARAM;
      break;
    }
    if ( columns->mpps_scale_factor < CVP_MIN_MPPS_SCALE_FACTOR ||
         columns->mpps_scale_factor > CVP_MAX_MPPS_SCALE_FACTOR )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_verify_clk_ctrl_columns(): Invalid mpps_scale_factor 0x%08X", columns->mpps_scale_factor );
      rc = APR_EBADPARAM;
      break;
    }
    if ( columns->bus_bw_scale_factor < CVP_MIN_BW_SCALE_FACTOR ||
         columns->bus_bw_scale_factor > CVP_MAX_BW_SCALE_FACTOR )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_verify_clk_ctrl_columns(): Invalid bus_bw_scale_factor 0x%08X", columns->bus_bw_scale_factor );
      rc = APR_EBADPARAM;
    break;
    }
    {
      /* Advance column pointer */
      columns = ( vss_param_clock_control_params_t* ) ( ( ( uint8_t* ) columns ) + advance_in_bytes );
      *column_size += advance_in_bytes;
    }
  }
  return rc;
}

/* Verify custom topology clock control table.
 */
static int32_t cvp_verify_clk_ctrl_params (
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* device_config_entry
)
{
  int32_t rc = APR_EOK;
  uint32_t column_size = 0;
  uint8_t* param_data_rdr;

  if ( session_obj == NULL || device_config_entry == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_clk_ctrl_params(): Invalid input parameter!" );
    return APR_EBADPARAM;
  }

  param_data_rdr = ( ( uint8_t* ) device_config_entry + sizeof( voice_param_data_t ) );

  if ( param_data_rdr != NULL )
  {
    session_obj->clk_ctrl_table.minor_version = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if ( param_data_rdr != NULL )
  {
    session_obj->clk_ctrl_table.size = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if ( param_data_rdr != NULL )
  {
    session_obj->clk_ctrl_table.num_columns = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if ( param_data_rdr != NULL )
  {
    session_obj->clk_ctrl_table.columns = ( void* ) param_data_rdr;
  }

  for ( ;; )
  {
    /* Verify clock control headers */
    if ( session_obj->clk_ctrl_table.minor_version != 0 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_clk_ctrl_params(): Invalid Version %d",
                                              session_obj->clk_ctrl_table.minor_version );
      rc = APR_EBADPARAM;
      break;
    }
    else if ( ( session_obj->clk_ctrl_table.size + ( 2 * sizeof( uint32_t ) ) ) != device_config_entry->param_size )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_verify_clk_ctrl_params(): Invalid data size 0x%08X with param size 0x%08X",
                                              session_obj->clk_ctrl_table.size,
                                              device_config_entry->param_size);
      rc = APR_EBADPARAM;
      break;
    }
    else if ( session_obj->clk_ctrl_table.num_columns == 0 ) // ToDo
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_clk_ctrl_params(): Invalid number of columns %d",
                                              session_obj->clk_ctrl_table.num_columns );
      rc = APR_EBADPARAM;
      break;
    }
    else if ( session_obj->clk_ctrl_table.columns == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_clk_ctrl_params(): Column definition is NULL %d" );
      rc = APR_EBADPARAM;
      break;
    }
    else
    {
      /* Verify clk_ctrl_table column data */
      rc = cvp_verify_clk_ctrl_columns ( session_obj->clk_ctrl_table.num_columns,
                                         session_obj->clk_ctrl_table.columns,
                                         &column_size );
      if ( rc != APR_EOK )
      {
        /* Invalid Column Defintitions */
        break;
      }
    }
    break;
  }

  return rc;
}

static int32_t cvp_clear_clock_control_info(
  cvp_session_object_t* session_obj
)
{
  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  session_obj->clk_ctrl_table.columns = NULL;
  session_obj->clk_ctrl_table.minor_version = 0;
  session_obj->clk_ctrl_table.num_columns = 0;
  session_obj->clk_ctrl_table.size = 0;

  return APR_EOK;
}

/* Parse custom topology clock control table.
 */
static int32_t cvp_parse_clk_ctrl_mod(
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* clk_ctrl_mod
)
{
  int32_t rc = APR_EOK;

  if ( ( session_obj == NULL ) || ( clk_ctrl_mod == NULL ) )
  {
    return APR_EBADPARAM;
  }

  switch ( clk_ctrl_mod->param_id )
  {
    case VSS_PARAM_CLOCK_CONTROL:
    {
      rc = cvp_verify_clk_ctrl_params( session_obj, clk_ctrl_mod );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_parse_clk_ctrl_mod(): param 0x%08X, cvp_verify_clk_ctrl_params rc %d",
               clk_ctrl_mod->param_id, rc );
        /* Invalid parameters, clear clock control info. */
        ( void ) cvp_clear_clock_control_info( session_obj );
      }
    }
    break;

    default:
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "cvp_parse_clk_ctrl_mod(): Unrecognized param ID 0x%08X for mod ID 0x%08X",
             clk_ctrl_mod->param_id, clk_ctrl_mod->module_id );
    break;
  }
  return rc;
}

/****************************************************************************
 * CVP CALIBRATION HELPER FUNCTIONS                                         *
 ****************************************************************************/
static bool_t cvp_media_id_is_var_sr (
  uint32_t media_id /* Assumes the media_id is valid. */
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_EVS:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t cvp_sr_is_valid (
  uint32_t sr
)
{
  bool_t rc;

  switch ( sr )
  {
  case 8000:
  case 16000:
  case 32000:
  case 41000:
  case 48000:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  return rc;
}

static bool_t cvp_media_id_is_nb_sr (
  uint32_t media_id
)
{
  bool_t rc;

  switch ( media_id )
  {
  case VSS_MEDIA_ID_NONE:
  case VSS_MEDIA_ID_13K:
  case VSS_MEDIA_ID_EVRC:
  case VSS_MEDIA_ID_4GV_NB:
  case VSS_MEDIA_ID_AMR_NB:
  case VSS_MEDIA_ID_EFR:
  case VSS_MEDIA_ID_FR:
  case VSS_MEDIA_ID_HR:
  case VSS_MEDIA_ID_PCM_8_KHZ:
  case VSS_MEDIA_ID_G711_ALAW:
  case VSS_MEDIA_ID_G711_MULAW:
  case VSS_MEDIA_ID_G711_LINEAR:
  case VSS_MEDIA_ID_G729:
  case VSS_MEDIA_ID_G722:
    rc = TRUE;
    break;

  default:
    rc = FALSE;
    break;
  }

  /* VSS_MEDIA_ID_NONE is not considered a valid parameter because
   * system cannot run without a media type.
   */

  return rc;
}

static int32_t cvp_verify_hdvoice_config_data (
  uint32_t* data_head
)
{
  int32_t rc = APR_EOK;
  vss_param_hdvoice_config_data_t* hdvoice_config_data = ( ( vss_param_hdvoice_config_data_t* ) data_head );

  if ( rc == APR_EOK )
  {
    switch( hdvoice_config_data->feature_id )
    {
      case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
      case VSS_ICOMMON_CAL_FEATURE_BEAMR:
      case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
        break;

      default:
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_hdvoice_config_data(): Invalid Feature ID: 0x%08X",
                                                hdvoice_config_data->feature_id );
        rc = APR_EBADPARAM;
        break;
    }
  }

  if ( rc == APR_EOK )
  {
    switch( hdvoice_config_data->enable_mode )
    {
      case VSS_PARAM_FEATURE_DEFAULT_OFF:
      case VSS_PARAM_FEATURE_DEFAULT_ON:
      case VSS_PARAM_FEATURE_FORCED_OFF:
      case VSS_PARAM_FEATURE_FORCED_ON:
        break;

      default:
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_hdvoice_config_data(): Invalid Enable Mode: 0x%08X",
                                                hdvoice_config_data->enable_mode );
        rc = APR_EBADPARAM;
        break;
    }
  }

  if ( rc == APR_EOK )
  {
    switch( hdvoice_config_data->rx_sampling_rate )
    {
      case 16000:
        break;

      default:
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_hdvoice_config_data(): Invalid Sampling Rate: 0x%08X",
                                                hdvoice_config_data->rx_sampling_rate );
        rc = APR_EBADPARAM;
        break;
    }
  }

  return rc;
}

static int32_t cvp_verify_sys_config (
  uint32_t num_columns,
  void* columns_head,
  uint32_t num_sys_configs,
  void* sys_config_head
)
{
  int32_t rc = APR_EOK;
  uint32_t i, j = 0;
  uint32_t* sys_config_column_value = ( ( uint32_t* ) sys_config_head );
  vss_param_cal_column_t* columns = ( ( vss_param_cal_column_t* ) columns_head );
  uint32_t column_id = 0;
  uint32_t column_type = 0;
  uint32_t column_size = 0;
  uint64_t typed_column_value = 0;

  for ( i = 0; i < num_sys_configs; i++ )
  {
    for ( j = 0; j < num_columns; j++ )
    {
      column_id = columns->id;
      column_type = columns->type;
      column_size = 0;

      switch ( column_type )
      {
        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT64:
          typed_column_value = ( *( uint64_t* ) sys_config_column_value );
          column_size = sizeof( uint64_t );
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT32:
          typed_column_value = ( *( uint32_t* ) sys_config_column_value );
          column_size = sizeof( uint32_t );
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT16:
          typed_column_value = ( *( uint16_t* ) sys_config_column_value );
          column_size = sizeof( uint16_t );
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT8:
          typed_column_value = ( *( uint8_t* ) sys_config_column_value );
          column_size = sizeof( uint8_t );
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Invalid Column Type: 0x%08X",
                                                  column_type );
          rc = APR_EBADPARAM;
          break;
      }

      switch ( typed_column_value )
      {
        case VSS_ICOMMON_CAL_NETWORK_ID_NONE:
        case VSS_ICOMMON_CAL_NETWORK_ID_CDMA:
        case VSS_ICOMMON_CAL_NETWORK_ID_GSM:
        case VSS_ICOMMON_CAL_NETWORK_ID_WCDMA:
        case VSS_ICOMMON_CAL_NETWORK_ID_VOIP:
        case VSS_ICOMMON_CAL_NETWORK_ID_LTE:
          if ( column_id != VSS_ICOMMON_CAL_COLUMN_NETWORK )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Unmatching Column Value: 0x%016x " \
                                                    "with Column ID: 0x%08X",
                                                    typed_column_value, column_id );
            rc = APR_EBADPARAM;
          }
          break;

        case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE:
        case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NB:
        case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_WB:
        case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_SWB:
        case VSS_ICOMMON_CAL_VOC_OPERATING_MODE_FB:
          if ( ( column_id != VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE ) &&
               ( column_id != VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE ) )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Unmatching Column Value: 0x%016x " \
                                                    "with Column ID: 0x%08X",
                                                    typed_column_value, column_id );
            rc = APR_EBADPARAM;
          }
          break;

        case VSS_MEDIA_ID_NONE:
        case VSS_MEDIA_ID_13K:
        case VSS_MEDIA_ID_EVRC:
        case VSS_MEDIA_ID_4GV_NB:
        case VSS_MEDIA_ID_4GV_WB:
        case VSS_MEDIA_ID_4GV_NW:
        case VSS_MEDIA_ID_4GV_NW2K:
        case VSS_MEDIA_ID_AMR_NB:
        case VSS_MEDIA_ID_AMR_WB:
        case VSS_MEDIA_ID_EAMR:
        case VSS_MEDIA_ID_EFR:
        case VSS_MEDIA_ID_FR:
        case VSS_MEDIA_ID_HR:
        case VSS_MEDIA_ID_PCM_8_KHZ:
        case VSS_MEDIA_ID_PCM_16_KHZ:
        case VSS_MEDIA_ID_PCM_32_KHZ:
        case VSS_MEDIA_ID_PCM_48_KHZ:
        case VSS_MEDIA_ID_G711_ALAW:
        case VSS_MEDIA_ID_G711_MULAW:
        case VSS_MEDIA_ID_G711_LINEAR:
        case VSS_MEDIA_ID_G729:
        case VSS_MEDIA_ID_G722:
        case VSS_MEDIA_ID_EVS:
          if ( column_id != VSS_ICOMMON_CAL_COLUMN_MEDIA_ID )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Unmatching Column Value: 0x%016x " \
                                                    "with Column ID: 0x%08X",
                                                    typed_column_value, column_id );
            rc = APR_EBADPARAM;
          }
          break;

        case VSS_ICOMMON_CAL_FEATURE_NONE:
        case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE:
        case VSS_ICOMMON_CAL_FEATURE_WIDEVOICE2:
        case VSS_ICOMMON_CAL_FEATURE_BEAMR:
          if ( column_id != VSS_ICOMMON_CAL_COLUMN_FEATURE )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Unmatching Column Value: 0x%016x " \
                                                    "with Column ID: 0x%08X",
                                                    typed_column_value, column_id );
            rc = APR_EBADPARAM;
          }
          break;

        case 0:
        case 8000:
        case 16000:
        case 32000:
        case 41000:
        case 48000:
          if ( ( column_id != VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE ) ||
                ( column_id != VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE ) )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Unmatching Column Value: 0x%08X " \
                                                    "with Column ID: 0x%08X",
                                                    typed_column_value, column_id );
            rc = APR_EBADPARAM;
          }
          break;

        default:
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config(): Undefined Column Value: 0x%08X \
                                                  with Column ID: 0x%08X",
                                                  typed_column_value, column_id );
          rc = APR_EBADPARAM;
          break;
      }

      if ( rc )
      {
        /*  Invalid Sysem Configuration List  */
        break;  /*  Break from Inner For Loop  */
      }
      else
      {
        /* Advance Column Pointer to find next ID */
        columns = ( vss_param_cal_column_t* ) ( ( ( uint8_t* ) columns ) + sizeof( columns->id ) + sizeof( columns->type ) + column_size );

        /* Advance to next Column Value in Sys Config List */
        sys_config_column_value = ( uint32_t* ) ( ( ( uint8_t* ) sys_config_column_value ) + column_size );
      }
    }
    if ( rc )
    {
      /*  Invalid Sysem Configuration List  */
      break;  /*  Break from Outer For Loop  */
    }
    else
    {
      /* Do Data Parse */
      rc = cvp_verify_hdvoice_config_data ( sys_config_column_value );

      if ( rc )
      {
        /** Invalid Config Data **/
        break;
      }
      else
      {
        sys_config_column_value = ( uint32_t* ) ( ( ( uint8_t* ) sys_config_column_value ) + sizeof ( vss_param_hdvoice_config_data_t ) );
        /* Return Column Pointer to the first Column */
        columns = ( ( vss_param_cal_column_t* ) columns_head );
      }
    }
  }

  return rc;
}

static int32_t cvp_verify_sys_config_columns (
  uint32_t num_columns,
  void* columns_head,
  uint32_t* column_size
)
{
  int32_t rc = APR_EOK;
  uint32_t i;
  uint32_t advance_in_bytes = 0;
  vss_param_cal_column_t* columns = ( ( vss_param_cal_column_t * ) columns_head );

  *column_size = 0;

  for ( i = 0; i < num_columns; i++ )
  {
    advance_in_bytes = 0;

    switch ( columns->type )
    {
    case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT32:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_verify_sys_config_columns(): Verified column type: 0x%08X " \
                                               "Array position: %d",
                                                columns->type, i );
        advance_in_bytes += sizeof( columns->type );
        advance_in_bytes += sizeof( uint32_t );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT8:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Verified Unexpected " \
                                                "column type: 0x%08X Array position: %d",
                                                columns->type, i );
        advance_in_bytes += sizeof( columns->type );
        advance_in_bytes += sizeof( uint8_t );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT16:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Verified Unexpected " \
                                                "column type: 0x%08X Array position: %d",
                                                columns->type, i );
        advance_in_bytes += sizeof( columns->type );
        advance_in_bytes += sizeof( uint16_t );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT64:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Verified Unexpected " \
                                                "column type: 0x%08X Array position: %d",
                                                columns->type, i );
        advance_in_bytes += sizeof( columns->type );
        advance_in_bytes += sizeof( uint64_t );
        break;
      }
    default:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined Column type " \
                                                "column type: 0x%08X Array position: %d",
                                                columns->type, i );
        rc = APR_EBADPARAM;
        break;
      }
    }

    switch ( columns->id )
    {
    /** Expected Columns **/
    case VSS_ICOMMON_CAL_COLUMN_NETWORK:
      {
        if ( columns->na_value.uint32_val != VSS_ICOMMON_CAL_NETWORK_ID_NONE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_NETWORK: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_MEDIA_ID:
      {
        if ( columns->na_value.uint32_val != VSS_MEDIA_ID_NONE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_MEDIA_ID: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE:
      {
        if ( columns->na_value.uint32_val != VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    /** Unexpected Columns **/
    case VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE:
      {
        if ( ( columns->na_value.uint32_val != 0 ) ||
              ( columns->na_value.uint8_val != 0 ) ||
              ( columns->na_value.uint16_val != 0 ) ||
              ( columns->na_value.uint64_val != 0 ) )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE:
      {
        if ( ( columns->na_value.uint32_val != 0 ) ||
              ( columns->na_value.uint8_val != 0 ) ||
              ( columns->na_value.uint16_val != 0 ) ||
              ( columns->na_value.uint64_val != 0 ) )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE:
      {
        if ( columns->na_value.uint32_val != VSS_ICOMMON_CAL_VOC_OPERATING_MODE_NONE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    case VSS_ICOMMON_CAL_COLUMN_FEATURE:
      {
        if ( columns->na_value.uint32_val != VSS_ICOMMON_CAL_FEATURE_NONE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined NA Value " \
                                                  "for VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE: 0x%08X",
                                                  columns->na_value.uint32_val );
          rc = APR_EBADPARAM;
        }
        advance_in_bytes += sizeof( columns->id );
        break;
      }
    default:
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_sys_config_columns(): Undefined Column ID " \
                                                "column ID: 0x%08X Array position: %d",
                                                columns->id, i );
        rc = APR_EBADPARAM;
        break;
      }
    }

    if ( rc )
    {
      /* Invalid Column Definitions */
      break; /** From Loop **/
    }
    else
    {
      /* Advance column pointer */
      columns = ( vss_param_cal_column_t* ) ( ( ( uint8_t* ) columns ) + advance_in_bytes );
      *column_size += advance_in_bytes;
    }
  }

  return rc;
}

static int32_t cvp_verify_feature_config (
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* device_config_entry
)
{
  int32_t rc = APR_EOK;
  uint32_t column_size = 0;
  uint8_t* param_data_rdr;

  if ( session_obj == NULL || device_config_entry == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Invalid input parameter!" );
    return APR_EBADPARAM;
  }

  param_data_rdr = ( ( uint8_t* ) device_config_entry + sizeof( voice_param_data_t ) );

  if ( param_data_rdr != NULL )
  {
    session_obj->hdvoice_config_info.hdvoice_config_hdr.minor_version = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if ( param_data_rdr != NULL )
  {
    session_obj->hdvoice_config_info.hdvoice_config_hdr.size = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if ( param_data_rdr != NULL )
  {
    session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns = ( *( uint32_t* ) param_data_rdr );
  }
  param_data_rdr += sizeof( uint32_t );

  if (param_data_rdr != NULL)
  {
    session_obj->hdvoice_config_info.hdvoice_config_hdr.columns = (void *) param_data_rdr;
  }

  for (;;)
  {
    /** Verify Feature Config headers **/
    if ( session_obj->hdvoice_config_info.hdvoice_config_hdr.minor_version != 0 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Invalid Version %d",
                                              session_obj->hdvoice_config_info.hdvoice_config_hdr.minor_version );
      rc = APR_EBADPARAM;
      break;
    }
    else if ( ( session_obj->hdvoice_config_info.hdvoice_config_hdr.size + ( 2*sizeof( uint32_t ) ) )!= device_config_entry->param_size )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Invalid HD voice data size 0x%08X with param size 0x%08X",
                                              session_obj->hdvoice_config_info.hdvoice_config_hdr.size,
                                              device_config_entry->param_size);
      rc = APR_EBADPARAM;
      break;
    }
    else if ( session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns == 0 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Invalid number of columns %d",
                                              session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns );
      rc = APR_EBADPARAM;
      break;
    }
    else if ( session_obj->hdvoice_config_info.hdvoice_config_hdr.columns == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Column definition in header is NULL %d" );
      rc = APR_EBADPARAM;
      break;
    }
    else
    {
      /** Verify Sys Config Column data **/
      rc = cvp_verify_sys_config_columns ( session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns,
                                           session_obj->hdvoice_config_info.hdvoice_config_hdr.columns,
                                           &column_size );

      if ( rc == APR_EOK )
      {
        param_data_rdr += column_size;

        if ( param_data_rdr != NULL )
        {
          session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries = ( *( uint32_t* ) param_data_rdr );
        }
        param_data_rdr += sizeof( uint32_t );

        session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head = ( ( void* )  param_data_rdr );

        if ( session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries == 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Invalid number of entries %d",
                                                  session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries );
          rc = APR_EBADPARAM;
          break;
        }
        else if ( session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head == NULL )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_verify_feature_config(): Sys Config Entries is NULL" );
          rc = APR_EBADPARAM;
          break;
        }
        else
        {
          /** Verify Sys Config Data **/
          rc = cvp_verify_sys_config( session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns,
                                      session_obj->hdvoice_config_info.hdvoice_config_hdr.columns,
                                      session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries,
                                      session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head );

          if ( rc )
          {
            /** Invalid Sys Config Data **/
            break;
          }
        }
      }
      else
      {
        /** Invalid Column Defintitions **/
        break;
      }
    }

    if ( rc == APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_verify_feature_config(): HD Voice Parameter Verified!" );
      break;
    }
  }

  return rc;
}

static int32_t cvp_clear_feature_config (
  cvp_session_object_t* session_obj
)
{
  session_obj->hdvoice_config_info.hdvoice_config_hdr.columns = NULL;
  session_obj->hdvoice_config_info.hdvoice_config_hdr.minor_version = 0;
  session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns = 0;
  session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries = 0;
  session_obj->hdvoice_config_info.hdvoice_config_hdr.size = 0;
  session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head = NULL;

  return APR_EOK;
}

static int32_t cvp_parse_device_config_wv_mod (
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* device_config_entry,
  uint32_t* temp_wv_sr
)
{
  cvp_device_config_wv_sr_t* device_config_wv_sr;
  int32_t rc = APR_EOK;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_parse_device_config_wv_mod(): " \
                                        "WV Mod Parser" );

  switch ( device_config_entry->param_id )
  {
  case VSS_PARAM_RX_SAMPLING_RATE:
    {
      device_config_wv_sr = ( ( cvp_device_config_wv_sr_t* ) device_config_entry );

      if ( cvp_sr_is_valid( device_config_wv_sr->rx_sr ) == FALSE )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_parse_device_config_wv_mod(): " \
                                                "Invalid widevoice Rx sampling rate, %d",
                                                device_config_wv_sr->rx_sr );
        rc = APR_EBADPARAM;
        break;
      }

      if ( session_obj->static_cal.is_registered || session_obj->dynamic_cal.is_registered )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_parse_device_config_wv_mod(): " \
                                              "Unable to register Device Config with " \
                                              "WideVoice Config with Static or Dynamic Calibration Tables" );
        break;
      }

      *temp_wv_sr = device_config_wv_sr->rx_sr;

      break;
    }
  default:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_parse_device_config_wv_mod(): " \
                                              "Invalid param_id 0x%08X for mod_id 0x%08x",
                                              device_config_entry->param_id,
                                              device_config_entry->module_id );
      break;
    }
  }

  return rc;
}

static int32_t cvp_parse_device_config_feature_mod (
  cvp_session_object_t* session_obj,
  cvp_device_config_entry_t* device_config_entry
)
{
  int32_t rc = APR_EOK;

  switch ( device_config_entry->param_id )
  {
  case VSS_PARAM_HDVOICE_CONFIG:
    {
      if ( session_obj->common_cal.is_registered || session_obj->volume_cal.is_registered )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_parse_device_config_feature_mod(): " \
                                              "Feature Config Module with HD Voice Incompatible " \
                                              "with Common or Volume Calibration Tables");
        break;
      }
      else
      {
        rc = cvp_verify_feature_config( session_obj, device_config_entry );
      }

      if ( rc )
      {
        /* Invalid parameter, clear feature config info */
        ( void ) cvp_clear_feature_config( session_obj );
      }
      break;
    }
  default:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_parse_device_config_feature_mod(): " \
                                              "Invalid param_id 0x%08X for mod_id 0x%08x",
                                              device_config_entry->param_id,
                                              device_config_entry->module_id );
      break;
    }
  }

  return rc;
}

static int32_t cvp_post_parse_device_config (
  cvp_session_object_t* session_obj,
  uint32_t temp_wv_sr
)
{
  if ( ( temp_wv_sr != 0 ) &&
       ( session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head == NULL ) )
  {
    /* Pick higher Rx sampling rate between widevoice and system_config settings,
     * if client has not set it.  */
    session_obj->widevoice_rx_sr = temp_wv_sr;

    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_post_parse_device_config(): " \
                                         "Device Config Registered with WideVoice Module" );

    if ( ( session_obj->is_sr_set_by_client == FALSE ) &&
          ( session_obj->widevoice_rx_sr > session_obj->target_set.system_config.rx_pp_sr ) )
    {
      session_obj->target_set.system_config.rx_pp_sr = session_obj->widevoice_rx_sr;
      session_obj->is_vocproc_config_changed = TRUE;
    }
  }

  return APR_EOK;
}


static int32_t cvp_verify_param_data_size (
  uint32_t param_size
)
{
  /* By design, all voice parameter size must be multiple of 4 bytes. */
  if ( param_size & 0x3 )
  {
    return APR_EFAILED;
  }

  return APR_EOK;
}

static uint32_t cvp_abs (
  int32_t value
)
{
  return ( value  < 0 ) ? -( value ) : value;
}

static int32_t cvp_convert_volume_step_to_mB(
  int32_t min_vol_mB,
  int32_t max_vol_mB,
  uint32_t total_vol_steps,
  uint32_t vol_step
)
{
  int32_t mB_per_step;
  int32_t vol_level;

  mB_per_step = ( max_vol_mB - min_vol_mB ) / total_vol_steps;
  vol_level = min_vol_mB + mB_per_step * vol_step;

  return vol_level;
}

static uint32_t cvp_convert_mB_to_linearQ28 (
  int32_t  volume_level_mB
)
{
  uint32_t linear_gain = 0;
    /* Computed linear gain in Q28 format. */
  int32_t vol_diff_one;
    /* Intermediates needed for computation. */
  uint32_t vol_diff_two;
    /* Intermediate needed for computation. */
  int32_t closest_mB;
    /* Closest known mB to given mB. */
  int32_t index;
    /* Index into volume interpolation table. */
  int64_t vol_diff_product;
    /* Intermediates needed for computation. */

  /* Find the closest (lower) adjacent mB from the known values.*/
  {
    closest_mB = volume_level_mB % CVP_VOLUME_INTERPOLATION_STEPS;

    if ( closest_mB < 0 )
    {
      closest_mB = volume_level_mB - closest_mB - CVP_VOLUME_INTERPOLATION_STEPS;
    }
    else
    {
      closest_mB = volume_level_mB - closest_mB;
    }

    vol_diff_one = volume_level_mB - closest_mB;

    index = volume_level_mB - CVP_MIN_VOLUME_MB_SUPPORTED;
    index = index / CVP_VOLUME_INTERPOLATION_STEPS;

    linear_gain = cvp_vol_interpolation_table[ index ];

    if ( vol_diff_one != 0 )
    {
      /* For all other values compute value by interpolation. */
      vol_diff_two = ( int32_t )( cvp_vol_interpolation_table[ index + 1 ] - linear_gain );
      vol_diff_product = ( ( int64_t )vol_diff_two ) * ( ( int64_t )vol_diff_one );
      linear_gain = linear_gain + ( uint32_t )( ( vol_diff_product / CVP_VOLUME_INTERPOLATION_STEPS ) );
    }
  }

  return linear_gain;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CALIBRATION HELPER FUNCTIONS FOR BACKWARD COMPATIBILITY SUPPORT         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* Find volume level from a cal entry. */
static int32_t cvp_find_vol_level_from_cal_entry (
  cvd_cal_table_descriptor_t* table_descriptor,
  void* data,
  uint32_t data_len,
  uint32_t* ret_vol_level
)
{
  int32_t rc;
  bool_t is_vol_level_found = FALSE;
  uint32_t consumed_len = 0;
  voice_param_data_t* cur_param;
  uint8_t* vol_data;

  if ( ret_vol_level == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_cal_entry(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  vol_data = ( uint8_t* )data;

  /* Initial validation */
  rc = cvd_cal_validate_entry( table_descriptor, data, data_len );
  if ( rc != APR_EOK )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_cal_entry(): Invalid table" );
    return rc;
  }

  while ( consumed_len < data_len )
  {
    if ( ( data_len - consumed_len ) < sizeof( voice_param_data_t ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_cal_entry(): Improperly formatted cal data, " \
                                            "not enough bytes to form voice_param_data_t" );
      rc = APR_EFAILED;
      break;
    }

    cur_param = ( ( voice_param_data_t* ) ( vol_data + consumed_len ) );

    /* Validate param size */
    rc = cvd_cal_validate_entry( table_descriptor, data,
                                 consumed_len + cur_param->param_size + sizeof(voice_param_data_t) );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_cal_entry(): Invalid table" );
      break;
    }

    consumed_len += ( sizeof( voice_param_data_t ) );

    if ( ( ( cur_param->module_id == VSS_MODULE_RX_VOLUME ) && ( cur_param->param_id == VSS_PARAM_VOLUME ) ) ||
         ( ( cur_param->module_id == VOICE_MODULE_RX_VOL  ) && ( cur_param->param_id == VOICE_PARAM_VOL  ) ) )
    {
      is_vol_level_found = TRUE;
      *ret_vol_level = *( ( int32_t* )( vol_data + consumed_len ) );
      rc = APR_EOK;
      break;
    }

    consumed_len += cur_param->param_size;
  }

  if ( is_vol_level_found == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_cal_entry(): No volume level found in this cal entry" );
    rc = APR_EFAILED;
  }

  return rc;
}

/* Find volume level from matched calibration entries. */
/* Caller: cvp_find_vol_level() and
           cvp_calibrate_network(). */
static int32_t cvp_find_vol_level_from_matched_cal_entries (
  cvp_session_object_t* session_obj,
  uint32_t* ret_vol_level
)
{
  int32_t rc;
  uint32_t idx;
  uint32_t vol_level;
  void* data;
  uint32_t data_len;
  cvd_cal_entry_t* cal_entry;
  bool_t is_found = FALSE;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( ret_vol_level == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_matched_cal_entries(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  cal_entry = session_obj->volume_cal.matching_entries;

  for( idx = 0; idx < session_obj->volume_cal.num_matching_entries; ++idx )
  {
    data = cal_entry->start_ptr;
    data_len = cal_entry->size;

    rc = cvp_find_vol_level_from_cal_entry( &session_obj->volume_cal.table_handle->table_descriptor, data, data_len, &vol_level );
    if ( rc == APR_EOK )
    {
      /* Found volume level, exit. */
      is_found = TRUE;
      *ret_vol_level = vol_level;
      rc = APR_EOK;
      break;
    }
    /* else: not found, keep searching. */

    cal_entry++;
  }

  if ( is_found == FALSE )
  {
    rc = APR_EFAILED;
  }

  return rc;
}

/* Caller:  cvp_find_vol_table_format(). */
static int32_t cvp_find_vol_default_entry (
  cvp_session_object_t* session_obj,
  uint32_t vol_index
)
{
  int32_t rc;
  cvd_cal_column_t columns[ CVP_VOLUME_NUM_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  /* Form a query key. */
  mmstd_memset( columns, 0, sizeof( columns ) );
  columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
  columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX;
  columns[ 0 ].value = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
  columns[ 1 ].value = vol_index;

  switch ( session_obj->direction )
  {
  case VSS_IVOCPROC_DIRECTION_RX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 2 ].value = CVP_DEFAULT_RX_PP_SR;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = CVP_DEFAULT_TX_PP_SR;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 3 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = CVP_DEFAULT_RX_PP_SR;
      columns[ 3 ].value = CVP_DEFAULT_TX_PP_SR;
      cal_key.num_columns = 4;
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  cal_key.columns = columns;

  /* Search volume level from matched entries. */
  for ( ;; )
  {
    /* Find matched entries for vol_index. */
    rc = cvd_cal_query_table( session_obj->volume_cal.table_handle,
                              &cal_key, sizeof( session_obj->volume_cal.matching_entries ),
                              session_obj->volume_cal.matching_entries,
                              &session_obj->volume_cal.num_matching_entries );
    if ( ( rc != APR_EOK ) || ( session_obj->volume_cal.num_matching_entries == 0 ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_vol_default_entry(): Cannot find entries for volume index %d, rc=0x%08X",
                                             vol_index, rc );
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_vol_default_entry(): network 0x%08X, Rx SR %d, Tx SR %d",
                                             columns[0].value,
                                             columns[2].value,
                                             columns[ 3 ].value );
      rc = APR_EFAILED;
      break;
    }

    rc = APR_EOK;
    break;
  }

  return rc;
}

/* Find volume level corresponding to a volume index from cal table. */
/* Caller: cvp_find_closest_vol_index(), and cvp_find_vol_table_format()
           cvp_calibrate_volume_interpolation(). */
static int32_t cvp_find_vol_level (
  cvp_session_object_t* session_obj,
  uint32_t vol_index,
  uint32_t* ret_vol_level
)
{
  int32_t rc;
  uint32_t vol_level;
  cvd_cal_column_t columns[ CVP_VOLUME_NUM_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( ret_vol_level == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  /* Form a query key. */
  mmstd_memset( columns, 0, sizeof( columns ) );
  columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
  columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX;
  columns[ 0 ].value = session_obj->target_set.system_config.network_id;
  columns[ 1 ].value = vol_index;

  switch ( session_obj->direction )
  {
  case VSS_IVOCPROC_DIRECTION_RX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.tx_pp_sr;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 3 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
      columns[ 3 ].value = session_obj->target_set.system_config.tx_pp_sr;
      cal_key.num_columns = 4;
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  cal_key.columns = columns;

  /* Search volume level from matched entries. */
  for ( ;; )
  {
    /* Find matched entries for vol_index. */
    rc = cvd_cal_query_table( session_obj->volume_cal.table_handle,
                              &cal_key, sizeof( session_obj->volume_cal.matching_entries ),
                              session_obj->volume_cal.matching_entries,
                              &session_obj->volume_cal.num_matching_entries );
    if ( ( rc != APR_EOK ) || ( session_obj->volume_cal.num_matching_entries == 0 ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_vol_level(): Cannot find entries for volume index %d, rc=0x%08X",
                                             vol_index, rc );
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_vol_level(): network 0x%08X, Rx SR %d, Tx SR %d",
                                             columns[0].value,
                                             columns[2].value,
                                             columns[ 3 ].value );
      rc = APR_EFAILED;
      break;
    }

    /* Find volume level from matched entries. */
    rc = cvp_find_vol_level_from_matched_cal_entries( session_obj, &vol_level );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level(): Cannot find volume level from entries, rc=0x%08X",
                                              rc );
      rc = APR_EFAILED;
      break;
    }

    *ret_vol_level = vol_level;

    rc = APR_EOK;
    break;
  }

  return rc;
}

/* Find the volume index that indexes to the closest volume level
   (to the target volume level) in calibration table.
   Caller: cvp_calibrate_volume_interpolation(). */
static int32_t cvp_find_closest_vol_index (
  cvp_session_object_t* session_obj,
  uint32_t in_vol_level,
  uint32_t* ret_vol_index
)
{
  int32_t rc;
  uint32_t vol_index;
  uint32_t vol_level = 0xFFFFFFFF;
  uint32_t distance = 0XFFFFFFFF;
  uint32_t cur_distance;
  bool_t is_found = FALSE;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( ret_vol_index == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_closest_vol_index(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  /* Note: Double closed region ([min, max]). */
  for( vol_index = session_obj->volume_cal.min_vol_index; vol_index <= session_obj->volume_cal.max_vol_index; ++vol_index )
  {
    rc = cvp_find_vol_level( session_obj, vol_index, &vol_level );
    if ( rc != APR_EOK )
    {
      /* Not found, keep searching. */
      continue;
    }

    cur_distance = cvp_abs( in_vol_level - vol_level );
    if ( distance > cur_distance )
    {
      /* Current distance is decreasing, keep searching. */
      distance = cur_distance;
    }
    else
    {
      /* Current distance is increasing, exit. */
      /* Assumption: volume level increases as volume index increases. */
      is_found = TRUE;
      *ret_vol_index = ( vol_index - 1 ); /* Assumption: vol index is incremented by 1. */
      rc = APR_EOK;
      break;
    }
  }

  if ( is_found == FALSE )
  {
    /* No volume level has been found. */
    rc = APR_EFAILED;
  }

  return rc;
}

/* Find volume table format from a cal entry. */
static int32_t cvp_find_format_from_cal_entry (
  cvd_cal_table_descriptor_t *table_descriptor,
  void* data,
  uint32_t data_len,
  bool_t* is_v1_format
)
{
  int32_t rc;
  bool_t is_vol_level_found = FALSE;
  uint32_t consumed_len = 0;
  voice_param_data_t* cur_param;
  uint8_t* vol_data;

  if ( is_v1_format == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_format_from_cal_entry(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  *is_v1_format = FALSE;
  vol_data = ( uint8_t* )data;

  /* Initial validation */
  rc = cvd_cal_validate_entry( table_descriptor, data, data_len );
  if ( rc != APR_EOK )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_format_from_cal_entry(): Invalid table" );
    return rc;
  }

  while ( consumed_len < data_len )
  {
    if ( ( data_len - consumed_len ) < sizeof( voice_param_data_t ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_format_from_cal_entry(): Improperly formatted cal data, " \
                                            "not enough bytes to form voice_param_data_t" );
      rc = APR_EFAILED;
      break;
    }

    cur_param = ( ( voice_param_data_t* ) ( vol_data + consumed_len ) );

    /* Validate param size */
    rc = cvd_cal_validate_entry( table_descriptor, data,
                                 consumed_len + cur_param->param_size + sizeof(voice_param_data_t) );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_format_from_cal_entry(): Invalid table" );
      break;
    }

    consumed_len += ( sizeof( voice_param_data_t ) );

    if ( ( cur_param->module_id == VOICE_MODULE_RX_VOL ) &&
         ( cur_param->param_id == VOICE_PARAM_VOL ) )
    {
      is_vol_level_found = TRUE;
      *is_v1_format = TRUE;
      rc = APR_EOK;
      break;
    }
    else if ( ( cur_param->module_id == VSS_MODULE_RX_VOLUME ) &&
              ( cur_param->param_id == VSS_PARAM_VOLUME ) )
    {
      is_vol_level_found = TRUE;
      *is_v1_format = FALSE;
      rc = APR_EOK;
      break;
    }

    consumed_len += cur_param->param_size;
  }

  if ( is_vol_level_found == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_format_from_cal_entry(): No format info found" );
    rc = APR_EFAILED;
  }

  return rc;
}

/* Find volume table format from matched calibration entries. */
static int32_t cvp_find_vol_table_format_from_matched_cal_entries (
  cvp_session_object_t* session_obj,
  bool_t* is_v1_format
)
{
  int32_t rc;
  uint32_t idx;
  void* data;
  uint32_t data_len;
  cvd_cal_entry_t* cal_entry;
  bool_t is_found = FALSE;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( is_v1_format == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_table_format_from_matched_cal_entries(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  cal_entry = session_obj->volume_cal.matching_entries;

  for( idx = 0; idx < session_obj->volume_cal.num_matching_entries; ++idx )
  {
    data = cal_entry->start_ptr;
    data_len = cal_entry->size;

    rc = cvp_find_format_from_cal_entry( &session_obj->volume_cal.table_handle->table_descriptor, data, data_len, is_v1_format );
    if ( rc == APR_EOK )
    {
      /* Found volume level, exit. */
      is_found = TRUE;
      break;
    }
    /* else: Not found, keep searching. */

    cal_entry++;
  }

  if ( is_found == FALSE )
  {
    rc = APR_EFAILED;
  }

  return rc;
}

/* Find volume table format. */
static int32_t cvp_find_vol_table_format (
  cvp_session_object_t* session_obj,
  bool_t* is_v1_format
)
{
  int32_t rc;

  for ( ;; )
  {
    if ( is_v1_format == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_table_format(): Invalid input parameter" );
      rc = APR_EBADPARAM;
      break;
    }

    *is_v1_format = FALSE;

    /* Query min volume index with defaultsampling rate. */
    /* This is to obatin matched entries for checking table format. */
    rc = cvp_find_vol_default_entry( session_obj, session_obj->volume_cal.min_vol_index );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_table_format(): Cannot find min volume level" );
      rc = APR_EFAILED;
      break;
    }

    /* Search through the matched entries. */
    rc = cvp_find_vol_table_format_from_matched_cal_entries( session_obj, is_v1_format );
    break;
  }

  return rc;
}

/* Applies volume calibration data to VPM for linear volume mapping use case,
 * using the rsp_fn provided by the caller for handling the individual
 * set_param response.
 */
static int32_t cvp_calibrate_volume_linear_mapping (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;
  cvd_cal_column_t columns[ CVP_VOLUME_NUM_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;
  uint32_t set_param_cnt;
  uint32_t vol_level;
  cvp_vol_set_param_t set_vol_param;
  uint32_t vol_level_linearQ28;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  session_obj->volume_cal.num_matching_entries = 0;
  session_obj->volume_cal.set_param_rsp_cnt = 0;
  session_obj->volume_cal.set_param_failed_rsp_cnt = 0;

  mmstd_memset( columns, 0, sizeof( columns ) );
  columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
  columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX;
  columns[ 0 ].value = session_obj->target_set.system_config.network_id;
  columns[ 1 ].value = session_obj->target_set.vol_step;

  switch ( session_obj->direction )
  {
  case VSS_IVOCPROC_DIRECTION_RX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.tx_pp_sr;
      cal_key.num_columns = 3;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
      columns[ 3 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
      columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
      columns[ 3 ].value = session_obj->target_set.system_config.tx_pp_sr;
      cal_key.num_columns = 4;
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  cal_key.columns = columns;

  for ( ;; )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_linear_mapping(): Network 0x%08X, vol_index %d",
                                          columns[0].value,
                                          columns[ 1 ].value );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_linear_mapping(): Rx SR %d, Tx SR %d",
                                          columns[2].value,
                                          columns[ 3 ].value );

    rc = cvd_cal_query_table(
           session_obj->volume_cal.table_handle,
           &cal_key, sizeof( session_obj->volume_cal.matching_entries ),
           session_obj->volume_cal.matching_entries,
           &session_obj->volume_cal.num_matching_entries );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_linear_mapping(): cvd_cal_query_table " \
                                              "failed, rc=0x%08X. Not calibrating.",
                                              rc );
      break;
    }

    if ( session_obj->volume_cal.num_matching_entries == 0 )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_volume_linear_mapping(): Cannot find a matching " \
                                           "entry. Not calibrating" );
      rc = APR_EIMMEDIATE;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_linear_mapping(): Found %d matched entries",
                                          session_obj->volume_cal.num_matching_entries );

    /* Apply the calibration data. */
    for ( set_param_cnt = 0; set_param_cnt < session_obj->volume_cal.num_matching_entries; ++set_param_cnt )
    {
      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                              session_obj->volume_cal.matching_entries[ set_param_cnt ].start_ptr );
      set_param.payload_size = session_obj->volume_cal.matching_entries[ set_param_cnt ].size;
      set_param.mem_map_handle = session_obj->volume_cal.vpm_mem_handle;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_linear_mapping(): payload_address: 0x%016X, " \
                                            "payload_size: %d, memory handle %d",
                                            session_obj->volume_cal.matching_entries[set_param_cnt].start_ptr,
                                            set_param.payload_size,
                                            set_param.mem_map_handle );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_param, sizeof( set_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    /* Find the volume level. */
    rc = cvp_find_vol_level_from_matched_cal_entries( session_obj, &vol_level );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_linear_mapping(): Cannot find volume " \
                                              "level from matched entries, rc=0x%08X",
                                              rc );
      break;
    }

    { /* Apply the volume level. */
      /* Calculate volume level in linear Q28 format and set volume level to FW. */
      if ( session_obj->volume_cal.is_v1_format == TRUE )
      {
        /* Convert from linearQ13 (32 bit) to linearQ28. */
        vol_level_linearQ28 = ( vol_level << 15 );
      }
      else
      {
        /* Convert from mb to linearQ28. */
        vol_level_linearQ28 = cvp_convert_mB_to_linearQ28( vol_level );
      }

      set_vol_param.payload_address_lsw = 0;
      set_vol_param.payload_address_msw = 0;
      set_vol_param.payload_size = sizeof( cvp_vol_param_t );
      set_vol_param.mem_map_handle = 0;
      set_vol_param.vol_param.module_id = VOICE_MODULE_RX_VOL;
      set_vol_param.vol_param.param_id = VOICE_PARAM_SOFT_VOL;
      set_vol_param.vol_param.param_size = sizeof( cvp_vol_param_data_t );
      set_vol_param.vol_param.reserved = 0;
      set_vol_param.vol_param.param_data.volume = vol_level_linearQ28;
      set_vol_param.vol_param.param_data.ramp_duration_ms = ( uint16_t )vol_ramp_duration;
      set_vol_param.vol_param.param_data.reserved = 0;

      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_linear_mapping(): vol_level = %d, " \
                                            "vol_level_linearQ28 = %d, vol_ramp_duration = %d",
                                            vol_level,
                                            vol_level_linearQ28,
                                            vol_ramp_duration );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_vol_param, sizeof( set_vol_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    break;
  }

  return rc;
}

/* Applies volume calibration data to VPM for non-linear volume mapping use
 * case (interpolation is required), using the rsp_fn provided by the caller
 * for handling the individual set_param response.
 */
static int32_t cvp_calibrate_volume_interpolation (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  uint32_t rc;
  uint32_t min_vol_level;
  uint32_t max_vol_level;
  uint32_t interpolated_vol_level;
  uint32_t set_param_cnt;
  cvp_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;
  uint32_t vol_index=0;
  cvp_vol_set_param_t set_vol_param;
  uint32_t vol_level_linearQ28;
  cvd_cal_column_t columns[ CVP_VOLUME_NUM_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  for ( ;; )
  {
    session_obj->volume_cal.num_matching_entries = 0;
    session_obj->volume_cal.set_param_rsp_cnt = 0;
    session_obj->volume_cal.set_param_failed_rsp_cnt = 0;

    /* Find min and max volume level for a specific network ID. */
    /* Input: network id, volume table, min_vol_index, and max_vol_index. */
    /* Output: min and max volume level (corresponding to the min_vol_index and max_vol_index). */
    /* Assumption: volume level increases as volume index increase. */
    rc = cvp_find_vol_level( session_obj, session_obj->volume_cal.min_vol_index, &min_vol_level );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_interpolation(): Cannot find min volume " \
                                            "level. Not calibrating" );
      break;
    }

    rc = cvp_find_vol_level( session_obj, session_obj->volume_cal.max_vol_index, &max_vol_level );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_interpolation(): Cannot find max volume level. " \
                                            "Not calibrating" );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): min_vol_level = %d, max_vol_level = %d",
                                          min_vol_level, max_vol_level );

    /* Interpolate target volume step into target volume level. */
    /* Input: client specified volume step, number of total volume steps, min and max volume level. */
    /* Output: interpolated volume level in mB. */
    interpolated_vol_level = cvp_convert_volume_step_to_mB(
                                min_vol_level,
                                max_vol_level,
                                session_obj->target_set.client_num_vol_steps,
                                session_obj->target_set.vol_step );

    /* Find the closest volume level in table, as well as the corresponding volume index. */
    /* Input: interpolated volume level in mB. */
    /* Output: closest volume level in table, and the corresponding volume index. */
    /* Assumption: volume level increases as volume index increase. */
    rc = cvp_find_closest_vol_index( session_obj, interpolated_vol_level, &vol_index );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_interpolation(): Cannot find closest volume level to " \
                                              "target volume level %d. Not calibrating",
                                              interpolated_vol_level );
      break;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): interpolated_vol_level = %d, " \
                                            "vol_index = %d",
                                            interpolated_vol_level,
                                            vol_index );
    }

    /* Find the calibration data of the closest volume level. */
    {
      mmstd_memset( columns, 0, sizeof( columns ) );
      columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
      columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX;
      columns[ 0 ].value = session_obj->target_set.system_config.network_id;
      columns[ 1 ].value = vol_index;

      switch ( session_obj->direction )
      {
      case VSS_IVOCPROC_DIRECTION_RX:
        {
          columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
          columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
          cal_key.num_columns = 3;
        }
        break;

      case VSS_IVOCPROC_DIRECTION_TX:
        {
          columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
          columns[ 2 ].value = session_obj->target_set.system_config.tx_pp_sr;
          cal_key.num_columns = 3;
        }
        break;

      case VSS_IVOCPROC_DIRECTION_RX_TX:
        {
          columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
          columns[ 3 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
          columns[ 2 ].value = session_obj->target_set.system_config.rx_pp_sr;
          columns[ 3 ].value = session_obj->target_set.system_config.tx_pp_sr;
          cal_key.num_columns = 4;
        }
        break;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }

      cal_key.columns = columns;

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): Network 0x%08X, vol_index %d",
                                            columns[0].value,
                                            columns[ 1 ].value );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): Rx SR %d, Tx SR %d",
                                            columns[2].value,
                                            columns[ 3 ].value );

      rc = cvd_cal_query_table(
             session_obj->volume_cal.table_handle,
             &cal_key, sizeof( session_obj->volume_cal.matching_entries ),
             session_obj->volume_cal.matching_entries,
             &session_obj->volume_cal.num_matching_entries );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_interpolation(): cvd_cal_query_table " \
                                                "failed, rc=0x%08X. Not calibrating.",
                                                rc );
        break;
      }

      if ( session_obj->volume_cal.num_matching_entries == 0 )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_volume_interpolation(): Cannot find a matching " \
                                             "entry. Not calibrating" );
        rc = APR_EIMMEDIATE;
        break;
      }

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): Find %d matched entries",
                                            session_obj->volume_cal.num_matching_entries );
    }

    /* Apply calibration data. */
    for ( set_param_cnt = 0; set_param_cnt < session_obj->volume_cal.num_matching_entries; ++set_param_cnt )
    {
      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                              session_obj->volume_cal.matching_entries[ set_param_cnt ].start_ptr );
      set_param.payload_size = session_obj->volume_cal.matching_entries[ set_param_cnt ].size;
      set_param.mem_map_handle = session_obj->volume_cal.vpm_mem_handle;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): payload_address: 0x%016X, " \
                                            "payload_size: %d, memory handle %d",
                                            session_obj->volume_cal.matching_entries[set_param_cnt].start_ptr,
                                            set_param.payload_size,
                                            set_param.mem_map_handle );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_param, sizeof( set_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    { /* Apply volume level. */
      /* Convert interpolated volume level in mB to Q28 format. */
      vol_level_linearQ28 = cvp_convert_mB_to_linearQ28( interpolated_vol_level );

      set_vol_param.payload_address_lsw = 0;
      set_vol_param.payload_address_msw = 0;
      set_vol_param.payload_size = sizeof( cvp_vol_param_t );
      set_vol_param.mem_map_handle = 0;
      set_vol_param.vol_param.module_id = VOICE_MODULE_RX_VOL;
      set_vol_param.vol_param.param_id = VOICE_PARAM_SOFT_VOL;
      set_vol_param.vol_param.param_size = sizeof( cvp_vol_param_data_t );
      set_vol_param.vol_param.reserved = 0;
      set_vol_param.vol_param.param_data.volume = vol_level_linearQ28;
      set_vol_param.vol_param.param_data.ramp_duration_ms = ( uint16_t )vol_ramp_duration;
      set_vol_param.vol_param.param_data.reserved = 0;

      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_volume_interpolation(): interpolated_vol_level = %d, " \
                                            "vol_level_linearQ28 = %d, vol_ramp_duration = %d",
                                            interpolated_vol_level,
                                            vol_level_linearQ28,
                                            vol_ramp_duration );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_vol_param, sizeof( set_vol_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    break;
  }

  return rc;
}

/* Find a module id /parameter id pair from cal entries. */
static int32_t cvp_find_mid_pid_pair_from_cal_entry (
  cvd_cal_table_descriptor_t* table_descriptor,
  void* data,
  uint32_t data_len,
  uint32_t mid,
  uint32_t pid,
  void** ret_data_addr,
  uint32_t* ret_data_len
)
{
  int32_t rc;
  bool_t is_found = FALSE;
  uint32_t consumed_len = 0;
  voice_param_data_t* cur_param;
  uint8_t* cal_data;

  if ( ( ret_data_addr == NULL ) || ( ret_data_len == NULL ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_mid_pid_pair_from_cal_entry(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  /* Initial validation */
  rc = cvd_cal_validate_entry( table_descriptor, data, data_len );
  if ( rc != APR_EOK )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_mid_pid_pair_from_cal_entry(): Invalid table" );
    return rc;
  }

  cal_data = ( uint8_t* )data;
  while ( consumed_len < data_len )
  {
    if ( ( data_len - consumed_len ) < sizeof( voice_param_data_t ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_mid_pid_pair_from_cal_entry(): Improperly formatted cal data, " \
                                            "not enough bytes to form voice_param_data_t" );
      rc = APR_EFAILED;
      break;
    }

    cur_param = ( ( voice_param_data_t* ) ( cal_data + consumed_len ) );

    /* Validate param size */
    rc = cvd_cal_validate_entry( table_descriptor, data,
                                 consumed_len + cur_param->param_size + sizeof(voice_param_data_t) );
    if ( rc != APR_EOK )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_mid_pid_pair_from_cal_entry(): Invalid table" );
      break;
    }

    consumed_len += ( sizeof( voice_param_data_t ) );

    if ( ( cur_param->module_id == mid ) && ( cur_param->param_id == pid ) )
    {
      is_found = TRUE;
      *ret_data_addr = ( void* )cur_param;
      *ret_data_len = cur_param->param_size + sizeof( voice_param_data_t );
      rc = APR_EOK;
      break;
    }

    consumed_len += cur_param->param_size;
  }

  if ( is_found == FALSE )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_mid_pid_pair_from_cal_entry(): No module id/parameter id " \
                                           "0X%08X/0X%08X pair found in calibration entry.",
                                           mid, pid );
    rc = APR_EFAILED;
  }

  return rc;
}

/* Find module id and paramter id pair from common calibration data. */
static int32_t cvp_find_common_mid_pid_pair (
  cvp_session_object_t* session_obj,
  uint32_t mid,
  uint32_t pid,
  void** ret_data_addr,
  uint32_t* ret_data_len
)
{
  int32_t rc;
  uint32_t idx;
  void* data;
  uint32_t data_len;
  cvd_cal_entry_t* cal_entry;
  bool_t is_found = FALSE;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  /* Assumption: matched entries have been found in network calibration procedure. */
  cal_entry = session_obj->common_cal.matching_entries;
  for( idx = 0; idx < session_obj->common_cal.num_matching_entries; ++idx )
  {
    data = cal_entry->start_ptr;
    data_len = cal_entry->size;
    rc = cvp_find_mid_pid_pair_from_cal_entry( &session_obj->common_cal.table_handle->table_descriptor, data, data_len, mid, pid, &data, &data_len );
    if ( rc == APR_EOK )
    {
      /* Found pair, exit. */
      is_found = TRUE;
      *ret_data_addr = data;
      *ret_data_len = data_len;
      rc = APR_EOK;
      break;
    }

    cal_entry++;
  }

  if ( is_found == FALSE )
  {
    rc = APR_EFAILED;
  }

  return rc;
}

/* Send set_param to all attached streams using the rsp_fn provided by the
 * caller for handling the individual set_param response.
 */
static void cvp_set_param_to_streams (
  cvp_session_object_t* session_obj,
  void* data,
  uint32_t data_len,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  vss_icommon_cmd_set_param_v2_t set_param;
  cvp_attached_stream_item_t* stream_item;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  uint32_t idx;
  uint32_t payload_size;
  aprv2_packet_t* packet;
  uint8_t* packet_payload;
  uint32_t packet_payload_left;

  pivot_node = &session_obj->attached_stream_list.dummy;
  for ( idx = 0; idx < session_obj->attached_stream_list.size; ++idx )
  {
    ( void ) apr_list_get_next( &(session_obj->attached_stream_list),
                                pivot_node, &cur_node );

    stream_item = ( cvp_attached_stream_item_t* )cur_node;
    pivot_node = cur_node;

    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;
    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    payload_size = sizeof( vss_icommon_cmd_set_param_v2_t ) + data_len;

    rc = __aprv2_cmd_alloc_ext(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           stream_item->addr, stream_item->port,
           job_obj->header.handle, VSS_ICOMMON_CMD_SET_PARAM_V2,
           payload_size, &packet );
    CVP_PANIC_ON_ERROR( rc );

    packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
    packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    set_param.mem_handle = 0; /* In-band. */
    set_param.mem_address = 0; /* Will be ignored. */
    set_param.mem_size = data_len;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_set_param_to_streams(): data_address: 0x%016X, " \
                                          "data_len: %d", data, data_len );

    ( void ) mmstd_memcpy( packet_payload, packet_payload_left, &set_param, sizeof( set_param ) );
    packet_payload_left -= sizeof( set_param );
    ( void ) mmstd_memcpy( ( packet_payload + sizeof( set_param ) ), packet_payload_left,
                           data, data_len );

    rc = __aprv2_cmd_forward( cvp_apr_handle, packet );
    CVP_COMM_ERROR( rc, stream_item->addr );
  }
}

/* Applies common calibration data to VPM using the rsp_fn provided by the
 * caller for handling the individual set_param response.
 */
static int32_t cvp_calibrate_common (
  cvp_session_object_t* session_obj,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;
  cvd_cal_column_t columns[ CVP_COMMON_NUM_CAL_COLUMNS ];
  cvd_cal_key_t cal_key;
  uint32_t set_param_cnt;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    session_obj->common_cal.num_matching_entries = 0;
    session_obj->common_cal.set_param_rsp_cnt = 0;
    session_obj->common_cal.set_param_failed_rsp_cnt = 0;

    if ( session_obj->common_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_common(): Calibration data is not registered " \
                                           "Not calibrating" );
      rc = APR_EIMMEDIATE;
      break;
    }

    mmstd_memset( columns, 0, sizeof( columns ) );
    columns[ 0 ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    columns[ 0 ].value = session_obj->target_set.system_config.network_id;

    switch ( session_obj->direction )
    {
    case VSS_IVOCPROC_DIRECTION_RX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.rx_pp_sr;
        cal_key.num_columns = 2;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_TX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.tx_pp_sr;
        cal_key.num_columns = 2;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX_TX:
      {
        columns[ 1 ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        columns[ 2 ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        columns[ 1 ].value = session_obj->target_set.system_config.rx_pp_sr;
        columns[ 2 ].value = session_obj->target_set.system_config.tx_pp_sr;
        cal_key.num_columns = 3;
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    cal_key.columns = columns;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_common(): Network 0x%08X, Rx SR %d, Tx SR %d",
                                          columns[0].value,
                                          columns[1].value,
                                          columns[ 2 ].value );

    rc = cvd_cal_query_table(
           session_obj->common_cal.table_handle, &cal_key,
           sizeof( session_obj->common_cal.matching_entries ),
           session_obj->common_cal.matching_entries,
           &session_obj->common_cal.num_matching_entries );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_common(): cvd_cal_query_table failed, rc=0x%08X. " \
                                              "Not calibrating.", rc );
      break;
    }

    if ( session_obj->common_cal.num_matching_entries == 0 )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_common(): Cannot find a matching entry. " \
                                           "Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_common(): Found %d matched entries",
                                          session_obj->common_cal.num_matching_entries );

    { /* Apply calibration data. */
      for ( set_param_cnt = 0; set_param_cnt < session_obj->common_cal.num_matching_entries; ++set_param_cnt )
      {
        rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
        CVP_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

        cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                                session_obj->common_cal.matching_entries[ set_param_cnt ].start_ptr );
        set_param.payload_size = session_obj->common_cal.matching_entries[ set_param_cnt ].size;
        set_param.mem_map_handle = session_obj->common_cal.vpm_mem_handle;

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_common(): payload_address: 0x%016X, " \
                                              "payload_size: %d, memory handle %d",
                                              session_obj->common_cal.matching_entries[set_param_cnt].start_ptr,
                                              set_param.payload_size,
                                              set_param.mem_map_handle );

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvp_vpm_addr, session_obj->vocproc_handle,
               job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               &set_param, sizeof( set_param ) );
        CVP_COMM_ERROR( rc, cvp_vpm_addr );
      }
    }

    break;
  }

  return rc;
}

/* Find WV/FENS cal data and send to all attached streams, using the rsp_fn
 * provided by the caller for handling the individual set_param response.
 * Assumption: Common calibration data has already been applied.
 */
static int32_t cvp_calibrate_wv_fens_from_common_cal (
  cvp_session_object_t* session_obj,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  void* data;
  uint32_t data_len;
  void* data_addr_array[ CVP_NUM_WV_FENS_CAL_FROM_COMMON_CAL_DATA ];
  uint32_t data_len_array[ CVP_NUM_WV_FENS_CAL_FROM_COMMON_CAL_DATA ];
  uint32_t num_found_mid_pid_pair = 0;
  uint32_t idx;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( ( session_obj->common_cal.is_registered == FALSE ) ||
         ( session_obj->common_cal.num_matching_entries == 0 ) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Calibration data is not available" );
      rc = APR_EIMMEDIATE;
      break;
    }

    /* Reset set param counter. */
    session_obj->common_cal.num_set_param_to_stream = 0;
    session_obj->common_cal.set_param_rsp_cnt = 0;
    session_obj->common_cal.set_param_failed_rsp_cnt = 0;

    if ( session_obj->target_set.system_config.rx_pp_sr > 8000 )
    { /* Find param/module ID pairs and pass to attached streams. */
      rc = cvp_find_common_mid_pid_pair(
             session_obj, VOICE_MODULE_WV, VOICE_PARAM_MOD_ENABLE,
             &data, &data_len );
      if ( rc == APR_EOK )
      {
        data_addr_array[ num_found_mid_pid_pair ] = data;
        data_len_array[ num_found_mid_pid_pair ] = data_len;
        ++num_found_mid_pid_pair;
      }
      else
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the " \
                                             "VOICE_MODULE_WV/VOICE_PARAM_MOD_ENABLE pair" );
      }

      rc = cvp_find_common_mid_pid_pair(
             session_obj, VOICE_MODULE_WV, VOICE_PARAM_WV,
             &data, &data_len );
      if ( rc == APR_EOK )
      {
        data_addr_array[ num_found_mid_pid_pair ] = data;
        data_len_array[ num_found_mid_pid_pair ] = data_len;
        ++num_found_mid_pid_pair;
      }
      else
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the \
                                             VOICE_MODULE_WV/VOICE_PARAM_WV pair" );
      }
    }

    rc = cvp_find_common_mid_pid_pair(
           session_obj, VOICE_MODULE_FNS, VOICE_PARAM_MOD_ENABLE,
           &data, &data_len );
    if ( rc == APR_EOK )
    {
      data_addr_array[ num_found_mid_pid_pair ] = data;
      data_len_array[ num_found_mid_pid_pair ] = data_len;
      ++num_found_mid_pid_pair;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the " \
                                           "VOICE_MODULE_FNS/VOICE_PARAM_MOD_ENABLE pair" );
    }

    rc = cvp_find_common_mid_pid_pair(
           session_obj, VOICE_MODULE_FNS, VOICE_PARAM_FNS,
           &data, &data_len );
    if ( rc == APR_EOK )
    {
      data_addr_array[ num_found_mid_pid_pair ] = data;
      data_len_array[ num_found_mid_pid_pair ] = data_len;
      ++num_found_mid_pid_pair;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the \
                                           VOICE_MODULE_FNS/VOICE_PARAM_FNS pair" );
    }

    rc = cvp_find_common_mid_pid_pair(
           session_obj, VOICE_MODULE_FNS_V2, VOICE_PARAM_MOD_ENABLE,
           &data, &data_len );
    if ( rc == APR_EOK )
    {
      data_addr_array[ num_found_mid_pid_pair ] = data;
      data_len_array[ num_found_mid_pid_pair ] = data_len;
      ++num_found_mid_pid_pair;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the " \
                                           "VOICE_MODULE_FNS_V2/VOICE_PARAM_MOD_ENABLE pair" );
    }

    rc = cvp_find_common_mid_pid_pair(
           session_obj, VOICE_MODULE_FNS_V2, VOICE_PARAM_FNS_V3,
           &data, &data_len );
    if ( rc == APR_EOK )
    {
      data_addr_array[ num_found_mid_pid_pair ] = data;
      data_len_array[ num_found_mid_pid_pair ] = data_len;
      ++num_found_mid_pid_pair;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_wv_fens_from_common_cal(): Could not find the \
                                           VOICE_MODULE_FNS_V2/VOICE_PARAM_FNS_V3 pair" );
    }

    if ( ( num_found_mid_pid_pair != 0 ) &&
         ( session_obj->attached_stream_list.size != 0 ) )
    { /* Set param to CVS. */
      session_obj->common_cal.num_set_param_to_stream =
        ( num_found_mid_pid_pair * session_obj->attached_stream_list.size );
      for ( idx = 0; idx < num_found_mid_pid_pair; ++idx )
      {
        cvp_set_param_to_streams( session_obj,
                                  data_addr_array[ idx ],
                                  data_len_array[ idx ],
                                  rsp_fn );
      }
    }
    else
    { /* No pairs found or no stream attached. */
      rc = APR_EIMMEDIATE;
    }

    break;
  }

  return rc;
}

/* Applies volume calibration data to VPM , using the rsp_fn provided by the
 * caller for handling the individual set_param response.
 */
static int32_t cvp_calibrate_volume (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->volume_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_volume(): Calibration data is not registered. \
                                           Not calibrating" );
      rc = APR_EIMMEDIATE;
      break;
    }

    if ( session_obj->target_set.client_num_vol_steps ==
         session_obj->volume_cal.num_vol_indices )
    {
      /* Linear volume mapping. Push volume calibration data and volume level
       * in Q28 format to VPM.
       */
      rc = cvp_calibrate_volume_linear_mapping(
             session_obj, vol_ramp_duration, rsp_fn );
    }
    else
    if ( session_obj->target_set.client_num_vol_steps >
         session_obj->volume_cal.num_vol_indices )
    {
      if ( session_obj->volume_cal.is_v1_format == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume(): Interpolation is not supported " \
                                              "for format V1" );
        rc = APR_EBADPARAM;
        break;
      }

      /* Interpolation is required. Push volume calibration data and volume
       * level in Q28 format to VPM.
       */
      rc = cvp_calibrate_volume_interpolation(
             session_obj, vol_ramp_duration, rsp_fn );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume(): Unexpected volume steps, client " \
                                              "volume steps %d, cal table volume steps %d",
                                              session_obj->target_set.client_num_vol_steps,
                                              session_obj->volume_cal.num_vol_indices );

      rc = APR_EUNEXPECTED;
    }

    break;
  }

  if ( rc == APR_EOK )
  { /* Commands have been sent to FW. Update the active set. */
    session_obj->active_set.vol_step = session_obj->target_set.vol_step;
    session_obj->active_set.client_num_vol_steps = session_obj->target_set.client_num_vol_steps;
  }

  return rc;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CALIBRATION HELPER FUNCTIONS RELATED TO STATIC AND DYNAMIC CALIBRATION  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void cvp_volume_level_query_cb_fn (
  cvd_cal_param_t* cal_param,
  void* cb_data
)
{
  cvp_vol_level_info_t* vol_level_info;

  for ( ;; )
  {
    if ( ( cal_param == NULL ) || ( cb_data == NULL ) )
    {
      break;
    }

    vol_level_info = ( ( cvp_vol_level_info_t* ) cb_data );

    if ( ( ( cal_param->module_id == VSS_MODULE_RX_VOLUME ) &&
           ( cal_param->param_id == VSS_PARAM_VOLUME ) ) ||
         ( ( cal_param->module_id == VOICE_MODULE_RX_VOL ) &&
           ( cal_param->param_id == VOICE_PARAM_VOL ) ) )
    {
      vol_level_info->is_vol_level_found = TRUE;
      vol_level_info->vol_level = *( ( uint32_t* )( cal_param->param_data ) );
    }

    break;
  }
}

/* Find volume level corresponding to a volume index from dynamic cal table. */
static int32_t cvp_find_vol_level_from_dynamic_cal (
  cvp_session_object_t* session_obj,
  uint32_t vol_index,
  uint32_t* ret_vol_level
)
{
  int32_t rc;
  cvd_cal_key_t cal_key;
  cvd_cal_column_t* query_key_columns;
  uint32_t column_index = 0;
  uint32_t cal_query_handle;
  cvp_vol_level_info_t vol_level_info;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->dynamic_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_find_vol_level_from_dynamic_cal(): " \
                                           "Dynamic calibration data is not registered." );
      rc = APR_EIMMEDIATE;
      break;
    }

    query_key_columns = session_obj->dynamic_cal.query_key_columns;

    mmstd_memset( query_key_columns, 0, sizeof( session_obj->dynamic_cal.query_key_columns ) );
    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.network_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOLUME_INDEX;
    query_key_columns[ column_index ].value = vol_index;
    column_index += 1;

    switch ( session_obj->direction )
    {
    case VSS_IVOCPROC_DIRECTION_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_MEDIA_ID;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.media_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_FEATURE;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.feature;

    cal_key.columns = query_key_columns;
    cal_key.num_columns = ( column_index + 1 );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_find_vol_level_from_dynamic_cal(): Network 0x%08X, Tx PP SR %d, Rx PP SR %d",
                                          session_obj->target_set.system_config.network_id,
                                          session_obj->target_set.system_config.tx_pp_sr,
                                          session_obj->target_set.system_config.rx_pp_sr );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_find_vol_level_from_dynamic_cal(): Volume index %d, Tx voc op mode 0x%08X, Rx voc op mode 0x%08X",
                                          vol_index,
                                          session_obj->target_set.system_config.tx_voc_op_mode,
                                          session_obj->target_set.system_config.rx_voc_op_mode );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_find_vol_level_from_dynamic_cal(): Media ID 0x%08X, Feature 0x%08X.",
                                          session_obj->target_set.system_config.media_id,
                                          session_obj->target_set.system_config.feature );

    rc = cvd_cal_query_init(
           session_obj->dynamic_cal.table_handle, &cal_key,
           session_obj->dynamic_cal.matching_entries,
           sizeof( session_obj->dynamic_cal.matching_entries ),
           &cal_query_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_vol_level_from_dynamic_cal(): cvd_cal_query_table_init failed, " \
                                              "rc=0x%08X.",
                                              rc );
      break;
    }

    vol_level_info.is_vol_level_found = FALSE;

    /* Determine the volume level from matching calibration entries. */
    rc = cvd_cal_query(
           cal_query_handle,
           cvp_volume_level_query_cb_fn,
           ( ( void * ) &vol_level_info ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_find_vol_level_from_dynamic_cal(): cvd_cal_query failed with rc = 0x%08X",
                                            rc );

      ( void ) cvd_cal_query_deinit( cal_query_handle );

      break;
    }

    if ( vol_level_info.is_vol_level_found == TRUE )
    {
      *ret_vol_level = vol_level_info.vol_level;
    }

    /* Log cal data. */
    {
      log_info_data.table_handle = session_obj->dynamic_cal.table_handle;
      log_info_data.cal_query_handle = cal_query_handle;
      log_info_data.data_seq_num = 0;

      log_info.instance =  ( ( session_obj->attached_mvm_handle << 16 ) |
                             ( session_obj->header.handle ) );
      log_info.call_num = session_obj->target_set.system_config.call_num;
      log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
      log_info.data_container_header_size = sizeof( log_info_data );
      log_info.data_container_header = &log_info_data;
      log_info.payload_buf = NULL;
      log_info.payload_size = 0;

      ( void ) cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_DYNAMIC_OUTPUT,
                              ( void* )&log_info, sizeof( log_info ) );
    }

    ( void ) cvd_cal_query_deinit( cal_query_handle );

    break;
  }

  return rc;
}

/* Find the volume index that indexes to the closest volume level
   (to the target volume level) in dynamic calibration table.
   Caller: cvp_calibrate_dynamic_with_volume_interpolation(). */
static int32_t cvp_find_closest_vol_index_from_dynamic_cal (
  cvp_session_object_t* session_obj,
  uint32_t in_vol_level,
  uint32_t* ret_vol_index
)
{
  int32_t rc;
  uint32_t vol_index;
  uint32_t vol_level = 0xFFFFFFFF;
  uint32_t distance = 0XFFFFFFFF;
  uint32_t cur_distance;
  bool_t is_found = FALSE;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( ret_vol_index == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_find_closest_vol_index_from_dynamic_cal(): Invalid input parameters" );
    return APR_EBADPARAM;
  }

  /* Note: Double closed region ([min, max]). */
  for( vol_index = session_obj->dynamic_cal.min_vol_index; vol_index <= session_obj->dynamic_cal.max_vol_index; ++vol_index )
  {
    rc = cvp_find_vol_level_from_dynamic_cal( session_obj, vol_index, &vol_level );
    if ( rc != APR_EOK )
    {
      /* Not found, keep searching. */
      continue;
    }

    cur_distance = cvp_abs( in_vol_level - vol_level );
    if ( distance > cur_distance )
    {
      /* Current distance is decreasing, keep searching. */
      distance = cur_distance;
    }
    else
    {
      /* Current distance is increasing, exit. */
      /* Assumption: volume level increases as volume index increases. */
      is_found = TRUE;
      *ret_vol_index = ( vol_index - 1 ); /* Assumption: vol index is incremented by 1. */
      rc = APR_EOK;
      break;
    }
  }

  if ( is_found == FALSE )
  {
    /* No volume level has been found. */
    rc = APR_EFAILED;
  }

  return rc;
}

/* Applies dynamic calibration data to VPM with linear volume mapping use case,
 * using the rsp_fn provided by the caller for handling the individual
 * set_param response.
 */
static int32_t cvp_calibrate_dynamic_with_linear_volume_mapping (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_cmd_set_param_v3_t set_param;
  cvd_cal_key_t cal_key;
  cvd_cal_column_t* query_key_columns;
  uint32_t column_index = 0;
  cvp_vol_set_param_t set_vol_param;
  cvp_vol_level_info_t vol_level_info;
  uint32_t vol_level_linearQ28 = 0;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->dynamic_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_dynamic_with_linear_volume_mapping(): " \
                                           "Dynamic calibration data is not registered. Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    session_obj->dynamic_cal.num_set_param_issued = 0;
    session_obj->dynamic_cal.set_param_rsp_cnt = 0;
    session_obj->dynamic_cal.set_param_failed_rsp_cnt = 0;

    query_key_columns = session_obj->dynamic_cal.query_key_columns;

    mmstd_memset( query_key_columns, 0, sizeof( session_obj->dynamic_cal.query_key_columns ) );
    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.network_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOLUME_INDEX;
    query_key_columns[ column_index ].value = session_obj->target_set.vol_step;
    column_index += 1;

    switch ( session_obj->direction )
    {
    case VSS_IVOCPROC_DIRECTION_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_MEDIA_ID;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.media_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_FEATURE;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.feature;

    cal_key.columns = query_key_columns;
    cal_key.num_columns = ( column_index + 1 );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_linear_volume_mapping(): Network 0x%08X, " \
                                          "Tx PP SR %d, Rx PP SR %d",
                                          session_obj->target_set.system_config.network_id,
                                          session_obj->target_set.system_config.tx_pp_sr,
                                          session_obj->target_set.system_config.rx_pp_sr );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_linear_volume_mapping(): Volume index %d, " \
                                          "Tx voc op mode 0x%08X, Rx voc op mode 0x%08X",
                                          session_obj->target_set.vol_step,
                                          session_obj->target_set.system_config.tx_voc_op_mode,
                                          session_obj->target_set.system_config.rx_voc_op_mode );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_linear_volume_mapping(): Media ID 0x%08X, Feature 0x%08X.",
                                          session_obj->target_set.system_config.media_id,
                                          session_obj->target_set.system_config.feature );

    rc = cvd_cal_query_init(
           session_obj->dynamic_cal.table_handle, &cal_key,
           session_obj->dynamic_cal.matching_entries,
           sizeof( session_obj->dynamic_cal.matching_entries ),
           &session_obj->dynamic_cal.query_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_with_linear_volume_mapping(): cvd_cal_query_table_init failed, " \
                                              "rc=0x%08X. Not calibrating.",
                                              rc );
      break;
    }

    vol_level_info.is_vol_level_found = FALSE;

    /* Determine the volume level from matching calibration entries. */
    rc = cvd_cal_query(
           session_obj->dynamic_cal.query_handle,
           cvp_volume_level_query_cb_fn,
           ( ( void * ) &vol_level_info ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_dynamic_with_linear_volume_mapping(): cvd_cal_query failed with rc = 0x%08X",
                                            rc );

      cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );
      break;
    }

    session_obj->dynamic_cal.num_set_param_issued += 1;

    if ( vol_level_info.is_vol_level_found == TRUE )
    {
      /* Calculate volume level in linear Q28 format and set volume level to FW. */
      if ( session_obj->dynamic_cal.is_v1_vol_format == TRUE )
      {
        /* Convert from linearQ13 (32 bit) to linearQ28. */
        vol_level_linearQ28 = ( vol_level_info.vol_level << 15 );
      }
      else
      {
        /* Convert from mb to linearQ28. */
        vol_level_linearQ28 = cvp_convert_mB_to_linearQ28( vol_level_info.vol_level );
      }

      session_obj->dynamic_cal.num_set_param_issued += 1;
    }

    {
      /* Send the calibration query handle to VPM for retrieving and applying
       * all matching entries.
       */
      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      set_param.cal_handle = session_obj->dynamic_cal.query_handle;

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V3,
             &set_param, sizeof( set_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );

      if ( vol_level_info.is_vol_level_found == TRUE )
      {
        set_vol_param.payload_address_lsw = 0;
        set_vol_param.payload_address_msw = 0;
        set_vol_param.payload_size = sizeof( cvp_vol_param_t );
        set_vol_param.mem_map_handle = 0;
        set_vol_param.vol_param.module_id = VOICE_MODULE_RX_VOL;
        set_vol_param.vol_param.param_id = VOICE_PARAM_SOFT_VOL;
        set_vol_param.vol_param.param_size = sizeof( cvp_vol_param_data_t );
        set_vol_param.vol_param.reserved = 0;
        set_vol_param.vol_param.param_data.volume = vol_level_linearQ28;
        set_vol_param.vol_param.param_data.ramp_duration_ms = ( ( uint16_t ) vol_ramp_duration );
        set_vol_param.vol_param.param_data.reserved = 0;

        rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
        CVP_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_dynamic_with_linear_volume_mapping(): vol_level = %d, " \
                                              "vol_level_linearQ28 = %d, vol_ramp_duration = %d",
                                              vol_level_info.vol_level,
                                              vol_level_linearQ28,
                                              vol_ramp_duration );

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvp_vpm_addr, session_obj->vocproc_handle,
               job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               &set_vol_param, sizeof( set_vol_param ) );
        CVP_COMM_ERROR( rc, cvp_vpm_addr );
      }
    }

    /* Log cal data. */
    {
      log_info_data.table_handle = session_obj->dynamic_cal.table_handle;
      log_info_data.cal_query_handle = session_obj->dynamic_cal.query_handle;
      log_info_data.data_seq_num = 0;

      log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                            ( session_obj->header.handle ) );
      log_info.call_num = session_obj->target_set.system_config.call_num;
      log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
      log_info.data_container_header_size = sizeof( log_info_data );
      log_info.data_container_header = &log_info_data;
      log_info.payload_size = 0;
      log_info.payload_buf = NULL;

      ( void ) cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_DYNAMIC_OUTPUT,
                              ( void* )&log_info, sizeof( log_info ) );
  }

    break;
  }

  return rc;
}

/* Applies dynamic calibration data to VPM for non-linear volume mapping use
 * case (interpolation is required), using the rsp_fn provided by the caller
 * for handling the individual set_param response.
 */
static int32_t cvp_calibrate_dynamic_with_volume_interpolation (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_cmd_set_param_v3_t set_param;
  cvd_cal_key_t cal_key;
  cvd_cal_column_t* query_key_columns;
  uint32_t column_index = 0;
  cvp_vol_set_param_t set_vol_param;
  uint32_t vol_level_linearQ28;
  uint32_t min_vol_level = 0;
  uint32_t max_vol_level = 0;
  uint32_t interpolated_vol_level;
  uint32_t vol_index;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  for ( ;; )
  {
    session_obj->dynamic_cal.num_set_param_issued = 0;
    session_obj->dynamic_cal.set_param_rsp_cnt = 0;
    session_obj->dynamic_cal.set_param_failed_rsp_cnt = 0;

    /* Find min and max volume level for a specific network ID. */
    /* Assumption: volume level increases as volume index increase. */
    rc = cvp_find_vol_level_from_dynamic_cal(
           session_obj, session_obj->dynamic_cal.min_vol_index, &min_vol_level );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_with_volume_interpolation(): Cannot find min volume " \
                                            "level. Not calibrating" );
      break;
    }

    rc = cvp_find_vol_level_from_dynamic_cal(
           session_obj, session_obj->dynamic_cal.max_vol_index, &max_vol_level );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_with_volume_interpolation(): Cannot find max volume level. " \
                                            "Not calibrating" );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_dynamic_with_volume_interpolation(): min_vol_level = %d, max_vol_level = %d",
                                          min_vol_level, max_vol_level );

    /* Interpolate target volume step into target volume level. */
    /* Input: client specified volume step, number of total volume steps, min and max volume level. */
    /* Output: interpolated volume level in mB. */
    interpolated_vol_level = cvp_convert_volume_step_to_mB(
                                min_vol_level,
                                max_vol_level,
                                session_obj->target_set.client_num_vol_steps,
                                session_obj->target_set.vol_step );

    /* Find the closest volume level in table, as well as the corresponding volume index. */
    /* Input: interpolated volume level in mB. */
    /* Output: closest volume level in table, and the corresponding volume index. */
    /* Assumption: volume level increases as volume index increase. */
    rc = cvp_find_closest_vol_index_from_dynamic_cal( session_obj, interpolated_vol_level, &vol_index );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_with_volume_interpolation(): Cannot find closest volume level to " \
                                              "target volume level %d. Not calibrating",
                                              interpolated_vol_level );
      break;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_dynamic_with_volume_interpolation(): interpolated_vol_level = %d, " \
                                            "vol_index = %d",
                                            interpolated_vol_level,
                                            vol_index );
    }

    { /* Find the calibration data of the closest volume level. */
      query_key_columns = session_obj->dynamic_cal.query_key_columns;

      mmstd_memset( query_key_columns, 0, sizeof( session_obj->dynamic_cal.query_key_columns ) );
      query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
      query_key_columns[ column_index ].value = session_obj->target_set.system_config.network_id;
      column_index += 1;

      query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOLUME_INDEX;
      query_key_columns[ column_index ].value = vol_index;
      column_index += 1;

      switch ( session_obj->direction )
      {
      case VSS_IVOCPROC_DIRECTION_TX:
        {
          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
          column_index += 1;

          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
          column_index += 1;
        }
        break;

      case VSS_IVOCPROC_DIRECTION_RX:
        {
          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
          column_index += 1;

          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
          column_index += 1;
        }
        break;

      case VSS_IVOCPROC_DIRECTION_RX_TX:
        {
          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
          column_index += 1;

          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
          column_index += 1;

          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
          column_index += 1;

          query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
          query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
          column_index += 1;
        }
        break;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }

      query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_MEDIA_ID;
      query_key_columns[ column_index ].value = session_obj->target_set.system_config.media_id;
      column_index += 1;

      query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_FEATURE;
      query_key_columns[ column_index ].value = session_obj->target_set.system_config.feature;

      cal_key.columns = query_key_columns;
      cal_key.num_columns = ( column_index + 1 );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_volume_interpolation(): Network 0x%08X, " \
                                            "Tx PP SR %d, Rx PP SR %d",
                                            session_obj->target_set.system_config.network_id,
                                            session_obj->target_set.system_config.tx_pp_sr,
                                            session_obj->target_set.system_config.rx_pp_sr );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_volume_interpolation(): Volume index %d, " \
                                            "Tx voc op mode 0x%08X, Rx voc op mode 0x%08X",
                                            vol_index,
                                            session_obj->target_set.system_config.tx_voc_op_mode,
                                            session_obj->target_set.system_config.rx_voc_op_mode );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Dynamic configured with the following settings. cvp_calibrate_dynamic_with_volume_interpolation(): Media ID 0x%08X, Feature 0x%08X.",
                                            session_obj->target_set.system_config.media_id,
                                            session_obj->target_set.system_config.feature );


      rc = cvd_cal_query_init(
             session_obj->dynamic_cal.table_handle, &cal_key,
             session_obj->dynamic_cal.matching_entries,
             sizeof( session_obj->dynamic_cal.matching_entries ),
             &session_obj->dynamic_cal.query_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_with_volume_interpolation(): cvd_cal_query_table_init failed, " \
                                                "rc=0x%08X. Not calibrating.",
                                                rc );
        break;
      }

      session_obj->dynamic_cal.num_set_param_issued = 2;
    }

    {
      /* Send the calibration query handle to VPM for retrieving and applying
       * all matching entries.
       */
      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      set_param.cal_handle = session_obj->dynamic_cal.query_handle;

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V3,
             &set_param, sizeof( set_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    { /* Apply volume level. */
      /* Convert interpolated volume level in mB to Q28 format. */
      vol_level_linearQ28 = cvp_convert_mB_to_linearQ28( interpolated_vol_level );
      set_vol_param.payload_address_lsw = 0;
      set_vol_param.payload_address_msw = 0;
      set_vol_param.payload_size = sizeof( cvp_vol_param_t );
      set_vol_param.mem_map_handle = 0;
      set_vol_param.vol_param.module_id = VOICE_MODULE_RX_VOL;
      set_vol_param.vol_param.param_id = VOICE_PARAM_SOFT_VOL;
      set_vol_param.vol_param.param_size = sizeof( cvp_vol_param_data_t );
      set_vol_param.vol_param.reserved = 0;
      set_vol_param.vol_param.param_data.volume = vol_level_linearQ28;
      set_vol_param.vol_param.param_data.ramp_duration_ms = ( ( uint16_t ) vol_ramp_duration );
      set_vol_param.vol_param.param_data.reserved = 0;

      rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
      CVP_PANIC_ON_ERROR( rc );
      job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_calibrate_dynamic_with_volume_interpolation(): interpolated_vol_level = %d, " \
                                            "vol_level_linearQ28 = %d, vol_ramp_duration = %d",
                                            interpolated_vol_level,
                                            vol_level_linearQ28,
                                            vol_ramp_duration );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
             &set_vol_param, sizeof( set_vol_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    /* Log cal data. */
    {
      log_info_data.table_handle = session_obj->dynamic_cal.table_handle;
      log_info_data.cal_query_handle = session_obj->dynamic_cal.query_handle;
      log_info_data.data_seq_num = 0;

      log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                            ( session_obj->header.handle ) );
      log_info.call_num = session_obj->target_set.system_config.call_num;
      log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
      log_info.data_container_header_size = sizeof( log_info_data );
      log_info.data_container_header = &log_info_data;
      log_info.payload_size = 0;
      log_info.payload_buf = NULL;

      ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_DYNAMIC_OUTPUT,
                                ( void* )&log_info, sizeof( log_info ) );
    }

    break;
  }

  return rc;
}

/* Send set_param to all attached streams using the rsp_fn provided by the
 * caller for handling the individual set_param response.
 */
static void cvp_set_param_to_streams_v2 (
  cvp_session_object_t* session_obj,
  cvd_cal_param_t* cal_param,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_attached_stream_item_t* stream_item;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  uint32_t idx;
  uint32_t payload_size;
  aprv2_packet_t* packet;
  uint8_t* packet_payload;
  uint32_t packet_payload_left;
  typedef struct set_param_header_t
  {
    vss_icommon_cmd_set_param_v2_t cmd_header;
    vss_icommon_param_data_t param_data_header;
  } set_param_header_t;
  set_param_header_t set_param_header;

  pivot_node = &session_obj->attached_stream_list.dummy;
  for ( idx = 0; idx < session_obj->attached_stream_list.size; ++idx )
  {
    ( void ) apr_list_get_next( &( session_obj->attached_stream_list ),
                                pivot_node, &cur_node );

    stream_item = ( ( cvp_attached_stream_item_t* ) cur_node );
    pivot_node = cur_node;

    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = rsp_fn;
    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    payload_size = ( sizeof( vss_icommon_cmd_set_param_v2_t ) +
                     sizeof( vss_icommon_param_data_t ) +
                     cal_param->param_data_size );

    rc = __aprv2_cmd_alloc_ext(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           stream_item->addr, stream_item->port,
           job_obj->header.handle, VSS_ICOMMON_CMD_SET_PARAM_V2,
           payload_size, &packet );
    CVP_PANIC_ON_ERROR( rc );

    packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, packet );
    packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    set_param_header.cmd_header.mem_handle = 0; /* In-band. */
    set_param_header.cmd_header.mem_address = 0; /* Will be ignored. */
    set_param_header.cmd_header.mem_size = ( sizeof( vss_icommon_param_data_t ) +
                                             cal_param->param_data_size );

    set_param_header.param_data_header.module_id = cal_param->module_id;
    set_param_header.param_data_header.param_id = cal_param->param_id;
    set_param_header.param_data_header.param_size = ( ( uint16_t ) cal_param->param_data_size );
    set_param_header.param_data_header.reserved = 0;

    ( void ) mmstd_memcpy( packet_payload, packet_payload_left,
                           &set_param_header, sizeof( set_param_header ) );
    packet_payload_left -= sizeof( set_param_header );
    ( void ) mmstd_memcpy( ( packet_payload + sizeof( set_param_header ) ),
                           packet_payload_left,
                           cal_param->param_data, cal_param->param_data_size );

    rc = __aprv2_cmd_forward( cvp_apr_handle, packet );
    CVP_COMM_ERROR( rc, stream_item->addr );
  }
}

static int32_t cvp_calibrate_static (
  cvp_session_object_t* session_obj,
  cvp_simple_job_object_t* job_obj
)
{
  int32_t rc;
  voice_cmd_set_param_v3_t set_param;
  cvd_cal_key_t cal_key;
  cvd_cal_column_t* query_key_columns;
  uint32_t column_index = 0;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_cal_data_header_t log_info_data;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->static_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_static(): Static calibration data is not registered. " \
                                           "Not calibrating." );
      rc = APR_EIMMEDIATE;
      break;
    }

    query_key_columns = session_obj->static_cal.query_key_columns;

    mmstd_memset( query_key_columns, 0, sizeof( session_obj->static_cal.query_key_columns ) );
    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_NETWORK;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.network_id;
    column_index += 1;

    switch ( session_obj->direction )
    {
    case VSS_IVOCPROC_DIRECTION_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    case VSS_IVOCPROC_DIRECTION_RX_TX:
      {
        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_pp_sr;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.tx_voc_op_mode;
        column_index += 1;

        query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE;
        query_key_columns[ column_index ].value = session_obj->target_set.system_config.rx_voc_op_mode;
        column_index += 1;
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_MEDIA_ID;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.media_id;
    column_index += 1;

    query_key_columns[ column_index ].id = VSS_ICOMMON_CAL_COLUMN_FEATURE;
    query_key_columns[ column_index ].value = session_obj->target_set.system_config.feature;

    cal_key.columns = query_key_columns;
    cal_key.num_columns = ( column_index + 1 );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Static configured with the following settings. cvp_calibrate_static(): Network 0x%08X, Tx PP SR %d, Rx PP SR %d",
                                          session_obj->target_set.system_config.network_id,
                                          session_obj->target_set.system_config.tx_pp_sr,
                                          session_obj->target_set.system_config.rx_pp_sr );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Static configured with the following settings. cvp_calibrate_static(): Tx voc op mode 0x%08X, Rx voc op mode 0x%08X, Media ID 0x%08X",
                                          session_obj->target_set.system_config.tx_voc_op_mode,
                                          session_obj->target_set.system_config.rx_voc_op_mode,
                                          session_obj->target_set.system_config.media_id );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: CVP Static configured with the following settings. cvp_calibrate_static(): Feature 0x%08X.",
                                          session_obj->target_set.system_config.feature );

    rc = cvd_cal_query_init(
           session_obj->static_cal.table_handle, &cal_key,
           session_obj->static_cal.matching_entries,
           sizeof( session_obj->static_cal.matching_entries ),
           &session_obj->static_cal.query_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_static(): cvd_cal_query_table_init failed, " \
                                              "rc=0x%08X. Not calibrating.",
                                              rc );
      break;
    }

    {
      /* Send the calibration query handle to VPM for walking through and
       * applying all the calibration parameters.
       */
      set_param.cal_handle = session_obj->static_cal.query_handle;

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VOICE_CMD_SET_PARAM_V3,
             &set_param, sizeof( set_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }

    /* Log cal data. */
    {
      log_info_data.table_handle = session_obj->static_cal.table_handle;
      log_info_data.cal_query_handle = session_obj->static_cal.query_handle;
      log_info_data.data_seq_num = 0;

      log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                            ( session_obj->header.handle ) );
      log_info.call_num = session_obj->target_set.system_config.call_num;
      log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_CAL_OUTPUT;
      log_info.data_container_header_size = sizeof( log_info_data );
      log_info.data_container_header = &log_info_data;
      log_info.payload_size = 0;
      log_info.payload_buf = NULL;

      ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_STATIC_OUTPUT,
                                ( void* )&log_info, sizeof( log_info ) );
    }

    break;
  }

  return rc;
}

/* Applies dynamic calibration data to VPM, using the rsp_fn provided by the
 * caller for handling the individual set_param response. The dynamic
 * calibration data contains the volume calibration.
 */
static int32_t cvp_calibrate_dynamic (
  cvp_session_object_t* session_obj,
  uint32_t vol_ramp_duration,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  for ( ;; )
  {
    if ( session_obj->dynamic_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_calibrate_dynamic(): Calibration data is not registered. " \
                                           "Not calibrating" );
      rc = APR_EIMMEDIATE;
      break;
    }

    if ( session_obj->target_set.client_num_vol_steps ==
         session_obj->dynamic_cal.num_vol_indices )
    {
      /* Linear volume mapping. Push dynamic calibration data and volume level
       * in Q28 format to VPM.
       */
      rc = cvp_calibrate_dynamic_with_linear_volume_mapping(
             session_obj, vol_ramp_duration, rsp_fn );
    }
    else
    if ( session_obj->target_set.client_num_vol_steps >
         session_obj->dynamic_cal.num_vol_indices )
    {
      if ( session_obj->dynamic_cal.is_v1_vol_format == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic(): Interpolation is not supported " \
                                              "for format V1" );
        rc = APR_EBADPARAM;
        break;
      }

      /* Interpolation is required. Push dynamic calibration data and volume
       * level in Q28 format to VPM.
       */
      rc = cvp_calibrate_dynamic_with_volume_interpolation(
             session_obj, vol_ramp_duration, rsp_fn );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic(): Unexpected volume steps, client " \
                                              "volume steps %d, cal table volume steps %d",
                                              session_obj->target_set.client_num_vol_steps,
                                              session_obj->volume_cal.num_vol_indices );

      rc = APR_EUNEXPECTED;
    }

    break;
  }

  if ( rc == APR_EOK )
  { /* Commands have been sent to FW. Update the active set. */
    session_obj->active_set.vol_step = session_obj->target_set.vol_step;
    session_obj->active_set.client_num_vol_steps = session_obj->target_set.client_num_vol_steps;
  }

  return rc;
}

void cvp_stream_modules_query_cb_fn (
  cvd_cal_param_t* cal_param,
  void* cb_data
)
{
  cvp_session_object_t* session_obj;

  for ( ;; )
  {
    if ( ( cal_param == NULL ) || ( cb_data == NULL ) )
    {
      break;
    }

    session_obj = ( ( cvp_session_object_t* ) cb_data );

    switch ( cal_param->module_id )
    {
      case VOICE_MODULE_WV:
        {
          if ( session_obj->target_set.system_config.rx_pp_sr > 8000 )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_stream_modules_query_cb_fn(): Found WV MID, PID = 0x%08X, data_ptr = 0x%08X",
                                                  cal_param->param_id,
                                                  cal_param->param_data );

            /* Note that WV enable is controlled by device config table rather than
             * by the calibration data. It is not expected that the calibration data
             * contains the WV enable parameter and we do not parse for it.
             */
            if ( cal_param->param_id == VOICE_PARAM_WV )
            {
              session_obj->stream_cal_info.is_wv_cal_param_found = TRUE;
              session_obj->stream_cal_info.wv_cal_param = *cal_param;
            }
          }
        }
        break;

      case VOICE_MODULE_WV_V2:
        {
          if ( session_obj->target_set.system_config.rx_pp_sr > 8000 )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_stream_modules_query_cb_fn(): Found WV V2 MID, PID = 0x%08X, data_ptr = 0x%08X",
                                                  cal_param->param_id,
                                                  cal_param->param_data );

            /* Note that WV V2 enable is controlled by device config table rather
             * than by the calibration data. It is not expected that the calibration
             * data contains the WV enable parameter and we do not parse for it.
             */
            if ( cal_param->param_id == VOICE_PARAM_WV_V2 )
            {
              session_obj->stream_cal_info.is_wv_v2_cal_param_found = TRUE;
              session_obj->stream_cal_info.wv_v2_cal_param = *cal_param;
            }
          }
        }
        break;

      case VOICE_MODULE_FNS:
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_stream_modules_query_cb_fn(): Found FNS MID, PID = 0x%08X, data_ptr = 0x%08X",
                                                cal_param->param_id,
                                                cal_param->param_data );
          switch ( cal_param->param_id )
          {
            case VOICE_PARAM_MOD_ENABLE:
              {
                session_obj->stream_cal_info.is_fens_enable_param_found = TRUE;
                session_obj->stream_cal_info.fens_enable_param = *cal_param;
              }
              break;

            case VOICE_PARAM_FNS:
              {
                session_obj->stream_cal_info.is_fens_cal_param_found = TRUE;
                session_obj->stream_cal_info.fens_cal_param = *cal_param;
              }
              break;

            case VOICE_PARAM_FNS_V2:
              {
                session_obj->stream_cal_info.is_fens_cal_param_v2_found = TRUE;
                session_obj->stream_cal_info.fens_cal_param_v2 = *cal_param;
              }
              break;

            default:
              break;
          }
        }
        break;

      case VOICE_MODULE_FNS_V2:
       {
         MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "cvp_stream_modules_query_cb_fn(): Found FNS V2 MID, PID=0x%08X, "
                "data_ptr=0x%08X", cal_param->param_id, cal_param->param_data );
         
         switch ( cal_param->param_id )
         {
           case VOICE_PARAM_MOD_ENABLE:
            {
                session_obj->stream_cal_info.is_fens_v2_enable_param_found = TRUE;
                session_obj->stream_cal_info.fens_v2_enable_param = *cal_param;
            }
            break;

           case VOICE_PARAM_FNS_V3:
            {
                session_obj->stream_cal_info.is_fens_v2_cal_param_v3_found = TRUE;
                session_obj->stream_cal_info.fens_v2_cal_param_v3 = *cal_param;
            }
            break;

           default:
             break;
         } 
       }
       break;

      case VOICE_MODULE_RX_GAIN:
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_stream_modules_query_cb_fn(): Found Rx Gain MID, PID = 0x%08X, data_ptr = 0x%08X",
                                                cal_param->param_id,
                                                cal_param->param_data );

          if ( cal_param->param_id == VOICE_PARAM_GAIN )
          {
            session_obj->stream_cal_info.is_rx_gain_cal_param_found = TRUE;
            session_obj->stream_cal_info.rx_gain_cal_param = *cal_param;
          }
        }
        break;

      default:
        break;
    }
    break;
  }
}

static int32_t cvp_find_stream_modules_from_matched_entries (
  cvp_session_object_t* session_obj,
  uint32_t cal_query_handle
)
{
  int32_t rc;

  rc = cvd_cal_query(
         cal_query_handle,
         cvp_stream_modules_query_cb_fn,
         ( ( void * ) session_obj ) );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_find_stream_modules_from_matched_entries(): cvd_cal_query failed with rc = 0x%08X",
                                          rc );
  }

  return rc;
}

static int32_t cvp_clear_cached_stream_cal (
  cvp_session_object_t* session_obj
)
{
  session_obj->stream_cal_info.is_wv_cal_param_found = FALSE;
  session_obj->stream_cal_info.is_wv_v2_cal_param_found = FALSE;
  session_obj->stream_cal_info.is_fens_enable_param_found = FALSE;
  session_obj->stream_cal_info.is_fens_cal_param_found = FALSE;
  session_obj->stream_cal_info.is_fens_cal_param_v2_found = FALSE;
  session_obj->stream_cal_info.is_fens_v2_enable_param_found = FALSE;
  session_obj->stream_cal_info.is_fens_v2_cal_param_v3_found = FALSE;
  session_obj->stream_cal_info.is_rx_gain_cal_param_found = FALSE;

  return APR_EOK;
}

static int32_t cvp_calibrate_stream_modules (
  cvp_session_object_t* session_obj,
  cvp_event_handler_fn_t rsp_fn
)
{
  int32_t rc = APR_EOK;
  uint8_t num_mid_pid_to_set = 0;

  /* Reset the set param counter. */
  session_obj->stream_cal_info.num_set_param_issued = 0;
  session_obj->stream_cal_info.set_param_rsp_cnt = 0;
  session_obj->stream_cal_info.set_param_failed_rsp_cnt = 0;

  if ( session_obj->stream_cal_info.is_wv_cal_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_wv_v2_cal_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_fens_enable_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_fens_cal_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_fens_cal_param_v2_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_fens_v2_enable_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_fens_v2_cal_param_v3_found )
  {
    num_mid_pid_to_set += 1;
  }

  if ( session_obj->stream_cal_info.is_rx_gain_cal_param_found )
  {
    num_mid_pid_to_set += 1;
  }

  session_obj->stream_cal_info.num_set_param_issued =
    ( num_mid_pid_to_set * session_obj->attached_stream_list.size );

  if ( session_obj->stream_cal_info.is_wv_cal_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.wv_cal_param,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_wv_v2_cal_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.wv_v2_cal_param,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_fens_enable_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.fens_enable_param,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_fens_cal_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.fens_cal_param,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_fens_cal_param_v2_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.fens_cal_param_v2,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_fens_v2_enable_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.fens_v2_enable_param,
                                 rsp_fn );
  }
  
  if ( session_obj->stream_cal_info.is_fens_v2_cal_param_v3_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.fens_v2_cal_param_v3,
                                 rsp_fn );
  }

  if ( session_obj->stream_cal_info.is_rx_gain_cal_param_found )
  {
    cvp_set_param_to_streams_v2( session_obj,
                                 &session_obj->stream_cal_info.rx_gain_cal_param,
                                 rsp_fn );
  }

  if ( num_mid_pid_to_set == 0 )
  {
    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/****************************************************************************
 * CVP SUBSYSTEM RESTART (SSR) HELPER FUNCTIONS                             *
 ****************************************************************************/

/**
 * Stop each of the VPCM sessions on behalf of CVP clients who reside in
 * a subsystem that is being restarted.
 */
static int32_t cvp_ssr_stop_vpcm (
  uint8_t domain_id
)
{
  int32_t rc;
  uint16_t session_idx;
  uint16_t session_count;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  cvp_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
  cvp_ssr_cleanup_cmd_tracking.rsp_cnt = 0;
  session_idx = 0;
  session_count = 0;

  while( ( session_idx < CVP_MAX_NUM_SESSIONS ) &&
         ( session_count < cvp_info.session_cnt ) )
  {
    session_obj = cvp_info.sessions[ session_idx ];

    /* Check whether the session_obj corresponding to session_idx is valid. */
    if( session_obj != ( ( cvp_session_object_t* ) CVP_SESSION_INVALID ) )
    {
      ++session_count;
      ++session_idx;

      if ( ( session_obj->vpcm_info.is_enabled == TRUE ) &&
           ( APR_GET_FIELD( APRV2_PKT_DOMAIN_ID,
                            session_obj->vpcm_info.client_addr ) == domain_id ) )
      {
        rc = cvp_create_simple_job_object( APR_NULL_V, &job_obj );
        CVP_PANIC_ON_ERROR( rc );

        job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] =
          cvp_ssr_cleanup_cmd_result_count_rsp_fn;

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvp_my_addr, APR_NULL_V,
               cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               job_obj->header.handle, VSS_IVPCM_CMD_STOP,
               NULL, 0 );
        CVP_COMM_ERROR( rc, cvp_my_addr );

        cvp_ssr_cleanup_cmd_tracking.num_cmd_issued++;
      }
    }
    else
    {
      ++session_idx;
    }
  }

  MSG_HIGH( "cvp_ssr_stop_vpcm(): session_idx = %d, session_count = %d, "
            "cvp_info.session_cnt = %d", session_idx, session_count,
            cvp_info.session_cnt );

  return APR_EOK;
}

/****************************************************************************
 * CVP STATE MACHINE HELPER FUNCTIONS                                       *
 ****************************************************************************/

static int32_t cvp_do_complete_goal (
  cvp_session_object_t* session_obj
)
{
  /* Complete the pending command and stay in the same state. */
  session_obj->session_ctrl.goal_status = session_obj->session_ctrl.status;
  session_obj->session_ctrl.goal_completed = TRUE;

  session_obj->session_ctrl.transition_job_handle = APR_NULL_V;
  session_obj->session_ctrl.goal = CVP_GOAL_ENUM_NONE;
  session_obj->session_ctrl.action = CVP_ACTION_ENUM_NONE;
  session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW );

  return APR_EOK;
}

static bool_t cvp_is_reinit_required(
  cvp_session_object_t* session_obj
)
{
  cvp_active_settings_t *active_set = &(session_obj->active_set);
  cvp_target_settings_t *target_set = &(session_obj->target_set);

  /* If there is a sample rate change, port change or topology change,
     indicate reinit. */
  if ( ( active_set->system_config.tx_pp_sr != target_set->system_config.tx_pp_sr ) ||
       ( active_set->system_config.rx_pp_sr != target_set->system_config.rx_pp_sr ) ||
       ( active_set->tx_port_id != target_set->tx_port_id ) ||
       ( active_set->rx_port_id != target_set->rx_port_id ) ||
       ( active_set->tx_topology_id != target_set->tx_topology_id ) ||
       ( active_set->rx_topology_id != target_set->rx_topology_id ) ||
       ( active_set->vocproc_mode != target_set->vocproc_mode ) ||
       ( active_set->ec_ref_port_id != target_set->ec_ref_port_id ) )
  {
    return TRUE;
  }

  return FALSE;
}

/* BACKWARD COMPATIBILITY */
/* Convert legacy network id to new network id + sample rates.
   (Also validate legacy network id in the process.) */
static int32_t cvp_convert_legacy_network_id (
  uint32_t legacy_network_id,
  uint32_t* ret_network_id,
  uint32_t* ret_rx_pp_sr,
  uint32_t* ret_tx_pp_sr
)
{
  int32_t rc;

  if ( ret_network_id == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  switch ( legacy_network_id )
  {
  case VSS_NETWORK_ID_DEFAULT:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_NONE;
      *ret_rx_pp_sr = CVP_DEFAULT_RX_PP_SR;
      *ret_tx_pp_sr = CVP_DEFAULT_TX_PP_SR;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_CDMA_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_CDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_GSM_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_GSM;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_WCDMA_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_WCDMA;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_VOIP_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_VOIP;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_NB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 8000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_WB:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 16000;
      rc = APR_EOK;
    }
    break;

  case VSS_NETWORK_ID_LTE_WV:
    {
      *ret_network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      *ret_rx_pp_sr = 16000;
      *ret_tx_pp_sr = 8000;
      rc = APR_EOK;
    }
    break;

  default:
    rc = APR_EBADPARAM;
    break;
  }

  return rc;
}


#if 0
static int32_t cvp_set_reserved_field_to_zero (
  uint8_t* data,
  uint32_t data_len
)
{
  uint32_t consumed_len = 0;
  voice_param_data_t* cur_param;

  while ( consumed_len < data_len )
  {
    cur_param = ( ( voice_param_data_t* ) &data[consumed_len] );
    cur_param->reserved = 0;
    consumed_len += ( sizeof( voice_param_data_t ) + cur_param->param_size );
  }

  return APR_EOK;
}
#endif //0

static void cvp_update_active_set (
   cvp_session_object_t* session_obj
)
{
  if ( session_obj == NULL )
  {
    return;
  }
  session_obj->active_set.tx_port_id = session_obj->target_set.tx_port_id;
  session_obj->active_set.rx_port_id = session_obj->target_set.rx_port_id;
  session_obj->active_set.tx_topology_id = session_obj->target_set.tx_topology_id;
  session_obj->active_set.rx_topology_id = session_obj->target_set.rx_topology_id;
  session_obj->active_set.vocproc_mode = session_obj->target_set.vocproc_mode;
  session_obj->active_set.ec_ref_port_id = session_obj->target_set.ec_ref_port_id;
  session_obj->active_set.system_config.tx_pp_sr = session_obj->target_set.system_config.tx_pp_sr;
  session_obj->active_set.system_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;
  session_obj->active_set.system_config.network_id = session_obj->target_set.system_config.network_id;

  return;
}

static void cvp_log_state_info (
  cvp_session_object_t* session_obj
)
{
  static cvp_state_enum_t prev_state = CVP_STATE_ENUM_UNINITIALIZED;
  static cvp_goal_enum_t prev_goal = CVP_GOAL_ENUM_UNINITIALIZED;
  static cvp_action_enum_t prev_action = CVP_ACTION_ENUM_UNINITIALIZED;

  if ( session_obj == APR_NULL_V )
    return;

  /* Show new state information. */
  if ( ( prev_state != session_obj->session_ctrl.state ) ||
       ( prev_goal != session_obj->session_ctrl.goal ) ||
       ( prev_action != session_obj->session_ctrl.action ) )
  {
    /* Update information. */
    prev_state = session_obj->session_ctrl.state;
    prev_goal = session_obj->session_ctrl.goal;
    prev_action = session_obj->session_ctrl.action;

    /* Log goal. */
    switch ( session_obj->session_ctrl.goal )
    {
    case CVP_GOAL_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_NONE", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_CREATE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_CREATE", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_ENABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_ENABLE", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_SET_MUTE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_SET_MUTE", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_DISABLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_DISABLE", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_DESTROY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_DESTROY", session_obj->header.handle );
      break;

    case CVP_GOAL_ENUM_REINIT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_GOAL_ENUM_REINIT", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVP_GOAL_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log state. */
    switch ( session_obj->session_ctrl.state )
    {
    case CVP_STATE_ENUM_RESET_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_RESET_ENTRY", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_RESET:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_RESET", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_IDLE_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_IDLE_ENTRY", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_IDLE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_IDLE", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_RUN_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_RUN_ENTRY", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_RUN:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_RUN", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_ERROR_ENTRY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_ERROR_ENTRY", session_obj->header.handle );
      break;

    case CVP_STATE_ENUM_ERROR:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_STATE_ENUM_ERROR", session_obj->header.handle );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVP_STATE_ENUM_INVALID", session_obj->header.handle );
      break;
    }

    /* Log action. */
    switch ( session_obj->session_ctrl.action )
    {
    case CVP_ACTION_ENUM_NONE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_NONE", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_COMPLETE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_COMPLETE", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CONTINUE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CONTINUE", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CREATE_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CREATE_VOCPROC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_REINIT_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_REINIT_VOCPROC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_COMMON:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_COMMON", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_VOICE_TIMING:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_VOICE_TIMING", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_VOLUME:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_VOLUME", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CLEAR_CACHED_STREAM_CAL:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CLEAR_CACHED_STREAM_CAL", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_STATIC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_STATIC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_STATIC_CAL:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_STATIC_CAL", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_DYNAMIC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_DYNAMIC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_DYNAMIC_CAL:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_DYNAMIC_CAL", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_TX_MUTE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_TX_MUTE", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_RX_MUTE:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_RX_MUTE", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_VP3:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_VP3", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_UI_PROPERTIES:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_UI_PROPERTIES", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_SOUNDFOCUS_SECTORS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_SOUNDFOCUS_SECTORS", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_ENABLE_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_ENABLE_VOCPROC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_BROADCAST_READY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_BROADCAST_READY", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_BROADCAST_NOT_READY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_BROADCAST_NOT_READY", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_DISABLE_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_DISABLE_VOCPROC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_GET_VP3:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_GET_VP3", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_BROADCAST_GOING_AWAY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_BROADCAST_GOING_AWAY", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_DESTROY_VOCPROC:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_DESTROY_VOCPROC", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_MAP_SHARED_MEMORY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_MAP_SHARED_MEMORY", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_UNMAP_SHARED_MEMORY:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_UNMAP_SHARED_MEMORY", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_TX_DTMF_DETECT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_TX_DTMF_DETECT", session_obj->header.handle );
      break;

    case CVP_ACTION_ENUM_SET_CLOCKS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_SET_CLOCKS", session_obj->header.handle  );
      break;

    case CVP_ACTION_ENUM_REVERT_CLOCKS:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_REVERT_CLOCKS", session_obj->header.handle  );
      break;

    case CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "session 0x%08X, CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT", session_obj->header.handle  );
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "session 0x%08X, CVP_ACTION_ENUM_INVALID", session_obj->header.handle );
      break;
    }
  }
}

/****************************************************************************
 * CVP STATE MACHINE RESPONSE FUNCTIONS                                     *
 ****************************************************************************/

static void cvp_simple_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* Complete the current action. */
  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* If completed action failed, log the error. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_simple_transition_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
              APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                            session_obj->session_ctrl.status );
  }

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_create_vocproc_session_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* Write access to vocproc_addr. */
  ( void ) apr_lock_enter( session_obj->lock );
  session_obj->vocproc_addr = cvp_vpm_addr;
  ( void ) apr_lock_leave( session_obj->lock );

  if ( session_obj->session_ctrl.status == APR_EOK )
  {
    /* Save the vocproc session handle and complete the current action. */
    /* Write access to vocproc_handle. */
    ( void ) apr_lock_enter( session_obj->lock );
    session_obj->vocproc_handle = packet->src_port;
    ( void ) apr_lock_leave( session_obj->lock );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_create_vocproc_session_transition_result_rsp_fn(): VocProc session addr: 0x%04X, handle: 0x%04X",
                                          session_obj->vocproc_addr,
                                          session_obj->vocproc_handle );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_create_vocproc_session_transition_result_rsp_fn(): Create VPM session failed with result 0x%08X",
                                            session_obj->session_ctrl.status );
  }

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_destroy_vocproc_session_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* Clear the conference session handle and complete the current action. */
  session_obj->vocproc_addr = APRV2_PKT_INIT_ADDR_V;
  session_obj->vocproc_handle = APR_NULL_V;
  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );

  MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_destroy_vocproc_session_transition_result_rsp_fn(): VocProc session destroyed");
}

static void cvp_set_vp3_param_transition_result_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* Complete the current action. */
  session_obj->session_ctrl.status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  /* If completed action failed, log the error. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_vp3_param_transition_result_rsp_fn(): Command 0x%08X failed with result 0x%08X",
                                            APRV2_PKT_GET_PAYLOAD(aprv2_ibasic_rsp_result_t, packet)->opcode,
                                            session_obj->session_ctrl.status );
  }

  /* make cache empty */
  session_obj->shared_heap.vp3_cache.data_len = 0;
  ( void ) mmstd_memset( &session_obj->shared_heap.vp3_cache, 0,
                           sizeof( session_obj->shared_heap.vp3_cache ) );

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_get_vp3_param_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;
  voice_param_data_t* vp3_data;
  cvd_cal_log_vp3_data_header_t log_info_vp3;
  cvd_cal_log_commit_info_t log_info;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* First uint32 in the packet payload is the status. */
  session_obj->session_ctrl.status = *( APRV2_PKT_GET_PAYLOAD( uint32_t, packet ) );

  if ( session_obj->session_ctrl.status == APR_EOK )
  {
    vp3_data = ( ( voice_param_data_t* ) session_obj->shared_heap.vp3_cache.data );
    session_obj->shared_heap.vp3_cache.data_len = ( sizeof( voice_param_data_t ) +
                                                    vp3_data->param_size ); /* header + data */

    /* Save VP3 data into global VP3 cache. */
    if ( ( session_obj->sound_device_info.is_available == TRUE ) &&
         ( vp3_data->param_size != 0 ) )
    {
      rc = cvp_update_global_vp3_heap( session_obj );
    }
	{
      /* Log VP3 Data */
      log_info_vp3.direction = session_obj->sound_device_info.device_pair.direction;
      log_info_vp3.rx_device_id = session_obj->sound_device_info.device_pair.rx_device_id;
      log_info_vp3.tx_device_id = session_obj->sound_device_info.device_pair.tx_device_id;

	  log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) | 
		  				    ( session_obj->header.handle ) );
	  log_info.call_num = session_obj->target_set.system_config.call_num;
	  log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_VP3;
	  log_info.data_container_header_size = sizeof( log_info_vp3 );
	  log_info.data_container_header = &log_info_vp3;
	  log_info.payload_size = session_obj->shared_heap.vp3_cache.data_len;
	  log_info.payload_buf = session_obj->shared_heap.vp3_cache.data;	
	
      ( void ) cvd_cal_log_data( LOG_ADSP_CVD_VP3_C, CVD_CAL_LOG_RAW_VP3_DATA_OUTPUT_TAP_POINT,
                                 ( void* )&log_info, sizeof( log_info ) );
	}
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_vp3_param_transition_rsp_fn(): GET_PARAM failed status 0x%08X",
                                            session_obj->session_ctrl.status );
  }

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_forward_cmd_vp3_get_data_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  uint32_t status;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* forwarding_job_obj;
  aprv2_ibasic_rsp_result_t basic_rsp;


  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &forwarding_job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( forwarding_job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* First uint32 in the packet payload is the status. */
  status = *( APRV2_PKT_GET_PAYLOAD( uint32_t, packet ) );

  if ( status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_forward_cmd_vp3_get_data_rsp_fn(): GET_PARAM failed status 0x%08X",
                                            status );
  }

  basic_rsp.opcode = forwarding_job_obj->orig_opcode;
  basic_rsp.status =  status;
  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
         cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
         forwarding_job_obj->orig_src_service, forwarding_job_obj->orig_src_port,
         forwarding_job_obj->orig_token, APRV2_IBASIC_RSP_RESULT,
         &basic_rsp, sizeof( basic_rsp ) );
  CVP_COMM_ERROR( rc, forwarding_job_obj->orig_src_service );

  rc = cvp_free_object( ( cvp_object_t* ) forwarding_job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

static void cvp_map_memory_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;
  voice_rsp_shared_mem_map_regions_t* mem_map_rsp;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  /* Save the vpm mem handle and complete the current action. */
  mem_map_rsp = APRV2_PKT_GET_PAYLOAD( voice_rsp_shared_mem_map_regions_t, packet );
  session_obj->shared_heap.vpm_mem_handle = mem_map_rsp->mem_map_handle;

  session_obj->session_ctrl.status = APR_EOK;

  cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */

  rc = cvp_free_object( ( cvp_object_t* ) job_obj );
  CVP_PANIC_ON_ERROR( rc );
  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  CVP_PANIC_ON_ERROR( rc );
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static void cvp_calibrate_common_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    session_obj->common_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_common_transition_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }
  /* Complete the current action once all calibration commands have been responded. */
  session_obj->common_cal.set_param_rsp_cnt++;

  if ( session_obj->common_cal.set_param_rsp_cnt ==
       session_obj->common_cal.num_matching_entries )
  {
    if ( session_obj->common_cal.set_param_failed_rsp_cnt != 0 )
    {
      session_obj->session_ctrl.status = APR_EFAILED;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_common_transition_rsp_fn(): Total number of set param failed is %d",
                                              session_obj->common_cal.set_param_failed_rsp_cnt );
    }
    else
    {
      session_obj->session_ctrl.status = APR_EOK;
    }

    cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static void cvp_calibrate_volume_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    session_obj->volume_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_transition_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }

  /* Complete the current action once all volume calibration commands have been responded. */
  session_obj->volume_cal.set_param_rsp_cnt++;

  /* Matched cal entries plus volume level. */
  if ( session_obj->volume_cal.set_param_rsp_cnt ==
       ( session_obj->volume_cal.num_matching_entries + 1 ) )
  {
    if ( session_obj->volume_cal.set_param_failed_rsp_cnt != 0 )
    {
      session_obj->session_ctrl.status = APR_EFAILED;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_volume_transition_rsp_fn(): Total number of set param failed is %d",
                                              session_obj->volume_cal.set_param_failed_rsp_cnt );
    }
    else
    {
      session_obj->session_ctrl.status = APR_EOK;
    }

    cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static void cvp_calibrate_dynamic_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    session_obj->dynamic_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_transition_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }

  /* Complete the current action once all set_param commands have been responded. */
  session_obj->dynamic_cal.set_param_rsp_cnt++;

  if ( session_obj->dynamic_cal.set_param_rsp_cnt ==
       session_obj->dynamic_cal.num_set_param_issued )
  {
    if ( session_obj->dynamic_cal.set_param_failed_rsp_cnt != 0 )
    {
      session_obj->session_ctrl.status = APR_EFAILED;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_dynamic_transition_rsp_fn(): Total number of set param failed is %d",
                                              session_obj->dynamic_cal.set_param_failed_rsp_cnt );
    }
    else
    {
      session_obj->session_ctrl.status = APR_EOK;
    }

    cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static void cvp_calibrate_wv_fens_from_common_cal_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    session_obj->common_cal.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_wv_fens_from_common_cal_transition_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }

  /* Complete the current action once all set param commands have been responded. */
  session_obj->common_cal.set_param_rsp_cnt++;

  if ( session_obj->common_cal.set_param_rsp_cnt ==
       session_obj->common_cal.num_set_param_to_stream )
  {
    if ( session_obj->common_cal.set_param_failed_rsp_cnt != 0 )
    {
      session_obj->session_ctrl.status = APR_EFAILED;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_wv_fens_from_common_cal_transition_rsp_fn(): Total number of set param failed is %d",
                                              session_obj->common_cal.set_param_failed_rsp_cnt );
    }
    else
    {
      session_obj->session_ctrl.status = APR_EOK;
    }

    cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static void cvp_calibrate_stream_modules_transition_rsp_fn (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  int32_t status;
  cvp_simple_job_object_t* job_obj;
  cvp_session_object_t* session_obj;

  rc = cvp_get_typed_object( packet->token, CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB,
                             ( ( cvp_object_t** ) &job_obj ) );
  CVP_PANIC_ON_ERROR( rc );
  rc = cvp_get_typed_object( job_obj->context_handle, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  CVP_PANIC_ON_ERROR( rc );

  status = APRV2_PKT_GET_PAYLOAD( aprv2_ibasic_rsp_result_t, packet )->status;

  if ( status != APR_EOK )
  {
    session_obj->stream_cal_info.set_param_failed_rsp_cnt++;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_stream_modules_transition_rsp_fn(): Last set param failed with 0x%08X",
                                            status );
  }

  /* Complete the current action once all set param commands have been responded. */
  session_obj->stream_cal_info.set_param_rsp_cnt++;

  if ( session_obj->stream_cal_info.set_param_rsp_cnt ==
       session_obj->stream_cal_info.num_set_param_issued )
  {
    if ( session_obj->stream_cal_info.set_param_failed_rsp_cnt != 0 )
    {
      session_obj->session_ctrl.status = APR_EFAILED;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_calibrate_stream_modules_transition_rsp_fn(): Total number of set param failed is %d",
                                              session_obj->stream_cal_info.set_param_failed_rsp_cnt );
    }
    else
    {
      session_obj->session_ctrl.status = APR_EOK;
    }

    cvp_signal_run( CVP_THREAD_PRIORITY_ENUM_LOW ); /* Trigger the session state control to run. */
  }

  ( void ) cvp_free_object( ( cvp_object_t* ) job_obj );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );
}

/****************************************************************************
 * CVP STATE MACHINE TRANSITION ACTION FUNCTIONS                            *
 ****************************************************************************/

static int32_t cvp_action_create_vocproc(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  vpm_create_session_v3_t create_session;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Initialize create session parameter. */
  create_session.tx_port = VSS_IVOCPROC_PORT_ID_NONE;
  create_session.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
  create_session.tx_sampling_rate = CVP_DEFAULT_TX_PP_SR;
  create_session.rx_port = VSS_IVOCPROC_PORT_ID_NONE;
  create_session.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
  create_session.rx_sampling_rate = CVP_DEFAULT_RX_PP_SR;
  create_session.audio_ref_port = VSS_IVOCPROC_PORT_ID_NONE;
  create_session.ec_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;

  /* Populate create session parameter based on available information. */
  switch ( session_obj->direction )
  {
  case VSS_IVOCPROC_DIRECTION_TX:
    {
      create_session.tx_port = session_obj->target_set.tx_port_id;
      create_session.tx_topology_id = session_obj->target_set.tx_topology_id;
      create_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
      create_session.ec_mode = session_obj->target_set.vocproc_mode;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX:
    {
      create_session.rx_port = session_obj->target_set.rx_port_id;
      create_session.rx_topology_id = session_obj->target_set.rx_topology_id;
      create_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
      create_session.ec_mode = session_obj->target_set.vocproc_mode;
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      create_session.tx_port = session_obj->target_set.tx_port_id;
      create_session.tx_topology_id = session_obj->target_set.tx_topology_id;
      create_session.rx_port = session_obj->target_set.rx_port_id;
      create_session.rx_topology_id = session_obj->target_set.rx_topology_id;
      create_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
      create_session.ec_mode = session_obj->target_set.vocproc_mode;
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  create_session.tx_sampling_rate =
    ( ( int16_t ) session_obj->target_set.system_config.tx_pp_sr );

  create_session.rx_sampling_rate =
    ( ( int16_t ) session_obj->target_set.system_config.rx_pp_sr );

  create_session.param_payload_virt_addr = ( uint32_t ) session_obj->topo_param.vpm_param;
  create_session.param_payload_size = session_obj->topo_param.vpm_param_size;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_create_vocproc(): tx_port: 0x%04X, tx_topology_id: "
         "0x%08X, tx_sampling_rate: %d", create_session.tx_port,
         create_session.tx_topology_id, create_session.tx_sampling_rate );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_create_vocproc(): rx_port: 0x%04X, rx_topology_id: 0x%08X, "
         "rx_sampling_rate: %d", create_session.rx_port,
         create_session.rx_topology_id, create_session.rx_sampling_rate );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_create_vocproc(): audio_ref_port: 0x%04X, "
         "ec_mode: 0x%08X", create_session.audio_ref_port,
         create_session.ec_mode );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] =
    cvp_create_vocproc_session_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, APR_NULL_V,
         job_obj->header.handle, VPM_CMD_CREATE_SESSION_V3,
         &create_session, sizeof( create_session ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_map_shared_memory (
  cvp_session_object_t* session_obj
)
{
#ifdef WINSIM

  session_obj->session_ctrl.status = APR_EOK; /* Fake the completion. */
  return APR_EIMMEDIATE; /* To trigger state machine in WINSIM */

#else

  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  struct voice_map_args_t {
    voice_cmd_shared_mem_map_regions_t map_memory;
    voice_shared_map_region_payload_t mem_region;
  } voice_map_args;

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_MAP_MEMORY ] = cvp_map_memory_transition_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  voice_map_args.map_memory.mem_pool_id = VOICE_MAP_MEMORY_SHMEM8_4K_POOL;
  voice_map_args.map_memory.num_regions = 1;
  voice_map_args.map_memory.property_flag = 2; /* Indicates mapping heap memory. */
  voice_map_args.mem_region.shm_addr_lsw = ( ( uint32_t ) &session_obj->shared_heap );
  voice_map_args.mem_region.shm_addr_msw = 0;
  voice_map_args.mem_region.mem_size_bytes = sizeof( session_obj->shared_heap );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, APR_NULL_V,
         job_obj->header.handle, VOICE_CMD_SHARED_MEM_MAP_REGIONS,
         &voice_map_args, sizeof( voice_map_args ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_map_shared_memory(): mem_pool_id: 0x%08X, num_regions:%d, "
         "property_flag: 0x%08X", voice_map_args.map_memory.mem_pool_id,
         voice_map_args.map_memory.num_regions,
         voice_map_args.map_memory.property_flag );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_map_shared_memory(): shm_addr_lsw: 0x%08X, shm_addr_msw: "
         "0x%08X, mem_size_bytes: %d", voice_map_args.mem_region.shm_addr_lsw,
         voice_map_args.mem_region.shm_addr_msw,
         voice_map_args.mem_region.mem_size_bytes );

  return APR_EOK;

#endif /* WINSIM */

}

static int32_t cvp_action_register_voice_activity_update_event (
  cvp_session_object_t* session_obj
)
{
  uint32_t rc;
  cvp_simple_job_object_t* job_obj;
  vpm_cmd_register_event_t register_event;

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  register_event.event_id = VPM_EVT_VOICE_ACTIVITY_UPDATE;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         session_obj->vocproc_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_REGISTER_EVENT,
         &register_event, sizeof( register_event ) );
  CVP_COMM_ERROR( rc, session_obj->vocproc_addr );

  return APR_EOK;
}

/* Sends reinit command to VPM. */
static int32_t cvp_action_reinit_vocproc (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  vpm_create_session_v3_t reinit_session;
  cvp_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );

  if ( job_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_action_reinit_vocproc(): "  );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  { /* Initialize parameters for reinit to default values. */
    reinit_session.tx_port = VSS_IVOCPROC_PORT_ID_NONE;
    reinit_session.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    reinit_session.tx_sampling_rate = CVP_DEFAULT_TX_PP_SR;
    reinit_session.rx_port = VSS_IVOCPROC_PORT_ID_NONE;
    reinit_session.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
    reinit_session.rx_sampling_rate = CVP_DEFAULT_RX_PP_SR;
    reinit_session.audio_ref_port = VSS_IVOCPROC_PORT_ID_NONE;
    reinit_session.ec_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;
  }

  { /* Populate parameters for reinit based on available information. */
    switch ( session_obj->direction )
    {
    case VSS_IVOCPROC_DIRECTION_TX:
      {
         reinit_session.tx_port = session_obj->target_set.tx_port_id;
         reinit_session.tx_topology_id = session_obj->target_set.tx_topology_id;
         reinit_session.ec_mode = session_obj->target_set.vocproc_mode;
         reinit_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
      }
      break;

     case VSS_IVOCPROC_DIRECTION_RX:
       {
         reinit_session.rx_port = session_obj->target_set.rx_port_id;
         reinit_session.rx_topology_id = session_obj->target_set.rx_topology_id;
         reinit_session.ec_mode = session_obj->target_set.vocproc_mode;
         reinit_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
       }
       break;

     case VSS_IVOCPROC_DIRECTION_RX_TX:
       {
         reinit_session.tx_port = session_obj->target_set.tx_port_id;
         reinit_session.tx_topology_id = session_obj->target_set.tx_topology_id;
         reinit_session.rx_port = session_obj->target_set.rx_port_id;
         reinit_session.rx_topology_id = session_obj->target_set.rx_topology_id;
         reinit_session.ec_mode = session_obj->target_set.vocproc_mode;
         reinit_session.audio_ref_port = session_obj->target_set.ec_ref_port_id;
       }
       break;

       default:
         CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
         break;
     }

     reinit_session.tx_sampling_rate =
       ( ( int16_t ) session_obj->target_set.system_config.tx_pp_sr );

     reinit_session.rx_sampling_rate =
       ( ( int16_t ) session_obj->target_set.system_config.rx_pp_sr );

    reinit_session.param_payload_virt_addr = ( uint32_t ) session_obj->topo_param.vpm_param;
    reinit_session.param_payload_size = session_obj->topo_param.vpm_param_size;
  }

  { /* Send reinit command to VPM using the job_obj provided by the caller. */
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_reinit_vocproc(): tx_port: 0x%04X, tx_topology_id: 0x%08X, "
           "tx_sampling_rate: %d", reinit_session.tx_port,
           reinit_session.tx_topology_id, reinit_session.tx_sampling_rate );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_reinit_vocproc(): rx_port: 0x%04X, rx_topology_id: 0x%08X, "
           "rx_sampling_rate: %d", reinit_session.rx_port,
           reinit_session.rx_topology_id, reinit_session.rx_sampling_rate );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_reinit_vocproc(): audio_ref_port: 0x%04X, ec_mode: 0x%08X",
           reinit_session.audio_ref_port, reinit_session.ec_mode );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           session_obj->vocproc_addr, session_obj->vocproc_handle,
           job_obj->header.handle, VPM_CMD_REINIT_SESSION_V3,
           &reinit_session, sizeof( reinit_session ) );
    CVP_COMM_ERROR( rc, session_obj->vocproc_addr );
  }

  return APR_EOK;
}

static int32_t cvp_action_unmap_shared_memory (
  cvp_session_object_t* session_obj
)
{
#ifdef WINSIM

  session_obj->session_ctrl.status = APR_EOK; /* Fake the completion. */
  return APR_EIMMEDIATE; /* To trigger state machine in WINSIM */

#else

  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_cmd_shared_mem_unmap_regions_t mem_unmap;

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  mem_unmap.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_unmap_shared_memory(): vpm_mem_handle: 0x%08X",
         mem_unmap.mem_map_handle );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, APR_NULL_V,
         job_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
         &mem_unmap, sizeof( mem_unmap ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;

#endif /* WINSIM */

}

static int32_t cvp_action_destroy_vocproc(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_destroy_vocproc_session_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_DESTROY_SESSION,
         NULL, 0 );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_set_voice_timing(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  vpm_set_timing_params_t timing_param;
  vpm_set_timing_params_v2_t timing_param_v2;

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] =
    cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  if ( session_obj->target_set.system_config.vsid == 0 )
  {
    /* BACKWARD COMPATIBILITY */
    /* If the CVD client does not provide a VSID, use VPM_CMD_SET_TIMING_PARAMS
     * to pass the VFR mode and vocproc timing offsets to VPM.
     */

    { /* Populate the timing parameters based on target_set. */
      timing_param.mode =
        session_obj->target_set.system_config.vfr_mode;

      timing_param.vptx_start_offset =
        session_obj->target_set.voice_timing.vp_tx_start_offset;

      timing_param.vptx_delivery_offset =
        session_obj->target_set.voice_timing.vp_tx_delivery_offset;

      timing_param.vprx_delivery_offset =
        session_obj->target_set.voice_timing.vp_rx_delivery_offset;
    }

    { /* Set timing on VPM. */
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_action_set_voice_timing(): VFR mode = %d, "
             "vptx_start_offset = %d, vptx_delivery_offset = %d",
             timing_param.mode, timing_param.vptx_start_offset,
             timing_param.vptx_delivery_offset );

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_action_set_voice_timing(): vprx_delivery_offset = %d",
             timing_param.vprx_delivery_offset );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VPM_CMD_SET_TIMING_PARAMS,
             &timing_param, sizeof( timing_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }
  }
  else
  {
    { /* Populate the timing parameters based on target_set. */
      timing_param_v2.vsid =
        session_obj->target_set.system_config.vsid;

      timing_param_v2.mode =
        session_obj->target_set.system_config.vfr_mode;

      timing_param_v2.vptx_start_offset =
        session_obj->target_set.voice_timing.vp_tx_start_offset;

      timing_param_v2.vptx_delivery_offset =
        session_obj->target_set.voice_timing.vp_tx_delivery_offset;

      timing_param_v2.vprx_delivery_offset =
        session_obj->target_set.voice_timing.vp_rx_delivery_offset;
    }

    { /* Set timing on VPM. */
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_action_set_voice_timing(): VSID = 0x%08X, VFR mode: %d, "
             "vptx_start_offset: %d", timing_param_v2.vsid,
             timing_param_v2.mode, timing_param_v2.vptx_start_offset );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_action_set_voice_timing(): vptx_delivery_offset = %d,"
             "vprx_delivery_offset = %d", timing_param_v2.vptx_delivery_offset,
             timing_param_v2.vprx_delivery_offset );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             job_obj->header.handle, VPM_CMD_SET_TIMING_PARAMS_V2,
             &timing_param_v2, sizeof( timing_param_v2 ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_common (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  rc = cvp_calibrate_common( session_obj, cvp_calibrate_common_transition_rsp_fn );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_wv_fens_from_common_cal (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  rc = cvp_calibrate_wv_fens_from_common_cal(
         session_obj, cvp_calibrate_wv_fens_from_common_cal_transition_rsp_fn );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_volume(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  /* When applying cached setting, set the ramp duartion to 0. */
  rc = cvp_calibrate_volume( session_obj, 0, cvp_calibrate_volume_transition_rsp_fn );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_clear_cached_stream_cal(
  cvp_session_object_t* session_obj
)
{
  ( void ) cvp_clear_cached_stream_cal( session_obj );

  return APR_EIMMEDIATE;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_static(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  rc = cvp_calibrate_static( session_obj, job_obj );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = cvp_free_object( ( cvp_object_t* ) job_obj );
    CVP_PANIC_ON_ERROR( rc );

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_dynamic(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  /* When applying cached setting, set the ramp duartion to 0. */
  rc = cvp_calibrate_dynamic( session_obj, 0, cvp_calibrate_dynamic_transition_rsp_fn );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_query_stream_modules_from_static_cal(
  cvp_session_object_t* session_obj
)
{
  ( void ) cvp_find_stream_modules_from_matched_entries(
             session_obj, session_obj->static_cal.query_handle );

  ( void ) cvd_cal_query_deinit( session_obj->static_cal.query_handle );

  return APR_EIMMEDIATE;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_query_stream_modules_from_dynamic_cal(
  cvp_session_object_t* session_obj
)
{
  ( void ) cvp_find_stream_modules_from_matched_entries(
             session_obj, session_obj->dynamic_cal.query_handle );

  ( void ) cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );

  return APR_EIMMEDIATE;
}

/* BACKWARD COMPATIBILITY. Refer to the comments under CVP_GOAL_ENUM_ENABLE
 * in cvp_state_idle.
 */
static int32_t cvp_action_calibrate_stream_modules(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  rc = cvp_calibrate_stream_modules( session_obj, cvp_calibrate_stream_modules_transition_rsp_fn );
  if ( rc )
  {
    if ( rc == APR_EIMMEDIATE )
    {
      session_obj->session_ctrl.status = APR_EOK;
    }
    else
    {
      session_obj->session_ctrl.status = rc;
    }

    rc = APR_EIMMEDIATE;
  }

  return rc;
}

static int32_t cvp_action_set_ui_properties( cvp_session_object_t* session_obj )
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;

  if ( session_obj->shared_heap.ui_prop_cache.data_len == 0 )
  {
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  set_param.payload_address_lsw = ( ( uint32_t ) session_obj->shared_heap.ui_prop_cache.data );
  set_param.payload_address_msw = 0;
  set_param.payload_size = session_obj->shared_heap.ui_prop_cache.data_len;
  set_param.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_ui_properties(): payload_address_lsw: 0x%08X, "
         "payload_address_msw: 0x%08X", set_param.payload_address_lsw,
         set_param.payload_address_msw );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_ui_properties(): payload_size: %d, "
         "mem_map_handle: 0x%08X", set_param.payload_size,
         set_param.mem_map_handle );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
         &set_param, sizeof( set_param ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_set_soundfocus_sectors(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  cvp_soundfocus_set_param_t soundfocus_set_param;
  vss_isoundfocus_cmd_set_sectors_t* set_sectors;

  if ( session_obj == NULL )
    return APR_EBADPARAM;

  set_sectors = &session_obj->target_set.soundfocus_sectors;

  MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_soundfocus_sectors(): enables[0-3] = "
         "[%d, %d, %d, %d]", set_sectors->enables[ 0 ], set_sectors->enables[ 1 ],
         set_sectors->enables[ 2 ], set_sectors->enables[ 3 ] );

  MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_soundfocus_sectors(): enables[4-7] = "
         "[%d, %d, %d, %d]", set_sectors->enables[ 4 ], set_sectors->enables[ 5 ],
         set_sectors->enables[ 6 ], set_sectors->enables[ 7 ] );

  MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_soundfocus_sectors(): start_angles[0-3] = "
         "[%d, %d, %d, %d]", set_sectors->start_angles[ 0 ], set_sectors->start_angles[ 1 ],
         set_sectors->start_angles[ 2 ], set_sectors->start_angles[ 3 ] );

  MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_soundfocus_sectors(): start_angles[4-7] = "
         "[%d, %d, %d, %d]", set_sectors->start_angles[ 4 ], set_sectors->start_angles[ 5 ],
         set_sectors->start_angles[ 6 ], set_sectors->start_angles[ 7 ] );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_soundfocus_sectors(): gain_step = %d", set_sectors->gain_step );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  soundfocus_set_param.payload_address_lsw = 0;
  soundfocus_set_param.payload_address_msw = 0;
  soundfocus_set_param.payload_size = sizeof( cvp_soundfocus_param_t );
  soundfocus_set_param.mem_map_handle = 0;
  soundfocus_set_param.param.module_id = VOICEPROC_MODULE_TX;
  soundfocus_set_param.param.param_id = VOICE_PARAM_FLUENCE_SOUNDFOCUS;
  soundfocus_set_param.param.param_size = sizeof( cvp_soundfocus_param_data_t );
  soundfocus_set_param.param.reserved = 0;
  mmstd_memcpy( soundfocus_set_param.param.param_data.start_angles,
                sizeof( set_sectors->start_angles ),
                set_sectors->start_angles,
                sizeof( set_sectors->start_angles ) );
  mmstd_memcpy( soundfocus_set_param.param.param_data.enables,
                sizeof( set_sectors->enables ),
                set_sectors->enables,
                sizeof( set_sectors->enables ) );
  soundfocus_set_param.param.param_data.gain_step = set_sectors->gain_step;
  soundfocus_set_param.param.param_data.reserved = 0;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
         &soundfocus_set_param, sizeof( soundfocus_set_param ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_set_tx_mute(
  cvp_session_object_t* session_obj,
  uint32_t mute_ramp_duration
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_set_soft_mute_v2_t set_soft_mute_args;

  if( NULL == session_obj )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  set_soft_mute_args.direction = VOICE_SET_MUTE_TX_ONLY;
  set_soft_mute_args.mute = session_obj->target_set.client_tx_mute;
  set_soft_mute_args.ramp_duration = ( int16_t )mute_ramp_duration;
  set_soft_mute_args.reserved = 0;
  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_tx_mute(): direction: %d, mute: %d, ramp_duration: %d",
         set_soft_mute_args.direction, set_soft_mute_args.mute,
         set_soft_mute_args.ramp_duration );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_SET_SOFT_MUTE_V2,
         &set_soft_mute_args, sizeof( set_soft_mute_args ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_set_rx_mute(
  cvp_session_object_t* session_obj,
  uint32_t mute_ramp_duration
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_set_soft_mute_v2_t set_soft_mute_args;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  set_soft_mute_args.direction = VOICE_SET_MUTE_RX_ONLY;
  set_soft_mute_args.mute = session_obj->target_set.client_rx_mute;
  set_soft_mute_args.ramp_duration = ( int16_t )mute_ramp_duration;
  set_soft_mute_args.reserved = 0;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_rx_mute(): direction: %d, mute: %d, ramp_duration: %d",
         set_soft_mute_args.direction, set_soft_mute_args.mute,
         set_soft_mute_args.ramp_duration );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_SET_SOFT_MUTE_V2,
         &set_soft_mute_args, sizeof( set_soft_mute_args ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_get_vp3(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;
  voice_get_param_v2_t get_param;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_get_vp3_param_transition_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  ( void ) mmstd_memset( &session_obj->shared_heap.vp3_cache, 0,
                         sizeof( session_obj->shared_heap.vp3_cache ) );

  get_param.payload_address_lsw = ( ( uint32_t ) session_obj->shared_heap.vp3_cache.data );
  get_param.payload_address_msw = 0;
  get_param.module_id = CVP_VP3_MOD_ID;
  get_param.param_id = CVP_VP3_PARAM_ID;
  get_param.param_max_size = CVP_MAX_VP3_DATA_LEN;
  get_param.reserved = 0;
  get_param.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
         &get_param, sizeof( get_param ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_set_vp3(
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  bool_t is_vp3_to_be_set = FALSE;
  cvp_simple_job_object_t* job_obj;
  voice_set_param_v2_t set_param;
  cvp_global_vp3_list_item_t* vp3_list_item;
  cvd_cal_log_vp3_data_header_t log_info_vp3;
  cvd_cal_log_commit_info_t log_info;

  if ( session_obj == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( session_obj->is_client_set_vp3_data == FALSE )
  {
    if ( session_obj->sound_device_info.is_available == TRUE )
    {
      rc = cvp_find_matching_item_from_global_vp3_heap(
             session_obj, &vp3_list_item );
      if ( rc == APR_EOK )
      {
        if ( vp3_list_item->vp3->data_len <= CVP_MAX_VP3_DATA_LEN )
        {
          mmstd_memcpy( session_obj->shared_heap.vp3_cache.data, CVP_MAX_VP3_DATA_LEN,
                        vp3_list_item->vp3->data, vp3_list_item->vp3->data_len );
          session_obj->shared_heap.vp3_cache.data_len = vp3_list_item->vp3->data_len;
          is_vp3_to_be_set = TRUE;
		  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_action_set_vp3(): Found VP3 Device Pair Cal. Rx=0x%08X, Tx=0x%08X",
               vp3_list_item->vp3->sound_device_pair.rx_device_id, vp3_list_item->vp3->sound_device_pair.tx_device_id );
        }
      }
    }
  }

  /* Check if we have vp3 data in the cache. */
  if ( ( is_vp3_to_be_set == FALSE ) && ( session_obj->is_client_set_vp3_data == FALSE ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_action_set_vp3(): No vp3 data in cache ! Not setting vp3" );
    session_obj->session_ctrl.status = APR_EOK;
    return APR_EIMMEDIATE;
  }

  session_obj->is_client_set_vp3_data = FALSE;
  {
    /* Log VP3 Data */
    log_info_vp3.direction = session_obj->sound_device_info.device_pair.direction;
    log_info_vp3.rx_device_id = session_obj->sound_device_info.device_pair.rx_device_id;
    log_info_vp3.tx_device_id = session_obj->sound_device_info.device_pair.tx_device_id;

    log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) | 
					      ( session_obj->header.handle ) );
    log_info.call_num = session_obj->target_set.system_config.call_num;
    log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_RAW_VP3;
    log_info.data_container_header_size = sizeof( log_info_vp3 );
    log_info.data_container_header = &log_info_vp3;
    log_info.payload_size = session_obj->shared_heap.vp3_cache.data_len;
    log_info.payload_buf = session_obj->shared_heap.vp3_cache.data;	

    ( void ) cvd_cal_log_data( LOG_ADSP_CVD_VP3_C, CVD_CAL_LOG_RAW_VP3_DATA_INPUT_TAP_POINT,
                               ( void* )&log_info, sizeof( log_info ) );
  }

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );

  /* making the cache 0 in response function */
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_set_vp3_param_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  /* Send data to VDSP. */
  set_param.payload_address_lsw = ( ( uint32_t ) session_obj->shared_heap.vp3_cache.data );
  set_param.payload_address_msw = 0;
  set_param.payload_size = session_obj->shared_heap.vp3_cache.data_len;
  set_param.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_vp3(): payload_address_lsw: 0x%08X, "
         "payload_address_msw: 0x%08X", set_param.payload_address_lsw,
         set_param.payload_address_msw );

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_action_set_vp3(): payload_size: %d, mem_map_handle: 0x%08X",
         set_param.payload_size, set_param.mem_map_handle );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
         &set_param, sizeof( set_param ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_enable_vocproc (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_START_SESSION,
         NULL, 0 );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_disable_vocproc (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

  session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_STOP_SESSION,
         NULL, 0 );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  return APR_EOK;
}

static int32_t cvp_action_tx_dtmf_detect (
  cvp_session_object_t* session_obj,
  bool_t enable_flag
)
{
  int32_t rc;
  cvp_simple_job_object_t* job_obj;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  if ( session_obj->set_tx_dtmf_detect.enable_flag == CVP_ENABLED )
  {
    vpm_set_tx_dtmf_detection_t vpm_tx_dtmf_detect;

    session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_transition_result_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = job_obj->header.handle;

    vpm_tx_dtmf_detect.enable = enable_flag;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_action_tx_dtmf_detect(): enable: %d", vpm_tx_dtmf_detect.enable );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           job_obj->header.handle, VSS_IVOCPROC_CMD_SET_TX_DTMF_DETECTION,
           &vpm_tx_dtmf_detect, sizeof( vpm_tx_dtmf_detect ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );
    return APR_EOK;
  }
  return APR_EIMMEDIATE;
}

static int32_t cvp_action_broadcast_ready (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  cvp_attached_stream_item_t* attached_stream;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Iterate through attached stream list and send EVENT_READY to each
   * attached stream. */
  pivot_node = &(session_obj->attached_stream_list.dummy);

  while( APR_EOK == apr_list_get_next( &(session_obj->attached_stream_list),
                                       pivot_node,
                                       &cur_node ) )
  {
    attached_stream = (cvp_attached_stream_item_t*) cur_node;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           attached_stream->addr, attached_stream->port,
           0, VSS_IVOCPROC_EVT_READY,
           NULL, 0 );
    CVP_COMM_ERROR( rc, attached_stream->addr );

    pivot_node = cur_node;
  }

  session_obj->session_ctrl.status = APR_EOK;
  return APR_EIMMEDIATE;
}

static int32_t cvp_action_broadcast_not_ready (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  cvp_attached_stream_item_t* attached_stream;

  if ( session_obj == NULL )
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );

  /* Iterate through attached stream list and send EVENT_NOT_READY to each
   * attached stream. */
  pivot_node = &(session_obj->attached_stream_list.dummy);

  while( APR_EOK == apr_list_get_next( &(session_obj->attached_stream_list),
                                       pivot_node,
                                       &cur_node ) )
  {
    attached_stream = (cvp_attached_stream_item_t*) cur_node;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           attached_stream->addr, attached_stream->port,
           0, VSS_IVOCPROC_EVT_NOT_READY,
           NULL, 0 );
    CVP_COMM_ERROR( rc, attached_stream->addr );

    pivot_node = cur_node;
  }

  session_obj->session_ctrl.status = APR_EOK;
  return APR_EIMMEDIATE;
}

static int32_t cvp_action_broadcast_going_away (
  cvp_session_object_t* session_obj
)
{
  /* ... */

  session_obj->session_ctrl.status = APR_EOK;
  return APR_EIMMEDIATE;
}

/****************************************************************************
 * CVP STATE MACHINE                                                        *
 ****************************************************************************/

static int32_t cvp_state_reset_entry (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  /* In case of failure , end the goal. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_state_reset_entry(): last action failed with result=0x%08X",
                                            session_obj->session_ctrl.status );
    rc = cvp_do_complete_goal( session_obj );
    CVP_PANIC_ON_ERROR( rc );

  }

  session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET;
  return APR_EIMMEDIATE;
}

static int32_t cvp_state_reset (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVP_GOAL_ENUM_NONE:
    break;

  case CVP_GOAL_ENUM_DESTROY:
    {
      /* Complete the pending command and stay in the same state. */
      session_obj->session_ctrl.status = APR_EOK;
      rc = cvp_do_complete_goal( session_obj );
    }
    break;

  case CVP_GOAL_ENUM_CREATE:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_SET_CLOCKS:
        {
           session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET_ENTRY;
           session_obj->session_ctrl.action = CVP_ACTION_ENUM_CREATE_VOCPROC;
           session_obj->session_ctrl.status = APR_EOK;

           rc = vccm_mmpm_request_clocks( VCCM_CLIENT_ID_CVP );
        }
        return APR_EIMMEDIATE;

      case CVP_ACTION_ENUM_CREATE_VOCPROC:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_MAP_SHARED_MEMORY;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_create_vocproc( session_obj );

          /* Update active_set. */
          session_obj->active_set.tx_port_id = session_obj->target_set.tx_port_id;
          session_obj->active_set.tx_topology_id = session_obj->target_set.tx_topology_id;
          session_obj->active_set.rx_port_id = session_obj->target_set.rx_port_id;
          session_obj->active_set.rx_topology_id = session_obj->target_set.rx_topology_id;
          session_obj->active_set.vocproc_mode = session_obj->target_set.vocproc_mode;
          session_obj->active_set.ec_ref_port_id = session_obj->target_set.ec_ref_port_id;
          session_obj->active_set.system_config.tx_pp_sr = session_obj->target_set.system_config.tx_pp_sr;
          session_obj->active_set.system_config.rx_pp_sr = session_obj->target_set.system_config.rx_pp_sr;
        }
        return rc;

      case CVP_ACTION_ENUM_MAP_SHARED_MEMORY:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_map_shared_memory( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_REVERT_CLOCKS;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_register_voice_activity_update_event( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_REVERT_CLOCKS:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_EOK;

          rc = vccm_mmpm_release_clocks( VCCM_CLIENT_ID_CVP );
        }
        return APR_EIMMEDIATE;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_state_idle_entry (
  cvp_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  /* In case of failure , end the goal. */
  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_state_idle_entry(): last action failed with result=0x%08X",
           session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE;
  return APR_EIMMEDIATE;
}

static int32_t cvp_state_idle (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVP_GOAL_ENUM_NONE:
    break;

  case CVP_GOAL_ENUM_CREATE:
  case CVP_GOAL_ENUM_DISABLE:
    {
      /* Complete the pending command and stay in the same state. */
      rc = cvp_do_complete_goal( session_obj );
    }
    break;

  case CVP_GOAL_ENUM_REINIT:
    {
      switch( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_CONTINUE:
      case CVP_ACTION_ENUM_REINIT_VOCPROC:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_reinit_vocproc( session_obj );

          ( void ) cvp_update_active_set( session_obj );

          { /* BACKWARD COMPATIBILITY. */
            session_obj->common_cal.is_calibrate_needed =
              session_obj->common_cal.is_registered;
            session_obj->volume_cal.is_calibrate_needed =
              session_obj->volume_cal.is_registered;
          }

          session_obj->static_cal.is_calibrate_needed =
            session_obj->static_cal.is_registered;
          session_obj->dynamic_cal.is_calibrate_needed =
            session_obj->dynamic_cal.is_registered;
        }
        return rc;

      case CVP_ACTION_ENUM_REGISTER_VOICE_ACTIVITY_UPDATE_EVENT:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_COMPLETE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_register_voice_activity_update_event( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_COMPLETE:
        { /* Complete the pending command and stay in the same state. */
          rc = cvp_do_complete_goal( session_obj );
        }
        return rc;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVP_GOAL_ENUM_ENABLE:
    {
      /* BACKWARD COMPATIBILITY NOTES:
       * Running stand-alone vocproc (i.e. the vocproc is not attached to MVM)
       * is a deprecated behavior. In order to support this behavior, we must
       * calibrate the vocproc.
       * Note that even the vocproc is able to run, its performance is not
       * guaranteed, because its KPPS requirement is not taken into account
       * by MVM concurrency manager.
       *
       * On the other hand, if the vocproc is attached to MVM, We skip the
       * actions for calibrating the vocproc. This is because that the vocproc
       * has already been calibrated upon receiving the VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
       * command from MVM.
       */

      switch ( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_CONTINUE:
      case CVP_ACTION_ENUM_CALIBRATE_COMMON: /* BACKWARD COMPATIBILITY. */
        {
          if ( session_obj->attached_mvm_handle != APR_NULL_V )
          {
            session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
            session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_VOICE_TIMING;
            session_obj->session_ctrl.status = APR_EOK;

            return APR_EIMMEDIATE;
          }

          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_common( session_obj );

          /* No need to update the active set. netowrk_id/sampling rate updated
           * in REINIT. */
        }
        return rc;

       /* TODO: this is a temporary solution until proper FW solution for
        * WV/FENS is available.
        */
      case CVP_ACTION_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_VOLUME;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_wv_fens_from_common_cal( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CALIBRATE_VOLUME: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CLEAR_CACHED_STREAM_CAL;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_volume( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CLEAR_CACHED_STREAM_CAL: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_STATIC;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvp_action_clear_cached_stream_cal( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CALIBRATE_STATIC: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_STATIC_CAL;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_static( session_obj );
          if ( rc )
          {
            session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_DYNAMIC;
          }
        }
        return rc;

      case CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_STATIC_CAL: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_DYNAMIC;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvp_action_query_stream_modules_from_static_cal( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CALIBRATE_DYNAMIC: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_DYNAMIC_CAL;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_dynamic( session_obj );
          if ( rc )
          {
            session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES;
          }
        }
        return rc;

      case CVP_ACTION_ENUM_QUERY_CAL_MODULES_FROM_DYNAMIC_CAL: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvp_action_query_stream_modules_from_dynamic_cal( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CALIBRATE_STREAM_MODULES: /* BACKWARD COMPATIBILITY. */
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_VOICE_TIMING;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_calibrate_stream_modules( session_obj );

          { /* Update active set. */
            session_obj->active_set.system_config.rx_voc_op_mode =
              session_obj->target_set.system_config.rx_voc_op_mode;

            session_obj->active_set.system_config.tx_voc_op_mode =
              session_obj->target_set.system_config.tx_voc_op_mode;

            session_obj->active_set.system_config.media_id =
              session_obj->target_set.system_config.media_id;

            session_obj->active_set.system_config.feature =
              session_obj->target_set.system_config.feature;
          }
        }
        return rc;

      case CVP_ACTION_ENUM_SET_VOICE_TIMING:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_TX_MUTE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_set_voice_timing( session_obj );

          /* Update the active set. */
          session_obj->active_set.system_config.vsid = session_obj->target_set.system_config.vsid;
          session_obj->active_set.system_config.vfr_mode = session_obj->target_set.system_config.vfr_mode;
          session_obj->active_set.voice_timing = session_obj->target_set.voice_timing;
        }
        return rc;

      case CVP_ACTION_ENUM_SET_TX_MUTE:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_RX_MUTE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          /* When applying cached setting, set the ramp duartion to 0. */
          rc = cvp_action_set_tx_mute( session_obj, 0 );

          /* Update active_set. */
          session_obj->active_set.client_tx_mute = session_obj->target_set.client_tx_mute;
        }
        return rc;

      case CVP_ACTION_ENUM_SET_RX_MUTE:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_VP3;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          /* When applying cached setting, set the ramp duartion to 0. */
          rc = cvp_action_set_rx_mute( session_obj, 0 );

          /* Update active_set. */
          session_obj->active_set.client_rx_mute = session_obj->target_set.client_rx_mute;
        }
        return rc;

      case CVP_ACTION_ENUM_SET_VP3:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_UI_PROPERTIES;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_set_vp3( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_SET_UI_PROPERTIES:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_ENABLE_VOCPROC;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_set_ui_properties( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_ENABLE_VOCPROC:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          if ( session_obj->is_client_set_sectors == TRUE )
          {
            session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_SOUNDFOCUS_SECTORS;
          }
          else
          {
            session_obj->session_ctrl.action = CVP_ACTION_ENUM_TX_DTMF_DETECT;
          }
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_enable_vocproc( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_SET_SOUNDFOCUS_SECTORS:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_TX_DTMF_DETECT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_set_soundfocus_sectors( session_obj );

          /* There is no active_set for sector configuration, because
             1) Sector configuration change will not cause KPPS/BW requirement change
                (confirmed with VCP team);
             2) Sector configuration is always pushed down to VPM. CVP does not
                cache the configuration (Fluence may adjust sector configuration due to
                various reasons, e.g., two-mic mirroring,and minimum sector restrictions, etc.) */
        }
        return rc;

      case CVP_ACTION_ENUM_TX_DTMF_DETECT:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_BROADCAST_READY;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvp_action_tx_dtmf_detect( session_obj, (bool_t) VSS_IVOCPROC_TX_DTMF_DETECTION_ENABLE );
        }
        return rc;

      case CVP_ACTION_ENUM_BROADCAST_READY:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_broadcast_ready( session_obj );
        }
        return rc;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVP_GOAL_ENUM_DESTROY:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_CONTINUE:
      case CVP_ACTION_ENUM_BROADCAST_GOING_AWAY:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_UNMAP_SHARED_MEMORY;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_broadcast_going_away( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_UNMAP_SHARED_MEMORY:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_DESTROY_VOCPROC;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_unmap_shared_memory( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_DESTROY_VOCPROC:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_destroy_vocproc( session_obj );
        }
        return rc;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }

    }

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_state_run_entry (
  cvp_session_object_t* session_obj
)
{
  if ( session_obj->session_ctrl.status == APR_UNDEFINED_ID_V )
  { /* Stay put. */
    return APR_EOK;
  }

  if ( session_obj->session_ctrl.status != APR_EOK )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_state_run_entry(): last action failed with result=0x%08X",
           session_obj->session_ctrl.status );
  }

  session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN;
  return APR_EIMMEDIATE;
}

static int32_t cvp_state_run (
  cvp_session_object_t* session_obj
)
{
  int32_t rc;

  switch ( session_obj->session_ctrl.goal )
  {
  case CVP_GOAL_ENUM_NONE:
    break;

  case CVP_GOAL_ENUM_REINIT:
  case CVP_GOAL_ENUM_DISABLE:
  case CVP_GOAL_ENUM_DESTROY:
  case CVP_GOAL_ENUM_ENABLE: /* This is a re-enable due to network change. */
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_BROADCAST_NOT_READY:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_TX_DTMF_DETECT;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_broadcast_not_ready( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_TX_DTMF_DETECT:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_GET_VP3;
          session_obj->session_ctrl.status = APR_EOK;

          rc = cvp_action_tx_dtmf_detect( session_obj, (bool_t) VSS_IVOCPROC_TX_DTMF_DETECTION_DISABLE );
        }
        return rc;

      case CVP_ACTION_ENUM_GET_VP3:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_DISABLE_VOCPROC;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_get_vp3( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_DISABLE_VOCPROC:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_IDLE_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_CONTINUE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          rc = cvp_action_disable_vocproc( session_obj );
        }
        return rc;

      case CVP_ACTION_ENUM_CONTINUE:
        {
          /* Complete the pending command and stay in the same state. */
          rc = cvp_do_complete_goal( session_obj );
        }
        return APR_EOK;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  case CVP_GOAL_ENUM_SET_MUTE:
    {
      switch ( session_obj->session_ctrl.action )
      {
      case CVP_ACTION_ENUM_NONE:
      case CVP_ACTION_ENUM_SET_TX_MUTE:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_SET_RX_MUTE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          if( session_obj->active_set.client_tx_mute != session_obj->target_set.client_tx_mute )
          {
            rc = cvp_action_set_tx_mute( session_obj, session_obj->target_set.mute_ramp_duration );
          }
          else
          {
            /* No change in mute settings on TX path. */
            session_obj->session_ctrl.status = APR_EOK;
            rc = APR_EIMMEDIATE;
          }
        }
        return rc;

      case CVP_ACTION_ENUM_SET_RX_MUTE:
        {
          session_obj->session_ctrl.state = CVP_STATE_ENUM_RUN_ENTRY;
          session_obj->session_ctrl.action = CVP_ACTION_ENUM_COMPLETE;
          session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;

          if( session_obj->active_set.client_rx_mute != session_obj->target_set.client_rx_mute )
          {
            rc = cvp_action_set_rx_mute( session_obj, session_obj->target_set.mute_ramp_duration );
          }
          else
          {
            /* No change in mute settings on RX path. */
            session_obj->session_ctrl.status = APR_EOK;
            rc = APR_EIMMEDIATE;
          }
        }
        return rc;

      case CVP_ACTION_ENUM_COMPLETE:
        {
          /* Update active_set. */
          session_obj->active_set.client_tx_mute = session_obj->target_set.client_tx_mute;
          session_obj->active_set.client_rx_mute = session_obj->target_set.client_rx_mute;
          session_obj->active_set.mute_ramp_duration = session_obj->target_set.mute_ramp_duration;

          /* Complete the pending command and stay in the same state. */
          rc = cvp_do_complete_goal( session_obj );
        }
        return APR_EOK;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_state_control (
  cvp_session_object_t* session_obj
)
{
  int32_t rc = APR_EOK;

  if ( session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  do
  {
    cvp_log_state_info( session_obj );

    switch ( session_obj->session_ctrl.state )
    {
    case CVP_STATE_ENUM_RESET_ENTRY:
      rc = cvp_state_reset_entry( session_obj );
      break;

    case CVP_STATE_ENUM_RESET:
      rc = cvp_state_reset( session_obj );
      break;

    case CVP_STATE_ENUM_IDLE_ENTRY:
      rc = cvp_state_idle_entry( session_obj );
      break;

    case CVP_STATE_ENUM_IDLE:
      rc = cvp_state_idle( session_obj );
      break;

    case CVP_STATE_ENUM_RUN_ENTRY:
      rc = cvp_state_run_entry( session_obj );
      break;

    case CVP_STATE_ENUM_RUN:
      rc = cvp_state_run( session_obj );
      break;

    /* ... */

    default:
      rc = APR_EUNEXPECTED;
      break;
    }
  }
  while ( rc == APR_EIMMEDIATE );

  cvp_log_state_info( session_obj );

  if ( rc == APR_ECONTINUE )
  { /* TODO: Use a timer to trigger the state machine to run after 5ms. */
  }

  return rc;
}

/****************************************************************************
 * HELPER FUNCIONS                                                          *
 ****************************************************************************/

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t cvp_helper_open_session_control (
  cvp_pending_control_t* ctrl,
  cvp_session_object_t** ret_session_obj,
  bool_t check_commit_state_flag
)
{
  int32_t rc;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    return APR_EBADPARAM;
  }

  client_addr = ctrl->packet->src_addr;

  if ( ret_session_obj == NULL )
  {
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EBADPARAM;
  }

  rc = cvp_get_typed_object( ctrl->packet->dst_port, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) ret_session_obj ) );
  if ( rc )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_helper_open_session_control(): Invalid session handle: "
           "cmd_opcode=0x%08X src_addr=0x%08X req_handle=0x%08X",
           ctrl->packet->opcode, ctrl->packet->src_addr, ctrl->packet->dst_port );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EHANDLE;
  }

  /* Check if session creation/dev switch has been committed successfully.
   */
  if ( check_commit_state_flag )
  {
    if ( (*ret_session_obj)->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_CREATE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                   "cvp_helper_open_session_control(): Unexpected topo_commit_state %d",
                   (*ret_session_obj)->topo_commit_state );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, ctrl->packet->src_addr );
      return APR_EFAILED;
    }
    if ( (*ret_session_obj)->topo_commit_state != CVP_TOPOLOGY_COMMIT_STATE_NONE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                   "cvp_helper_open_session_control(): topo_commit_state %d",
                   (*ret_session_obj)->topo_commit_state );
    }
  }

  return APR_EOK;
}

/* Returns APR_EOK when done, else the packet is completed and an error returned. */
static int32_t cvp_helper_validate_payload_size_control (
  cvp_pending_control_t* ctrl,
  uint32_t valid_size
)
{
  int32_t rc;
  uint32_t size;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
  if ( size != valid_size )
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_helper_validate_payload_size_control(): Invalid payload size, "
           "opcode=0x%08X, valid_size=%d, actual_size=%d", ctrl->packet->opcode,
           valid_size, size );

    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
    CVP_COMM_ERROR( rc, client_addr );

    return APR_EBADPARAM;
  }

  return APR_EOK;
}

static int32_t cvp_helper_create_new_goal_control (
  cvp_pending_control_t* ctrl,
  cvp_session_object_t* session_obj,
  cvp_goal_enum_t new_goal
)
{
  if ( ( ctrl == NULL ) || ( session_obj == NULL ) )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  if ( session_obj->session_ctrl.goal != CVP_GOAL_ENUM_NONE )
  { /* The session shouldn't be doing anything. */
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  session_obj->session_ctrl.goal = new_goal;
    /* The goal is reset by the state machine on completion. */

  session_obj->session_ctrl.goal_completed = FALSE;
  session_obj->session_ctrl.goal_status = APR_UNDEFINED_ID_V;

  ctrl->session_obj = session_obj;
    /* Store pointer in the ctrl to the session whose pending commmand is
     * currently being executed.
     */

  return APR_EOK;
}

/* Returns APR_EOK when done and packet completed, else APR_EPENDING. */
static int32_t cvp_helper_simple_wait_for_goal_completion_control (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;

  if ( ctrl == NULL )
  {
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
  }

  /* Wait until the job is done. */
  if ( ctrl->session_obj->session_ctrl.goal_completed )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet,
                                  ctrl->session_obj->session_ctrl.goal_status );
    CVP_COMM_ERROR( rc, client_addr );

    ctrl->session_obj = APR_NULL_V;

    return APR_EOK;
  }

  return APR_EPENDING;
}

static int32_t cvp_forward_evt_tx_dtmf_detected_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint32_t handle;

  handle = p_packet->dst_port;
  rc = cvp_access_session_obj_start( handle, &session_obj );
  if ( rc == APR_EOK )
  { /* Redirect this event to the client. */
    /* Read access to set_tx_dtmf_detect. */
    ( void ) apr_lock_enter ( session_obj->lock );
    p_packet->src_addr = p_packet->dst_addr;
    p_packet->src_port = p_packet->dst_port;
    p_packet->dst_addr = session_obj->set_tx_dtmf_detect.client_addr;
    p_packet->dst_port = session_obj->set_tx_dtmf_detect.client_port;
    rc = __aprv2_cmd_forward( cvp_apr_handle, p_packet );
    CVP_COMM_ERROR( rc, session_obj->set_tx_dtmf_detect.client_addr );
    ( void ) apr_lock_leave ( session_obj->lock );
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_forward_evt_tx_dtmf_detected_processing(): dst_port: 0x%04X, "
           "result: 0x%08X", p_packet->dst_port, rc );
    rc = __aprv2_cmd_free( cvp_apr_handle, p_packet );
  }

  ( void ) cvp_access_session_obj_end( handle );

  return APR_EOK;
}

static int32_t cvp_forward_voice_activity_event_to_mvm (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj = NULL;
  uint32_t handle = packet->dst_port;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
       "cvp_forward_voice_activity_event_to_mvm(): CVP recieving "
       "VPM_EVT_VOICE_ACTIVITY_UPDATE" );

  rc = cvp_access_session_obj_start( handle, &session_obj );
  if( APR_EOK == rc )
  {
    ( void ) apr_lock_enter ( session_obj->lock );

    packet->opcode = VSS_ICOMMON_EVT_VOICE_ACTIVITY_UPDATE;
    packet->src_addr = packet->dst_addr;
    packet->src_port = packet->dst_port;
    packet->dst_addr = cvp_mvm_addr;
    packet->dst_port = session_obj->attached_mvm_handle;
    ( void ) apr_lock_leave ( session_obj->lock );

    rc = __aprv2_cmd_forward( cvp_apr_handle, packet );
    CVP_COMM_ERROR( rc, cvp_mvm_addr );
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_forward_voice_activity_event_to_mvm(): dst_port: 0x%04X, "
           "result: 0x%08X", packet->dst_port, rc );
    rc = __aprv2_cmd_free( cvp_apr_handle, packet );
  }

  ( void ) cvp_access_session_obj_end( handle );

  return APR_EOK;
}

/* Returns APR_EOK when completed.  If an entry was found, it is populated into ret_data_entry */
static int32_t cvp_helper_search_hdvoice_config (
  cvp_session_object_t * session_obj,
  vss_ihdvoice_cmd_get_config_t* search_values,
  vss_ihdvoice_rsp_get_config_t* ret_data_entry
)
{
  int32_t rc = APR_EOK;
  uint32_t i, j = 0;
  uint32_t* sys_config_column_value = (uint32_t*) session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head;
  vss_param_cal_column_t* columns = (vss_param_cal_column_t*) session_obj->hdvoice_config_info.hdvoice_config_hdr.columns;
  uint32_t column_id = 0;
  uint32_t column_type = 0;
  uint32_t column_size = 0;
  uint32_t matched_column_value = 0;
  uint64_t typed_column_value = 0;
  vss_ihdvoice_rsp_get_config_t* matched_entry = NULL;

  if ( ( session_obj->is_device_config_registered == FALSE ) ||
       ( sys_config_column_value == NULL ) )
    return APR_EOK;

  for ( i = 0; i < session_obj->hdvoice_config_info.hdvoice_config_hdr.num_sys_config_entries; i++)
  {
    matched_column_value = 0;

    for ( j = 0; j < session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns; j++ )
    {
      column_id = columns->id;
      column_type = columns->type;
      column_size = 0;

      switch ( column_type )
      {
        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT64:
          typed_column_value = *(uint64_t *)sys_config_column_value;
          column_size = sizeof (uint64_t);
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT32:
          typed_column_value = *(uint32_t *)sys_config_column_value;
          column_size = sizeof (uint32_t);
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT16:
          typed_column_value = *(uint16_t *)sys_config_column_value;
          column_size = sizeof (uint16_t);
          break;

        case VSS_ICOMMON_CAL_COLUMN_TYPE_UINT8:
          typed_column_value = *(uint8_t *)sys_config_column_value;
          column_size = sizeof (uint8_t);
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_helper_search_hdvoice_config(): Invalid Column "
                 "Type: 0x%08X", column_type);
          rc = APR_EBADPARAM;
          break;
      }

      switch ( column_id )
      {
        case VSS_ICOMMON_CAL_COLUMN_NETWORK:
          if ( search_values->network_id == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Network "
                   "ID=0x%08x", search_values->network_id );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_RX_PP_SAMPLING_RATE:
          if ( search_values->rx_pp_sr == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Rx Sampling "
                   "Rate ID=0x%08x", search_values->rx_pp_sr );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_TX_PP_SAMPLING_RATE:
          if ( search_values->tx_pp_sr == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Tx Sampling "
                   "Rate ID=0x%08x", search_values->tx_pp_sr );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_MEDIA_ID:
          if ( search_values->media_id == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Media ID=0x%08x",
                   search_values->media_id );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_TX_VOC_OPERATING_MODE:
          if ( search_values->tx_voc_op_mode == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Tx Voc "
                   "Operating Mode=0x%08x", search_values->tx_voc_op_mode );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_RX_VOC_OPERATING_MODE:
          if ( search_values->rx_voc_op_mode == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Rx Voc "
                   "Operating Mode=0x%08x", search_values->rx_voc_op_mode );
            matched_column_value++;
          }
          break;

        case VSS_ICOMMON_CAL_COLUMN_FEATURE:
          if ( search_values->feature_id == typed_column_value )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
                   "cvp_helper_search_hdvoice_config(): Matched Feature=0x%08x",
                   search_values->feature_id );
            matched_column_value++;
          }
          break;

        default:
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_helper_search_hdvoice_config(): Undefined Column "
                 "Value: 0x%08X with Column ID: 0x%08X", typed_column_value,
                 column_id );
          rc = APR_EBADPARAM;
          break;
      }

            /* Advance Column Pointer to find next ID */
      *(uint8_t**) &columns = (uint8_t*) columns + sizeof(columns->id) + sizeof(columns->type) + column_size;

      /* Advance to next Column Value in Sys Config List */
      *(uint8_t**) &sys_config_column_value = (uint8_t*) sys_config_column_value + column_size;
    }

    /** If a matched entry is found, return entry and exit **/
    if ( matched_column_value == session_obj->hdvoice_config_info.hdvoice_config_hdr.num_columns )
    {
      matched_entry = (vss_ihdvoice_rsp_get_config_t *) sys_config_column_value;
      ret_data_entry->enable_mode = matched_entry->enable_mode;
      ret_data_entry->feature_id = matched_entry->feature_id;
      ret_data_entry->rx_pp_sr = matched_entry->rx_pp_sr;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "cvp_helper_search_hdvoice_config(): Matched Entry Found");

      break;
    }

    *(uint8_t**) &sys_config_column_value += sizeof(vss_param_hdvoice_config_data_t);
    /* Return Column Pointer to the first Column */
    columns = (vss_param_cal_column_t*) session_obj->hdvoice_config_info.hdvoice_config_hdr.columns;
  }

  return rc;
}

/****************************************************************************
 * NON-SEQUENTIAL COMMAND PROCESSING FUNCTIONS                              *
 ****************************************************************************/

static int32_t cvp_set_ui_property_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_simple_job_object_t* job_obj;
  vss_icommon_cmd_set_ui_property_t* payload;
  uint32_t payload_size;
  voice_set_param_v2_t set_param;
  bool_t is_cached = FALSE;
  uint8_t* cur_ui_prop_cache_slot;
  uint32_t remaining_ui_prop_cache_len;
  uint8_t* destination_ui_prop_cache_slot = NULL;
  uint8_t* remaining_ui_prop;
  uint32_t remaining_ui_prop_len;
  voice_param_data_t* cached_ui_prop;
  uint32_t cached_ui_prop_len;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_get_typed_object( packet->dst_port, CVP_OBJECT_TYPE_ENUM_SESSION,
                               ( ( cvp_object_t** ) &session_obj ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_ui_property_cmd_processing(): Invalid session handle, "
             "dst_port: 0x%04X, result: 0x%08X", packet->dst_port, rc );
      rc = APR_EHANDLE;
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_ui_property_t, packet );
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    if ( payload_size < sizeof( vss_icommon_cmd_set_ui_property_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_ui_property_cmd_processing(): Unexpected payload size, "
             "%d < %d", payload_size, sizeof( vss_icommon_cmd_set_ui_property_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvp_verify_param_data_size( payload->param_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_ui_property_cmd_processing(): Invalid UI param data "
             "size, %d", payload->param_size );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload->param_size > CVP_MAX_UI_PROP_DATA_LEN )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_ui_property_cmd_processing(): Invalid param data "
             "length %d > Max length %d", payload->param_size,
             CVP_MAX_UI_PROP_DATA_LEN );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload_size != ( payload->param_size + sizeof( voice_param_data_t ) ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_ui_property_cmd_processing(): Invalid payload "
             "size %d != %d", payload_size,
             ( payload->param_size + sizeof( voice_param_data_t ) ) );
      rc = APR_EBADPARAM;
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "cvp_set_ui_property_cmd_processing(): Module Id 0x%08X / Param Id "
           "0x%08X received", payload->module_id, payload->param_id );

    cur_ui_prop_cache_slot = session_obj->shared_heap.ui_prop_cache.data;
    remaining_ui_prop_cache_len = sizeof( session_obj->shared_heap.ui_prop_cache.data );
    remaining_ui_prop_len = session_obj->shared_heap.ui_prop_cache.data_len;

    /* Cache UI properties. */
    while ( remaining_ui_prop_cache_len > 0 )
    {
      cached_ui_prop = ( ( voice_param_data_t* ) cur_ui_prop_cache_slot );
      cached_ui_prop_len = ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) );

      if ( ( cached_ui_prop->module_id == payload->module_id ) &&
           ( cached_ui_prop->param_id == payload->param_id ) )
      {
        /* Value already exists. Cache it. */
        if ( cached_ui_prop_len != payload_size )
        { /* New UI property data size is different than the cached UI property data. */
          /* Shuffle the UI property cache to accommodate the new size. */
          remaining_ui_prop_len -= cached_ui_prop_len;
          remaining_ui_prop = ( cur_ui_prop_cache_slot + cached_ui_prop_len );

          while ( remaining_ui_prop_len >= cached_ui_prop_len )
          {
            ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, remaining_ui_prop, cached_ui_prop_len );
            remaining_ui_prop_len -= cached_ui_prop_len;
            remaining_ui_prop += cached_ui_prop_len;
            cur_ui_prop_cache_slot += cached_ui_prop_len;
            remaining_ui_prop_cache_len -= cached_ui_prop_len;
          }

          if ( remaining_ui_prop_len != 0 )
          {
            ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, remaining_ui_prop, remaining_ui_prop_len );
            cur_ui_prop_cache_slot += remaining_ui_prop_len;
            remaining_ui_prop_cache_len -= remaining_ui_prop_len;
          }

          ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, payload, payload_size );

          if ( cached_ui_prop_len < payload_size )
          { /* Increase the cached data length due to increased UI prop data. */
            session_obj->shared_heap.ui_prop_cache.data_len += ( payload_size - cached_ui_prop_len );
          }
          else
          { /* Decrease the cached data length due to decreased UI prop data. */
            session_obj->shared_heap.ui_prop_cache.data_len -= ( cached_ui_prop_len - payload_size );

            /* Set the decreased part of the cache that used to contain data to zero. */
            ( void ) mmstd_memset( ( cur_ui_prop_cache_slot + payload_size ), 0,
                                   ( cached_ui_prop_len - payload_size ) );
          }
        }
        else
        {
          ( void ) mmstd_memcpy( cur_ui_prop_cache_slot, remaining_ui_prop_cache_len, payload, payload_size );
        }

        destination_ui_prop_cache_slot = cur_ui_prop_cache_slot;
        is_cached = TRUE;
        break;
      }

      /* Save the free slot. */
      if ( ( cached_ui_prop->module_id == 0 ) && ( cached_ui_prop->param_id == 0 ) )
      {
        destination_ui_prop_cache_slot = cur_ui_prop_cache_slot;
        break;
      }

      cur_ui_prop_cache_slot += cached_ui_prop_len;
      remaining_ui_prop_cache_len -= cached_ui_prop_len;
      remaining_ui_prop_len -= cached_ui_prop_len;
    }

    /* If not already cached, copy the parameter to the first free slot */
    if( ( is_cached == FALSE ) && ( destination_ui_prop_cache_slot != NULL ) &&
        ( session_obj->shared_heap.ui_prop_cache.num_ui_prop != CVP_MAX_UI_PROP ) )
    {
      ( void ) mmstd_memcpy( destination_ui_prop_cache_slot,
                             remaining_ui_prop_cache_len,
                             payload, payload_size );
      session_obj->shared_heap.ui_prop_cache.data_len += payload_size;
      session_obj->shared_heap.ui_prop_cache.num_ui_prop++;
      is_cached = TRUE;
    }

    if ( is_cached == FALSE )
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "cvp_set_ui_property_cmd_processing(): Reached Maximum cached "
              "entries %d", CVP_MAX_UI_PROP );
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "cvp_set_ui_property_cmd_processing(): Error caching Module id "
              "0x%08X Param id 0x%08X", payload->module_id, payload->param_id );
       rc = APR_ENORESOURCE;
       break;
    }

    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_self_destruct_result_rsp_fn;

    /* Send SET_PARAM command to VPM. */
    set_param.payload_address_lsw = ( ( uint32_t ) destination_ui_prop_cache_slot );
    set_param.payload_address_msw = 0;
    set_param.payload_size = payload_size;
    set_param.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_set_ui_property_cmd_processing(): payload_address_lsw: 0x%08X, "
           "payload_address_msw: 0x%08X", set_param.payload_address_lsw,
           set_param.payload_address_msw );

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_set_ui_property_cmd_processing(): payload_size: %d, "
           "mem_map_handle: 0x%08X", set_param.payload_size,
           set_param.mem_map_handle );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
           &set_param, sizeof( set_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );

    break;
  }

  client_addr = packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_get_ui_property_cmd_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* track_job_obj;
  vss_icommon_cmd_get_ui_property_t* payload;
  uint32_t payload_size;
  voice_get_param_v2_t get_param;
  bool_t ui_prop_exists = FALSE;
  uint8_t* cur_ui_prop_cache_slot;
  uint32_t remaining_ui_prop_cache_len;
  voice_param_data_t* cached_ui_prop;
  uint32_t get_ui_status;
  aprv2_packet_t* rsp_packet;
  uint8_t* rsp_payload;
  uint32_t rsp_payload_size;
  uint32_t rsp_payload_left;
  uint16_t client_addr;

  client_addr = packet->src_addr;

  rc = cvp_get_typed_object( packet->dst_port, CVP_OBJECT_TYPE_ENUM_SESSION,
                             ( ( cvp_object_t** ) &session_obj ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_get_ui_property_cmd_processing(): Invalid session handle, "
           "dst_port: 0x%04X, result: 0x%08X", packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, APR_EHANDLE );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_get_ui_property_t, packet );
  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

  if ( payload_size != sizeof( vss_icommon_cmd_get_ui_property_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_get_ui_property_cmd_processing(): Unexpected payload size, "
           "%d != %d", payload_size, sizeof( vss_icommon_cmd_get_ui_property_t ) );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, APR_EBADPARAM );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  rc = cvp_verify_param_data_size( payload->param_size );
  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_get_ui_property_cmd_processing(): Invalid UI param data size, "
           "%d", payload->param_size );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, packet, APR_EBADPARAM );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  remaining_ui_prop_cache_len = session_obj->shared_heap.ui_prop_cache.data_len;
  cur_ui_prop_cache_slot = session_obj->shared_heap.ui_prop_cache.data;

  while ( remaining_ui_prop_cache_len != 0 )
  {
    cached_ui_prop = ( ( voice_param_data_t* ) cur_ui_prop_cache_slot );

    if ( ( cached_ui_prop->module_id == payload->module_id ) &&
         ( cached_ui_prop->param_id == payload->param_id ) )
    {
      ui_prop_exists = TRUE;
      get_ui_status = 0;
      break;
    }

    cur_ui_prop_cache_slot += ( cached_ui_prop->param_size +
                                sizeof( voice_param_data_t ) );
    remaining_ui_prop_cache_len -= ( cached_ui_prop->param_size +
                                     sizeof( voice_param_data_t ) );
  }

  if ( ui_prop_exists == TRUE )
  {
    rsp_payload_size = ( sizeof( vss_icommon_rsp_get_ui_property_t ) +
                         cached_ui_prop->param_size );

    rc = __aprv2_cmd_alloc_ext(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           packet->src_addr, packet->src_port,
           packet->token, VSS_ICOMMON_RSP_GET_UI_PROPERTY,
           rsp_payload_size, &rsp_packet );
    CVP_PANIC_ON_ERROR( rc );

    rsp_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, rsp_packet );
    rsp_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( rsp_packet->header );

    ( void ) mmstd_memcpy( rsp_payload, rsp_payload_left, &get_ui_status, sizeof( get_ui_status ) );
    rsp_payload_left -= sizeof( get_ui_status );

    ( void ) mmstd_memcpy( ( rsp_payload + sizeof( get_ui_status ) ), rsp_payload_left,
                           cached_ui_prop, ( cached_ui_prop->param_size + sizeof( voice_param_data_t ) ) );

    rc = __aprv2_cmd_forward( cvp_apr_handle, rsp_packet);
    CVP_COMM_ERROR( rc, client_addr );
  }
  else
  { /* If the UI property is not cached, get it from VPM. */
    rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVP_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_get_param_rsp_fn;

    /* Store the original destination and opcode info from the packet */
    track_job_obj->orig_src_service = packet->src_addr;
    track_job_obj->orig_src_port = packet->src_port;
    track_job_obj->orig_opcode = packet->opcode;
    track_job_obj->orig_token = packet->token;

    get_param.payload_address_lsw = 0;
    get_param.payload_address_msw = 0;
    get_param.module_id = payload->module_id;
    get_param.param_id = payload->param_id;
    get_param.param_max_size = ( ( uint16_t )( payload->param_size + sizeof( voice_param_data_t ) ) );
    get_param.reserved = 0;
    get_param.mem_map_handle = 0; /* in-band */

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "CVD_CAL_MSG_PARAM: Sending GET_PARAM_V2 to VCP. "
           "cvp_get_ui_property_cmd_processing(): Token=%d Param ID=0x%08x",
           packet->token, payload->param_id );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );
  }

  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );

  return APR_EOK;
}

static int32_t cvp_set_tx_dtmf_detection_cmd_processing(
  aprv2_packet_t* p_packet
)
{
  int32_t rc;
  vss_ivocproc_cmd_set_tx_dtmf_detection_t *p_in_args;
  uint32_t in_size;
  cvp_session_object_t *session_obj;
  cvp_simple_job_object_t *job_obj;
  vpm_set_tx_dtmf_detection_t vpm_tx_dtmf_detect;
  uint16_t client_addr;
  uint32_t handle;

  client_addr = p_packet->src_addr;
  handle = p_packet->dst_port;
  rc = cvp_access_session_obj_start( handle, &session_obj );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_set_tx_dtmf_detection_cmd_processing(): Invalid session "
           "handle, dst_port: 0x%04X, result: 0x%08X", p_packet->dst_port, rc );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, p_packet, APR_EHANDLE );
    CVP_COMM_ERROR( rc, client_addr );
    ( void ) cvp_access_session_obj_end( handle );
    return APR_EOK;
  }

  p_in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_tx_dtmf_detection_t,
                                     p_packet );
  in_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( p_packet->header );

  if ( in_size != sizeof( vss_ivocproc_cmd_set_tx_dtmf_detection_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_set_tx_dtmf_detection_cmd_processing(): Unexpected payload "
           "size, %d != %d", in_size,
           sizeof( vss_ivocproc_cmd_set_tx_dtmf_detection_t ) );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, p_packet, APR_EFAILED );
    CVP_COMM_ERROR( rc, client_addr );
    ( void ) cvp_access_session_obj_end( handle );
    return APR_EOK;
  }

  /* If the current tx dtmf detect status is same as the request,
     return already done. */
  ( void ) apr_lock_enter ( session_obj->lock );
  if ( session_obj->set_tx_dtmf_detect.enable_flag == p_in_args->enable )
  {
    rc = __aprv2_cmd_end_command( cvp_apr_handle, p_packet, APR_EALREADY );
    CVP_COMM_ERROR( rc, client_addr );
    ( void ) apr_lock_leave ( session_obj->lock );
    ( void ) cvp_access_session_obj_end( handle );
    return APR_EOK;
  }

  /* Store the enable status. */
  session_obj->set_tx_dtmf_detect.enable_flag = p_in_args->enable;

  /* If the request is to enable, save the client addr and port. */
  if ( session_obj->set_tx_dtmf_detect.enable_flag )
  {
    session_obj->set_tx_dtmf_detect.client_addr = p_packet->src_addr;
    session_obj->set_tx_dtmf_detect.client_port = p_packet->src_port;
  }
  else
  {
    /* If the request is to disable, check whether the client
       is same as the one that issued enable. */
    if ( session_obj->set_tx_dtmf_detect.client_addr != p_packet->src_addr ||
         session_obj->set_tx_dtmf_detect.client_port != p_packet->src_port )
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, p_packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      ( void ) apr_lock_leave ( session_obj->lock );
      ( void ) cvp_access_session_obj_end( handle );
      return APR_EOK;
    }
  }

  rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_self_destruct_result_rsp_fn;

  vpm_tx_dtmf_detect.enable = p_in_args->enable;

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "cvp_set_tx_dtmf_detection_cmd_processing(): enable: %d",
         vpm_tx_dtmf_detect.enable );

  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( uint16_t ) session_obj->header.handle,
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_SET_TX_DTMF_DETECTION,
         &vpm_tx_dtmf_detect, sizeof( vpm_tx_dtmf_detect ) );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );
  ( void ) apr_lock_leave ( session_obj->lock );

  rc = __aprv2_cmd_end_command( cvp_apr_handle, p_packet, APR_EOK );
  CVP_COMM_ERROR( rc, client_addr );

  ( void ) cvp_access_session_obj_end( handle );

  return APR_EOK;;
}

/****************************************************************************
 * SEQUENTIAL COMMAND PROCESSING FUNCTIONS                                  *
 ****************************************************************************/

static int32_t cvp_construct_session (
  cvp_session_object_t** ret_session_obj,
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_ivocproc_cmd_create_full_control_session_v3_t* in_args;
  uint32_t network_id;
  uint32_t rx_pp_sr;
  uint32_t tx_pp_sr;
  cvp_session_object_t* session_obj;

  rc = cvp_create_session_object( &session_obj );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  /* Update the packet->dst_port to the new session handle. */
  ctrl->packet->dst_port = ( ( uint16_t ) session_obj->header.handle );

  /* Initialize addresses. */
  session_obj->self_addr = cvp_my_addr;
  session_obj->full_ctrl_client_addr = ctrl->packet->src_addr;
  session_obj->full_ctrl_client_port = ctrl->packet->src_port;

  in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_create_full_control_session_v3_t, ctrl->packet );

  /* Set the target_set based on command paramters. */
  switch ( in_args->profile_id )
  {
  case VSS_NETWORK_ID_DEFAULT:
  case VSS_NETWORK_ID_VOIP_NB:
  case VSS_NETWORK_ID_VOIP_WB:
  case VSS_NETWORK_ID_VOIP_WV:
  case VSS_NETWORK_ID_CDMA_NB:
  case VSS_NETWORK_ID_CDMA_WB:
  case VSS_NETWORK_ID_CDMA_WV:
  case VSS_NETWORK_ID_GSM_NB:
  case VSS_NETWORK_ID_GSM_WB:
  case VSS_NETWORK_ID_GSM_WV:
  case VSS_NETWORK_ID_WCDMA_NB:
  case VSS_NETWORK_ID_WCDMA_WB:
  case VSS_NETWORK_ID_WCDMA_WV:
  case VSS_NETWORK_ID_LTE_NB:
  case VSS_NETWORK_ID_LTE_WB:
  case VSS_NETWORK_ID_LTE_WV:
  {
    /* Convert the legacy network id to the new network_id.
     * (And validate the parameter in the process.)
     */
    rc = cvp_convert_legacy_network_id( in_args->profile_id,
                                         &network_id,
                                         &rx_pp_sr,
                                         &tx_pp_sr );
    if ( rc )
    {
      rc = cvp_free_object( ( cvp_object_t* ) session_obj );
      CVP_PANIC_ON_ERROR( rc );
      return APR_EBADPARAM;
    }

    /* Save parameters in target set to be picked up by state machine. */
    session_obj->target_set.system_config.network_id = network_id;
    session_obj->target_set.system_config.rx_pp_sr = rx_pp_sr;
    session_obj->target_set.system_config.tx_pp_sr = tx_pp_sr;

    break;
  }
  case VSS_ICOMMON_CAL_NETWORK_ID_NONE:
  case VSS_ICOMMON_CAL_NETWORK_ID_CDMA:
  case VSS_ICOMMON_CAL_NETWORK_ID_GSM:
  case VSS_ICOMMON_CAL_NETWORK_ID_WCDMA:
  case VSS_ICOMMON_CAL_NETWORK_ID_VOIP:
  case VSS_ICOMMON_CAL_NETWORK_ID_LTE:
    session_obj->target_set.system_config.network_id = in_args->profile_id;
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_construct_session(): Invalid profile ID 0x%08X",
                                            in_args->profile_id );
    rc = cvp_free_object( ( cvp_object_t* ) session_obj );
    CVP_PANIC_ON_ERROR( rc );
    return APR_EBADPARAM;
  }

  switch ( in_args->direction )
  {
  case VSS_IVOCPROC_DIRECTION_TX:
    {
      session_obj->target_set.tx_port_id = in_args->tx_port_id;
      session_obj->target_set.tx_topology_id = in_args->tx_topology_id;
      session_obj->target_set.rx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
      session_obj->target_set.rx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        if ( session_obj->target_set.tx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.tx_port_id = AFE_PORT_ID_PSEUDOPORT_01;
        }
        else
        {
          session_obj->is_valid_afe_port_provided = TRUE;
        }
      }
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX:
    {
      session_obj->target_set.tx_port_id = VSS_IVOCPROC_PORT_ID_NONE;
      session_obj->target_set.tx_topology_id = VSS_IVOCPROC_TOPOLOGY_ID_NONE;
      session_obj->target_set.rx_port_id = in_args->rx_port_id;
      session_obj->target_set.rx_topology_id = in_args->rx_topology_id;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        if ( session_obj->target_set.rx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.rx_port_id = AFE_PORT_ID_PSEUDOPORT_02;
        }
        else
        {
          session_obj->is_valid_afe_port_provided = TRUE;
        }
      }
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      session_obj->target_set.tx_port_id = in_args->tx_port_id;
      session_obj->target_set.tx_topology_id = in_args->tx_topology_id;
      session_obj->target_set.rx_port_id = in_args->rx_port_id;
      session_obj->target_set.rx_topology_id = in_args->rx_topology_id;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        session_obj->is_valid_afe_port_provided = TRUE;

        if ( session_obj->target_set.tx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.tx_port_id = AFE_PORT_ID_PSEUDOPORT_01;
        }

        if ( session_obj->target_set.rx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.rx_port_id = AFE_PORT_ID_PSEUDOPORT_02;
        }
      }
    }
    break;

  default:
    rc = cvp_free_object( ( cvp_object_t* ) session_obj );
    CVP_PANIC_ON_ERROR( rc );
    return APR_EBADPARAM;
  }
  session_obj->direction = in_args->direction;
  *ret_session_obj = session_obj;
  return APR_EOK;
}

static int32_t cvp_destroy_vpm_session (
  cvp_session_object_t* session_obj,
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr = ctrl->packet->src_addr;
  cvp_simple_job_object_t* job_obj;
#ifndef WINSIM
  voice_cmd_shared_mem_unmap_regions_t mem_unmap;

  if ( session_obj->shared_heap.vpm_mem_handle != APR_NULL_V )
  { /* Unmap shared memory if required */
    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_self_destruct_result_rsp_fn;

    mem_unmap.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, APR_NULL_V,
           job_obj->header.handle, VOICE_CMD_SHARED_MEM_UNMAP_REGIONS,
           &mem_unmap, sizeof( mem_unmap ) );
    CVP_COMM_ERROR( rc, client_addr );
  }
#endif /* WINSIM */

  if ( session_obj->vocproc_handle != APR_NULL_V )
  { /* if there was a VPM session created with valid handle,
     then destroy it. */
    rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
    CVP_PANIC_ON_ERROR( rc );
    job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_simple_self_destruct_result_rsp_fn;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           job_obj->header.handle,
           VPM_CMD_DESTROY_SESSION, NULL, 0 );
    CVP_COMM_ERROR( rc, client_addr );
  }
  return APR_EOK;
}

static int32_t cvp_create_full_session_v3_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;
  uint32_t i;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* TODO: If a session with the same name as this one that already has
     * a full control client exists, reject this command. */

    rc = cvp_construct_session( &session_obj, ctrl );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_CREATE;
    /* Clear in-band topology params */
    session_obj->topo_param.num_dev_chans.rx_num_channels = 0;
    session_obj->topo_param.num_dev_chans.tx_num_channels = 0;
    /* Clear VPM topology params */
    for ( i = 0; i < CVP_MAX_NUM_TOPO_PARAMS; i++)
    {
      session_obj->topo_param.vpm_param[i].param_size = 0;
      session_obj->topo_param.vpm_param[i].param_virt_addr = NULL;
    }
  }

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_topology_commit_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  client_addr = ctrl->packet->src_addr;
  rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
  if ( rc )
    return APR_EOK;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_topology_commit_cmd_ctrl(): topo_commit_state = %d", session_obj->topo_commit_state );
  rc = cvp_extract_topology_params( session_obj, FALSE );
  if ( rc )
  {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
      "cvp_topology_commit_cmd_ctrl(): Invalid topology_params" );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
        CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
  }
  if ( ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_CREATE ) &&
      ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RESET ) )
    {
    rc = cvp_helper_create_new_goal_control( ctrl, session_obj,
                                             CVP_GOAL_ENUM_CREATE );
    if ( rc ) return APR_EOK;
  }
  else
  {
    if ( ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_SET_DEVICE ) &&
        ( session_obj->session_ctrl.state == CVP_STATE_ENUM_IDLE ) )
    {
        if ( cvp_is_reinit_required( session_obj ) == TRUE )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "cvp_topology_commit_cmd_ctrl(): set goal RE-INIT" );
          rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
          if ( rc ) return APR_EOK;
        }
        else
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
		        "cvp_topology_commit_cmd_ctrl(): Re-init not needed" );
          session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_NONE;
          rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
          CVP_COMM_ERROR( rc, client_addr );
          return APR_EOK;
        }
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
      "cvp_topology_commit_cmd_ctrl(): Invalid state %d, topo_commit_state = %d",
                                              session_obj->session_ctrl.state,
                                              session_obj->topo_commit_state );
      if ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_SET_DEVICE )
        session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_NONE;
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }
  }
  else
  {
    if ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_SET_DEVICE )
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK )
      {
        session_obj->is_vocproc_config_changed = TRUE;
        session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_NONE;
        return APR_EOK;
      }
    }
    else
    {
    /* Wait until the job is done. */
    if ( ctrl->session_obj->session_ctrl.goal_completed )
    {
      /* NOTE: The shared memory related actions fails in SIM mode causing
       *       CVP creation failure. We opt not to check for shared memory
       *       errors now since the PC test code currently has no support for
       *       shared memory operations nor remote shared memory operations.
       */

      if ( ctrl->session_obj->session_ctrl.goal_status )
      {
        rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
        CVP_PANIC_ON_ERROR( rc );

        rc = cvp_destroy_vpm_session( session_obj, ctrl );

        /* Respond with failure. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle,
                                      ctrl->packet,
                                      ctrl->session_obj->session_ctrl.goal_status );
        CVP_COMM_ERROR( rc, client_addr );

        CVP_PANIC_ON_ERROR( rc );
        session_obj->session_ctrl.state = CVP_STATE_ENUM_RESET;
      }
      else
      {
        cvp_add_session( &cvp_info, ctrl->session_obj );

        /* Respond success. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );

        ctrl->session_obj->session_ctrl.goal_completed = FALSE;
        ctrl->session_obj->session_ctrl.goal_status = APR_UNDEFINED_ID_V;
        ctrl->session_obj = APR_NULL_V;
        session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_NONE;
      }
      return APR_EOK;
    }
    }
  }

  rc = cvp_state_control( session_obj );

  return APR_EPENDING;
}


static int32_t cvp_create_full_session_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* TODO: If a session with the same name as this one that already has
     * a full control client exists, reject this command. */

    rc = cvp_construct_session( &session_obj, ctrl );
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvp_helper_create_new_goal_control( ctrl, session_obj,
                                             CVP_GOAL_ENUM_CREATE );
    if ( rc ) return APR_EOK;
  }
  else
  {
    /* Wait until the job is done. */
    if ( ctrl->session_obj->session_ctrl.goal_completed )
    {
#ifndef WINSIM
      /* NOTE: The shared memory related actions fails in SIM mode causing
       *       CVP creation failure. We opt not to check for shared memory
       *       errors now since the PC test code currently has no support for
       *       shared memory operations nor remote shared memory operations.
       */

      if ( ctrl->session_obj->session_ctrl.goal_status )
      {
        rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
        CVP_PANIC_ON_ERROR( rc );
        rc = cvp_destroy_vpm_session( session_obj, ctrl );
        /* Respond with failure and deallocate session obj. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle,
                                      ctrl->packet,
                                      ctrl->session_obj->session_ctrl.goal_status );
        CVP_COMM_ERROR( rc, client_addr );

        rc = cvp_free_object( ( cvp_object_t* ) ( ctrl->session_obj ) );
        CVP_PANIC_ON_ERROR( rc );

        ctrl->session_obj = APR_NULL_V;
      }
      else
#endif /* !WINSIM */
      {
        cvp_add_session( &cvp_info, ctrl->session_obj );

        /* Respond success. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );

        ctrl->session_obj->session_ctrl.goal_completed = FALSE;
        ctrl->session_obj->session_ctrl.goal_status = APR_UNDEFINED_ID_V;
        ctrl->session_obj = APR_NULL_V;
      }

      return APR_EOK;
    }
  }

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  CVP_PANIC_ON_ERROR( rc );

  rc = cvp_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t cvp_create_full_session_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_ivocproc_cmd_create_full_control_session_t* in_args;
  vss_ivocproc_cmd_create_full_control_session_v2_t create_session_v2;
  uint32_t payload_size;
  uint32_t session_name_size;
  aprv2_packet_t* new_packet;
  uint8_t* new_packet_payload;
  uint32_t new_packet_payload_size;
  uint32_t new_packet_payload_left;
  uint16_t client_addr;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
    if ( payload_size < sizeof( vss_ivocproc_cmd_create_full_control_session_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_create_full_session_cmd_ctrl(): Unexpected payload size, "
             "%d < %d", payload_size,
             sizeof( vss_ivocproc_cmd_create_full_control_session_t ) );

      client_addr = ctrl->packet->src_addr;

      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    in_args = APRV2_PKT_GET_PAYLOAD(
                vss_ivocproc_cmd_create_full_control_session_t, ctrl->packet );

    session_name_size = ( payload_size - sizeof( *in_args ) );

    /* Note that we are calling cvp_create_full_session_v2_cmd_ctrl in order
     * to leverage code. We create a new command packet and replace the
     * current command packet in the pending control with the new packet. The
     * new packet contains:
     * 1. vss_ivocproc_cmd_create_full_control_session_v2_t arguments (with the
     *    client specified vocproc properties and with vocproc_mode and
     *    ec_ref_port_id set to default values), and the session name provided
     *    by the client.
     * 2. The opcode for the new packet is the original packet's opcode (i.e.
     *    VSS_IVOCPROC_CMD_CREATE_FULL_CONTROL_SESSION), so that when
     *    vss_ivocproc_cmd_create_full_control_session_v2_t finishes processing
     *    the packet, we are using the correct opcode when sending a response
     *    back to the client.
     */

    create_session_v2.direction = in_args->direction;
    create_session_v2.tx_port_id = ( ( uint16_t ) in_args->tx_port_id );
    create_session_v2.tx_topology_id = in_args->tx_topology_id;
    create_session_v2.rx_port_id = ( ( uint16_t ) in_args->rx_port_id );
    create_session_v2.rx_topology_id = in_args->rx_topology_id;
    create_session_v2.profile_id = in_args->network_id;
    create_session_v2.vocproc_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;
    create_session_v2.ec_ref_port_id = VSS_IVOCPROC_PORT_ID_NONE;

    new_packet_payload_size = ( sizeof( create_session_v2 ) + session_name_size );

    rc = __aprv2_cmd_alloc_ext(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->dst_addr, ctrl->packet->dst_port,
           ctrl->packet->token, ctrl->packet->opcode,
           new_packet_payload_size , &new_packet );
    CVP_PANIC_ON_ERROR( rc );

    new_packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, new_packet );
    new_packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( new_packet->header );

    ( void ) mmstd_memcpy( new_packet_payload, new_packet_payload_left,
               &create_session_v2, sizeof( create_session_v2 ) );
    new_packet_payload_left -= sizeof( create_session_v2 );

    if ( session_name_size > 0 )
    {
      ( void ) mmstd_memcpy(
                 ( new_packet_payload + sizeof( create_session_v2 ) ), new_packet_payload_left,
                 ( ( ( uint8_t* ) in_args ) + sizeof( *in_args ) ), session_name_size );
    }

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    ctrl->packet = new_packet;
  }

  return cvp_create_full_session_v2_cmd_ctrl( ctrl );
}


static int32_t cvp_destroy_session_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  uint16_t client_addr;
  cvp_session_object_t* session_obj;

  client_addr = ctrl->packet->src_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
  if ( rc )
  {
    return APR_EOK;
  }

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( ( session_obj->session_ctrl.state != CVP_STATE_ENUM_RUN ) &&
         ( session_obj->attached_mvm_handle == APR_NULL_V ) )
    {
      /* Make sure disable and detach is called before creating
         the destroy goal.
         The check is only done once because disable/detach/destroy
         commands are processed in the same thread. In future, if they
         are separated into different threads, the check may be moved
         out and then incompleted goal shall be cleared.
      */
      rc = cvp_helper_create_new_goal_control( ctrl, session_obj,
                                               CVP_GOAL_ENUM_DESTROY );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_destroy_session_cmd_ctrl(): Failed to create new goal CVP_GOAL_ENUM_DESTROY, "
               "result: 0x%08X", rc );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
        CVP_COMM_ERROR( rc, client_addr );
        return APR_EOK;
      }
    }
    else
    {
      /* Fail the destroy Command if the CVP state machine is in RUN state
       * or CVP is still attached to MVM.
       * This guarantees that destroy command is not processed while the session
       * object resources may be accessed by other commands (from other threads)
       */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_destroy_session_cmd_ctrl(): cvp_state = %d, attached_mvm_handle = 0x%04x",
             session_obj->session_ctrl.state, session_obj->attached_mvm_handle );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_destroy_session_cmd_ctrl(): Cannot handle DESTROY CMD right now" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    /* Session object reference count is set to 1 during session creation
     * hence decrementing it. When reference count hits 0 its guaranteed
     * that no other command is accessing the session object.
     */
    apr_lock_enter( cvp_ref_cnt_lock );
    --session_obj->ref_cnt;
    apr_lock_leave( cvp_ref_cnt_lock );
  }

  /* Destroy Command is not processed until
   * session_obj is not accessed by any other command.
   */
  apr_lock_enter( cvp_ref_cnt_lock );
  if ( session_obj->ref_cnt > 0 )
  {
    apr_lock_leave( cvp_ref_cnt_lock );
    return APR_EPENDING;
  }
  apr_lock_leave( cvp_ref_cnt_lock );

  rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
  if ( rc == APR_EOK )
  {
    /* Remove session tracking from CVP info. */
    cvp_remove_session( &cvp_info, session_obj );
    rc = cvp_free_object( ( cvp_object_t* ) session_obj );
    CVP_PANIC_ON_ERROR( rc );
    return APR_EOK;
  }

  /**
   * TODO: On destroy session failure we need to keep the session object
   *       around because we need to continue the reset process. Aborted
   *       CVD session objects need to be queued somewhere for later
   *       destruction. We still have to run the session state control on the
   *       session object until the RESET is complete.
   *
   *       The current handling assumes the destroy operation from the
   *       lower layers are always successful.
   */

  /**
   * Run the CVD session state machine. The CVD session state machine should
   * only need to run when there are pending commands to process.
   */
  rc = cvp_state_control( session_obj );

  return APR_EPENDING;
}


static int32_t cvp_enable_cmd_ctrl (
  cvp_pending_control_t* ctrl,
  bool_t modem_enable
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    { /* Temporary hack. Refer to is_valid_afe_port_provided for details. */
      if ( ( modem_enable == FALSE ) &&
           ( session_obj->is_valid_afe_port_provided == FALSE ) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_enable_cmd_ctrl(): Failed to enable vocproc since valid "
             "AFE ports have not been provided." );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet,
                                      APR_EFAILED );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }

    if ( modem_enable )
    {
      session_obj->modem_enable = TRUE;
    }
    else
    {
      session_obj->public_enable = TRUE;
    }

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* TODO: Should we check if we have enough information to run?
       * At a minimum we need port IDs.
       */

      /* The vocproc will be enabled under the following scenerios:
       * 1. BACKWARD COMPATIBILITY: This vocproc is not attached to MVM, and
       *    client enables the vocproc. This is a deprecated behavior.
       * 2. This vocproc is attached to MVM. Both the client and MVM has issued
       *    enable command to this vocproc. There is no configuration change
       *    ever since the last time when VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
       *    has been processed.
       */
      if ( session_obj->public_enable && session_obj->modem_enable )
      {
        if ( ( session_obj->attached_mvm_handle != APR_NULL_V ) &&
             ( session_obj->is_vocproc_config_changed == TRUE ) )
        {
          /* If the vocproc is attached to MVM, and there is a configuration
           * change since the last time when VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG
           * has been processed, cancel the modem_enable flag, end the current
           * command and send VSS_IVOCPROC_EVT_RECONFIG to MVM.
           */
          session_obj->modem_enable = FALSE;

          rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
          CVP_COMM_ERROR( rc, client_addr );

          rc = __aprv2_cmd_alloc_send(
                 cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                 cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                 cvp_mvm_addr, session_obj->attached_mvm_handle,
                 0, VSS_IVOCPROC_EVT_RECONFIG,
                 NULL, 0 );
          CVP_COMM_ERROR( rc, cvp_mvm_addr );

          break;
        }

        rc = cvp_helper_create_new_goal_control(
               ctrl, session_obj, CVP_GOAL_ENUM_ENABLE );
        if ( rc ) break;
      }
      else
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK ) break;
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

static int32_t cvp_disable_cmd_ctrl (
  cvp_pending_control_t* ctrl,
  bool_t modem_disable
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  if ( modem_disable )
  {
    session_obj->modem_enable = FALSE;
  }
  else
  {
    session_obj->public_enable = FALSE;
  }

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    /* Either a public disable or modem disable has been requested. If in RUN
     * state, go to IDLE, otherwise do nothing. */
    if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
    {
      rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_DISABLE );
      if ( rc ) return APR_EOK;
    }
    else
    { /* Already disabled. Reply success. */
      client_addr = ctrl->packet->src_addr;

      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }
  else
  {
    rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
    if ( rc == APR_EOK ) return APR_EOK;
  }

  rc = cvp_state_control( session_obj );

  return APR_EPENDING;
}

static int32_t cvp_set_session_params (
  cvp_session_object_t* session_obj,
  cvp_pending_control_t* ctrl
)
{
  vss_ivocproc_cmd_set_device_v3_t* in_args;

  if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
  { /* Set device when the vocproc is running is not allowed. */
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "cvp_set_session_params(): Vocproc is running." );
    return APR_EFAILED;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_device_v3_t, ctrl->packet );

  switch ( session_obj->direction )
  {
  case VSS_IVOCPROC_DIRECTION_TX:
    {
      session_obj->target_set.tx_port_id = in_args->tx_port_id;
      session_obj->target_set.tx_topology_id = in_args->tx_topology_id;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        if ( session_obj->target_set.tx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.tx_port_id = AFE_PORT_ID_PSEUDOPORT_01;
        }
        else
        {
          session_obj->is_valid_afe_port_provided = TRUE;
        }
      }
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX:
    {
      session_obj->target_set.rx_port_id = in_args->rx_port_id;
      session_obj->target_set.rx_topology_id = in_args->rx_topology_id;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        if ( session_obj->target_set.rx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.rx_port_id = AFE_PORT_ID_PSEUDOPORT_02;
        }
        else
        {
          session_obj->is_valid_afe_port_provided = TRUE;
        }
      }
    }
    break;

  case VSS_IVOCPROC_DIRECTION_RX_TX:
    {
      session_obj->target_set.tx_port_id = in_args->tx_port_id;
      session_obj->target_set.tx_topology_id = in_args->tx_topology_id;
      session_obj->target_set.rx_port_id = in_args->rx_port_id;
      session_obj->target_set.rx_topology_id = in_args->rx_topology_id;
      session_obj->target_set.vocproc_mode = in_args->vocproc_mode;
      session_obj->target_set.ec_ref_port_id = in_args->ec_ref_port_id;

      { /*Temporary hack. Refer to is_valid_afe_port_provided for details. */
        session_obj->is_valid_afe_port_provided = TRUE;

        if ( session_obj->target_set.tx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.tx_port_id = AFE_PORT_ID_PSEUDOPORT_01;
        }

        if ( session_obj->target_set.rx_port_id == VSS_IVOCPROC_PORT_ID_NONE )
        {
          session_obj->is_valid_afe_port_provided = FALSE;
          session_obj->target_set.rx_port_id = AFE_PORT_ID_PSEUDOPORT_02;
        }
      }
    }
    break;

  default:
    CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_set_device_v3_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;
  uint32_t i;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->topo_commit_state != CVP_TOPOLOGY_COMMIT_STATE_NONE )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                   "cvp_set_device_v3_cmd_ctrl(): Unexpected topo_commit_state %d",
                   session_obj->topo_commit_state );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
        CVP_COMM_ERROR( rc, ctrl->packet->src_addr );
        break;
      }
      rc = cvp_helper_validate_payload_size_control(
             ctrl, sizeof( vss_ivocproc_cmd_set_device_v3_t ) );
      if ( rc ) break;

      rc = cvp_set_session_params( session_obj, ctrl );
      if ( rc )
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
      session_obj->topo_commit_state = CVP_TOPOLOGY_COMMIT_STATE_SET_DEVICE;
      /* Clear in-band topology params */
      session_obj->topo_param.num_dev_chans.rx_num_channels = 0;
      session_obj->topo_param.num_dev_chans.tx_num_channels = 0;
      /* Clear VPM topology params */
      for ( i = 0; i < CVP_MAX_NUM_TOPO_PARAMS; i++)
      {
        session_obj->topo_param.vpm_param[i].param_size = 0;
        session_obj->topo_param.vpm_param[i].param_virt_addr = NULL;
      }
    }
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_set_device_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      rc = cvp_helper_validate_payload_size_control(
             ctrl, sizeof( vss_ivocproc_cmd_set_device_v2_t ) );
      if ( rc ) break;

      rc = cvp_set_session_params( session_obj, ctrl );
      if ( rc )
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
      session_obj->is_client_set_sectors = FALSE;

      if ( cvp_is_reinit_required( session_obj ) == TRUE )
      {
        rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
        if ( rc ) break;
      }
      else
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );

      if ( rc == APR_EOK )
      {
        session_obj->is_vocproc_config_changed = TRUE;
        break;
      }
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

static int32_t cvp_set_device_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_ivocproc_cmd_set_device_t* in_args;
  vss_ivocproc_cmd_set_device_v2_t set_device_v2;
  aprv2_packet_t* new_packet;
  uint8_t* new_packet_payload;
  uint32_t new_packet_payload_left;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ivocproc_cmd_set_device_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD(
                vss_ivocproc_cmd_set_device_t, ctrl->packet );

    /* Note that we are calling cvp_set_device_v2_cmd_ctrl in order to leverage
     * code. We create a new command packet and replace the current command
     * packet in the pending control with the new packet. The new packet
     * contains:
     * 1. vss_ivocproc_cmd_set_device_v2_t arguments (with the client specified
     *    vocproc properties and with vocproc_mode and ec_ref_port_id set to
     *    default values).
     * 2. The opcode for the new packet is the original packet's opcode (i.e.
     *    VSS_IVOCPROC_CMD_SET_DEVICE), so that when cvp_set_device_v2_cmd_ctrl
     *    finishes processing the packet, we are using the correct opcode when
     *    sending a response back to the client.
     */

    set_device_v2.tx_port_id = ( ( uint16_t ) in_args->tx_port_id );
    set_device_v2.tx_topology_id = in_args->tx_topology_id;
    set_device_v2.rx_port_id = ( ( uint16_t ) in_args->rx_port_id );
    set_device_v2.rx_topology_id = in_args->rx_topology_id;
    set_device_v2.vocproc_mode = VSS_IVOCPROC_VOCPROC_MODE_EC_INT_MIXING;
    set_device_v2.ec_ref_port_id = VSS_IVOCPROC_PORT_ID_NONE;

    rc = __aprv2_cmd_alloc_ext(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->dst_addr, ctrl->packet->dst_port,
           ctrl->packet->token, ctrl->packet->opcode,
           sizeof( set_device_v2 ), &new_packet );
    CVP_PANIC_ON_ERROR( rc );

    new_packet_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, new_packet );
    new_packet_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE ( new_packet->header );

    ( void ) mmstd_memcpy( new_packet_payload, new_packet_payload_left, &set_device_v2, sizeof( set_device_v2 ) );

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    ctrl->packet = new_packet;
  }

  return cvp_set_device_v2_cmd_ctrl( ctrl );
}

static int32_t cvp_attach_stream_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_attach_stream_t* in_args;
  cvp_attached_stream_item_t* stream_item;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  vss_ivocproc_rsp_attach_stream_t rsp_attach_stream;
  uint32_t size;
  uint32_t i;
  bool_t is_already_attached = FALSE;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    /* Validate in-bound payload size. */
    size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
    if ( size != sizeof( vss_ivocproc_cmd_attach_stream_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_attach_stream_cmd_ctrl(): Unexpected payload size, %d != %d",
             size, sizeof( vss_ivocproc_cmd_attach_stream_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_attach_stream_t, ctrl->packet );

    /* Validate direction of attaching stream. */
    /* TODO: Currently we enforce that stream direction must exactly match
     * vocproc direction. This logic probably needs revision. */
    if ( in_args->direction != session_obj->direction )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_attach_stream_cmd_ctrl(): Unexpected direction, %d != %d",
             in_args->direction, session_obj->direction);
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    /* If not attaching a stream that has already been attached, store the new stream
     * handle and address and reply with VPM handle and direction. Otherwise reply
     * with APR_EALREADY. */
    pivot_node = &(session_obj->attached_stream_list.dummy);
    for ( i = 0; i < session_obj->attached_stream_list.size; i++ )
    {
      ( void ) apr_list_get_next( &session_obj->attached_stream_list,
                                  pivot_node, &cur_node );

      stream_item = (cvp_attached_stream_item_t*) cur_node;

      if ( stream_item->addr == ctrl->packet->src_addr &&
           stream_item->port == ctrl->packet->src_port )
      {
        is_already_attached = TRUE;
        break;
      }

      pivot_node = cur_node;
    }

    if ( is_already_attached == FALSE )
    {
      /* Store attaching stream handle and address. */
      stream_item = apr_memmgr_malloc( &cvp_heapmgr, sizeof( cvp_attached_stream_item_t ) );
      if ( stream_item == NULL )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_attach_stream_cmd_ctrl(): Failed to allocate "
             "attached_stream_item" );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_ENORESOURCE );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      stream_item->addr = ctrl->packet->src_addr;
      stream_item->port = ctrl->packet->src_port;

      ( void ) apr_list_init_node( ( apr_list_node_t* )stream_item );
      ( void ) apr_list_add_tail( &(session_obj->attached_stream_list),
                                  &(stream_item->link) );

      /* Respond with VPM handle and direction. */
      rsp_attach_stream.vdsp_session_handle = session_obj->vocproc_handle;
      rsp_attach_stream.direction = ( ( uint16_t ) session_obj->direction );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_attach_stream_cmd_ctrl(): VPM handle: 0x%04X, direction: %d",
             rsp_attach_stream.vdsp_session_handle, rsp_attach_stream.direction);

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             ctrl->packet->src_addr, ctrl->packet->src_port,
             ctrl->packet->token, VSS_IVOCPROC_RSP_ATTACH_STREAM,
             &rsp_attach_stream, sizeof( rsp_attach_stream ) );
      CVP_COMM_ERROR( rc, client_addr );

      /** If CVP is running, send EVT_READY to CVS. This is required
        * for the case if CVP is attached after CVP is enabled.
        * Ideally, CVP should be attached first and then enabled.
        */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = __aprv2_cmd_alloc_send(
                cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                ctrl->packet->src_addr, ctrl->packet->src_port,
                0, VSS_IVOCPROC_EVT_READY,
                NULL, 0 );
        CVP_COMM_ERROR( rc, client_addr );
      }

      /* Free incoming packet. */
      rc = __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
      CVP_PANIC_ON_ERROR( rc );
      break;
    }
    else
    {
      /* Respond with APR_EALREADY. */
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }
  }

  return APR_EOK;
}

static int32_t cvp_detach_stream_cmd_ctrl(
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  uint32_t i;
  cvp_session_object_t* session_obj;
  apr_list_node_t* pivot_node;
  apr_list_node_t* cur_node;
  cvp_attached_stream_item_t* stream_item;
  vss_ivocproc_rsp_detach_stream_t rsp_detach_stream;
  bool_t is_attached = FALSE;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    /* Look for this stream in the attached stream list and delete it. */
    pivot_node = &session_obj->attached_stream_list.dummy;
    for ( i = 0; i < session_obj->attached_stream_list.size; i++ )
    {
      ( void ) apr_list_get_next( &session_obj->attached_stream_list,
                                  pivot_node, &cur_node );

      stream_item = (cvp_attached_stream_item_t*) cur_node;

      if ( stream_item->addr == ctrl->packet->src_addr &&
           stream_item->port == ctrl->packet->src_port )
      {
        ( void ) apr_list_delete( &(session_obj->attached_stream_list),
                                  cur_node );

        is_attached = TRUE;
        apr_memmgr_free( &cvp_heapmgr, stream_item );

        break;
      }

      pivot_node = cur_node;
    }

    if ( is_attached == TRUE )
    {
      /* Respond with VPM handle and direction. */
      rsp_detach_stream.vdsp_session_handle = session_obj->vocproc_handle;
      rsp_detach_stream.direction = ( ( uint16_t ) session_obj->direction );

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "cvp_detach_stream_cmd_processing(): VPM handle: 0x%04X, "
             "direction: %d", rsp_detach_stream.vdsp_session_handle,
             rsp_detach_stream.direction );

      rc = __aprv2_cmd_alloc_send(
                cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
                session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                ctrl->packet->src_addr, ctrl->packet->src_port,
                ctrl->packet->token, VSS_IVOCPROC_RSP_DETACH_STREAM,
                &rsp_detach_stream, sizeof( rsp_detach_stream ) );
      CVP_COMM_ERROR( rc, client_addr );

      /* Free incoming packet. */
      rc = __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
      CVP_PANIC_ON_ERROR( rc );

    }
    else
    {
      /* Respond with APR_EALREADY. */
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
    }

    break;
  }

  return APR_EOK;
}

/* This function sets the volume index. The volume index is used to indexing
 * into the registered volume/dynamic calibration table. The corresponding
 * entry in the table is used for volume calibration. Volume setting will be
 * applied immediately( i.e. ramp duration is 0 ) when the session is in RUN
 * state.
 */
static int32_t cvp_set_rx_vol_index_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_set_rx_volume_index_t* in_args;
  uint32_t size;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Validate the inbound payload size. */
      size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
      if ( size != sizeof( vss_ivocproc_cmd_set_rx_volume_index_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_set_rx_vol_index_cmd_ctrl(): Unexpected size, %d != %d",
               size, sizeof( vss_ivocproc_cmd_set_rx_volume_index_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_rx_volume_index_t,
                                       ctrl->packet );

      if ( session_obj->volume_cal.is_registered == TRUE )
      { /* BACKWARD COMPATIBILITY */
        /* Validate payload. */
        if ( in_args->vol_index >= session_obj->volume_cal.num_vol_indices )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_set_rx_vol_index_cmd_ctrl(): Unexpected "
                 "vol_index(%d) > MAX(%d)", in_args->vol_index,
                 session_obj->volume_cal.num_vol_indices );
          rc = APR_EBADPARAM;
          break;
        }
      }

      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        /* Validate payload. */
        if ( in_args->vol_index >= session_obj->dynamic_cal.num_vol_indices )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_set_rx_vol_index_cmd_ctrl(): Unexpected "
                 "vol_index(%d) > MAX(%d)", in_args->vol_index,
                 session_obj->dynamic_cal.num_vol_indices );
          rc = APR_EBADPARAM;
          break;
        }
      }

      session_obj->target_set.vol_step = in_args->vol_index;

      /* Check if calibration is required. */
      if ( session_obj->target_set.vol_step == session_obj->active_set.vol_step )
      {
        /* No volume change. No calibration required. */
        rc = APR_EALREADY;
        break;
      }

      /* If in RUN state, calibrate for volume. Note that changing the volume
       * level at RUN time will not cause additional module to be
       * enabled/disabled (confimred with VCP team). This is because by design,
       * each volume level in the volume calibration table uses the same set of
       * module enable/disable parameters, and volume level change only changes
       * the gain-dependent parameter data values. Therefore, there will not be
       * any KPPS requirement change due to volume level change.
       *
       * If in IDLE state, leave settings in the target_set. They will be
       * picked up when we go to RUN (either upon receiving a
       * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command if the vocproc is attached
       * to MVM, or upon IDLE to RUN transition if the vocproc is not attached
       * to MVM).
       */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        if ( session_obj->volume_cal.is_registered == TRUE )
        { /* BACKWARD COMPATIBILITY */
          rc = cvp_calibrate_volume( session_obj, 0, cvp_calibrate_volume_rsp_fn );
          if ( rc )
          {
            rc = APR_EFAILED;
            break;
          }
          else
          { /* Commands have been sent, wait for response. */
            return APR_EPENDING;
          }
        }
        else
        if ( session_obj->dynamic_cal.is_registered == TRUE )
        {
          rc = cvp_calibrate_dynamic( session_obj, 0, cvp_calibrate_dynamic_rsp_fn );
          if ( rc )
          {
            rc = APR_EFAILED;
            break;
          }
          else
          { /* Commands have been sent, wait for response. */
            return APR_EPENDING;
          }
        }
        else
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_set_rx_vol_index_cmd_ctrl(): Calibration not registered." );
          rc = APR_EFAILED;
          break;
        }
      }
      else
      {
        session_obj->volume_cal.is_calibrate_needed = TRUE; /* BACKWARD COMPATIBILITY */
        session_obj->dynamic_cal.is_calibrate_needed = TRUE;
        session_obj->is_vocproc_config_changed = TRUE;
        rc = APR_EOK;
        break;
      }
    }
    else
    { /* Wait for response. */
      if ( session_obj->volume_cal.is_registered == TRUE )
      { /* BACKWARD COMPATIBILITY */
        if ( session_obj->volume_cal.set_param_rsp_cnt ==
             ( session_obj->volume_cal.num_matching_entries + 1 ) )
        {
          rc = APR_EOK;
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        if ( session_obj->dynamic_cal.set_param_rsp_cnt ==
             session_obj->dynamic_cal.num_set_param_issued )
        {
          rc = APR_EOK;
          ( void ) cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      {
        rc = APR_EFAILED;
        break;
      }
    }
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

/* This function sets the total number of volume steps. When the number
 * of volume steps is greater than the number of indicies in the volume
 * calibration table, interpolation is required.
 */
static int32_t cvp_set_number_of_volume_steps_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivolume_cmd_set_number_of_steps_t* in_args;
  uint32_t size;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  /* Validate the inbound payload size. */
  size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
  if ( size != sizeof( vss_ivolume_cmd_set_number_of_steps_t ) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_set_number_of_volume_steps_cmd_ctrl(): Unexpected size, "
           "%d != %d", size, sizeof( vss_ivolume_cmd_set_number_of_steps_t ) );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  in_args = APRV2_PKT_GET_PAYLOAD( vss_ivolume_cmd_set_number_of_steps_t, ctrl->packet );

  if ( session_obj->volume_cal.is_registered == TRUE )
  { /* BACKWARD COMPATIBILITY */
    /* Validate payload. */
    if ( in_args->value < session_obj->volume_cal.num_vol_indices )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_number_of_volume_steps_cmd_ctrl(): Invalid number of "
             "volume steps %d < min %d", in_args->value,
             session_obj->volume_cal.num_vol_indices );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }

  if ( session_obj->dynamic_cal.is_registered == TRUE )
  {
    /* Validate payload. */
    if ( in_args->value < session_obj->dynamic_cal.num_vol_indices )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_number_of_volume_steps_cmd_ctrl(): Invalid number of "
             "volume steps %d < min %d", in_args->value,
             session_obj->dynamic_cal.num_vol_indices );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
  }

  session_obj->target_set.client_num_vol_steps = in_args->value;
  session_obj->is_client_set_num_vol_steps = TRUE;

  if ( session_obj->active_set.client_num_vol_steps !=
       session_obj->target_set.client_num_vol_steps )
  {
    session_obj->volume_cal.is_calibrate_needed = TRUE; /* BACKWARD COMPATIBILITY */
    session_obj->dynamic_cal.is_calibrate_needed = TRUE;
    session_obj->is_vocproc_config_changed = TRUE;
  }

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

/* This function sets the volume step. When the number of volume
 * steps is greater than the number of indicies in the volume
 * calibration table, volume interpolation is required. Volume
 * setting will be applied gradually during the ramp_duration_ms.
 */
static int32_t cvp_set_volume_step_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivolume_cmd_set_step_t* in_args;
  uint32_t size;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Volume/dynamic calibration table has not been registered yet. */
      if ( ( session_obj->volume_cal.is_registered == FALSE ) &&
           ( session_obj->dynamic_cal.is_registered == FALSE ) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_set_volume_step_cmd_ctrl(): Volume or dynamic cal table is "
             "not registered yet." );
        rc = APR_EFAILED;
        break;
      }

      /* Validate the inbound payload size. */
      size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
      if ( size != sizeof( vss_ivolume_cmd_set_step_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_set_volume_step_cmd_ctrl(): Unexpected size, %d != %d",
               size, sizeof( vss_ivolume_cmd_set_step_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      in_args = APRV2_PKT_GET_PAYLOAD( vss_ivolume_cmd_set_step_t, ctrl->packet );

      /* Validate payload. */
      if ( ( in_args->direction != VSS_IVOLUME_DIRECTION_RX ) ||
           ( in_args->value >= session_obj->target_set.client_num_vol_steps ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_set_volume_step_cmd_ctrl(): Bad parameter: Volume "
               "direction = %d step number = %d", in_args->direction,
               in_args->value );
        rc = APR_EBADPARAM;
        break;
      }

      session_obj->target_set.vol_step = in_args->value;

      /* Check if calibration is required. */
      if ( ( session_obj->target_set.vol_step == session_obj->active_set.vol_step ) &&
           ( session_obj->target_set.client_num_vol_steps == session_obj->active_set.client_num_vol_steps ) )
      {
        /* No volume change. No calibration required. */
        rc = APR_EALREADY;
        break;
      }

      /* If in RUN state, calibrate for volume. Note that changing the volume
       * level at RUN time will not cause additional module to be
       * enabled/disabled (confimred with VCP team). This is because by design,
       * each volume level in the volume calibration table uses the same set of
       * module enable/disable parameters, and volume level change only changes
       * the gain-dependent parameter data values. Therefore, there will not be
       * any KPPS requirement change due to volume level change.
       *
       * If in IDLE state, leave settings in the target_set. They will be
       * picked up when we go to RUN (either upon receiving a
       * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command if the vocproc is attached
       * to MVM, or upon IDLE to RUN transition if the vocproc is not attached
       * to MVM).
       */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        if ( session_obj->volume_cal.is_registered == TRUE )
        { /* BACKWARD COMPATIBILITY */
          rc = cvp_calibrate_volume(
                 session_obj, in_args->ramp_duration_ms, cvp_calibrate_volume_rsp_fn );
          if ( rc )
          {
            rc = APR_EFAILED;
            break;
          }
          else
          { /* Commands have been sent, wait for response. */
            return APR_EPENDING;
          }
        }
        else
        if ( session_obj->dynamic_cal.is_registered == TRUE )
        {
          rc = cvp_calibrate_dynamic(
                 session_obj, in_args->ramp_duration_ms, cvp_calibrate_dynamic_rsp_fn );
          if ( rc )
          {
            rc = APR_EFAILED;
            break;
          }
          else
          { /* Commands have been sent, wait for response. */
            return APR_EPENDING;
          }
        }
      }
      else
      {
        session_obj->volume_cal.is_calibrate_needed = TRUE; /* BACKWARD COMPATIBILITY */
        session_obj->dynamic_cal.is_calibrate_needed = TRUE;
        session_obj->is_vocproc_config_changed = TRUE;
        rc = APR_EOK;
        break;
      }
    }
    else
    { /* Wait for response. */
      if ( session_obj->volume_cal.is_registered == TRUE )
      { /* BACKWARD COMPATIBILITY */
        if ( session_obj->volume_cal.set_param_rsp_cnt ==
             ( session_obj->volume_cal.num_matching_entries + 1 ) )
        {
          rc = APR_EOK;
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        if ( session_obj->dynamic_cal.set_param_rsp_cnt ==
             session_obj->dynamic_cal.num_set_param_issued )
        {
          rc = APR_EOK;
          ( void ) cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
    }
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

/* This function applies mute setting immediately( i.e. ramp duration is 0 ). */
static int32_t cvp_set_mute_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_set_mute_t* in_args;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Validate the inbound payload size. */
      rc = cvp_helper_validate_payload_size_control(
             ctrl, sizeof( vss_ivocproc_cmd_set_mute_t ) );
      if ( rc ) break;

      in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_mute_t, ctrl->packet );

      /* Validate payload content. */
      if ( ( in_args->direction != VSS_IVOCPROC_DIRECTION_TX &&
             in_args->direction != VSS_IVOCPROC_DIRECTION_RX &&
             in_args->direction != VSS_IVOCPROC_DIRECTION_RX_TX ) ||
           ( in_args->mute_flag != CVP_MUTE_ENABLE &&
             in_args->mute_flag != CVP_MUTE_DISABLE ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_set_mute_cmd_ctrl(): Unexpected params, direction: %d, "
               "mute_flag: %d", in_args->direction, in_args->mute_flag );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      if ( ctrl->packet->src_addr == cvp_mvm_addr )
      {
        switch ( in_args->direction )
        {
          case VSS_IVOCPROC_DIRECTION_TX:
            session_obj->target_set.mvm_tx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          case VSS_IVOCPROC_DIRECTION_RX:
            session_obj->target_set.mvm_rx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          case VSS_IVOCPROC_DIRECTION_RX_TX:
            session_obj->target_set.mvm_tx_mute = ( ( bool_t ) in_args->mute_flag );
            session_obj->target_set.mvm_rx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          default:
            CVP_PANIC_ON_ERROR( rc );
            break;
        }
      }
      else /* Client address. */
      {
        switch ( in_args->direction )
        {
          case VSS_IVOCPROC_DIRECTION_TX:
            session_obj->target_set.client_tx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          case VSS_IVOCPROC_DIRECTION_RX:
            session_obj->target_set.client_rx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          case VSS_IVOCPROC_DIRECTION_RX_TX:
            session_obj->target_set.client_tx_mute = ( ( bool_t ) in_args->mute_flag );
            session_obj->target_set.client_rx_mute = ( ( bool_t ) in_args->mute_flag );
            break;

          default:
            CVP_PANIC_ON_ERROR( rc );
            break;
        }
      }

      if ( ( session_obj->active_set.mvm_tx_mute == session_obj->target_set.mvm_tx_mute ) &&
           ( session_obj->active_set.mvm_rx_mute == session_obj->target_set.mvm_rx_mute ) &&
           ( session_obj->active_set.client_tx_mute == session_obj->target_set.client_tx_mute ) &&
           ( session_obj->active_set.client_rx_mute == session_obj->target_set.client_rx_mute ) )
      {
        /* No change in mute settings. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      session_obj->target_set.mute_ramp_duration = 0;

      /* If in RUN state, create new goal to set mute on the VDSP.
       * If in IDLE state, leave settings in the target_set.
       * They will be picked up when we go to RUN.
       */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_SET_MUTE );
        if ( rc ) break;
      }
      else
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK ) break;
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

/* This function applies mute setting gradually over the specified ramp duration. */
static int32_t cvp_volume_mute_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivolume_cmd_mute_v2_t* in_args;
  vss_icommon_evt_voice_activity_update_t vactivity;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Validate the inbound payload size. */
      rc = cvp_helper_validate_payload_size_control(
             ctrl, sizeof( vss_ivolume_cmd_mute_v2_t ) );
      if ( rc ) break;

      in_args = APRV2_PKT_GET_PAYLOAD( vss_ivolume_cmd_mute_v2_t, ctrl->packet );

      /* Validate payload content. */
      if ( ( ( in_args->direction != VSS_IVOLUME_DIRECTION_TX ) &&
             ( in_args->direction != VSS_IVOLUME_DIRECTION_RX ) ) ||
           ( ( in_args->mute_flag != VSS_IVOLUME_MUTE_OFF ) &&
             ( in_args->mute_flag != VSS_IVOLUME_MUTE_ON ) ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_volume_mute_v2_cmd_ctrl(): Unexpected params, "
               " direction: %d, mute_flag: %d", in_args->direction,
               in_args->mute_flag );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      switch ( in_args->direction )
      {
        case VSS_IVOLUME_DIRECTION_TX:
          session_obj->target_set.client_tx_mute = ( ( bool_t ) in_args->mute_flag );
          break;

        case VSS_IVOLUME_DIRECTION_RX:
          session_obj->target_set.client_rx_mute = ( ( bool_t ) in_args->mute_flag );
          break;

        default:
          CVP_PANIC_ON_ERROR( rc );
          break;
      }

      {
        /* Publish this UI MUTE/UNMUTE voice activity. */
        if( TRUE == session_obj->target_set.client_tx_mute )
        {
          vactivity.activity = VSS_ICOMMON_VOICE_ACTIVITY_UI_VOCPROC_TX_MUTE;
        }
        else
        {
          vactivity.activity = VSS_ICOMMON_VOICE_ACTIVITY_UI_VOCPROC_TX_UNMUTE;
        }

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
               cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvp_mvm_addr, session_obj->attached_mvm_handle,
               0, VSS_ICOMMON_EVT_VOICE_ACTIVITY_UPDATE,
               &vactivity, sizeof( vactivity ) );
        CVP_COMM_ERROR( rc, cvp_mvm_addr );
      }

      if ( ( session_obj->active_set.client_tx_mute == session_obj->target_set.client_tx_mute ) &&
           ( session_obj->active_set.client_rx_mute == session_obj->target_set.client_rx_mute ) )
      {
        /* No change in mute settings. */
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      session_obj->target_set.mute_ramp_duration = in_args->ramp_duration_ms;

      /* If in RUN state, create new goal to set mute on the VDSP.
       * If in IDLE state, leave settings in the target_set.
       * They will be picked up when we go to RUN.
       */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_SET_MUTE );
        if ( rc ) break;
      }
      else
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK ) break;
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

static int32_t cvp_vpcm_start_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivpcm_cmd_start_v2_t* payload;
  vss_ivpcm_tap_point_t* tap_points;
  voice_tap_point_v2_t dst_tap_point;
  uint32_t num_tap_points;
  uint32_t payload_len;
  uint32_t expected_payload_len;
  uint32_t tap_point_index;
  uint32_t target_payload_len;
  aprv2_packet_t* dst_packet;
  uint8_t* dst_payload;
  uint32_t dst_payload_left;
  uint32_t cvd_mem_handle;
  uint32_t vpm_mem_handle;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Only one START command is allowed. */
      if ( session_obj->vpcm_info.is_enabled == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_start_v2_cmd_ctrl(): VPCM already started" );
        rc = APR_EALREADY;
        break;
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_cmd_start_v2_t, ctrl->packet );
      payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_len < sizeof( vss_ivpcm_cmd_start_v2_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_start_v2_cmd_ctrl(): Unexpected payload size, %d < %d",
               payload_len, sizeof( vss_ivpcm_cmd_start_v2_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      num_tap_points = payload->num_tap_points;

      if ( num_tap_points == 0 )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_start_v2_cmd_ctrl(): Invalid num tap points(0)" );
        rc = APR_EBADPARAM;
        break;
      }

      /* expected_payload_len = ( sizeof( mem_handle ) + sizeof( num_tap_points ) +
       *                        ( num_tap_points * sizeof( each tap point ) ) ).
       */
      expected_payload_len = ( sizeof( uint32_t ) + sizeof( uint32_t ) +
                               ( num_tap_points * sizeof( vss_ivpcm_tap_point_t ) ) );

      if ( payload_len != expected_payload_len )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "cvp_vpcm_start_v2_cmd_ctrl(): Invalid data. Payload len: %d, "
              "expected len: %d", payload_len, expected_payload_len );
        rc = APR_EBADPARAM;
        break;
      }

      cvd_mem_handle = payload->mem_handle;

      rc = cvd_mem_mapper_validate_handle( cvd_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_start_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X",
               cvd_mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      ( void ) cvd_mem_mapper_get_vpm_mem_handle( cvd_mem_handle, &vpm_mem_handle );

      tap_points = ( ( vss_ivpcm_tap_point_t* )
                     ( ( ( uint8_t* ) payload ) + sizeof( vss_ivpcm_cmd_start_v2_t ) ) );

      /* target_payload_len = ( sizeof( num_tap_points ) +
                                ( num_tap_points * sizeof( each tap point ) ) ). */
      target_payload_len = ( sizeof( uint32_t ) +
                             ( num_tap_points * sizeof( voice_tap_point_v2_t ) ) );

      rc = cvp_create_simple_job_object(
             session_obj->header.handle,
             ( ( cvp_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVP_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_ext(
            cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
            session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
            cvp_vpm_addr, session_obj->vocproc_handle,
            ctrl->pendjob_obj->header.handle, VOICE_CMD_START_HOST_PCM_V2,
            target_payload_len, &dst_packet );
      CVP_PANIC_ON_ERROR( rc );

      dst_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, dst_packet );
      dst_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( dst_packet->header );

      /* Copy number of tap points. */
      ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &num_tap_points, sizeof( num_tap_points ) );
      dst_payload_left -= sizeof( num_tap_points );
      dst_payload += sizeof( uint32_t );

      for ( tap_point_index = 0; tap_point_index < num_tap_points; ++tap_point_index )
      {
        switch ( tap_points->tap_point )
        {
        case VSS_IVPCM_TAP_POINT_TX_DEFAULT:
          dst_tap_point.tap_point = VOICEPROC_MODULE_TX;
          break;

        case VSS_IVPCM_TAP_POINT_RX_DEFAULT:
          dst_tap_point.tap_point = VOICEPROC_MODULE_RX;
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_vpcm_start_v2_cmd_ctrl(): Invalid tap point: 0x%8X",
                 tap_points->tap_point );
          /* Free the allocated packet. */
          ( void ) __aprv2_cmd_free( cvp_apr_handle, dst_packet );
          rc = APR_EBADPARAM;
          break;
        }

        if ( rc ) break;

        switch ( tap_points->direction )
        {
        case VSS_IVPCM_TAP_POINT_DIR_OUT:
          dst_tap_point.direction = VOICE_HOST_PCM_READ;
          break;

        case VSS_IVPCM_TAP_POINT_DIR_IN:
          dst_tap_point.direction = VOICE_HOST_PCM_WRITE;
          break;

        case VSS_IVPCM_TAP_POINT_DIR_OUT_IN:
          dst_tap_point.direction = ( VOICE_HOST_PCM_READ | VOICE_HOST_PCM_WRITE );
          break;

        default:
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "cvp_vpcm_start_v2_cmd_ctrl(): Invalid dir: %d",
                 tap_points->direction );
          /* Free the allocated packet. */
          ( void ) __aprv2_cmd_free( cvp_apr_handle, dst_packet );
          rc = APR_EBADPARAM;
          break;
        }

        if ( rc ) break;

        dst_tap_point.sampling_rate = tap_points->sampling_rate;
        dst_tap_point.duration_ms = tap_points->duration;
        dst_tap_point.reserved = 0;
        dst_tap_point.mem_map_handle = vpm_mem_handle;

        /* Copy each tap point structure to destination payload. */
        ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &dst_tap_point, sizeof( dst_tap_point ) );
        dst_payload_left -= sizeof( dst_tap_point );
        dst_payload += sizeof( dst_tap_point );
        /* Advance to the next tap point. */
        tap_points += 1;
      }

      if ( rc ) break;

      rc = __aprv2_cmd_forward( cvp_apr_handle, dst_packet );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }
    else
      if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
    {
      uint32_t status = ctrl->pendjob_obj->simple_job.status;

      if ( status == APR_EOK )
      { /* Store the addr and handle of the client on start command success. */
        /* Write access to vpcm_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->vpcm_info.client_addr = ctrl->packet->src_addr;
        session_obj->vpcm_info.client_handle = ctrl->packet->src_port;
        session_obj->vpcm_info.is_enabled = TRUE;
        payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_cmd_start_v2_t, ctrl->packet );
        session_obj->vpcm_info.mem_handle = payload->mem_handle;
        ( void ) apr_lock_leave( session_obj->lock );
      }

      ( void ) cvp_free_object( ctrl->pendjob_obj );

      rc = status;
      break;
    }

    return APR_EPENDING;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_vpcm_stop_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Only one STOP command is allowed. */
      if ( session_obj->vpcm_info.is_enabled == FALSE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_stop_cmd_ctrl(): VPCM not started" );
        rc = APR_EALREADY;
        break;
      }

      rc = cvp_create_simple_job_object(
             session_obj->header.handle,
             ( ( cvp_simple_job_object_t** ) &ctrl->pendjob_obj ) );
      CVP_PANIC_ON_ERROR( rc );

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             ctrl->pendjob_obj->header.handle, VOICE_CMD_STOP_HOST_PCM,
             NULL, 0 );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );
    }
    else
    if ( ctrl->pendjob_obj->simple_job.is_completed == TRUE )
    {
      uint32_t status = ctrl->pendjob_obj->simple_job.status;

      if ( status == APR_EOK )
      {
        /* Write access to vpcm_info. */
        ( void ) apr_lock_enter( session_obj->lock );
        session_obj->vpcm_info.is_enabled = FALSE;
        session_obj->vpcm_info.client_addr = APRV2_PKT_INIT_ADDR_V;
        session_obj->vpcm_info.client_handle = APR_NULL_V;
        session_obj->vpcm_info.mem_handle = 0;
        ( void ) apr_lock_leave( session_obj->lock );
      }

      ( void ) cvp_free_object( ctrl->pendjob_obj );

      rc = status;
      break;
    }

    return APR_EPENDING;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_eval_cal_indexing_mem_size_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_eval_cal_indexing_mem_size_t* payload;
  uint32_t payload_len;
  cvd_virt_addr_t cal_virt_addr;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  vss_ivocproc_rsp_eval_cal_indexing_mem_size_t eval_cal_indexing_rsp;
  uint32_t required_index_mem_size;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Invalid session "
             "handle: 0x%04X, result: 0x%08X", ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_eval_cal_indexing_mem_size_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_ivocproc_cmd_eval_cal_indexing_mem_size_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Unexpected "
             "payload size, %d !< %d", payload_len,
             sizeof( vss_ivocproc_cmd_eval_cal_indexing_mem_size_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Invalid "
             "mem_handle: 0x%08X", payload->mem_handle );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Mis-aligned "
             "mem_address: lsw: 0x%08X msw: 0x%08X", ( uint32_t )payload->mem_address,
             ( uint32_t )( payload->mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                   payload->mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Memory is not within "
             "range, mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
             payload->mem_handle, ( uint32_t )payload->mem_address,
             ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle,
                                          payload->mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Cannot get virtual "
             "address, mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X",
             payload->mem_handle, ( uint32_t )payload->mem_address,
             ( uint32_t )( payload->mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.data_mem_handle = payload->mem_handle;
    cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
    cal_table_descriptor.size = payload->mem_size;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_istream_cmd_eval_cal_indexing_mem_size_t ) ) );

    rc = cvd_cal_eval_indexing_mem_size( &cal_table_descriptor,
                                         &required_index_mem_size );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_eval_cal_indexing_mem_size_cmd_ctrl(): Error in evaluating "
           "the cal table" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    eval_cal_indexing_rsp.mem_size = required_index_mem_size;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
           cvp_my_addr, ctrl->packet->dst_port,
           ctrl->packet->src_addr, ctrl->packet->src_port,
           ctrl->packet->token, VSS_IVOCPROC_RSP_EVAL_CAL_INDEXING_MEM_SIZE,
           &eval_cal_indexing_rsp, sizeof( eval_cal_indexing_rsp ) );
    CVP_COMM_ERROR( rc, client_addr );

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvp_register_calibration_data_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_calibration_data_v2_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_register_calibration_data_v2_cmd_ctrl(): Invalid session "
             "handle, dst_port: 0x%04X, result: 0x%08X", ctrl->packet->dst_port,
             rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_register_calibration_data_v2_cmd_ctrl(): Calibration data "
           "already registered" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_register_calibration_data_v2_cmd_ctrl(): Calibration already "
           "registered via static cal registration command." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head != NULL) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_register_calibration_data_v2_cmd_ctrl(): Feature Configuration "
           "Module in Device Config is incompatible. Removing Feature Config Data." );
      ( void ) cvp_clear_feature_config ( session_obj );
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_calibration_data_v2_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_ivocproc_cmd_register_calibration_data_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_register_calibration_data_v2_cmd_ctrl(): Unexpected payload "
             "size, %d !< %d", payload_len,
             sizeof( vss_ivocproc_cmd_register_calibration_data_v2_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v2_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v2_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v2_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                          payload->cal_mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v2_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_calibration_data_v2_cmd_ctrl(): cal memory virtual address 0x%016X, size %d",
                                            cal_virt_addr.ptr,
                                            payload->cal_mem_size );
    }

    ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->cal_mem_handle,
                                                &session_obj->common_cal.vpm_mem_handle );

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_ivocproc_cmd_register_calibration_data_v2_t ) ) );

    rc = cvd_cal_parse_table( 0, 0, &cal_table_descriptor,
                              &session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v2_cmd_ctrl(): Error in parsing calibration table" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->common_cal.is_registered = TRUE;
    session_obj->common_cal.is_calibrate_needed = TRUE;

    session_obj->is_vocproc_config_changed = TRUE;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Common Calibration table successfully. cvp_register_calibration_data_v2_cmd_ctrl(): Table size reported: %d",
                                           cal_table_descriptor.size );

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvp_register_calibration_data_v3_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_calibration_data_v3_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  cvd_virt_addr_t index_mem_virt_addr;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Invalid session handle, dst_port: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Calibration data already registered" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Calibration already registered via static cal registration command." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head != NULL) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Feature Configuration Module in Device Config is incompatible. Removing Feature Config Data." );
      ( void ) cvp_clear_feature_config ( session_obj );
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_calibration_data_v3_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_ivocproc_cmd_register_calibration_data_v3_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Unexpected payload size, %d !< %d",
                                              payload_len,
                                              sizeof( vss_ivocproc_cmd_register_calibration_data_v3_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v3_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                          payload->cal_mem_address,
                                          &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_calibration_data_v3_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_calibration_data_v3_cmd_ctrl(): cal memory virt addr 0x%016X, size %d",
                                            cal_virt_addr.ptr,
                                            payload->cal_mem_size );
    }

    ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->cal_mem_handle,
                                                &session_obj->common_cal.vpm_mem_handle );

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = payload->cal_indexing_mem_handle;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_ivocproc_cmd_register_calibration_data_v2_t ) ) );

    /* Map indexing memory. */
    if ( payload->cal_indexing_mem_handle != 0 )
    {
      rc = cvd_cal_eval_indexing_mem_size( &cal_table_descriptor,
                                           &session_obj->common_cal.required_index_mem_size );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Error in evaluating the cal table" );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      if ( payload->cal_indexing_mem_size < session_obj->common_cal.required_index_mem_size )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Unexpected cal index mem size %d < %d",
                                                payload->cal_indexing_mem_size,
                                                session_obj->common_cal.required_index_mem_size );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }

    if ( payload->cal_indexing_mem_handle != 0 )
    {
      rc = cvd_mem_mapper_validate_handle( payload->cal_indexing_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Invalid cal_indexing_mem_handle: 0x%08X",
                                                payload->cal_indexing_mem_handle );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_indexing_mem_address: 0x%016X",
                                                payload->cal_indexing_mem_address );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Mis-aligned cal_indexing_mem_size: %d",
                                                payload->cal_indexing_mem_size );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address,
                                                     payload->cal_indexing_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Memory is not within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d",
                                                payload->cal_indexing_mem_handle,
                                                payload->cal_indexing_mem_address,
                                                payload->cal_indexing_mem_size );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_indexing_mem_handle,
                                            payload->cal_indexing_mem_address,
                                            &index_mem_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Cannot get virtual address, mem_handle: 0x%08X, indexing memory addr: 0x%016X",
                                                payload->cal_indexing_mem_handle,
                                                payload->cal_indexing_mem_address );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_calibration_data_v3_cmd_ctrl(): indexing memory virt addr 0x%016X, size %d",
                                              index_mem_virt_addr.ptr,
                                              payload->cal_indexing_mem_size );
      }
    }

    rc = cvd_cal_parse_table( index_mem_virt_addr.ptr, payload->cal_indexing_mem_size,
                              &cal_table_descriptor,
                              &session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_calibration_data_v3_cmd_ctrl(): Error in parsing calibration table" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->common_cal.is_registered = TRUE;
    session_obj->common_cal.is_calibrate_needed = TRUE;

    session_obj->is_vocproc_config_changed = TRUE;

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Common Calibration table successfully. cvp_register_calibration_data_v3_cmd_ctrl(): Table size reported: %d",
                                           cal_table_descriptor.size );

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvp_deregister_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_calibration_data_cmd_ctrl(): Invalid session handle, dst_port: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->common_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_calibration_data_cmd_ctrl(): Calibration not registered" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table( session_obj->common_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_calibration_data_cmd_ctrl(): Failed to deregister calibration data" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
    }
    else
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
      CVP_COMM_ERROR( rc, client_addr );
    }

    session_obj->common_cal.is_registered = FALSE;
    session_obj->common_cal.is_calibrate_needed = FALSE;

    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvp_register_volume_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  int32_t rc1;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_volume_calibration_data_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  uint32_t min_vol_index;
  uint32_t max_vol_index;
  uint32_t checkpoint = 0;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->volume_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Calibration already registered" );
        rc = APR_EALREADY;
        break;
      }

      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Calibration already registered via dynamic cal registration command." );
        rc = APR_EALREADY;
        break;
      }

      if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head != NULL) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Feature Configuration Module in Device Config is incompatible. Removing Feature Config Data." );
        ( void ) cvp_clear_feature_config ( session_obj );
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_volume_calibration_data_t, ctrl->packet );
      payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_len < sizeof( vss_ivocproc_cmd_register_volume_calibration_data_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Unexpected payload size, %d !< %d",
                                                payload_len,
                                                sizeof( vss_ivocproc_cmd_register_volume_calibration_data_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                                payload->cal_mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_cmd_ctrl(): Mis-aligned cal_mem_address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                     payload->cal_mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_cmd_ctrl(): Memory is not within range,"
          " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                              payload->cal_mem_address,
                                              &cal_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
          ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_volume_calibration_data_cmd_ctrl(): cal memory virtual address 0x%016X, size %d",
                                            cal_virt_addr.ptr,
                                            payload->cal_mem_size );

      ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->cal_mem_handle,
                                                  &session_obj->volume_cal.vpm_mem_handle );

      ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

      cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
      cal_table_descriptor.size = payload->cal_mem_size;
      cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
      cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
      cal_table_descriptor.num_columns = payload->num_columns;
      cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                       ( ( ( uint8_t* ) payload ) +
                                         sizeof ( vss_ivocproc_cmd_register_volume_calibration_data_t ) ) );

      rc = cvd_cal_parse_table( 0, 0, &cal_table_descriptor,
                                &session_obj->volume_cal.table_handle );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): Error in parsing calibration table" );
        rc = APR_EFAILED;
        break;
      }
      checkpoint = 1;

      /* Determine volume indices. */
      /* Assumption: same volume indices for all networks. */
      rc = cvd_cal_find_min_max_column_value ( session_obj->volume_cal.table_handle,
                                        VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX, &min_vol_index, &max_vol_index );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): " \
                                              "Cannot find min/max volume indices" );
        rc = APR_EBADPARAM;
        break;
      }

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_volume_calibration_data_cmd_ctrl(): " \
                                            "min volume index %d, max volume index %d",
                                            min_vol_index, max_vol_index );

      session_obj->volume_cal.min_vol_index = min_vol_index;
      session_obj->volume_cal.max_vol_index = max_vol_index;
      session_obj->volume_cal.num_vol_indices = ( max_vol_index - min_vol_index + 1 );

      if ( session_obj->is_client_set_num_vol_steps == FALSE )
      {
        session_obj->target_set.client_num_vol_steps = session_obj->volume_cal.num_vol_indices;
      }

      rc = cvp_find_vol_table_format( session_obj, &session_obj->volume_cal.is_v1_format );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_register_volume_calibration_data_cmd_ctrl(): \
                                             Cannot find table format info" );
        session_obj->volume_cal.is_v1_format = FALSE; /* Assume v2 version if not able to determin. */
      }

      session_obj->volume_cal.is_registered = TRUE;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Volume Calibration table successfully. cvp_register_volume_calibration_data_cmd_ctrl(): Table size reported: %d",
                                             cal_table_descriptor.size );

      checkpoint = 2;

      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = cvp_calibrate_volume( session_obj, 0, cvp_calibrate_volume_rsp_fn );
        if ( rc )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_cmd_ctrl(): " \
                                                "volume not applied" );
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      {
        session_obj->volume_cal.is_calibrate_needed = TRUE;
        session_obj->is_vocproc_config_changed = TRUE;
      }
    }
    else
    { /* Wait for response. */
      if ( session_obj->volume_cal.set_param_rsp_cnt ==
           ( session_obj->volume_cal.num_matching_entries + 1 ) )
      {
        if ( session_obj->volume_cal.set_param_failed_rsp_cnt != 0 )
        {
          rc = APR_EFAILED;
          break;
        }
      }
      else
      {
        return APR_EPENDING;
      }
    }

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 2:
    session_obj->volume_cal.is_registered = FALSE;
    /*-fallthru */

  case 1:
    {
      rc1 = cvd_cal_discard_table( session_obj->volume_cal.table_handle );
      CVP_PANIC_ON_ERROR( rc1 );
    }
    /*-fallthru */

  default:
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
    }
    break;
  }

  return APR_EOK;
}

static int32_t cvp_register_volume_calibration_data_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  int32_t rc1;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_volume_calibration_data_v2_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  uint32_t min_vol_index;
  uint32_t max_vol_index;
  cvd_virt_addr_t index_mem_virt_addr;
  uint32_t checkpoint = 0;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->volume_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Calibration already registered" );
        rc = APR_EALREADY;
        break;
      }

      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Calibration already registered via dynamic cal registration command." );
        rc = APR_EALREADY;
        break;
      }

      if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head != NULL) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Feature Configuration Module in Device Config is incompatible. Removing Feature Config Data." );
        ( void ) cvp_clear_feature_config ( session_obj );
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_volume_calibration_data_v2_t, ctrl->packet );
      payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_len < sizeof( vss_ivocproc_cmd_register_volume_calibration_data_v2_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Unexpected payload size, %d !< %d",
                                                payload_len,
                                                sizeof( vss_ivocproc_cmd_register_volume_calibration_data_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                                payload->cal_mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Mis-aligned cal_mem_address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                     payload->cal_mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Memory is not within range,"
          " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                              payload->cal_mem_address,
                                              &cal_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
          ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): cal memory virtual address 0x%016X, size %d",
                                            cal_virt_addr.ptr,
                                            payload->cal_mem_size );

      ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->cal_mem_handle,
                                                  &session_obj->volume_cal.vpm_mem_handle );

      ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

      cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
      cal_table_descriptor.size = payload->cal_mem_size;
      cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
      cal_table_descriptor.indexing_mem_handle = payload->cal_indexing_mem_handle;
      cal_table_descriptor.num_columns = payload->num_columns;
      cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                       ( ( ( uint8_t* ) payload ) +
                                         sizeof ( vss_ivocproc_cmd_register_volume_calibration_data_t ) ) );
      /* Map indexing memory. */
      if ( payload->cal_indexing_mem_handle != 0 )
      {
        rc = cvd_cal_eval_indexing_mem_size( &cal_table_descriptor,
                                             &session_obj->volume_cal.required_index_mem_size );
        if ( rc )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Error in evaluating the cal table" );
          rc = APR_EFAILED;
          break;
        }

        if ( payload->cal_indexing_mem_size < session_obj->volume_cal.required_index_mem_size )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Unexpected cal index mem size %d < %d",
                                                  payload->cal_indexing_mem_size,
                                                  session_obj->volume_cal.required_index_mem_size );
          rc = APR_EBADPARAM;
          break;
        }
      }

      if ( payload->cal_indexing_mem_handle != 0 )
      {
        rc = cvd_mem_mapper_validate_handle( payload->cal_indexing_mem_handle );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Invalid cal_indexing_mem_handle: 0x%08X",
                                                  payload->cal_indexing_mem_handle );
          rc = APR_EHANDLE;
          break;
        }

        rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Mis-aligned cal_indexing_mem_address: 0x%016X",
                                                  payload->cal_indexing_mem_address );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_validate_attributes_align( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_size );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Mis-aligned cal_indexing_mem_size: %d",
                                                  payload->cal_indexing_mem_size );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_indexing_mem_handle, payload->cal_indexing_mem_address,
                                                       payload->cal_indexing_mem_size );
        if ( rc )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Memory is not within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d",
                                                  payload->cal_indexing_mem_handle,
                                                  payload->cal_indexing_mem_address,
                                                  payload->cal_indexing_mem_size );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_indexing_mem_handle,
                                                payload->cal_indexing_mem_address,
                                                &index_mem_virt_addr );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Cannot get virtual address, mem_handle: 0x%08X, indexing memory addr: 0x%016X",
                                                  payload->cal_indexing_mem_handle,
                                                  payload->cal_indexing_mem_address );
          rc = APR_EFAILED;
          break;
        }
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): indexing memory virtual address 0x%016X, size %d",
                                              index_mem_virt_addr.ptr,
                                              payload->cal_indexing_mem_size );
      }

      rc = cvd_cal_parse_table( index_mem_virt_addr.ptr, payload->cal_indexing_mem_size, &cal_table_descriptor,
                                &session_obj->volume_cal.table_handle );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): Error in parsing calibration table" );
        rc = APR_EFAILED;
        break;
      }
      checkpoint = 1;

      /* Determine volume indices. */
      /* Assumption: same number of volume indices for all networks. */
      rc = cvd_cal_find_min_max_column_value ( session_obj->volume_cal.table_handle,
                                        VSS_ICOMMON_CAL_COLUMN_VOLUME_INDEX, &min_vol_index, &max_vol_index );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): " \
                                              "Cannot find min/max volume indices" );
        rc = cvd_cal_discard_table( session_obj->volume_cal.table_handle );
        CVP_PANIC_ON_ERROR( rc );
        rc = APR_EBADPARAM;
        break;
      }

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): " \
                                            "min volume index %d, max volume index %d",
                                            min_vol_index, max_vol_index );

      session_obj->volume_cal.min_vol_index = min_vol_index;
      session_obj->volume_cal.max_vol_index = max_vol_index;
      session_obj->volume_cal.num_vol_indices = ( max_vol_index - min_vol_index + 1 );

      rc = cvp_find_vol_table_format( session_obj, &session_obj->volume_cal.is_v1_format );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): " \
                                             "Cannot find table format info" );
        session_obj->volume_cal.is_v1_format = FALSE; /* Assume v2 version if not able to determin. */
        rc = APR_EOK;
      }

      session_obj->volume_cal.is_registered = TRUE;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Registered Volume Calibration table successfully. cvp_register_volume_calibration_data_v2_cmd_ctrl(): Table size reported: %d",
                                             cal_table_descriptor.size );

      checkpoint = 2;

      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = cvp_calibrate_volume( session_obj, 0, cvp_calibrate_volume_rsp_fn );
        if ( rc )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_volume_calibration_data_v2_cmd_ctrl(): " \
                                                "volume not applied" );
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      {
        session_obj->volume_cal.is_calibrate_needed = TRUE;
        session_obj->is_vocproc_config_changed = TRUE;
      }
    }
    else
    { /* Wait for response. */
      if ( session_obj->volume_cal.set_param_rsp_cnt ==
           ( session_obj->volume_cal.num_matching_entries + 1 ) )
      {
        if ( session_obj->volume_cal.set_param_failed_rsp_cnt != 0 )
        {
          rc = APR_EFAILED;
          break;
        }
      }
      else
      {
        return APR_EPENDING;
      }
    }

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 2:
    session_obj->volume_cal.is_registered = FALSE;
    /*-fallthru */

  case 1:
    {
      rc1 = cvd_cal_discard_table( session_obj->volume_cal.table_handle );
      CVP_PANIC_ON_ERROR( rc1 );
    }
    /*-fallthru */

  default:
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
    }
    break;
  }

  return APR_EOK;
}

/* BACKWARD COMPATIBILITY */
static int32_t cvp_deregister_volume_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_volume_calibration_data_cmd_ctrl():  Invalid session handle, dst_port: 0x%04X, result: 0x%08X",
                                              ctrl->packet->dst_port, rc );
      break;
    }

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->volume_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_volume_calibration_data_cmd_ctrl(): Calibration not registered" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table( session_obj->volume_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_volume_calibration_data_cmd_ctrl(): Failed to deregister calibration data" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
    }
    else
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
      CVP_COMM_ERROR( rc, client_addr );
    }

    session_obj->volume_cal.is_registered = FALSE;
    session_obj->volume_cal.is_calibrate_needed = FALSE;

    break;
  }

  return APR_EOK;
}

static int32_t cvp_register_static_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_static_calibration_data_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  uint16_t client_addr;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_table_header_t log_info_table;

  bool_t is_mapped_memory_in_range = FALSE;
  /* Flag to indicate if memory range of mem_map command
   * and cal registration are in sync
   */

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->static_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): Calibration data already registered." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->common_cal.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): Calibration already registered via deprecated cal registration command." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head == NULL) )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): WideVoice Module in Device Config is incompatible with registration.  Resetting to Default Rx Sampling Rate." );
      session_obj->widevoice_rx_sr = CVP_DEFAULT_RX_PP_SR;

      if ( cvp_media_id_is_nb_sr( session_obj->active_set.system_config.media_id ) == TRUE )
      {
        if ( session_obj->active_set.system_config.rx_pp_sr > CVP_DEFAULT_RX_PP_SR )
        {
          session_obj->target_set.system_config.rx_pp_sr = CVP_DEFAULT_RX_PP_SR;
          session_obj->is_vocproc_config_changed = TRUE;
        }
      }
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_static_calibration_data_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_ivocproc_cmd_register_static_calibration_data_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): Unexpected payload size, %d < %d",
                                              payload_len,
                                              sizeof( vss_ivocproc_cmd_register_static_calibration_data_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                              payload->cal_mem_handle );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_static_calibration_data_cmd_ctrl(): Mis-aligned cal_mem_address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                   payload->cal_mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_static_calibration_data_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
        ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                             payload->cal_mem_address,
                                             &cal_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_static_calibration_data_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
        ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_static_calibration_data_cmd_ctrl(): cal memory virtual address 0x%016X, size %d",
                                            cal_virt_addr.ptr,
                                            payload->cal_mem_size );
    }

    is_mapped_memory_in_range = TRUE;
    ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

    cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
    cal_table_descriptor.size = payload->cal_mem_size;
    cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
    cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
    cal_table_descriptor.num_columns = payload->num_columns;
    cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                     ( ( ( uint8_t* ) payload ) +
                                       sizeof ( vss_ivocproc_cmd_register_static_calibration_data_t ) ) );

    rc = cvd_cal_parse_table_v2( &cal_table_descriptor,
                                 &session_obj->static_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_static_calibration_data_cmd_ctrl(): Error in parsing calibration table" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->static_cal.is_registered = TRUE;
    session_obj->static_cal.is_calibrate_needed = TRUE;

    session_obj->is_vocproc_config_changed = TRUE;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

    /* Log vocproc static cal table. */
  if ( is_mapped_memory_in_range == TRUE )
  {
    log_info_table.table_handle = session_obj->static_cal.table_handle;

    log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                            ( session_obj->header.handle ) );
    log_info.call_num = session_obj->target_set.system_config.call_num;
    log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_TABLE;
    log_info.data_container_header_size = sizeof( log_info_table );
    log_info.data_container_header = &log_info_table;
    log_info.payload_size = cal_table_descriptor.size;
    log_info.payload_buf = cal_table_descriptor.start_ptr;

    ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_STATIC_TABLE,
                              ( void* )&log_info, sizeof( log_info ) );
  }

  return APR_EOK;
}

static int32_t cvp_deregister_static_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->static_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_static_calibration_data_cmd_ctrl(): Calibration not registered." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table_v2( session_obj->static_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_static_calibration_data_cmd_ctrl(): Failed to deregister calibration data." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
    }
    else
    {
      session_obj->static_cal.is_registered = FALSE;
      session_obj->static_cal.is_calibrate_needed = FALSE;
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
      CVP_COMM_ERROR( rc, client_addr );
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvp_register_dynamic_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  int32_t rc1;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_dynamic_calibration_data_t* payload;
  uint32_t payload_len;
  cvd_cal_table_descriptor_t cal_table_descriptor;
  cvd_virt_addr_t cal_virt_addr;
  uint32_t min_vol_index;
  uint32_t max_vol_index;
  uint32_t checkpoint = 0;
  uint16_t client_addr;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_table_header_t log_info_table;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->dynamic_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): Calibration already registered." );
        rc = APR_EALREADY;
        break;
      }

      if ( session_obj->volume_cal.is_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): Calibration already registered via deprecated volume cal registration command." );
        rc = APR_EALREADY;
        break;
      }

      if ( (session_obj->is_device_config_registered == TRUE) && (session_obj->hdvoice_config_info.hdvoice_config_hdr.sys_config_list_head == NULL) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): WideVoice Module in Device Config is incompatible with registration.  Resetting to Default Rx Sampling Rate." );
        session_obj->widevoice_rx_sr = CVP_DEFAULT_RX_PP_SR;

        if ( cvp_media_id_is_nb_sr( session_obj->active_set.system_config.media_id ) == TRUE )
        {
          if ( session_obj->active_set.system_config.rx_pp_sr > CVP_DEFAULT_RX_PP_SR )
          {
            session_obj->target_set.system_config.rx_pp_sr = CVP_DEFAULT_RX_PP_SR;
            session_obj->is_vocproc_config_changed = TRUE;
          }
        }
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_dynamic_calibration_data_t, ctrl->packet );
      payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_len < sizeof( vss_ivocproc_cmd_register_dynamic_calibration_data_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): Unexpected payload size, %d < %d",
                                                payload_len,
                                                sizeof( vss_ivocproc_cmd_register_dynamic_calibration_data_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_handle( payload->cal_mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): Invalid cal_mem_handle: 0x%08X",
                                                payload->cal_mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->cal_mem_handle, payload->cal_mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_dynamic_calibration_data_cmd_ctrl(): Mis-aligned cal_mem_address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->cal_mem_handle, payload->cal_mem_address,
                                                     payload->cal_mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_dynamic_calibration_data_cmd_ctrl(): Memory is not within range,"
          " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->cal_mem_handle, ( uint32_t )payload->cal_mem_address,
          ( uint32_t )( payload->cal_mem_address >> 32 ), payload->cal_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->cal_mem_handle,
                                               payload->cal_mem_address,
                                               &cal_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_dynamic_calibration_data_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->cal_mem_handle,
          ( uint32_t )payload->cal_mem_address, ( uint32_t )( payload->cal_mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_dynamic_calibration_data_cmd_ctrl(): cal memory virtual address 0x%016X, size %d",
                                              cal_virt_addr.ptr,
                                              payload->cal_mem_size );
      }

      ( void ) cvd_mem_mapper_cache_invalidate_v2( &cal_virt_addr, payload->cal_mem_size );

      cal_table_descriptor.start_ptr = cal_virt_addr.ptr;
      cal_table_descriptor.size = payload->cal_mem_size;
      cal_table_descriptor.data_mem_handle = payload->cal_mem_handle;
      cal_table_descriptor.indexing_mem_handle = APR_NULL_V;
      cal_table_descriptor.num_columns = payload->num_columns;
      cal_table_descriptor.columns = ( ( cvd_cal_column_descriptor_t* )
                                       ( ( ( uint8_t* ) payload ) +
                                         sizeof ( vss_ivocproc_cmd_register_dynamic_calibration_data_t ) ) );

      rc = cvd_cal_parse_table_v2( &cal_table_descriptor,
                                   &session_obj->dynamic_cal.table_handle );

      /* Log vocproc dynamic cal table. */
      {
        log_info_table.table_handle = session_obj->dynamic_cal.table_handle;

        log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                              ( session_obj->header.handle ) );
        log_info.call_num = session_obj->target_set.system_config.call_num;
        log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_TABLE;
        log_info.data_container_header_size = sizeof( log_info_table );
        log_info.data_container_header = &log_info_table;
        log_info.payload_size = cal_table_descriptor.size;
        log_info.payload_buf = cal_table_descriptor.start_ptr;

        ( void )cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_VOCPROC_DYNAMIC_TABLE,
                                ( void* )&log_info, sizeof( log_info ) );
      }

      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): Error in parsing calibration table" );
        rc = APR_EFAILED;
        break;
      }
      checkpoint = 1;

      /* Determine volume indices. */
      /* Assumption: same volume indices for all networks, all vocoders and all
       * vocoder operating mode. */
      rc = cvd_cal_find_min_max_column_value_v2 (
             session_obj->dynamic_cal.table_handle,
             VSS_ICOMMON_CAL_COLUMN_RX_VOLUME_INDEX, &min_vol_index, &max_vol_index );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): " \
                                              "Cannot find min/max volume indices" );
        rc = APR_EBADPARAM;
        break;
      }

      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_dynamic_calibration_data_cmd_ctrl(): " \
                                            "min volume index %d, max volume index %d",
                                            min_vol_index, max_vol_index );

      session_obj->dynamic_cal.min_vol_index = min_vol_index;
      session_obj->dynamic_cal.max_vol_index = max_vol_index;
      session_obj->dynamic_cal.num_vol_indices = ( max_vol_index - min_vol_index + 1 );

      if ( session_obj->is_client_set_num_vol_steps == FALSE )
      {
        session_obj->target_set.client_num_vol_steps = session_obj->dynamic_cal.num_vol_indices;
      }

      rc = cvd_cal_find_vol_cal_format(
            session_obj->dynamic_cal.table_handle,
            &session_obj->dynamic_cal.is_v1_vol_format );
      if ( rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_register_dynamic_calibration_data_cmd_ctrl(): " \
                                             "Cannot find table format info" );
        session_obj->dynamic_cal.is_v1_vol_format = FALSE; /* Assume v2 version if not able to determine. */
      }

      session_obj->dynamic_cal.is_registered = TRUE;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: PVC: Registered Device Dynamic Calibration Table successfully. cvp_register_dynamic_calibration_data_cmd_ctrl(): Table size reported: %d",
                                             cal_table_descriptor.size );

      checkpoint = 2;

      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        rc = cvp_calibrate_dynamic( session_obj, 0, cvp_calibrate_dynamic_rsp_fn );
        if ( rc )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_dynamic_calibration_data_cmd_ctrl(): " \
                                                "dynamic cal not applied" );
          break;
        }
        else
        {
          return APR_EPENDING;
        }
      }
      else
      {
        session_obj->dynamic_cal.is_calibrate_needed = TRUE;
        session_obj->is_vocproc_config_changed = TRUE;
      }
    }
    else
    { /* Wait for response. */
      if ( session_obj->dynamic_cal.set_param_rsp_cnt ==
           session_obj->dynamic_cal.num_set_param_issued )
      {
        if ( session_obj->dynamic_cal.set_param_failed_rsp_cnt != 0 )
        {
          rc = APR_EFAILED;
          break;
        }
      }
      else
      {
        return APR_EPENDING;
      }
    }

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 2:
    session_obj->dynamic_cal.is_registered = FALSE;
    /*-fallthru */

  case 1:
    {
      rc1 = cvd_cal_discard_table_v2( session_obj->dynamic_cal.table_handle );
      CVP_PANIC_ON_ERROR( rc1 );
    }
    /*-fallthru */

  default:
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
    }
    break;
  }

  return APR_EOK;
}

static int32_t cvp_deregister_dynamic_calibration_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->dynamic_cal.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_dynamic_calibration_data_cmd_ctrl(): Calibration not registered." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    rc = cvd_cal_discard_table_v2( session_obj->dynamic_cal.table_handle );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_dynamic_calibration_data_cmd_ctrl(): Failed to deregister calibration data." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
    }
    else
    {
      session_obj->dynamic_cal.is_registered = FALSE;
      session_obj->dynamic_cal.is_calibrate_needed = FALSE;
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
      CVP_COMM_ERROR( rc, client_addr );
    }

    break;
  }

  return APR_EOK;
}

static int32_t cvp_register_device_config_cmd_ctrl  (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_register_device_config_t* payload;
  uint32_t payload_size;
  cvd_virt_addr_t device_config_base_virt_addr;
  cvp_device_config_entry_t* device_config_entry;
  uint16_t client_addr;
  uint32_t parsed_size = 0;
  uint32_t temp_wv_sr = 0;
  cvd_cal_log_commit_info_t log_info;
  cvd_cal_log_table_header_t log_info_table;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->is_device_config_registered == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): Device config data already registered" );
        rc = APR_EALREADY;
        break;
      }

      payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
      if ( payload_size != sizeof( vss_ivocproc_cmd_register_device_config_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): Unexpected payload size, %d != %d",
                                                payload_size,
                                                sizeof( vss_ivocproc_cmd_register_device_config_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_register_device_config_t, ctrl->packet );

      rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                                payload->mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_device_config_cmd_ctrl(): Mis-aligned mem address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                     payload->mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_device_config_cmd_ctrl(): Memory is not within range,"
          " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->mem_handle, ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle, payload->mem_address,
                                            &device_config_base_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_register_device_config_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
          ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }

      if ( payload->mem_size < sizeof( cvp_device_config_entry_t ) )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): Mem size is too small, %4d",
                                                payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      ( void ) cvd_mem_mapper_cache_invalidate_v2( &device_config_base_virt_addr, payload->mem_size );

      /* Log device config data. */
      {
        log_info_table.table_handle = 0;

        log_info.instance = ( ( session_obj->attached_mvm_handle << 16 ) |
                              ( session_obj->header.handle ) );
        log_info.call_num = session_obj->target_set.system_config.call_num;
        log_info.data_container_id = CVD_CAL_LOG_DATA_CONTAINER_TABLE;
        log_info.data_container_header_size = sizeof( log_info_table );
        log_info.data_container_header = &log_info_table;
        log_info.payload_size = payload->mem_size;
        log_info.payload_buf = device_config_base_virt_addr.ptr;

        ( void ) cvd_cal_log_data ( ( log_code_type )LOG_ADSP_CVD_CAL_DATA_C, CVD_CAL_LOG_DEVICE_CONFIG_TABLE,
                                ( void* )&log_info, sizeof( log_info ) );
      }
      do
      {
        device_config_entry =
          ( cvp_device_config_entry_t* ) ( ( ( uint8_t* ) device_config_base_virt_addr.ptr ) + parsed_size );

        if ( ( device_config_entry == NULL ) ||
             ( ( ( uint8_t* ) device_config_entry ) + sizeof( voice_param_data_t ) == NULL ) )
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): " \
                                                "Device Config Entry is NULL");
          rc = APR_EFAILED;
          break;
        }

        if ( ( ( ( ( uint8_t* ) device_config_entry ) + sizeof( cvp_device_config_entry_t ) ) > ( ( ( uint8_t* ) device_config_base_virt_addr.ptr ) + payload->mem_size ) ) ||
             ( ( ( ( uint8_t* ) device_config_entry ) + device_config_entry->param_size ) > ( ( ( uint8_t* ) device_config_base_virt_addr.ptr ) + payload->mem_size ) ) )
        {
          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): " \
                                                  "Device Config Entry out Virt Mem Bounds. " \
                                                  "entry_addr=0x%08x entry_size=0x%08x, virt_addr=0x%08x, virt_mem_size=0x%08x",
                                                  device_config_entry,
                                                  device_config_entry->param_size,
                                                  device_config_base_virt_addr.ptr,
                                                  payload->mem_size);
          rc = APR_EFAILED;
          break;
        }

        switch ( device_config_entry->module_id )
        {
        case VSS_MODULE_WIDEVOICE:
          {
            rc = cvp_parse_device_config_wv_mod ( session_obj, device_config_entry, &temp_wv_sr );
            break;
          }
        case VSS_MODULE_FEATURE_CONFIG:
          {
            rc = cvp_parse_device_config_feature_mod ( session_obj, device_config_entry );
            break;
          }
        case VSS_MODULE_SOUND_DEVICE:
          {
            rc = cvp_parse_sound_device_mod( session_obj, device_config_entry );
            break;
          }
        case VSS_MODULE_CLOCK_CONTROL:
          {
            rc = cvp_parse_clk_ctrl_mod( session_obj, device_config_entry );
            break;
          }
        default:
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_device_config_cmd_ctrl(): " \
                                                    "Invalid mod_id 0x%08X",
                                                    device_config_entry->module_id );
            break;
          }
        }

        parsed_size += ( device_config_entry->param_size + sizeof( voice_param_data_t ) );

        if ( rc )
        {
          /* Error in Parsing */
          break;
        }
      } while ( parsed_size < payload->mem_size );

      if ( rc )
      {
        /* Propagate errors from Device Config Parsing */
        break;
      }

      session_obj->is_device_config_registered = TRUE;

      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: PVC: Registered Device Configuration Table successfully. cvp_register_device_config_cmd_ctrl(): Table size reported: %d",
                                             payload->mem_size );

      ( void ) cvp_post_parse_device_config ( session_obj, temp_wv_sr );

      if ( cvp_is_reinit_required( session_obj ) == TRUE )
      {
        rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
        if ( rc ) break;
      }
      else
      {
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK )
      {
        return rc;
      }
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  /* If at least one module was successful, pass Device Config registration */
  if ( session_obj->is_device_config_registered == TRUE )
  {
    rc = APR_EOK;
  }
  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_deregister_device_config_cmd_ctrl  (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( session_obj->is_device_config_registered == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_deregister_device_config_cmd_ctrl(): Device config data not registered" );
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
    CVP_COMM_ERROR( rc, client_addr );
    return APR_EOK;
  }

  session_obj->widevoice_rx_sr = CVP_DEFAULT_RX_PP_SR;
  session_obj->is_device_config_registered = FALSE;

  ( void ) cvp_clear_feature_config( session_obj );
  ( void ) cvp_clear_sound_device_info( session_obj );
  ( void ) cvp_clear_clock_control_info( session_obj );

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_set_sampling_rate_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_set_sampling_rate_t* in_args;
  uint32_t requested_rx_sr;
  uint32_t requested_tx_sr;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ivocproc_cmd_set_sampling_rate_t ) );
    if ( rc ) break;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      { /* Setting sampling rate when the vocproc is running is not allowed. */
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_sampling_rate_cmd_ctrl(): Vocproc is running." );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_sampling_rate_t,
                                       ctrl->packet );

      requested_rx_sr = session_obj->target_set.system_config.rx_pp_sr;
      requested_tx_sr = session_obj->target_set.system_config.tx_pp_sr;

      switch ( session_obj->direction )
      {
      case VSS_IVOCPROC_DIRECTION_RX:
        requested_rx_sr = in_args->rx;
        break;

      case VSS_IVOCPROC_DIRECTION_TX:
        requested_tx_sr = in_args->tx;
        break;

      case VSS_IVOCPROC_DIRECTION_RX_TX:
        {
          requested_rx_sr = in_args->rx;
          requested_tx_sr = in_args->tx;
        }
        break;

      default:
        CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
        break;
      }

      if ( ( cvp_sr_is_valid( requested_rx_sr ) == FALSE ) ||
           ( cvp_sr_is_valid( requested_tx_sr ) == FALSE ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_sampling_rate_cmd_ctrl(): Invalid Rx or Tx sampling " \
                                                "rate, Rx = %d, Tx = %d",
                                                requested_rx_sr,
                                                requested_tx_sr );
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }

      session_obj->is_sr_set_by_client = TRUE;

      if ( ( session_obj->target_set.system_config.rx_pp_sr != requested_rx_sr ) ||
           ( session_obj->target_set.system_config.tx_pp_sr != requested_tx_sr ) )
      {
        session_obj->is_vocproc_config_changed = TRUE;

        /* Client settings take precedence over MVM settings. Active Set is
         * being updted in State: IDLE and Goal: REINIT
        */
        session_obj->target_set.system_config.rx_pp_sr = requested_rx_sr;
        session_obj->target_set.system_config.tx_pp_sr = requested_tx_sr;

        rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
        if ( rc ) break;
      }
      else
      {
        rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
        CVP_COMM_ERROR( rc, client_addr );
        break;
      }
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK )
      {
        break;
      }
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}


/* Search clock voting scale factors for the custom topology use cases.
 */
static int32_t cvp_get_clock_scale_factors (
  cvp_session_object_t* session_obj,
  uint32_t* tx_mpps_scale_factor,
  uint32_t* tx_bw_scale_factor,
  uint32_t* rx_mpps_scale_factor,
  uint32_t* rx_bw_scale_factor
)
{
  uint32_t use_case_idx;
  uint32_t num_clk_columns;
  uint32_t param_match_cnt;
  uint32_t max_param_match_cnt;
  vss_param_clock_control_params_t* clk_column;
  vss_param_clock_control_params_t default_vals = { VSS_IVOCPROC_DIRECTION_RX_TX,
                                                   VSS_PARAM_SAMPLING_RATE_ANY,
                                                   VSS_ICOMMON_CAL_NETWORK_ID_NONE,
                                                   VSS_MEDIA_ID_NONE,
                                                   0,
                                                   0 };
  cvp_clk_control_config_t* clk_table_ptr = &session_obj->clk_ctrl_table;
  vss_icommon_cmd_set_system_config_t* sys_config = &session_obj->active_set.system_config;

  /* Default values. */
  *tx_mpps_scale_factor = 0;
  *tx_bw_scale_factor = 0;
  *rx_mpps_scale_factor = 0;
  *rx_bw_scale_factor = 0;

  if ( sys_config == NULL  )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_clock_scale_factors(): Invalid params" );
    return APR_EBADPARAM;
  }

  if ( session_obj->is_device_config_registered == FALSE )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_get_clock_scale_factors(): Device config table not registered with CVP" );
    return APR_EOK;
  }

  if ( ( clk_table_ptr == NULL ) || ( clk_table_ptr->columns == NULL ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_get_clock_scale_factors(): No clock table" );
    return APR_EOK;
  }

  num_clk_columns = clk_table_ptr->num_columns;

  /* Find the matching clock column that can support the current voice use case. */
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_get_clock_scale_factors(): " \
                                         "network_id 0x%04X media_id 0x%04X",
                                         sys_config->network_id,
                                         sys_config->media_id );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_get_clock_scale_factors(): " \
                                         "tx_sampl_rate 0x%08X rx_sampl_rate 0x%04X, num_clk_columns %d",
                                         sys_config->tx_pp_sr,
                                         sys_config->rx_pp_sr, num_clk_columns );

  /* Find match for Tx scale factors */
  max_param_match_cnt = 0;
  for ( use_case_idx = 0; use_case_idx < num_clk_columns; use_case_idx++ )
  {
    clk_column = ( vss_param_clock_control_params_t* )
                 ( ( uint8_t* ) clk_table_ptr->columns + ( use_case_idx * sizeof ( vss_param_clock_control_params_t ) ) );
    param_match_cnt = 0;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "direction 0x%08X network_id 0x%04X media_id 0x%04X",
                                         clk_column->direction,
                                         clk_column->network_id,
                                         clk_column->media_type_id);

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "mpps_scale_factor %d bus_bw_scale_factor %d sampling_rate %d",
                                         clk_column->mpps_scale_factor,
                                         clk_column->bus_bw_scale_factor,
                                         clk_column->sampling_rate);

    if ( ( VSS_IVOCPROC_DIRECTION_TX == clk_column->direction ) &&
         ( sys_config->tx_pp_sr == clk_column->sampling_rate ) &&
         ( sys_config->network_id == clk_column->network_id ) &&
         ( sys_config->media_id == clk_column->media_type_id ) )
    {
      *tx_mpps_scale_factor = clk_column->mpps_scale_factor;
      *tx_bw_scale_factor = clk_column->bus_bw_scale_factor;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "Tx Match found: direction %d mpps_scale_factor %d bus_bw_scale_factor %d",
                                         clk_column->direction,
                                         clk_column->mpps_scale_factor,
                                         clk_column->bus_bw_scale_factor );
      break;
    }

    if ( ( VSS_IVOCPROC_DIRECTION_TX == clk_column->direction ) ||
         ( default_vals.direction == clk_column->direction ) )
    {
      if ( VSS_IVOCPROC_DIRECTION_TX == clk_column->direction )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->tx_pp_sr == clk_column->sampling_rate ) ||
         ( default_vals.sampling_rate == clk_column->sampling_rate ) )
    {
      if ( sys_config->tx_pp_sr == clk_column->sampling_rate )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->network_id == clk_column->network_id ) ||
         ( default_vals.network_id == clk_column->network_id ) )
    {
      if ( sys_config->network_id == clk_column->network_id )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->media_id == clk_column->media_type_id ) ||
         ( default_vals.media_type_id == clk_column->media_type_id ) )
    {
      if ( sys_config->media_id == clk_column->media_type_id )
        param_match_cnt++;
    }
    else
      continue;

    if ( param_match_cnt >= max_param_match_cnt )
    {
      max_param_match_cnt = param_match_cnt;
      *tx_mpps_scale_factor = clk_column->mpps_scale_factor;
      *tx_bw_scale_factor = clk_column->bus_bw_scale_factor;
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "Tx Close Match: max_param_match_cnt %d mpps_scale_factor %d bus_bw_scale_factor %d",
                                         max_param_match_cnt,
                                         clk_column->mpps_scale_factor,
                                         clk_column->bus_bw_scale_factor );
    }
  }

  /* Find match for Rx scale factors */
  max_param_match_cnt = 0;
  for ( use_case_idx = 0; use_case_idx < num_clk_columns; use_case_idx++ )
  {
    clk_column = ( vss_param_clock_control_params_t* )
                 ( ( uint8_t* ) clk_table_ptr->columns + ( use_case_idx * sizeof ( vss_param_clock_control_params_t ) ) );
    param_match_cnt = 0;
    if ( ( VSS_IVOCPROC_DIRECTION_RX == clk_column->direction ) &&
         ( sys_config->rx_pp_sr == clk_column->sampling_rate ) &&
         ( sys_config->network_id == clk_column->network_id ) &&
         ( sys_config->media_id == clk_column->media_type_id ) )
    {
      *rx_mpps_scale_factor = clk_column->mpps_scale_factor;
      *rx_bw_scale_factor = clk_column->bus_bw_scale_factor;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "Rx Match found: direction %d mpps_scale_factor %d bus_bw_scale_factor %d",
                                         clk_column->direction,
                                         clk_column->mpps_scale_factor,
                                         clk_column->bus_bw_scale_factor );
      break;
    }

    if ( ( VSS_IVOCPROC_DIRECTION_RX == clk_column->direction ) ||
         ( default_vals.direction == clk_column->direction ) )
    {
      if ( VSS_IVOCPROC_DIRECTION_RX == clk_column->direction )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->rx_pp_sr == clk_column->sampling_rate ) ||
         ( default_vals.sampling_rate == clk_column->sampling_rate ) )
    {
      if ( sys_config->rx_pp_sr == clk_column->sampling_rate )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->network_id == clk_column->network_id ) ||
         ( default_vals.network_id == clk_column->network_id ) )
    {
      if ( sys_config->network_id == clk_column->network_id )
        param_match_cnt++;
    }
    else
      continue;

    if ( ( sys_config->media_id == clk_column->media_type_id ) ||
         ( default_vals.media_type_id == clk_column->media_type_id ) )
    {
      if ( sys_config->media_id == clk_column->media_type_id )
        param_match_cnt++;
    }
    else
      continue;

    if ( param_match_cnt >= max_param_match_cnt )
    {
      max_param_match_cnt = param_match_cnt;
      *rx_mpps_scale_factor = clk_column->mpps_scale_factor;
      *rx_bw_scale_factor = clk_column->bus_bw_scale_factor;
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_get_clock_scale_factors(): " \
                                         "Rx Close Match: max_param_match_cnt %d mpps_scale_factor %d bus_bw_scale_factor %d",
                                         max_param_match_cnt,
                                         clk_column->mpps_scale_factor,
                                         clk_column->bus_bw_scale_factor );
    }
  }

  return APR_EOK;
}

static int32_t cvp_set_system_config_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_icommon_cmd_set_system_config_t* in_args;
  cvp_sequencer_job_object_t* seqjob_obj;
  cvp_simple_job_object_t* subjob_obj;
  vss_icommon_rsp_set_system_config_t set_system_config_rsp;
  uint16_t client_addr;
  enum {
    CVP_SEQUENCER_ENUM_UNINITIALIZED,
    CVP_SEQUENCER_ENUM_REINIT,
    CVP_SEQUENCER_ENUM_WAIT_1,
    CVP_SEQUENCER_ENUM_CALIBRATE_COMMON, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_WAIT_2, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_WAIT_3, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_WAIT_4, /* BACKWARD COMPATIBILITY */
    CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL,
    CVP_SEQUENCER_ENUM_CALIBRATE_STATIC,
    CVP_SEQUENCER_ENUM_WAIT_5,
    CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_STATIC_CAL,
    CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC,
    CVP_SEQUENCER_ENUM_WAIT_6,
    CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL,
    CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES,
    CVP_SEQUENCER_ENUM_WAIT_7,
    CVP_SEQUENCER_ENUM_GET_KPPS,
    CVP_SEQUENCER_ENUM_WAIT_8,
    CVP_SEQUENCER_ENUM_COMPLETE,
    CVP_SEQUENCER_ENUM_INVALID
  };

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
    { /* Setting system config when the vocproc is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_system_config_cmd_ctrl(): Vocproc is running." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );

      return APR_EOK;
    }

    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_icommon_cmd_set_system_config_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_system_config_t,
                                     ctrl->packet );

    session_obj->target_set.system_config.network_id = in_args->network_id;
    session_obj->target_set.system_config.media_id = in_args->media_id;
    session_obj->target_set.system_config.rx_voc_op_mode = in_args->rx_voc_op_mode;
    session_obj->target_set.system_config.tx_voc_op_mode = in_args->tx_voc_op_mode;
    session_obj->target_set.system_config.feature = in_args->feature;
    session_obj->target_set.system_config.vsid = in_args->vsid;
    session_obj->target_set.system_config.vfr_mode = in_args->vfr_mode;
    session_obj->target_set.system_config.call_num = in_args->call_num;

    /* Update target sampling rates if they have not been set by client. */
    if ( session_obj->is_sr_set_by_client == FALSE )
    {
      session_obj->target_set.system_config.tx_pp_sr = in_args->tx_pp_sr;

      /* Take max of ( system config Rx samplig rate ) and ( Widevoice Rx
       * sampling rate.
       */
      if ( session_obj->widevoice_rx_sr > in_args->rx_pp_sr )
      {
        session_obj->target_set.system_config.rx_pp_sr = session_obj->widevoice_rx_sr;
      }
      else
      {
        session_obj->target_set.system_config.rx_pp_sr = in_args->rx_pp_sr;
      }
    }

    if ( TRUE == cvp_media_id_is_var_sr( session_obj->target_set.system_config.media_id ) )
    {
      /* For EVS, MVM dictates the variable vocoder sampling rate value. Sampling rates will be applied
         during REINIT */
      if ( VSS_MEDIA_ID_EVS == session_obj->target_set.system_config.media_id  )
      {
        session_obj->target_set.system_config.dec_sr = in_args->dec_sr;
        session_obj->target_set.system_config.enc_sr = in_args->enc_sr;
        session_obj->target_set.system_config.rx_pp_sr = in_args->rx_pp_sr;
        session_obj->target_set.system_config.tx_pp_sr = in_args->tx_pp_sr;
      }
    }

    if ( ( session_obj->active_set.system_config.network_id !=
           session_obj->target_set.system_config.network_id ) ||
         ( session_obj->active_set.system_config.tx_pp_sr !=
           session_obj->target_set.system_config.tx_pp_sr ) ||
         ( session_obj->active_set.system_config.rx_pp_sr !=
           session_obj->target_set.system_config.rx_pp_sr ) )
    { /* BACKWARD COMPATIBILITY */
      session_obj->common_cal.is_calibrate_needed =
        session_obj->common_cal.is_registered;

      session_obj->volume_cal.is_calibrate_needed =
        session_obj->volume_cal.is_registered;
    }

    if ( ( session_obj->active_set.system_config.network_id !=
           session_obj->target_set.system_config.network_id ) ||
         ( session_obj->active_set.system_config.rx_pp_sr !=
           session_obj->target_set.system_config.rx_pp_sr ) ||
         ( session_obj->active_set.system_config.tx_pp_sr !=
           session_obj->target_set.system_config.tx_pp_sr ) ||
         ( session_obj->active_set.system_config.media_id !=
           session_obj->target_set.system_config.media_id ) ||
         ( session_obj->active_set.system_config.rx_voc_op_mode !=
           session_obj->target_set.system_config.rx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.tx_voc_op_mode !=
           session_obj->target_set.system_config.tx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.feature !=
           session_obj->target_set.system_config.feature ) )
    {
      session_obj->static_cal.is_calibrate_needed =
        session_obj->static_cal.is_registered;

      session_obj->dynamic_cal.is_calibrate_needed =
        session_obj->dynamic_cal.is_registered;
    }

    rc = cvp_create_sequencer_job_object(
           ( cvp_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVP_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = CVP_SEQUENCER_ENUM_REINIT;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVP_SEQUENCER_ENUM_REINIT:
      { /* Reinit VPM if required. */
        if ( cvp_is_reinit_required( session_obj ) == FALSE )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_COMMON;
          continue;
        }
        else
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_1;
          rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
          if ( rc )
          {
            MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "CVP_GOAL_ENUM_REINIT creation failed with status as 0x%X",
                                                    rc );
            seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_COMMON;
            continue;
          }
        }
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for VPM reinit to be done. */

        if ( ctrl->session_obj->session_ctrl.goal_completed == FALSE )
        {
           rc = cvp_state_control( session_obj );
           return APR_EPENDING;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_COMMON;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_COMMON: /* BACKWARD COMPATIBILITY */
      { /* Apply the common calibration data to VPM. */
        if ( session_obj->common_cal.is_calibrate_needed == FALSE )
        {
          /* Skip applying common calibration as well as the WV/FENS
           * calibration (since WV/FENS are part of the common calibration).
           */
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME;
          continue;
        }

        rc = cvp_calibrate_common( session_obj, cvp_calibrate_common_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_2;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_2: /* BACKWARD COMPATIBILITY */
      { /* Wait for the common calibration data to be applied to VPM. */
        if ( session_obj->common_cal.set_param_rsp_cnt <
             session_obj->common_cal.num_matching_entries )
        {
          return APR_EPENDING;
        }

        if ( session_obj->common_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for common calibration is %d",
                                                  session_obj->common_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_WV_FENS_FROM_COMMON_CAL: /* BACKWARD COMPATIBILITY */
      {
        /* Apply WideVoice and FENS calibration data to CVS.
         * TODO: this is a temporary solution until proper FW solution for
         * WV/FENS is available.
         */
        rc = cvp_calibrate_wv_fens_from_common_cal(
               session_obj, cvp_calibrate_wv_fens_from_common_cal_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_3;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_3: /* BACKWARD COMPATIBILITY */
      { /* Wait for the WV/FENS calibration data to be applied to CVS. */
        if ( session_obj->common_cal.set_param_rsp_cnt <
             session_obj->common_cal.num_set_param_to_stream )
        {
          return APR_EPENDING;
        }

        if ( session_obj->common_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for WV/FENS calibration is %d",
                                                  session_obj->common_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_VOLUME: /* BACKWARD COMPATIBILITY */
      { /* Apply the volume calibration data to VPM. */
        if ( session_obj->volume_cal.is_calibrate_needed == FALSE )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL;
          continue;
        }

        /* When applying cached setting, set the ramp duartion to 0. */
        rc = cvp_calibrate_volume( session_obj, 0, cvp_calibrate_volume_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_4;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_4: /* BACKWARD COMPATIBILITY */
      { /* Wait for the volume calibration data to be applied to VPM. */
        if ( session_obj->volume_cal.set_param_rsp_cnt <
             session_obj->volume_cal.num_matching_entries + 1 )
        {
          return APR_EPENDING;
        }

        if ( session_obj->volume_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for volume calibration is %d",
                                                  session_obj->common_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL:
      {
        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_STATIC;

        ( void ) cvp_clear_cached_stream_cal( session_obj );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_STATIC:
      { /* Apply the static calibration data to VPM. */
        if ( session_obj->static_cal.is_calibrate_needed == FALSE )
        {
          /* Skip applying static calibration and finding the WV/FENS
           * calibration from the static calibration table.
           */
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC;
          continue;
        }

        rc = cvp_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVP_PANIC_ON_ERROR( rc );
        seqjob_obj->subjob_obj = ( ( cvp_object_t* ) subjob_obj );

        rc = cvp_calibrate_static( session_obj, subjob_obj );
        if ( rc )
        {
          /* Skip finding the WV/FENS calibration from the static calibration
           * table.
           */
          ( void ) cvp_free_object( ( cvp_object_t* ) subjob_obj );

          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_5;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_5:
      { /* Wait for the static calibration data to be processed by VPM. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_STATIC_CAL;

        ( void ) cvp_free_object( ( cvp_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_STATIC_CAL:
      { /* Find the WV and FENS calibration data from the static cal table. */
        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC;

        ( void ) cvp_find_stream_modules_from_matched_entries(
                   session_obj, session_obj->static_cal.query_handle );

        ( void ) cvd_cal_query_deinit( session_obj->static_cal.query_handle );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC:
      { /* Apply the dynamic calibration data to VPM. */
        if ( session_obj->dynamic_cal.is_calibrate_needed == FALSE )
        {
          /* Skip applying dynamic calibration and finding the FENS
           * calibration from the dynamic calibration table.
           */
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES;
          continue;
        }

        /* When applying cached setting, set the ramp duartion to 0. */
        rc = cvp_calibrate_dynamic( session_obj, 0, cvp_calibrate_dynamic_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_6;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_6:
      { /* Wait for the dynamic calibration data to be processed by VPM. */
        if ( session_obj->dynamic_cal.set_param_rsp_cnt <
             session_obj->dynamic_cal.num_set_param_issued )
        {
          return APR_EPENDING;
        }

        if ( session_obj->dynamic_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for dynamic calibration is %d",
                                                  session_obj->dynamic_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL:
      { /* Find the WV and FENS calibration data from the static cal table. */
        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES;

        ( void ) cvp_find_stream_modules_from_matched_entries(
                   session_obj, session_obj->dynamic_cal.query_handle );

        ( void ) cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES:
      {
        /* Apply WideVoice and FENS calibration data to CVS.
         * TODO: this is a temporary solution until proper FW solution for
         * WV/FENS is available.
         */
        rc = cvp_calibrate_stream_modules(
               session_obj, cvp_calibrate_stream_modules_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_GET_KPPS;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_7;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_7:
      { /* Wait for the WV/FENS calibration data to be applied to CVS. */
        if ( session_obj->stream_cal_info.set_param_rsp_cnt <
             session_obj->stream_cal_info.num_set_param_issued )
        {
          return APR_EPENDING;
        }

        if ( session_obj->stream_cal_info.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_stream_set_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for WV/FENS calibration is %d",
                                                  session_obj->stream_cal_info.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_GET_KPPS;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_GET_KPPS:
      { /* Retrieve the VPM's KPPS requirement. */
        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_8;

        rc = cvp_create_simple_job_object( session_obj->header.handle, &subjob_obj );
        CVP_PANIC_ON_ERROR( rc );
        subjob_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_KPPS ] = cvp_get_kpps_rsp_fn;

        seqjob_obj->subjob_obj = ( ( cvp_object_t* ) subjob_obj );

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
               session_obj->vocproc_addr, session_obj->vocproc_handle,
               subjob_obj->header.handle, VPM_CMD_GET_KPPS,
               NULL, 0 );
        CVP_COMM_ERROR( rc, session_obj->vocproc_addr );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_8:
      { /* Wait for get KPPS to complete. */
        subjob_obj = &seqjob_obj->subjob_obj->simple_job;

        if ( subjob_obj->is_completed == FALSE )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;

        ( void ) cvp_free_object( ( cvp_object_t* ) subjob_obj );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        session_obj->common_cal.is_calibrate_needed = FALSE; /* BACKWARD COMPATIBILITY */
        session_obj->volume_cal.is_calibrate_needed = FALSE; /* BACKWARD COMPATIBILITY */
        session_obj->static_cal.is_calibrate_needed = FALSE;
        session_obj->dynamic_cal.is_calibrate_needed = FALSE;
        session_obj->is_vocproc_config_changed = FALSE;

        session_obj->active_set.system_config.network_id =
          session_obj->target_set.system_config.network_id;

        session_obj->active_set.system_config.rx_voc_op_mode =
          session_obj->target_set.system_config.rx_voc_op_mode;

        session_obj->active_set.system_config.tx_voc_op_mode =
          session_obj->target_set.system_config.tx_voc_op_mode;

        session_obj->active_set.system_config.media_id =
          session_obj->target_set.system_config.media_id;

        session_obj->active_set.system_config.feature =
          session_obj->target_set.system_config.feature;

        set_system_config_rsp.vp_rx_kpps = session_obj->kpps_info.vp_rx;
        set_system_config_rsp.vp_tx_kpps = session_obj->kpps_info.vp_tx;
        set_system_config_rsp.tx_pp_sr = session_obj->active_set.system_config.tx_pp_sr;
        set_system_config_rsp.rx_pp_sr = session_obj->active_set.system_config.rx_pp_sr;
        set_system_config_rsp.vocproc_tx_topology_id = session_obj->active_set.tx_topology_id;
        set_system_config_rsp.vocproc_rx_topology_id = session_obj->active_set.rx_topology_id;
        set_system_config_rsp.tx_num_channels = session_obj->topo_param.num_dev_chans.tx_num_channels;

        /* Retrieve MPPS and bus BW scaling factors from the device config table,
         * if one is registered with CVP. */
        cvp_get_clock_scale_factors( session_obj,
                                     &set_system_config_rsp.tx_mpps_scale_factor,
                                     &set_system_config_rsp.tx_bw_scale_factor,
                                     &set_system_config_rsp.rx_mpps_scale_factor,
                                     &set_system_config_rsp.rx_bw_scale_factor );

        rc = __aprv2_cmd_alloc_send(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
               ctrl->packet->dst_addr, ctrl->packet->dst_port,
               ctrl->packet->src_addr, ctrl->packet->src_port,
               ctrl->packet->token, VSS_ICOMMON_RSP_SET_SYSTEM_CONFIG,
               &set_system_config_rsp, sizeof( set_system_config_rsp ) );
        CVP_COMM_ERROR( rc, client_addr );

        ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

        ( void ) cvp_free_object( ( cvp_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvp_set_dynamic_system_config_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_icommon_cmd_set_dynamic_system_config_t* in_args;
  cvp_sequencer_job_object_t* seqjob_obj;
  enum {
    CVP_SEQUENCER_ENUM_UNINITIALIZED,
    CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC,
    CVP_SEQUENCER_ENUM_WAIT_1,
    CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL,
    CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL,
    CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES,
    CVP_SEQUENCER_ENUM_WAIT_2,
    CVP_SEQUENCER_ENUM_COMPLETE,
    CVP_SEQUENCER_ENUM_INVALID
  };

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_icommon_cmd_set_dynamic_system_config_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_dynamic_system_config_t,
                                     ctrl->packet );

    session_obj->target_set.system_config.rx_voc_op_mode = in_args->rx_voc_op_mode;
    session_obj->target_set.system_config.tx_voc_op_mode = in_args->tx_voc_op_mode;
    session_obj->target_set.system_config.feature = in_args->feature_id;
    session_obj->target_set.system_config.rx_pp_sr = in_args->rx_pp_sr;

    if ( ( session_obj->active_set.system_config.rx_voc_op_mode !=
           session_obj->target_set.system_config.rx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.tx_voc_op_mode !=
           session_obj->target_set.system_config.tx_voc_op_mode ) ||
         ( session_obj->active_set.system_config.feature !=
           session_obj->target_set.system_config.feature ) ||
         ( session_obj->active_set.system_config.rx_pp_sr !=
           session_obj->target_set.system_config.rx_pp_sr ) )
    {
      session_obj->dynamic_cal.is_calibrate_needed =
        session_obj->dynamic_cal.is_registered;
    }

    rc = cvp_create_sequencer_job_object(
           ( cvp_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVP_PANIC_ON_ERROR( rc );

    ctrl->pendjob_obj->sequencer_job.state = CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
    case CVP_SEQUENCER_ENUM_CALIBRATE_DYNAMIC:
      { /* Apply the dynamic calibration data to VPM. */
        if ( session_obj->dynamic_cal.is_calibrate_needed == FALSE )
        {
          /* Skip applying dynamic calibration and finding the FENS
           * calibration from the dynamic calibration table.
           */
          seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        rc = cvp_calibrate_dynamic( session_obj, 0, cvp_calibrate_dynamic_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_1;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for the dynamic calibration data to be processed by VPM. */
        if ( session_obj->dynamic_cal.set_param_rsp_cnt <
             session_obj->dynamic_cal.num_set_param_issued )
        {
          return APR_EPENDING;
        }

        if ( session_obj->dynamic_cal.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_dynamic_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for dynamic calibration is %d",
                                                  session_obj->dynamic_cal.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CLEAR_CACHED_WV_FENS_CAL:
      {
        seqjob_obj->state = CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL;

        ( void ) cvp_clear_cached_stream_cal( session_obj );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_QUERY_WV_FENS_FROM_DYNAMIC_CAL:
      { /* Find the WV and FENS calibration data from the static cal table. */
        seqjob_obj->state = CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES;

        ( void ) cvp_find_stream_modules_from_matched_entries(
                   session_obj, session_obj->dynamic_cal.query_handle );

        ( void ) cvd_cal_query_deinit( session_obj->dynamic_cal.query_handle );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_CALIBRATE_STREAM_MODULES:
      {
        /* Apply WideVoice and FENS calibration data to CVS.
         * TODO: this is a temporary solution until proper FW solution for
         * WV/FENS is available.
         */
        rc = cvp_calibrate_stream_modules(
               session_obj, cvp_calibrate_stream_modules_rsp_fn );
        if ( rc )
        {
          seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
          continue;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_2;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_2:
      { /* Wait for the WV/FENS calibration data to be applied to CVS. */
        if ( session_obj->stream_cal_info.set_param_rsp_cnt <
             session_obj->stream_cal_info.num_set_param_issued )
        {
          return APR_EPENDING;
        }

        if ( session_obj->stream_cal_info.set_param_failed_rsp_cnt != 0 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_dynamic_system_config_cmd_ctrl(): Total number " \
                                                  "of set param failed for WV/FENS calibration is %d",
                                                  session_obj->stream_cal_info.set_param_failed_rsp_cnt );
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        session_obj->dynamic_cal.is_calibrate_needed = FALSE;

        session_obj->active_set.system_config.rx_voc_op_mode =
          session_obj->target_set.system_config.rx_voc_op_mode;

        session_obj->active_set.system_config.tx_voc_op_mode =
          session_obj->target_set.system_config.tx_voc_op_mode;

        session_obj->active_set.system_config.feature =
          session_obj->target_set.system_config.feature;

        session_obj->active_set.system_config.rx_pp_sr =
          session_obj->target_set.system_config.rx_pp_sr;

        ( void ) __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );

        ( void ) cvp_free_object( ( cvp_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvp_set_param_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_simple_job_object_t* job_obj;
  cvp_sequencer_job_object_t* seqjob_obj;
  cvp_simple_job_object_t* subjob_obj;
  vss_icommon_cmd_set_param_v2_t* payload;
  uint32_t payload_size;
  uint32_t expected_payload_size;
  uint8_t* param_data = NULL;
  cvd_virt_addr_t param_data_virt_addr;
  voice_set_param_v2_t set_param;
  aprv2_packet_t* dst_packet;
  uint8_t* dst_payload;
  uint32_t dst_payload_size;
  uint32_t dst_payload_left;
  uint32_t vpm_mem_handle;
  uint16_t client_addr;
  uint32_t param_id;
  uint16_t param_size;
  uint8_t* param_payload_data;
  enum {
    CVP_SEQUENCER_ENUM_UNINITIALIZED,
    CVP_SEQUENCER_ENUM_WAIT_1,
    CVP_SEQUENCER_ENUM_GET_KPPS,
    CVP_SEQUENCER_ENUM_WAIT_2,
    CVP_SEQUENCER_ENUM_RECONFIG,
    CVP_SEQUENCER_ENUM_COMPLETE,
    CVP_SEQUENCER_ENUM_INVALID
  };

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_set_param_v2_t, ctrl->packet );
      payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

      if ( payload_size < sizeof( vss_icommon_cmd_set_param_v2_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_param_v2_cmd_ctrl(): Unexpected payload size, %d < %d",
                                                payload_size,
                                                sizeof( vss_icommon_cmd_set_param_v2_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvp_verify_param_data_size( payload->mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_param_v2_cmd_ctrl(): Invalid param data size, %d",
                                                payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      dst_payload_size = sizeof( voice_set_param_v2_t );

      if ( payload->mem_handle == 0 )
      { /* Parameter data is in-band. */
        expected_payload_size = ( sizeof( vss_icommon_cmd_set_param_v2_t ) + payload->mem_size );
        if ( payload_size != expected_payload_size )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_param_v2_cmd_ctrl(): Unexpected payload size, %d != %d",
                                                  payload_size,
                                                  expected_payload_size );
          rc = APR_EBADPARAM;
          break;
        }

        param_data = ( ( uint8_t* ) payload );
        param_data += sizeof( vss_icommon_cmd_set_param_v2_t );
        vpm_mem_handle = 0;
        dst_payload_size += payload->mem_size;

        param_id = *( uint32_t* ) ( param_data +  sizeof( uint32_t ) );
        param_size = *( uint32_t* ) ( param_data + ( 2 * ( sizeof( uint32_t ) ) ) );
        param_payload_data = ( uint8_t* ) ( param_data + ( 3 * ( sizeof( uint32_t ) ) ) );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Param ID[0]=0x%08x",
                                              param_id );

        if ( param_size >= 16 )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[0-2]=0x%08x, " \
                                                "0x%08x, 0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ),
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ),
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[3]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
        }
        else if ( param_size >= 4 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[0]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ) );
        }
      }
      else
      { /* Parameter data is out-of-band. */
        rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
        if ( rc )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_param_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                                  payload->mem_handle );
          rc = APR_EHANDLE;
          break;
        }

        ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->mem_handle, &vpm_mem_handle );

        rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvp_set_param_v2_cmd_ctrl(): Mis-aligned mem address:"
            " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
            ( uint32_t )( payload->mem_address >> 32 ) );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                       payload->mem_size );
        if ( rc )
        {
          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvp_set_param_v2_cmd_ctrl(): Memory is not within range,"
            " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
            payload->mem_handle, ( uint32_t )payload->mem_address,
            ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
          rc = APR_EBADPARAM;
          break;
        }

        rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle, payload->mem_address,
                                              &param_data_virt_addr );
        if ( rc )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvp_set_param_v2_cmd_ctrl(): Cannot get virtual address,"
            " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
            ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
          rc = APR_EFAILED;
          break;
        }

        param_id = *( uint32_t* ) ( ( uint8_t* ) param_data_virt_addr.ptr +  sizeof( uint32_t ) );
        param_size = *( uint32_t* ) ( ( uint8_t* ) param_data_virt_addr.ptr + ( 2 * ( sizeof( uint32_t ) ) ) );
        param_payload_data =  ( ( uint8_t* ) param_data_virt_addr.ptr + ( 3 * ( sizeof( uint32_t ) ) ) );

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Param ID[0]=0x%08x",
                                              param_id );

        if ( param_size >= 16 )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[0-2]=0x%08x, " \
                                                "0x%08x, 0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ),
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + sizeof( uint32_t ) ),
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 2 * sizeof( uint32_t ) ) ) );

          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[3]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) + ( 3 * sizeof( uint32_t ) ) ) );
        }
        else if ( param_size >= 4 )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): Payload[0]=0x%08x",
                                                *( uint32_t* ) ( param_payload_data + sizeof( voice_param_data_t ) ) );
        }
      }

      session_obj->set_param_info.num_set_params = 1;
      session_obj->set_param_info.rsp_cnt = 0;
      session_obj->set_param_info.status = APR_EOK;

      { /* Set param on VPM. */
        rc = cvp_create_simple_job_object( session_obj->header.handle, &job_obj );
        CVP_PANIC_ON_ERROR( rc );
        job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_set_param_rsp_fn;

        cvd_mem_mapper_set_virt_addr_to_uint32( &set_param.payload_address_msw, &set_param.payload_address_lsw,
                                                param_data_virt_addr.ptr );
        set_param.payload_size = payload->mem_size;
        set_param.mem_map_handle = vpm_mem_handle;

        rc = __aprv2_cmd_alloc_ext(
               cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
               session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
               cvp_vpm_addr, session_obj->vocproc_handle,
               job_obj->header.handle, VOICE_CMD_SET_PARAM_V2,
               dst_payload_size, &dst_packet );
        CVP_PANIC_ON_ERROR( rc );

        dst_payload = APRV2_PKT_GET_PAYLOAD( uint8_t, dst_packet );
        dst_payload_left = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( dst_packet->header );

        ( void ) mmstd_memcpy( dst_payload, dst_payload_left, &set_param, sizeof( set_param ) );
        dst_payload_left -= sizeof( set_param );

        if ( param_data != NULL )
        {
          ( void ) mmstd_memcpy(
                     ( dst_payload + sizeof( set_param ) ), dst_payload_left,
                     param_data, payload->mem_size );
        }

        rc = __aprv2_cmd_forward( cvp_apr_handle, dst_packet );
        CVP_COMM_ERROR( rc, cvp_vpm_addr );
      }

      { /* Temporary hack for setting the WV,FENS and Rx Gain parameters for RTC to work.
         * The WV/FENS/Rx Gain calibration data is in the vocproc common table, however,
         * the WV/FENS/Rx Gain modules are in the stream. For RTC to work, the set_param
         * for WV/FENS/Rx Gain on the vocproc must be forwarded to all the attached
         * streams.
         *
         * This hack is required until WV/FENS/Rx Gain is instantiated per
         * stream<->vocproc connection, and VPM can calibrate the WV/FENS/Rx Gain.
         *
         * Note: To completely remove this hack. The session object structure
         * as well as the set param handling need to be cleaned up, as there
         * will no longer be a need to keep track of the number of parameters
         * to set/been_set and the set_param status. Creating a track job
         * object to handle the single set param on VPM is sufficient.
         */
        void* data;
        uint32_t data_len;
        void* data_addr_array[ CVP_NUM_WV_FENS_CAL_FROM_COMMON_CAL_DATA ];
        uint32_t data_len_array[ CVP_NUM_WV_FENS_CAL_FROM_COMMON_CAL_DATA ];
        uint32_t num_found_mid_pid_pair = 0;
        uint32_t idx;
        /* This descriptor is only created for validation purposes */
        cvd_cal_table_descriptor_t table_descriptor;
        mmstd_memset( &table_descriptor, 0, sizeof( table_descriptor ) );
        table_descriptor.data_mem_handle = payload->mem_handle;
        table_descriptor.size = payload->mem_size;

        if ( vpm_mem_handle == 0 )
        {
          param_data_virt_addr.ptr = (void*)param_data;
        }
        table_descriptor.start_ptr = param_data_virt_addr.ptr;

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_WV,
               VOICE_PARAM_MOD_ENABLE, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_WV,
               VOICE_PARAM_WV, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_FNS,
               VOICE_PARAM_MOD_ENABLE, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_FNS,
               VOICE_PARAM_FNS, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_FNS,
               VOICE_PARAM_FNS_V2, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_FNS_V2,
               VOICE_PARAM_MOD_ENABLE, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_FNS_V2,
               VOICE_PARAM_FNS_V3, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_WV_V2,
               VOICE_PARAM_MOD_ENABLE, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_WV_V2,
               VOICE_PARAM_WV_V2, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        rc = cvp_find_mid_pid_pair_from_cal_entry (
               &table_descriptor,
               param_data_virt_addr.ptr, payload->mem_size, VOICE_MODULE_RX_GAIN,
               VOICE_PARAM_GAIN, &data, &data_len );
        if ( rc == APR_EOK )
        {
          data_addr_array[ num_found_mid_pid_pair ] = data;
          data_len_array[ num_found_mid_pid_pair ] = data_len;
          ++num_found_mid_pid_pair;
        }

        if ( num_found_mid_pid_pair > 0 )
        {
          if ( session_obj->attached_stream_list.size > 0 )
          {
            session_obj->set_param_info.num_set_params +=
              ( num_found_mid_pid_pair * session_obj->attached_stream_list.size );

            /* Set WV/FENS/Rx Gain params on all the attached streams. */
            for ( idx = 0; idx < num_found_mid_pid_pair; idx++ )
            {
              cvp_set_param_to_streams( session_obj,
                                        data_addr_array[ idx ],
                                        data_len_array[ idx ],
                                        cvp_set_param_rsp_fn );
            }
          }
          else
          {
            /* Current design does not support setting the WV/FENS/Rx Gain params if no
             * stream is attached to the vocproc.
             */
            MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_param_v2_cmd_ctrl(): Setting WV/FENS/Rx Gain parameters is not supported when no stream is attached to the vocproc." );
            session_obj->set_param_info.status = APR_EUNSUPPORTED;
          }
        }
      }

      rc = cvp_create_sequencer_job_object(
           ( cvp_sequencer_job_object_t** ) &ctrl->pendjob_obj );
      CVP_PANIC_ON_ERROR( rc );

      ctrl->pendjob_obj->sequencer_job.state = CVP_SEQUENCER_ENUM_WAIT_1;

      return APR_EPENDING;
    }
    else
    {
        /* If this vocproc is running and it is attached to an MVM session, send
         * VSS_IVOCPROC_EVT_RECONFIG event to MVM if there is a KPPS change
         * due to set_param.
         * Note:
         * 1. The vocproc's handling for VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG is
         *    optimized such that calibration data is not applied unless there
         *    is a need. Therefore we won't overwrite the params that has been
         *    set by set_param with calibration data.
         * 2. It is only possible to receive a set_param from external CVD
         *    client when this vocproc is in RUN state, because set_param is
         *    only expected to be used for real-time calibration purpose.
         */

        seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

        for( ;; )
        {
          switch ( seqjob_obj->state )
          {
            case CVP_SEQUENCER_ENUM_WAIT_1:
              { /* Wait for SET_PARAM_V2 to complete. */
                if ( session_obj->set_param_info.rsp_cnt <
                     session_obj->set_param_info.num_set_params )
                {
                  return APR_EPENDING;
                }

                if ( ( CVP_STATE_ENUM_RUN == session_obj->session_ctrl.state ) &&
                     ( APR_NULL_V != session_obj->attached_mvm_handle ) &&
                     ( APR_EOK == session_obj->set_param_info.status ) )
                {
                  seqjob_obj->state = CVP_SEQUENCER_ENUM_GET_KPPS;
                }
                else
                {
                  seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
                  continue;
                }
              }
              /*-fallthru */

            case CVP_SEQUENCER_ENUM_GET_KPPS:
              { /* Retrieve the VPM's KPPS requirement. */
                seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_2;

                rc = cvp_create_simple_job_object( session_obj->header.handle, &subjob_obj );
                CVP_PANIC_ON_ERROR( rc );
                subjob_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_KPPS ] = cvp_get_kpps_rsp_fn;

                seqjob_obj->subjob_obj = ( ( cvp_object_t* ) subjob_obj );

                rc = __aprv2_cmd_alloc_send(
                       cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                       cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                       session_obj->vocproc_addr, session_obj->vocproc_handle,
                       subjob_obj->header.handle, VPM_CMD_GET_KPPS,
                       NULL, 0 );
                CVP_COMM_ERROR( rc, session_obj->vocproc_addr );
                MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): sent get_kpps to VPM" );
              }
              /*-fallthru */

            case CVP_SEQUENCER_ENUM_WAIT_2:
              { /* Wait for get KPPS to complete. */
                subjob_obj = &seqjob_obj->subjob_obj->simple_job;

                if ( subjob_obj->is_completed == FALSE )
                {
                  return APR_EPENDING;
                }

                if ( session_obj->is_kpps_changed == TRUE )
                {
                  seqjob_obj->state = CVP_SEQUENCER_ENUM_RECONFIG;
                  ( void ) cvp_free_object( ( cvp_object_t* ) subjob_obj );
                }
                else
                {
                  seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
                  ( void ) cvp_free_object( ( cvp_object_t* ) subjob_obj );
                  continue;
                }
              }
                /*-fallthru */

            case CVP_SEQUENCER_ENUM_RECONFIG:
              {
                seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;

                rc = __aprv2_cmd_alloc_send(
                       cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
                       cvp_my_addr, ( ( uint16_t ) session_obj->header.handle ),
                       cvp_mvm_addr, session_obj->attached_mvm_handle,
                       0, VSS_IVOCPROC_EVT_RECONFIG,
                       NULL, 0 );
                CVP_COMM_ERROR( rc, cvp_mvm_addr );
                MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): sent reconfig to MVM" );

                session_obj->is_kpps_changed = FALSE;
              }
               /* fallthru */

            case CVP_SEQUENCER_ENUM_COMPLETE:
              { /* End the sequencer. */
                ( void ) cvp_free_object( ( cvp_object_t* ) seqjob_obj );
                ctrl->pendjob_obj = NULL;
                rc = APR_EOK;
                MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: cvp_set_param_v2_cmd_ctrl(): COMPLETE" );
              }
              break;

            default:
              CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
              break;
          }

        break;
      }
    }

    break;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, session_obj->set_param_info.status );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_get_param_v2_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* track_job_obj;
  vss_icommon_cmd_get_param_v2_t* payload;
  uint32_t payload_size;
  uint64_t param_data_virt_addr = 0;
  voice_get_param_v2_t get_param;
  uint32_t vpm_mem_handle;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    payload = APRV2_PKT_GET_PAYLOAD( vss_icommon_cmd_get_param_v2_t, ctrl->packet );
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_size != sizeof( vss_icommon_cmd_get_param_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_param_v2_cmd_ctrl(): Unexpected payload size, %d != %d",
                                              payload_size,
                                              sizeof( vss_icommon_cmd_get_param_v2_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    { /* Temporary hack for getting the WV,FENS, and Rx Gain parameters for RTC to work.
       * The WV/FENS/Rx Gain calibration data is in the vocproc common table, however,
       * the WV/FENS/Rx Gain modules are in the stream. For RTC to work, the get_param
       * for WV/FENS/Rx Gain on the vocproc must be forwarded to one of the attached
       * streams. Note that all the attached streams have the same WV/FENS/Rx Gain
       * calibration, so getting the param data from one of the streams is
       * sufficient.
       *
       * This hack is required until WV/FENS/Rx Gain is instantiated per
       * stream<->vocproc connection, and VPM can calibrate the WV/FENS/Rx Gain.
       */
      cvp_attached_stream_item_t* attached_stream;

      if ( ( payload->module_id == VOICE_MODULE_WV ) ||
           ( payload->module_id == VOICE_MODULE_WV_V2 ) ||
           ( payload->module_id == VOICE_MODULE_FNS ) ||
           ( payload->module_id == VOICE_MODULE_FNS_V2 ) ||
           ( payload->module_id == VOICE_MODULE_RX_GAIN ) )
      {
        if ( session_obj->attached_stream_list.size > 0 )
        {
          rc = apr_list_peak_head( &session_obj->attached_stream_list,
                                   ( ( apr_list_node_t** ) &attached_stream ) );
          CVP_PANIC_ON_ERROR( rc );

          rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
          CVP_PANIC_ON_ERROR( rc );

          track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
          track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_get_param_rsp_fn;

          /* Store the original destination and opcode info from the packet. */
          track_job_obj->orig_src_service = ctrl->packet->src_addr;
          track_job_obj->orig_src_port = ctrl->packet->src_port;
          track_job_obj->orig_opcode = ctrl->packet->opcode;
          track_job_obj->orig_token = ctrl->packet->token;

          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: Sending GET_PARAM_V2 to VCP. cvp_get_param_v2_cmd_ctrl(): Token=%d Param ID=0x%08x",
                                                ctrl->packet->token, payload->param_id );

          rc = __aprv2_cmd_alloc_send(
                 cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
                 session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
                 attached_stream->addr, attached_stream->port,
                 track_job_obj->header.handle, ctrl->packet->opcode,
                 APRV2_PKT_GET_PAYLOAD( void, ctrl->packet ),
                 APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header ) );
          CVP_COMM_ERROR( rc, attached_stream->addr );

          ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
        }
        else
        {
          /* Current design does not support getting the WV/FENS params if no
           * stream is attached to the vocproc.
           */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_param_v2_cmd_ctrl(): Get param for module ID 0x%08x is not supported when no stream is attached to the vocproc.",
                                                  payload->module_id );
          rc = APR_EUNSUPPORTED;
        }

        break;
      }
    }

    rc = cvp_verify_param_data_size( payload->mem_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_param_v2_cmd_ctrl(): Invalid param data size, %d",
                                              payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    if ( payload->mem_handle == 0 )
    { /* Parameter data is in-band. */
      vpm_mem_handle = 0;
    }
    else
    { /* Parameter data is out-of-band. */
      rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_param_v2_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                                payload->mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->mem_handle, &vpm_mem_handle );

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_get_param_v2_cmd_ctrl(): Mis-aligned mem address:"
          " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_get_param_v2_cmd_ctrl():"
          " Mis-aligned mem size: 0x%08X", payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                     payload->mem_size );
      if ( rc )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_get_param_v2_cmd_ctrl(): Memory is not within range,"
          " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
          payload->mem_handle, ( uint32_t )payload->mem_address,
          ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( payload->mem_handle, payload->mem_address,
                                            &param_data_virt_addr );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "cvp_get_param_v2_cmd_ctrl(): Cannot get virtual address,"
          " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
          ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
        rc = APR_EFAILED;
        break;
      }
    }

    get_param.payload_address_lsw = ( ( uint32_t ) param_data_virt_addr );
    get_param.payload_address_msw = ( ( uint32_t ) ( param_data_virt_addr >> 32 ) );
    get_param.module_id = payload->module_id;
    get_param.param_id = payload->param_id;
    get_param.param_max_size = payload->mem_size;
    get_param.reserved = 0;
    get_param.mem_map_handle = vpm_mem_handle;

    rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVP_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_get_param_rsp_fn;

    /* Store the original destination and opcode info from the packet. */
    track_job_obj->orig_src_service = ctrl->packet->src_addr;
    track_job_obj->orig_src_port = ctrl->packet->src_port;
    track_job_obj->orig_opcode = ctrl->packet->opcode;
    track_job_obj->orig_token = ctrl->packet->token;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG_PARAM: Sending GET_PARAM_V2 to VCP. cvp_get_param_v2_cmd_ctrl(): Token=%d Param ID=0x%08x",
                                          ctrl->packet->token, payload->param_id );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    break;
  }

  if ( rc )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
    CVP_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t cvp_vp3_get_data_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivp3_cmd_get_data_t* payload;
  uint32_t payload_size;
  cvd_virt_addr_t param_data_virt_addr;
  uint32_t vpm_mem_handle;
  cvp_track_job_object_t* track_job_obj;
  voice_get_param_v2_t get_param;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    payload = APRV2_PKT_GET_PAYLOAD( vss_ivp3_cmd_get_data_t, ctrl->packet );
    payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_size < sizeof( vss_ivp3_cmd_get_data_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_get_data_cmd_ctrl(): Unexpected payload size, %d < %d",
                                              payload_size,
                                              sizeof( vss_ivp3_cmd_get_data_t ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvp_verify_param_data_size( payload->mem_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_get_data_cmd_ctrl(): Invalid param data size, %d",
                                              payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    /* Parameter data is out-of-band. */
    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_get_data_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                              payload->mem_handle );
      rc = APR_EHANDLE;
      break;
    }

    ( void ) cvd_mem_mapper_get_vpm_mem_handle( payload->mem_handle, &vpm_mem_handle );

    rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_get_data_cmd_ctrl(): Mis-aligned mem address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                   payload->mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_get_data_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->mem_handle, ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle, payload->mem_address,
                                             &param_data_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_get_data_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
        ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
      rc = APR_EFAILED;
      break;
    }

    if ( session_obj->shared_heap.vp3_cache.data_len != 0 )
    {
      if ( payload->mem_size < session_obj->shared_heap.vp3_cache.data_len )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_get_data_cmd_ctrl(): size of VP3 buffer provided by client 0x%X is less than VP3 cache buffer size 0x%X",
                                                payload->mem_size,
                                                session_obj->shared_heap.vp3_cache.data_len );
        rc = APR_EFAILED;
      }
      else
      {
        mmstd_memcpy( param_data_virt_addr.ptr, payload->mem_size,
                         session_obj->shared_heap.vp3_cache.data,
                      session_obj->shared_heap.vp3_cache.data_len );

        rc = cvd_mem_mapper_cache_flush_v2( &param_data_virt_addr, payload->mem_size );
      }

      break;
    }
    else
    {
      rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
      CVP_PANIC_ON_ERROR( rc );
      track_job_obj->orig_src_service = ctrl->packet->src_addr;
      track_job_obj->orig_src_port = ctrl->packet->src_port;
      track_job_obj->orig_token = ctrl->packet->token;
      track_job_obj->orig_opcode = VSS_IVP3_CMD_GET_DATA;
      track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
      track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_cmd_vp3_get_data_rsp_fn;

      session_obj->session_ctrl.transition_job_handle = track_job_obj->header.handle;

      get_param.payload_address_lsw = ( ( uint32_t ) ( param_data_virt_addr.ptr ) );
      get_param.payload_address_msw = 0;
      get_param.module_id = CVP_VP3_MOD_ID;
      get_param.param_id = CVP_VP3_PARAM_ID;
      get_param.param_max_size = payload->mem_size;
      get_param.reserved = 0;
      get_param.mem_map_handle = vpm_mem_handle;

      rc = __aprv2_cmd_alloc_send(
             cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
             session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
             cvp_vpm_addr, session_obj->vocproc_handle,
             track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
             &get_param, sizeof( get_param ) );
      CVP_COMM_ERROR( rc, cvp_vpm_addr );

      ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
      return APR_EOK;
    }
  }

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_vp3_set_data_cmd_ctrl(
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivp3_cmd_set_data_t* payload;
  cvd_virt_addr_t param_data_virt_addr;
  voice_param_data_t* vp3_data;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  for ( ;; )
  {
    if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
    {
      rc = APR_EFAILED;
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivp3_cmd_set_data_t, ctrl->packet );

    rc = cvp_helper_validate_payload_size_control( ctrl, sizeof( vss_ivp3_cmd_set_data_t ) );
    if ( rc ) return APR_EOK;

    rc = cvp_verify_param_data_size( payload->mem_size );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_set_data_cmd_ctrl(): Invalid param data size, %d",
                                              payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    /* Parameter data is out-of-band. */
    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_set_data_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                              payload->mem_handle );
      rc = APR_EHANDLE;
      break;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_set_data_cmd_ctrl(): Mis-aligned mem address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ) );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                       payload->mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_set_data_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->mem_handle, ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle, payload->mem_address,
                                             &param_data_virt_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_vp3_set_data_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
        ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
      rc = APR_EFAILED;
      break;
    }

    rc = cvd_mem_mapper_cache_invalidate_v2( &param_data_virt_addr, payload->mem_size );
    if ( rc ) break;

       if ( payload->mem_size > CVP_MAX_VP3_DATA_LEN )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_vp3_set_data_cmd_ctrl(): Client provided VP3 size 0x%X is greater than maximum supported of 1024 bytes",
                                              payload->mem_size );
      rc = APR_EUNSUPPORTED;
      break;
    }

    mmstd_memcpy( session_obj->shared_heap.vp3_cache.data, CVP_MAX_VP3_DATA_LEN,
                     param_data_virt_addr.ptr, payload->mem_size );
    vp3_data = ( ( voice_param_data_t* ) session_obj->shared_heap.vp3_cache.data );
    session_obj->shared_heap.vp3_cache.data_len = ( sizeof( voice_param_data_t ) +
                                                    vp3_data->param_size ); /* header + data */

    session_obj->is_client_set_vp3_data = TRUE;

    rc = APR_EOK;
    break;
  }

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_vp3_get_size_cmd_ctrl (
  cvp_pending_control_t* ctrl
 )
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* track_job_obj;
  voice_get_param_v2_t get_param;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVP_PANIC_ON_ERROR( rc );

    track_job_obj->orig_src_service = ctrl->packet->src_addr;
    track_job_obj->orig_src_port = ctrl->packet->src_port;
    track_job_obj->orig_token = ctrl->packet->token;
    track_job_obj->orig_opcode = VSS_IVP3_CMD_GET_SIZE;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_vp3_get_size_rsp_fn;

    session_obj->session_ctrl.transition_job_handle = track_job_obj->header.handle;

    get_param.payload_address_lsw = 0;
    get_param.payload_address_msw = 0;
    get_param.module_id = CVP_VP3_MOD_ID;
    get_param.param_id = CVP_VP3_SIZE_PARAM_ID;
    get_param.param_max_size = ( sizeof( voice_param_data_t ) + sizeof( vss_ivp3_rsp_get_size_t ) );
    get_param.reserved = 0;
    get_param.mem_map_handle = 0; /* in-band */

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );
  }

  ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
  return APR_EOK;
}

static int32_t cvp_reinit_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      rc = cvp_helper_create_new_goal_control( ctrl, session_obj, CVP_GOAL_ENUM_REINIT );
      if ( rc ) break;
    }
    else
    {
      rc = cvp_helper_simple_wait_for_goal_completion_control( ctrl );
      if ( rc == APR_EOK )
      {
        break;
      }
    }

    rc = cvp_state_control( session_obj );

    return APR_EPENDING;
  }

  return APR_EOK;
}

static int32_t cvp_mvm_attach_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->attached_mvm_handle == ctrl->packet->src_port )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mvm_attach_cmd_ctrl(): Vocproc has already been " \
                                              "attached to the MVM session handle 0x%04X.",
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->attached_mvm_handle != APR_NULL_V )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mvm_attach_cmd_ctrl(): Vocproc is currently attached " \
                                              "to the MVM session handle 0x%04X, and cannot be attached " \
                                              "to the new MVM session handle 0x%04X",
                                              session_obj->attached_mvm_handle,
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->attached_mvm_handle = ctrl->packet->src_port;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvp_mvm_detach_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->attached_mvm_handle == APR_NULL_V )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mvm_detach_cmd_ctrl(): Vocproc is currently not attached " \
                                            "to any MVM session handle." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    if ( session_obj->attached_mvm_handle != ctrl->packet->src_port )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_mvm_detach_cmd_ctrl(): Vocproc is currently attached to " \
                                              "the MVM session handle 0x%04X, and the caller MVM session's " \
                                              "handle is 0x%04X.",
                                              session_obj->attached_mvm_handle,
                                              ctrl->packet->src_port );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->attached_mvm_handle = APR_NULL_V;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvp_set_voice_timing_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_set_voice_timing_t* in_args;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ivocproc_cmd_set_voice_timing_t ) );
    if ( rc ) break;

    if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
    { /* Setting voice timing when the vocproc is running is not allowed. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_voice_timing_cmd_ctrl(): Vocproc is running." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_set_voice_timing_t,
                                     ctrl->packet );

    /* Save the timing parameters in target_set to be picked up by the state
     * machine.
     */
    session_obj->target_set.voice_timing = *in_args;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );

    break;
  }

  return APR_EOK;
}

static int32_t cvp_get_avsync_delays_cmd_ctrl (
  cvp_pending_control_t * ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t *job_obj;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  /* Response with APR_EFAILED if CVP is not running. */
  if( session_obj->session_ctrl.state != CVP_STATE_ENUM_RUN )
  {
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
    CVP_COMM_ERROR( rc, client_addr );

    return APR_EOK;
  }

  /* Create a track object to forward the response from VPM to MVM. */
  rc = cvp_create_track_job_object( session_obj->header.handle, &job_obj );
  CVP_PANIC_ON_ERROR( rc );

  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] =
    cvp_forward_command_result_rsp_fn;
  job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_DELAYS ] =
    cvp_get_algorithmic_delay_result_rsp_fn;

  job_obj->orig_src_service = ctrl->packet->src_addr;
  job_obj->orig_src_port = ctrl->packet->src_port;
  job_obj->orig_dst_port = ctrl->packet->dst_port;
  job_obj->orig_opcode = ctrl->packet->opcode;
  job_obj->orig_token = ctrl->packet->token;

  /* Send the VPM_CMD_GET_DELAY cmd to VPM. */
  rc = __aprv2_cmd_alloc_send(
         cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
         session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
         cvp_vpm_addr, session_obj->vocproc_handle,
         job_obj->header.handle, VPM_CMD_GET_DELAY,
         NULL, 0 );
  CVP_COMM_ERROR( rc, cvp_vpm_addr );

  /* Free the apr command packet. Success response will be sent from
   * handler set for CVP_RESPONSE_FN_ENUM_GET_DELAYS above.
   */
  ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

  return APR_EOK;
}

static int32_t cvp_ssr_cleanup_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  vss_issr_cmd_cleanup_t* in_args;
  cvp_sequencer_job_object_t* seqjob_obj;
  uint16_t client_addr;
  enum {
    CVP_SEQUENCER_ENUM_UNINITIALIZED,
    CVP_SEQUENCER_ENUM_STOP_VPCM,
    CVP_SEQUENCER_ENUM_WAIT_1,
    CVP_SEQUENCER_ENUM_COMPLETE,
    CVP_SEQUENCER_ENUM_INVALID
  };

  /* Note:
   * 1. A sequencer is used for the processing of this command even though
   *    there is only a single sequencer state. This is done to allow the
   *    implementation to be easily extended for the future phases of the SSR
   *    design, where VSS_ISSR_CMD_CLEANUP command can potentially trigger
   *    a multi-staged CVP cleanup.
   *
   * 2. The current implementation has a limitation where it only handles
   *    Modem SSR properly - it cleans up only the CVP features that can be
   *    enabled by Modem client. The only such feature is VPCM. The way that
   *    VPCM is enabled by the Modem is that Modem gets a CVP handle from HLOS
   *    and use that CVP handle to start VPCM.
   *
   * 3. In general, we don't guarantee that we will handle SSR properly if
   *    resource handles (such as session handles) are shared between
   *    processors. However, this VPCM use case is a special and known use
   *    case, and we are required to handle this.
   */

  if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
  {
    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_issr_cmd_cleanup_t ) );
    if ( rc ) return APR_EOK;

    in_args = APRV2_PKT_GET_PAYLOAD( vss_issr_cmd_cleanup_t, ctrl->packet );

    rc = cvp_create_sequencer_job_object(
           ( cvp_sequencer_job_object_t** ) &ctrl->pendjob_obj );
    CVP_PANIC_ON_ERROR( rc );

    seqjob_obj = &ctrl->pendjob_obj->sequencer_job;
    seqjob_obj->use_case.ssr_cleanup.domain_id = in_args->domain_id;

    ctrl->pendjob_obj->sequencer_job.state = CVP_SEQUENCER_ENUM_STOP_VPCM;
  }

  seqjob_obj = &ctrl->pendjob_obj->sequencer_job;

  for ( ;; )
  {
    switch ( seqjob_obj->state )
    {
      case CVP_SEQUENCER_ENUM_STOP_VPCM:
      {
        /* Stop each of the VPCM sessions on behalf of CVP clients who reside
         * in a subsystem that is being restarted.
         */
        seqjob_obj->state = CVP_SEQUENCER_ENUM_WAIT_1;

        ( void ) cvp_ssr_stop_vpcm( seqjob_obj->use_case.ssr_cleanup.domain_id );
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_WAIT_1:
      { /* Wait for CVP to respond for each vpcm stop command. */
        if ( cvp_ssr_cleanup_cmd_tracking.rsp_cnt <
             cvp_ssr_cleanup_cmd_tracking.num_cmd_issued )
        {
          return APR_EPENDING;
        }

        seqjob_obj->state = CVP_SEQUENCER_ENUM_COMPLETE;
        seqjob_obj->status = APR_EOK;
      }
      /*-fallthru */

    case CVP_SEQUENCER_ENUM_COMPLETE:
      { /* End the sequencer. */
        client_addr = ctrl->packet->src_addr;

        rc = __aprv2_cmd_end_command(
               cvp_apr_handle, ctrl->packet, seqjob_obj->status );
        CVP_COMM_ERROR( rc, client_addr );

        ( void ) cvp_free_object( ( cvp_object_t* ) seqjob_obj );
        ctrl->pendjob_obj = NULL;
      }
      return APR_EOK;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    break;
  }

  return APR_EPENDING;
}

static int32_t cvp_get_hdvoice_config_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  uint16_t client_addr;
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ihdvoice_cmd_get_config_t* in_args;
  vss_ihdvoice_rsp_get_config_t hdvoice_config_entry;

  hdvoice_config_entry.enable_mode = VSS_PARAM_FEATURE_FORCED_OFF;
  hdvoice_config_entry.rx_pp_sr = 0;
  hdvoice_config_entry.feature_id = VSS_ICOMMON_CAL_FEATURE_NONE;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  client_addr = ctrl->packet->src_addr;

  rc = cvp_helper_validate_payload_size_control(
        ctrl, sizeof( vss_ihdvoice_cmd_get_config_t ) );
  if ( rc ) return APR_EOK;


  in_args = APRV2_PKT_GET_PAYLOAD( vss_ihdvoice_cmd_get_config_t,
                                  ctrl->packet );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Determining if HD Voice should be enabled by searching Device Config based on MVM system config. cvp_get_hdvoice_config_cmd_ctrl(): Network 0x%08X, " \
                                        "TX PP SR %d, RX PP SR %d",
                                        in_args->network_id,
                                        in_args->tx_pp_sr,
                                        in_args->rx_pp_sr );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Determining if HD Voice should be enabled by searching Device Config based on MVM system config. cvp_get_hdvoice_config_cmd_ctrl(): TX voc op mode 0x%08X, " \
                                        "RX voc op mode 0x%08X, Media ID 0x%08X",
                                        in_args->tx_voc_op_mode,
                                        in_args->rx_voc_op_mode,
                                        in_args->media_id );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Determining if HD Voice should be enabled by searching Device Config based on MVM system config. cvp_get_hdvoice_config_cmd_ctrl(): Feature 0x%08X.",
                                        in_args->feature_id );

  ( void ) cvp_helper_search_hdvoice_config( session_obj, in_args, &hdvoice_config_entry );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "CVD_CAL_MSG: Found the following feature for the current device and system config. cvp_get_hdvoice_config_cmd_ctrl(): " \
                                        "Feature=0x%08x Enable Mode=%d RX PP SR=%d",
                                        hdvoice_config_entry.feature_id,
                                        hdvoice_config_entry.enable_mode,
                                        hdvoice_config_entry.rx_pp_sr );

  rc = __aprv2_cmd_alloc_send(
      cvp_apr_handle, APRV2_PKT_MSGTYPE_CMDRSP_V,
      ctrl->packet->dst_addr, ctrl->packet->dst_port,
      ctrl->packet->src_addr, ctrl->packet->src_port,
      ctrl->packet->token, VSS_IHDVOICE_RSP_GET_CONFIG,
      &hdvoice_config_entry, sizeof( hdvoice_config_entry ) );
  CVP_COMM_ERROR( rc, client_addr );

  ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );
  return APR_EOK;
}

static int32_t cvp_soundfocus_validate_angles (
  vss_isoundfocus_cmd_set_sectors_t* set_sectors
)
{
  int32_t rc = APR_EOK;
  int32_t idx;

  if ( set_sectors == NULL )
    return APR_EBADPARAM;

  /* start_angles array has eight elements.
     Validate below conditions if the sectors are enabled:
     - Angle range is [0, 359].
     - Angles must be strictly increasing. */
  for ( idx = 0; idx < 7; ++idx )
  {
    if ( ( set_sectors->enables[ idx ] != VSS_ISOUNDFOCUS_SECTOR_NOT_USED ) &&
         ( set_sectors->start_angles[ idx ] > 359 ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_soundfocus_validate_angles(): bad param: "
             "start_angles[%d] = %d",
             idx, set_sectors->start_angles[ idx ] );
      rc = APR_EBADPARAM;
      break;
    }

    if ( ( set_sectors->enables[ idx + 1 ] != VSS_ISOUNDFOCUS_SECTOR_NOT_USED ) &&
         ( set_sectors->start_angles[ idx ] >= set_sectors->start_angles[ idx + 1 ] ) )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_soundfocus_validate_angles(): bad param: "
             "start_angles[%d] = %d, start_angles[%d] = %d",
             idx, set_sectors->start_angles[ idx ],
             idx+1, set_sectors->start_angles[ idx+1 ] );
      rc = APR_EBADPARAM;
      break;
    }
  }

  if ( ( set_sectors->enables[ 7 ] != VSS_ISOUNDFOCUS_SECTOR_NOT_USED ) &&
       ( set_sectors->start_angles[ 7 ] > 359 ) )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_soundfocus_validate_angles(): bad param: "
           "start_angles[7] = %d ", set_sectors->start_angles[ 7 ] );
    rc = APR_EBADPARAM;
  }

  return rc;
}

static int32_t cvp_soundfocus_set_sectors_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_isoundfocus_cmd_set_sectors_t* in_args;
  uint32_t size;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    if ( ctrl->state == CVP_PENDING_CMD_STATE_ENUM_EXECUTE )
    {
      /* Validate the inbound payload size. */
      size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );
      if ( size != sizeof( vss_isoundfocus_cmd_set_sectors_t ) )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_soundfocus_set_sectors_cmd_ctrl(): Unexpected size, %d != %d",
               size, sizeof( vss_isoundfocus_cmd_set_sectors_t ) );
        rc = APR_EBADPARAM;
        break;
      }

      in_args = APRV2_PKT_GET_PAYLOAD( vss_isoundfocus_cmd_set_sectors_t, ctrl->packet );

      /* Validate payload. */
      if ( cvp_soundfocus_validate_angles( in_args ) != APR_EOK )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_soundfocus_set_sectors_cmd_ctrl(): Bad param" );
        rc = APR_EBADPARAM;
        break;
      }

      session_obj->target_set.soundfocus_sectors = *in_args;
      session_obj->is_client_set_sectors = TRUE;

      /* If in RUN state, set the sectors on VPM. Note that changing the
       * sectors at RUN time will not cause any KPPS requirement change
       * (confimred with VCP team).
       *
       * If in IDLE state, leave settings in the target_set. They will be
       * picked up when we go to RUN (either upon receiving a
       * VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG command if the vocproc is attached
       * to MVM, or upon IDLE to RUN transition if the vocproc is not attached
       * to MVM).
       */
      if ( session_obj->session_ctrl.state == CVP_STATE_ENUM_RUN )
      {
        session_obj->session_ctrl.status = APR_UNDEFINED_ID_V;
        rc = cvp_action_set_soundfocus_sectors( session_obj );
      }
      else
      {
        rc = APR_EOK;
        break;
      }
    }
    else if ( session_obj->session_ctrl.status != APR_UNDEFINED_ID_V )
    {
      rc = session_obj->session_ctrl.status;
      break;
    }

    return APR_EPENDING;
  }

  client_addr = ctrl->packet->src_addr;

  rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
  CVP_COMM_ERROR( rc, client_addr );

  return APR_EOK;
}

static int32_t cvp_soundfocus_get_sectors_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* track_job_obj;
  voice_get_param_v2_t get_param;
  uint16_t client_addr;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    get_param.payload_address_lsw = 0;
    get_param.payload_address_msw = 0;
    get_param.module_id = VOICEPROC_MODULE_TX;
    get_param.param_id = VOICE_PARAM_FLUENCE_SOUNDFOCUS;
    get_param.param_max_size = sizeof( cvp_soundfocus_param_t );
    get_param.reserved = 0;
    get_param.mem_map_handle = 0;

    rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVP_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_soundfocus_get_sectors_rsp_fn;

    /* Store the original destination and opcode info from the packet. */
    track_job_obj->orig_src_service = ctrl->packet->src_addr;
    track_job_obj->orig_src_port = ctrl->packet->src_port;
    track_job_obj->orig_opcode = ctrl->packet->opcode;
    track_job_obj->orig_token = ctrl->packet->token;

    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_soundfocus_get_sectors_cmd_ctrl(): Sending GET_PARAM_V2 to VPM" );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    break;
  }

  if ( rc )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
    CVP_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

static int32_t cvp_sourcetrack_get_activity_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  cvp_track_job_object_t* track_job_obj;
  vss_isourcetrack_cmd_get_activity_t* get_activity;
  uint16_t client_addr;
  uint32_t vpm_mem_handle;
  cvd_virt_addr_t param_data_virt_addr;
  voice_get_param_v2_t get_param;

  rc = cvp_helper_open_session_control( ctrl, &session_obj, TRUE );
  if ( rc ) return APR_EOK;

  rc = cvp_helper_validate_payload_size_control(
         ctrl, sizeof( vss_isourcetrack_cmd_get_activity_t ) );
  if ( rc ) return APR_EOK;

  for ( ;; )
  {
    client_addr = ctrl->packet->src_addr;

    get_activity = APRV2_PKT_GET_PAYLOAD( vss_isourcetrack_cmd_get_activity_t,
                                          ctrl->packet );

    { /* Parameter validation. */
      if ( get_activity->mem_size < sizeof( vss_isourcetrack_activity_data_t ) )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): mem size is too small: %d",
               get_activity->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_handle( get_activity->mem_handle );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): Invalid mem_handle: 0x%08X",
               get_activity->mem_handle );
        rc = APR_EHANDLE;
        break;
      }

      ( void ) cvd_mem_mapper_get_vpm_mem_handle( get_activity->mem_handle, &vpm_mem_handle );

      rc = cvd_mem_mapper_validate_attributes_align( get_activity->mem_handle, get_activity->mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): Mis-aligned mem address: 0x%016X",
               get_activity->mem_address );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( get_activity->mem_handle, get_activity->mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): Mis-aligned mem size: 0x%08X",
               get_activity->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( get_activity->mem_handle, get_activity->mem_address,
                                                     get_activity->mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): Memory is not within range, "
               "mem_handle: 0x%08X, addr: 0x%016X, size: %d",
               get_activity->mem_handle, get_activity->mem_address, get_activity->mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr_v2( get_activity->mem_handle, get_activity->mem_address,
                                               &param_data_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_sourcetrack_get_activity_cmd_ctrl(): Cannot get virtual address, "
               "mem_handle: 0x%08X, addr: 0x%016X", get_activity->mem_handle, get_activity->mem_address );
        rc = APR_EFAILED;
        break;
      }
    }

    session_obj->sourcetrack_mem_addr = param_data_virt_addr;

    get_param.payload_address_lsw = ( ( uint32_t ) &session_obj->shared_heap.sourcetrack_activity );
    get_param.payload_address_msw = 0;
    get_param.module_id = VOICEPROC_MODULE_TX;
    get_param.param_id = VOICE_PARAM_FLUENCE_SOURCETRACKING;
    get_param.param_max_size = get_activity->mem_size;
    get_param.reserved = 4096;
    get_param.mem_map_handle = session_obj->shared_heap.vpm_mem_handle;

    rc = cvp_create_track_job_object( session_obj->header.handle, &track_job_obj );
    CVP_PANIC_ON_ERROR( rc );
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_RESULT ] = cvp_forward_command_result_rsp_fn;
    track_job_obj->fn_table[ CVP_RESPONSE_FN_ENUM_GET_PARAM ] = cvp_forward_command_sourcetrack_get_activity_rsp_fn;

    /* Store the original destination and opcode info from the packet. */
    track_job_obj->orig_src_service = ctrl->packet->src_addr;
    track_job_obj->orig_src_port = ctrl->packet->src_port;
    track_job_obj->orig_opcode = ctrl->packet->opcode;
    track_job_obj->orig_token = ctrl->packet->token;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "cvp_sourcetrack_get_activity_cmd_ctrl(): "
           "mem_handle: 0x%08X, addr: 0x%016X, size: %d",
           get_activity->mem_handle, get_activity->mem_address, get_activity->mem_size );

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_SEQCMD_V,
           session_obj->self_addr, ( ( uint16_t ) session_obj->header.handle ),
           cvp_vpm_addr, session_obj->vocproc_handle,
           track_job_obj->header.handle, VOICE_CMD_GET_PARAM_V2,
           &get_param, sizeof( get_param ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );

    ( void ) __aprv2_cmd_free( cvp_apr_handle, ctrl->packet );

    break;
  }

  if ( rc )
  {
    client_addr = ctrl->packet->src_addr;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
    CVP_COMM_ERROR( rc, client_addr );
  }

  return APR_EOK;
}

/* Set device channel count information.
 */
static int32_t cvp_set_dev_channels_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_topology_set_dev_channels_t* in_args;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    rc = cvp_helper_validate_payload_size_control(
           ctrl, sizeof( vss_ivocproc_cmd_topology_set_dev_channels_t ) );
    if ( rc ) break;

    if ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_NONE )
    { /* Setting topology params is not allowed after commit. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_set_dev_channels_cmd_ctrl(): not allowed." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    in_args = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_topology_set_dev_channels_t,
                                     ctrl->packet );

    /* Error check.
     */
    if ( ( in_args->tx_num_channels < VSS_NUM_DEV_CHANNELS_1 ) ||
      ( in_args->tx_num_channels > VSS_NUM_DEV_CHANNELS_4 ) ||
      ( in_args->rx_num_channels != VSS_NUM_DEV_CHANNELS_1 ) )
    { /* In valid topology params. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_extract_topology_params(): In valid params: #Mics %d, #Speaker Channels %d",
        in_args->tx_num_channels, in_args->rx_num_channels );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }
    /* Save the topology parameters.
     */
    session_obj->topo_param.num_dev_chans.tx_num_channels = in_args->tx_num_channels;
    session_obj->topo_param.num_dev_chans.rx_num_channels = in_args->rx_num_channels;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
      "cvp_set_dev_channels_cmd_ctrl(): tx_num_channels = %d, rx_num_channels = %d",
      in_args->tx_num_channels, in_args->rx_num_channels);

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );
    break;
  }

  return APR_EOK;
}

/* Register OOB topology parameters.
 */
static int32_t cvp_register_topology_params_cmd_ctrl  (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivocproc_cmd_topology_register_params_t* payload;
  uint32_t payload_len;
  uint16_t client_addr;
  cvd_virt_addr_t vir_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
    if ( rc ) break;
    client_addr = ctrl->packet->src_addr;

    if ( session_obj->topo_param.is_registered == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
      "cvp_register_topology_params_cmd_ctrl(): topology params already registered." );
      rc = APR_EALREADY;
    }
    if ( session_obj->topo_commit_state == CVP_TOPOLOGY_COMMIT_STATE_NONE )
    { /* Registering topology params is not allowed after commit. */
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_topology_params_cmd_ctrl(): not allowed." );
      rc = APR_EFAILED;
    }
    if ( rc )
    {
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivocproc_cmd_topology_register_params_t, ctrl->packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( ctrl->packet->header );

    if ( payload_len < sizeof( vss_ivocproc_cmd_topology_register_params_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_topology_params_cmd_ctrl(): "
                                              "Unexpected payload size, %d < %d",
                                              payload_len,
                                              sizeof( vss_ivocproc_cmd_register_static_calibration_data_t ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    /* Validate Config param shared memory buffer. */
    rc = cvd_mem_mapper_validate_handle( payload->mem_handle );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_register_topology_params_cmd_ctrl(): Invalid mem_handle: 0x%08X",
                                              payload->mem_handle );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EHANDLE );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvd_mem_mapper_validate_attributes_align( payload->mem_handle, payload->mem_address );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_topology_params_cmd_ctrl(): Mis-aligned mem address:"
        " lsw: 0x%08X, msw: 0x%08X", ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvd_mem_mapper_validate_mem_is_in_region( payload->mem_handle, payload->mem_address,
                                                   payload->mem_size );
    if ( rc )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_topology_params_cmd_ctrl(): Memory is not within range,"
        " cal_mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X, size: %d",
        payload->mem_handle, ( uint32_t )payload->mem_address,
        ( uint32_t )( payload->mem_address >> 32 ), payload->mem_size );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EBADPARAM );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }

    rc = cvd_mem_mapper_get_virtual_addr_v2( payload->mem_handle,
                                             payload->mem_address,
                                             &vir_addr );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_topology_params_cmd_ctrl(): Cannot get virtual address,"
        " mem_handle: 0x%08X, addr: lsw: 0x%08X, msw: 0x%08X", payload->mem_handle,
        ( uint32_t )payload->mem_address, ( uint32_t )( payload->mem_address >> 32 ) );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EFAILED );
      CVP_COMM_ERROR( rc, client_addr );
      return APR_EOK;
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_register_topology_params_cmd_ctrl(): param memory virtual address 0x%016X, size %d",
                                            vir_addr.ptr,
                                            payload->mem_size );
    }

    ( void ) cvd_mem_mapper_cache_invalidate_v2( &vir_addr, payload->mem_size );
    session_obj->topo_param.vir_addr = vir_addr.ptr;
    session_obj->topo_param.mem_size = payload->mem_size;
    rc = cvp_extract_topology_params( session_obj, TRUE );
    if ( rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
        "cvp_register_topology_params_cmd_ctrl(): Invalid topology params" );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, rc );
      CVP_COMM_ERROR( rc, client_addr );
      session_obj->topo_param.vir_addr = NULL;
      session_obj->topo_param.mem_size = 0;
      return APR_EOK;
    }
    session_obj->topo_param.is_registered = TRUE;

    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );
    break;
  }
  return APR_EOK;
}

/* Parse OOB topology parameters.
 */
static int32_t cvp_extract_topology_params  (
  cvp_session_object_t* session_obj,
  bool_t error_check_only_flag
)
{
  void* vir_addr;
  cvp_topology_param_entry_t* topo_param_entry;
  uint32_t parsed_size = 0;
  uint32_t mem_size;
  uint32_t param_cnt = 0;
  vpm_create_param_t* vpm_param_ptr;

  if ( session_obj == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): " \
        "session_obj not found");
    return APR_EFAILED;
  }

  if ( ( error_check_only_flag == FALSE ) && ( session_obj->topo_param.is_registered != TRUE ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_extract_topology_params(): " \
        "OOB Topo params not registered");
    /* Extract in-band params if available */
    if ( session_obj->topo_param.num_dev_chans.tx_num_channels != 0 )
    {
      session_obj->topo_param.vpm_param[ 0 ].param_id = VSS_PARAM_NUM_DEV_CHANNELS;
      session_obj->topo_param.vpm_param[ 0 ].param_size = sizeof ( vss_param_num_dev_channels_t );
      session_obj->topo_param.vpm_param[ 0 ].param_virt_addr =
                                    ( uint32_t ) ( & (session_obj->topo_param.num_dev_chans ) );
    }
    return APR_EOK;
  }
  vir_addr = session_obj->topo_param.vir_addr;
  mem_size = session_obj->topo_param.mem_size;

  do
  {
    topo_param_entry =
      ( cvp_topology_param_entry_t* ) ( ( ( uint8_t* ) vir_addr ) + parsed_size );

    if ( topo_param_entry == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): " \
        "topo_param Entry is NULL");
      return APR_EFAILED;
    }

    if ( ( ( ( ( uint8_t* ) topo_param_entry ) + sizeof( cvp_topology_param_entry_t ) ) >
         ( ( ( uint8_t* ) vir_addr ) + mem_size ) ) ||
         ( ( ( ( uint8_t* ) topo_param_entry ) + topo_param_entry->param_size ) >
         ( ( ( uint8_t* ) vir_addr ) + mem_size ) ) )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): " \
        "Topology_param Entry out Virt Mem Bounds. " \
        "entry_addr=0x%08x entry_size=0x%08x, virt_addr=0x%08x, virt_mem_size=0x%08x",
        topo_param_entry,
        topo_param_entry->param_size,
        vir_addr,
        mem_size);
      return APR_EFAILED;
    }

    switch ( topo_param_entry->module_id )
    {
      case VSS_MODULE_CVD_GENERIC:
      {
        /* Extract Config Params. */
        switch ( topo_param_entry->param_id )
        {
          case VSS_PARAM_NUM_DEV_CHANNELS:
          {
            uint16_t num_mics;
            uint16_t num_spkr_chans;
            vss_param_num_dev_channels_t* params_ptr;

            if ( topo_param_entry->param_size != sizeof ( vss_param_num_dev_channels_t ) )
            { /* In valid topology params. */
              MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): In valid param size." );
              return APR_EBADPARAM;
            }
            params_ptr = ( vss_param_num_dev_channels_t* ) ( ( ( uint8_t* ) &topo_param_entry->param_size ) +
                                                             sizeof ( vss_param_num_dev_channels_t ) );
            if ( params_ptr == NULL )
            { /* In valid topology params. */
              MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): In valid param address." );
              return APR_EBADPARAM;
            }
            num_mics = params_ptr->tx_num_channels;
            num_spkr_chans = params_ptr->rx_num_channels;

            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED, "cvp_extract_topology_params(): #Mics %d, #Speaker Channels %d",
                                              num_mics, num_spkr_chans );
            /* Error check.
             */
            if ( ( num_mics < VSS_NUM_DEV_CHANNELS_1 ) ||
            ( num_mics > VSS_NUM_DEV_CHANNELS_4 ) ||
            ( num_spkr_chans != VSS_NUM_DEV_CHANNELS_1 ) )
            { /* In valid topology params. */
              MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "cvp_extract_topology_params(): In valid params: #Mics %d, #Speaker Channels %d",
                num_mics, num_spkr_chans );
              return APR_EBADPARAM;
            }
            if ( error_check_only_flag )
              break;
            vpm_param_ptr = &session_obj->topo_param.vpm_param[ param_cnt ];
            vpm_param_ptr->param_id = topo_param_entry->param_id;
            vpm_param_ptr->param_size = topo_param_entry->param_size;
            /* Apply in-band topology params if available. */
            if ( session_obj->topo_param.num_dev_chans.tx_num_channels > 0 )
              vpm_param_ptr->param_virt_addr = ( uint32_t ) ( & (session_obj->topo_param.num_dev_chans ) );
            else
              vpm_param_ptr->param_virt_addr = ( uint32_t ) params_ptr;
            session_obj->topo_param.vpm_param_size += sizeof ( vpm_create_param_t );
            param_cnt++;
            break;
          }
          default:
          {
             MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): " \
             "Invalid param_id 0x%08X", topo_param_entry->param_id );
            break;
          }
        }
        break;
      }
      default:
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_extract_topology_params(): " \
          "Invalid mod_id 0x%08X",
          topo_param_entry->module_id );
        break;
      }
    }
    parsed_size += ( topo_param_entry->param_size + sizeof( voice_param_data_t ) );
  } while ( parsed_size < mem_size );

  return APR_EOK;
}

/* Deregister OOB topology parameters.
 */
static int32_t cvp_deregister_topology_params_cmd_ctrl (
  cvp_pending_control_t* ctrl
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  uint16_t client_addr;

  for ( ;; )
  {
    rc = cvp_helper_open_session_control( ctrl, &session_obj, FALSE );
    if ( rc ) break;

    client_addr = ctrl->packet->src_addr;

    if ( session_obj->topo_param.is_registered == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
      "cvp_deregister_topology_params_cmd_ctrl(): topology params not registered." );
      rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EALREADY );
      CVP_COMM_ERROR( rc, client_addr );
      break;
    }

    session_obj->topo_param.is_registered = FALSE;
    session_obj->topo_param.mem_size = 0;
    session_obj->topo_param.vir_addr = NULL;
    rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EOK );
    CVP_COMM_ERROR( rc, client_addr );
    break;
  }

  return APR_EOK;
}

static int32_t cvp_vpcm_evt_push_buffer_to_vpm_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivpcm_evt_push_buffer_v2_t* payload;
  uint32_t payload_len;
  voice_evt_push_host_pcm_buf_v2_t push_buf_event;
  uint64_t out_buf_virt_addr = 0;
  uint64_t in_buf_virt_addr = 0;
  uint32_t cvd_mem_handle;
  uint32_t handle;
  bool_t is_lock_acquired = TRUE;

  handle = packet->dst_port;
  for ( ;; )
  {
    rc = cvp_access_session_obj_start( handle, &session_obj );
    if ( rc != APR_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Invalid session "
             "handle, packet->dst_port: 0x%04X", packet->dst_port );
      is_lock_acquired = FALSE;
      break;
    }

    /* Read access to vpcm_info/vocproc_handle. */
    ( void ) apr_lock_enter ( session_obj->lock );
    if ( session_obj->vpcm_info.is_enabled == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "cvp_vpcm_evt_push_buffer_to_vpm_processing(): VPCM not started" );
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( vss_ivpcm_evt_push_buffer_v2_t, packet );
    payload_len = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );

    if ( payload_len != sizeof( vss_ivpcm_evt_push_buffer_v2_t ) )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Invalid data. "
             "Payload len: %d, expected len: %d", payload_len,
             sizeof( vss_ivpcm_evt_push_buffer_v2_t ) );
      break;
    }

    cvd_mem_handle = session_obj->vpcm_info.mem_handle;

    if ( payload->push_buf_mask & VSS_IVPCM_PUSH_BUFFER_MASK_OUTPUT_BUFFER )
    {
      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->out_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Mis-aligned "
               "output buf mem address: 0x%016X", payload->out_buf_mem_address );
        break;
      }

      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->out_buf_mem_size );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Mis-aligned "
               "output buf mem size: %d", payload->out_buf_mem_size );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( cvd_mem_handle, payload->out_buf_mem_address,
                                                     payload->out_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Memory is not "
               "within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d",
               cvd_mem_handle, payload->out_buf_mem_address,
               payload->out_buf_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( cvd_mem_handle,
                                            payload->out_buf_mem_address,
                                            &out_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Cannot get "
               "virtual address, mem_handle 0x%08X, out_buf_mem_address 0x%016X",
               cvd_mem_handle, payload->out_buf_mem_address );
        rc = APR_EFAILED;
        break;
      }
    }

    if ( payload->push_buf_mask & VSS_IVPCM_PUSH_BUFFER_MASK_INPUT_BUFFER )
    {
      rc = cvd_mem_mapper_validate_attributes_align( cvd_mem_handle, payload->in_buf_mem_address );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Misaligned input "
               "buf mem address: 0x%016X", payload->in_buf_mem_address );
        break;
      }

      rc = cvd_mem_mapper_validate_mem_is_in_region( cvd_mem_handle, payload->in_buf_mem_address,
                                                     payload->in_buf_mem_size );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Memory is not "
               "within range, mem_handle: 0x%08X, addr: 0x%016X, size: %d",
               cvd_mem_handle, payload->in_buf_mem_address,
               payload->in_buf_mem_size );
        rc = APR_EBADPARAM;
        break;
      }

      rc = cvd_mem_mapper_get_virtual_addr( cvd_mem_handle,
                                            payload->in_buf_mem_address,
                                            &in_buf_virt_addr );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Cannot get "
               "virtual address, mem_handle: 0x%08X, in_buf_mem_address: "
               "0x%016X", cvd_mem_handle, payload->in_buf_mem_address );
        rc = APR_EFAILED;
        break;
      }
    }

    switch( payload->tap_point )
    {
    case VSS_IVPCM_TAP_POINT_TX_DEFAULT:
      push_buf_event.tap_point = VOICEPROC_MODULE_TX;
      break;

    case VSS_IVPCM_TAP_POINT_RX_DEFAULT:
      push_buf_event.tap_point = VOICEPROC_MODULE_RX;
      break;

    default:
      /* Drop and free the push buffer event. */
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "cvp_vpcm_evt_push_buffer_to_vpm_processing(): Invalid tap "
            "point: 0x%08X", payload->tap_point );
      rc = APR_EBADPARAM;
      break;
    }

    if ( rc ) break;

    push_buf_event.mask = payload->push_buf_mask;
    push_buf_event.rd_buff_addr_lsw = ( ( uint32_t ) out_buf_virt_addr );
    push_buf_event.rd_buff_addr_msw = ( ( uint32_t ) ( out_buf_virt_addr >> 32 ) );
    push_buf_event.wr_buff_addr_lsw = ( ( uint32_t ) in_buf_virt_addr );
    push_buf_event.wr_buff_addr_msw = ( ( uint32_t ) ( in_buf_virt_addr >> 32 ) );
    push_buf_event.rd_buff_size = payload->out_buf_mem_size;
    push_buf_event.wr_buff_size = payload->in_buf_mem_size;
    push_buf_event.sampling_rate = payload->sampling_rate;
    push_buf_event.wr_num_chan = payload->num_in_channels;

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           session_obj->self_addr, ( uint16_t ) session_obj->header.handle,
           cvp_vpm_addr, session_obj->vocproc_handle,
           packet->token, VOICE_EVT_PUSH_HOST_BUF_V2,
           &push_buf_event, sizeof( push_buf_event ) );
    CVP_COMM_ERROR( rc, cvp_vpm_addr );

    break;
  }

  if ( is_lock_acquired == TRUE )
  {
    ( void ) apr_lock_leave ( session_obj->lock );
  }

  ( void ) cvp_access_session_obj_end( handle );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );

  return APR_EOK;
}

static int32_t cvp_vpcm_evt_notify_to_client_processing (
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_session_object_t* session_obj;
  vss_ivpcm_evt_notify_v2_t notify_event;
  voice_evt_notify_host_pcm_buf_v2_t* payload;
  uint64_t out_buf_virt_addr;
  uint64_t out_buf_mem_addr;
  uint64_t in_buf_virt_addr;
  uint64_t in_buf_mem_addr;
  uint32_t handle;
  bool_t is_lock_acquired = TRUE;

  handle = packet->dst_port;
  for ( ;; )
  {
    rc = cvp_access_session_obj_start ( handle,  &session_obj );
    if ( rc != APR_EOK || session_obj->vpcm_info.client_addr == APRV2_PKT_INIT_ADDR_V )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_evt_notify_to_client_processing(): Invalid session "
             "handle, packet->dst_port: 0x%04X", packet->dst_port );
      is_lock_acquired = FALSE;
      break;
    }

    payload = APRV2_PKT_GET_PAYLOAD( voice_evt_notify_host_pcm_buf_v2_t, packet );

    switch( payload->tap_point )
    {
    case VOICEPROC_MODULE_TX:
      notify_event.tap_point = VSS_IVPCM_TAP_POINT_TX_DEFAULT;
      break;

    case VOICEPROC_MODULE_RX:
      notify_event.tap_point = VSS_IVPCM_TAP_POINT_RX_DEFAULT;
      break;

    default:
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "cvp_vpcm_evt_notify_to_client_processing(): Invalid "
             "tap point: 0x%08X", payload->tap_point );
      is_lock_acquired = FALSE;
      rc = APR_EBADPARAM;
      break;
    }

    /* Read access to vpcm_info.*/
    apr_lock_enter( session_obj->lock );

    notify_event.notify_mask = payload->mask;
    if ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER )
    {
      out_buf_virt_addr = payload->rd_buff_addr_msw;
      out_buf_virt_addr = ( ( out_buf_virt_addr << 32 ) | payload->rd_buff_addr_lsw );

      rc = cvd_mem_mapper_get_mem_addr( session_obj->vpcm_info.mem_handle,
                                        out_buf_virt_addr, &out_buf_mem_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_notify_to_client_processing(): Unable to get "
               "mem_addr for out_buf_virt_addr: 0x%016X", out_buf_virt_addr );
        break;
      }

      notify_event.out_buf_mem_address = out_buf_mem_addr;
    }

    if ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER )
    {
      in_buf_virt_addr = payload->wr_buff_addr_msw;
      in_buf_virt_addr = ( ( in_buf_virt_addr << 32 ) | payload->wr_buff_addr_lsw );

      rc = cvd_mem_mapper_get_mem_addr( session_obj->vpcm_info.mem_handle,
                                        in_buf_virt_addr, &in_buf_mem_addr );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "cvp_vpcm_evt_notify_to_client_processing(): Unable to "
               "get mem_addr for in_buf_virt_addr: 0x%016X", in_buf_virt_addr );
        break;
      }

      notify_event.in_buf_mem_address = in_buf_mem_addr;
    }

    notify_event.filled_out_size = payload->rd_buff_size;
    notify_event.request_buf_size = payload->wr_buff_size;
    notify_event.sampling_rate = payload->sampling_rate;

    /* For OUTPUT only mode or dual mic OUTPUT-INPUT mode, write buffer size
       and hence request buffer size will be zero and need to be derived from
       read buffer size. */
    if ( notify_event.request_buf_size == 0 )
    {
      /* Derive request buffer size from read buffer size. */
      notify_event.request_buf_size = payload->rd_buff_size;

      /* If the output buffer mask is not set, set the filled out size to zero. */
      if ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER ) == 0 )
      {
        notify_event.filled_out_size = 0;
      }
    }

    /* If the mode is NONE or INPUT only, number of output channels is derived
       from num of write channels from FW. */
    if ( ( notify_event.notify_mask &
           ( VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER | VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER ) ) == 0
         || ( ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_INPUT_BUFFER ) != 0 )
         && ( ( notify_event.notify_mask & VSS_IVPCM_NOTIFY_MASK_OUTPUT_BUFFER ) == 0 ) ) )
    {
      /* Number of expected input channels. */
      notify_event.num_out_channels = payload->wr_num_chan;
    }
    else /* If OUTPUT only or OUTPUT-INPUT mode. */
    {
      /* Number of output channels.  */
      notify_event.num_out_channels = payload->rd_num_chan;
    }

    rc = __aprv2_cmd_alloc_send(
           cvp_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
           session_obj->self_addr, ( uint16_t ) session_obj->header.handle,
           session_obj->vpcm_info.client_addr, session_obj->vpcm_info.client_handle,
           packet->token, VSS_IVPCM_EVT_NOTIFY_V2,
           &notify_event, sizeof( notify_event ) );
    CVP_COMM_ERROR( rc, session_obj->vpcm_info.client_addr );

    break;
  }

  if ( is_lock_acquired == TRUE )
  {
   ( void ) apr_lock_leave ( session_obj->lock );
  }

  ( void ) cvp_access_session_obj_end( handle );
  ( void ) __aprv2_cmd_free( cvp_apr_handle, packet );

  return APR_EOK;
}

/****************************************************************************
 * DISPATCHING ROUTINES                                                     *
 ****************************************************************************/

static int32_t cvp_isr_dispatch_fn (
  aprv2_packet_t* packet,
  void* dispatch_data
)
{
  cvp_queue_incoming_packet( CVP_THREAD_PRIORITY_ENUM_HIGH, packet );

  return APR_EOK;
}

static void cvp_response_fn_trampoline (
  uint32_t fn_index,
  aprv2_packet_t* packet
)
{
  int32_t rc;
  cvp_object_t* object;
  uint32_t msg_type;

  msg_type = APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header );

  if ( msg_type == APRV2_PKT_MSGTYPE_EVENT_V )
  {
    rc = cvp_get_object( packet->dst_port, &object );
  }
  else
  {
    rc = cvp_get_object( packet->token, &object );
  }

  if ( rc == APR_EOK )
  {
    switch ( object->header.type )
    {
    case CVP_OBJECT_TYPE_ENUM_SIMPLE_JOB:
      object->simple_job.fn_table[ fn_index ]( packet );
      return;

    default:
      break;
    }
  }

  rc = __aprv2_cmd_free( cvp_apr_handle, packet );
}

/* Process high priority events, responses, and non-gating commands in task
context. */
static void cvp_high_task_process_nongating_commands ( void )
{
  cvp_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &cvp_high_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
    /* Currently high priority task just dispatch commands to
       medium priority task. */
    default:
      cvp_queue_incoming_packet( CVP_THREAD_PRIORITY_ENUM_MED, packet );
      break;
    }
  }
}

static void cvp_high_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void cvp_med_task_process_nongating_commands ( void )
{
  cvp_work_item_t* work_item;
  aprv2_packet_t* packet;

  while ( apr_list_remove_head( &cvp_med_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
      case VSS_IVPCM_EVT_PUSH_BUFFER_V2:
        ( void ) cvp_vpcm_evt_push_buffer_to_vpm_processing( packet );
        break;

      case VOICE_EVT_HOST_BUF_AVAILABLE_V2:
        ( void ) cvp_vpcm_evt_notify_to_client_processing( packet );
        break;

      case VSS_IVOCPROC_EVT_TX_DTMF_DETECTED:
        ( void ) cvp_forward_evt_tx_dtmf_detected_processing( packet );
        break;

      case VSS_IVOCPROC_CMD_SET_TX_DTMF_DETECTION:
        ( void ) cvp_set_tx_dtmf_detection_cmd_processing( packet );
        break;

      case VPM_EVT_VOICE_ACTIVITY_UPDATE:
        ( void ) cvp_forward_voice_activity_event_to_mvm( packet );
        break;

      default:
        cvp_queue_incoming_packet( CVP_THREAD_PRIORITY_ENUM_LOW, packet );
        break;
    }

  }

  return;
}

static void cvp_med_task_process_gating_commands ( void )
{
  /* Currently nothing here. */
}

static void cvp_low_task_process_nongating_commands ( void )
{
  int32_t rc;
  cvp_work_item_t* work_item;
  aprv2_packet_t* packet;
  uint16_t client_addr;

  while ( apr_list_remove_head( &cvp_low_task_incoming_cmd_q,
                                ( ( apr_list_node_t** ) &work_item ) )
          == APR_EOK )
  {
    packet = ( ( aprv2_packet_t* ) work_item->packet );
    ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );

    switch ( packet->opcode )
    {
      case APRV2_IBASIC_EVT_ACCEPTED:
        /* TODO: Events use dst_port, but trampoline use token for results. */
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_ACCEPTED, packet );
        break;

      case APRV2_IBASIC_RSP_RESULT:
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_RESULT, packet );
        break;

      case VSS_ICOMMON_RSP_GET_PARAM:
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_GET_PARAM, packet );
        break;

      case VOICE_RSP_SHARED_MEM_MAP_REGIONS:
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_MAP_MEMORY, packet );
        break;

      case VPM_RSP_GET_KPPS_ACK:
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_GET_KPPS, packet );
        break;

      case VPM_RSP_GET_DELAY_ACK:
        cvp_response_fn_trampoline( CVP_RESPONSE_FN_ENUM_GET_DELAYS, packet );
        break;

      case VSS_ICOMMON_CMD_SET_UI_PROPERTY:
        ( void ) cvp_set_ui_property_cmd_processing( packet );
        break;

      case VSS_ICOMMON_CMD_GET_UI_PROPERTY:
        ( void ) cvp_get_ui_property_cmd_processing( packet );
        break;

      case VSS_ISSR_CMD_CLEANUP:
        cvp_queue_pending_packet( &cvp_ssr_pending_ctrl.cmd_q, packet );
        break;

      default:
        if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header ) ==
             APRV2_PKT_MSGTYPE_SEQCMD_V )
        {
          /* Sequential command. Put it on the pending command queue. */
          cvp_queue_pending_packet( &cvp_low_task_pending_ctrl.cmd_q, packet );
          break;
        }
        else if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header ) ==
                  APRV2_PKT_MSGTYPE_NSEQCMD_V )
        { /* Unsupported non-sequential command. Ack back with unsupported. */
          client_addr = packet->src_addr;

          rc = __aprv2_cmd_end_command( cvp_apr_handle, packet,
                                        APR_EUNSUPPORTED );
          CVP_COMM_ERROR( rc, client_addr );
        }
        else
        {
          /* Unsupported event. Drop it. */
          rc = __aprv2_cmd_free( cvp_apr_handle, packet );
          CVP_PANIC_ON_ERROR( rc );
        }
        rc = APR_EOK;
        break;
    }
  }
}

static void cvp_low_task_process_gating_commands ( void )
{
  int32_t rc;
  cvp_pending_control_t* ctrl = &cvp_low_task_pending_ctrl;
  cvp_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case CVP_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );
        }
        ctrl->state = CVP_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case CVP_PENDING_CMD_STATE_ENUM_EXECUTE:
    case CVP_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_IVOCPROC_CMD_CREATE_FULL_CONTROL_SESSION:
          rc = cvp_create_full_session_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_CREATE_FULL_CONTROL_SESSION_V2:
          rc = cvp_create_full_session_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_CREATE_FULL_CONTROL_SESSION_V3:
          rc = cvp_create_full_session_v3_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_TOPOLOGY_COMMIT:
          rc = cvp_topology_commit_cmd_ctrl( ctrl );
          break;

        /* TODO: case VSS_IVOCRPOC_CMD_CREATE_PASSIVE_CONTROL_SESSION:
          rc = cvp_create_passive_session_cmd_control( ctrl );
          break; */

        case APRV2_IBASIC_CMD_DESTROY_SESSION:
          rc = cvp_destroy_session_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_ENABLE:
          rc = cvp_enable_cmd_ctrl( ctrl, FALSE );
          break;

        case VSS_IVOCPROC_CMD_MVM_ENABLE:
          rc = cvp_enable_cmd_ctrl( ctrl, TRUE );
          break;

        case VSS_IVOCPROC_CMD_DISABLE:
          rc = cvp_disable_cmd_ctrl( ctrl, FALSE );
          break;

        case VSS_IVOCPROC_CMD_MVM_DISABLE:
          rc = cvp_disable_cmd_ctrl( ctrl, TRUE );
          break;

        case VSS_IVOCPROC_CMD_SET_DEVICE:
          rc = cvp_set_device_cmd_ctrl( ctrl );
          break;

        case VSS_IVOLUME_CMD_SET_NUMBER_OF_STEPS:
          rc = cvp_set_number_of_volume_steps_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_DEVICE_V2:
          rc = cvp_set_device_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_DEVICE_V3:
          rc = cvp_set_device_v3_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_ATTACH_STREAM:
          rc = cvp_attach_stream_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_DETACH_STREAM:
          rc = cvp_detach_stream_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_RX_VOLUME_INDEX:
          rc = cvp_set_rx_vol_index_cmd_ctrl( ctrl );
          break;

        case VSS_IVOLUME_CMD_SET_STEP:
          rc = cvp_set_volume_step_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_MUTE:/* Deprecated.*/
          rc = cvp_set_mute_cmd_ctrl( ctrl );
          break;

        case VSS_IVOLUME_CMD_MUTE_V2:
          rc = cvp_volume_mute_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVPCM_CMD_START_V2:
          rc = cvp_vpcm_start_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVPCM_CMD_STOP:
          rc = cvp_vpcm_stop_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_EVAL_CAL_INDEXING_MEM_SIZE:
          rc = cvp_eval_cal_indexing_mem_size_cmd_ctrl( ctrl );
          break;

        /* BACKWARD COMPATIBILITY */
        case VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V2:
          rc = cvp_register_calibration_data_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REGISTER_CALIBRATION_DATA_V3:
          rc = cvp_register_calibration_data_v3_cmd_ctrl( ctrl );
          break;

        /* BACKWARD COMPATIBILITY */
        case VSS_IVOCPROC_CMD_DEREGISTER_CALIBRATION_DATA:
          rc = cvp_deregister_calibration_data_cmd_ctrl( ctrl );
          break;

        /* BACKWARD COMPATIBILITY */
        case VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA:
          rc = cvp_register_volume_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REGISTER_VOLUME_CALIBRATION_DATA_V2:
          rc = cvp_register_volume_calibration_data_v2_cmd_ctrl( ctrl );
          break;

        /* BACKWARD COMPATIBILITY */
        case VSS_IVOCPROC_CMD_DEREGISTER_VOLUME_CALIBRATION_DATA:
          rc = cvp_deregister_volume_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REGISTER_DEVICE_CONFIG:
          rc = cvp_register_device_config_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_DEREGISTER_DEVICE_CONFIG:
          rc = cvp_deregister_device_config_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REGISTER_STATIC_CALIBRATION_DATA:
          rc = cvp_register_static_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_DEREGISTER_STATIC_CALIBRATION_DATA:
          rc = cvp_deregister_static_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REGISTER_DYNAMIC_CALIBRATION_DATA:
          rc = cvp_register_dynamic_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_DEREGISTER_DYNAMIC_CALIBRATION_DATA:
          rc = cvp_deregister_dynamic_calibration_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_SAMPLING_RATE:
          rc = cvp_set_sampling_rate_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_SYSTEM_CONFIG:
          rc = cvp_set_system_config_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_DYNAMIC_SYSTEM_CONFIG:
          rc = cvp_set_dynamic_system_config_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_SET_PARAM_V2:
          rc = cvp_set_param_v2_cmd_ctrl( ctrl );
          break;

        case VSS_ICOMMON_CMD_GET_PARAM_V2:
          rc = cvp_get_param_v2_cmd_ctrl( ctrl );
          break;

        case VSS_IVP3_CMD_GET_DATA:
          rc = cvp_vp3_get_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVP3_CMD_SET_DATA:
          rc = cvp_vp3_set_data_cmd_ctrl( ctrl );
          break;

        case VSS_IVP3_CMD_GET_SIZE:
          rc = cvp_vp3_get_size_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_REINIT:
          rc = cvp_reinit_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_MVM_ATTACH:
          rc = cvp_mvm_attach_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_MVM_DETACH:
          rc = cvp_mvm_detach_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_SET_VOICE_TIMING:
          rc = cvp_set_voice_timing_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_GET_AVSYNC_DELAYS:
          rc = cvp_get_avsync_delays_cmd_ctrl( ctrl );
          break;

        case VSS_IHDVOICE_CMD_GET_CONFIG:
          rc = cvp_get_hdvoice_config_cmd_ctrl( ctrl );
          break;

        case VSS_ISOUNDFOCUS_CMD_SET_SECTORS:
          rc = cvp_soundfocus_set_sectors_cmd_ctrl( ctrl );
          break;

        case VSS_ISOUNDFOCUS_CMD_GET_SECTORS:
          rc = cvp_soundfocus_get_sectors_cmd_ctrl( ctrl );
          break;

        case VSS_ISOURCETRACK_CMD_GET_ACTIVITY:
          rc = cvp_sourcetrack_get_activity_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_TOPOLOGY_SET_DEV_CHANNELS:
          rc = cvp_set_dev_channels_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_TOPOLOGY_REGISTER_PARAMS:
          rc = cvp_register_topology_params_cmd_ctrl( ctrl );
          break;

        case VSS_IVOCPROC_CMD_TOPOLOGY_DEREGISTER_PARAMS:
          rc = cvp_deregister_topology_params_cmd_ctrl( ctrl );
          break;

        default:
          { /* Usupported sequential command. Ack back with unsupported. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            CVP_COMM_ERROR( rc, client_addr );
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = CVP_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = CVP_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

static void cvp_thread_process_ssr_gating_commands ( void )
{
  int32_t rc;
  cvp_pending_control_t* ctrl = &cvp_ssr_pending_ctrl;
  cvp_work_item_t* work_item;
  uint16_t client_addr;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
    case CVP_PENDING_CMD_STATE_ENUM_FETCH:
      {
        { /* Fetch the next pending command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc )
          { /* Return when the pending command queue is empty. */
            return;
          }
          ctrl->packet = work_item->packet;
          ( void ) apr_list_add_tail( &cvp_free_cmd_q, &work_item->link );
        }
        ctrl->state = CVP_PENDING_CMD_STATE_ENUM_EXECUTE;
      }
      break;

    case CVP_PENDING_CMD_STATE_ENUM_EXECUTE:
    case CVP_PENDING_CMD_STATE_ENUM_CONTINUE:
      {
        switch ( ctrl->packet->opcode )
        {
        case VSS_ISSR_CMD_CLEANUP:
          rc = cvp_ssr_cleanup_cmd_ctrl( ctrl );
          break;

        default:
          { /* Usupported sequential command. Ack back with unsupported. */
            client_addr = ctrl->packet->src_addr;

            rc = __aprv2_cmd_end_command( cvp_apr_handle, ctrl->packet, APR_EUNSUPPORTED );
            CVP_COMM_ERROR( rc, client_addr );
          }
          break;
        }

        /* Evaluate the pending command completion status. */
        switch ( rc )
        {
        case APR_EOK:
          /* The current command is finished so fetch the next command. */
          ctrl->state = CVP_PENDING_CMD_STATE_ENUM_FETCH;
          break;

        case APR_EPENDING:
          /* Assuming the current pending command control routine returns
           * APR_EPENDING the overall progress stalls until one or more
           * external events or responses are received.
           */
          ctrl->state = CVP_PENDING_CMD_STATE_ENUM_CONTINUE;
          return;

        default:
          CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
          break;
        }
      }
      break;

    default:
      CVP_PANIC_ON_ERROR( APR_EUNEXPECTED );
      return;
    }
  }
}

/****************************************************************************
 * TASK ROUTINES                                                            *
 ****************************************************************************/

static int32_t cvp_run_high_task ( void )
{
  cvp_high_task_process_nongating_commands( );
  cvp_high_task_process_gating_commands( );

  return APR_EOK;
}

static int32_t cvp_high_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvp_high_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_high_task_state = CVP_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvp_thread_event );

  do
  {
    rc = apr_event_wait( cvp_high_task_event );
   ( void ) cvp_run_high_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvp_high_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_high_task_state = CVP_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t cvp_run_med_task ( void )
{
  cvp_med_task_process_nongating_commands( );
  cvp_med_task_process_gating_commands( );

  return APR_EOK;
}

static int32_t cvp_med_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvp_med_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_med_task_state = CVP_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvp_thread_event );

  do
  {
    rc = apr_event_wait( cvp_med_task_event );
   ( void ) cvp_run_med_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvp_med_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_med_task_state = CVP_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static int32_t cvp_run_low_task ( void )
{
  cvp_low_task_process_nongating_commands( );
  cvp_low_task_process_gating_commands( );
  cvp_thread_process_ssr_gating_commands( );

  return APR_EOK;
}

static int32_t cvp_low_task ( void* param )
{
  int32_t rc;

  rc = apr_event_create( &cvp_low_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_low_task_state = CVP_THREAD_STATE_ENUM_READY;
  apr_event_signal( cvp_thread_event );

  do
  {
    rc = apr_event_wait( cvp_low_task_event );
   ( void ) cvp_run_low_task( );
  }
  while ( rc == APR_EOK );

  rc = apr_event_destroy( cvp_low_task_event );
  CVP_PANIC_ON_ERROR( rc );

  cvp_low_task_state = CVP_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}

static uint32_t cvp_run ( void )
{
  cvp_run_high_task( );
  cvp_run_med_task( );
  cvp_run_low_task( );

  return APR_EOK;
}

/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static int32_t cvp_init ( void )
{
  int32_t rc;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvp_init()======" );

  { /* Initialize the locks. */
    ( void ) apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &cvp_int_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvp_med_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvp_low_task_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvp_ref_cnt_lock );
  }
  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &cvp_heapmgr, ( ( void* ) &cvp_heap_pool ),
                          sizeof( cvp_heap_pool ), NULL, NULL );
      /* memheap mustn't be called from interrupt context. No locking is
       * required in task context because all commands are serialized.
       */
  }
  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = cvp_object_table;
    params.total_bits = CVP_HANDLE_TOTAL_BITS_V;
    params.index_bits = CVP_HANDLE_INDEX_BITS_V;
    params.lock_fn = cvp_int_lock_fn;
    params.unlock_fn = cvp_int_unlock_fn;
    ( void ) apr_objmgr_construct( &cvp_objmgr, &params );
      /* TODO: When the ISR and task context contentions becomes a problem
       *       the objmgr could be split into ISR/task and task-only stores
       *       to reduce the bottleneck.
       */
  }
  { /* Initialize the session management. */
    ( void ) cvp_pending_control_init( &cvp_high_task_pending_ctrl );
    ( void ) cvp_pending_control_init( &cvp_med_task_pending_ctrl );
    ( void ) cvp_pending_control_init( &cvp_low_task_pending_ctrl );
  }
  { /* Initialize the SSR management. */
    ( void ) cvp_pending_control_init( &cvp_ssr_pending_ctrl );

    cvp_ssr_cleanup_cmd_tracking.num_cmd_issued = 0;
    cvp_ssr_cleanup_cmd_tracking.rsp_cnt = 0;
  }
  { /* Initialize the command queue management. */
    uint32_t i;

    { /* Populate the free command structures. */
      ( void ) apr_list_init_v2( &cvp_free_cmd_q,
                                 cvp_int_lock_fn, cvp_int_unlock_fn );
      for ( i = 0; i < CVP_NUM_COMMANDS_V; ++i )
      {
        ( void ) apr_list_init_node( ( ( apr_list_node_t* ) &cvp_cmd_pool[ i ] ) );
        ( void ) apr_list_add_tail( &cvp_free_cmd_q,
                                    ( ( apr_list_node_t* ) &cvp_cmd_pool[ i ] ) );
      }
    }

    ( void ) apr_list_init_v2( &cvp_high_task_incoming_cmd_q,
                               cvp_int_lock_fn, cvp_int_unlock_fn );

    ( void ) apr_list_init_v2( &cvp_med_task_incoming_cmd_q,
                               cvp_med_task_lock_fn, cvp_med_task_unlock_fn );

    ( void ) apr_list_init_v2( &cvp_low_task_incoming_cmd_q,
                               cvp_low_task_lock_fn, cvp_low_task_unlock_fn );
  }
  { /* Initialize the global VP3 management. */
    uint32_t i;

    { /* Allocate global VP3 heap. */
      cvp_global_vp3_heap = apr_memmgr_malloc(
                              &cvp_heapmgr,
                              ( sizeof( cvp_per_device_pair_vp3_t ) *
                                CVP_MAX_NUM_DEVICE_PAIRS_OF_VP3_STORED ) );

      if ( cvp_global_vp3_heap == NULL )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvp_init(): Failed to allocate cvp_global_vp3_heap" );
        rc = APR_ENORESOURCE;
        CVP_PANIC_ON_ERROR( rc );
      }

      ( void )mmstd_memset( cvp_global_vp3_heap, 0,
                            ( sizeof( cvp_per_device_pair_vp3_t ) *
                            CVP_MAX_NUM_DEVICE_PAIRS_OF_VP3_STORED ) );
    }

    { /* Populate the free VP3 tracking structures. */
      ( void ) apr_list_init_v2( &cvp_global_vp3_list_free_q, NULL, NULL );
      for ( i = 0; i < CVP_MAX_NUM_DEVICE_PAIRS_OF_VP3_STORED; ++i )
      {
        cvp_global_vp3_list_item_pool[ i ].vp3 = ( &cvp_global_vp3_heap[ i ] );

        ( void ) apr_list_init_node( ( apr_list_node_t* ) &cvp_global_vp3_list_item_pool[ i ] );
        ( void ) apr_list_add_tail( &cvp_global_vp3_list_free_q,
                                    ( ( apr_list_node_t* ) &cvp_global_vp3_list_item_pool[ i ] ) );
      }
    }

    ( void ) apr_list_init_v2( &cvp_global_vp3_list_used_q, NULL, NULL );
  }
  { /* Create the CVP task. */
     rc = apr_event_create( &cvp_thread_event );
     CVP_PANIC_ON_ERROR( rc );

    ( void ) apr_thread_create( &cvp_high_task_handle, CVP_HIGH_TASK_NAME,
                                CVP_HIGH_TASK_PRIORITY, cvp_high_task_stack, CVP_HIGH_TASK_STACK_SIZE,
                                cvp_high_task, NULL );

    /* Wait for service thread to be created. */
    ( void ) apr_event_wait( cvp_thread_event );

    ( void ) apr_thread_create( &cvp_med_task_handle, CVP_MED_TASK_NAME,
                                CVP_MED_TASK_PRIORITY, cvp_med_task_stack, CVP_MED_TASK_STACK_SIZE,
                                cvp_med_task, NULL );

    /* Wait for service thread to be created. */
    ( void ) apr_event_wait( cvp_thread_event );

    ( void ) apr_thread_create( &cvp_low_task_handle, CVP_LOW_TASK_NAME,
                                CVP_LOW_TASK_PRIORITY, cvp_low_task_stack, CVP_LOW_TASK_STACK_SIZE,
                                cvp_low_task, NULL );

    /* Wait for service thread to be created. */
    ( void ) apr_event_wait( cvp_thread_event );

    rc = apr_event_destroy( cvp_thread_event );
    CVP_PANIC_ON_ERROR( rc );
  }
  { /* Clean up main CVP info structure for session tracking. */
    uint32_t i;

    cvp_info.session_cnt = 0;

  for( i = 0; i < CVP_MAX_NUM_SESSIONS; ++i )
  {
    cvp_info.sessions[ i ] = ( ( cvp_session_object_t*) CVP_SESSION_INVALID );
  }
  }
  { /* Initialize the APR resource. */
    rc = __aprv2_cmd_register2(
           &cvp_apr_handle, cvp_my_dns, sizeof( cvp_my_dns ), 0,
           cvp_isr_dispatch_fn, NULL, &cvp_my_addr );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvp_init(): registered with APR rc=0x%08X",
                                           rc );
  }

  ( void ) vccm_mmpm_register( VCCM_CLIENT_ID_CVP );

  return APR_EOK;
}

static int32_t cvp_post_init ( void )
{
  { /* Perform DNS look-ups now after services have registered in init. */
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvp_cvs_dns, sizeof( cvp_cvs_dns ), &cvp_cvs_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvp_vpm_dns, sizeof( cvp_vpm_dns ), &cvp_vpm_addr );
    ( void ) __aprv2_cmd_local_dns_lookup(
               cvp_mvm_dns, sizeof( cvp_mvm_dns ), &cvp_mvm_addr );
  }

  cvp_initialized = TRUE;

  return APR_EOK;
}

static int32_t cvp_pre_deinit ( void )
{
  return APR_EOK;
}

static int32_t cvp_deinit ( void )
{
  ( void ) vccm_mmpm_deregister( VCCM_CLIENT_ID_CVP );

  /* TODO: Ensure no race conditions on deregister. */

  cvp_initialized = FALSE;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvp_deinit()======" );

  { /* Release the APR resource. */
    ( void ) __aprv2_cmd_deregister( cvp_apr_handle );
  }
  { /* Destroy the CVP task. */
    { /* Destroy the CVP task. */
      ( void ) apr_event_signal_abortall( cvp_low_task_event );

      while ( cvp_low_task_state != CVP_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvp_low_task_handle );
    }
    { /* Destroy the CVP task. */
      ( void ) apr_event_signal_abortall( cvp_med_task_event );

      while ( cvp_med_task_state != CVP_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvp_med_task_handle );
    }
    { /* Destroy the CVP task. */
      ( void ) apr_event_signal_abortall( cvp_high_task_event );

      while ( cvp_high_task_state != CVP_THREAD_STATE_ENUM_EXIT )
      {
        ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
      }

      ( void ) apr_thread_destroy( cvp_high_task_handle );
    }
  }
  { /* Release the VP3 management. */
    ( void ) apr_list_destroy( &cvp_global_vp3_list_used_q );
    ( void ) apr_list_destroy( &cvp_global_vp3_list_free_q );
    apr_memmgr_free( &cvp_heapmgr, cvp_global_vp3_heap );
  }
  { /* Release the command queue management. */
    ( void ) apr_list_destroy( &cvp_low_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvp_med_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvp_high_task_incoming_cmd_q );
    ( void ) apr_list_destroy( &cvp_free_cmd_q );
  }
  { /* Release the SSR management. */
    ( void ) cvp_pending_control_destroy( &cvp_ssr_pending_ctrl );
  }
  { /* Release the session management. */
    ( void ) cvp_pending_control_destroy( &cvp_low_task_pending_ctrl );
    ( void ) cvp_pending_control_destroy( &cvp_med_task_pending_ctrl );
    ( void ) cvp_pending_control_destroy( &cvp_high_task_pending_ctrl );
  }
  { /* Release the object management. */
    ( void ) apr_objmgr_destruct( &cvp_objmgr );
  }
  { /* Release the locks. */
    ( void ) apr_lock_destroy( cvp_med_task_lock );
    ( void ) apr_lock_destroy( cvp_low_task_lock );
    ( void ) apr_lock_destroy( cvp_ref_cnt_lock );
    ( void ) apr_lock_destroy( cvp_int_lock );
  }

  { /* Print out debug info. on object sizes. */
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "apr_objmgr_object_t size = %d",
                                          sizeof( apr_objmgr_object_t ) );
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, "cvp_object_t size = %d",
                                          sizeof( cvp_object_t ) );
  }

  return APR_EOK;
}

APR_EXTERNAL int32_t cvp_call (
  cvp_callindex_enum_t index,
  void* params,
  uint32_t size
)
{
  int32_t rc;

  switch ( index )
  {
  case CVP_CALLINDEX_ENUM_INIT:
    rc = cvp_init( );
    break;

  case CVP_CALLINDEX_ENUM_POSTINIT:
    rc = cvp_post_init( );
    break;

  case CVP_CALLINDEX_ENUM_PREDEINIT:
    rc = cvp_pre_deinit( );
    break;

  case CVP_CALLINDEX_ENUM_DEINIT:
    rc = cvp_deinit( );
    break;

  case CVP_CALLINDEX_ENUM_RUN:
    rc = cvp_run( );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unsupported callindex (%d)",
                                            index );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

