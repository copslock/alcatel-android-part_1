#===============================================================================
#
# MULTISHAKE Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------
Import('env')
from glob import glob
from os.path import join, basename
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/Sensors/common/usam/algo/multishake/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends within module
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# External depends outside module
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Internal depends within module
#-------------------------------------------------------------------------------
env.Append(CPPPATH = [
   "${BUILD_ROOT}/Sensors/api",
   "${BUILD_ROOT}/Sensors/common/inc",
   "${BUILD_ROOT}/Sensors/common/util/mathtools/inc",
   "${BUILD_ROOT}/Sensors/common/usam/framework/inc",
   "${BUILD_ROOT}/Sensors/common/usam/util/inc",
   "${BUILD_ROOT}/Sensors/common/usam/algo/basic_gestures/inc",
   "${BUILD_ROOT}/Sensors/common/usam/algo/multishake/inc",
   "${BUILD_ROOT}/Sensors/common/util/queue/inc",
   "${BUILD_ROOT}/Sensors/common/smr/inc",
   "${BUILD_ROOT}/Sensors/common/util/memmgr/inc",
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
MULTISHAKE_SOURCES  = ['${BUILDPATH}/' + basename(fname)
          for fname in glob(join(env.subst(SRCPATH), '*.c'))]

algo_multishake_lib = env.AddBinaryLibrary(['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
                                    '${BUILDPATH}/sam_algo_multishake', MULTISHAKE_SOURCES)

# Sources  and headers that should not be shared
MULTISHAKE_CLEAN_SOURCES = env.FindFiles(['*.c'], '${BUILD_ROOT}/Sensors/common/usam/algo/multishake/src')
MULTISHAKE_CLEAN_SOURCES += env.FindFiles(['*.h'], '${BUILD_ROOT}/Sensors/common/usam/algo/multishake/inc')

# Clean sources
env.CleanPack(['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'], MULTISHAKE_CLEAN_SOURCES)
