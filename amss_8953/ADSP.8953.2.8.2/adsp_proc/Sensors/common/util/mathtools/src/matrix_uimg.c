/*============================================================================
  @file matrix.c

  Matrix utility source file

  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
============================================================================*/

#include "matrix.h"
#include "fixed_point.h"

/*===========================================================================

  FUNCTION: matrix_zero

  ===========================================================================*/
/*!
  @brief Sets a matrix's values to zero

  @param[io] m: The matrix

  @return
  None.

  @note
*/
/*=========================================================================*/
void matrix_zero(matrix_type *m)
{
  int32_t i, j;

  for (i=0; i<m->rows; i++)
  {
    for (j=0; j<m->cols; j++)
    {
      MATRIX_ELEM(m, i, j) = 0;
    }
  }
}
