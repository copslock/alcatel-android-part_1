/*==============================================================================
  @file sns_dl_reg_cmd.c

  @brief
  Implementation of the SNS Dynamic Loading service (DL svc) commands such as
  iterate and search required for reading the list of boot critical shared
  object based SNS DDF based drivers and SAM based SNS algorithms from
  the registry. 

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
#include <stringl.h>
#include <string.h>
#include <stdbool.h>

#include "sns_reg_api_v02.h"
#include "sns_dl_priv.h"

//------------------------------------------------------------------------------
// DATA DEFINITIONS
//------------------------------------------------------------------------------

#define SNS_DL_REG_CMD_LOCK(p_reg_cmd)    SNS_DL_LOCK((p_reg_cmd)->mutex)
#define SNS_DL_REG_CMD_UNLOCK(p_reg_cmd)  SNS_DL_UNLOCK((p_reg_cmd)->mutex)

/**
  @note It is expected that these are defined in the scons/makefile/build
  system. In case they are not we define *reasonable* values.
  These were chosen because the number of registry entries are expected 
  to be a multiple of 100.
  Additionally at the time of implementing this code, the numbers below 
  were an acceptable batching request number to set.
*/
#ifndef FEATURE_SNS_DL_READ_BATCH_LARGE_NUM
  #define FEATURE_SNS_DL_READ_BATCH_LARGE_NUM 20
#endif //FEATURE_SNS_DL_READ_BATCH_LARGE_NUM

#ifndef FEATURE_SNS_DL_READ_BATCH_SMALL_NUM
  #define FEATURE_SNS_DL_READ_BATCH_SMALL_NUM 5
#endif //FEATURE_SNS_DL_READ_BATCH_SMALL_NUM

#define FEATURE_SNS_DL_READ_BATCH_MIN_NUM     1

#define SNS_DL_SIG_REG_READ_BATCH             0x01
#define SNS_DL_SIG_REG_TIMEOUT                0x02
#define SNS_DL_SIG_REG_ERROR                  0x04
#define SNS_DL_SIG_REG_FORCE_EXIT             0x08
#define SNS_DL_SIG_REG_ALL                    (SNS_DL_SIG_REG_READ_BATCH | \
                                                SNS_DL_SIG_REG_TIMEOUT   | \
                                                SNS_DL_SIG_REG_ERROR     | \
                                                SNS_DL_SIG_REG_FORCE_EXIT)

typedef enum
{
  SNS_DL_PROCESS_REG_RESP_FIRST,
  SNS_DL_PROCESS_REG_RESP_ALL
} sns_dl_for_each_op_t;

//------------------------------------------------------------------------------
// DATA DECLARATIONS
//------------------------------------------------------------------------------
/**
  This array contains the step decrements in which batching of requests can
  be sent to the SNS registry service.
  
  For example if there were 108 registry entries and the batch sizes were
  20, 5 and 1. We would see the following:
    108/20 = 5 iterations of a batch of 20 messages totalling 100 followed by
    8/5    = 1 iteration of a batch of 5 totalling 105 messages followed by
    3/1    = 3 iterations of 1 message each totalling 108 messages

  This as can be tuned at will with the idea keeping in mind the registry size
  that needs to be covered for reading.
  
  @note The last element in the array has to be 1 always.
*/
static const uint32_t read_batch_sizes[] =
{ 
  FEATURE_SNS_DL_READ_BATCH_LARGE_NUM,
  FEATURE_SNS_DL_READ_BATCH_SMALL_NUM,

  /* this must *always* be the last line in this array */
  FEATURE_SNS_DL_READ_BATCH_MIN_NUM
};
static const int num_batch_size_elements = (sizeof(read_batch_sizes) / 
                                              sizeof(read_batch_sizes[0]));

//------------------------------------------------------------------------------
// FORWARD DECLARATIONS
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// IMPLEMENTATION 
//------------------------------------------------------------------------------

/**
  @brief If the user specified a timeout, this call is issued it is likely 
  the command has not yet completed and we terminate the command and notify the 
  user.
*/
STATIC
void sns_dl_reg_cmd_timeout(void *timeout_param)
{
  uint8_t os_err;
  bool is_valid_reg_cmd;
  sns_dl_svc_t *p_svc;
  sns_dl_reg_cmd_t *p_reg_cmd = (sns_dl_reg_cmd_t*)timeout_param;
  /* first ensure that the command is still valid */
  is_valid_reg_cmd = sns_dl_svc_validate_reg_cmd_and_lock_svc(p_reg_cmd);
  if (is_valid_reg_cmd) {
    SNS_DL_REG_CMD_LOCK(p_reg_cmd);
    p_svc = p_reg_cmd->p_svc;
    /* unblock the caller's thread and notify them of a timeout */
    sns_os_sigs_post(p_reg_cmd->sig, SNS_DL_SIG_REG_TIMEOUT,
                      OS_FLAG_SET, &os_err);
    SNS_ASSERT(OS_ERR_NONE == os_err);
    SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
    sns_dl_svc_reg_cmd_unlock_svc(p_svc);
    SNS_DL_PRINTF(FATAL, "DLSVC Timeout Seen! 0x%p %d",
                          p_reg_cmd, is_valid_reg_cmd);
  }
}

/**
  @brief In an error situation, we attempt to wakeup the blocked caller and
  notify them of an error.
*/
void sns_dl_reg_cmd_handle_exit(sns_dl_reg_cmd_t *p_reg_cmd)
{
  bool is_valid_reg_cmd;
  sns_dl_svc_t *p_svc;

  /* first ensure that the command is still valid */
  is_valid_reg_cmd = sns_dl_svc_validate_reg_cmd_and_lock_svc(p_reg_cmd);
  if (is_valid_reg_cmd) {
    p_svc = p_reg_cmd->p_svc;
    /* force the caller to exit */
    sns_dl_reg_cmd_force_exit(p_reg_cmd);
    sns_dl_svc_reg_cmd_unlock_svc(p_svc);
  }
  SNS_DL_PRINTF(FATAL, "DLSVC Exit Request Seen! 0x%p Valid:%d",
                         p_reg_cmd, is_valid_reg_cmd);
}

/**
  @brief Implementation to unblock the caller thread and notify them of an
  error.
*/
void sns_dl_reg_cmd_force_exit(sns_dl_reg_cmd_t *p_reg_cmd)
{
  uint8_t os_err;
  SNS_DL_REG_CMD_LOCK(p_reg_cmd);
  sns_os_sigs_post(p_reg_cmd->sig, SNS_DL_SIG_REG_FORCE_EXIT,
                    OS_FLAG_SET, &os_err);
  SNS_ASSERT(OS_ERR_NONE == os_err);
  SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);  
}

/**
  @brief Implementation to create and initialize a DL registry command to
  either iterate or search the registry for dynamic loading config data.
  
  @note To be used in conjunction with sns_dl_destroy_reg_cmd.
*/
STATIC
sns_dl_reg_cmd_t* sns_dl_create_reg_cmd
(
  sns_dl_svc_t    *p_svc,
  uint32_t        group_id_start,
  uint32_t        group_id_stop,
  uint32_t        timeout_us,
  sns_dl_reg_cmd_type_t cmd_type,
  pfn_sns_dl_reg_cmd pfn,
  void            *pfn_cb_data,
  pfn_sns_dl_reg_cmd_resp pfn_reg_resp,
  void            *srch_data,
  uint32_t        srch_data_len,
  uint8_t         srch_position,
  bool            timing_profile_flag,
  sns_err_code_e  *p_err_code
)
{
  sns_err_code_e err_code = SNS_ERR_BAD_PARM;
  uint8_t os_err;
  sns_dl_reg_cmd_t *p_reg_cmd = NULL;
  bool mutex_created = false, sig_created = false, timer_created = false;
  uint32_t timeout_ticks;
  
  if (p_svc) {
    err_code = SNS_ERR_NOMEM;
    p_reg_cmd = p_svc->pfn_malloc(sizeof(sns_dl_reg_cmd_t));
    if (p_reg_cmd) {
      p_reg_cmd->sig = sns_os_sigs_create((OS_FLAGS)SNS_DL_SIG_REG_ALL, &os_err);
      if ((OS_ERR_NONE != os_err) || (NULL == p_reg_cmd->sig)) {
        err_code = SNS_ERR_FAILED;
        SNS_DL_PRINTF(ERROR, "create_reg_cmd Sig Create Failed %#x", os_err);
        goto bail;
      }
      sig_created = true;
      p_reg_cmd->mutex = NULL;
      p_reg_cmd->mutex = sns_os_mutex_create(SNS_DL_MODULE_MTX_PRIO, &os_err);
      if ((OS_ERR_NONE != os_err) || (NULL == p_reg_cmd->mutex)) {
        err_code = SNS_ERR_FAILED;
        SNS_DL_PRINTF(ERROR, "create_reg_cmd Mutex Create Failed %#x", os_err);
        goto bail;
      }
      mutex_created = true;
      err_code = sns_em_create_timer_obj(sns_dl_reg_cmd_timeout,
                                        p_reg_cmd,
                                        SNS_EM_TIMER_TYPE_ONESHOT,
                                        &(p_reg_cmd->timeout_timer));
      if (SNS_SUCCESS != err_code) {
        SNS_DL_PRINTF(ERROR, "create_reg_cmd Timer Create Failed %#x", err_code);
        goto bail;
      }
      timer_created = true;
      if (timeout_us > 0) {
        timeout_ticks = sns_em_convert_usec_to_localtick(timeout_us);
        err_code = sns_em_register_timer(p_reg_cmd->timeout_timer, timeout_ticks);
        if (SNS_SUCCESS != err_code) {
          SNS_DL_PRINTF(ERROR, "create_reg_cmd Timer Register Fail %#x",err_code);
          goto bail;
        }
      }
      p_reg_cmd->group_id_start = group_id_start;
      p_reg_cmd->group_id_stop  = group_id_stop;
      p_reg_cmd->timeout_us     = timeout_us;
      p_reg_cmd->cmd_type       = cmd_type;
      p_reg_cmd->pfn            = pfn;
      p_reg_cmd->pfn_cb_data    = pfn_cb_data;
      p_reg_cmd->pfn_reg_cmd_resp  = pfn_reg_resp;
      p_reg_cmd->srch_data      = srch_data;
      p_reg_cmd->srch_data_len  = srch_data_len;
      p_reg_cmd->srch_pos       = srch_position;
      p_reg_cmd->timing_profile_flag = timing_profile_flag;
      p_reg_cmd->p_svc          = p_svc;
      sns_q_init(&(p_reg_cmd->reg_response_q));
      sns_dl_svc_enqueue_command(p_svc, p_reg_cmd);
      err_code = SNS_SUCCESS;
    }
  }
bail:
  if (SNS_SUCCESS != err_code) {
    if (p_reg_cmd) {
      if (timer_created) {
        sns_em_delete_timer_obj(p_reg_cmd->timeout_timer);
      }
      if (mutex_created) {
        sns_os_mutex_del(p_reg_cmd->mutex, 0, &os_err);
      }
      if (sig_created) {
        sns_os_sigs_destroy(p_reg_cmd->sig, &os_err);
      }
      p_svc->pfn_free(p_reg_cmd);
      p_reg_cmd = NULL;
    }
  }
  *p_err_code = err_code;
  return p_reg_cmd;
}

/**
  @brief Helper to invoke the DL reg command
*/
STATIC
sns_err_code_e execute_reg_cmd(sns_dl_reg_cmd_t* p_reg_cmd)
{ 
  return p_reg_cmd->pfn(p_reg_cmd, p_reg_cmd->pfn_cb_data);
}

/**
  @brief Implementation to create and initialize a DL registry command to
  either iterate or search the registry for dynamic loading config data.
  
  @note To be used in conjunction with sns_dl_create_reg_cmd.
*/
STATIC
void sns_dl_destroy_reg_cmd(sns_dl_reg_cmd_t *p_reg_cmd)
{
  uint8_t os_err;
  sns_dl_reg_cmd_resp_t *p_resp;
  pfn_sns_dl_free pfn_free = p_reg_cmd->p_svc->pfn_free;
  SNS_DL_REG_CMD_LOCK(p_reg_cmd);
  while (sns_q_cnt(&(p_reg_cmd->reg_response_q)) > 0) {
    p_resp = (sns_dl_reg_cmd_resp_t*)sns_q_get(&(p_reg_cmd->reg_response_q));
    if (p_resp) {
      FREEIFPFN(pfn_free, p_resp->ptr_to_free);
      pfn_free(p_resp);
    }
  }
  SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
  sns_em_cancel_timer(p_reg_cmd->timeout_timer);
  sns_em_delete_timer_obj(p_reg_cmd->timeout_timer);
  sns_os_sigs_destroy(p_reg_cmd->sig, &os_err);
  sns_os_mutex_del(p_reg_cmd->mutex, 0, &os_err);
  sns_dl_svc_dequeue_command(p_reg_cmd->p_svc, p_reg_cmd);
  pfn_free(p_reg_cmd);
}

/**
  @brief Implementation to init member of the reg cmd before processing
  a batch request.
*/
STATIC
void pre_batch_read_init(sns_dl_reg_cmd_t *p_reg_cmd, uint32_t batch_size)
{
  SNS_DL_REG_CMD_LOCK(p_reg_cmd);
  p_reg_cmd->batch_size = batch_size;
  p_reg_cmd->err_responses = 0;
  p_reg_cmd->msg_responses = 0;
  SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
}

void sns_dl_handle_reg_cmd_resp
(
  sns_dl_reg_cmd_t *p_reg_cmd,
  uint32_t  group_id,
  void      *data,
  uint32_t  data_size,
  bool      is_err,
  void      *ptr_to_free,
  bool      *user_callback_to_free_mem
)
{
  bool is_valid_reg_cmd;
  sns_dl_svc_t *p_svc;

  /* by default we dont handle destroying the pointer to free */
  *user_callback_to_free_mem = false;
  /* validate the registry command */
  is_valid_reg_cmd = sns_dl_svc_validate_reg_cmd_and_lock_svc(p_reg_cmd);
  if (is_valid_reg_cmd) {
    SNS_DL_REG_CMD_LOCK(p_reg_cmd);
    p_svc = p_reg_cmd->p_svc;
    if (p_reg_cmd->pfn_reg_cmd_resp) {
      p_reg_cmd->pfn_reg_cmd_resp(p_reg_cmd, group_id, data, data_size, is_err,
                                    ptr_to_free, user_callback_to_free_mem);
    }
    SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
    sns_dl_svc_reg_cmd_unlock_svc(p_svc);
  }
}

/**
  @brief Implementation to read all the responses that arrive as part of
  a batch request.
*/
STATIC
void sns_dl_reg_cmd_read_cb
(
  sns_dl_reg_cmd_t *p_reg_cmd,
  uint32_t  group_id,
  void      *data,
  uint32_t  data_size,
  bool      is_err,
  void      *ptr_to_free,
  bool      *user_callback_to_free_mem
)
{
  uint8_t err;
  sns_dl_reg_cmd_resp_t *p_reg_cmd_resp = NULL;
  sns_dl_svc_t *p_svc;
  uint32_t err_resp = 0, msg_resp = 0, num_responses;
  bool handler_to_free_mem = false;

  p_svc = p_reg_cmd->p_svc;
  /* check for any transport/communication errors */
  if (!is_err) {
    /* this is not an error queue the message */
    p_reg_cmd_resp = p_svc->pfn_malloc(sizeof(sns_dl_reg_cmd_resp_t));
    SNS_ASSERT(NULL != p_reg_cmd_resp);
    p_reg_cmd_resp->reg_data      = data;
    p_reg_cmd_resp->reg_data_size = data_size;
    p_reg_cmd_resp->group_id      = group_id;
    p_reg_cmd_resp->ptr_to_free   = ptr_to_free;
    msg_resp = 1;
  } else {
    err_resp = 1;
  }
  if (p_reg_cmd_resp) {
    sns_q_link(p_reg_cmd_resp, (sns_q_link_s*)p_reg_cmd_resp);
    sns_q_put(&p_reg_cmd->reg_response_q, (sns_q_link_s*)p_reg_cmd_resp);
    handler_to_free_mem = true;
  }
  /*
    accumulate the responses error or valid messages to help determine if
    the batching has completed
  */
  p_reg_cmd->err_responses += err_resp;
  p_reg_cmd->msg_responses += msg_resp;
  num_responses = p_reg_cmd->msg_responses + p_reg_cmd->err_responses;
  /* all messages in the batch received wakeup the caller */
  if (num_responses == p_reg_cmd->batch_size) {
    sns_os_sigs_post(p_reg_cmd->sig, SNS_DL_SIG_REG_READ_BATCH,
                      OS_FLAG_SET, &err);
    SNS_ASSERT(OS_ERR_NONE == err);
  }
  *user_callback_to_free_mem = handler_to_free_mem;
}

/**
  @brief Implementation to dispatch batch read requests to the DL registry.
*/
sns_err_code_e block_batch_read_caller
(
  sns_dl_reg_cmd_t *p_reg_cmd,
  uint32_t          batch_size,
  bool              *terminate_reads
)
{
  sns_err_code_e  ret_val = SNS_SUCCESS;
  uint32_t msg_cnt = 0, tout_cnt = 0, err_cnt = 0, exit_cnt = 0;
  OS_FLAGS sig_flags = 0;
  uint8_t os_err;
  OS_FLAGS sig_mask = SNS_DL_SIG_REG_ALL;
  sns_q_link_s *q_curr;
  bool terminate_reads_flag = false, exit_loop = true;
  sns_dl_reg_cmd_resp_t *p_reg_resp;
  sns_reg_dyn_cfg_base_t *p_cfg_base;
  int cnt;

  for (;;) {
    /* 
      block caller until a timeout error or all messages in a batch
      have been received
    */
    sig_flags = sns_os_sigs_pend( p_reg_cmd->sig, sig_mask,
                                  OS_FLAG_WAIT_SET_ANY + OS_FLAG_CONSUME,
                                  0, &os_err );
    SNS_ASSERT(OS_ERR_NONE == os_err);
    /* a batch of DL registry messages have been completed */
    if (sig_flags & SNS_DL_SIG_REG_READ_BATCH) {
      msg_cnt++;
      /* check if we've received a termination row ie a row with a 0 revision */
      SNS_DL_REG_CMD_LOCK(p_reg_cmd);
      cnt = sns_q_cnt(&(p_reg_cmd->reg_response_q));
      if (cnt > 0) {
        if (!p_reg_cmd->timing_profile_flag) {
          for ( q_curr = sns_q_check(&(p_reg_cmd->reg_response_q));
                NULL != q_curr;
                q_curr = sns_q_next(&(p_reg_cmd->reg_response_q), q_curr) ) {
            p_reg_resp = (sns_dl_reg_cmd_resp_t*)q_curr;
            p_cfg_base = (sns_reg_dyn_cfg_base_t*)p_reg_resp->reg_data;
            if (p_cfg_base) {
              if (0 == p_cfg_base->rev) {
                /* set the indication that we dont need to send more batches */
                terminate_reads_flag = true;
                break;
              }
            }
          }
        }
      } else {
        /* no reads were queued so stop reading more */
        terminate_reads_flag = true;
      }
      SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
    }
    
    if (sig_flags & SNS_DL_SIG_REG_TIMEOUT) {
      /* we've timed out, send the error along */
      tout_cnt++;
      terminate_reads_flag = true;
      ret_val = SNS_ERR_TIMEOUT;
      SNS_DL_PRINTF(ERROR, "block_batch_read_caller Timeout!");
      break;
    }

    if (sig_flags & SNS_DL_SIG_REG_ERROR) {
      /* we've encountered a runtime error, send the error along */
      err_cnt++;
      terminate_reads_flag = true;
      ret_val = SNS_ERR_NOTSUPPORTED;
      SNS_DL_PRINTF(ERROR, "block_batch_read_caller Error Signal Received!");
      break;
    }
    if (sig_flags & SNS_DL_SIG_REG_FORCE_EXIT) {
      exit_cnt++;
      terminate_reads_flag = true;
      ret_val = SNS_ERR_FAILED;
      SNS_DL_PRINTF(ERROR, "block_batch_read_caller Exit Called!");
      break;
    }
    SNS_DL_PRINTF(LOW, "block_batch_read_caller MSG#:%d TO%d ERR:%d Exit:%d",
                            msg_cnt, tout_cnt, err_cnt, exit_cnt);
    if (exit_loop) {
      break;
    }
  }
  SNS_DL_PRINTF(LOW, "block_batch_read_caller reads terminated? %d",
                        terminate_reads_flag);

  /* flag to indicate that no additional reads need to be performed */
  *terminate_reads = terminate_reads_flag;
  return ret_val;
}

STATIC
sns_err_code_e batch_read_handler(sns_dl_reg_cmd_t *p_reg_cmd, void* data)
{
  sns_err_code_e  ret_val = SNS_ERR_NOTFOUND;
  int batch_idx = 0, read_batch_size;
  int total_num_msgs_to_be_sent = 0;
  int num_msgs_to_be_sent, num_msgs_sent = 0, msg_ctr, batch_ctr, num_batches;
  uint32_t stop_id, start_id, grp_id;
  bool terminate_read = false;
  
  start_id  = p_reg_cmd->group_id_start;
  stop_id   = p_reg_cmd->group_id_stop;
  total_num_msgs_to_be_sent = stop_id - start_id + 1;

  if (num_msgs_sent < total_num_msgs_to_be_sent) {
    for (batch_idx = 0; batch_idx < num_batch_size_elements; batch_idx++) {
      if (num_msgs_sent < total_num_msgs_to_be_sent) {
        read_batch_size = read_batch_sizes[batch_idx];
        num_msgs_to_be_sent = stop_id - start_id + 1;
        num_batches = num_msgs_to_be_sent / read_batch_size;
        for (batch_ctr = 0; batch_ctr < num_batches; batch_ctr++) {
          SNS_DL_PRINTF(FATAL,
                        "sns_reg_for_each Batch Start %d Total:%d Num Sent %d",
                        batch_idx, total_num_msgs_to_be_sent, num_msgs_sent);

          pre_batch_read_init(p_reg_cmd, read_batch_size);
          for (msg_ctr = 0; msg_ctr < read_batch_size; msg_ctr++) {
            grp_id = num_msgs_sent + p_reg_cmd->group_id_start;
            /* update the group id to read */
            p_reg_cmd->group_id_single = grp_id;
            ret_val = sns_dl_reg_client_perform_ops(p_reg_cmd);
            if (SNS_SUCCESS != ret_val) {
              SNS_DL_PRINTF(ERROR, "sns_reg_for_each Read Error %#x", ret_val);
              goto bail;
            }
            num_msgs_sent++;
          }
          ret_val = block_batch_read_caller(p_reg_cmd,
                                              read_batch_size,
                                              &terminate_read);
          if (SNS_SUCCESS != ret_val) {
            /* either a timeout or some error has occurred */
            SNS_DL_PRINTF(ERROR, "sns_reg_for_each Block Error %#x", ret_val);
            goto bail;
          }
          if (terminate_read) {
            goto bail;
          }
          start_id += read_batch_size;

          SNS_DL_PRINTF(FATAL, "sns_reg_for_each Batch Stop %d %d",
                                batch_idx, num_msgs_sent);
        }
      }
    }
    /* finished dispatching all read messages */
    ret_val = SNS_SUCCESS;
  }

bail:
  return ret_val;
}

/**
  @brief Helper API to create a registry command to iterate through
  group_id_start and group_id_stop.
*/
STATIC
sns_dl_reg_cmd_t* sns_dl_reg_create_iterator_cmd
(
  sns_dl_svc_t    *p_svc,
  uint32_t        group_id_start,
  uint32_t        group_id_stop,
  uint32_t        timeout_us,
  bool            timing_profile_flag,
  sns_err_code_e  *p_err_code
)
{
  return sns_dl_create_reg_cmd(p_svc,
                                group_id_start,
                                group_id_stop,
                                timeout_us,
                                DL_REG_CMD_ITERATE,
                                batch_read_handler, NULL,
                                sns_dl_reg_cmd_read_cb,
                                NULL, 0, 0, timing_profile_flag, p_err_code);
}

/**
  @brief Implementation to iterate through a set of registry groups and read
  their contents. We essentially send out messages in batches and accumulate
  the responses on the registry queue. Once done we iterator or match in case
  the operation was 'search' and invoke appropriate callbacks.
*/
STATIC
void sns_reg_for_each_reg_resp
(
  sns_dl_reg_cmd_t      *p_reg_cmd,
  pfn_sns_reg_for_each  pfn_for_each,
  pfn_sns_reg_match     pfn_match,
  sns_reg_dyn_cfg_t     *p_cfg,
  int                   *p_num_proccessed
)
{
  sns_err_code_e err_code;
  sns_dl_reg_cmd_resp_t *p_resp;
  int cnt, num_responses_processed = 0, num_responses_received = 0;
  bool cmp = false;
  sns_q_link_s *q_curr;

  SNS_DL_REG_CMD_LOCK(p_reg_cmd);
  cnt = sns_q_cnt(&(p_reg_cmd->reg_response_q));
  SNS_DL_PRINTF(ERROR, "sns_reg_for_each_reg_resp # MSGS:%d", cnt);
  if (cnt > 0) {
    for ( q_curr = sns_q_check(&(p_reg_cmd->reg_response_q));
          NULL != q_curr;
          q_curr = sns_q_next(&(p_reg_cmd->reg_response_q), q_curr) ) {
      p_resp = (sns_dl_reg_cmd_resp_t*)q_curr;
      if (p_resp) {
        num_responses_received++;
        if ((NULL != p_resp->reg_data) && (p_resp->reg_data_size > 0)) {
          err_code = memcpy_reg_data(p_cfg,
                              p_resp->reg_data,
                              p_resp->reg_data_size);
          if (SNS_SUCCESS == err_code) {
            if (DL_REG_CMD_ITERATE == p_reg_cmd->cmd_type) {
                num_responses_processed++;
                SNS_DL_PRINTF(LOW, "Calling CB#%d", num_responses_processed);
                if (pfn_for_each) {
                  pfn_for_each(p_cfg);
                }
            } else {
              if (pfn_match) {
                cmp = pfn_match(p_reg_cmd->srch_data,
                                p_reg_cmd->srch_data_len, p_cfg);
              }
              if (cmp) {
                num_responses_processed++;
                SNS_DL_PRINTF(LOW, "Match Found!");
                break;
              }
            }
          }
        }
      }
    }
  }
  SNS_DL_REG_CMD_UNLOCK(p_reg_cmd);
  SNS_DL_PRINTF(LOW, "sns_reg_for_each_reg_resp ret_val:%d, Num Responses:%d",
                        num_responses_processed, num_responses_received);
                        
  if (p_num_proccessed)
    *p_num_proccessed = num_responses_processed;
}
/**
  @brief see sns_dl_svc_for_each for details
*/
sns_err_code_e sns_dl_svc_reg_for_each
(
  sns_dl_svc_t          *p_svc,
  pfn_sns_reg_for_each  pfn,
  sns_reg_dyn_cfg_t     *p_cfg_param,
  uint32_t              group_id_start,
  uint32_t              group_id_stop,
  uint32_t              timeout_us,
  bool                  timing_profile_flag
)
{
  sns_err_code_e  ret_val;
  sns_dl_reg_cmd_t *p_reg_cmd;
  int num_cb_processed = 0;
  
  /* check if the SVC is fully initialize and if not try to init */
  ret_val = sns_dl_svc_check_and_initialize(p_svc);
  if (SNS_SUCCESS != ret_val) {
    SNS_DL_PRINTF(ERROR, "sns_dl_svc_reg_for_each chk & init %#x", ret_val);
    goto bail;
  }
  /* create the iterator command */
  p_reg_cmd = sns_dl_reg_create_iterator_cmd(p_svc,
                                              group_id_start,
                                              group_id_stop,
                                              timeout_us,
                                              timing_profile_flag,
                                              &ret_val);
  if (!p_reg_cmd) {
    SNS_DL_PRINTF(ERROR, "Creating Registry Command Failed!", ret_val);
    goto bail;
  }
  /* if created, now execute the command */
  ret_val = execute_reg_cmd(p_reg_cmd);
  SNS_DL_PRINTF(ERROR, "Done Executing Iterator CMD %#x", ret_val);
  /* iterate through the received messages and execute the user callback */
  if (SNS_SUCCESS == ret_val) {
    sns_reg_for_each_reg_resp(p_reg_cmd, pfn, NULL, p_cfg_param, 
                                &num_cb_processed);
  }
  SNS_DL_PRINTF(LOW, "Done Iter CMD Num Callbacks:%d Return Val%#x",
                        ret_val, num_cb_processed);
  sns_dl_destroy_reg_cmd(p_reg_cmd);
bail:
  return ret_val;
}

/**
  @brief Implementation to perform registry search. Essentially this uses
  the batch read paradigm but with a single batch read since we essentially
  rely on the search capability in the registry service to locate the registry
  group of interest.
*/
STATIC
sns_err_code_e reg_search_handler(sns_dl_reg_cmd_t *p_reg_cmd, void* data)
{
  sns_err_code_e  ret_val = SNS_ERR_NOTFOUND;
  /* batch size is 1 only because we sent a single search request across */
  uint32_t read_batch_size = 1;
  bool terminate_read_ignored;
  pre_batch_read_init(p_reg_cmd, read_batch_size);
  ret_val = sns_dl_reg_client_perform_ops(p_reg_cmd);
  if (SNS_SUCCESS == ret_val) {
    ret_val = block_batch_read_caller(p_reg_cmd,
                                      read_batch_size,
                                      &terminate_read_ignored);
    if (SNS_SUCCESS != ret_val) {
      /* either a timeout or some error has occurred */
      SNS_DL_PRINTF(ERROR, "sns_reg_for_each Block Error %#x", ret_val);
    }
  } else {
    SNS_DL_PRINTF(ERROR, "SRCH sns_dl_reg_client_perform_ops Err %#x", ret_val);
  }

  return ret_val;
}
/**
  @brief Helper API to create a registry command to search by key 'UUID'
*/
STATIC
sns_dl_reg_cmd_t* sns_dl_reg_create_search_cmd
(
  sns_dl_svc_t    *p_svc,
  uint32_t        group_id_start,
  uint32_t        group_id_stop,
  uint32_t        timeout_us,
  void            *data,
  uint32_t        data_len,
  uint8_t         search_position,
  bool            timing_profile_flag,
  sns_err_code_e  *p_err_code
)
{
  return sns_dl_create_reg_cmd(p_svc,
                                group_id_start,
                                group_id_stop,
                                timeout_us,
                                DL_REG_CMD_SEARCH,
                                reg_search_handler, NULL,
                                sns_dl_reg_cmd_read_cb,
                                data, data_len, search_position,
                                timing_profile_flag,
                                p_err_code);
}
/**
  @brief see sns_dl_svc_search_identifier for details
*/
sns_err_code_e sns_dl_svc_reg_search_identifier
(
  sns_dl_svc_t      *p_svc,
  sns_reg_dyn_cfg_t *p_cfg,
  void              *id,
  size_t            id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t          group_id_start,
  uint32_t          group_id_stop,
  uint32_t          timeout_us,
  bool              timing_profile_flag
)
{
  sns_err_code_e ret_val = SNS_ERR_NOTFOUND;
  sns_dl_reg_cmd_t *p_reg_cmd;
  int found_data = 0;  
  
  /* check if the SVC is fully initialize and if not try to init */
  ret_val = sns_dl_svc_check_and_initialize(p_svc);
  if (SNS_SUCCESS != ret_val) {
    SNS_DL_PRINTF(ERROR, "sns_dl_reg_search_identifier chk&init %#x", ret_val);
    goto bail;
  }

  /* at this time search by UUID is only supported and the only use case */
  switch (id_type) {
    case SNS_DL_REG_UUID:
      if (SNS_DRIVER_UUID_SZ != id_sz) {
        ret_val = SNS_ERR_BAD_PARM;
        goto bail;
      }
      p_reg_cmd = sns_dl_reg_create_search_cmd(p_svc,
                                              group_id_start, group_id_stop,
                                              timeout_us,
                                              id, id_sz, SNS_DSO_UUID_OFFSET,
                                              timing_profile_flag,
                                              &ret_val);
      if (p_reg_cmd) {
        ret_val = execute_reg_cmd(p_reg_cmd);
        SNS_DL_PRINTF(ERROR, "Done Executing SRCH CMD %#x", ret_val);
        /* obtain the received message and execute the user callback */
        if (SNS_SUCCESS == ret_val) {
          sns_reg_for_each_reg_resp(p_reg_cmd, NULL, sns_dl_cmp_uuid_cb,
                                      p_cfg, &found_data);
          if (0 == found_data) {
            ret_val = SNS_ERR_NOTFOUND;
          }
        }
        SNS_DL_PRINTF(LOW, "Done SRCH CMD Found?:%d Return Val%#x",
                              found_data, ret_val);
        sns_dl_destroy_reg_cmd(p_reg_cmd);
      } else {
        SNS_DL_PRINTF(ERROR, "Creating Registry Command Failed!", ret_val);
      }
      break;
    default:
      ret_val = SNS_ERR_BAD_PARM;
      break;
  };

bail:
  return ret_val;
}

STATIC
sns_err_code_e timeout_block_handler(sns_dl_reg_cmd_t *p_reg_cmd, void* data)
{
  sns_err_code_e ret_val;
  bool terminate_read = false;
  int read_batch_size = 1;
  pre_batch_read_init(p_reg_cmd, read_batch_size);
  ret_val = block_batch_read_caller(p_reg_cmd,read_batch_size, &terminate_read);
  return ret_val;
}

sns_err_code_e sns_dl_reg_test_hook_timeout
(
  sns_dl_svc_handle_t svc_handle,
  uint32_t timeout_us
)
{
  sns_err_code_e ret_val = SNS_ERR_NOTSUPPORTED;

  sns_dl_svc_t      *p_svc;
  sns_dl_reg_cmd_t  *p_reg_cmd;

  GET_SVC_FROM_HANDLE(svc_handle, p_svc);
  p_reg_cmd = sns_dl_create_reg_cmd(p_svc,
                                SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                                SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                                timeout_us,
                                DL_REG_CMD_ITERATE,
                                timeout_block_handler, NULL,
                                sns_dl_reg_cmd_read_cb,
                                NULL, 0, 0, false, &ret_val);
  SNS_DL_PRINTF(ERROR, "Created Timeout Command %#x 0x%p", ret_val, p_reg_cmd);
  if (p_reg_cmd) {
    SNS_DL_PRINTF(ERROR, "Executing Timeout CMD %#x 0x%p", ret_val, p_reg_cmd);
    ret_val = execute_reg_cmd(p_reg_cmd);
    SNS_DL_PRINTF(ERROR, "Executed Timeout CMD %#x 0x%p", ret_val, p_reg_cmd);
    sns_dl_destroy_reg_cmd(p_reg_cmd);
    SNS_DL_PRINTF(ERROR, "Destroyed Timeout CMD %#x 0x%p", ret_val, p_reg_cmd);
  }

  return ret_val;
}

STATIC
void sns_dl_reg_test_hook_err_timeout_handler(void *timeout_param)
{
  sns_dl_reg_cmd_t *p_reg_cmd = (sns_dl_reg_cmd_t*)timeout_param;
  sns_dl_reg_cmd_handle_exit(p_reg_cmd);
}

sns_err_code_e sns_dl_reg_test_hook_error
(
  sns_dl_svc_handle_t svc_handle,
  uint32_t timeout_us
)
{
  sns_err_code_e ret_val = SNS_ERR_NOTSUPPORTED;

  sns_dl_svc_t      *p_svc;
  sns_dl_reg_cmd_t  *p_reg_cmd;
  sns_em_timer_obj_t err_timeout_timer;
  bool timer_created = false;
  uint32_t timeout_ticks;

  GET_SVC_FROM_HANDLE(svc_handle, p_svc);

  p_reg_cmd = sns_dl_create_reg_cmd(p_svc,
                                SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                                SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                                timeout_us,
                                DL_REG_CMD_ITERATE,
                                timeout_block_handler, NULL,
                                sns_dl_reg_cmd_read_cb,
                                NULL, 0, 0, false, &ret_val);
  SNS_DL_PRINTF(ERROR, "Created Error CMD %#x 0x%p", ret_val, p_reg_cmd);
  if (p_reg_cmd) {
    ret_val = sns_em_create_timer_obj(sns_dl_reg_test_hook_err_timeout_handler,
                                      p_reg_cmd,
                                      SNS_EM_TIMER_TYPE_ONESHOT,
                                      &err_timeout_timer);  

    if (SNS_SUCCESS == ret_val) {
      timer_created = true;
      /* 2 second timer */
      timeout_ticks = sns_em_convert_usec_to_localtick(2000000);
      ret_val = sns_em_register_timer(err_timeout_timer, timeout_ticks);
      if (SNS_SUCCESS == ret_val) {
        SNS_DL_PRINTF(ERROR, "Executing Err CMD %#x 0x%p", ret_val, p_reg_cmd);
        ret_val = execute_reg_cmd(p_reg_cmd);
        SNS_DL_PRINTF(ERROR, "Executed Err CMD %#x 0x%p",  ret_val, p_reg_cmd);
      } else {
        SNS_DL_PRINTF(ERROR, "Err Timer Register Fail %#x", ret_val);
      }
    } else {
      SNS_DL_PRINTF(ERROR, "Create Error Test Hook Timer Failed %#x", ret_val);
    }
    sns_dl_destroy_reg_cmd(p_reg_cmd);
    SNS_DL_PRINTF(ERROR, "Destroyed Error CMD %#x 0x%p", ret_val, p_reg_cmd);
  }
  if (timer_created) {
    sns_em_cancel_timer(err_timeout_timer);
    sns_em_delete_timer_obj(err_timeout_timer);
  }

  return ret_val;
}
