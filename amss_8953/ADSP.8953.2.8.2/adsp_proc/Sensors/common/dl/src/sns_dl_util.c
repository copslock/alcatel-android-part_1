/*==============================================================================
  @file sns_dl_util.c

  @brief
  Implementation of miscellaneous DL Service utility functions

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

#include "sns_reg_api_v02.h"
#include "sns_dl_priv.h"

/**
  @brief API to read registry data that is packed in little endian to
  the platform's native endianess for a uint32_t data.
  
  @param[in] data -- An unsigned 32 bit integer packed in little endian format
  
  @return
    32 bit unsigned integer in the platform's endianess
*/
uint32_t sns_dl_read_platform_endian_uint32(uint32_t data)
{
  uint32_t ret_val;
  int x = 1;
  if (*((char*)&x) == 1)
    /* platform is little endian, just copy over */
    ret_val = data;
  else
    /* platform is big endian convert - little endian value to big */
    ret_val = (((data & 0xff) << 24)     |
               ((data & 0xff00) << 8)    |
               ((data & 0xff0000) >> 8)  |
               ((data & 0xff000000) >> 24));
  return ret_val;
}

/**
  @brief API to copy members of a structure of type sns_reg_dyn_cfg_v01_t
  to the generic sns_reg_dyn_cfg_t structure.
  
  @param[out] p_dest -- Pointer to a generic DL reg config object
  @param[in]  p_src  -- Pointer to a sns_reg_dyn_cfg_v01_t DL reg config object
  
  @return
    None
*/
void sns_dl_memcpy_v01(sns_reg_dyn_cfg_t *p_dest, sns_reg_dyn_cfg_v01_t *p_src)
{
  memscpy(p_dest->uuid, SNS_DRIVER_UUID_SZ,
            p_src->uuid, SNS_DRIVER_UUID_SZ_V01);
  strlcpy(p_dest->full_file_path, p_src->full_file_path, SNS_DSO_FILEPATH_SZ);
  strlcpy(p_dest->entry_symbol, p_src->entry_symbol, SNS_DSO_LOAD_SYM_NAME_SZ);
  p_dest->load_attrib = p_src->load_attrib;
}

/**
  @brief API to determine if the start and stop group ids provided is within
  the range of permitted registry groups that contain dynamic loading data
  pertaining to SNS algorithms.
  
  @param[in]  group_id_start -- Start group ID
  @param[in]  group_id_stop  -- Start group ID
  
  @return
    true -- The start and stop group ids are within the permitted range
    false -- The start and stop group ids are not within the permitted range
*/
bool sns_dl_is_in_algo_window(uint32_t group_id_start, uint32_t group_id_stop)
{
  bool ret_val = false;
  if (group_id_start <= group_id_stop) {
    if ( ((SNS_REG_SAM_GROUP_DYN_ALGO_INFO_FIRST_V02 <= group_id_start)  &&
          (group_id_stop <= SNS_REG_SAM_GROUP_DYN_ALGO_INFO_LAST_V02)) ) {
        ret_val = true;
    }
  }
  return ret_val;  
}

/**
  @brief API to determine if the start and stop group ids provided is within
  the range of permitted registry groups that contain dynamic loading data
  pertaining to SNS drivers.
  
  @param[in]  group_id_start -- Start group ID
  @param[in]  group_id_stop  -- Start group ID
  
  @return
    true -- The start and stop group ids are within the permitted range
    false -- The start and stop group ids are not within the permitted range
*/
bool sns_dl_is_in_driver_window(uint32_t group_id_start, uint32_t group_id_stop)
{
  bool ret_val = false;
  if (group_id_start <= group_id_stop) {
    if ( ((SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02 <= group_id_start)  &&
          (group_id_stop <= SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02)) ) {
        ret_val = true;
    }
  }
  return ret_val;
}

/**
  @brief API to determine if the start and stop group ids provided is within
  the range of permitted registry groups that contain dynamic loading data
  pertaining to SNS drivers and algorithms.
  
  @param[in]  group_id_start -- Start group ID
  @param[in]  group_id_stop  -- Start group ID
  
  @return
    true -- The start and stop group ids are within the permitted range
    false -- The start and stop group ids are not within the permitted range
*/
bool sns_dl_is_in_window(uint32_t group_id_start, uint32_t group_id_stop)
{
  bool ret_val;
  if (sns_dl_is_in_algo_window(group_id_start, group_id_stop)) {
    ret_val = true;
  } else if (sns_dl_is_in_driver_window(group_id_start, group_id_stop)) {
    ret_val = true;
  } else {
    ret_val = false;
  }
  return ret_val;
}

/**
  @brief API to determine if the UUID in the generic config object matches
  the one supplied by the caller.
  
  @param[in]  id    -- Pointer to data to compare
  @param[in]  id_sz -- Size of the data to compare
  
  @return
    true  -- UUIDs match
    false -- UUIDs do not match
*/
bool sns_dl_cmp_uuid_cb(void* id, uint32_t id_sz, sns_reg_dyn_cfg_t *p_cfg)
{
  int cmp;
  bool ret_val;
  cmp = memcmp(p_cfg->uuid, (uint8_t*)id, SNS_DRIVER_UUID_SZ);
  ret_val = ((0 == cmp)?true:false);
  return ret_val;
}

/**
  @brief API to copy over registry data into the generic config object.
  The copy is made depending on what supported revisions are 

  @param[in]  p_cfg   -- Pointer to generic config data to be initialized
  @param[in]  src     -- Pointer to the start of the registry data
  @param[in]  src_sz  -- Size of the registry data

  @return
    SNS_SUCCESS           --  Success
    SNS_ERR_BAD_MSG_ID    --  The data from the registry is not supported
                              on this platform
    SNS_ERR_BAD_MSG_SIZE  --  The size of the registry is not supported
                              on this platform
*/
sns_err_code_e memcpy_reg_data
(
  sns_reg_dyn_cfg_t *p_cfg,
  void              *src,
  uint32_t          src_sz
)
{
  sns_err_code_e ret_val;
  sns_reg_dyn_cfg_v01_t *p_cfg_v01;
  uint32_t rev_tmp;
  /* first read the revision and check if supported on this platform */
  rev_tmp = sns_dl_read_platform_endian_uint32(*((uint32_t*)src));
  switch (rev_tmp) {
    case 1:
      /* revision is supported, now check the size */
      if (src_sz != sizeof(sns_reg_dyn_cfg_v01_t)) {
        ret_val = SNS_ERR_BAD_MSG_SIZE;
      } else {
        /* copy over the received data into p_cfg */
        p_cfg_v01 = (sns_reg_dyn_cfg_v01_t*)src;
        /* adjust the revision endianess */
        p_cfg_v01->rev = rev_tmp;
        rev_tmp = sns_dl_read_platform_endian_uint32(p_cfg_v01->load_attrib);
        /* adjust the load attribute endianess */
        p_cfg_v01->load_attrib = rev_tmp;
        /* memcpy the entire registry data into p_cfg */
        sns_dl_memcpy_v01(p_cfg, p_cfg_v01);
        ret_val = SNS_SUCCESS;
      }
      break;
    default:
      ret_val = SNS_ERR_BAD_MSG_ID;
      break;
  };
  return ret_val;
}
