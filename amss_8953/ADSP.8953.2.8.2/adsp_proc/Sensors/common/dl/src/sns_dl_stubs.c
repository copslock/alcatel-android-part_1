/*==============================================================================
  @file sns_dl_stubs.c

  @brief
  Implementation of the DL Service local registry. Primarily used for testing.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

#include "sns_common.h"
#include "sns_reg_api_v02.h"
#include "sns_reg_common.h"
#include "sns_dl_priv.h"

#define DUMMY_UUID \
  {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0}

/**
  @brief Data structure to load the required SNS algorithms. This is based on 
  V01 type config i.e. sns_reg_dyn_cfg_v01_t
*/
static
sns_reg_dyn_cfg_v01_t sns_algo_dyn_config_map[] =
{
  {1, DUMMY_UUID, "oem_1.so", "get_sns_algo_registration_list", 0, ""},
  {1, DUMMY_UUID, "ped.so",   "get_sns_algo_registration_list", 0, ""},
  {1, DUMMY_UUID, "qmd.so",   "get_sns_algo_registration_list", 0, ""},

  /* last line must be as is */
  {0, DUMMY_UUID, "", "", 0, ""},
};

/**
  @brief Data structure to load the required SNS drivers. This is based on 
  V01 type config i.e. sns_reg_dyn_cfg_v01_t
*/
static  
sns_reg_dyn_cfg_v01_t sns_drv_config_map[] =
{
  {1, SNS_REG_UUID_BMG160,  "sns_dd_bmg160.so", "get_sns_ddf_driver_vtbl", 0, ""},
  //{1, SNS_REG_UUID_BMA2X2,  "sns_dd_bma2x2_unified.so", "get_sns_ddf_driver_vtbl", 0, ""},
  {1, SNS_REG_UUID_BMA2X2,  "sns_dd_bma2x2.so", "get_sns_ddf_driver_vtbl", 0, ""},
  {1, SNS_REG_UUID_HSCD008, "sns_dd_mag_hscdtd008.so", "get_sns_ddf_driver_vtbl", 0, ""},

  /* last line must be as is */
  {0, DUMMY_UUID, "", "", 0, ""},
};

/**
  @brief  API to initialize the DL SVC object when using the local
          registry tables.

  @note Paired with sns_dl_svc_stub_dtor
  
  @param[in]  p_svc   --  Pointer to the SVC object
  @param[in]  p_attr  --  Pointer to the attribute structure required
                          for initialization

  @return
    SNS_SUCCESS         - Success
*/
sns_err_code_e sns_dl_svc_stub_ctor
(
  sns_dl_svc_t  *p_svc,
  sns_dl_attr_t *p_attr
)
{
  if ((p_attr) && (p_attr->pfn_init_cb)) {
    p_attr->pfn_init_cb(p_attr->init_cb_param, true, SNS_SUCCESS);
  }
  return SNS_SUCCESS;
}

/**
  @brief API to de-initialize the DL SVC object.

  @param[in]  p_svc -- Pointer to the SVC object
  
  @note Paired with sns_dl_svc_reg_ctor

  @return
    SNS_SUCCESS         -- Success
*/
sns_err_code_e sns_dl_svc_stub_dtor(sns_dl_svc_t *p_svc)
{
  return SNS_SUCCESS;
}

sns_err_code_e
sns_dl_svc_stub_is_init(sns_dl_svc_t *p_svc,
                        pfn_is_initialized_cb pfn,
                        void *param,
                        uint32_t timeout_us)
{
  if (pfn) {
    pfn(param, true, SNS_SUCCESS);
  }
  return SNS_SUCCESS;
}

/**
  @brief  API to facilitate iterating within the local DL registry.
  
  @param[in]  p_svc   --  Pointer to the SVC object
  @param[in]  pfn     --  Callback to be issued for every registry
                              item within the scope of the iteration
                              start, stop range
  @param[in|out] p_cfg_param -- Pointer to a config object that will be passed
                                as a parameter for every invocation of pfn
  @param[in]  group_id_start -- Start group index within the registry
  @param[in]  group_id_stop  --  Stop group index within the registry
  @param[in]  timeout_us  --  Time out to block before returning back to the
                              caller. If the timeout is 0, the caller is
                              blocked until the registry search operation
                              completes.
  @param[in]  timing_profile_flag -- 
                  Flag useful in profiling how long a search query would take
                  using additional log messages. Primarily used by test code.

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred before the operation could complete
    SNS_ERR_NOTFOUND    - No algorithms or drivers found
    SNS_ERR_FAILED      - General failure
*/
sns_err_code_e sns_dl_svc_stub_for_each
(
  sns_dl_svc_t          *p_svc,
  pfn_sns_reg_for_each  pfn,
  sns_reg_dyn_cfg_t     *p_cfg_param,
  uint32_t              group_id_start,
  uint32_t              group_id_stop,
  uint32_t              timeout_us,
  bool                  timing_profile_flag
)
{
  sns_err_code_e ret_val;
  uint32_t        rev_tmp, done;
  sns_reg_dyn_cfg_v01_t *p_cfg = NULL;
  sns_reg_dyn_cfg_v01_t *p_cfg_v01;
  
  if (sns_dl_is_in_algo_window(group_id_start, group_id_stop)) {
    p_cfg = sns_algo_dyn_config_map;
  } else if (sns_dl_is_in_driver_window(group_id_start, group_id_stop)) {
    p_cfg = sns_drv_config_map;
  } else {
    ret_val = SNS_ERR_BAD_PARM;
    goto bail;
  }
  done = 0;
  ret_val = SNS_SUCCESS;
  while (p_cfg) {
    memset(p_cfg_param, 0, sizeof(sns_reg_dyn_cfg_t));
    rev_tmp = sns_dl_read_platform_endian_uint32(*((uint32_t*)p_cfg));
    switch (rev_tmp) {
      case 0:
        /* no more rows to process */
        ret_val = SNS_SUCCESS;
        done = 1;
        break;
      case 1:
        /* revision #1 supported */
        p_cfg_v01 = (sns_reg_dyn_cfg_v01_t*)p_cfg;
        p_cfg_v01->rev = rev_tmp;
        rev_tmp = sns_dl_read_platform_endian_uint32(p_cfg_v01->load_attrib);
        p_cfg_v01->load_attrib = rev_tmp;
        sns_dl_memcpy_v01(p_cfg_param, p_cfg_v01);
        ret_val = SNS_SUCCESS;
        break;
      default:
        /* unsupported revision */
        ret_val = SNS_ERR_BAD_MSG_ID;
        break;
    };
    if ((SNS_SUCCESS == ret_val) && (!done)) {
      pfn(p_cfg_param);
      p_cfg++;
    } else {
      SNS_DL_PRINTF(ERROR, "Exiting Loop Err:%d Done:%d", ret_val, done);
      break;
    }
  }
bail:
  return ret_val;
}

/**
  @brief  Helper API to facilitate searching within the local DL registry
          table entries. The key to search the tables is variable and depends
          on the supplied match function.
  
  @param[in]  p_svc           --  Pointer to the SVC object
  @param[out] p_cfg_param     --  A valid pointer to fill in the registry data.
  @param[in]  group_id_start  -- Start group index within the registry
  @param[in]  group_id_stop   --  Stop group index within the registry
  @param[in]  pfn_match       --  Match function use for comparison of
                                registry data with the supplied id and id_sz
  @param[in]  id              --  Key/identifier to search within the registry
  @param[in]  id_sz           --  Key size

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred before the operation could complete
    SNS_ERR_NOTFOUND    - No algorithms or drivers found
    SNS_ERR_FAILED      - General failure
*/
STATIC
bool linear_search_stub
(
  sns_dl_svc_t      *p_svc,
  sns_reg_dyn_cfg_t *p_cfg_param,
  uint32_t          group_id_start,
  uint32_t          group_id_stop,
  pfn_sns_reg_match pfn_match,
  void              *id,
  size_t            id_sz
)
{
  int done = 0;
  bool ret_val = false;
  uint32_t rev_tmp;
  sns_err_code_e err_code;
  sns_reg_dyn_cfg_v01_t *p_cfg;
  sns_reg_dyn_cfg_v01_t *p_cfg_v01;

  if (!p_cfg_param) {
    err_code = SNS_ERR_BAD_PARM;
    goto bail;
  }
  if (sns_dl_is_in_algo_window(group_id_start, group_id_stop)) {
    p_cfg = sns_algo_dyn_config_map;
  } else if (sns_dl_is_in_driver_window(group_id_start, group_id_stop)) {
    p_cfg = sns_drv_config_map;
  } else {
    err_code = SNS_ERR_BAD_PARM;
    goto bail;
  }
  while (p_cfg) {
    rev_tmp = sns_dl_read_platform_endian_uint32(*((uint32_t*)p_cfg));
    switch (rev_tmp) {
      case 0:
        /* no more rows to process */
        err_code = SNS_SUCCESS;
        done = 1;
        break;
      case 1:
        /* revision #1 supported */
        p_cfg_v01 = (sns_reg_dyn_cfg_v01_t*)p_cfg;
        p_cfg_v01->rev = rev_tmp;
        rev_tmp = sns_dl_read_platform_endian_uint32(p_cfg_v01->load_attrib);
        p_cfg_v01->load_attrib = rev_tmp;
        sns_dl_memcpy_v01(p_cfg_param, p_cfg_v01);
        ret_val = pfn_match(id, id_sz, p_cfg_param);
        if (ret_val)
          done = 1;
        err_code = SNS_SUCCESS;
        break;
      default:
        /* unsupported revision */
        err_code = SNS_ERR_BAD_MSG_ID;
        break;
    };
    if ((SNS_SUCCESS == err_code) && (!done)) {
      p_cfg++;
    } else {
      SNS_DL_PRINTF(ERROR, "Exiting Loop Err:%d Done:%d", err_code, done);
      break;
    }
  }
bail:
  return ret_val;
}

/**
  @brief  API to facilitate searching within the local DL registry.
  
  @param[in]  handle  --  A valid handle
  @param[out] p_cfg   --  A valid pointer to the contain the registry data.
  @param[in]  id      --  Key/identifier to search within the registry
  @param[in]  id_sz   --  Key size
  @param[in]  id_type --  Type of key/identifier associated with id
  @param[in]  group_id_start -- Start group index within the registry
  @param[in]  group_id_stop  --  Stop group index within the registry
  @param[in]  timeout_us  --  Time out to block before returning back to the
                              caller. If the timeout is 0, the caller is
                              blocked until the registry search operation
                              completes.
  @param[in]  timing_profile_flag -- 
                  Flag useful in profiling how long a search query would take
                  using additional log messages. Primarily used by test code.

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred before the operation could complete
    SNS_ERR_NOTFOUND    - No algorithms or drivers found
    SNS_ERR_FAILED      - General failure
*/
sns_err_code_e sns_dl_svc_stub_search_identifier
(
  sns_dl_svc_t      *p_svc,
  sns_reg_dyn_cfg_t *p_cfg,
  void              *id,
  size_t            id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t          group_id_start,
  uint32_t          group_id_stop,
  uint32_t          timeout_us,
  bool              timing_profile_flag
)
{
  bool cmp;
  sns_err_code_e ret_val = SNS_ERR_NOTFOUND;

  switch (id_type) {
    case SNS_DL_REG_UUID:
      cmp = linear_search_stub(p_svc, p_cfg,
                                group_id_start, group_id_stop,
                                sns_dl_cmp_uuid_cb, id, id_sz);
      if (cmp)
        ret_val = SNS_SUCCESS;
      break;
    default:
      ret_val = SNS_ERR_BAD_PARM;
      break;
  };

  return ret_val;
}

/**
  @brief  Print diagnostic information pertaining to the specified SVC obj.
  
  @param[in]  p_svc  -- Pointer to the SVC object
  
  @return None.
*/
void sns_dl_svc_stub_print
(
  sns_dl_svc_t  *p_svc
)
{

}
