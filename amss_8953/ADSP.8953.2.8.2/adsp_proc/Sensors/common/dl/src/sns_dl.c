/*==============================================================================
  @file sns_dl.c

  @brief
  Implementation of the SNS Dynamic Loading service (DL svc) required for
  reading the list of boot critical shared object based SNS DDF based drivers
  and SAM based SNS algorithms from the registry.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
#include <string.h>
#include <stringl.h>

#include "sns_reg_api_v02.h"
#include "sns_init.h"
#include "sns_dl_priv.h"

//-----------------------------------------------------------------------------
//  DATA DECLARATIONS
//-----------------------------------------------------------------------------
/**
  @brief SNS DL MGR MUTEX Lock/Unlock helper macros
*/
#define SNS_DL_MGR_LOCK(p)            SNS_DL_LOCK((p)->mutex)
#define SNS_DL_MGR_UNLOCK(p)          SNS_DL_UNLOCK((p)->mutex)
#define SNS_DL_MGR_LOCK_VAR(p, err)   SNS_DL_LOCK_VAR((p)->mutex, (err))
#define SNS_DL_MGR_UNLOCK_VAR(p, err) SNS_DL_UNLOCK_VAR((p)->mutex, (err))

/**
  @brief SNS DL SVC obj MUTEX Lock/Unlock helper macros
*/
#define SNS_DL_SVC_LOCK(p)            SNS_DL_LOCK((p)->mutex)
#define SNS_DL_SVC_UNLOCK(p)          SNS_DL_UNLOCK((p)->mutex)
#define SNS_DL_SVC_LOCK_VAR(p, err)   SNS_DL_LOCK_VAR((p)->mutex, (err))
#define SNS_DL_SVC_UNLOCK_VAR(p, err) SNS_DL_UNLOCK_VAR((p)->mutex, (err))

//-----------------------------------------------------------------------------
//  VARIABLES
//-----------------------------------------------------------------------------
/* global flag to indicate if the DL SVC global data have been initialized */
static int is_sns_dl_mgr_initialized = 0;

/* global SVC root that holds all client handles */
sns_dl_svc_mgr_t sns_dl_mgr;

//-----------------------------------------------------------------------------
//  FORWARD DECLARATIONS
//-----------------------------------------------------------------------------

bool sns_dl_svc_are_pending_reg_cmds_present(sns_dl_svc_t *p_svc);

// TODO: Most of these forward declarations are completely unnecessary with
// some more logical code organization.
STATIC bool sns_dl_mgr_validate_svc_and_lock_mgr(sns_dl_svc_t *p_svc_param);

STATIC void sns_dl_svc_unlock_mgr(sns_dl_svc_mgr_t *p_mgr);

STATIC
sns_err_code_e sns_dl_svc_reg_dtor(sns_dl_svc_t *p_svc);

STATIC
sns_err_code_e sns_dl_svc_reg_is_init
(
  sns_dl_svc_t *p_svc,
  pfn_is_initialized_cb pfn_init_cb,
  void *init_cb_param,
  uint32_t timeout_us
);

STATIC
sns_err_code_e sns_dl_svc_for_each
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg_param,
  uint32_t group_id_start,
  uint32_t group_id_stop,
  uint32_t timeout_us,
  bool timing_profile_flag
);

STATIC
sns_err_code_e sns_dl_svc_search_identifier
(
  sns_dl_svc_handle_t svc_handle,
  sns_reg_dyn_cfg_t   *p_cfg,
  void                *id,
  size_t              id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t            group_id_start,
  uint32_t            group_id_stop,
  uint32_t            timeout_us,
  bool                timing_profile_flag
);

STATIC void sns_dl_svc_reg_print_state(sns_dl_svc_t *p_svc);

//------------------------------------------------------------------------------
//  FUNCTIONS
//------------------------------------------------------------------------------
sns_err_code_e sns_dl_set_attr_name
(
  sns_dl_attr_t *p_attr,
  const char *name,
  size_t name_sz
)
{
  if ((p_attr) && (name)) {
    if (name_sz > SNS_DL_CLIENT_NAME_SZ)
      name_sz = SNS_DL_CLIENT_NAME_SZ;
    strlcpy(p_attr->name, name, name_sz);
    return SNS_SUCCESS;
  }

  return SNS_ERR_BAD_PTR;
}

sns_err_code_e sns_dl_get_attr_name
(
  sns_dl_attr_t *p_attr,
  char *name,
  size_t name_sz
)
{
  if ((p_attr) && (name)) {
    if (name_sz > SNS_DL_CLIENT_NAME_SZ)
      name_sz = SNS_DL_CLIENT_NAME_SZ;
    strlcpy(name, p_attr->name, name_sz);
    return SNS_SUCCESS;
  }
  return SNS_ERR_BAD_PTR;
}

/**
  @brief Whether the DL MGR singleton has been initialized

  @return
    true - MGR initialized
    false - MGR not initialized
*/
STATIC
bool sns_dl_mgr_is_initialized(void)
{
  return ((1 == is_sns_dl_mgr_initialized)?true:false);
}

/**
  @brief Initialize the DL SVC. This must be called prior to any
  any client calling of the sns_dl_svc_* APIs.

  @return
    SNS_SUCCESS -- Success
    SNS_ERR_FAILED -- Otherwise
*/
sns_err_code_e sns_dl_init(void)
{
  sns_err_code_e ret_val = SNS_SUCCESS;
  /**
      @note use atomic ops to check is_sns_dl_mgr_initialized otherwise ensure
      single threaded-ness for the very first call of sns_dl_init().
      SNS OSA does not have atomic ops. On the DSP we ensure serialization
      using sns_init FWK.
  */
  if (0 == is_sns_dl_mgr_initialized) {
    is_sns_dl_mgr_initialized = 1;

    {
      uint8_t os_err = 0;
      sns_q_init(&(sns_dl_mgr.sns_dl_svc_q));
      sns_dl_mgr.mutex = NULL;
      sns_dl_mgr.mutex = sns_os_mutex_create(SNS_DL_MODULE_MTX_PRIO, &os_err);
      SNS_ASSERT((OS_ERR_NONE == os_err) && (NULL != sns_dl_mgr.mutex));
      ret_val = sns_dl_reg_ctor(&(sns_dl_mgr.reg));
      /**
        @note in the future, this can be changed to allow caller(s) to
        handle the error
      */
      SNS_ASSERT(SNS_SUCCESS == ret_val);
    }

    SNS_DL_PRINTF(HIGH, "SNS DL Init#:%d", is_sns_dl_mgr_initialized);
    /* let the SNS init FWK know that we are done */
    sns_init_done();
  }

  return ret_val;
}

/**
  @brief Deinit SNS DL. We expect initialization upon process init, however
  is up to client of SNS DL to destroy themselves as part of their clean up.

  @return
    SNS_SUCCESS -- Success
*/
sns_err_code_e sns_dl_deinit(void)
{
  return SNS_SUCCESS;
}

sns_err_code_e sns_dl_svc_destroy
(
  sns_dl_svc_handle_t svc_handle
)
{
  sns_err_code_e  ret_val = SNS_ERR_NOTSUPPORTED;

  pfn_sns_dl_free pfn_free;
  sns_dl_svc_t    *p_svc;
  GET_SVC_FROM_HANDLE(svc_handle, p_svc);
  if (!p_svc) {
    ret_val = SNS_ERR_BAD_PARM;
  } else {
    /* clean up */
    ret_val = p_svc->vtbl.dtor(p_svc);
    if (SNS_SUCCESS == ret_val) {
      pfn_free = p_svc->pfn_free;
      p_svc->init_magic   = 0;
      p_svc->pfn_malloc   = NULL;
      p_svc->pfn_free     = NULL;
      memset(&(p_svc->vtbl), 0, sizeof(sns_dl_svc_vtbl_t));
      pfn_free(p_svc);
    } else {
      SNS_DL_PRINTF(ERROR, "DL SVC Destroy Failed ", ret_val);
    }
  }

  return ret_val;
}

sns_err_code_e
sns_dl_svc_is_initialized
(
  sns_dl_svc_handle_t svc_handle,
  pfn_is_initialized_cb pfn_init_cb,
  void *init_cb_param,
  uint32_t timeout_us
)
{
  sns_err_code_e ret_val = SNS_ERR_NOTSUPPORTED;

  sns_dl_svc_t    *p_svc;
  if (!pfn_init_cb) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  else
  {
    GET_SVC_FROM_HANDLE(svc_handle, p_svc);
    if (!p_svc) {
      ret_val = SNS_ERR_BAD_PARM;
    } else {
      ret_val = p_svc->vtbl.is_init(p_svc, pfn_init_cb, init_cb_param, timeout_us);
    }
  }

  return ret_val;
}

sns_err_code_e sns_dl_reg_for_each_algorithm(sns_dl_svc_handle_t svc_handle,
                                              pfn_sns_reg_for_each pfn,
                                              sns_reg_dyn_cfg_t *p_cfg,
                                              uint32_t timeout_us)

{
  return sns_dl_svc_for_each(svc_handle,
                              pfn,
                              p_cfg,
                              SNS_REG_SAM_GROUP_DYN_ALGO_INFO_FIRST_V02,
                              SNS_REG_SAM_GROUP_DYN_ALGO_INFO_LAST_V02,
                              timeout_us, false);
}

sns_err_code_e sns_dl_reg_for_each_driver(sns_dl_svc_handle_t svc_handle,
                                            pfn_sns_reg_for_each pfn,
                                            sns_reg_dyn_cfg_t *p_cfg,
                                            uint32_t timeout_us)
{
  return sns_dl_svc_for_each(svc_handle,
                              pfn,
                              p_cfg,
                              SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                              SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                              timeout_us, false);
}

sns_err_code_e sns_dl_reg_search_uuid
(
  sns_dl_svc_handle_t svc_handle,
  void* id,
  size_t id_sz,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
)
{
  return sns_dl_svc_search_identifier(svc_handle,
                                    p_cfg, id, id_sz, SNS_DL_REG_UUID,
                                    SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                                    SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                                    timeout_us, false);
}

void sns_dl_svc_print_state
(
  sns_dl_svc_handle_t svc_handle
)
{
  sns_dl_svc_t  *p_svc;
  GET_SVC_FROM_HANDLE(svc_handle, p_svc);
  if (p_svc) {
    p_svc->vtbl.print(p_svc);
  } else {
    SNS_DL_PRINTF(HIGH,
                  "DL SVC Bad Handle! Is MGR Init:%d",
                  is_sns_dl_mgr_initialized);
  }
}

/**
  @brief  Initialize the DL SVC object (using the registry service).

  @note Paired with sns_dl_svc_reg_dtor

  @param[in]  p_svc   --  Pointer to the SVC object
  @param[in]  p_attr  --  Pointer to the attribute structure required
                          for initialization

  @return
    SNS_SUCCESS         - Success
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred before the operation could complete
    SNS_ERR_FAILED      - General failure
*/
STATIC
sns_err_code_e sns_dl_svc_reg_ctor
(
  sns_dl_svc_t  *p_svc,
  sns_dl_attr_t *p_attr
)
{
  sns_err_code_e ret_val = SNS_SUCCESS;
  uint8_t os_err;
  bool is_init = false;

  p_svc->mutex = NULL;
  p_svc->mutex = sns_os_mutex_create(SNS_DL_MODULE_MTX_PRIO, &os_err);
  if (OS_ERR_NONE != os_err) {
    ret_val = SNS_ERR_NOMEM;
    SNS_DL_PRINTF(ERROR, "DL SVC Mutex Create Failed! %#x", os_err);
  }
  else
  {
    /* init the various SVC obj fields */
    strlcpy(p_svc->name, p_attr->name, SNS_DL_CLIENT_NAME_SZ);
    /* init the default the state and err code  */
    p_svc->init_status  = STATE_UNINIT;
    p_svc->err_code     = SNS_ERR_WOULDBLOCK;
    /* init the mgr reference  */
    p_svc->p_mgr        = &sns_dl_mgr;
    /* flag to indicate whether a connection with the registry service
      has been established or not */
    p_svc->reg_client_handle_valid = false;
    /* initialize the line and function that changed the state of the SVC obj */
    p_svc->line         = 0;
    p_svc->func_name[0] = 0;
    /* init queue that will contain all the incoming client requests */
    sns_q_init(&(p_svc->request_q));
    /* add this object to the MGR queue */
    SNS_DL_MGR_LOCK(&sns_dl_mgr);
    sns_q_link(p_svc, (sns_q_link_s*)p_svc);
    sns_q_put(&(sns_dl_mgr.sns_dl_svc_q), (sns_q_link_s*)p_svc);
    SNS_DL_MGR_UNLOCK(&sns_dl_mgr);

    /* either block the calling thread or return immediately */
    if (p_attr->is_blocking) {
      /* block until the either the DL registry service is available
        or a timeout occurs */
      ret_val = sns_dl_reg_init_connection(p_svc, p_attr->timeout_us, &is_init);

      /* if user specified a call back, call it */
      if (p_attr->pfn_init_cb) {
        p_attr->pfn_init_cb(p_attr->init_cb_param, is_init, ret_val);
      }

      if (SNS_SUCCESS != ret_val) {
        sns_os_mutex_del(p_svc->mutex, 0, &os_err);
      }
    }
  }

  return ret_val;
}

/**
  @brief De-initialize the DL SVC object.

  @param[in]  p_svc -- Pointer to the SVC object

  @note Paired with sns_dl_svc_reg_ctor

  @return
    SNS_SUCCESS         -- Success
    SNS_ERR_NOTALLOWED  -- Operation not permitted since there are
                           pending requests
*/
STATIC
sns_err_code_e sns_dl_svc_reg_dtor
(
  sns_dl_svc_t *p_svc
)
{
  uint8_t os_err = 0;
  sns_err_code_e ret_val = SNS_SUCCESS;
  int cnt;

  /* deinit svc obj and remove from the mgr queue */
  SNS_DL_MGR_LOCK(&sns_dl_mgr);
  SNS_DL_SVC_LOCK(p_svc);

  /* check if there any outstanding client requests if so do not deinit */
  cnt = sns_q_cnt(&(p_svc->request_q));
  if (cnt != 0) {
    ret_val = SNS_ERR_NOTALLOWED;
  } else {
    /* terminate connection to the registry service */
    ret_val = sns_dl_reg_deinit_connection(p_svc);
    SNS_ASSERT(SNS_SUCCESS == ret_val);
    p_svc->reg_client_handle_valid = false;
    /* dequeue of the mgr queue */
    sns_q_delete(&(p_svc->q_link));
    p_svc->p_mgr        = NULL;
    p_svc->name[0]      = 0;
    p_svc->line         = 0;
    p_svc->func_name[0] = 0;
  }

  SNS_DL_SVC_UNLOCK(p_svc);
  SNS_DL_MGR_UNLOCK(&sns_dl_mgr);

  /* once we are done with the mutex tear it down */
  if (0 == cnt) {
    sns_os_mutex_del(p_svc->mutex, 0, &os_err);
    SNS_ASSERT(OS_ERR_NONE == os_err);
  }

  return ret_val;
}

/**
  @brief Inform the client that an error has occurred within the remote
  registry service. Clients should not make any new requests (such as searc
  UUID or for_each algorithm).

  @param[in]  p_svc       -- SVC object
  @param[in]  err_code    -- Code describing the error encountered
*/
void sns_dl_svc_handle_error
(
  sns_dl_svc_t *p_svc,
  sns_err_code_e err_code
)
{
  bool is_valid;
  sns_q_link_s *q_req;
  sns_dl_reg_cmd_t *p_reg_cmd;

  /* check if the svc object is still valid and lock the mgr */
  is_valid = sns_dl_mgr_validate_svc_and_lock_mgr(p_svc);
  if (is_valid) {
    /* DL svc obj is valid, perform cleanup */
    SNS_DL_SVC_LOCK(p_svc);

    /* dequeue all pending client requests */
    for ( q_req = sns_q_check(&(p_svc->request_q)); NULL != q_req;
        q_req = sns_q_next(&(p_svc->request_q), q_req) ) {
      p_reg_cmd = (sns_dl_reg_cmd_t*)q_req;
      sns_dl_reg_cmd_force_exit(p_reg_cmd);
    }

    /* update state of the obj to error to prevent any new requests */
    SNS_DL_SVC_UPDATE_STATE(p_svc, STATE_ERROR, err_code);
    SNS_DL_SVC_UNLOCK(p_svc);
    /* unlock the mgr */
    sns_dl_svc_unlock_mgr(p_svc->p_mgr);
  }
}


/**
  @brief Return the state of the svc obj and any associated error code.

  @param[in]  p_svc       -- SVC object
  @param[out] p_state     -- Pointer to return the state of the SVC obj
  @param[out] p_err_code  -- Pointer to return the error code of the SVC obj
*/
void sns_dl_svc_get_init_state
(
  sns_dl_svc_t        *p_svc,
  sns_dl_svc_state_t  *p_state,
  sns_err_code_e      *p_err_code
)
{
  SNS_DL_SVC_LOCK(p_svc);
  *p_state = p_svc->init_status;
  *p_err_code = p_svc->err_code;
  SNS_DL_SVC_UNLOCK(p_svc);
}

/**
  @brief Update the runtime state of the SVC object.

  @param[in] p_svc      - Pointer to the SVC object
  @param[in] state      - New state to transition the SVC obj
  @param[in] err_code   - New error code to transition the SVC obj
  @param[in] func_name  - Function name that updated the state.
                          Used for debugging.
  @param[in] line       - Line# of function that updated the state.
                          Used for debugging.
*/
void sns_dl_svc_update_init_state
(
  sns_dl_svc_t        *p_svc,
  sns_dl_svc_state_t  state,
  sns_err_code_e      err_code,
  const char          *func_name,
  int                 line
)
{
  SNS_DL_SVC_LOCK(p_svc);
  switch (p_svc->init_status) {
    case STATE_READY:
      /* once ready ignore any ready to init state changes */
      if (STATE_READY_TO_INIT == state) {
        state = STATE_READY;
        /* preserve current err code */
        err_code = p_svc->err_code;
      }
      break;
    case STATE_ERROR:
      /* error is a terminating state */
      state = STATE_ERROR;
      /* preserve current err code */
      err_code = p_svc->err_code;
      break;
    default:
      /* allow the update to go through */
      break;
  };
  p_svc->init_status = state;
  p_svc->err_code = err_code;
  p_svc->line = line;
  strlcpy(p_svc->func_name, func_name, SNS_DL_FUNC_NAME_SZ);
  SNS_DL_SVC_UNLOCK(p_svc);
}

/**
  @brief Determine if the USMR/(QMI) client interface to the registry
  service has been created.

  @param[in]  p_svc       - SVC object
  @param[in]  pfn_init_cb - Callback function to be invoked either on timeout,
                            error or successful registry service connection
  @param[in]  init_cb_param - Callback function parameter to be returned
  @param[in]  timeout_us  - Blocking timeout supplied by caller
*/
STATIC
sns_err_code_e sns_dl_svc_reg_is_init
(
  sns_dl_svc_t *p_svc,
  pfn_is_initialized_cb pfn_init_cb,
  void        *init_cb_param,
  uint32_t    timeout_us
)
{
  sns_err_code_e      ret_val = SNS_SUCCESS;
  sns_dl_svc_state_t  state;
  bool                is_init = false;

  /* check if this SVC obj has been initialized */
  sns_dl_svc_get_init_state(p_svc, &state, &ret_val);
  if (STATE_UNINIT == state) {
    /* if uninitialized, create connection to registry service */
    ret_val = sns_dl_reg_is_initialized_cb(p_svc, &(sns_dl_mgr.reg),
                                            pfn_init_cb, init_cb_param,
                                            timeout_us);
  } else {
    /* if state is ready just return */
    if ((STATE_READY == state) || (STATE_READY_TO_INIT == state)) {
      is_init = true;
    }
    /* invoke callback */
    pfn_init_cb(init_cb_param, is_init, ret_val);
  }

  return ret_val;
}

/**
  @brief Check if the SVC object has been initialized or ready to be
  initialized. If not, proceed to initialize. This is a blocking call.

  @param[in]  p_svc       - Pointer to the SVC object

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred when initializing the DL SVC
*/
sns_err_code_e sns_dl_svc_check_and_initialize
(
  sns_dl_svc_t *p_svc
)
{
  bool is_init;
  sns_err_code_e ret_val;

  SNS_DL_SVC_LOCK(p_svc);
  if (STATE_READY == p_svc->init_status) {
    ret_val = SNS_SUCCESS;
  } else if (STATE_READY_TO_INIT == p_svc->init_status) {
    ret_val = sns_dl_reg_init_connection(p_svc, 0, &is_init);
  } else {
    ret_val = p_svc->err_code;
  }
  SNS_DL_SVC_UNLOCK(p_svc);

  return ret_val;
}

/**
  @brief Atomically add a new request onto the svc queue.

  @param[in]  p_svc       - SVC object
*/
void sns_dl_svc_enqueue_command
(
  sns_dl_svc_t *p_svc,
  sns_dl_reg_cmd_t *p_reg_cmd
)
{
  SNS_DL_SVC_LOCK(p_svc);
  sns_q_link(p_reg_cmd, (sns_q_link_s*)p_reg_cmd);
  sns_q_put(&(p_svc->request_q), (sns_q_link_s*)p_reg_cmd);
  SNS_DL_SVC_UNLOCK(p_svc);
}

/**
  @brief Atomically remove a request from the svc queue.

  @param[in]  p_svc       - Pointer to the SVC object
*/
void sns_dl_svc_dequeue_command
(
  sns_dl_svc_t *p_svc,
  sns_dl_reg_cmd_t *p_reg_cmd
)
{
  SNS_DL_SVC_LOCK(p_svc);
  sns_q_delete(&(p_reg_cmd->q_link));
  SNS_DL_SVC_UNLOCK(p_svc);
}

/**
  @brief Atomically validate a SVC object and lock the MGR.
  Reason for this is because during an exception condition such as an error
  the SVC handle might have been removed of the queue.

  We lock the MGR so as to first determine if the SVC obj is still enqueued
  with the MGR and if so return if valid or not. If valid, the MGR stays locked
  until sns_dl_svc_unlock_mgr is called.

  @note: This API is to be used in conjunction with sns_dl_svc_unlock_mgr.

  @param[in]  p_svc_param  - Pointer to the SVC object to check

  @return
    true - p_svc_param is valid, false otherwise.
*/
STATIC
bool sns_dl_mgr_validate_svc_and_lock_mgr
(
  sns_dl_svc_t *p_svc_param
)
{
  uint8_t os_err;
  sns_q_link_s *q_svc = NULL;
  sns_dl_svc_t *p_svc = NULL;

  SNS_DL_MGR_LOCK_VAR(&sns_dl_mgr, os_err);
  for ( q_svc = sns_q_check(&(sns_dl_mgr.sns_dl_svc_q)); NULL != q_svc;
        q_svc = sns_q_next(&(sns_dl_mgr.sns_dl_svc_q), q_svc) ) {
    p_svc = (sns_dl_svc_t*)q_svc;

    if (p_svc == p_svc_param) {
      break;
    }
  }

  if (p_svc != p_svc_param) {
    SNS_DL_MGR_UNLOCK_VAR(&sns_dl_mgr, os_err);
    return false;
  }
  else {
    return true;
  }
}

/**
  @brief Unlock a MGR. Used only with sns_dl_mgr_validate_svc_and_lock_mgr.
*/
STATIC
void sns_dl_svc_unlock_mgr
(
  sns_dl_svc_mgr_t *p_mgr
)
{
  uint8_t os_err;
  SNS_DL_MGR_UNLOCK_VAR(p_mgr, os_err);
}

/**
  @brief Atomically validate a registry command associated
  with a SVC obj.  Reason for this is because during an asynchronous
  exception condition such as an error the SVC might might have already
  been removed. We lock the MGR so as to first determine if the SVC obj
  associated with the command is still enqueued with the MGR and and
  if so we check if the command is still valid by checking if it is enqueued
  on the SVC queue.

  @note:  This API is to be used in conjunction with
          sns_dl_svc_reg_cmd_unlock_svc.

  @param[in]  p_reg_cmd  - Pointer to the reg command object to validate

  @return
    true - p_reg_cmd is valid, false otherwise.
*/
bool sns_dl_svc_validate_reg_cmd_and_lock_svc
(
  sns_dl_reg_cmd_t *p_reg_cmd
)
{
  bool found = false;
  sns_q_link_s *q_svc, *q_req;
  sns_dl_svc_t *p_svc;
  uint8_t os_err;

  SNS_DL_MGR_LOCK_VAR(&sns_dl_mgr, os_err);
  for ( q_svc = sns_q_check(&(sns_dl_mgr.sns_dl_svc_q)); NULL != q_svc;
        q_svc = sns_q_next(&(sns_dl_mgr.sns_dl_svc_q), q_svc) ) {
    p_svc = (sns_dl_svc_t*)q_svc;

    SNS_DL_SVC_LOCK_VAR(p_svc, os_err);
    for ( q_req = sns_q_check(&(p_svc->request_q)); NULL != q_req;
          q_req = sns_q_next(&(p_svc->request_q), q_req) ) {
      if (p_reg_cmd == (sns_dl_reg_cmd_t*)q_req) {
        found = true;
        break;
      }
    }

    if(!found) {
      SNS_DL_SVC_UNLOCK_VAR(p_svc, os_err);
    } else {
      break;
    }
  }

  if(!found) {
    SNS_DL_MGR_UNLOCK_VAR(&sns_dl_mgr, os_err);
  }

  return found;
}

/**
  @brief Unlock the SVC obj and MGR that was associated with
  the supplied command. API sns_dl_svc_validate_reg_cmd_and_lock_svc is
  expected to have been called prior to using this API.

  @param[in]  p_svc       - Pointer to the SVC object
*/
void sns_dl_svc_reg_cmd_unlock_svc
(
  sns_dl_svc_t *p_svc
)
{
  uint8_t os_err;
  SNS_DL_SVC_UNLOCK_VAR(p_svc, os_err);
  SNS_DL_MGR_UNLOCK_VAR(&sns_dl_mgr, os_err);
}

/**
  @brief  Print diagnostic information pertaining to the specified SVC obj.

  @param[in]  p_svc       - SVC object
*/
STATIC
void sns_dl_svc_reg_print_state(sns_dl_svc_t *p_svc)
{
  SNS_DL_PRINTF(HIGH,
                "Is MGR Init:%d DL SVC Init State:%#x ErrCode:%#x Line:%d",
                is_sns_dl_mgr_initialized, p_svc->init_status,
                p_svc->err_code, p_svc->line);
}

/**
  @brief  Helper API to facilitate searching within the DL registry.

  @param[in]  handle  --  A valid handle
  @param[out] p_cfg   --  A valid pointer to the contain the registry data.
  @param[in]  id      --  Key/identifier to search within the registry
  @param[in]  id_sz   --  Key size
  @param[in]  id_type --  Type of key/identifier associated with id
  @param[in]  group_id_start -- Start group index within the registry
  @param[in]  group_id_stop  --  Stop group index within the registry
  @param[in]  timeout_us  --  Time out to block before returning back to the
                              caller. If the timeout is 0, the caller is
                              blocked until the registry search operation
                              completes.
  @param[in]  timing_profile_flag --
                  Flag useful in profiling how long a search query would take
                  using additional log messages. Primarily used by test code.
*/
STATIC
sns_err_code_e sns_dl_svc_search_identifier
(
  sns_dl_svc_handle_t svc_handle,
  sns_reg_dyn_cfg_t   *p_cfg,
  void                *id,
  size_t              id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t            group_id_start,
  uint32_t            group_id_stop,
  uint32_t            timeout_us,
  bool                timing_profile_flag
)
{
  sns_err_code_e ret_val;
  sns_dl_svc_t      *p_svc;

  GET_SVC_FROM_HANDLE(svc_handle, p_svc);
  if ((!p_svc) || (!p_cfg) || (!id) || (0 == id_sz) ||
      !sns_dl_is_in_window(group_id_start, group_id_stop)) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  else {
    /* force init to zero */
    memset(p_cfg, 0, sizeof(sns_reg_dyn_cfg_t));
    ret_val = p_svc->vtbl.search(p_svc, p_cfg, id, id_sz, id_type,
                                  group_id_start, group_id_stop, timeout_us,
                                  timing_profile_flag);
  }

  return ret_val;
}

/**
  @brief  Iterate over all services, and pass the config object to each.

  @param[in]  handle  --  A valid handle
  @param[in]  pfn     --  Callback to be issued for every registry
                              item within the scope of the iteration
                              start, stop range
  @param[in|out] p_cfg_param -- Pointer to a config object that will be passed
                                as a parameter for every invocation of pfn
  @param[in]  group_id_start -- Start group index within the registry
  @param[in]  group_id_stop  --  Stop group index within the registry
  @param[in]  timeout_us  --  Time out to block before returning back to the
                              caller. If the timeout is 0, the caller is
                              blocked until the registry search operation
                              completes.
  @param[in]  timing_profile_flag --
                  Flag useful in profiling how long a search query would take
                  using additional log messages. Primarily used by test code.

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_WOULDBLOCK  - Time out occurred before the operation could complete
    SNS_ERR_NOTFOUND    - No algorithms or drivers found
    SNS_ERR_FAILED      - General failure
*/
STATIC
sns_err_code_e sns_dl_svc_for_each
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg_param,
  uint32_t group_id_start,
  uint32_t group_id_stop,
  uint32_t timeout_us,
  bool timing_profile_flag
)
{
  sns_err_code_e    ret_val;
  sns_dl_svc_t      *p_svc;

  GET_SVC_FROM_HANDLE(svc_handle, p_svc);
  if ((!p_svc) || (!p_cfg_param) || (!pfn) ||
      !sns_dl_is_in_window(group_id_start, group_id_stop)) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  else {
    ret_val = p_svc->vtbl.for_each(p_svc, pfn, p_cfg_param,
                                  group_id_start, group_id_stop,
                                  timeout_us, timing_profile_flag);
  }

  return ret_val;
}

sns_err_code_e sns_dl_reg_test_hook_timing_for_each_algo
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
)
{
  return sns_dl_svc_for_each(svc_handle,
                              pfn,
                              p_cfg,
                              SNS_REG_SAM_GROUP_DYN_ALGO_INFO_FIRST_V02,
                              SNS_REG_SAM_GROUP_DYN_ALGO_INFO_LAST_V02,
                              timeout_us, true);
}

sns_err_code_e sns_dl_reg_test_hook_timing_for_each_driver
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
)
{
  return sns_dl_svc_for_each(svc_handle,
                              pfn,
                              p_cfg,
                              SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                              SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                              timeout_us, true);
}

sns_err_code_e sns_dl_reg_test_hook_timing_search_uuid
(
  sns_dl_svc_handle_t svc_handle,
  void* id,
  size_t id_sz,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
)
{
  return sns_dl_svc_search_identifier(svc_handle,
                                  p_cfg, id, id_sz, SNS_DL_REG_UUID,
                                  SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_FIRST_V02,
                                  SNS_REG_SAM_GROUP_DYN_DRIVER_INFO_LAST_V02,
                                  timeout_us, true);
}

/**
  @brief API to initialize the DL SVC structure
  @param[in] p_attr     -- Valid pointer to an attribute object

  @return
    SNS_SUCCESS - Call successful
    SNS_ERR_BAD_PTR - Invalid pointer
*/
sns_err_code_e sns_dl_attr_init(sns_dl_attr_t *p_attr)
{
  if (p_attr) {
    p_attr->pfn_malloc  = NULL;
    p_attr->pfn_free    = NULL;
    p_attr->pfn_init_cb = NULL;
    p_attr->init_cb_param = NULL;
    p_attr->name[0]     = 0;
    p_attr->init_magic  = SNS_DL_INIT_MAGIC;
    p_attr->is_blocking = true;
    p_attr->use_local_registry = false;
    p_attr->timeout_us  = 0;
    return SNS_SUCCESS;
  }
  return SNS_ERR_BAD_PTR;
}

/**
  @brief API to de-initialize the DL SVC structure

  @param[in] p_attr     -- Valid pointer to an attribute object
  @return
    SNS_SUCCESS - Call successful
    Failure otherwise
*/
sns_err_code_e sns_dl_attr_deinit(sns_dl_attr_t *p_attr)
{
  sns_err_code_e ret_val;
  ret_val = sns_dl_attr_init(p_attr);
  if (SNS_SUCCESS == ret_val)
    p_attr->init_magic = 0;
  return ret_val;
}

/**
  @brief API to assign an allocator and deallocator API for the
  DL SVC operations.

  @param[in] p_attr     -- Valid pointer to an attribute object
  @param[in] pfn_malloc -- Valid pointer allocator API
  @param[in] pfn_free   -- Valid pointer de-allocator API

  @return
    SNS_SUCCESS - Call successful
    SNS_ERR_BAD_PARM - Bad parameter was passed in.
*/
sns_err_code_e sns_dl_set_attr_allocator
(
  sns_dl_attr_t     *p_attr,
  pfn_sns_dl_malloc pfn_malloc,
  pfn_sns_dl_free   pfn_free
)
{
  if ((p_attr) && (pfn_malloc) && (pfn_free)) {
    p_attr->pfn_malloc = pfn_malloc;
    p_attr->pfn_free = pfn_free;
    return SNS_SUCCESS;
  }
  return SNS_ERR_BAD_PARM;
}

/**
  @brief API to describe how a client could either block for a period
  specified by time out and/or issue a callback with the result of
  initialization of DL SVC handle.

  @note: The client is expected to block until the initialization dependencies
  are met or provide a callback which will be issued once the time out expires.
  If a callback is provided, it will always be invoked either on a successful
  initialization, a failure or time out.

  @param[in] p_attr       --  Valid pointer to an attribute object
  @param[in] should_block --  Boolean denoting weather the caller should be
                              blocked or not waiting on initialization to
                              complete.
  @param[in] timeout_us   --  Time out in usec to block the caller until all
                              DL SVC initialization dependencies are met.
  @param[in] pfn          --  Callback function pointer that is called on a
                              successful initialization, a failure or time out.
  @param[in] init_cb_param -- Caller supplied parameter to be passed in when
                              the callback is issued.

  @return
    SNS_SUCCESS - Call successful
    SNS_ERR_BAD_PARM - Bad parameter was passed in.
*/
sns_err_code_e sns_dl_set_attr_blocking_info
(
  sns_dl_attr_t *p_attr,
  bool          should_block,
  uint32_t      timeout_us,
  pfn_is_initialized_cb pfn,
  void          *init_cb_param
)
{
  sns_err_code_e ret_val;
  if ((p_attr) && ((should_block) || (pfn))) {
    p_attr->is_blocking = should_block;
    p_attr->timeout_us = timeout_us;
    p_attr->pfn_init_cb = pfn;
    p_attr->init_cb_param = init_cb_param;
    ret_val = SNS_SUCCESS;
  } else {
    ret_val = SNS_ERR_BAD_PARM;
  }
  return ret_val;
}

/**
  @brief API to select whether the local or SNS registry should be consulted
  for dynamic loading of SNS algos and drivers.

  @param[in] p_attr     -- Valid pointer to an attribute object

  @return
    SNS_SUCCESS - Call successful
    Failure otherwise
*/
sns_err_code_e sns_dl_attr_use_local_registry(sns_dl_attr_t *p_attr)
{
  sns_err_code_e ret_val;

  if (p_attr) {
    p_attr->use_local_registry = true;
    ret_val = SNS_SUCCESS;
  } else {
    ret_val = SNS_ERR_BAD_PARM;
  }

  return ret_val;
}

sns_err_code_e sns_dl_svc_create
(
  sns_dl_svc_handle_t *p_handle,
  sns_dl_attr_t       *p_attr
)
{
  sns_err_code_e ret_val = SNS_ERR_NOTSUPPORTED;
  sns_dl_svc_t *p_svc = NULL;

  if ((!p_handle) || (!p_attr)) {
    ret_val = SNS_ERR_BAD_PTR;
  }
  /* ensure that the attr was truly initialized */
  else if (SNS_DL_INIT_MAGIC != p_attr->init_magic) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  /* check the validity of the malloc/free pointers */
  else if ((!p_attr->pfn_malloc) || (!p_attr->pfn_free)) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  /* check if the user either wants to block or supplied a CB or both */
  else if ((!p_attr->is_blocking) && (!p_attr->pfn_init_cb)) {
    ret_val = SNS_ERR_BAD_PARM;
  }
  /* ensure init is called before creation */
  else if (!sns_dl_mgr_is_initialized()) {
    ret_val = SNS_ERR_NOTALLOWED;
  } else {
    /* create and initialize the dl svc object */
    p_svc = (sns_dl_svc_t*)p_attr->pfn_malloc(sizeof(sns_dl_svc_t));
    if (!p_svc) {
      ret_val = SNS_ERR_NOMEM;
      SNS_DL_PRINTF(ERROR, "DL SVC Create No Memory!");
    }
    else {
      /* init the allocators and magic */
      p_svc->pfn_malloc   = p_attr->pfn_malloc;
      p_svc->pfn_free     = p_attr->pfn_free;
      p_svc->init_magic   = SNS_DL_INIT_MAGIC;
      /* init the SVC vtbl */
      if (!p_attr->use_local_registry) {
        p_svc->vtbl.ctor      = sns_dl_svc_reg_ctor;
        p_svc->vtbl.dtor      = sns_dl_svc_reg_dtor;
        p_svc->vtbl.is_init   = sns_dl_svc_reg_is_init;
        p_svc->vtbl.for_each  = sns_dl_svc_reg_for_each;
        p_svc->vtbl.search    = sns_dl_svc_reg_search_identifier;
        p_svc->vtbl.print     = sns_dl_svc_reg_print_state;
      } else {
        p_svc->vtbl.ctor      = sns_dl_svc_stub_ctor;
        p_svc->vtbl.dtor      = sns_dl_svc_stub_dtor;
        p_svc->vtbl.is_init   = sns_dl_svc_stub_is_init;
        p_svc->vtbl.for_each  = sns_dl_svc_stub_for_each;
        p_svc->vtbl.search    = sns_dl_svc_stub_search_identifier;
        p_svc->vtbl.print     = sns_dl_svc_stub_print;
      }
      ret_val = p_svc->vtbl.ctor(p_svc, p_attr);

      if(SNS_SUCCESS != ret_val) {
        p_attr->pfn_free(p_svc);
        *p_handle = SNS_DL_HANDLE_INITIALIZER;
      } else {
        *p_handle = (sns_dl_svc_handle_t)p_svc;
      }
    }
  }

  return ret_val;
}
