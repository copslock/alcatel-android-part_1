/*==============================================================================
  @file sns_dl_dbg.h

  @brief
  Header file describing data structures and interfaces that clients of DL
  SVC can use to report the state of the shared objects that they loaded from 
  the  DL registry. 

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
#ifndef SNS_DL_DBG_H
#define SNS_DL_DBG_H

#include "sns_dl.h"
#include "sns_sam_algo_api.h"

/**
  @brief Shell data structure that contains references relevant debug info
  pertaining to shared object debugging.
*/
struct sns_dl_info_s
{
  /**< -- A string pointer to the SNS algo/driver entry symbol */
  const char    *entry_symbol;
  /**< -- A string pointer to the SNS algo/driver DSO file path */
  const char    *full_file_path;
  /**< -- A string pointer any diagnostic string. 
    Example: ouptut of dlerror() can be used here */
  const char    *err_str;
  /**< -- Load attributes associated with dynamic loading */
  uint32_t      load_attrib;
  /**< -- A handle to the DSO file */
  const void    *file_handle;
  /**< -- A pointer the UUID associated with a SNS DDF driver */
  const uint8_t *uuid;
  /**< -- A pointer to the SUID associated with a SNS USAM algo */
  sns_sam_sensor_uid const *p_suid;
};
typedef struct sns_dl_info_s sns_dl_info_t;

/**
  @brief Function define that clients can implement to get information
  regarding a DSO based shared object.
*/
typedef void (*pfn_sns_dl_get_info)(sns_dl_info_t *);

#endif  //SNS_DL_DBG_H
