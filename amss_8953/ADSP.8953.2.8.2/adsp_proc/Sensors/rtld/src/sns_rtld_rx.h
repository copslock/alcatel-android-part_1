/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include <sys/mman.h>

// disclaimer
// TODO
// This heap makes the following assumptions
// - single threaded access
// - only callable from big image
// - poor fragmentation management
// - uses general heap to manage nodes

void sns_rtld_rx_walk(void);
void sns_rtld_rx_deinit(void);
int sns_rtld_rx_init(void);
int sns_rtld_rx_malloc(size_t length, void** rx, void** rw);
void sns_rtld_rx_free(void* va);
int sns_rtld_rx_mprotect(void* va, size_t length, int prot, int ctx);

