/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include <sys/mman.h>

#include "sns_memmgr.h"

int sns_rtld_rw_malloc(size_t length, void** rw, void** rw2);
void sns_rtld_rw_free(void* va);
int sns_rtld_rw_mprotect(void* va, size_t length, int prot, int ctx);
