/*==============================================================================
  Copyright (c) 2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "dlfcn.h"
#include "HAP_debug.h"
#include "sigverify.h"

#include "sns_rtld_rx.h"
#include "sns_rtld_rw.h"

char* builtin[] __attribute__((weak)) = {(char*)"libc.so", (char*)"libstdc++.so", (char*)"libgcc.so"};

int sns_rtld_init(void)
{
  DL_vtbl vtbl = {
    sizeof(DL_vtbl),
    HAP_debug_v2,
    SigVerify_start,
    SigVerify_verifyseg,
    SigVerify_stop,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    sns_rtld_rx_malloc,
    sns_rtld_rx_free,
    sns_rtld_rx_mprotect,
    sns_rtld_rw_malloc,
    sns_rtld_rw_free,
    sns_rtld_rw_mprotect,
  };

  if (1 != dlinitex(3, builtin, &vtbl)) {
    return 1;
  }
  return 0;
}

void sns_rtld_deinit(void)
{
  return;
}
