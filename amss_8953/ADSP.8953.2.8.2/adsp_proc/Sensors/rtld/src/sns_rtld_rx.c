/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

// disclaimer - see header file for details

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

#ifndef FARF_ERROR
#define FARF_ERROR 1
#endif
#include "HAP_farf.h"

#include "qurt_types.h"
#include "qurt_memory.h"

#include "qlist.h"

#define FREEIF(p) do { if (p) free((void*)(p)), (p) = 0; } while(0)

// TODO must be ISLAND when released to PW
#define SNS_RTLD_RX_POOL "QURTOS_ISLAND_POOL"
//#define SNS_RTLD_RX_POOL "DEFAULT_PHYSPOOL"
#define SNS_RTLD_RX_ALIGNMENT 4
#define SNS_RTLD_RX_BUF_SIZE (16 * 0x1000)

#define round_down(a, x)   ((x) & ~((a) - 1))
#define round_up(a, x) round_down(a, (x) + (a) - 1)

static int sns_rtld_is_init = 0; // is this heap initialized?
static int sns_rtld_is_mapped = 0; // is the rw region mapped?
static qurt_mem_region_t sns_rtld_rx_reg = 0; // rx region
static qurt_addr_t sns_rtld_rx_va = 0; // rx virtual address
static qurt_addr_t sns_rtld_rx_pa = 0; // rx physical address
static qurt_mem_region_t sns_rtld_rw_reg = 0; // rw region
static qurt_addr_t sns_rtld_rw_va = 0; // rw virtual address

typedef struct sns_rtld_rx_qnode {
  QNode qn;
  int size;
  int va;
} sns_rtld_rx_qnode;

static QLIST_DEFINE_INIT(qlfree); // ordered by va
static QLIST_DEFINE_INIT(qlused);

void sns_rtld_rx_walk(void)
{
  #if FARF_HIGH == 1
  QNode* qn;
  int i = 0;
  // look for smallest free node larger then length
  QLIST_FOR_ALL(&qlfree, qn) {
    sns_rtld_rx_qnode* srrqn = (sns_rtld_rx_qnode*)qn;
    FARF(HIGH, "RXHEAP:   walk free %03d 0x%06X %05d", i++, srrqn->va, srrqn->size);
  }
  i = 0;
  // look for smallest free node larger then length
  QLIST_FOR_ALL(&qlused, qn) {
    sns_rtld_rx_qnode* srrqn = (sns_rtld_rx_qnode*)qn;
    FARF(HIGH, "RXHEAP:   walk used %03d 0x%06X %05d", i++, srrqn->va, srrqn->size);
  }
  #endif
}

// TODO untested and unused
void sns_rtld_rx_deinit(void)
{
  QNode* qn = 0;

  while (0 != (qn = QList_Pop(&qlfree))) {
    free(qn);
  }
  while (0 != (qn = QList_Pop(&qlused))) {
    free(qn);
  }
  sns_rtld_rw_va = 0;
  if (sns_rtld_rw_reg) {
    qurt_mem_region_delete(sns_rtld_rw_reg);
    sns_rtld_rw_reg = 0;
  }
  sns_rtld_rx_va = 0;
  sns_rtld_rx_pa = 0;
  if (sns_rtld_rx_reg) {
    qurt_mem_region_delete(sns_rtld_rx_reg);
    sns_rtld_rx_reg = 0;
  }
  sns_rtld_is_init = 0;
}

void sns_rtld_init_cleanup(sns_rtld_rx_qnode* srrqn)
{
  FARF(ERROR, "init failed rx %x rw %x pa %x", sns_rtld_rx_va, sns_rtld_rw_va, sns_rtld_rx_pa);
  if (srrqn) {
    QNode_Dequeue(&srrqn->qn);
    free(srrqn);
  }
  if (sns_rtld_rw_reg) {
    qurt_mem_region_delete(sns_rtld_rw_reg);
    sns_rtld_rw_reg = 0;
  }
  sns_rtld_rx_va = 0;
  sns_rtld_rx_pa = 0;
  if (sns_rtld_rx_reg) {
    qurt_mem_region_delete(sns_rtld_rx_reg);
    sns_rtld_rx_reg = 0;
  }
}

int sns_rtld_rx_init(void)
{
  qurt_mem_pool_t pool;
  qurt_mem_region_attr_t attr = {0};
  sns_rtld_rx_qnode* srrqn = 0;

  if (sns_rtld_is_init) {
    return 0;
  }

  FARF(ERROR, "sns_rtld_rx_init");

  // allocate RX region
  qurt_mem_region_attr_init(&attr);
  qurt_mem_region_attr_set_mapping(&attr, QURT_MEM_MAPPING_VIRTUAL); // TODO use VIRTUAL_RANDOM
  qurt_mem_region_attr_set_perms(&attr, QURT_PERM_READ | QURT_PERM_EXECUTE);
  if (0 != qurt_mem_pool_attach(SNS_RTLD_RX_POOL, &pool)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  if (0 != qurt_mem_region_create(&sns_rtld_rx_reg,
                                            SNS_RTLD_RX_BUF_SIZE, pool, &attr)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  if (0 != qurt_mem_region_attr_get(sns_rtld_rx_reg, &attr)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  qurt_mem_region_attr_get_virtaddr(&attr, (qurt_addr_t*)&sns_rtld_rx_va);
  qurt_mem_region_attr_get_physaddr(&attr, &sns_rtld_rx_pa);

  // create RW region used for loading
  // TODO don't keep this region around
  qurt_mem_region_attr_init(&attr);
  qurt_mem_region_attr_set_mapping(&attr, QURT_MEM_MAPPING_NONE);
  if (0 != qurt_mem_pool_attach(SNS_RTLD_RX_POOL, &pool)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  if (0 != qurt_mem_region_create(&sns_rtld_rw_reg,
                                  SNS_RTLD_RX_BUF_SIZE, pool, &attr)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  if (0 != qurt_mem_region_attr_get(sns_rtld_rw_reg, &attr)) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  qurt_mem_region_attr_get_virtaddr(&attr, (qurt_addr_t*)&sns_rtld_rw_va);

  // setup heap structures
  if ( 0 == (srrqn = (sns_rtld_rx_qnode*)malloc(sizeof(sns_rtld_rx_qnode)))) {
    sns_rtld_init_cleanup(srrqn);
    return 1;
  }
  srrqn->size = SNS_RTLD_RX_BUF_SIZE;
  srrqn->va = (int)sns_rtld_rx_va;
  QList_AppendNode(&qlfree, &srrqn->qn);

  // init done
  sns_rtld_is_init = 1;
  FARF(ERROR, "init success rx %x rw %x pa %x", sns_rtld_rx_va, sns_rtld_rw_va, sns_rtld_rx_pa);

  return 0;
}

static void sns_rtld_rx_remove_mapping(void)
{
  if (sns_rtld_is_mapped) {
  qurt_mapping_remove(sns_rtld_rw_va, sns_rtld_rx_pa, SNS_RTLD_RX_BUF_SIZE);
    sns_rtld_is_mapped = 0;
  }
}

static int sns_rtld_rx_create_mapping(void)
{
  int nErr = 0;

  if (!sns_rtld_is_mapped) {
    nErr = qurt_mapping_create(sns_rtld_rw_va, sns_rtld_rx_pa,
                             SNS_RTLD_RX_BUF_SIZE,
                             QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE,
                             QURT_PERM_READ | QURT_PERM_WRITE);
    sns_rtld_is_mapped = 1;
  }
  return nErr;
}

// must pass rx va
void sns_rtld_rx_free(void* va)
{
  QNode* qn;
  sns_rtld_rx_qnode* srrqnused = 0;

  // look for matching used node
  QLIST_FOR_ALL(&qlused, qn) {
    srrqnused = (sns_rtld_rx_qnode*)qn;
    if (va == (void*)srrqnused->va) {
      break; // found it
    }
  }
  if (0 == srrqnused) {
    FARF(ERROR, "uimg rx heap: invalid ptr %p passed to free", va);
    return; // va not found
  }
  // remove from used list
  QNode_DequeueZ(&srrqnused->qn);

  {
    sns_rtld_rx_qnode* srrqnfree = 0;

    if (!QList_IsEmpty(&qlfree)) { 
      QLIST_FOR_ALL(&qlfree, qn) {
        srrqnfree = (sns_rtld_rx_qnode*)qn;
        if (srrqnused->va < srrqnfree->va) {
          QNode_InsPrev(&srrqnfree->qn, &srrqnused->qn);
          break;
        }
        srrqnfree = 0;
      }
    }
    if (0 == srrqnfree) {
      // larger then all nodes or empty list, append to end
      QList_AppendNode(&qlfree, &srrqnused->qn);
    }
  }
  // try and consolidate free nodes
  {
    QNode* qnnext = 0;
    sns_rtld_rx_qnode* srrqn = 0;
    sns_rtld_rx_qnode* srrqnnext = 0;
    QLIST_NEXTSAFE_FOR_ALL(&qlfree, qn, qnnext) {
      srrqn = (sns_rtld_rx_qnode*)qn;
      srrqnnext = (sns_rtld_rx_qnode*)qnnext;
      if (qnnext && srrqn->va + srrqn->size == srrqnnext->va) {
        srrqnnext->va = srrqn->va;
        srrqnnext->size += srrqn->size;
        QNode_Dequeue(qn);
        free(qn);
      }
    }
  }
  sns_rtld_rx_remove_mapping();
}

int sns_rtld_rx_malloc(size_t length, void** rx, void** rw)
{
  QNode* qn;
  sns_rtld_rx_qnode* srrqnfree = 0;
  sns_rtld_rx_qnode* srrqnused = 0;
  unsigned int smallest = -1;
  
  if (0 != sns_rtld_rx_init()) {
    return 1;
  }

  if (0 == length) {
    return 1; // zero length not allowed
  }
  length = round_up(SNS_RTLD_RX_ALIGNMENT, length); // aligned va's only

  // look for smallest free node larger then length
  QLIST_FOR_ALL(&qlfree, qn) {
    sns_rtld_rx_qnode* srrqn = (sns_rtld_rx_qnode*)qn;
    if (srrqn->size >= length && srrqn->size < smallest) {
      smallest = srrqn->size;
      srrqnfree = srrqn;
    }
  }
  if (0 == srrqnfree) {
    FARF(ERROR, "uimg rx heap: failed to alloc %d bytes", length);
    return 1; // no free blocks big enough
  }

  if (srrqnfree->size == length) {
    // request equals block length, use it
    QNode_Dequeue(&srrqnfree->qn);
    QList_AppendNode(&qlused, &srrqnfree->qn);
    srrqnused = srrqnfree;
  } else {
    // cut off length bytes from end of block into new node
    srrqnused = (sns_rtld_rx_qnode*)malloc(sizeof(sns_rtld_rx_qnode));
    if (!srrqnused) {
     return 0;
    }
    srrqnused->size = length;
    srrqnused->va = srrqnfree->va;
    srrqnfree->size -= length;
    srrqnfree->va += length;
    QList_AppendNode(&qlused, &srrqnused->qn);
  }

  if (srrqnused->va & 0x3) {
    FARF(ERROR, "Unaligned rx malloc 0x%X %d\n", srrqnused->va, length);
    return 1;
  }

  if (0 != sns_rtld_rx_create_mapping()) {
    sns_rtld_rx_free((void*)srrqnused->va);
    return 1;
  }

  *rx = (void*)srrqnused->va;
  *rw = (void*)(sns_rtld_rw_va + (srrqnused->va - sns_rtld_rx_va));

  return 0;
}

int sns_rtld_rx_mprotect(void* va, size_t length, int prot, int ctx)
{
  qurt_mem_cache_clean(sns_rtld_rw_va, SNS_RTLD_RX_BUF_SIZE, QURT_MEM_CACHE_FLUSH, QURT_MEM_DCACHE);
  sns_rtld_rx_remove_mapping();
  qurt_mem_cache_clean(sns_rtld_rx_va, SNS_RTLD_RX_BUF_SIZE, QURT_MEM_CACHE_INVALIDATE, QURT_MEM_ICACHE);
  return 0;
}

