/*
 * Copyright (c) 2016, ams AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when                 who              what, where, why
------------    --------      --------------------------------------------------
28/04/2016      Byron Shi      Make tmd2725 driver for Qualcomm aDSP
08/05/2016      Byron Shi      ALS use interrupt
09/05/2016      Byron Shi      Add NV params, ALS SELF TEST, get_data() support
13/06/2016      Byron Shi      Add ALS polling in DRI mode, update Prox algorithm
23/06/2016      Byron Shi      Add Prox SELF TEST, checklist for QCOM, update BSD header
16/08/2016      Byron Shi      Support OpenSSC v3.4/v3.5
05/09/2016      Byron Shi      Support Data log Packets

==============================================================================*/

#ifndef __DD_TMD2725_H
#define __DD_TMD2725_H

#include "DDITlmm.h"
#include "DALDeviceId.h"

/* Enable this to see driver debug messages */
#define TMD2725_DD_DEBUG
/* Enable this to support uimg */
//#define TMD2725_CONFIG_ENABLE_UIMAGE
/* Enable als polling in dri mode */
//#define TMD2725_ALS_POLLING_IN_DRI

#define TMD2725_LDO_GPIO_CONTROL

#ifdef TMD2725_DD_DEBUG

	#define DBG_MEDIUM_PRIO                           DBG_MED_PRIO
	#define TMD2725_DD_MSG_0(level,msg)               MSG(MSG_SSID_SNS,   DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">")
	#define TMD2725_DD_MSG_1(level,msg,p1)            MSG_1(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1)
	#define TMD2725_DD_MSG_2(level,msg,p1,p2)         MSG_2(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1,p2)
	#define TMD2725_DD_MSG_3(level,msg,p1,p2,p3)      MSG_3(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1,p2,p3)

	#define TMD2725_DD_UIMG_MSG_0(level,msg)          UMSG(MSG_SSID_SNS,   DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">")
	#define TMD2725_DD_UIMG_MSG_1(level,msg,p1)       UMSG_1(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1)
	#define TMD2725_DD_UIMG_MSG_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1,p2)
	#define TMD2725_DD_UIMG_MSG_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_SNS, DBG_##level##_PRIO, "DD:TMD2725: - <" msg ">",p1,p2,p3)

#else /* TMD2725_DD_DEBUG */
	#define TMD2725_DD_MSG_0(level,msg)
	#define TMD2725_DD_MSG_1(level,msg,p1)
	#define TMD2725_DD_MSG_2(level,msg,p1,p2)
	#define TMD2725_DD_MSG_3(level,msg,p1,p2,p3)

	#define TMD2725_DD_UIMG_MSG_0(level,msg)
	#define TMD2725_DD_UIMG_MSG_1(level,msg,p1)
	#define TMD2725_DD_UIMG_MSG_2(level,msg,p1,p2)
	#define TMD2725_DD_UIMG_MSG_3(level,msg,p1,p2,p3)
#endif /* TMD2725_DD_DEBUG */

#ifndef TMD2725_CONFIG_ENABLE_UIMAGE
#define sns_ddf_malloc_ex(ptr, size, handle)                       sns_ddf_malloc(ptr, size)
#define sns_ddf_memhandler_malloc_ex(mem_handler, size, handle)    sns_ddf_memhandler_malloc(mem_handler, size)
#define sns_ddf_mfree_ex(ptr, handle)                              sns_ddf_mfree(ptr)
#endif

/* Default LUX and Color coefficients */
#define D_Factor                533
#define B_Coef                  136
#define C_Coef                  636
#define D_Coef                  58

/* Attributes for TMD2725 data type */
#define SNS_DD_ALS_RANGE_MAX    32767
#define SNS_DD_ALS_RES          1 /* unit of this data type is lux */
#define SNS_DD_ALS_BITS         16
#define SNS_DD_ALS_FREQ         50
#define SNS_DD_ALS_PWR          70 /* unit of uA */
#define SNS_DD_ALS_LO_PWR       0.7 /* unit of uA */

/* Attributes for TMD2725 data type */
#define SNS_DD_PROX_RANGE_MAX   0.05 /* unit of this data type is meter */
#define SNS_DD_PROX_RES         1
#define SNS_DD_PROX_BITS        8
#define SNS_DD_PROX_FREQ        50
#define SNS_DD_PROX_PWR         145 /* unit of uA */
#define SNS_DD_PROX_LO_PWR      0.7 /* unit of uA */

#define SNS_DD_PROX_THRESH_VERY_NEAR     250 /* unit of ADC count */
#define SNS_DD_PROX_THRESH_CONTAMINATED  150 /* unit of ADC count */

#define SNS_DD_PROX_THRESH_NEAR          80 /* unit of ADC count */
#define SNS_DD_PROX_THRESH_FAR           50 /* unit of ADC count */

/* Constants */
/* Register definitions */
#define TMD2725_ENABLE_REG      0x80 /* bit3 WEN, bit2 PEN, bit1 AEN, bit0 PON */
#define TMD2725_ATIME_REG       0x81 /* 2.8ms */
#define TMD2725_PRATE_REG       0x82 /* 88us */
#define TMD2725_WTIME_REG       0x83 /* 2.8ms, and 2.8ms*12 if WLONG */
#define TMD2725_AILTL_REG       0x84
#define TMD2725_AILTH_REG       0x85
#define TMD2725_AIHTL_REG       0x86
#define TMD2725_AIHTH_REG       0x87
#define TMD2725_PILT_REG        0x88
#define TMD2725_PIHT_REG        0x8A
#define TMD2725_PERS_REG        0x8C /* bit7:4 PPERS, bit3:0 APERS */
#define TMD2725_CFG0_REG        0x8D /* bit7:3 must be set to 10000b, bit2 WLONG */
#define	TMD2725_PCFG0_REG       0x8E /* bit7:6 PULSE LEN, bit5:0 PPLUSES */
#define	TMD2725_PCFG1_REG       0x8F /* bit7:6 PGAIN, bit4:0 PLDRIVE 6mA */
#define TMD2725_CFG1_REG        0x90 /* bit1:0 AGAIN */
#define	TMD2725_REVID_REG       0x91
#define	TMD2725_ID_REG          0x92
#define	TMD2725_STATUS_REG      0x93 /* ASAT|PSAT|PINT|AINT|CINT|RESERVED|PSAT_RELECTIVE|PSAT_AMBIENT */
#define TMD2725_PHOTOPICL_REG   0x94
#define TMD2725_PHOTOPICH_REG   0x95
#define TMD2725_ALS_IRL_REG     0X96
#define TMD2725_ALS_IRH_REG     0X97
#define	TMD2725_PDATA_REG       0x9C
#define	TMD2725_CFG2_REG        0x9F /* bit2 AGAINL */
#define	TMD2725_CFG3_REG        0xAB /* bit7 INT_READ_CLEAR, bit6 ALS, bit4 SAI */
#define	TMD2725_POFFSETL_REG    0xC0
#define	TMD2725_POFFSETH_REG    0xC1
#define	TMD2725_CALIB_REG       0xD7 /* bit5 ELEC_CALIB, bit1 START_CALIB */
#define	TMD2725_CALIBCFG_REG    0xD9 /* bit7:5 BINSRCH_TARGET, bit3 AUTO_OFFSET_ADJ, bit2:0 PROX_AVG */
#define	TMD2725_CALIBSTAT_REG   0xDC /* bit0 CALIB_FINISHED */
#define	TMD2725_INTENAB_REG     0xDD /* ASIEN|PSIEN|PIEN|AIEN|CIEN|RESERVE|RESERVE|RESERVE */
#define	TMD2725_POFFSET_MAG_REG 0xE6
#define	TMD2725_POFFSET_SIGN_REG 0xE7

enum tmd2725_enable_reg {
	PON                    = (1 << 0),
	AEN                    = (1 << 1),
	PEN                    = (1 << 2),
	WEN                    = (1 << 3),
};

enum tmd2725_pers_reg {
	APERS                  = (0x0F),
	PPERS                  = (0xF0),
};

enum tmd2725_cfg0_reg {
	WLONG                  = (1 << 2),
};

enum tmd2725_pcfg0_reg {
	PPLEN_4US              = (0 << 6),
	PPLEN_8US              = (1 << 6),
	PPLEN_16US             = (2 << 6),
	PPLEN_32US             = (3 << 6),
};

enum tmd2725_pcfg1_reg {
	PGAIN_1                = (0 << 6),
	PGAIN_2                = (1 << 6),
	PGAIN_4                = (2 << 6),
	PGAIN_8                = (3 << 6),
};

enum tmd2725_cfg1_reg {
	AGAIN_1                = (0 << 0),
	AGAIN_4                = (1 << 0),
	AGAIN_16               = (2 << 0),
	AGAIN_64               = (3 << 0),
};

enum tmd2725_status_reg {
	ASAT                   = (1 << 7),
	PSAT                   = (1 << 6),
	PINT                   = (1 << 5),
	AINT                   = (1 << 4),
	CINT                   = (1 << 3),
	PSAT_REFLECTIVE        = (1 << 1),
	PSAT_AMBIENT           = (1 << 0),
};

enum tmd2725_cfg2_reg{
	AGAINL                 = (1 << 2),
};

enum tmd2725_cfg3_reg {
	INT_READ_CLEAR         = (1 << 7),
	ALS                    = (1 << 6),
	SAT                    = (1 << 4),
};

enum tmd2725_calib_reg {
	ELEC_CALIB             = (1 << 5),
	START_OFFSET_CALIB     = (1 << 0),
};

enum tmd2725_calibcfg_reg {
	BINSRCH_TARGET_0       = (0 << 5),
	BINSRCH_TARGET_1       = (1 << 5),
	BINSRCH_TARGET_3       = (2 << 5),
	BINSRCH_TARGET_7       = (3 << 5),
	BINSRCH_TARGET_15      = (4 << 5),
	BINSRCH_TARGET_31      = (5 << 5),
	BINSRCH_TARGET_63      = (6 << 5),
	BINSRCH_TARGET_127     = (7 << 5),
	AUTO_OFFSET_ADJ        = (1 << 3),
	PROX_DATA_AVG_DISABLE  = (0 << 0),
	PROX_DATA_AVG_2        = (1 << 0),
	PROX_DATA_AVG_4        = (2 << 0),
	PROX_DATA_AVG_8        = (3 << 0),
	PROX_DATA_AVG_16       = (4 << 0),
	PROX_DATA_AVG_32       = (5 << 0),
	PROX_DATA_AVG_64       = (6 << 0),
	PROX_DATA_AVG_128      = (7 << 0),
};

enum tmd2725_calibstat_reg {
	CALIB_FINISHED         = (1 << 0),
};

enum tmd2725_intenab_reg {
	ASIEN                  = (1 << 7),
	PSIEN                  = (1 << 6),
	PIEN                   = (1 << 5),
	AIEN                   = (1 << 4),
	CIEN                   = (1 << 3),
};

/* Error codes indicating reasons for test failures */
typedef enum {
	SNS_DD_TMD2725_SUCCESS         = 0,
	SNS_DD_TMD2725_CAL_FAILED_LUX  = 1, /* CAL error - Received lux value out of range */
	SNS_DD_TMD2725_CAL_FAILED_RST  = 2, /* CAL error - cannot reset the device */
	SNS_DD_TMD2725_CAL_FAILED_REG  = 3, /* CAL error - cannot write back to registry */
	SNS_DD_TMD2725_CAL_FAILED_ENB  = 4, /* CAL error - cannot enable (error in set power state) */
	SNS_DD_TMD2725_CAL_FAILED_PROX = 5  /* CAL error - Proximity calibration out of range */
} sns_dd_test_err_e;

#define ATIME_MS(ms)            (uint8_t)((ms*100 + 140)/280 - 1)
#define PTIME_US(us)            (uint8_t)((us + 44)/88 - 1)
#define WTIME_MS(ms)            (uint8_t)((ms*100 + 140)/280 - 1)
#define ALS_PERSIST(p)          (((p) & 0xf) << 0)
#define PROX_PERSIST(p)         (((p) & 0xf) << 4)
#define PROX_PULSE_CNT(c)       (((c-1) & 0x3f) << 0)
#define PROX_LDRIVE_MA(ma)      (uint8_t)((ma + 3)/6 - 1)
#define	TMD2725_ATIME_PER_100   280
#define TMD2725_PTIME_PER_1000  88
#define TMD2725_WTIME_PER_100   280
#define MAX_ALS_VALUE           0xffff
#define	MIN_ALS_VALUE           5
#define	GAIN_SWITCH_LEVEL       100

/* For lux calibration */
#define AMS_MAGIC_NUM           11
#define CALIB_LUX_LOW           250
#define CALIB_LUX_HIGH          350

#define DELAY_MS_AFTER_PON      5

/* Timer Arg definitions */
#define	CALIB_TIMER_ARG         0
#define	POLLING_TIMER_ARG       1

#ifdef TMD2725_LDO_GPIO_CONTROL
#define TMD2725_LDO_GPIO             8
#endif

typedef struct {
	int32_t  raw;
	int32_t  detected;
} tmd2725_prox_info_s;

typedef struct {
	uint32_t cpl;
	uint32_t saturation;
	uint16_t deltaP;
	uint16_t lux_scale_1000;
	uint32_t photopic;
	uint32_t ir;
	uint32_t mlux;
	uint32_t lux;
	uint32_t avg_lux;
} tmd2725_als_info_s;

typedef struct {
	uint8_t  als_time;
	uint8_t  prox_time;
	uint8_t  wait_time;
	uint16_t als_th_low;
	uint16_t als_th_high;
	uint8_t  prox_th_low;
	uint8_t  prox_th_high;
	uint8_t  prox_th_contaminated;
	uint8_t  prox_th_verynear;
	uint8_t  persistence;
	uint8_t  cfg0;
	uint8_t  prox_cfg0;
	uint8_t  prox_cfg1;
	uint8_t  cfg1;
	uint8_t  cfg3;
} tmd2725_parameters_s;

/* data structure for NV items */
typedef struct {
	uint8_t  visible_ratio;   /* visible light transmission ratio of the glass/pipe in % */
	uint8_t  ir_ratio;        /* IR transmission ratio of light glass/pipe in % */
	uint16_t dc_offset;       /* DC offset in ADC count */
	uint16_t thresh_near;     /* near detection threshold in ADC count adjusted by DC offset */
	uint16_t thresh_far;      /* far detection threshold in ADC count adjusted by DC offset */
	uint16_t prox_factor;     /* PROX multiplicative factor in % */
	uint16_t als_factor;      /* ALS multiplicative factor in % */

	uint8_t  magic;           /* unique number for ams */
} tmd2725_nv_db_s;

typedef struct {
	tmd2725_parameters_s     params;
	tmd2725_als_info_s       als_inf;
	tmd2725_prox_info_s      prox_inf;
	bool                     als_gain_auto;
	bool                     als_enabled;
	bool                     prox_enabled;
} tmd2725_hub_chip_s;

typedef struct {
	sns_ddf_handle_t         smgr_handle; /* SMGR handle */               
	sns_ddf_handle_t         port_handle; /* Port handle for bus access */
	sns_ddf_timer_s          calib_timer; /* timer obj */
	sns_ddf_sensor_e         calib_timer_arg;
	sns_ddf_timer_s          polling_timer; /* timer obj */
	sns_ddf_sensor_e         polling_timer_arg;
	uint32_t                 gpio1; /* GPIO for interrupt */
	sns_ddf_odr_t            prox_odr; /* Sensor ODR setting */
	sns_ddf_odr_t            als_odr; /* Sensor ODR setting */
	sns_ddf_powerstate_e     powerstate;
	sns_ddf_sensor_data_s*   s_data;
	sns_ddf_sensor_sample_s* samples;
	bool                     dri_enabled;
	tmd2725_hub_chip_s       chip;

	tmd2725_nv_db_s          nv_db;
	uint16_t                 nv_db_size;
	
	#ifdef TMD2725_LDO_GPIO_CONTROL
	DalDeviceHandle     *gpio_handle;
	uint32_t            gpio_ldo_pin_cfg;
	#endif
} sns_dd_tmd2725_state_t;

#endif // __DD_TMD2725_H
