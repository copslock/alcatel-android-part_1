/******************** (C) COPYRIGHT 2014 STMicroelectronics ********************
 *
 * File Name         : sns_dd_accel_lis2hh.h
 * Authors           : Karimuddin Sayed
 * Version           : V 3.0.0
 * Date              : 04/02/2015
 * Description       : LIS2HH Accelerometer driver header file
 *
 ********************************************************************************
 * Copyright (c) 2014, STMicroelectronics.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the STMicroelectronics nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#ifndef __LIS2HH__
#define __LIS2HH__

#include "fixed_point.h"
#include "sns_ddf_attrib.h"
#include "sns_ddf_common.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "sns_ddf_signal.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define DEVICE_NAME "LIS2HH"
//compilation flags
//please define all here
//#ifdef SENSORS_DD_DEV_FLAG
//#define BUILD_DRAGON_BOARD         1
//#endif
#define DEBUG_LOGS_VERBOSE         0
#define STM_LIS2HH_LOGDATA         1
#define DEBUG_DATA                 0
#define DUMP_REGISTER_DATA         1
#define UIMAGE_SUPPORT             0

#if BUILD_DRAGON_BOARD
#include "sns_log_api_public.h"
#include "sns_log_types_public.h"
#include "qurt_elite_diag.h"
//#define sns_dd_acc_lis2hh_if sns_dd_vendor_if_2
//#define SNS_LOG_CONVERTED_SENSOR_DATA    SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC
#else
#include "log_codes.h"
#include "sns_log_types.h"
#include "sns_log_api.h"
#endif
#define SNS_LOG_CONVERTED_SENSOR_DATA    6
#ifndef UIMAGE_SUPPORT
#define sns_dd_lis2hh_malloc(ptr, size, smgr)            sns_ddf_malloc(ptr, size)
#define sns_dd_lis2hh_mfree(ptr, smgr)                   sns_ddf_mfree(ptr)
#define sns_dd_lis2hh_memhandler_malloc(ptr, size, smgr) sns_ddf_memhandler_malloc(ptr, size)

#if (BUILD_DRAGON_BOARD) && (DEBUG_LOGS_VERBOSE)
#define LIS2HH_MSG_0(level,msg)          MSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_1(level,msg,p1)       MSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)    MSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD) && (DEBUG_LOGS_VERBOSE)

#define LIS2HH_MSG_0(level,msg)          MSG(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_1(level,msg,p1)       MSG_1(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)    MSG_2(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!DEBUG_LOGS_VERBOSE)

#define LIS2HH_MSG_0(level,msg)
#define LIS2HH_MSG_1(level,msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3)

#endif
//enable error logs only
#if (BUILD_DRAGON_BOARD)
#define LIS2HH_MSG_E_0(level,msg)          MSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_E_1(level,msg,p1)       MSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_E_2(level,msg,p1,p2)    MSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_E_3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD)
#define LIS2HH_MSG_E_0(level,msg)          MSG(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_E_1(level,msg,p1)       MSG_1(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_E_2(level,msg,p1,p2)    MSG_2(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_E_3(level,msg,p1,p2,p3) MSG_3(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#endif

#if (BUILD_DRAGON_BOARD) && (DEBUG_DATA)
#define LIS2HH_DATA_MSG_0(level,msg)          MSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)       MSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)    MSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD) && (DEBUG_DATA)
#define LIS2HH_DATA_MSG_0(level,msg)          MSG(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)       MSG_1(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)    MSG_2(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)
#else
#define LIS2HH_DATA_MSG_0(level,msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3)
#endif
#else
#define sns_dd_lis2hh_malloc(ptr, size, smgr)            sns_ddf_malloc_ex(ptr, size, smgr)
#define sns_dd_lis2hh_mfree(ptr, smgr)                   sns_ddf_mfree_ex(ptr, smgr)
#define sns_dd_lis2hh_memhandler_malloc(ptr, size, smgr) sns_ddf_memhandler_malloc_ex(ptr, size, smgr)

#if (BUILD_DRAGON_BOARD) && (DEBUG_LOGS_VERBOSE)
#define LIS2HH_MSG_0(level,msg)          UMSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_1(level,msg,p1)       UMSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD) && (DEBUG_LOGS_VERBOSE)

#define LIS2HH_MSG_0(level,msg)          UMSG(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_1(level,msg,p1)       UMSG_1(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!DEBUG_LOGS_VERBOSE)

#define LIS2HH_MSG_0(level,msg)
#define LIS2HH_MSG_1(level,msg,p1)
#define LIS2HH_MSG_2(level,msg,p1,p2)
#define LIS2HH_MSG_3(level,msg,p1,p2,p3)

#endif
//enable error logs only
#if (BUILD_DRAGON_BOARD)
#define LIS2HH_MSG_E_0(level,msg)          UMSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_E_1(level,msg,p1)       UMSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_E_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_E_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD)
#define LIS2HH_MSG_E_0(level,msg)          UMSG(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_MSG_E_1(level,msg,p1)       UMSG_1(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_MSG_E_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_MSG_E_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#endif

#if (BUILD_DRAGON_BOARD) && (DEBUG_DATA)
#define LIS2HH_DATA_MSG_0(level,msg)          UMSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)       UMSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)    UMSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3) UMSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)

#elif (!BUILD_DRAGON_BOARD) && (DEBUG_DATA)
#define LIS2HH_DATA_MSG_0(level,msg)          UMSG(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)       UMSG_1(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)    UMSG_2(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3) UMSG_3(MSG_ID_SNS,DBG_##level##_PRIO, DEVICE_NAME" - "msg,p1,p2,p3)
#else
#define LIS2HH_DATA_MSG_0(level,msg)
#define LIS2HH_DATA_MSG_1(level,msg,p1)
#define LIS2HH_DATA_MSG_2(level,msg,p1,p2)
#define LIS2HH_DATA_MSG_3(level,msg,p1,p2,p3)
#endif
#endif


//#define STM_LIS2HH_LOGDATA

#define STM_LIS2HH_NUM_READ_BYTES  6

/**
 * Accelerometer LIS2HH Full Scales in register setting.
 */
typedef enum
{
  STM_LIS2HH_RANGE_2G   = 0x04,  /*corresponding value in register 4 setting*/
  STM_LIS2HH_RANGE_4G   = 0x24,
  STM_LIS2HH_RANGE_8G   = 0x34
} stm_lis2hh_range;

#define STM_LIS2HH_NUM_AXES   3
#define STM_LIS2HH_RANGE_NUM  3
#define STM_LIS2HH_MAX_RANGE  STM_LIS2HH_RANGE_8G

/**
 * Accelerometer LIS2HH ranges, converted to Q16.
 */
#define STM_LIS2HH_RANGE_2G_MIN    FX_FLTTOFIX_Q16(-2*G)
#define STM_LIS2HH_RANGE_2G_MAX    FX_FLTTOFIX_Q16(2*G)
#define STM_LIS2HH_RANGE_4G_MIN    FX_FLTTOFIX_Q16(-4*G)
#define STM_LIS2HH_RANGE_4G_MAX    FX_FLTTOFIX_Q16(4*G)
#define STM_LIS2HH_RANGE_8G_MIN    FX_FLTTOFIX_Q16(-8*G)
#define STM_LIS2HH_RANGE_8G_MAX    FX_FLTTOFIX_Q16(8*G)

/**
 * Accelerometer LIS2HH sensitivity for each range, converted to ug/digit.
 */
typedef enum
{
  STM_LIS2HH_SSTVT_2G  = 61,
  STM_LIS2HH_SSTVT_4G  = 122,
  STM_LIS2HH_SSTVT_8G  = 244
}stm_lis2hh_sstvt;

/**
 * Accelerometer LIS2HH output data rate in register 1 setting
 */
typedef enum
{
  STM_LIS2HH_ODR_OFF   = 0x00,
  STM_LIS2HH_ODR10     = 0x9F,  /* 10Hz output data rate */
  STM_LIS2HH_ODR50     = 0xAF,  /* 50Hz output data rate */
  STM_LIS2HH_ODR100    = 0xBF,  /* 100Hz output data rate */
  STM_LIS2HH_ODR200    = 0xCF,  /* 200Hz output data rate */
  STM_LIS2HH_ODR400    = 0xDF,  /* 400Hz output data rate */
  STM_LIS2HH_ODR800   = 0xEF   /* 800Hz output data rate */
} stm_lis2hh_odr;


/**
 * Accelerometer LIS2HH Driver State Information Structure
 */
typedef struct {
  /**< Handle used with sns_ddf_notify_data. */
  sns_ddf_handle_t smgr_handle;

  /**< Handle used to access the I2C bus. */
  sns_ddf_handle_t port_handle;

  /**< Current range selection.*/
  int8_t range_idx;

  /**< Current accel sampling frequency. */
  sns_ddf_odr_t cur_rate_idx;

  /**< Current sensitivity. */
  stm_lis2hh_sstvt sstvt_adj;

  /**< the time when new ODR is settled */
  sns_ddf_time_t   odr_settled_ts;

  /**< Current power state: ACTIVE or LOWPOWER */
  sns_ddf_powerstate_e power_state;

  /**< Device-to-phone axis mapping. */
  sns_ddf_axes_map_s axes_map;

  /** GPIO number used for interrupts */
  uint32_t gpio_num;

  /** LIS2HH zero-g biases after inertial SW self test and calibration */
  q16_t biases[STM_LIS2HH_NUM_AXES];

  /** fifo data */
  sns_ddf_sensor_data_s fifo_data;

  /** fifo raw data buffer */
  uint8_t *fifo_raw_buffer;

  /** FIFO enabled or not*/
  bool fifo_enabled;

  /** FIFO and INT enabled or not*/
  bool fifo_int_enabled;

  /** FIFO watermark level*/
  uint16_t fifo_wmk;

  /** last FIFO watermark interrupt timestamp*/
  sns_ddf_time_t last_timestamp;

#if DUMP_REGISTER_DATA
  uint8_t g_value;
#endif

} sns_dd_acc_lis2hh_state_t;


#define STM_LIS2HH_ODR_NUM         6
#define STM_LIS2HH_MAX_ODR         STM_LIS2HH_ODR800
#define STM_LIS2HH_MAX_FIFO        32

/**
 * Accelerometer LIS2HH register address
 */
#define STM_LIS2HH_WHO_AM_I_A      0x0F
#define STM_LIS2HH_CTRL_REG1_A     0x20
#define STM_LIS2HH_CTRL_REG2_A     0x21
#define STM_LIS2HH_CTRL_REG3_A     0x22
#define STM_LIS2HH_CTRL_REG4_A     0x23
#define STM_LIS2HH_CTRL_REG5_A     0x24
#define STM_LIS2HH_CTRL_REG6_A     0x25
#define STM_LIS2HH_CTRL_REG7_A     0x26
#define STM_LIS2HH_STATUS_REG_A    0x27
#define STM_LIS2HH_OUT_X_L_A       0x28
#define STM_LIS2HH_OUT_X_H_A       0x29
#define STM_LIS2HH_OUT_Y_L_A       0x2A
#define STM_LIS2HH_OUT_Y_H_A       0x2B
#define STM_LIS2HH_OUT_Z_L_A       0x2C
#define STM_LIS2HH_OUT_Z_H_A       0x2D
#define STM_LIS2HH_FIFO_MODE       0x2E
#define STM_LIS2HH_FIFO_SRC        0x2F
#define STM_LIS2HH_INT1_CFG        0x30
#define STM_LIS2HH_INT1_SRC        0x31

/**
 * Accelerometer LIS2HH SW inertial self test settings
 */
#define STM_LIS2HH_SWST_TPL_BIAS   40      //Unit: mg
#define STM_LIS2HH_SWST_MAX_BIAS   200     //Unit: mg

#endif  /* __LIS2HH__ */
