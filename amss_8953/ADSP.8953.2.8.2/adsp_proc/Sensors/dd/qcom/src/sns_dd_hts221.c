/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
*
* File Name         : sns_dd_hts221.c
* Authors           : Wei Wang
* Version           : V 1.0.7
* Date              : 04/03/2015
* Description       : HTS221 humidity sensor driver source file
*
********************************************************************************
* Copyright (c) 2015, STMicroelectronics.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of the STMicroelectronics nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXHUMID OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************
* REVISON HISTORY
*
* VERSION | DATE          | DESCRIPTION
* 1.0.7   | 04/07/2015    | Update code for uImage. Add code to support DRI.
* 1.0.6   | 12/17/2014    | Update according to Maansy's suggestion.
* 1.0.5   | 11/07/2014    | Support uImage
* 1.0.4   | 08/20/2014    | Use ODR instead of one-shot.
* 1.0.3   | 08/20/2014    | Minor change.
*                           Build with HD22-NK291-3_2.1.00. Test on BSP 2.1.
*                           Test Self-Test in name of SNS_DDF_SENSOR_HUMIDITY.
* 1.0.2   | 05/30/2014    | Port to KK-2.0-Beta.
*                           Correct bug.
* 1.0.1   | 05/14/2014    | Update sns_dd_hts221_run_test().
* 1.0.0   | 05/07/2014    | Created.
*******************************************************************************/

/*==============================================================================
Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
#include "sns_dd_hts221.h"

extern sns_ddf_driver_if_s sns_dd_hts221_if;

#define HTS221_ODR_NUM          3
static sns_ddf_lowpass_freq_t hts221_lowpass[HTS221_ODR_NUM] =
{
    FX_FLTTOFIX_Q16(1.0/2),    //SampleRate=1Hz
    FX_FLTTOFIX_Q16(7.0/2),    //SampleRate=7Hz
    FX_FLTTOFIX_Q16(12/2)      //SampleRate=12Hz
};
static sns_ddf_odr_t hts221_odr_hz[HTS221_ODR_NUM] =
{
    1,
    7,
    12
};
static uint8_t hts221_odr_code[HTS221_ODR_NUM]={1,2,3};

/** 
 * @brief Resets the driver and device so they return to the state they were
 *        in after init() was called.
 *  
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_reset(sns_ddf_handle_t dd_handle)
{
    sns_dd_hts221_state_t* state = (sns_dd_hts221_state_t*)dd_handle;
    sns_ddf_status_e status;
    uint8_t reg[4] = {0};
    uint8_t rw_bytes;

    reg[0] = 0      //AV_CONF
        | (4<<3)    // AVGT: 0-0.08(C); 1-0.05; 2-0.04; 3-0.03; 4-0.02; 5-0.015; 6-0.01; 7-0.007
        | 4;        // AVGH: 0-0.4(RH%); 1-0.3; 2-0.2; 3-0.15; 4-0.1; 5-0.07; 6-0.05; 7-0.03
    reg[1] = 0x0    //CTRL_REG1
        | (0<<7)    // PD: 0-power-down, 1-normal.
        | (1<<2)    // BDU:
        | 3;        // ODR[1:0]: 0-one shot; 1-1Hz; 2-7Hz; 3-12.5Hz.
    reg[2] = 0      //CTRL_REG2
        | (0<<7)    // BOOT:
        | (0<<1)    // Heater: 0-off; 1-on.
        | 0;        // ONE_SHOT:
    reg[3] = 0      //CTRL_REG3
        | (0<<7)    // DRDY_H_L: 0-active high; 1-active low.
        | (0<<6)    // PP_OD: 0-push-pull; 1-open-drain.
        | (0<<2);   // DRDY Enable: 0-disable; 1-enable.
    status=sns_dd_hts221_write_regs(AUTO_INCREMENT|STM_HTS221_CTRL_REG1, &reg[1], 3);
    if (status!=SNS_DDF_SUCCESS) return status;
    status=sns_dd_hts221_write_regs(STM_HTS221_AV_CONF, &reg[0], 1);
    if (status!=SNS_DDF_SUCCESS) return status;
    state->ctrl_reg1=reg[1];
    state->ctrl_reg3=reg[3];
    state->driver_mode = 0;
    state->dd_humid_odr = state->dd_temp_odr = 0;
    state->sensor_odr_code = reg[1]&0x3;
    sns_ddf_delay(1000);    // wait sensor to power down by delaying 1 ms.
    return SNS_DDF_SUCCESS;
}

static sns_ddf_status_e hts221_check_whoami(sns_dd_hts221_state_t* state)
{
    sns_ddf_status_e status;
    uint8_t reg = 0;
    uint8_t rw_bytes;
    status=sns_dd_hts221_read_regs(STM_HTS221_WHO_AM_I, &reg, 1);
    if (status!=SNS_DDF_SUCCESS)
    {
        DD_MSG_0(ERROR, "Can't open device.");
    }
    else if (reg!=STM_HTS221_WHO_AM_I_VALUE)
    {
        DD_MSG_1(ERROR, "Wrong WhoAmI=%d.", reg);
        status=SNS_DDF_EFAIL;
    }
    return status;
}

static sns_ddf_status_e hts221_set_power(sns_dd_hts221_state_t* state, sns_ddf_powerstate_e power_state)
{
    sns_ddf_status_e status;
    uint8_t rw_bytes = 0;
    if ((SNS_DDF_POWERSTATE_LOWPOWER==power_state)&&(state->driver_mode&HTS221_DRVMODE_bPowerAll))
    {
        state->ctrl_reg1&=0x7F;     // Configure CTRL_REG1 to power-down mode.
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG1, &state->ctrl_reg1, 1);
        if (status!=SNS_DDF_SUCCESS) return status;
    }
    else if ((SNS_DDF_POWERSTATE_ACTIVE==power_state)&&!(state->driver_mode&HTS221_DRVMODE_bPowerAll))
    {
        state->ctrl_reg1|=0x80;     // Configure CTRL_REG1 to active mode.
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG1, &state->ctrl_reg1, 1);
        if (status!=SNS_DDF_SUCCESS) return status;
    }
    return SNS_DDF_SUCCESS;
}

static sns_ddf_status_e hts221_match_odr(sns_ddf_odr_t desired_odr, sns_ddf_odr_t* proposed_odr, int* odr_index)
{
    int i;
    for (i=0; i<HTS221_ODR_NUM; i++) if (desired_odr<=hts221_odr_hz[i]) break;
    if (i>=HTS221_ODR_NUM) i=HTS221_ODR_NUM-1;
    *proposed_odr=hts221_odr_hz[i];
    *odr_index=i;
    return SNS_DDF_SUCCESS;
}

static sns_ddf_status_e hts221_set_odr(sns_dd_hts221_state_t* state, int odr_code)
{
    sns_ddf_status_e status;
    uint8_t rw_bytes;
    if ( (0<odr_code)&&(odr_code<=HTS221_ODR_NUM) )
    {
        state->ctrl_reg1=(state->ctrl_reg1&0xFC)|(odr_code);
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG1, &state->ctrl_reg1, 1);
        if (status!=SNS_DDF_SUCCESS) return status;
        state->sensor_odr_code=odr_code;
        sns_ddf_delay(1000);	// delay 1ms.
    }
    else
    {
        status=SNS_DDF_EINVALID_PARAM;
    }
    return status;
}

static sns_ddf_status_e hts221_set_dri(sns_dd_hts221_state_t* state, int enable)
{
    sns_ddf_status_e status=SNS_DDF_SUCCESS;
    uint8_t rw_bytes;
    if (enable && !(state->ctrl_reg3&0x04))
    {
        state->ctrl_reg3|=0x04;
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG3, &state->ctrl_reg3, 1);
    }
    else if (!enable && (state->ctrl_reg3&0x04) )
    {
        state->ctrl_reg3&=~0x04;
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG3, &state->ctrl_reg3, 1);
    }
    return status;
}

/** 
 * @brief Initializes the hts221 driver and sets up sensor.
 *  
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_init(
    sns_ddf_handle_t* dd_handle_ptr,
    sns_ddf_handle_t smgr_handle,
    sns_ddf_nv_params_s* nv_params,
    sns_ddf_device_access_s device_info[],
    uint32_t num_devices,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_e* sensors[],
    uint32_t* num_sensors)
{
    sns_ddf_status_e status;
    sns_dd_hts221_state_t* state;
    static sns_ddf_sensor_e my_sensors[] = {SNS_DDF_SENSOR_HUMIDITY, SNS_DDF_SENSOR_AMBIENT_TEMP};
    uint8_t reg[16] = {0};
    uint8_t rw_bytes;
    int16_t TX0, TX1, TY0, TY1;
    int16_t HX0, HX1, HY0, HY1;
    
    DD_MSG_0(MED, "Init:");

#if defined(BUILD_UIMAGE)
    /* Update smgr sensor data for the driver to indicate uImage support */
    sns_ddf_smgr_set_uimg_refac(smgr_handle);
#endif

    // Allocate a driver instance.
    status = sns_dd_malloc((void**)&state, sizeof(sns_dd_hts221_state_t), smgr_handle);
    if (status!=SNS_DDF_SUCCESS) return SNS_DDF_ENOMEM;
    memset(state, 0x0, sizeof(sns_dd_hts221_state_t));
    *dd_handle_ptr = state;
    state->smgr_handle = smgr_handle;
    state->gpio_num = device_info->first_gpio;
    state->last_irq_timestamp = 0;
    state->driver_mode = 0;
    state->self_test_enable=false;

     // Open communication port to the device.
    status = sns_ddf_open_port(&state->port_handle, &device_info->port_config);
    if(status != SNS_DDF_SUCCESS)
    {
        DD_MSG_0(ERROR, "Init: can't open i2c.");
        sns_dd_mfree(state, smgr_handle);
        return status;
    }

    // Check WhoAmIRegister to make sure this is the correct driver
    status=hts221_check_whoami(state);
    if (status!=SNS_DDF_SUCCESS)
    {
        sns_ddf_close_port(state->port_handle);
        sns_dd_mfree(state, smgr_handle);
        return status;
    }

    // Resets the HTS221
    status = sns_dd_hts221_reset(state);
    if(status != SNS_DDF_SUCCESS)
    {
        sns_ddf_close_port(state->port_handle);
        sns_dd_mfree(state, smgr_handle);
        DD_MSG_0(ERROR, "Init: can't reset device.");
        return status;
    }

    // Retrieve calibration data.
    status=sns_dd_hts221_read_regs(AUTO_INCREMENT|STM_HTS221_CALIB0, reg, 16);
    TX0=(reg[0xD]<<8)|reg[0xC];
    TX1=(reg[0xF]<<8)|reg[0xE];
    TY0=((reg[5]&0x03)<<8)|reg[2];
    TY1=((reg[5]&0x0C)<<6)|reg[3];
    state->TX1_TX0=TX1-TX0;
    state->TY1_TY0=TY1-TY0;
    state->T1001=TX1*TY0-TX0*TY1;

    HX0=(reg[7]<<8)|reg[6];
    HX1=(reg[0xB]<<8)|reg[0xA];
    HY0=reg[0];
    HY1=reg[1];
    state->HX1_HX0=HX1-HX0;
    state->HY1_HY0=HY1-HY0;
    state->H1001=HX1*HY0-HX0*HY1;

    // Set timer for run_test()
    status = sns_ddf_timer_init(&state->timer_obj,
                                state,
                                &sns_dd_hts221_if,
                                state, //always return dd_handle_ptr
                                false); // not periodic
    if(status != SNS_DDF_SUCCESS)
    {
        sns_ddf_close_port(state->port_handle);
        sns_dd_mfree(state, smgr_handle);
        DD_MSG_0(ERROR, "Init: timer error.");
        return status;
    }

    // Fill out output parameters.
    *num_sensors = 2;
    *sensors = my_sensors;
    DD_MSG_0(MED, "Init: succeed.");
    return SNS_DDF_SUCCESS;
}

/** 
 * @brief Retrieves the value of an attribute for a hts221 sensor.
 *  
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_get_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    sns_ddf_memhandler_s* memhandler,
    void** value,
    uint32_t* num_elems)
{
	sns_dd_hts221_state_t* state;
    if ( (dd_handle==NULL) || (value==NULL) ||
       ((sensor!=SNS_DDF_SENSOR_AMBIENT_TEMP)&&(sensor!=SNS_DDF_SENSOR_HUMIDITY)&&(sensor!=SNS_DDF_SENSOR__ALL)) )
    {
        DD_MSG_0(ERROR, "GetAttr(): invalid parameter.");
        return SNS_DDF_EINVALID_PARAM;
    }
    DD_MSG_2(MED, "GetAttr(S%d, A%d):", sensor, attrib);
    state = (sns_dd_hts221_state_t*)dd_handle;
    switch(attrib)
    {
        case SNS_DDF_ATTRIB_POWER_INFO:
        {
            sns_ddf_power_info_s *power;
            power = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_power_info_s), state->smgr_handle);
            if(NULL == power) return SNS_DDF_ENOMEM;
            power->active_current = 20;     //current consumption, unit uA
            power->lowpower_current = 1;
            *value = power;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_RANGE:
        {
            sns_ddf_range_s *range;
            range = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_range_s), state->smgr_handle);
            if(NULL == range) return SNS_DDF_ENOMEM;
            if (sensor==SNS_DDF_SENSOR_HUMIDITY)
            {
                range->min = STM_HTS221_HUMID_RANGE_MIN;
                range->max = STM_HTS221_HUMID_RANGE_MAX;
            }
            else
            {
                range->min = STM_HTS221_TEMP_RANGE_MIN;
                range->max = STM_HTS221_TEMP_RANGE_MAX;
            }
            *num_elems = 1;
            *value = range;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_RESOLUTION:
        {
            sns_ddf_resolution_t *res;
            res = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_resolution_t), state->smgr_handle);
            if (NULL==res) return SNS_DDF_ENOMEM;
            if (sensor==SNS_DDF_SENSOR_HUMIDITY) *res = STM_HTS221_MAX_RES_HUMID;
            else *res = STM_HTS221_MAX_RES_TEMP;
            *value = res;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_RESOLUTION_ADC:
        {
            sns_ddf_resolution_adc_s *res;
            res = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_resolution_adc_s), state->smgr_handle);
            if(NULL == res) return SNS_DDF_ENOMEM;
            res->bit_len = (sensor==SNS_DDF_SENSOR_HUMIDITY)?16:16;
            res->max_freq = hts221_odr_hz[HTS221_ODR_NUM-1];;
            *value = res;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_LOWPASS:
        {
            *value = hts221_lowpass;
            *num_elems = HTS221_ODR_NUM;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_ODR:
        {
            sns_ddf_odr_t *odr;
            odr = sns_dd_memhandler_malloc(memhandler ,sizeof(sns_ddf_odr_t), state->smgr_handle);
            if (odr==NULL) return SNS_DDF_ENOMEM;
            *odr = (sensor==SNS_DDF_SENSOR_HUMIDITY)? state->dd_humid_odr : state->dd_temp_odr;
            *value = odr;
            *num_elems = 1;
            DD_MSG_1(MED, "\tOdr=%d", *odr);
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_SUPPORTED_ODR_LIST:
        {
            *value = hts221_odr_hz;
            *num_elems = HTS221_ODR_NUM;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_DELAYS:
        {
            sns_ddf_delays_s *hts221_delays;
            hts221_delays = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_delays_s), state->smgr_handle);
            if(NULL == hts221_delays) return SNS_DDF_ENOMEM;
            hts221_delays->time_to_active = 5;
            hts221_delays->time_to_data = 1;
            *value = hts221_delays;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_DRIVER_INFO:
        {
            sns_ddf_driver_info_s *info;
            info = sns_dd_memhandler_malloc(memhandler,sizeof(sns_ddf_driver_info_s), state->smgr_handle);
            if(NULL == info) return SNS_DDF_ENOMEM;
            info->name = "STM HTS221 Driver";
            info->version = 1;
            *value = info;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        case SNS_DDF_ATTRIB_DEVICE_INFO:
        {
            sns_ddf_device_info_s *info;
            info = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_device_info_s), state->smgr_handle);
            if (NULL==info) return SNS_DDF_ENOMEM;
            if (sensor==SNS_DDF_SENSOR_HUMIDITY) info->name = "Hygrometer";
            else info->name = "Thermometer";
            info->vendor = "STMicroelectronics";
            info->model = DEVICE_NAME;
            info->version = 1;
            *value = info;
            *num_elems = 1;
            return SNS_DDF_SUCCESS;
        }
        default:
        {
        	DD_MSG_2(MED, "\tnot supported.", sensor, attrib);
            return SNS_DDF_EINVALID_ATTR;
        }
    }
}

/** 
 * @brief Sets a hts221 sensor attribute to a specific value.
 *  
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_set_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    void* value)
{
    sns_ddf_status_e status=SNS_DDF_SUCCESS;
    sns_dd_hts221_state_t* state;
    if ( (dd_handle==NULL) || (value==NULL) ||
        ((sensor!=SNS_DDF_SENSOR_AMBIENT_TEMP)&&(sensor!=SNS_DDF_SENSOR_HUMIDITY)&&(sensor!=SNS_DDF_SENSOR__ALL)) )
    {
        DD_MSG_2(ERROR, "SetAttr(S%d, A%d): invalid parameter.", sensor, attrib);
        return SNS_DDF_EINVALID_PARAM;
    }
    state = (sns_dd_hts221_state_t*)dd_handle;
    switch(attrib)
    {
        case SNS_DDF_ATTRIB_POWER_STATE:
        {
            sns_ddf_powerstate_e* req_power_state = value;
            int mode_bit, new_mode;
            DD_MSG_3(MED, "SetAttr(S%d, A%d): PowerState=%d", sensor, attrib, *req_power_state);
            if (sensor==SNS_DDF_SENSOR_HUMIDITY) mode_bit=HTS221_DRVMODE_bPowerHumid;
            else if (sensor==SNS_DDF_SENSOR_AMBIENT_TEMP) mode_bit=HTS221_DRVMODE_bPowerTemp;
            else mode_bit=HTS221_DRVMODE_bPowerAll;
            if (*req_power_state==SNS_DDF_POWERSTATE_ACTIVE) new_mode=state->driver_mode|mode_bit;
            else new_mode=state->driver_mode&~mode_bit;
            if ( ((new_mode&HTS221_DRVMODE_bPowerAll)==0) != ((state->driver_mode&HTS221_DRVMODE_bPowerAll)==0))
                status=hts221_set_power(state, *req_power_state);
            state->driver_mode=new_mode;
            return status;
        }
        case SNS_DDF_ATTRIB_RANGE:
        {
            uint8_t range_idx = *(uint8_t*)value;
            if (range_idx!=0) status=SNS_DDF_EINVALID_PARAM;
            DD_MSG_3(MED, "SetAttr(S%d, A%d): RangeIndex=%d.", sensor, attrib, range_idx);
            return status;
        }
        case SNS_DDF_ATTRIB_ODR:
        {
            sns_ddf_odr_t* odr = (sns_ddf_odr_t*)value;
            sns_ddf_odr_t desired_odr, proposed_odr;
            int odr_index;

            desired_odr=*odr;
            if (*odr==0)
            {
                if (sensor==SNS_DDF_SENSOR__ALL) state->dd_humid_odr = state->dd_temp_odr = 0;
                else if (sensor==SNS_DDF_SENSOR_HUMIDITY) state->dd_humid_odr = 0;
                else if (sensor==SNS_DDF_SENSOR_AMBIENT_TEMP) state->dd_temp_odr = 0;
            }
            else
            {
                hts221_match_odr(*odr, &proposed_odr, &odr_index);
                if (sensor==SNS_DDF_SENSOR__ALL)
                {
                    if (hts221_odr_code[odr_index]!=state->sensor_odr_code)
                    {
                        status = hts221_set_odr(state, hts221_odr_code[odr_index]);
                        state->sensor_odr_code = hts221_odr_code[odr_index];
                    }
                    *odr = state->dd_humid_odr = state->dd_temp_odr = proposed_odr;
                }
                else if (sensor==SNS_DDF_SENSOR_HUMIDITY)
                {
                    if (hts221_odr_code[odr_index]!=state->sensor_odr_code)
                    {
                        status = hts221_set_odr(state, hts221_odr_code[odr_index]);
                        state->sensor_odr_code = hts221_odr_code[odr_index];
                        if (state->dd_temp_odr!=0)
                        {
                            state->dd_temp_odr=proposed_odr;
                            sns_ddf_smgr_notify_event(state->smgr_handle, SNS_DDF_SENSOR_AMBIENT_TEMP, SNS_DDF_EVENT_ODR_CHANGED);
                        }
                    }
                    *odr=state->dd_humid_odr=proposed_odr;
                }
                else //(sensor==SNS_DDF_SENSOR_AMBIENT_TEMP)
                {
                    if (state->dd_humid_odr!=0) // not allow TEMP change odr.
                    {
                        *odr=state->dd_temp_odr=state->dd_humid_odr;
                    }
                    else    // state->dd_humid_odr==0: allows TEMP change odr.
                    {
                        if (hts221_odr_code[odr_index]!=state->sensor_odr_code)
                        {
                            status = hts221_set_odr(state, hts221_odr_code[odr_index]);
                            state->sensor_odr_code = hts221_odr_code[odr_index];
                        }
                        *odr=state->dd_temp_odr=proposed_odr;
                    }
                }
            } //*odr!=0;
            DD_MSG_4(MED, "SetAttr(S%d,A%d): DesiredOdr=%d, SetOdr=%d", sensor, attrib, desired_odr, *odr);
            return status;
        }
        default:
        {
        	DD_MSG_2(MED, "SetAttr(S%d, A%d): not supported.", sensor, attrib);
            return SNS_DDF_EINVALID_ATTR;
        }
    }
}

/**
 * @brief Implement enable_sched_data() DDF API.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_enable_sched_data(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    bool              enable)
{
    sns_ddf_status_e status;
    sns_dd_hts221_state_t *state;
    int driver_mode_old;
    uint32_t drdy;
    uint8_t reg[4];
    uint8_t rw_bytes;

    DD_MSG_2(MED, "EnableSchedData(S%d, E%d):", sensor, enable);
    if ((dd_handle==NULL)||((sensor!=SNS_DDF_SENSOR_HUMIDITY)&&(sensor!=SNS_DDF_SENSOR_AMBIENT_TEMP)&&(sensor!=SNS_DDF_SENSOR__ALL)))
    {
        DD_MSG_0(ERROR, "dd_handle is NULL");
        return SNS_DDF_EINVALID_PARAM;
    }

    state=(sns_dd_hts221_state_t*)dd_handle;
    driver_mode_old = state->driver_mode;
    if (sensor==SNS_DDF_SENSOR__ALL)
    {
        if (enable) state->driver_mode|=HTS221_DRVMODE_bScheduleAll;
        else state->driver_mode&=~HTS221_DRVMODE_bScheduleAll;
    }
    else if (sensor==SNS_DDF_SENSOR_HUMIDITY)
    {
        if (enable) state->driver_mode|=HTS221_DRVMODE_bScheduleHumid;
        else state->driver_mode&=~HTS221_DRVMODE_bScheduleHumid;
    }
    else //if (sensor==SNS_DDF_SENSOR_AMBIENT_TEMP)
    {
        if (enable) state->driver_mode|=HTS221_DRVMODE_bScheduleTemp;
        else state->driver_mode&=~HTS221_DRVMODE_bScheduleTemp;
    }
    status=hts221_set_dri(state, (state->driver_mode&HTS221_DRVMODE_bScheduleAll));
    if (status==SNS_DDF_SUCCESS)
    {
        DD_MSG_2(MED, "\tOldMode=%02X, NewMode=%02X", driver_mode_old, state->driver_mode);
        if (!(driver_mode_old&HTS221_DRVMODE_bScheduleAll) && (state->driver_mode&HTS221_DRVMODE_bScheduleAll))
        {
            status = sns_ddf_signal_register(state->gpio_num, state, &sns_dd_hts221_if, SNS_DDF_SIGNAL_IRQ_RISING);
            if (status!=SNS_DDF_SUCCESS) return status;
            status = sns_ddf_signal_gpio_read(state->gpio_num, &drdy);
            if (drdy) status=sns_dd_hts221_read_regs(AUTO_INCREMENT|STM_HTS221_HUMID_OUT_L, reg, 4); // clear existing output buffer.
        }
        else if ((driver_mode_old&HTS221_DRVMODE_bScheduleAll) && !(state->driver_mode&HTS221_DRVMODE_bScheduleAll))
        {
            status = sns_ddf_signal_deregister(state->gpio_num);
        }
    }
    return status;
}

/** 
 * @brief Probe HTS221.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_probe(
    sns_ddf_device_access_s* device_info,
    sns_ddf_memhandler_s*    memhandler,
    uint32_t*                num_sensors,
    sns_ddf_sensor_e**       sensors )
{
    sns_ddf_status_e status;
    sns_ddf_handle_t port_handle;
    uint8_t reg = 0;
    uint8_t rw_bytes;

    *num_sensors = 0;
    *sensors = NULL;

    status = sns_ddf_open_port(&port_handle, &(device_info->port_config));
    if(status != SNS_DDF_SUCCESS) return status;

    /* Read & Verify Device ID */
    status = sns_ddf_read_port( port_handle,
                              STM_HTS221_WHO_AM_I,
                              &reg,
                              1,
                              &rw_bytes );
    if (status!=SNS_DDF_SUCCESS || rw_bytes!=1)
    {
        sns_ddf_close_port( port_handle );
        return status;
    }
    if (reg!=STM_HTS221_WHO_AM_I_VALUE)
    {
        /* Incorrect value. Return now with nothing detected */
        sns_ddf_close_port( port_handle );
        return SNS_DDF_SUCCESS;
    }

    /* Registers are correct. This is probably a HTS221 */
    *num_sensors = 2;
    *sensors = sns_ddf_memhandler_malloc( memhandler, sizeof(sns_ddf_sensor_e) * *num_sensors );
    if (*sensors!=NULL)
    {
        (*sensors)[0] = SNS_DDF_SENSOR_HUMIDITY;
        (*sensors)[1] = SNS_DDF_SENSOR_AMBIENT_TEMP;
        status = SNS_DDF_SUCCESS;
    }
    else
    {
        status = SNS_DDF_ENOMEM;
    }
    sns_ddf_close_port(port_handle);
    return status;
}

/**
 * @brief Runs a factory test case.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_run_test
(
    sns_ddf_handle_t    dd_handle,
    sns_ddf_sensor_e    sensor_type,
    sns_ddf_test_e      test,
    uint32_t*           err
)
{
    sns_dd_hts221_state_t* state;
    sns_ddf_status_e status;

    if (dd_handle==NULL)
    {
        DD_MSG_0(ERROR, "RunTest: dd_handle is NULL");
        return SNS_DDF_EINVALID_PARAM;
    }

    DD_MSG_1(MED, "RunTest: %d.", test);
    state=(sns_dd_hts221_state_t*)dd_handle;
    if ((sensor_type!=SNS_DDF_SENSOR_HUMIDITY) && (sensor_type!=SNS_DDF_SENSOR__ALL)) return SNS_DDF_EINVALID_PARAM;
    if (state->driver_mode&HTS221_DRVMODE_bPowerAll) return SNS_DDF_EDEVICE_BUSY;
    if (test==SNS_DDF_TEST_SELF)
    {
        uint8_t reg = 0;
        uint8_t rw_bytes;
        status=sns_dd_hts221_read_regs(STM_HTS221_WHO_AM_I, &reg, 1);
        if (status!=SNS_DDF_SUCCESS) return status;
        if (reg!=STM_HTS221_WHO_AM_I_VALUE) return SNS_DDF_EFAIL;
        return SNS_DDF_SUCCESS;
    }
    else if (test==SNS_DDF_TEST_SELF_SW)
    {
        state->self_test_enable=true;
        // Start a timer for few ms here
        status = sns_ddf_timer_start(state->timer_obj, 10000);
        if(status != SNS_DDF_SUCCESS) return status;
        // Return pending
        return SNS_DDF_PENDING;
    }
    else
    {
    	DD_MSG_0(MED, "Test not supported.");
        return SNS_DDF_EINVALID_TEST;
    }
}

void sns_dd_hts221_handle_timer(sns_ddf_handle_t dd_handle, void* arg)
{
    sns_dd_hts221_state_t* state;
    sns_ddf_status_e status;
    uint8_t ctrlreg1;
    uint8_t rw_bytes;
    q16_t humid[10], tmpt[10];
    int i, dup_count_h, dup_count_t;
    uint32_t err;

    if (dd_handle==NULL)
    {
        DD_MSG_0(ERROR, "HandleTimer: dd_handle is NULL");
        return;
    }
    state=(sns_dd_hts221_state_t*)dd_handle;
    if (state->self_test_enable)
    {
        state->self_test_enable=false;
        err=STMERR_TEST_OK;

        // 1. Set HTS221 to active mode, start sampling at 12.5Hz.
        DD_MSG_0(MED, "1. Set sensor active.");
        ctrlreg1=0x87; //PD=1; BDU=1; ODR=12.5Hz
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG1, &ctrlreg1, 1);
        if (status!=SNS_DDF_SUCCESS)
        {
            err=STMERR_TEST_BUS_ERROR;
            sns_ddf_smgr_notify_test_complete(state->smgr_handle,
                        SNS_DDF_SENSOR_HUMIDITY,
                        status, err);
            return;
        }

        // 2. Collect 10 samples for both humidity and temperature
        DD_MSG_0(MED, "2. Collect 10 samples.");
        for (i=0; i<10; i++)
        {
            sns_ddf_delay(100000);    // Wait 100ms for one sample ready.
            status=hts221_get_ht(state, &humid[i], &tmpt[i]);
            if (status!=SNS_DDF_SUCCESS)
            {
                err=STMERR_TEST_BUS_ERROR;
                sns_ddf_smgr_notify_test_complete(state->smgr_handle,
                   SNS_DDF_SENSOR_HUMIDITY, status, err);
                return;
            }
            DD_MSG_4(MED, "RH=%d.%d%%, T=%d.%dC", Q16INT(humid[i]), Q16DEC2(humid[i]), Q16INT(tmpt[i]), Q16DEC2(tmpt[i]));
        }

        // 3. Analyze data.
        DD_MSG_0(MED, "3. Analyze data.");
        dup_count_h=dup_count_t=0;
        DD_MSG_2(MED, "RH: ST MIN=%d, MAX=%d",Q16INT(STM_HTS221_HUMID_ST_MIN), Q16INT(STM_HTS221_HUMID_ST_MAX));
        DD_MSG_2(MED, "T: ST MIN=%d, MAX=%d",Q16INT(STM_HTS221_TEMP_ST_MIN), Q16INT(STM_HTS221_TEMP_ST_MAX));
        for (i=0;i<10;i++)
        {
            if ((humid[i]<STM_HTS221_HUMID_ST_MIN)||(humid[i]>STM_HTS221_HUMID_ST_MAX))
                err|=STMERR_TEST_SENSOR1_ST_FAIL;
            if ( (i>0) && (humid[i]==humid[i-1]) ) dup_count_h++;
            if ((tmpt[i]<STM_HTS221_TEMP_ST_MIN)||(tmpt[i]>STM_HTS221_TEMP_ST_MAX))
                err|=STMERR_TEST_SENSOR2_ST_FAIL;
            if ( (i>0) && (tmpt[i]==tmpt[i-1]) ) dup_count_t++;
        }
        if (dup_count_h>5) err|=STMERR_TEST_SENSOR1_ST_FAIL;
        if (dup_count_t>5) err|=STMERR_TEST_SENSOR2_ST_FAIL;

        // 4. restore CTRL_REG1 and put sensor power down.
        DD_MSG_0(MED, "4. Set sensor power down.");
        status=sns_dd_hts221_write_regs(STM_HTS221_CTRL_REG1, &state->ctrl_reg1, 1);
        if (status!=SNS_DDF_SUCCESS)
        {
            err=STMERR_TEST_BUS_ERROR;
            sns_ddf_smgr_notify_test_complete(state->smgr_handle,
                    SNS_DDF_SENSOR_HUMIDITY,
                    status, err);
            return;
        }

        if (err!=STMERR_TEST_OK) status = SNS_DDF_EFAIL;
        sns_ddf_smgr_notify_test_complete(state->smgr_handle,
                    SNS_DDF_SENSOR_HUMIDITY,
                    status, err);
    }
    return;
}



