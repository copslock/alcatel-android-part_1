/*==============================================================================

    S E N S O R S   AMBIENT LIGHT AND PROXIMITY  D R I V E R

DESCRIPTION

   Implements the driver for ROHM ALS (Light) and Proximity Sensor RPR0521
   This driver has 3 sub-modules:
   1. The common handler that provides the driver interface
   2. The light(ALS) driver that handles light data type
   3. The proximity driver that handles proximity data type

********************************************************************************
* Copyright (c) 2012, ROHM Semiconductor.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of ROHM nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************

==============================================================================*/
/*-----------------------------------------------------------------------------
 * Copyright (c) 2012 - 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
  -----------------------------------------------------------------------------*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/core/pkg/dsps/rel/2.2/adsp_proc/Sensors/dd/qcom/src/sns_dd_rpr0521.c#9 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
24-Mar-2014   am      First Draft
==============================================================================*/

/*============================================================================
                                INCLUDE FILES
============================================================================*/
#include "sns_ddf_attrib.h"
#include "sns_ddf_common.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_signal.h"
#include "sns_dd_rpr0521_priv.h"
#include "sns_debug_str.h"
#include "sns_debug_api.h"
#include "sns_memmgr.h"
#include "log_codes.h"
#include "sns_log_types.h"
#include "sns_log_api.h"

#define CHECK_STATUS(status)        \
  if ( status != SNS_DDF_SUCCESS )  \
  {                                 \
    return status;                  \
  }

/*============================================================================
                            STATIC VARIABLE DEFINITIONS
============================================================================*/
extern volatile boolean EnableI2C;

#define COEFFICIENT               (4)


//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-977279, 2015/12/01,grace modify in 2015.11.27 ,light sensor performance under different light
const unsigned long data0_coefficient[COEFFICIENT] = {9723, 7658, 3186, 3332};
const unsigned long data1_coefficient[COEFFICIENT] = {6156, 4341, 1396,  1459};
const unsigned long judge_coefficient[COEFFICIENT] = {1000,  1400,  1700, 3100};
//[BUFFIX]-Mod-End- by TCTNB.ZXZ

typedef struct {
	unsigned long long data;
	unsigned long long data0;
	unsigned long long data1;
	unsigned char gain_data0;
	unsigned char gain_data1;
	unsigned long dev_unit;
	//unsigned char als_time;
	unsigned short als_time; //grace modify in 2014.4.11
	unsigned short als_data0;
	unsigned short als_data1;
} CALC_DATA;

typedef struct {
	unsigned long positive;
	unsigned long decimal;
} CALC_ANS;


#if 0
/* mode control table */
#define MODE_CTL_FACTOR (16)
static const struct MCTL_TABLE {
    short ALS;
    short PS;
} mode_table[MODE_CTL_FACTOR] = {
    {  0,   0},   /*  0 */
    {  0,  10},   /*  1 */
    {  0,  40},   /*  2 */
    {  0, 100},   /*  3 */
    {  0, 400},   /*  4 */
    {100,  50},   /*  5 */
    {100, 100},   /*  6 */
    {100, 400},   /*  7 */
    {400,   0},   /*  8 */
    {400, 100},   /*  9 */
    {400,   0},   /* 10 */
    {400, 400},   /* 11 */
    {  50,  50},   /* 12 */
    {  0,   0},   /* 13 */
    {  0,   0},   /* 14 */
    {  0,   0}    /* 15 */
};
#endif

/* gain table */
#define GAIN_FACTOR (16)
static const struct GAIN_TABLE {
    char DATA0;
    char DATA1;
} gain_table[GAIN_FACTOR] = {
    {  1,   1},   /*  0 */
    {  1,   2},   /*  1 */
    {  1, 64},   /*  2 */
    {  1,128},   /*  3 */
    {  2,   1},   /*  4 */
    {  2,   2},   /*  5 */
    {  2,  64},   /*  6 */
    {  2,128},   /*  7 */
    { 64,   1},   /*  8 */
    { 64,   2},   /*  9 */
    { 64,  64},   /* 10 */
    //{ 64,  64},   /* 11 */
    { 64,  128},   /* 11 */ //grace modify in 2014.4.11
    {128,   1},   /* 12 */
    {128,   2},   /* 13 */
    {128,  64},   /* 14 */
    {128, 128}    /* 15 */
};


#define   ALSPRX_DDF_TIMER_MARGIN_US   (15000)

static const sns_ddf_sensor_e sns_dd_rpr0521_sensor_types[SNS_DD_RPR0521_NUM_SENSOR_TYPES] =
{
  SNS_DDF_SENSOR_PROXIMITY,
  SNS_DDF_SENSOR_AMBIENT
};

static sns_ddf_sensor_e prx_timer = SNS_DDF_SENSOR_PROXIMITY;
static sns_ddf_sensor_e als_timer = SNS_DDF_SENSOR_AMBIENT;

/*============================================================================
                           STATIC FUNCTION PROTOTYPES
============================================================================*/

static sns_ddf_status_e sns_dd_rpr0521_init
(
  sns_ddf_handle_t*        dd_handle_ptr,
  sns_ddf_handle_t         smgr_handle,
  sns_ddf_nv_params_s*     nv_params,
  sns_ddf_device_access_s  device_info[],
  uint32_t                 num_devices,
  sns_ddf_memhandler_s*    memhandler,
  sns_ddf_sensor_e*        sensors[],
  uint32_t*                num_sensors
);

static sns_ddf_status_e sns_dd_rpr0521_get_data
(
  sns_ddf_handle_t        dd_handle,
  sns_ddf_sensor_e        sensors[],
  uint32_t                num_sensors,
  sns_ddf_memhandler_s*   memhandler,
  sns_ddf_sensor_data_s*  data[] /* ignored by this async driver */
);

static sns_ddf_status_e sns_dd_rpr0521_set_attrib
(
  sns_ddf_handle_t     dd_handle,
  sns_ddf_sensor_e     sensor,
  sns_ddf_attribute_e  attrib,
  void*                value
);

static sns_ddf_status_e sns_dd_rpr0521_get_attrib
(
  sns_ddf_handle_t     dd_handle,
  sns_ddf_sensor_e     sensor,
  sns_ddf_attribute_e  attrib,
  sns_ddf_memhandler_s* memhandler,
  void**               value,
  uint32_t*            num_elems
);

static void sns_dd_rpr0521_handle_timer
(
  sns_ddf_handle_t dd_handle,
  void* arg
);

static void sns_dd_rpr0521_handle_irq
(
  sns_ddf_handle_t dd_handle,
  uint32_t          gpio_num,
  sns_ddf_time_t    timestamp
);

static sns_ddf_status_e sns_dd_rpr0521_reset
(
  sns_ddf_handle_t dd_handle
);

sns_ddf_status_e sns_dd_rpr0521_enable_sched_data
(
  sns_ddf_handle_t  handle,
  sns_ddf_sensor_e  sensor,
  bool              enable
);

static sns_ddf_status_e sns_dd_rpr0521_calibration
(
  sns_ddf_handle_t  dd_handle,
  sns_ddf_sensor_e  sensor,
  sns_ddf_test_e    test,
  uint32_t*         err
);

static sns_ddf_status_e sns_dd_rpr0521_probe
(
 sns_ddf_device_access_s* device_info,
 sns_ddf_memhandler_s*    memhandler,
 uint32_t*                num_sensors,
 sns_ddf_sensor_e**       sensors
);

#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
void sns_dd_rpr0521_stop_general_timer( sns_dd_rpr0521_state_t* dd_ptr, uint32_t sensor_type );
#endif


sns_ddf_driver_if_s sns_dd_rpr0521_driver_fn_list =
{
	.init                 = &sns_dd_rpr0521_init,
	.get_data             = &sns_dd_rpr0521_get_data,
	.set_attrib           = &sns_dd_rpr0521_set_attrib,
	.get_attrib           = &sns_dd_rpr0521_get_attrib,
	.handle_timer         = &sns_dd_rpr0521_handle_timer,
	.handle_irq           = &sns_dd_rpr0521_handle_irq,
	.reset                = &sns_dd_rpr0521_reset,
	.run_test             = &sns_dd_rpr0521_calibration, /* calibration test - ALS and PRX*/
	.enable_sched_data    = &sns_dd_rpr0521_enable_sched_data,
	.probe                = &sns_dd_rpr0521_probe,
	.trigger_fifo_data    = NULL
};

/*===========================================================================
FUNCTION      sns_dd_rpr0521_log

DESCRIPTION   Log the latest sensor data

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None
===========================================================================*/
void sns_dd_rpr0521_log(
   sns_dd_rpr0521_state_t* dd_ptr,
   sns_ddf_sensor_e       sensor_type,
   uint32_t               data1,
   uint32_t               data1_q16,
   uint32_t               data2,
   uint32_t               data2_q16,
   uint32_t               raw_data )
{
   sns_err_code_e err_code;
   sns_log_sensor_data_pkt_s* log_struct_ptr;

   ALSPRX_MSG_0(HIGH, "rpr0521_log\n");
   /* Allocate log packet */
   err_code = sns_logpkt_malloc(SNS_LOG_CONVERTED_SENSOR_DATA,
                                sizeof(sns_log_sensor_data_pkt_s),
                                (void**)&log_struct_ptr);

   if ((err_code == SNS_SUCCESS) && (log_struct_ptr != NULL))
   {
     log_struct_ptr->version = SNS_LOG_SENSOR_DATA_PKT_VERSION;
     log_struct_ptr->sensor_id = sensor_type;
     log_struct_ptr->vendor_id = SNS_DDF_VENDOR_ROHM;

     /* Timestamp the log with sample time */
     log_struct_ptr->timestamp = sns_ddf_get_timestamp();

     /* Log the sensor data */
     if (sensor_type == SNS_DDF_SENSOR_AMBIENT)
     {
       log_struct_ptr->num_data_types = 3;
       log_struct_ptr->data[0] = data1;
       log_struct_ptr->data[1] = data1_q16;
       log_struct_ptr->data[2] = raw_data;
      }
     else
     {
       log_struct_ptr->num_data_types = 5;
       log_struct_ptr->data[0] = data1;
       log_struct_ptr->data[1] = data1_q16;
       log_struct_ptr->data[2] = data2;
       log_struct_ptr->data[3] = data2_q16;
       log_struct_ptr->data[4] = raw_data;
     }

     /* Commit log (also frees up the log packet memory) */
     err_code = sns_logpkt_commit(SNS_LOG_CONVERTED_SENSOR_DATA,
                                  log_struct_ptr);
   }
   else
   {
     dd_ptr->dropped_logs++;
     ALSPRX_MSG_3(ERROR,"log - %d dropped=%d err=%d",
                  11, dd_ptr->dropped_logs, err_code);
   }
}

/*===========================================================================

  FUNCTION:   sns_dd_isl29028_int_disable

===========================================================================*/
/*!
  @brief Disable ISL29028 INT since it shares a pin with our part.

  @detail
  Resets  the driver state structure

  @param[in] handle  Handle to a driver instance.

  @return Success if the driver was able to reset its state and the device.
          Otherwise a specific error code is returned.
*/
/*=========================================================================*/
#ifdef FEATURE_RPR0521_INT_PIN_CONFIG

#define ISL29028_I2C_ADDR 0x45
#define SNS_DD_ISL_ALSPRX_CONFIG_REG         0x01
#define SNS_DD_ISL_ALSPRX_INTERRUPT_REG      0x02
#define SNS_DD_ISL_ALSPRX_PROX_LT_REG        0x03
#define SNS_DD_ISL_ALSPRX_PROX_HT_REG        0x04
#define SNS_DD_ISL_ALSPRX_ALSIR_TH1_REG      0x05
#define SNS_DD_ISL_ALSPRX_ALSIR_TH2_REG      0x06
#define SNS_DD_ISL_ALSPRX_ALSIR_TH3_REG      0x07
#define SNS_DD_ISL_ALSPRX_ALSIR_TEST1_REG    0x0E
#define SNS_DD_ISL_ALSPRX_ALSIR_TEST2_REG    0x0F

typedef struct {
   sns_ddf_handle_t         smgr_handle;    /* SDDI handle used to notify_data */
   uint32_t                 dropped_logs;
   sns_ddf_handle_t         port_handle;    /* handle used to access the I2C bus */
   sns_dd_als_db_type       sns_dd_als_db;
   sns_dd_prx_db_type       sns_dd_prx_db;
   sns_dd_rpr0521_common_db_type sns_dd_alsprx_common_db;
} sns_dd_alsprx_state_t;

sns_ddf_status_e sns_dd_isl29028_int_disable()
{
  sns_ddf_status_e status;
  uint8_t data, out;
  sns_dd_alsprx_state_t* ptr;
  sns_ddf_i2c_config_s    i2c_config;
  sns_ddf_device_access_s device;

  device.device_select = 0;
  device.port_config.bus = SNS_DDF_BUS_I2C;
  device.port_config.bus_config.i2c  = &i2c_config;
  i2c_config.addr_type = SNS_DDF_I2C_ADDR_7BIT;
  i2c_config.bus_acq_timeout = -1;
  i2c_config.bus_freq = 400;
  i2c_config.dev_type = SNS_DDF_I2C_DEVICE_REGADDR;
  i2c_config.read_opt = SNS_DDF_I2C_START_BEFORE_RD;
  i2c_config.slave_addr = ISL29028_I2C_ADDR;
  i2c_config.xfer_timeout = -1;

  status = sns_ddf_malloc((void **)&ptr, sizeof(sns_dd_alsprx_state_t));
  if (status != SNS_DDF_SUCCESS)
  {
    return status;
  }
  status = sns_ddf_open_port(&(ptr->port_handle),
                              &(device.port_config));

  if (status != SNS_DDF_SUCCESS)
  {
    sns_ddf_mfree(ptr);
    return status;
  }

  /* ISL29028a reset sequence */
  data =  0x29;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TEST2_REG,
                     &data, 1, &out);

  data =  0x0;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TEST1_REG,
                     &data, 1, &out);

  data =  0x0;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TEST2_REG,
                     &data, 1, &out);

  data =  0x0;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_CONFIG_REG,
                     &data, 1, &out);

  /* write valid information to INTERRUPT register */
  data =  0x7; //16 conversions for ALS; AND
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_INTERRUPT_REG,
                     &data, 1, &out);

  /* update thresholds so interrupts are not generated */
  data =  0xFF;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_PROX_LT_REG,
                     &data, 1, &out);

  data =  0xFF;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_PROX_HT_REG,
                     &data, 1, &out);

  data =  0xFF;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TH1_REG,
                     &data, 1, &out);

  data =  0xFF;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TH2_REG,
                     &data, 1, &out);

  data =  0xFF;
  sns_ddf_write_port(ptr->port_handle,
                     SNS_DD_ISL_ALSPRX_ALSIR_TH3_REG,
                     &data, 1, &out);

  sns_ddf_close_port(ptr->port_handle);
  sns_ddf_mfree(ptr);
  if (status != SNS_DDF_SUCCESS)
  {
     return status;
  }

  return SNS_DDF_SUCCESS;
}
#endif


/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_prx_thresh

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_set_prx_thresh
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t                 pilt,
  uint16_t                 piht
)
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t          bytes_w;
  uint8_t          thresh[4];

   ALSPRX_MSG_0(HIGH, "rpr0521_set_prx_thresh\n");
  thresh[2] = (pilt & 0xFF); /* PILTL */
  thresh[3] = (pilt >> 8);   /* PILTH */
  thresh[0] = (piht & 0xFF); /* PIHTL */
  thresh[1] = (piht >> 8);   /* PIHTH */

  status = sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_PIHTL_ADDR,
                              (uint8_t*)&thresh[0], 4, &bytes_w);
  CHECK_STATUS(status);

#ifdef ALSPRX_DEBUG
  {
    uint8_t   i2c_read_data[4];

    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PILTL_ADDR,
                               (uint8_t*)&i2c_read_data[0], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PILTH_ADDR,
                               (uint8_t*)&i2c_read_data[1], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PIHTL_ADDR,
                               (uint8_t*)&i2c_read_data[2], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PIHTH_ADDR,
                               (uint8_t*)&i2c_read_data[3], 1, &bytes_w);

    ALSPRX_MSG_2(HIGH, "set_prx_thresh_low - data0=%d data1=%d",
                 i2c_read_data[0], i2c_read_data[1]);
    ALSPRX_MSG_2(HIGH, "set_prx_thresh_high - data2=%d data3=%d",
                 i2c_read_data[2], i2c_read_data[3]);
  }
#endif

  return SNS_DDF_SUCCESS;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_als_thresh

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_set_als_thresh
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t                 ailt,
  uint16_t                 aiht
)
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t          bytes_w;
  uint8_t          thresh[4];

   ALSPRX_MSG_0(HIGH, "rpr0521_set_als_thres\n");
  thresh[2] = (ailt & 0xFF); /* AILTL */
  thresh[3] = (ailt >> 8);   /* AILTH */
  thresh[0] = (aiht & 0xFF); /* AIHTL */
  thresh[1] = (aiht >> 8);   /* AIHTH */


  status = sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_AIHTL_ADDR,
                              (uint8_t*)&thresh[0], 4, &bytes_w);
  CHECK_STATUS(status);

#ifdef ALSPRX_DEBUG
  {
    uint8_t   i2c_read_data[4];

    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_AILTL_ADDR,
                               (uint8_t*)&i2c_read_data[0], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_AILTH_ADDR,
                               (uint8_t*)&i2c_read_data[1], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_AIHTL_ADDR,
                               (uint8_t*)&i2c_read_data[2], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_AIHTH_ADDR,
                               (uint8_t*)&i2c_read_data[3], 1, &bytes_w);

    ALSPRX_MSG_2(HIGH, "set_als_thresh_low - data0=%d data1=%d",
                 i2c_read_data[0], i2c_read_data[1]);
    ALSPRX_MSG_2(HIGH, "set_als_thresh_high - data2=%d data3=%d",
                 i2c_read_data[2], i2c_read_data[3]);

    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PILTL_ADDR,
                               (uint8_t*)&i2c_read_data[0], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PILTH_ADDR,
                               (uint8_t*)&i2c_read_data[1], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PIHTL_ADDR,
                               (uint8_t*)&i2c_read_data[2], 1, &bytes_w);
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PIHTH_ADDR,
                               (uint8_t*)&i2c_read_data[3], 1, &bytes_w);

    ALSPRX_MSG_2(HIGH, "set_prx_thresh_low - data0=%d data1=%d",
                 i2c_read_data[0], i2c_read_data[1]);
    ALSPRX_MSG_2(HIGH, "set_prx_thresh_high - data2=%d data3=%d",
                 i2c_read_data[2], i2c_read_data[3]);
  }
#endif

  return SNS_DDF_SUCCESS;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_cycle_time

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_set_cycle_time
(
  sns_dd_rpr0521_state_t* dd_ptr
)
{
  uint32_t max_odr;
  uint32_t odr;

   ALSPRX_MSG_0(HIGH, "rpr0521_set_cycle\n");
  max_odr = MAX(dd_ptr->sns_dd_rpr0521_common_db.als_req_odr,
                dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr);

  if (max_odr >= 17)
  {
    /* resulting freq = 20Hz */
    odr = 20; /* Hz */
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 40000; /* us */
  }
  else if (max_odr >= 13)
  {
    /* resulting freq = 16.8Hz */
    odr = 16;
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 50000;
  }
  else if (max_odr >= 9)
  {
    /* resulting freq = 12.8Hz */
    odr = 12;
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 66667;
  }
  else if (max_odr >= 5)
  {
    /* resulting freq = 8.9Hz */
    odr = 8;
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 100000;
  }
  else if (max_odr > 0)
  {
    /* resulting freq = 4.7Hz */
    /* (max_odr > 0) && (max_odr <=5) */
    odr = 4;
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 200000;
  }
  else
  {
    odr = 0;
    dd_ptr->sns_dd_rpr0521_common_db.cycle_time = 0;
  }

  dd_ptr->sns_dd_rpr0521_common_db.als_odr = odr; /* Hz */
  dd_ptr->sns_dd_rpr0521_common_db.prx_odr = odr; /* Hz */
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_update_intg_time

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_update_intg_time
(
  sns_dd_rpr0521_state_t* dd_ptr,
  bool             prx_enabled,
  bool             als_enabled,
  bool             static_setting
)
{
  uint8_t          atime=0;
//  uint8_t          ptime=0; //grace modify in 2014.4.11
  uint8_t          ttime=0;
  uint32_t         cycle_time_us;
  uint8_t          bytes_w;
  sns_ddf_status_e status;

   ALSPRX_MSG_0(HIGH, "rpr0521_update_intg\n");

  //grace modify in 2014.4.11 begin
   status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_ENABLE_ADDR,
                               (uint8_t*)&atime, 1, &bytes_w);
  //grace modify in 2014.4.11 end
  if (static_setting)
  {
    /* ATIME */
    atime = (atime & 0xf0) | BOTH100MS;   //grace modify in 2014.4.11
    //atime = 0x06;	//als 100ms and ps 100ms
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&atime, 1, &bytes_w);
    CHECK_STATUS(status);
  }
  else
  {
    /* Set ATIME, PTIME */
    cycle_time_us = dd_ptr->sns_dd_rpr0521_common_db.cycle_time;

//grace modify in 2014.4.11 begin

#if 0
    /* ATIME */
    if (als_enabled)
    {
      atime  = 100;
    }

    /* PTIME */
    if (als_enabled)
    {
	if(cycle_time_us < 100000)
	  ptime = 50;
	else
	  ptime = 100;
    }
    else	//als is not enabled
    {
	if(cycle_time_us < 100000)
	  ptime = 40;
	else
	  ptime = 100;
    }

    /* Make ttime */
    if(atime == 0)	//als not enabled
	ttime = (ptime == 40) ? 2 : 3;
    else
	ttime = (ptime == 50) ? 5 : 6;

#else
	if(cycle_time_us < 100000)
	  ttime = ALS100MS_PS50MS;
	else
	  ttime = BOTH100MS;

	  ttime = (atime & 0xf0) | ttime;

#endif
//grace modify in 2014.4.11 end

    /* Program */
	sns_ddf_delay(10000);
   ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_set_intg --> %x\n", ttime);
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                &ttime, 1, &bytes_w);
    CHECK_STATUS(status);
  }

  //grace modify in 2014.4.11
  //dd_ptr->enable_reg_data = ttime; //grace have question
  return SNS_DDF_SUCCESS;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_clear_int_reg

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  sns_ddf_status_e

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_clear_int_reg
(
  sns_dd_rpr0521_state_t* dd_ptr
)
{
  uint8_t i2c_data = 0;
  uint8_t bytes_w;
  sns_ddf_status_e status;

   ALSPRX_MSG_0(HIGH, "rpr0521_clr_int\n");
  status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_STATUS_ADDR,
                               (uint8_t*)&i2c_data, 1, &bytes_w);	//Read the register to clear interrupt. Don't care read value.
  ALSPRX_MSG_0(LOW, "clear_int_reg");
  return status;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_enable_interrupt

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_enable_interrupt
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e  sensor
)
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint16_t         i2c_data = 0x0;
//  uint16_t			enabled = 0x0;
  uint8_t          bytes_w;
//  uint16_t         i2c_pon_data;

   ALSPRX_MSG_0(HIGH, "rpr0521 enable int\n");
  ALSPRX_MSG_3(LOW, "ENABLE INTERRUPT START    enable_interrupt - sensor=%d prx=%d als=%d",
               sensor, dd_ptr->sns_dd_prx_db.enable, dd_ptr->sns_dd_als_db.enable);
  TCT_ALSPRX_MSG_3(LOW, "ENABLE INTERRUPT START    enable_interrupt - sensor=%d prx=%d als=%d",
               sensor, dd_ptr->sns_dd_prx_db.enable, dd_ptr->sns_dd_als_db.enable);

  if ((sensor != SNS_DDF_SENSOR_PROXIMITY) && (sensor != SNS_DDF_SENSOR_AMBIENT))
  {
    return SNS_DDF_EINVALID_PARAM;
  }

#ifdef ALSPRX_DEBUG
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_STATUS_ADDR,
                               (uint8_t*)&i2c_data, 1, &bytes_w);
#endif

  if (SNS_DDF_SENSOR_PROXIMITY == sensor)	//enable ps int
  {
    if (dd_ptr->sns_dd_prx_db.enable == TRUE)
    {
      ALSPRX_MSG_0(LOW, "ENABLE INTERRUPT END    proximity already enabled");
		TCT_ALSPRX_MSG_0(LOW, "ENABLE INTERRUPT proximity already enabled");
      return SNS_DDF_SUCCESS;
    }

#if 1
    /* ENABLE=PON */
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_ENABLE_ADDR,
                               (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);
/*
	i2c_pon_data = i2c_data & 0x01 & (0x01<<4);	//PS int enable and outside trigger

    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_STATUS_ADDR,
                                (uint8_t*)&i2c_pon_data, 1, &bytes_w);
    CHECK_STATUS(status);
    */
#endif


    /* Cleared interrupts by read register*/
 //   status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
 //   CHECK_STATUS(status);

    status = sns_dd_rpr0521_update_intg_time(dd_ptr, TRUE, dd_ptr->sns_dd_als_db.enable, FALSE);
    CHECK_STATUS(status);

    //status = sns_dd_rpr0521_set_prx_thresh(dd_ptr, SNS_DD_RPR0521_PILT_INIT, SNS_DD_RPR0521_PIHT_INIT);  //grace have question
    status = sns_dd_rpr0521_set_prx_thresh(dd_ptr, dd_ptr->sns_dd_prx_db.thresh_far, dd_ptr->sns_dd_prx_db.thresh_near );  //grace modify in 2014.4.11
    CHECK_STATUS(status);

    if (dd_ptr->sns_dd_als_db.enable == FALSE)
    {
      status = sns_dd_rpr0521_set_als_thresh(dd_ptr, 0, 0xFFFF); /* don't want AINT set */
      CHECK_STATUS(status);
    }

    /* ENABLE */
    //grace modify in 2014.4.11 begin
    //i2c_data = i2c_data | (0x01 << 6);
    i2c_data = i2c_data | PS_EN;
    //grace modify in 2014.4.11 end
	TCT_ALSPRX_MSG_0(LOW, "PS enable");

    dd_ptr->sns_dd_prx_db.enable = TRUE;
  }
  else if (SNS_DDF_SENSOR_AMBIENT == sensor)
  {
    if (dd_ptr->sns_dd_als_db.enable == TRUE)
    {
      ALSPRX_MSG_0(LOW, "ENABLE INTERRUPT END    light already enabled");
      return SNS_DDF_SUCCESS;
    }
      TCT_ALSPRX_MSG_0(LOW, "ALS enable");

#if 1
    /* ENABLE=PON */
/*    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_ENABLE_ADDR,
                               (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);*/

    /* Cleared interrupts by read register */
 //   status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
 //   CHECK_STATUS(status);
#endif

    status = sns_dd_rpr0521_update_intg_time(dd_ptr, dd_ptr->sns_dd_prx_db.enable, TRUE, FALSE);
    CHECK_STATUS(status);

    status = sns_dd_rpr0521_set_als_thresh(dd_ptr, 0xFFFF, 0);
    CHECK_STATUS(status);

    if (!dd_ptr->sns_dd_prx_db.enable)
    {
       status = sns_dd_rpr0521_set_prx_thresh(dd_ptr, 0, 0xFFFF); /* PINT should not be set */
        CHECK_STATUS(status);
    }

	status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_ENABLE_ADDR,		//	Added 2016/1/18
                               (uint8_t*)&i2c_data, 1, &bytes_w);
	CHECK_STATUS(status);

    /* ENABLE */
    //grace modify in 2014.4.11 begin
    //i2c_data = i2c_data | (0x01 << 7); //grace modify in 2014.4.11
    i2c_data = i2c_data | ALS_EN;
    //grace modify in 2014.4.11 end

    dd_ptr->sns_dd_als_db.enable = TRUE;
  }

  /* Update ENABLE bits*/

  status = sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_ENABLE_ADDR,
                              (uint8_t*)&i2c_data, 1, &bytes_w);
  CHECK_STATUS(status);

  dd_ptr->enable_reg_data = i2c_data;

  status = sns_ddf_signal_register(dd_ptr->interrupt_gpio, dd_ptr,
                                   &sns_dd_rpr0521_driver_fn_list, SNS_DDF_SIGNAL_IRQ_FALLING);

  ALSPRX_MSG_3(LOW, "ENABLE INTERRUPT END    enable_int - %d data=%d status=%d", 1100, i2c_data, status);

  return status;

}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_disable_interrupt

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_disable_interrupt
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e  sensor
)
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint16_t         i2c_data;// = SNS_DD_RPR0521_PWR_DOWN;
  //uint16_t         i2c_pon_data;// = SNS_DD_RPR0521_PWR_ON;
  uint8_t          bytes_w;

   ALSPRX_MSG_0(HIGH, "rpr0521 disable int\n");
  ALSPRX_MSG_2(LOW, "DISABLE INTERRUPT START    disable_int - %d sensor=%d", 1200, sensor);
	TCT_ALSPRX_MSG_2(HIGH, "rpr0521 disable int als enable=%d,ps enable=%d\n",dd_ptr->sns_dd_als_db.enable,dd_ptr->sns_dd_prx_db.enable);

  status = sns_ddf_read_port(dd_ptr->port_handle,
							 SNS_DD_RPR0521_ENABLE_ADDR,
							 (uint8_t*)&i2c_data, 1, &bytes_w);
  CHECK_STATUS(status);

  if (SNS_DDF_SENSOR_PROXIMITY == sensor)
  {
    if (dd_ptr->sns_dd_prx_db.enable == FALSE)
    {
      return SNS_DDF_SUCCESS;
	  ALSPRX_MSG_0(HIGH, "RPR0521 disable interrupt : ps_enable == false\n");
    }

    //grace modify in 2014.4.11 begin
    //i2c_data = i2c_data & 0xBF;	//shutdown PS
    i2c_data = i2c_data & (~PS_EN);	//shutdown PS
    //grace modify in 2014.4.11 end
   TCT_ALSPRX_MSG_0(HIGH, "rpr0521 disable PS int\n");

    dd_ptr->sns_dd_prx_db.enable = FALSE;
    dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr = 0;
    dd_ptr->sns_dd_rpr0521_common_db.prx_odr = 0;
  }
  else if (SNS_DDF_SENSOR_AMBIENT == sensor)
  {
    if (dd_ptr->sns_dd_als_db.enable == FALSE)
    {
      return SNS_DDF_SUCCESS;
    }

    //grace modify in 2014.4.11
    //i2c_data = i2c_data & 0x7F;	//shutdown ALS
    i2c_data = i2c_data & (~ALS_EN);	//shutdown ALS
    //grace modify in 2014.4.11 end
   TCT_ALSPRX_MSG_0(HIGH, "rpr0521 disable ALS int\n");

    dd_ptr->sns_dd_als_db.enable = FALSE;
    dd_ptr->sns_dd_rpr0521_common_db.als_req_odr = 0;
    dd_ptr->sns_dd_rpr0521_common_db.als_odr = 0;
  }
  else
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  /* Update ENABLE bits*/
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,			// Added 2016/1/18
                                (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);

  /* Update Integration times and thresholds */
  if (dd_ptr->sns_dd_als_db.enable || dd_ptr->sns_dd_prx_db.enable)
  {
#if 1
    /* ENABLE=PON */

    /* status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_pon_data, 1, &bytes_w);
    CHECK_STATUS(status);*/ //grace modify in 2014.4.11

    /* Clear interrupts */
    status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
    CHECK_STATUS(status);
#endif

    status = sns_dd_rpr0521_update_intg_time(dd_ptr, dd_ptr->sns_dd_prx_db.enable,
                                              dd_ptr->sns_dd_als_db.enable, FALSE);
    CHECK_STATUS(status);

    if (dd_ptr->sns_dd_als_db.enable)
    {
        status = sns_dd_rpr0521_set_als_thresh(dd_ptr, 0xFFFF, 0);
        CHECK_STATUS(status);
    }

    if (dd_ptr->sns_dd_prx_db.enable)
    {
      //status = sns_dd_rpr0521_set_prx_thresh(dd_ptr, SNS_DD_RPR0521_PILT_INIT, SNS_DD_RPR0521_PIHT_INIT); //grace have question. calibration isn't added.
      status = sns_dd_rpr0521_set_prx_thresh(dd_ptr, dd_ptr->sns_dd_prx_db.thresh_far, dd_ptr->sns_dd_prx_db.thresh_near );  //grace modify in 2014.4.11
      CHECK_STATUS(status);
    }

    /* Update ENABLE bits*/

/*    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);*/

	status = sns_ddf_read_port(dd_ptr->port_handle,
							 SNS_DD_RPR0521_ENABLE_ADDR,
							 (uint8_t*)&i2c_data, 1, &bytes_w);		// Added 2016/1/18
 	CHECK_STATUS(status);

    dd_ptr->enable_reg_data = i2c_data;

  }
  else
  {
    /* Clear ENABLE bits*/
    //grace modify in 2014.4.11 begin
    //i2c_data = 0x06; //dd_ptr->enable_reg_data & 0x0F;
    i2c_data = i2c_data & (~PS_EN) & (~ALS_EN); //dd_ptr->enable_reg_data & 0x0F;
    //grace modify in 2014.4.11 end
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);
    dd_ptr->enable_reg_data = i2c_data;

    /* Clear interrupts */
    status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
    CHECK_STATUS(status);

    /* De-register INT notification */
    status = sns_ddf_signal_deregister(dd_ptr->interrupt_gpio);
    CHECK_STATUS(status);
  }

  ALSPRX_MSG_1(LOW, "DISABLE INTERRUPT END    status=%d", status);
  return status;

}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_enable_sched_data

DESCRIPTION

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_enable_sched_data
(
  sns_ddf_handle_t  handle,
  sns_ddf_sensor_e  sensor,
  bool              enable
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t*)handle;

   ALSPRX_MSG_0(HIGH, "rpr0521 enable sched data\n");
  ALSPRX_MSG_3(LOW, "sched - %d sensor=%d enable=%d", 1000, sensor, enable);
  TCT_ALSPRX_MSG_2(HIGH, "rpr0521 enable sched data,sensor=%d enable=%d\n",sensor, enable);

  if(enable)
  {
  // need reset ps_data_max, ps_data_min ,prx_calibration when enable psensor
  #ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
      if((sensor == SNS_DDF_SENSOR_PROXIMITY)&&(dd_ptr->sns_dd_rpr0521_common_db.pwr_mode == SNS_DDF_POWERSTATE_ACTIVE))
      {
		dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = true;
		dd_ptr->ps_data_max=0;
		dd_ptr->ps_data_min=0xFFF;
	}
  #endif
      return sns_dd_rpr0521_enable_interrupt(dd_ptr, sensor);
  }
  else
  {
  #ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
  TCT_ALSPRX_MSG_2(HIGH, "rpr0521 enable sched data,sensor=%d prx_calibration=%d\n",sensor, dd_ptr->sns_dd_rpr0521_common_db.prx_calibration);
      if((sensor == SNS_DDF_SENSOR_PROXIMITY)&&(dd_ptr->sns_dd_rpr0521_common_db.pwr_mode == SNS_DDF_POWERSTATE_ACTIVE))
      {
        TCT_ALSPRX_MSG_0(HIGH, "rpr0521 enable sched data, stop timer when disable psensor\n");
	sns_dd_rpr0521_stop_general_timer( dd_ptr, sensor );
	dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = false;
	}
  #endif
      return sns_dd_rpr0521_disable_interrupt(dd_ptr, sensor);
  }
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_als_change_pcnt

DESCRIPTION   Sets the ALS change [ercentage

DEPENDENCIES  None

RETURN VALUE  SNS_DDF_SUCCESS on success

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_set_als_change_pcnt
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint32_t                 als_change_pcnt
)
{

   ALSPRX_MSG_0(HIGH, "rpr0521 set pcnt\n");
   ;	//do not support ppcnt
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_prx_factor

DESCRIPTION   Sets the Proximity calibration factor

DEPENDENCIES  None

RETURN VALUE  SNS_DDF_SUCCESS on success

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_set_prx_factor
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e        sensor_type,
  uint16_t                prx_factor
)
{
  uint16_t   i2c_data = 0x0;
  uint8_t    bytes_w;
  sns_ddf_status_e status=SNS_DDF_SUCCESS;
#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
  uint16_t   prx_compared = 0;
#endif

   ALSPRX_MSG_0(HIGH, "rpr0521 set prx factor\n");
   TCT_ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_set_prx_factor IN \n");

  if (sensor_type == SNS_DDF_SENSOR_PROXIMITY) {
	ALSPRX_MSG_1(HIGH, "set_prx_factor factor=%d", prx_factor);
	TCT_ALSPRX_MSG_1(HIGH, "set_prx_factor factor=%d", prx_factor);



#if 0
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = prx_factor;
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near = prx_factor+SNS_DD_RPR0521_PRX_NEAR_THRESHOLD;
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_far = prx_factor+SNS_DD_RPR0521_PRX_FAR_THRESHOLD;
#endif
#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
	prx_compared = prx_factor ;

	TCT_ALSPRX_MSG_2(HIGH, "rpr0521 use init, init_calib_pass=%d ,init_ps_crosstalk=%d", dd_ptr->init_calib_pass, dd_ptr->init_ps_crosstalk );

	if (dd_ptr->init_calib_pass == 1)
	{
		if (prx_factor > ((dd_ptr->init_ps_crosstalk)+120))
		{
			prx_compared =  dd_ptr->init_ps_crosstalk;
			ALSPRX_MSG_1(HIGH, "rpr0521 use init, prx_compared=%d", prx_compared );
			TCT_ALSPRX_MSG_1(HIGH, "rpr0521 use init, prx_compared=%d", prx_compared );
		}
	}
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = prx_compared;
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near = prx_compared+SNS_DD_RPR0521_PRX_NEAR_THRESHOLD;
      dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_far = prx_compared+SNS_DD_RPR0521_PRX_FAR_THRESHOLD;

    if (dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near >= 0xfff)
    {
		ALSPRX_MSG_1(ERROR, "rpr0521 set prx factor thresh_near=%d \n",dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near);
		TCT_ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_set_prx_factor thresh_near=%d\n",dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near);

		dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near = 4095;
		dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_far = dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near-(SNS_DD_RPR0521_PRX_NEAR_THRESHOLD-SNS_DD_RPR0521_PRX_FAR_THRESHOLD);
    }
#endif
    dd_ptr->sns_dd_prx_db.thresh_near = dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near;
    dd_ptr->sns_dd_prx_db.thresh_far = dd_ptr->sns_dd_rpr0521_common_db.nv_db.thresh_far;

    ALSPRX_MSG_2(HIGH, "sns_dd_rpr0521_set_prx_factor - thresh_near=%d ,thresh_far=%d", dd_ptr->sns_dd_prx_db.thresh_near, dd_ptr->sns_dd_prx_db.thresh_far);
    TCT_ALSPRX_MSG_2(HIGH, "sns_dd_rpr0521_set_prx_factor - thresh_near=%d ,thresh_far=%d", dd_ptr->sns_dd_prx_db.thresh_near, dd_ptr->sns_dd_prx_db.thresh_far);
    /* PILT */

    i2c_data  = dd_ptr->sns_dd_prx_db.thresh_far;
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                  SNS_DD_RPR0521_PILTL_ADDR,
                                  (uint8_t*)&i2c_data,
                                  2,
                                  &bytes_w);

    if ( status != SNS_DDF_SUCCESS )
    {
	ALSPRX_MSG_0(ERROR, "sns_dd_rpr0521_set_prx_factor write PILT fail");
	TCT_ALSPRX_MSG_0(ERROR, "sns_dd_rpr0521_set_prx_factor write PILT fail");
      return status;
    }

    /* PIHT */
    i2c_data  = dd_ptr->sns_dd_prx_db.thresh_near;
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                  SNS_DD_RPR0521_PIHTL_ADDR,
                                  (uint8_t*)&i2c_data,
                                  2,
                                  &bytes_w);

    if ( status != SNS_DDF_SUCCESS )
    {
	ALSPRX_MSG_0(ERROR, "sns_dd_rpr0521_set_prx_factor write PIHT fail");
	TCT_ALSPRX_MSG_0(ERROR, "sns_dd_rpr0521_set_prx_factor write PIHT fail");
      return status;
    }
  }

  return status;
}

#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
static sns_ddf_status_e sns_dd_rpr0521_ps_calib
(
  sns_dd_rpr0521_state_t* dd_ptr
)
{
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	uint8_t   i2c_read_data[2], i2c_data, ps_old_setting, interrupt_old_setting;
	uint8_t bytes_w;
 	uint16_t  pdata;
	uint16_t average = 0;
 	uint16_t i;
	uint8_t infrared_data;


	TCT_ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_calibration IN \n");

	//disable ps interrupt begin
	status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_STATUS_ADDR,
                               (uint8_t*)&interrupt_old_setting, 1, &bytes_w);
	CHECK_STATUS(status);

	i2c_data = interrupt_old_setting &0XFE;
	status = sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_STATUS_ADDR,
                              (uint8_t*)&i2c_data, 1, &bytes_w);
  	CHECK_STATUS(status);
       //disable ps interrupt end

	//set ps measurment time to 10ms begin
	status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_ENABLE_ADDR,
                               (uint8_t*)&ps_old_setting, 1, &bytes_w);
	if(status != SNS_DDF_SUCCESS)
	{
		ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 1 read or write port fail status=%d \n",status);
		TCT_ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 1 read or write port fail status=%d \n",status);

		sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_STATUS_ADDR,
                              (uint8_t*)&interrupt_old_setting, 1, &bytes_w);
		return -1;
	}

	i2c_data = (ps_old_setting & 0x30) |PS_EN|PS10MS;
	status = sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_ENABLE_ADDR,
                              (uint8_t*)&i2c_data, 1, &bytes_w);
  	if(status != SNS_DDF_SUCCESS)
	{
		ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 2 read or write port fail status=%d \n",status);
		TCT_ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 2 read or write port fail status=%d \n",status);

		sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_STATUS_ADDR,
                              (uint8_t*)&interrupt_old_setting, 1, &bytes_w);
		return -1;
	}
	//set ps measurment time to 10ms end

	sns_ddf_delay(20000);

	status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PRX_ADDR,
                               (uint8_t*)&i2c_data, 1, &bytes_w);
	if(status != SNS_DDF_SUCCESS)
	{
		ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 2 read or write port fail status=%d \n",status);
		TCT_ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 2 read or write port fail status=%d \n",status);
		goto err_exit;
	}

	infrared_data = i2c_data;

	if(infrared_data>>6)
	{
		goto err_exit;
	}

	for(i = 0; i < 5; i ++)
	{
		sns_ddf_delay(15000);

		status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PDATAL_ADDR,
                               (uint8_t*)&i2c_read_data, 2, &bytes_w);

		if(status != SNS_DDF_SUCCESS)
		{
			ALSPRX_MSG_0(ERROR, "rpr521 ps_calib i2c read ps data fail\n");
			TCT_ALSPRX_MSG_1(ERROR, "sns_dd_rpr0521_ps_calib 3 read port fail status=%d \n",status);
			goto err_exit;
		}
		pdata = (uint16_t)(((uint16_t)i2c_read_data[1] << 8) | i2c_read_data[0]);
		average += pdata & 0xFFF;
	}

	average /= 5;

	ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_ps_calib average=%d \n",average);
	TCT_ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_ps_calib average=%d \n",average);

	sns_dd_rpr0521_set_prx_factor(dd_ptr, SNS_DDF_SENSOR_PROXIMITY, average);

	//sns_ddf_smgr_notify_event(dd_ptr->smgr_handle, SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_EVENT_UPDATE_REGISTRY_GROUP);

       sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_ENABLE_ADDR,
                              (uint8_t*)&ps_old_setting, 1, &bytes_w);

	sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_STATUS_ADDR,
                              (uint8_t*)&interrupt_old_setting, 1, &bytes_w);

		dd_ptr->init_ps_crosstalk = average;
		dd_ptr->init_calib_pass = 1;

	ALSPRX_MSG_1(HIGH, "rpr521 PS calibration end : average = %d\n", average);
	TCT_ALSPRX_MSG_1(HIGH, "rpr521 PS calibration end : average = %d\n", average);
	TCT_ALSPRX_MSG_0(HIGH, "rpr521 PS calibration end\r\n");
       return SNS_DDF_SUCCESS;

err_exit:

	sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_ENABLE_ADDR,
                              (uint8_t*)&ps_old_setting, 1, &bytes_w);

	sns_ddf_write_port(dd_ptr->port_handle,
                              SNS_DD_RPR0521_STATUS_ADDR,
                              (uint8_t*)&interrupt_old_setting, 1, &bytes_w);

	return -1;
}
#endif


/*===========================================================================

FUNCTION      sns_dd_rpr0521_start_general_timer

DESCRIPTION   Starts a timer

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_start_general_timer
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint32_t               sensor_type,
  uint32_t duration
)
{
  sns_ddf_status_e status;

   ALSPRX_MSG_1(HIGH, "rpr0521 start general timer, sensor = %d\n", sensor_type);
  if (sensor_type == SNS_DDF_SENSOR_AMBIENT) {
    if ( dd_ptr->sns_dd_als_db.sns_dd_timer.active )
    {
      return;
    }

    status = sns_ddf_timer_start(dd_ptr->sns_dd_als_db.timer, duration);
    if ( status != SNS_DDF_SUCCESS )
    {
      return;
    }

    dd_ptr->sns_dd_als_db.sns_dd_timer.active = true;
  }
  else { // SNS_DDF_SENSOR_PROXIMITY
    if ( dd_ptr->sns_dd_prx_db.sns_dd_timer.active )
    {
      return;
    }

    status = sns_ddf_timer_start(dd_ptr->sns_dd_prx_db.timer, duration);
    if ( status != SNS_DDF_SUCCESS )
    {
      return;
    }

    dd_ptr->sns_dd_prx_db.sns_dd_timer.active = true;
  }
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_stop_general_timer

DESCRIPTION   Stops a timer

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_stop_general_timer( sns_dd_rpr0521_state_t* dd_ptr, uint32_t sensor_type )
{
   ALSPRX_MSG_1(HIGH, "rpr0521 stop general timer sensor = %d\n", sensor_type);
  if (sensor_type == SNS_DDF_SENSOR_AMBIENT || sensor_type == SNS_DDF_SENSOR__ALL ) {
    if ( true == dd_ptr->sns_dd_als_db.sns_dd_timer.active ) {
      sns_ddf_timer_cancel(dd_ptr->sns_dd_als_db.timer);
      dd_ptr->sns_dd_als_db.sns_dd_timer.active = false;
    }
  }
  if (sensor_type == SNS_DDF_SENSOR_PROXIMITY || sensor_type == SNS_DDF_SENSOR__ALL ) {
    if ( true == dd_ptr->sns_dd_prx_db.sns_dd_timer.active ) {
      sns_ddf_timer_cancel(dd_ptr->sns_dd_prx_db.timer);
      dd_ptr->sns_dd_prx_db.sns_dd_timer.active = false;
    }
  }
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_req_data

DESCRIPTION   A function that handles data request from sub-drivers

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_req_data
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint32_t               sensor_type
)
{
  sns_ddf_status_e status=SNS_DDF_SUCCESS;

   ALSPRX_MSG_1(HIGH, "rpr0521 req data sensor %d\n", sensor_type);
  if ( (sensor_type == SNS_DDF_SENSOR_AMBIENT) || (sensor_type == SNS_DDF_SENSOR_PROXIMITY) )
  {
    sns_dd_rpr0521_start_general_timer (dd_ptr, sensor_type,
                                100000);
  }
  else
  {
    status = SNS_DDF_EFAIL;
  }

  return status;
}

/******************************************************************************
 * NAME       : long_long_divider
 * FUNCTION   : calc divider of unsigned long long int or unsgined long
 * REMARKS    :
 *****************************************************************************/
static void long_long_divider(unsigned long long data, unsigned long base_divier, unsigned long *answer, unsigned long long *overplus)
{
#define MASK_LONG 0xFFFFFFFF
    volatile unsigned long long divier;
    volatile unsigned long      unit_sft;

    if ((long long)data < 0)	// . If data MSB is 1, it may go to endless loop.
    	{
    	data /= 2;	//0xFFFFFFFFFFFFFFFF / 2 = 0x7FFFFFFFFFFFFFFF
	base_divier /= 2;
    	}
    divier = base_divier;
    if (data > MASK_LONG) {
        unit_sft = 0;
        while (data > divier) {
            unit_sft++;
            divier = divier << 1;
        }
        while (data > base_divier) {
            if (data > divier) {
                *answer += 1 << unit_sft;
                data    -= divier;
            }
            unit_sft--;
            divier = divier >> 1;
        }
        *overplus = data;
    } else {
        *answer = (unsigned long)(data & MASK_LONG) / base_divier;
        /* calculate over plus and shift 16bit */
        *overplus = (unsigned long long)(data - (*answer * base_divier));
    }
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_als_convert_to_mlux

DESCRIPTION   Convert a raw data to a real milli lux

DEPENDENCIES  None

RETURN VALUE  milli lux value or 0 if there was a error

SIDE EFFECT   None

===========================================================================*/
uint32_t sns_dd_rpr0521_als_convert_to_mlux
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t data0,
  uint16_t data1,
  uint16_t gain_index,
  uint16_t time
)
{
#define DECIMAL_BIT      (15)
#define JUDGE_FIXED_COEF (1000)
//#define MAX_OUTRANGE     (655350)  //grace modify in 2014.4.9
//[BUFFIX]-Mod- by TCTNB.ZXZ,PR-977279, 2015/12/01, grace modify in 2015.11.27 ,for lightsensor max value can to 3000
#define MAX_OUTRANGE     (90000)
//[BUFFIX]-Mod-End- by TCTNB.ZXZ
#define MAXRANGE_NMODE   (0xFFFF)
#define MAXSET_CASE      (4)
#define CUT_UNIT	(1000)	// Lux to mLux
#define CALC_ERROR	(1010)

	uint32_t                final_data;
	CALC_DATA          calc_data;
	CALC_ANS           calc_ans;
	unsigned long      calc_judge;
	unsigned char      set_case;
	unsigned long      div_answer;
	unsigned long long div_overplus;
	unsigned long long overplus;
	unsigned long      max_range;

   ALSPRX_MSG_0(HIGH, "rpr0521 calc als\n");
	/* set the value of measured als data */
	calc_data.als_data0  = data0;
	calc_data.als_data1  = data1;
	calc_data.gain_data0 = gain_table[gain_index&0x0F].DATA0;
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-1246250, 2016/01/13, remove the delay ,if not will make other sensor interrupt late ,confirmed with fae grace
	//sns_ddf_delay(10000);
 	TCT_ALSPRX_MSG_2(MEDIUM, "cdata = %d, irdata = %d", data0, data1);
	//sns_ddf_delay(10000);
//[BUFFIX]-Mod-End- by TCTNB.ZXZ
 	ALSPRX_MSG_2(MEDIUM, "gain_index = %d, atime = %d", gain_index, time);
	ALSPRX_MSG_1(MEDIUM, "calc_data.gain_data0 = %d", calc_data.gain_data0);  //grace modify in 2014.4.9

	/* set max range */
	if (calc_data.gain_data0 == 0)
	{
		/* issue error value when gain is 0 */
		ALSPRX_MSG_0(HIGH, "rpr0521 calc_data.gain_data0 == 0\n"); //grace modify in 2014.4.9
		return (CALC_ERROR);
	}
	else
	{
		max_range = MAX_OUTRANGE / calc_data.gain_data0;
	}

	/* calculate data */
	if (calc_data.als_data0 == MAXRANGE_NMODE)
	{
		calc_ans.positive = max_range;
		calc_ans.decimal  = 0;
	}
	else
	{
		/* get the value which is measured from power table */
		calc_data.als_time = time&0xFF;
		if (calc_data.als_time == 0)
		{
			/* issue error value when time is 0 */
			ALSPRX_MSG_0(HIGH, "calc_data.als_time  == 0\n"); //grace modify in 2014.4.9
			return (CALC_ERROR);
		}

		calc_judge = calc_data.als_data1 * JUDGE_FIXED_COEF;
		if (calc_judge < (calc_data.als_data0 * judge_coefficient[0]))
		{
			set_case = 0;
		}
		else if (calc_judge < (calc_data.als_data0 * judge_coefficient[1]))
		{
			set_case = 1;
		}
		else if (calc_judge < (calc_data.als_data0 * judge_coefficient[2]))
		{
			set_case = 2;
		}
		else if (calc_judge < (calc_data.als_data0 * judge_coefficient[3]))
		{
			 set_case = 3;
		}
		else
		{
			set_case = MAXSET_CASE;
		}

		ALSPRX_MSG_2(MEDIUM, "rpr0521-set_case = %d , calc_data.als_time= %d", set_case, calc_data.als_time); //grace modify in 2014.4.9
		calc_ans.positive = 0;
		if (set_case >= MAXSET_CASE)
		{
			calc_ans.decimal = 0;	//which means that lux output is 0
		}
		else
		{
			calc_data.gain_data1 = gain_table[gain_index&0x0F].DATA1;
			ALSPRX_MSG_1(MEDIUM, "calc_data.gain_data1 = %d", calc_data.gain_data1); //grace modify in 2014.4.9
			if (calc_data.gain_data1 == 0)
			{
				/* issue error value when gain is 0 */
				ALSPRX_MSG_0(HIGH, "rpr0521 calc_data.gain_data1 == 0\n"); //grace modify in 2014.4.9
				return (CALC_ERROR);
			}
			calc_data.data0      = (unsigned long long )(data0_coefficient[set_case] * calc_data.als_data0) * calc_data.gain_data1;
			calc_data.data1      = (unsigned long long )(data1_coefficient[set_case] * calc_data.als_data1) * calc_data.gain_data0;
			if(calc_data.data0 < calc_data.data1)	//In this case, data will be less than 0. As data is unsigned long long, it will become extremely big.
			{
				ALSPRX_MSG_0(HIGH, "rpr0521 calc_data.data0 < calc_data.data1\n"); //grace modify in 2014.4.9
				return (CALC_ERROR);
			}
			else
			{
				calc_data.data       = (calc_data.data0 - calc_data.data1);
			}
			calc_data.dev_unit   = calc_data.gain_data0 * calc_data.gain_data1 * calc_data.als_time * 10;	//24 bit at max (128 * 128 * 100 * 10)
			if (calc_data.dev_unit == 0)
			{
				/* issue error value when dev_unit is 0 */
				ALSPRX_MSG_0(HIGH, "rpr0521 calc_data.dev_unit == 0\n"); //grace modify in 2014.4.9
				return (CALC_ERROR);
			}

			/* calculate a positive number */
			div_answer   = 0;
			div_overplus = 0;
			long_long_divider(calc_data.data, calc_data.dev_unit, &div_answer, &div_overplus);
			calc_ans.positive = div_answer;
			/* calculate a decimal number */
			calc_ans.decimal = 0;
			overplus         = div_overplus;
			if (calc_ans.positive < max_range)
			{
				if (overplus != 0)
				{
					overplus     = overplus << DECIMAL_BIT;
					div_answer   = 0;
					div_overplus = 0;
					long_long_divider(overplus, calc_data.dev_unit, &div_answer, &div_overplus);
					calc_ans.decimal = div_answer;
				}
			}

			else
			{
				calc_ans.positive = max_range;
			}
		}
	}
//[BUFFIX]-Mod-Begin by TCTNB.ZXZ,PR-977279, 2015/12/01, grace modify in 2015.11.27 ,add for lightsensor value in sun
	if (calc_ans.positive > JUDGE_OUTDOOR)
		calc_ans.positive = calc_ans.positive * COEF_OUTDOOR /10;
//[BUFFIX]-Mod-End by TCTNB.ZXZ,PR-977279, 2015/12/01


	final_data = calc_ans.positive * CUT_UNIT + ((calc_ans.decimal * CUT_UNIT) >> DECIMAL_BIT);
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-1246250, 2016/01/13, remove the delay ,if not will make other sensor interrupt late ,confirmed with fae grace
	//sns_ddf_delay(10000);
//[BUFFIX]-Mod-End- by TCTNB.ZXZ
 	TCT_ALSPRX_MSG_1(MEDIUM, "final data = %d", final_data);
	return (final_data);

#undef DECIMAL_BIT
#undef JUDGE_FIXED_COEF
#undef MAX_OUTRANGE
#undef MAXRANGE_NMODE
#undef MAXSET_CASE
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_async_notify

DESCRIPTION   Notify to SNSM with a sensor data.

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_async_notify
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e       sensor_type,
  uint32_t               data_val,
  uint32_t               prx_bin ,
  uint32_t               raw_data
)
{
  uint8_t num_sensors = 1, num_samples=2;
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint32_t data_lux;

  ALSPRX_MSG_2(HIGH, "async_notify - %d sensor=%d", 700, sensor_type);

  if ( sensor_type == SNS_DDF_SENSOR_AMBIENT )
  {
    /* convert to lux */
    data_lux = (uint32_t) (data_val / 1000);

    /* saturate */
      data_lux = (data_lux > SNS_DD_RPR0521_ALS_MAX_LUX) ? SNS_DD_RPR0521_ALS_MAX_LUX: data_lux;

    /* related to floating point library removal; change to FX_CONV_Q16(val,0) */
    dd_ptr->sensor_sample[0].sample = FX_CONV_Q16(data_lux, 0);
    dd_ptr->sensor_sample[0].status = SNS_DDF_SUCCESS;

    dd_ptr->sensor_sample[1].sample = raw_data;  /* ADC output - cdata */
    dd_ptr->sensor_sample[1].status = SNS_DDF_SUCCESS;

    sns_dd_rpr0521_log(dd_ptr, sensor_type, data_val, dd_ptr->sensor_sample[0].sample, 0, 0, raw_data);

    ALSPRX_MSG_2(LOW, "async_notify - als data Lux=%d cdata=%d\r\n", data_lux, raw_data);
  }
  else
  {
    dd_ptr->sensor_sample[0].sample = FX_CONV_Q16(prx_bin, 0);  /* proximity binary */
    dd_ptr->sensor_sample[0].status = SNS_DDF_SUCCESS;

    dd_ptr->sensor_sample[1].sample = raw_data;  /* ADC output - pdata */
    dd_ptr->sensor_sample[1].status = SNS_DDF_SUCCESS;
    sns_ddf_delay(10000); //grace have question
    ALSPRX_MSG_2(HIGH, "async_notify - prx data %d %d\r\n", prx_bin, raw_data);

    sns_dd_rpr0521_log(dd_ptr, sensor_type, prx_bin, dd_ptr->sensor_sample[0].sample, 0, 0, raw_data);

    ALSPRX_MSG_2(LOW, "async_notify - prx data %d %d\r\n", prx_bin, raw_data);
  }

  dd_ptr->sensor_data.status = status;
  dd_ptr->sensor_data.sensor = sensor_type;
  dd_ptr->sensor_data.num_samples = num_samples;
  if (dd_ptr->dri_enabled)
  {
    dd_ptr->sensor_data.timestamp = dd_ptr->int_timestamp;
  }
  else
  {
  dd_ptr->sensor_data.timestamp = sns_ddf_get_timestamp();
  }
  dd_ptr->sensor_data.samples = dd_ptr->sensor_sample;

  sns_ddf_smgr_notify_data(dd_ptr->smgr_handle, &dd_ptr->sensor_data, num_sensors);
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_als_rsp

DESCRIPTION   This function is called by the arbitration manager when ALS(light) data
              is available

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_als_rsp
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t               raw_data,
  uint32_t               data_mlux
)
{

  uint16_t ailt;
  uint16_t aiht;
  uint16_t diff;

   ALSPRX_MSG_0(HIGH, "rpr0521 als rsp\n");
  if (dd_ptr->dri_enabled)
  {
    /*diff = PCNT_OF_RAW(dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_change_pcnt,
                       raw_data);*/ //grace modify in 2014.4.11
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-862366, 2015/11/05, change the step of light sensor threhold,    1/5 diff make light sensor data change not quick
       //diff = raw_data /5;
       diff = raw_data /20;
//[BUFFIX]-Mod-End- by TCTNB.ZXZ

    ailt = (raw_data - diff);
    aiht = (raw_data + diff);
    sns_dd_rpr0521_set_als_thresh(dd_ptr, ailt, aiht);
    ALSPRX_MSG_3(HIGH, "als_rsp - raw_data=%d ailt=%d aiht=%d",
                 raw_data, ailt, aiht);
  }

  sns_dd_rpr0521_async_notify(dd_ptr, SNS_DDF_SENSOR_AMBIENT,
                             data_mlux, 0,
                             (uint32_t) raw_data);

  dd_ptr->sns_dd_als_db.last_mlux  = data_mlux;

}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_als_raw_data_ready

DESCRIPTION   This function is called when the ALS raw data is ready

DEPENDENCIES  None

RETURN VALUE  int32_t	0 success, 1 pending, -1 error

SIDE EFFECT   None

===========================================================================*/
bool sns_dd_rpr0521_als_raw_data_ready
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t cdata,
  uint16_t irdata,
  uint16_t gain_index,
  uint16_t atime
)
{
  uint32_t data_mlux;

#if 1
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	uint8_t          bytes_w;
	uint8_t          i2c_data = 0x0;
	uint8_t          en_i2c_data = 0x0;
	#endif

   ALSPRX_MSG_0(HIGH, "rpr0521 als raw data\n");
  /* convert reading to mlux */
  data_mlux = sns_dd_rpr0521_als_convert_to_mlux(dd_ptr,
                                                cdata,
                                                irdata,
                                                gain_index,
                                                atime);

    //we do not recommend auto-gain-adjustment. If it is required, pls contact our team.

// Begin- modified by TCTSH, tao-yu, PR-1533388, 2016/1/28
/*[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,FR-1887445, 2016/03/31*/
/*this feature is for all product, can resolve the als value is not accuracy in low light .
default als gain: ido4=idol4s=idol4 vdf =64X;
idol4 als >1000lux ,change gain to 1X,  <1000lux change to 64X;
idol4s and idol4 vdf >600lux , change gain to 2X , <500lux change to 64X;
*/
#if 1
	if( data_mlux >= SNS_DD_RPR0521_ALS_MLUX_HIGH && dd_ptr->sns_dd_als_db.last_mlux < SNS_DD_RPR0521_ALS_MLUX_HIGH)
	{

		status = sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);


		en_i2c_data=en_i2c_data &0x7f;
		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);
		TCT_ALSPRX_MSG_2(HIGH, "data_mlux:%d ,enable status 0x%x",data_mlux ,en_i2c_data);

		status = sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ALS_ADDR,
				&i2c_data, 1, &bytes_w);
		CHECK_STATUS(status);

		i2c_data = (LEDCURRENT_100MA | SNS_DD_RPR0521_ALS_GAIN_CHANGE << 4 | SNS_DD_RPR0521_ALS_GAIN_CHANGE << 2);
		dd_ptr->sns_dd_als_db.als_gain_index = SNS_DD_RPR0521_ALS_GAIN_CHANGE << 2 | SNS_DD_RPR0521_ALS_GAIN_CHANGE;

		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ALS_ADDR,
				&i2c_data, 1, &bytes_w);
		CHECK_STATUS(status);
		TCT_ALSPRX_MSG_0(ERROR, "rpr0521 als raw data : change gain to 2\n");

		en_i2c_data=en_i2c_data |0x80;
		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);
		TCT_ALSPRX_MSG_1(HIGH, "enable status 0x%x", en_i2c_data);

	}

	if( data_mlux < SNS_DD_RPR0521_ALS_MLUX_LOW&& dd_ptr->sns_dd_als_db.last_mlux >= SNS_DD_RPR0521_ALS_MLUX_LOW)
	{

		status = sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);

		en_i2c_data=en_i2c_data &0x7f;
		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);

		TCT_ALSPRX_MSG_2(HIGH, "data_mlux:%d ,enable status 0x%x",data_mlux, en_i2c_data);

		status = sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ALS_ADDR,
				&i2c_data, 1, &bytes_w);
		CHECK_STATUS(status);

		i2c_data = (LEDCURRENT_100MA | SNS_DD_ALS_GAIN_64X << 4 | SNS_DD_ALS_GAIN_64X << 2);
		dd_ptr->sns_dd_als_db.als_gain_index = SNS_DD_ALS_GAIN_64X << 2 | SNS_DD_ALS_GAIN_64X;

		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ALS_ADDR,
				&i2c_data, 1, &bytes_w);
		CHECK_STATUS(status);
		TCT_ALSPRX_MSG_0(ERROR, "rpr0521 als raw data : change gain to 64\n");

		en_i2c_data=en_i2c_data |0x80;
		status = sns_ddf_write_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&en_i2c_data, 1, &bytes_w);

		TCT_ALSPRX_MSG_1(HIGH, "enable status 0x%x", en_i2c_data);

	}
#endif
/*[BUFFIX]-Mod-End- by TCTNB.ZXZ,FR-1887445, 2016/03/31*/
// End- modified by TCTSH, tao-yu, PR-1533388, 2016/1/28

    // report to DSPS as the lux range is ok
    sns_dd_rpr0521_als_rsp(dd_ptr, cdata, data_mlux);

  return true;	// ok to update DSPS
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_prx_rsp

DESCRIPTION   This function is called by the proximity common handler when proximity data
              is available

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_prx_rsp
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t               pdata
)
{
   uint8_t   i2c_data = 0x0;
   uint8_t   bytes_rw;
   sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_PRX_ADDR,
				(uint8_t*)&i2c_data, 1, &bytes_rw);//grace test in 2014.4.9
   ALSPRX_MSG_2(HIGH, "RPR0521 prx rsp : ps_gain = %d, reg0x43 = %x\n", dd_ptr->sns_dd_prx_db.ps_gain_index, i2c_data);//grace modify in 2014.4.9
   ALSPRX_MSG_3(HIGH, "RPR0521 prx rsp : ps_data = %d, thresh_near = %d, far = %d\n", pdata,dd_ptr->sns_dd_prx_db.thresh_near ,dd_ptr->sns_dd_prx_db.thresh_far);
  if ( pdata >= dd_ptr->sns_dd_prx_db.thresh_near )
  {
	ALSPRX_MSG_0(LOW, "RPR0521 prx rsp : psdata >= sns_dd_prx_db.thresh_near");

//grace modify in 2014.4.22 begin
		sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_PRX_ADDR,
				(uint8_t*)&i2c_data, 1, &bytes_rw);
//grace modify in 2014.4.22 end
    //if ( dd_ptr->sns_dd_als_db.irdata < (65536) ) /* just in case bright sunlight will false trigger it */
    if ( i2c_data>>6 == INFRA_LOW) //grace modify in 2014.4.22
    {
      if ( dd_ptr->sns_dd_prx_db.last_nearby != SNS_PRX_NEAR_BY )
      {
        dd_ptr->sns_dd_prx_db.prx_detection_state = SNS_PRX_NEAR_BY;
      }
      dd_ptr->sns_dd_prx_db.last_nearby = SNS_PRX_NEAR_BY;

      if (dd_ptr->dri_enabled)
      {
	ALSPRX_MSG_0(LOW, "RPR0521 prx rsp : psdata >= sns_dd_prx_db.thresh_near =>dri_enabled");
	/*sns_dd_rpr0521_set_prx_thresh(dd_ptr,
                                       SNS_DD_RPR0521_PILT_NEAR,
                                       SNS_DD_RPR0521_PIHT_NEAR);*/
        sns_dd_rpr0521_set_prx_thresh(dd_ptr, dd_ptr->sns_dd_prx_db.thresh_far, SNS_DD_RPR0521_PIHT_NEAR );//grace modify in 2014.4.22
      }
    }
    else
    {
    	ALSPRX_MSG_0(LOW, "RPR0521 prx rsp : psdata >= sns_dd_prx_db.thresh_near =>INFRA_LOW");
      if ( dd_ptr->sns_dd_prx_db.last_nearby != SNS_PRX_FAR_AWAY )
      {
        dd_ptr->sns_dd_prx_db.prx_detection_state = SNS_PRX_FAR_AWAY;
        dd_ptr->sns_dd_prx_db.last_nearby = SNS_PRX_FAR_AWAY;
        /*sns_dd_rpr0521_set_prx_thresh(dd_ptr,
                                       SNS_DD_RPR0521_PILT_FAR,
                                       SNS_DD_RPR0521_PIHT_FAR);*/
        sns_dd_rpr0521_set_prx_thresh(dd_ptr, SNS_DD_RPR0521_PILT_FAR, dd_ptr->sns_dd_prx_db.thresh_near);//grace modify in 2014.4.22

      }
    }
  }
  else if ( pdata < dd_ptr->sns_dd_prx_db.thresh_far )
  {
    ALSPRX_MSG_0(LOW, "RPR0521 prx rsp : psdata < sns_dd_prx_db.thresh_far");
    if ( dd_ptr->sns_dd_prx_db.last_nearby != SNS_PRX_FAR_AWAY )
    {
      dd_ptr->sns_dd_prx_db.prx_detection_state = SNS_PRX_FAR_AWAY;
    }
    dd_ptr->sns_dd_prx_db.last_nearby = SNS_PRX_FAR_AWAY;

    if (dd_ptr->dri_enabled)
    {
       ALSPRX_MSG_0(LOW, "RPR0521 prx rsp : psdata < sns_dd_prx_db.thresh_far =>dri_enabled");
       /*sns_dd_rpr0521_set_prx_thresh(dd_ptr,
                                      SNS_DD_RPR0521_PILT_FAR,
                                      SNS_DD_RPR0521_PIHT_FAR);*/ //grace have question. calibration data isn't added
       sns_dd_rpr0521_set_prx_thresh(dd_ptr, SNS_DD_RPR0521_PILT_FAR, dd_ptr->sns_dd_prx_db.thresh_near); //grace modify in 2014.4.22
    }
  }
}
/*===========================================================================

FUNCTION      sns_dd_rpr0521_prxcomm_rsp

DESCRIPTION   This function is called by the arbitration manager when proximity
              data is available

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_prxcomm_rsp
(
  sns_dd_rpr0521_state_t* dd_ptr,
  uint16_t               pdata
)
{
   ALSPRX_MSG_1(HIGH, "rpr0521 prxcomm %d\n", pdata);
  *(uint32_t *)&dd_ptr->sns_dd_prx_db.prxdist_data_cache = 0;

  sns_dd_rpr0521_prx_rsp(dd_ptr, pdata); /* binary decision */
  sns_dd_rpr0521_async_notify(dd_ptr,
                                 SNS_DDF_SENSOR_PROXIMITY,
                                 *(uint32_t *)&dd_ptr->sns_dd_prx_db.prxdist_data_cache,
                                 dd_ptr->sns_dd_prx_db.prx_detection_state,
                                 (uint32_t) pdata);

TCT_ALSPRX_MSG_2(HIGH, "sns_dd_rpr0521_prxcomm_rsp , pdata=%d, status=%d ! \n",pdata,dd_ptr->sns_dd_prx_db.prx_detection_state);

}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_handle_device_indication

DESCRIPTION   This function is called by the arbitration manager when proximity
              data is available

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
void sns_dd_rpr0521_handle_device_indication
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e         sensor_type,
  bool                     second_type
)
{

  uint8_t   i2c_read_data[6];
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t   bytes_rw;
  uint16_t  adata0, adata1, pdata;
  int32_t   als_update_flag=0;
  uint8_t   i2c_data;
  uint32_t  data_mlux;

   ALSPRX_MSG_1(HIGH, "RPR0521 handle device indication : sensor_type = %d\n", sensor_type);

  if (second_type == FALSE)
  {
    /* read cdata, irdata and pdata */
    status = sns_ddf_read_port(dd_ptr->port_handle,
                               SNS_DD_RPR0521_PDATAL_ADDR,
                               (uint8_t*)&i2c_read_data, 6, &bytes_rw);

    if ( status != SNS_DDF_SUCCESS )
    {
      ALSPRX_MSG_2(LOW, "handle_dev_ind - %d %d", 800, 2);
      return;
    }

/*modify Begin- by TCTSH tao-yu, PR-977004, 2015/11/25, correct parameter type*/
  pdata = (uint16_t)(((uint16_t)i2c_read_data[1] << 8) | i2c_read_data[0]);
  adata0 = (uint16_t)(((uint16_t)i2c_read_data[3] << 8) | i2c_read_data[2]);
  adata1 = (uint16_t)(((uint16_t)i2c_read_data[5] << 8) | i2c_read_data[4]);
/*modify Begin- by TCTSH tao-yu, PR-977004, 2015/11/25, correct parameter type*/
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-1246250, 2016/01/13, remove the delay ,if not will make other sensor interrupt late ,confirmed with fae grace
  //sns_ddf_delay(10000);
//[BUFFIX]-Mod-End- by TCTNB.ZXZ
    ALSPRX_MSG_3(ERROR, "RPR0521 handle device indication : Cdata0 = %d, IRdata1 = %d, PSdata = %d\n", adata0, adata1, pdata);
    status = sns_ddf_read_port(dd_ptr->port_handle,
				SNS_DD_RPR0521_ENABLE_ADDR,
				(uint8_t*)&i2c_read_data, 1, &bytes_rw);
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-1246250, 2016/01/13, remove the delay ,if not will make other sensor interrupt late ,confirmed with fae grace
	//sns_ddf_delay(10000);
//[BUFFIX]-Mod-End- by TCTNB.ZXZ
	ALSPRX_MSG_1(HIGH, "enable status 0x%x", i2c_read_data[0]);

    /* keep a copy */
    dd_ptr->sns_dd_als_db.cdata = adata0;
    dd_ptr->sns_dd_als_db.irdata = adata1;
    dd_ptr->sns_dd_prx_db.pdata = pdata;
  }

  if (sensor_type == SNS_DDF_SENSOR_AMBIENT)  {
     // ALSPRX_MSG_1(MEDIUM, "als_calibration =%d\r\n", dd_ptr->sns_dd_rpr0521_common_db.als_calibration);
    if (dd_ptr->sns_dd_rpr0521_common_db.als_calibration) {

      dd_ptr->sns_dd_rpr0521_common_db.als_calibration = false;
      // complete
      data_mlux = sns_dd_rpr0521_als_convert_to_mlux(dd_ptr, dd_ptr->sns_dd_als_db.cdata,
      		dd_ptr->sns_dd_als_db.irdata, dd_ptr->sns_dd_als_db.als_gain_index, 100); //dd_ptr->sns_dd_als_db.als_atime is always 100
      data_mlux = data_mlux/1000; // convert mlux to lux

      TCT_ALSPRX_MSG_1(MEDIUM, "handle_dev_ind - lux =%d\r\n", data_mlux);
#if 0
/*
	if ( (data_mlux <= (dd_ptr->sns_dd_als_db.als_cal_standard_value*110)/100) &&
		(data_mlux >= (dd_ptr->sns_dd_als_db.als_cal_standard_value*90)/100)){
        dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor = 100;
	TCT_ALSPRX_MSG_1(HIGH, "TCT handle_dev_ind  SUCCESS 1111 dd_ptr->als_cal_flag=%d",dd_ptr->sns_dd_als_db.als_cal_standard_value);

      }

	else if ( (data_mlux >= (dd_ptr->sns_dd_als_db.als_cal_standard_value*20)/100) &&
		(data_mlux <= (dd_ptr->sns_dd_als_db.als_cal_standard_value*140)/100) ){
        dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor = (dd_ptr->sns_dd_als_db.als_cal_standard_value * 100)/data_mlux;
        TCT_ALSPRX_MSG_2(HIGH, "TCT handle_dev_ind  SUCCESS 222 als_cal_standard_value=%d , als_factor=%d",
        dd_ptr->sns_dd_als_db.als_cal_standard_value,dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor);

      }
// VERTU - zhangxianzhu - BUG-2397823
*/

        dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor = (dd_ptr->sns_dd_als_db.als_cal_standard_value * 100)/data_mlux;
        TCT_ALSPRX_MSG_2(HIGH, "TCT handle_dev_ind  SUCCESS als_cal_standard_value=%d , als_factor=%d",
        dd_ptr->sns_dd_als_db.als_cal_standard_value,dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor);


	sns_ddf_smgr_notify_event(dd_ptr->smgr_handle, sensor_type, SNS_DDF_EVENT_UPDATE_REGISTRY_GROUP);
	dd_ptr->sns_dd_als_db.als_cal_flag=RPR_ALS_CAL_UPDATE;
	TCT_ALSPRX_MSG_1(HIGH, "TCT handle_dev_ind  SUCCESS 1111 dd_ptr->als_cal_flag=%d",dd_ptr->sns_dd_als_db.als_cal_flag);
	/* end vertu - BUG-2397823*/

	/* end vertu - JIRA TRON-143*/

#endif
    }

  /* VERTU - zhangxianzhu - BUG-2477540*/
  if ((dd_ptr->dri_enabled) || ((!(dd_ptr->dri_enabled)) && (dd_ptr->sns_dd_rpr0521_common_db.als_get_data)))
	{
	/* end vertu - BUG-2477540*/
        /* indicate the data to the light(ALS) sub-driver */
        als_update_flag = sns_dd_rpr0521_als_raw_data_ready(dd_ptr,
                                                             dd_ptr->sns_dd_als_db.cdata,
                                                             dd_ptr->sns_dd_als_db.irdata,
                                                             dd_ptr->sns_dd_als_db.als_gain_index,
                                                             100);//dd_ptr->sns_dd_als_db.als_atime is always 100
        if (als_update_flag) {
            dd_ptr->sns_dd_rpr0521_common_db.als_get_data = false;
        }
    }
  }
  else {  // SNS_DDF_SENSOR_PROXIMITY
    if (dd_ptr->sns_dd_rpr0521_common_db.prx_calibration) {
	/* VERTU - zhangxianzhu - BUG-2477540*/
	sns_dd_rpr0521_stop_general_timer( dd_ptr, sensor_type );
	dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = false;
	if ( dd_ptr->sns_dd_prx_db.pdata > dd_ptr->ps_data_max ) {
		dd_ptr->ps_data_max = dd_ptr->sns_dd_prx_db.pdata;
	}
	if ( dd_ptr->sns_dd_prx_db.pdata < dd_ptr->ps_data_min) {
		dd_ptr->ps_data_min = dd_ptr->sns_dd_prx_db.pdata;
	}
	ALSPRX_MSG_2(HIGH, "RPR0521 handle device indication : max = %d, min = %d\n", dd_ptr->ps_data_max, dd_ptr->ps_data_min);
//zxz this debug log need remove when in MP
	//TCT_ALSPRX_MSG_2(HIGH, "RPR0521 handle device indication : max = %d, min = %d\n", dd_ptr->ps_data_max, dd_ptr->ps_data_min);

	if ( (dd_ptr->ps_data_max - dd_ptr->ps_data_min) < JUDGE_STOP_CALIB ) {
		ALSPRX_MSG_0(HIGH, "RPR0521 handle device indication : continue getting data for calib\n");
		TCT_ALSPRX_MSG_0(HIGH, "RPR0521 handle device indication : continue getting data for calib\n");
		dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = true;
		sns_dd_rpr0521_req_data (dd_ptr, sensor_type);
	}
      else
      {// complete
		dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_pdata = dd_ptr->ps_data_min;
		ALSPRX_MSG_1(HIGH, "RPR0521 handle device indication : DRI calib_ps_data = %d\n", dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_pdata);
		TCT_ALSPRX_MSG_1(HIGH, "RPR0521 handle device indication : DRI calib_ps_data = %d\n", dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_pdata);
          	sns_dd_rpr0521_set_prx_factor(dd_ptr, sensor_type, dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_pdata);
      }
    }

  if ((dd_ptr->dri_enabled) || ((!dd_ptr->dri_enabled) && (dd_ptr->sns_dd_rpr0521_common_db.prx_get_data)))
	{
        dd_ptr->sns_dd_rpr0521_common_db.prx_get_data = false;
        /* indicate the data to the proximity common handler */
	TCT_ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_handle_device_indication ! \n");
        sns_dd_rpr0521_prxcomm_rsp (dd_ptr, dd_ptr->sns_dd_prx_db.pdata);
      }
	ALSPRX_MSG_2(HIGH, "RPR0521 handle device indication : dri_enabled = %d, ps_get_data = %d\n", dd_ptr->dri_enabled, dd_ptr->sns_dd_rpr0521_common_db.prx_get_data);
	/* end vertu - BUG-2477540*/
  }

  if (!dd_ptr->dri_enabled) {
    if (sensor_type ==  SNS_DDF_SENSOR_AMBIENT) {
      if ((!als_update_flag)) {
        sns_dd_rpr0521_start_general_timer (dd_ptr, SNS_DDF_SENSOR_AMBIENT,
                                             100000);
        return;
      }
    }

    i2c_data = dd_ptr->enable_reg_data;
	ALSPRX_MSG_1(HIGH, "RPR0521 handle device indication : Enable_reg = %x\n", i2c_data); // VERTU - zhangxianzhu - BUG-2477540
    sns_ddf_write_port(dd_ptr->port_handle,
                       SNS_DD_RPR0521_ENABLE_ADDR,
                       (uint8_t*)&i2c_data,
                       1,
                       &bytes_rw);
  }

	dd_ptr->dri_enabled = false; // VERTU - zhangxianzhu - BUG-2477540

  ALSPRX_MSG_2(LOW, "handle_dev_ind - %d %d", 800, 10);
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_handle_irq

===========================================================================*/
/*!
  @brief Interrupt handler

  @detail
  Called in response to an interrupt for this driver.

  @see sns_ddf_driver_if_s.handle_irq()

  @param[in] dd_handle  Handle to a driver instance.
  @param[in] irq        The IRQ representing the interrupt that occured.

  @return None
*/
/*=========================================================================*/
static void sns_dd_rpr0521_handle_irq
(
  sns_ddf_handle_t  handle,
  uint32_t          gpio_num,
  sns_ddf_time_t    timestamp
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t*)handle;
  sns_ddf_status_e         status = SNS_DDF_SUCCESS;
  uint8_t                  bytes_rw;
  uint8_t                  i2c_data = 0x0;
//  uint8_t                  disable_data = 0;
//  uint8_t                  reg_addr=0;


   dd_ptr->dri_enabled = true;

  ALSPRX_MSG_1(MEDIUM, "RPR0521 handle IRQ START : gpio_num = %d\n", gpio_num);


  /* STATUS */
  status = sns_ddf_read_port(dd_ptr->port_handle,
                             SNS_DD_RPR0521_STATUS_ADDR,
                             (uint8_t*)&i2c_data, 1, &bytes_rw);
  if(status != SNS_DDF_SUCCESS)
  {
    ALSPRX_MSG_3(LOW, "irq - %d %d port read result=%d", 600, 5, status);
    return;
  }

  ALSPRX_MSG_1(LOW, "RPR0521 handle IRQ : Status_reg = %x\n", i2c_data); // VERTU - zhangxianzhu - JIRA TRON-143 - 1843402-[HLR-591] - BSP - ALS calibration

  dd_ptr->int_timestamp = timestamp;

  /* Check which interrupts occured */
  if ((i2c_data & SNS_DD_RPR0521_PINT_STATUS ) && dd_ptr->sns_dd_prx_db.enable)
  {
    /* Proximity Interrupt */
	ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_handle_irq PROXIMITY interrupt ! \n");
    TCT_ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_handle_irq PROXIMITY interrupt ! \n");
    sns_dd_rpr0521_handle_device_indication(dd_ptr,
                                             SNS_DDF_SENSOR_PROXIMITY,
                                             FALSE);

  }

  if ((i2c_data & SNS_DD_RPR0521_AINT_STATUS) && dd_ptr->sns_dd_als_db.enable)
  {
    /* ALS Interrupt */
    /*sns_dd_rpr0521_handle_device_indication(dd_ptr,
                                             SNS_DDF_SENSOR_AMBIENT,
                                             reg_addr);*/ //grace modify in 2014.4.11
    sns_dd_rpr0521_handle_device_indication(dd_ptr,
                                             SNS_DDF_SENSOR_AMBIENT,
                                             FALSE);
  }

#ifdef ALSPRX_DEBUG
    if ((i2c_data & SNS_DD_RPR0521_PINT_STATUS ) && !(dd_ptr->sns_dd_prx_db.enable))
      MSG(MSG_SSID_SNS,DBG_ERROR_PRIO, "ALSPRX - PINT set, but PRX not enabled");

    if ((i2c_data & SNS_DD_RPR0521_AINT_STATUS) && !(dd_ptr->sns_dd_als_db.enable))
      MSG(MSG_SSID_SNS,DBG_ERROR_PRIO, "ALSPRX - AINT set, but ALS not enabled");
#endif

  /* Clear interrupts */
  status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
  if(status != SNS_DDF_SUCCESS)
  {
    ALSPRX_MSG_0(ERROR, "Clear interrupts Fail in sns_dd_rpr0521_handle_irq !");
    TCT_ALSPRX_MSG_0(ERROR, "Clear interrupts Fail in sns_dd_rpr0521_handle_irq !");
  }

}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_handle_timer

===========================================================================*/
/*!
  @brief Called when the timer set by this driver has expired. This is
         the callback function submitted when initializing a timer.

  @detail
  This will be called within the context of the Sensors Manager task.
  It indicates that sensor data is ready

  @param[in] dd_handle  Handle to a driver instance.
  @param[in] arg        The argument submitted when the timer was set.

  @return None
*/
/*=========================================================================*/
static void sns_dd_rpr0521_handle_timer
(
  sns_ddf_handle_t handle,
  void* arg
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t*)handle;
  sns_ddf_sensor_e       sensor_type=*(sns_ddf_sensor_e*)arg;
   sns_ddf_delay(100);

	ALSPRX_MSG_1(MEDIUM, "RPR0521 handle timer In : sensor_type = %d\n", sensor_type); // VERTU - zhangxianzhu - JIRA TRON-143 - 1843402-[HLR-591] - BSP - ALS calibration

  if (sensor_type == SNS_DDF_SENSOR_AMBIENT) {
    if ( false == dd_ptr->sns_dd_als_db.sns_dd_timer.active ) return; // when sensor_type is als
  }
  else {  // SNS_DDF_SENSOR_PROXIMITY
    if ( false == dd_ptr->sns_dd_prx_db.sns_dd_timer.active ) return; // when sensor_type is prx
  }

  ALSPRX_MSG_2(LOW, "handle_timer - %d sensor=%d", 500, sensor_type);

  sns_dd_rpr0521_stop_general_timer(dd_ptr, sensor_type);

  if (((sensor_type == SNS_DDF_SENSOR_AMBIENT) &&
       ((dd_ptr->sns_dd_rpr0521_common_db.als_get_data) || (dd_ptr->sns_dd_rpr0521_common_db.als_calibration))) ||
      ((sensor_type == SNS_DDF_SENSOR_PROXIMITY) &&
       ((dd_ptr->sns_dd_rpr0521_common_db.prx_get_data) || (dd_ptr->sns_dd_rpr0521_common_db.prx_calibration)))) {
    sns_dd_rpr0521_handle_device_indication(dd_ptr, sensor_type, FALSE);
    }

}

/*==============================================================================

FUNCTION      sns_dd_rpr0521_als_init

DESCRIPTION   Initializes the light(ALS) driver

DEPENDENCIES  None

RETURN VALUE  TRUE on success, FALSE otherwise

SIDE EFFECT   None

==============================================================================*/
sns_ddf_status_e sns_dd_rpr0521_als_init (sns_dd_rpr0521_state_t* dd_ptr)
{
  sns_dd_als_db_type *als_db_ptr = &dd_ptr->sns_dd_als_db;

   ALSPRX_MSG_0(HIGH, "rpr0521 als init\n");
  als_db_ptr->last_mlux         = SNS_DD_ALS_DFLT_MILLI_LUX;
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-862366, 2015/11/05, change gain to 2X , the data will easy spill over
  //als_db_ptr->als_gain_index    = SNS_DD_ALS_GAIN_64X << 2 | SNS_DD_ALS_GAIN_64X;
  als_db_ptr->als_gain_index    = SNS_DD_ALS_GAIN_64X << 2 | SNS_DD_ALS_GAIN_64X; // VERTU - zhangxianzhu - JIRA TRON-143 - 1843402-[HLR-591] - BSP - ALS calibration
//[BUFFIX]-Mod-End- by TCTNB.ZXZ,
  als_db_ptr->enable            = false;
  als_db_ptr->sns_dd_timer.active = false;

  dd_ptr->sns_dd_als_db.saturated_lux = SNS_DD_RPR0521_ALS_MAX_LUX;

  return SNS_DDF_SUCCESS;
}

/*==============================================================================

FUNCTION      sns_dd_rpr0521_prx_init

DESCRIPTION   initializes the ALS/Proximty

DEPENDENCIES  None

RETURN VALUE  TRUE on success, FALSE otherwise

SIDE EFFECT   None

==============================================================================*/
sns_ddf_status_e sns_dd_rpr0521_prx_init ( sns_dd_rpr0521_state_t* dd_ptr )
{
  sns_dd_prx_db_type     *prx_db_ptr     = &dd_ptr->sns_dd_prx_db;

   ALSPRX_MSG_0(HIGH, "rpr0521 prx init\n");
  prx_db_ptr->last_nearby = SNS_PRX_NEAR_BY_UNKNOWN;
  prx_db_ptr->thresh_near = dd_ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor + SNS_DD_RPR0521_PRX_NEAR_THRESHOLD;
  prx_db_ptr->thresh_far = dd_ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor + SNS_DD_RPR0521_PRX_FAR_THRESHOLD;
  //prx_db_ptr->ps_gain_index = SNS_DD_PS_GAIN_4X;
 // prx_db_ptr->ps_gain_index = SNS_DD_PS_GAIN_2X; //grace modify in 2014.7.22
  prx_db_ptr->ps_gain_index = SNS_DD_PS_GAIN_1X;

  prx_db_ptr->enable     = false;
  prx_db_ptr->sns_dd_timer.active = false;

  return SNS_DDF_SUCCESS;
}

/*==============================================================================

FUNCTION      sns_dd_rpr0521_common_init

DESCRIPTION   initializes the ALS/Proximty

DEPENDENCIES  None

RETURN VALUE  TRUE on success, FALSE otherwise

SIDE EFFECT   None

==============================================================================*/

sns_ddf_status_e sns_dd_rpr0521_common_init ( sns_dd_rpr0521_state_t* dd_ptr )
{
   ALSPRX_MSG_0(HIGH, "rpr0521 common init\n");
	TCT_ALSPRX_MSG_0(HIGH, "rpr0521 common init\n"); // VERTU - zhangxianzhu - JIRA TRON-143 - 1843402-[HLR-591] - BSP - ALS calibration
  dd_ptr->sns_dd_rpr0521_common_db.prx_odr = 0;
  dd_ptr->sns_dd_rpr0521_common_db.als_odr = 0;
  dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr = 0;
  dd_ptr->sns_dd_rpr0521_common_db.als_req_odr = 0;
  dd_ptr->sns_dd_rpr0521_common_db.state = SNS_DD_DEV_STOPPED;

  dd_ptr->sns_dd_rpr0521_common_db.als_get_data = false;
  dd_ptr->sns_dd_rpr0521_common_db.prx_get_data = false;
  dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = false;
  dd_ptr->sns_dd_rpr0521_common_db.als_calibration = false;

  return SNS_DDF_SUCCESS;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_init_data_struct

DESCRIPTION   Initialize data structures for ALS and PRX

DEPENDENCIES  None

RETURN VALUE  SNS_DDF_SUCCESS on success

SIDE EFFECT   None

===========================================================================*/
static sns_ddf_status_e sns_dd_rpr0521_init_data_struct
(
  sns_dd_rpr0521_state_t* dd_ptr
)
{
  sns_ddf_status_e init_status = SNS_DDF_SUCCESS;

   ALSPRX_MSG_0(HIGH, "rpr0521 init data\n");
  /* Initialize the common database */
  if ( (init_status = sns_dd_rpr0521_common_init(dd_ptr)) != SNS_DDF_SUCCESS )
  {
    return init_status;
  }

  if ( (init_status = sns_dd_rpr0521_als_init(dd_ptr)) != SNS_DDF_SUCCESS )
  {
    return init_status;
  }

  if ( (init_status = sns_dd_rpr0521_prx_init(dd_ptr)) != SNS_DDF_SUCCESS )
  {
    return init_status;
  }

  return init_status;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_powerstate

DESCRIPTION   Sets the power state of the ALS/Proximity Sensor

DEPENDENCIES  None

RETURN VALUE  TRUE on success, FALSE otherwise

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_set_powerstate
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e     sensor_type,
  sns_ddf_powerstate_e   op_mode,
  bool                init_data
)
{
  uint8_t   i2c_data = 0x0;
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t          bytes_w;

   ALSPRX_MSG_2(HIGH, "set_powerstate, sensor type %d, op_mode %d\n", sensor_type, op_mode);
   TCT_ALSPRX_MSG_2(HIGH, "set_powerstate, sensor type %d, op_mode %d\n", sensor_type, op_mode);

  if (EnableI2C != TRUE)
    return SNS_DDF_SUCCESS;

  if ( op_mode == SNS_DDF_POWERSTATE_ACTIVE )
  {
    /* Clear interrupts */
    status = sns_dd_rpr0521_clear_int_reg(dd_ptr);
    CHECK_STATUS(status);

    /* PERS */ // default is 1
    //grace modify in 2014.4.11 begin
    /* CONFIG */
    i2c_data = (LEDCURRENT_100MA | SNS_DD_ALS_GAIN_64X << 4 | SNS_DD_ALS_GAIN_64X << 2);
    dd_ptr->sns_dd_als_db.als_gain_index = SNS_DD_ALS_GAIN_64X << 2 | SNS_DD_ALS_GAIN_64X;

    status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_ALS_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
    CHECK_STATUS(status);


    //i2c_data = (SNS_DD_PS_GAIN_4X << 4) | 0x1;
   // i2c_data = (SNS_DD_PS_GAIN_2X << 4) | 0x1; //grace modify in 2014.7.22
    i2c_data = (SNS_DD_PS_GAIN_1X << 4) | 0x1;

    status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_PRX_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
    CHECK_STATUS(status);

    i2c_data =(PS_THH_BOTH_OUTSIDE| POLA_ACTIVEL | OUTPUT_LATCH | MODE_BOTH);
    status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_STATUS_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
    CHECK_STATUS(status);

#if 1   //grace modify in 2014.4.11
    /* CONTROL */
/*    i2c_data  = (BOTH100MS	//dd_ptr->enable_reg_data
    	|((sensor_type == SNS_DDF_SENSOR__ALL) ? (ALS_EN|PS_EN) : 0)		// Added 2016/1/18
    	|((sensor_type == SNS_DDF_SENSOR_PROXIMITY) ? (PS_EN) : 0)
    	|((sensor_type == SNS_DDF_SENSOR_AMBIENT) ? (ALS_EN) : 0));*/
    /*i2c_data  = (0x06	//dd_ptr->enable_reg_data
    	|((sensor_type == SNS_DDF_SENSOR__ALL) ? (0x3<<6) : 0)
    	|((sensor_type == SNS_DDF_SENSOR_PROXIMITY) ? (0x1<<6) : 0)
    	|((sensor_type == SNS_DDF_SENSOR_AMBIENT) ? (0x1<<7) : 0));*/
#endif
   //grace modify in 2014.4.11 end

// Added 2016/1/18
	i2c_data = 0x0;
	if(sensor_type == SNS_DDF_SENSOR_PROXIMITY) {
		i2c_data  = (BOTH100MS | PS_EN);
		dd_ptr->sns_dd_prx_db.enable = TRUE;
	}
	if(sensor_type == SNS_DDF_SENSOR_AMBIENT) {
		i2c_data  = (BOTH100MS | ALS_EN);
		dd_ptr->sns_dd_als_db.enable = TRUE;
	}
// Added 2016/1/18

   sns_ddf_delay(10000);
   ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_set_powerstate --> %x\n", i2c_data);
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);

    status = sns_dd_rpr0521_update_intg_time(dd_ptr, FALSE, FALSE, TRUE);
    CHECK_STATUS(status);
  }
  else /* when op_mode is SNS_DDF_POWERSTATE_LOWPOWER */
  {
    status = sns_ddf_read_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_data, 1, &bytes_w);
    CHECK_STATUS(status);

    i2c_data = i2c_data & (~ALS_EN) & (~PS_EN);  //grace modify in 2014.4.11

    if (sensor_type == SNS_DDF_SENSOR__ALL) {
        if (init_data == true)
        {
            /* cleanup */
            status = sns_dd_rpr0521_init_data_struct(dd_ptr);
          CHECK_STATUS(status);
        }
	//i2c_data = i2c_data & 0x3F; //grace modify in 2014.4.11
    }
    else if (sensor_type == SNS_DDF_SENSOR_AMBIENT) {
        if (dd_ptr->sns_dd_prx_db.enable) {
            //i2c_data = i2c_data & 0x7F; //grace modify in 2014.4.11
	     i2c_data = i2c_data |PS_EN;
        }

        dd_ptr->sns_dd_als_db.enable = false;
    }
    else { /* SNS_DDF_SENSOR_PROXIMITY */
        if (dd_ptr->sns_dd_als_db.enable) {
            //i2c_data = i2c_data & 0xBF;	//clear bit 6 //grace modify in 2014.4.11
            i2c_data = i2c_data | ALS_EN;
        }

        dd_ptr->sns_dd_prx_db.enable = false;
    }

    //if  ((!dd_ptr->dri_enabled) && ((i2c_data&0x3F) == 0)) //grace modify in 2014.4.11
    if  ((!dd_ptr->dri_enabled) && ((i2c_data& (ALS_EN|PS_EN)) == 0))
    {
      sns_dd_rpr0521_stop_general_timer( dd_ptr, sensor_type ); /* stop any pending timers */
    }
	sns_ddf_delay(10000);
   ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_set_powerstate 2 --> %x\n", i2c_data);
    status = sns_ddf_write_port(dd_ptr->port_handle,
                                SNS_DD_RPR0521_ENABLE_ADDR,
                                (uint8_t*)&i2c_data,
                                1,
                                &bytes_w);
    CHECK_STATUS(status);
    dd_ptr->enable_reg_data = i2c_data;
  }

  dd_ptr->sns_dd_rpr0521_common_db.pwr_mode = op_mode;
  dd_ptr->sns_dd_rpr0521_common_db.state = SNS_DD_DEV_CONFIGURED;

	TCT_ALSPRX_MSG_0(HIGH, "set_powerstate, SUCCESS\n");

  return status;
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_set_odr

DESCRIPTION   Sets ODR of the ALS/Proximity Sensor

DEPENDENCIES  None

RETURN VALUE  SNS_DDF_SUCCESS on success

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_rpr0521_set_odr
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e         sensor_type,
  uint32_t                 odr
)
{
  sns_ddf_status_e status=SNS_DDF_SUCCESS;
  bool             change=TRUE;

   ALSPRX_MSG_2(HIGH, "rpr0521 set odr sensor_type=%d, ODR=%d \n",sensor_type,odr);
   TCT_ALSPRX_MSG_2(HIGH, "rpr0521 set odr sensor_type=%d, ODR=%d \n",sensor_type,odr);
  switch ( sensor_type )
  {
    case SNS_DDF_SENSOR_AMBIENT:
      if (dd_ptr->sns_dd_rpr0521_common_db.als_req_odr == odr)
      {
        change = FALSE;
      }
      dd_ptr->sns_dd_rpr0521_common_db.als_req_odr = odr;
      break;

    case SNS_DDF_SENSOR_PROXIMITY:
      if (dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr == odr)
    {
        change = FALSE;
    }
      dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr = odr;
      break;

  default:
      status = SNS_DDF_EFAIL;
  }

  if (change)
  {
    sns_dd_rpr0521_set_cycle_time(dd_ptr);
  }

  return status;
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_reset

===========================================================================*/
/*!
  @brief Resets the driver and device so they return to the state they were
         in after init() was called.

  @detail
  Resets  the driver state structure

  @param[in] handle  Handle to a driver instance.

  @return Success if the driver was able to reset its state and the device.
          Otherwise a specific error code is returned.
*/
/*=========================================================================*/
static sns_ddf_status_e sns_dd_rpr0521_reset
(
  sns_ddf_handle_t dd_handle
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t *)dd_handle;
  sns_ddf_status_e init_status = SNS_DDF_SUCCESS;
  uint8_t bytes_w;
  //uint8_t i2c_data[2];
  uint8_t i2c_data; //grace modify in 2014.4.11
  sns_ddf_status_e status = SNS_DDF_SUCCESS;

   ALSPRX_MSG_0(HIGH, "rpr0521 reset\n");
  ALSPRX_MSG_1(LOW, "reset - %d", 400);
  //dd_ptr->driver_state=6;

  if ( dd_handle == NULL )
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  if ( (init_status = sns_dd_rpr0521_init_data_struct(dd_ptr)) != SNS_DDF_SUCCESS )
  {
    return init_status;
  }

#if 0  //grace modify in 2014.4.11
  //i2c_data[0] = 0x02; //grace modify in 2014.4.9
  i2c_data[0] = 0x2a;
  dd_ptr->sns_dd_als_db.als_gain_index = (i2c_data[0] & 0x3C) >> 2;
  i2c_data[1] = 0x21;
  status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_ALS_ADDR,
							  (uint8_t*)&i2c_data[0],
							  2,
							  &bytes_w);
  CHECK_STATUS(status);
  i2c_data[0] = 0x23;
  status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_STATUS_ADDR,
							  (uint8_t*)&i2c_data[1],
							  2,
							  &bytes_w);
  CHECK_STATUS(status);
#else

  i2c_data = (LEDCURRENT_100MA | SNS_DD_ALS_GAIN_64X << 4 | SNS_DD_ALS_GAIN_64X << 2);
  dd_ptr->sns_dd_als_db.als_gain_index = SNS_DD_ALS_GAIN_64X << 2 | SNS_DD_ALS_GAIN_64X;

  status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_ALS_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
  CHECK_STATUS(status);


  //i2c_data = (SNS_DD_PS_GAIN_4X << 4) | 0x1;
 //i2c_data = (SNS_DD_PS_GAIN_2X << 4) | 0x1;  //grace modify in 2014.7.22
   i2c_data = (SNS_DD_PS_GAIN_1X << 4) | 0x1;

  status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_PRX_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
  CHECK_STATUS(status);

  i2c_data =(PS_THH_BOTH_OUTSIDE| POLA_ACTIVEL | OUTPUT_LATCH | MODE_BOTH);
  status = sns_ddf_write_port(dd_ptr->port_handle,
							  SNS_DD_RPR0521_STATUS_ADDR,
							  (uint8_t*)&i2c_data,
							  1,
							  &bytes_w);
  CHECK_STATUS(status);

#endif

#ifdef FEATURE_RPR0521_INT_PIN_CONFIG
  init_status = sns_dd_isl29028_int_disable();
  ALSPRX_MSG_2(LOW, "reset - %d int disable result=%d", 0, init_status);
#endif

  if ( (init_status = sns_dd_rpr0521_set_powerstate(dd_ptr, SNS_DDF_SENSOR__ALL,
                          SNS_DDF_POWERSTATE_LOWPOWER, false)) != SNS_DDF_SUCCESS )
  {
    return init_status;
  }

  //dd_ptr->driver_state=10;

  return init_status;
}


/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_init

===========================================================================*/
/*!
  @brief Initializes the Ambient Light Sensing and Proximity device driver
   Allocates a handle to a driver instance, opens a communication port to
   associated devices, configures the driver and devices, and places
   the devices in the default power state. Returns the instance handle along
   with a list of supported sensors. This function will be called at init
   time.

   @param[out] dd_handle_ptr  Pointer that this function must malloc and
                              populate. This is a handle to the driver
                              instance that will be passed in to all other
                              functions. NB: Do not use @a memhandler to
                              allocate this memory.
   @param[in]  smgr_handle    Handle used to identify this driver when it
                              calls into Sensors Manager functions.
   @param[in]  nv_params      NV parameters retrieved for the driver.
   @param[in]  device_info    Access info for physical devices controlled by
                              this driver. Used to configure the bus
                              and talk to the devices.
   @param[in]  num_devices    Number of elements in @a device_info.
   @param[in]  memhandler     Memory handler used to dynamically allocate
                              output parameters, if applicable. NB: Do not
                              use memhandler to allocate memory for
                              @a dd_handle_ptr.
   @param[out] sensors        List of supported sensors, allocated,
                              populated, and returned by this function.
   @param[out] num_sensors    Number of elements in @a sensors.

   @return Success if @a dd_handle_ptr was allocated and the driver was
           configured properly. Otherwise a specific error code is returned.

*/
/*=========================================================================*/
static sns_ddf_status_e sns_dd_rpr0521_init
(
  sns_ddf_handle_t*        dd_ptr,
  sns_ddf_handle_t         smgr_handle,
  sns_ddf_nv_params_s*     nv_params,
  sns_ddf_device_access_s  device_info[],
  uint32_t                 num_devices,
  sns_ddf_memhandler_s*    memhandler,
  sns_ddf_sensor_e*        sensors[],
  uint32_t*                num_sensors
)
{

  sns_ddf_status_e       stat = SNS_DDF_SUCCESS;
  sns_dd_rpr0521_state_t* ptr;
  uint8_t     id_value;
  uint8_t     bytes_rw;
  sns_dd_nv_db_type* nv_ptr;

   sns_ddf_delay(50000);
   ALSPRX_MSG_0(HIGH, "rpr0521 init\n");
  TCT_ALSPRX_MSG_0(HIGH, "rpr0521 init \n");

  if ( (dd_ptr == NULL)      ||
       (num_sensors == NULL) ||
       (sensors == NULL) )
  {
    TCT_ALSPRX_MSG_0(ERROR, "rpr0521 init invalid param\n");
    return SNS_DDF_EINVALID_PARAM;
  }

  if ( sns_ddf_malloc((void **)&ptr, sizeof(sns_dd_rpr0521_state_t)) != SNS_DDF_SUCCESS )
  {
    return SNS_DDF_ENOMEM;
  }
  ptr->smgr_handle = smgr_handle;

  /* open normal port */
  if ( (stat = sns_ddf_open_port(&ptr->port_handle, &device_info->port_config))!= SNS_DDF_SUCCESS )
  {
      TCT_ALSPRX_MSG_0(ERROR, "rpr0521 init BUS init fail\n");
    sns_ddf_mfree(ptr);
    return stat;
  }

#ifndef QDSP6
  /* open additional port for zero byte write(ZBW) */
  device_info->port_config.bus_config.i2c->dev_type = SNS_DDF_I2C_DEVICE_DEFAULT;
  if ( (stat = sns_ddf_open_port(&ptr->ZBW_port_handle, &device_info->port_config))!= SNS_DDF_SUCCESS )
  {
    return stat;
  }
#endif

  *dd_ptr = (sns_ddf_handle_t)ptr;

  ptr->interrupt_gpio = device_info->first_gpio;
  ptr->dri_enabled= FALSE;
  ptr->driver_state = 1;

  /* timer for ALS */
  stat = sns_ddf_timer_init(&ptr->sns_dd_als_db.timer,
                            *dd_ptr,
                            &sns_dd_rpr0521_driver_fn_list,
                            (void *)&als_timer,
                            false);
  if ( stat != SNS_DDF_SUCCESS )
  {
    sns_ddf_mfree(ptr);
    return stat;
  }

  /* timer for PROX */
  stat = sns_ddf_timer_init(&ptr->sns_dd_prx_db.timer,
                            *dd_ptr,
                            &sns_dd_rpr0521_driver_fn_list,
                            (void *)&prx_timer,
                            false);

  if ( stat != SNS_DDF_SUCCESS )
  {
    sns_ddf_mfree(ptr);
    return stat;
  }

  /* Fill out supported sensor info */
    *num_sensors = SNS_DD_RPR0521_NUM_SENSOR_TYPES;
    *sensors = (sns_ddf_sensor_e *)sns_dd_rpr0521_sensor_types;

#if 1
if (EnableI2C == TRUE)
{
  /* Check ID Register if it is our product */
  stat = sns_ddf_read_port(ptr->port_handle,
                             SNS_DD_RPR0521_ID_ADDR,
                             (uint8_t*)&id_value,
                             1,
                             &bytes_rw);

  if ( stat != SNS_DDF_SUCCESS )
  {
    ALSPRX_MSG_1(LOW, "init - stat=%d", stat);
    TCT_ALSPRX_MSG_1(ERROR, "rpr0521 can't read sensor, I2C error = %d  !\n",stat);

    sns_ddf_timer_release(ptr->sns_dd_prx_db.timer);
    sns_ddf_timer_release(ptr->sns_dd_als_db.timer);
    sns_ddf_mfree(ptr);
    return stat;
  }

  ptr->driver_state=2;


    if (id_value != SNS_DD_RPR0521_ID)
    {
      ptr->driver_state= ((id_value<<8)| 3);
      ALSPRX_MSG_2(LOW, "init - %d id=%d", 20, id_value);
      TCT_ALSPRX_MSG_0(ERROR, "rpr0521 read wrong sensor ID\n");

      sns_ddf_close_port(ptr->port_handle);
      sns_ddf_timer_release(ptr->sns_dd_prx_db.timer);
      sns_ddf_timer_release(ptr->sns_dd_als_db.timer);
      sns_ddf_mfree(ptr);
      return SNS_DDF_EFAIL;
    }
}
#endif

  ptr->driver_state=5;

  /* Resets the driver */
  //stat = sns_dd_rpr0521_reset((sns_ddf_handle_t) ptr); /*modify Begin- by TCTSH tao-yu, PR-977004, 2015/11/25*/

  /* Recall NV parameters if any */
  if (nv_params) {
    bool data_from_registry = FALSE;

    if ( (nv_params->status == SNS_DDF_SUCCESS) && (nv_params->data_len >= sizeof(sns_dd_nv_db_type)) ) {
      nv_ptr = (sns_dd_nv_db_type *)nv_params->data;
      if ( (nv_ptr->version_num != 0) &&
           (nv_ptr->id != 0) &&
           (nv_params->data_len != 0) ) {

        // update value from NV parameters
        data_from_registry = TRUE;
        ptr->sns_dd_rpr0521_common_db.nv_size = nv_params->data_len;
        ptr->sns_dd_rpr0521_common_db.nv_db.visible_ratio = nv_ptr->visible_ratio;
        ptr->sns_dd_rpr0521_common_db.nv_db.ir_ratio = nv_ptr->ir_ratio;
        ptr->sns_dd_rpr0521_common_db.nv_db.als_factor = nv_ptr->als_factor;
        ptr->sns_dd_rpr0521_common_db.nv_db.version_num = nv_ptr->version_num;
        ptr->sns_dd_rpr0521_common_db.nv_db.id = nv_ptr->id;
        // update prx factor and thresh through this

#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
        ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = DEFAULT_CROSSTALK;
		sns_dd_rpr0521_set_prx_factor(ptr, SNS_DDF_SENSOR_PROXIMITY, ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor);
#else
        sns_dd_rpr0521_set_prx_factor(ptr, SNS_DDF_SENSOR_PROXIMITY, nv_ptr->prx_factor);
#endif

        ALSPRX_MSG_1(LOW, "prx_factor=%d", nv_ptr->prx_factor );
        // update als change pcnt
        sns_dd_rpr0521_set_als_change_pcnt(ptr, nv_ptr->als_change_pcnt);
      }
    }
    if (!data_from_registry)
    {
      // default value of NV parameters
      ptr->sns_dd_rpr0521_common_db.nv_size = 128;
      ptr->sns_dd_rpr0521_common_db.nv_db.visible_ratio = 20; // 20% visible transmittance
      ptr->sns_dd_rpr0521_common_db.nv_db.ir_ratio = 80; // 80% ir transmittance
      ptr->sns_dd_rpr0521_common_db.nv_db.dc_offset = 0; // DC offset
      ptr->sns_dd_rpr0521_common_db.nv_db.thresh_near = SNS_DD_RPR0521_PRX_NEAR_THRESHOLD;
      ptr->sns_dd_rpr0521_common_db.nv_db.thresh_far = SNS_DD_RPR0521_PRX_FAR_THRESHOLD;
#ifdef ADSP_HWCONFIG_L
	#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
      ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = DEFAULT_CROSSTALK;
	#else
      //ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = 400; //default prx_factor
      ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = 2499; /*modified by TCTSH tao-yu, PR-977004, 2015/11/25*/
	#endif
#else
	#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
      ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = DEFAULT_CROSSTALK;
	#else
      //ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = 0;
      ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor = 2499; /*modified by TCTSH tao-yu, PR-977004, 2015/11/25*/
	#endif
#endif
      ptr->sns_dd_rpr0521_common_db.nv_db.als_factor = 100; // factor of 1
      ptr->sns_dd_rpr0521_common_db.nv_db.version_num = 1;
      ptr->sns_dd_rpr0521_common_db.nv_db.id = 0x10|SNS_DDF_VENDOR_ROHM; // ?
      ptr->sns_dd_rpr0521_common_db.nv_db.als_change_pcnt = SNS_DD_RPR0521_DEFAULT_ALS_CHANGE_PCNT;
      sns_ddf_smgr_notify_event(ptr->smgr_handle, SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_EVENT_UPDATE_REGISTRY_GROUP);
      ALSPRX_MSG_1(LOW, "prx_factor=%d", ptr->sns_dd_rpr0521_common_db.nv_db.prx_factor);
    }

  }
	/* VERTU - zhangxianzhu - BUG-2477540*/
	#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
	ptr->init_ps_crosstalk=0;
	ptr->init_calib_pass=0;
	ptr->ps_data_max=0;
	ptr->ps_data_min=0xFFF;

	ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_init: init calib In\n");
	TCT_ALSPRX_MSG_0(HIGH, "sns_dd_rpr0521_init : init calib In\n");
	sns_dd_rpr0521_ps_calib(ptr);
    #endif


   TCT_ALSPRX_MSG_0(HIGH, "rpr0521 init success !\n");
     //sns_ddf_delay(50000);
   ALSPRX_MSG_0(HIGH, "rpr0521 init end\n");
  return stat;
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_set_attrib

===========================================================================*/
/*!
  @brief Sets an attribute of the ALS/Prx sensor

  @detail
  Called by SMGR to set certain device attributes that are
  programmable. Curently its the power mode, resolution and ODR.

  @param[in] dd_handle   Handle to a driver instance.
  @param[in] sensor Sensor for which this attribute is to be set.
  @param[in] attrib      Attribute to be set.
  @param[in] value      Value to set this attribute.

  @return
    The error code definition within the DDF
    SNS_DDF_SUCCESS on success; Otherwise SNS_DDF_EBUS or
    SNS_DDF_EINVALID_PARAM

*/
/*=========================================================================*/
static sns_ddf_status_e sns_dd_rpr0521_set_attrib
(
  sns_ddf_handle_t     dd_handle,
  sns_ddf_sensor_e     sensor,
  sns_ddf_attribute_e  attrib,
  void*                value
)
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;

 ALSPRX_MSG_2(HIGH, "rpr0521 set attrib sensor = %d, attr = %d\n", sensor, attrib); // VERTU - zhangxianzhu - BUG-2477540
//  ALSPRX_MSG_3(LOW, "set_attr - %d sensor=%d attr=%d", 300, sensor, attrib);
//  ALSPRX_MSG_2(LOW, "", 300, *(uint64_t *)value);
   TCT_ALSPRX_MSG_2(HIGH, "rpr0521 set attrib sensor = %d, attr = %d\n", sensor, attrib);

  if ( (dd_handle == NULL) ||
       (value == NULL) )
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  switch ( attrib )
  {
    case SNS_DDF_ATTRIB_POWER_STATE:
      /* set power mode */
      /* don't check if als or prx, either one will do */
      status = sns_dd_rpr0521_set_powerstate((sns_dd_rpr0521_state_t *)dd_handle,
                                              sensor,
                                            *(sns_ddf_powerstate_e *)value,
                                            true);

	/*modify Begin- by TCTSH tao-yu, PR-977004, 2015/11/25, p-sensor calibration*/
	CHECK_STATUS(status);
      break;

    case SNS_DDF_ATTRIB_RESOLUTION_ADC:
      /* set resolution */	//nothing will happen because we do not support resolution change
      break;

    case SNS_DDF_ATTRIB_ODR:
      /* output data rate */
      status = sns_dd_rpr0521_set_odr((sns_dd_rpr0521_state_t *)dd_handle, sensor,*(uint32_t *)value); // VERTU - zhangxianzhu - BUG-2477540
	ALSPRX_MSG_2(HIGH, "sns_dd_rpr0521_set_attrib sensor=%d ,ODR=%d \n",sensor,(*(uint32_t *)value));

      break;

    default:
      return SNS_DDF_EINVALID_PARAM;
  }

  return(status);
}

/*===========================================================================

FUNCTION      sns_dd_rpr0521_common_request_data

DESCRIPTION   As a async driver, this driver needs preparing a sensor data.
              This function calls an appropriate preparing function according to
              the data type.

DEPENDENCIES  None

RETURN VALUE  FALSE if successful, TRUE otherwise

SIDE EFFECT   None
===========================================================================*/
bool sns_dd_rpr0521_common_request_data
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_sensor_e       sensor_type
)
{
  uint8_t   i2c_data = 0x0;
  uint8_t          bytes_w;

   ALSPRX_MSG_0(HIGH, "rpr0521 common req data\n");
  /* make sure device is in the right state */
  if ( dd_ptr->sns_dd_rpr0521_common_db.state != SNS_DD_DEV_CONFIGURED )
  {
    /* device state is stopped OR invalid */
    return true;
  }

  /* Request data from device */
  if ( sensor_type == SNS_DDF_SENSOR_AMBIENT )
  {
    dd_ptr->sns_dd_rpr0521_common_db.als_get_data = true;

    if (dd_ptr->sns_dd_als_db.enable == false) {
      if (dd_ptr->sns_dd_prx_db.enable) { /* if prox already enabled */
        //i2c_data = 0x06|(0x3 << 6); //grace modify in 2014.4.11
	 i2c_data = BOTH100MS|ALS_EN |PS_EN;
      }
      else {
        //i2c_data = 0x06|(0x1 << 7); //grace modify in 2014.4.11
	 i2c_data = BOTH100MS|ALS_EN;
      }

      dd_ptr->sns_dd_als_db.enable = true;

	sns_ddf_delay(10000);
   ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_common_data --> %x\n", i2c_data);
      sns_ddf_write_port(dd_ptr->port_handle,
                       SNS_DD_RPR0521_ENABLE_ADDR,
                       (uint8_t*)&i2c_data,
                       1,
                       &bytes_w);
      dd_ptr->enable_reg_data = i2c_data;
    }

    sns_dd_rpr0521_req_data(dd_ptr, SNS_DDF_SENSOR_AMBIENT);
  }
  else /* SNS_DDF_SENSOR_PROXIMITY */
  {
    dd_ptr->sns_dd_rpr0521_common_db.prx_get_data = true;

    if (dd_ptr->sns_dd_prx_db.enable == false) {
      if (dd_ptr->sns_dd_als_db.enable) { /* if als already enabled */
        //i2c_data = 0x06|(0x3 << 6); //grace modify in 2014.4.11
	 i2c_data = BOTH100MS |ALS_EN |PS_EN;
      }
      else {
        //i2c_data = 0x06|(0x1 << 6); //grace modify in 2014.4.11
	 i2c_data = BOTH100MS|PS_EN;
      }
      dd_ptr->sns_dd_prx_db.enable = true;

	sns_ddf_delay(10000);
   ALSPRX_MSG_1(HIGH, "sns_dd_rpr0521_common_data --> %x\n", i2c_data);
      sns_ddf_write_port(dd_ptr->port_handle,
                       SNS_DD_RPR0521_ENABLE_ADDR,
                       (uint8_t*)&i2c_data,
                       1,
                       &bytes_w);
      dd_ptr->enable_reg_data = i2c_data;
    }

    sns_dd_rpr0521_req_data (dd_ptr, SNS_DDF_SENSOR_PROXIMITY);
  }

  return false;
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_get_data

===========================================================================*/
/*!
  @brief Called by the SMGR to get data

  @detail
  Requests a single sample of sensor data from each of the specified
  sensors. Data is returned immediately after being read from the
  sensor, in which case data[] is populated in the same order it was
  requested

  This driver is a pure asynchronous one, so no data will be written to buffer.
  As a result, the return value will be always PENDING if the process does
  not fail.  This driver will notify the Sensors Manager via asynchronous
  notification when data is available.

  @param[in]  dd_handle    Handle to a driver instance.
  @param[in]  sensors      List of sensors for which data is requested.
  @param[in]  num_sensors  Length of @a sensors.
  @param[in]  memhandler   Memory handler used to dynamically allocate
                           output parameters, if applicable.
  @param[out] data         Sampled sensor data. The number of elements must
                           match @a num_sensors.

  @return SNS_DDF_SUCCESS if data was populated successfully. If any of the
          sensors queried are to be read asynchronously SNS_DDF_PENDING is
          returned and data is via @a sns_ddf_smgr_data_notify() when
          available. Otherwise a specific error code is returned.

*/
/*=========================================================================*/
static sns_ddf_status_e sns_dd_rpr0521_get_data
(
  sns_ddf_handle_t        dd_handle,
  sns_ddf_sensor_e        sensors[],
  uint32_t                num_sensors,
  sns_ddf_memhandler_s*   memhandler,
  sns_ddf_sensor_data_s*  data[] /* ignored by this async driver */
)
{
  sns_ddf_status_e status = SNS_DDF_PENDING;
  uint8_t          i;

   sns_ddf_delay(50000);
   ALSPRX_MSG_0(HIGH, "rpr0521 get data\n");
   ALSPRX_MSG_3(LOW, "get_data - %d sensor=%d num=%d",
               200, sensors[0], num_sensors);

  if ( (dd_handle == NULL) || (sensors == NULL) ||
       (num_sensors < 1) || (num_sensors > 2) )
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  if (EnableI2C == FALSE)
  {
     static uint8_t toggle[2] = {0};
     sns_ddf_sensor_data_s *data_ptr;
     sns_ddf_sensor_sample_s *sample_ptr;
     int8 i;

     *data = sns_ddf_memhandler_malloc(
         memhandler, num_sensors * sizeof(sns_ddf_sensor_data_s));

     if(*data == NULL)
         return SNS_DDF_ENOMEM;

     for (i = 0; i < num_sensors; i++)
     {
       data_ptr = &((*data)[i]);

       if( (sample_ptr = sns_ddf_memhandler_malloc(memhandler,
                           (2*sizeof(sns_ddf_sensor_sample_s)))) == NULL)
       {
         return SNS_DDF_ENOMEM;
       }

       sns_ddf_delay(50);

       if (sensors[i] == SNS_DDF_SENSOR_PROXIMITY)
       {
         data_ptr->sensor = SNS_DDF_SENSOR_PROXIMITY;
         if (toggle[i])
         {
           sample_ptr[0].sample = FX_CONV_Q16(0, 0); //far = 0; near = 1
           sample_ptr[0].status = SNS_DDF_SUCCESS;
           sample_ptr[1].sample = 100; //ADC
           sample_ptr[1].status = SNS_DDF_SUCCESS;
         }
         else
         {
           sample_ptr[0].sample = FX_CONV_Q16(1, 0); //far = 0; near = 1
           sample_ptr[0].status = SNS_DDF_SUCCESS;
           sample_ptr[1].sample = 5; //ADC
           sample_ptr[1].status = SNS_DDF_SUCCESS;
         }
       }
       else
       {
         data_ptr->sensor = SNS_DDF_SENSOR_AMBIENT;
         if (toggle[i])
         {
           sample_ptr[0].sample = (int32_t)(20<<16); //20 lux
           sample_ptr[0].status = SNS_DDF_SUCCESS;
           sample_ptr[1].sample = 60; //ADC
           sample_ptr[1].status = SNS_DDF_SUCCESS;
         }
         else
         {
           sample_ptr[0].sample = (int32_t)(50<<16); //50 lux
           sample_ptr[0].status = SNS_DDF_SUCCESS;
           sample_ptr[1].sample = 150; //ADC
           sample_ptr[1].status = SNS_DDF_SUCCESS;
         }
       }
       data_ptr->status = SNS_DDF_SUCCESS;
       data_ptr->num_samples = 2;
       data_ptr->timestamp = sns_ddf_get_timestamp();
       data_ptr->samples = sample_ptr;
       toggle[i] = ~toggle[i];
     }

     return SNS_DDF_SUCCESS;
  }

  /* Look at all the requests */
  for ( i = 0; i < num_sensors; i++ )
  {
    if ( sensors[i] != SNS_DDF_SENSOR_AMBIENT &&
         sensors[i] != SNS_DDF_SENSOR_PROXIMITY )
    {
      status = SNS_DDF_EINVALID_PARAM;
      break;
    }

    if ( sns_dd_rpr0521_common_request_data((sns_dd_rpr0521_state_t *)dd_handle,
                       sensors[i]) == true )
    {
      status = SNS_DDF_EFAIL;
      break;
    }
  }

  return status;
}

/*==============================================================================

FUNCTION      sns_dd_rpr0521_prx_query

DESCRIPTION   Called by sns_dd_rpr0521_get_attrib to get an attribute value for proximitydistance data type

DEPENDENCIES  None

RETURN VALUE  Attribute value pointer on success, NULL otherwise

SIDE EFFECT   None

==============================================================================*/
sns_ddf_status_e sns_dd_rpr0521_prx_query
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_memhandler_s*  memhandler,
  sns_ddf_attribute_e    attrib,
  void**                 value,
  uint32_t*              num_elems
)
{
  uint16_t* thresh_ptr;
  uint16_t* accu_ptr;
  sns_ddf_power_info_s* power_ptr;

   ALSPRX_MSG_0(HIGH, "rpr0521 prx query\n");
  switch ( attrib )
  {
    case SNS_DDF_ATTRIB_RANGE:
    {
      sns_ddf_range_s *device_ranges;
      if( (*value = sns_ddf_memhandler_malloc(memhandler,
                      sizeof(sns_ddf_range_s)))  == NULL)
      {
          return SNS_DDF_ENOMEM;
      }
      device_ranges = *value;
      *num_elems = 1;
      device_ranges->min = FX_FLTTOFIX_Q16(0.00); /* From 0 mm*/
      device_ranges->max = FX_FLTTOFIX_Q16(0.05);  /* to 50 mm*/
      break;
    }

    case SNS_DDF_ATTRIB_RESOLUTION_ADC:
    {
      sns_ddf_resolution_adc_s* resp;
      if ( (resp = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_resolution_adc_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      resp->bit_len = 16; //grace have question
      resp->max_freq = 20;
      *value = resp;
      break;
    }

    case SNS_DDF_ATTRIB_RESOLUTION:
    {
      q16_t* resp;
      if ( (resp = sns_ddf_memhandler_malloc(memhandler, sizeof(q16_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      *resp =  SNS_DD_PRX_RES;
       *value = resp;
      break;
    }

    case SNS_DDF_ATTRIB_POWER_INFO:
      if ( (power_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_power_info_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      power_ptr->active_current = SNS_DD_PRX_PWR;
      power_ptr->lowpower_current = SNS_DD_ALS_LO_PWR;
      *(sns_ddf_power_info_s **)value = power_ptr;
      break;

    case SNS_DDF_ATTRIB_ACCURACY:
      if ( (accu_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(uint16_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      *accu_ptr = SNS_DD_PRX_ACCURACY;
      *(uint16_t **)value = accu_ptr;
      break;

    case SNS_DDF_ATTRIB_THRESHOLD:
      if ( (thresh_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(uint16_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      *thresh_ptr = dd_ptr->sns_dd_prx_db.thresh_near;
      *(uint16_t **)value = thresh_ptr;
      break;

    case SNS_DDF_ATTRIB_ODR:
    {
      uint32_t* odr_ptr;

      if ( (odr_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(uint32_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      if ( dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr > 0 )
      {
        *odr_ptr = dd_ptr->sns_dd_rpr0521_common_db.prx_odr;
      }
      else
      {
        *odr_ptr = dd_ptr->sns_dd_rpr0521_common_db.prx_req_odr;
      }
      *value = odr_ptr;
      ALSPRX_MSG_2(LOW, "prx_query - %d odr=%d", 100, *odr_ptr);
      break;
    }

    default:
      return SNS_DDF_EINVALID_ATTR;
  }
  return SNS_DDF_SUCCESS;
}

/*==============================================================================

FUNCTION      sns_dd_rpr0521_als_query

DESCRIPTION   Called by sns_dd_rpr0521_get_attrib to get an attribute value for ALS(light) data type

DEPENDENCIES  None

RETURN VALUE  Attribute value pointer on success, NULL otherwise

SIDE EFFECT   None

==============================================================================*/
sns_ddf_status_e sns_dd_rpr0521_als_query
(
  sns_dd_rpr0521_state_t* dd_ptr,
  sns_ddf_memhandler_s*  memhandler,
  sns_ddf_attribute_e    attrib,
  void**                 value,
  uint32_t*              num_elems
)
{
  sns_ddf_power_info_s* power_ptr;
  uint16_t* accu_ptr;

   ALSPRX_MSG_0(HIGH, "rpr0521 als query\n");
  switch ( attrib )
  {
    case SNS_DDF_ATTRIB_RANGE:
    {
      sns_ddf_range_s *device_ranges;
      if( (*value = sns_ddf_memhandler_malloc(memhandler,
                      sizeof(sns_ddf_range_s)))  == NULL)
      {
          return SNS_DDF_ENOMEM;
      }
      device_ranges = *value;
      *num_elems = 1;
      device_ranges->min = FX_FLTTOFIX_Q16(0.01); // open air is 0.01 Lux but with cover glass+black ink it will be 1 Lux
      device_ranges->max = FX_FLTTOFIX_Q16(10000.0);
      break;
    }
    case SNS_DDF_ATTRIB_RESOLUTION_ADC:
    {
      sns_ddf_resolution_adc_s* resp;
      if ( (resp = sns_ddf_memhandler_malloc(memhandler, 3 * sizeof(sns_ddf_resolution_adc_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }

      *num_elems = 3;

      resp[0].bit_len = 16;
      resp[0].max_freq = 20;
      *value = resp;
      break;
    }

    case SNS_DDF_ATTRIB_RESOLUTION:
    {
      sns_ddf_resolution_t* resp;
      if ( (resp = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_resolution_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;

      *resp = SNS_DD_ALS_RES;
      *value = resp;
      break;
    }

    case SNS_DDF_ATTRIB_POWER_INFO:
      if ( (power_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_power_info_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      power_ptr->active_current = SNS_DD_ALS_PWR;
      power_ptr->lowpower_current = SNS_DD_ALS_LO_PWR;
      *(sns_ddf_power_info_s **)value = power_ptr;
      break;

    case SNS_DDF_ATTRIB_ACCURACY:
      if ( (accu_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(uint16_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
     *accu_ptr = SNS_DD_ALS_ACCURACY;
      *(uint16_t **)value = accu_ptr;
      break;

    case SNS_DDF_ATTRIB_ODR:
    {
      uint32_t* odr_ptr;

      if ( (odr_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(uint32_t))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
      *num_elems = 1;
      if ( dd_ptr->sns_dd_rpr0521_common_db.als_req_odr > 0 )
      {
        *odr_ptr = dd_ptr->sns_dd_rpr0521_common_db.als_odr;
      }
      else
      {
        *odr_ptr = dd_ptr->sns_dd_rpr0521_common_db.als_req_odr;
      }
      *value = odr_ptr;
      ALSPRX_MSG_2(LOW, "als_query - %d odr=%d", 100, *odr_ptr);
      break;
    }

    default:
      return SNS_DDF_EINVALID_ATTR;
  }
  return SNS_DDF_SUCCESS;
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_get_attrib

===========================================================================*/
/*!
  @brief Called by the SMGR to retrieves the value of an attribute of
  the sensor.

  @detail
  Returns the requested attribute

  @param[in]  handle      Handle to a driver instance.
  @param[in]  sensor      Sensor whose attribute is to be retrieved.
  @param[in]  attrib      Attribute to be retrieved.
  @param[in]  memhandler  Memory handler used to dynamically allocate
                          output parameters, if applicable.
  @param[out] value       Pointer that this function will allocate or set
                          to the attribute's value.
  @param[out] num_elems   Number of elements in @a value.

  @return
    Success if the attribute was retrieved and the buffer was
    populated. Otherwise a specific error code is returned.
*/
/*=========================================================================*/

static sns_ddf_status_e sns_dd_rpr0521_get_attrib
(
  sns_ddf_handle_t     handle,
  sns_ddf_sensor_e     sensor,
  sns_ddf_attribute_e  attrib,
  sns_ddf_memhandler_s* memhandler,
  void**                value,
  uint32_t*             num_elems
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t *)handle;
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  bool generic_attrib = false;
  sns_ddf_driver_info_s *driver_info_ptr;
  sns_ddf_device_info_s *device_info_ptr;
  sns_ddf_registry_group_s *reg_group_ptr;
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-977279, 2015/12/01,grace help modify in 2015.12.02(can check email) ,for memory leak
#if 0
  uint8_t *reg_group_data_ptr;
#endif
//[BUFFIX]-Mod-End- by TCTNB.ZXZ,PR-977279, 2015/12/01

   ALSPRX_MSG_0(HIGH, "rpr0521 get attrib\n");
  ALSPRX_MSG_2(LOW, "get_attr - driver_state=%d gpio=%x",
               dd_ptr->driver_state, dd_ptr->interrupt_gpio);

  if ( (handle == NULL)      ||
       (memhandler == NULL) )
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  ALSPRX_MSG_3(LOW, "get_attr - %d sensor=%d attr=%d",
               100, sensor, attrib);

  /* check first if the query is for generic attributes */
  switch ( attrib )
  {
    case SNS_DDF_ATTRIB_DRIVER_INFO:
      if ( (driver_info_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_driver_info_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }

      driver_info_ptr->name = "RPR0521 Proximity & Light";
      driver_info_ptr->version = 2;
      *(sns_ddf_driver_info_s**)value = driver_info_ptr;
      *num_elems = 1;
      generic_attrib = true;
      break;

    case SNS_DDF_ATTRIB_DEVICE_INFO:
      if ( (device_info_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_device_info_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }

      device_info_ptr->name = "Proximity & Light";
      device_info_ptr->vendor = "ROHM";
      device_info_ptr->model = "RPR0521";
      device_info_ptr->version = 2;
      *(sns_ddf_device_info_s**)value = device_info_ptr; /* Is memcopy needed instead? */
      *num_elems = 1;
      generic_attrib = true;
      break;

    case SNS_DDF_ATTRIB_REGISTRY_GROUP:
      if ( (reg_group_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_ddf_registry_group_s))) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }
//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-977279, 2015/12/01, grace help modify in 2015.12.02(can check email) ,for memory leak
#if 0
    if ( (reg_group_data_ptr = sns_ddf_memhandler_malloc(memhandler, dd_ptr->sns_dd_rpr0521_common_db.nv_size)) == NULL )
      {
        return SNS_DDF_ENOMEM;
      }

      SNS_OS_MEMCOPY(reg_group_data_ptr, &dd_ptr->sns_dd_rpr0521_common_db.nv_db, sizeof(sns_dd_nv_db_type));

      reg_group_ptr->group_data = reg_group_data_ptr;
#endif

      reg_group_ptr->group_data = (uint8_t*)&dd_ptr->sns_dd_rpr0521_common_db.nv_db;
//[BUFFIX]-Mod-End- by TCTNB.ZXZ,PR-977279, 2015/12/01

      reg_group_ptr->size = dd_ptr->sns_dd_rpr0521_common_db.nv_size;
      *(sns_ddf_registry_group_s**)value = reg_group_ptr;
      ALSPRX_MSG_2(LOW, "get_attrib - size=%d data=%d",
                   reg_group_ptr->size, (uint8_t) *reg_group_ptr->group_data);

      ALSPRX_MSG_3(LOW, "get_attrib - ratio=%d factor=%d id=%d",
                   dd_ptr->sns_dd_rpr0521_common_db.nv_db.visible_ratio,
                   dd_ptr->sns_dd_rpr0521_common_db.nv_db.als_factor,
                   dd_ptr->sns_dd_rpr0521_common_db.nv_db.id );

      *num_elems = 1;
      generic_attrib = true;
      break;

    default:
      /* do nothing */
      break;
  }

  /* The query is not for generic attribute but is for specific data type */
  if ( generic_attrib != true )
  {
    switch ( sensor )
    {
      case SNS_DDF_SENSOR_AMBIENT:
        status = sns_dd_rpr0521_als_query(dd_ptr, memhandler, attrib, value, num_elems);
        break;

      case SNS_DDF_SENSOR_PROXIMITY:
        status = sns_dd_rpr0521_prx_query(dd_ptr, memhandler, attrib, value, num_elems);
        break;

      default:
        status = SNS_DDF_EINVALID_ATTR;
    }
  }

  return status;
}

/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_calibration

===========================================================================*/
/*!
  @ Called by the SMGR to run calibration test for ALS or PRX.

  @detail
  Returns the cal_factor in error, to be stored in NV memory

  @param[in]  handle      Handle to a driver instance.
  @param[in]  sensor_type Sensor whose attribute is to be retrieved.
  @param[in] test         Test ID used to decide what to test.
  @param[out] err         prx_factor or als_factor.

  @return
    Success if no error. Otherwise a specific error code is returned.
*/
/*=========================================================================*/

static sns_ddf_status_e sns_dd_rpr0521_calibration
(
  sns_ddf_handle_t    handle,
  sns_ddf_sensor_e    sensor_type,
  sns_ddf_test_e      test,
  uint32_t*           err
)
{
  sns_dd_rpr0521_state_t* dd_ptr = (sns_dd_rpr0521_state_t *)handle;
  sns_ddf_status_e status = SNS_DDF_PENDING;
  //sns_ddf_status_e init_status = SNS_DDF_EFAIL;
  uint8_t   i2c_data = 0x0;
  uint8_t   bytes_w;

   ALSPRX_MSG_1(HIGH, "rpr0521 calib %d\n", sensor_type);
  if (handle == NULL)
  {
    return SNS_DDF_ENOMEM;
  }

  ALSPRX_MSG_2(LOW, "calib - prx data %d %d\r\n", sensor_type, test);
  /* check what is the test type */
  switch ( test )
  {
    case SNS_DDF_TEST_SELF:
    case SNS_DDF_TEST_OEM:

      if (sensor_type == SNS_DDF_SENSOR_PROXIMITY) {
#ifdef FEATURE_RPR0521_INT_PIN_CONFIG
          init_status = sns_dd_isl29028_int_disable();
          ALSPRX_MSG_2(LOW, "calib - %d prox dis stat=%d", 0, init_status);
#endif
          /*init_status = sns_dd_rpr0521_set_powerstate(dd_ptr, SNS_DDF_SENSOR__ALL,
                                                      SNS_DDF_POWERSTATE_ACTIVE, false);*/ //grace modify in 2014.4.11

	   /*init_status = */sns_dd_rpr0521_set_powerstate(dd_ptr, SNS_DDF_SENSOR_PROXIMITY,
                                                      SNS_DDF_POWERSTATE_ACTIVE, false);
          if (dd_ptr->sns_dd_prx_db.enable == false) {
		ALSPRX_MSG_0(HIGH, "prx data disable\r\n");
            dd_ptr->sns_dd_prx_db.enable = true;
            //i2c_data = 0x46;//dd_ptr->enable_reg_data|0x40;
            i2c_data = BOTH100MS | PS_EN;//grace modify in 2014.4.11;

            sns_ddf_write_port(dd_ptr->port_handle,
                             SNS_DD_RPR0521_ENABLE_ADDR,
                             (uint8_t*)&i2c_data,
                             1,
                             &bytes_w);

          }
          /*else if (dd_ptr->sns_dd_rpr0521_common_db.prx_get_data == true) {

            return SNS_DDF_EDEVICE_BUSY;
          }*/

          dd_ptr->sns_dd_rpr0521_common_db.prx_calibration = true;
          dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_loop = 0;
          dd_ptr->sns_dd_rpr0521_common_db.prx_calibration_pdata = 0;

          ALSPRX_MSG_1(HIGH, "prx data %d\r\n", 100);

          sns_dd_rpr0521_req_data (dd_ptr, sensor_type);
          //sns_dd_rpr0521_common_request_data(dd_ptr, sensor_type);
      }
      else if (sensor_type == SNS_DDF_SENSOR_AMBIENT) {
#ifdef FEATURE_RPR0521_INT_PIN_CONFIG
        init_status = sns_dd_isl29028_int_disable();
        ALSPRX_MSG_2(LOW, "calib - %d als dis stat=%d", 0, init_status);
#endif

        /*init_status = sns_dd_rpr0521_set_powerstate(dd_ptr, SNS_DDF_SENSOR__ALL,
                                                    SNS_DDF_POWERSTATE_ACTIVE, false);*/ //grace modify in 2014.4.11

	 /*init_status = */sns_dd_rpr0521_set_powerstate(dd_ptr, SNS_DDF_SENSOR_AMBIENT,
                                                    SNS_DDF_POWERSTATE_ACTIVE, false);

        if (dd_ptr->sns_dd_als_db.enable == false) {

          dd_ptr->sns_dd_als_db.enable = true;
          //i2c_data = 0x86; //dd_ptr->enable_reg_data|0x80;
          i2c_data = BOTH100MS | ALS_EN;//grace modify in 2014.4.11;

          sns_ddf_write_port(dd_ptr->port_handle,
                           SNS_DD_RPR0521_ENABLE_ADDR,
                           (uint8_t*)&i2c_data,
                           1,
                           &bytes_w);
        }
        else if (dd_ptr->sns_dd_rpr0521_common_db.als_get_data == true) {
          return SNS_DDF_EDEVICE_BUSY;
        }

        dd_ptr->sns_dd_rpr0521_common_db.als_calibration = true;
        sns_dd_rpr0521_req_data (dd_ptr, sensor_type);
        //sns_dd_rpr0521_common_request_data(dd_ptr, sensor_type);
      }

      break;

    default:
      /* do nothing */
      return SNS_DDF_EINVALID_TEST;
  }

  return status;
}
/*===========================================================================

  FUNCTION:   sns_dd_rpr0521_probe

===========================================================================*/
/**
 * @brief Probes for the device with a given configuration.
 *
 * This commands the driver to look for the device with the specified
 * configuration (ie, I2C address/bus defined in the sns_ddf_device_access_s
 * struct.
 *
 * @param[in]  dev_info    Access info for physicol devices controlled by
 *                         this driver. Used to determine if the device is
 *                         physically present.
 * @param[in]  memhandler  Memory handler used to dynamically allocate
 *                         output parameters, if applicable.
 * @param[out] num_sensors Number of sensors supported. 0 if none.
 * @param[out] sensor_type Array of sensor types supported, with num_sensor
 *                         elements. Allocated by this function.
 *
 * @return SNS_DDF_SUCCESS if the part was probed function completed, even
 *         if no device was found (in which case num_sensors will be set to
 *         0).
 */
static sns_ddf_status_e sns_dd_rpr0521_probe
(
 sns_ddf_device_access_s* device_info,
 sns_ddf_memhandler_s*    memhandler,
 uint32_t*                num_sensors,
 sns_ddf_sensor_e**       sensors
)
{
  sns_ddf_status_e status;
  sns_ddf_handle_t port_handle;
  uint8_t i2c_buff;
  uint8_t bytes_read;

  *num_sensors = 0;
  *sensors = NULL;

   sns_ddf_delay(50000);
   ALSPRX_MSG_0(HIGH, "rpr0521 probe\n");
  status = sns_ddf_open_port(&port_handle, &(device_info->port_config));
  if(status != SNS_DDF_SUCCESS)
  {
    return status;
  }

  /* Read & Verify Device ID */
  status = sns_ddf_read_port( port_handle,
                              SNS_DD_RPR0521_ID_ADDR,
                              &i2c_buff,
                              1,
                              &bytes_read );
   sns_ddf_delay(50000);
   ALSPRX_MSG_1(HIGH, "rpr0521 probe,ID = %d\n",i2c_buff);
  if(status != SNS_DDF_SUCCESS || bytes_read != 1)
  {
    sns_ddf_close_port( port_handle );
    return status;
  }
  if( i2c_buff != SNS_DD_RPR0521_ID )
  {
    /* Incorrect value. Return now with nothing detected */
    sns_ddf_close_port( port_handle );
    return SNS_DDF_SUCCESS;
  }

  /* ID is correct. This could be an RPR0521 */
  *num_sensors = 2;
  *sensors = sns_ddf_memhandler_malloc( memhandler,
                                        sizeof(sns_ddf_sensor_e) * *num_sensors );
  if( *sensors != NULL )
  {
    (*sensors)[0] = SNS_DDF_SENSOR_PROXIMITY;
    (*sensors)[1] = SNS_DDF_SENSOR_AMBIENT;
    status = SNS_DDF_SUCCESS;
  } else {
    status = SNS_DDF_ENOMEM;
  }
     sns_ddf_delay(50000);
   ALSPRX_MSG_1(HIGH, "rpr0521 probe, status = %d\n",status);
    sns_ddf_close_port( port_handle );
  return status;
}


