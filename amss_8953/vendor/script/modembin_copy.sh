#!/bin/bash

if [ -z $1 ] ; then
    echo "Didn't specify cpu chip type, exiting now ..."
    exit 1
fi

CPU_CHIP_TYPE=$1
buildid_8953=(JAASANAZ 8953.genw3k.prod 8953 SANAANAA)

if [ "$CPU_CHIP_TYPE" == "8953" ] ; then
    buildid=(${buildid_8953[@]})
    NON_CPU_CHIP_TYPE=$CPU_CHIP_TYPE
else
    echo "CPU chip type error, exiting now..."
    exit 1    
fi

Boot_BuildId=${buildid[0]}
Modem_BuildId=${buildid[1]}
Rpm_BuildId=${buildid[2]}
Tz_BuildId=${buildid[3]}

TopPath=$(pwd)

CommBPath=$TopPath/MSM8953.LA.2.0/common/build
if [ "$CPU_CHIP_TYPE" == "8953" ] ; then
    CommOPath=$TopPath/MSM8953.LA.2.0/common/build/bin/asic
else
    echo "CPU chip type error, exiting now..."
    exit 1
fi
BootOPath=$TopPath/BOOT.BF.3.3/boot_images/build/ms/bin/$Boot_BuildId
RpmOPath=$TopPath/RPM.BF.2.4/rpm_proc/build/ms/bin/$CPU_CHIP_TYPE
TzOPath=$TopPath/TZ.BF.4.0.5/trustzone_images/build/ms/bin/$Tz_BuildId
AdspOPath=$TopPath/ADSP.8953.2.8.2/adsp_proc/build/dynamic_signed/$CPU_CHIP_TYPE
CommTlPath=$TopPath/MSM8953.LA.2.0/common/sectools/resources/build/fileversion2/
########--------prepareimage-------############
if [ -f $BootOPath/sbl1.mbn ] &&
   [ -f $RpmOPath/rpm.mbn ] &&
   [ -f $TzOPath/tz.mbn ] &&
   [ -f $TzOPath/cmnlib.mbn ] &&
   [ -f $TzOPath/keymaster.mbn ] &&
   [ -f $AdspOPath/adspso.bin ] &&
   [ -f $CommTlPath/sec.dat ] &&
   [ -f $CommOPath/NON-HLOS.bin ] ; then
    pushd $CommBPath > /dev/null
    cp $BootOPath/sbl1.mbn ./sbl1.mbn
    cp $RpmOPath/rpm.mbn ./rpm.mbn
    cp $TzOPath/tz.mbn ./tz.mbn
    cp $TzOPath/keymaster.mbn ./keymaster.mbn
    cp $TzOPath/cmnlib.mbn ./cmnlib.mbn
    cp $TzOPath/cmnlib64.mbn ./cmnlib64.mbn
    cp -f $TzOPath/devcfg.mbn ./devcfg.mbn
    cp -f $TzOPath/lksecapp.mbn ./lksecapp.mbn
    cp $AdspOPath/adspso.bin ./adspso.bin
    cp $CommTlPath/sec.dat ./sec.dat
    cp $CommOPath/NON-HLOS.bin ./NON-HLOS.bin
    #copy elf files to common/build folder
    cp $TopPath/MPSS.TA.2.3/modem_proc/build/ms/orig_MODEM_PROC_IMG_${Modem_BuildId}Q.elf ./orig_MODEM_PROC_IMG_${Modem_BuildId}Q.elf
    cp $TopPath/BOOT.BF.3.3/boot_images/core/bsp/bootloaders/sbl1/build/${Boot_BuildId}/SBL1_ASIC.elf ./SBL1_ASIC.elf
    cp $TopPath/RPM.BF.2.4/rpm_proc/core/bsp/rpm/build/${CPU_CHIP_TYPE}/RPM_AAAAANAAR_rpm.elf ./RPM_AAAAANAAR_rpm.elf
    cp $TopPath/TZ.BF.4.0.5/trustzone_images/core/bsp/monitor/build/$Tz_BuildId/mon.elf ./mon.elf
    cp $TopPath/TZ.BF.4.0.5/trustzone_images/core/bsp/qsee/build/$Tz_BuildId/qsee.elf ./qsee.elf
    cp $TopPath/CNSS.PR.4.0/wcnss_proc/build/ms/${CPU_CHIP_TYPE}_PRONTO_MR.elf ./${CPU_CHIP_TYPE}_PRONTO_MR.elf
    qsr4file=($(ls $TopPath/MPSS.TA.2.3/modem_proc/build/myps/qshrink | grep -E "*.qsr4"))
    if [ ${#qsr4file[@]} -ge 1 ] ; then
        for file in ${qsr4file[@]}
        do
            cp $TopPath/MPSS.TA.2.3/modem_proc/build/myps/qshrink/$file ./$file
        done
    fi
    if [ -f "$TopPath/ADSP.8953.2.8.2/adsp_proc/build/ms/M${CPU_CHIP_TYPE}AAAAAAAAQ1234.elf" ] ; then
        cp "$TopPath/ADSP.8953.2.8.2/adsp_proc/build/ms/M${CPU_CHIP_TYPE}AAAAAAAAQ1234.elf" ./M${CPU_CHIP_TYPE}AAAAAAAAQ1234.elf
    fi
    if [ -f "$TopPath/ADSP.8953.2.8.2/adsp_proc/build/ms/M${CPU_CHIP_TYPE}AAAAAAAAQ1234_SENSOR.elf" ] ; then
        cp "$TopPath/ADSP.8953.2.8.2/adsp_proc/build/ms/M${CPU_CHIP_TYPE}AAAAAAAAQ1234_SENSOR.elf" ./M${CPU_CHIP_TYPE}AAAAAAAAQ1234_SENSOR.elf
    fi
    if [ -f "$TopPath/TZ.BF.4.0.5/trustzone_images/core/bsp/hypervisor/build/$Tz_BuildId/hyp.elf" ]; then
        cp $TopPath/TZ.BF.4.0.5/trustzone_images/core/bsp/hypervisor/build/$Tz_BuildId/hyp.elf ./hyp.elf
    fi
    popd > /dev/null
else
    echo "==================== Can't find one of the target files,please check ======================"
    exit 0
fi
