#===============================================================================
#
# DAL CONFIG Lib
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/dev/tz.bf/4.0/abhianan.TZ.BF.4.0.DEVCFGFWK_HYPERVISOR/trustzone_images/core/dal/config/build/SConscript#1 $
#  $DateTime: 2014/09/18 12:22:25 $
#  $Author: abhianan $
#  $Change: 6620110 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when         who     what, where, why
# --------     ---     ---------------------------------------------------------
# 11/24/15   sg      Add support for multi device config
# 10/31/2014   aa      Create
#
#===============================================================================
Import('env')
import os, glob
env = env.Clone()

#-------------------------------------------------------------------------------
# Load dal config builders
#-------------------------------------------------------------------------------
env.Tool('dalconfig_builder', toolpath = ['.'])
env.Tool('devcfgHwInfo_builder', toolpath = ['.'])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${DAL_ROOT}/config"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External paths, NOTE: DALConfig is a special case as it may require any
# ".h" file which may not be a public API
#-------------------------------------------------------------------------------

EXTERNAL_API = [
   'MODEM_PMIC',                  #pm.h
   'MODEM_API',
]
env.RequireExternalApi(EXTERNAL_API)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'HAL',
   'BUSES',
   'HWENGINES',
   'SYSTEMDRIVERS',
   'SYSTEMDRIVERS_DALCONFIG',
   'DEBUGTOOLS',
   'SERVICES',
   'APTTESTS',
   'KERNEL_MEMORYMANAGER',
   'KERNEL'
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
if env.GetUsesFlag('USES_DEVCFG') is True:
   #Generate required dalsystem xml files for each SOC
   soc_list = []
   devcfg_xml_tag_list = env.get('DEVCFG_XML_TAGS')
   for tag in devcfg_xml_tag_list:
       soc = tag.split('_')[0]
       if soc not in soc_list:
           soc_list.append(soc)
   
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   
   env.AddDevCfgInfo(DEVCFG_IMG,
   {
      'soc_xml' : ['${BUILD_ROOT}/core/dal/config/tz/dalsystem_tz.xml']
   })
   # Provide info about dalsystem_modem xml files to the build system
   
   # Rules for dal config c files
   if 'DAL_DEVCFG_IMG' in env:
      DEVCFG_SOURCES =  []
      # Create the master xml files
      DALConfig_out = {}
      DALConfig_cfile = {}
      # Generate the master xml files for only those tags that have been used in the build
      # passed through to the build system using AddDevCfgInfo. Ignore the unused tafs in 
      # image_cfg.xml

      # Create one master xml file per tag
      for tag in devcfg_xml_tag_list:
         appendstr = tag.replace('_xml', '')
         devcfg_gen_xml = '${BUILDPATH}/DevCfg_master_' + appendstr + '.xml'
         devcfg_gen_env = '${BUILDPATH}/DevCfg_env_' + appendstr + '.txt'
         DALConfig_out[tag] = env.DevcfgBuilder([devcfg_gen_xml, devcfg_gen_env] , None, CONFIG=[tag], DATA_COLLECT='DAL_DEVCFG_IMG', UPDATE_ENV=True)
         if (os.path.exists(env.RealPath(devcfg_gen_env))) :
            lines = [line.strip() for line in open(env.RealPath(devcfg_gen_env), 'r')]
            env.get('CPPPATH').extend(lines)        


      # Create a dictionary with the following format: {'8026_PLATFORM_OEM_xml': {'DALConfig_8026_PLATFORM_OEM': '8026_PLATFORM_OEM_data'}
      dal_gen_src_dict = {}
      for word in devcfg_xml_tag_list:
         dal_gen_src_dict[word] = {('${BUILDPATH}/DALConfig_' + word.replace('_xml', '')) : ('${BUILDPATH}/' + word.replace('_xml', '_data'))}
      
      # Create DalConfig file
      dalconfig_libs = []
      # DALConfig & data file generation
      DALConfig_src = []
      for xml_tag, dalconfig_files_dict in dal_gen_src_dict.items():
         for config_file, data_file in dalconfig_files_dict.items():
            DALConfig_src = env.DALConfigSrcBuilder([config_file+'.c', data_file+'.c'], [DALConfig_out[xml_tag][0]], CONFIG=xml_tag)
            dalconfig_libname = '${BUILDPATH}/' + 'DALConfig_' + xml_tag
            devcfg_gen_env = '${BUILDPATH}/DevCfg_env_' + xml_tag.replace('_xml', '') + '.txt'
            if (os.path.exists(env.RealPath(devcfg_gen_env))) :
               lines = [line.strip() for line in open(env.RealPath(devcfg_gen_env), 'r')]
               env.get('CPPPATH').extend(lines)        
               
            env.AddLibrary(DEVCFG_IMG, dalconfig_libname, DALConfig_src[0])
            dalconfig_libs.append(dalconfig_libname)
            # Devcfg data file object
            env.AddObject(DEVCFG_IMG, DALConfig_src[1])
   # Create DALModDir file
   dal_gen_src = ['${BUILDPATH}/DALModDir_tz.c']
      
   # Devcfg master xml file
   dalmoddir_gen_xml = '${BUILDPATH}/DevCfg_master.xml'
   dalmoddir_gen_env = '${BUILDPATH}/DevCfg_env.txt'
   dalmoddir_xml_tag_list = env.get('DEVCFG_XML_TAGS')
   DALModDir_out = env.DevcfgBuilder([dalmoddir_gen_xml,dalmoddir_gen_env], None, CONFIG=dalmoddir_xml_tag_list, DATA_COLLECT='DAL_DEVCFG_IMG', UPDATE_ENV=False)
   
   # FIX ME
   DALModDir_src = env.DALModDirSrcBuilder(dal_gen_src, [DALModDir_out[0]])
   DEVCFG_SOURCES.extend(DALModDir_src) 
   env.AddLibrary(DEVCFG_IMG, '${BUILDPATH}/DALModDir', DEVCFG_SOURCES)
   
   dal_ddi_src = ['${INC_ROOT}/core/api/systemdrivers/Chipinfo.h', '${INC_ROOT}/core/api/systemdrivers/PlatformInfoDefs.h' ]
   dal_ddi_file = ['${BUILDPATH}/DevCfgSegInfo.c']
   DevCfgSegInfo_src = env.DevcfgPropInfoBuilder(dal_ddi_file, dal_ddi_src)
   env.AddObject(DEVCFG_IMG, DevCfgSegInfo_src[0])
   tcsr_soc_hw_ver_addr = env.get('DEVCFG_TCSR_SOC_HW_ADDR')
   env.Append(CPPDEFINES = ['DEVCFG_TCSR_SOC_HW_ADDR=' + tcsr_soc_hw_ver_addr])  
