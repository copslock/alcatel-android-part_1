#include "DALSysInt.h" 

#include "DALSysTypes.h" 


extern unsigned int DALPROP_PropsInfo_8937_xml_tz;
extern unsigned int DALPROP_PropsInfo_8917_xml_tz;

DEVCFG_TARGET_INFO devcfg_target_soc_info_tz[ ] =
{
	{ 8202, (DALProps *)&DALPROP_PropsInfo_8937_xml_tz, NULL, 0},
	{ 8207, (DALProps *)&DALPROP_PropsInfo_8937_xml_tz, NULL, 0},
	{ 8204, (DALProps *)&DALPROP_PropsInfo_8917_xml_tz, NULL, 0},
	{ 8208, (DALProps *)&DALPROP_PropsInfo_8917_xml_tz, NULL, 0},
	{0, NULL, NULL, 0}
};
