typedef unsigned long uintptr_t;
typedef long intptr_t;
typedef signed char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long int64_t;
typedef long intmax_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;
typedef unsigned long uintmax_t;
typedef int8_t int_fast8_t;
typedef int64_t int_fast64_t;
typedef int8_t int_least8_t;
typedef int16_t int_least16_t;
typedef int32_t int_least32_t;
typedef int64_t int_least64_t;
typedef uint8_t uint_fast8_t;
typedef uint64_t uint_fast64_t;
typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;
typedef uint32_t uint_least32_t;
typedef uint64_t uint_least64_t;
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
typedef unsigned char boolean;
typedef uint32_t bool32;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;
typedef unsigned long uintnt;
typedef uint8_t byte;
typedef unsigned short word;
typedef unsigned int dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed int sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
      typedef long long int64;
      typedef unsigned long long uint64;
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
    extern dword rex_fiq_lock(void);
    extern void rex_fiq_free(void);
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
typedef enum TzBspBlspPeripheralId TzBspBlspPeripheralId;
enum TzBspBlspPeripheralId
{
   BLSP_QUP_1,
   BLSP_QUP_2,
   BLSP_QUP_3,
   BLSP_QUP_4,
   BLSP_QUP_5,
   BLSP_QUP_6,
   BLSP_QUP_7,
   BLSP_QUP_8,
   BLSP_QUP_9,
   BLSP_QUP_10,
   BLSP_QUP_11,
   BLSP_QUP_12,
   BLSP_QUP_END,
   BLSP_UART_START=15,
   BLSP_UART_1 = BLSP_UART_START,
   BLSP_UART_2,
   BLSP_UART_3,
   BLSP_UART_4,
   BLSP_UART_5,
   BLSP_UART_6,
   BLSP_UART_7,
   BLSP_UART_8,
   BLSP_UART_9,
   BLSP_UART_10,
   BLSP_UART_11,
   BLSP_UART_12,
   BLSP_INVALID_ID = 0xFFFFFFFF,
};
typedef enum TzBspBlspProtocol TzBspBlspProtocol;
enum TzBspBlspProtocol
{
   PROTOCOL_SPI,
   PROTOCOL_I2C,
   PROTOCOL_UART_2_LINE,
   PROTOCOL_UART_4_LINE
};
 int tzbsp_blsp_initaccess(void);
int tzbsp_blsp_modify_ownership(uint32 peripheralId, uint32 owner_ss);
typedef enum
{
  DALPLATFORMINFO_TYPE_UNKNOWN = 0x00,
  DALPLATFORMINFO_TYPE_SURF = 0x01,
  DALPLATFORMINFO_TYPE_FFA = 0x02,
  DALPLATFORMINFO_TYPE_FLUID = 0x03,
  DALPLATFORMINFO_TYPE_FUSION = 0x04,
  DALPLATFORMINFO_TYPE_OEM = 0x05,
  DALPLATFORMINFO_TYPE_QT = 0x06,
  DALPLATFORMINFO_TYPE_CDP = DALPLATFORMINFO_TYPE_SURF,
  DALPLATFORMINFO_TYPE_MTP = 0x08,
  DALPLATFORMINFO_TYPE_MTP_MDM = DALPLATFORMINFO_TYPE_MTP,
  DALPLATFORMINFO_TYPE_MTP_MSM = DALPLATFORMINFO_TYPE_MTP,
  DALPLATFORMINFO_TYPE_LIQUID = 0x09,
  DALPLATFORMINFO_TYPE_DRAGONBOARD = 0x0A,
  DALPLATFORMINFO_TYPE_QRD = 0x0B,
  DALPLATFORMINFO_TYPE_EVB = 0x0C,
  DALPLATFORMINFO_TYPE_HRD = 0x0D,
  DALPLATFORMINFO_TYPE_DTV = 0x0E,
  DALPLATFORMINFO_TYPE_RUMI = 0x0F,
  DALPLATFORMINFO_TYPE_VIRTIO = 0x10,
  DALPLATFORMINFO_TYPE_GOBI = 0x11,
  DALPLATFORMINFO_TYPE_CBH = 0x12,
  DALPLATFORMINFO_TYPE_BTS = 0x13,
  DALPLATFORMINFO_TYPE_XPM = 0x14,
  DALPLATFORMINFO_TYPE_RCM = 0x15,
  DALPLATFORMINFO_TYPE_DMA = 0x16,
  DALPLATFORMINFO_TYPE_STP = 0x17,
  DALPLATFORMINFO_TYPE_SBC = 0x18,
  DALPLATFORMINFO_TYPE_ADP = 0x19,
  DALPLATFORMINFO_TYPE_CHI = 0x1A,
  DALPLATFORMINFO_TYPE_SDP = 0x1B,
  DALPLATFORMINFO_TYPE_RRP = 0x1C,
  DALPLATFORMINFO_NUM_TYPES,
  DALPLATFORMINFO_TYPE_32BITS = 0x7FFFFFFF
} DalPlatformInfoPlatformType;
typedef enum
{
  DALPLATFORMINFO_KEY_UNKNOWN = 0x00,
  DALPLATFORMINFO_KEY_DDR_FREQ = 0x01,
  DALPLATFORMINFO_KEY_GFX_FREQ = 0x02,
  DALPLATFORMINFO_KEY_CAMERA_FREQ = 0x03,
  DALPLATFORMINFO_KEY_FUSION = 0x04,
  DALPLATFORMINFO_KEY_CUST = 0x05,
  DALPLATFORMINFO_KEY_NAND_SCRUB = 0x06,
  DALPLATFORMINFO_NUM_KEYS,
  DALPLATFORMINFO_KEY_32BITS = 0x7FFFFFFF
} DalPlatformInfoKeyType;
typedef struct
{
  DalPlatformInfoPlatformType platform;
  uint32 version;
  uint32 subtype;
  boolean fusion;
} DalPlatformInfoPlatformInfoType;
typedef struct DalPlatformInfoSMemPMICType
{
  uint32 nPMICModel;
  uint32 nPMICVersion;
} DalPlatformInfoSMemPMICType;
typedef struct
{
  uint32 nFormat;
  uint32 eChipId;
  uint32 nChipVersion;
  char aBuildId[32];
  uint32 nRawChipId;
  uint32 nRawChipVersion;
  DalPlatformInfoPlatformType ePlatformType;
  uint32 nPlatformVersion;
  uint32 bFusion;
  uint32 nPlatformSubtype;
  DalPlatformInfoSMemPMICType aPMICInfo[3];
  uint32 nFoundryId;
  uint32 nChipSerial;
  uint32 nNumPMICs;
  uint32 nPMICArrayOffset;
} DalPlatformInfoSMemType;
<driver name="NULL">
   <global_def>
      <var_seq name="PLATFORMS" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         DALPLATFORMINFO_TYPE_QRD, 0x0,
         end
      </var_seq>
   </global_def>
   <device id="ALTERNATE_PLAT_GLOBAL_PROP">
      <props name="ALTERNATE_PLATFORMS" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> PLATFORMS </props>
   </device>
</driver>
<driver name="NULL">
   <device id="/dev/buses/qup/blsp_qup_3_alt1">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_3 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_I2C </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 10, 11, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 3 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 11 </props>
   </device>
</driver>
