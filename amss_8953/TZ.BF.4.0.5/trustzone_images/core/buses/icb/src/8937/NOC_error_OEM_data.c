/*==============================================================================

FILE:      NOC_error_OEM_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.tz/1.0.5/buses/icb/src/8937/NOC_error_OEM_data.c#1 $ 
$DateTime: 2016/03/24 12:08:26 $
$Author: pwbldsvc $
$Change: 10135604 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2015/09/15  ddk   Disabling error fatal for bringup.
2015/05/07  tb     Port to 8952
2014/12/09  tb     Created
 
        Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "NOC_error.h"
#include "NOC_error_HWIO.h"

/*============================================================================
                           TARGET SPECIFIC DATA
============================================================================*/

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/* OEM NOC Configuration Data*/
const static NOCERR_info_type_oem NOCERR_cfgdata_oem[] = 
{ 
//NOCERR_INFO_OEM(name,               intr_en, intr_fatal)
  NOCERR_INFO_OEM(PCNOC,              TRUE,    TRUE),  /* Peripheral/Config NOC */
  NOCERR_INFO_OEM(SNOC,               TRUE,    TRUE),  /* System NOC */
};

const NOCERR_propdata_type_oem NOCERR_propdata_oem =
{
    /* Length of the config data array */
    sizeof(NOCERR_cfgdata_oem)/sizeof(NOCERR_info_type_oem), 
    /* Pointer to OEM config data array */ 
    NOCERR_cfgdata_oem,
};
