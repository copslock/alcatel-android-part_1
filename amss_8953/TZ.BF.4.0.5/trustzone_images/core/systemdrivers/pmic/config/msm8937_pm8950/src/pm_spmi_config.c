/*! \file
*  \n
*  \brief  pm_spmi_config.c
*  \n
*  \n This file contains pmic configuration data specific for SPMI Controller's
      Peripheral for MSM8996 device..
*  \n
*  \n &copy; Copyright 2014 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.tz/1.0.5/systemdrivers/pmic/config/msm8937_pm8950/src/pm_spmi_config.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/15   vtw    Updated for V2 support.
03/02/15   vtw    Updated PVC port master numbers.
07/16/13   vtw    Created.
========================================================================== */

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/

#include "pm_spmi.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

/* Converts a PVC port index to an overall PMIC Arbiter port index */
#define PVC_PORT_TO_PMIC_ARB_PORT(p) (p+1)

/*   Port assignment in 8952  */
#define KRAIT_L2_PORT   2   /* Krait L2  */
#define MSS_SAW_PORT    3   /* MSS SAW   */

#define MSS_CPR_PORT    5   /* MSS CPR   */
#define TOP_CPR_PORT    4   /* Top CPR   */
#define MSS_MGPI_PORT   0   /* MGPI   */




/* SPMI channels allocation per peripheral is moved to SBL not in TZ as used to
 * To support target that the allocation is still TZ.
 * Set number of peripherals 0 to bypass.
 */
//const uint32 pm_num_periph = 0;

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/


/* PMIC MMU/SMMU configuration table. */

SpmiCfg_ChannelCfg pm_spmi_pheriph_cfg [] =
{
    
    {0xE, 0x08, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},          /* PON    */
    {0xE, 0x06, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },          /* SPMI   */
    {0xE, 0x05, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },          /* INT    */
    
    /*  PMI8950 Peripherals */
    {0x3, 0xDE, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},          /* LAB    */
    {0x3, 0xDC, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},          /* IBB    */
    {0x3, 0xD9, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* WLED1_SINK */
    {0x3, 0xD8, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* WLED1_CTRL   */
    {0x3, 0xD3, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* FLASH1   */
    {0x3, 0xC0, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* HAPTICS    */
    {0x3, 0xB0, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* PWM    */
    {0x3, 0x40, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},          /* LDO1   */
    
    
    {0x2, 0xFE, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },          /* TRIM   */
    {0x2, 0xC1, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },          /* GPIO2   */
    {0x2, 0xC0, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },          /* GPIO1   */
    {0x2, 0xA3, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },         /* MPP4    */
    {0x2, 0xA2, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },         /* MPP3    */
    {0x2, 0xA1, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },         /* MPP2    */
    {0x2, 0xA0, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER   },         /* MPP1    */
    {0x2, 0x79, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },          /* PBS_CLIENT8    */
    {0x2, 0x78, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},          /* PBS_CLIENT7    */
    {0x2, 0x77, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},     /* PBS_CLIENT6    */
    {0x2, 0x76, 0,  PM_RPM_OWNER,         PM_APPS_HLOS_OWNER},         /* PBS_CLIENT5    */
    {0x2, 0x75, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PBS_CLIENT4    */
    {0x2, 0x74, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PBS_CLIENT3    */
    {0x2, 0x73, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PBS_CLIENT2    */
    {0x2, 0x72, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PBS_CLIENT1    */
    {0x2, 0x71, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PBS_CLIENT0    */
    {0x2, 0x70, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },         /* PBS_CORE    */
    {0x2, 0x59, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },         /* CLK_DIST    */
    {0x2, 0x44, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* FG_MEMIF    */
    {0x2, 0x43, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* FG_ADC_MDM    */
    {0x2, 0x42, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* FG_ADC_USR    */
    {0x2, 0x41, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* FG_BATT    */
    {0x2, 0x40, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* FG_SOC    */
    {0x2, 0x31, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* VADC1_USR    */
    {0x2, 0x2C, 0,  PM_RPM_OWNER,         PM_RPM_OWNER      },         /* MBG1    */
    {0x2, 0x1C, 0,  PM_MSS_OWNER,         PM_MSS_OWNER      },         /* BUA    */
    {0x2, 0x1B, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* BSI    */
    {0x2, 0x16, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_MISC    */
    {0x2, 0x14, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_DC    */
    {0x2, 0x13, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_USB    */
    {0x2, 0x12, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_BAT_IF    */
    {0x2, 0x11, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_OTG    */
    {0x2, 0x10, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* SMBCHGL_CHGR    */
    {0x2, 0x09, 0,   PM_RPM_OWNER,         SPMI_OPEN_OWNER},         /* MISC    */
    {0x2, 0x08, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},   /* PON    */
    {0x2, 0x06, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* SPMI   */
    {0x2, 0x05, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* INT    */
    {0x2, 0x04, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* BUS    */
    {0x2, 0x01, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* REVID    */
    
    
    /*  PM8950 Peripherals */
    {0x1, 0xF3, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* CDC_NCP_FREQ          */
    {0x1, 0xF2, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* CDC_BOOST_FREQ          */
    {0x1, 0xF1, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* CDC_A          */
    {0x1, 0xF0, 0,   PM_APPS_HLOS_OWNER,   PM_APPS_HLOS_OWNER},         /* CDC_D          */
    {0x1, 0xBC, 0,   PM_RPM_OWNER,         PM_APPS_HLOS_OWNER},         /* PWM          */
    {0x1, 0x56, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO23          */
    {0x1, 0x55, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO22          */
    {0x1, 0x54, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO19          */
    {0x1, 0x53, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO21          */
    {0x1, 0x52, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO20          */
    {0x1, 0x51, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO18          */
    {0x1, 0x50, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO17          */
    {0x1, 0x4F, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO16          */
    {0x1, 0x4E, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},         /* LDO15          */  /* Hack to test */
    {0x1, 0x4D, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},         /* LDO14          */  /* Hack to test */
    {0x1, 0x4C, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO13          */  /* Hack to test */
    {0x1, 0x4B, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* LDO12          */
    {0x1, 0x4A, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO11          */
    {0x1, 0x49, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO10          */
    {0x1, 0x48, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO9          */
    {0x1, 0x47, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO8          */
    {0x1, 0x46, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO7          */  /* Hack to test */
    {0x1, 0x45, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO6          */
    {0x1, 0x44, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO5          */
    {0x1, 0x43, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO4          */
    {0x1, 0x42, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO3          */
    {0x1, 0x41, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO2          */   /* Hack to test */
    {0x1, 0x40, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LDO1          */
    {0x1, 0x25, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S6_FREQ          */
    {0x1, 0x24, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S6_PS          */
    {0x1, 0x23, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S6_CTRL          */
    {0x1, 0x22, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S5_FREQ          */
    {0x1, 0x21, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S5_PS          */
    {0x1, 0x20, 0,  PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* S5_CTRL          */
    {0x1, 0x1F, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S4_FREQ          */
    {0x1, 0x1E, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S4_PS          */
    {0x1, 0x1D, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S4_CTRL          */
    {0x1, 0x1C, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S3_FREQ          */
    {0x1, 0x1B, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S3_PS          */
    {0x1, 0x1A, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S3_CTRL          */
    {0x1, 0x19, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S2_FREQ          */
    {0x1, 0x18, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S2_PS          */
    {0x1, 0x17, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S2_CTRL          */
    {0x1, 0x16, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S1_FREQ          */
    {0x1, 0x15, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* S1_PS          */
    {0x1, 0x14, 0,  PM_MSS_OWNER,          PM_MSS_OWNER},         /* S1_CTRL          */
    {0x1, 0x10, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* BCLK_GEN_MAIN          */
    
    
    {0x0, 0xFE, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* TRIM          */
    {0x0, 0xC7, 0,  PM_WCONNECT_OWNER,    SPMI_OPEN_OWNER},        /* GPIO8          */
    {0x0, 0xC6, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* GPIO7          */
    {0x0, 0xC5, 0,  PM_WCONNECT_OWNER,    SPMI_OPEN_OWNER},        /* GPIO6          */
    {0x0, 0xC4, 0,  PM_WCONNECT_OWNER,    SPMI_OPEN_OWNER},        /* GPIO5          */
    {0x0, 0xC3, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* GPIO4          */
    {0x0, 0xC2, 0,  PM_MSS_OWNER,         SPMI_OPEN_OWNER},         /* GPIO3          */
    {0x0, 0xC1, 0,  PM_MSS_OWNER,         SPMI_OPEN_OWNER},         /* GPIO2          */
    {0x0, 0xC0, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* GPIO1          */
    {0x0, 0xA3, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* MPP4          */
    {0x0, 0xA2, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* MPP3          */
    {0x0, 0xA1, 0,  PM_APPS_HLOS_OWNER,   SPMI_OPEN_OWNER},       /* MPP2          */
    {0x0, 0xA0, 0,  PM_RPM_OWNER,         SPMI_OPEN_OWNER},         /* MPP1          */
    {0x0, 0x78, 0,   PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},       /* PBS_CLIENT7          */
    {0x0, 0x77, 0,   PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},       /* PBS_CLIENT6          */
    {0x0, 0x76, 0,   PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},       /* PBS_CLIENT5          */
    {0x0, 0x75, 0,   PM_RPM_OWNER,          PM_RPM_OWNER},         /* PBS_CLIENT4          */
    {0x0, 0x74, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},         /* PBS_CLIENT3          */
    {0x0, 0x73, 0,   PM_APPS_HLOS_OWNER,    PM_APPS_HLOS_OWNER},         /* PBS_CLIENT2          */
    {0x0, 0x72, 0,   PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},         /* PBS_CLIENT1          */
    {0x0, 0x71, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* PBS_CLIENT0          */
    {0x0, 0x70, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* PBS_CORE          */
    {0x0, 0x62, 0,  PM_APPS_HLOS_OWNER,    PM_RPM_OWNER},       /* RTC_TIMER          */
    {0x0, 0x61, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* RTC_ALARM          */
    {0x0, 0x60, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},        /* RTC_RW          */
    {0x0, 0x5D, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* DIV_CLK3          */
    {0x0, 0x5C, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* DIV_CLK2          */
    {0x0, 0x5B, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* DIV_CLK1          */
    {0x0, 0x5A, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* SLEEP_CLK1          */
    {0x0, 0x59, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* CLK_DIST          */
    {0x0, 0x58, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* LN_BB_CLK          */
    {0x0, 0x55, 0,  PM_RPM_OWNER,         PM_MSS_OWNER},         /* RF_CLK2          */
    {0x0, 0x54, 0,  PM_RPM_OWNER,         PM_MSS_OWNER},         /* RF_CLK1          */
    {0x0, 0x52, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* BB_CLK2          */
    {0x0, 0x51, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* BB_CLK1          */
    {0x0, 0x50, 0,  PM_RPM_OWNER,         PM_MSS_OWNER},         /* XO          */
    {0x0, 0x35, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* VADC4_LC_VBAT          */
    {0x0, 0x34, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* VADC2_LC_BTM_2          */
    {0x0, 0x32, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},         /* VADC3_LC_MDM          */
    {0x0, 0x31, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* VADC1_LC_USR          */
    {0x0, 0x2C, 0,  PM_RPM_OWNER,          PM_RPM_OWNER},         /* MBG1          */
    {0x0, 0x28, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* COIN          */
    {0x0, 0x24, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* TEMP_ALARM          */
    {0x0, 0x1C, 0,  PM_MSS_OWNER,         PM_MSS_OWNER},         /* BUA_EXT_CHARGER          */
    {0x0, 0x0A, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* VREFLPDDR          */
    {0x0, 0x09, 0,  PM_RPM_OWNER,           SPMI_OPEN_OWNER},      /* MISC          */
    {0x0, 0x08, 0,  PM_APPS_HLOS_OWNER,     PM_APPS_HLOS_OWNER},        /* PON          */
    {0x0, 0x06, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* SPMI   */
    {0x0, 0x05, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* INT    */
    {0x0, 0x04, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* BUS          */
    {0x0, 0x01, 0,  PM_RPM_OWNER,         PM_RPM_OWNER},         /* REVID          */

};

/* Number of spmi channels config entries. . */
const uint32 pm_spmi_pheriph_cfg_sz = sizeof(pm_spmi_pheriph_cfg) / sizeof(SpmiCfg_ChannelCfg);


/*
 * pm_pvc_L2_addr
 *
 * PVC port addr for Krait used for ext BUCK Control
 */

static SpmiCfg_Ppid pm_pvc_L2_addr[] =
{
    {0x1, 0x2041}, /* PMIC 8952, Periph S5_CTRL, S5_CTRL_VOLTAGE_CTL2 */
    {0x1, 0x206C}, /* PMIC 8952, Periph S5_CTRL, S5_CTRL_OCP */
    {0x1, 0x2045}, /* PMIC 8952, Periph S5_CTRL, S5_CTRL_MODE_CTL */
    {0x1, 0x2046}, /* PMIC 8952, Periph S5_CTRL, S5_CTRL_EN_CTL */
};

/*
 * pm_pvc_mgpi_addr
 *
 * PVC port addr for MGPI.
 */
static SpmiCfg_Ppid pm_pvc_mgpi_addr[] =
{
  {0x0, 0x094A}, /* PMIC 8952, TX_GTR_THRES_CTL */
};
/*
 * pm_pvc_mss_saw_addr
 *
 * PVC port addr for mss.
 */

static SpmiCfg_Ppid pm_pvc_mss_saw_addr[] =
{
    {0x1, 0x1441}, /* PMIC 8952, Periph S1_CTRL, S1_CTRL_VOLTAGE_CTL2 */
    {0x1, 0x146C}, /* PMIC 8952, Periph S1_CTRL, S1_CTRL_OCP */
    {0x1, 0x1445}, /* PMIC 8952, Periph S1_CTRL, S1_CTRL_MODE_CTL */
    {0x1, 0x1446}, /* PMIC 8952, Periph S1_CTRL, S1_CTRL_EN_CTL */
};

/*
 * pm_arb_pvc_cfg
 *
 * PMIC Arbiter PVC ports config.
 */
const SpmiCfg_PvcPortCfg pm_arb_pvc_cfg[] =
{
  {
    /* .pvcPortId           = */ KRAIT_L2_PORT,
    /* .priority  = */ SPMI_ACCESS_PRIORITY_LOW,
    /* .ppids       = */ pm_pvc_L2_addr,
    /* .numPpids           = */ sizeof(pm_pvc_L2_addr)/sizeof(SpmiCfg_Ppid)
  },
  {
    /* .pvcPortId           = */ MSS_SAW_PORT,
    /* .priority = */ SPMI_ACCESS_PRIORITY_LOW,
    /* .ppids       = */ pm_pvc_mss_saw_addr,
    /* .numPpids           = */ sizeof(pm_pvc_mss_saw_addr)/sizeof(SpmiCfg_Ppid)
  },
  {
    /* .pvcPortId           = */ MSS_MGPI_PORT,
    /* .priority = */ SPMI_ACCESS_PRIORITY_LOW,
    /* .ppids       = */ pm_pvc_mgpi_addr,
    /* .numPpids           = */ sizeof(pm_pvc_mgpi_addr)/sizeof(SpmiCfg_Ppid)
  },
};

/* Number of pvc ports. */
const uint32 pm_num_pvc_port = sizeof(pm_arb_pvc_cfg) / sizeof(SpmiCfg_PvcPortCfg);

const uint8 pm_arb_priorities[] =
{
    0,                                         /* SW port -- highest priority */
    PVC_PORT_TO_PMIC_ARB_PORT( MSS_MGPI_PORT ), /* port 3, next highest priority */
    PVC_PORT_TO_PMIC_ARB_PORT( KRAIT_L2_PORT ), /* port 4, next highest priority */
    PVC_PORT_TO_PMIC_ARB_PORT( MSS_SAW_PORT )   /* port 5, next highest priority */
};

/* Number of port prioriries. */
const uint32 pm_num_prio = sizeof(pm_arb_priorities) / sizeof(uint8);
































































































































































