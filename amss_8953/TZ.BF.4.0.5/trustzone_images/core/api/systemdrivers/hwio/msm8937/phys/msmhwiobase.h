#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.tz/1.0.5/api/systemdrivers/hwio/msm8937/phys/msmhwiobase.h#1 $
  $DateTime: 2016/03/24 12:08:26 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: DEHR_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define DEHR_RAM_SIZE_BASE                                          0x00001fff
#define DEHR_RAM_SIZE_BASE_SIZE                                     0x00000000
#define DEHR_RAM_SIZE_BASE_PHYS                                     0x00001fff

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM_SIZE
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_SIZE_BASE                                       0x00003fff
#define SYSTEM_IMEM_SIZE_BASE_SIZE                                  0x00000000
#define SYSTEM_IMEM_SIZE_BASE_PHYS                                  0x00003fff

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_SIZE_BASE                                    0x00004fff
#define RPM_SS_MSG_RAM_SIZE_BASE_SIZE                               0x00000000
#define RPM_SS_MSG_RAM_SIZE_BASE_PHYS                               0x00004fff

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM_SIZE
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_SIZE_BASE                                         0x00008000
#define LPASS_LPM_SIZE_BASE_SIZE                                    0x00000000
#define LPASS_LPM_SIZE_BASE_PHYS                                    0x00008000

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_SIZE_BASE                                      0x0000ffff
#define RPM_DATA_RAM_SIZE_BASE_SIZE                                 0x00000000
#define RPM_DATA_RAM_SIZE_BASE_PHYS                                 0x0000ffff

/*----------------------------------------------------------------------------
 * BASE: PCNOC_0_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_0_BUS_TIMEOUT_BASE                                    0x00023000
#define PCNOC_0_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_0_BUS_TIMEOUT_BASE_PHYS                               0x00023000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_SIZE_BASE                                      0x00023fff
#define RPM_CODE_RAM_SIZE_BASE_SIZE                                 0x00000000
#define RPM_CODE_RAM_SIZE_BASE_PHYS                                 0x00023fff

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_SIZE
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_SIZE_BASE                                          0x00023fff
#define BOOT_ROM_SIZE_BASE_SIZE                                     0x00000000
#define BOOT_ROM_SIZE_BASE_PHYS                                     0x00023fff

/*----------------------------------------------------------------------------
 * BASE: PCNOC_1_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_1_BUS_TIMEOUT_BASE                                    0x00024000
#define PCNOC_1_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_1_BUS_TIMEOUT_BASE_PHYS                               0x00024000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_2_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_2_BUS_TIMEOUT_BASE                                    0x00025000
#define PCNOC_2_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_2_BUS_TIMEOUT_BASE_PHYS                               0x00025000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_3_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_3_BUS_TIMEOUT_BASE                                    0x00026000
#define PCNOC_3_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_3_BUS_TIMEOUT_BASE_PHYS                               0x00026000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_4_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_4_BUS_TIMEOUT_BASE                                    0x00027000
#define PCNOC_4_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_4_BUS_TIMEOUT_BASE_PHYS                               0x00027000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_5_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_5_BUS_TIMEOUT_BASE                                    0x00028000
#define PCNOC_5_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_5_BUS_TIMEOUT_BASE_PHYS                               0x00028000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_6_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_6_BUS_TIMEOUT_BASE                                    0x00029000
#define PCNOC_6_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_6_BUS_TIMEOUT_BASE_PHYS                               0x00029000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_7_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_7_BUS_TIMEOUT_BASE                                    0x0002a000
#define PCNOC_7_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_7_BUS_TIMEOUT_BASE_PHYS                               0x0002a000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_8_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_8_BUS_TIMEOUT_BASE                                    0x0002b000
#define PCNOC_8_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_8_BUS_TIMEOUT_BASE_PHYS                               0x0002b000

/*----------------------------------------------------------------------------
 * BASE: PCNOC_9_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define PCNOC_9_BUS_TIMEOUT_BASE                                    0x0002c000
#define PCNOC_9_BUS_TIMEOUT_BASE_SIZE                               0x00001000
#define PCNOC_9_BUS_TIMEOUT_BASE_PHYS                               0x0002c000

/*----------------------------------------------------------------------------
 * BASE: XPU_CFG_SNOC_CFG_MPU1132_4_M18L12_AHB
 *--------------------------------------------------------------------------*/

#define XPU_CFG_SNOC_CFG_MPU1132_4_M18L12_AHB_BASE                  0x0002d000
#define XPU_CFG_SNOC_CFG_MPU1132_4_M18L12_AHB_BASE_SIZE             0x00001000
#define XPU_CFG_SNOC_CFG_MPU1132_4_M18L12_AHB_BASE_PHYS             0x0002d000

/*----------------------------------------------------------------------------
 * BASE: XPU_CFG_PCNOC_CFG_MPU1132_4_M18L12_AHB
 *--------------------------------------------------------------------------*/

#define XPU_CFG_PCNOC_CFG_MPU1132_4_M18L12_AHB_BASE                 0x0002e000
#define XPU_CFG_PCNOC_CFG_MPU1132_4_M18L12_AHB_BASE_SIZE            0x00001000
#define XPU_CFG_PCNOC_CFG_MPU1132_4_M18L12_AHB_BASE_PHYS            0x0002e000

/*----------------------------------------------------------------------------
 * BASE: XPU_CFG_Q6PCNOC_CFG_MPU0032A_20_M31L10_AXI_FEERO
 *--------------------------------------------------------------------------*/

#define XPU_CFG_Q6PCNOC_CFG_MPU0032A_20_M31L10_AXI_FEERO_BASE       0x00032000
#define XPU_CFG_Q6PCNOC_CFG_MPU0032A_20_M31L10_AXI_FEERO_BASE_SIZE  0x00001000
#define XPU_CFG_Q6PCNOC_CFG_MPU0032A_20_M31L10_AXI_FEERO_BASE_PHYS  0x00032000

/*----------------------------------------------------------------------------
 * BASE: XPU_CFG_RPM_CFG_MPU1132_2_M19L12_AHB
 *--------------------------------------------------------------------------*/

#define XPU_CFG_RPM_CFG_MPU1132_2_M19L12_AHB_BASE                   0x00033000
#define XPU_CFG_RPM_CFG_MPU1132_2_M19L12_AHB_BASE_SIZE              0x00001000
#define XPU_CFG_RPM_CFG_MPU1132_2_M19L12_AHB_BASE_PHYS              0x00033000

/*----------------------------------------------------------------------------
 * BASE: SMMU_500_MPU_WRAPPER
 *--------------------------------------------------------------------------*/

#define SMMU_500_MPU_WRAPPER_BASE                                   0x00034000
#define SMMU_500_MPU_WRAPPER_BASE_SIZE                              0x00004700
#define SMMU_500_MPU_WRAPPER_BASE_PHYS                              0x00034000

/*----------------------------------------------------------------------------
 * BASE: SPDM_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                       0x00040000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                  0x00005000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                  0x00040000

/*----------------------------------------------------------------------------
 * BASE: RBCPR_WRAPPER
 *--------------------------------------------------------------------------*/

#define RBCPR_WRAPPER_BASE                                          0x00048000
#define RBCPR_WRAPPER_BASE_SIZE                                     0x00001000
#define RBCPR_WRAPPER_BASE_PHYS                                     0x00048000

/*----------------------------------------------------------------------------
 * BASE: OCIMEM_WRAPPER_CSR
 *--------------------------------------------------------------------------*/

#define OCIMEM_WRAPPER_CSR_BASE                                     0x00050000
#define OCIMEM_WRAPPER_CSR_BASE_SIZE                                0x00004000
#define OCIMEM_WRAPPER_CSR_BASE_PHYS                                0x00050000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0x00060000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00000000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x00060000

/*----------------------------------------------------------------------------
 * BASE: RPM_MSG_RAM
 *--------------------------------------------------------------------------*/

#define RPM_MSG_RAM_BASE                                            0x00060000
#define RPM_MSG_RAM_BASE_SIZE                                       0x00000000
#define RPM_MSG_RAM_BASE_PHYS                                       0x00060000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE                             0x00064ffe
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                        0x00064ffe

/*----------------------------------------------------------------------------
 * BASE: PDM_PERPH_WEB
 *--------------------------------------------------------------------------*/

#define PDM_PERPH_WEB_BASE                                          0x00068000
#define PDM_PERPH_WEB_BASE_SIZE                                     0x00004000
#define PDM_PERPH_WEB_BASE_PHYS                                     0x00068000

/*----------------------------------------------------------------------------
 * BASE: USB2_FEMTO_PHY_CM_DWC_USB2_SW
 *--------------------------------------------------------------------------*/

#define USB2_FEMTO_PHY_CM_DWC_USB2_SW_BASE                          0x0006c000
#define USB2_FEMTO_PHY_CM_DWC_USB2_SW_BASE_SIZE                     0x00000200
#define USB2_FEMTO_PHY_CM_DWC_USB2_SW_BASE_PHYS                     0x0006c000

/*----------------------------------------------------------------------------
 * BASE: TOP_AHB2PHY_SWMAN_TOP
 *--------------------------------------------------------------------------*/

#define TOP_AHB2PHY_SWMAN_TOP_BASE                                  0x0006cc00
#define TOP_AHB2PHY_SWMAN_TOP_BASE_SIZE                             0x00000200
#define TOP_AHB2PHY_SWMAN_TOP_BASE_PHYS                             0x0006cc00

/*----------------------------------------------------------------------------
 * BASE: TOP_AHB2PHY_BROADCAST_SWMAN_TOP
 *--------------------------------------------------------------------------*/

#define TOP_AHB2PHY_BROADCAST_SWMAN_TOP_BASE                        0x0006ce00
#define TOP_AHB2PHY_BROADCAST_SWMAN_TOP_BASE_SIZE                   0x00000200
#define TOP_AHB2PHY_BROADCAST_SWMAN_TOP_BASE_PHYS                   0x0006ce00

/*----------------------------------------------------------------------------
 * BASE: PRONTO_CMEM_SIZE
 *--------------------------------------------------------------------------*/

#define PRONTO_CMEM_SIZE_BASE                                       0x00080000
#define PRONTO_CMEM_SIZE_BASE_SIZE                                  0x00000000
#define PRONTO_CMEM_SIZE_BASE_PHYS                                  0x00080000

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0x000a0000
#define SECURITY_CONTROL_BASE_SIZE                                  0x0000f000
#define SECURITY_CONTROL_BASE_PHYS                                  0x000a0000

/*----------------------------------------------------------------------------
 * BASE: DCC_WRAPPER
 *--------------------------------------------------------------------------*/

#define DCC_WRAPPER_BASE                                            0x000b0000
#define DCC_WRAPPER_BASE_SIZE                                       0x00004000
#define DCC_WRAPPER_BASE_PHYS                                       0x000b0000

/*----------------------------------------------------------------------------
 * BASE: PRNG_PRNG_TOP
 *--------------------------------------------------------------------------*/

#define PRNG_PRNG_TOP_BASE                                          0x000e0000
#define PRNG_PRNG_TOP_BASE_SIZE                                     0x00010000
#define PRNG_PRNG_TOP_BASE_PHYS                                     0x000e0000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_START_ADDRESS_BASE                                 0x00100000
#define BOOT_ROM_START_ADDRESS_BASE_SIZE                            0x00000000
#define BOOT_ROM_START_ADDRESS_BASE_PHYS                            0x00100000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_END_ADDRESS_BASE                                   0x00123ffe
#define BOOT_ROM_END_ADDRESS_BASE_SIZE                              0x00000000
#define BOOT_ROM_END_ADDRESS_BASE_PHYS                              0x00123ffe

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_BASE                                               0x001ff000
#define BOOT_ROM_BASE_SIZE                                          0x00001000
#define BOOT_ROM_BASE_PHYS                                          0x001ff000

/*----------------------------------------------------------------------------
 * BASE: APSS_L2_MEM_SIZE
 *--------------------------------------------------------------------------*/

#define APSS_L2_MEM_SIZE_BASE                                       0x001fffff
#define APSS_L2_MEM_SIZE_BASE_SIZE                                  0x00000000
#define APSS_L2_MEM_SIZE_BASE_PHYS                                  0x001fffff

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_START_ADDRESS_BASE                             0x00200000
#define RPM_CODE_RAM_START_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_CODE_RAM_START_ADDRESS_BASE_PHYS                        0x00200000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_END_ADDRESS_BASE                               0x00223ffe
#define RPM_CODE_RAM_END_ADDRESS_BASE_SIZE                          0x00000000
#define RPM_CODE_RAM_END_ADDRESS_BASE_PHYS                          0x00223ffe

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0x00280000
#define RPM_BASE_SIZE                                               0x00009000
#define RPM_BASE_PHYS                                               0x00280000

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_START_ADDRESS_BASE                             0x00290000
#define RPM_DATA_RAM_START_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_DATA_RAM_START_ADDRESS_BASE_PHYS                        0x00290000

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_END_ADDRESS_BASE                               0x0029fffe
#define RPM_DATA_RAM_END_ADDRESS_BASE_SIZE                          0x00000000
#define RPM_DATA_RAM_END_ADDRESS_BASE_PHYS                          0x0029fffe

/*----------------------------------------------------------------------------
 * BASE: MSS_TCM_SIZE
 *--------------------------------------------------------------------------*/

#define MSS_TCM_SIZE_BASE                                           0x003fffff
#define MSS_TCM_SIZE_BASE_SIZE                                      0x00000000
#define MSS_TCM_SIZE_BASE_PHYS                                      0x003fffff

/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/

#define BIMC_BASE                                                   0x00400000
#define BIMC_BASE_SIZE                                              0x0005a000
#define BIMC_BASE_PHYS                                              0x00400000

/*----------------------------------------------------------------------------
 * BASE: EBI1_PHY_CFG
 *--------------------------------------------------------------------------*/

#define EBI1_PHY_CFG_BASE                                           0x00480000
#define EBI1_PHY_CFG_BASE_SIZE                                      0x00020000
#define EBI1_PHY_CFG_BASE_PHYS                                      0x00480000

/*----------------------------------------------------------------------------
 * BASE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                               0x004a0000
#define MPM2_MPM_BASE_SIZE                                          0x00010000
#define MPM2_MPM_BASE_PHYS                                          0x004a0000

/*----------------------------------------------------------------------------
 * BASE: DEHR_BIMC_WRAPPER
 *--------------------------------------------------------------------------*/

#define DEHR_BIMC_WRAPPER_BASE                                      0x004b0000
#define DEHR_BIMC_WRAPPER_BASE_SIZE                                 0x00004000
#define DEHR_BIMC_WRAPPER_BASE_PHYS                                 0x004b0000

/*----------------------------------------------------------------------------
 * BASE: DEHR_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define DEHR_RAM_START_ADDRESS_BASE                                 0x004b4000
#define DEHR_RAM_START_ADDRESS_BASE_SIZE                            0x00000000
#define DEHR_RAM_START_ADDRESS_BASE_PHYS                            0x004b4000

/*----------------------------------------------------------------------------
 * BASE: DEHR_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define DEHR_RAM_END_ADDRESS_BASE                                   0x004b5ffe
#define DEHR_RAM_END_ADDRESS_BASE_SIZE                              0x00000000
#define DEHR_RAM_END_ADDRESS_BASE_PHYS                              0x004b5ffe

/*----------------------------------------------------------------------------
 * BASE: PC_NOC
 *--------------------------------------------------------------------------*/

#define PC_NOC_BASE                                                 0x00500000
#define PC_NOC_BASE_SIZE                                            0x00015000
#define PC_NOC_BASE_PHYS                                            0x00500000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0x00580000
#define SYSTEM_NOC_BASE_SIZE                                        0x00017000
#define SYSTEM_NOC_BASE_PHYS                                        0x00580000

/*----------------------------------------------------------------------------
 * BASE: CRYPTO0_CRYPTO_TOP
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                     0x00700000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                0x00040000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                0x00700000

/*----------------------------------------------------------------------------
 * BASE: CATS_64BIT_SIZE
 *--------------------------------------------------------------------------*/

#define CATS_64BIT_SIZE_BASE                                        0x01000000
#define CATS_64BIT_SIZE_BASE_SIZE                                   0x00000000
#define CATS_64BIT_SIZE_BASE_PHYS                                   0x01000000

/*----------------------------------------------------------------------------
 * BASE: CATS_128BIT_SIZE
 *--------------------------------------------------------------------------*/

#define CATS_128BIT_SIZE_BASE                                       0x01000000
#define CATS_128BIT_SIZE_BASE_SIZE                                  0x00000000
#define CATS_128BIT_SIZE_BASE_PHYS                                  0x01000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM_SIZE
 *--------------------------------------------------------------------------*/

#define QDSS_STM_SIZE_BASE                                          0x01000000
#define QDSS_STM_SIZE_BASE_SIZE                                     0x00000000
#define QDSS_STM_SIZE_BASE_PHYS                                     0x01000000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0x01000000
#define TLMM_BASE_SIZE                                              0x00301000
#define TLMM_BASE_PHYS                                              0x01000000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0x01800000
#define CLK_CTL_BASE_SIZE                                           0x00082000
#define CLK_CTL_BASE_PHYS                                           0x01800000

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0x01900000
#define CORE_TOP_CSR_BASE_SIZE                                      0x00058000
#define CORE_TOP_CSR_BASE_PHYS                                      0x01900000

/*----------------------------------------------------------------------------
 * BASE: MDSS_MDSS_TOP
 *--------------------------------------------------------------------------*/

#define MDSS_MDSS_TOP_BASE                                          0x01a00000
#define MDSS_MDSS_TOP_BASE_SIZE                                     0x000db000
#define MDSS_MDSS_TOP_BASE_PHYS                                     0x01a00000

/*----------------------------------------------------------------------------
 * BASE: CAMSS_CAMERA_SS
 *--------------------------------------------------------------------------*/

#define CAMSS_CAMERA_SS_BASE                                        0x01b00000
#define CAMSS_CAMERA_SS_BASE_SIZE                                   0x000a3000
#define CAMSS_CAMERA_SS_BASE_PHYS                                   0x01b00000

/*----------------------------------------------------------------------------
 * BASE: A5X_A5X
 *--------------------------------------------------------------------------*/

#define A5X_A5X_BASE                                                0x01c00000
#define A5X_A5X_BASE_SIZE                                           0x00062000
#define A5X_A5X_BASE_PHYS                                           0x01c00000

/*----------------------------------------------------------------------------
 * BASE: OXILI_OXILI
 *--------------------------------------------------------------------------*/

#define OXILI_OXILI_BASE                                            0x01c00000
#define OXILI_OXILI_BASE_SIZE                                       0x00020000
#define OXILI_OXILI_BASE_PHYS                                       0x01c00000

/*----------------------------------------------------------------------------
 * BASE: VENUS0_VENUS
 *--------------------------------------------------------------------------*/

#define VENUS0_VENUS_BASE                                           0x01d00000
#define VENUS0_VENUS_BASE_SIZE                                      0x000f1000
#define VENUS0_VENUS_BASE_PHYS                                      0x01d00000

/*----------------------------------------------------------------------------
 * BASE: SMMU_500_REG_WRAPPER
 *--------------------------------------------------------------------------*/

#define SMMU_500_REG_WRAPPER_BASE                                   0x01e00000
#define SMMU_500_REG_WRAPPER_BASE_SIZE                              0x0010c000
#define SMMU_500_REG_WRAPPER_BASE_PHYS                              0x01e00000

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0x02000000
#define PMIC_ARB_BASE_SIZE                                          0x01908000
#define PMIC_ARB_BASE_PHYS                                          0x02000000

/*----------------------------------------------------------------------------
 * BASE: MSS_TOP
 *--------------------------------------------------------------------------*/

#define MSS_TOP_BASE                                                0x04000000
#define MSS_TOP_BASE_SIZE                                           0x00bb1000
#define MSS_TOP_BASE_PHYS                                           0x04000000

/*----------------------------------------------------------------------------
 * BASE: MSS_TCM
 *--------------------------------------------------------------------------*/

#define MSS_TCM_BASE                                                0x04400000
#define MSS_TCM_BASE_SIZE                                           0x00000000
#define MSS_TCM_BASE_PHYS                                           0x04400000

/*----------------------------------------------------------------------------
 * BASE: MSS_TCM_END
 *--------------------------------------------------------------------------*/

#define MSS_TCM_END_BASE                                            0x047ffffe
#define MSS_TCM_END_BASE_SIZE                                       0x00000000
#define MSS_TCM_END_BASE_PHYS                                       0x047ffffe

/*----------------------------------------------------------------------------
 * BASE: QDSS_APB_DEC_QDSS_APB
 *--------------------------------------------------------------------------*/

#define QDSS_APB_DEC_QDSS_APB_BASE                                  0x06000000
#define QDSS_APB_DEC_QDSS_APB_BASE_SIZE                             0x00029000
#define QDSS_APB_DEC_QDSS_APB_BASE_PHYS                             0x06000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_AHB_DEC_QDSS_AHB
 *--------------------------------------------------------------------------*/

#define QDSS_AHB_DEC_QDSS_AHB_BASE                                  0x06040000
#define QDSS_AHB_DEC_QDSS_AHB_BASE_SIZE                             0x00019000
#define QDSS_AHB_DEC_QDSS_AHB_BASE_PHYS                             0x06040000

/*----------------------------------------------------------------------------
 * BASE: QDSS_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define QDSS_WRAPPER_TOP_BASE                                       0x06100000
#define QDSS_WRAPPER_TOP_BASE_SIZE                                  0x00033000
#define QDSS_WRAPPER_TOP_BASE_PHYS                                  0x06100000

/*----------------------------------------------------------------------------
 * BASE: SDC1_SDCC5_TOP
 *--------------------------------------------------------------------------*/

#define SDC1_SDCC5_TOP_BASE                                         0x07800000
#define SDC1_SDCC5_TOP_BASE_SIZE                                    0x00027000
#define SDC1_SDCC5_TOP_BASE_PHYS                                    0x07800000

/*----------------------------------------------------------------------------
 * BASE: SDC2_SDCC5_TOP
 *--------------------------------------------------------------------------*/

#define SDC2_SDCC5_TOP_BASE                                         0x07840000
#define SDC2_SDCC5_TOP_BASE_SIZE                                    0x00026000
#define SDC2_SDCC5_TOP_BASE_PHYS                                    0x07840000

/*----------------------------------------------------------------------------
 * BASE: BLSP1_BLSP
 *--------------------------------------------------------------------------*/

#define BLSP1_BLSP_BASE                                             0x07880000
#define BLSP1_BLSP_BASE_SIZE                                        0x00039000
#define BLSP1_BLSP_BASE_PHYS                                        0x07880000

/*----------------------------------------------------------------------------
 * BASE: USB2_HSIC_USB_OTG_HS_EP16_PIPES4
 *--------------------------------------------------------------------------*/

#define USB2_HSIC_USB_OTG_HS_EP16_PIPES4_BASE                       0x078c0000
#define USB2_HSIC_USB_OTG_HS_EP16_PIPES4_BASE_SIZE                  0x0001c000
#define USB2_HSIC_USB_OTG_HS_EP16_PIPES4_BASE_PHYS                  0x078c0000

/*----------------------------------------------------------------------------
 * BASE: IPA_WRAPPER
 *--------------------------------------------------------------------------*/

#define IPA_WRAPPER_BASE                                            0x07900000
#define IPA_WRAPPER_BASE_SIZE                                       0x00080000
#define IPA_WRAPPER_BASE_PHYS                                       0x07900000

/*----------------------------------------------------------------------------
 * BASE: BLSP2_BLSP
 *--------------------------------------------------------------------------*/

#define BLSP2_BLSP_BASE                                             0x07ac0000
#define BLSP2_BLSP_BASE_SIZE                                        0x00039000
#define BLSP2_BLSP_BASE_PHYS                                        0x07ac0000

/*----------------------------------------------------------------------------
 * BASE: APSS_L2_MEM
 *--------------------------------------------------------------------------*/

#define APSS_L2_MEM_BASE                                            0x08000000
#define APSS_L2_MEM_BASE_SIZE                                       0x00000000
#define APSS_L2_MEM_BASE_PHYS                                       0x08000000

/*----------------------------------------------------------------------------
 * BASE: APSS_L2_MEM_END
 *--------------------------------------------------------------------------*/

#define APSS_L2_MEM_END_BASE                                        0x081ffffe
#define APSS_L2_MEM_END_BASE_SIZE                                   0x00000000
#define APSS_L2_MEM_END_BASE_PHYS                                   0x081ffffe

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_BASE                                            0x08600000
#define SYSTEM_IMEM_BASE_SIZE                                       0x00000000
#define SYSTEM_IMEM_BASE_PHYS                                       0x08600000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM_END
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_END_BASE                                        0x08603ffe
#define SYSTEM_IMEM_END_BASE_SIZE                                   0x00000000
#define SYSTEM_IMEM_END_BASE_PHYS                                   0x08603ffe

/*----------------------------------------------------------------------------
 * BASE: SNOC_1_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define SNOC_1_BUS_TIMEOUT_BASE                                     0x08800000
#define SNOC_1_BUS_TIMEOUT_BASE_SIZE                                0x00001000
#define SNOC_1_BUS_TIMEOUT_BASE_PHYS                                0x08800000

/*----------------------------------------------------------------------------
 * BASE: SNOC_0_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define SNOC_0_BUS_TIMEOUT_BASE                                     0x08801000
#define SNOC_0_BUS_TIMEOUT_BASE_SIZE                                0x00001000
#define SNOC_0_BUS_TIMEOUT_BASE_PHYS                                0x08801000

/*----------------------------------------------------------------------------
 * BASE: SNOC_2_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define SNOC_2_BUS_TIMEOUT_BASE                                     0x08803000
#define SNOC_2_BUS_TIMEOUT_BASE_SIZE                                0x00001000
#define SNOC_2_BUS_TIMEOUT_BASE_PHYS                                0x08803000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM
 *--------------------------------------------------------------------------*/

#define QDSS_STM_BASE                                               0x09000000
#define QDSS_STM_BASE_SIZE                                          0x00000000
#define QDSS_STM_BASE_PHYS                                          0x09000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM_END
 *--------------------------------------------------------------------------*/

#define QDSS_STM_END_BASE                                           0x09ffffff
#define QDSS_STM_END_BASE_SIZE                                      0x00000000
#define QDSS_STM_END_BASE_PHYS                                      0x09ffffff

/*----------------------------------------------------------------------------
 * BASE: WCSS_WCSS
 *--------------------------------------------------------------------------*/

#define WCSS_WCSS_BASE                                              0x0a000000
#define WCSS_WCSS_BASE_SIZE                                         0x004c1000
#define WCSS_WCSS_BASE_PHYS                                         0x0a000000

/*----------------------------------------------------------------------------
 * BASE: PRONTO_CMEM
 *--------------------------------------------------------------------------*/

#define PRONTO_CMEM_BASE                                            0x0a280000
#define PRONTO_CMEM_BASE_SIZE                                       0x00000000
#define PRONTO_CMEM_BASE_PHYS                                       0x0a280000

/*----------------------------------------------------------------------------
 * BASE: PRONTO_CMEM_END
 *--------------------------------------------------------------------------*/

#define PRONTO_CMEM_END_BASE                                        0x0a2fffff
#define PRONTO_CMEM_END_BASE_SIZE                                   0x00000000
#define PRONTO_CMEM_END_BASE_PHYS                                   0x0a2fffff

/*----------------------------------------------------------------------------
 * BASE: A53SS
 *--------------------------------------------------------------------------*/

#define A53SS_BASE                                                  0x0b000000
#define A53SS_BASE_SIZE                                             0x001dc000
#define A53SS_BASE_PHYS                                             0x0b000000

/*----------------------------------------------------------------------------
 * BASE: LPASS
 *--------------------------------------------------------------------------*/

#define LPASS_BASE                                                  0x0c000000
#define LPASS_BASE_SIZE                                             0x002b1000
#define LPASS_BASE_PHYS                                             0x0c000000

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_BASE                                              0x0c0e0000
#define LPASS_LPM_BASE_SIZE                                         0x00000000
#define LPASS_LPM_BASE_PHYS                                         0x0c0e0000

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM_END
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_END_BASE                                          0x0c0e7fff
#define LPASS_LPM_END_BASE_SIZE                                     0x00000000
#define LPASS_LPM_END_BASE_PHYS                                     0x0c0e7fff

/*----------------------------------------------------------------------------
 * BASE: CATS_128BIT
 *--------------------------------------------------------------------------*/

#define CATS_128BIT_BASE                                            0x0e000000
#define CATS_128BIT_BASE_SIZE                                       0x00000000
#define CATS_128BIT_BASE_PHYS                                       0x0e000000

/*----------------------------------------------------------------------------
 * BASE: CATS_128BIT_END
 *--------------------------------------------------------------------------*/

#define CATS_128BIT_END_BASE                                        0x0effffff
#define CATS_128BIT_END_BASE_SIZE                                   0x00000000
#define CATS_128BIT_END_BASE_PHYS                                   0x0effffff

/*----------------------------------------------------------------------------
 * BASE: CATS_64BIT
 *--------------------------------------------------------------------------*/

#define CATS_64BIT_BASE                                             0x0f000000
#define CATS_64BIT_BASE_SIZE                                        0x00000000
#define CATS_64BIT_BASE_PHYS                                        0x0f000000

/*----------------------------------------------------------------------------
 * BASE: CATS_64BIT_END
 *--------------------------------------------------------------------------*/

#define CATS_64BIT_END_BASE                                         0x0fffffff
#define CATS_64BIT_END_BASE_SIZE                                    0x00000000
#define CATS_64BIT_END_BASE_PHYS                                    0x0fffffff

/*----------------------------------------------------------------------------
 * BASE: EBI1_MEM
 *--------------------------------------------------------------------------*/

#define EBI1_MEM_BASE                                               0x10000000
#define EBI1_MEM_BASE_SIZE                                          0x00000000
#define EBI1_MEM_BASE_PHYS                                          0x10000000

/*----------------------------------------------------------------------------
 * BASE: EBI1_MEM_SIZE
 *--------------------------------------------------------------------------*/

#define EBI1_MEM_SIZE_BASE                                          0xefffffff
#define EBI1_MEM_SIZE_BASE_SIZE                                     0x00000000
#define EBI1_MEM_SIZE_BASE_PHYS                                     0xefffffff

/*----------------------------------------------------------------------------
 * BASE: EBI1_MEM_END
 *--------------------------------------------------------------------------*/

#define EBI1_MEM_END_BASE                                           0xfffffffe
#define EBI1_MEM_END_BASE_SIZE                                      0x00000000
#define EBI1_MEM_END_BASE_PHYS                                      0xfffffffe


#endif /* __MSMHWIOBASE_H__ */
