#===========================================================================
#  Copyright (c) 2011-2016 QUALCOMM Incorporated.
#  All Rights Reserved.
#  Qualcomm Confidential and Proprietary
#===========================================================================
#
#
# GENERAL DESCRIPTION
#    build script
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
Import('env')

if env.has_key('USES_NO_CP'):
  env.Append(CCFLAGS = ' -DUSES_NO_CP ')

env.Append(CPPDEFINES = 'SAMPLE_APP=1')

test_fuse_inc = ['#../../core/securemsm/trustzone/qsapps/tclapp/inc/' + env['CHIPSET']]
includes = ['#../../core/securemsm/trustzone/qsapps/tclapp/inc',
            '#../../core/api/kernel/libstd/stringl',
            '#../../core/securemsm/trustzone/qsapps/smplserv/inc',
            '#../../core/securemsm/sse/qsee/SecureUI/inc',
            '#../../core/api/boot/qfprom',
            '#../../core/securemsm/secmath/shared/inc',
            '#../../core/api/securemsm/trustzone/gp',
            '#../../core/securemsm/trustzone/qsapps/libs/applib/qsee/src',
            '#../../core/securemsm/sse/qsee/SecureUILib/include',
            '#../../core/securemsm/sse/qsee/SecureTouch/drTs/include',
            '#../../core/securemsm/trustzone/qsapps/libs/biometric/inc',
            '#../../core/kernel/smmu/v2/inc/',
            '#../../core/securemsm/trustzone/qsee/mink/include',
           ]
includes.extend(test_fuse_inc)

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------
sources = [
        'tclapp.c',
        'common.c',
]

if env.has_key('USES_NO_CP'):
  sources.extend(
       ['app_content_protection_noship.c',
        'cp_utils.c',
       ]
  )

if env['PROC'] == 'scorpion':
  target_name = 'tclapp'
  app_name = 'Aliapy'
else:
  target_name = 'tclapp64'
  app_name = 'Aliapy64'

#-------------------------------------------------------------------------------
# Add metadata to image
#-------------------------------------------------------------------------------
md = {
   'appName':    app_name,
   'privileges': ['default',
                  'I2C',
                  'OEMUnwrapKeys',
                  'CertValidate',
                  'SPI',
                  'TLMM',
                  'SecureDisplay',
                  'IntMask',
                  'OEMBuf',
                  'TransNSAddr',
                 ],
}
if env['PROC'] == 'scorpion':
  md['memoryType'] = 'Unprotected'

tclapp_units = env.SecureAppBuilder(
  sources = sources,
  includes = includes,
  metadata = md,
  image = target_name,
)

for image in env['IMAGE_ALIASES']:
  op = env.Alias(image, tclapp_units)
