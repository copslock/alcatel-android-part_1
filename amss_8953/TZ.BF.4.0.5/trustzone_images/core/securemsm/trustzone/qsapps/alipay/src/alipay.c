/* Copyright (C) 2016 Tcl Corporation Limited */
/*
@file app_main.c
@brief App main entry point.

*/
/*===========================================================================
   Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //source/qcom/qct/core/pkg/trustzone/rel/2.0/trustzone_images/core/securemsm/trustzone/qsapps/sampleapp/src/app_main.c#19 $
  $DateTime: 2014/09/11 16:15:52 $
  $Author: pwbldsvc $
cat /d/tzdbg/qsee_log

# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
  08/4/14   pdosi      Added missing fs api tests
===========================================================================*/
#include <stdarg.h>
#include <stdio.h>
#include "biometric_result.h"
#include "qsee_clk.h"
#include "qsee_log.h"
#include "qsee_services.h"
#include "qsee_timer.h"
#include "qsee_heap.h"
#include "qsee_fuse.h"
#include "qsee_prng.h"
#include "qsee_cipher.h"
#include "qsee_kdf.h"
#include "qsee_stor.h"
#include "qsee_hmac.h"
#include "qsee_hash.h"
#include "qsee_services.h"
#include "qsee_sfs.h"
#include "qsee_core.h"
#include "qsee_fs.h"

#include "common.h"

#define MAX_FINGERS_PER_USER 10
#define MIN_GET_ID_LIST_BUFFER_LENGTH    ((sizeof(uint32_t) + sizeof(int32_t)) * MAX_FINGERS_PER_USER)
#define FP_LIST_FILE "/data/ifaa_fplist"
#define ALIPAYSFS "/persist/data/provision.dat"
#define gSS 0x27

static const uint32_t MAGICN=0x574D434C;
static const uint8_t k1[QSEE_AES256_KEY_SIZE]={ 0xF3, 0x34, 0x73, 0x17, 0xA5, 0x84, 0x74, 0x11, 0x13, 0x29, 0xE1, 0x63, 0xF7, 0xC9, 0xB5, 0xFF, 0xFF, 0x43, 0x37, 0x91, 0xF7, 0x47, 0x53, 0x17, 0x23, 0x93, 0xE7, 0x79, 0xF7, 0xC7, 0x41, 0xF4 };
static uint8_t k2[QSEE_AES256_KEY_SIZE];
static uint8_t k3[QSEE_AES256_KEY_SIZE];
static uint8_t k4[QSEE_AES256_KEY_SIZE];
static const char label2[] = "TCL alipay crypto label";
static const char label3[] = "TCL alipay hmac label";
static const uint8_t context[QSEE_AES256_KEY_SIZE] = {0x8b,0x95,0xC8,0x4a,0x7c,0xc6,0x41,0x66,0x71,0x57,0xc3,0xca,0x29,0xcd,0x7e,0xa5, 0x7f,0xf5,0x57,0x9f,0x3e,0xeb,0x5d,0x65,0x59,0x8b,0x59,0x7a,0x33,0xa7,0x9b,0x87};
static uint8_t iv[] =
{
  0x71, 0x53, 0x37, 0x43, 0x59, 0x67, 0x83, 0x97,
  0xa8, 0xb6, 0xc5, 0xd7, 0xe3, 0x07, 0x47, 0xf4
};

#define QBLEN 4096
static uint8_t qbuf[QBLEN];
#define KLEN 515
#define DEVLEN 40

#define errchk(fmt, ret) \
{ \
  if(ret) \
  { \
    LOGE(fmt, ret); \
    return; \
  } \
}

#define errchkret(fmt, ret) \
{ \
  if(ret) \
  { \
    LOGE(fmt, ret); \
    return ret; \
  } \
}

typedef struct
{
  int cmd;
  int len;
}__attribute__((packed)) qseecom_cmd;

typedef int IFAA_Result;

typedef enum {
    IFAA_NO = 0,
    IFAA_YES
} IFAABoolean;

typedef struct
{
  uint8_t *buf;
  uint32_t len;
}vlb_t;

typedef struct
{
  vlb_t n;
  vlb_t d;
  vlb_t e;
}IFAA_RsaKey;

extern IFAA_Result IFAA_TaInvokeCmd(void* in, uint32_t in_len, void* out, uint32_t *out_len);
extern IFAA_Result IFAA_TaAddEntry(int bioType, int entry, void* func);
extern IFAA_Result IFAA_GetFpAuthenticatorVersion(uint32_t *version);
extern IFAA_Result IFAA_GetFpLastIdentifiedResult(uint8_t *buf_id, uint32_t *id_len);
extern IFAABoolean IFAA_FpIdCompare(const uint8_t* buf_l, const uint8_t* buf_r, uint32_t buf_len);
extern IFAA_Result IFAA_RsaSignDigest(const IFAA_RsaKey* key, const uint8_t* digest, uint32_t digest_len, uint8_t* sig, uint32_t* sig_len);

IFAA_Result IFAA_GetFpEnrolledIdList(uint8_t *buf_id_list, uint32_t *id_list_len)
{
  LOGD("entern %s", __func__);
  int fd;
  uint8_t fp_list_in_file[sizeof(uint32_t) + MAX_FINGERS_PER_USER * sizeof(int32_t)];
  uint32_t i = 0;
  uint32_t *s = NULL;
  uint32_t len = sizeof(uint32_t);
  int32_t fp_count;
  int32_t f_ret;

  if(NULL == buf_id_list || NULL == id_list_len || MIN_GET_ID_LIST_BUFFER_LENGTH > *id_list_len)
  {
    LOGE("Invalid parameters");
    return 0x7a000003; //IFAA_ERR_BAD_PARAM;
  }
  fd = open((const char *) FP_LIST_FILE, O_RDWR);
  if(fd < 0)
  {
    LOGE("open %s failed", FP_LIST_FILE);
    return 0x7a000001; //IFAA_unkonw_err
  }
  f_ret = read(fd, fp_list_in_file, sizeof(fp_list_in_file));
  if(f_ret < 0)
  {
    LOGE("read %s failed ", FP_LIST_FILE);
    return 0x7a000001; //IFAA_unkonw_err
  }
  hexdump2("fp islist", fp_list_in_file, f_ret);

  s = (uint32_t*) fp_list_in_file;
  fp_count = *s;
  *id_list_len = fp_count * (sizeof(uint32_t) + sizeof(int32_t));
  s++;
  while (i < fp_count)
  {
    memcpy(buf_id_list, &len, sizeof(uint32_t));
    buf_id_list += 4;

    memcpy(buf_id_list, s + i, sizeof(uint32_t));
    buf_id_list += 4;
    i++;
  }

  return 0;
}

IFAA_Result TEE_GetFpLastIdentifiedResult(int32_t* id)
{
  LOGD("entern %s", __func__);

  unsigned long long time;
  int lastid=0;
  uint8_t datatn[32];
  BIO_ERROR_CODE ret=0;
  bio_result bio;
  bio.method = BIO_FINGERPRINT_MATCHING;
  uint64_t uptime_counter;
  bio_buffer authenticator_ta_name;
  authenticator_ta_name.data = datatn;
  authenticator_ta_name.size = 32;

  if(NULL == id)
  {
    LOGE("Invalid parameters");
    return 0x7a000003; //IFAA_ERR_BAD_PARAM;
  }

  ret = bio_get_auth_result(&bio, &uptime_counter, NULL, &authenticator_ta_name);

  LOGD("bio_get_auth_result ret=%d", ret);
  LOGD("BIO_AUTH_METHOD methoed=%d", bio.method);
  LOGD("BIO result=%d", bio.result);
  LOGD("BIO userid=%lld", bio.user_id);
  LOGD("BIO userentityid=%lld", bio.user_entity_id);
  LOGD("BIO transaction_id=%lld", bio.transaction_id);
  LOGD("BIO uptime=%lld", uptime_counter);
  hexdump2("authenticator_ta_name", authenticator_ta_name.data, authenticator_ta_name.size);

  if(!ret && bio.method == BIO_FINGERPRINT_MATCHING && ! memcmp(authenticator_ta_name.data, "goodixfp", 8))
  {
    time=qsee_get_uptime() - uptime_counter;
    LOGD("elapse time %lld", time);
    if(time < 3000)
      *id=(int)bio.user_entity_id;
    else
    {
      LOGE("last id time out");
      return 0x7a000007; //timeout
    }
  }
  else
  {
    LOGE("auth something error");
    return 0x7a000001; //unknow
  }

  LOGD("id is %x", *id);
  return 0;
}

static int getFromSFS(qsee_secctrl_secure_status_t *ss)
{
  int ret = 0;
  int fd = qsee_sfs_open(ALIPAYSFS, O_RDONLY);
  int len = sizeof(qsee_secctrl_secure_status_t)+KLEN+DEVLEN;
  if(0 == fd)
  {
    ret = qsee_sfs_error(fd);
    LOGE("qsee_sfs_open %s failed errno=%d", ALIPAYSFS, ret);
    return ret;
  }
  ret = qsee_sfs_read(fd, (char *)qbuf+3072, len);
  if(ret != len)
  {
    LOGE("qsee_sfs_read %s ret=%d, errno=%d", ALIPAYSFS, ret, qsee_sfs_error(fd));
    qsee_sfs_close( fd );
    return ret;
  }
  ret = qsee_sfs_close(fd);
  if(ret != 0)
    LOGW("qsee_sfs_close %s ret=%d, errno=%d", ALIPAYSFS, ret, qsee_sfs_error(fd));

  if(*((uint32_t *)(qbuf+3072)) != ss->value[0])
  {
    LOGE("secure state not match dev=%x, data=%x", ss->value[0], *((uint32_t *)(qbuf+3072)));
    return -1;
  }


  return ret;
}

static int getFromRpmb(qsee_secctrl_secure_status_t *ss)
{
  int ret = -1;
  LOGD("not support");
  return ret;
}

static int getRsa(IFAA_RsaKey * rsa)
{
  int ret=0;
  qsee_secctrl_secure_status_t ss;

  ret = qsee_get_secure_state(&ss);
  errchkret("get secure state ret = %d", ret);
  if(ss.value[0] & gSS)
    ret = getFromSFS(&ss);
  else
    ret = getFromRpmb(&ss);

  rsa->n.buf = qbuf+3072+sizeof(qsee_secctrl_secure_status_t);
  rsa->n.len = 256;
  rsa->e.buf = qbuf+3072+512+sizeof(qsee_secctrl_secure_status_t);
  rsa->e.len = 3;
  rsa->d.buf = qbuf+3072+256+sizeof(qsee_secctrl_secure_status_t);
  rsa->d.len = 256;
/*
  hexdump2("n", rsa->n.buf, rsa->n.len);
  hexdump2("e", rsa->e.buf, rsa->e.len);
  hexdump2("d", rsa->d.buf, rsa->d.len);
*/
  return ret;
}

IFAA_Result IFAA_AuthenticatorSignDigest(const uint8_t* digest, uint32_t digest_len, uint8_t* signature, uint32_t* sig_len)
{
  IFAA_Result ret = 0; //IFAA_ERR_SUCCESS;
  IFAA_RsaKey rsaprikey;

  LOGD("AuthenticatorSignDigest");
  if (!digest || !signature|| digest_len <= 0)
  {
    LOGE("Invalid parameters");
    return 0x7a000003; //IFAA_ERR_BAD_PARAM;
  }

  ret = getRsa(&rsaprikey);
  if(ret)
  {
    LOGE("get rsa failed ret=%d", ret);
    return 0x7a000001;
  }

  ret = IFAA_RsaSignDigest(&rsaprikey, digest, digest_len, signature, sig_len);
  if (ret != 0) //IFAA_ERR_SUCCESS)
  {
    LOGE("AuthenticatorSignDigest err!");
    return 0x7a000003; //IFAA_ERR_BAD_PARAM;
  }
  return ret;
}
int32_t TEE_GetAuthenticatorVersion(void)
{
  return 1;
}

IFAA_Result TEE_GetDeviceId(uint8_t *devid, uint32_t *devidlen)
{
  int ret=0;
  qsee_secctrl_secure_status_t ss;
  if(NULL == devid || 40 != *devidlen)
  {
    LOGE("devid null or devidlen != 40!");
    return 0x7a000003; //IFAA_ERR_BAD_PARAM;
  }

  ret = qsee_get_secure_state(&ss);
  errchkret("get secure state ret = %d", ret);
  if(ss.value[0] & gSS)
    ret = getFromSFS(&ss);
  else
    ret = getFromRpmb(&ss);
  if(ret)
  {
    LOGE("get dev id ret=%d", ret);
    return 0x7a000001;
  }
  memcpy(devid, qbuf+3072+sizeof(qsee_secctrl_secure_status_t)+KLEN, *devidlen);
  hexdump2("devid", devid, *devidlen);

  return ret;
}

int aesCrypto(QSEE_CIPHER_ALGO_ET alg, QSEE_CIPHER_MODE_ET mode, uint8_t *msg, uint32_t msglen, 
           uint8_t *key, uint32_t keylen, uint8_t *iv, uint32_t ivlen, uint8_t *tmsg, uint32_t *tmsglen, bool enc)
{
  int ret=0;
  qsee_cipher_ctx *ctx = 0;
  QSEE_CIPHER_PAD_ET pad = QSEE_CIPHER_PAD_PKCS7;  //NO_PAD; //
  
  LOGD("enter aesCrypto");
  if(qsee_cipher_init(alg, &ctx) < 0)
  {
    LOGE("qsee_cipher_init failed");
    ret = -1;
    goto free;
  }

  if(qsee_cipher_set_param(ctx, QSEE_CIPHER_PARAM_KEY, key, keylen) < 0)
  {
    LOGE("qsee_cipher__set_param key failed");
    ret = -1;
    goto free;
  }

  if(qsee_cipher_set_param(ctx, QSEE_CIPHER_PARAM_MODE, &mode, sizeof(mode)) < 0)
  {
    LOGE("qsee_cipher__set_param mode failed");
    ret = -1;
    goto free;
  }

  if(iv == NULL || qsee_cipher_set_param(ctx, QSEE_CIPHER_PARAM_IV, iv, ivlen) < 0)
  {
    LOGE("qsee_cipher__set_param iv failed");
    ret = -1;
    goto free;
  }

  if(qsee_cipher_set_param(ctx, QSEE_CIPHER_PARAM_PAD, &pad, sizeof(pad)) < 0)
  {
    LOGE("qsee_cipher__set_param pad failed");
    ret = -1;
    goto free;
  }

  if(enc)
  {
    //LOGD("msglen=%d, tmsglen=%d", msglen, *tmsglen);
    ret = qsee_cipher_encrypt(ctx, msg, msglen, tmsg, tmsglen);
    //LOGD("msglen=%d, tmsglen=%d ret=%d", msglen, *tmsglen, ret);
  }
  else
  {
    //qsee_set_bandwidth
    ret = qsee_cipher_decrypt(ctx, msg, msglen, tmsg, tmsglen);
  }


free:
  if(ctx)
    qsee_cipher_free_ctx(ctx);
  LOGD("exit aesCrypto ret=%d", ret);

  return ret;
}


void tz_app_init(void)
{
  qsee_log_set_mask(QSEE_LOG_MSG_ERROR | QSEE_LOG_MSG_DEBUG | QSEE_LOG_MSG_FATAL);
  LOGD("alipay Init ");

  IFAA_TaAddEntry(1, 1, IFAA_GetFpLastIdentifiedResult);
  IFAA_TaAddEntry(1, 2, IFAA_GetFpAuthenticatorVersion);
  IFAA_TaAddEntry(1, 3, IFAA_GetFpEnrolledIdList);
  IFAA_TaAddEntry(1, 4, IFAA_FpIdCompare);
}

static void processAliCmd(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen)
{
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;
  IFAA_Result ret=0;

  if(qsci->len < sizeof(qseecom_cmd))
  {
    LOGW("no ali payload len=%d", qsci->len);
    qsco->len = 0;
    *(int *)(((char *)qsco)+cmdlen) = -1;
    return;
  }
  rsplen -= sizeof(IFAA_Result);
  ret = IFAA_TaInvokeCmd((void *)(qsci+1), qsci->len-sizeof(qseecom_cmd), (((char *)qsco) + cmdlen + sizeof(IFAA_Result)), &rsplen);
  if(ret)
    LOGE("Seems IFAA_TaInvokeCmd failed ret=%d", ret);
  qsco->len = rsplen;
  *(int *)(((char *)qsco)+cmdlen) = ret;
  hexdump2("Process alicmd", (((char *)qsco) + cmdlen), rsplen+sizeof(IFAA_Result));
  return;
}

static int initKey23()
{
  int ret=0;

  ret = qsee_kdf((const void *)k1, QSEE_AES256_KEY_SIZE, (const void *)label2, strlen(label2), (const void *)context, 32, (void *)k2, QSEE_AES256_KEY_SIZE);
  errchkret("kdf crypto ret=%d", ret);
  ret = qsee_kdf((const void *)k1, QSEE_AES256_KEY_SIZE, (const void *)label3, strlen(label3), (const void *)context, 32, (void *)k3, QSEE_AES256_KEY_SIZE);
  errchkret("kdf hmac ret=%d", ret);

  return ret;
}

static int saveToSFS(uint8_t *buf, int len)
{
  int ret = 0;
  int fd = qsee_sfs_open(ALIPAYSFS, O_RDWR | O_CREAT | O_TRUNC );
  if(0 == fd)
  {
    ret = qsee_sfs_error(fd);
    LOGE("qsee_sfs_open %s failed errno=%d", ALIPAYSFS, ret);
    return ret;
  }
  ret = qsee_sfs_write(fd, (const char *)buf, len);
  if(ret != len)
  {
    LOGE("qsee_sfs_write %s ret=%d, errno=%d", ALIPAYSFS, ret, qsee_sfs_error(fd));
    qsee_sfs_close( fd );
    return ret;
  }
  ret = qsee_sfs_close(fd);
  if(ret != 0)
    LOGW("qsee_sfs_close %s ret=%d, errno=%d", ALIPAYSFS, ret, qsee_sfs_error(fd));

  return ret;
}

static int saveToRpmb(uint8_t *buf, int len)
{
  LOGD("not support");
  return -1;
}

static int saveData(uint8_t *buf, int len)
{
  int ret = 0;
  qsee_secctrl_secure_status_t * ss = (qsee_secctrl_secure_status_t *)buf;

  if(ss->value[0] & gSS)
    ret = saveToSFS(buf, len);
  else
    ret = saveToRpmb(buf, len);

  return ret;
}

static void processData(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen)
{
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;
  int ret=0;
  int tlen = QBLEN/2; // QBLEN-sizeof(int);
  bool enc=true;

  *(int *)(((char *)qsco)+cmdlen) = -1;
  if(qsci->len < sizeof(qseecom_cmd))
  {
    LOGW("no ali payload len=%d", qsci->len);
    return;
  }

  if(qsci->len > QBLEN/2)
  {
    LOGE("Too long data len=%d", qsci->len);
    return;
  }

  ret = initKey23();
  errchk("init key ret=%d", ret);
  ret = aesCrypto(QSEE_CIPHER_ALGO_AES_256, QSEE_CIPHER_MODE_CBC, (uint8_t *)(qsci+1), qsci->len-sizeof(qseecom_cmd), k2, QSEE_AES256_KEY_SIZE, iv, sizeof(iv), qbuf+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t), (uint32_t *)&tlen, enc);
  errchk("aes crypto ret=%d", ret);
  LOGD("tlen=%d", tlen);
  *((uint32_t *)qbuf) = MAGICN;
  ret = qsee_get_secure_state((qsee_secctrl_secure_status_t *)(qbuf+sizeof(uint32_t)));
  errchk("qsee_get_secure_state ret=%d", ret);
  ret = qsee_hmac(QSEE_HMAC_SHA256, (const uint8_t *)qbuf, tlen+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t), k3, QSEE_AES256_KEY_SIZE, qbuf+tlen+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t));
  errchk("hmac ret=%d", ret);
  memcpy((((char *)qsco) + cmdlen + sizeof(int)),qbuf,  tlen+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t)+QSEE_HMAC_DIGEST_SIZE_SHA256);
  *(int *)(((char *)qsco)+cmdlen) = 0;
  qsco->len = tlen+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t)+QSEE_HMAC_DIGEST_SIZE_SHA256;
}

static void provisionData(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen)
{
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;
  int ret=0;
  int tlen = QBLEN/2; // QBLEN-sizeof(int);
  bool enc=false;
  int len = qsci->len;
  uint8_t *encBuf=NULL;
  int encLen=0;
  uint8_t *decBuf=NULL;
  

  *(int *)(((char *)qsco)+cmdlen) = -1;
  if(len < sizeof(qseecom_cmd))
  {
    LOGW("no ali payload len=%d", len);
    return;
  }

  LOGD("provision data");

  ret = initKey23();
  errchk("init key ret=%d", ret);
  ret = qsee_hmac(QSEE_HMAC_SHA256, (const uint8_t *)(qsci+1), len-sizeof(qseecom_cmd)-QSEE_SHA256_HASH_SZ-DEVLEN * 2, k3, QSEE_AES256_KEY_SIZE, qbuf);
  if(memcmp(qbuf, (uint8_t *)(qsci+1)+len-sizeof(qseecom_cmd)-QSEE_SHA256_HASH_SZ-DEVLEN * 2, QSEE_SHA256_HASH_SZ))
  {
    LOGE("compare HMAC error");
    hexdump2("ori hmac:", (uint8_t *)(qsci+1)+len-QSEE_SHA256_HASH_SZ-DEVLEN * 2, QSEE_SHA256_HASH_SZ);
    hexdump2("calc hmac:", qbuf, QSEE_SHA256_HASH_SZ);
    return;
  }
  if(*((uint32_t *)(qsci+1)) != MAGICN)
  {
    LOGE("Magic is diff %x", *((uint32_t *)(qsci+1)));
    return;
  }
  ret = qsee_get_secure_state((qsee_secctrl_secure_status_t *)(qbuf+QSEE_SHA256_HASH_SZ));
  errchk("qsee_get_secure_state ret=%d", ret);
  if(memcmp(qbuf+QSEE_SHA256_HASH_SZ, ((uint32_t *)(qsci+1)+1), sizeof(qsee_secctrl_secure_status_t)))
  {
    LOGE("secure status diff dev=%x, data=%x", *((uint32_t *)(qbuf+QSEE_SHA256_HASH_SZ)), *((uint32_t *)(qsci+1)+1));
    return;
  }
  encBuf = (uint8_t *)(qsci+1)+sizeof(uint32_t)+sizeof(qsee_secctrl_secure_status_t);
  encLen = len-sizeof(qseecom_cmd)-sizeof(uint32_t)-sizeof(qsee_secctrl_secure_status_t)-QSEE_SHA256_HASH_SZ-DEVLEN * 2;
  decBuf = qbuf+QSEE_SHA256_HASH_SZ+sizeof(qsee_secctrl_secure_status_t);
  LOGD("encLen=%d", encLen);
  ret = aesCrypto(QSEE_CIPHER_ALGO_AES_256, QSEE_CIPHER_MODE_CBC, encBuf, encLen, k2, QSEE_AES256_KEY_SIZE, iv, sizeof(iv), decBuf, (uint32_t *)&tlen, enc);
  errchk("aes crypto ret=%d", ret);
  LOGD("tlen=%d", tlen);
  if(memcmp(((uint8_t *)(qsci))+len-DEVLEN*2, ((uint8_t *)(qsci))+len-DEVLEN, DEVLEN))
  {
    LOGE("device id error");
    hexdump2("dev id 1 : ", ((uint8_t *)(qsci))+len-DEVLEN*2, DEVLEN);
    hexdump2("dev id 2 : ", ((uint8_t *)(qsci))+len-DEVLEN, DEVLEN);
    return;
  }
  memcpy(decBuf+tlen, ((uint8_t *)(qsci))+len-DEVLEN, DEVLEN);
  ret = saveData(qbuf+QSEE_SHA256_HASH_SZ, sizeof(qsee_secctrl_secure_status_t)+tlen+DEVLEN);
  errchk("save data ret=%d", ret);
  *(int *)(((char *)qsco)+cmdlen) = 0;
  qsco->len=0;
}

static void authData(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen)
{
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;
  IFAA_RsaKey rsaprikey;

  if(getRsa(&rsaprikey))
  {
    LOGE("auth data failed");
    *(int *)(((char *)qsco)+cmdlen) = -1;
  }
  else
  {
    LOGE("auth data ok");
    *(int *)(((char *)qsco)+cmdlen) = 0;
  }  
}

void tz_app_cmd_handler(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen)
{
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;

  if(NULL == cmd || NULL == rsp || 0 == cmdlen || 0 == rsplen)
  {
    LOGE("cmd=%x, in_len=%d, rsp=%x, out_len=%d", cmd, cmdlen, rsp, rsplen);
    return;
  }
  LOGD("cmd = %d, cmdlen=%d, rsplen=%d", qsci->cmd, cmdlen, rsplen);

  switch(qsci->cmd)
  {
  case 3:
    processAliCmd(cmd, cmdlen, rsp, rsplen);
    break;
  case 5:
    processData(cmd, cmdlen, rsp, rsplen);
    break;
  case 6:
    provisionData(cmd, cmdlen, rsp, rsplen);
    break;
  case 7:
    authData(cmd, cmdlen, rsp, rsplen);
    break;
  default:
    LOGW("Not support cmd=%d", qsci->cmd);
    qsco->len = 0;
    *(int *)(((char *)qsco)+cmdlen) = -1;
    break;
  }


  return;
}


void tz_app_shutdown(void)
{
  LOGD("aliay exit");
}

