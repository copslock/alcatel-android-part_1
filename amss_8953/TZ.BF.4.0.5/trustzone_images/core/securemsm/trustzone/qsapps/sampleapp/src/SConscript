#===========================================================================
#  Copyright (c) 2011-2016 QUALCOMM Incorporated.
#  All Rights Reserved.
#  Qualcomm Confidential and Proprietary
#===========================================================================
#
#
# GENERAL DESCRIPTION
#    build script
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
Import('env')

if env.has_key('USES_NO_CP'):
  env.Append(CCFLAGS = ' -DUSES_NO_CP ')

env.Append(CPPDEFINES = 'SAMPLE_APP=1')

test_fuse_inc = ['#../../core/securemsm/trustzone/qsapps/sampleapp/inc/' + env['CHIPSET']]
includes = ['#../../core/securemsm/trustzone/qsapps/sampleapp/inc',
            '#../../core/api/kernel/libstd/stringl',
            '#../../core/securemsm/trustzone/qsapps/smplserv/inc',
            '#../../core/securemsm/sse/qsee/SecureUI/inc',
            '#../../core/api/boot/qfprom',
            '#../../core/securemsm/secmath/shared/inc',
            '#../../core/api/securemsm/trustzone/gp',
            '#../../core/securemsm/trustzone/qsapps/libs/applib/qsee/src',
            '#../../core/securemsm/sse/qsee/SecureUILib/include',
            '#../../core/securemsm/sse/qsee/SecureTouch/drTs/include',
            '#../../core/securemsm/trustzone/qsapps/libs/biometric/inc',
            '#../../core/kernel/smmu/v2/inc/',
            '#../../core/securemsm/trustzone/qsee/mink/include',
           ]
includes.extend(test_fuse_inc)

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------
sources = [
        'app_main.c',
        'app_smplserv_test.c',
        'app_smplcert_test.c',
        'app_crypto.c',
        'app_sfs.c',
        'app_rsa.c',
        'qsee_api_tests.c',
        'app_cmnlib.c',
        'app_message_passing.c',
        'app_fuses.c',
        'app_stor.c',
        'app_content_protection.c',
        'app_secure_ui.c',
        'app_bio_lib.c',
        'app_bulletin_board.c',
]

if env.has_key('USES_NO_CP'):
  sources.extend(
       ['app_content_protection_noship.c',
        'cp_utils.c',
       ]
  )

if env['PROC'] == 'scorpion':
  target_name = 'smplap32'
  app_name = 'SampleApp'
else:
  target_name = 'smplap64'
  app_name = 'SampleApp64'

#-------------------------------------------------------------------------------
# Add metadata to image
#-------------------------------------------------------------------------------
md = {
   'appName':    app_name,
   'privileges': ['default',
                  'I2C',
                  'OEMUnwrapKeys',
                  'CertValidate',
                  'SPI',
                  'TLMM',
                  'SecureDisplay',
                  'IntMask',
                  'OEMBuf',
                  'TransNSAddr',
                 ],
}
if env['PROC'] == 'scorpion':
  md['memoryType'] = 'Unprotected'

sampleapp_units = env.SecureAppBuilder(
  sources = sources,
  includes = includes,
  metadata = md,
  image = target_name
)

for image in env['IMAGE_ALIASES']:
  op = env.Alias(image, sampleapp_units)
