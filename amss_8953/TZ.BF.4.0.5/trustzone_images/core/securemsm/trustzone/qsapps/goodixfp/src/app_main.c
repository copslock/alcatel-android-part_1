/* Copyright (C) 2016 Tcl Corporation Limited */
/*
@file app_main.c
@brief App main entry point.

*/
/*===========================================================================
   Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //source/qcom/qct/core/pkg/trustzone/rel/2.0/trustzone_images/core/securemsm/trustzone/qsapps/sampleapp/src/app_main.c#19 $
  $DateTime: 2014/09/11 16:15:52 $
  $Author: pwbldsvc $
cat /d/tzdbg/qsee_log

# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
  08/4/14   pdosi      Added missing fs api tests
===========================================================================*/
#include <stdarg.h>
#include <stdio.h>
#include "qsee_sfs.h"
#include "qsee_spi.h"
#include "qsee_log.h"

#include "gx_product.h"
#define QSEE4_0_5
#ifdef QSEE4_0_5
#include "qsee_blsp.h"
/*
 * Modify Ownership of QUP to particular subsystem
 * QSEE_QUP_DEVICE_ID between QSEE_QUP_DEVICE_ID_1 to QSEE_QUP_DEVICE_ID_12
*/
#define QSEE_QUP_DEVICE_ID QSEE_QUP_DEVICE_ID_8

#ifdef MSM8917
#define QSEE_QUP_DEVICE_ID QSEE_QUP_DEVICE_ID_8
#endif

#endif

/** 
 * Sample app name 
 * Modify the app name to your specific app name  
 */
extern char TZ_APP_NAME[];
extern void gx_tz_app_init(void);
extern void gx_tz_app_cmd_handler(void* cmd, uint32_t cmdlen, void* rsp, uint32_t rsplen);
extern void gx_tz_app_shutdown(void);
const int msg_debug_enable = 1;
const int msg_error_enable = 1;
qsee_spi_device_id_t spi_device_id = QSEE_SPI_DEVICE_8;

const char FP_ROOT_DIR[] = "/data/";
//const char GX_FILE_ROOT[] = "/persist/data/";
const char TEMPLATE_INDEX_TABLE_PATH[] = "/data/test";
const char g_fp_root_dir[] = {"/data/"};
const GF_FS_Type g_file_system_type = FS_ENC_SW;
const GF_Product_t gf_product_current = MILANE;
const int Customer_Define_Enroll_Restrict_Duplicate = 0; // 0:1
const int Customer_Define_Enroll_Restrict_Rollback = 0; // 0:1
const int Customer_Define_Enroll_Max_Record_Num = 0; //resvse
const int Customer_Define_Enroll_Max_Overlay_Ratio = 85;
const int Customer_Define_Enroll_Min_Enroll_Count = 8;
const int Customer_Define_Enroll_Max_Preoverlay_Ratio = 70;
const int Customer_Define_Enroll_image_coverage = 65;
const int Customer_Define_Enroll_image_quality = 25;
const int Customer_Define_Template_Register_Count = 12;
const int Customer_Define_Template_Study_Count = 0;
const int Customer_Define_Identify_image_coverage = 65;
const int Customer_Define_Identify_image_quality = 15;
const int Customer_Define_Identify_image_retry_time = 2;

const int Customer_Define_Mp_Test_Base_Num = 1;
const int Customer_Define_Mp_Test_Image_Num = 5;
const int Customer_Define_Sensor_Defect_Pixel_Test = 1; //0:1

void TEE_LogPrintf(int level, const char* format, ...)
{
    va_list args;
	char log_str[1024];

	/* Temporary workaround for not trying to print floats
     * that will crash the MSM9884 platform */
    if (strstr(format, "%f") || strstr(format, "%.") || strstr(format, "%10f")) {
    	qsee_log(QSEE_LOG_MSG_ERROR,"Trying to print floats %s", format);
  	} else {
		va_start(args, format);
		vsprintf(log_str, format, args);
		if(level) {
    		qsee_log(QSEE_LOG_MSG_ERROR, "%s", log_str);
		} else {
			qsee_log(QSEE_LOG_MSG_ERROR, "%s", log_str);
		}
    	va_end(args);
	}
}

void tz_app_init(void)
{
#ifdef QSEE4_0_5
    qsee_blsp_modify_ownership(QSEE_QUP_DEVICE_ID-1, QSEE_TZ_ID);
#endif
    gx_tz_app_init();
}

void tz_app_cmd_handler(void* cmd, uint32_t cmdlen, 
                        void* rsp, uint32_t rsplen)
{
	QSEE_LOG(QSEE_LOG_MSG_LOW, "tz_app_cmd_handler entered");

	if (cmd == NULL || rsp == NULL || cmdlen == 0 || rsplen == 0){
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "TZ App invalid inparam");
		return;
	}

    gx_tz_app_cmd_handler(cmd, cmdlen, rsp, rsplen);
}

/**
  @brief 
    App specific shutdown
    App will be given a chance to shutdown gracefully
*/
void tz_app_shutdown(void)
{
    gx_tz_app_shutdown();
}

