/* Copyright (C) 2016 Tcl Corporation Limited */
/*===========================================================================

                            EDIT HISTORY FOR FILE
# who       when     what, where, why
# --------   ---     ---------------------------------------------------------
  Antares 20160127, initial.
# --------   ---     ---------------------------------------------------------

                            IMPORTANT NOTE
# Pls modified g_file_system_type in app_main.c to FS_OEM to test after you
# done the implement thing.
===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <qsee_fs.h>

/**
 * make dir in file system implemented by oem
 * @param path
		   [in]  path: e.g., "/fingerprint"
 * @return int: 0 for success, -1 upon failure
 */
int oem_fs_mkdir(const char* path){ return -1; }

/**
 * delete dir in file system implemented by oem
 * @param path
		   [in]  path: e.g., "/fingerprint"
 * @return int: 0 for success, -1 upon failure
 */
int oem_fs_rmdir(const char* path){ return -1; }

/**
 * open file for read/write in file system implemented by oem
 * @param path
		   [in]  path: e.g., "/fingerprint/template.bin"
 * @param flags
		   [in]  access control; ref for <qsee_fs.h>; e.g.,
				 O_RDONLY, O_WRONLY, O_RDWR, O_CREAT, O_TRUNC
 * @return int: > 0 for success; <=0 upon failure
 */
int oem_fs_open(const char* path, int flags){ return -1; }

/**
 * read file
 * @param fd
		   [in]  value return by oem_fs_open successfully executed
 * @param buf
		   [out] buffer for holding file data
 * @param nbytes
		   [in]  bytes need reading;
 * @return int:
 *         nbytes for success;
 *         >=0 but < nbytes if (current offset + nbytes >= file size)
 *         -1 upon failure
 */
int oem_fs_read(int fd, char *buf, int nbytes){ return -1; }

/**
 * write file
 * @param fd
		   [in]  value return by oem_fs_open successfully executed
 * @param buf
		   [in] buffer for holding data
 * @param nbytes
		   [in]  bytes need writing;
 * @return int:
 *         nbytes for success;
 *         >=0 but < nbytes if (current offset + nbytes > max file size)
 *         -1 upon failure
 */
int oem_fs_write(int fd, const char *buf, int nbytes){ return -1; }

/**
 * close file in file system implemented by oem;
 * all data buffered should sync to disk after this call;
 * @param fd
		   [in]  value return by oem_fs_open successfully executed
 * @return int: 0 for success, -1 upon failure
 */
int oem_fs_close(int fd){ return -1; }

/**
 * get file length;
 * @param fd
		   [in]  value return by oem_fs_open successfully executed
 * @return int: actual size for success, -1 upon failure
 */
int oem_fs_getLength(int fd){ return -1; }

/**
 * remove file from file system implemented by oem
 * @param path
		   [in]  path: e.g., "/fingerprint/template.bin"
 * @return int: 0 for success; -1 upon failure
 */
int oem_fs_remove(const char* path){ return -1; }

/**
 * set file read/write position;
 * note: if the absolute offset large than the file size, enlarge it with 0 filled.
 * @param fd
		   [in]  value return by oem_fs_open successfully executed
 * @param offset
		   [in]  different value base on flags
 * @param flags
		   [in]  base select for offset; ref for <qsee_fs.h>; e.g.,
				 SEEK_SET, SEEK_CUR, SEEK_END
 * @return int: absolute offset of read/write for success, -1 upon failure
 */
int oem_fs_seek(int fd, int offset, int flags){ return -1; }
