#ifndef SMMUDYNAMICSID_H
#define SMMUDYNAMICSID_H


#include "SMMUTypes.h"
#include "IxErrno.h"


typedef enum
{
  SMMU_INSTANCE_APPS	   = 0,
  SMMU_INSTANCE_GPU 	   = 1,
  SMMU_INSTANCE_COUNT	   = 2,
  SMMU_INSTANCE_MAX        = 0x7FFFFFFF,
} SMMU_Instance_e_type;


#endif
