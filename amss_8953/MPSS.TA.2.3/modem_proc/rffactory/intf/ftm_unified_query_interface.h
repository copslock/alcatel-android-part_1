#ifndef FTM_UNIFIED_QUERY_INTERFACE_H
#define FTM_UNIFIED_QUERY_INTERFACE_H
/*!
  @file
  ftm_unified_query_interface.h

  @brief
  Common framework to perform FTM Unified Query
*/

/*======================================================================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document are confidential
  and proprietary information of Qualcomm Technologies Incorporated and all rights therein are
  expressly reserved. By accepting this material the recipient agrees that this material and the
  information contained therein are held in confidence and in trust and will not be used, copied,
  reproduced in whole or in part, nor its contents revealed in any manner to others without the
  express written permission of Qualcomm Technologies Incorporated.

======================================================================================================================*/

/*======================================================================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rffactory.mpss/1.0/intf/ftm_unified_query_interface.h#3 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------------------------------------------------
08/18/16   dyx     Add Unified Query Tx Info subcommand related API
08/17/16   chm     Add Active Client Allocations command
06/07/15   jfc     Initial Release

======================================================================================================================*/

#include "comdef.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef T_WINNT
#error code not present
#endif


/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define RFM device mask */
typedef uint16 ftm_unified_query_rfm_device_mask_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define chain */
typedef uint32 ftm_unified_query_chain_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define carrier */
typedef uint32 ftm_unified_query_carrier_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define carrier mask */
typedef uint8 ftm_unified_query_carrier_mask_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define band */
typedef uint32 ftm_unified_query_band_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define channel */
typedef uint32 ftm_unified_query_channel_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define bandwidth */
typedef uint32 ftm_unified_query_bandwidth_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition for Unified Query's  rfm_device.  */
typedef uint32 ftm_unified_query_rfm_device_type;

/*! Type definition for unified_query's sig_path.  */
typedef uint32 ftm_unified_query_sig_path_type;

/*! Type definition for unified_query's antenna_path.  */
typedef uint16 ftm_unified_query_antenna_path_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the version field in unified_query command packet.  */
typedef uint32 ftm_unified_query_version_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the error code mask for response packet. Each bit indicates a type of error. */
typedef uint32 ftm_unified_query_error_code_mask_type;

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_CID
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of commands available for FTM Unified Query command */
typedef enum
{
  FTM_UNIFIED_QUERY_CMD_UNASSIGNED = 0, /*!< 0 : Unassigned command */

  FTM_UNIFIED_QUERY_CMD_QUERY_RECOMMENDED_RADIO_ALLOCATION = 1, /*!< 1 : RF Radio Allocation command */

  FTM_UNIFIED_QUERY_CMD_QUERY_ACTIVE_CLIENT_ALLOCATION = 2, /*!< 2 : RF Active Client Allocation command */

  FTM_UNIFIED_QUERY_CMD_QUERY_UNIFIED_TX_INFO = 3,  /*!< 3 : RF Unified Tx Info Query subcommand */


  FTM_UNIFIED_QUERY_CMD_NUM  /*!< Max : Defines maximum number of command IDs */

} ftm_unified_query_command_enum_type;

/*! @} */

/*====================================================================================================================*/
/*!
  @name Unified TX Info Property ID

  @brief
  Unified TX Info Property Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for Recommended Radio Allocations Command.
  These enumeration are used to define the content of test command packet.
  ftm_trm_rra_property_names[] Must be updated when this list is updated.*/
typedef enum
{
  FTM_UNIFIED_TX_INFO_PROP_UNASSIGNED          = 0,  /*!< Unassigned property */
  FTM_UNIFIED_TX_INFO_PROP_SUB_IDX             = 1,  /*!< Subscription Index from ftm_rf_test_subscriber_enum_type */
  FTM_UNIFIED_TX_INFO_PROP_TECH                = 2,  /*!< Tech from ftm_rf_technology_type values */
  FTM_UNIFIED_TX_INFO_PROP_BAND                = 3,  /*!< Band from sys_band_class_e_type values */
  FTM_UNIFIED_TX_INFO_PROP_CHAN                = 4,  /*!< Chan */
  FTM_UNIFIED_TX_INFO_PROP_BANDWIDTH           = 5,  /*!< Bandwidth in Hz */
  FTM_UNIFIED_TX_INFO_PROP_CARRIER_IDX         = 8,  /*!< Carrier Index 0/1/2/n */
  FTM_UNIFIED_TX_INFO_PROP_DEVICE              = 12, /*!< Device from rfm_device_enum_type values */
  FTM_UNIFIED_TX_INFO_PROP_SIGNAL_PATH         = 13, /*!< Signal */
  FTM_UNIFIED_TX_INFO_PROP_ANTENNA_PATH        = 14, /*!< Antenna Path */
  FTM_UNIFIED_TX_INFO_PROP_ACTION_MAX_TX_POWER        = 15, /*!< Target Max Tx Power*/

  /* ADD MORE ITEMS ABOVE THIS LINE */
  FTM_UNIFIED_TX_INFO_PROP_NUM                      /*!< Max : Defines maximum number of properties */

} ftm_unified_tx_info_property_type;

extern const char *ftm_unified_tx_info_property_names[];

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of subscribers */
typedef enum
{
  FTM_UNIFIED_QUERY_SUBSCRIBER_0 = 0, /*!< 0 : First subscriber */

  FTM_UNIFIED_QUERY_SUBSCRIBER_1 = 1, /*!< 1 : Second subscriber */

  FTM_UNIFIED_QUERY_SUBSCRIBER_NUM,  /*!< Max : Defines maximum number of subscriber IDs */

  FTM_UNIFIED_QUERY_SUBSCRIBER_NA = 0xFF  /*!< Not Applicable */

} ftm_unified_query_subscriber_enum_type;

/*====================================================================================================================*/
/*!

  @name Active Client Allocations Property ID

  @brief
  Active Client Allocations Property ID list
*/
/*! @{ */
      
/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for Active Client Allocations Command. 
  These enumeration are used to define the content of test command packet. 
  ftm_trm_aca_property_names[] Must be updated when this list is updated.*/
typedef enum
{
  FTM_TRM_ACA_PROP_UNASSIGNED          = 0,  /*!< Unassigned property */
  FTM_TRM_ACA_PROP_SUB_IDX             = 1,  /*!< Subscription Index from ftm_rf_test_subscriber_enum_type */
  FTM_TRM_ACA_PROP_TECH                = 2,  /*!< Tech from ftm_rf_technology_type values */
  FTM_TRM_ACA_PROP_RXTX                = 3,  /*!< Indicates if request is for Rx or Tx path from ftm_rf_test_device_radio_trx_t*/
  FTM_TRM_ACA_PROP_CHAIN               = 4,  /*!< Chain 0/1/2/3/n. 0:PRx/Tx 1: DRx >= 2: HO_RxD*/
  FTM_TRM_ACA_PROP_CARRIER_IDX         = 5,  /*!< Carrier Index 0/1/2/n */
  FTM_TRM_ACA_PROP_DEVICE              = 6,  /*!< Device from rfm_device_enum_type values */
  FTM_TRM_ACA_PROP_SIGNAL_PATH         = 7,  /*!< Signal */
  FTM_TRM_ACA_PROP_ANTENNA_PATH        = 8,  /*!< Antenna Path */
  FTM_TRM_ACA_PROP_ANTENNA_NUM         = 9,  /*!< Number of Antennas */
  FTM_TRM_ACA_PROP_CURR_ASDIV_CFG      = 10,  /*!< Current Antenna config for ASDIV, i.e. Port state from trm_ant_switch_state_e_type */
  FTM_TRM_ACA_PROP_CURR_CAL_STATE      = 11,  /*!< Current Antenna Cal state for ASDIV,  */
  FTM_TRM_ACA_PROP_PENDING_ASDIV_CFG   = 12,  /*!< Pending Antenna config for ASDIV */
  FTM_TRM_ACA_PROP_PENDING_CAL_STATE   = 13,  /*!< Pending Antenna Cal state for ASDIV */

  /* ADD MORE ITEMS ABOVE THIS LINE */
  FTM_TRM_ACA_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_trm_aca_property_type;

extern const char *ftm_trm_aca_property_names[];


#ifdef T_WINNT
#error code not present
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* FTM_UNIFIED_QUERY_INTERFACE_H */

