/*!
  @file
  ftm_unified_query_interface.c

  @brief
  Common framework to perform query RF related info in FTM mode
*/

/*======================================================================================================================

  Copyright (c) 2015 - 2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document are confidential 
  and proprietary information of Qualcomm Technologies Incorporated and all rights therein are 
  expressly reserved. By accepting this material the recipient agrees that this material and the 
  information contained therein are held in confidence and in trust and will not be used, copied, 
  reproduced in whole or in part, nor its contents revealed in any manner to others without the 
  express written permission of Qualcomm Technologies Incorporated.

======================================================================================================================*/

/*======================================================================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rffactory.mpss/1.0/src/ftm_unified_query_interface.c#1 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------------------------------------------------
08/03/15   chm     Initial Release

======================================================================================================================*/

#include "comdef.h"
#include "rflm_defs.h"
#include "ftm_unified_query_interface.h"

/*--------------------------------------------------------------------------------------------------------------------*/
/*!
  @brief
  Active Client Allocations Property Name Display Info
*/
const char *ftm_trm_aca_property_names[] = 
{
  "UNASSIGNED",               /* FTM_TRM_ACA_PROP_UNASSIGNED */
  "SUB_IDX",                  /* FTM_TRM_ACA_PROP_SUB_IDX */
  "TECH",                     /* FTM_TRM_ACA_PROP_TECH */
  "RXTX",                     /* FTM_TRM_ACA_PROP_RXTX */
  "CHAIN",                    /* FTM_TRM_ACA_PROP_CHAIN */
  "CARRIER_IDX",              /* FTM_TRM_ACA_PROP_CARRIER_IDX */
  "RFM_DEVICE",               /* FTM_TRM_ACA_PROP_DEVICE */
  "SIG_PATH",                 /* FTM_TRM_ACA_PROP_SIGNAL_PATH */
  "ANTENNA_PATH",             /* FTM_TRM_ACA_PROP_ANTENNA_PATH */
  "ANTENNA_NUM",              /* FTM_TRM_ACA_PROP_ANTENNA_NUM */
  "CURR_ASDIV_CFG",           /* FTM_TRM_ACA_PROP_CURR_ASDIV_CFG */
  "CURR_ASDIV_CAL",           /* FTM_TRM_ACA_PROP_CURR_CAL_STATE */
  "PEND_ASDIV_CFG",           /* FTM_TRM_ACA_PROP_PENDING_ASDIV_CFG */
  "PEND_ASDIV_CAL",           /* FTM_TRM_ACA_PROP_PENDING_CAL_STATE */
};

COMPILE_ASSERT(sizeof(ftm_trm_aca_property_names)/sizeof(ftm_trm_aca_property_names[0]) == FTM_TRM_ACA_PROP_NUM);




