#===============================================================================
#
# SNS SAM SENSOR THRESH QMI APIs
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#                      EDIT HISTORY FOR FILE
#
#  $Header: //components/rel/qmimsgs.mpss/4.6/sns_sam_sensor_thresh/build/qmi_sns_sam_sensor_thresh.scons#1 $
#  $DateTime: 2016/02/01 11:12:56 $
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 01/27/16   dna     Baseline version
#===============================================================================
Import('env')
from glob import glob
from os.path import join, basename

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

# ------------------------------------------------------------------------------
# Include Paths
#---------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# APIs needed to build the SNS SAM SENSOR THRESH APIs
#-------------------------------------------------------------------------------

# Should be first
env.RequirePublicApi([
  'COMMON',
  'SNS_SAM_SENSOR_THRESH',
  ])

env.RequirePublicApi([
  'MPROC',
  ], area='CORE')

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
SNS_SAM_SENSOR_THRESH_C_SOURCES = ['${BUILDPATH}/' + basename(fname)
                 for fname in glob(join(env.subst(SRCPATH), '*.c'))]

# Add our library to the following build tags:
#   QMIMSGS_MPSS for MPSS images
#   QMIMSGS_ADSP for ADSP images
env.AddLibrary (['QMIMSGS_MPSS','QMIMSGS_ADSP'],
                 '${BUILDPATH}/qmimsgs_sns_sam_sensor_thresh', [SNS_SAM_SENSOR_THRESH_C_SOURCES])

# Load test units
env.LoadSoftwareUnits()
