#===============================================================================
#
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014-2015 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 09/02/15   AK      Dynamic Loading Changes
# 11/24/14   KKO     Created
#===============================================================================
import sys
import os
import re
from glob import glob
from sets import Set

Import('env')

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
# alias first alias is always the target then the other possibles
aliases = ['root_pd_img']

build_tags = ['CORE_QDSP6_SW', 'CORE_ROOT_PD', 'MODEM_MODEM', 'CORE_MODEM', 'MULTIMEDIA_QDSP6_SW', 'MULTIMEDIA_ROOT_PD', 'QDSS_EN_IMG', 'QMIMSGS_MPSS', 'CTA_APPS']

if os.path.exists(os.path.join(env.subst("${BUILD_ROOT}"), "build/manifest.xml")) or 'CRMSERVERNAME' in os.environ:
   build_tags.append('IMAGE_TREE_VERSION_AUTO_GENERATE')
   build_tags.append('IMAGE_TREE_UUID_AUTO_GENERATE')

build_tools = ['buildspec_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/swe_builder.py',
            '${BUILD_ROOT}/core/bsp/build/scripts/tms_builder_servreg']

# init environment variables
env.InitImageVars(
   alias_list=aliases,           # aliases
   proc='qdsp6',              # proc
   config='qdsp6_sw',                # config type, proc_name
   plat='qurt',
   target='MODEM_PROC_IMG_${BUILD_ID}',# target (elf, image file name)
   build_tags = build_tags,
   deprecated_build_tags = ['CBSP_QDSP6_SW_IMAGE'],
   tools = build_tools
   )

if not env.CheckAlias():
   Return()

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.LoadToolScript('build_utils', toolpath = ['${BUILD_ROOT}/build/scripts'])
env.InitTargetVariation()
env.InitBuildConfig()
env.Replace(MBN_FILE="${BUILD_MS_ROOT}/bin/${SHORT_BUILDPATH}/dsp2")
env.Replace(USES_DEVCFG = 'yes')
env.Replace(USES_MULTI_DEVCFG = 'yes')
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_IMG')
env.Append(USES_QDSS_SWE_MULTIPD = True)
if ('USES_FEATURE_DYNAMIC_LOADING_GLOBAL' in env and 'USES_INTERNAL_BUILD' in env) :
   env.Replace(USES_FEATURE_DYNAMIC_LOADING = 'yes')

if 'USES_ISLAND_BUILDER' in env:
   env.LoadToolScript("${BUILD_ROOT}/core/bsp/build/scripts/island_builder.py")
   env.LoadToolScript("${BUILD_ROOT}/core/kernel/qurt/scripts/island_analysis.py")
   env.LoadToolScript("${BUILD_ROOT}/core/bsp/build/scripts/lcs_autogen.py")

if env['MSM_ID'] in ['9x55','8998']:
  env.Replace(QURT_BUILD_CONFIG="modemv61")
elif env['MSM_ID'] in ['8994','9x45','8996','8909','9609']:
  env.Replace(QURT_BUILD_CONFIG="modemv56")
elif env['MSM_ID'] in ['8916','8936']:
  env.Replace(QURT_BUILD_CONFIG="modemv5")
else:
  env.PrintWarning("-------------------------------------------------------------------------------")
  env.PrintWarning("*** QuRT target could not be determined")
  env.PrintWarning("-------------------------------------------------------------------------------")
  Exit(1)

#---------------------------------------------------------------------------
# root PD virtual address
#---------------------------------------------------------------------------
env.Replace(HEXAGON_IMAGE_ENTRY = '0xC0000000')

#---------------------------------------------------------------------------
# Load in the tools scripts
#---------------------------------------------------------------------------
env.LoadToolScript('hexagon', toolpath = ['${BUILD_ROOT}/tools/build/scons/scripts'])
env.LoadToolScript('qdsp6_defs', toolpath = ['${BUILD_ROOT}/tools/build/scons/scripts'])
env.LoadToolScript('sleep_lpr_builder', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])

#------------------------------------------------------------------------------
# Find the location of the config XML for CoreBSP.
#---------------------- --------------------------------------------------------
env.Replace(CONFIG_XML = env.FindConfigFiles('cust_config.xml')[0])

env.Append(CFLAGS = ARGUMENTS.get('T_CFLAGS', '') + ' ')
env.Append(LFLAGS = ARGUMENTS.get('T_LFLAGS', '') + ' ')

hexagon_rtos_release = env.get('HEXAGON_RTOS_RELEASE')

# Tell the linker to look for it's config files in the $BUILDPATH directory
for config_path in env.FindConfigPaths():
   env.Append(LFLAGS = "-L " + config_path + ' ')

#---------------------------------------------------------------------------
# Add FW linker file paths
#---------------------------------------------------------------------------
   
fw_components = ['common'];
for comp in fw_components:
   install_api_path = env.get('BUILD_ROOT')+ '/fw/' + comp + '/api'
   env.AppendLinkerPath(install_api_path)
   install_api_path = env.get('BUILD_ROOT')+ '/fw_' + comp + '/api'
   env.AppendLinkerPath(install_api_path)

   
#---------------------------------------------------------------------------
# Proof of concept, need to find a correct location
#---------------------------------------------------------------------------

env.Append(CPPDEFINES=['MODEM_FW_NUM_PRIO=64'])
env.AddUsesFlags('USES_RCINIT_PLAYBOOK') # MUST OCCUR BEFORE FIRST LoadAreaSoftwareUnits
env.AddUsesFlags('USES_SERVICE_REGISTRY_PLAYBOOK')
  
#---------------------------------------------------------------------------
# Libs/Objs
#---------------------------------------------------------------------------
modem_proc_img_libs = []
modem_proc_img_objs = []

#---------------------------------------------------------------------------
# Libraries Section
#---------------------------------------------------------------------------

core_qurt_env = env.Clone()
#core_qurt_env.Replace(QURT_BUILD_CONFIG="modemv5")
core_qurt_env.Replace(CONFIG_XML = env.FindConfigFiles('cust_config.xml')[0])
core_qurt_items = core_qurt_env.LoadAreaSoftwareUnits('core', ["kernel/qurt"])
modem_proc_img_libs.extend(core_qurt_items['LIBS'])
modem_proc_img_objs.extend(core_qurt_items['OBJS'])

# ---------------------------------------------------------------------------
# Make SWE master list from individual image list
# ---------------------------------------------------------------------------

SWE_LISTS = [    #  SWE LISTS CONTAINING INTERMEDIATS EVENT INFO 
	  '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/sweevent_list.txt',
     '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/sweevent_list.txt',
   ]
env.SWEMergeBuilder(['${SHORT_BUILDPATH}/sweevent_consolidate_list.txt'],SWE_LISTS)

SWE_OUTPUT_LISTS = [    # SWE LISTS THAT WILL CONTAIN SWE INFO FROM MASTER FILE 
	  '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/sweevent_master_list.txt',
     '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/sweevent_master_list.txt',
   ]
env.SWEReplicateBuilder(SWE_OUTPUT_LISTS,['${SHORT_BUILDPATH}/sweevent_consolidate_list.txt'])

# ---------------------------------------------------------------------------
# Make Sleep LPR master list from individual image list
# ---------------------------------------------------------------------------

SleepLPR_LISTS = [    #  Sleep LPR LISTS CONTAINING INTERMEDIATS EVENT INFO 
     '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/sleep_LPR_list.txt',
     '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/sleep_LPR_list.txt',
   ]
env.SleepLPRMergeBuilder(['${SHORT_BUILDPATH}/SleepLPR_consolidate_list.txt'],SleepLPR_LISTS)

SleepLPR_OUTPUT_LISTS = [    # Sleep LPR LISTS THAT WILL CONTAIN SWE INFO FROM MASTER FILE 
     '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/sleep_LPR_master_list.txt',
     '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/sleep_LPR_master_list.txt',
   ]
env.SleepLPRReplicateBuilder(SleepLPR_OUTPUT_LISTS,['${SHORT_BUILDPATH}/SleepLPR_consolidate_list.txt'])

image_units= []
# ---------------------------------------------------------------------------
# Make RCINIT Playboook from playlists
# ---------------------------------------------------------------------------

if 'USES_RCINIT' in env:

   RCINIT_PLAYLISTS = [    # PLAYLISTS INTERMEDIATS BUILT WITH LIBRARY
	  '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/rcinit.rcpl',
     '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/rcinit.rcpl',

   ]
   if 'USES_GNSS_SA' not in env:
       RCINIT_PLAYLISTS += ['${BUILD_ROOT}/scripts/bsp/audio_root_libs/build/${SHORT_BUILDPATH}/rcinit.rcpl',]
   env.AddRcinitPlaybookParams(build_tags, image_units, modem_proc_img_objs, RCINIT_PLAYLISTS)
 
#------------------------------------------------------------------------------
# Init secure varables
#------------------------------------------------------------------------------
env.Replace(ENABLE_ENCRYPT = True)


#------------------------------------------------------------------------------
# Putting the image toghther
#------------------------------------------------------------------------------
image_units += [modem_proc_img_objs, modem_proc_img_libs]

env.Append(ORIG_PREPEND = 'orig_')

env.Append(LFLAGS = ' '.join([
  '-L',
  '/'.join([
    env.subst('${BUILD_ROOT}').replace('\\', '/'),
    'rfc_thor/rf_card/build/modem_root_libs',env.subst('${PROC}').replace('\\', '/'),
    env.subst('${SHORT_BUILDPATH}').replace('\\', '/'),
  ]),
  '',
]))

env.Append(LFLAGS = ' '.join([
  '-L',
  '/'.join([
    env.subst('${BUILD_ROOT}').replace('\\', '/'),
    'rfc_thor/build/modem_root_libs',env.subst('${PROC}').replace('\\', '/'),
    env.subst('${SHORT_BUILDPATH}').replace('\\', '/'),
  ]),
  '',
]))

#------------------------------------------------------------------------------
# Update linker flags, Support for Shared library versioning 
#------------------------------------------------------------------------------
if env.has_key('USES_FEATURE_DYNAMIC_LOADING'):
   # Update linker flags for dynamic section
   env.Append(LINKFLAGS = "--force-dynamic --dynamic-linker= ")
   
   #Load needed scripts
   env.LoadToolScript('sharedlib_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
   env.LoadToolScript('dlinker_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
   
   # Update link line flags for shared library to map version section to unique NOTE segement
   shlib_lcs_fpath = env.NormPath('${BUILD_ROOT}/core/bsp/build/scripts/shlib_add_version_segment.lcs', posix=True)
   env.Append(SHLINKFLAGS = '-T' + shlib_lcs_fpath )

img_version_tag = 'IMAGE_TREE_VERSION_AUTO_GENERATE'
if env.IsKeyEnable(img_version_tag):
   env.LoadToolScript('version_builder', toolpath = ['${BUILD_SCRIPTS_ROOT}'])
   qc_version_c = env.QC_VersionBuilder(img_version_tag, '${SHORT_BUILDPATH}/qc_version.c', "${BUILD_ROOT}/build/manifest.xml")
   qc_version_o = env.AddObject(img_version_tag, qc_version_c)
   image_units += qc_version_o
   modem_proc_img_objs.append(qc_version_o)
   oem_version_c = env.OEM_VersionBuilder(img_version_tag, '${SHORT_BUILDPATH}/oem_version.c')
   oem_version_o = env.AddObject(img_version_tag, oem_version_c)
   image_units += oem_version_o
   modem_proc_img_objs.append(oem_version_o)
   
img_uuid_tag = 'IMAGE_TREE_UUID_AUTO_GENERATE'
if env.IsKeyEnable(img_uuid_tag):
   env.LoadToolScript('version_builder', toolpath = ['${BUILD_SCRIPTS_ROOT}'])
   oem_uuid_c = env.OEM_UUID_Builder(img_uuid_tag, '${SHORT_BUILDPATH}/oem_uuid.c', [modem_proc_img_objs, modem_proc_img_libs],TARGET_IMG_PATH = env.RealPath("${SHORT_BUILDPATH}/${TARGET_NAME}"))
   oem_uuid_o = env.AddObject(img_uuid_tag, oem_uuid_c)
   image_units += oem_uuid_o
   modem_proc_img_objs.append(oem_uuid_o)
   
#--- SERVICE REGISTRY Playbook Extension, Image Specific Details -----------BEGIN

# ONLY WHEN tms_builder_servreg SCONS TOOL LOADED
if 'USES_SERVICE_REGISTRY' in env and 'USES_SERVICE_REGISTRY_PLAYBOOK' in env:

   # LIST OF ALL PLAYLISTS THAT WILL CONTRIBUTE TO MAKE THE OUTPUT JSON FILE
   SERVREG_PLAYLISTS = [ # IMAGE_OWNER ADDS SPECIFIC SERVREG_PLAYLISTS, ONE PER LIBRARY, EXPLICIT, NOT AUTO_MAGIC
      '${BUILD_ROOT}/scripts/bsp/root_pd_img/build/${SHORT_BUILDPATH}/servreg_playlist.pl',
      '${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/servreg_playlist.pl',
      '${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/servreg_playlist.pl',
   ]
   if 'USES_GNSS_SA' not in env:
       SERVREG_PLAYLISTS += ['${BUILD_ROOT}/scripts/bsp/audio_root_libs/build/${SHORT_BUILDPATH}/servreg_playlist.pl',]

   servreg_out_json_install = env.AddServregPlaybookParams(build_tags, image_units, modem_proc_img_objs, SERVREG_PLAYLISTS, True)

#--- SERVICE REGISTRY Playbook Extension, Image Specific Details ----------- END

env.Append(LINK_DIRS = " -L${BUILD_ROOT}/core/kernel/qurt/build/root_pd_img/${PROC}/${SHORT_BUILDPATH}/install/${QURT_BUILD_CONFIG}/lib")

if 'IMAGE_BUILD_LOCAL_FILES' in env:
   #-------------------------------------------------------------------------
   # Local Files
   #-------------------------------------------------------------------------
   if (('USES_LCS_FILE' in env) and ('USES_LCS_AUTOGEN' not in env)):
      # Dynamically load lcs_builder
      env.LoadToolScript("${BUILD_SCRIPTS_ROOT}/lcs_builder.py")
      # Location and name of lcs file
      qurt_lcs = env.FindConfigFiles('${BUILD_ASIC}_QuRT_${TARGET_IMAGE}.lcs')[0]
      lcs_file = env.LcsBuilder("${BUILD_MS_ROOT}/${ORIG_PREPEND}${TARGET_NAME}.elf.lcs", qurt_lcs)

      image_units += lcs_file
      
if 'IMAGE_BUILD_LINK' in env:
   #-------------------------------------------------------------------------
   # List of list-files containing libraries generated else where.
   #-------------------------------------------------------------------------
   #Dynamic linker symbol list files from all areas
   core_root_dynsym_lst = env.RealPath('${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/CORE_ROOT_LIBS_SHLIBS_DYNSYMS_${BUILD_ID}.txt')
   modem_root_dynsym_lst = env.RealPath('${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/MODEM_ROOT_LIBS_SHLIBS_DYNSYMS_${BUILD_ID}.txt')
         
   #List of Versioned Dynamic libraries within build from all areas, ToDo: Consolidate all the list files from all areas
   core_root_versioned_shlibs_lf = env.RealPath('${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/CORE_ROOT_LIBS_VERSIONED_SHLIBS_${BUILD_ID}.txt', posix=True)   
   modem_root_versioned_shlibs_lf = env.RealPath('${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/MODEM_ROOT_LIBS_VERSIONED_SHLIBS_${BUILD_ID}.txt', posix=True)   

   core_root_lf = env.RealPath('${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/CORE_ROOT_LIBS_${BUILD_ID}.txt', posix=True)   
   modem_root_lf = env.RealPath('${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/MODEM_ROOT_LIBS_${BUILD_ID}.txt', posix=True)   
   if 'USES_GNSS_SA' not in env:
      audio_root_lf = env.RealPath('${BUILD_ROOT}/scripts/bsp/audio_root_libs/build/${SHORT_BUILDPATH}/AUDIO_ROOT_LIBS_${BUILD_ID}.txt', posix=True) 
   #-------------------------------------------------------------------------
   # Link image
   #-------------------------------------------------------------------------
   image_units = []
   
   #-------------------------------------------------------------------------
   # Dynamic Loading Support
   #-------------------------------------------------------------------------
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING'):
      sym_list = [core_root_dynsym_lst, modem_root_dynsym_lst]
      version_sym_list = [core_root_versioned_shlibs_lf, modem_root_versioned_shlibs_lf]

      #Generate versioning init list files
      env.LoadToolScript('${BUILD_ROOT}/core/bsp/build/scripts/dlfs_version_builder.py')
      version_info_init_list_c  = env.AddSharedLibraryVersionListFromLf("${SHORT_BUILDPATH}/shlibs_version_info_init_list.c", version_sym_list)
      version_info_init_list_o  = env.AddObject(build_tags, version_info_init_list_c)
      modem_proc_img_objs.append(version_info_init_list_o)
      
      #Generate dynamic symbols list files for exporting symbols for shared library
      dynsym_export_lst = '${SHORT_BUILDPATH}/dynsymbols.lst'
      dynsym_extern_lst = '${SHORT_BUILDPATH}/externs.lst'
      sym_lst_all = env.DLExposeLSTBuilder([dynsym_export_lst, dynsym_extern_lst], sym_list)
      
      #Add dynamic symbols list files as dependency on final image
      image_units += sym_lst_all  
      
   if 'USES_GNSS_SA' not in env:
       listfiles = [ core_root_lf, modem_root_lf, audio_root_lf]
   else:
       listfiles = [ core_root_lf, modem_root_lf]
       
   specfiles = []
   if 'USES_ISLAND' in env:
      # Provide locations of linker template, island specification, and kernel script.
      core_root_libs_island_txt=env.RealPath('${BUILD_ROOT}/scripts/bsp/core_root_libs/build/${SHORT_BUILDPATH}/CORE_ROOT_LIBS_${BUILD_ID}_island.txt')
      modem_root_libs_island_txt=env.RealPath('${BUILD_ROOT}/scripts/bsp/modem_root_libs/build/${SHORT_BUILDPATH}/MODEM_ROOT_LIBS_${BUILD_ID}_island.txt')
      island_analysis_filename=env.RealPath('${BUILD_ROOT}/scripts/bsp/root_pd_img/build/${SHORT_BUILDPATH}/island_analysis.txt')
      qurt_island_list = env.GenerateIslandList()
      chipset, flavor, purpose = os.environ.get('VARIANT_NAME', '').split('.')
      libc_island = env.RealPath('${BUILD_ROOT}/config/'+chipset+'/hex.ispec')
      island_analysis_file = env.IslandAnalysis(island_analysis_filename,[core_root_libs_island_txt, modem_root_libs_island_txt, libc_island, qurt_island_list],['${BUILD_ROOT}','${QDSP6_RELEASE_LIB_DIR}'],[]) 
      specfiles = [core_root_libs_island_txt, modem_root_libs_island_txt, libc_island, qurt_island_list]  
      image_units += qurt_island_list
      
   # This will be run if we are autogenerating the LCS file.
   if 'USES_LCS_AUTOGEN' in env:
      # Temporary requirement to replace '.start' with '.qurtstart'
      # Get the LINKFLAGS.
      linkflags = env.get('LINKFLAGS')
      # Search to see if '.start' is being used.
      if re.match(r'.* .start*',linkflags):
         # Modify the linkflags to use '.qurtstart'.
         modified_linkflags = re.sub(r'\.start','.qurtstart', linkflags)
         env.Replace(LINKFLAGS = modified_linkflags)
      else:
         env.PrintError("Cannot find .start in LINKFLAGS.  This is expected.")
         env.PrintError("In Island linking, we need to replace .start with .qurtstart")
         exit(1)
      linker_template = env.FindConfigFiles('${BUILD_ASIC}_QuRT_${TARGET_IMAGE}.lcs')[0]
      generated_linker = "${BUILD_MS_ROOT}/${ORIG_PREPEND}${TARGET_NAME}.elf.lcs"
      gen_linker_path = env.RealPath("${BUILD_MS_ROOT}").replace('\\', '/')
      env.AppendLinkerPath(gen_linker_path)
      lcs_autogen = env.GenerateLCSFileFromTemplate(generated_linker, linker_template, specfiles)
      if 'USES_ISLAND' in env:
          env.Depends(lcs_autogen, island_analysis_file)    
      image_units += [lcs_autogen]
   
   if env.has_key('USES_FEATURE_DYNAMIC_LOADING'):
      #import pdb; pdb.set_trace()
      image_elf = env.AddProgram(
         "${BUILD_MS_ROOT}/${ORIG_PREPEND}${TARGET_NAME}",         # target name
         modem_proc_img_objs,                         # sources (objects files must have at least 1)
         LIBS=modem_proc_img_libs,                    # libraries
         LISTFILES=listfiles,                         # other libraries generated externally
         SHARED_LIBS_DYNSYM=sym_lst_all[0],           # shared lib dynamic symbols
         SHARED_LIBS_EXTSYM=sym_lst_all[1]            # shared lib symbols to extern
      )
   else:
      image_elf = env.AddProgram(
         "${BUILD_MS_ROOT}/${ORIG_PREPEND}${TARGET_NAME}",         # target name
         modem_proc_img_objs,                         # sources (objects files must have at least 1)
         LIBS=modem_proc_img_libs,                    # libraries
         LISTFILES=listfiles                          # other libraries generated externally
      )
   
   if 'USES_LCS_FILE' in env:
      if 'USES_LCS_AUTOGEN' in env:
         env.Depends(image_elf, lcs_autogen)
      else:
         env.Depends(image_elf, lcs_file)
         
   if 'USES_SERVICE_REGISTRY' in env and 'USES_SERVICE_REGISTRY_PLAYBOOK' in env:
      env.Depends(image_elf, servreg_out_json_install)


   #---------------------------------------------------------------------------
   # Use the MYPS_LINKER, if specified, for CR 340311
   #---------------------------------------------------------------------------
   if 'USES_MYPS_LINKER' in env:  # {{{
      myps_paths = [
        'perf/scripts',
      ]

      myps_paths = [
        os.path.abspath(
          os.path.join(*(
           [env.subst('${BUILD_ROOT}')]
           + i.split('/'))
          )
        )
        for i in myps_paths
      ]

      for potential_myps_dir in myps_paths:
        if os.path.exists(potential_myps_dir):
          myps_dir = potential_myps_dir
          break

      # memory report generation? y/n?
      env.Append(MEMREPORT = ARGUMENTS.get('MEMREPORT', '0'))

      env.LoadToolScript(os.path.join(myps_dir, 'hook_pplk.py'))
      image_elf, qshrink_qdb = env.Pplk(
        [
          '${SHORT_BUILDPATH}/${TARGET_NAME}.elf',
          '${SHORT_BUILDPATH}/qdsp6m.qdb'
        ],
        [image_elf]
      )
   # }}}


if 'IMAGE_BUILD_POST_LINK' in env:

   #-------------------------------------------------------------------------
   # Install ELF, reloc files
   #-------------------------------------------------------------------------
   # copy elf and reloc to needed locations for AMSS tools to load on target
   install_target_elf = env.InstallAs("${BUILD_MS_ROOT}/M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_NODEVCFG.elf", image_elf)

   install_target_qdb = env.InstallAs(
      "${BUILD_MS_ROOT}/bin/${SHORT_BUILDPATH}/qdsp6m.qdb", qshrink_qdb)

   if 'USES_ELF_TO_BLAST_EXTRACTOR' in env:
      #-------------------------------------------------------------------------
      # Run Elf To Blast Extractor (temp hack until code can move to modem_proc.scons
      #-------------------------------------------------------------------------
      env.LoadToolScript("${BUILD_ROOT}/tools/elf_to_blast_extractor/elf_to_blast_extractor_builder.py")

      address_map_files = env.ElfToBlastExtractorBuilder('${SHORT_BUILDPATH}/symbol_to_addressmap.txt', install_target_elf)
      image_units += address_map_files

   if 'USES_CMMBUILDER' in env:
       cmmscripts = env.CMMGenerateFiles()
       image_units += cmmscripts
       
   image_units += [
      install_target_elf,
      install_target_qdb,
      # target_mbn,
   ]

#=========================================================================
# Finish up...
env.BindAliasesToTargets(image_units)

# This probably needs to go someplace else.
env.CMMBuilder(None, None)
