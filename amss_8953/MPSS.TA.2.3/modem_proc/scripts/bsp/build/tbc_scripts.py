#==============================================================================
# SCONS Tool for Scripts Component Utilities.
#
# Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
# $Header: //components/rel/scripts.mpss/1.0/bsp/build/tbc_scripts.py#5 $
#
#==============================================================================

'''
Contains utility scripts for the scripts component.

'''
import os
def generate(env):
    env.AddMethod(generate_dynamic_loading_libs,"GenerateDynamicLoadingFiles")
    env.AddMethod(add_servreg_playlist_params,"AddServregPlaylistParams")
    env.AddMethod(add_servreg_playbook_params,"AddServregPlaybookParams")


def exists(env):
    return env.Detect('tbc_scripts_utils')


def generate_dynamic_loading_libs(env, image_units, img_shlibs, img_versioned_shlibs, image_name):

   #List of all versioned shared libs being generated under an area
   image_versioned_shared_libs_lst_name = "${SHORT_BUILDPATH}/" + image_name +"_VERSIONED_SHLIBS_${BUILD_ID}.txt"
   #List of all shared libs being generated under an area
   image_shared_libs_lst_name =  "${SHORT_BUILDPATH}/" + image_name + "_SHLIBS_${BUILD_ID}.txt"
   #List of all symbols imported under an area
   sh_libs_sym_lst_name = "${SHORT_BUILDPATH}/"+ image_name+ "_SHLIBS_DYNSYMS_${BUILD_ID}.txt"
   
   #load needed tool scripts
   env.LoadToolScript('${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',)
   env.LoadToolScript('${BUILD_ROOT}/core/bsp/build/scripts/dlfs_version_builder.py')
   env.LoadToolScript('sharedlib_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
         
   #generate consolidated list of all undef symbols for DSOs building under modem_root_libs   
   dll_items= []
   sh_libs_sym_lst= env.RealPath(sh_libs_sym_lst_name)
   sh_libs_sym_lst_node = env.SharedLibGenerateList([sh_libs_sym_lst], img_shlibs)
   #Add to dll items
   dll_items += sh_libs_sym_lst_node
   dll_items += img_shlibs
   
   #Create lib list files for shared libraries, sign all shared libs under modem_root_libs
   #List of versioned DSOs under core_root_libs
   image_versioned_shared_libs = env.ListFileBuilder(image_versioned_shared_libs_lst_name, [img_versioned_shlibs], relative_path=None, posix=True)
   #List of all DSOs under core_root_libs
   image_shared_libs = env.ListFileBuilder(image_shared_libs_lst_name, [img_shlibs], relative_path=None, posix=True)
   #Add to dll items
   dll_items += [image_versioned_shared_libs, image_shared_libs]
   
   #Sign all the shared libraries
   for shlib in img_shlibs :
      env.PrintInfo("Generating mbn for dynamic shared library: " + str(shlib))
   
      env.Replace(MBN_FILE=''.join(["${BUILD_MS_ROOT}/bin/${SHORT_BUILDPATH}/", shlib.name]))

      install_shlib_mbn = env.InstallAs(''.join(["${SHORT_BUILDPATH}/", shlib.name, '.debug']), shlib)
      target_shlib_mbn = env.MbnBuilder(''.join(["${SHORT_BUILDPATH}/", shlib.name, '.mbn']), 
                                     install_shlib_mbn, 
                                     IMAGE_TYPE='qdsp6sw', MBN_TYPE="elf", 
                                     FLASH_TYPE=env['TARGET_FLASH_TYPE'])
   
      #Add to dll items
      dll_items += [install_shlib_mbn, target_shlib_mbn]
      
   return dll_items
#End of dynamic_loading_work

#--- SERVICE REGISTRY Playbook Extension, Image Specific Details -----------BEGIN
def add_servreg_playbook_params(env, build_tags, image_units, image_objs, servreg_playlists, root_pd):
   
   # FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
   if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
       if os.makedirs(env.RealPath('${SHORT_BUILDPATH}')):
           raise

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_servreg_env = env.Clone()
   
   if not root_pd :
     subdomain = 'audio_pd'
   else :
     subdomain = 'root_pd'
     
   if env.get('CHIPSET').startswith('mdm'):
     soc = 'mdm'
   else :
     soc = 'msm'

   # EVERY IMAGE OWNER HAS TO DEFINE THE DOMAIN SCOPES USING AddServRegDomain() API
   playbook_servreg_env.AddServRegDomain(
      build_tags,
      {
         'soc'                  : soc,
         'domain'               : 'modem',
         'subdomain'            : subdomain,
      })

   # PLAYLIST TO STORE THE SERVICE REGISTRY - DOMAIN INFORMATION
   servreg_out_pl = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/servreg_playlist.pl')
   playbook_servreg_env.AddServRegPlaylist(build_tags, servreg_out_pl)
   playbook_servreg_env.Depends(build_tags, servreg_out_pl)
   image_units.append(servreg_out_pl)

   # CREATION OF OUTPUT JSON FILE
   servreg_out_json = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/${TARGET_NAME}_servreg.json')
   playbook_servreg_env.AddServRegPlaybook(build_tags, servreg_out_json, servreg_playlists)
   playbook_servreg_env.Depends(build_tags, servreg_out_json)
   image_units.append(servreg_out_json)

   # CREATEION OF OUTPUT C FILE i.e THE SERVICE REGISTRY LOCAL DATABASE
   servreg_out_c = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/servreg_local_db_augtogen.c')
   playbook_servreg_env.AddServRegPlaybook(build_tags, servreg_out_c, servreg_playlists)
   servreg_obj = playbook_servreg_env.AddObject(build_tags, servreg_out_c)
   playbook_servreg_env.Depends(build_tags, servreg_out_c)
   playbook_servreg_env.Depends(servreg_out_c, servreg_out_json)
   image_objs.append(servreg_obj)

   # INSTALL JSON FILE IN LOCAL DIRECTORY
   if not root_pd:
      servreg_out_json_install = playbook_servreg_env.InstallAs(env.RealPath("${BUILD_MS_ROOT}/servreg/${BUILD_ID}/modemua.jsn"), servreg_out_json)
   else :
      servreg_out_json_install = playbook_servreg_env.InstallAs(env.RealPath("${BUILD_MS_ROOT}/servreg/${BUILD_ID}/modemr.jsn"), servreg_out_json)
   
   return servreg_out_json_install

#--- SERVICE REGISTRY Playbook Extension, Image Specific Details ----------- END

def add_servreg_playlist_params(env, build_tags, image_units):
    
   # FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
   if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
       if os.makedirs(env.RealPath('${SHORT_BUILDPATH}')):
           raise
            
   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_servreg_env = env.Clone()

   # PLAYLIST OUTPUT THIS LIBRARY
   servreg_out_pl = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/servreg_playlist.pl')
   playbook_servreg_env.AddServRegPlaylist(build_tags, servreg_out_pl)
   playbook_servreg_env.Depends(build_tags, servreg_out_pl)
   image_units.append(servreg_out_pl)