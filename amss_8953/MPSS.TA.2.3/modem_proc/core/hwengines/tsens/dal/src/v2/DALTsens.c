/*============================================================================
  FILE:         TsensDevice.c

  OVERVIEW:     Implementation of the TSENS device library

  DEPENDENCIES: None

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.9.1/hwengines/tsens/dal/src/v2/DALTsens.c#1 $$DateTime: 2015/09/29 21:52:15 $$Author: pwbldsvc $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-02-04  jjo  Add calibration API.
  2014-12-10  jjo  Modify for hardware temperature conversion.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DDIPlatformInfo.h"
#include "DALTsens.h"
#include "HALtsens.h"
#include "TsensBsp.h"
#include "msg.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef enum
{
   TSENS_DEVICE_STATE_INIT = 0,
   TSENS_DEVICE_STATE_ERROR,
   TSENS_DEVICE_STATE_READY
} TsensDeviceStateType;

typedef struct
{
   uint32 uNumSensors;
   uint32 uSensorsEnabledMask;
   TsensDeviceStateType eDeviceState;
   TsensBspType *pBsp;
   DALSYSSyncHandle hSync;
   DALSYS_SYNC_OBJECT(syncObject);
   DalDeviceHandle *phHWIO;
   DALBOOL bDataReady;
} TsensDeviceDataType;


/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
extern uint8 *gpuMpm2MpmBase;
extern uint8 *gpuSecCtrlBase;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static TsensDeviceDataType tsensDeviceData;
static TsensDeviceDataType *pDeviceData = NULL;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
static void TsensLock(void)
{
   (void)DALSYS_SyncEnter(pDeviceData->hSync);
}

static void TsensUnLock(void)
{
   (void)DALSYS_SyncLeave(pDeviceData->hSync);
}

static DALResult Tsens_MapHWIORegion(const char *szBase,
                                     uint8 **ppuVirtAddr)
{
   DALResult status;

   status = DalHWIO_MapRegion(pDeviceData->phHWIO,
                              szBase,
                              ppuVirtAddr);

   return status;
}


static DALResult Tsens_GetTempInternal(uint32 uSensor, int32 *pnDeciDegC)
{
   DALResult status = DAL_SUCCESS;
   int32 nDeciDegC = 0;
   int32 nDeciDegCTry1;
   int32 nDeciDegCTry2;
   int32 nDeciDegCTry3;
   boolean bValid;

   if ((1 << uSensor) & pDeviceData->uSensorsEnabledMask)
   {
   bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nDeciDegCTry1);
   if (bValid == TRUE)
   {
      nDeciDegC = nDeciDegCTry1;
   }
   else
   {
      bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nDeciDegCTry2);
      if (bValid == TRUE)
      {
         nDeciDegC = nDeciDegCTry2;
      }
      else
      {
         bValid = HAL_tsens_GetSensorPrevTemp(uSensor, &nDeciDegCTry3);
         if (bValid == TRUE)
         {
            nDeciDegC = nDeciDegCTry3;
         }
         else if (nDeciDegCTry1 == nDeciDegCTry2)
         {
            nDeciDegC = nDeciDegCTry1;
         }
         else if (nDeciDegCTry2 == nDeciDegCTry3)
         {
            nDeciDegC = nDeciDegCTry2;
         }
         else
         {
            nDeciDegC = nDeciDegCTry1;
         }
      }
   }

   *pnDeciDegC = (int32)nDeciDegC;
      }
   else
   {
      status = TSENS_ERROR_UNSUPPORTED;
   }


   return status;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
 * Functions specific to the DalTsens interface
 * ----------------------------------------------------------------------*/
DALResult Tsens_SetThreshold(TsensClientCtxt *pCtxt, uint32 uSensor, TsensThresholdType eThreshold, int32 nDeciDegC, DALSYSEventHandle hEvent)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult Tsens_GetTemp(TsensClientCtxt *pCtxt, uint32 uSensor, TsensTempType *pTemp)
{
   int32 nDeciDegC;
   DALResult status;
   TsensBspType *pBsp;

   pBsp = pDeviceData->pBsp;

   if (uSensor >= pBsp->uNumSensors)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   if (pTemp == NULL)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   status = Tsens_GetTempInternal(uSensor, &nDeciDegC);
   if (status !=  DAL_SUCCESS)
   {
      return status;
   }

   pTemp->nDegC = nDeciDegC / 10;

   MSG_2(MSG_SSID_ADC,
         MSG_LEGACY_HIGH,
         "TSENS: Sensor = %u, DeciDegC = %i",
         uSensor,
         nDeciDegC);

   return DAL_SUCCESS;
}

DALResult Tsens_GetTempRange(TsensClientCtxt *pCtxt, uint32 uSensor, TsensTempRangeType *pTempRange)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult Tsens_DeviceInit(TsensClientCtxt *pCtxt)
{
   DALResult status = DAL_SUCCESS;
   DALSYSPropertyVar propertyVar;
   TsensDevCtxt *pDevCtxt = pCtxt->pTsensDevCtxt;
   TsensBspType *pBsp;

   pDeviceData = &tsensDeviceData;
   pDeviceData->eDeviceState = TSENS_DEVICE_STATE_INIT;
   pDeviceData->bDataReady = FALSE;

   /* Get the BSP */
   status = DALSYS_GetPropertyValue(pDevCtxt->hProp, 
                                    "tsens_bsp", 
                                    0, 
                                    &propertyVar);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Device failed to get tsens_bsp property");

      goto error;
   }

   pDeviceData->pBsp = (TsensBspType *)propertyVar.Val.pStruct;
   
   pBsp = pDeviceData->pBsp;
   pDeviceData->uNumSensors = pBsp->uNumSensors;
   pDeviceData->uSensorsEnabledMask = pDeviceData->pBsp->uSensorEnableMask;

   /* Initialize synchronization object */
   status = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                              &pDeviceData->hSync,
                              &pDeviceData->syncObject);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent((DALDEVICEID)DALDEVICEID_TSENS, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Adc could not create device synchronization object\n");
      goto error;
   }
   
   /* Attach to the HWIO DAL and map the memory regions */
   status = DAL_DeviceAttach(DALDEVICEID_HWIO, &pDeviceData->phHWIO);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent((DALDEVICEID)DALDEVICEID_TSENS, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Failed to attach to HWIO with status=0x%08x\n", (unsigned int)status);
      goto error;
   }
   
   status = Tsens_MapHWIORegion("SECURITY_CONTROL", &gpuSecCtrlBase);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent((DALDEVICEID)DALDEVICEID_TSENS, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Failed to map memory for TSENS QFPROM registers with status=0x%08x\n", (unsigned int)status);
      goto error;
   }
   
   status = Tsens_MapHWIORegion("MPM2_MPM", &gpuMpm2MpmBase);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent((DALDEVICEID)DALDEVICEID_TSENS, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_Init : Failed to map memory for TSENS control registers with status=0x%08x\n", (unsigned int)status);
      goto error;
   }

   pDeviceData->eDeviceState = TSENS_DEVICE_STATE_READY;

   return DAL_SUCCESS;

error:
   pDeviceData->eDeviceState = TSENS_DEVICE_STATE_ERROR;
   return status;
}


DALResult Tsens_DeviceDeInit(TsensClientCtxt *pCtxt)
{
   TsensLock();

   pDeviceData->bDataReady = FALSE;
   pDeviceData->eDeviceState = TSENS_DEVICE_STATE_INIT;

   TsensUnLock();

   return DAL_SUCCESS;
}

DALResult Tsens_GetNumSensors(TsensClientCtxt *pCtxt, uint32 *puNumSensors)
{
   if (puNumSensors == NULL)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   *puNumSensors = pDeviceData->uNumSensors;

   return DAL_SUCCESS;

}

DALResult Tsens_SetEnableThresholds(TsensClientCtxt *pCtxt, DALBOOL bEnableThresholds)
{
   return DAL_ERROR;
}

DALResult
Tsens_SetCriticalThreshold( DalDeviceHandle * h, uint32 nSensor, TsensCriticalThresholdType eThreshold, int32 nDegC)
{
   return DAL_ERROR;
}

DALResult Tsens_CheckCalibration(TsensClientCtxt *pCtxt, uint32 uSensor)
{
   return DAL_SUCCESS;
}

/*-------------------------------------------------------------------------
 * Functions specific to the DAL interface
 * ----------------------------------------------------------------------*/
DALResult
Tsens_DriverInit(TsensDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_DriverDeInit(TsensDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_RegisterClient(TsensClientCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_DisableClient(uint32 uClient)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_DeregisterClient(TsensClientCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

