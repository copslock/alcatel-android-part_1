#===============================================================================
#
# ERR Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.9.1/debugtools/err/build/err_user.scons#1 $
#  $DateTime: 2015/09/29 21:52:15 $
#  $Author: pwbldsvc $
#  $Change: 9128810 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/debugtools/err/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Features and Definitions
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES=['ERR_CFG_QURT'])
env.Append(CPPDEFINES=['ERR_HW_QDSP6'])

env.PublishPrivateApi("ERR", [
   "${INC_ROOT}/core/debugtools/err/src/q6",
   "${INC_ROOT}/core/debugtools/err/src/qurt"
])

if 'USES_DEVCFG' in env:
    env.Append(CPPDEFINES=['ERR_USES_DEVCFG'])

#-------------------------------------------------------------------------------
# External depends outside CoreBSP
#-------------------------------------------------------------------------------

env.RequireExternalApi([
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'DAL',
   'DEBUGTOOLS',
   'HAL',
   'MPROC',  # MPROC dependency since we are using smem from User PD as well.   
   'POWER',
   'SERVICES',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Sources
#-------------------------------------------------------------------------------

ERR_USERPD_SOURCES = [
   '${BUILDPATH}/err_pd.c',
   '${BUILDPATH}/err_pd_exception_handler.c',
   '${BUILDPATH}/err_pd_exception_task.c',
   '${BUILDPATH}/err_qdi_client.c',
   ]

#Source meant for Uerr, Should be explicitly added for island mode
UERR_USERPD_FULL_SOURCES = [
   '${BUILDPATH}/q6/err_jettison_core.s',
   ]

if 'USES_ISLAND' in env:
   env.Append(CFLAGS = "-DUERR_ISLAND_MODE ")

if 'USES_ISLAND' not in env:
   ERR_USERPD_SOURCES.append('${BUILDPATH}/q6/err_jettison_core.s')

island_section = ['.text.uErr','.data.uErr']
   
VERSION_IMG = [
   'IMAGE_TREE_VERSION_AUTO_GENERATE',
]

if env.IsTargetEnable(VERSION_IMG):
   ERR_USERPD_SOURCES.append('${BUILDPATH}/err_image_version.c')
else:
   ERR_USERPD_SOURCES.append('${BUILDPATH}/err_image_version_void.c')

#-------------------------------------------------------------------------------
# Add SMEM Logging sources only for ADSP
#-------------------------------------------------------------------------------
if env.IsTargetEnable(['CORE_QDSP6_AUDIO_SW','CORE_QDSP6_SENSOR_SW']):
  ERR_USERPD_SOURCES.append('${BUILDPATH}/err_smem_log.c')
else:
  ERR_USERPD_SOURCES.append('${BUILDPATH}/err_smem_log_stub.c')

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Add USER PD sources
#-------------------------------------------------------------------------------
#uImage is not enabled for LPASS 
ERR_IMGS = [
     'CORE_QDSP6_SENSOR_SW', 'CORE_SLPI_USER', 'CORE_SSC_SLPI_USER', 
     'CORE_QDSP6_AUDIO_SW', 'CORE_USER_PD'
]

err_full_libs = env.AddLibrary(ERR_IMGS,
  '${BUILDPATH}/err_user', ERR_USERPD_SOURCES )

if 'USES_ISLAND' in env:
  env.AddIslandLibrary(ERR_IMGS, err_full_libs, island_section )
  uerr_full_libs = env.AddLibrary(ERR_IMGS,'${BUILDPATH}/uerr_user', UERR_USERPD_FULL_SOURCES)
  env.AddIslandLibrary(ERR_IMGS, uerr_full_libs )


# Register initializations with rcinit
RCINIT_INIT_INIT = {
   'sequence_group'             : 'RCINIT_GROUP_1',                  # required
   'init_name'                  : 'err_init',                        # required
   'init_function'              : 'err_init',                        # required
}

RCINIT_INIT_INIT_QDI = {
   'sequence_group'             : 'RCINIT_GROUP_1',                  # required
   'init_name'                  : 'errqdiclntinit',                  # required
   'init_function'              : 'err_qdi_client_init',             # required
   'dependencies'               : ['err_init',]
}

if 'USES_RCINIT' in env:
  env.AddRCInitFunc( ERR_IMGS, RCINIT_INIT_INIT )
  env.AddRCInitFunc( ERR_IMGS, RCINIT_INIT_INIT_QDI )
