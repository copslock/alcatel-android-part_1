/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         E R R O R     R E P O R T I N G    S E R V I C E S

GENERAL DESCRIPTION
  This module provides error reporting services for both fatal and
  non-fatal errors.  This module is not a task, but rather a set of
  callable procedures which run in the context of the calling task.

Copyright (c) 1992-2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        Edit History

$Header: //components/rel/core.mpss/3.9.1/debugtools/err/src/err.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/15   abh     Added changes for extended smem logging
12/18/13   rks     CR589332:Adding an un-cached variable to figure out faulty 
                   call back functionin error handling
09/30/13   rks     CR526960: replace memcpy with memscpy
12/10/08   tbg     Removed INTLOCK usage for QDSP6
09/15/08   tbg     Integration changes for MDM8200
07/15/08   tbg     Code clean up to remove older features
04/04/08   tbg     Added support for native rex environment
11/30/07   tbg     Updated automation settings for FEATURE_ERR_DEBUG_HANDLER.
10/08/07   tbg     Some code cleanup + added 6k support.
02/07/07   as      Fixed lint critical.
02/01/07   bfc     Fixed a reference to an obsolete SMEM API
09/26/07   hwu     Use new APIs to reduce multiprocessor specific code.
01/08/07   tbg     Added support for new err_auto_action API.  Also updated
                   ERR_DEBUG_HANDLER to support THIN_UI builds.
12/15/06   as      Fixed critical lint errors.
11/02/06   tbg     Fixed additional issues with MDP driver in new error handler
10/19/06   tbg     Updated ERR_EXTENDED_STORE to work on 7k MSMs.  Also added
                   new feature FEATURE_ERR_DEBUG_HANDLER.
09/26/06   ptm     Use new APIs to reduce multiprocessor specific code.
01/19/06   pc      Removed data in all records in err_cache on err_clr.
12/12/05   tbg     Switch flag for mem_info from boolean to 32bit.
                   Changed sprintf calls to snprintf.
11/11/05   tbg     Changed ERR_EXTENDED_STORE to share NV item for max files
                   with debug trace (if feature is defined)
10/31/05   as      Fixed lint errors.
08/30/05   ih      Added return of 1 to err_reset_check when a reset is
                   detected.
08/11/05   as      Fixed complier warnings.
06/06/05   as      Added checks to do brew heap dump: if err_reset_flag is
                   true and if BREW heap variables are initialized.
04/17/04   ash     Added (FEATURE_MEM_USAGE_AT_EXCEPTION)
                   for application memory usage during crash
02/16/05   as      Added a check in err_log_store to write to NV only if
                   tasks are not locked.
02/15/05   as      Added FEATURE_DIAG_DEBUG_6275. This temporary feature will
                   be used until Raven code is stabilized.
11/30/04   as      Added FEATURE_AUTO_RESET. This feature resets the phone
                   automatically without user intervention if the phone
                   encounters a critical failure
10/07/04   tbg     Added FEATURE_ERR_SAVE_CORE.
08/03/04   tbg     Extended FEATURE_ERR_EXTENDED_STORE to support multiple
                   files on EFS, as well as multiple logs per session.
05/14/04   eav     Added FEATURE_SAVE_DEBUG_TRACE.
04/27/04   eav     Added FEATURE_SAVE_DEBUG_TRACE.  Moved ERR_DATA_MAGIC_NUMBER
                   and ERR_DATA_RESET_MAGIC_NUMBER to err.h, so they are
                   accessible in msg.c.
04/06/04   tbg     Added FEATURE_ERR_EXTENDED_STORE
10/28/03   as      Changes to fix errors when compiling with RVCT2.0
10/08/03   gr      Redefined ERR_FATAL to make one function call instead of
                   three to save code space.
07/24/03   as      Added ERR_FATAL_REPORT(   ) macro
03/04/03   djm     WPLT only modification to display LED uptime counter when
                   ERR_FATAL is encountered.
08/20/02   lad     PLATFORM_LTK and F_DIAG_COUPLED_ARCHITECTURE support.
06/10/02   cs      Updated err_init to read all the NV log records.
02/12/02    gr     Modified err_fatal_put_log to support five digit line
                   numbers.
06/27/01   lad     Cosmetic changes.
04/06/01   lad     Moved local definition of nv_err_log_type.
                   Changed use of NV_MAX_ERR_LOG to ERR_MAX_LOG.
02/23/01   lad     Featurized coupled code and did some cleanup.
08/28/00   lad     Added T_O to condition for ERR_HAS_LCD_MESSAGE.
05/12/99   lad     Fixed err_log_store() to give fatal errors prescedence
                   over non-fatal errors.
11/15/98   jct     Removed pragmas at top of file, moved _disable prototype
                   inside a check to see if the method is already defined
11/05/98   jct     Merged in changes for ARM
09/02/98   mk      Added hw_power_off() to err_fatal_put_log() for MSM3000.
05/15/98   bns     Added support for SSS2 phones.
08/08/97   dak     Fixed ERR_FATAL handling for WLL targets. Jump to download
                   if phone is on hook. Define ERR_FORCE_DOWNLOAD to jump
                   to downloader on very early ERR_FATALs.
07/23/97   jah     Turn off ringer on MSM 2 phones in err_fatal_put_log().
07/02/97   jjn     Change on 05/23/97 was for Module hardware with 20pin
                   SAMTEC connector.  SYS_WAKE is on a different GPIO pin
                   on 30pin SAMTEC connector for X2 Module hardware.
                   Software has to be changed to accompany that HW change.
06/17/97   dhh     Added target T_Q for the Q Phones.
05/23/97   jjn     Assert SYS_WAKE when ERR_FATAL occurs (for the Module only)
01/22/97   rdh     Used Pascal directive on err_*_put_log to save ROM.
11/02/96   jah     Configured for TGP (T_T), added ERR_HAS_LCD_MESSAGE
05/24/96    ls     Fixed reentrancy problems and other errors.
07/21/95   ras     disabled interrupts in err_fatal_put_log
07/05/95   jah     Added lcd_message() for Gemini
07/16/93   jah     Changed LCD_MESSAGE() to lcd_message(), and updated
                   comments, per code review.
06/29/93   jah     Added LCD_MESSAGE support for LCD display of fatal errors.
04/27/93   jah     Made T_P changes to support fatal error test code download.
10/29/92    ip     Updated for full error logging and reporting.
08/13/92    ip     Release with interim indefinite wait for ERR_FATAL.
03/07/92    ip     Initial creation.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "qurt.h"
#include "qurt_memory.h"
#include "qurt_event.h"
#include "erri.h"
#include <stdlib.h>
#include <stringl.h>
#include "sys_m_internal.h"
#include "tms_utils.h"
#include "err_reset_detect.h"
#include "err_decompress.h"
#include "msg.h"
#include "DALSys.h"
#include "DDITimetick.h"
#include "err_smem_log.h"


/*===========================================================================

                      Prototypes for internal functions

===========================================================================*/
void err_emergency_error_recovery(void) ERR_NORETURN_ATTRIBUTE UERR_CODE_SECTION ;
extern void err_halt_execution( void ) ERR_NORETURN_ATTRIBUTE; //from err_jettison_core.s
static void err_reentrancy_violated(void) ERR_NORETURN_ATTRIBUTE UERR_CODE_SECTION;
static void err_raise_to_kernel(void) UERR_CODE_SECTION;
void err_fatal_handler(void) ERR_NORETURN_ATTRIBUTE;
void err_fatal_lock(void) UERR_CODE_SECTION;
static void err_minimal_logging (const err_const_type* const_blk, uint32 code1, 
		         uint32 code2, uint32 code3) UERR_CODE_SECTION;
void err_fatal_jettison_core (unsigned int line, const char *file_name,
  const char *format, uint32 param1, uint32 param2, uint32 param3) ERR_NORETURN_ATTRIBUTE;



/*===========================================================================

                      Prototypes for external functions

===========================================================================*/
extern void err_target_update_coredump_and_f3_trace( err_fatal_params_type * params );
extern void err_target_get_time(void);
extern ERR_OS_TCB_TYPE *  err_target_get_tcb(void);
extern void err_target_init (void);
extern void dog_force_bite( void );
extern void dog_force_kick( void );
extern void err_update_image_versioning_info (void);

extern void err_write_timestamp_string(void);


extern char* err_ssr_smem_buf_ptr;
extern boolean err_sfr_locked ;

/*===========================================================================

                 Defines and variable declarations for module

===========================================================================*/
#define ERR_SET_AND_FLUSH_PTR(PTR, VAL) \
  do { \
     PTR = VAL; \
     asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &PTR )));\
   } while (0)

#define ERR_FLUSH_ADDR(ADDR) \
  do { \
     asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &ADDR )));\
   } while (0)

#ifdef FEATURE_GENERIC_ERR_FATAL_MSG
/* Used to replace all ERR_FATAL message strings.
 * Reduces code size by eliminating other const strings used for errors.
 */
const char err_generic_msg[] = "Error Fatal, parameters: %d %d %d";
const char err_generic_msg_dynamic[] = "Error Fatal, check coredump.err.aux_msg";
#endif

  
/* Struct used to hold coredump data */
coredump_type coredump;
uint32 coredump_count=0;
err_fatal_params_type err_fatal_params UERR_DATA_SECTION;

/* Ptr used by assembly routines to grab registers */
/*  (update this as needed if struct changes)      */
arch_coredump_type* UERR_DATA_SECTION arch_coredump_ptr = 
                          (arch_coredump_type*)(&err_fatal_params.array);

static ERR_MUTEX_TYPE err_fatal_mutex UERR_DATA_SECTION;
static boolean err_fatal_mutex_init UERR_DATA_SECTION;
static boolean err_services_init_complete = FALSE;
static DalDeviceHandle* phTimetickHandle UERR_DATA_SECTION;

/* SFR (Subsystem restart Failure Reason) decl's */
#ifdef FEATURE_SMEM
  char* err_ssr_smem_buf_ptr= NULL;
  static const char err_sfr_init_string[]="SFR Init: wdog or kernel error suspected.";
  boolean err_sfr_locked = FALSE;
#endif  /* FEATURE_SMEM */
static char* err_dynamic_msg_scratch_buf = 0;

/* The function tables below are processed by the error handler
 * in the following order:
 *   1. err_preflush_internal (one time)
 *   2. err_preflush_external - Part of coredump structure (one time)
 *   3. err_flush_internal    (one time - transitions to non-returning kernel code)
 */

/* Decalared in err_target_*.c */
extern const err_cb_ptr err_preflush_internal[];

/* Flush will be done by kernel */
static const err_cb_ptr err_flush_internal[] =
{
  err_raise_to_kernel,   /* Does not return */
  /* NULL must be last in the array */
  NULL
};

#ifndef TMS_UTILS_BUF_SZ_ZERO   
#define TMS_UTILS_BUF_SZ_ZERO       0
#endif

/*===========================================================================

                              Function definitions

===========================================================================*/

/*===========================================================================

FUNCTION ERR_INITIALIZE_COREDUMP

DESCRIPTION
  Initializes coredump

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
static void err_initialize_coredump (void)
{
  /* Set type and version values */
  coredump.version = ERR_COREDUMP_VERSION;
  coredump.arch.type = ERR_ARCH_COREDUMP_TYPE;
  coredump.arch.version = ERR_ARCH_COREDUMP_VER;  
  coredump.os.type = ERR_OS_COREDUMP_TYPE;
  coredump.os.version = ERR_OS_COREDUMP_VER;
  coredump.err.version = ERR_COREDUMP_VER;

} /* err_initialize_coredump */

/*===========================================================================

FUNCTION ERR_INIT

DESCRIPTION
  This function initializes error services and calls into target based 
  error intializing routine.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  Error log is loaded from NVM into RAM resident cache.

===========================================================================*/

void err_init (void)
{
  /* Init Timer */
  DalTimetick_Attach("SystemTimer", &phTimetickHandle);

  if(!err_fatal_mutex_init)
  {
    ERR_MUTEX_INIT(&err_fatal_mutex);
    err_fatal_mutex_init=TRUE;
  }

  #ifdef FEATURE_SMEM
    err_ssr_smem_buf_ptr = (char * )sys_m_init_sfr_buffer();
    if (NULL != err_ssr_smem_buf_ptr)
    {
      strlcpy (err_ssr_smem_buf_ptr, err_sfr_init_string, ERR_SSR_REASON_SIZE_BYTES);
    }
  #endif

  /* initialize the smem buffer for writing extended log info */
  err_smem_log_init();
  
  /* Write the timestamp string into the smem buffer (only from Root PD).
     * This API should always be called before writing any other info to the
     * SMEM buffer. Since the smem parser tool depends on the below string
     * to locate the err smem buffer.
     */
  err_write_timestamp_string();

  err_target_init();
  
  /* We use a scratch buf from the heap to minimize risk of an 
  overwrite corrupting the coredump structure or stack */
  err_dynamic_msg_scratch_buf = (char*)malloc(ERR_DYNAMIC_MSG_SIZE);
  if (err_dynamic_msg_scratch_buf)
  {
    memset (err_dynamic_msg_scratch_buf, 0, ERR_DYNAMIC_MSG_SIZE);
  }

  ERR_LOG_MSG("Err service initialized.");

  err_initialize_coredump();

  err_update_image_versioning_info();

  /* Write the updated image versioning information to smem buffer (only from Root PD) */
  err_smem_log_image_version();

  err_services_init_complete = TRUE;

} /* err_init */

/*===========================================================================

FUNCTION ERR_GET_TIMETICK

DESCRIPTION
  Add timetick value to specified address if timetick handle is initialized

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None .

===========================================================================*/

static void err_get_timetick(DalTimetickTime64Type * tick)
{
  if (NULL != phTimetickHandle)
  {
    /* Best effort, no failure action */
    DalTimetick_GetTimetick64(phTimetickHandle, tick );
  }  
} /* err_get_timetick */

/*===========================================================================

FUNCTION ERR_UPDATE_COREDUMP_TID_AND_IS_EXCEPTION

DESCRIPTION
 Updates tid information to coredump only the tid is not updated earlier.

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS

===========================================================================*/

void err_update_coredump_tid_and_is_exception(uint32 tid, boolean is_exception)
{
  if ( coredump.err.tid == 0 )
  {
    coredump.err.tid = tid;
  }
  coredump.err.is_exception = is_exception;

} /* err_update_coredump_tid_and_is_exception */

/*===========================================================================

FUNCTION ERR_CALL_NEXT_TO_STM_CB

DESCRIPTION
 Calls err next to STM cb

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS

===========================================================================*/

void err_call_next_to_STM_CB(void)
{
  /* Kick Dog */
  dog_force_kick();
    
  /* Set the start time for postflush callbacks */
  err_get_timetick( &(coredump.err.err_next_to_STM.cb_start_tick) );
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_next_to_STM.cb_start_tick );

  if ( coredump.err.err_next_to_STM.err_cb )
  {
    coredump.err.err_current_cb = 
	      coredump.err.err_next_to_STM.err_cb;
    coredump.err.err_next_to_STM.cb_start = TRUE;

    ERR_FLUSH_ADDR(coredump.err.err_current_cb);
    ERR_FLUSH_ADDR(coredump.err.err_next_to_STM.cb_start);
 
    coredump.err.err_current_cb();
  }

  /* Kick Dog */
  dog_force_kick();

} /* err_call_next_to_STM_CB */

/*===========================================================================

FUNCTION ERROR_FATAL_HANDLER

DESCRIPTION
  This function is invoked from err_fatal_jettison_core. When using JTAG,
  default breakpoint for ERR_FATAL should be placed at this function.
  Will log error to SMEM, kill the PA, and copy the coredump data into
  the err_data structure in unintialized memory.


DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS
  **************************************************************
  ************ THERE IS NO RETURN FROM THIS FUNCTION ***********
  **************************************************************

===========================================================================*/
void err_fatal_handler ( void )
{
  int fptr_index;
  static uint32 err_count=0;

  /* Clean Cache */
  qurt_mem_cache_clean(0,0, QURT_MEM_CACHE_FLUSH_ALL, QURT_MEM_DCACHE);

  err_count++;

  if((err_count>1) || (err_services_init_complete!=TRUE))
  {
    err_initialize_coredump();

    /* May not return */
    err_emergency_error_recovery();
  }

  fptr_index=0;
  while(err_preflush_internal[fptr_index] != NULL)
  {
    /* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(coredump.err.err_current_cb, err_preflush_internal[fptr_index]); 
    coredump.err.err_current_cb();
    fptr_index++;
  }

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_LOG_EXTERNEL_CBS_PRE);
	  
  for(fptr_index=0; fptr_index<ERR_MAX_PREFLUSH_CB; fptr_index++)
  {
    err_get_timetick( 
      &(coredump.err.err_preflush_external[fptr_index].cb_start_tick));

    ERR_FLUSH_ADDR(coredump.err.err_preflush_external[fptr_index].cb_start_tick);
    
    /* Cycle through external functions */
    if(coredump.err.err_preflush_external[fptr_index].err_cb!= 0)
    {
      /* Kick Dog */
      dog_force_kick();

      coredump.err.err_current_cb = 
	      coredump.err.err_preflush_external[fptr_index].err_cb;
      coredump.err.err_preflush_external[fptr_index].cb_start = TRUE;

      ERR_FLUSH_ADDR(coredump.err.err_current_cb);
      ERR_FLUSH_ADDR(coredump.err.err_preflush_external[fptr_index].cb_start);
 
      coredump.err.err_current_cb();
    }
  }

  /* Set the start time for non existent external cb, Can be used to calculate 
   * total time spanned by callbacks */
  err_get_timetick( 
      &(coredump.err.err_preflush_external[ERR_MAX_PREFLUSH_CB].cb_start_tick));
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_preflush_external[ERR_MAX_PREFLUSH_CB].cb_start_tick );

  /* Kick Dog */
  dog_force_kick();

  /* Set the start time for postflush callbacks */
  err_get_timetick( &(coredump.err.err_postflush_external.cb_start_tick));
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_postflush_external.cb_start_tick );

  if ( coredump.err.err_postflush_external.err_cb )
  {
    coredump.err.err_current_cb = 
	      coredump.err.err_postflush_external.err_cb;
    coredump.err.err_postflush_external.cb_start = TRUE;

    ERR_FLUSH_ADDR(coredump.err.err_current_cb);
    ERR_FLUSH_ADDR(coredump.err.err_postflush_external.cb_start);
 
    coredump.err.err_current_cb();

    /* Kick Dog */
    dog_force_kick();
  }

  /* Main loop (cache flush happens here, along with other
   * one-time post-flush operations */
  fptr_index=0;
  while(err_flush_internal[fptr_index] != NULL)
  {
    /* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(coredump.err.err_current_cb, err_flush_internal[fptr_index]); 
    coredump.err.err_current_cb();
    fptr_index++;
  }

  /* Must not reach here */
  err_halt_execution();  

} /* err_fatal_handler */


/*===========================================================================

FUNCTION ERR_FATAL_TRY_LOCK
DESCRIPTION
  Updates Error functionality at the entry of error. This is intentionally 
  used from exception handler.
============================================================================*/
void err_fatal_try_lock(void)
{
  if(err_fatal_mutex_init==TRUE)
  {
  ERR_MUTEX_TRY_LOCK(&err_fatal_mutex);
  }
  
  /* err_get_timetick is not island mode safe */
  err_get_timetick( &(coredump.err.err_handler_start_time));

  /* Kick Dog */
  dog_force_kick();

} /* err_fatal_try_lock */


/*===========================================================================

FUNCTION ERR_FATAL_LOCK
DESCRIPTION
  Gets mutex for err_fatal to prevent multiple and/or cascading errors
============================================================================*/
void err_fatal_lock(void)
{
  static boolean err_reentrancy_flag UERR_DATA_SECTION ;
  if(err_fatal_mutex_init==TRUE)
  {
    ERR_MUTEX_LOCK(&err_fatal_mutex);

    //mutex does not prevent the same thread being routed back into err_fatal by a bad callback
    if(err_reentrancy_flag)
    {
      //does not return
      err_reentrancy_violated();
    }
    else
    {
      err_reentrancy_flag = TRUE;
    }
  }
  else
  {
    /* If not intialized then this is an early ERR_FATAL */
    /* Proceed anyway so it can be handled */
  }

  /* Kick Dog */
  dog_force_kick();
  
} /* err_fatal_lock */


/*===========================================================================

FUNCTION ERR_FATAL_CORE_DUMP
DESCRIPTION
  Logs fatal error information, including a core dump.

  NOTE: There is no return from this function.
============================================================================*/
void err_fatal_core_dump (
  unsigned int line,      /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format   /* format string */
)
{
  err_fatal_lock();
  err_fatal_jettison_core(line, file_name, format, 0, 0, 0);
}


/*===========================================================================

FUNCTION ERR_FATAL_JETTISON_CORE
DESCRIPTION
  Logs fatal error information, including a core dump.
  Not to be called directly by outside code -- for that, use the function
  err_fatal_core_dump().

  NOTE: There is no return from this function.
============================================================================*/
#define ERR_FATAL_EXCEPTION_REENTRANCY "ERR_FATAL_EXCEPTION_REENTRANCY"

void err_fatal_jettison_core (
  unsigned int line,       /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format,    /* format string */
  uint32 param1,
  uint32 param2,
  uint32 param3
)
{

  uint32 stack_base = 0; /* lower start address of the stack */
  uint32 stack_end = 0;   /* higher adress, i.e. range of the stack */

  /* NOTE: register information should already be saved prior to
   * calling this function.
   */

  qurt_thread_attr_t *attr = NULL;

  /* Get tcb_ptr (if not pre-filled by err_exception_handler) */
  if (!coredump.os.tcb_ptr) {
    coredump.os.tcb_ptr = err_fatal_params.tcb;
  }

  /* Store line number */
  coredump.err.linenum = line;

  /* Copy file name */
  if(file_name != 0)
  {
    (void) strlcpy((char *)coredump.err.filename,
                       (char *)file_name,
                       ERR_LOG_MAX_FILE_LEN);
  }

  /* Copy message string */
  if(format != 0)
  {
    (void) strlcpy((char *)coredump.err.message,
                       (char *)format,
                       ERR_LOG_MAX_MSG_LEN);
  }

  coredump.err.param[0]=param1;
  coredump.err.param[1]=param2;
  coredump.err.param[2]=param3;

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_JETTISION_CORE_POST);

  /* Get tcb name from tid name. It uses k0lock which is internal to kernel. 
   * But that doesn�t preempt the caller task in any way */
  attr = qurt_system_stm_thread_attr_get(coredump.err.tid);

  if ( attr != NULL )
  {
    strlcpy(coredump.err.tcb_name, attr->name, QURT_THREAD_ATTR_NAME_MAXLEN );
    stack_base = (uint32)attr->stack_addr;      /* base address of the stack */
    stack_end = stack_base + attr->stack_size;  /* Range of the stack */
  }

  coredump.err.stack_error = STACK_NO_ERROR;

  /* Only check for stack_error if we got the stack addresses */
  if ( stack_base!= 0 && stack_end != 0 )
  {
    /* Check for stack overflow */
    if ( (coredump.arch.regs.name.fp < stack_base) ||
    	   (coredump.arch.regs.name.sp < stack_base) )
    {
      /* stack overflow detected */
  	  coredump.err.stack_error |= STACK_OVERFLOW;
    }

    /* Check for stack underflow */
    if ( (coredump.arch.regs.name.fp > stack_end) ||
    	   (coredump.arch.regs.name.sp > stack_end) )
    {
      /* stack underflow detected */
  	  coredump.err.stack_error |= STACK_UNDERFLOW;
    }
  }
  else
  {
    coredump.err.stack_error |= STACK_ERR_OBTAINING;
  }  

  /* Update the coredump pid */
  coredump.err.pd_id = qurt_process_get_id(); 

  /* Check if an user exceptions and ERR_FATAL arrived simultaneously */
  if ( err_fatal_params.crumb_trail_bmsk != 0 && 
        coredump.err.is_exception == TRUE)
  {
    /* Record secondary failure to coredump */
    strlcpy(coredump.err.aux_msg, ERR_FATAL_EXCEPTION_REENTRANCY, 
	    sizeof(ERR_FATAL_EXCEPTION_REENTRANCY));   
  } 

  /* Call ERR_FATAL handler (no return) */
  err_fatal_handler();

}

/*=========================================================================

FUNCTION err_log_ssr_failure_reason

DESCRIPTION
  Used to log a minimal set of failure reason to smem.  Primarily to assist
  locating faulting subsystem in many-subsystem architectures.

DEPENDENCIES
  smem

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void err_log_ssr_failure_reason(void)
{
  #ifdef FEATURE_SMEM
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_LOG_SSR_PRE);

  if (!err_sfr_locked)
  {
    word sfr_line = coredump.err.linenum;
    const char *sfr_file_ptr = coredump.err.filename;
    const char *sfr_msg_ptr = coredump.err.message;
    uint32 sfr_p0 = coredump.err.param[0];
    uint32 sfr_p1 = coredump.err.param[1];
    uint32 sfr_p2 = coredump.err.param[2];
    char *sfr_buf_ptr = err_ssr_smem_buf_ptr;
    int sfr_written = 0;

    if (sfr_buf_ptr && sfr_file_ptr)
    {
      /* Write "__MODULE__:__LINE:" */
      sfr_written = tms_utils_fmt(sfr_buf_ptr, ERR_SSR_REASON_SIZE_BYTES, 
		                     "%s:%d:", sfr_file_ptr, sfr_line);

      if ((sfr_written > 0) && sfr_msg_ptr)
      {
        /* Append err fatal message */
        if(sfr_written > TMS_UTILS_BUF_SZ_ZERO){
           sfr_buf_ptr += (sfr_written-1); 
	   /*-1 since tms_utils_fmt() API returns written length with NULL 
	    * charectar increment and overwrite previous null-term*/
        }
        sfr_written += tms_utils_fmt(sfr_buf_ptr, 
			(ERR_SSR_REASON_SIZE_BYTES-sfr_written), 
			 sfr_msg_ptr, sfr_p0, sfr_p1, sfr_p2);
      }
      /* Commit the write before proceeding */
      ERR_MEMORY_BARRIER();

      err_sfr_locked = TRUE;
    } /* (sfr_buf_ptr && sfr_file_ptr) */
  } /* (!err_sfr_locked) */

  #endif /* FEATURE_SMEM */
} /* err_log_ssr_failure_reason */

/*=========================================================================

FUNCTION err_emergency_error_recovery

DESCRIPTION
  Action to be taken when more than one error has occurred, or if an
  error occurs before err_init has completed.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  No return from this function

===========================================================================*/
void err_emergency_error_recovery( void )
{
  /* Define action to be taken when multiple crashes occur */
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_EMERGENCY_RECOVERY_PRE);

  /* flush cache, etc - does not return*/
  err_raise_to_kernel();

  /* Stop active thread - will not reach here*/
  err_halt_execution();
}

/*=========================================================================

FUNCTION err_crash_cb_register

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called after an ERR_FATAL
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.

  !!!These functions MUST NOT call ERR_FATAL/ASSERT under ANY circumstances!!!

DEPENDENCIES
  None

RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_register(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(coredump.err.err_preflush_external[i].err_cb == NULL)
	{
	  coredump.err.err_preflush_external[i].err_cb = cb;
	  rval = TRUE;
	  break;
	}
  }

  return rval;
}


/*=========================================================================

FUNCTION err_crash_cb_dereg

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(coredump.err.err_preflush_external[i].err_cb == cb)
	{
	  coredump.err.err_preflush_external[i].err_cb = NULL;
	  rval = TRUE;
	  break;
	}
  }

  return rval;
}
/*=========================================================================

FUNCTION err_crash_cb_reg_next_to_STM

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called immediately after
  STM API call 
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.
 
  !!!These functions MUST NOT call ERR_FATAL under ANY circumstances!!!

DEPENDENCIES
  None
 
RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.

SIDE EFFECTS
  Only one registration of such API is supported so if its used by more than one
  clients than it will overwrite the old registered callback,
  this API was provided only for special case handling to stop ULT Audio Core
  in DPM PL
===========================================================================*/
boolean err_crash_cb_reg_next_to_STM(err_cb_ptr cb)
{
  if(NULL == coredump.err.err_next_to_STM.err_cb )
  {
    /* Check if already a callback registered*/
    coredump.err.err_next_to_STM.err_cb = cb;
    return TRUE;
  }
  else{
     return FALSE;
  }
}
/*=========================================================================

FUNCTION err_crash_cb_dereg_next_to_STM

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg_next_to_STM(err_cb_ptr cb)
{
  if(coredump.err.err_next_to_STM.err_cb == cb)
  {
    coredump.err.err_next_to_STM.err_cb = NULL;
    return TRUE;
  }
  else{
    return FALSE;
  }
}

/*===========================================================================
FUNCTION err_crash_cb_postflush_register
DESCRIPTION
  Register for callback function. The Callback function will be called after
  external functions are already called.
  It will be upto the callback to resolve its cache issues. 
============================================================================*/


boolean err_crash_cb_postflush_register (err_cb_ptr cb)
{
  if(NULL == coredump.err.err_postflush_external.err_cb )
  {
    /* Check if already a callback registered*/
    coredump.err.err_postflush_external.err_cb = cb;
    return TRUE;
  }
  else{
     return FALSE;
  }

} /* err_crash_cb_postflush_register*/

boolean err_crash_cb_postflush_deregister (err_cb_ptr cb)
{
  if(coredump.err.err_postflush_external.err_cb == cb)
  {
    coredump.err.err_postflush_external.err_cb = NULL;
    return TRUE;
  }
  else{
    return FALSE;
  }
} /* err_crash_cb_postflush_deregister */


/*=========================================================================

FUNCTION err_raise_to_kernel

DESCRIPTION
  Function which will terminate user space/PD handling and raise to kernel

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void err_raise_to_kernel(void)
{
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_RAISE_TO_KERNEL_PRE);
  
  err_get_timetick(&(coredump.err.err_handler_end_time));

  #ifdef ERR_FATAL_FLUSH_CACHE_NO_RETURN
    ERR_FATAL_FLUSH_CACHE_NO_RETURN();
  #else
    #warning ERR_FATAL_FLUSH_CACHE_NO_RETURN not defined
  #endif

  /* Above should not have returned, let dog expire */
  while(1)
  {
    //compiler bug workaround for RVCT 4.1 P 631 & static analysis
    static volatile int err_always_false = 0;
    if (err_always_false)
    {
      return; /* this must NEVER return */
    }
  };

}

/*===========================================================================

FUNCTION       err_fatal_post_exception_processing

DESCRIPTION
  This is called from exception handler after error fatal raises an 
  exception

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void err_fatal_post_exception_processing(void)
{
  err_target_get_time();

  /* Copy trace log from island mode data structure */
  coredump.err.crumb_trail_bmsk |= err_fatal_params.crumb_trail_bmsk; 
  
  /* Copy compressed prt to coredump */
  coredump.err.compressed_ptr = err_fatal_params.msg_const_ptr;

  /* Copy register content */
  memscpy( coredump.arch.regs.array, SIZEOF_ALLOCATED_COREDUMP_REG,
             err_fatal_params.array,  SIZEOF_ARCH_COREDUMP_REGISTERS * sizeof(uint32) );

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_CACHCE_CLEAN_PRE);
  
  /* Clean Cache */
  qurt_mem_cache_clean(0,0, QURT_MEM_CACHE_FLUSH_ALL, QURT_MEM_DCACHE);

  err_call_next_to_STM_CB();

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_COREDUMP_UPDATED_PRE);

  /* Decompress and f3 trace */
  err_target_update_coredump_and_f3_trace(&err_fatal_params);

} /* err_fatal_post_exception_processing */

	

/*===========================================================================

FUNCTION       err_minimal_logging

DESCRIPTION

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

static void err_minimal_logging 
(
  const err_const_type* const_blk, 
  uint32                code1, 
  uint32                code2, 
  uint32                code3
)
{
   err_fatal_params.param1 = code1;
   err_fatal_params.param2 = code2;
   err_fatal_params.param3 = code3;
   err_fatal_params.tcb    = err_target_get_tcb();
   err_fatal_params.msg_const_ptr = const_blk;

} /* err_minimal_logging*/


/*===========================================================================

FUNCTION       err_Fatal_internal

DESCRIPTION
  Do not call any of these functions directly.  They should be accessed by
  macros defined in err.h exclusively.
  Code is expected to on run in uImage as well.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void err_Fatal_internal3 
(
  const err_const_type* const_blk, 
  uint32                code1, 
  uint32                code2, 
  uint32                code3
)
{
 /* Enter critical section */
 err_fatal_lock();
 
 /* Capture registers */
 jettison_core();
 
 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_PRE);
 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_JETTISON_CORE_POST);

 /* Capture minimal log, Updates err_fatal_params */
 err_minimal_logging( const_blk, code1, code2, code3 );

 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_RAISE_EXCEPTION_PRE);

 /*  Raise an exception to qurt */
 qurt_exception_raise_nonfatal( ERR_RAISE_EXCEPTION_ARG(ERR_TYPE_ERR_FATAL) );

 /* Must not reach here */
 dog_force_bite();

 while(1);

}

void err_Fatal_internal2 (const err_const_type* const_blk, uint32 code1, uint32 code2)
{
  err_Fatal_internal3(const_blk, code1, code2, 0 );
}

void err_Fatal_internal1 (const err_const_type* const_blk, uint32 code1)
{
  err_Fatal_internal3(const_blk, code1, 0, 0 );
}

void err_Fatal_internal0 (const err_const_type* const_blk)
{
  err_Fatal_internal3(const_blk, 0, 0, 0 );
}

/*===========================================================================

FUNCTION       err_Fatal_dynamic

DESCRIPTION
  Do not call any of these functions directly.  They should be accessed by
  macros defined in err.h exclusively.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void err_process_dynamic_callback(err_dynamic_msg_cb callback)
{
  if (err_services_init_complete && callback && err_dynamic_msg_scratch_buf)
  {
    ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_DYNAMIC_CALLBACK_PRE);
    callback(err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);

    /* Was something written? */
    if (err_dynamic_msg_scratch_buf[0] != 0)
    {
      /* Guarantee null termination */
      err_dynamic_msg_scratch_buf[ERR_DYNAMIC_MSG_SIZE-1]=0;

      /* Copy to relevant structures */
      memscpy(coredump.err.aux_msg, sizeof(coredump.err.aux_msg), 
		err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
      #ifdef FEATURE_SMEM
      if (err_ssr_smem_buf_ptr&& !err_sfr_locked)
      {
        memscpy(err_ssr_smem_buf_ptr, ERR_SSR_REASON_SIZE_BYTES, 
	         err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
        err_sfr_locked = TRUE;
      }
      #endif /* FEATURE_SMEM */
    }
  }
}

void err_process_dynamic_callback_1
(
  err_dynamic_msg_cb_1 callback, 
  const uint32 cb_param
)
{
  if (err_services_init_complete && callback && err_dynamic_msg_scratch_buf)
  {
    ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_DYNAMIC_CALLBACK_PRE);
    callback(err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE, cb_param);

    /* Was something written? */
    if (err_dynamic_msg_scratch_buf[0] != 0)
    {
      /* Guarantee null termination */
      err_dynamic_msg_scratch_buf[ERR_DYNAMIC_MSG_SIZE-1]=0;

      /* Copy to relevant structures */
      memscpy(coredump.err.aux_msg, sizeof(coredump.err.aux_msg), 
		err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
      #ifdef FEATURE_SMEM
      if (err_ssr_smem_buf_ptr&& !err_sfr_locked)
      {
        memscpy(err_ssr_smem_buf_ptr, ERR_SSR_REASON_SIZE_BYTES, 
		err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
        err_sfr_locked = TRUE;
      }
      #endif /* FEATURE_SMEM */
    }
  }
}


void err_Fatal_internal_dynamic 
(
  const err_const_type* const_blk, 
  err_dynamic_msg_cb callback
)
{

 /* Enter critical section */
 err_fatal_lock();
 
 /* Capture registers */
 jettison_core();
 
 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_PRE);
 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_JETTISON_CORE_POST);
 
 /* Capture minimal log, Updates err_fatal_params */
 err_minimal_logging( const_blk, 0, 0, 0 );

 /* Allow callee to fill SFR and aux_msg */
 err_process_dynamic_callback(callback);

 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_RAISE_EXCEPTION_PRE);

 /*  Raise an exception to qurt */
 qurt_exception_raise_nonfatal( ERR_RAISE_EXCEPTION_ARG(ERR_TYPE_ERR_FATAL) );

 /* Must not reach here */
 dog_force_bite();

 while(1);

}

void err_Fatal_internal_dynamic_1 
(
  const err_const_type* const_blk, 
  err_dynamic_msg_cb_1 callback, 
  const uint32 cb_param
)
{

 /* Enter critical section */
 err_fatal_lock();
 
 /* Capture registers */
 jettison_core();
 
 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_PRE);
 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_JETTISON_CORE_POST);
 
 /* Capture minimal log, Updates err_fatal_params */
 err_minimal_logging( const_blk, 0, 0, 0 );

 /* Allow callee to fill SFR and aux_msg */
 err_process_dynamic_callback_1(callback, cb_param);

 ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_RAISE_EXCEPTION_PRE);

 /*  Raise an exception to qurt */
 qurt_exception_raise_nonfatal( ERR_RAISE_EXCEPTION_ARG(ERR_TYPE_ERR_FATAL) );

 /* Must not reach here */
 dog_force_bite();

 while(1);

}

/*=========================================================================

FUNCTION err_reentrancy_violated

DESCRIPTION
  This will only be called when ERR_FATAL is called while processing an
  ERR_FATAL.  It usually means that somone has registered a non-compliant
  callback function using 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
#define ERR_REENTRANCY_STRING "ERR_FATAL reentrancy violation, remove cb until resolved"
static void err_reentrancy_violated(void)
{
  if ( qurt_island_get_status() == FALSE )
  {
    /* Record secondary failure to coredump */
    strlcpy(coredump.err.aux_msg, ERR_REENTRANCY_STRING, sizeof(ERR_REENTRANCY_STRING));
  }

  err_emergency_error_recovery();

}
