#ifndef QDSS_Q6B_CONTROL_H
#define QDSS_Q6B_CONTROL_H
/**
 *
 * @file qdss_q6b_control.h
 *
 * QUALCOMM Debug Subsystem (QDSS) control interface (acting as a proxy for
 * Q6B) with the drivers which communicate to Q6B.
 *
 */
/*=============================================================================
   Copyright (c) 2015 Qualcomm Technologies, Inc.
   All rights reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
   $Header: //components/rel/core.mpss/3.9.1/api/debugtrace/qdss_q6b_control.h#1 $

   when       who     what, where, why
   --------   ---    -----------------------------------------------------------
   01/26/15   lht    Initial revision.

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qurt/qurt_trace.h"     // Q6A parameter values.

/*---------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ------------------------------------------------------------------------*/

// QDSS return values of Q6B control functions.
#define QDSS_Q6B_CTRL_RVAL_SUCCESS        0
#define QDSS_Q6B_CTRL_RVAL_NOT_SUPPORTED  1

/*---------------
 * ETM Controls
 *   Setting values to be the same as Q6A QURT
 * ------------*/

// ETM configuration formats available.
#define QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_ID_DSP    1

// ETM configuration format of this code
#define QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_ID  QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_ID_DSP
#define QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_NAME  "dsp"

// Q6B's (driver SW) return values of ETM functions.
#define QDSS_DSP_ETM_RVAL_SUCCESS   QURT_ETM_SETUP_OK
#define QDSS_DSP_ETM_RVAL_ERR       QURT_ETM_SETUP_ERR

// ETM state parameter values
#define QDSS_DSP_ETM_STATE_OFF   QURT_ETM_OFF
#define QDSS_DSP_ETM_STATE_ON    QURT_ETM_ON

/* DSP configurable parameters */

// ETM Mode (a.k.a. type) bit mask values, may be logically Or'd together
#define QDSS_DSP_ETM_MODE_DEFAULT         QURT_ETM_TYPE_PC_AND_MEMORY_ADDR
#define QDSS_DSP_ETM_MODE_PC_ADDR         QURT_ETM_TYPE_PC_ADDR
#define QDSS_DSP_ETM_MODE_MEMORY_ADDR     QURT_ETM_TYPE_MEMORY_ADDR
#define QDSS_DSP_ETM_MODE_TESTBUS         QURT_ETM_TYPE_TESTBUS
#define QDSS_DSP_ETM_MODE_CYCLE_ACCURATE  QURT_ETM_TYPE_CYCLE_ACCURATE
#define QDSS_DSP_ETM_MODE_CYCLE_COARSE    QURT_ETM_TYPE_CYCLE_COARSE

// ETM Route values
#define QDSS_DSP_ETM_ROUTE_DEFAULT  QURT_ETM_ROUTE_TO_QDSS
#define QDSS_DSP_ETM_ROUTE_QDSS     QURT_ETM_ROUTE_TO_QDSS
#define QDSS_DSP_ETM_ROUTE_Q6ETB    QURT_ETM_ROUTE_TO_Q6ETB

// ETM Filter bit mask values, may be logically Or'd together
#define QDSS_DSP_ETM_FILTER_DEFAULT QURT_ETM_TRACE_FILTER_ALL
#define QDSS_DSP_ETM_FILTER_HNUM0   QURT_ETM_TRACE_FILTER_HNUM0
#define QDSS_DSP_ETM_FILTER_HNUM1   QURT_ETM_TRACE_FILTER_HNUM1
#define QDSS_DSP_ETM_FILTER_HNUM2   QURT_ETM_TRACE_FILTER_HNUM2
#define QDSS_DSP_ETM_FILTER_HNUM3   QURT_ETM_TRACE_FILTER_HNUM3
#define QDSS_DSP_ETM_FILTER_ALL     QURT_ETM_TRACE_FILTER_ALL

/*---------------------------------------------------------------------------
 * Type Declarations
 * ------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
   Callback function to enable/disable the ETM.

   @param[in]  flag  QDSS_DSP_ETM_STATE_OFF = Disable
                     QDSS_DSP_ETM_STATE_ON = Enable
   @return
      QDSS_DSP_ETM_RVAL_SUCCESS  Success
      QDSS_DSP_ETM_RVAL_ERR      Failure
 */
typedef unsigned int (*qdss_q6b_ctrl_etm_enable_cb_t)(unsigned int state);

/*-------------------------------------------------------------------------*/
/**
   Callback function to configure the ETM trace.
   @param[in]  mode     See QDSS_DSP_ETM_MODE_xxx. May be OR'd together.
   @param[in]  route    See QDSS_DSP_ETM_ROUTE_xxx.
   @param[in]  filter   See QDSS_DSP_ETM_FILTER_xxx. May be OR'd together.
   @return
      QDSS_DSP_ETM_RVAL_SUCCESS   Success
      QDSS_DSP_ETM_RVAL_ERR       Failure
 */
typedef unsigned int (*qdss_q6b_ctrl_etm_set_config_cb_t)(
      unsigned int mode,
      unsigned int route,
      unsigned int filter
   );

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/**
   Register the callback to enable/disable the ETM.

   @param[in]  cb_fcn   Pointer to the callback function.

   @return
      QDSS_Q6B_CTRL_RVAL_SUCCESS       Successful
      QDSS_Q6B_CTRL_RVAL_NOT_SUPPORTED Not supported
 */
int qdss_q6b_ctrl_register_etm_enable_cb(
   qdss_q6b_ctrl_etm_enable_cb_t cb_fcn
);

/*-------------------------------------------------------------------------*/
/**
   Register the callback to configure the ETM trace.

   @param[in]   cb_fcn   Pointer to the callback function.

   @return
      QDSS_Q6B_CTRL_RVAL_SUCCESS       Successful
      QDSS_Q6B_CTRL_RVAL_NOT_SUPPORTED Not supported
 */
int qdss_q6b_ctrl_register_etm_set_config_cb(
   qdss_q6b_ctrl_etm_set_config_cb_t cb_fcn
);


#endif /* QDSS_Q6B_CONTROL_H */

