#include "qmi_tct_server.h"
#include "tct.h"
#include "fs_public.h"
#include "nv.h"
#include "sfs_api.h"
/* ================================ DEFINE ================================== */
#define TCT_QMI_LOG_ENABLE


/* ========================================================================== */


/* =============================== DEBUG LOG ================================ */
#ifdef TCT_QMI_LOG_ENABLE
#include <stdio.h> /* vsnprintf() */
#include <stdarg.h> /* va_list */
static char _log_buffer[ 256 ];
static rex_crit_sect_type _log_crit_sect;

static void _log_printf ( const char * fmt, ... )
{
  va_list args;

  va_start( args, fmt );
  rex_enter_crit_sect(&_log_crit_sect);
  vsnprintf( _log_buffer, sizeof( _log_buffer ), fmt, args );
  MSG_SPRINTF_1( MSG_SSID_LEGACY, MSG_LEGACY_HIGH, "%s", _log_buffer );
  rex_leave_crit_sect(&_log_crit_sect);
  va_end( args );
}

#define TLOG _log_printf
#else
#define TLOG
#endif /* TCT_QMI_DEBUG */

/* ========================================================================== */


/* ================================= EXTERN ================================= */
extern void tct_server_event_loop(dword param);
/* ========================================================================== */


/* ============================= GLOBAL VARIABLE ============================ */
qmi_tct_server_data_type tct_server_data;
/* ========================================================================== */

qmi_core_server_error_type
qmi_tct_ping_req_v01_handler
(
    void               *server_data,
    void               *conn_obj,
    qmi_req_handle     req_handle,
    uint32_t           msg_id,
    void               *req_c_struct,
    uint32_t           req_c_struct_len
)
{
    qmi_core_server_error_type rc;
    test_req_msg_v01 *req_msg;
    test_resp_msg_v01 *resp_msg;

    resp_msg = (test_resp_msg_v01 *)calloc(1, sizeof(test_resp_msg_v01));
    if (!resp_msg) {
        return QMI_CORE_SERVER_MEMORY_ERR;
    }
    memset(resp_msg, 0, sizeof(test_resp_msg_v01));

    req_msg = (test_req_msg_v01 *)req_c_struct;
    if (!strncmp(req_msg->ping, "ping", 4)) {
        TLOG("receive ping");
        memcpy(resp_msg->pong, "pong", 4);
    }

    //qmi_tct_send_hello_ind();

    /* Send a response back to client */
    rc = qmi_core_server_send_resp(req_handle,
                                   QMI_PING_RESP_V01,
                                   resp_msg,
                                   sizeof(test_resp_msg_v01));

    free(resp_msg);

    return rc;
}

qmi_indication_error_type qmi_tct_send_hello_ind(void)
{
    qmi_indication_error_type err;
    test_ind_msg_v01 ind_msg;

    memcpy(&ind_msg.hello, "hello", 5);

    err = qmi_indication_send_broadcast(
            tct_server_data.tct_server->core_object.core_server_data->service_handle,
            QMI_HELLO_IND_V01, &ind_msg, sizeof(test_ind_msg_v01));

    return err;
}

#if 0
//FR-346355, add nv read interface
nv_stat_enum_type
nvio_read_rfnv_item(
  uint32_t    item,
  void   * data_ptr,
  uint32 *item_size,
  int type
)
{
  int   return_size;
  char  f_name[120];
  struct fs_stat temp_buf;

  if(type==0) //rfnv
       if(item==67300){
            (void) snprintf(f_name, sizeof(f_name), "%s", "/nv/item_files/mcs/tcxomgr/field_cal_params");
       } else
            (void) snprintf(f_name, sizeof(f_name), "%s%08ld", "/nv/item_files/rfnv/", item);

  else if(type==1)
     (void) snprintf(f_name, sizeof(f_name), "%s%ld", "/nvm/num/", item);
  /* Check if the file is present or not. */
  if(efs_stat(f_name,&temp_buf) == -1) {
    return NV_NOTACTIVE_S;
  }

  *item_size=temp_buf.st_size;
  return_size = efs_get(f_name, data_ptr, *item_size);
  //read failed if rfnv big then 2048 bit
  if (return_size == -1)
    return  NV_FAIL_S;

  return NV_DONE_S;
} /* End of nvio_read_rfnv_item*/
#endif

qmi_core_server_error_type
qmi_nv_read_req_v01_handler
(
    void               *server_data,
    void               *conn_obj,
    qmi_req_handle     req_handle,
    uint32_t           msg_id,
    void               *req_c_struct,
    uint32_t           req_c_struct_len
)
{
    qmi_core_server_error_type rc;
    rc=QMI_CORE_SERVER_INTERNAL_ERR;
    retrofit_nv_read_req_msg_v01 *req_msg;
    retrofit_nv_read_resp_msg_v01 *resp_msg;
    struct fs_stat stat;


  req_msg = (retrofit_nv_read_req_msg_v01 *)req_c_struct;
  resp_msg = (retrofit_nv_read_resp_msg_v01 *)calloc(1,
                                            sizeof(retrofit_nv_read_resp_msg_v01));

  if (!resp_msg){
                   MSG_ERROR("qmi_nv_read_req_v01_handler alloc memory error", 0,0,0);
                    return QMI_CORE_SERVER_MEMORY_ERR;
   }
    memset(resp_msg,0,sizeof(retrofit_nv_read_resp_msg_v01));
    resp_msg->result = NV_DONE_S;

#if 0
   TLOG("qmi_nv_read_req_v01_handler========== item_id=%d", req_msg->item_id);
if(req_msg->item_id){
       if(req_msg->item_id<20000)                type=1;
       else if (req_msg->item_id>=20000)      type=0;

       resp_msg->result=nvio_read_rfnv_item(req_msg->item_id,resp_msg->nv_data,&resp_msg->nv_data_len,type);
}else{
        resp_msg->result= 22;
}
#endif

    /* Check if the file is present or not. */
    if (efs_stat(req_msg->efs_path, &stat) != 0) {
        TLOG("qmi_nv_read_req_v01_handler efs_stat failed, efs_errno=%d", efs_errno);
        resp_msg->result = NV_NOTACTIVE_S;
    } else {
        resp_msg->nv_data_len = stat.st_size;
        if (efs_get(req_msg->efs_path, resp_msg->nv_data, resp_msg->nv_data_len) < 0) {
            TLOG("qmi_nv_read_req_v01_handler efs_get %s failed, efs_errno=%d", req_msg->efs_path, efs_errno);
            resp_msg->result = NV_FAIL_S;
        }
    }

     rc = qmi_core_server_send_resp(req_handle,
                                   QMI_RETROFIT_NV_READ_RESP_V01,
                                   resp_msg,
                                   sizeof(retrofit_nv_read_resp_msg_v01));
     free(resp_msg);
     return rc;
}

void nv_diag_read(nvdiag_req_msg_v01 *req_msg, nvdiag_resp_msg_v01 *resp_msg)
{
    resp_msg->item = req_msg->item;
    memscpy((void *) ( resp_msg->value ),
            QMI_NV_DIAG_ITEM_SIZE_V01,
            (void *) ( req_msg->value ),
            QMI_NV_DIAG_ITEM_SIZE_V01);
    resp_msg->item_size = req_msg->item_size;
    void *pItemData = (void*) &resp_msg->value[0];

	char file[QMI_NV_DIAG_FILE_PATH_V01] = {0,};

    if(req_msg->file[0] == 0){
        if(req_msg->item < NV_MAX_I){
            (void)snprintf(file, sizeof(file),"/nvm/num/%d", (int)req_msg->item);
        }else{
            (void)snprintf(file, sizeof(file),"/nv/item_files/rfnv/%d", (int)req_msg->item);
        }
    }
    else
    {
       memscpy((void*) ( file ),
           QMI_NV_DIAG_FILE_PATH_V01,
           (void*) ( req_msg->file ),
           QMI_NV_DIAG_FILE_PATH_V01
           );

    }

    memscpy((void*) ( resp_msg->file ),
           QMI_NV_DIAG_FILE_PATH_V01,
           (void*) (file ),
           QMI_NV_DIAG_FILE_PATH_V01
           );

	if(strstr(file, "/sfs/"))
	{
		int fd = sfs_open(file,O_RDONLY);
	    if(0 == fd){
	        TLOG("qmi_nv_diag_req_v01_handler sfs_open failed ###############$$$$$$$$$$$$$$$$$$$\n");
			resp_msg->result = 1;
			return;
	    }

		uint32 filesize = 0;
		sfs_getSize(fd, &filesize);
	    int return_size = sfs_read(fd, pItemData, filesize);
		TLOG("qmi_nv_diag_req_v01_handler sfs_read size is %d, need size is %d ###############$$$$$$$$$$$$$$$$$$$\n", return_size, filesize);
	    resp_msg->result = (return_size ==  filesize) ? 0 : 1;
		resp_msg->item_size = (uint16_t)filesize;

		sfs_close(fd);
	}
	else //efs
	{
		struct fs_stat temp_buf;
		if(efs_stat(file,&temp_buf) == -1) {
			TLOG("qmi_nv_diag_req_v01_handler efs_stat file %s failed ###############$$$$$$$$$$$$$$$$$$$\n", file);
			TLOG("error no is %d ###############$$$$$$$$$$$$$$$$$$$\n", efs_errno);
			resp_msg->result = 2;
			return;
		}

	    if(req_msg->item_size == 0){
	        req_msg->item_size = temp_buf.st_size;
	        resp_msg->item_size = temp_buf.st_size;
	    }

	    int fd = efs_open(file,O_RDONLY);
	    if(fd < 0){
			TLOG("qmi_nv_diag_req_v01_handler efs_open failed ###############$$$$$$$$$$$$$$$$$$$\n");
			efs_close(fd);
			resp_msg->result = 3;
			return;
	    }

	    int return_size = efs_read(fd, pItemData, req_msg->item_size);
		TLOG("qmi_nv_diag_req_v01_handler efs_read size is %d, need size is %d ###############$$$$$$$$$$$$$$$$$$$\n", return_size, req_msg->item_size);
	    resp_msg->result = (return_size == req_msg->item_size) ? 0 : 1;
		efs_close(fd);
	}
}

void nv_diag_write(nvdiag_req_msg_v01 *req_msg, nvdiag_resp_msg_v01 *resp_msg)
{
	void *pItemData = (void*) &req_msg->value[0];
	char	file[QMI_NV_DIAG_FILE_PATH_V01] = {0,};

	int fd;

	resp_msg->item = req_msg->item;

	memscpy((void*) ( file ),
		   QMI_NV_DIAG_FILE_PATH_V01,
		   (void*) ( req_msg->file ),
		   QMI_NV_DIAG_FILE_PATH_V01
		   );

	memscpy((void *) ( resp_msg->value),
			 QMI_NV_DIAG_ITEM_SIZE_V01,
			(void *) ( req_msg->value),
			QMI_NV_DIAG_ITEM_SIZE_V01);
	resp_msg->item_size = req_msg->item_size;

	if(req_msg->file[0] == 0){
	   if(req_msg->item < NV_MAX_I){
		   (void)snprintf(file, sizeof(file),"/nvm/num/%d", (int)req_msg->item);
	   }else{
		   (void)snprintf(file, sizeof(file), "/nv/item_files/rfnv/%d", (int)req_msg->item);
	   }
	}else{
		  memscpy((void*) ( file ),
		   QMI_NV_DIAG_FILE_PATH_V01,
		   (void*) ( req_msg->file ),
		   QMI_NV_DIAG_FILE_PATH_V01
		   );
	}

	memscpy((void*) ( resp_msg->file ),
		   QMI_NV_DIAG_FILE_PATH_V01,
		   (void*) ( file ),
		   QMI_NV_DIAG_FILE_PATH_V01
		   );

	resp_msg->result = 1;
	/* if the file already exists remove it */
	int  sfs_status = 0;
    fd = sfs_open(file, O_RDONLY);
	if(fd)
	{
		(void)sfs_close(fd);
		sfs_status = sfs_rm(file);
		if(sfs_status != E_SUCCESS)
		{
			TLOG("nv_diag_write error 11111111111 ###############$$$$$$$$$$$$$$$$$$$\n");
			return;
	    }
    }

	fd = sfs_open(file, O_RDWR|O_CREAT);
    if(!fd)
    {
		TLOG("nv_diag_write error 222222222222 ###############$$$$$$$$$$$$$$$$$$$\n");
		return;
    }

	sfs_status = sfs_write(fd, (char *)pItemData, (int32)req_msg->item_size);
	resp_msg->result = (sfs_status == req_msg->item_size) ? 0 : 1;
    sfs_status = sfs_close(fd);
}

qmi_core_server_error_type
qmi_nv_diag_req_v01_handler
(
    void               *server_data,
    void               *conn_obj,
    qmi_req_handle     req_handle,
    uint32_t           msg_id,
    void               *req_c_struct,
    uint32_t           req_c_struct_len
)
{
	MSG_ERROR("qmi_nv_diag_req_v01_handler Enter #####################\n", 0,0,0);
	qmi_core_server_error_type rc = QMI_CORE_SERVER_INTERNAL_ERR;
	nvdiag_req_msg_v01 *req_msg;
	nvdiag_resp_msg_v01 *resp_msg;

	req_msg = (nvdiag_req_msg_v01 *)req_c_struct;
	resp_msg = (nvdiag_resp_msg_v01 *)calloc(1, sizeof(nvdiag_resp_msg_v01));

	if(!resp_msg)
	{
		MSG_ERROR("qmi_nv_diag_req_v01_handler alloc memory error", 0,0,0);
		return QMI_CORE_SERVER_MEMORY_ERR;
	}

    memset(resp_msg,0,sizeof(nvdiag_resp_msg_v01));
    resp_msg->result = NV_DONE_S;

    if(req_msg->operation == 0)//read
    {
		nv_diag_read(req_msg, resp_msg);
	}
	else if(req_msg->operation == 1)
	{
		nv_diag_write(req_msg, resp_msg);
	}
	else
	{
		//ASSERT(0);
	}

	//send result
	rc = qmi_core_server_send_resp(req_handle,
	                           QMI_NV_DIAG_RESP_V01,
	                           resp_msg,
	                           sizeof(nvdiag_resp_msg_v01));
	free(resp_msg);
	return rc;
}

qmi_core_server_error_type
	qmi_getlist_req_v01_handler

(
    void               *server_data,
    void               *conn_obj,
    qmi_req_handle     req_handle,
    uint32_t           msg_id,
    void               *req_c_struct,
    uint32_t           req_c_struct_len
)
{
	TLOG("qmi_getlist_req_v01_handler Enter #####################\n", 0,0,0);
	qmi_core_server_error_type rc = QMI_CORE_SERVER_INTERNAL_ERR;
	getlist_req_msg_v01 *req_msg;
	getlist_resp_msg_v01 *resp_msg;

	req_msg = (getlist_req_msg_v01 *)req_c_struct;
	resp_msg = (getlist_resp_msg_v01 *)calloc(1, sizeof(getlist_resp_msg_v01));

	if(!resp_msg)
	{
		TLOG("qmi_getlist_req_v01_handler alloc memory error", 0,0,0);
		return QMI_CORE_SERVER_MEMORY_ERR;
	}

    memset(resp_msg,0,sizeof(getlist_resp_msg_v01));
    resp_msg->result = NV_FAIL_S;

    sfs_file_info_list file_list;
	memset(&file_list, 0x0, sizeof(file_list));

	int i = 0;
	char files[1025] = {0,};

	char path[1024] = {0,};
	(void)snprintf(path, sizeof(path),"%s", req_msg->path);
	int status = sfs_get_filelist((const char*)path, &file_list);

	if(!status) {
		resp_msg->num = file_list.file_num;
		resp_msg->result = NV_DONE_S;
		for(i = 0; i < file_list.file_num; i++) {
			strcat(files, file_list.file_info[i].file_name);
			strcat(files, "--end");
		}

		strncpy((char*)resp_msg->fileList, (const char*)files, 1024);
		sfs_clean_filelist(&file_list);
	}

	//send result
	rc = qmi_core_server_send_resp(req_handle,
	                           QMI_GETLIST_RESP_V01,
	                           resp_msg,
	                           sizeof(getlist_resp_msg_v01));
	free(resp_msg);
	return rc;


}


/*
 * Dispatch Table for tct server
 */
qmi_msg_handler_type tct_server_dispatcher[] =
{
    {QMI_PING_REQ_V01, (qmi_dispatch_fn_type)qmi_tct_ping_req_v01_handler},
    {QMI_RETROFIT_NV_READ_REQ_V01, (qmi_dispatch_fn_type)qmi_nv_read_req_v01_handler},
    {QMI_NV_DIAG_REQ_V01, (qmi_dispatch_fn_type)qmi_nv_diag_req_v01_handler},
    {QMI_GETLIST_REQ_V01, (qmi_dispatch_fn_type)qmi_getlist_req_v01_handler},
};


tct_server_class_type *tct_server_new(char *object_name, uint32_t instance_id,
        qmi_core_server_error_type *err)
{
    tct_server_class_type *tct_server_object;
    /* The default priority of one greater than DIAG's task priority is
       considered if NULL is passed to the API */
    uint32_t priority = 14;
    uint32_t index;
    unsigned long sig = QMI_TCT_SVC_WAIT_SIG;

    TLOG("tct_server_new.............. ");

    tct_server_object = ALLOCATOR(sizeof(tct_server_class_type));

    if (tct_server_object == NULL) {
        *err = QMI_CORE_SERVER_MEMORY_ERR;
        return NULL;
    }

    /* Construct and Initialize the core server object */
    *err = qmi_core_server_new(&(tct_server_object->core_object),
                               object_name,
                               instance_id, /* Instance id */
                               1, /* Task flag */
                               (void *)tct_server_event_loop,
                               (void *)&priority,
                               NULL,
                               (void *)&sig,
                               NULL,
                               tct_server_dispatcher,
                               sizeof(tct_server_dispatcher)/sizeof(qmi_msg_handler_type));

    if (*err == QMI_CORE_SERVER_NO_ERR) {
        /* Initialize the client data */
        for (index = 0; index < MAX_NUM_CLIENTS; index++ ) {
            memset(&(tct_server_object->client_data[index].conn_obj), 0,
                    sizeof(qmi_core_conn_obj_type));
            tct_server_object->client_data[index].active_flag = 0;
        }
        return tct_server_object;
    } else {
        DEALLOCATE(tct_server_object);
        return NULL;
    }
}

uint32_t tct_server_get_client_index(tct_server_class_type *me)
{
    uint32_t client_index;

    for (client_index = 0; client_index < MAX_NUM_CLIENTS; client_index++) {
        if (me->client_data[client_index].active_flag == 0) {
            break;
        }
    }

    return client_index;
}

qmi_csi_cb_error tct_server_connect(qmi_client_handle client_handle,
        tct_server_class_type *me, void **connection_handle)
{
    qmi_core_server_error_type rc;
    uint32_t client_index;

    TLOG("tct_server_connect +++++++++++++++++++");

    rc = qmi_core_server_check_valid_object(me);
    if (rc != QMI_CORE_SERVER_NO_ERR) {
        TLOG("tct_server_connect qmi_core_server_check_valid_object fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    }

    if ((client_index = tct_server_get_client_index(me)) == MAX_NUM_CLIENTS) {
        TLOG("tct_server_connect tct_server_get_client_index");
        return QMI_CSI_CB_CONN_REFUSED;
    }
    TLOG("tct_server_connect......client_index(%d)", client_index);

    /* Register the client */
    rc = qmi_core_server_register_client(&(me->client_data[client_index].conn_obj),
                                         &(me->core_object),
                                         client_handle,
                                         0, /* Number of indication */
                                         &(me->client_data[client_index]));


    if (rc != QMI_CORE_SERVER_NO_ERR) {
        TLOG("tct_server_connect qmi_core_server_register_client fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    } else {
        /* Set this client as active client */
        TLOG("tct_server_connect me->client_data[client_index].active_flag = 1");
        me->client_data[client_index].active_flag = 1;
    }

    /* Assign connection handle to the address of the connection object */
    *connection_handle = &(me->client_data[client_index].conn_obj);

    TLOG("tct_server_connect -------------------");

    return QMI_CSI_CB_NO_ERR;
}

qmi_csi_cb_error tct_server_process_req(void *connection_handle,
        qmi_req_handle req_handle, int msg_id, void *req_c_struct,
        int req_c_struct_len, tct_server_class_type *me)
{
    qmi_core_server_error_type rc;
    qmi_core_conn_obj_type *conn_obj;

    TLOG("tct_server_process_req PROCESS REQUEST CALLBACK CALLED..........");

    rc = qmi_core_server_check_valid_object(me);
    if (rc != QMI_CORE_SERVER_NO_ERR || connection_handle == NULL) {
        TLOG("tct_server_process_req qmi_core_server_check_valid_object fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    }

    conn_obj = (qmi_core_conn_obj_type *)connection_handle;

    /* Response/Request handler functions will eventually get called based on msg_id
       received */

    rc = qmi_core_server_dispatch_msg(conn_obj,
                                      me,
                                      req_handle,
                                      msg_id,
                                      req_c_struct,
                                      req_c_struct_len);

    if (rc != QMI_CORE_SERVER_NO_ERR) {
        TLOG("tct_server_process_req qmi_core_server_dispatch_msg fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    } else {
        return QMI_CSI_CB_NO_ERR;
    }
}

qmi_csi_cb_error tct_server_disconnect(void *connection_handle,
        tct_server_class_type *me)
{
    qmi_core_server_error_type rc;
    qmi_core_conn_obj_type *conn_obj;
    tct_server_client_data_type *client_data;

    TLOG("tct_server_disconnect +++++++++++++++++++");

    rc = qmi_core_server_check_valid_object(me);
    if (rc != QMI_CORE_SERVER_NO_ERR || connection_handle == NULL) {
        TLOG("tct_server_disconnect qmi_core_server_check_valid_object fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    }

    conn_obj = (qmi_core_conn_obj_type *)connection_handle;

    rc = qmi_core_server_unregister_client(conn_obj);
    if (rc != QMI_CORE_SERVER_NO_ERR) {
        TLOG("tct_server_disconnect qmi_core_server_unregister_client fail");
        return QMI_CSI_CB_INTERNAL_ERR;
    } else {
        client_data = qmi_core_server_get_client_data((qmi_core_conn_obj_type *)conn_obj);
        if (client_data) {
            client_data->active_flag = 0;
            TLOG("tct_server_disconnect client_data->active_flag = 0");
        }
    }

    TLOG("tct_server_disconnect -------------------");

    return QMI_CSI_CB_NO_ERR;
}

qmi_core_server_error_type tct_server_register(tct_server_class_type *me)
{
    qmi_core_server_error_type rc;

    rc = qmi_core_server_check_valid_object(me);
    if (rc != QMI_CORE_SERVER_NO_ERR) {
        return rc;
    }

    /* Registering the service object and callbacks with QCSI framework
       using the core server object  */
    rc = qmi_core_server_register(me,
                                  tct_get_service_object_v01(),
                                  (qmi_csi_connect)tct_server_connect,
                                  (qmi_csi_disconnect)tct_server_disconnect,
                                  (qmi_csi_process_req)tct_server_process_req);

   return rc;
}

qmi_core_server_error_type tct_server_start_server(tct_server_class_type *me)
{
    qmi_core_server_error_type rc;

    TLOG("tct_server_start_server");

    rc = qmi_core_server_check_valid_object(me);
    if (rc != QMI_CORE_SERVER_NO_ERR) {
        return rc;
    }

    rc = qmi_core_server_start_server(me);

    return rc;
}

void tct_server_start(void)
{
    qmi_core_server_error_type rc;

#ifdef TCT_QMI_LOG_ENABLE
    rex_init_crit_sect(&_log_crit_sect);
#endif /* TCT_QMI_DEBUG */

    TLOG("tct_server_start");
    /* New server */
    tct_server_data.tct_server = tct_server_new("qmi_tct", 1, &rc);

    if (tct_server_data.tct_server != NULL) {
        tct_server_start_server(tct_server_data.tct_server);
    } else {
        TLOG("tct_server_new fail, rc(%d)", rc);
    }
}

