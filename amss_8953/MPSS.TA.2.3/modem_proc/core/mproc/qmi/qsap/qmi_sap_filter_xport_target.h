/******************************************************************************
  @file    qmi_sap_filter_target_ext.h
  @brief   The QMI Restricted Access Proxy (SAP) Filter target header file

  DESCRIPTION
  QMI Restricted Access Proxy Filter routines.  

  ---------------------------------------------------------------------------
  Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
#include "sio.h"

typedef struct qsap_filter_port_map_type
{
  sio_port_id_type filter_port;
  sio_port_id_type smd_port;
  void (*smd_to_bridge_cb)(dsm_item_type **item);
}qsap_filter_port_map_type;

#define QSAP_FILTER_PORT_MAP_ARRAY_SIZE (sizeof(qsap_filter_port_map)/sizeof(qsap_filter_port_map_type))
#define QSAP_FILTER_QMUX2_VERSION 3
#define QSAP_FILTER_QMUX2_PORT_INDEX 1

static qsap_filter_port_map_type qsap_filter_port_map[] = 
{
    /* Tethered1 - a2 */
  { SIO_PORT_QSAP_FILTER, SIO_PORT_SMD_DATA40, NULL},
  /* Tethered2 - a2 */
  { SIO_PORT_QSAP_FILTER_1, SIO_PORT_SMD_DATA39, NULL},
  /* Tethered3 - a2 */
  { SIO_PORT_QSAP_FILTER_2, SIO_PORT_SMD_DATA38, NULL},
  /* Tethered4 */
  { SIO_PORT_QSAP_FILTER_3, SIO_PORT_SMD_DATA37, NULL},
  /* Tethered5 */
  { SIO_PORT_QSAP_FILTER_4, SIO_PORT_SMD_DATA36, NULL},

  { SIO_PORT_QSAP_FILTER_5, SIO_PORT_SMD_DATA35, NULL},

  { SIO_PORT_QSAP_FILTER_6, SIO_PORT_SMD_DATA34, NULL},

  { SIO_PORT_QSAP_FILTER_7, SIO_PORT_SMD_DATA33, NULL},
  /* Currently no map to SMD Port */
  { SIO_PORT_QSAP_FILTER_8, SIO_PORT_QSAP_FILTER_8, NULL},
  /* Currently no map to SMD Port */
  { SIO_PORT_QSAP_FILTER_9, SIO_PORT_QSAP_FILTER_9, NULL},
  /* No mapping for QMUX2 */
  { SIO_PORT_QSAP_FILTER_QMUX2, SIO_PORT_QSAP_FILTER_QMUX2, NULL}
};
