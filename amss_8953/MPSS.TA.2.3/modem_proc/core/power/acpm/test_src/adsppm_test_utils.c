/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include "mmpm.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "adsppm_test.h"
#include "adsppm_test_param.h"
#include "adsppm_test_main.h"
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>

extern AdsppmTestGlbCtxType gAdsppmTestCtx;

__inline uint32 GetTestDebugLevel(void)
{
    return gAdsppmTestCtx.testLogLevel;
}
