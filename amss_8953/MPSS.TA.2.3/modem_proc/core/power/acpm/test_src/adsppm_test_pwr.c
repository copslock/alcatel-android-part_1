/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"



extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern const uint32 gReqVregParam[];
extern char *result[2];



/* Power_01 */
/* Test GDHS LPASS CORES */
AdsppmTestType testgdhs1 [] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
   /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id3 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id5 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id7 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id8 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
       /* Test Id9 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, DREG}
    },
};


#define POWER_01_NAME    "power_01"
#define POWER_01_DETAILS "Calls MMPM_Request and Release for lpass cores, verifies block powers up correctly."

/** 
  @ingroup Power 
  @test Power_01
  @brief Calls MMPM_Request and Release for lpass cores, verifies block powers up correctly.

*/

/* Power_01 */
MMPM_STATUS Test_Power_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", POWER_01_NAME, POWER_01_DETAILS);
  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testgdhs1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testgdhs1, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Power_01 - %s,%d\n",result[!sts], sts);


    return sts;
}

/* Power_02 */
/* Test GDHS for mutiple clients same GDHS. Only the last release should disable GDHS */
AdsppmTestType testgdhs2 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR}
    },
    /* Test Id2 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR}
    },
    /* Test Id3 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {RELPWR, DREG}
    },
    /* Test Id4 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {RELPWR, DREG}
    }
};


// These are used in the test case logging, and in the menu text.
#define POWER_02_NAME    "power_2"
#define POWER_02_DETAILS "Test GDHS for mutiple clients same GDHS. Only the last release should disable GDHS."

/** 
  @ingroup Power 
  @test Power_2
  @brief Test GDHS for mutiple clients same GDHS. Only the last release should disable GDHS.

*/

/* Power_4 */
MMPM_STATUS Test_Power_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", POWER_02_NAME, POWER_02_DETAILS);

  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testgdhs2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testgdhs2, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Power_2 - %s,%d\n",result[!sts], sts);


    return sts;
}


/* Power_03 */
/* Test GDHS for same client - request-release-release. This should fail in the second release */
AdsppmTestType testgdhs3 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQPWR, RELPWR, RELPWR, DREG}
    }
};


// These are used in the test case logging, and in the menu text.
#define POWER_03_NAME    "power_3"
#define POWER_03_DETAILS "Make multiple requests from one client then a single release.  Ensure power is off."

/** 
  @ingroup Power 
  @test Power_3
  @brief Make multiple requests from one client then a single release.  Ensure power is off.

	
*/

/* Power_5 */
MMPM_STATUS Test_Power_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", POWER_03_NAME, POWER_03_DETAILS);

 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testgdhs3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testgdhs3, numTest, testSts);

    // todo - Add better checking of the testSts to make sure only the expected failure occurred.

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Power_3 - %s,%d\n",result[!sts], sts);


    return sts;
}


/* Power_4 */
/* Test multiple clients request and only the last release should release GDHS. Need to check log to status */
MMPM_STATUS TestGdhsMultReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        sts = Test_RequestPwr(clientId1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_RequestPwr(clientId2);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Pwr Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st pwr request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_ReleasePwr(clientId1);
        if (MMPM_STATUS_SUCCESS != sts)
		  {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Release\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_ReleasePwr(clientId2);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Release \n");
                retSts = MMPM_STATUS_FAILED;
            }
         sts = Test_Deregister(clientId1);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Test_Deregister1\n");
			retSts = MMPM_STATUS_FAILED;
		}
        sts = Test_Deregister(clientId2);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Test_Deregister2\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}


// These are used in the test case logging, and in the menu text.
#define POWER_04_NAME    "power_4"
#define POWER_04_DETAILS "Calls MMPM_Request with multiple clients for the same owner ID, releases in random order, only last release should power down."

/** 
  @ingroup Power 
  @test Power_4
  @brief Calls MMPM_Request with multiple clients for the same owner ID, releases in random order, only last release should power down.

	
*/

/* Power_4 */
MMPM_STATUS Test_Power_04(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", POWER_04_NAME, POWER_04_DETAILS);
    sts = TestGdhsMultReqDiffClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Power_04 - %s,%d\n",result[!sts], sts);
    return sts;
}


void Test_Power(void) 
{

    /* GDHS tests */
    /* Power_01 */
    Test_Power_01();
#ifndef TEST_PROFILING
    /* Power_02 */
    Test_Power_02();

    /* Power_03 */
    Test_Power_03();

    /* Power_04 */
    Test_Power_04();
#endif

}
