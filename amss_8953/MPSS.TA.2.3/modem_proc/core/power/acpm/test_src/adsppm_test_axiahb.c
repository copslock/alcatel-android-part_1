/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"

extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern const uint32 gReqRegProgParam[];
extern const uint32 gAxiClkMap[MAX_CORE][MAX_AXI_CLK];
extern char *result[2];

/* RegisterProg_01 */
/* Request AHB clk */
AdsppmTestType testahb1 [] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id2 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id3 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id5 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id7 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id8 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
    /* Test Id9 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    },
};


#define REGPROG_01_NAME    "regprog_01"
#define REGPROG_01_DETAILS "Verify node call is done, check all AHB clocks. Verify slow/fast speed."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_01
  @brief Verify node call is done, check all AHB clocks. Verify slow/fast speed.

*/

/* RegisterProg_01 */
MMPM_STATUS Test_RegisterProg_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_01_NAME, REGPROG_01_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb1, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_01 - %s,%d\n",result[!sts], sts);
    return sts;
}



/* RegisterProg_02 */
/* Release AHB clk with out request. This should return failure */
AdsppmTestType testahb2 [] =
{
    /* Test Id1 */
    {1, 3, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, RELREGP, DREG}
    }
};


#define REGPROG_02_NAME    "regprog_02"
#define REGPROG_02_DETAILS "Verify calling release, without calling request, fails."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_02
  @brief Verify calling release, without calling request, fails.

*/

/* RegisterProg_11 */
MMPM_STATUS Test_RegisterProg_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_02_NAME, REGPROG_02_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb2, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_02 should fail- %s,%d\n",result[!sts], sts);

    return sts;
}


/* RegisterProg_03 */
/* RegProg test from same client - req, release, release. This should be a failure */
AdsppmTestType testahb3 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, RELREGP, DREG}
    }
};


#define REGPROG_03_NAME    "regprog_03"
#define REGPROG_03_DETAILS "Verify calling release multiple times fails after first release, should fail."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_03
  @brief Verify calling release multiple times fails after first release.

	
*/

/* RegisterProg_03 */
MMPM_STATUS Test_RegisterProg_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_03_NAME, REGPROG_03_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb3, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_03 should fail- %s,%d\n",result[!sts], sts);
    return sts;
}


/* RegisterProg_04 */
/* RegProg test from same client - req, release.This is a false test since coreid is invalid. This should be a failure */
AdsppmTestType testahb4 [] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_START, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP, RELREGP, DREG}
    }
};


#define REGPROG_04_NAME    "regprog_04"
#define REGPROG_04_DETAILS "This is a false test since coreid is invalid. This should be a failure."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_04
  @brief Ensure blocks with no AHB return error.
*/

/* RegisterProg_04 */
MMPM_STATUS Test_RegisterProg_04(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_04_NAME, REGPROG_04_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb4)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb4, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_04,should fail - %s,%d\n",result[!sts], sts);
    return sts;
}


/* RegisterProg_05 */
/* RegProg test from multiple client - req from client1, release from client2, the client2 release should fail */
AdsppmTestType testahb5 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP}
    },
    /* Test Id1 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, RELREGP}
    },
    /* Test Id1 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {DREG}
    },
    /* Test Id1 */
    {2, 1, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {DREG}
    }
};


#define REGPROG_05_NAME    "regprog_05"
#define REGPROG_05_DETAILS "With two clients, only one requests, the othe releases.  Should fail release."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_05
  @brief With two clients, only one requests, the othe releases.  Should fail release.

	
*/

/* RegisterProg_05 */
MMPM_STATUS Test_RegisterProg_05(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_05_NAME, REGPROG_05_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb5)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb5, numTest, testSts);
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_05, should fail- %s,%d\n",result[!sts], sts);
    return sts;
}



/* RegisterProg_06 */
/* Reg prog test from different client for same ahb clk. Req1-req2- release1 . This should not release AHB clk. 
This also verifies the last release , releases AHB clk */
MMPM_STATUS TestRegPMultiReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, match;

    coreId = MMPM_CORE_ID_LPASS_AVSYNC;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        match = gReqRegProgParam[index];
        sts = Test_RequestRegProg(clientId1, match);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_RequestRegProg(clientId2, match);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_ReleaseRegProg(clientId1);
                if (MMPM_STATUS_SUCCESS == sts)
                {
                    sts = Test_ReleaseRegProg(clientId2);
                    if (MMPM_STATUS_SUCCESS != sts)
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Release\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
                } 
                else
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Release\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            }
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId1);		
			
			if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 Test_Deregister1\n");
                retSts = MMPM_STATUS_FAILED;
            }
		
		sts = Test_Deregister(clientId2);
		if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_Deregister2\n");
                retSts = MMPM_STATUS_FAILED;
            }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}



#define REGPROG_06_NAME    "regprog_06"
#define REGPROG_06_DETAILS "Verify multiple clients of the same owner ID are handled.  Only last release should disable AHB."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_06
  @brief Verify multiple clients of the same owner ID are handled.  Only last release should disable AHB.

*/
/* RegisterProg_06 */
MMPM_STATUS Test_RegisterProg_06(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_06_NAME, REGPROG_06_DETAILS);
    sts = TestRegPMultiReqDiffClient();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_06 - %s,%d\n",result[!sts], sts);
    return sts;
}



/* RegisterProg_07 */
/* Test Release of register programming disables AHB clk after multiple requests from same client */
MMPM_STATUS TestRegPReleaseAHBClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId = 0, match;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        match = gReqRegProgParam[index];
        sts = Test_RequestRegProg(clientId, MMPM_REG_PROG_NORM);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_RequestRegProg(clientId, MMPM_REG_PROG_FAST);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_ReleaseRegProg(clientId);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Release\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } 
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
        {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
                    retSts = MMPM_STATUS_FAILED;
        }
		
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}


#define REGPROG_07_NAME    "regprog_07"
#define REGPROG_07_DETAILS "Verify a single release with multiple requests disables AHB."

/** 
  @ingroup RegisterProg 
  @test RegisterProg_01
  @brief Verify a single release with multiple requests disables AHB.


*/

/* RegisterProg_07 */
MMPM_STATUS Test_RegisterProg_07(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_07_NAME, REGPROG_07_DETAILS);
    sts = TestRegPReleaseAHBClk();
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_07 - %s,%d\n",result[!sts], sts);
    return sts;
}


/* RegisterProg_08 */
/* Test MMSS AHB clocks for request and release */
MMPM_STATUS TestRegPMMSSAHBClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, clientId = 0, i;

    for (i = MMPM_CORE_ID_LPASS_START; i < MMPM_CORE_ID_LPASS_END; i++)
    {
        
	coreId = i;
	instanceId = MMPM_CORE_INSTANCE_0;

		clientId = Test_Register(coreId, instanceId);
		if (clientId)
		{
			sts = Test_RequestRegProg(clientId, MMPM_REG_PROG_NORM);
			if (MMPM_STATUS_SUCCESS != sts)
			{
			   ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in AHB request-MMPM_REG_PROG_NORM , coreId  %ld\n", coreId);
			   retSts = MMPM_STATUS_FAILED;
					
			}
			else
			{
			   sts = Test_ReleaseRegProg(clientId);
				if (MMPM_STATUS_SUCCESS != sts)
				{
							ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in AHB release , coreId  %ld\n", coreId);
							retSts = MMPM_STATUS_FAILED;
				}
			}
		}
	else
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
			retSts = MMPM_STATUS_FAILED;
		}
	sts = Test_Deregister(clientId);
	if (MMPM_STATUS_SUCCESS != sts)
			{
			   ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
			   retSts = MMPM_STATUS_FAILED;
					
			}
            
    }

    return retSts;
}


#define REGPROG_08_NAME    "regprog_08"
#define REGPROG_08_DETAILS "Test MMSS AHB clocks for request and release.\n"

/** 
  @ingroup RegisterProg 
  @test RegisterProg_08
  @brief Test MMSS AHB clocks for request and release.

  Uses TestRegPMMSSAHBClk() to invoke the test.
  This will call:
    - MMPM_Register_Ext_Ext
    - MMPM_Request - using MMPM_RSC_ID_REG_PROG resource ID
    - MMPM_Request - using MMPM_RSC_ID_REG_PROG resource ID
    - MMPM_GetInfo - using MMPM_INFO_ID_CLK_FREQ info ID  with forceMeasure set to 1
    - MMPM_Deregister_Ext_Ext

  The following clocks will be checked:  All MMSS AHB clocks

  A pass condition is when then MMPM returns success for all calls,
  the clock freq is correct for request(non-zero) and release (0).

  @param none

  @return MMPM_STATUS_SUCCESS Pass
  @return All other MMPM status Fail due to MMPM error
  @return -1 Fail due to incorrect clock frequency during request and release
  
*/

/* RegisterProg_08 */
MMPM_STATUS Test_RegisterProg_08(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_08_NAME, REGPROG_08_DETAILS);
    sts = TestRegPMMSSAHBClk();
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_08 - %s,%d\n",result[!sts], sts);
    return sts;
}



/* RegisterProg_09 */
/* same client, request AHB clock first then request bw, RELBW, AHB clock should not be turned off */
AdsppmTestType testahb9 [] =
{
 
	/* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQREGP}
    },
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },
    /* Test Id1 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELBW}
    },
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELREGP, DREG}
    }
};


#define REGPROG_09_NAME    "regprogBW_09"
#define REGPROG_09_DETAILS "request AHB clock first then request bw, RELBW, AHB clock should not be turned off\n."

/** 
  @ingroup RegisterProg and bw request to verify AHB clock enable/disable
  @test RegisterProg_09
  @brief Verify calling release, without calling request, fails.

*/

/* RegisterProg_11 */
MMPM_STATUS Test_RegisterProg_09(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", REGPROG_09_NAME, REGPROG_09_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testahb9)/sizeof(AdsppmTestType);
    sts = InvokeTest(testahb9, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "RegisterProg_09 - %s,%d\n",result[!sts], sts);

    return sts;
}



void Test_AxiAhb(void) 
{

    /* Register Programming tests */
    /* RegisterProg_01 */
    Test_RegisterProg_01();

#ifndef TEST_PROFILING
    /* RegisterProg_02 */
    Test_RegisterProg_02();

    /* RegisterProg_03 */
    Test_RegisterProg_03();

    /* RegisterProg_04 */
    Test_RegisterProg_04();

    /* RegisterProg_05 */
    Test_RegisterProg_05();

    /* RegisterProg_06 */
    Test_RegisterProg_06();

    /* RegisterProg_07 */
    Test_RegisterProg_07();

    /* RegisterProg_08 */
    Test_RegisterProg_08();

	/* RegisterProg_09 */
    Test_RegisterProg_09();
#endif
}

