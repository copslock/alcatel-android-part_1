/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"



extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern char *result[2];



/* MemPower_01 */
/* Test MEMPWR LPASS CORES */
AdsppmTestType testmempwr1 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR, RELMEMPWR, DREG}
    },
   /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id3 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
        {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id5 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
        {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id7 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
        {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id8 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
       /* Test Id9 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
        {REG, REQMEMPWR, RELMEMPWR, DREG}
    },
};


#define MEMPOWER_01_NAME    "mempower_01"
#define MEMPOWER_01_DETAILS "Calls MMPM_Setlog, Request and Release for lpass cores, verifies block mem powers up correctly."

/** 
  @ingroup Mem Power 
  @test MemPower_01
  @brief Calls MMPM_Setlog, Request and Release for lpass cores, verifies block powers up correctly.

*/

/* Power_01 */
MMPM_STATUS Test_MemPower_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", MEMPOWER_01_NAME, MEMPOWER_01_DETAILS);
  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmempwr1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmempwr1, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT, "MemPower_01 - %s,%d\n",result[!sts], sts);

    return sts;
}

/* Power_02 */
/* Test mempwr for mutiple clients same core. Only the last release should disable mempwr */
AdsppmTestType testmempwr2 [] =
{
    /* Test Id1 */
    {1, 3, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR}
    },
    /* Test Id2 */
    {2, 3, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR}
    },
    /* Test Id3 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {RELMEMPWR, DREG}
    },
    /* Test Id4 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {RELMEMPWR, DREG}
    }
};


// These are used in the test case logging, and in the menu text.
#define MEMPOWER_02_NAME    "mempower_2"
#define MEMPOWER_02_DETAILS "Test MEMPWR for mutiple clients same core. Only the last release should disable mempwr."

/** 
  @ingroup Power 
  @test Power_2
  @brief Test GDHS for mutiple clients same GDHS. Only the last release should disable GDHS.

*/

/* Power_4 */
MMPM_STATUS Test_MemPower_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", MEMPOWER_02_NAME, MEMPOWER_02_DETAILS);
  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmempwr2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmempwr2, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,"MemPower_2 - %s,%d\n",result[!sts], sts);


    return sts;
}


/* Power_03 */
/* Test mempwr for same client - request-release-release. This should fail in the second release */
AdsppmTestType testmempwr3 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, SETPARAM, REQMEMPWR, RELMEMPWR, RELMEMPWR, DREG}
    }
};


// These are used in the test case logging, and in the menu text.
#define MEMPOWER_03_NAME    "mempower_3"
#define MEMPOWER_03_DETAILS "Make one requests from one client the release twice, should fail the test case."

/** 
  @ingroup Power 
  @test MemPower_3
  @brief Make one requests from one client the release twice, should fail the test case.

	
*/

/* MemPower_3*/
MMPM_STATUS Test_MemPower_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,"Running %s test: %s\n", MEMPOWER_03_NAME, MEMPOWER_03_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmempwr3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmempwr3, numTest, testSts);

    // todo - Add better checking of the testSts to make sure only the expected failure occurred.

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT, "MemPower_3,should fail - %s,%d\n",result[!sts], sts);

    return sts;
}



void Test_MemPower(void) 
{

     /* Power_01 */
    Test_MemPower_01();
	

#ifndef TEST_PROFILING
    /* Power_02 */
    Test_MemPower_02();

    /* Power_03 */
    Test_MemPower_03();
#endif 

}
