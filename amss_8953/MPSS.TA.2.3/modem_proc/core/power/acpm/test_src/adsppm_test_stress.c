/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"

#define STRESS_TEST_LOOP    60
#define MAX_MMPM_CLIENTS    64

extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern const uint32 gReqVregParam[];
extern const Mmpm2FreqPlanTestParamTableType gFreqPlanParam[];
extern const Mmpm2ReqBwTestParamTableType gReqBwParam[];
extern const uint32 gReqRegProgParam[];
extern const uint32 gReqSleepLatencyParam[];
extern const uint32 gReqMipsParam[];
extern char *result[2];

/* Stress_01 */
/* Stress test register - deregister */
MMPM_STATUS TestStressRegDereg(void)
{
    MMPM_STATUS   sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 i, j, coreId = MMPM_CORE_ID_LPASS_AIF, instanceId = MMPM_CORE_INSTANCE_0, clientId[MAX_TEST_CLIENT], setFail = 0;

    memset(clientId, 0,  sizeof(clientId));
    for (i = 0; i < 2; i++)
    {
        for(j = 0; j < STRESS_TEST_LOOP; j++)
        {

            clientId[j] = Test_Register(coreId, instanceId);
            if ((!clientId[j]) && (j < MAX_MMPM_CLIENTS) && !setFail)
            {
                ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "Test_LoopRegister: Error in register failed at loop %ld\n", j);
                retSts = MMPM_STATUS_FAILED;
            }
        }

        for(j = 0; j < STRESS_TEST_LOOP; j++)
        {

        	if (clientId[j])
            {
                sts = Test_Deregister(clientId[j]);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "Test_LoopRegister: Error in Deregister failed at loop %ld\n", j);
                    retSts = MMPM_STATUS_FAILED;
                }
            }
        }
    }
    return retSts;
}

#define STRESS_01_NAME    "stress_01"
#define STRESS_01_DETAILS "Calls MMPM_Register_Ext_Ext and MMPM_Deregister_Ext_Ext in a loop , randomize release pattern, dregister all at the end, check for memory leak."

/** 
  @ingroup Stress 
  @test stress_01
  @brief Calls MMPM_Register_Ext_Ext and MMPM_Deregister_Ext_Ext in a loop , randomize release pattern, dregister all at the end, check for memory leak.

	
*/

/* Stress_01_System_05 */
MMPM_STATUS Test_Stress_01_System(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_01_NAME, STRESS_01_DETAILS);


    sts = TestStressRegDereg();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "System_05_Stress_01 - %s,%d\n",result[!sts], sts);


    return sts;
}


/* Stress_02 */
/* Test multiple clients request and check the hightest clk is set */
MMPM_STATUS TestStressClkMultReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, i;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {

            clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
            sts = Test_RequestClk(clientId1, &clk, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                clk.freq = gFreqPlanParam[index].clkParam[0].freq[1] * 1000;
                sts = Test_RequestClk(clientId2, &clk, 1);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Test_Request\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st test request\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseClk(clientId1, &clk.clkId, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_ReleaseClk(clientId2, &clk.clkId, 1);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Release clk\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            }else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Release clk\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
         sts = Test_Deregister(clientId1);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister1\n");
			retSts = MMPM_STATUS_FAILED;
		}
        sts = Test_Deregister(clientId2);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister2\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}

#define STRESS_02_NAME    "stress_02"
#define STRESS_02_DETAILS "Stress out clock calls with multiple clients setting/clearing clocks multiple times, large loop.  Verify final freq. and memory leaks."

/** 
  @ingroup Stress 
  @test stress_02
  @brief Stress out clock calls with multiple clients setting/clearing clocks multiple times, large loop.  Verify final freq. and memory leaks.


*/

/* Clock_01 */
MMPM_STATUS Test_Stress_02_Clock(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_02_NAME, STRESS_02_DETAILS);


    sts = TestStressClkMultReqDiffClient();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_05_Stress_02 - %s,%d\n",result[!sts], sts);


    return sts;
}


/* Stress_03 */
/* For multiple clients BW stress test - Req1- req2 - release1 - rel 2 -ahb clk should be released */
MMPM_STATUS TestStressBwMultiClientTest2(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw, bw2;
    uint32 coreId, instanceId, index, clientId1 = 100, clientId2 = 0, i;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {

            bw.masterPort = gReqBwParam[index].bw[0].masterPort;
            bw.slavePort = gReqBwParam[index].bw[0].slavePort;
            bw.bwVal = gReqBwParam[index].bw[0].bwVal;
            bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
            bw.usageType = gReqBwParam[index].bw[0].usageType;

            sts = Test_RequestBw(clientId1, &bw, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                //TODO verify AHB clocks
                
                bw2.masterPort = gReqBwParam[index].bw[1].masterPort;
				bw2.slavePort = gReqBwParam[index].bw[1].slavePort;
				bw2.bwVal = gReqBwParam[index].bw[1].bwVal;
				bw2.usagePercent = gReqBwParam[index].bw[1].usagePercent;
				bw2.usageType = gReqBwParam[index].bw[1].usageType;

                sts = Test_RequestBw(clientId2, &bw2, 1);
                if (MMPM_STATUS_SUCCESS == sts)
                {
                    //TODO verify AHB clocks

                    sts = Test_ReleaseBw(clientId1);
                    if(MMPM_STATUS_SUCCESS == sts)
                    {
                        //TODO verify AHB clocks
                        sts = Test_ReleaseBw(clientId2);
                        if(MMPM_STATUS_SUCCESS != sts)
                        {
                           //TODO verify AHB clocks
                            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd BW release\n");
                            retSts = MMPM_STATUS_FAILED;
                        }
                    } else
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2st BW release\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
                } else
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2st BW request\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}

#define STRESS_03_NAME    "stress_03"
#define STRESS_03_DETAILS "Stress out BW calls with multiple clients & requests.  Over allocate fabric handles & release, check for memory leaks."

/** 
  @ingroup Stress 
  @test stress_03
  @brief Stress out BW calls with multiple clients & requests.  Over allocate fabric handles & release, check for memory leaks.

	
*/

/* Stress_03 */
MMPM_STATUS Test_Stress_03_Bandwidth(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_03_NAME, STRESS_03_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_03_NAME, STRESS_TEST_LOOP);

    sts = TestStressBwMultiClientTest2();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_05_Stress_03 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_04 */
/* Stress Test multiple clients gdhs request and only the last release should release GDHS. Need to verify logs for gdhs status */
MMPM_STATUS TestStressGdhsMultReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, i;

    coreId = MMPM_CORE_ID_LPASS_DML;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);

            sts = Test_RequestPwr(clientId1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_RequestPwr(clientId2);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Pwr Request\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st pwr request\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleasePwr(clientId1);
            if ((MMPM_STATUS_SUCCESS == sts) || (MMPM_STATUS_RESOURCEINUSE == sts))
            {
                sts = Test_ReleasePwr(clientId2);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Release \n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}

#define STRESS_04_NAME    "stress_04"
#define STRESS_04_DETAILS "Calls request and release in a large loop, check for memory leaks & power state."

/** 
  @ingroup Stress 
  @test stress_04
  @brief Calls request and release in a large loop, check for memory leaks & power state.

	
*/

/* Stress_04 */
MMPM_STATUS Test_Stress_04_Power(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_04_NAME, STRESS_04_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_04_NAME, STRESS_TEST_LOOP);

    sts = TestStressGdhsMultReqDiffClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Power_11_Stress_04 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_05 */
/* Stress Test multiple clients request and release Vreg. After release of last clients the Vreg should be disabled
Need to verify log and code to check the Vreg status */
MMPM_STATUS TestStressVregMultReqDiffClient(void)
{
    MMPM_STATUS retSts = MMPM_STATUS_FAILED;
    /*MMPM_STATUS sts = MMPM_STATUS_FAILED; 
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, i, milliVol = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];
    milliVol = gReqVregParam[index];

    if (clientId1 && clientId2)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);

        	   sts = Test_RequestVreg(clientId1, milliVol);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 1st Vreg Request\n");
                retSts = MMPM_STATUS_FAILED;
            } 
            sts = Test_RequestVreg(clientId2, milliVol);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 2nd Vreg Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseVreg(clientId1);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 1st Vreg client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseVreg(clientId2);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 2nd Vreg client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        AdsppmTestDebugLog( "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 */
    return retSts;
}

#define STRESS_05_NAME    "stress_05"
#define STRESS_05_DETAILS "Mutiple enable/disable VREG - ensure VREG is enabled each time."

/** 
  @ingroup Stress 
  @test stress_05
  @brief Mutiple enable/disable VREG - ensure VREG is enabled each time.

	
*/

/* Stress_05 */
MMPM_STATUS Test_Stress_05_PmicVreg(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_05_NAME, STRESS_05_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_05_NAME, STRESS_TEST_LOOP);

    sts = TestStressVregMultReqDiffClient();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "PMICVreg_08_Stress_05 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_06 */
/* Stress Test same client reqeust- release. Each request should enable Vreg and release should disable Vreg
Need to verify log and code to check the Vreg status */
MMPM_STATUS TestStressVregSingleClient(void)
{
    MMPM_STATUS retSts = MMPM_STATUS_FAILED;
  /*  MMPM_STATUS sts = MMPM_STATUS_FAILED;
    uint32 coreId, instanceId, index, clientId = 0, i, milliVol = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];
    milliVol = gReqVregParam[index];
    if (clientId)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);

        	sts = Test_RequestVreg(clientId, milliVol);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 1st Vreg Request\n");
                retSts = MMPM_STATUS_FAILED;
            } 
            sts = Test_ReleaseVreg(clientId);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                AdsppmTestDebugLog( "Error in 1st Vreg client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId);
    } else
    {
        AdsppmTestDebugLog( "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 */
    return retSts;
}


#define STRESS_06_NAME    "stress_06"
#define STRESS_06_DETAILS "Stress out VREG calls with multiple clients setting/clearing VREG multiple times, large loop.  Verify enabled and disabled and memory leaks."

/** 
  @ingroup Stress 
  @test stress_06
  @brief Stress out VREG calls with multiple clients setting/clearing VREG multiple times, large loop.  Verify enabled and disabled and memory leaks.

	
*/

/* Stress_06 */
MMPM_STATUS Test_Stress_06_PmicVreg(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_06_NAME, STRESS_06_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_06_NAME, STRESS_TEST_LOOP);

    sts = TestStressVregSingleClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "PMICVreg_05_Stress_06 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_07 */
/* Reg prog stress test from different client for same ahb clk. Req1-req2- rel1-rel2 .Verify clks */
MMPM_STATUS TestStressRegPMultiReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, match, i;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        match = gReqRegProgParam[index];
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);

        	sts = Test_RequestRegProg(clientId1, match);
            if (MMPM_STATUS_SUCCESS == sts)
            {
               //TODO : verify AHB clock
                sts = Test_RequestRegProg(clientId2, match);
                if (MMPM_STATUS_SUCCESS == sts)
                {
                    //TODO verify AHB clock
                    sts = Test_ReleaseRegProg(clientId1);
                    if (MMPM_STATUS_SUCCESS == sts)
                    {
                       //TODO verify AHB clocks
                        sts = Test_ReleaseRegProg(clientId2);
                        if (MMPM_STATUS_SUCCESS != sts)
                        {
                            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Release\n");
                            retSts = MMPM_STATUS_FAILED;
                        }
                    } 
                    else
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Release\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
                }
                else
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Request\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define STRESS_07_NAME    "stress_07"
#define STRESS_07_DETAILS "Stress out AHB calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks."

/** 
  @ingroup Stress 
  @test stress_07
  @brief Stress out AHB calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks.

	
*/

/* Stress_07 */
MMPM_STATUS Test_Stress_07_RegisterProg(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_07_NAME, STRESS_07_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_07_NAME, STRESS_TEST_LOOP);

    sts = TestStressRegPMultiReqDiffClient();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "RegisterProg_05_Stress_07 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_08 */
/* Reg prog stress test from same client for same ahb clk. Req1-release .Verify clks */
MMPM_STATUS TestStressRegPMultiReqSingleClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId = 0, match, i;

    coreId = MMPM_CORE_ID_LPASS_LPM;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        match = gReqRegProgParam[index];
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);

        	sts = Test_RequestRegProg(clientId, match);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                //TODO: verify AHB clocks
                sts = Test_ReleaseRegProg(clientId);
                if (MMPM_STATUS_SUCCESS != sts)
                {
                   //TODO: verify AHB clocks
                     ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Release\n");
                     retSts = MMPM_STATUS_FAILED;
                 }
            } else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}

#define STRESS_08_NAME    "stress_08"
#define STRESS_08_DETAILS "Mutiple enable/disable AHB - ensure AHB is enabled each time."

/** 
  @ingroup Stress 
  @test stress_08
  @brief Mutiple enable/disable AHB - ensure AHB is enabled each time.

	
*/

/* Stress_08 */
MMPM_STATUS Test_Stress_08_RegisterProg(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_08_NAME, STRESS_08_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_08_NAME, STRESS_TEST_LOOP);

    sts = TestStressRegPMultiReqSingleClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "RegisterProg_08_Stress_08 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* Stress_09 */
/* Stress Test multiple clients request and release MIPS. Need to verify log and code to check the Mips status */
MMPM_STATUS TestStressMipsMultReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, i, mips = 0;

    coreId = MMPM_CORE_ID_LPASS_AVSYNC;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];
    mips = gReqMipsParam[index];

    if (clientId1 && clientId2)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);
            
            sts = Test_RequestMips(clientId1, mips);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Mips Request\n");
                retSts = MMPM_STATUS_FAILED;
            } 
            sts = Test_RequestMips(clientId2, mips);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Mips Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseMips(clientId1);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Mips client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseMips(clientId2);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Mips client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}

#define STRESS_09_NAME    "stress_09"
#define STRESS_09_DETAILS "Stress out MIPS calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks."

/** 
  @ingroup Stress 
  @test stress_09
  @brief Stress out MIPS calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks.

	
*/

/* Stress_09 */
MMPM_STATUS Test_Stress_09_Mips(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_09_NAME, STRESS_09_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_09_NAME, STRESS_TEST_LOOP);

    sts = TestStressMipsMultReqDiffClient();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "MIPS_04_Stress_09 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);
    return sts;
}


/* Stress_10 */
/* Stress Test multiple clients request and release sleep Latency. Need to verify log and code to check the sleep latency status */
MMPM_STATUS TestStressSLMultReqSLDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0, i, microSec = 0;

    coreId = MMPM_CORE_ID_LPASS_SLIMBUS;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];
    microSec = gReqSleepLatencyParam[index];

    if (clientId1 && clientId2)
    {
        for (i = 0; (i < STRESS_TEST_LOOP) && (MMPM_STATUS_SUCCESS == retSts); i++)
        {
        	// Update progress based on loop count.
        	AdsppmTestDebugLogTestUpdate(i);
            
            sts = Test_RequestSleepLatency(clientId1, microSec);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Sleep latency Request\n");
                retSts = MMPM_STATUS_FAILED;
            } 
            sts = Test_RequestSleepLatency(clientId2, microSec);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Sleep latency Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseSleepLatency(clientId1);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st Sleep latency client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
            sts = Test_ReleaseSleepLatency(clientId2);
            if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Sleep latency client release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        Test_Deregister(clientId1);
        Test_Deregister(clientId2);
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}

#define STRESS_10_NAME    "stress_10"
#define STRESS_10_DETAILS "Stress out Sleep calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks."

/** 
  @ingroup Stress 
  @test stress_10
  @brief Stress out Sleep calls with multiple clients multiple times, large loop.  Verify enabled and disabled and memory leaks.

	
*/

/* Stress_10 */
MMPM_STATUS Test_Stress_10_Sleep(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", STRESS_10_NAME, STRESS_10_DETAILS);
    AdsppmTestDebugLogTestStart(STRESS_10_NAME, STRESS_TEST_LOOP);

    sts = TestStressSLMultReqSLDiffClient();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Sleep_04_Stress_10 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}




/* Test all MMPM2 stress tests */
void Test_Stress(void) 
{
#ifndef TEST_PROFILING
    /* Stress_01 */
    Test_Stress_01_System();

    /* Stress_02 */
    Test_Stress_02_Clock();

    /* Stress_03 */
    Test_Stress_03_Bandwidth();

    /* Stress_04 */
    Test_Stress_04_Power();

    /* Stress_05 */
    Test_Stress_05_PmicVreg();

    /* Stress_06 */
    Test_Stress_06_PmicVreg();

    /* Stress_07 */
    Test_Stress_07_RegisterProg();

    /* Stress_08 */
    Test_Stress_08_RegisterProg();

    /* Stress_09 */
    Test_Stress_09_Mips();

    /* Stress_10 */
    Test_Stress_10_Sleep();
#endif
    
  
}

