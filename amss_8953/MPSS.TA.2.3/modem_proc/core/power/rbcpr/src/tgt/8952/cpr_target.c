/*============================================================================
@file cpr_target.c

Target-specific CPR configuration data for 8952.

Copyright � 2013, 2015 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.9.1/power/rbcpr/src/tgt/8952/cpr_target.c#1 $
=============================================================================*/
#include "HALhwio.h"
#include "HAL_cpr.h"
#include "mss_cpr_hwio.h"
#include "cpr_efuse.h"
#include "cpr_internal.h"
#include "cpr.h"
#include "CoreVerify.h"
#include "pm_version.h"
#include "DDIChipInfo.h"
#include "VCSDefs.h" /* Enums for the power modes */

/** Contains all the CPR context data. */
extern CprContextType cprContext; 

//there are 5 fuses
#define NUM_8952_VMODES_FUSES 5


#define CPR_ROSC_COUNT            8

/** Database of fuse locations and fields we want to read from. */
static CprEfuseMasterDatabaseType cprEfuseMasterDb =
{
  //.redundancySel = { HWIO_QFPROM_CORR_SPARE_REG27_ROW0_MSB_ADDR, 0xe0000000, 29 },
  .redundancySel = { 0, 0, 0 },

  .numFuseEntries = NUM_8952_VMODES_FUSES,
  .primaryDb = 
  {
    //.disCpr  = {HWIO_QFPROM_CORR_SPARE_REG28_ROW1_LSB_ADDR, 0x100000, 20},
    .disCpr  = {0, 0, 0},
    .fuseVmodeCfgPtrs =
    {
      //MSS CPR fuses are CPR 1 on 8952
      [0] =
      {
        .modeEnum  = VCS_CORNER_LOW,		//SVS
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_SHFT},  
        .shiftLeftBy = 4,
      },
      [1] =
      {
        .modeEnum = VCS_CORNER_LOW_PLUS,		//SVS+
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_SHFT},  
        .shiftLeftBy = 4,
      },
      [2] =
      {
        .modeEnum  = VCS_CORNER_NOMINAL,
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .shiftLeftBy = 0,
      },
      [3] = 
      {
        .modeEnum  = VCS_CORNER_NOMINAL_PLUS,
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .shiftLeftBy = 0,
      },
      [4] =
      {
        .modeEnum  = VCS_CORNER_TURBO,
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_SHFT},  
        .shiftLeftBy = 0,        
      }
    }
  },
  .redundantDb =
  {
    .disCpr  = {0, 0, 0},
    .fuseVmodeCfgPtrs =
     { 
      //MSS CPR fuses are CPR 1 on 8952
      [0] =
      {
        .modeEnum  = VCS_CORNER_LOW,		//SVS
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_SHFT},  
        .shiftLeftBy = 4,
      },
      [1] =
      {
        .modeEnum = VCS_CORNER_LOW_PLUS,		//SVS+
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_SVS_3_0_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR1_TARG_VOLT_SVS_4_SHFT},  
        .shiftLeftBy = 4,
      },
      [2] =
        {
        .modeEnum  = VCS_CORNER_NOMINAL, 
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .shiftLeftBy = 0,
      },
      [3] = 
      {
        .modeEnum  = VCS_CORNER_NOMINAL_PLUS,           //NOM+
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_NOM_SHFT},  
        .shiftLeftBy = 0,
      },
      [4] =
      {
        .modeEnum  = VCS_CORNER_TURBO,
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_SHFT},  
        .targetVsteps1 = {HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_BMSK,
                          HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR1_TARG_VOLT_TUR_SHFT},  
        .shiftLeftBy = 0,
      }
    }
  }
};



#define NUM_8952_VMODES 5

/** Target specific config. */
static CprTargetCfgType cprTargetCfg =
{
  .irq                     = 93,       // From MSS HPG document
  .delayBeforeNextMeas     = 5,        // millisecs
  .voltageStepSize         = 12500,    // microVolts
  .numVmodeQuotientsAvail  = NUM_8952_VMODES,

};



CprVmodeBoundsAndOffsetCfgType boundsAndOffsetCfg8952[NUM_8952_VMODES] =
  { [0] =
    {
      .voltageCeiling = 0,
      .voltageFloor = 0,
      .initVoltOffsetSteps = 0,
    },
    [1] =
    {
      .voltageCeiling = 0,
      .voltageFloor = 0,
      .initVoltOffsetSteps = 0,
    },
    [2] =
    {
      .voltageCeiling = 0,
      .voltageFloor = 0,
      .initVoltOffsetSteps = 0,
    },
    [3] =
    {
      .voltageCeiling = 0,
      .voltageFloor = 0,
      .initVoltOffsetSteps = 0,
    },
    [4] =
    {
      .voltageCeiling = 0,
      .voltageFloor = 0,
      .initVoltOffsetSteps = 0,
    },
  };





/** Hardware version specific config needed by the HAL. */
static HalCprTargetConfigType halCprTargetCfg =
  {
    .staticCfg =
    {
      .configVersionText = "8952 10_16",

      .sensorMask[0] = 0,
      .sensorMask[1] = 0,

      .speedpushed_ROs = 0xC,     //2 and 3 are masked

      .sensorBypass[0] = 0,
      .sensorBypass[1] = 0,

      .CTL__DN_THRESHOLD = 2,
      .CTL__UP_THRESHOLD = 0,

      .SW_VLEVEL__SW_VLEVEL = 0x20,

      .TIMER_ADJUST__CONSECUTIVE_DN = 0x2,

      .sensorsOnCpuBlock[0] = 0x3000,
      .sensorsOnCpuBlock[1] = 0,
    },
    .halVmodeCfgPtrs =  // pointers to the vmode (SVS, SVS+, etc) configs to be filled in at init
    { 
       0,0,0,0,0,0,0,0,0,
    }
  };


/* Global Variable to store MSM version and part specific information */
HalCprVmodeCfgType * vmode_mode_settings_8952 = NULL;

// 100 mV margin  
HalCprVmodeCfgType vmode_mode_settings_8952v1_0_GF[NUM_8952_VMODES] =
   {  [0] =
     {                           /* Low SVS 5mV*/
      .modeEnum              = VCS_CORNER_LOW,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 900000,
      .initialCeilingVoltage = 1050000,
      .factoryOpenLoopCeilingCalculationVoltage = 1050000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 408,
      .oscCfg[1].target      = 414,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 339,
      .oscCfg[5].target      = 357,
      .oscCfg[6].target      = 545,
      .oscCfg[7].target      = 549,
     },
    [1] =
    {                           /* SVS 27.5mV */
      .modeEnum              = VCS_CORNER_LOW_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 27500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 975000,
      .initialCeilingVoltage = 1155000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1155000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 540,
      .oscCfg[1].target      = 543,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 454,
      .oscCfg[5].target      = 472,
      .oscCfg[6].target      = 685,
      .oscCfg[7].target      = 680,
    },
    [2] =
      {                           /* SVS+ 25mV */
        .modeEnum              = VCS_CORNER_NOMINAL,
        .stepQuotient          = 26,
        .idleClocks            = 15,
        .static_margin         = 25000,
        .static_margin_adjust  = 0,
        .initialFloorVoltage   = 1037500,    
        .initialCeilingVoltage = 1225000, 
        .factoryOpenLoopCeilingCalculationVoltage = 1225000,
        .floorCalc_correction  = 0,
        .oscCfg[0].target      = 635,
        .oscCfg[1].target      = 636,
        .oscCfg[2].target      = 0,
        .oscCfg[3].target      = 0,
        .oscCfg[4].target      = 539,
        .oscCfg[5].target      = 554,
        .oscCfg[6].target      = 781,
        .oscCfg[7].target      = 769,
      },
    [3] =
    {                           /* NOM 12.5mV */
      .modeEnum              = VCS_CORNER_NOMINAL_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 12500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1075000,
      .initialCeilingVoltage = 1287500,   
      .factoryOpenLoopCeilingCalculationVoltage = 1287500,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 701,
      .oscCfg[1].target      = 699,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 598,
      .oscCfg[5].target      = 611,
      .oscCfg[6].target      = 846,
      .oscCfg[7].target      = 828,
    },
    [4] =
    {                           /* TUR 0mV */
      .modeEnum              = VCS_CORNER_TURBO,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1125000,
      .initialCeilingVoltage = 1350000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1350000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 784,
      .oscCfg[1].target      = 779,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 673,
      .oscCfg[5].target      = 682,
      .oscCfg[6].target      = 927,
      .oscCfg[7].target      = 901,
    },
};


/* Static margins  
Turbo 	      -37.5 mV
Nominal L1    NA
Nominal	      0mV
SVS L1        NA
SVS           75mv
 */

HalCprVmodeCfgType vmode_mode_settings_8952v1_0_TSMC[NUM_8952_VMODES] =
   {  [0] =
     {                          
      .modeEnum              = VCS_CORNER_LOW,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 75000,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 900000,
      .initialCeilingVoltage = 1050000,
      .factoryOpenLoopCeilingCalculationVoltage = 1050000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 280,
      .oscCfg[1].target      = 286,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 227,
      .oscCfg[5].target      = 245,
      .oscCfg[6].target      = 401,
      .oscCfg[7].target      = 413,
     },
    [1] =
    {                        
      .modeEnum              = VCS_CORNER_LOW_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 100000,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1150000,
      .initialCeilingVoltage = 1150000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1150000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 412,
      .oscCfg[1].target      = 415,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 342,
      .oscCfg[5].target      = 360,
      .oscCfg[6].target      = 541,
      .oscCfg[7].target      = 544,
    },
    [2] =
      {                           
      .modeEnum              = VCS_CORNER_NOMINAL,
        .stepQuotient        = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1037500,
      .initialCeilingVoltage = 1225000,   
      .factoryOpenLoopCeilingCalculationVoltage = 1225000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 507,
      .oscCfg[1].target      = 508,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 427,
      .oscCfg[5].target      = 442,
      .oscCfg[6].target      = 637,
      .oscCfg[7].target      = 633,
    },
    [3] =
    {                           
      .modeEnum              = VCS_CORNER_NOMINAL_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 100000,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1287500,
      .initialCeilingVoltage = 1287500,
      .factoryOpenLoopCeilingCalculationVoltage = 1287500,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 573,
      .oscCfg[1].target      = 571,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 486,
      .oscCfg[5].target      = 499,
      .oscCfg[6].target      = 702,
      .oscCfg[7].target      = 692,
    },
    [4] =
    {                         
      .modeEnum              = VCS_CORNER_TURBO,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = -37500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1125000,
      .initialCeilingVoltage = 1350000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1350000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 656,
      .oscCfg[1].target      = 651,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 561,
      .oscCfg[5].target      = 570,
      .oscCfg[6].target      = 783,
      .oscCfg[7].target      = 765,
    },
};

// 100 mV margin  
HalCprVmodeCfgType vmode_mode_settings_8952v1_1_GF[NUM_8952_VMODES] =
   {  [0] =
     {                           /* Low SVS 5mV*/
      .modeEnum              = VCS_CORNER_LOW,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 900000,
      .initialCeilingVoltage = 1050000,
      .factoryOpenLoopCeilingCalculationVoltage = 1050000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 408,
      .oscCfg[1].target      = 414,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 339,
      .oscCfg[5].target      = 357,
      .oscCfg[6].target      = 545,
      .oscCfg[7].target      = 549,
     },
    [1] =
    {                           /* SVS 27.5mV */
      .modeEnum             = VCS_CORNER_LOW_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 27500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 975000,
      .initialCeilingVoltage = 1155000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1155000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 540,
      .oscCfg[1].target      = 543,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 454,
      .oscCfg[5].target      = 472,
      .oscCfg[6].target      = 685,
      .oscCfg[7].target      = 680,
    },
    [2] =
      {                           /* SVS+ 25mV */
        .modeEnum = VCS_CORNER_NOMINAL,
        .stepQuotient       = 26,
        .idleClocks = 15,
        .static_margin         = 25000,
        .static_margin_adjust  = 0,
        .initialFloorVoltage = 1037500,    
        .initialCeilingVoltage = 1225000, 
        .factoryOpenLoopCeilingCalculationVoltage = 1225000,
        .floorCalc_correction = 0,
        .oscCfg[0].target = 635,
        .oscCfg[1].target = 636,
        .oscCfg[2].target = 0,
        .oscCfg[3].target = 0,
        .oscCfg[4].target = 539,
        .oscCfg[5].target = 554,
        .oscCfg[6].target = 781,
        .oscCfg[7].target = 769,
      },
    [3] =
    {                           /* NOM 12.5mV */
      .modeEnum             = VCS_CORNER_NOMINAL_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 12500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1075000,
      .initialCeilingVoltage = 1287500,   
      .factoryOpenLoopCeilingCalculationVoltage = 1287500,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 701,
      .oscCfg[1].target      = 699,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 598,
      .oscCfg[5].target      = 611,
      .oscCfg[6].target      = 846,
      .oscCfg[7].target      = 828,
    },
    [4] =
    {                           /* TUR 0mV */
      .modeEnum             = VCS_CORNER_TURBO,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1125000,
      .initialCeilingVoltage = 1350000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1350000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 784,
      .oscCfg[1].target      = 779,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 673,
      .oscCfg[5].target      = 682,
      .oscCfg[6].target      = 927,
      .oscCfg[7].target      = 901,
    },
};

/*Static margins  
  Turbo      -12.5mV 
  Nominal L1 -12.5mV
  Nominal      0mV 
  SVS L1     -12.5mV 
  SVS         37.5mV
*/
HalCprVmodeCfgType vmode_mode_settings_8952v1_1_TSMC[NUM_8952_VMODES] =
   {  [0] =
     {                          
      .modeEnum              = VCS_CORNER_LOW,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = 37500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 900000,
      .initialCeilingVoltage = 1050000,
      .factoryOpenLoopCeilingCalculationVoltage = 1050000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 280,
      .oscCfg[1].target      = 286,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 227,
      .oscCfg[5].target      = 245,
      .oscCfg[6].target      = 401,
      .oscCfg[7].target      = 413,
     },
    [1] =
    {                        
      .modeEnum             = VCS_CORNER_LOW_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = -12500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 975000,
      .initialCeilingVoltage = 1150000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1150000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 412,
      .oscCfg[1].target      = 415,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 342,
      .oscCfg[5].target      = 360,
      .oscCfg[6].target      = 541,
      .oscCfg[7].target      = 544,
    },
    [2] =
      {                           
      .modeEnum             = VCS_CORNER_NOMINAL,
        .stepQuotient       = 26,
      .idleClocks            = 15,
      .static_margin         = 0,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1037500,
      .initialCeilingVoltage = 1225000,   
      .factoryOpenLoopCeilingCalculationVoltage = 1225000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 507,
      .oscCfg[1].target      = 508,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 427,
      .oscCfg[5].target      = 442,
      .oscCfg[6].target      = 637,
      .oscCfg[7].target      = 633,
    },
    [3] =
    {                           
      .modeEnum             = VCS_CORNER_NOMINAL_PLUS,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = -12500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1075000,
      .initialCeilingVoltage = 1287500,
      .factoryOpenLoopCeilingCalculationVoltage = 1287500,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 573,
      .oscCfg[1].target      = 571,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 486,
      .oscCfg[5].target      = 499,
      .oscCfg[6].target      = 702,
      .oscCfg[7].target      = 692,
    },
    [4] =
    {                         
      .modeEnum             = VCS_CORNER_TURBO,
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .static_margin         = -12500,
      .static_margin_adjust  = 0,
      .initialFloorVoltage   = 1125000,
      .initialCeilingVoltage = 1350000, 
      .factoryOpenLoopCeilingCalculationVoltage = 1350000,
      .floorCalc_correction  = 0,
      .oscCfg[0].target      = 656,
      .oscCfg[1].target      = 651,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 561,
      .oscCfg[5].target      = 570,
      .oscCfg[6].target      = 783,
      .oscCfg[7].target      = 765,
    },
};


/* Voltage Scaling vector for TSMC */ 
static uint32	voltage_scaling_vector_TSMC[CPR_ROSC_COUNT] =
{
	1280,
	1280,
	0,
	0,
	1120,
	1120,
	1440,
	1360,
};

/* If required for GF part need to modify Code*/
/*Voltage Scaling vector for GF */
/*static uint32	voltage_scaling_vector_GF[CPR_ROSC_COUNT] =
{
	1429,
	1386,
	1695,
	1626,
	1271,
	1321,
	1552,
	1464,
};*/


/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
/**
 * <!-- CprGetTargetConfiguration -->
 *
 * @brief Get target-specific configuration info, including reading efuses etc.
 *
 * @param cprContext: cprContext.targetCfg and cprContext.halTargetCfg will
 * will be set to non-NULL pointers.
 */
void CprGetTargetConfiguration( void )
{
  DalChipInfoVersionType chipVersion;
  DalChipInfoFamilyType familyVersion;
  uint32 pmic_version;
  int i;
  static boolean cprTargetInitDone = FALSE;

  if (cprTargetInitDone)
  {
    return;
  }
  cprTargetInitDone = TRUE;
  
  CORE_VERIFY(familyVersion = DalChipInfo_ChipFamily());
  CORE_VERIFY(chipVersion = DalChipInfo_ChipVersion());
  
  cprContext.partValue = HalCPRGetPartInfo ();
  cprContext.PartInfo = (cprContext.partValue &0x700) >> 8;
 
  //read the chip reference 
  cprContext.fuse_reference_value = 0; //*(uint32*)0XFC4B81F0;
 
  // New feature April 2014 Floor calculations will be enabled after validation
  cprContext.enableFlags_OpenLoop_FloorVoltageEnabled = TRUE;

  // Fill in more cprContext info with the target config pointer. 
  cprContext.targetCfg = &cprTargetCfg;
  
  // Initialize the vmode array pointers to null.
  // The halCprTargetCfg.halVmodeCfgPtrs array will hold quotients, initial voltages, and 
  // other mode (SVS, SVS+ etc) info.  The mode enums determine which pointers get filled in.
  for (i=0; i<CPR_MAX_VMODE_COUNT; i++)
  {
    halCprTargetCfg.halVmodeCfgPtrs[i] = 0;
  }

  //store the version text and sensor mask info info 
  cprContext.halTargetCfg = &halCprTargetCfg;

  if (1 == cprContext.PartInfo)
      {
    // GF part
    if(chipVersion == DALCHIPINFO_VERSION(1,1))
      {
      vmode_mode_settings_8952 = vmode_mode_settings_8952v1_1_GF;
        }
    else
    {
      vmode_mode_settings_8952 = vmode_mode_settings_8952v1_0_GF;
      }
    }
  else
  {
    // TSMC part
    if(chipVersion == DALCHIPINFO_VERSION(1,1))
    {
      vmode_mode_settings_8952 = vmode_mode_settings_8952v1_1_TSMC;
    }
    else
    {
      vmode_mode_settings_8952 = vmode_mode_settings_8952v1_0_TSMC;
    }
  }
   //fill in the modes that we have configurations for in this file. 
    for (i=0; i<NUM_8952_VMODES; i++)
    {
      //make sure the enum will fit in the list of modes. 
    if (vmode_mode_settings_8952[i].modeEnum >= CPR_MAX_VMODE_COUNT){
        // add an error message to the ulog to try to make this easy to debug if it ever happens. 
        //ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "Fatal Error mode enum larger than supported range!");
        CORE_VERIFY(0);
      }
      // Put the pointer to the HalCprVmodeCfgType in the appropriate location so they can be looked up based on the enums.
      // The unused slots remain NULL.
	halCprTargetCfg.halVmodeCfgPtrs[vmode_mode_settings_8952[i].modeEnum] = &(vmode_mode_settings_8952[i]);
  
      //fill in the vmodeBoundsAndOffset pointers in appropriate location so they can be looked up based on the enums
    cprContext.targetCfg->vmodeBoundsAndOffsetCfgPtrs[vmode_mode_settings_8952[i].modeEnum] = &(boundsAndOffsetCfg8952[i]);
    }
  
  // Enable Closed Loop CPR on v1 8952
  cprContext.enableFlags_ClosedLoopEnabled = TRUE;	
    
  // Some items in cprTargetCfg should be read in from efuses. (per chip CPR enable, initVoltOffsetSteps) 
  cprContext.enableFlags_DisabledByFuse = CprEfuseGetData(&cprEfuseMasterDb, &(cprContext.targetCfg->vmodeBoundsAndOffsetCfgPtrs[0]));

  pmic_version = pm_get_pmic_model(0);       //get the (0 primary) pmic info	

  cprContext.detectedPMIC = pmic_version;   //store the PMIC for debug and logging later 
  cprContext.detectedChipVer = chipVersion; //store the chip version for debug and logging later 

  //9x35 needed the Mem and IO unclamped at init.  
  //HalCprUnclampMemAndIO();
}

/**
 * <!-- CprGetTargetCeilingVoltageForMode -->
 *
 * @brief Calculate the ceiling voltage based on the limits and configuration/
 *
 * @param vmode : enum for the corner (SVS, NOM etc) 
 * @param voltageUV : pointer to uint32 where the voltage will be returned. 
 *
 * @return Currently always returns DAL_SUCCESS, function will error fatal if an unknow enum is used.
 */
DALResult CprGetTargetCeilingVoltageForMode( uint32 vmode, uint32* voltageUV ){
  int proposed_cpr_ceiling;

  // Get the target configuration setup, will return if already done.
  CprGetTargetConfiguration();
  
  //Make sure the mode we're being asked about is one that we have 
  //configuration data for.
  CORE_VERIFY(halCprTargetCfg.halVmodeCfgPtrs[vmode] != NULL);

  // If only the ceiling was being used:
  //proposed_cpr_ceiling = halCprTargetCfg.halVmodeCfgPtrs[vmode]->initialCeilingVoltage;

  // Apply the open loop fuse adjustment
  proposed_cpr_ceiling = cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage +
           (cprContext.targetCfg->vmodeBoundsAndOffsetCfgPtrs[vmode]->initVoltOffsetSteps * 10000);

  //clip positive voltages above the ceiling
  if (proposed_cpr_ceiling > cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage)
  {
    proposed_cpr_ceiling = cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage;
  } 

  //round up to the nearest PMIC step
  if(proposed_cpr_ceiling%12500)
       proposed_cpr_ceiling += 12500 - proposed_cpr_ceiling%12500;
		
  CORE_VERIFY(proposed_cpr_ceiling%12500==0);  
  
  if(proposed_cpr_ceiling > cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage )
  {
    proposed_cpr_ceiling = cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage;
  }
  
  *voltageUV = proposed_cpr_ceiling;
  return DAL_SUCCESS;
}


/**
 * <!-- CprGetTargetFloorVoltageForMode -->
 *
 * @brief Calculate the floor voltage based on the limits and configuration/
 *
 * @param vmode : enum for the corner (SVS, NOM etc) 
 * @param voltageUV : pointer to uint32 where the voltage will be returned. 
 *
 * @return Currently always returns DAL_SUCCESS, function will error fatal if an unknow enum is used.
 */
DALResult CprGetTargetFloorVoltageForMode( uint32 vmode, uint32* voltageUV ){
  uint32 proposed_cpr_floor;
  
  // Get the target configuration setup, will return if already done.
  CprGetTargetConfiguration();

  //Make sure the mode we're being asked about is one that we have 
  //configuration data for.
  CORE_VERIFY(halCprTargetCfg.halVmodeCfgPtrs[vmode] != NULL);

  proposed_cpr_floor = halCprTargetCfg.halVmodeCfgPtrs[vmode]->initialFloorVoltage;

  if (cprContext.enableFlags_OpenLoop_FloorVoltageEnabled == TRUE)
  {  
    uint32 capped_mss_fuse;
    int32 fuse = cprContext.targetCfg->vmodeBoundsAndOffsetCfgPtrs[vmode]->initVoltOffsetSteps;

	//the fuse should be 0 or a negative value
    capped_mss_fuse = fuse > 0 ? 0 : fuse;

	//apply the formula
    proposed_cpr_floor = ((halCprTargetCfg.halVmodeCfgPtrs[vmode]->initialCeilingVoltage * 90)/100)
                            + 10000 * capped_mss_fuse + 37500;

    //apply the floorCalc_correction
    proposed_cpr_floor = proposed_cpr_floor +  halCprTargetCfg.halVmodeCfgPtrs[vmode]->floorCalc_correction;
	
	//clip to the minimum voltage for the corner.  
	if (proposed_cpr_floor < halCprTargetCfg.halVmodeCfgPtrs[vmode]->initialFloorVoltage)
	{
	  proposed_cpr_floor = halCprTargetCfg.halVmodeCfgPtrs[vmode]->initialFloorVoltage;
	}
  } 

  //round up to the nearest PMIC step
  if(proposed_cpr_floor%12500)
        proposed_cpr_floor += 12500 - proposed_cpr_floor%12500;

  CORE_VERIFY(proposed_cpr_floor%12500==0);
  /* SVS+ and NOM+ closed loop CPR is disabled as there is no margin recommendation from SPT. 
     So,initial floor is made same as initial ceiling for SVS+ and NOM+. 
	 Below check ensures that calculated floor does not go beyond initial ceiling. */
  if(proposed_cpr_floor > cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage )
  {
     proposed_cpr_floor = cprContext.halTargetCfg->halVmodeCfgPtrs[vmode]->initialCeilingVoltage;
  }
  
  *voltageUV = proposed_cpr_floor;

  return DAL_SUCCESS;
}


/**
 * <!-- CprBspInitRoscTarget -->
 *
 * @brief Dynamically generate CPR quotient values on run time based on static margin for each corner
 */
void CprBspInitRoscTarget()
{
    int32 calc_value = 0;
    if (FALSE == cprContext.enableFlags_ClosedLoopEnabled)
	    return;
	
    for (int i=0; i < CPR_MAX_VMODE_COUNT; i++)	
    {
		for (int j=0; j < CPR_ROSC_COUNT; j++)
		{          
            if(cprContext.halTargetCfg->halVmodeCfgPtrs[i])
            {            
				/* divide it by 1000000, because voltage delta is in uv and voltage scaling factor (kv) is also multiplied 
                   by 1000 to avoid floating point arthmatic */
				calc_value = ((cprContext.halTargetCfg->halVmodeCfgPtrs[i]->static_margin) * (voltage_scaling_vector_TSMC[j]));
				calc_value = (int32)calc_value/1000000;
			
				/* adjust precision from above calculation */
				cprContext.halTargetCfg->halVmodeCfgPtrs[i]->oscCfg[j].target+=  calc_value + cprContext.halTargetCfg->halVmodeCfgPtrs[i]->static_margin_adjust;
				ULOG_RT_PRINTF_5(cprContext.ulogHandle, "(calc_value: %d) " 			
				"(static_margin: %d) (static_margin_adjust : %d) (vmode: %d ) (osCcfg: %d) ",			
				calc_value, cprContext.halTargetCfg->halVmodeCfgPtrs[i]->static_margin,
				cprContext.halTargetCfg->halVmodeCfgPtrs[i]->static_margin_adjust, i,
				cprContext.halTargetCfg->halVmodeCfgPtrs[i]->oscCfg[j].target);
            }
			
		}
    }
}


