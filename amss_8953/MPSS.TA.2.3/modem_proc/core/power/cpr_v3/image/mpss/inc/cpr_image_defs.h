/**
 * @file:  cpr_image_defs.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/image/mpss/inc/cpr_image_defs.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_IMAGE_DEFS_H
#define	CPR_IMAGE_DEFS_H

#include "comdef.h"
#include "HALhwio.h"
#include "CoreVerify.h"
#include "msmhwiobase.h"
#include "DDIChipInfo.h"

#define CPR_CHIPINFO_VERSION(major, minor) DALCHIPINFO_VERSION(major, minor)
#define CPR_ASSERT(x) CORE_VERIFY(x)

#define CPR_IMAGE_LOCK_CORE()   cpr_image_mutex_lock()
#define CPR_IMAGE_UNLOCK_CORE() cpr_image_mutex_unlock()

void cpr_image_mutex_lock(void);
void cpr_image_mutex_unlock(void);
#endif

