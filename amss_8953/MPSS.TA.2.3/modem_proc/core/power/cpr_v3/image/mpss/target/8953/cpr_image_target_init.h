/**
 * @file:  cpr_image_target_init.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/image/mpss/target/8953/cpr_image_target_init.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_IMAGE_TARGET_INIT_H
#define CPR_IMAGE_TARGET_INIT_H

#include "cpr_cfg.h"

//******************************************************************************
// Default Enablement Structures
//******************************************************************************

extern cpr_cfg_funcs CPR_INIT_NONE; /* used to disable CPR */
extern cpr_cfg_funcs CPR_INIT_OPEN_LOOP; /* used for CX rail */
extern cpr_cfg_funcs CPR_INIT_SW_CLOSED_LOOP; /* used for MSS rail */

#endif

