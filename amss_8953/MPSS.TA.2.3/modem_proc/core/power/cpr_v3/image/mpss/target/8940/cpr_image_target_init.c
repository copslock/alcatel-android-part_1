/**
 * @file:  cpr_image_init.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/17 20:35:29 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/image/mpss/target/8940/cpr_image_target_init.c#1 $
 * $Change: 9915170 $
 */
#include <string.h>
#include "cpr_logs.h"
#include "cpr_data.h"
#include "cpr_rail.h"
#include "cpr_smem.h"
#include "cpr_image.h"
#include "cpr_measurements.h"
#include "cpr_cfg.h"
#include "cpr_hal.h"
#include "cpr_utils.h"
#include "cpr_image_target_init.h"

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static cpr_cfg_funcs* init_open_loop(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Initializing open loop on %s", rail->name );
    cpr_rail_set_initial_voltages( rail, false, false );
    return NULL;
}

static cpr_cfg_funcs* init_closed_loop(cpr_rail* rail)
{

#if 0 /* enable when aging ready */
    cpr_measurements_aging( rail );
#endif

    init_open_loop(rail);

    CPR_LOG_TRACE( "Configuring closed loop on %s", rail->name );

    cpr_rail_update_target_quotients( rail, &cpr_info.railStates[rail->railIdx] );
    cpr_hal_init_rail_hw( &rail->hal, &rail->halRailCfg );

    return NULL;
}

static cpr_cfg_funcs* enable_closed_loop(cpr_rail* rail)
{
    CPR_LOG_TRACE( "Enabling closed loop on %s", rail->name );

    cpr_rail_register_isr( rail );

    return NULL;
}

//******************************************************************************
// Default Enablement Structures
//******************************************************************************

cpr_cfg_funcs CPR_INIT_NONE           = {.cMode = CPR_CONTROL_NONE,           .init = NULL,             .enable = NULL};
cpr_cfg_funcs CPR_INIT_OPEN_LOOP      = {.cMode = CPR_CONTROL_OPEN_LOOP,      .init = init_open_loop,   .enable = NULL};
cpr_cfg_funcs CPR_INIT_SW_CLOSED_LOOP = {.cMode = CPR_CONTROL_SW_CLOSED_LOOP, .init = init_closed_loop, .enable = enable_closed_loop};
