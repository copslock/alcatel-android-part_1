/**
 * @file:  cpr_image_target.c
 * @brief: image-specific and target-specific CPR functions.
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/image/mpss/target/8953/cpr_image_target.c#1 $
 * $Change: 9639486 $
 */
#include "cpr_image_target.h"
#include "cpr_data.h"
#include "cpr_cfg.h"
#include "cpr_utils.h"
#include "DDIChipInfo.h"

/**
*
* <!-- cpr_image_target_get_cx_eldo_voltage -->
*
*/
uint32 cpr_image_target_get_cx_eldo_voltage(cpr_domain_info* info)
{
    cpr_submode_settings* modeState;
    uint32 voltage_uv;

    cpr_utils_get_mode_settings( CPR_RAIL_CX, info, NULL, &modeState );

    /*
     * TODO: get correct magic formula from PTE
     */
    voltage_uv = modeState->ceiling;

    switch(info->u.mode)
    {
        case CPR_VOLTAGE_MODE_MIN_SVS:
            voltage_uv -= 60000;
            break;

        case CPR_VOLTAGE_MODE_LOW_SVS:
            voltage_uv -= 20000;
            break;

        case CPR_VOLTAGE_MODE_SVS:
            voltage_uv -= 90000;
            break;

        default:
            /*
             * not supported
             */
            CPR_LOG_FATAL("Unsupported mode for eLDO voltage: %d", info->u.mode);
    }

    CPR_LOG_TRACE("Calculated eLDO voltage for mode %d : %d uv", info->u.mode, voltage_uv);

    return voltage_uv;
}

/**
*
* <!-- cpr_image_target_get_foundry -->
*
* @brief Get the foundry and return a foundry enum that our CPR driver understands
*
*/
cpr_foundry_id cpr_image_target_get_foundry(void)
{
    return CPR_FOUNDRY_SS;
}

/**
*
* <!-- cpr_image_target_convert_vcs_to_cpr -->
*
* @brief Converts VCS corner enum to CPR voltage mode enum
*
*/
cpr_voltage_mode cpr_image_target_convert_vcs_to_cpr(VCSCornerType corner)
{
    cpr_voltage_mode mode = CPR_VOLTAGE_MODE_OFF;

    switch(corner)
    {
        case VCS_CORNER_OFF            : mode = CPR_VOLTAGE_MODE_OFF;        break;
        case VCS_CORNER_RETENTION      : mode = CPR_VOLTAGE_MODE_RETENTION;  break;
        case VCS_CORNER_RETENTION_PLUS : mode = CPR_VOLTAGE_MODE_RETENTION;  break;
        case VCS_CORNER_LOW_MINUS      : mode = CPR_VOLTAGE_MODE_LOW_SVS;    break;
        case VCS_CORNER_LOW            : mode = CPR_VOLTAGE_MODE_SVS;        break;
        case VCS_CORNER_LOW_PLUS       : mode = CPR_VOLTAGE_MODE_SVS_L1;     break;
        case VCS_CORNER_NOMINAL        : mode = CPR_VOLTAGE_MODE_NOMINAL;    break;
        case VCS_CORNER_NOMINAL_PLUS   : mode = CPR_VOLTAGE_MODE_NOMINAL_L1; break;
        case VCS_CORNER_TURBO          : mode = CPR_VOLTAGE_MODE_TURBO;      break;
        default: CPR_LOG_FATAL("Unsupported VCS corner %u", corner); break;
    }

    return mode;
}

/**
*
* <!-- cpr_image_target_convert_vcs_to_cpr -->
*
* @brief Converts CPR voltage mode enum to VCS corner enum
*
*/
VCSCornerType cpr_image_target_convert_cpr_to_vcs(cpr_voltage_mode mode)
{
    VCSCornerType corner = VCS_CORNER_OFF;

    switch(mode)
    {
        case CPR_VOLTAGE_MODE_OFF:        corner = VCS_CORNER_OFF;          break;
        case CPR_VOLTAGE_MODE_RETENTION:  corner = VCS_CORNER_RETENTION;    break;
        case CPR_VOLTAGE_MODE_LOW_SVS:    corner = VCS_CORNER_LOW_MINUS;    break;
        case CPR_VOLTAGE_MODE_SVS:        corner = VCS_CORNER_LOW;          break;
        case CPR_VOLTAGE_MODE_SVS_L1:     corner = VCS_CORNER_LOW_PLUS;     break;
        case CPR_VOLTAGE_MODE_NOMINAL:    corner = VCS_CORNER_NOMINAL;      break;
        case CPR_VOLTAGE_MODE_NOMINAL_L1: corner = VCS_CORNER_NOMINAL_PLUS; break;
        case CPR_VOLTAGE_MODE_TURBO:      corner = VCS_CORNER_TURBO;        break;
        default: CPR_LOG_FATAL("Unsupported CPR mode %u", mode); break;
    }

    return corner;
}

