/*===========================================================================

Copyright (c) 2014, 2016 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary and Confidential.

===========================================================================*/
#include <stdlib.h>
#include <stdbool.h>

#include "cpr_v3.h"
#include "cpr_image.h"
#include "cpr_image_target.h"
#include "cpr_cfg.h"
#include "cpr_data.h"
#include "cpr_utils.h"
#include "cpr_isr.h"
#include "cpr_mpss_hwio.h" /* for MSS_CLAMP_MEM register */

#include "CoreVerify.h"
#include "CoreMutex.h"
#include "comdef.h"
#include "VCSDefs.h" /* Enums for the VCS power modes */
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DDIClock.h"
#include "DDIVCS.h"
#include "DDIInterruptController.h"
#include "ClockDefs.h"
#include "DDIChipInfo.h"
#include "busywait.h"
#include "cpr_measurements.h"

#define CPR_NPA_RESOURCE_THERMAL        "/therm/mitigate/cpr_cold"
#define CPR_NPA_RESOURCE_VDD_MSS        VCS_NPA_RESOURCE_VDD_MSS
#define CPR_NPA_RESOURCE_VDD_MSS_UV     VCS_NPA_RESOURCE_VDD_MSS_UV

#define CPR_VDD_MSS_CLIENT_NAME         "CPR_client"
#define CPR_PRE_SWITCH_EVT_HDL_NAME     "CPR_pre_switch_cb"
#define CPR_POST_SWITCH_EVT_HDL_NAME    "CPR_post_switch_cb"
#define CPR_THERMAL_EVT_HDL_NAME        "CPR_thermal_change_cb"


//******************************************************************************
//******************************************************************************
//
// Global Data
//
//******************************************************************************
//******************************************************************************

static cpr_therm_cb       therm_event_handler;
static DalDeviceHandle   *cpr_dal_hdl_clk;
static DalDeviceHandle   *cpr_dal_hdl_vcs;
static CoreMutex         *cpr_lock_hdl;
static npa_client_handle  cpr_npa_hdl_vddmss; /* used for rail mode request */
static DalDeviceHandle   *cpr_dal_hdl_int_ctrl;

static cpr_thermal_region cpr_current_therm_state = CPR_THERMAL_REGION_COOL;

static cpr_voltage_mode next_mode;

//******************************************************************************
//******************************************************************************
//
// Local Helper Functions
//
//******************************************************************************
//******************************************************************************

/**
*
* <!-- therm_change_event_cb -->
*
* @brief Listener for thermal events
*
*/
static void therm_change_event_cb(void *context,
                                  unsigned int event_type,
                                  void *data,
                                  unsigned int data_size)
{
    if(data != NULL)
    {
        cpr_thermal_region new_state = (cpr_thermal_region)((npa_event_data*)data)->state;

        CPR_LOG_TRACE("Therm change event: (OldState=%u) -> (NewState=%u)",
            cpr_current_therm_state,
            new_state);

        if(cpr_current_therm_state != new_state)
        {
            cpr_current_therm_state = new_state;

            therm_event_handler(new_state);
        }
    }
}

/**
*
* <!-- therm_resource_available_cb -->
*
* @brief Listener for thermal event availability
*
*/
static void therm_resource_available_cb(void *context,
                                        unsigned int event_type,
                                        void *data,
                                        unsigned int data_size)
{
    npa_event_handle  hdl;
    npa_query_type    query_result;

    CPR_LOG_INFO("Registering for thermal change cb");

    hdl = npa_create_change_event_cb(CPR_NPA_RESOURCE_THERMAL,
                                     CPR_THERMAL_EVT_HDL_NAME,
                                     therm_change_event_cb,
                                     NULL );

    if(NPA_QUERY_SUCCESS == npa_query_by_event(hdl, NPA_QUERY_CURRENT_STATE, &query_result))
    {
        therm_event_handler( (cpr_thermal_region)query_result.data.state );
    }
}

/**
*
* <!-- cpr_image_pre_switch_cb -->
*
* @brief Listener for pre-switch event.
*
*/
static void cpr_image_pre_switch_cb(void *context,
                                    unsigned int event_type,
                                    void *data,
                                    unsigned int data_size)
{
    VCSNPARailEventDataType *rail_event_data = (VCSNPARailEventDataType *)data;

    next_mode = cpr_image_target_convert_vcs_to_cpr(rail_event_data->PostChange.eCorner);

    CPR_LOG_TRACE("Pre-switch event: (corner=%u)",
                  rail_event_data->PreChange.eCorner);

#if 0 /* TODO: enable this when we don't need to support old APIs */
    cpr_pre_state_switch((cpr_domain_id)context);
#endif
}

/**
*
* <!-- cpr_image_post_switch_cb -->
*
* @brief Listener for post-switch event.
*
*/
static void cpr_image_post_switch_cb(void *context,
                                     unsigned int event_type,
                                     void *data,
                                     unsigned int data_size)
{
    VCSNPARailEventDataType *rail_event_data = (VCSNPARailEventDataType *)data; // Note: check if this needs to be enabled later

    CPR_LOG_TRACE("Post-switch event: (corner=%u)",
                  rail_event_data->PostChange.eCorner);

#if 0 /* TODO: enable this when we don't need to support old APIs */
    if(!rail_event_data->bIsNAS)
    {
        cpr_domain_id   railId = (cpr_domain_id)context;
        cpr_domain_info info   = {CPR_DOMAIN_TYPE_MODE_BASED, {(cpr_voltage_mode)rail_event_data->PostChange.eCorner}};

        cpr_post_state_switch(railId, &info, rail_event_data->PostChange.nVoltageUV);
    }
#endif
}

/**
*
* <!-- cpr_image_register_pre_switch_cb -->
*
* @brief Register the callback for VCS pre/post voltage-switch event.
*
*/
static void cpr_image_register_pre_switch_cb(cpr_domain_id railId, npa_callback callback)
{
    npa_event_handle  hdl = NULL;

    CPR_LOG_INFO(" Registering for pre-switch event cb");

    hdl = npa_create_event_cb(CPR_NPA_RESOURCE_VDD_MSS_UV,
                              CPR_PRE_SWITCH_EVT_HDL_NAME,
                              (npa_event_trigger_type)VCS_NPA_RAIL_EVENT_PRE_CHANGE,
                              callback,
                              (void*) railId);

    CPR_ASSERT(hdl);
}

/**
*
* <!-- cpr_image_register_post_switch_cb -->
*
* @brief Register the callback for VCS pre/post voltage-switch event.
*
*/
static void cpr_image_register_post_switch_cb(cpr_domain_id railId, npa_callback callback)
{
    npa_event_handle  hdl = NULL;

    CPR_LOG_INFO(" Registering for post-switch event cb");

    hdl = npa_create_event_cb(CPR_NPA_RESOURCE_VDD_MSS_UV,
                              CPR_PRE_SWITCH_EVT_HDL_NAME,
                              (npa_event_trigger_type)VCS_NPA_RAIL_EVENT_POST_CHANGE,
                              callback,
                              (void*) railId);

    CPR_ASSERT(hdl);
}

/**
*
* <!-- register_thermal_cb -->
*
*/
void register_thermal_cb(void)
{
    if(!therm_event_handler)
    {
        CPR_LOG_INFO(" Registering for thermal resource available cb");

        npa_resource_available_cb("/therm/mitigate/cpr_cold",
                                  therm_resource_available_cb,
                                  NULL);
    }
}

//******************************************************************************
//******************************************************************************
//
// Image API Functions
//
//******************************************************************************
//******************************************************************************

/**
*
* <!-- cpr_image_warmup_init -->
*
* This is RC Init function called after CPR and vcs_cpr RC Init tasks
* in order to settles closed loop voltage on all supported modes.
*
*/
void cpr_image_warmup_init(void)
{
    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return;
    }

    CPR_IMAGE_LOCK_CORE();

    CPR_LOG_INFO("cpr_image_warmup_init");

#if 0 /* TODO: enable when settling change is ready */
    /*
     * Need to settle voltage on all supported modes on MSS rail to help MVC mode.
     */
    cpr_rail* rail = cpr_get_rail( CPR_RAIL_MSS );

    for(int i = 0; i < rail->vp->modesCount; i++)
    {
        CPR_LOG_TRACE( "Settling on %s mode %d", rail->name, rail->vp->supportedModes[i] );

        cpr_measurements_settle( rail, rail->vp->supportedModes[i] );
    }

    CPR_LOG_INFO( "--- Done with settling ---" );
#endif

    cpr_image_register_pre_switch_cb(CPR_RAIL_MSS, cpr_image_pre_switch_cb);
    cpr_image_register_post_switch_cb(CPR_RAIL_MSS, cpr_image_post_switch_cb);

    register_thermal_cb();

    CPR_LOG_INFO("Enable CPR IRQ");

    DalInterruptController_InterruptEnable(cpr_dal_hdl_int_ctrl, cpr_isr_get_interrupt(CPR_RAIL_MSS));

    CPR_LOG_INFO("--- cpr_image_warmup_init_completed ---");

    CPR_IMAGE_UNLOCK_CORE();
}

/**
*
* <!-- cpr_image_register_thermal_cb -->
*
* NOTE: should be enabled after settling completed
*/
void cpr_image_register_thermal_cb(cpr_therm_cb cb)
{
    /*
     * register callback after settling in cpr_image_warmup_init()
     */
    therm_event_handler = cb;
}

/**
*
* <!-- cpr_image_register_isr -->
*
* NOTE: should be enabled after settling completed.
*
*/
void cpr_image_register_isr(cpr_domain_id railId, uint32 interrupt, cpr_image_isr isr, void* ctx)
{
    if(!cpr_dal_hdl_int_ctrl)
    {
        // Get an interrupt controller handle
        CORE_DAL_VERIFY(DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER, &cpr_dal_hdl_int_ctrl));
    }

    // Register the CPR interrupt.
    CORE_DAL_VERIFY(DalInterruptController_RegisterISR(cpr_dal_hdl_int_ctrl,
                                                       interrupt,
                                                       (DALIRQ)isr,
                                                       (DALISRCtx)ctx,
                                                       DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER));

    /*
     * Don't enable CPR IRQ until settling completes in cpr_image_warmup_init()
     */
    DalInterruptController_InterruptDisable(cpr_dal_hdl_int_ctrl, interrupt);
}

/**
*
* <!-- cpr_image_get_chip_version -->
*
* @brief Calls DalChipInfo_ChipVersion on the modem version of the CPR driver.
*
*/
uint32 cpr_image_get_chip_version(void)
{
    return (uint32)DalChipInfo_ChipVersion();
}

/**
*
* <!-- cpr_image_get_foundry -->
*
* @brief Get the foundry and return a foundry enum that our CPR driver understands.
*
*/
cpr_foundry_id cpr_image_get_foundry(void)
{
    return cpr_image_target_get_foundry();
}

/**
*
* <!-- cpr_image_enable_clock -->
*
* @brief Turn the CPR clock on.
*
*/
void cpr_image_enable_clock(const char* clkId)
{
    /*
     * TODO: Remove clock driver dependency to make settling work.
     */
    ClockIdType clockId;

    CPR_LOG_INFO(" Enable clock: %s", clkId);

    if(!cpr_dal_hdl_clk)
    {
        // Attach to Clock Driver
        CORE_DAL_VERIFY( DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &cpr_dal_hdl_clk));
    }

    // Enable the rbcpr bus clock.
    DalClock_GetClockId(cpr_dal_hdl_clk, clkId, &clockId);
    DalClock_EnableClock(cpr_dal_hdl_clk, clockId);

    // Take the clock out of reset state.
    DalClock_ResetClock(cpr_dal_hdl_clk, clockId, CLOCK_RESET_DEASSERT);
}

/**
*
* <!-- cpr_image_set_rail_mode -->
*
* @brief set the rail's current voltage mode to the specified mode and change the physical voltage on the rail
*
*
*/
boolean cpr_image_set_rail_mode(cpr_domain_id railId, cpr_domain_info* info)
{
    npa_query_type query_result;
    boolean ret = false;

    //only supporting MSS
    if (railId != CPR_RAIL_MSS)
    {
        return ret;
    }

    if(!cpr_npa_hdl_vddmss)
    {
        /* Create vdd_mss client handle. */
        cpr_npa_hdl_vddmss = npa_create_sync_client(CPR_NPA_RESOURCE_VDD_MSS,
                                                    CPR_VDD_MSS_CLIENT_NAME,
                                                    NPA_CLIENT_REQUIRED);
    }

    CPR_LOG_INFO(" Requesting rail mode switch to %u", info->u.mode);

    next_mode = info->u.mode;

    npa_issue_required_request(cpr_npa_hdl_vddmss, cpr_image_target_convert_cpr_to_vcs(info->u.mode));

    if(NPA_QUERY_SUCCESS == npa_query_by_client(cpr_npa_hdl_vddmss, NPA_QUERY_CURRENT_STATE, &query_result))
    {
        next_mode = cpr_image_target_convert_vcs_to_cpr(query_result.data.state);

        if(next_mode == info->u.mode)
        {
            CPR_LOG_INFO(" Rail mode switched to %u", next_mode);
            ret = true;
        }
        else
        {
            CPR_LOG_ERROR(" Failed to switch rail mode to %u (%u)", info->u.mode, next_mode);
        }
    }
    else
    {
        CPR_LOG_ERROR(" Failed to query %s", CPR_NPA_RESOURCE_VDD_MSS);
    }

    return ret;
}

/**
*
* <!-- cpr_image_rail_transition_voltage -->
*
* @brief change the physical voltage on the rail.
*
*/
boolean cpr_image_rail_transition_voltage(cpr_domain_id railId)
{
    DALResult ret;
    boolean transition_completed = false;

    if (railId != CPR_RAIL_MSS)
    {
        return transition_completed;
    }

    if(!cpr_dal_hdl_vcs)
    {
        // Attach to VCS for voltage changes.
        CORE_DAL_VERIFY( DAL_StringDeviceAttach("VCS", &cpr_dal_hdl_vcs));
    }

    CPR_LOG_INFO(" Transition rail voltage: Start");

    //Change the physical voltage on the rail
    cpr_mode_settings* mode_setting = cpr_utils_get_active_mode_setting(railId);
    CPR_ASSERT(mode_setting);
    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {mode_setting->mode}};

    ret = DalVCS_SetRailVoltage(cpr_dal_hdl_vcs, VCS_RAIL_MSS, cpr_get_voltage(railId, &info));

    if(DAL_SUCCESS == ret)
    {
        transition_completed = true;

        CPR_LOG_INFO(" Transition rail voltage: Completed");
    }
    else
    {
        /*
         * Should not abort as VCS driver could be just in the process of corner/voltage transition
         */
        CPR_LOG_WARNING(" DalVCS_SetRailVoltage failed: (ret=%d)", ret);
    }

    return transition_completed;
}

/**
*
* <!-- cpr_image_malloc -->
*
*/
void* cpr_image_malloc(uint32 size)
{
    void* buf = calloc(1, size);

    CPR_ASSERT(buf);

    return buf;
}

/**
*
* <!-- cpr_image_free -->
*
*/
void cpr_image_free(void* buf)
{
    free(buf);
}

/**
*
* <!-- cpr_image_open_remote_cfg -->
*
*/
void cpr_image_open_remote_cfg(void** cfg, uint32* size)
{
}

/**
*
* <!-- cpr_image_close_remote_cfg -->
*
*/
void cpr_image_close_remote_cfg(void)
{
}

/**
*
* <!-- cpr_image_wait -->
*
*/
void cpr_image_wait(uint32 us)
{
    busywait(us);
}

/**
*
* <!-- cpr_image_mutex_lock -->
*
*/
void cpr_image_mutex_lock(void)
{
    if(!cpr_lock_hdl)
    {
        cpr_lock_hdl = Core_MutexCreate(CORE_MUTEXATTR_DEFAULT);
        CPR_ASSERT(cpr_lock_hdl);
    }

    Core_MutexLock(cpr_lock_hdl);
}

/**
*
* <!-- cpr_image_mutex_unlock -->
*
*/
void cpr_image_mutex_unlock(void)
{
    if(cpr_lock_hdl)
    {
        Core_MutexUnlock(cpr_lock_hdl);
    }
}

/**
*
* <!-- cpr_image_get_eldo_voltage -->
*
*/
uint32 cpr_image_get_eldo_voltage(cpr_domain_id railId, cpr_domain_info* info)
{
    if(railId != CPR_RAIL_CX)
    {
        CPR_LOG_FATAL("Unsupported rail for calculating eLDO voltage: 0x%x", railId);
    }

    return cpr_image_target_get_cx_eldo_voltage(info);
}

/**
*
* <!-- cpr_image_mask_sensors -->
*
*/
void cpr_image_enter_sleep(void)
{
    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return;
    }

    CPR_LOG_INFO("Enter sleep");

    cpr_utils_set_disable_vote(CPR_RAIL_MSS, CPR_DISABLE_VOTE_SLEEP);

    cpr_pre_state_switch(CPR_RAIL_MSS);
}

/**
*
* <!-- cpr_image_unmask_sensors -->
*
*/
void cpr_image_exit_sleep(void)
{
    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return;
    }

    if(cpr_utils_has_disable_vote(CPR_RAIL_MSS, CPR_DISABLE_VOTE_SLEEP))
    {
        CPR_LOG_INFO("Exit sleep: (mode:%u)", next_mode);

        cpr_utils_clear_disable_vote(CPR_RAIL_MSS, CPR_DISABLE_VOTE_SLEEP);

        cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {next_mode}};

        /*
         * Make sure CPR_DISABLE_VOTE_MODE_SWITCH vote is set before calling
         * cpr_post_state_switch().
         *
         * There is a case where cpr_pre_state_switch()/cpr_post_state_switch()
         * are called after cpr_enter_sleep() called and CPR_DISABLE_VOTE_MODE_SWITCH is
         * cleared. It causes an abort in cpr_post_state_switch() since
         * CPR_DISABLE_VOTE_MODE_SWITCH vote is no longer set.
         */
        cpr_utils_set_disable_vote(CPR_RAIL_MSS, CPR_DISABLE_VOTE_MODE_SWITCH);

        cpr_post_state_switch(CPR_RAIL_MSS, &info, cpr_get_voltage(CPR_RAIL_MSS, &info));
    }
}

boolean cpr_image_can_resume_control(cpr_domain_id railId)
{
    cpr_rail_state* rail_state = cpr_utils_get_rail_state(railId);

    return (rail_state->disableVotes == 0);
}

void cpr_image_measurements_start()
{
    /*
     * need to unclamp memory in order to enable sensors
     */
    HWIO_OUTF(MSS_CLAMP_MEM, UNCLAMP_ALL, 0x1);
    HWIO_OUTF(MSS_CLAMP_IO, UNCLAMP_ALL, 0x1);
}

void cpr_image_measurements_stop()
{
    /*
     * reset the clamp
     */
    HWIO_OUTF(MSS_CLAMP_MEM, UNCLAMP_ALL, 0x0);
    HWIO_OUTF(MSS_CLAMP_IO, UNCLAMP_ALL, 0x0);
}

/*==============================================================================
**
** Wrapper for OLD CPR APIs
** TODO: remove old CPR APIs 
**
**==============================================================================
*/

DALResult CPR_GetRailVoltageRecommendation( VCSRailType eRail, VCSCornerType eCorner, uint32 *nVoltageUV )
{
    if (eRail != VCS_RAIL_MSS || eCorner >= VCS_CORNER_NUM_OF_CORNERS || nVoltageUV == NULL)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {cpr_image_target_convert_vcs_to_cpr(eCorner)}};

    *nVoltageUV = cpr_get_voltage(CPR_RAIL_MSS, &info);

    CPR_LOG_TRACE("CPR_GetRailVoltageRecommendation: (mode:%u) (voltage:%u)", info.u.mode, *nVoltageUV);
    next_mode = info.u.mode;
    return DAL_SUCCESS;
}

DALResult CprGetFloorAndCeiling( VCSRailType eRail, VCSCornerType eCorner, uint32* floorVoltageUV, uint32* ceilingVoltageUV )
{
    if (eRail != VCS_RAIL_MSS || eCorner >= VCS_CORNER_NUM_OF_CORNERS || floorVoltageUV == NULL || ceilingVoltageUV == NULL)
    {
        return DAL_ERROR;
    }

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {cpr_image_target_convert_vcs_to_cpr(eCorner)}};

    *ceilingVoltageUV = cpr_get_ceiling_voltage(CPR_RAIL_MSS, &info);
    *floorVoltageUV   = cpr_get_floor_voltage(CPR_RAIL_MSS, &info);

    CPR_LOG_TRACE("CprGetFloorAndCeiling: (mode:%u) (floor:%u) (ceiling:%u)", info.u.mode, *floorVoltageUV, *ceilingVoltageUV);

    return DAL_SUCCESS;
}

DALResult  CPR_GetSafeVoltage(VCSRailType  eRail, VCSCornerType eCorner, uint32 *nSafeVoltageuV)
{
    if (eRail != VCS_RAIL_MSS || eCorner >= VCS_CORNER_NUM_OF_CORNERS || nSafeVoltageuV == NULL)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {cpr_image_target_convert_vcs_to_cpr(eCorner)}};

    *nSafeVoltageuV = cpr_get_safe_voltage(CPR_RAIL_MSS, &info);

    CPR_LOG_TRACE("CPR_GetSafeVoltage: (mode:%u) (voltage:%u)", info.u.mode, *nSafeVoltageuV);

    return DAL_SUCCESS;
}

DALResult CPR_GetEldoVoltageRecommendation(VCSRailType eRail, VCSCornerType eCorner, uint32 *nVoltageUV)
{
    if (eRail != VCS_RAIL_CX || eCorner >= VCS_CORNER_NUM_OF_CORNERS || nVoltageUV == NULL)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {cpr_image_target_convert_vcs_to_cpr(eCorner)}};

    *nVoltageUV = cpr_get_eldo_voltage(CPR_RAIL_CX, &info);

    CPR_LOG_TRACE("CPR_GetEldoVoltageRecommendation: (mode:%u) (voltage:%u)", info.u.mode, *nVoltageUV);

    return DAL_SUCCESS;
}

DALResult CPR_Enable( VCSRailType eRail )
{
    if (eRail != VCS_RAIL_MSS)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_Enable (mode:%u)", next_mode);

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {next_mode}};

    cpr_post_state_switch(CPR_RAIL_MSS, &info, cpr_get_voltage(CPR_RAIL_MSS, &info));

    return DAL_SUCCESS;
}

DALResult CPR_Disable( VCSRailType eRail )
{
    if (eRail != VCS_RAIL_MSS)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_Disable");

    cpr_pre_state_switch(CPR_RAIL_MSS);

    return DAL_SUCCESS;
}

DALResult CPR_RelinquishedControl_Enter(VCSRailType  eRail)
{
    if (eRail != VCS_RAIL_MSS)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_RelinquishedControl_Enter");

    cpr_relinquish_control(CPR_RAIL_MSS);

    return DAL_SUCCESS;
}

DALResult CPR_RelinquishedControl_Exit(VCSRailType  eRail)
{
    if (eRail != VCS_RAIL_MSS)
    {
        return DAL_ERROR_INVALID_PARAMETER;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_RelinquishedControl_Exit (mode:%u)", next_mode);

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {next_mode}};

    cpr_resume_control(CPR_RAIL_MSS, &info, cpr_get_safe_voltage(CPR_RAIL_MSS, &info));

    return DAL_SUCCESS;
}

DALResult CPR_MaskSensors(uint32 mask)
{
    if (mask != CPR_MASK_MSS_MODEM_SLEEP)
    {
        return DAL_ERROR;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_MaskSensors");

    cpr_image_enter_sleep();

    return DAL_SUCCESS;
}

DALResult CPR_UnmaskSensors(uint32 mask)
{
    if (mask != CPR_MASK_MSS_MODEM_SLEEP)
    {
        return DAL_ERROR;
    }

    if(!cpr_utils_is_closed_loop_mode(CPR_RAIL_MSS))
    {
        return DAL_SUCCESS;
    }

    CPR_LOG_TRACE("CPR_UnmaskSensors");

    cpr_image_exit_sleep();

    return DAL_SUCCESS;
}

