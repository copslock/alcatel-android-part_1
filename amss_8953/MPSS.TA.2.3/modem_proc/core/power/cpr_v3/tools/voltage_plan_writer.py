#! /usr/bin/env python
import os
import sys
from __builtin__ import enumerate
from voltage_plan import *

__all__ = ['write_cpr_cfg']

sourceFile = None
debug = False


def writefile(msg):
    if debug: print msg
    if sourceFile: sourceFile.write(msg + '\n')

###############################################################################
# Margins
###############################################################################
writtenMargins = []
MARGIN_ADJUSTMENT_TEMPLATE = '''
cpr_margin_cfg {name} = {{
    .count = {count},
    .data = {data}
}};
'''
MARGINS_TEMPLATE = '{{ .cprRevMin = {min}, .cprRevMax = {max}, .openLoop = {ol}, .closedLoop = {cl}, .maxFloorToCeil = {fToC} }}'

def write_margins(margins):
    global writtenMargins
    marginsDecls = []
    uuid = 0

    if len(margins) == 0:
        return 'NULL'


    for m in margins:
        uuid = abs(reduce(lambda acc, (i,x): acc + (i+1)*x, enumerate(m.__dict__.values()), uuid))
        marginsDecls.append(MARGINS_TEMPLATE.format(**m.__dict__))

    name = 'margins_' + str(uuid)

    formatVals = {
        'name': name,
        'count': len(marginsDecls),
        'data': '(cpr_margin_data[]) { ' + ',\n                                  '.join(marginsDecls) + ' }',
    }

    if name not in writtenMargins:
        writtenMargins.append(name)
        writefile(MARGIN_ADJUSTMENT_TEMPLATE.format(**formatVals))

    return '&' + name

###############################################################################
# Fuses
###############################################################################
writtenFuses = []
FUSE_DATA_TEMPLATE = 'CPR_FUSE_MAPPING({0}, {1})'
INDIVIDUAL_FUSE_CFG_TEMPLATE = '''(cpr_fuse[]) {{ {{.count = {count}, .data = (struct raw_fuse_data[]) {{ {data} }} }} }}'''
FUSE_CFG_TEMPLATE = '''
cpr_fuse_data {0} =
{{
    .volt       = {1},
    .quot       = {2},
    .rosel      = {3},
    .quotOffset = {4},
    .voltOffset = {5},
}};
'''


def write_fuses(allFuses):
    global writtenFuses
    uuid = 0
    allfuseparams = []

    if len(allFuses) == 0:
        return 'NULL'

    for fuses in allFuses:
        fuseparams = []

        if not fuses:
            allfuseparams.append('NULL')
            continue

        for f in fuses:
            uuid += hash(f) & (1 << 32) - 1
            fuseparams.append(FUSE_DATA_TEMPLATE.format(*f))

        formatVals = {
            'data': ', '.join(fuseparams),
            'count': len(fuseparams),
        }
        allfuseparams.append(INDIVIDUAL_FUSE_CFG_TEMPLATE.format(**formatVals))

    if len(allfuseparams) > 0:
        name = 'fuses_' + str(uuid)

        if name not in writtenFuses:
            writtenFuses.append(name)
            writefile(FUSE_CFG_TEMPLATE.format(name, *allfuseparams))

        return '&' + name

    return 'NULL'

###############################################################################
# Quotients
###############################################################################
writtenQuots = []
QUOTIENT_DATA_TEMPLATE = '{{ .ro = {ro:2}, .quotient = {quot:4} }}'
QUOTIENT_CFG_TEMPLATE = '''
cpr_quotient_cfg {name} =
{{
    .count = {count},
    .quots = (cpr_quotient[]) {{ {quots} }},
    .kvs = (uint32[]) {{ {kvs} }},
}};
'''


def write_quotients(quots):
    global writtenQuots
    uuid = 0
    quotparams = []

    if len(quots) == 0:
        return 'NULL'

    for q in quots:
        uuid += hash(q) & (1 << 32) - 1
        quotparams.append(QUOTIENT_DATA_TEMPLATE.format(**q.__dict__))

    name = 'quotients_' + str(uuid)

    formatVals = {
        'name': name,
        'quots': ',\n                                '.join(quotparams),
        'kvs': ', '.join(str(q.kv) for q in quots),
        'count': len(quots),
    }

    if name not in writtenQuots:
        writtenQuots.append(name)
        writefile(QUOTIENT_CFG_TEMPLATE.format(**formatVals))

    return '&' + name

###############################################################################
# Voltage Plan
###############################################################################
writtenVPs = []
MODE_PARAMS_TEMPLATE = '''
cpr_voltage_plan {name} =
{{
    .modesCount = {supported_count},
    .idxLookupFunc = {lookup_func},
    .tempAdjControls = (cpr_temp_adjust_controls[]) {{ {temp_adj_controls} }},
    .supportedModes = (cpr_voltage_mode[]) {{ {supported_modes} }},
    .minMode = {min_mode},
    .modes = (struct cpr_voltage_data[]) {{{data}}},
}};
'''

VOLTAGE_DATA_TEMPLATE = '''
        // {mode}
        {{.fref = {fuseRef}, .freqDelta = {f_delta}, .fuses = {fuses_ptr}, .quotients = {quots_ptr}, .margins = {margin_list},
         .marginModeTableSize = {marginModeTableSize},
         .marginModeTable = (cpr_temp_adjust_temp_bands[]) {{ {margin_mode_table} }},
         .subModesCount = {sub_modes_cnt},
         .subModes = (struct cpr_freq_data[]) {{ {sub_modes} }} }}'''

TEMP_MARGIN_CONTROLS = ''' 
        {{
        .tempAdjTempPoints = (cpr_temp_adjust_temp_points[]) {{ {temp_point} }},
        .tempAdjMiscReg = (cpr_temp_adjust_misc_reg[]) {{ {misc_reg} }},
        .tempAdjCtl = (cpr_temp_adjust_margin_adj_ctl[]) {{ {adj_ctl} }},
        }}'''

VOLTAGE_SUB_MODE_DATA_TEMPLATE = '{{.ceiling = {1:7}, .floor = {2:7}, .freq = {0:7}}}'

TEMP_BAND_TEMPLATE = '{{ .tempBand0 = {0:}, .tempBand1 = {1:}, .tempBand2 = {2:}, .tempBand3 = {3:} }}'

TEMP_POINT_TEMPLATE = '''{{ .cprMarginTempPoint0 = {0:}, .cprMarginTempPoint1 = {1:}, .cprMarginTempPoint2 = {2:} }}'''

MISC_REG_TEMPLATE = '''{{ .tempSensorStartId = {0:}, .tempSensorEndId = {1:} }}'''

MARGIN_ADJ_CTL_TEMPLATE = '''{{ .singleStepQuot = {0:}, .perRoKvMarginEn = {1:} }}'''

def write_mode_params(modes, swSettings, currentRail):
    global writtenVPs
    uuid = 0
    supportedModes = []
    modeDecls = []
    tempPoints = []
    foundLowestDisabledMode = False  # used for interpolation
    tempEnableFeature = 1

    # Iterate over all modes
    for i, (modeId, mode) in enumerate(modes):
        subModeDecls = []
        tempBandParams = []
        lowestDisabledMode = None
        marginModeSize = 0;

        if modeId.lower() == 'retention':
            continue

        # Iterate over all submodes
        for e, fq, c, fl in zip(mode.enabled, mode.freq, mode.ceiling, mode.floor):
            if e:
                subModeDecls.append(VOLTAGE_SUB_MODE_DATA_TEMPLATE.format(fq, c, fl))
            elif not foundLowestDisabledMode and not lowestDisabledMode:
                subModeDecls.append(VOLTAGE_SUB_MODE_DATA_TEMPLATE.format(fq, c, fl))
                lowestDisabledMode = True

        if len(subModeDecls) == 0:
            continue

        uuid += hash(mode) & (1 << 32) - 1

        if not lowestDisabledMode:
            supportedModes.append('CPR_VOLTAGE_MODE_' + modeId)

        fDelta = 0
        if i > 0 and mode.freq[-1] > 0:
            for id, m in modes[:i][::-1]:
                if m.freq[-1] > 0:
                    fDelta = mode.freq[-1] - m.freq[-1]
                    if m.enabled[-1]: break

        if len(mode.coreTable) != 0:
            for ct in mode.coreTable:
                if ct[0] == None or ct[1] == None or ct[2] == None or ct[3] == None:
                    break;
                tempBandParams.append(TEMP_BAND_TEMPLATE.format(ct[0], ct[1], ct[2], ct[3]))
                marginModeSize = marginModeSize + 1
        else:
            tempBandParams.append(TEMP_BAND_TEMPLATE.format('NULL', 'NULL', 'NULL', 'NULL'))
            tempEnableFeature = 0

        formatVals = {
            'mode': modeId,
            'margin_list': write_margins(mode.margins),
            'f_delta': fDelta,
            'quots_ptr': write_quotients(mode.quotients),
            'fuses_ptr': write_fuses(mode.fuses),
            'margin_mode_table' : ',\n                                                    '.join(tempBandParams),
            'marginModeTableSize' : marginModeSize,
            'sub_modes_cnt': len(subModeDecls),
            'sub_modes': (',\n' + ' ' * 48).join(subModeDecls)
        }
        formatVals.update(mode.__dict__)

        if not lowestDisabledMode:
            modeDecls.append(VOLTAGE_DATA_TEMPLATE.format(**formatVals))
        else:
            foundLowestDisabledMode = '(struct cpr_voltage_data[]) {{{0}}}'.format(VOLTAGE_DATA_TEMPLATE.format(**formatVals)) 

    if swSettings[currentRail].get('CPR_MARGIN_TEMP_POINT0N1.POINT0') == None or swSettings[currentRail].get('CPR_MARGIN_TEMP_POINT0N1.POINT1') == None \
    or swSettings[currentRail].get('CPR_MARGIN_TEMP_POINT2.POINT2') == None or swSettings[currentRail].get('CPR_MISC_REGISTER.TEMP_SENSOR_ID_START') == None \
    or swSettings[currentRail].get('CPR_MISC_REGISTER.TEMP_SENSOR_ID_END') == None or swSettings[currentRail].get('CPR_MARGIN_ADJ_CTL.KV_MARGIN_ADJ_SINGLE_STEP_QUOT') == None \
    or swSettings[currentRail].get('CPR_MARGIN_ADJ_CTL.PER_RO_KV_MARGIN_EN') == None:
        tempEnableFeature = 0

    if tempEnableFeature:
        tempPoint = TEMP_POINT_TEMPLATE.format(
            swSettings[currentRail]['CPR_MARGIN_TEMP_POINT0N1.POINT0'], swSettings[currentRail]['CPR_MARGIN_TEMP_POINT0N1.POINT1'], 
            swSettings[currentRail]['CPR_MARGIN_TEMP_POINT2.POINT2'])

        miscReg = MISC_REG_TEMPLATE.format(
            swSettings[currentRail]['CPR_MISC_REGISTER.TEMP_SENSOR_ID_START'], swSettings[currentRail]['CPR_MISC_REGISTER.TEMP_SENSOR_ID_END'])

        marginAdjCtl = MARGIN_ADJ_CTL_TEMPLATE.format(
            swSettings[currentRail]['CPR_MARGIN_ADJ_CTL.KV_MARGIN_ADJ_SINGLE_STEP_QUOT'], swSettings[currentRail]['CPR_MARGIN_ADJ_CTL.PER_RO_KV_MARGIN_EN'])

        formatVals = {
            'temp_point': tempPoint,
            'misc_reg' : miscReg,
            'adj_ctl' : marginAdjCtl,
        }
        tempAdjControls = TEMP_MARGIN_CONTROLS.format(**formatVals)
    else:
        tempAdjControls = 'NULL'

    name = 'voltage_plan_' + str(uuid)

    formatVals = {
        'name': name,
        'supported_count': len(supportedModes),
        'lookup_func': createIdxFunction('cpr_get_mode_idx', 'cpr_voltage_mode', supportedModes, True),
        'temp_adj_controls' : tempAdjControls,
        'supported_modes': ',\n                                             '.join(supportedModes),
        'min_mode': foundLowestDisabledMode if foundLowestDisabledMode else 'NULL',
        'data': ',\n'.join(modeDecls),
    }

    if name not in writtenVPs:
        writtenVPs.append(name)
        writefile(MODE_PARAMS_TEMPLATE.format(**formatVals))

    return '&' + name

###############################################################################
# Aging Config
###############################################################################
writtenAgingCfg = []
AGING_CONFIG_TEMPLATE = '''
cpr_aging_cfg {name} =
{{
    .modeToRun     = {mode},
    .sensorID      = {sensor_id},
    .kv            = {kv_x100},
    .scalingFactor = {scaling_factor_x10},
    .marginLimit   = {margin_limit}
}};
'''

def write_aging_config(rail, aging_cfg):
    global writtenAgingCfg
    uuid = 0

    if len(aging_cfg) == 0:
        return 'NULL'

    for a in aging_cfg.items():
        uuid += hash(a) & (1 << 32) - 1

    #name = 'aging_' + str(uuid)
    name = 'aging_cfg_' + rail.lower()

    formatVals = {
        'name'               : name,
        'mode'               : 'CPR_VOLTAGE_MODE_%s' % aging_cfg['mode'],
        'sensor_id'          : aging_cfg['sensor_id'],
        'kv_x100'            : aging_cfg['kv_x100'],
        'scaling_factor_x10' : aging_cfg['scaling_factor_x10'],
        'margin_limit'       : aging_cfg['margin_limit'],
    }

    if name not in writtenAgingCfg:
        writtenAgingCfg.append(name)
        writefile(AGING_CONFIG_TEMPLATE.format(**formatVals))

    return '&' + name

###############################################################################
# SW Settings
###############################################################################
writtenSwSettings = []
SW_SETTINGS_TEMPLATE = '''
cpr_sw_settings {name} =
{{
    .stepquotmax          = {step_quot_max},
    .stepquotmin          = {step_quot_min},
    .autocontinterval     = {auto_cont_interval},
    .upthresh             = {up_thresh},
    .dnthresh             = {dn_thresh},
    .consecup             = {consec_up},
    .consecdn             = {consec_dn},
    .clamptimerinterval   = {clamp_timer_interval},
    .sensorbypass         = {sensor_bypass},
    .sensormask           = {sensor_mask},
    .countrepeat          = {count_repeat},
    .countmode            = {count_mode},
    
}};
'''
#TODO hash value calculation
def write_sw_settings(rail, sw_settings):
    global writtenSwSettings
    gt = sw_settings.get('gcnt', [])

    if len(sw_settings) == 0:
        return 'NULL'

    name = 'sw_settings' 

    formatVals = {
        'name'                  : name,
        'step_quot_max'         : sw_settings['step_quot_max'],
        'step_quot_min'         : sw_settings['step_quot_min'],
        'auto_cont_interval'    : sw_settings['auto_cont_interval'],
        'up_thresh'             : sw_settings['up_thresh'],  
        'dn_thresh'             : sw_settings['dn_thresh'],
        'consec_up'             : sw_settings['consec_up'],
        'consec_dn'             : sw_settings['consec_dn'],
        'idle_clocks'           : sw_settings['idle_clocks'],
        'clamp_timer_interval'  : sw_settings['clamp_timer_interval'],
        'sensor_bypass'         : sw_settings['sensor_bypass'],
        'sensor_mask'           : sw_settings['sensor_mask'],
        'count_repeat'          : sw_settings['count_repeat'],
        'count_mode'            : sw_settings['count_mode'],
        'gcnts'                 : ('(uint32[]){{ {0} }}'.format(','.join(str(v) for v in gt)), 'NULL')[len(gt) == 0],
        #'gcnts'                 : sw_settings['gcnt'],        
    }

    if name not in writtenSwSettings:
        writtenSwSettings.append(name)
        writefile(SW_SETTINGS_TEMPLATE.format(**formatVals))

    return '&' + name

###############################################################################
# Index Function
###############################################################################
GET_IDX_TEMPLATE = '''
uint32 {name}({type} val)
{{
    switch(val) {{
        {mapping}
        default:
            CPR_LOG_FATAL("Unsupported voltage mode: 0x%x", val);
            break;
    }}

    CPR_ASSERT(0);
    return 0;
}}
'''
writtenIdxFuncs = { }

ALL_SUPPORTED_MODES = [
    'CPR_VOLTAGE_MODE_MIN_SVS',
    'CPR_VOLTAGE_MODE_LOW_SVS',
    'CPR_VOLTAGE_MODE_SVS',
    'CPR_VOLTAGE_MODE_SVS_L1',
    'CPR_VOLTAGE_MODE_NOMINAL',
    'CPR_VOLTAGE_MODE_NOMINAL_L1',
    'CPR_VOLTAGE_MODE_TURBO',
    'CPR_VOLTAGE_MODE_SUPER_TURBO',
    'CPR_VOLTAGE_MODE_SUPER_TURBO_NO_CPR',
]

def createIdxFunction(name, type, values, reusable=False):
    if reusable:
        k = (name, type) + tuple(values)
        if k in writtenIdxFuncs:
            return writtenIdxFuncs[k]

        name += '_' + str(len(writtenIdxFuncs))
        writtenIdxFuncs[k] = name

    mapping = []
    idx = 0
    for mode in ALL_SUPPORTED_MODES:
        s = 'case %s:' % (mode)
        if (idx < len(values)-1 and mode == values[idx]) or (mode == ALL_SUPPORTED_MODES[-1]):
            s = '%s return %d;' % (s, idx)
            idx += 1
        mapping.append(s)
    writefile(GET_IDX_TEMPLATE.format(name=name,
                                      type=type,
                                      mapping='\n        '.join(mapping)))

    return name

###############################################################################
# Versioned Voltage Plan
###############################################################################
VERSIONED_VOLTAGE_PLAN_TEMPLATE = '''
static cpr_versioned_voltage_plan {name} = {{
    .rail    = {rail},
    .list = (struct voltage_plan_list[]) {{{data}    }},
    .count = {count},
    .agingCfg = {aging_cfg}
}};
'''
VOLTAGE_PLAN_TEMPLATE = '''
        {{
            .version = {{ CPR_FOUNDRY_{foundry}, CPR_CHIPINFO_VERSION({minminor},{minmajor}), CPR_CHIPINFO_VERSION({maxminor},{maxmajor}) }},
            .cfg  = {cfg}
        }},
'''
GET_VVP_FUNC_TEMPLATE = '''
const cpr_versioned_voltage_plan* cpr_get_versioned_voltage_plan(cpr_domain_id railId)
{{
    switch(railId)
    {{
        {0}
        default:
            CPR_LOG_FATAL("Unsupported railId: 0x%x", railId);
            break;
    }}

    CPR_ASSERT(0);
    return NULL;
}}
'''


def write_cpr_cfg(rails, sourcefile, destfile, enab_info=None):
    global sourceFile
    sourceFile = open(destfile, 'wb')

    writefile('#include "cpr_voltage_plan.h"')
    writefile('#include "cpr_logs.h"')
    writefile('#include "cpr_cfg.h"')
    writefile('#include "cpr_fuses_hwio.h"')

    allvps = { }
    vps, hashval = get_voltage_plan(rails, sourcefile, enab_info)

    writefile('\n//hash value of Voltage Plan file (extracted all cell values)')
    writefile('const char cpr_voltage_plan_hash_value[] = "%s";\n' % hashval)

    for k in vps:
        vp = vps[k]
        if isinstance(vp, dict):
            continue
        currentRail = vp.rail
        vp.rail = 'CPR_RAIL_' + vp.rail

        try:
            formatVals = {
            'cfg': write_mode_params(vp.modes, vps['sw_settings'], currentRail),
        }
        except Exception, e:
            formatVals = {
                'cfg': write_mode_params(vp.modes, 'NULL', currentRail),
            }
        formatVals.update(vp.__dict__)
        allvps.setdefault(vp.rail, []).append(VOLTAGE_PLAN_TEMPLATE.format(**formatVals))

    def vvpName(k):
        return k.lower() + '_vvp'

    aging_cfg_list = {}
    for k in allvps:
        rail = k.split('_')[-1]
        if vps.has_key('aging') and vps['aging'].has_key(rail):
            aging_cfg_list[k] = write_aging_config(rail, vps['aging'][rail])
    
    sw_settings_list = {}
    for k in allvps:
        rail = k.split('_')[-1]
        if vps.has_key('sw_settings') and vps['sw_settings'].has_key(rail):
            sw_settings_list[k] = write_sw_settings(rail, vps['sw_settings'][rail])

    for k in allvps:
        formatVals = {
            'name': vvpName(k),
            'rail': k,
            'data': ''.join(x for x in allvps[k]),
            'count': len(allvps[k]),
            'aging_cfg': 'NULL' if not aging_cfg_list.has_key(k) else aging_cfg_list[k],
        }
        writefile(VERSIONED_VOLTAGE_PLAN_TEMPLATE.format(**formatVals))

    writefile(GET_VVP_FUNC_TEMPLATE.format('\n        '.join(('case {0}: return &{1};'.format(k, vvpName(k)) for k in allvps))))

    sourceFile.close()

###############################################################################
# Main
###############################################################################
if __name__ == '__main__':
    tgt = 'test'
    rails = None

    if len(sys.argv) >= 4:
        rails = sys.argv[3:]

    if len(sys.argv) >= 3:
        cfg = sys.argv[1]
        output = sys.argv[2]
    else:
        # debug = True
        # rails = ['CX', 'MX', 'VDDA', 'SSC_MX', 'SSC_CX']
        cfg = '../target/{0}/Voltage Plan - {0}.xlsx'.format(tgt)
        output = '../build/out/gen/cpr_voltage_plan.c'
        pass

    write_cpr_cfg(rails, cfg, output)
