#! /usr/bin/env python
import os
import sys
from enablement import *

__all__ = ['write_cpr_cfg']

sourceFile = None
headerFile = None

debug = False


def error(msg):
    print msg
    sys.exit(1)


def writehdr(msg=''):
    if debug: print msg
    if headerFile: headerFile.write(msg + '\n')


def writesrc(msg=''):
    if debug: print msg
    if sourceFile: sourceFile.write(msg + '\n')

###############################################################################
# Misc Cfg
###############################################################################
MISC_CFG_TEMPLATE = '''
cpr_misc_cfg cpr_misc = {{
    .cprRev = {0}
}};
'''
def write_misc_cfg(vals):
    writehdr('extern cpr_misc_cfg cpr_misc;')
    if not vals['cpr_rev_fuse'] or vals['cpr_rev_fuse'] == 'NULL':
        fuseDecl = '{.count = 0, .data = NULL }'
    else:
        fuseDecl = '{{.count = 1, .data = (struct raw_fuse_data[]) {{  {0} }} }}'.format(vals['cpr_rev_fuse'])
    writesrc(MISC_CFG_TEMPLATE.format(fuseDecl))


###############################################################################
# Controller Cfg
###############################################################################
CONTROLLER_CFG_TEMPLATE = '''
static cpr_controller {name} = {{
    .hal      = {{ .base = {base} }},
    .refClk  = "{ref_clk}",
    .ahbClk  = "{ahb_clk}",
    .stepQuotMin = {step_quot_min},
    .stepQuotMax = {step_quot_max},
    .bypassSensors       = {bypassedSensors},
    .bypassSensorsCount  = {bypassCount},
    .disableSensors      = {disabledSensors},
    .disableSensorsCount = {disableCount},
}};
'''


def write_controller_cfg(name, vals):
    bs = vals.get('bypassed_sensors', [])
    ds = vals.get('disabled_sensors', [])
    #sr = vals.get('supported_rails', [])

    name = name.lower() + '_controller'

    formatVals = {
        'name': name,
        'ref_clk': '',
        'ahb_clk': '',
        'bypassedSensors': ('(uint8[]){{ {0} }}'.format(','.join(str(v) for v in bs)), 'NULL')[len(bs) == 0],
        'disabledSensors': ('(uint8[]){{ {0} }}'.format(','.join(str(v) for v in ds)), 'NULL')[len(ds) == 0],
        #'supportedRails' : ('(cpr_domain_id[]){{ {0} }}'.format(','.join('CPR_RAIL_%s'%(v) for v in sr)), 'NULL')[len(sr) == 0],
        'bypassCount': len(bs),
        'disableCount': len(ds),
        #'supportedRailsCount': len(sr),
    }
    formatVals.update(vals)
    writesrc(CONTROLLER_CFG_TEMPLATE.format(**formatVals))

    return name


###############################################################################
# Enablements
###############################################################################
VERSION_TEMPLATE = '{{CPR_{0}, CPR_CHIPINFO_VERSION({1}), CPR_CHIPINFO_VERSION({2})}}'
VERSION_RANGE_TEMPLATE = '{{ .versions = (cpr_version[]){{ {0} }},\n                  .count = {1} }}'
ENABLEMENT_TEMPLATE = '''
static cpr_enablement {name} =
{{
    .id       = {id},
    .funcs    = &{control_mode},
    .versions = {vlist},
    .dynamicfloor       = {dynamic_floor},
    .enabletempadj      = {enable_temp_adj},
    .testMargin         = {test_margin},
    .fuseMultiplier     = {fuse_multiplier},
    .stepSize           = {step_size},
    .thermalAdjustment  = {{ {thermal} }},
}};
'''


def write_enablement(i, id, vals, versions):
    versionId = vals['version']
    if versionId not in versions:
        error('Invalid version reference "{0}" on enablement "{1}"'.format(versionId, id))

    def x(val): return str(val).replace('.', ',')

    name = id.lower() + '_enablement_' + str(i)
    versionStrings = [VERSION_TEMPLATE.format(v[0], x(v[1]), x(v[2])) for v in versions[versionId]]

    formatVals = {
        'id': id,
        'name': name,
        'thermal': ', '.join(map(str, vals.get('thermal_regions', [0]))),
        'vlist': VERSION_RANGE_TEMPLATE.format(',\n                                               '.join(versionStrings), len(versionStrings))
    }
    formatVals.update(vals)
    writesrc(ENABLEMENT_TEMPLATE.format(**formatVals))

    return name

###############################################################################
# Rail Cfg
###############################################################################
V2_RAIL_CFG_TEMPLATE = '// .verCfg.v2 = nothing needed'
V3_RAIL_CFG_TEMPLATE = '.verCfg.v3 = {{ .sensors = {0}, .sensorsCount = {1} }}'
RAIL_CFG_TEMPLATE = '''
static cpr_rail {name} = {{
    .id  = {id},
    .name = "{debugName}",
    .hal = {{ .base = {base}, .thread = {thread}, .type = {type} }},
    .interruptId = {interrupt},
    .halRailCfg = {{
        .upThresh = {up_thresh},
        .dnThresh = {dn_thresh},
        .consecUp = {consec_up},
        .consecDn = {consec_dn},
        {halCfg}
    }},
    .settleModes      = {settleList},
    .settleModesCount = {settleCount},
}};
'''
RAIL_CFG_TEMPLATE_NO_HW = '''
static cpr_rail {name} = {{
    .id  = {id},
    .name = "{debugName}",
}};
'''


def write_rail_cfg(enumId, id, vals, controller):
    name = enumId.lower() + '_cfg'

    if 'thread' in vals:
        s = vals.get('sensors', [])
        if len(s) > 0:
            sarraytemplate = '(uint8[]) {{ {0} }}'.format(','.join(str(v) for v in s))
        else:
            sarraytemplate = 'NULL'
        vtemplate = V3_RAIL_CFG_TEMPLATE.format(sarraytemplate, len(s))
        name += '_v3'
    else:
        vtemplate = V2_RAIL_CFG_TEMPLATE
        name += '_v2'
        vals['thread'] = 0  # Doesnt matter for v2

    if 'settle_modes' in vals:
        SETTLE_TEMPLATE = '(cpr_voltage_mode[]) {{ {0} }}'.format(', '.join(vals['settle_modes']))
    else:
        SETTLE_TEMPLATE = 'NULL'

    formatVals = {
        'id': enumId,
        'name': name,
        'debugName': id,
        'halCfg': vtemplate,
        'settleList': SETTLE_TEMPLATE,
        'settleCount': len(vals.get('settle_modes', [])),
    }
    formatVals.update(vals)
    if controller is not None:
        formatVals.update(controller)
        writesrc(RAIL_CFG_TEMPLATE.format(**formatVals))
    else:
        writesrc(RAIL_CFG_TEMPLATE_NO_HW.format(**formatVals))

    return name

###############################################################################
# Main output
###############################################################################
GET_RAIL_FUNC_TEMPLATE_SIG = 'cpr_rail* cpr_get_rail(cpr_domain_id railId)'
GET_RAIL_FUNC_TEMPLATE = GET_RAIL_FUNC_TEMPLATE_SIG + '''
{{
    switch(railId)
    {{
        {0}
        default:
            CPR_LOG_FATAL("Unsupported railId 0x%x", railId);
            break;
    }}

    CPR_ASSERT(0);
    return NULL;
}}
'''
GET_RAIL_IDX_FUNC_TEMPLATE_SIG = 'uint32 cpr_get_rail_idx(cpr_domain_id railId)'
GET_RAIL_IDX_FUNC_TEMPLATE = GET_RAIL_IDX_FUNC_TEMPLATE_SIG + '''
{{
    switch(railId)
    {{
        {0}
        default:
            CPR_LOG_FATAL("Unsupported railId 0x%x", railId);
            break;
    }}

    CPR_ASSERT(0);
    return 0;
}}
'''


def write_cpr_cfg(img, input, src, hdr):
    global sourceFile, headerFile
    if src: sourceFile = open(src, 'wb')
    if hdr: headerFile = open(hdr, 'wb')

    writesrc('#include "{0}"'.format(os.path.basename(hdr)))
    writesrc('#include "cpr_image_target_init.h"')
    writesrc('#include "cpr_cfg.h"')
    writesrc('#include "cpr_target_hwio.h"')
    writesrc('#include "cpr_logs.h"')

    #versions, rails, controllers, enablements, misc = get_cpr_enablements(input)
    enab_cfg = get_cpr_enablements(input)

    writesrc('\n//hash value of enablement.cfg')
    writesrc('const char cpr_enablement_cfg_hash_value[] = "%s";\n' % enab_cfg.hashval)

    # Filter out enablements we dont care about in this build
    for id in enab_cfg.enablements.keys():
        newList = [l for l in enab_cfg.enablements[id] if img in l['images']]
        if len(newList) > 0:
            enab_cfg.enablements[id] = newList
        else:
            del enab_cfg.enablements[id]

    railsToWrite = [k for k in enab_cfg.rails if k in enab_cfg.enablements]
    controllersToWrite = []

    for c in enab_cfg.controllers:
        if not enab_cfg.controllers[c].has_key('supported_rails'):
            enab_cfg.controllers[c]['supported_rails'] = [r for r in railsToWrite if enab_cfg.rails[r]['controller'] == c]

    for c in enab_cfg.controllers:
        for r in enab_cfg.controllers[c]['supported_rails']:
            if any([e.get('require_controller','true')=='true' for e in enab_cfg.enablements[r]]):
                controllersToWrite.append(c)
                break

    controllernames = []
    for id, vals in ((k, enab_cfg.controllers[k]) for k in controllersToWrite):
        controllernames.append(write_controller_cfg(id, vals))

    railnames = { }
    for id, vals in ((k, enab_cfg.rails[k]) for k in railsToWrite):
        enumId = 'CPR_RAIL_' + id
        ctrlId = vals['controller']
        if ctrlId not in enab_cfg.controllers and any([enab_cfg.get('require_controller','true')=='true' for enab_cfg in enab_cfg.enablements[id]]):
            error('Controller "{0}" not found for rail {1}', ctrlId, id)

        controller = enab_cfg.controllers[ctrlId] if ctrlId in controllersToWrite else None

        railnames[enumId] = write_rail_cfg(enumId, id, vals, controller)

    enablementnames = []
    for id, elist in enab_cfg.enablements.items():
        id = 'CPR_RAIL_' + id
        for i, vals in enumerate(elist):
            enablementnames.append(write_enablement(i, id, vals, enab_cfg.versions))

    # Just trying to manipulate the order for prettier debugging
    # GET_RAIL_IDX_FUNC_TEMPLATE determines the order of the rails in the global data struct
    sortedrailnames = sorted(railnames.keys(), key=lambda x: (len(x), x))

    writesrc(GET_RAIL_FUNC_TEMPLATE.format('\n        '.join(('case {0}: return &{1};'.format(k, railnames[k]) for k in railnames))))
    writesrc(GET_RAIL_IDX_FUNC_TEMPLATE.format('\n        '.join(('case {0}: return {1};'.format(k, i) for i, k in enumerate(sortedrailnames)))))

    writesrc('cpr_rail* cpr_rails[] = {{ {0} }};'.format(', '.join('&' + r for r in railnames.values())))
    if controllernames:
        writesrc('cpr_controller* cpr_controllers[] = {{ {0} }};'.format(', '.join('&' + r for r in controllernames)))
    else:
        writesrc('cpr_controller* cpr_controllers[] = { NULL };')
    writesrc('cpr_enablement* cpr_enablements[] = {{ {0} }};'.format(', '.join('&' + r for r in enablementnames)))

    # Header file
    writehdr('#ifndef CPR_ENABLEMENT_H')
    writehdr('#define CPR_ENABLEMENT_H')
    writehdr()
    writehdr('#include "cpr_cfg.h"')
    writehdr()

    write_misc_cfg(enab_cfg.misc)

    writehdr('#define CPR_NUM_ENABLEMENTS ' + str(len(enablementnames)))
    writehdr('#define CPR_NUM_CONTROLLERS ' + str(len(controllernames)))
    writehdr('#define CPR_NUM_RAILS ' + str(len(railnames)))
    writehdr()
    writehdr(GET_RAIL_FUNC_TEMPLATE_SIG + ';')
    writehdr(GET_RAIL_IDX_FUNC_TEMPLATE_SIG + ';')
    writehdr('extern cpr_rail* cpr_rails[];')
    writehdr('extern cpr_controller* cpr_controllers[];')
    writehdr('extern cpr_enablement* cpr_enablements[];')
    writehdr()
    writehdr('#endif')

    sourceFile.close()

    return railsToWrite, enab_cfg

###############################################################################
# Main
###############################################################################
if __name__ == '__main__':
    img = 'test'
    cfg = '../target/test/enablement.cfg'
    src = '../build/out/gen/cpr_enablement.c'
    hdr = '../build/out/gen/cpr_enablement.h'

    if len(sys.argv) == 5:
        img = sys.argv[1]
        cfg = sys.argv[2]
        src = sys.argv[3]
        hdr = sys.argv[4]
    else:
        debug = True

    write_cpr_cfg(img, cfg, src, hdr)
