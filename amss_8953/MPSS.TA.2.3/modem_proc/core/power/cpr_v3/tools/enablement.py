#! /usr/bin/env python

import os, sys, textwrap, operator
from unittest import main, TestCase
from pyparsing import *
import hashlib

__all__ = ['get_cpr_enablements']

# *******************************************************************************
# CPR Config Grammar
# *******************************************************************************

# Primitives
from pyparsing import Optional

text                =  Word(alphas + "-_", alphanums + "-_")
nullableText        =  text.copy().setParseAction(lambda t: ('', t[0])[t[0] != '-'])
hexNumber           =  Combine(Literal('0x') + Word(hexnums)).setParseAction(lambda t: int(t[0], 16))
decimalNumber       =  Combine(Optional('-') + Word(nums)).setParseAction(lambda t: int(t[0]))
number              =  hexNumber | decimalNumber
numberRange         =  (number + Literal('..').suppress() + number).setParseAction(lambda t: range(t[0], t[1] + 1))  # 1..5
complexNumberRange  =  delimitedList(numberRange ^ number)  # 0..7, 8, 10..12
boolean             =  (number | text).setParseAction(lambda t: ('true', 'false')[t[0] in ['false', 'f', 'False', 'FALSE', 0]])
wild                =  Literal('*')
wildText            =  wild | text
floatNumber         =  Combine(number + Literal('.') + number).setParseAction(lambda t: float(t[0]))
dash                =  Literal('-').suppress()
upperText           =  Word(alphas.upper() + "_", alphanums.upper() + "_")
lBracket            =  Literal('[').suppress()
rBracket            =  Literal(']').suppress()
equals              =  Literal('=')
quotedToken         =  QuotedString('"')

# CPR primitives
def keyValAssign(name, value=number, d=None):
    if d is not None:
        return Optional(Suppress(Keyword(name) + equals) + Optional(value(name), default=d))
    return Suppress(Keyword(name) + equals) + value(name)

def keyValNumberList(name, d=None):
    return keyValAssign(name, Group(delimitedList(number)), d)


def namedTag(name=upperText, params=Empty(), suppress=False):
    return lBracket + (Suppress(Keyword(name)('tag')) if suppress else Keyword(name)('tag')) + params + rBracket


idToken = keyValAssign('id', quotedToken)
versionToken = keyValAssign('version', quotedToken)

# Lines
versionLine = Group(text('name') + wildText('foundary') + floatNumber('min') + dash + floatNumber('max'))
enablementLine = Group(text('version') + text('control_mode') + Optional(number, default=0)('test_margin'))

# Tables
versionTable = Group(OneOrMore(versionLine))('table')

# Rail & Controller Parameters
v3RailParams = (keyValNumberList('sensors', []) ^ keyValAssign('sensors', complexNumberRange, [])) & keyValAssign('thread')
railParams = Group(
    Optional(v3RailParams) &
    keyValAssign('controller', quotedToken) &
    Optional(keyValAssign('settle_modes', delimitedList(text))) &
    Each(map(keyValAssign, ['interrupt', 'up_thresh', 'dn_thresh', 'consec_up', 'consec_dn'])))('params')

enablementParams = Group(
    keyValAssign('images', Group(delimitedList(text))) &
    keyValAssign('control_mode', text) &
    keyValAssign('dynamic_floor', boolean, 'true') &
    keyValAssign('enable_temp_adj', boolean, 'true') &
    keyValAssign('require_controller', boolean, 'true') &
    keyValNumberList('thermal_regions', [0]) &
    Each(map(keyValAssign, ['step_size', 'test_margin', 'fuse_multiplier', 'aging_margin'])))('params')

controllerParams = Group(
    keyValAssign('type', text) &
    keyValAssign('ref_clk', quotedToken, '') &
    keyValAssign('ahb_clk', quotedToken, '') &
    keyValNumberList('disabled_sensors', []) &
    keyValNumberList('bypassed_sensors', []) &
    #keyValNumberList('supported_rails', []) &
    keyValAssign('base', SkipTo(LineEnd())) &
    Each(map(keyValAssign, ['gcnt', 'step_quot_min', 'step_quot_max'])))('params')

miscParams = Group(
    keyValAssign('cpr_rev_fuse', SkipTo(LineEnd()) + LineEnd().suppress()))('params')

# Tags
versionTag = 'VERSIONS'
railTag = 'RAIL'
controllerTag = 'CONTROLLER'
enablementTag = 'ENABLEMENT'
miscTag = 'MISC'

# Block Level
versionBlock = Group(namedTag(versionTag, Optional(idToken)) + versionTable)
railBlock = Group(namedTag(railTag, idToken) + railParams)
controllerBlock = Group(namedTag(controllerTag, idToken) + controllerParams)
enablementBlock = Group(namedTag(enablementTag, idToken) + Group(OneOrMore(Group(namedTag('VERSION', idToken) + enablementParams)))('list'))
miscBlock = Group(namedTag(miscTag) + miscParams)

# Doc Level
doc = ZeroOrMore(enablementBlock ^ versionBlock ^ railBlock ^ controllerBlock ^ miscBlock)('blocks')

# *******************************************************************************
# Parser
# *******************************************************************************

class EnablementParser():
    def __init__(self, file):
        self.file = file
        self.misc = { }
        self.enablements = { }
        self.versions = { }
        self.rails = { }
        self.controllers = { }
        md5 = hashlib.md5()
        with open(file, 'rb') as binfile:
            md5.update(binfile.read())
        self.hashval = md5.hexdigest().upper()

    def __parse_version(self, blk):
        if blk.id:
            self.versions[blk.id] = []

        for entry in blk.table:
            if entry.foundary == '*':
                entry.foundary = 'FOUNDRY_ANY'

            key = (entry.foundary, entry.min, entry.max)
            self.versions[entry.name] = [key]
            if blk.id:
                self.versions[blk.id].append(key)


    def __parse_enablement(self, blk):
        for b in blk.list:
            d = b.params.asDict()
            d['version'] = b.id
            for i in d:
                if isinstance(d[i], ParseResults):
                    d[i] = d[i].asList()

            l = self.enablements.get(blk.id, [])
            l.append(d)
            self.enablements[blk.id] = l

    def __parse_rail(self, blk):
        d = blk.params.asDict()
        self.rails[blk.id] = d
        for i in d:
            if isinstance(d[i], ParseResults):
                d[i] = d[i].asList()

    def __parse_controller(self, blk):
        d = blk.params.asDict()
        self.controllers[blk.id] = d
        for i in d:
            if isinstance(d[i], ParseResults):
                d[i] = d[i].asList()

    def __parse_misc(self, blk):
        d = blk.params.asDict()
        self.misc = d
        for i in d:
            if isinstance(d[i], ParseResults):
               d[i] = d[i][0]

    def parse(self):
        doc.ignore(pythonStyleComment)
        doc.ignore(cppStyleComment)
        r = doc.parseFile(self.file, True)

        tagLookup = {
            enablementTag: self.__parse_enablement,
            versionTag: self.__parse_version,
            railTag: self.__parse_rail,
            controllerTag: self.__parse_controller,
            miscTag: self.__parse_misc,
        }

        for blk in r.blocks:
            try:
                tagLookup[blk.tag](blk)
            except KeyError as e:
                print("No parser found for tag", e.args)
                raise e


def get_cpr_enablements(input):
    ep = EnablementParser(input)
    ep.parse()
    #return ep.versions, ep.rails, ep.controllers, ep.enablements, ep.misc
    return ep


if __name__ == "__main__":
    #v, r, c, e, m = get_cpr_enablements('../target/test/enablement.cfg')
    #v, r, c, e, m = get_cpr_enablements(sys.argv[1])
    ep = get_cpr_enablements(sys.argv[1])

    print 'VERSIONS'
    for i in ep.versions:
        print(i, ep.versions[i])

    print
    print 'RAILS'
    for i in ep.rails:
        print(i, ep.rails[i])

    print
    print 'CONTROLLERS'
    for i in ep.controllers:
        print(i, ep.controllers[i])

    print
    print 'ENABLEMENTS'
    for i in ep.enablements:
        print(i, ep.enablements[i])

    print
    print 'MISC'
    for i in ep.misc:
        print(i, ep.misc[i])

    print 'Hash Value:', ep.hashval

