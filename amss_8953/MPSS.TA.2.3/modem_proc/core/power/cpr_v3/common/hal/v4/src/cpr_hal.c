/**
 * @file:  cpr_hal.c
 *
 * Copyright (c) 2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/16 03:52:08 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/hal/v4/src/cpr_hal.c#1 $
 * $Change: 9902037 $
 *
 */

#include "../../v3/src/cpr_hal.c"

void cpr_hal_write_temp_margin_table(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp) 
{
    int i=0;
    uint32 dataAddress;
    uint32 dataValue;

    dataAddress = HWIO_CPR_MARGIN_TEMP_CORE0_ADDR(hdl->base);
	
    CPR_LOG_INFO( "Setting SDELTA Table" );

    // Write the margin table to registers
    for(i = 0; i < vp->modes[modeIndex].marginModeTableSize; i++)
    {
      dataValue = CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand0, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND0) |
                  CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand1, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND1) |
                  CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand2, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND2) |
                  CPR_HWIO_SET_FIELD_VALUE(vp->modes[modeIndex].marginModeTable[i].tempBand3, HWIO_CPR_MARGIN_TEMP_CORE0_MARGIN_TEMPBAND3);
      CPR_HWIO_OUT(dataAddress, dataValue);
      dataAddress += 0x4;
    }
}

void cpr_hal_mode_change_temp_adj(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{
  uint32 dataAddressADJ;
  dataAddressADJ = HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base);
    
  // If temperature adjustment is not enabled
  if (vp->tempAdjControls == NULL || enablement->enabletempadj != true)
  {
    // By HPG specification. If temperature adjustment is not enabled then set 0 to kvMarginAdjEn
    CPR_LOG_INFO( "Temperature Adjustment is not enabled, exiting temperature adjustment init");
    CPR_HWIO_OUT_FIELD(dataAddressADJ, HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 0);
    return;
  }
  // Write the new SDELTA table for the new corner
  cpr_hal_write_temp_margin_table(modeIndex, hdl, vp);
}

void cpr_hal_write_temp_margin_init_registers(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{
    uint32 dataAddressADJ, dataAddressMISC, dataAddress0N1, dataAddress2;
    dataAddressADJ = HWIO_CPR_MARGIN_ADJ_CTL_ADDR(hdl->base);
    dataAddress0N1 = HWIO_CPR_MARGIN_TEMP_POINT0N1_ADDR(hdl->base);
    dataAddress2 = HWIO_CPR_MARGIN_TEMP_POINT2_ADDR(hdl->base);
    dataAddressMISC = HWIO_CPR_MISC_REGISTER_ADDR(hdl->base);

    // If temperature adjustment is not enabled
    if (vp->tempAdjControls == NULL || enablement->enabletempadj != true)
    {
      // By HPG specification. If temperature adjustment is not enabled then set 0 to kvMarginAdjEn
      CPR_LOG_INFO( "Temperature Adjustment is not enabled, exiting temperature adjustment init");
      CPR_HWIO_OUT_FIELD(dataAddressADJ, HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 0);
      return;
    }

    // Write the CPR MISC Register which contains the temp sensor id start and end
    CPR_HWIO_OUT_FIELD(dataAddressMISC, HWIO_CPR_MISC_REGISTER_TEMP_SENSOR_ID_START, vp->tempAdjControls->tempAdjMiscReg->tempSensorStartId);
    CPR_HWIO_OUT_FIELD(dataAddressMISC, HWIO_CPR_MISC_REGISTER_TEMP_SENSOR_ID_END, vp->tempAdjControls->tempAdjMiscReg->tempSensorEndId); 

    // Write the point 0, 1, 2 temperature thresholds
    CPR_HWIO_OUT_FIELD(dataAddress0N1, HWIO_CPR_MARGIN_TEMP_POINT0N1_POINT0, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint0 * 10);
    CPR_HWIO_OUT_FIELD(dataAddress0N1, HWIO_CPR_MARGIN_TEMP_POINT0N1_POINT1, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint1 * 10);
    CPR_HWIO_OUT_FIELD(dataAddress2, HWIO_CPR_MARGIN_TEMP_POINT2_POINT2, vp->tempAdjControls->tempAdjTempPoints->cprMarginTempPoint2 * 10);   

    cpr_hal_write_temp_margin_table(modeIndex, hdl, vp);

    // DEBUGGING PURPOSES ONLY
    if (vp->tempAdjControls->tempAdjCtl->perRoKvMarginEn)
    {
      CPR_HWIO_OUT_FIELD(dataAddressADJ, HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_SINGLE_STEP_QUOT, vp->tempAdjControls->tempAdjCtl->singleStepQuot);
      CPR_HWIO_OUT_FIELD(dataAddressADJ, HWIO_CPR_MARGIN_ADJ_CTL_PER_RO_KV_MARGIN_EN, vp->tempAdjControls->tempAdjCtl->perRoKvMarginEn);
    }

    // Enable the feature
    CPR_HWIO_OUT_FIELD(dataAddressADJ, HWIO_CPR_MARGIN_ADJ_CTL_KV_MARGIN_ADJ_EN, 1);
}

