/**
 * @file:  cpr_hal.h
 * @brief:
 *
 * Copyright (c) 2015, 2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/08 00:37:59 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/hal/inc/cpr_hal.h#3 $
 * $Change: 10025929 $
 *
 */
#ifndef CPR_HAL_H
#define	CPR_HAL_H

#include "cpr_types.h"
#include "cpr_target_hwio.h"
#include "cpr_voltage_plan.h"


#define CPR_HWIO_IN(addr)        __inpdw((uint8*)(addr))
#define CPR_HWIO_OUT(addr, val)  __outpdw((uint8*)(addr), val)

#define CPR_HWIO_OUT_SET(addr, mask)    CPR_HWIO_OUT(addr, ((CPR_HWIO_IN(addr)) | (mask)))
#define CPR_HWIO_OUT_CLEAR(addr, mask)  CPR_HWIO_OUT(addr, ((CPR_HWIO_IN(addr)) & (~(mask))))

#define CPR_HWIO_IN_MASK(addr, mask)         (CPR_HWIO_IN(addr) & (mask))
#define CPR_HWIO_OUT_MASK(addr, mask, val)    CPR_HWIO_OUT(addr, ((CPR_HWIO_IN(addr) & (~(mask))) | ((val) & (mask))))

#define CPR_HWIO_GET_FIELD_VALUE(value, field)  (((value) & field##_BMSK) >> field##_SHFT)
#define CPR_HWIO_SET_FIELD_VALUE(value, field)  (((value) << field##_SHFT) & field##_BMSK)
#define CPR_HWIO_IN_FIELD(addr, field)       (CPR_HWIO_IN_MASK( addr, field##_BMSK ) >> field##_SHFT)
#define CPR_HWIO_OUT_FIELD(addr, field, val)  CPR_HWIO_OUT_MASK( addr, field##_BMSK, (val) << field##_SHFT )

typedef struct
{
  uintptr_t base;
  uint32 thread;
  cpr_controller_type type;
} cpr_hal_handle;

//typedef struct
//{
//
//} cpr_hal_v2_cfg;

typedef struct
{
    uint8 *sensors;
    uint16 sensorsCount;
} cpr_hal_v3_cfg;

typedef struct
{
    uint8 upThresh;
    uint8 dnThresh;
    uint8 consecUp;
    uint8 consecDn;
    union {
        //cpr_hal_v2_cfg v2;
        cpr_hal_v3_cfg v3;
    } verCfg;
#if 0
    uint32 autoContInterval;
    uint8 gcntCount;
    uint8 *gcnt;
#endif
} cpr_hal_rail_cfg;

void cpr_hal_get_results( cpr_hal_handle* hdl, cpr_results* results);
void cpr_hal_ack_result(cpr_hal_handle* hdl, boolean ack);
void cpr_hal_clear_interrupts(cpr_hal_handle* hdl);
void cpr_hal_configure_controller(cpr_hal_handle* hdl, uint16 stepQuotMin, uint16 stepQuotMax);
void cpr_hal_enable_controller(cpr_hal_handle* hdl);
void cpr_hal_bypass_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count);
void cpr_hal_disable_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count);
void cpr_hal_disable_controller(cpr_hal_handle* hdl);
void cpr_hal_init_rail_hw(cpr_hal_handle* hdl, cpr_hal_rail_cfg* cfg);
void cpr_hal_start_poll(cpr_hal_handle* hdl);
void cpr_hal_stop_poll(cpr_hal_handle* hdl);
void cpr_hal_enable_up_interrupt(cpr_hal_handle* hdl, boolean enable);
void cpr_hal_enable_down_interrupt(cpr_hal_handle* hdl, boolean enable);
void cpr_hal_enable_rail(cpr_hal_handle* hdl, boolean up, boolean down, boolean swControl);
void cpr_hal_set_targets(cpr_hal_handle* hdl, cpr_voltage_mode mode, uint32 freq, cpr_quotient* tgts, uint32 count);
void cpr_hal_disable_rail(cpr_hal_handle* hdl);
void cpr_hal_write_temp_margin_table(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp);
#endif

