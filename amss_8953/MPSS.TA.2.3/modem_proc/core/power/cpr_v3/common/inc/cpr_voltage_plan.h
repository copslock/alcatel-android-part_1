/**
 * @file:  cpr_voltage_plan.h
 * @brief:
 *
 * Copyright (c) 2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/16 03:52:08 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_voltage_plan.h#2 $
 * $Change: 9902037 $
 */
#ifndef CPR_VOLTAGE_PLAN_H
#define	CPR_VOLTAGE_PLAN_H

#include "cpr_types.h"

#define CPR_FUSE_MAPPING(base_register, field) { \
    .address = HWIO_##base_register##_ADDR, \
    .offset  = HWIO_##base_register##_##field##_SHFT, \
    .mask    = HWIO_##base_register##_##field##_BMSK }


typedef uint32(*cpr_idx_func)(cpr_voltage_mode mode);

typedef struct
{
    struct raw_fuse_data {
        uintptr_t address;
        uint32 offset;
        uint32 mask;
    } *data;
    uint16 count;
} cpr_fuse;

typedef struct
{
    cpr_fuse* volt;
    cpr_fuse* quot;
    cpr_fuse* rosel;
    cpr_fuse* quotOffset;
    cpr_fuse* voltOffset;
}cpr_fuse_data;

typedef struct
{
    cpr_foundry_id foundry;
    uint32 min;
    uint32 max;
} cpr_version;

typedef struct
{
    cpr_version* versions;
    uint8 count;
} cpr_version_range;

typedef struct
{
    cpr_quotient *quots;
    uint32 *kvs;
    uint16 count;
} cpr_quotient_cfg;

typedef struct
{
    uint8 cprRevMin;
    uint8 cprRevMax;
    int32 openLoop;
    int32 closedLoop;
    int32 maxFloorToCeil;
} cpr_margin_data;

typedef struct
{
    uint8 count;
    cpr_margin_data *data;
} cpr_margin_cfg;

// Added for temperature margin adjustment feature
typedef struct 
{
    uint8 tempBand0;
    uint8 tempBand1;
    uint8 tempBand2;
    uint8 tempBand3;
} cpr_temp_adjust_temp_bands;

// Added for temperature margin adjustment feature
typedef struct 
{
    uint16 cprMarginTempPoint0;
    uint16 cprMarginTempPoint1;
    uint16 cprMarginTempPoint2;
} cpr_temp_adjust_temp_points;

// Added for temperature margin adjustment feature
typedef struct 
{
    uint8 tempSensorStartId;
    uint8 tempSensorEndId;
} cpr_temp_adjust_misc_reg;

// Added for temperature margin adjustment feature
typedef struct 
{
    uint8 singleStepQuot;
    uint8 perRoKvMarginEn;
} cpr_temp_adjust_margin_adj_ctl;

// Added for temperature margin adjustment feature
typedef struct 
{
    cpr_temp_adjust_temp_points* tempAdjTempPoints; // Used for temp margin adjustment 
    cpr_temp_adjust_misc_reg* tempAdjMiscReg; // Used for temp margin adjustment 
    cpr_temp_adjust_margin_adj_ctl* tempAdjCtl; // Used for temp margin adjustment 
} cpr_temp_adjust_controls;

typedef struct
{
    uint8 modesCount;
    cpr_voltage_mode* supportedModes;
    cpr_idx_func idxLookupFunc;
    cpr_temp_adjust_controls* tempAdjControls; // Used for temp margin adjustment 

    struct cpr_voltage_data* minMode; // Used for interpolation only

    struct cpr_voltage_data
    {
        uint32 fref;
        cpr_margin_cfg* margins;
        cpr_fuse_data* fuses;
        cpr_temp_adjust_temp_bands* marginModeTable; // Used for temp margin adjustment
        uint32 freqDelta;
        cpr_quotient_cfg* quotients;
        uint8 marginModeTableSize; // Used for temp margin adjustment
        uint8 subModesCount;
        struct cpr_freq_data {
            uint32 freq;
            uint32 ceiling;
            uint32 floor;
        } *subModes;
    } *modes;
} cpr_voltage_plan;

typedef struct cpr_freq_data cpr_freq_data;

/*
* TODO: need to revisit how to manage aging config when implementing aging
*/
typedef struct cpr_aging_cfg  
{
    cpr_voltage_mode   modeToRun;
    uint8              sensorID;
    uint32             kv; /* x100 */
    uint32             scalingFactor; /* x10 */
    int32              marginLimit;
} cpr_aging_cfg;

typedef struct cpr_sw_settings
{
    uint32             stepquotmax;
    uint32             stepquotmin;
    uint32             autocontinterval;
    uint32             upthresh;
    uint32             dnthresh;
    uint32             consecup;
    uint32             consecdn;
    uint32             clamptimerinterval;
    uint32             sensorbypass;
    uint32             sensormask;
    uint32             countrepeat;
    uint32             countmode;
    uint32             *gcnts;
} cpr_sw_settings;

typedef struct
{
    cpr_domain_id rail;
    struct voltage_plan_list {
        cpr_version version;
        cpr_voltage_plan* cfg;
    } *list;
    uint8 count;
    cpr_aging_cfg *agingCfg; // TODO: need to revisit how to manage aging config when implementing aging
} cpr_versioned_voltage_plan;


const cpr_versioned_voltage_plan* cpr_get_versioned_voltage_plan(cpr_domain_id railId);

#endif
