/**
 * @file:  cpr_isr.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_isr.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_ISR_H
#define	CPR_ISR_H

#include "cpr_types.h"

/**
 * Gets the CPR interrupt number for the given rail
 */
uint32 cpr_isr_get_interrupt(cpr_rail_id_t railId);

/**
 * Runs the CPR ISR for the given rail.
 */
void cpr_isr_process(cpr_rail_id_t railId);

#endif

