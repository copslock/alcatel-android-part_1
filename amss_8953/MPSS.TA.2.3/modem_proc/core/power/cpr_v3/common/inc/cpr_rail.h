/**
 * @file:  cpr_rail.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_rail.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_RAIL_H
#define	CPR_RAIL_H

#include "cpr_data.h"
#include "cpr_cfg.h"

void cpr_rail_init(cpr_rail* rail);
void cpr_rail_get_results(cpr_rail* rail, cpr_results* rslts);
boolean cpr_rail_process_results(cpr_rail* rail, cpr_results* rslts);
void cpr_rail_update_target_quotients(cpr_rail* rail, cpr_rail_state* railState);
void cpr_rail_set_initial_voltages(cpr_rail* rail, boolean useFloorFuses, boolean useCeilingFuses);
void cpr_rail_enable(cpr_rail* rail, cpr_mode_settings* modeState, cpr_submode_settings* submodeState, boolean changeOveride);
void cpr_rail_disable(cpr_rail* rail);
void cpr_rail_register_isr(cpr_rail* rail);
void cpr_rail_isr(void* ctx);

#endif

