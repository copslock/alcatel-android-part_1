/**
 * @file:  cpr_controller.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_controller.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_CONTROLLER_H
#define	CPR_CONTROLLER_H

#include "cpr_cfg.h"

void cpr_controller_init(cpr_controller* controller);

#endif

