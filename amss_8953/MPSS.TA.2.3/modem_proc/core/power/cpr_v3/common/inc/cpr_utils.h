/**
 * @file:  cpr_utils.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/04/19 20:16:34 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_utils.h#2 $
 * $Change: 10298483 $
 */
#ifndef CPR_UTILS_H
#define	CPR_UTILS_H

#include "cpr_types.h"
#include "cpr_cfg.h"
#include "cpr_data.h"
#include "cpr_voltage_plan.h"
#include "cpr_enablement.h"

static inline cpr_rail_state* cpr_utils_get_rail_state(cpr_domain_id railId)
{
    return &cpr_info.railStates[cpr_get_rail_idx( railId )];
}

static inline void cpr_utils_set_disable_vote(cpr_domain_id railId, uint32 vote)
{
    cpr_info.railStates[cpr_get_rail_idx( railId )].disableVotes |= vote;
    CPR_LOG_TRACE(" DisableVotes: 0x%x (set voter:0x%x)", cpr_info.railStates[cpr_get_rail_idx( railId )].disableVotes, vote);
    CPR_STATIC_LOG_RAIL_INFO(&cpr_info.railStates[cpr_get_rail_idx( railId )]);
}

static inline void cpr_utils_clear_disable_vote(cpr_domain_id railId, uint32 vote)
{
    cpr_info.railStates[cpr_get_rail_idx( railId )].disableVotes &= ~vote;
    CPR_LOG_TRACE(" DisableVotes: 0x%x (cleared voter:0x%x)", cpr_info.railStates[cpr_get_rail_idx( railId )].disableVotes, vote);
    CPR_STATIC_LOG_RAIL_INFO(&cpr_info.railStates[cpr_get_rail_idx( railId )]);
}

static inline boolean cpr_utils_has_disable_vote(cpr_domain_id railId, uint32 vote)
{
    return (0 != (cpr_info.railStates[cpr_get_rail_idx( railId )].disableVotes & vote));
}

static inline cpr_mode_settings* cpr_utils_get_active_mode_setting(cpr_domain_id railId)
{
    return cpr_info.railStates[cpr_get_rail_idx( railId )].activeMode;
}

static inline cpr_mode_settings* cpr_utils_get_previous_mode_setting(cpr_domain_id railId)
{
    return cpr_info.railStates[cpr_get_rail_idx( railId )].previousMode;
}

static inline boolean cpr_utils_is_closed_loop_mode(cpr_domain_id railId)
{
    return ((cpr_info.railStates[cpr_get_rail_idx(railId)].cMode == CPR_CONTROL_SW_CLOSED_LOOP) ||
            (cpr_info.railStates[cpr_get_rail_idx(railId)].cMode == CPR_CONTROL_HW_CLOSED_LOOP));
}

cpr_margin_data* cpr_utils_get_margins(cpr_margin_cfg* marginList);
void cpr_utils_get_mode_settings(cpr_domain_id railId, cpr_domain_info* info, cpr_mode_settings** mode, cpr_submode_settings** submode);
int16 cpr_utils_decode_fuse_value(cpr_fuse* fuse, uint8 packingFactor, boolean isSigned);
void cpr_utils_set_active_mode_setting(cpr_domain_id railId, cpr_mode_settings* newModeSetting, cpr_submode_settings* newSubModeSetting);

#endif

