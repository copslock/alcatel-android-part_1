/**
 * @file:  cpr_types.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_types.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_TYPES_H
#define CPR_TYPES_H

#include <stdbool.h>
#include <stdint.h>
#include "cpr_defs.h"
#include "cpr_image_defs.h"

typedef enum
{
    CPR_FOUNDRY_TSMC,
    CPR_FOUNDRY_GF,
    CPR_FOUNDRY_SS,
    CPR_FOUNDRY_IBM,
    CPR_FOUNDRY_UMC,
    CPR_FOUNDRY_ANY
} cpr_foundry_id;

typedef enum {
    CPR_THERMAL_REGION_NORMAL,
    CPR_THERMAL_REGION_COOL,
    CPR_THERMAL_REGION_COLD,
    CPR_THERMAL_REGION_CIRITICAL_COLD,
    CPR_THERMAL_REGION_MAX
} cpr_thermal_region;

typedef enum {
    CPR_MARGIN_ADJUSTMENT_SOURCE_AGING,
    CPR_MARGIN_ADJUSTMENT_SOURCE_THERMAL,
    CPR_MARGIN_ADJUSTMENT_SOURCE_TEST,
    CPR_MARGIN_ADJUSTMENT_SOURCE_MAX
} cpr_margin_adjustment_source;

typedef enum {
    CPR_CONTROL_NONE,
    CPR_CONTROL_OPEN_LOOP,
    CPR_CONTROL_SW_CLOSED_LOOP,
    CPR_CONTROL_HW_CLOSED_LOOP
} cpr_control_mode;

typedef enum {
    CPR_CONTROLLER_TYPE_SW_CL_ONLY,
    CPR_CONTROLLER_TYPE_SW_CL_MULTI_RAILS,
    CPR_CONTROLLER_TYPE_HW_CL_CAPABLE /* APPS CPR only */
} cpr_controller_type;

typedef struct
{
    uint16 ro;
    uint32 quotient;
} cpr_quotient;

typedef struct
{
    boolean busy;
    boolean up;
    boolean down;
    uint32 steps;
    uint32 maxQuot;
    uint32 minQuot;
} cpr_results;

#endif
