#! /usr/bin/env python

import os, re

__all__ = ['get_fuse_mapping']

HWIO_FILE = 'hwio/cpr_fuses_hwio.h'
REGISTER_PREFIX = 'QFPROM_CORR_CALIB'
REGISTER_SUFFIX = 'BMSK'


def get_fuse_mapping(fuse, cfgDir):
    '''
    Will return a list of (register, field) tuples that correspond to the passed in fuse value.
    Order will be by fuse bit order (msb first)
    '''
    mapping = []
    for line in open(os.path.join(cfgDir, HWIO_FILE), 'r'):
        searchStr = 'HWIO_(?P<register>{prefix}_[0-9A-Z_]+?)_(?P<field>{field}(?P<bits>_[0-9_]+)?)_{suffix}'
        searchStr = searchStr.format(prefix=REGISTER_PREFIX, field=fuse, suffix=REGISTER_SUFFIX)

        m = re.search(searchStr, line)
        if m:
            msBit = 0
            if m.group('bits'):
                # print m.group('bits')
                msBit = int(m.group('bits').split('_')[1])

            mapping.append((msBit, m.group('register'), m.group('field')))

    mapping = sorted(mapping, key=lambda x: x[0], reverse=True)

    if len(mapping) != 0:
        return [(x[1], x[2]) for x in mapping]

    return None

# uggh :/
mutationMap= {
     'TUR': ['TURBO'],
     'NOM': ['NOMINAL', 'NORMAL'],
     'SVS': [],
     'LOWSVS': []
}

def get_fuses(cfgDir, volt, voltOffset, quot=None, rosel=None, offset=None):
    instance = volt.split('_')[0]
    mode = volt.split('_')[3]

    volt = get_fuse_mapping(volt, cfgDir)

    for mutatedMode in [mode] + mutationMap[mode]:
        if not quot:
            quot = '_'.join([instance, mutatedMode, 'QUOT_VMIN'])
            quot = get_fuse_mapping(quot, cfgDir)

        if not rosel:
            rosel = '_'.join([instance, mutatedMode, 'ROSEL'])
            rosel = get_fuse_mapping(rosel, cfgDir)

        if not offset:
            offset = '_'.join([instance, mutatedMode, 'QUOT_OFFSET'])
            offset = get_fuse_mapping(offset, cfgDir)

    if  voltOffset:
        voltOffset = get_fuse_mapping(voltOffset, cfgDir)

    return (volt, quot, rosel, offset,voltOffset)


if __name__ == "__main__":
    for f in ('CPR2_TARG_VOLT_LOWSVS', 'CPR2_TARG_VOLT_SVS', 'CPR2_TARG_VOLT_NOM','CPR2_TARG_VOLT_TUR'):
        print f
        for x in get_fuses('.', f):
            print '\t', x
