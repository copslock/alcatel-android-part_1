/*===========================================================================

  Copyright (c) 2015-2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_target_quotients.h"
#include "cpr_device_hw_version.h"

static const cpr_target_quotient_versioned_config_t mss_8940_TSMC_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8940}
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           275,   272,   436,   424,   235,   243,   365,   371},
        {CPR_VOLTAGE_MODE_SVS_L1,        389,   383,   579,   564,   337,   347,   492,   497},
        {CPR_VOLTAGE_MODE_NOMINAL,       474,   466,   673,   644,   414,   423,   575,   565},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    557,   546,   760,   711,   487,   494,   653,   625},
        {CPR_VOLTAGE_MODE_TURBO,         646,   632,   846,   781,   569,   558,   728,   687},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {157, 150, 175, 157, 137, 132, 157, 137},
};
static const cpr_target_quotient_versioned_config_t mss_8940_GF_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8940}
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           248,   284,   497,   477,   264,   228,   445,   426},
        {CPR_VOLTAGE_MODE_SVS_L1,        355,   392,   636,   609,   361,   325,   571,   544},
        {CPR_VOLTAGE_MODE_NOMINAL,       440,   475,   740,   707,   436,   403,   666,   631},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    499,   532,   808,   772,   487,   457,   728,   688},
        {CPR_VOLTAGE_MODE_TURBO,         573,   602,   892,   850,   550,   524,   804,   758},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {150, 146, 181, 170, 131, 137, 164, 151},
};
static const cpr_target_quotient_versioned_config_t mss_8940_SMIC_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8940}
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           294,   308,   445,   408,   274,   267,   391,   356},
        {CPR_VOLTAGE_MODE_SVS_L1,        408,   415,   577,   528,   371,   369,   507,   464},
        {CPR_VOLTAGE_MODE_NOMINAL,       498,   497,   677,   618,   445,   449,   595,   544},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    559,   551,   743,   677,   494,   503,   653,   597},
        {CPR_VOLTAGE_MODE_TURBO,         636,   619,   824,   749,   555,   570,   724,   662},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {160, 144, 173, 155, 130, 142, 151, 139},
};
//8920 target Specific
	
static const cpr_target_quotient_versioned_config_t mss_8920_TSMC_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8920}
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           275,   272,   436,   424,   235,   243,   365,   371},
        {CPR_VOLTAGE_MODE_SVS_L1,        389,   383,   579,   564,   337,   347,   492,   497},
        {CPR_VOLTAGE_MODE_NOMINAL,       474,   466,   673,   644,   414,   423,   575,   565},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    557,   546,   760,   711,   487,   494,   653,   625},
        {CPR_VOLTAGE_MODE_TURBO,         646,   632,   846,   781,   569,   558,   728,   687},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {157, 150, 175, 157, 137, 132, 157, 137},
};
static const cpr_target_quotient_versioned_config_t mss_8920_GF_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8920}
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           248,   284,   497,   477,   264,   228,   445,   426},
        {CPR_VOLTAGE_MODE_SVS_L1,        355,   392,   636,   609,   361,   325,   571,   544},
        {CPR_VOLTAGE_MODE_NOMINAL,       440,   475,   740,   707,   436,   403,   666,   631},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    499,   532,   808,   772,   487,   457,   728,   688},
        {CPR_VOLTAGE_MODE_TURBO,         573,   602,   892,   850,   550,   524,   804,   758},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {150, 146, 181, 170, 131, 137, 164, 151},
};


static const cpr_target_quotient_versioned_config_t mss_8920_SMIC_target_quotients =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF,DALCHIPINFO_FAMILY_MSM8920 }
        },
        .foundry_range_count = 1,
    },
    .target_quotient_level = (const cpr_target_quotient_level_t[])
    {
        //Mode,                        RO[0], RO[1], RO[2], RO[3], RO[4], RO[5], RO[6], RO[7]
        {CPR_VOLTAGE_MODE_SVS,           294,   308,   445,   408,   274,   267,   391,   356},
        {CPR_VOLTAGE_MODE_SVS_L1,        408,   415,   577,   528,   371,   369,   507,   464},
        {CPR_VOLTAGE_MODE_NOMINAL,       498,   497,   677,   618,   445,   449,   595,   544},
        {CPR_VOLTAGE_MODE_NOMINAL_L1,    559,   551,   743,   677,   494,   503,   653,   597},
        {CPR_VOLTAGE_MODE_TURBO,         636,   619,   824,   749,   555,   570,   724,   662},
    },
    .target_quotient_level_count = 5,
    .ro_kv_x_100 = {160, 144, 173, 155, 130, 142, 151, 139},
};


static const cpr_target_quotient_rail_config_t mss_8940_target_quotient_config =
{
    .rail_id = CPR_RAIL_MSS,
    .versioned_target_quotient_config = (const cpr_target_quotient_versioned_config_t*[])
    {
        &mss_8940_TSMC_target_quotients,
        &mss_8940_GF_target_quotients,
        &mss_8940_SMIC_target_quotients,
		&mss_8920_TSMC_target_quotients,
        &mss_8920_GF_target_quotients,
        &mss_8920_SMIC_target_quotients,
    },
    .versioned_target_quotient_config_count = 6,
};

const cpr_target_quotient_global_config_t cpr_bsp_target_quotient_config = 
{
    .rail_config = (const cpr_target_quotient_rail_config_t*[])
    {
        &mss_8940_target_quotient_config,
    },
    .rail_config_count = 1,
};
