/*
==============================================================================

FILE:         VCSBSP.c

DESCRIPTION:
  This file contains VCS bsp data for DAL based driver.

==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/vcs/config/msm8952/VCSBSP.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
01/22/14   lil     Created.

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/


#include "comdef.h"
#include "VCSBSP.h"
#include "pmapp_npa.h"


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * VDD_MSS corner voltage.
 */
static VCSCornerVoltageRangeType VCS_CornerVoltageRangeMSS[] =
{
  {
    .eCorner = VCS_CORNER_LOW,
    .nMinUV  = 900000,
    .nMaxUV  = 1050000,
  },
  {
    .eCorner = VCS_CORNER_LOW_PLUS,
    .nMinUV  = 975000,
    .nMaxUV  = 1155000,
  },
  {
    .eCorner = VCS_CORNER_NOMINAL,
    .nMinUV  = 1037500,
    .nMaxUV  = 1225000,
  },
  {
    .eCorner = VCS_CORNER_NOMINAL_PLUS,
    .nMinUV  = 1075000,
    .nMaxUV  = 1287500,
  },
  {
    .eCorner = VCS_CORNER_TURBO,
    .nMinUV  = 1125000,
    .nMaxUV  = 1350000,
  }
};


/*
 *  VCS_RailCornerConfigsCX
 *
 *  Set of rail corner configurations.
 */
static VCSRailCornerConfigType VCS_CornerConfigCX[] =
{
  {
    .eCornerMin        = VCS_CORNER_LOW,
    .eCornerMax        = VCS_CORNER_TURBO,
    .eCornerInit       = VCS_CORNER_NOMINAL,
    .pVoltageRange     = NULL, /* We do not know about CX's voltage table */
    .nNumVoltageRanges = 0,    /* We do not directly manage CX voltages */
    .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
  }
};


/*
 *  VCS_CornerConfigsMSS
 *
 *  Set of rail corner configurations.
 */
static VCSRailCornerConfigType VCS_CornerConfigMSS[] =
{
  {
    .eCornerMin        = VCS_CORNER_LOW,
    .eCornerMax        = VCS_CORNER_TURBO,
    .eCornerInit       = VCS_CORNER_NOMINAL,
    .pVoltageRange     = VCS_CornerVoltageRangeMSS,
    .nNumVoltageRanges = ARR_SIZE(VCS_CornerVoltageRangeMSS),
    .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} }
  }
};


/*
 * VCS_RailConfigs
 *
 * Set of rail configurations.
 */
static VCSRailConfigType VCS_RailConfigs[] =
{
  {
    .eRail             = VCS_RAIL_CX,
    .szName            = VCS_NPA_RESOURCE_VDD_CX,
    .pCornerConfig     = VCS_CornerConfigCX,
    .nNumCornerConfigs = ARR_SIZE(VCS_CornerConfigCX),
    .szNameDependency  = PMIC_NPA_GROUP_ID_RAIL_CX,
    .bEnableCPR        = FALSE,
    .bEnableDVS        = TRUE
  },
  {
    .eRail             = VCS_RAIL_MSS,
    .szName            = VCS_NPA_RESOURCE_VDD_MSS,
    .pCornerConfig     = VCS_CornerConfigMSS,
    .nNumCornerConfigs = ARR_SIZE(VCS_CornerConfigMSS),
    .szNameDependency  = PMIC_NPA_GROUP_ID_RAIL_MX,
    .bEnableCPR        = TRUE,
    .bEnableDVS        = TRUE
  }
};

/*
 * List of CPUs.
 */
static VCSCPUConfigType VCS_CPUConfigs[] =
{
  {
    .eCPU       = CLOCK_CPU_MSS_Q6,
    .szName     = "/clk/cpu",
    .eRail      = VCS_RAIL_MSS,
  },
  {
    .eCPU       = CLOCK_CPU_MSS_Q6_CP,
    .szName     = "/clk/cpu1",
    .eRail      = VCS_RAIL_MSS,
  },
};


/*
 * Corner mapping from VCS enum to PMIC enum
 */
uint32 VCS_CornerPMICMap[VCS_CORNER_NUM_OF_CORNERS] =
{
  PMIC_NPA_MODE_ID_CORE_RAIL_OFF,            // VCS_CORNER_OFF
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION,      // VCS_CORNER_RETENTION
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION_PLUS, // VCS_CORNER_RETENTION_PLUS
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS,      // VCS_CORNER_LOW_MINUS
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW,            // VCS_CORNER_LOW
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_PLUS,       // VCS_CORNER_LOW_PLUS
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,        // VCS_CORNER_NOMINAL
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL_PLUS,   // VCS_CORNER_NOMINAL_PLUS
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO           // VCS_CORNER_TURBO
};


/*
 * VCS Log Default Configuration.
 */
const VCSLogType VCS_LogDefaultConfig[] =
{  
  {
    /* .nLogSize = */ 4096,
  }
};


/*
 *  VCS_BSPConfig
 *
 *  List and length of Rail and CPU configurations.
 */
const VCSBSPConfigType VCS_BSPConfig =
{
  .pRailConfig     = VCS_RailConfigs,
  .nNumRailConfigs = ARR_SIZE(VCS_RailConfigs),
  .pCPUConfig      = VCS_CPUConfigs,
  .nNumCPUConfigs  = ARR_SIZE(VCS_CPUConfigs),
  .pnCornerPMICMap = VCS_CornerPMICMap
};

