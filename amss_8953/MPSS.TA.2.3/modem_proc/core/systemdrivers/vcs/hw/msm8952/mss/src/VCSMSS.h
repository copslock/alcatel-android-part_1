#ifndef VCSMSS_H
#define VCSMSS_H
/*
===========================================================================
*/
/**
  @file VCSMSS.h 
  
  Internal header file for the VCS device driver on the MSS image.
*/
/*  
  ====================================================================

  Copyright (c) 2011-15 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/vcs/hw/msm8952/mss/src/VCSMSS.h#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.

  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "DDIVCS.h"
#include "VCSMSSBSP.h"


/*=========================================================================
      Macro Definitions
==========================================================================*/


/*
 * NPA client names
 */
#define VCS_NPA_CLIENT_NAME_VDD_MSS_DISABLE_SCALING "/vdd/mss/disable_scaling"

/*
 * VCS configuration names in EFS .ini file
 * Shared with ClockDriver
 */
#define VCS_EFS_INI_FILENAME                   "/nv/item_files/clock/settings.ini"

/*
 * EFS Sections for MPSS VCS.
 */
#define VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION    "MPSS_VDDCX"
#define VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION   "MPSS_VDDMSS"

/*
 * EFS Keys for MPSS VCS.
 */
#define VCS_EFS_RAIL_DVS_FLAG                  "EnableDVS"
#define VCS_EFS_RAIL_CPR_FLAG                  "EnableCPR"
#define VCS_EFS_RAIL_MIN_CORNER                "MinCorner"
#define VCS_EFS_RAIL_MAX_CORNER                "MaxCorner"

#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MIN     "RetentionMin"
#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MAX     "RetentionMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_MIN           "LowMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_MAX           "LowMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MIN      "LowPlusMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MAX      "LowPlusMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MIN       "NominalMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MAX       "NominalMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MIN  "NominalPlusMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MAX  "NominalPlusMax"

#define VCS_EFS_RAIL_VOLTAGE_TURBO_MIN         "TurboMin"
#define VCS_EFS_RAIL_VOLTAGE_TURBO_MAX         "TurboMax"

/*
 * Value for indicating that the corner should be disabled.
 * A corner is only disabled if both the min and max are equal to this field.
 */
#define VCS_EFS_CORNER_DISABLED                (-1)


/*=========================================================================
      Type Definitions
==========================================================================*/

#if 0
/**
 * VCS driver image context.
 *
 *  pBSPConfig - Pointer to BSP data.
 */
typedef struct
{
  VCSImageBSPConfigType *pBSPConfig;
} VCSImageCtxtType;
#endif

/*=========================================================================
      Functions
==========================================================================*/


#endif /* !VCSMSS_H */

