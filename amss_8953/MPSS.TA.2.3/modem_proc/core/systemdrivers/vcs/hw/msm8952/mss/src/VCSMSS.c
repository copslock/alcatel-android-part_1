/*
===========================================================================
*/
/**
  @file VCSMSS.c 
  
  Main entry point for the MSS VCS driver.
*/
/*  
  ====================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/vcs/hw/msm8952/mss/src/VCSMSS.c#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "VCSDriver.h"
#include "VCSMSS.h"
#include "VCSMSSHWIO.h"
#include "pmapp_pwr.h"
#include "pmapp_npa.h"
#include <CoreIni.h>
#include "cpr.h"
#include "DDIPlatformInfo.h"
#include <npa_resource.h>

/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Extern Definitions
==========================================================================*/


extern void npa_update_resource_state(npa_resource *resource, npa_resource_state new_state);


/*=========================================================================
      Function prototypes
==========================================================================*/


static npa_resource_state VCS_NPAMSSUVDriverFunc(npa_resource*, npa_client_handle, npa_resource_state);


/*=========================================================================
      Data
==========================================================================*/


//static VCSImageCtxtType VCS_ImageCtxt;

/*
 * The MSS Rail voltage broadcast node and resource. Broadcasts voltage in
 * microvolt on changes.
 */

typedef struct
{
  npa_resource_definition resource;
  npa_node_definition     node;
} VCS_NPAMSSUVResourceType;


static VCS_NPAMSSUVResourceType VCS_NPAMSSUVResource =
{
  /* Resource */
  {
    VCS_NPA_RESOURCE_VDD_MSS_UV,     /* Name          */
    "uV",                            /* Units         */
    0,                               /* Max Value TBD */
    &npa_no_client_plugin,           /* Plugin        */
    NPA_RESOURCE_DEFAULT,            /* Attributes    */
    NULL,                            /* User data     */
    NULL,                            /* Query fn      */
    NULL                             /* Query lnk fn  */
  },

  /* Node */
  {
    "/node" VCS_NPA_RESOURCE_VDD_MSS_UV,  /* Name           */
    &VCS_NPAMSSUVDriverFunc,              /* Driver fn      */
    NPA_NODE_DEFAULT,                     /* Attributes     */
    NULL,                                 /* User data      */
    0,                                    /* Num Dependency */
    NULL,                                 /* Dependency     */
    1,                                    /* Num resources  */
    &VCS_NPAMSSUVResource.resource        /* Resource       */
  }
};


/* =========================================================================
      Prototypes
==========================================================================*/


/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : VCS_NPAMSSUVDriverFunc
** =========================================================================*/
/**
  MSS Rail uV resource driver function

  This is a dummy function to satisfy NPA node requirement of a
  driver function for every resource.

  @return
  state.

  @dependencies
  None.
*/
npa_resource_state VCS_NPAMSSUVDriverFunc
(
  npa_resource *pResource,
  npa_client_handle hClient,
  npa_resource_state nState
)
{
  if(hClient->type == NPA_CLIENT_INITIALIZE)
  {
    return nState;
  }
  else
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: MSS Rail uV broadcast resource operation not supported.");

    return (npa_resource_state)0;
  }
}


/* =========================================================================
**  Function : VCS_MapCornerToAccMemSel
** =========================================================================*/
/**
  Maps a MSS voltage rail corner to an ACC memory select setting.

  This function maps VDD_MSS corners to ACC settings.

  @param eCorner [in] -- /vdd/mss corner to map.

  @return
  ACC memory select setting.

  @dependencies
  None.
*/
static uint32 VCS_MapCornerToAccMemSel
(
  VCSCornerType eCorner
)
{
  switch(eCorner)
  {
    case VCS_CORNER_TURBO:          
    case VCS_CORNER_NOMINAL_PLUS:    return 0x3;
    case VCS_CORNER_NOMINAL:        
    case VCS_CORNER_LOW_PLUS:        return 0x1;
    case VCS_CORNER_LOW:             return 0x0;

    default:
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Invalid /vdd/mss corner passed to mapping function.");

      /*
       * This return value is necessary for the compiler even with ERR_FATAL.
       */
      return 0;
  }

} /* END VCS_MapCornerToAccMemSel */


/* =========================================================================
**  Function : VCS_SetRailMode
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailMode
(
  VCSDrvCtxt     *pDrvCtxt,
  VCSRailType     eRail,
  VCSRailModeType eMode
)
{
  /*-----------------------------------------------------------------------*/
  /* We only support setting VDD_MSS rail to default mode.                 */
  /*-----------------------------------------------------------------------*/

  if (eMode != VCS_RAIL_MODE_CPR)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* The rail is always in default mode. There's nothing to be done.       */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCS_SetRailMode */


/* =========================================================================
**  Function : VCS_SetRailVoltage
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailVoltage
(
  VCSDrvCtxt  *pDrvCtxt,
  VCSRailType  eRail,
  uint32       nVoltageUV
)
{
  int                        nLockResult;
  pm_err_flag_type           pm_err;
  VCSRailNodeType           *pRail;
  VCSCornerVoltageRangeType *pVoltageRange;

  /*-----------------------------------------------------------------------*/
  /* We only support setting VDD_MSS rail voltage.                         */
  /*-----------------------------------------------------------------------*/

  if (eRail != VCS_RAIL_MSS)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* Get rail node.                                                        */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[eRail];
  if (pRail == NULL || pRail->pVoltageTable == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Protect operation on rail with a critical section.                    */
  /* If the lock is already taken (outstanding voltage corner switch) then */
  /* return immediately.                                                   */
  /*-----------------------------------------------------------------------*/

  nLockResult = npa_resource_trylock(pRail->resource.handle);
  if (nLockResult == -1)
  {
    return DAL_ERROR_BUSY_RESOURCE;
  }

  /*-----------------------------------------------------------------------*/
  /* Get voltage range for the current corner.                             */
  /*-----------------------------------------------------------------------*/

  pVoltageRange = pRail->pVoltageTable->apCornerMap[pRail->eCorner];
  if (pVoltageRange == NULL)
  {
    npa_resource_unlock(pRail->resource.handle);
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Validate the requested voltage.                                       */
  /*-----------------------------------------------------------------------*/

  if (nVoltageUV < pVoltageRange->nMinUV || nVoltageUV > pVoltageRange->nMaxUV)
  {
    npa_resource_unlock(pRail->resource.handle);
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Adjust the rail voltage.                                              */
  /*-----------------------------------------------------------------------*/

  pm_err = pmapp_pwr_set_vdd_mss(nVoltageUV);
  if (pm_err != PM_ERR_FLAG__SUCCESS)
  {
    npa_resource_unlock(pRail->resource.handle);

    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: PMIC failed to update VDD_MSS.");

    return DAL_ERROR_INTERNAL;
  }

  pRail->nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Update the MSS broadcast rail voltage resource.                       */
  /*-----------------------------------------------------------------------*/

  npa_update_resource_state(VCS_NPAMSSUVResource.resource.handle, pRail->nVoltageUV);

  /*-----------------------------------------------------------------------*/
  /* Release lock.                                                         */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END VCS_SetRailVoltage */


/* =========================================================================
**  Function : VCS_SetVDDMSSCornerCPR
** =========================================================================*/
/**
  Set VDD_MSS rail to a requested corner in CPR mode.

  @param pRail [in]   -- Pointer to the rail node.
  @param eCorner [in] -- Requested corner.

  @return
  DAL_SUCCESS -- Successfully configured VDD_MSS to requested corner.
  DAL_ERROR -- Failed to configure VDD_MSS to requested corner.

  @dependencies
  None.
*/

static DALResult VCS_SetVDDMSSCornerCPR
(
  VCSRailNodeType *pRail,
  VCSCornerType    eCorner
)
{
  DALResult                  eResult;
  pm_err_flag_type           pm_err;
  uint32                     nVoltageUV;
  VCSCornerVoltageRangeType *pVoltageRange;
  VCSDrvCtxt                *pDrvCtxt;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL || eCorner >= VCS_CORNER_NUM_OF_CORNERS)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the driver context.                                               */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Disable CPR before changing the voltage.                              */
  /*-----------------------------------------------------------------------*/

  if(!pRail->nDisableCPR)
  {
    /*
     * Log the CPR disable event.
     */
    ULOG_RT_PRINTF_1(
      pDrvCtxt->hVCSLog,
      "Disabling CPR on rail[%s].",
      pRail->pBSPConfig->szName);

    eResult = CPR_Disable(pRail->eRail);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR returned error[%lu] when called to disable.",
        eResult);

      return DAL_ERROR_INTERNAL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Gate voltage for the OFF corner case.                                 */
  /*-----------------------------------------------------------------------*/

  if (eCorner == VCS_CORNER_OFF)
  {
    /*
     * Voltage we want the rail set to.
     */
    nVoltageUV = 0;
    }

  /*-----------------------------------------------------------------------*/
  /* Get the voltage recommendation from CPR if not turning off the rail.  */
  /*-----------------------------------------------------------------------*/

  else
  {
    /*
     * Init the voltage to the max voltage at the specified corner.
     */
    pVoltageRange = pRail->pVoltageTable->apCornerMap[eCorner];
    nVoltageUV = pVoltageRange->nMaxUV;

    if(!pRail->nDisableCPR)
    {
    eResult =
      CPR_GetRailVoltageRecommendation(
        pRail->eRail,
        eCorner,
        &nVoltageUV);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
            "DALLOG Device VCS: CPR returned error[%lu] when called for voltage recommendation.",
            eResult);

          return DAL_ERROR_INTERNAL;
    }
  }

    /*
     *Bound the CPR requested voltage if necessary.
     */
  if (nVoltageUV < pVoltageRange->nMinUV)
  {
    nVoltageUV = pVoltageRange->nMinUV;
  }
  else if (nVoltageUV > pVoltageRange->nMaxUV)
  {
    nVoltageUV = pVoltageRange->nMaxUV;
  }
  }

  /*-----------------------------------------------------------------------*/
  /* Adjust the rail voltage.                                              */
  /*-----------------------------------------------------------------------*/

  if ((DalPlatformInfo_Platform() != DALPLATFORMINFO_TYPE_RUMI) &&
      (DalPlatformInfo_Platform() != DALPLATFORMINFO_TYPE_VIRTIO))
  {
  pm_err = pmapp_pwr_set_vdd_mss(nVoltageUV);

  if (pm_err != PM_ERR_FLAG__SUCCESS)
  {
        DALSYS_LogEvent(
          0,
          DALSYS_LOGEVENT_FATAL_ERROR,
          "DALLOG Device VCS: PMIC API returned error[%lu] for voltage[%lu]",
          pm_err,
          nVoltageUV);

    return DAL_ERROR_INTERNAL;
  }
  }

  pRail->nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Update the MSS broadcast rail voltage resource                        */
  /*-----------------------------------------------------------------------*/

  npa_update_resource_state(VCS_NPAMSSUVResource.resource.handle, pRail->nVoltageUV);

  /*-----------------------------------------------------------------------*/
  /* Enable CPR if the rail is still on.                                   */
  /*-----------------------------------------------------------------------*/

  if (eCorner != VCS_CORNER_OFF && !pRail->nDisableCPR)
  {
    /*
     * Log the CPR enable event.
     */
    ULOG_RT_PRINTF_1(
      pDrvCtxt->hVCSLog,
      "Enabling CPR on rail[%s].",
      pRail->pBSPConfig->szName);

    eResult = CPR_Enable(pRail->eRail);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR returned error[%lu] when called to enable.",
        eResult);

      return DAL_ERROR_INTERNAL;
    }
  }

  return DAL_SUCCESS;

} /* END VCS_SetVDDMSSCornerCPR */

/* =========================================================================
**  Function : VCS_SetACCSetting
** =========================================================================*/
/**
  Updates the ACC setting for /vdd/mss.

  This function updates the ACC setting for the /vdd/mss rail.

  @param eCorner [in] -- New /vdd/mss corner.

  @return
  None.

  @dependencies
  None.
*/

static void VCS_SetACCSetting
(
  VCSCornerType eCorner
)
{
  uint32 eVRegMSSCornerNewACC, eVRegMSSCornerACC;

  /*-----------------------------------------------------------------------*/
  /* Map the current ACC setting to a /vdd/mss voltage rail corner.        */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCornerACC = HWIO_INF(MSS_TCSR_ACC_SEL, ACC_MEM_SEL);

  eVRegMSSCornerNewACC =  VCS_MapCornerToAccMemSel(eCorner);

  /*-----------------------------------------------------------------------*/
  /* ACC settings from high to low.                                        */
  /*                                                                       */
  /* NOTE: Incremental scaling of ACC settings is required for multi       */
  /*       corner switches due to grey coding of this HW API.              */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerACC > eVRegMSSCornerNewACC)
  {
    /*
     * Transition [Super Turbo or Turbo -> Nominal]
     * Set ACC to nominal.
     */
    if (eVRegMSSCornerACC > VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL))
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL));
    }

    /*
     * Transition [Nominal -> Low]
     * Set ACC to low.
     */
    if (eVRegMSSCornerNewACC < VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL))
    {
       HWIO_OUTF(
         MSS_TCSR_ACC_SEL,
         ACC_MEM_SEL,
         VCS_MapCornerToAccMemSel(VCS_CORNER_LOW));
    }
  }

  /*-----------------------------------------------------------------------*/
  /* ACC settings from low to high.                                                                               */
  /*                                                                                                                            */
  /* NOTE: Incremental scaling of ACC settings is required for multi                                */
  /*       corner switches due to grey coding of this HW API.                                             */
  /*-----------------------------------------------------------------------*/

  else if (eVRegMSSCornerACC < eVRegMSSCornerNewACC)
  {
    /*
     * Transition [Low -> Nominal]
     * Set ACC to nominal.
     */
    if (eVRegMSSCornerACC < VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL))
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL));
    }

    /*
     * Transition [Nominal -> Turbo or Super Turbo]
     * Set ACC to high.
     */
    if (eVRegMSSCornerNewACC > VCS_MapCornerToAccMemSel(VCS_CORNER_NOMINAL))
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        VCS_MapCornerToAccMemSel(VCS_CORNER_TURBO));
    }
  }

}

/* =========================================================================
**  Function : VCS_SetVDDMSSCorner
** =========================================================================*/
/**
  Set VDD_MSS rail to a requested corner.

  @param pRail [in]   -- Pointer to the rail node.
  @param eCorner [in] -- Requested corner.

  @return
  DAL_SUCCESS -- Successfully configured VDD_MSS to requested corner.
  DAL_ERROR -- Failed to configure VDD_MSS to requested corner.

  @dependencies
  None.
*/

static DALResult VCS_SetVDDMSSCorner
(
  VCSRailNodeType *pRail,
  VCSCornerType    eCorner
)
{
  DALResult eResult = DAL_ERROR;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL || eCorner >= VCS_CORNER_NUM_OF_CORNERS || 
      pRail->eMode != VCS_RAIL_MODE_CPR)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-voltage update.                                                   */
  /*-----------------------------------------------------------------------*/

  if (eCorner < pRail->eCorner)
  {
    /*
     * ACC settings from high to low.
     */
     VCS_SetACCSetting(eCorner);
  }

  /*-----------------------------------------------------------------------*/
  /* Update the voltage corner.                                            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_SetVDDMSSCornerCPR(pRail, eCorner);
  
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to set rail to requested voltage.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Post-voltage update.                                                   */
  /*-----------------------------------------------------------------------*/

  if (eCorner > pRail->eCorner)
  {
    /*
     * ACC settings from low to high.
     */
    VCS_SetACCSetting(eCorner);
  }

  return DAL_SUCCESS;

} /* END VCS_SetVDDMSSCorner */

#if 0
/* =========================================================================
**  Function : VCS_LoadNV_CX
** =========================================================================*/
/**
  Load EFS data for CX rail.

  @param pDrvCtxt [in] -- Pointer to the VCS driver context.
  @param hConfig [in] -- Handle to core config.

  @return
  DAL_SUCCESS -- Successfully parsed EFS data for CX.
  DAL_ERROR -- Failed to parse EFS data for CX.

  @dependencies
  None.
*/

static DALResult VCS_LoadNV_CX
(
  VCSDrvCtxt       *pDrvCtxt,
  CoreConfigHandle  hConfig
)
{
  VCSRailNodeType         *pRail;
  VCSNPARailEventDataType  RailEventData;
  uint32                   nReadResult, nData;

  /*-----------------------------------------------------------------------*/
  /* Get rail nodes.                                                       */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_CX];
  if (pRail == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Invalid CX rail structure");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: EFS data update.                              */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Update max corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MAX_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMax = MIN(nData, VCS_CORNER_MAX);
  }

  /*-----------------------------------------------------------------------*/
  /* Update min corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MIN_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMin = MIN(nData, pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Update DVS.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_DVS_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableDVS |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableDVS = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: EFS data update.                                */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Notify clients of the new max limit for this rail.                    */
  /*-----------------------------------------------------------------------*/

  RailEventData.PreChange.eCorner  = VCS_CORNER_OFF;
  RailEventData.PostChange.eCorner = pRail->eCornerMax;

  npa_dispatch_custom_events(
    pRail->resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_LIMIT_MAX,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: limit max update.                             */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Verify votes reduced within supported range.                          */
  /*-----------------------------------------------------------------------*/

  if (pRail->resource.handle->request_state > pRail->eCornerMax)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: request[%l] on rail[%s] above max[%l]",
      pRail->resource.handle->request_state,
      pRail->pBSPConfig->szName,
      pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Trigger impulse to update the rail active state.                      */
  /* NOTE: We update the resoure state to 0 in order for the impulse       */
  /*       request to make it's way into the driver function.              */
  /*       This workaround will be replaced by a new NPA API.              */
  /*-----------------------------------------------------------------------*/

  npa_update_resource_state(pRail->resource.handle, 0);
  npa_issue_impulse_request(pRail->hClientImpulse);

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: limit max update.                               */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END of VCS_LoadNV_CX */
#endif


/* =========================================================================
**  Function : VCS_LoadNV_MSS_RailCorners
** =========================================================================*/
/**
  Load EFS data for MSS rail corners.

  @param pRail   [in] -- Pointer to the MSS rail node.
  @param hConfig [in] -- Handle to core config.
  @param eCorner [in] -- MSS rail corner.

  @return
   none

  @dependencies
  None.
*/
static void VCS_LoadNV_MSS_RailCorners
(
  VCSRailNodeType  *pRail,
  CoreConfigHandle  hConfig,
  VCSCornerType     eCorner
)
{
  VCSCornerVoltageRangeType *pCornerRange;
  uint32                     nReadResult, nData;
  char*                      szCornerMinUVEFSEntryName;
  char*                      szCornerMaxUVEFSEntryName;
  
  pCornerRange = pRail->pVoltageTable->apCornerMap[eCorner];
  switch (eCorner)
  {
    case VCS_CORNER_LOW:
    {
      szCornerMinUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_LOW_MIN;
      szCornerMaxUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_LOW_MAX;
      break;
    }
    case VCS_CORNER_LOW_PLUS:
    {
      szCornerMinUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MIN;
      szCornerMaxUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MAX;
      break;
    }
    case VCS_CORNER_NOMINAL:
    {
      szCornerMinUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_NOMINAL_MIN;
      szCornerMaxUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_NOMINAL_MAX;
      break;
    }
    case VCS_CORNER_NOMINAL_PLUS:
    {
      szCornerMinUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MIN;
      szCornerMaxUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MAX;
      break;
    }
    case VCS_CORNER_TURBO:
    {
      szCornerMinUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_TURBO_MIN;
      szCornerMaxUVEFSEntryName = VCS_EFS_RAIL_VOLTAGE_TURBO_MAX;
      break;
    }
    default: 
      return;
  }
  
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        szCornerMinUVEFSEntryName,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }
    
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        szCornerMaxUVEFSEntryName,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }
    
    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[eCorner] = NULL;
    }
  }
} /* END of VCS_LoadNV_MSS_RailCorners */

/* =========================================================================
**  Function : VCS_LoadNV_MSS
** =========================================================================*/
/**
  Load EFS data for MSS rail.

  @param pDrvCtxt [in] -- Pointer to the VCS driver context.
  @param hConfig [in] -- Handle to core config.

  @return
  DAL_SUCCESS -- Successfully parsed EFS data for MSS.
  DAL_ERROR -- Failed to parse EFS data for MSS.

  @dependencies
  None.
*/

static DALResult VCS_LoadNV_MSS
(
  VCSDrvCtxt       *pDrvCtxt,
  CoreConfigHandle  hConfig
)
{
  VCSRailNodeType           *pRail;
  uint32                     nReadResult, nData;
  VCSNPARailEventDataType    RailEventData;
  VCSCornerType              eCorner;

  /*-----------------------------------------------------------------------*/
  /* Get rail nodes.                                                       */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_MSS];
  if (pRail == NULL || pRail->pVoltageTable == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Invalid MSS rail structures");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: EFS data update.                              */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Update CPR.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_CPR_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableCPR |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableCPR = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update max corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_MAX_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMax = MIN(nData, VCS_CORNER_MAX);
  }

  /*-----------------------------------------------------------------------*/
  /* Update min corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_MIN_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMin = MIN(nData, pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Update DVS.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_DVS_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableDVS |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableDVS = 0;
    }
  }
  
  for (eCorner = pRail->eCornerMin; eCorner <= pRail->eCornerMax; eCorner++)
  {
    VCS_LoadNV_MSS_RailCorners(pRail, hConfig, eCorner);
  }

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: EFS data update.                                */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Notify clients of the new max limit for this rail.                    */
  /*-----------------------------------------------------------------------*/

  RailEventData.PreChange.eCorner  = VCS_CORNER_OFF;
  RailEventData.PostChange.eCorner = pRail->eCornerMax;

  npa_dispatch_custom_events(
    pRail->resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_LIMIT_MAX,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: limit max update.                             */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Verify votes reduced within supported range.                          */
  /*-----------------------------------------------------------------------*/

  if (pRail->resource.handle->request_state > pRail->eCornerMax)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: request[%l] on rail[%s] above max[%l]",
      pRail->resource.handle->request_state,
      pRail->pBSPConfig->szName,
      pRail->eCornerMax);

    return DAL_ERROR_NOT_ALLOWED;
  }

  /*-----------------------------------------------------------------------*/
  /* Trigger impulse to update the rail active state.                      */
  /* NOTE: We update the resoure state to 0 in order for the impulse       */
  /*       request to make it's way into the driver function.              */
  /*       This workaround will be replaced by a new NPA API.              */
  /*-----------------------------------------------------------------------*/

  npa_update_resource_state(pRail->resource.handle, 0);
  npa_issue_impulse_request(pRail->hClientImpulse);

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: limit max update.                               */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END of VCS_LoadNV_MSS */


/* =========================================================================
**  Function : VCS_LoadNV
** =========================================================================*/
/*
  See VCSMSS.h
*/

void VCS_LoadNV
(
  void
)
{
  VCSDrvCtxt       *pDrvCtxt;
  CoreConfigHandle  hConfig;
  DALResult         eResult;

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Read clock configuration file.                                        */
  /*-----------------------------------------------------------------------*/

  hConfig = CoreIni_ConfigCreate(VCS_EFS_INI_FILENAME);
  if (hConfig == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_INFO,
      "DALLOG Device VCS: Unable to read EFS file: %s",
      VCS_EFS_INI_FILENAME);

    return;
  }
#if 0
  /*-----------------------------------------------------------------------*/
  /* Load the EFS data for CX.                                             */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_LoadNV_CX(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: unable to load EFS data for rail CX");

    return;
  }
#endif
  /*-----------------------------------------------------------------------*/
  /* Load the EFS data for MSS.                                            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_LoadNV_MSS(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: unable to load EFS data for rail CX");

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Destroy the handle.                                                   */
  /*-----------------------------------------------------------------------*/

  CoreIni_ConfigDestroy(hConfig);

} /* END VCS_LoadNV */


/* =========================================================================
**  Function : VCS_InitVDDMSSRailVoltageTable
** =========================================================================*/
/**
  Initializes the voltage table for this HW version.

  @param pRail [in] -- Pointer rail node.
  @return
  DAL_ERROR if a voltage table not initialized, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult VCS_InitVDDMSSRailVoltageTable
(
  VCSRailNodeType *pRail
)
{
  DALResult                  eResult;
  uint32                     i, nFloorUV, nCeilingUV;
  VCSCornerVoltageRangeType *pRailVoltageRange;
  VCSRailVoltageTableType   *pVoltageTable;
  VCSDrvCtxt                *pDrvCtxt;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Query CPR for open loop voltages.                                     */
  /*-----------------------------------------------------------------------*/

  ULOG_RT_PRINTF_0(pDrvCtxt->hVCSLog, "[Open-Loop Voltages]");

  pVoltageTable = pRail->pVoltageTable;
  for (i = 0; i < pVoltageTable->nNumVoltageRanges; i++)
  {
    // Copy corner voltage table from BSP
    pRailVoltageRange = &pVoltageTable->pVoltageRange[i];

    // Query CPR for open-loop voltages.
    eResult =
      CprGetFloorAndCeiling(
        pRail->eRail,
        pRailVoltageRange->eCorner,
        &nFloorUV,
        &nCeilingUV);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CprGetFloorandCeiling returned error[%lu].",
        eResult);

      return DAL_ERROR_INTERNAL;
    }

    /*-----------------------------------------------------------------------*/
    /* Log the voltages.                                                     */
    /*-----------------------------------------------------------------------*/

    ULOG_RT_PRINTF_6(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Corner[%lu] BSP-Min[%lu] BSP-Max[%lu] CPR-Min[%lu] CPR-Max[%lu]",
      pRail->pBSPConfig->szName,
      pDrvCtxt->aszCornerNameMap[pRailVoltageRange->eCorner],
      pRailVoltageRange->nMinUV,
      pRailVoltageRange->nMaxUV,
      nFloorUV,
      nCeilingUV);

    // Bounds check open-loop ceiling voltage.
    if ((nCeilingUV < pRailVoltageRange->nMaxUV) &&
        (nCeilingUV >= pRailVoltageRange->nMinUV))
    {
      pRailVoltageRange->nMaxUV = nCeilingUV;
    }

    // Bounds check open-loop floor voltage.
    if (nFloorUV > pRailVoltageRange->nMinUV)
    {
      if (nFloorUV < pRailVoltageRange->nMaxUV)
      {
        pRailVoltageRange->nMinUV = nFloorUV;
      }
      else
      {
        pRailVoltageRange->nMinUV = pRailVoltageRange->nMaxUV;
      }
    }

    /*-----------------------------------------------------------------------*/
    /* Log the final selected values.                                        */
    /*-----------------------------------------------------------------------*/

    ULOG_RT_PRINTF_4(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Corner[%lu] Selected Min[%lu] Max[%lu]",
      pRail->pBSPConfig->szName,
      pDrvCtxt->aszCornerNameMap[pRailVoltageRange->eCorner],
      pRailVoltageRange->nMinUV,
      pRailVoltageRange->nMaxUV);
  }

  return DAL_SUCCESS;

} /* END VCS_InitVDDMSSRailVoltageTable */


/* =========================================================================
**  Function : VCS_InitVDDMSS
** =========================================================================*/
/**
  Initialize the VDD_MSS rail.

  This function initializes the VDD_MSS NPA resource.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_SUCCESS -- Initialization was successful.
  DAL_ERROR -- Initialization failed.

  @dependencies
  None.
*/

static DALResult VCS_InitVDDMSS
(
  VCSDrvCtxt *pDrvCtxt
)
{
  VCSRailNodeType           *pRail;
  //VCSImageCtxtType          *pImageCtxt;
  npa_resource_state         nInitialState;
  uint32                     nMaxVoltageRange;
  DALResult                  eResult;
  VCSRailCornerConfigType   *pSupportedCornerConfig;

  //pImageCtxt = (VCSImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get a pointer to the VDD_MSS rail data node.                          */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_MSS];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Hook up the rail's "set corner" function pointer.                     */
  /*-----------------------------------------------------------------------*/

  pRail->fpSetRailCorner = &VCS_SetVDDMSSCorner;

  /*-----------------------------------------------------------------------*/
  /* Initialize the voltage table for VDD_MSS.                             */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitVDDMSSRailVoltageTable(pRail);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to initialize CPUs.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Define the initial state and create the MSS Rail broadcast node       */
  /* Choose the maximum voltage from the highest voltage corner supported  */
  /* by this target from the BSP data.                                     */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Find the corner configuration supported for this hardware.            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_DetectRailBSPVersion(pRail, &pSupportedCornerConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to detect a valid BSP version.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* The last corner has the highest voltage range.                        */
  /*-----------------------------------------------------------------------*/

  nMaxVoltageRange = pSupportedCornerConfig->nNumVoltageRanges - 1;

  /*-----------------------------------------------------------------------*/
  /* Assign the max value and create the node     .                        */
  /*-----------------------------------------------------------------------*/

  VCS_NPAMSSUVResource.resource.max =
    pSupportedCornerConfig->pVoltageRange[nMaxVoltageRange].nMaxUV;

  nInitialState = 0;
  npa_define_node(&VCS_NPAMSSUVResource.node, &nInitialState, NULL);

  return DAL_SUCCESS;

} /* END VCS_InitVDDMSS */


/* =========================================================================
**  Function : VCS_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCS_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
  DALResult            eResult;
  //VCSPropertyValueType PropVal;

  #if 0
  /*-----------------------------------------------------------------------*/
  /* Assign the image context.                                             */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt->pImageCtxt = &VCS_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get the image BSP/XML.                                                */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_GetPropertyValue("VCSImageBSPConfig", &PropVal);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: VCS_GetPropertyValue failed.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Store the pointer to the image BSP in the image context.              */
  /*-----------------------------------------------------------------------*/

  VCS_ImageCtxt.pBSPConfig = (VCSImageBSPConfigType *)PropVal;
  #endif

  /*-----------------------------------------------------------------------*/
  /* Initialize VDD_MSS.                                                   */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitVDDMSS(pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to init VDD_MSS.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Done.                                                                 */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCS_InitImage */


/* =========================================================================
**  Function : VCSStub_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCSStub_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCSStub_InitImage */

