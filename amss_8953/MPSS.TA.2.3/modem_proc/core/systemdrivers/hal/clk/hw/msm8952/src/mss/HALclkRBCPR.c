/*
==============================================================================

FILE:         HALclkRBCPR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   RBCPR clocks.

   List of clock domains:
     - HAL_clk_mMSSRBCPRREFClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/hal/clk/hw/msm8952/src/mss/HALclkRBCPR.c#1 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/09/2014           Auto-generated.


==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;

/* ============================================================================
**    Data
** ==========================================================================*/

/*
 * HAL_clk_mRBCPRRefClockControl
 *
 * Functions for controlling RBCPR reference bus clock
 */
HAL_clk_ClockControlType HAL_clk_mRBCPRRefClockControl =
{
  /* .Enable           = */ HAL_clk_GenericEnable,
  /* .Disable          = */ HAL_clk_GenericDisable,
  /* .IsEnabled        = */ HAL_clk_GenericIsEnabled,
  /* .IsOn             = */ HAL_clk_GenericIsOn,
  /* .Reset            = */ HAL_clk_GenericReset,
  /* .IsReset          = */ HAL_clk_GenericIsReset,
  /* .Config           = */ NULL,
  /* .DetectConfig     = */ NULL,
  /* .ConfigDivider    = */ NULL,
  /* .DetectDivider    = */ NULL,
  /* .ConfigFootswitch = */ NULL,
};

/*                           
 *  HAL_clk_mRBCPRREFClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBCPRREFClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_rbcpr_ref",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_RBCPR_REF_CBCR), HWIO_OFFS(MSS_RBCPR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mRBCPRRefClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_RBCPR_REF
  },
};


/*
 * HAL_clk_mMSSRBCPRClkDomain
 *
 * RBCPRREF clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSRBCPRClkDomain =
{
  /* .nCGRAddr             = */ NULL,
  /* .pmClocks             = */ HAL_clk_mRBCPRREFClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBCPRREFClkDomainClks)/sizeof(HAL_clk_mRBCPRREFClkDomainClks[0]),
  /* .pmControl            = */ NULL,
  /* .pmNextClockDomain    = */ NULL
};

