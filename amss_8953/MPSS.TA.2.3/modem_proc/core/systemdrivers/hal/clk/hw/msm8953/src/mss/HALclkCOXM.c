/*
==============================================================================

FILE:         HALclkCOXM.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   COXM clocks.

   List of clock domains:
     - HAL_clk_mMSSCLKBITCOXMSRCClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/hal/clk/hw/msm8953/src/mss/HALclkCOXM.c#2 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCLKBITCOXMSRCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCLKBITCOXMSRCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_bit_coxm",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BIT_COXM_CBCR), HWIO_OFFS(MSS_COXM_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BIT_COXM
  },
};


/*
 * HAL_clk_mMSSCLKBITCOXMSRCClkDomain
 *
 * CLKBITCOXMSRC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKBITCOXMSRCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_BIT_COXM_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mCLKBITCOXMSRCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCLKBITCOXMSRCClkDomainClks)/sizeof(HAL_clk_mCLKBITCOXMSRCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

