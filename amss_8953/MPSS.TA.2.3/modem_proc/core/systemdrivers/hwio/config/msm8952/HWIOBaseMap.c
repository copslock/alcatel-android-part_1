
/*
===========================================================================
*/
/**
  @file HWIOBaseMap.c
  @brief Auto-generated HWIO Device Configuration base file.

  DESCRIPTION:
    This file contains Device Configuration data structures for mapping
    physical and virtual memory for HWIO blocks.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/hwio/config/msm8952/HWIOBaseMap.c#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "DalHWIO.h"
#include "msmhwiobase.h"


/*=========================================================================
      Data Definitions
==========================================================================*/

static HWIOModuleType HWIOModules_PRNG_PRNG[] =
{
  { "PRNG_PRNG",                                   0x00000000, 0x00000140 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_XPU_CFG_SNOC_CFG_MPU1132_4_M19L12_AHB[] =
{
  { "XPU_CFG_SNOC_CFG_MPU1132_4_M19L12_AHB",       0x00000000, 0x00000400 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SPDM_WRAPPER_TOP[] =
{
  { "SPDM_SPDM_CREG",                              0x00000000, 0x00000120 },
  { "SPDM_SPDM_OLEM",                              0x00001000, 0x0000015c },
  { "SPDM_SPDM_RTEM",                              0x00002000, 0x00000318 },
  { "SPDM_SPDM_SREG",                              0x00004000, 0x00000120 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SECURITY_CONTROL[] =
{
  { "SECURITY_CONTROL_CORE",                       0x00000000, 0x00007000 },
  { "SEC_CTRL_APU_APU1132_10",                     0x00007000, 0x000006d8 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PDM_PERPH_WEB[] =
{
  { "PDM_WEB_TCXO4",                               0x00000000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BOOT_ROM[] =
{
  { "BOOT_ROM_MPU1032_3_M20L12_AHB",               0x00000000, 0x00000380 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM[] =
{
  { "RPM_DEC",                                     0x00000000, 0x00002000 },
  { "RPM_QTMR_AC",                                 0x00002000, 0x00001000 },
  { "RPM_F0_QTMR_V1_F0",                           0x00003000, 0x00001000 },
  { "RPM_F1_QTMR_V1_F1",                           0x00004000, 0x00001000 },
  { "RPM_APU",                                     0x00007000, 0x00000300 },
  { "RPM_VMIDMT",                                  0x00008000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BIMC[] =
{
  { "BIMC_GLOBAL0",                                0x00000000, 0x00001000 },
  { "BIMC_GLOBAL1",                                0x00001000, 0x00001000 },
  { "BIMC_GLOBAL2",                                0x00002000, 0x00001000 },
  { "BIMC_PERFMON",                                0x00003000, 0x00001000 },
  { "BIMC_DTE",                                    0x00006000, 0x0000005c },
  { "BIMC_M_APP_MPORT",                            0x00008000, 0x00001000 },
  { "BIMC_M_APP_PROF",                             0x00009000, 0x00001000 },
  { "BIMC_M_DSP_MPORT",                            0x0000c000, 0x00001000 },
  { "BIMC_M_DSP_PROF",                             0x0000d000, 0x00001000 },
  { "BIMC_M_GPU_MPORT",                            0x00010000, 0x00001000 },
  { "BIMC_M_GPU_PROF",                             0x00011000, 0x00001000 },
  { "BIMC_M_MMSS0_MPORT",                          0x00014000, 0x00001000 },
  { "BIMC_M_MMSS0_PROF",                           0x00015000, 0x00001000 },
  { "BIMC_M_MMSS1_MPORT",                          0x00018000, 0x00001000 },
  { "BIMC_M_MMSS1_PROF",                           0x00019000, 0x00001000 },
  { "BIMC_M_SYS_MPORT",                            0x0001c000, 0x00001000 },
  { "BIMC_M_SYS_PROF",                             0x0001d000, 0x00001000 },
  { "BIMC_M_TCU_MPORT",                            0x00020000, 0x00001000 },
  { "BIMC_M_TCU_PROF",                             0x00021000, 0x00001000 },
  { "BIMC_S_SYS_SWAY",                             0x00058000, 0x00001000 },
  { "BIMC_S_DEFAULT_SWAY",                         0x00060000, 0x00001000 },
  { "BIMC_S_DDR0_ARB",                             0x00049000, 0x00001000 },
  { "BIMC_S_SYS_ARB",                              0x00059000, 0x00001000 },
  { "BIMC_S_DEFAULT_ARB",                          0x00061000, 0x00001000 },
  { "BIMC_S_DDR0_SCMO",                            0x00048000, 0x00001000 },
  { "BIMC_S_DDR0",                                 0x0004a000, 0x00001080 },
  { "BIMC_S_DDR0_DPE",                             0x0004c000, 0x00001000 },
  { "BIMC_S_DDR0_SHKE",                            0x0004d000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MPM2_MPM[] =
{
  { "MPM2_MPM",                                    0x00000000, 0x00001000 },
  { "MPM2_G_CTRL_CNTR",                            0x00001000, 0x00001000 },
  { "MPM2_G_RD_CNTR",                              0x00002000, 0x00001000 },
  { "MPM2_SLP_CNTR",                               0x00003000, 0x00001000 },
  { "MPM2_QTIMR_AC",                               0x00004000, 0x00001000 },
  { "MPM2_QTIMR_V1",                               0x00005000, 0x00001000 },
  { "MPM2_TSYNC",                                  0x00006000, 0x00001000 },
  { "MPM2_APU",                                    0x00007000, 0x00000780 },
  { "MPM2_TSENS",                                  0x00008000, 0x00001000 },
  { "MPM2_TSENS_TM",                               0x00009000, 0x00001000 },
  { "MPM2_WDOG",                                   0x0000a000, 0x00000020 },
  { "MPM2_PSHOLD",                                 0x0000b000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_DEHR_BIMC_WRAPPER[] =
{
  { "DEHR_BIMC",                                   0x00002000, 0x00002000 },
  { "DEHR_XPU",                                    0x00000000, 0x00000300 },
  { "DEHR_VMIDMT",                                 0x00001000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PC_NOC[] =
{
  { "PC_NOC",                                      0x00000000, 0x00012080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SYSTEM_NOC[] =
{
  { "SYSTEM_NOC",                                  0x00000000, 0x00016080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CRYPTO0_CRYPTO_TOP[] =
{
  { "CRYPTO0_CRYPTO",                              0x0003a000, 0x00006000 },
  { "CRYPTO0_CRYPTO_BAM",                          0x00004000, 0x00020000 },
  { "CRYPTO0_CRYPTO_BAM_XPU2_BAM",                 0x00002000, 0x00002000 },
  { "CRYPTO0_CRYPTO_BAM_VMIDMT_BAM",               0x00000000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_APB_DEC_QDSS_APB[] =
{
  { "QDSS_APB_DEC_DAPROM",                         0x00000000, 0x00001000 },
  { "QDSS_APB_DEC_QDSSCSR",                        0x00001000, 0x00001000 },
  { "QDSS_APB_DEC_CXSTM_2_32_32_TRUE",             0x00002000, 0x00001000 },
  { "QDSS_APB_DEC_CTI0_CTI0_CSCTI",                0x00010000, 0x00001000 },
  { "QDSS_APB_DEC_CTI1_CTI1_CSCTI",                0x00011000, 0x00001000 },
  { "QDSS_APB_DEC_CTI2_CTI2_CSCTI",                0x00012000, 0x00001000 },
  { "QDSS_APB_DEC_CTI3_CTI3_CSCTI",                0x00013000, 0x00001000 },
  { "QDSS_APB_DEC_CTI4_CTI4_CSCTI",                0x00014000, 0x00001000 },
  { "QDSS_APB_DEC_CTI5_CTI5_CSCTI",                0x00015000, 0x00001000 },
  { "QDSS_APB_DEC_CTI6_CTI6_CSCTI",                0x00016000, 0x00001000 },
  { "QDSS_APB_DEC_CTI7_CTI7_CSCTI",                0x00017000, 0x00001000 },
  { "QDSS_APB_DEC_CTI8_CTI8_CSCTI",                0x00018000, 0x00001000 },
  { "QDSS_APB_DEC_CSTPIU_CSTPIU_CSTPIU",           0x00020000, 0x00001000 },
  { "QDSS_APB_DEC_IN_FUN0_IN_FUN0_CXATBFUNNEL_128W8SP", 0x00021000, 0x00001000 },
  { "QDSS_APB_DEC_IN_FUN1_IN_FUN1_CXATBFUNNEL_128W8SP", 0x00022000, 0x00001000 },
  { "QDSS_APB_DEC_MERG_FUN_MERG_FUN_CXATBFUNNEL_128W2SP", 0x00023000, 0x00001000 },
  { "QDSS_APB_DEC_REPL64_REPL64_CXATBREPLICATOR_64WP", 0x00024000, 0x00001000 },
  { "QDSS_APB_DEC_ETFETB_ETFETB_CXTMC_F128W16K",   0x00025000, 0x00001000 },
  { "QDSS_APB_DEC_ETR_ETR_CXTMC_R64W32D",          0x00026000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_AHB_DEC_QDSS_AHB[] =
{
  { "QDSS_AHB_DEC_NDPBAM_NDPBAM_BAM_NDP_TOP_AUTO_SCALE_V2_0", 0x00010000, 0x00019000 },
  { "QDSS_AHB_DEC_NDPBAM_BAM",                     0x00014000, 0x00015000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_TLMM[] =
{
  { "TLMM_MPU1132_16_M22L12_AHB",                  0x00300000, 0x00000a00 },
  { "TLMM_CSR",                                    0x00000000, 0x00300000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CLK_CTL[] =
{
  { "GCC_CLK_CTL_REG",                             0x00000000, 0x00080000 },
  { "GCC_RPU_RPU1132_32_L12",                      0x00080000, 0x00001200 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CORE_TOP_CSR[] =
{
  { "TCSR_TCSR_MUTEX",                             0x00005000, 0x00020000 },
  { "TCSR_REGS_APU1132_16",                        0x00036000, 0x00000a00 },
  { "TCSR_TCSR_REGS",                              0x00037000, 0x00021000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PMIC_ARB[] =
{
  { "SPMI_CFG_TOP",                                0x00000000, 0x0000d000 },
  { "SPMI_GENI_CFG",                               0x0000a000, 0x00000700 },
  { "SPMI_CFG",                                    0x0000a700, 0x00001a00 },
  { "SPMI_PIC",                                    0x01800000, 0x00200000 },
  { "PMIC_ARB_MPU1132_25_M25L12_AHB",              0x0000e000, 0x00000e6d },
  { "PMIC_ARB_CORE",                               0x0000f000, 0x00001000 },
  { "PMIC_ARB_CORE_REGISTERS",                     0x00400000, 0x00800000 },
  { "PMIC_ARB_CORE_REGISTERS_OBS",                 0x00c00000, 0x00800000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MSS_TOP[] =
{
  { "MSS_APU0132_5",                               0x00000000, 0x00000480 },
  { "MSS_RMB",                                     0x00020000, 0x00010000 },
  { "MSS_QDSP6SS_PUB",                             0x00080000, 0x00004040 },
  { "QDSP6SS_PRIVATE",                             0x00100000, 0x00080000 },
  { "QDSP6SS_CSR",                                 0x00100000, 0x00008028 },
  { "QDSP6SS_L2VIC",                               0x00110000, 0x00001000 },
  { "QDSP6SS_QTMR_AC",                             0x00120000, 0x00001000 },
  { "QDSP6SS_QTMR_F0_0",                           0x00121000, 0x00001000 },
  { "QDSP6SS_QTMR_F1_1",                           0x00122000, 0x00001000 },
  { "QDSP6SS_QTMR_F2_2",                           0x00123000, 0x00001000 },
  { "QDSP6SS_SAW2",                                0x00130000, 0x00000ff0 },
  { "MSS_PERPH",                                   0x00180000, 0x0000f020 },
  { "MSS_UIM0_UART_DM",                            0x00190000, 0x000001c0 },
  { "MSS_UIM1_UART_DM",                            0x00198000, 0x000001c0 },
  { "MSS_CXM_UART_DM",                             0x001a0000, 0x00000200 },
  { "MSS_UIM2_UART_DM",                            0x001a8000, 0x000001c0 },
  { "MSS_CONF_BUS_TIMEOUT",                        0x001b0000, 0x00001000 },
  { "MSS_RBCPR_WRAPPER",                           0x001b1000, 0x00001000 },
  { "MSS_COMBODAC_COMP",                           0x001b2000, 0x00001000 },
  { "COMBODAC_CSR",                                0x001b2000, 0x00000800 },
  { "COMBODAC_REGARRAY",                           0x001b2800, 0x00000800 },
  { "MSS_MGPI",                                    0x001b3000, 0x00000128 },
  { "MSS_CRYPTO_TOP",                              0x001c0000, 0x00040000 },
  { "MSS_CRYPTO",                                  0x001fa000, 0x00006000 },
  { "MSS_CRYPTO_BAM",                              0x001c4000, 0x00015000 },
  { "MSS_NAV",                                     0x00200000, 0x000f888d },
  { "MODEM_TOP",                                   0x00300000, 0x000b8000 },
  { "TDEC_WRAP_TOP",                               0x003b0000, 0x00008000 },
  { "TDEC_DB1",                                    0x003b0000, 0x000000a8 },
  { "TD_CFG_TRIF_DB1",                             0x003b1000, 0x00000a00 },
  { "TD_TRIF_DB1",                                 0x003b2000, 0x00000900 },
  { "TDECIB_MEM_DB1",                              0x003b3000, 0x00001000 },
  { "TDEC",                                        0x003b4000, 0x000000a8 },
  { "TD_CFG_TRIF",                                 0x003b5000, 0x00000a00 },
  { "TD_TRIF",                                     0x003b6000, 0x00000900 },
  { "TDECIB_MEM",                                  0x003b7000, 0x00001000 },
  { "TX_TOP",                                      0x003a0000, 0x00010000 },
  { "TX",                                          0x003a0000, 0x00001000 },
  { "TX_UNIFIED_TOP",                              0x003a1000, 0x00001000 },
  { "TX_UNIFIED",                                  0x003a1000, 0x00000800 },
  { "TX_UNIFIED_MEM",                              0x003a1800, 0x00000800 },
  { "TX_MEM",                                      0x003a2000, 0x00001000 },
  { "TX_BRDG",                                     0x003a3000, 0x00000050 },
  { "TXR_A0",                                      0x003a5000, 0x00001000 },
  { "TXC_A1",                                      0x003a6000, 0x00001000 },
  { "TXR_A1",                                      0x003a7000, 0x00001000 },
  { "TXC_A0",                                      0x003a4000, 0x00001000 },
  { "TXC_MEM",                                     0x003a8000, 0x00001800 },
  { "O_TX",                                        0x003aa000, 0x00000200 },
  { "O_TX_WMORE_TS_TRIF",                          0x003aa200, 0x00000400 },
  { "O_TX_ENC_TS_TRIF",                            0x003aa600, 0x00000200 },
  { "O_TX_MOD_TS_TRIF",                            0x003aa800, 0x00000700 },
  { "O_TX_CA",                                     0x003ab000, 0x00000200 },
  { "O_TX_WMORE_TS_TRIF_CA",                       0x003ab200, 0x00000400 },
  { "O_TX_ENC_TS_TRIF_CA",                         0x003ab600, 0x00000200 },
  { "O_TX_MOD_TS_TRIF_CA",                         0x003ab800, 0x00000700 },
  { "RXFE",                                        0x00380000, 0x00020000 },
  { "RXFE_TOP_CFG",                                0x00380000, 0x00001000 },
  { "RXFE_ADC_ADC0",                               0x00382000, 0x00000100 },
  { "RXFE_ADC_ADC1",                               0x00382100, 0x00000100 },
  { "RXFE_ADC_ADC2",                               0x00382200, 0x00000100 },
  { "RXFE_ADC_ADC3",                               0x00382300, 0x00000100 },
  { "RXFE_WB_WB0",                                 0x00384000, 0x00001000 },
  { "RXFE_WB_WB1",                                 0x00385000, 0x00001000 },
  { "RXFE_WB_WB2",                                 0x00386000, 0x00001000 },
  { "RXFE_WB_WB3",                                 0x00387000, 0x00001000 },
  { "RXFE_NB_NB0",                                 0x00390000, 0x00001000 },
  { "RXFE_NB_NB1",                                 0x00391000, 0x00001000 },
  { "RXFE_NB_NB2",                                 0x00392000, 0x00001000 },
  { "RXFE_NB_NB3",                                 0x00393000, 0x00001000 },
  { "RXFE_NB_NB4",                                 0x00394000, 0x00001000 },
  { "RXFE_NB_NB5",                                 0x00395000, 0x00001000 },
  { "RXFE_BRDG",                                   0x0039f000, 0x00000100 },
  { "DEMBACK_TOP",                                 0x00340000, 0x00040000 },
  { "DEMBACK_COMMON",                              0x00340000, 0x00000100 },
  { "DEMBACK_BRDG",                                0x00340100, 0x00000100 },
  { "LTE_DEMBACK_DB1",                             0x00351000, 0x00000050 },
  { "LTE_DEMBACK",                                 0x00341000, 0x00000050 },
  { "UMTS_DEMBACK",                                0x00342000, 0x00000300 },
  { "TDS_DEMBACK",                                 0x00342400, 0x00000100 },
  { "CDMA_DEINT",                                  0x00342600, 0x00000100 },
  { "CDMA_WIDGET",                                 0x00342900, 0x00000100 },
  { "HDR_DEINT",                                   0x00342c00, 0x00000100 },
  { "TBVD_CCH_TRIF_DB1",                           0x00353000, 0x00000400 },
  { "TBVD_CCH_TRIF",                               0x00343000, 0x00000400 },
  { "DB_BUF_DB1",                                  0x00354000, 0x00004000 },
  { "DB_BUF",                                      0x00344000, 0x00004000 },
  { "SVD_TBVD_DB1",                                0x00358000, 0x00000300 },
  { "SVD_TBVD",                                    0x00348000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF_DB1",                    0x00359000, 0x00000300 },
  { "LTE_DEMBACK_SCH_TRIF",                        0x00349000, 0x00000300 },
  { "LTE_DEMBACK_CCH_TRIF_DB1",                    0x0035a000, 0x00000c00 },
  { "LTE_DEMBACK_CCH_TRIF",                        0x0034a000, 0x00000c00 },
  { "LTE_REENC_TS_TRIF",                           0x0034b000, 0x00000400 },
  { "W_DBACK_HS_TRIF",                             0x0034c000, 0x00000400 },
  { "W_DBACK_NONHS_TRIF",                          0x0034d000, 0x00000400 },
  { "T_DBACK_TRIF",                                0x0034e000, 0x00000500 },
  { "HDR_DEINT_TS_TRIF",                           0x0034f000, 0x00000300 },
  { "DB_BUF_PAGE",                                 0x0035c000, 0x00004000 },
  { "DECOB",                                       0x00360000, 0x00016800 },
  { "MTC_TOP",                                     0x00300000, 0x00040000 },
  { "CCS",                                         0x00300000, 0x00020000 },
  { "PDMEM",                                       0x00300000, 0x00010000 },
  { "CONTROL",                                     0x0031fc00, 0x00000400 },
  { "MTC_CLK",                                     0x00320000, 0x00000400 },
  { "MCDMA",                                       0x00320400, 0x00000400 },
  { "A2",                                          0x00320800, 0x00000400 },
  { "DBG",                                         0x00320c00, 0x00000400 },
  { "MTC_BRDG",                                    0x00321000, 0x00000400 },
  { "A2_MEM",                                      0x00322000, 0x00002000 },
  { "MCDMA_TS_TRIF",                               0x00324000, 0x00000a00 },
  { "DBG_TS_TRIF",                                 0x00324c00, 0x00000200 },
  { "STMR_EXPY",                                   0x00325000, 0x00000400 },
  { "ENCRYPT",                                     0x00325400, 0x000000fd },
  { "UNIV_STMR",                                   0x00328800, 0x00000400 },
  { "RFC",                                         0x00330000, 0x00010000 },
  { "RFC_PDMEM",                                   0x00330000, 0x00008000 },
  { "RFC_SWI",                                     0x0033fe00, 0x00000200 },
  { "MSS_CP_QDSP6SS_PUB",                          0x00b00000, 0x00004040 },
  { "CP_QDSP6SS_PRIVATE",                          0x00b80000, 0x00080000 },
  { "CP_QDSP6SS_CSR",                              0x00b80000, 0x00008028 },
  { "CP_QDSP6SS_L2VIC",                            0x00b90000, 0x00001000 },
  { "CP_QDSP6SS_QDSP6SS_QTMR_AC",                  0x00ba0000, 0x00001000 },
  { "CP_QDSP6SS_QTMR_F0_0",                        0x00ba1000, 0x00001000 },
  { "CP_QDSP6SS_QTMR_F1_1",                        0x00ba2000, 0x00001000 },
  { "CP_QDSP6SS_QTMR_F2_2",                        0x00ba3000, 0x00001000 },
  { "CP_QDSP6SS_QDSP6SS_SAW2",                     0x00bb0000, 0x00000ff0 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BLSP1_BLSP[] =
{
  { "BLSP1_BLSP_BAM",                              0x00004000, 0x0001f000 },
  { "BLSP1_BLSP_BAM_XPU2",                         0x00002000, 0x00002000 },
  { "BLSP1_BLSP_BAM_VMIDMT",                       0x00000000, 0x00001000 },
  { "BLSP1_BLSP_UART0_UART0_DM",                   0x0002f000, 0x00000200 },
  { "BLSP1_BLSP_UART1_UART1_DM",                   0x00030000, 0x00000200 },
  { "BLSP1_BLSP_QUP0",                             0x00035000, 0x00000600 },
  { "BLSP1_BLSP_QUP1",                             0x00036000, 0x00000600 },
  { "BLSP1_BLSP_QUP2",                             0x00037000, 0x00000600 },
  { "BLSP1_BLSP_QUP3",                             0x00038000, 0x00000600 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_IPA_WRAPPER[] =
{
  { "BAM_NDP",                                     0x00000000, 0x0002b000 },
  { "BAM",                                         0x00004000, 0x00027000 },
  { "XPU2",                                        0x00002000, 0x00002000 },
  { "IPA_VMIDMT",                                  0x00030000, 0x00001000 },
  { "IPA",                                         0x00040000, 0x00010000 },
  { "IPA_UC",                                      0x00050000, 0x00014000 },
  { "IPA_UC_RAM",                                  0x00050000, 0x00008000 },
  { "IPA_UC_PER",                                  0x00060000, 0x00002000 },
  { "IPA_UC_MBOX",                                 0x00062000, 0x00002000 },
  { NULL, 0, 0 }
};

HWIOPhysRegionType HWIOBaseMap[] =
{
  {
    "PRNG_PRNG",
    (DALSYSMemAddr)0x00022000,
    0x00001000,
    (DALSYSMemAddr)PRNG_PRNG_BASE,
    HWIOModules_PRNG_PRNG
  },
  {
    "XPU_CFG_SNOC_CFG_MPU1132_4_M19L12_AHB",
    (DALSYSMemAddr)0x0002d000,
    0x00001000,
    (DALSYSMemAddr)XPU_CFG_SNOC_CFG_MPU1132_4_M19L12_AHB_BASE,
    HWIOModules_XPU_CFG_SNOC_CFG_MPU1132_4_M19L12_AHB
  },
  {
    "SPDM_WRAPPER_TOP",
    (DALSYSMemAddr)0x00040000,
    0x00005000,
    (DALSYSMemAddr)SPDM_WRAPPER_TOP_BASE,
    HWIOModules_SPDM_WRAPPER_TOP
  },
  {
    "SECURITY_CONTROL",
    (DALSYSMemAddr)0x00058000,
    0x00008000,
    (DALSYSMemAddr)SECURITY_CONTROL_BASE,
    HWIOModules_SECURITY_CONTROL
  },
  {
    "RPM_SS_MSG_RAM_START_ADDRESS",
    (DALSYSMemAddr)0x00060000,
    0x00005000,
    (DALSYSMemAddr)RPM_SS_MSG_RAM_START_ADDRESS_BASE,
    HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS
  },
  {
    "PDM_PERPH_WEB",
    (DALSYSMemAddr)0x00068000,
    0x00004000,
    (DALSYSMemAddr)PDM_PERPH_WEB_BASE,
    HWIOModules_PDM_PERPH_WEB
  },
  {
    "BOOT_ROM",
    (DALSYSMemAddr)0x001ff000,
    0x00001000,
    (DALSYSMemAddr)BOOT_ROM_BASE,
    HWIOModules_BOOT_ROM
  },
  {
    "RPM",
    (DALSYSMemAddr)0x00280000,
    0x00009000,
    (DALSYSMemAddr)RPM_BASE,
    HWIOModules_RPM
  },
  {
    "BIMC",
    (DALSYSMemAddr)0x00400000,
    0x0006a000,
    (DALSYSMemAddr)BIMC_BASE,
    HWIOModules_BIMC
  },
  {
    "MPM2_MPM",
    (DALSYSMemAddr)0x004a0000,
    0x0000c000,
    (DALSYSMemAddr)MPM2_MPM_BASE,
    HWIOModules_MPM2_MPM
  },
  {
    "DEHR_BIMC_WRAPPER",
    (DALSYSMemAddr)0x004b0000,
    0x00004000,
    (DALSYSMemAddr)DEHR_BIMC_WRAPPER_BASE,
    HWIOModules_DEHR_BIMC_WRAPPER
  },
  {
    "PC_NOC",
    (DALSYSMemAddr)0x00500000,
    0x00013000,
    (DALSYSMemAddr)PC_NOC_BASE,
    HWIOModules_PC_NOC
  },
  {
    "SYSTEM_NOC",
    (DALSYSMemAddr)0x00580000,
    0x00017000,
    (DALSYSMemAddr)SYSTEM_NOC_BASE,
    HWIOModules_SYSTEM_NOC
  },
  {
    "CRYPTO0_CRYPTO_TOP",
    (DALSYSMemAddr)0x00700000,
    0x00040000,
    (DALSYSMemAddr)CRYPTO0_CRYPTO_TOP_BASE,
    HWIOModules_CRYPTO0_CRYPTO_TOP
  },
  {
    "QDSS_APB_DEC_QDSS_APB",
    (DALSYSMemAddr)0x00800000,
    0x00030000,
    (DALSYSMemAddr)QDSS_APB_DEC_QDSS_APB_BASE,
    HWIOModules_QDSS_APB_DEC_QDSS_APB
  },
  {
    "QDSS_AHB_DEC_QDSS_AHB",
    (DALSYSMemAddr)0x00870000,
    0x00030000,
    (DALSYSMemAddr)QDSS_AHB_DEC_QDSS_AHB_BASE,
    HWIOModules_QDSS_AHB_DEC_QDSS_AHB
  },
  {
    "TLMM",
    (DALSYSMemAddr)0x01000000,
    0x00301000,
    (DALSYSMemAddr)TLMM_BASE,
    HWIOModules_TLMM
  },
  {
    "CLK_CTL",
    (DALSYSMemAddr)0x01800000,
    0x00082000,
    (DALSYSMemAddr)CLK_CTL_BASE,
    HWIOModules_CLK_CTL
  },
  {
    "CORE_TOP_CSR",
    (DALSYSMemAddr)0x01900000,
    0x00058000,
    (DALSYSMemAddr)CORE_TOP_CSR_BASE,
    HWIOModules_CORE_TOP_CSR
  },
  {
    "PMIC_ARB",
    (DALSYSMemAddr)0x02000000,
    0x01907000,
    (DALSYSMemAddr)PMIC_ARB_BASE,
    HWIOModules_PMIC_ARB
  },
  {
    "MSS_TOP",
    (DALSYSMemAddr)0x04000000,
    0x00bb1000,
    (DALSYSMemAddr)MSS_TOP_BASE,
    HWIOModules_MSS_TOP
  },
  {
    "BLSP1_BLSP",
    (DALSYSMemAddr)0x07880000,
    0x0003b000,
    (DALSYSMemAddr)BLSP1_BLSP_BASE,
    HWIOModules_BLSP1_BLSP
  },
  {
    "IPA_WRAPPER",
    (DALSYSMemAddr)0x07900000,
    0x00063000,
    (DALSYSMemAddr)IPA_WRAPPER_BASE,
    HWIOModules_IPA_WRAPPER
  },
  { NULL, 0, 0, 0, NULL }
};

