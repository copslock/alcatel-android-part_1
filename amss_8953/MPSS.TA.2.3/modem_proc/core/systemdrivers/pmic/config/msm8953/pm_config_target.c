/*! \file
 *
 *  \brief  pm_config_target.c ----This file contains customizable target specific driver settings & PMIC registers.
 *  \details This file contains customizable target specific
 * driver settings & PMIC registers. This file is generated from database functional
 * configuration information that is maintained for each of the targets.
 *
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *
 *  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/pmic/config/msm8953/pm_config_target.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/08/15   ps      Added support for MSM8953 Target (CR-903867)
02/27/15   mr      Added support for MSM8976 Target (CR-790476)
10/22/13   rk      GPIO1 used for BUA in 8916.
10/03/13   rk      Changes required for new ULT BUCKs and LDOs
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pm_target_information.h"
#include "pm_npa.h"

#include "pm_mpp_driver.h"
#include "pm_xo_driver.h"
#include "pm_pbs_client.h"
#include "pm_uicc_app.h"
#include "pm_pwr_alg.h"
#include "pmapp_pwr.h"


/*===========================================================================

                    TARGET SPECIFIC DATA DEFINITIONS

===========================================================================*/

/* PM Variant Info for VIRTIO/RUMI only */
uint32  pm_model_info[][3] =
{
    /* PMIC_Model, AllLayer_Rev, MetalLayer_Rev */
    {          22,            1,              0 },    /* PMIC_IS_PM8953 */
    {          17,            1,              0 },    /* PMIC_IS_PMI8950 */
    {          12,            1,              0 },    /* PMIC_IS_PM8004 */
    {           0,            0,              0 }     /* Array terminator */
};

/* PM8953, PMi8950 */
uint32  num_of_smps[]   = {  7,  0};
uint32  num_of_ldo[]    = { 23,  1};
uint32  num_of_clkbuf[] = { 14, 10};
uint32  num_of_gpio[]   = {  8,  2};
uint32  num_of_mpp[]    = {  4,  4};
uint32  num_of_rtc[]    = {  1,  0};
uint32  num_of_talm[]   = {  1,  0};
uint32  num_of_megaxo[] = {  1,  0};
uint32  num_of_boost[]  = {  0,  0};
uint32  num_of_vs[]     = {  0,  0};

pm_xo_core_specific_info_type xocore_specific[1] =
{
    {0x7FF, 0x3F}
};
//use as initial values only, values may be changed runtime and are not static. 
pm_rail_cfg_info_type pm_mss_config_info[4] =
{
    [0]     = {TRUE, 0},  //Used for Voltage_CTL2 in old scheme, used for VSET_LB in Unified Reg 
    [1]     = {TRUE, 0},  //Used for PBS_TRIGGER in old scheme, used for VSET_UB in Unified Reg
    [2]     = {FALSE, 0},
    [3]     = {FALSE, 0},
};

pm_mpp_specific_info_type mpp_specific[3] =
{
    {0x9E0, 4},    /* PM8953 */
    {0x9E0, 4},    /* PMi8950 */
};

pm_uicc_specific_info_type uicc_specific[1] =
{
    {PM_GPIO_3, PM_PBS_CLIENT_3, PM_LDO_14, PM_LDO_15, PM_LDO_INVALID, PM_LDO_INVALID}
};


uint32  mss_stepper_rate = 1500;

pm_pwr_resource_info_type mss_rail[1] =
{
    {RPM_SMPS_A_REQ, 1, NULL, NULL, &mss_stepper_rate }
    // data1 : PBS sequence associated with MSS (if necessary)
    // data2 : mss ocp workaround : disable ocp, save mode, mode=pwm, Enable mss, enable ocp, restore mode
    // data3 : mss stepper rate
};


