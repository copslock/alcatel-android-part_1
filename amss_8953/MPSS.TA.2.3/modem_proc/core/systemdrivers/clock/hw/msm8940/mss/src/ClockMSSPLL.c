/*
===========================================================================
*/
/**
  @file ClockMSSPLL.c 
  
  Modem PLL NPA node definitions for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All rights reserved.
  QUALCOMM Proprietary and Confidential.

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/clock/hw/msm8940/mss/src/ClockMSSPLL.c#2 $
  $DateTime: 2016/02/29 21:06:26 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/29/16   shm     Updated for 8940.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockDriver.h"
#include "ClockMSS.h"
#include "DALDeviceId.h"
#include "DDIVCS.h"

#include <DALSys.h>
#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>
#include "pmapp_npa.h"


/*=========================================================================
      Macros
==========================================================================*/


/*
 * Generic active value for appropriate PLL node.
 */
#define CLOCK_PMIC_NPA_MODE_ID_GENERIC_ACTIVE      1


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Prototypes
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/


/*=========================================================================
      Functions
==========================================================================*/

/* =========================================================================
**  Function : Clock_InitPLL
** =========================================================================*/
/*
  See ClockMSS.h
*/ 

DALResult Clock_InitPLL
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockCPUConfigType     *pConfig;
  ClockSourceNodeType    *pSource;
  HAL_clk_PLLConfigType  *pHALConfig;
  ClockImageCtxtType     *pImageCtxt;
  DALResult               eResult;
  uint32                  nPL, nConfig;
  uint32                  nSourceIndex;
  boolean                 bResult;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Find the max performance level.                                       */
  /*-----------------------------------------------------------------------*/

  nPL = pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel;
  nConfig = pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel[nPL];
  pConfig = &pImageCtxt->pBSPConfig->pCPUConfig[nConfig];

  /*-----------------------------------------------------------------------*/
  /* Satisfy voltage dependency for the max performance level.             */
  /*-----------------------------------------------------------------------*/

  eResult =
    DalVCS_SetCPUCorner(
      pImageCtxt->hVCS,
      CLOCK_CPU_MSS_Q6,
      pConfig->eCornerMSS);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to set voltage for max performance level.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Get source node.                                                      */
  /*-----------------------------------------------------------------------*/

  nSourceIndex = pDrvCtxt->anSourceIndex[pConfig->Mux.HALConfig.eSource];
  if (nSourceIndex == 0xFF)
  {
    return DAL_ERROR;
  }

  pSource = &pDrvCtxt->aSources[nSourceIndex];
  if (pSource == NULL)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Mark the Q6 PLL as having already been calibrated - done in MBA.      */
  /*-----------------------------------------------------------------------*/

  pSource->nFlags |= CLOCK_FLAG_SOURCE_CALIBRATED;

  /*-----------------------------------------------------------------------*/
  /* Slew MPLL0 (Q6PLL) to the max freq config                             */
  /*-----------------------------------------------------------------------*/

  pHALConfig = &pConfig->Mux.pSourceFreqConfig->HALConfig;
  bResult =
    HAL_clk_ConfigPLL(
      pSource->eSource,
      pHALConfig,
      HAL_CLK_SOURCE_CONFIG_MODE_SLEW);
  if (bResult != TRUE)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Set the active config for Q6PLL.                                      */
  /*-----------------------------------------------------------------------*/

  pSource->pActiveFreqConfig = pConfig->Mux.pSourceFreqConfig;

  /*-----------------------------------------------------------------------*/
  /* Configure MPLL1 to the default performance level.                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MPLL1, NULL);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitPLL */

