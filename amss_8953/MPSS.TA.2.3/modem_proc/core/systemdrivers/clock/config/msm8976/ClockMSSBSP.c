/*
===========================================================================
*/
/**
  @file ClockMSSBSP.c 
  
  BSP data for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2011-15 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/clock/config/msm8976/ClockMSSBSP.c#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  09/23/13   stu     Branched from 8x62 code base.
  10/19/11   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "ClockMSSBSP.h"
#include "comdef.h"
#include "VCSDefs.h"


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * Enumeration of QDSP6 performance levels.
 */
enum
{
  CLOCK_QDSP6_PERF_LEVEL_0,
  CLOCK_QDSP6_PERF_LEVEL_1,
  CLOCK_QDSP6_PERF_LEVEL_2,
  CLOCK_QDSP6_PERF_LEVEL_3,
  CLOCK_QDSP6_PERF_LEVEL_4,
  CLOCK_QDSP6_PERF_LEVEL_5,
  CLOCK_QDSP6_PERF_LEVEL_6,
  CLOCK_QDSP6_PERF_LEVEL_7,
  CLOCK_QDSP6_PERF_LEVEL_8,
  CLOCK_QDSP6_PERF_LEVEL_9,
  CLOCK_QDSP6_PERF_LEVEL_10,
  CLOCK_QDSP6_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of Scalar QDSP6 configurations.
 */
enum
{
  CLOCK_QDSP6_CONFIG_XO,
  CLOCK_QDSP6_CONFIG_115P20MHz,
  CLOCK_QDSP6_CONFIG_144MHz,
  CLOCK_QDSP6_CONFIG_230P40MHz,
  CLOCK_QDSP6_CONFIG_288MHz,
  CLOCK_QDSP6_CONFIG_384MHz,
  CLOCK_QDSP6_CONFIG_480MHz,
  CLOCK_QDSP6_CONFIG_576MHz,
  CLOCK_QDSP6_CONFIG_614P40MHz,
  CLOCK_QDSP6_CONFIG_691P20MHz,
  CLOCK_QDSP6_CONFIG_TOTAL
};

/*
 * Enumeration of Vector QDSP6 configurations.
 */
enum
{
  CLOCK_QDSP6CP_CONFIG_XO,
  CLOCK_QDSP6CP_CONFIG_115P20MHz,
  CLOCK_QDSP6CP_CONFIG_144MHz,
  CLOCK_QDSP6CP_CONFIG_211P20MHz,
  CLOCK_QDSP6CP_CONFIG_230P40MHz,
  CLOCK_QDSP6CP_CONFIG_288MHz,
  CLOCK_QDSP6CP_CONFIG_364P80MHz,
  CLOCK_QDSP6CP_CONFIG_460P8MHz,
  CLOCK_QDSP6CP_CONFIG_556P80MHz,
  CLOCK_QDSP6CP_CONFIG_576MHz,
  CLOCK_QDSP6CP_CONFIG_652P80MHz,
  CLOCK_QDSP6CP_CONFIG_TOTAL
};


/*
 * Enumeration of MSS Config Bus performance levels.
 */
enum
{
  CLOCK_CONFIG_BUS_PERF_LEVEL_0,
  CLOCK_CONFIG_BUS_PERF_LEVEL_1,
  CLOCK_CONFIG_BUS_PERF_LEVEL_2,
  CLOCK_CONFIG_BUS_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of MSS Config Bus configurations.
 */
enum
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz,
  CLOCK_CONFIG_BUS_CONFIG_TOTAL
};


/*=========================================================================
      Data
==========================================================================*/

/*
 * The Q6PLL source frequency configurations are defined in ClockBSP.c
 */
extern ClockSourceFreqConfigType SourceFreqConfig_Q6PLL[10];

/*
 * Scalar QDSP6 performance levels.
 */
static uint32 Clock_CPUPerfLevels[] =
{
  CLOCK_QDSP6_CONFIG_XO,
  CLOCK_QDSP6_CONFIG_115P20MHz,
  CLOCK_QDSP6_CONFIG_144MHz,
  CLOCK_QDSP6_CONFIG_230P40MHz,
  CLOCK_QDSP6_CONFIG_288MHz,
  CLOCK_QDSP6_CONFIG_384MHz,
  CLOCK_QDSP6_CONFIG_480MHz,
  CLOCK_QDSP6_CONFIG_576MHz,
  CLOCK_QDSP6_CONFIG_614P40MHz,
  CLOCK_QDSP6_CONFIG_691P20MHz
};

/*
 * Vector QDSP6 performance levels.
 */
static uint32 Clock_CPU1PerfLevels[] =
{
  CLOCK_QDSP6CP_CONFIG_XO,
  CLOCK_QDSP6CP_CONFIG_115P20MHz,
  CLOCK_QDSP6CP_CONFIG_144MHz,
  CLOCK_QDSP6CP_CONFIG_211P20MHz,
  CLOCK_QDSP6CP_CONFIG_230P40MHz,
  CLOCK_QDSP6CP_CONFIG_288MHz,
  CLOCK_QDSP6CP_CONFIG_364P80MHz,
  CLOCK_QDSP6CP_CONFIG_460P8MHz,
  CLOCK_QDSP6CP_CONFIG_556P80MHz,
  CLOCK_QDSP6CP_CONFIG_576MHz,
  CLOCK_QDSP6CP_CONFIG_652P80MHz,
};


/*
 * Config Bus performance levels
 */
static uint32 Clock_ConfigBusPerfLevels [] =
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz,
};


/*
 * Mux configuration for different CPU frequencies.
 */
static ClockCPUConfigType Clock_CPUConfig[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz    = 19200 * 1000,
      .HALConfig  = {HAL_CLK_SOURCE_XO, 2},
      .HWVersion  = {{0x0, 0x0}, {0xFF, 0xFF}},
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 115200 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 10},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576.00 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 144000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 8},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 230400 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 6},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[10] // 691.20 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 288000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 4},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 384000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[1] // 384.0 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 480000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[4] // 480 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_PLUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 576000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_NOMINAL
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 614400 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[8] // 614.4 MHz
    },
    .eCornerMSS = VCS_CORNER_NOMINAL_PLUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 691200 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL0, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[10] // 691.2 MHz
    },
    .eCornerMSS = VCS_CORNER_TURBO
  },
};

/*
 * Mux configuration for different CPU frequencies.
 */
static ClockCPUConfigType Clock_CPU1Config[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz    = 19200 * 1000,
      .HALConfig  = {HAL_CLK_SOURCE_XO, 2},
      .HWVersion  = {{0x0, 0x0}, {0xFF, 0xFF}},
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 115200 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 10},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576.00 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 144000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 8},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 211200 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 4},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[2] // 422.4 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_MINUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 230400 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 6},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[10] // 691.20 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 288000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 4},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 364800 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[0] // 364.8 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 460800 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[3] // 460.8 MHz
    },
    .eCornerMSS = VCS_CORNER_LOW_PLUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 556800 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[6] // 556.8 MHz
    },
    .eCornerMSS = VCS_CORNER_NOMINAL
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 576000 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[7] // 576 MHz
    },
    .eCornerMSS = VCS_CORNER_NOMINAL_PLUS
  },
  {
    .CoreConfig     = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux            =
    {
      .nFreqHz           = 652800 * 1000,
      .HALConfig         = {HAL_CLK_SOURCE_MPLL2, 2},
      .HWVersion         = {{0x0, 0x0}, {0xFF, 0xFF}},
      .pSourceFreqConfig = &SourceFreqConfig_Q6PLL[9] // 652.8 MHz
    },
    .eCornerMSS = VCS_CORNER_TURBO
  },
};


/*
 * Performance level configuration data for the CPU clock.
 */
static ClockCPUPerfConfigType Clock_CPUPerfConfig[] =
{
  {
    .HWVersion      = {{0x0, 0x0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_9,
    .anPerfLevel    = Clock_CPUPerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_CPUPerfLevels)
  }
};

/*
 * Performance level configuration data for the CPU clock.
 */
static ClockCPUPerfConfigType Clock_CPU1PerfConfig[] =
{
  {
    .HWVersion      = {{0x0, 0x0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_10,
    .anPerfLevel    = Clock_CPU1PerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_CPU1PerfLevels)
  }
};


/*
 * Mux configuration for different MSS Config Bus frequencies.
 *
 * NOTE:
 *  The HW running off the MSS config bus is powered by /vdd/cx except for
 *  crypto which is powered by /vdd/mss.  The below configurations contain
 *  voltage requirements for both power domains to support all HW running
 *  off the MSS config bus.
 */
static ClockConfigBusConfigType Clock_ConfigBusConfig[] =
{
  /*  NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    /* .Mux */   { 19200 * 1000, { HAL_CLK_SOURCE_XO, 2 }, VCS_CORNER_LOW }
  },
  {
    /* .Mux */   { 72000 * 1000, { HAL_CLK_SOURCE_MPLL1, 16 }, VCS_CORNER_LOW }
  },
  {
    /* .Mux */   { 144000 * 1000, { HAL_CLK_SOURCE_MPLL1, 8 }, VCS_CORNER_NOMINAL }
  }
};

/*
 * Performance level configuration data for the MSS Config Bus.
 */
static ClockConfigBusPerfConfigType Clock_ConfigBusPerfConfig =
{
  .nMinPerfLevel = CLOCK_CONFIG_BUS_PERF_LEVEL_0,
  .nMaxPerfLevel = CLOCK_CONFIG_BUS_PERF_LEVEL_1,
  .anPerfLevel   = Clock_ConfigBusPerfLevels
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfig =
{
  .bEnableDCS               = TRUE,
  .pCPUConfig               = Clock_CPUConfig,
  .pCPU1Config              = Clock_CPU1Config,
  .pCPUPerfConfig           = Clock_CPUPerfConfig,
  .pCPU1PerfConfig          = Clock_CPU1PerfConfig,
  .nNumCPUPerfLevelConfigs  = ARR_SIZE(Clock_CPUPerfConfig),
  .nNumCPU1PerfLevelConfigs = ARR_SIZE(Clock_CPU1PerfConfig),
  .pConfigBusConfig         = Clock_ConfigBusConfig,
  .pConfigBusPerfConfig     = &Clock_ConfigBusPerfConfig
};
