0. Using Q6ZIP
-------------------------------------------------------------------------------

A. Initialize Q6ZIP package

   ...

   /* Somewhere in DL Pager initialization */

   ...

   /* Initialize Q6ZIP */
   q6zip_module_init();


B. Initialize Q6ZIP implementation
   Q6ZIP has two implementations - Q6ZIP_SW and Q6ZIP_IPA. Currently only
   Q6ZIP_SW is supported. Here's how to initialize it ...

   /* q6zip_initialized is invoked *ONCE* Q6ZIP_SW has initialized.
      q6zip_request_processed is invoked *ONCE* for every request submitted */
   Q6ZIP_SW.init( ro_dict, ro_dict_len, q6zip_initialized, q6zip_requested_processed );


C. Submit *ONE* request

   {
       q6zip_error_t     error;
       q6zip_request_t * request = q6zip_request_alloc();

       request->src.ptr = <pointer-to-source-buffer>
       request->src.len = <length-in-bytes-of-source-buffer>
       request->dst.ptr = <pointer-to-destination-buffer>
       request->dst.len = <length-in-bytes-of-destination-buffer>
       request->priority = <priority-of-request>
       request->algo = <algorithm-to-use>

       error = Q6ZIP_SW.submit( &request );

       /* If error != Q6ZIP_ERROR_NONE, request was not submitted */
   }

D. Submit a *PAIR OF* requests
   {
       q6zip_request_pair_t pair;

       pair->first = q6zip_request_alloc();
       pair->second = q6zip_request_alloc();

       /* Ensure that both requests are non-NULL! */

       /* Fill *each* request */
       error = Q6ZIP_SW.submit_pair( &pair );

       /* If error != Q6ZIP_ERROR_NONE, request was not submitted */
   }

E. Cleanup Q6ZIP implementation

   Q6ZIP_SW.fini();

F. Cleanup Q6ZIP package

   q6zip_module_fini();

