/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //source/qcom/qct/core/kernel/dl_pager_qurt/dev/async_pager/inc/dlpager.h

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rr     Header file for demand paging
===========================================================================*/
#define THREAD_NULL (0xffffffff)


/* Align to page boundry with possible truncation 
 * size is a power of 2 */
#define DLPAGER_ALIGN_DOWN(addr, size) (addr & ~(size - 1))

/* Align to page boundry rounding up to a higher address page
 * size is a power of 2 */
#define DLPAGER_ALIGN_UP(addr, size) ((addr + size - 1) & ~(size - 1))

int __attribute__((__constructor__))  dlpager_init(void);
