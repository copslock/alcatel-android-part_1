/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_q6zip_iface.h#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who          what, where, why
--------   -------      ----------------------------------------------------------
12/09/14   rc,rr,cp     Header file for interfacing with q6zip module
===========================================================================*/

#include <dlpager_types.h>

void dlpager_q6zip_schedule_decompression(unsigned int fault_page_va, unsigned int thread_id, unsigned int tasklog_idx);

void dlpager_q6zip_schedule_compression(unsigned int src_page_va, dlpager_iovec_t rw_iovec, dlpager_priority_t prio);

int dlpager_q6zip_init(void);

/**
 * @brief Initialize any post NPA dependent modules
 * @return 0
 */
int dlpager_q6zip_npa_init (void);

