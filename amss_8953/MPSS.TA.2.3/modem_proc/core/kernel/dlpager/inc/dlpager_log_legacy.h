#pragma once
#include "qurt.h"
/** Log entry that logs the pager activity. The entries are logged when 
    when page fault happens. */
struct dlpager_log_entry {
    unsigned int loaded_addr; /**< Loaded address (address with in this page caused page fault */
    unsigned int evicted_addr; /**< Evicted address. If no page is evicted it shows "0" */
    qurt_thread_t thread_id; /**< Fault thread Id */
    unsigned long long pcycles; 
    unsigned long long ticks;
};

