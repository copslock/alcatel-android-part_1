#===============================================================================
#
# DL PAGER Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2009 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/build/SConscript#3 $
#  $DateTime: 2015/12/17 03:15:10 $
#  $Author: pwbldsvc $
#-------------------------------------------------------------------------------
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who      what, where, why
# ---------- -------- ----------------------------------------------------------
# 05/10/2015 anandj   Exclude Q6ZIP IPA dependencies for core-only builds
# 03/15/2010 op       created 
#===============================================================================
Import('env')
env = env.Clone()
env.Replace(HEXAGONCC_OPT = "${HEXAGON_OPT_2}")

import sys
sys.path.append("../compressor")
import q6zip_versions

# @warning USES_COREIMG is defined for *core-only* builds. However, unlike
# other uses flags, it doesnt make it into env[ 'USES_FLAGS' ]. Hack below
# is to append it so that correct Q6Zip versions are picked
if env.has_key( 'USES_COREIMG' ) and env[ 'USES_COREIMG' ] == 'yes':
    env[ 'USES_FLAGS' ].append( 'USES_COREIMG' )

# Initialize q6zip_versions module
# @warning *MUST* be done before accessing variables exported by this module
q6zip_versions.init( env[ 'USES_FLAGS' ] )

env.Append(CCFLAGS = q6zip_versions.CCFLAGS)

# Local/ultimate check if plugin should be used
# For core-only builds, Q6Zip plugin can *never* be used as it lies outside core/
is_q6zip_plugin_used = not 'USES_COREIMG' in env and 'USES_Q6ZIP_PLUGIN' in env

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${COREBSP_ROOT}/kernel/dlpager"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)
env.VariantDir('${BUILDPATH}/compressor/'+q6zip_versions.RO_RUNTIME_PARTIAL_SRCPATH, q6zip_versions.RO_RUNTIME_FULL_SRCPATH, duplicate=0)
env.VariantDir('${BUILDPATH}/compressor/'+q6zip_versions.RW_RUNTIME_PARTIAL_SRCPATH, q6zip_versions.RW_RUNTIME_FULL_SRCPATH, duplicate=0)


IMAGE_SOURCES = ['MODEM_MODEM','CBSP_MODEM_IMAGE','CORE_MODEM','CORE_QDSP6_SW']

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [ 'KERNEL' , 'DAL', 'POWER' , 'SERVICES', 'STORAGE', 'DEBUGTOOLS', 'WIREDCONNECTIVITY']

env.PublishPrivateApi('DLPAGER', ['${COREBSP_ROOT}/kernel/dlpager/inc', q6zip_versions.RO_RUNTIME_FULL_INCPATH, q6zip_versions.RW_RUNTIME_FULL_INCPATH])
env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
DL_PAGER_LOADER_C_SOURCES = [
   '${BUILDPATH}/src/dlpager_handlers.c',
   '${BUILDPATH}/src/dlpager_log.c',
   '${BUILDPATH}/src/dlpager_meta.c',
   '${BUILDPATH}/src/dlpager_pagesm.c',
   '${BUILDPATH}/src/dlpager_q6zip_iface.c',
   '${BUILDPATH}/src/dlpager_swappool.c',
   '${BUILDPATH}/src/dlpager_waitlist.c',
   '${BUILDPATH}/src/dlpager_stats.c',
   '${BUILDPATH}/src/dlpager_rwbuffer.c',
   '${BUILDPATH}/src/dlpager_t32.c',
   '${BUILDPATH}/src/dlpager_debug.c',
   '${BUILDPATH}/src/dlpager_queue.c',
   '${BUILDPATH}/src/q6_dczero.s',
]

DL_PAGER_LIB_C_SOURCES = [
   '${BUILDPATH}/src/dlpager.c', 
]

# Base Q6ZIP related sources
Q6ZIP_BASE_SOURCES = [
   '${BUILDPATH}/src/histogram.c',
   '${BUILDPATH}/src/q6zip_clk.c',
   '${BUILDPATH}/src/q6zip_context.c',
   '${BUILDPATH}/src/q6zip_log.c',
   '${BUILDPATH}/src/q6zip_module.c',
   '${BUILDPATH}/src/q6zip_request.c',
   '${BUILDPATH}/src/q6zip_sw.c',
   '${BUILDPATH}/src/q6zip_sw_worker.c',
   '${BUILDPATH}/src/q6zip_waiter.c',
   '${BUILDPATH}/src/q6zip_worker.c',
]

# Append implementation of Q6ZIP SW algorithms
Q6ZIP_BASE_SOURCES += [ '${BUILDPATH}/compressor/' + src for src in q6zip_versions.SRCLIST ]

# Q6ZIP IPA related sources
Q6ZIP_IPA_SOURCES = [
   '${BUILDPATH}/src/q6zip_ipa.c',
   '${BUILDPATH}/src/q6zip_ipa_async_initializer.c'
]

# Q6ZIP (on-target) test sources. Useful for IPA vector runs
# @warning Needs to be included manually!
Q6ZIP_TEST_SOURCES = [ 
   '${BUILDPATH}/src/q6zip_test.c',
   '${BUILDPATH}/src/q6zip_test_scenario_copy.c',
   '${BUILDPATH}/src/q6zip_test_scenario_init.c',
   '${BUILDPATH}/src/q6zip_test_scenario_error_ro.c',
   '${BUILDPATH}/src/q6zip_test_scenario_unzip_ro.c',
   '${BUILDPATH}/src/q6zip_test_scenario_unzip_rw.c',
   '${BUILDPATH}/src/q6zip_test_scenario_zip_rw.c',
   '${BUILDPATH}/src/q6zip_test_scenario_mixed.c',
   '${BUILDPATH}/src/q6zip_test_vectors_ro.c',
   '${BUILDPATH}/src/q6zip_test_vectors_rw.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

if env.has_key('USES_QURT_DEMAND_PAGE'):

   sources = [ DL_PAGER_LIB_C_SOURCES, DL_PAGER_LOADER_C_SOURCES ]

   # If Q6ZIP plugin is *not* used, need to pull-in Q6Zip sources for
   # for compilation as part of DL pager library
   if not is_q6zip_plugin_used:
      sources +=  Q6ZIP_BASE_SOURCES

      if q6zip_versions.ENABLE_Q6ZIP_IPA:
         sources += Q6ZIP_IPA_SOURCES

   env.AddLibrary(
      ['MODEM_MODEM','CBSP_MODEM_IMAGE','MODEM_IMAGE','CORE_MODEM','CORE_QDSP6_SW'],
      '${BUILDPATH}/dlpager', sources )

RCINIT_IMG = ['CORE_MODEM', 'CORE_QDSP6_SW']
if 'USES_RCINIT' in env and 'USES_QURT_DEMAND_PAGE' in env:

   if not is_q6zip_plugin_used:
      env.AddRCInitFunc(         # Code Fragment in TMC: YES
       RCINIT_IMG,               # define TMC_RCINIT_INIT_TIMER_TASK_INIT
       {
        'sequence_group'             : 'RCINIT_GROUP_0',              # required
         'init_name'                  : 'dlpager_q6zip',              # required
         'init_function'              : 'dlpager_q6zip_npa_init',     # required
         'dependencies'               : ['npa','clk_regime']
       })

   env.AddRCInitFunc(           # Code Fragment in TMC: NO
    RCINIT_IMG,                 # define RCINIT_IMG
    {
     'sequence_group'             : 'RCINIT_GROUP_0',              # required
     'init_name'                  : 'dlpager',                     # required
     'init_function'              : 'dlpager_init',                # required
     'dependencies'               : ['dlpager_q6zip',]

    })

if 'USES_CMMBUILDER' in env:
    OPTPATH=env.subst(env['BUILD_ROOT'] + "/core/kernel/dlpager/compressor")
    env.AddCMMScripts ('MPSS', [OPTPATH], { 'q6zip_t32.opt' : ['Q6ZIP Tool', 'Q6ZPATH'] }, 'Kernel')
