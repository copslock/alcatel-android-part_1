#include "rw_compress.h"
#include <string.h>
#include <stdint.h>
#include <hexagon_protos.h>
#include <math.h>

#define NUM_ANCHORS 4 // must be a power of 2
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_CODE_BITS 3
#define NUM_ZERO_FLE_CODE_BITS 1
#define VAR_CODE_BITS(b) (1 + ((b&1)<<1))
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))
 
#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif


/*********************************************************************************************************/        



#define SKIP_BITS_W_CHECK(compressedInPtr,bitCount,hold,n)      \
  bitCount -= n;                                    \
  hold >>= n;                                                   \
  hold |= (uint64_t)(*(compressedInPtr)) << bitCount;     \
  if (UNLIKELY(bitCount <= WORD_SIZE_IN_BITS)) compressedInPtr++;       \
  if (UNLIKELY(bitCount <= WORD_SIZE_IN_BITS)) bitCount += WORD_SIZE_IN_BITS; 



#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)


/* DCZERO the output buffer, 32 bytes at a time.
   Assume page size is a multiple of 32
   Assume dest addr is multiple of 32
*/
/* linked in from dczero assembly version in dlpager/src */
void dczero(unsigned int addr, unsigned int size);


#define SUPER_ZERO_WORDS 16
//#define SUPER_RAW_WORDS 16
#define SUPER_RAW_WORDS 10
#define ZERO_FLE_WORDS 4 // sed

#define NUM_DELTA_BITS 9 // sed
#define PARTIAL_MATCH_MASK   ((~(0)) << NUM_DELTA_BITS)
//#define NUM_LOOKBACK_BITS(x) ((unsigned int)ceil(log(x)/log(2))) 
/*unsigned int NUM_LOOKBACK_BITS(x)
{
        int num_leading_zeros = Q6_R_cl0_R(x);
        int pcount = Q6_R_popcount_P(x);
        int num_lookback_bits = 32 - (num_leading_zeros + 1);
        if(pcount > 1)
                num_lookback_bits++;
        return num_lookback_bits;
}*/
//#define NUM_LOOKBACK_BITS(x)  (32 - (Q6_R_cl0_R(x) + 1) + (Q6_R_popcount_P(x) > 1 ? 1 : 0))
#define NUM_LOOKBACK_BITS 7 // sed

//uncompress a word-aligned buffer compressed of length in_len words into a word-aligned buffer uncompressed of of lengh out_len words
//unsigned int deltaUncompress(unsigned int *compressed,unsigned int in_len,unsigned int *uncompressed,unsigned int out_len)
unsigned int deltaUncompress(unsigned int *compressed,unsigned int *uncompressed,unsigned int out_len)
{
  int i;
  unsigned int delta;
  unsigned int code,val,size,anchor,size_of_raw_stream;
  int lookback;
  unsigned int numAvailBits = 0;
  unsigned int bufIndex = 0;
  unsigned long long int hold = 0;
  unsigned int bits;
  void* jump_table[] = {
    &&CODE_ZEROFLE, //0 // 000
    &&CODE_ZERO,    //1 // 001
    &&CODE_ZEROFLE, //2 // 010
    &&CODE_FULL,    //3 // 011
    &&CODE_ZEROFLE, //4 // 100
    &&CODE_PARTIAL, //5 // 101
    &&CODE_ZEROFLE, //6 // 110
    &&CODE_RAW      //7 // 111
  };
  unsigned int *uncompressed_start = uncompressed;
  unsigned int *lastWord = &uncompressed[out_len-1];
  unsigned int *compressed_raw;
  unsigned int *compressed_encoded;
  int debug_val = 0;

  void *jumpTarget;
  void *nextJumpTarget;


  dczero((unsigned int)uncompressed, 4096);

  size = 4096;

  // when compressed buffers are reallocated, they may not be dword aligned
  // in that case, the first word is -1 and should be skipped
  if(compressed[0] == 0xFFFFFFFF){
          compressed++;
  }
  size_of_raw_stream = compressed[0] & 0x0000FFFF; // mask out size-of-encoded-section, if there
  compressed_raw     = &(compressed[1]);
  compressed_encoded = &(compressed[size_of_raw_stream + 1]); // +1 to skip size/first entry

  
  //load up 64bits into the holding register
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,0);

  //process the input
  bits = GET_BITS(hold,NUM_CODE_BITS);
  jumpTarget=jump_table[bits];
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,1+((bits&1)<<1));
  bits = GET_BITS(hold,NUM_CODE_BITS);
  nextJumpTarget=jump_table[bits];
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits));
  goto *jumpTarget;



CODE_SUPER_ZERO:
    //SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS*SUPER_ZERO_WORDS); // TODO: why does this even work?
    debug_val = uncompressed - uncompressed_start;
    //printf("word @ super zero: %d\n", debug_val);
    SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,4);
    uncompressed += SUPER_ZERO_WORDS;
    if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
    //goto CODE_ZERO;
    goto CODE_ZEROFLE;
CODE_SUPER_RAW:
    SKIP_BITS_W_CHECK(compressed_encoded, numAvailBits, hold, NUM_CODE_BITS*SUPER_RAW_WORDS);
    for(i = 0; i < SUPER_RAW_WORDS; ++i){
        val = *compressed_raw++;
        *uncompressed++ = val;
    }
    if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
    goto CODE_RAW;

CODE_ZEROFLE:
      if ( (nextJumpTarget == &&CODE_ZEROFLE) && ( ( ((unsigned int)hold) & 0x0000000F) == 0 ) ) goto CODE_SUPER_ZERO;
      jumpTarget = nextJumpTarget;
      bits = GET_BITS(hold, NUM_CODE_BITS);
      nextJumpTarget = jump_table[bits];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits));
      uncompressed += ZERO_FLE_WORDS;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;

CODE_ZERO:
      //if ( (nextJumpTarget == &&CODE_ZERO) && ( ( ((unsigned int)hold) & 0x0000000F) == 0 ) ) goto CODE_SUPER_ZERO; // TODO: Why does this even work?
      jumpTarget = nextJumpTarget;
      bits = GET_BITS(hold, NUM_CODE_BITS);
      nextJumpTarget = jump_table[bits];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits));
      uncompressed++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;

CODE_FULL:
      lookback = 0xFFFFFF80 | (GET_BITS(hold, NUM_LOOKBACK_BITS));
      lookback = (~lookback) + 1;
      jumpTarget = nextJumpTarget;
      bits = GET_BITS(hold>>NUM_LOOKBACK_BITS,NUM_CODE_BITS);
      nextJumpTarget = jump_table[bits];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits)+NUM_LOOKBACK_BITS);
      *uncompressed = *(uncompressed - lookback);
      uncompressed++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;

CODE_PARTIAL:
      lookback = 0xFFFFFF80 | (GET_BITS(hold, NUM_LOOKBACK_BITS));
      lookback = (~lookback) + 1;
      delta = GET_BITS(hold>>NUM_LOOKBACK_BITS, NUM_DELTA_BITS);
      jumpTarget = nextJumpTarget;
      bits = GET_BITS(hold>>(NUM_DELTA_BITS+NUM_LOOKBACK_BITS),NUM_CODE_BITS);
      nextJumpTarget = jump_table[bits];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits)+NUM_DELTA_BITS+NUM_LOOKBACK_BITS);
      *uncompressed = ((*(uncompressed - lookback) & PARTIAL_MATCH_MASK) | delta);
      uncompressed++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;

CODE_RAW:
      // no exact or partial match
      if ((nextJumpTarget == &&CODE_RAW ) && ( (((unsigned int)hold) & 0x3FFFFFFF) == 0x3FFFFFFF ) ) goto CODE_SUPER_RAW;
      val = *compressed_raw++;
      *uncompressed++ = val;

      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;

      jumpTarget=nextJumpTarget;
      bits = GET_BITS(hold,NUM_CODE_BITS);
      nextJumpTarget=jump_table[bits];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,VAR_CODE_BITS(bits));
      goto *jumpTarget;

RETLAB:
      return (size);
}
