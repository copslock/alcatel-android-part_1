#! /usr/bin/env python
import struct
import math
import ctypes

firstHeader=1
payloadHoldList=[]

#codeHistogram=[0,0,0,0]
codeHistogram=[0,0,0,0,0,0,0,0]

#d = 0
c = 0

def pushCompressedBitsRaw(bits,compressed):
    global numCompressedWordsRaw
    compressed.append(bits)

def pushCompressedBitsEncoded(bits,numBits,compressed):
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsEncoded
    #global d

    temp = bits << numCompressedPartialBitsEncoded
    numCompressedPartialBitsEncoded += numBits
    compressedPartialWordEncoded = compressedPartialWordEncoded | temp

    #if d < 10:
    #    print "\td: %d" % d
    #    print "\tcompressedPartialWordEncoded 0x%x" % compressedPartialWordEncoded
    #    print "\tnumCompressedWordsEncoded: %d" % numCompressedWordsEncoded
    #    print "\tnumCompressedPartialBitsEncoded: %d" % numCompressedPartialBitsEncoded
    #d += 1

    if numCompressedPartialBitsEncoded >= 32:                                                                               
        compressed.append(compressedPartialWordEncoded & 0xFFFFFFFF)
        numCompressedWordsEncoded += 1                                              
        compressedPartialWordEncoded = compressedPartialWordEncoded >> 32   
        numCompressedPartialBitsEncoded -= 32
        
def finalizeCompressedBits(compressed):  
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsencoded

    if numCompressedPartialBitsEncoded > 0:
        compressed.append(compressedPartialWordEncoded & 0xFFFFFFFF)  
        numCompressedWordsEncoded += 1   

def pushHeader(bits,numBits,compressed):
    global payloadHoldList
    global codeHistogram
    codeHistogram[bits]+=1
    pushCompressedBitsEncoded(bits,numBits,compressed)

    while(payloadHoldList):
        [bits1,numBits1]=payloadHoldList.pop()
        pushCompressedBitsEncoded(bits1,numBits1,compressed)

def pushPayload(bits,numBits,compressed):
    global payloadHoldList
    payloadHoldList.append([bits,numBits])
    #pushCompressedBits(bits,numBits,compressed)
    #[bits1,numBits1]=payloadHoldList.pop()
    #pushCompressedBits(bits1,numBits1,compressed)



NUM_CODE_BITS = 3
NUM_ZERO_FLE_CODE_BITS = 1
RWCHASH_NUM_HASH_BITS = 9 # sed
RWCHASH_NUM_MASK_BITS = 9 # sed
MSB_KEEP_MASK = ((~(0)) << RWCHASH_NUM_MASK_BITS)  #  bit-wise and with this mask to keep the unmasked upper bits of the input word.
LSB_KEEP_MASK = ~MSB_KEEP_MASK  # bit-wise and with this mask to extract the unmatched part of an input word from that word.
NUM_LOOKBACK_BITS = 7 # sed
MAX_LOOKBACK      = (1 << NUM_LOOKBACK_BITS)
#NEG_LOOKBACK_MSB = ((~0) << NUM_LOOKBACK_BITS)
ZERO_FLE_MAX = 4 # sed

#def NUM_LOOKBACK_BITS(x):
#        return int(math.ceil(math.log(x, 2)))



def rwc_hash(i):
        #return ((i) * 2654435761) >> (32 - RWCHASH_NUM_HASH_BITS)
        return ((i * 2654435761) & 0xFFFFFFFF) >> (32 - RWCHASH_NUM_HASH_BITS)




fp_code_sequence = open("code_sequence", "w")
code_sequence = [0] * 1024
code_sequence_i = 0

def update_code_sequence(code):
        global code_sequence
        global code_sequence_i

        code_sequence[code_sequence_i] = code
        code_sequence_i += 1

def deltaCompress (uncompressed,compressedStreamRaw):
    global numCompressedWordsRaw
    global compressedPartialWordRaw
    global numCompressedPartialBitsRaw
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsEncoded
    global hashtable
    global hashtable_partial
    global code_sequence
    global code_sequence_i

    numCompressedWordsRaw = 0
    compressedPartialWordRaw = 0
    numCompressedPartialBitsRaw = 0
    numCompressedWordsEncoded = 0
    compressedPartialWordEncoded = 0
    numCompressedPartialBitsEncoded = 0

    compressedStreamEncoded = []
    compressedStreamRaw.append(0) # will overwrite with size of raw stream later

    hashtable = [0] * (1 << RWCHASH_NUM_HASH_BITS)
    hashtable_partial = [0] * (1 << RWCHASH_NUM_HASH_BITS)
    zero_count = 0

    

    for i in xrange(len(uncompressed)):
        val = uncompressed[i]
        #if i < 20:
        #        print "i: %d" % i

        if (val == 0):
            zero_count += 1
            if zero_count == ZERO_FLE_MAX:
                # zeroFLE-4 match, code 0
                #if i < 20:
                #    print "\tzerofle"
                pushHeader(0, NUM_ZERO_FLE_CODE_BITS, compressedStreamEncoded)
                zero_count = 0
                update_code_sequence(0)
            continue

        while zero_count > 0:
            # zero match, code 001
            #if i < 20:
            #    print "\tzero"
            pushHeader(1, NUM_CODE_BITS, compressedStreamEncoded)
            zero_count -= 1
            update_code_sequence(1)


        idx = rwc_hash(val)
        idx_val = hashtable[idx]
        uncompr_idx   = idx_val >> 32
        hashtable_val = idx_val & 0xFFFFFFFF
        lookback = i - uncompr_idx

        idx_partial = rwc_hash(val & MSB_KEEP_MASK)
        #if i < 20:
        #    print "\tidx: %d" % idx
        #    #print "\tidx_partial: %d" % idx_partial
        #    print "\tfull attempt with lookback: %d" % lookback
        #    print "\tfull attempt with hashtable_val: %d" % hashtable_val

        #if (hashtable_val == val) and (lookback > 0) and (lookback < MAX_LOOKBACK):
        #if (hashtable_val == val) and (hashtable_val > 0) and (lookback < MAX_LOOKBACK):
        # cporter trying s/thing use right above for fix
        if (hashtable_val == val) and (lookback < MAX_LOOKBACK):
            # full match, code 011
            #if i < 20:
            #    print "\tfull"
            #    print "\tval: 0x%x" % val
            #    print "\thashtable_val: 0x%x" % hashtable_val
            #    print "\tlookback: 0x%x" % lookback
            #    #print "\tnegative lookback: 0x%x" % (((~lookback)+1) & 0xFFFFFFFF)
            #    print "\tnegative lookback: 0x%x" % (((~lookback)+1) & 0x0000007F)
            pushHeader(3, NUM_CODE_BITS, compressedStreamEncoded)
            pushPayload((((~lookback)+1) & 0x0000007F), NUM_LOOKBACK_BITS, compressedStreamEncoded)
            hashtable[idx]                 = (i << 32) | val
            update_code_sequence(3)
            hashtable_partial[idx_partial] = (i << 32) | val
            continue

        idx_val_partial = hashtable_partial[idx_partial]
        uncompr_idx_partial = idx_val_partial >> 32
        hashtable_val_partial = idx_val_partial & 0xFFFFFFFF
        lookback_partial = i - uncompr_idx_partial
        #if i < 20:
        #    print "\tpartial attempt with lookback_partial: %d" % lookback_partial
        #    print "\tpartial attempt with hashtable_val_partial: %d" % hashtable_val_partial
        #if ((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) and (lookback_partial > 0) and (lookback_partial < MAX_LOOKBACK):
        if ((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) and (hashtable_val_partial > 0) and (lookback_partial < MAX_LOOKBACK):
        # cporter trying something. see right above for fix
        #if ((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) and (lookback_partial < MAX_LOOKBACK):
            # partial match, code 101
            #if i < 20:
            #    print "\tpartial"
            #    print "\tval: 0x%x" % val
            #    print "\thashtable_val_partial: 0x%x" % hashtable_val_partial
            #    print "\tlookback_partial: 0x%x" % lookback_partial
            #    #print "\tnegative lookback_partial: 0x%x" % (((~lookback_partial)+1) & 0xFFFFFFFF)
            #    print "\tnegative lookback_partial: 0x%x" % (((~lookback_partial)+1) & 0x0000007F)
            pushHeader(5, NUM_CODE_BITS, compressedStreamEncoded)
            #pushPayload(((val&LSB_KEEP_MASK)<<NUM_LOOKBACK_BITS)+lookback_partial, RWCHASH_NUM_MASK_BITS + NUM_LOOKBACK_BITS, compressedStreamEncoded)
            pushPayload(((val&LSB_KEEP_MASK)<<NUM_LOOKBACK_BITS)+(((~lookback_partial)+1) & 0x0000007F), RWCHASH_NUM_MASK_BITS + NUM_LOOKBACK_BITS, compressedStreamEncoded)
            hashtable[idx]                 = (i << 32) | val
            hashtable_partial[idx_partial] = (i << 32) | val
            continue


        # no zero, full, or partial match
        # leave the word decompressed
        # add it to the hashtable
        hashtable[idx] = (i << 32) | val
        hashtable_partial[idx_partial] = (i << 32) | val
        #if i < 20:
        #    print "\traw: 0x%x" % val

        # raw, code 111
        pushHeader(7, NUM_CODE_BITS, compressedStreamEncoded)
        pushCompressedBitsRaw(val, compressedStreamRaw)
        numCompressedWordsRaw += 1                                              
        update_code_sequence(7)

    while zero_count > 0:
        # zero match, code 001
        pushHeader(1, NUM_CODE_BITS, compressedStreamEncoded)
        zero_count -= 1
        update_code_sequence(1)
    # push an extra dummy code, otherwise compressed streams ends in DATA:DATA
    # it needs to end CODE:DATA:CODE:DATA:CODE:DATA
    # push dummy zero match, code 001
    pushHeader(1, NUM_CODE_BITS, compressedStreamEncoded)
    finalizeCompressedBits(compressedStreamEncoded)

    # pad compressedStreamRaw to ensure compressedStreamEncoded begins on double-word boundary
    #print "page %d" % c
    #print "code_sequence_i %d" % code_sequence_i
    #print "numCompressedWordsRaw %d" % numCompressedWordsRaw 
    #print "numCompressedWordsEncoded %d" % numCompressedWordsEncoded
    #print "p"
    #print "\tnumCompressedWordsRaw: %d" % numCompressedWordsRaw
    #print "\tnumCompressedWordsEncoded: %d" % numCompressedWordsEncoded
    if (numCompressedWordsRaw % 2) == 0:
        #print "\t\tpadding raw"
        compressedStreamRaw.append(0)
        numCompressedWordsRaw += 1                                              
    # pad compressedStreamEncoded to ensure compressedStreamEncoded /ends/ on double-word boundary
    if (numCompressedWordsEncoded % 2) != 0: # != 0, because we want the encoded stream to end on dword boundary
        #print "\t\tpadding encoded"
        compressedStreamEncoded.append(0)
        # IMPORTANT: do not increase this. we want to extend the block only for padding purposes but have
        # the decompressor unaware of this extra word at the end
        #numCompressedWordsEncoded += 1 
    # append compressed stream to end of raw stream
    for i in xrange(len(compressedStreamEncoded)):
        compressedStreamRaw.append(compressedStreamEncoded[i])

    # store the length of data stream in 0th word
    #print "size of compressedStreamRaw: %d" % len(compressedStreamRaw)
    #print "size of compressedStreamEncoded: %d" % len(compressedStreamEncoded)
    #print "numCompressedWordsEncoded: %d" % numCompressedWordsEncoded
    #print "numCompressedWordsRaw: %d" % numCompressedWordsRaw
    compressedStreamRaw[0] = (numCompressedWordsEncoded << 16) | numCompressedWordsRaw
    #print "compressedStreamRaw[1]: 0x%x" % compressedStreamRaw[1]
    #print "compressedStreamRaw[numCompressedWordsRaw + 1]: 0x%x" % compressedStreamRaw[numCompressedWordsRaw + 1]
    #print "compressedStreamRaw[numCompressedWordsRaw + 2]: 0x%x" % compressedStreamRaw[numCompressedWordsRaw + 2]
    #print "uncompressed[0]: 0x%x" % uncompressed[0]
    #print "uncompressed[1]: 0x%x" % uncompressed[1]

    for code in code_sequence:
        fp_code_sequence.write("%d " % code)
    fp_code_sequence.write("\n")
    code_sequence = [0] * 1024
    code_sequence_i = 0


    # FIXME ? what to return? rw_py_compress doesn't use the result anyway
    return numCompressedWordsRaw + numCompressedWordsEncoded

BLOCK_SIZE = 1024

def rw_py_compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
    global codeHistogram
    global c
    global fp_code_sequence

    instrList=[]
    for word in (input[i:i+4] for i in xrange(0,len(input),4)):
        if len(word) == 4:
            instrList.append( struct.unpack('I',word)[0] )

    n_blocks = len(instrList)/BLOCK_SIZE
    print "n_blocks of RW = %d"%(n_blocks)
    v_addrs = []
    va = 0
    num_blocks_i = int(n_blocks)
    print "Num blocks = " + str(num_blocks_i)
    print "VA_start = " + str(VA_start)
    if(num_blocks_i%2 == 0):
        #we need to ensure we have odd number of blocks 
        #so that va starts at an 8-byte aligned address
        va = int(VA_start + 2 + 2 + 4 * num_blocks_i + 4)  #2 bytes for n_blocks, 2 for 0, 4 per block start addr, 4 padding
    else:
        va = int(VA_start + 2 + 2 + 4 * num_blocks_i)  #2 bytes for n_blocks, 2 for 0, 4 per block start addr
    
    
    
    compressed_text = []
    for block in xrange(n_blocks):
        if(va%8 != 0):
                print "We have a problem. Start addr not DWORD aligned " + str(va)
                sys.exit(0)
        v_addrs.append( struct.pack('I',va) )
        compressed = []
        #print "calling deltaCompress, block = %d"%(block)
        deltaCompress(instrList[block*BLOCK_SIZE:(block+1)*BLOCK_SIZE],compressed)
        c = c + 1
        #print "compressed len = %d"%(len(compressed))
        #print "len(compressed): %d" % len(compressed)
        for word in compressed:
            compressed_text.append(struct.pack('I',word))
        va += 4 * len(compressed)

    print codeHistogram

    print "creating metadata for RW"
    q6zip_rw_alg_version = 0x0001 # 0x<2-byte minor><2-byte major>
    metadata = [struct.pack("H",n_blocks), struct.pack("H",q6zip_rw_alg_version)]
    metadata += v_addrs
    if(num_blocks_i%2 == 0):
        metadata += struct.pack("I",0)
    metadata += compressed_text

    fp_code_sequence.close()

    return ''.join(metadata)  #joins list elements together as string with no spaces

if __name__ == '__main__':
    rw_py_compress()

