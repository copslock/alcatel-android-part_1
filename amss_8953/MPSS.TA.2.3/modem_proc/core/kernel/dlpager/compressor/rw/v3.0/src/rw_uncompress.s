	.file	"rw_uncompress.c"
	.text
.p2align 2
.p2align 4,,15
.globl deltaUncompressTest0
.type	deltaUncompressTest0, @function
deltaUncompressTest0:
	{ loop0(.L21,#1023) } 
	.falign
	.L21:
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest1
.type	deltaUncompressTest1, @function
deltaUncompressTest1:
	{ loop0(.L210,#1023) } 
    { R2=##.L210}
	{ SA0 = R2 }
	.falign
	.L210:
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest2
.type	deltaUncompressTest2, @function
deltaUncompressTest2:
	{ loop0(.L22,#1023) }
    { R2=##.L22}
	.falign
	.L22:
	{ SA0 = R2 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest3
.type	deltaUncompressTest3, @function
deltaUncompressTest3:
	{ loop0(.L23,#1023) }
    { R2=##.L24}
	{ R3=##.L23 }
	.falign
	.L23:
	{ SA0 = R2 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
    { jumpr r31 }
	.falign
	.L24:
	{ SA0 = R3 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }


.globl deltaUncompressTest4
.type	deltaUncompressTest4, @function
deltaUncompressTest4:
	{ r0=#1023; R2=##.L25}
	.falign
	.L25:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r2} 
	{ jumpr r31 }


.globl deltaUncompressTest5
.type	deltaUncompressTest5, @function
deltaUncompressTest5:
	{ r0=#1023; R2=##.L26}
	{ R3=##.L27 }
	.falign
	.L26:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r3} 
	{ jumpr r31 }
	.falign
	.L27:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r2} 
	{ jumpr r31 }






	.p2align 2
	.p2align 4,,15
	.globl deltaUncompress
	.type	deltaUncompress, @function
deltaUncompress:
	// saved LR + FP regs size (bytes) = 0
	// callee saved regs size (bytes) = 48
	// local vars size (bytes) = 32
	// fixed args size (bytes) = 0
	{
		r5 = and(r0,#31)
		memd(r29+#-8) = r17:16
		memd(r29+#-16) = r19:18
		r29 = add(r29,#-80)
	}
	{
		r1 = add(r1,add(r5,#31))
		r9:8 = memd(r6=##C.6.3689)
		r4 = #32
	}
	{
		r1 = lsr(r1,#5)
		/*r3 = add(##-4,asl(r3,#2))*/
		memd(r29+#0) = r9:8
	}
	{
		r4=combine(r4.l,r1.l)
		r9:8 = memd(r6+#8)
		memd(r29+#40) = r25:24
		r5 = sub(r0,r5)
	}
	{
		memd(r29+#56) = r21:20
		memd(r29+#48) = r23:22
		/*r24 = add(r2,r3)*/
		r24=#1024
		r7:6=combine(#32,r4)
	}
	{
		memd(r29+#32) = r27:26
		memd(r29+#8) = r9:8
	}
	{
		//l2fetch(r5,r7:6)
	}
	{
		loop0(.L2,#128)
		r1 = r2
	}
.falign
.L2:
    {
		dczeroa(r1)
		r1 = add(r1,#32)
	}:endloop0 // start=.L2
	{
		r21 = add(r0,#4)
		r19 = #0
		r5 = #0
	}
	{
		memw(r29+#16) = #0
		r1 = #0
		r22 = add(r29,#16)
		r23 = r29
	}
	{
		r18 = memw(r21+#4)
		r4 = memw(r0+#0)
		/*r16 = add(r2,#4)*/
		r16= r2
		r21 = add(r21,#4)
	}
	{
		r0 = memw(r0+#4)
		memw(r22+#12) = #0
		r17 = #60
		r25 = #0
	}
	{
		r5:4 |= asl(r1:0,#32)
		r1:0 = asl(r19:18,#62)
		memw(r22+#8) = #0
		memw(r22+#4) = #0
	}
	{
		r1:0 |= lsr(r5:4,#2)
		r19:18 = asl(r19:18,#60)
		r4 = and(r4,#3)
		r26 = #-1024
	}
	{
		r3 = and(r0,#3)
		r19:18 |= lsr(r1:0,#2)
		r0 = memw(r29+r4<<#2)
		r27 = #1023
	}
	{
		lc0=r24
		r20 = memw(r29+r3<<#2)
	}
	{
		jumpr r0
		r24=##.L4
		r8=add(r16,#4096)
	}

.p2align 5
.L100:
	{ r0 = add(r17,#-32)      ; r3 = #0             ; r16 = add(r16,#64) ;r2 = memw(r21++#4)      }
	{ r5:4 = asl(r3:2,r0)     ; r0=sub(r8,r16)      ; r2=add(r8,#-4)     }
	{ r5:4 |= lsr(r19:18,#32) ; r0=asr(r0,#2)       ; p0=cmp.gt(r16,r2)  ;dcfetch(r21+#128)    }
	{ r19:18 = r5:4           ; LC0=r0 ;            ; if (!p0) jump .L4  }
	{ jump .L7} 

.p2align 5
.L4:
	{ SA0=r20 ; r0 = add(r17,#-2) ;  p0=cmp.eq(R20,r24)          ; p0=cmp.eq(r18,#0) ; if (p0.new) jump:nt .L100   }
	{ p0 = cmp.gtu(r0,#32)        ; r3 = #0                     ; r2 = memw(r21+#0) }
	{ if (p0) r17 = r0            ; r5:4 = asl(r3:2,r0)         ;r1 = and(r18,#3)        }
	{ if (!p0) r21 = add(r21,#4)  ; if (!p0) r17 = add(r17,#30) ; r5:4 |= lsr(r19:18,#2) }
	{ r20 = memw(r23+r1<<#2)      ; r19:18 = r5:4               ; r16 = add(r16,#4)      ; dcfetch(r21+#64)   }:endloop0
	{ jump .L7  }
.p2align 5
.L9:
	{ r3:2 = lsr(r19:18,#2)  ; r0 = add(r17,#-4)     ; r4 = memw(r21+#0)           ; SA0=r20 }
	{ r1 = and(r2,#3)   	 ; r2 = and(r18,#3)      ; p0 = cmp.gtu(r0,#32)        ; r5:4 = combine(#0,r4) }
	{ r2 = memw(r22+r2<<#2)  ; if (p0) r17 = r0      ; if (!p0) r17 = add(r17,#28) ; r5:4 = asl(r5:4,r0 )  }
	{ memw(r16++#4) = r2     ; r20 = memw(r23+r1<<#2) ; if (!p0) r21 = add(r21,#4)  ; r5:4 |= lsr(r19:18,#4) }
	{ r19:18 = r5:4          ; dcfetch(r21+#64)     }:endloop0
	{ jump .L7 }
.p2align 5
.L13:
	{ r1:0 = lsr(r19:18,#12) ; r3 = and(r18,#3)      ; r2 = add(r17,#-14)      ; SA0=r20                 }
	{ r0 = and(r0,#3)        ; r4 = memw(r22+r3<<#2) ; p0 = cmp.gtu(r2,#32)    ; r7:6 = lsr(r19:18,#2) }
	{ r20 = memw(r23+r0<<#2)  ; r0 = and(r6,r27)      ; r5 = memw(r21+#0)          }
	{ r5:4 = combine(#0,r5)  ; r0 |= and(r4,r26)     }
	{ memw(r22+r3<<#2) = r0  ; r3:2 = asl(r5:4,r2)   ; if (p0) r17 = r2           }
	{ r3:2 |= lsr(r19:18,#14); if (!p0) r17 = add(r17,#18)   ; dcfetch(r21+#64)   }
	{ r19:18 = r3:2          ; if (!p0) r21 = add(r21,#4); memw(r16++#4) = r0 }:endloop0
	{ jump .L7 }
.p2align 5
.L16:
	{ SA0=r20                    ; r2 = memw(r21++#4)      ; memw(r16++#4) = r18     ; r1 = add(r17,#-32) }
	{ r0 = add(r17,#-2)          ; memw(r22+r25<<#2) = r18 ; r3:2 = combine(#0,r2)   ; r5 = memw(r21+#0)     }
	{ r3:2 = asl(r3:2,r1)        ; p0 = cmp.gtu(r0,#32)    ; if (!p0.new) r17 = add(r17,#30) }
	{ if (!p0) r21 = add(r21,#4) ; r3:2 |= lsr(r19:18,#32) ; r19:18 = combine(#0,r5) ; if (p0) r17 = r0    }
	{ r19:18 = asl(r19:18,r0)    ; r1 = and(r2,#3)         ; r0 = add(r25,#1)        ;dcfetch(r21+#64)   }
	{ r19:18 |= lsr(r3:2,#2)     ; r20 = memw(r23+r1<<#2)  ; r25 = and(r0,#3)        }:endloop0
.L7:
	{
		r0 = #4096
		r27:26 = memd(r29+#32)
		r25:24 = memd(r29+#40)
	}
	{
		r23:22 = memd(r29+#48)
		r21:20 = memd(r29+#56)
	}
	{
		r19:18 = memd(r29+#64)
		r17:16 = memd(r29+#72)
		r29 = add(r29,#80)
		jumpr r31
	}
	.size	deltaUncompress, .-deltaUncompress
	.section	.rodata
	.p2align 3
	.type	C.6.3689, @object
	.size	C.6.3689, 16
C.6.3689:
	.word	.L4
	.word	.L9
	.word	.L13
	.word	.L16
	.ident	"GCC: (Sourcery QuIC Lite 5.0-413) 4.4.0"
