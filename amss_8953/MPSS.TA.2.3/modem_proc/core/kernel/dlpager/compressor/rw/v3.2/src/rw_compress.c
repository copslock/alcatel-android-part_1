#include "rw_compress.h"
#include <string.h>
#include <stdint.h>
#include <hexagon_protos.h>
#include <math.h>


#define NUM_CODE_BITS 3
#define NUM_ZERO_FLE_CODE_BITS 1
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))

#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif


#define RWCHASH_NUM_HASH_BITS 9 // sed
#define RWCHASH_NUM_MASK_BITS 9 // sed
#define MSB_KEEP_MASK ((~(0)) << RWCHASH_NUM_MASK_BITS)  // bit-wise and with this mask to keep the unmasked upper bits of the input word.
#define LSB_KEEP_MASK ~MSB_KEEP_MASK
#define RWC_HASH_FUNCTION(i)    (((i) * 2654435761U) >> (32 - RWCHASH_NUM_HASH_BITS))
//#define NUM_LOOKBACK_BITS(x) ((unsigned int)ceil(log(x)/log(2))) 
/*unsigned int NUM_LOOKBACK_BITS(x)
  {
  int num_leading_zeros = Q6_R_cl0_R(x);
  int pcount = Q6_R_popcount_P(x);
  int num_lookback_bits = 32 - (num_leading_zeros + 1);
  if(pcount > 1)
  num_lookback_bits++;
  return num_lookback_bits;
  }*/
// TODO fix this to a best length
//#define NUM_LOOKBACK_BITS(x)  (32 - (Q6_R_cl0_R(x) + 1) + (Q6_R_popcount_P(x) > 1 ? 1 : 0))
#define NUM_LOOKBACK_BITS 7 // sed
#define LOOKBACK_MASK ((~(0U)) >> (32 - NUM_LOOKBACK_BITS))
#define MAX_LOOKBACK      (1 << NUM_LOOKBACK_BITS)
#define ZERO_FLE_MAX 4 // sed


#define pushCompressedBits(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
{ \
        unsigned long long int temp = ((unsigned long long int) bits) << compressedPartialBits;                                  \
        compressedPartialBits += numBits;                                                                                        \
        compressedPartialWord = compressedPartialWord|temp;                                                                      \
        *compressedWordStream = compressedPartialWord;      \
        if (compressedPartialBits >= WORD_SIZE_IN_BITS) compressedWordStream++ ; \
        if (compressedPartialBits >= WORD_SIZE_IN_BITS) shift=WORD_SIZE_IN_BITS; \
        compressedPartialWord = compressedPartialWord >> shift;      \
        if (compressedPartialBits >= WORD_SIZE_IN_BITS) compressedPartialBits -= WORD_SIZE_IN_BITS;                              \
        shift=0;\
}


#define pushHeader(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
        pushCompressedBits(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
if (UNLIKELY(payloadHoldBits!=0)) pushCompressedBits(payloadHoldVal,payloadHoldBits,compressedWordStream,compressedPartialBits,compressedPartialWord) 


#define finalizeCompressedBits(compressedWordStream,compressedPartialBits,compressedPartialWord) \
        *compressedWordStream = compressedPartialWord;          \
if (compressedPartialBits>0) compressedWordStream++;   

//unsigned int hashtable[512] __attribute__(( aligned( 32 ) ));
//unsigned int hashtable_partial[512] __attribute__(( aligned( 32 ) ));
//unsigned int hash_lookback[512] __attribute__(( aligned( 32 ) ));
//unsigned int hash_lookback_partial[512] __attribute__(( aligned( 32 ) ));

// Was working before trying the unsigned long long
//unsigned int lookback_table[(1<<RWCHASH_NUM_HASH_BITS)]         __attribute__(( aligned( 32 ) ));
//unsigned int lookback_table_partial[(1<<RWCHASH_NUM_HASH_BITS)] __attribute__(( aligned( 32 ) ));

unsigned long long lookback_table[(1<<RWCHASH_NUM_HASH_BITS)]         __attribute__(( aligned( 32 ) ));
unsigned long long lookback_table_partial[(1<<RWCHASH_NUM_HASH_BITS)] __attribute__(( aligned( 32 ) ));


#define ZERO_FLE_CODE 0 //   0
#define ZERO_CODE     1 // 001
#define FULL_CODE     3 // 011
#define PARTIAL_CODE  5 // 101
#define RAW_CODE      7 // 111




/*********************************************************************************************************/        
//compress a word-aligned buffer uncompressed of length in_len words into a word-aligned buffer compressed
unsigned int deltaCompress(unsigned int *uncompressed,unsigned int *compressed,unsigned int in_len)
{
        unsigned int i, val;
        unsigned int numCompressedPartialBits = 0;
        unsigned long long compressedPartialWord;
        unsigned int payloadHoldBits = 0;
        unsigned int payloadHoldVal;
        unsigned int shift = 0;
        unsigned int *uncompressed_start       = uncompressed;
        unsigned int *compressed_raw           = compressed + 1; // 0th word will hold size of compressed raw
        unsigned int *compressed_raw_start     = compressed_raw;
        unsigned int *compressed_encoded       = compressed_raw + 1025; // in worst case, encoded portion should start at 1024 + 1 (padding) beyond compressed-raw
                                                                        // (or, put another way, 1024 (raw) + 1 (padding) + 1 (num compressed words) = 1026 total beyond start)
        unsigned int *compressed_encoded_start = compressed_encoded;
        unsigned int num_compressed_words_raw;
        unsigned int num_compressed_words_encoded;
        unsigned int idx, uncompr_idx;
        unsigned int idx_partial, uncompr_idx_partial;
        unsigned int hashtable_val;
        unsigned int hashtable_val_partial;
        int lookback, lookback_partial;
        unsigned int zero_count;
        unsigned long long idx_val;
        unsigned long long idx_val_partial;

        // TODO, move this outside of the function... or may need to be smarter about it
        dczero(lookback_table, 4096);
        dczero(lookback_table_partial, 4096);

        compressedPartialWord = 0ULL;
        payloadHoldVal = 0;
        zero_count = 0;

        //printf("num lookback bits: %d\n", NUM_LOOKBACK_BITS);
        //printf("num mask bits: %d\n", RWCHASH_NUM_MASK_BITS);
        //printf("zero fle max: %d\n", ZERO_FLE_MAX);
        //printf("ht bits: %d\n", RWCHASH_NUM_HASH_BITS);

        //loop over input
        for (i = 0; i < 1024; i++)
        {
                val = *uncompressed++;
                //if(i < 20){
                //        printf("i: %d\n", i);
                //}

                // zero match
                if(val == 0){
                        zero_count++;
                        continue;
                }

                while(zero_count >= ZERO_FLE_MAX){
                        //if(i < 20){
                        //        printf("\tzero-fle\n");
                        //}
                        pushHeader(ZERO_FLE_CODE, NUM_ZERO_FLE_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                        payloadHoldVal  = 0;
                        payloadHoldBits = 0;
                        zero_count -= 4;
                }
                while(zero_count > 0){
                        //if(i < 20){
                        //        printf("\tzero\n");
                        //}
                        pushHeader(ZERO_CODE, NUM_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                        payloadHoldVal  = 0;
                        payloadHoldBits = 0;
                        zero_count--;
                }

                // full match
                idx = RWC_HASH_FUNCTION(val);
                idx_val = lookback_table[idx];
                uncompr_idx   = (unsigned int) (idx_val >> 32);
                //hashtable_val = (unsigned int) (idx_val & 0xFFFFFFFF);
                hashtable_val = (unsigned int) (idx_val);
                lookback = i - uncompr_idx;

                idx_partial = RWC_HASH_FUNCTION(val & MSB_KEEP_MASK);
                //if(i < 20){
                //        printf("\tidx: %d\n", idx);
                //        printf("\tidx_partial: %d\n", idx_partial);
                //        printf("\tattempting full with lookback: %d\n", lookback);
                //        printf("\tattempting full with idx_val: 0x%llx\n", idx_val);
                //        printf("\tattempting full with uncompr_idx: %d\n", uncompr_idx);
                //        printf("\tattempting full with hashtable_val: %d\n", hashtable_val);
                //        printf("\tlookback_table[195]: 0x%llx\n", lookback_table[195]);
                //        printf("\tval that would be stored: 0x%llx\n", (((unsigned long long) i) << 32) | ((unsigned long long) val));
                //}

                //if((hashtable_val == val) && lookback && (lookback < MAX_LOOKBACK)){
                //if((hashtable_val == val) && hashtable_val && (lookback < MAX_LOOKBACK)){
                if((hashtable_val == val) && (lookback < MAX_LOOKBACK)){
                //if((hashtable_val == val) && hashtable_val && lookback && (lookback < MAX_LOOKBACK)){
                        //if(i < 20){
                        //        printf("\tfull\n");
                        //}
                        pushHeader(FULL_CODE, NUM_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                        // want to delay-push the lookbcak, which is num-lookback-bits long, into the encoded stream
                        payloadHoldVal  = ((~lookback) + 1) & LOOKBACK_MASK;
                        payloadHoldBits = NUM_LOOKBACK_BITS;
                        lookback_table[idx]                 = (((unsigned long long) i) << 32) | ((unsigned long long) val);
                        lookback_table_partial[idx_partial] = (((unsigned long long) i) << 32) | ((unsigned long long) val);
                        continue;
                }

                // partial match
                idx_val_partial = lookback_table_partial[idx_partial];
                uncompr_idx_partial   = (unsigned int) (idx_val_partial >> 32);
                hashtable_val_partial = (unsigned int) (idx_val_partial & 0xFFFFFFFF);
                lookback_partial = i - uncompr_idx_partial;
                //if(i < 20){
                //        printf("\tattempting partial with lookback_partial: %d\n", lookback_partial);
                //        printf("\tattempting partial with hashtable_val_partial: %d\n", hashtable_val_partial);
                //}
                //if(((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) && lookback_partial && (lookback_partial < MAX_LOOKBACK)){
                if(((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) && hashtable_val_partial && (lookback_partial < MAX_LOOKBACK)){
                //if(((hashtable_val_partial & MSB_KEEP_MASK) == (val & MSB_KEEP_MASK)) && hashtable_val_partial && lookback && (lookback_partial < MAX_LOOKBACK)){
                        //if(i < 20){
                        //        printf("\tpartial\n");
                        //        printf("\thashtable_val_partial: 0x%x\n", hashtable_val_partial);
                        //}
                        pushHeader(PARTIAL_CODE, NUM_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                        payloadHoldVal  = ((val&LSB_KEEP_MASK) << NUM_LOOKBACK_BITS) + (((~lookback_partial) + 1) & LOOKBACK_MASK);
                        payloadHoldBits = RWCHASH_NUM_MASK_BITS + NUM_LOOKBACK_BITS;
                        lookback_table[idx]                 = (((unsigned long long) i) << 32) | ((unsigned long long) val);
                        lookback_table_partial[idx_partial] = (((unsigned long long) i) << 32) | ((unsigned long long) val);
                        continue;
                }
                //if(i < 20){
                //        printf("\traw\n");
                //}


                // raw
                // no full or partial match, code 11
                // leave the word decompressed.
                // add it to the hashtable
                lookback_table[idx]                 = (((unsigned long long) i) << 32) | ((unsigned long long) val);
                lookback_table_partial[idx_partial] = (((unsigned long long) i) << 32) | ((unsigned long long) val);

                pushHeader(RAW_CODE, NUM_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord); 
                *compressed_raw++ = val;
                payloadHoldVal  = 0;
                payloadHoldBits = 0;

        } //for loop

        while(zero_count >= ZERO_FLE_MAX){
                pushHeader(ZERO_FLE_CODE, NUM_ZERO_FLE_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                payloadHoldVal  = 0;
                payloadHoldBits = 0;
                zero_count -= 4;
        }
        while(zero_count > 0){
                pushHeader(ZERO_CODE, NUM_CODE_BITS, compressed_encoded, numCompressedPartialBits, compressedPartialWord);
                payloadHoldVal  = 0;
                payloadHoldBits = 0;
                zero_count--;
        }

        //take care of the remaining bits
        pushHeader(ZERO_CODE,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
        finalizeCompressedBits(compressed_encoded,numCompressedPartialBits,compressedPartialWord);

        num_compressed_words_encoded = compressed_encoded - compressed_encoded_start;
        num_compressed_words_raw     = compressed_raw     - compressed_raw_start;

        compressed[0] = (num_compressed_words_encoded << 16) | num_compressed_words_raw;

        //return 5664; // return size of the compressed buffer in bytes
        return 6560; // return size of the compressed buffer in bytes
                     // worst case: 4096 bytes of raw + 2432 bytes of partial (19 bits per partial * 1024 words)
                     //             + 4 bytes size + 4 bytes padidng for dword alignemtn
                     // rounded up for 32-byte alignment
}

