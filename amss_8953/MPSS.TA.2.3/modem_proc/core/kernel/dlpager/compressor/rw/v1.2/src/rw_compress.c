#include "rw_compress.h"
#include <string.h>
#include <stdint.h>

#define NUM_ANCHORS 4 // must be a power of 2
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_ANCHOR_BITS 2
#define NUM_CODE_BITS 2
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))
 
#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif

#define pushCompressedBits(bits,numBits,compressedWordStream,numCompressedWords,compressedPartialBits,compressedPartialWord) \
  { \
    unsigned long long int temp = ((unsigned long long int) bits) << compressedPartialBits;                                  \
    compressedPartialBits += numBits;                                                                                        \
    compressedPartialWord = compressedPartialWord|temp;                                                                      \
    if (compressedPartialBits >= WORD_SIZE_IN_BITS)                                                                          \
    {                                                                                                                        \
        compressedWordStream[numCompressedWords++] = compressedPartialWord;                                                  \
        compressedPartialWord = compressedPartialWord >> WORD_SIZE_IN_BITS;                                                  \
        compressedPartialBits -= WORD_SIZE_IN_BITS;                                                                          \
    } \
  }

#define finalizeCompressedBits(compressedWordStream,numCompressedWords,compressedPartialBits,compressedPartialWord) \
  if (compressedPartialBits>0)                                                                                      \
  {                                                                                                                 \
    compressedWordStream[numCompressedWords++] = compressedPartialWord;                                             \
  } 

//check for exact or partial match, whichever is first in the anchor list
//code for exact match is 01 and for partial match is 10
#define CHECK_ANCHOR(anchor) \
        anchor_val = anchors[anchor]; \
        if (anchor_val == val) \
        { \
          pushCompressedBits((anchor << NUM_CODE_BITS) + 1,NUM_ANCHOR_BITS + NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord); \
          continue; \
        } \
        if ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)) \
        { \
          pushCompressedBits(((val&0x3FF)<<(NUM_ANCHOR_BITS+NUM_CODE_BITS))+((anchor<<NUM_CODE_BITS)+2),NUM_ANCHOR_BITS+NUM_CODE_BITS+10,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord); \
          anchors[anchor] = val; \
          continue; \
        } \
        anchor = (anchor + (NUM_ANCHORS - 1)) & (NUM_ANCHORS - 1); \

/*********************************************************************************************************/        

//compress a word-aligned buffer uncompressed of length in_len words into a word-aligned buffer compressed
unsigned int deltaCompress(unsigned int *uncompressed,unsigned int *compressed,unsigned int in_len)
{
  unsigned int i,val, anchorIndex = 3;
  unsigned int anchor1 = 0, anchor2 = 0, anchor3 = 0, anchor4 = 0;
  unsigned int numCompressedPartialBits = 0,numCompressedWords = 0;
  unsigned long long int compressedPartialWord = 0;

  
  //write size to output for use by decompressor
  //compressed[numCompressedWords++] = in_len;
  //loop over input
  for (i = 0; i < in_len - 1; i++)
  {
    val = uncompressed[i];
    if (val == 0) 
    {
      //code is 00
      pushCompressedBits(0,NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
      //no need to add as anchor as 0 is always a special case
    }
    else 
    {
      //non-zero value, check for exact or partial match with anchors
      //traverse the anchors such that most recently seen ones come first
      unsigned int val1 = val ^ anchor1;
      unsigned int val2 = val ^ anchor2;
      unsigned int val3 = val ^ anchor3;
      unsigned int val4 = val ^ anchor4;
      unsigned int val5 = val1 & 0xFFFFFC00;
      unsigned int val6 = val2 & 0xFFFFFC00;
      unsigned int val7 = val3 & 0xFFFFFC00;
      unsigned int val8 = val4 & 0xFFFFFC00;


      


      if(!val5)
      { 
       if(!val1)
        {
          pushCompressedBits((0 << NUM_CODE_BITS) + 1,NUM_ANCHOR_BITS + NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
          continue;
        }
        pushCompressedBits(((val&0x3FF)<<(NUM_ANCHOR_BITS+NUM_CODE_BITS))+((0<<NUM_CODE_BITS)+2),NUM_ANCHOR_BITS+NUM_CODE_BITS+10,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
        anchor1 = val;
      }
      else if(!val6)
      {
        if(!val2)
        {
          pushCompressedBits((1 << NUM_CODE_BITS) + 1,NUM_ANCHOR_BITS + NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
          continue;
        }
        pushCompressedBits(((val&0x3FF)<<(NUM_ANCHOR_BITS+NUM_CODE_BITS))+((1<<NUM_CODE_BITS)+2),NUM_ANCHOR_BITS+NUM_CODE_BITS+10,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
        anchor2 = val;
      }
      else if(!val7)
      {
        if(!val3)
        {
          pushCompressedBits((2 << NUM_CODE_BITS) + 1,NUM_ANCHOR_BITS + NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
          continue;
        }
        pushCompressedBits(((val&0x3FF)<<(NUM_ANCHOR_BITS+NUM_CODE_BITS))+((2<<NUM_CODE_BITS)+2),NUM_ANCHOR_BITS+NUM_CODE_BITS+10,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
        anchor3 = val;
      }
      else if(!val8)
      {
        if(!val4)
        {
          pushCompressedBits((3 << NUM_CODE_BITS) + 1,NUM_ANCHOR_BITS + NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
          continue;
        }
        pushCompressedBits(((val&0x3FF)<<(NUM_ANCHOR_BITS+NUM_CODE_BITS))+((3<<NUM_CODE_BITS)+2),NUM_ANCHOR_BITS+NUM_CODE_BITS+10,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
        anchor4 = val;
      }
      else
      {
        //no full or partial match, leave the word uncompressed, add it as an anchor


        anchorIndex = (anchorIndex + 1) & (NUM_ANCHORS - 1);
        switch(anchorIndex)
        {
          case 0: 
            anchor1 = val;
            break;
          case 1: 
            anchor2 = val;
            break;
          case 2: 
            anchor3 = val;
            break;
          case 3: 
            anchor4 = val;
            break;
        }
        //code is 11
        pushCompressedBits(3,NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord); 
        //push the word
        pushCompressedBits(val,WORD_SIZE_IN_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
      }


    } //val != 0
  }  //push last word as a miss on compression for optimized decompress
  val = uncompressed[in_len-1];
    //code is 11 push it
  pushCompressedBits(3,NUM_CODE_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord); 
    //push the word
  pushCompressedBits(val,WORD_SIZE_IN_BITS,compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
   //for loop
  //take care of the remaining bits
  finalizeCompressedBits(compressed,numCompressedWords,numCompressedPartialBits,compressedPartialWord);
  return (numCompressedWords << 2); 
}


