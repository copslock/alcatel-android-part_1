#===============================================================================
#
# GENERAL DESCRIPTION
#     Q6Zip algorithm version selector script. It is invoked by ...
#         * SCons. For paths and determination of files implementing
#           Q6Zip SW algorithms
#         * elfManipulator. For compressing candidate sections using the
#           selected Q6Zip algorithms
#
# INITIALIZATION
#     init *MUST* be called with a list of uses flag string. For example, if
#     invoking through SCons, it is the value of env[ 'USES_FLAGS' ]
#
# OUTPUT
#     Following are outputs from this module (after invocation of init())
#         * RO_VERSION
#         * RW_VERSION
#         * ENABLE_Q6ZIP_IPA
#         * [RO|RW]_BUILDTIME_PATH
#         * [RO|RW]_RUNTIME_FULL_SRCPATH
#         * [RO|RW]_RUNTIME_PARTIAL_SRCPATH
#
# Copyright (c) 2014 by Qualcomm Technologies, Inc. All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#===============================================================================
import glob
import os
import inspect

"""Map to convert USES_Q6ZIP_XXX flag to a RO algorithm version string"""
USES_FLAG_TO_RO_VERSION = {
    'USES_Q6ZIP_RO_V3MD2' : '3-d2',
    'USES_Q6ZIP_RO_V5'    : '5',
    'USES_Q6ZIP_RO_V7'    : '7'
}

"""Map to convert USES_Q6ZIP_XXX flag to a RW algorithm version string"""
USES_FLAG_TO_RW_VERSION = {
    'USES_Q6ZIP_RW_V1P1'  : '1.1'
}

def _sorted_glob (path):
    """Globs the path and returns a *case-insensitive* sorted list
    """
    files = glob.glob( path )
    if len( files ):
        files.sort( cmp=lambda a, b: cmp( a.lower(), b.lower() ) )
    return files

def _init ():
    """Internal function for initialization
    """
    global RO_BUILDTIME_PATH
    global RO_RUNTIME_FULL_SRCPATH, RO_RUNTIME_FULL_INCPATH, RO_RUNTIME_PARTIAL_SRCPATH
    global RW_BUILDTIME_PATH
    global RW_RUNTIME_FULL_SRCPATH, RW_RUNTIME_FULL_INCPATH, RW_RUNTIME_PARTIAL_SRCPATH
    global SRCLIST
    global CCFLAGS

    curr_dir = os.path.split(inspect.getframeinfo(inspect.currentframe()).filename)[0]

    RO_BUILDTIME_PATH          = "%s/ro/%s/bin"  % (curr_dir, RO_VERSION)
    RO_RUNTIME_FULL_SRCPATH    = "%s/ro/%s/src" % (curr_dir, RO_VERSION)
    RO_RUNTIME_FULL_INCPATH    = "%s/ro/%s/inc" % (curr_dir, RO_VERSION)
    RO_RUNTIME_PARTIAL_SRCPATH = "ro/%s"        % (RO_VERSION)

    RW_BUILDTIME_PATH          = "%s/rw/%s/bin"  % (curr_dir, RW_VERSION)
    RW_RUNTIME_FULL_SRCPATH    = "%s/rw/%s/src" % (curr_dir, RW_VERSION) 
    RW_RUNTIME_FULL_INCPATH    = "%s/rw/%s/inc" % (curr_dir, RW_VERSION)
    RW_RUNTIME_PARTIAL_SRCPATH = "rw/%s"        % (RW_VERSION)

    sources = _sorted_glob( os.path.join( RO_RUNTIME_FULL_SRCPATH, 'q6zip_uncompress.?' ) )
    if len( sources ):
        RO_DECOMPRESSOR_SRC = os.path.basename( sources[ -1 ] )

    sources = _sorted_glob( os.path.join( RW_RUNTIME_FULL_SRCPATH, 'rw_compress.?' ) )
    if len( sources ):
        RW_COMPRESSOR_SRC = os.path.basename( sources[ -1 ] )

    sources = _sorted_glob( os.path.join( RW_RUNTIME_FULL_SRCPATH, 'rw_uncompress.?' ) )
    if len( sources ):
        RW_DECOMPRESSOR_SRC = os.path.basename( sources[ -1 ] )
    else:
        RW_DECOMPRESSOR_SRC = RW_COMPRESSOR_SRC

    CCFLAGS  = '-DQ6ZIP_RO_ALG_VERS=\\\"'+RO_VERSION+'\\\"'
    CCFLAGS += ' -DQ6ZIP_RW_ALG_VERS=\\\"'+RW_VERSION+'\\\"'
    if ENABLE_Q6ZIP_IPA:
        CCFLAGS = CCFLAGS + ' -DENABLE_Q6ZIP_IPA'

    SRCLIST = []
    SRCLIST.append('ro/%s/src/%s' % (RO_VERSION, RO_DECOMPRESSOR_SRC))
    SRCLIST.append('rw/%s/src/%s' % (RW_VERSION, RW_COMPRESSOR_SRC))
    if RW_DECOMPRESSOR_SRC != RW_COMPRESSOR_SRC:
            SRCLIST.append('rw/%s/src/%s' % (RW_VERSION, RW_DECOMPRESSOR_SRC))

def init (uses_flags):
    """Initialize Q6Zip versions module using a list of USES_Q6ZIP_XXX
       flags
    """
    global RO_VERSION, RW_VERSION, ENABLE_Q6ZIP_IPA

    if 'USES_COREIMG' in uses_flags:

       # For core-only builds (in TA flavors) ...
       #     a. Dont use Q6Zip IPA. Prevents external dependencies (out of core)
       #     b. Hard-code RO and RW versions
       ENABLE_Q6ZIP_IPA = False
       RO_VERSION = 'v3-d2'
       RW_VERSION = 'v1.1'

    elif 'USES_Q6ZIP_SW_ONLY' in uses_flags:

       # Q6Zip SW. Look at the uses flag to see what Q6Zip RO and RW algorithm
       # versions are selected

       ENABLE_Q6ZIP_IPA = False
       uses_flags_set = set( uses_flags )
       ro_version_flags = set( USES_FLAG_TO_RO_VERSION.keys() ).intersection( uses_flags_set )
       if len( ro_version_flags ) == 0:
           raise ValueError, 'no Q6Zip RO version specified'
       if len( ro_version_flags ) > 1:
           raise ValueError, 'more than one Q6Zip RO version specified. %s' % ro_version_flags

       # We have exactly one Q6Zip RO version
       RO_VERSION = 'v%s' % USES_FLAG_TO_RO_VERSION[ ro_version_flags.pop() ]

       rw_version_flags = set( USES_FLAG_TO_RW_VERSION.keys() ).intersection( uses_flags_set )
       if len( rw_version_flags ) == 0:
           raise ValueError, 'no Q6Zip RW version specified'
       if len( rw_version_flags ) > 1:
           raise ValueError, 'more than one Q6Zip RW version specified. %s' % rw_version_flags

       # We have exactly one Q6Zip RW version
       RW_VERSION = 'v%s' % USES_FLAG_TO_RW_VERSION[ rw_version_flags.pop() ]

    else:

       # Q6Zip IPA. Q6Zip RO and RW versions *MUST* be HW-matched

       ENABLE_Q6ZIP_IPA = True
       RO_VERSION = 'v3-d2'
       RW_VERSION = 'v1.1'

    # Now that versions and Q6Zip loader selection is set, invoke the internal
    # function to complete initialization
    _init()
