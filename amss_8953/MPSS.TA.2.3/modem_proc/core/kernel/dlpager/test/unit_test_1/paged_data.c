#include <stdio.h>
#include <dlpager_test.h>


const unsigned int swapped_data[TEST_RANGE] = { [ 0 ... TEST_RANGE-1 ] = TEST_PATTERN}; //256KB

/* Simulated data structure which is one-to-one replica of swapped_data.
 * When we have to load from swapped_data, we simply copy it over. More
 * elements in this array to cover other read only data */
unsigned int sim_swapped_data[TEST_RANGE+1000];

int test_func(void)
{
    printf ("enter test_func\n");
    return 0;

}
