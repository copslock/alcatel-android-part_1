#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <qurt.h>

#include <dlpager_main.h>
#include <memload_handler.h>

#define PAGE_SIZE (4 * 1024) // 4 KB
#define SWAP_POOL_SIZE (50 * 1024) // 100 KB

#define MAX_FAULTING_THREADS 8
#define TEST_THREAD_STACK_SIZE (4 * 1024)
#define TEST_RANGE  64*1024

extern unsigned int swapped_data [];

extern unsigned int __swapped_segments_start__;
extern unsigned int __swapped_segments_end__;

extern void dump_global_table (void);
extern void dump_swap_table (void);
extern void dump_log_table (void);
extern void sim_load_setup (void);

qurt_thread_t tid[MAX_FAULTING_THREADS];

void unit_test_page_miss (void)
{
    int i, size;
    qurt_thread_t thread_id;
    unsigned int addr, fault_addr;

    addr = (unsigned int)&__swapped_segments_start__;
    size = (unsigned int)&__swapped_segments_end__ - (unsigned int)&__swapped_segments_start__;

    thread_id = qurt_thread_get_id ();
    for (i = 0; i < size/PAGE_SIZE; i++) {
        fault_addr = addr + (i * PAGE_SIZE);
        memload_fault_handler (fault_addr, thread_id);
    }
}

int dlpager_get_attr (dlpager_attr_t *pAttr)
{

    pAttr->swap_pool[0].size = SWAP_POOL_SIZE;
    pAttr->swap_pool[0].page_size = PAGE_SIZE;
    pAttr->num_swap_pools = 1;

    return 0;
}

int main (int arc, char **argv)
{
    int i, r, r1;
    volatile int *ptr=&r;

    r = dlpager_init ();
    assert (r == 0);

    printf ("dl_pager is started\n");
    printf (".....\n Unit Testing \n....\n");


    /* Wait till pager is started. Delay without using timer */
    for (i = 0; i < 0xffff; i++) {
        r1 = *ptr;
    }

    dump_global_table ();
    dump_swap_table ();

    unit_test_page_miss ();

    dump_global_table ();
    dump_swap_table ();

    dump_log_table ();

    printf ("Done Unit testing pager\n");
    exit (0);
}
