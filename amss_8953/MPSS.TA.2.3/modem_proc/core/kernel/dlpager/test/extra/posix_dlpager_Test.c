#include <stdio.h>
#include <stdlib.h>
#include "dlpager_elf_loader.h"
#ifdef BLAST_DLPAGER_DEBUG
#include <pthread.h>
#include "dlpager_flash_dal.h"
#include "memload_handler.h"

unsigned int randomNoBetween(unsigned int x,unsigned int y);

/* global variables */
int pageIndex[MAX_TEST_PAGES]={0};
boolean testPagerThreadAll;

unsigned int pagerreadbuf1[1024];

unsigned int pagerreadbuf2[1024];

unsigned int pagerreadbuf3[1024];

unsigned int pagerreadbuf4[1024];

unsigned int pagerreadbuf5[1024];

unsigned int pagerreadbuf6[1024];

unsigned int pagerreadbuf7[1024];

unsigned int pagerreadbuf8[1024];

unsigned int pagerreadbuf9[1024];

unsigned int pagerreadbuf10[1024];



extern void dlpager_test_text_type(void);

void dlpager_test_pthread1(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

  unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

  id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 2, id=%d \n", id);

  readbuf = (unsigned int*) pagerreadbuf1;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
      virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);

	  flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
			      &hash_segment_offset);
	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  201;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread2(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;
	
#ifdef PERFORMANCE_TEST1
	
		unsigned long long int startpcycles , endpcycles, nopcycles;
	
#endif
	
		id = pthread_self();
		DL_DEBUG(" Starting DL test Thread 1, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf2;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
      virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);

	  flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
			      &hash_segment_offset);
	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  202;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}


void dlpager_test_pthread3(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;
	
#ifdef PERFORMANCE_TEST1
	
		unsigned long long int startpcycles , endpcycles, nopcycles;
	
#endif
	
	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 3, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf3;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

		if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  203;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread4(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 4, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf4;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }
  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  204;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread5(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 5, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf5;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  205;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread6(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 6, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf6;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  206;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread7(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 7, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf7;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	    dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  207;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread8(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 8, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf8;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  208;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread9(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 9, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf9;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }
  
    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

	  if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  =  209;//randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];
	  
	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


      // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

void dlpager_test_pthread10(void)
{
  unsigned int i;
  int exit_status = 100;
  unsigned int  page=0;
  boolean readAll = TRUE;
  unsigned int virt_addr, vaddr;
  boolean status;
  unsigned int* readbuf, *pv;
  unsigned int poolid, data;
  boolean sequenceTest = TRUE;
  unsigned int flags;
  volatile unsigned int  val;
	pthread_t id;
	unsigned int hash_segment_offset;

#ifdef PERFORMANCE_TEST1

	unsigned long long int startpcycles , endpcycles, nopcycles;

#endif

	id = pthread_self();
	DL_DEBUG(" Starting DL test Thread 10, id=%d \n", id);
  readbuf = (unsigned int*) pagerreadbuf10;
  
  while (1)
  {
    do
    {
      if (!sequenceTest)
        page = randomNoBetween(0, MAX_TEST_PAGES-1);

	  dlpager_test_text_type();
     
      pageIndex[page] = 1; // Page read 
      
      for(i=0; i<MAX_TEST_PAGES; i++)
      {
        if (pageIndex[i]== 0)
        {
          readAll = FALSE;
		  break;
        }
      }

	  if (readAll)
	  {
        DL_DEBUG(" >>> All page is read PASS \n", page);   
        memset (&pageIndex[0], 0,  sizeof(pageIndex));
	  }

    virt_addr = (unsigned int)&__swapped_segments_start__ + (page * SIZE_4KB);
		flags = elf_get_swapped_segment_info(NULL, &flags, NULL, NULL, NULL, NULL, NULL, virt_addr,
						&hash_segment_offset);

		if ( MI_PBT_POOL_INDEX_VALUE(flags) == 0 )
	  {
		poolid = 0;
		data = 0x100 | (((page/10)<<4) + ((page%10)+1));
	  }
	  else
	  {
		poolid = 1;
		data = 0x200 | (((page/10)<<4) | ((page%10)+1));
	  }

  
      pv = (unsigned int *)virt_addr;
      status = TRUE;

	  i  = 210;  //randomNoBetween(480, 1023);
	  
	  // reuse flag for virtual address
	  vaddr = virt_addr + (i*4);
	  
	  DL_DEBUG("\n>> trigger page fault at Vaddr=0x%lx, i=0x%lx vaddr4KB=0x%lx, page= %d,  data=0x%x\n",
	  vaddr,i, virt_addr, page,data);	

	  /* performance */
#ifdef PERFORMANCE_TEST1
	  startpcycles = qurt_get_core_pcycles(); 
#endif

	  val = pv[i];

	  
#ifdef PERFORMANCE_TEST1 
	  endpcycles = qurt_get_core_pcycles(); 
	  nopcycles = endpcycles - startpcycles;
	  DL_DEBUG3("\n >> Performance 1 pagefault %ld pcycles  \n", nopcycles);	
#endif


	  // compare page data
      for(i=0; i < (SIZE_4KB/4); i++)
      {
      
        if (pv[i] != data) //readbuf[i])
        {
      	  DL_DEBUG(" \n ***FAIL - Compare Data fail, page %d, pv[%d]=0x%x,  val =0x%x, data =0x%x \n", page,i, pv[i],val, data);
          status =FALSE;
          assert(!"dlpager: Fail to virtual_page\n");
  	      break;
        }
      }
      
      if (status)
      {
        DL_DEBUG(" Read page  id %d  PASS \n", page);	 
      }
	  if (sequenceTest)
	  	 page = (page+1)%100;
      
    }while (!readAll);
  }
  
   pthread_exit((void*) exit_status);
   (void*) exit_status;
}

#define TF_SLEEP(x)     qtimer_sleep(1000 * (x))

void dlpager_test_setup(void)
{
	int 					 tmp					 = 0;
	int 					 * exit_status = &tmp;


  testPagerThreadAll = TRUE;
  pthread_attr_t  thread_attr;
  int status;
  pthread_t pthrd1, pthrd2, pthrd3, pthrd4, pthrd5;
  pthread_t pthrd6, pthrd7, pthrd8, pthrd9, pthrd10;

  /* testing Thread 1 */
  status = pthread_attr_init(&thread_attr);
  assert(status == 0);
  pthread_attr_setthreadname(&thread_attr, "thrd_DL_test1");
  status = pthread_create(&pthrd1, &thread_attr, dlpager_test_pthread1, 0);
  assert(status == 0);

  if (testPagerThreadAll)
  {

    /* testing Thread 2 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test2");
    status = pthread_create(&pthrd2, &thread_attr, dlpager_test_pthread2, 0);
    assert(status == 0);


    /* testing Thread 3 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test3");
    status = pthread_create(&pthrd3, &thread_attr, dlpager_test_pthread3, 0);
    assert(status == 0);
    
    /* testing Thread 4 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test4");
    status = pthread_create(&pthrd4, &thread_attr, dlpager_test_pthread4, 0);
    assert(status == 0);
    
    /* testing Thread 5 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test5");
    status = pthread_create(&pthrd5, &thread_attr, dlpager_test_pthread5, 0);
     assert(status == 0);
    
    /* testing Thread 6 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test6");
    status = pthread_create(&pthrd6, &thread_attr, dlpager_test_pthread6, 0);
    assert(status == 0);
    
    /* testing Thread 7 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test7");
    status = pthread_create(&pthrd7, &thread_attr, dlpager_test_pthread7, 0);
    assert(status == 0);
    
    /* testing Thread 8 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test8");
    status = pthread_create(&pthrd8, &thread_attr, dlpager_test_pthread8, 0);
    assert(status == 0);
    
    /* testing Thread 9 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test9");
    status = pthread_create(&pthrd9, &thread_attr, dlpager_test_pthread9, 0);
    assert(status == 0);
    
    /* testing Thread 10 */
    pthread_attr_setthreadname(&thread_attr, "thrd_DL_test10");
    status = pthread_create(&pthrd10, &thread_attr, dlpager_test_pthread10, 0);
    assert(status == 0);
  }

}

unsigned int random() {
return (unsigned int)((rand()<<15) | rand());
}

/* Return random number from x to y inclusive */
unsigned int randomNoBetween(unsigned int x, unsigned int y) 
{
  return (random() %(y-x+1))+x;
}
#else

void dlpager_test_setup(void)
{
  return;
}
#endif
