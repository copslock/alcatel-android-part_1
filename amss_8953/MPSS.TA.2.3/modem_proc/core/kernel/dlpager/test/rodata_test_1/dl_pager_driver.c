#include <qurt.h>
#include <dlpager_main.h>
#include <stdlib.h>

#include <dlpager_test.h>
#include <stringl.h>

extern unsigned int sim_swapped_data[TEST_RANGE+1000];
extern unsigned int __swapped_segments_start__;

#if 0
int dlpager_ext_init (void)
{
   return 1;
}
#endif /* 0 */

void sim_load_setup (void)
{
    int i;

    for (i = 0; i < TEST_RANGE + 1000; i++) {
        sim_swapped_data[i] = TEST_PATTERN;
    }
}

void dlpager_load_virtual_page (unsigned int src_addr,
		               unsigned int dest_addr, unsigned int page_size)
{
   unsigned int sim_load_offset;

   sim_load_offset = src_addr - (unsigned int)&__swapped_segments_start__;

   memscpy ((void *)dest_addr, page_size, (void *)sim_swapped_data + sim_load_offset, page_size);

}
