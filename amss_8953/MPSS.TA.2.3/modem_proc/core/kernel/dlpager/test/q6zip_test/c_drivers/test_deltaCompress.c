#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "rw_compress.h"

#if __hexagon__
#include "hexagon_sim_timer.h"
#endif

int data_size;
int out_size;
FILE* fp_data;
FILE* fp_out;

char data_buf [0x8000] __attribute__ ((aligned (32)));
char out_buf  [0x8000] __attribute__ ((aligned (32)));

void dczero(uint32_t addr, uint32_t size);

unsigned long long int test_deltaCompress(char *data_file, char *output_file)
{
        fp_data = fopen(data_file, "rb");
        assert(fp_data); 

        fp_out = fopen(output_file, "wb");
        assert(fp_out);

        fseek(fp_data, 0L, SEEK_END);
        data_size = ftell(fp_data);
        fseek(fp_data, 0L, SEEK_SET);
        //printf("data size %d\n", data_size);

        //char* data_buf = (char*)malloc (data_size);
        //assert (data_buf);

#ifndef SWAP_ENDIAN
        int data_buf_size = fread(data_buf, 1, data_size , fp_data);
#else
        int data_buf_size = 0;
        int i, j;
        for (i = 0; i < data_size; i += 4)
        {
                data_buf_size += fread(data_buf + i + 3, 1, 1 , fp_data);
                data_buf_size += fread(data_buf + i + 2, 1, 1 , fp_data);
                data_buf_size += fread(data_buf + i + 1, 1, 1 , fp_data);
                data_buf_size += fread(data_buf + i + 0, 1, 1 , fp_data);
                //    printf("%02X %02X %02X %02X ", *(data_buf + i + 0), *(data_buf + i + 1), *(data_buf + i + 2), *(data_buf + i + 3));
        }
#endif


        int total_out = 0;
        int total_count = 0;

        out_size = 1024;
        dczero((uint32_t)out_buf, out_size*4);

#if __hexagon__
        unsigned long long int start_time,end_time,delta;
        start_time=hexagon_sim_read_pcycles();
#endif
        out_size=deltaCompress((unsigned int*)data_buf, (unsigned int *)out_buf, out_size);

#if __hexagon__
        end_time=hexagon_sim_read_pcycles();
        delta=(end_time-start_time);
#endif

        //printf("pCycles=%d\n",(int)(end_time-start_time));
        //printf("data_size=%d out_size=%d\n",data_size,out_size);
        fwrite(out_buf, out_size,1,fp_out);
        fclose(fp_out);
        fclose(fp_data);
#if __hexagon__
        return delta;
#endif
        return 0;
}
