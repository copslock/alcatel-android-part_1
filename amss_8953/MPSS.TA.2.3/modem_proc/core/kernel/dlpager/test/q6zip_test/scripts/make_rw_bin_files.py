import struct
import os

# Input : path\\get_rw_data.out (the output of get_rw_data.cmm)
#         (optional) number of blocks/files to produce
# Output: set of files, compressed[0-x].bin 
#         returns x+1 (number of compressed bin files)

def make_rw_bin_files(get_rw_data_out, num_blocks = None):

        i = 0
        with open(get_rw_data_out, "r") as fp_in:
                
                fp_out = open("compressed"+str(i)+".bin", "wb")        
                for line in fp_in:
                        line = line.strip()
                        if line:
                                fp_out.write(struct.pack('c', chr(int(line, 16))))
                        else:
                                fp_out.close()
                                i = i + 1
                                fp_out = open("compressed"+str(i)+".bin", "wb")
                                if num_blocks and i >= num_blocks:
                                        break
                fp_out.close()
                
                # last file was opened and not needed. delete it.
                os.remove("compressed"+str(i)+".bin")
        return i
        
        
if __name__ == "__main__":
        make_rw_bin_files("get_rw_data.out")