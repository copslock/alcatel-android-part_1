/*==========================================================================
 * FILE:         dlpager.c
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  This file provides the implementation of pager service
 *               initilaiztion and creation of pager thread. It servers as an
 *               interface to Kernel.
 *
 * Copyright (c) 2010-2013 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rr      Re-write of dlpager_main.c for async dlpager
===========================================================================*/
#include <qurt.h>
#include <stdlib.h>
#include <stringl.h>
#include <assert.h>
#include <dlpager_params.h>
#include <dlpager_main.h>
#include <dlpager_meta.h>
#include <dlpager_pagesm.h>
#include <dlpager_q6zip_iface.h>
#include <dlpager_swappool.h>
#include <dlpager_types.h>
#include <dlpager_waitlist.h>
#include <dlpager.h>
#include <dlpager_rwbuffer.h>
#include <dlpager_stats.h>

/* DL PAGER Versioning */
static volatile char dlpager_version[] = DLPAGER_VERSION;

dlpager_event_t to_dlpager_event(unsigned int ssr_cause, unsigned int fault_addr)
{
  switch(ssr_cause)
  {
    
    case 0x60:
    case 0x61:
         return TLB_MISS_X;

    case 0x70:
         return TLB_MISS_R;    
                 
    case 0x71:
         return TLB_MISS_W;
         break;

    case 0x23:
    default:
         return INVALID_EVENT;         
  }  
}

dlpager_event_t disable_write_detect(dlpager_event_t event, unsigned int fault_addr)
{

  if(event == TLB_MISS_R
	 && dlpager_meta_is_addr_in_rwdata_range(fault_addr))
         return TLB_MISS_W;

  return event;
}

static void dlpager_main(void * arg0)
{
  qurt_sysevent_pagefault_t pf_data;
  dlpager_event_t event;
  unsigned int rtn_val;

  dlpager_init();
  while( 1 )
  {
    rtn_val = qurt_exception_wait_pagefault( &pf_data );
    ASSERT( QURT_EOK == rtn_val );

    dlpager_stats_faultresumetimer_start( pf_data.thread_id );
    dlpager_stats_fault_to_ipa_start( pf_data.thread_id );
    
    event = to_dlpager_event( pf_data.ssr_cause, pf_data.fault_addr );
    ASSERT( event != INVALID_EVENT );

    event = disable_write_detect(event, pf_data.fault_addr);
    //for async need to move this into decompress task
    //for now permanently mapped at init
    //dlpager_swappool_rw_mapping_add();

    dlpager_pagesm_handle_event( event, pf_data.fault_addr, pf_data.thread_id );

    //for async need to move this into decompress task
    //dlpager_swappool_rw_mapping_remove();
  } 
}

extern unsigned int __attribute__ ((weak)) __swapped_segments_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_end__;
                                        
int dlpager_init(void)
{
  qurt_thread_attr_t thread_attr;
  qurt_thread_t dlpager_tid;
  int status, i;
  void *pStack;
  
  static int flag = 0;
  if(!flag){
        flag = 1;
  }else{
        return 0;
  }

   if(&__swapped_segments_start__ == &__swapped_segments_end__)
     return 0;

  /* Dummy memcpy to ensure that symbol dlpager_version doesnt get excluded
     due to optimizations */
  i = strlen( DLPAGER_VERSION );
  memscpy( (void*)&dlpager_version, i, DLPAGER_VERSION, i );

  /* meta init must happen before swappool init
     if we want to reclaim rw compressed section 
     for swappool */
  (void)dlpager_meta_init();
  (void)dlpager_pagesm_init();  
  (void)dlpager_q6zip_init();
  (void)dlpager_swappool_init(DLPAGER_SWAPPOOL_SIZE);
  (void)dlpager_waitlist_init();
  (void)dlpager_rwbuffer_init();

  // Remove this when it is done in q6zip
  dlpager_swappool_rw_mapping_add();

  /* Demand paging Thread */
  qurt_thread_attr_init (&thread_attr);
  pStack = malloc (DLPAGER_MAIN_STACK_SIZE);
  ASSERT(pStack != NULL);
  qurt_thread_attr_set_name (&thread_attr, "DLPager_main");
  qurt_thread_attr_set_stack_addr (&thread_attr, pStack);
  qurt_thread_attr_set_stack_size (&thread_attr, DLPAGER_MAIN_STACK_SIZE);
  qurt_thread_attr_set_priority (&thread_attr, DLPAGER_MAIN_PRIO);
  status = qurt_thread_create (&dlpager_tid, &thread_attr, dlpager_main, (void *)0);
  ASSERT(status == 0);
  
  return 0;
}



