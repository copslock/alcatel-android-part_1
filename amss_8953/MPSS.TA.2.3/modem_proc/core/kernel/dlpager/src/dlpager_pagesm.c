/*==========================================================================
 * FILE:         dlpager_pagesm.c
 *
 * SERVICES:     DL PAGER STATE MACHINE
 *
 * DESCRIPTION:  This file implements page state lookup/transitions for the async dlpager
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_pagesm.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rr      First version
===========================================================================*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <qurt.h>
#include <dlpager.h>
#include <dlpager_types.h>
#include <dlpager_params.h>
#include <dlpager_pagesm.h>
#include <dlpager_handlers.h>
#include <dlpager_log.h>
#include <dlpager_stats.h>
#include <dlpager_debug.h>

/* Actual page state machine */
struct {
  dlpager_page_state_t *table;
  unsigned int count; 
  qurt_mutex_t mutex; 
} dlpager_pagesm; 

/* A mutex for a page in transit*/
typedef struct
{
  qurt_mutex_t mutex;
  unsigned int page_va;
  unsigned short ref_count;  
  unsigned short valid;
} page_mutex_t;
struct
{
  unsigned int num_allocd;
  unsigned int max_allocd;
  page_mutex_t table[MAX_PENDING_PAGES];
}page_mutexes;

typedef int pagesm_pagemutex_t;

static pagesm_pagemutex_t pagesm_pagemutex_get(unsigned int page_va)
{
  
  int iter, free_entry = -1, matched_entry = -1;
  int retval = -1;

  qurt_pimutex_lock(&dlpager_pagesm.mutex);

  for(iter =0; iter < MAX_PENDING_PAGES; iter++)
  {
    if((page_mutexes.table[iter].valid == 1)
       && (page_mutexes.table[iter].page_va == page_va))
    {       
       matched_entry = iter;                    
       break;
    }

    if((page_mutexes.table[iter].valid == 0)
       && (free_entry == -1))
    {
         free_entry = iter;
    }    
  }

  if(matched_entry != -1)
  {
    page_mutexes.table[matched_entry].ref_count ++;
    retval = matched_entry;
  }
  else if(free_entry != -1)
  {
    page_mutexes.table[free_entry].valid = 1;
    page_mutexes.table[free_entry].page_va = page_va;
    page_mutexes.table[free_entry].ref_count = 1;
    page_mutexes.num_allocd ++;
    if(page_mutexes.max_allocd < page_mutexes.num_allocd)
      page_mutexes.max_allocd = page_mutexes.num_allocd;
    retval = free_entry;
  }         
  else
  {
    ASSERT(0);    
  }
  qurt_pimutex_unlock(&dlpager_pagesm.mutex);
  return retval;
}

static inline void pagesm_pagemutex_lock(pagesm_pagemutex_t iter)
{
  qurt_pimutex_lock(&page_mutexes.table[iter].mutex);
}

static inline void pagesm_pagemutex_unlock(pagesm_pagemutex_t iter)
{
  qurt_pimutex_unlock(&page_mutexes.table[iter].mutex);
}

static inline void pagesm_pagemutex_free(pagesm_pagemutex_t matched_entry)
{
  qurt_pimutex_lock(&dlpager_pagesm.mutex);
  /* ensure matched entry is valid*/
  ASSERT(page_mutexes.table[matched_entry].valid == 1);
  
  page_mutexes.table[matched_entry].ref_count--;

  if(page_mutexes.table[matched_entry].ref_count == 0)
  {
    page_mutexes.table[matched_entry].valid = 0;
    page_mutexes.table[matched_entry].page_va = 0; 
    page_mutexes.num_allocd --;   
  }  
  qurt_pimutex_unlock(&dlpager_pagesm.mutex);
}

static void pagesm_pagemutex_init()
{
   int i;

   page_mutexes.num_allocd = 0;
   page_mutexes.max_allocd = 0;

   for(i=0; i < MAX_PENDING_PAGES; i++)
   {
     page_mutexes.table[i].page_va = 0;
     page_mutexes.table[i].ref_count = 0;
     qurt_pimutex_init(& page_mutexes.table[i].mutex);
     page_mutexes.table[i].valid = 0;
   }
}

extern unsigned int __attribute__((weak)) __swapped_segments_start__;
extern unsigned int __attribute__((weak)) __swapped_segments_bss_start__;
extern unsigned int __attribute__((weak)) __swapped_segments_end__;


static dlpager_page_state_t next_state [MAX_STATES][MAX_EVENTS] =
{
                                             /*TLB_MISS_X,                   TLB_MISS_R,                   TLB_MISS_W,                          EVICT_PAGE,       DECOMPRESSION_COMPLETE, COMPRESSION_COMPLETE,       SOFT_CLEAN_PAGE,                      HARD_CLEAN_PAGE,                  SOFT_CLEAN_FAILED */
  /* UNMAPPED_CLEAN */                       { UNMAPPED_CLEAN_DECOMPRESSING, UNMAPPED_CLEAN_DECOMPRESSING, UNMAPPED_DIRTY_DECOMPRESSING,        INVALID_STATE,    INVALID_STATE,          INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },             
  /* UNMAPPED_BSS */                         { INVALID_STATE,                INVALID_STATE,                MAPPED_DIRTY,                        INVALID_STATE,    INVALID_STATE,          INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },  
  /* UNMAPPED_CLEAN_DECOMPRESSING */         { UNMAPPED_CLEAN_DECOMPRESSING, UNMAPPED_CLEAN_DECOMPRESSING, INVALID_STATE,                       INVALID_STATE,    MAPPED_CLEAN,           INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },
  /* UNMAPPED_DIRTY_DECOMPRESSING */         { INVALID_STATE,                INVALID_STATE,                UNMAPPED_DIRTY_DECOMPRESSING,        INVALID_STATE,    MAPPED_DIRTY,           INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },  
  /* UNMAPPED_DIRTY_COMPRESSING_ALLOCATED */ { INVALID_STATE,                INVALID_STATE,                MAPPED_DIRTY_COMPRESSING,            INVALID_STATE,    INVALID_STATE,          UNMAPPED_CLEAN_ALLOCATED,   INVALID_STATE,                        INVALID_STATE,                    MAPPED_DIRTY     },
  /* UNMAPPED_CLEAN_ALLOCATED */             { INVALID_STATE,                INVALID_STATE,                MAPPED_DIRTY,                        UNMAPPED_CLEAN,   INVALID_STATE,          INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },
  /* UNMAPPED_HARD_CLEAN_COMPRESSING */      { INVALID_STATE,                INVALID_STATE,                UNMAPPED_HARD_CLEAN_COMPRESSING,     INVALID_STATE,    INVALID_STATE,          UNMAPPED_CLEAN,             INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },
  /* MAPPED_CLEAN */                         { MAPPED_CLEAN,                 MAPPED_CLEAN,                 INVALID_STATE,                       UNMAPPED_CLEAN,   INVALID_STATE,          INVALID_STATE,              INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },
  /* MAPPED_DIRTY */                         { INVALID_STATE,                INVALID_STATE,                MAPPED_DIRTY,                        INVALID_STATE,    INVALID_STATE,          INVALID_STATE,              UNMAPPED_DIRTY_COMPRESSING_ALLOCATED, UNMAPPED_HARD_CLEAN_COMPRESSING,  INVALID_STATE    },
  /* MAPPED_DIRTY_COMPRESSING */             { INVALID_STATE,                INVALID_STATE,                MAPPED_DIRTY_COMPRESSING,            INVALID_STATE,    INVALID_STATE,          MAPPED_DIRTY,               INVALID_STATE,                        INVALID_STATE,                    INVALID_STATE    },
};

static dlpager_handler_fptr_t dlpager_handlers[MAX_STATES][MAX_EVENTS] = {{NULL}};

unsigned int dlpager_pagesm_is_page_evictable(unsigned int page_va)
{
   unsigned page_idx;
   unsigned int evictable;
   
   if(page_va == 0)
   {
      /* this page is unmapped, hence evictable */
      return 1;
   }
     
   ASSERT(((page_va >= (unsigned int)&__swapped_segments_start__)
       && (page_va <= (unsigned int)&__swapped_segments_end__)));
  
   page_idx = (page_va - (unsigned int)&__swapped_segments_start__) >> PAGE_SHIFT;   
   evictable = ((dlpager_pagesm.table[page_idx] == MAPPED_CLEAN) 
                 || (dlpager_pagesm.table[page_idx] == UNMAPPED_CLEAN_ALLOCATED));

   return evictable;
}

unsigned int dlpager_pagesm_is_page_cleanable(unsigned int page_va)
{
   unsigned page_idx;
   unsigned int cleanable;

   if(page_va == 0)
   {
     /* this page is unmapped, already clean. Not cleanable */
     return 0;
   }

   ASSERT(((page_va >= (unsigned int)&__swapped_segments_start__)
       && (page_va <= (unsigned int)&__swapped_segments_end__)));

   page_idx = (page_va - (unsigned int)&__swapped_segments_start__) >> PAGE_SHIFT;   
   cleanable = (dlpager_pagesm.table[page_idx] == MAPPED_DIRTY);   

   return cleanable;
}

void dlpager_pagesm_handle_event(dlpager_event_t event, unsigned int page_va, qurt_thread_t task_id)
{
  dlpager_page_state_t state_begin, state_end;
  unsigned int page_idx, log_idx;
  pagesm_pagemutex_t page_mutex;
  
  ASSERT( ((page_va >= (unsigned int)&__swapped_segments_start__)
      && (page_va < (unsigned int)&__swapped_segments_end__)));

  page_idx = (page_va - (unsigned int)&__swapped_segments_start__) >> PAGE_SHIFT;
  page_va = DLPAGER_ALIGN_DOWN(page_va, PAGE_SIZE);
  
  page_mutex = pagesm_pagemutex_get(page_va);
  pagesm_pagemutex_lock(page_mutex);

  state_begin = dlpager_pagesm.table[page_idx];
  ASSERT(state_begin < INVALID_STATE);
  state_end = next_state[state_begin][event];      
  ASSERT(state_end < INVALID_STATE);
  log_idx = dlpager_pagelog_start(state_begin, state_end, event, (void*) page_va, task_id);
  dlpager_debug_handle_start_state(state_begin);
  dlpager_pagesm.table[page_idx] = state_end;      
  dlpager_handlers[state_begin][event](page_va, task_id);  
  
  pagesm_pagemutex_unlock(page_mutex);
  pagesm_pagemutex_free(page_mutex);

  dlpager_pagelog_end(log_idx);
  dlpager_debug_handle_end_state(state_end);
  dlpager_stats.transitions[state_begin][state_end]++;
}

int dlpager_pagesm_init(void)
{
  unsigned int table_size;
  unsigned int segment_size, num_global_pages;
  unsigned int i,j;
 
  segment_size = DLPAGER_ALIGN_UP(((unsigned int)&__swapped_segments_end__), PAGE_SIZE) - DLPAGER_ALIGN_DOWN(((unsigned int)&__swapped_segments_start__), PAGE_SIZE);
  num_global_pages = segment_size >> PAGE_SHIFT;

  ASSERT(num_global_pages != 0);

  table_size = num_global_pages * sizeof(dlpager_page_state_t);
  dlpager_pagesm.table = (dlpager_page_state_t *) malloc(table_size);
  ASSERT(dlpager_pagesm.table != NULL);
  dlpager_pagesm.count = num_global_pages;

  /* RO and RW pages start as UNMAPPED_CLEAN */
  for(i = (unsigned int)&__swapped_segments_start__, j = 0; i < (unsigned int)&__swapped_segments_bss_start__; i+= PAGE_SIZE, j++)
  {
    dlpager_pagesm.table[j] =  UNMAPPED_CLEAN;    
  } 
  /* BSS pages start as UNMAPPED_BSS */
  for(;i < (unsigned int)&__swapped_segments_end__; i+= PAGE_SIZE, j++)
  {
    dlpager_pagesm.table[j] =  UNMAPPED_BSS;    
  }   
  
  qurt_pimutex_init(& dlpager_pagesm.mutex);
  pagesm_pagemutex_init();

  for(i = 0; i < MAX_STATES; i++)
    for(j = 0; j < MAX_EVENTS; j++)
    {
      dlpager_handlers[i][j] = &dlpager_handler_default;
    }

  dlpager_handlers[UNMAPPED_CLEAN][TLB_MISS_X] = dlpager_handler_tlb_miss_x_at_unmapped_clean;
  dlpager_handlers[UNMAPPED_CLEAN][TLB_MISS_R] = dlpager_handler_tlb_miss_r_at_unmapped_clean;
  dlpager_handlers[UNMAPPED_CLEAN][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_clean;

  dlpager_handlers[UNMAPPED_BSS][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_bss;
  
  dlpager_handlers[UNMAPPED_CLEAN_DECOMPRESSING][TLB_MISS_X] = dlpager_handler_tlb_miss_x_at_unmapped_clean_decompressing;
  dlpager_handlers[UNMAPPED_CLEAN_DECOMPRESSING][TLB_MISS_R] = dlpager_handler_tlb_miss_r_at_unmapped_clean_decompressing;
  dlpager_handlers[UNMAPPED_CLEAN_DECOMPRESSING][DECOMPRESSION_COMPLETE] = dlpager_handler_decompression_complete_at_unmapped_clean_decompressing;

  dlpager_handlers[UNMAPPED_DIRTY_DECOMPRESSING][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_dirty_decompressing;
  dlpager_handlers[UNMAPPED_DIRTY_DECOMPRESSING][DECOMPRESSION_COMPLETE] = dlpager_handler_decompression_complete_at_unmapped_dirty_decompressing;
 
  dlpager_handlers[UNMAPPED_CLEAN_ALLOCATED][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_clean_allocated;
  dlpager_handlers[UNMAPPED_CLEAN_ALLOCATED][EVICT_PAGE] = dlpager_handler_evict_page_at_unmapped_clean_allocated;  

  dlpager_handlers[MAPPED_CLEAN][EVICT_PAGE] = dlpager_handler_evict_page_at_mapped_clean;
  
  dlpager_handlers[UNMAPPED_DIRTY_COMPRESSING_ALLOCATED][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_dirty_compressing_allocated;
  dlpager_handlers[UNMAPPED_DIRTY_COMPRESSING_ALLOCATED][COMPRESSION_COMPLETE] = dlpager_handler_compression_complete_at_unmapped_dirty_compressing_allocated;  
  dlpager_handlers[UNMAPPED_DIRTY_COMPRESSING_ALLOCATED][SOFT_CLEAN_FAILED] = dlpager_handler_soft_clean_failed_at_unmapped_dirty_compressing_allocated;

  dlpager_handlers[UNMAPPED_HARD_CLEAN_COMPRESSING][TLB_MISS_W] = dlpager_handler_tlb_miss_w_at_unmapped_hard_clean_compressing;
  dlpager_handlers[UNMAPPED_HARD_CLEAN_COMPRESSING][COMPRESSION_COMPLETE] = dlpager_handler_compression_complete_at_unmapped_hard_clean_compressing;
 
  dlpager_handlers[MAPPED_DIRTY][SOFT_CLEAN_PAGE] = dlpager_handler_soft_clean_page_at_mapped_dirty;
  dlpager_handlers[MAPPED_DIRTY][HARD_CLEAN_PAGE] = dlpager_handler_hard_clean_page_at_mapped_dirty;

  dlpager_handlers[MAPPED_DIRTY_COMPRESSING][COMPRESSION_COMPLETE] = dlpager_handler_compression_complete_at_mapped_dirty_compressing;
 
  return 0;
}


