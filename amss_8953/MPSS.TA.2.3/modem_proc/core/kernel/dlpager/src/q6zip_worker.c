#include "assert.h"
#include "q6zip_assert.h"
#include "q6zip_worker.h"
#include <stdlib.h>

#define Q6ZIP_WORKER_MIN_STACK_SIZE (1 * 1024)

static void q6zip_worker_entry_wrapper (void * argument)
{
    q6zip_worker_t * worker = (q6zip_worker_t *)argument;

    /* @todo anandj QuRT task entry dont return void * */
    (void) worker->entry( worker->argument );
}

void q6zip_worker_fork (
    q6zip_worker_t *        worker, 
    char const *            name,
    unsigned int            stack_size,
    q6zip_worker_entry_fn_t entry, 
    unsigned int            priority,
    void *                  argument
)
{
    qurt_thread_attr_t attr;
    int error;

    worker->entry    = entry;
    worker->priority = priority;
    worker->argument = argument;

    ASSERT( stack_size >= Q6ZIP_WORKER_MIN_STACK_SIZE );

    worker->stack = malloc( stack_size );
    ASSERT( worker->stack );

    qurt_thread_attr_set_name( &attr, (char *)name );
    qurt_thread_attr_set_stack_addr( &attr, worker->stack );
    qurt_thread_attr_set_stack_size( &attr, stack_size );
    qurt_thread_attr_set_priority( &attr, priority );

    error = qurt_thread_create( &worker->thread, &attr, q6zip_worker_entry_wrapper, worker );

    /* @todo anandj *MUST* err_fatal here! */
    ASSERT( error == QURT_EOK );
}

void q6zip_worker_join (q6zip_worker_t * worker)
{
    int status;
    int error;

    error = qurt_thread_join( worker->thread, &status );
    Q6ZIP_ASSERT( error == QURT_EOK );
    free( worker->stack );
}
