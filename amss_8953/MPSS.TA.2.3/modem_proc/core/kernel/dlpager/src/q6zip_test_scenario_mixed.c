#include "err.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test_scenario_mixed.h"
#include "q6zip_test_vectors_ro.h"
#include "q6zip_test_vectors_rw.h"
#include "q6zip_waiter.h"

typedef struct
{
    q6zip_test_vectors_index_t const * index;
    unsigned int num_vectors;
    q6zip_iovec_t dst;
    unsigned int cur_vector;
    unsigned int pass;
    q6zip_algo_t algo;

} q6zip_test_vectors_t;

/** @brief Destination/uncompressed page
    @warning *MUST* be aligned on cache line */
static uint8 unzipped_page[ 4096 ] __attribute__(( aligned( 32 ) ));

/** @brief Destination/compressed page
    @warning *MUST* be aligned on cache line */
static uint8 zipped_page[ 4352 ] __attribute__(( aligned( 32 ) ));

static q6zip_waiter_t waiter;

q6zip_test_vectors_t vectors[] = {
    {
        RO_VECTORS_INDEX,
        Q6ZIP_ARRAY_SIZE( RO_VECTORS_INDEX ),
        {
            .ptr = unzipped_page,
            .len = sizeof( unzipped_page )
        },
        0,
        0,
        Q6ZIP_ALGO_UNZIP_RO
    },
    {
        RW_VECTORS_INDEX,
        Q6ZIP_ARRAY_SIZE( RW_VECTORS_INDEX ),
        {
            .ptr = unzipped_page,
            .len = sizeof( unzipped_page )
        },
        0,
        0,
        Q6ZIP_ALGO_UNZIP_RW,
    },
    {
        RW_VECTORS_INDEX,
        Q6ZIP_ARRAY_SIZE( RW_VECTORS_INDEX ),
        {
            .ptr = zipped_page,
            .len = sizeof( zipped_page )
        },
        0,
        0,
        Q6ZIP_ALGO_ZIP_RW,
    }
};

static void initialize (q6zip_test_scenario_t * scenario)
{
    (void) zipped_page;
    q6zip_waiter_init( &waiter );
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
}

static void start (q6zip_test_scenario_t * scenario)
{
    q6zip_test_vectors_t * vset;
    unsigned int vector_set_index = 0;
    boolean bit_exact_match;
    unsigned int i;
    q6zip_iovec_t expected = { NULL, 0 };

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ENTER: Mixed tests. rod_rwd_dst=0x%x", unzipped_page, 0, 0 );

    for ( i = 0; i < 16 /*Q6ZIP_ARRAY_SIZE( RW_VECTORS_INDEX )*/; i++ )
    {
        vset = &vectors[ vector_set_index ];

        switch ( vset->algo )
        {
            case Q6ZIP_ALGO_UNZIP_RO:

                expected.ptr = ( void * )vset->index[ vset->cur_vector ].unzipped;
                expected.len = vset->index[ vset->cur_vector ].unzipped_size;

                q6zip_test_unzip_ro(
                    vset->dst.ptr,
                    vset->dst.len,
                    ( void * ) vset->index[ vset->cur_vector ].zipped,
                    vset->index[ vset->cur_vector ].zipped_size,
                    ( void * ) &vset->index[ vset->cur_vector ],
                    &waiter );
                break;

            case Q6ZIP_ALGO_UNZIP_RW:

                expected.ptr = ( void * )vset->index[ vset->cur_vector ].unzipped;
                expected.len = vset->index[ vset->cur_vector ].unzipped_size;

                q6zip_test_unzip_rw(
                    vset->dst.ptr,
                    vset->dst.len,
                    ( void * )vset->index[ vset->cur_vector ].zipped,
                    vset->index[ vset->cur_vector ].zipped_size,
                    ( void * ) &vset->index[ vset->cur_vector ],
                    &waiter );
                break;

            case Q6ZIP_ALGO_ZIP_RW:
                expected.ptr = ( void * )vset->index[ vset->cur_vector ].zipped;
                expected.len = vset->index[ vset->cur_vector ].zipped_size;

                q6zip_test_zip_rw(
                    vset->dst.ptr,
                    vset->dst.len,
                    ( void * )vset->index[ vset->cur_vector ].unzipped,
                    vset->index[ vset->cur_vector ].unzipped_size,
                    Q6ZIP_FIXED_PRIORITY_HIGH,
                    ( void * ) &vset->index[ vset->cur_vector ],
                    &waiter );
                break;

            default:
                Q6ZIP_ASSERT( FALSE );
                break;
        }

        bit_exact_match = ( memcmp( expected.ptr, vset->dst.ptr, expected.len ) == 0 );

        if ( !bit_exact_match )
        {
            break;
        }

        vset->pass++;

        /* Next vector in the set */
        vset->cur_vector = ( vset->cur_vector + 1 ) % vset->num_vectors;

        /* Next vector set */
        vector_set_index = ( vector_set_index + 1 ) % Q6ZIP_ARRAY_SIZE( vectors );
    }

    if ( bit_exact_match )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "PASS: rod=%u rwd=%u rwc=%u",
               vectors[ 0 ].pass, vectors[ 1 ].pass, vectors[ 2 ].pass );
    }
    else
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "FAIL: vector_set_index=%u vector_index=%u\n",
               vector_set_index, vset->cur_vector, 0 );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "LEAVE: Mixed tests", 0, 0, 0 );
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    q6zip_test_vectors_index_t const * test_vector;

    Q6ZIP_ASSERT( !response->error );

    test_vector = ( q6zip_test_vectors_index_t const * )request->user.other;
    Q6ZIP_ASSERT( test_vector );

    switch ( request->algo )
    {
        case Q6ZIP_ALGO_UNZIP_RO:
        case Q6ZIP_ALGO_UNZIP_RW:
            Q6ZIP_ASSERT( response->len == test_vector->unzipped_size );
            break;

        case Q6ZIP_ALGO_ZIP_RW:
            Q6ZIP_ASSERT( response->len == test_vector->zipped_size );
            break;

        default:
            Q6ZIP_ASSERT( FALSE );
            break;
    }

    q6zip_waiter_wake( &waiter );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_mixed =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
