#include "q6zip_ipa_initializer.h"
#include "q6zip_params.h"
#include "q6zip_worker.h"
#include "qurt.h"

#define Q6ZIP_IPA_INIT_REGISTERED        ( 1 << 0 )
#define Q6ZIP_IPA_INIT_DICTIONARY_LOADED ( 1 << 1 )
#define Q6ZIP_IPA_INIT_ABORT             ( 1 << 2 )
#define Q6ZIP_IPA_INIT_TIMEOUT           ( 1 << 3 )

#define Q6ZIP_IPA_INIT_SIGNALS           \
    ( Q6ZIP_IPA_INIT_REGISTERED        | \
      Q6ZIP_IPA_INIT_DICTIONARY_LOADED | \
      Q6ZIP_IPA_INIT_ABORT             | \
      Q6ZIP_IPA_INIT_TIMEOUT             \
    )

/** @brief States this initializer can be in */
typedef enum
{
    Q6ZIP_IPA_INIT_STATE_NONE,
    Q6ZIP_IPA_INIT_STATE_REGISTRATION_IN_PROGRESS,
    Q6ZIP_IPA_INIT_STATE_DICT_LOAD_IN_PROGRESS,
    Q6ZIP_IPA_INIT_STATE_DONE
} q6zip_ipa_init_state_t;

typedef struct
{
    /** @brief IPA async initialization worker thread */
    q6zip_worker_t worker;
    /** @brief RO dictionary */
    q6zip_iovec_t const * dict;

    /** @brief Callback to invoke once IPA initialization has completed */
    q6zip_initialized_cb_t initialized;

    /** @brief Signals for this IPA async initializer */
    qurt_signal_t signals;

    unsigned int last_signals;

    /** @brief Timer to keep a check on timely responses from IPA during
               initialization stage */
    qurt_timer_t ipa_init_timer;

    /** @brief Current state of the initializer */
    q6zip_ipa_init_state_t state;

} q6zip_ipa_async_initializer_t;

static q6zip_ipa_async_initializer_t q6zip_ipa_async_initializer_module;
static q6zip_ipa_async_initializer_t * module;

static void * q6zip_ipa_initializer_worker (void * argument)
{
    unsigned int signals;
    boolean working = TRUE;

    module->state = Q6ZIP_IPA_INIT_STATE_REGISTRATION_IN_PROGRESS;

    /*fprintf( stdout, " info: Q6ZIP IPA initializer started\n" );*/

    while ( working )
    {
        signals = qurt_signal_wait_any( &module->signals, Q6ZIP_IPA_INIT_SIGNALS );
        qurt_signal_clear( &module->signals, signals );
        module->last_signals = signals;

        if ( signals & Q6ZIP_IPA_INIT_REGISTERED )
        {
            ipa_zip_dict_type_s dict;

            qurt_timer_stop( module->ipa_init_timer );

            if ( Q6ZIP_PARAM_IPA_DICT_LOAD_TIMEOUT )
            {
                qurt_timer_restart( module->ipa_init_timer, Q6ZIP_PARAM_IPA_DICT_LOAD_TIMEOUT );
            }

            module->state = Q6ZIP_IPA_INIT_STATE_DICT_LOAD_IN_PROGRESS;

#if defined( Q6ZIP_UNIT_TEST ) || defined( ENABLE_Q6ZIP_IPA )
            /* Q6ZIP unit-test environment and Q6 IPA driver on target supports
               dictionary load. */

            /* Initiate dictionary load */
            dict.dict_buf_ptr = module->dict->ptr;
            dict.dict_buf_len = module->dict->len;
            ipa_zip_load_dict( &dict );
#else
            /* On other targets, Q6 IPA driver doesnt support dictionary load.
               So, we fake it */
            qurt_signal_set( &module->signals, Q6ZIP_IPA_INIT_DICTIONARY_LOADED );
#endif
        }
        else if ( signals & Q6ZIP_IPA_INIT_DICTIONARY_LOADED )
        {
            qurt_timer_stop( module->ipa_init_timer );

            /* Dictionary loaded. Vote for IPA clock.
               @todo anandj For initial testing only eventually, we need to
               boost clocks dynamically */
            ipa_zip_clk_req( IPA_ZIP_CLK_REQ_MAX_CLK );

            module->state = Q6ZIP_IPA_INIT_STATE_DONE;

            /* We're done with IPA initialization */
            module->initialized( TRUE );

            working = FALSE;
        }
        else if ( signals & ( Q6ZIP_IPA_INIT_ABORT | Q6ZIP_IPA_INIT_TIMEOUT ) )
        {
            qurt_timer_stop( module->ipa_init_timer );

            /* Notify user */
            module->initialized( FALSE );
            working = FALSE;
        }
    }

    /*fprintf( stdout, " info: Q6ZIP IPA initializer ended\n" );*/
    return NULL;
}

void q6zip_ipa_initializer_init (
    q6zip_iovec_t const *  dict,
    q6zip_initialized_cb_t initialzed
)
{
    qurt_timer_attr_t timer_attr;

    module = &q6zip_ipa_async_initializer_module;

    module->dict = dict;
    module->initialized = initialzed;

    /* We are using signals for user <-> worker communication */
    qurt_signal_init( &module->signals );

    qurt_timer_attr_init( &timer_attr );
    qurt_timer_attr_set_duration( &timer_attr, QURT_TIMER_MAX_DURATION );
    qurt_timer_create( &module->ipa_init_timer, &timer_attr, &module->signals, Q6ZIP_IPA_INIT_TIMEOUT );

    qurt_timer_stop( module->ipa_init_timer );

    if ( Q6ZIP_PARAM_IPA_REGISTRATION_TIMEOUT )
    {
        qurt_timer_restart( module->ipa_init_timer, Q6ZIP_PARAM_IPA_REGISTRATION_TIMEOUT );
    }

    /* Fork the worker. It will handle initialization sequence */
    q6zip_worker_fork( 
        &module->worker, 
        "q6zip_ipa_init",
        Q6ZIP_PARAM_IPA_INIT_WORKER_STACK_SIZE,
        q6zip_ipa_initializer_worker, 
        106, 
        NULL );
}

void q6zip_ipa_initializer_notify (
    ipa_zip_cb_event_type_e  event,
    ipa_zip_cb_status_type_e status
)
{
    if ( status != IPA_ZIP_CB_STATUS_PASS )
    {
        /* @todo anandj Record reason for failure */

        /* Tell initialization worker to abort */
        qurt_signal_set( &module->signals, Q6ZIP_IPA_INIT_ABORT );
        return;
    }

    switch ( event )
    {
        case IPA_ZIP_CB_EVENT_REGISTER:
            qurt_signal_set( &module->signals, Q6ZIP_IPA_INIT_REGISTERED );
            break;

        case IPA_ZIP_CB_EVENT_DICT_LOAD:
            qurt_signal_set( &module->signals, Q6ZIP_IPA_INIT_DICTIONARY_LOADED );
            break;

        default:
            /* @warning These events are not part of initialization and 
               therefore are of no interest to this module */
            break;
    }
}
