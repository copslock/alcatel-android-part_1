/*==========================================================================
 * FILE:         dlpager_swappool.c
 *
 * SERVICES:     DL PAGER SWAP POOL
 *
 * DESCRIPTION:  This file provides functionality to manage dlpager swap pool. 
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_swappool.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rr      Refactored from memload_handler.c
===========================================================================*/
#include <qurt.h>
#include <dlpager.h>
#include <assert.h>
#include <stdlib.h>
#include <dlpager.h>
#include <dlpager_params.h>
#include <dlpager_swappool.h>
#include <hexagon_protos.h>

unsigned int dlpager_soft_cleaning_threshold = DLPAGER_SOFT_CLEANING_THRESHOLD;
unsigned int dlpager_hard_cleaning_threshold = DLPAGER_HARD_CLEANING_THRESHOLD; 

/** Data structre that serves as the basic node/data holder for loaded_pages list
 */
typedef struct _swappool_node {
  struct _swappool_node *next;
  struct _swappool_node *prev;
  unsigned int va;
} swappool_node_t;

struct dlpager_swappool_s {
    qurt_mem_region_t region; /* QuRT region of swap pool */
    qurt_mem_region_t region_rx; /* QuRT region of allocated virtual memory */
    unsigned int region_size; /* size of the allocated swap pool */
    unsigned int seg_addr_base; /**< Base address of the segment used for loading */
    unsigned int p_addr_base; /**< Base physical address of the swap pool */ 
    unsigned int v_swap_rw_addr_base; /**< Base virtual address of the swap pool for RW operation */
    unsigned int v_swap_rx_addr_base; /**< Base virtual address of the swap pool for RX operations */
    unsigned int page_shift; /**< x, where 2 power x is the page size */
    unsigned int max_swap_pages; /* Max. number of swappable pages */
    unsigned int max_glob_pages; /* Max. number of pages in the segment */
    swappool_node_t* swap_list_head;  /*list of swap pages*/
    swappool_node_t* loaded_pages; /* Table of virt addr of the loaded pages and swap nodes */
    unsigned int *global_pages; /* Mapping table of the overall segment */
    qurt_mutex_t mutex;        /* Mutex protects swappool operations */
    qurt_sem_t  num_evictable_pages; /* Semaphore tracks number of clean pages in the system */
}dlpager_swappool;

/** @brief Visibility into swap pools (currently only one) for Trace32 &
    crashscope usage */
struct dlpager_swappool_s * swap_pools[ 1 ];

extern unsigned int __attribute__ ((weak)) __swapped_segments_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_end__;

void dlpager_swappool_get_info (qurt_addr_t * const va, unsigned int * const size)
{
    *va = dlpager_swappool.v_swap_rw_addr_base;
    *size = dlpager_swappool.region_size;
}

static void swappool_remove_node(swappool_node_t **ring, swappool_node_t *node)
{
  node->prev->next = node->next;
  node->next->prev = node->prev;
  if (*ring == node) {
    *ring = node->next;
  }
}

static void swappool_insert_head(swappool_node_t **ring, swappool_node_t *node)
{
  node->next = *ring;
  node->prev = (*ring)->prev;
  node->prev->next = node;
  node->next->prev = node;  
  *ring = node;
}

#if 0  /*TODO: Commented it out to overcome compiler warning treated as error. Retaining it, just incase it may be needed in future */
static void swappool_insert_tail(swappool_node_t **ring, swappool_node_t *node)
{
  swappool_node_t *tail = (*ring)->prev;
  node->next = *ring;
  node->prev = tail;  
  (*ring)->prev = node;
  tail->next = node;
}
#endif

static inline swappool_node_t* swappool_get_node_ptr(unsigned int pa)
{
  unsigned int addr_idx;
  swappool_node_t *node;
   
  addr_idx = (pa - dlpager_swappool.p_addr_base) >> dlpager_swappool.page_shift;
  node = &dlpager_swappool.loaded_pages[addr_idx];
 
  return node;
}

static inline void swappool_get_mapping(unsigned int va, unsigned int *pa)
{
  unsigned int va_offset;

  va_offset = (va - (unsigned int)&__swapped_segments_start__) >> 12;
  *pa = dlpager_swappool.global_pages[va_offset];  
}

dlpager_swappool_mapping_t dlpager_swappool_get_lru_mapping(unsigned int offset_from_lru)
{
  dlpager_swappool_mapping_t mapping;
  swappool_node_t *node;
  unsigned int addr_idx;
  
  ASSERT(offset_from_lru < dlpager_swappool.max_swap_pages);

  qurt_pimutex_lock(&dlpager_swappool.mutex);
  node = dlpager_swappool.swap_list_head->prev;
  while(offset_from_lru --)
  {
    node = node->prev;
  }
    
  addr_idx = node - dlpager_swappool.loaded_pages;
  mapping.pa = dlpager_swappool.p_addr_base + (addr_idx << dlpager_swappool.page_shift);
  mapping.va = node->va;
  qurt_pimutex_unlock(&dlpager_swappool.mutex);
  return mapping;
}

void dlpager_swappool_mru_page(unsigned int va)
{
  unsigned int va_offset, pa;
  swappool_node_t *node;

  qurt_pimutex_lock(&dlpager_swappool.mutex); 

  va_offset = (va - (unsigned int)&__swapped_segments_start__) >> 12;
  pa = dlpager_swappool.global_pages[va_offset];
  node = swappool_get_node_ptr(pa);

  swappool_remove_node(&dlpager_swappool.swap_list_head, node);
  swappool_insert_head(&dlpager_swappool.swap_list_head, node);

  qurt_pimutex_unlock(&dlpager_swappool.mutex);
}


void dlpager_swappool_mru_pages(unsigned int *pa_array, unsigned int size)
{
  swappool_node_t *node;
  unsigned int i;

  if(size == 0)
     return;

  qurt_pimutex_lock(&dlpager_swappool.mutex); 
  for(i = 0; i < size; i++)
  {
    node = swappool_get_node_ptr(pa_array[i]); 
    swappool_remove_node(&dlpager_swappool.swap_list_head, node);
    swappool_insert_head(&dlpager_swappool.swap_list_head, node);
  }
  qurt_pimutex_unlock(&dlpager_swappool.mutex); 
}


//TODO: assumes 4K page size
inline void dlpager_swappool_get_mapping(unsigned int va, unsigned int *pa)
{
  swappool_get_mapping(va, pa);
}

void dlpager_swappool_rw_mapping_add()
{
  qurt_mapping_create(dlpager_swappool.v_swap_rw_addr_base,
                        dlpager_swappool.p_addr_base,
                        dlpager_swappool.region_size,
                        QURT_MEM_CACHE_WRITEBACK,
                        QURT_PERM_READ | QURT_PERM_WRITE);
}

void dlpager_swappool_rw_mapping_remove()
{
  qurt_mapping_remove(dlpager_swappool.v_swap_rw_addr_base,
                        dlpager_swappool.p_addr_base,
                        dlpager_swappool.region_size);
}

//TODO: assumes 4K page size
void dlpager_swappool_lookup(unsigned int va, dlpager_swappool_page_info_t *ret)
{
  unsigned int pa, pool_offset, va_offset;

  qurt_pimutex_lock(&dlpager_swappool.mutex); 
  va_offset = (va - (unsigned int)&__swapped_segments_start__) >> 12;
  pa = dlpager_swappool.global_pages[va_offset];
  pool_offset = (pa - dlpager_swappool.p_addr_base);

  ret->pa = pa;
  ret->va = va;
  ret->va_swap_rw = dlpager_swappool.v_swap_rw_addr_base + pool_offset;
  ret->va_swap_rx = dlpager_swappool.v_swap_rx_addr_base + pool_offset;
  qurt_pimutex_unlock(&dlpager_swappool.mutex); 
}

void dlpager_swappool_remove_mapping(unsigned int va)
{
  swappool_node_t *node;
  unsigned int pa;
  unsigned int va_offset;

  qurt_pimutex_lock(&dlpager_swappool.mutex); 
  va_offset = (va - (unsigned int)&__swapped_segments_start__) >> 12;
  pa = dlpager_swappool.global_pages[va_offset];  

  node = swappool_get_node_ptr(pa);
  node->va = 0; 

  dlpager_swappool.global_pages[va_offset]=0;
  //swappool_remove_node(&dlpager_swappool.swap_list_head, node);
  //swappool_insert_tail(&dlpager_swappool.swap_list_head, node);
  qurt_pimutex_unlock(&dlpager_swappool.mutex); 
}

unsigned int dlpager_swappool_evictable_pages_dec()
{

  qurt_sem_down(&dlpager_swappool.num_evictable_pages);
  return (unsigned int) qurt_sem_get_val(&dlpager_swappool.num_evictable_pages);
}

void dlpager_swappool_evictable_pages_inc()
{
  qurt_sem_up(&dlpager_swappool.num_evictable_pages);
}

void dlpager_swappool_insert_mapping(unsigned int va, unsigned int pa)
{
  unsigned int va_offset;
  swappool_node_t *node;

  qurt_pimutex_lock(&dlpager_swappool.mutex); 
  node = swappool_get_node_ptr(pa);
  node->va = va;

  va_offset = (va - (unsigned int)&__swapped_segments_start__) >> 12;
  ASSERT(dlpager_swappool.global_pages[va_offset] == 0);
  dlpager_swappool.global_pages[va_offset]=pa;

  swappool_remove_node(&dlpager_swappool.swap_list_head, node);
  swappool_insert_head(&dlpager_swappool.swap_list_head, node);
  qurt_pimutex_unlock(&dlpager_swappool.mutex); 
}

void dlpager_swappool_lock()
{
  qurt_pimutex_lock(&dlpager_swappool.mutex); 
}

void dlpager_swappool_unlock()
{
  qurt_pimutex_unlock(&dlpager_swappool.mutex); 
}

void dlpager_swappool_init(unsigned int pool_size)
{
  qurt_mem_region_attr_t attr;
  qurt_mem_region_t region;
  qurt_mem_pool_t default_pool;
  int ret;
  unsigned int i, pool_mem_size;

  qurt_mem_pool_attach ("DEFAULT_PHYSPOOL", &default_pool);  
  unsigned int p_addr, v_rw_addr, v_rx_addr, r_size;
  unsigned int seg_base, seg_size;


  /* Segment (not loaded) base address and size */
  seg_base = DLPAGER_ALIGN_DOWN ((unsigned int)&__swapped_segments_start__, PAGE_SIZE);
  seg_size = DLPAGER_ALIGN_UP (((unsigned int)&__swapped_segments_end__ - seg_base), PAGE_SIZE);

  /* swap pool don't need to be bigger than the segment */
  if (pool_size > seg_size) {
      pool_size = seg_size;
  }

  /* Create memory region for swap pool */
  qurt_mem_region_attr_init (&attr);
  qurt_mem_region_attr_set_mapping (&attr, QURT_MEM_MAPPING_VIRTUAL_RANDOM);
  ret = qurt_mem_region_create (&region, pool_size, default_pool, &attr);
  ASSERT (ret == QURT_EOK);
  qurt_mem_region_attr_get (region, &attr);
  qurt_mem_region_attr_get_physaddr (&attr, &p_addr);
  qurt_mem_region_attr_get_virtaddr (&attr, &v_rw_addr);
  qurt_mem_region_attr_get_size (&attr, &r_size);

  dlpager_swappool.seg_addr_base = seg_base;
  dlpager_swappool.region = region;
  dlpager_swappool.region_size = r_size;
  dlpager_swappool.p_addr_base = p_addr;
  dlpager_swappool.v_swap_rw_addr_base = v_rw_addr;
  dlpager_swappool.page_shift = PAGE_SHIFT;
  dlpager_swappool.max_glob_pages = seg_size >> PAGE_SHIFT;
  dlpager_swappool.max_swap_pages = r_size >>  PAGE_SHIFT;
  qurt_pimutex_init(&dlpager_swappool.mutex);
  qurt_sem_init_val(&dlpager_swappool.num_evictable_pages, dlpager_swappool.max_swap_pages); 

 /* Reserve virtual memory for RX operations */
  qurt_mem_region_attr_init (&attr);
  qurt_mem_region_attr_set_mapping (&attr, QURT_MEM_MAPPING_NONE);
  ret = qurt_mem_region_create (&region, r_size, default_pool, &attr);
  ASSERT (ret == QURT_EOK);
  qurt_mem_region_attr_get (region, &attr);
  qurt_mem_region_attr_get_virtaddr (&attr, &v_rx_addr);
  
  dlpager_swappool.v_swap_rx_addr_base = v_rx_addr;

  /* Map vitual memory for RX operation */
  qurt_mapping_create (v_rx_addr, p_addr, r_size, QURT_MEM_CACHE_WRITEBACK, QURT_PERM_READ | QURT_PERM_EXECUTE);

  /* Initialize swap pool for paging */
  /* Allocate data structure for swap pool managemenet */
  pool_mem_size =   (sizeof(unsigned int) * dlpager_swappool.max_glob_pages) +
                    (sizeof(swappool_node_t) * dlpager_swappool.max_swap_pages);
  
  dlpager_swappool.loaded_pages = (swappool_node_t*)malloc(pool_mem_size);
  ASSERT(dlpager_swappool.loaded_pages != NULL);
  dlpager_swappool.global_pages = (unsigned int*)&(dlpager_swappool.loaded_pages[dlpager_swappool.max_swap_pages]);
   
  memset((void *)dlpager_swappool.loaded_pages, (int)0, pool_mem_size);
 
  /* 
   * Initialize loadable pages to reflect no mapping.
   * This is a doubly linked list, circular buffer.
   * First element's prev points to last element.
   * Last element's next points to first element.
   */
    for (i = 0; i < dlpager_swappool.max_swap_pages; i++) {
        dlpager_swappool.loaded_pages[i].next = &dlpager_swappool.loaded_pages[i+1];
        dlpager_swappool.loaded_pages[i].prev = &dlpager_swappool.loaded_pages[i-1];    
    }
    dlpager_swappool.loaded_pages[0].prev = &dlpager_swappool.loaded_pages[dlpager_swappool.max_swap_pages-1];
    dlpager_swappool.loaded_pages[dlpager_swappool.max_swap_pages-1].next = &dlpager_swappool.loaded_pages[0];
    
   dlpager_swappool.swap_list_head =  &dlpager_swappool.loaded_pages[0];

  /* Invalidate the pool in both I-cache and D-cache */
    qurt_mem_cache_clean ((qurt_addr_t)v_rx_addr, pool_size,
                          QURT_MEM_CACHE_INVALIDATE, QURT_MEM_ICACHE);
    qurt_mem_cache_clean ((qurt_addr_t)v_rw_addr, pool_size,
                          QURT_MEM_CACHE_INVALIDATE, QURT_MEM_DCACHE);


  /*Removing the RW mappings as the swap pool has been configured*/
  qurt_mapping_remove(v_rw_addr, p_addr, r_size);      
  
  /* dlpager_cleaning_threshold shold not be greater than swappool size */
  if(dlpager_soft_cleaning_threshold > dlpager_swappool.max_swap_pages)
    dlpager_soft_cleaning_threshold = dlpager_swappool.max_swap_pages;

  swap_pools[ 0 ] = &dlpager_swappool;
}
