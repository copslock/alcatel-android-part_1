/*==========================================================================
 * FILE:         dlpager_main.c
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  This file provides the implementation of pager service
 *               initilaiztion and creation of pager thread. It servers as an
 *               interface to Kernel.
 *
 * Copyright (c) 2010-2013 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_main.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/01/13   bc      Re-write dlpager_posix_main.c
===========================================================================*/
#include <stdlib.h>
#include <assert.h>

#include <qurt.h>
#include "dlpager_compiler.h"
#include <dlpager_main.h>
#include <memload_handler.h>
#include "dlpager_log_legacy.h"
#include "dlpager_swapmem.h"

#define PAGE_SIZE_4K     (4*1024)      /* 4K page, the smallest pages supported */
#define PAGE_SIZE_4M     (4*1024*1024) /* 4M page, the largest page supported */


void dlpager_q6zip_init(void);

/* dlpager Thread ID */
qurt_thread_t dlpager_tid;

/* dlpager attributes set externally */
dlpager_attr_t  dlpager_attr;

/* Index to the log table */
unsigned int dlpager_log_idx;
/* Log page misses */
struct dlpager_log_entry dlpager_log[DLPAGER_MAX_LOG_ENTRIES];

/* Enable / Disable logging */
int dlpager_log_enabled = 1;



extern unsigned int __attribute__ ((weak)) __swapped_segments_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_end__;

unsigned int start_va_uncompressed_text = 0;
unsigned int end_va_uncompressed_text = 0;
unsigned int start_va_compressed_text = 0;
unsigned int end_va_compressed_text = 0;
unsigned int start_va_uncompressed_rw = 0;
unsigned int end_va_uncompressed_rw = 0;
unsigned int start_va_compressed_rw = 0;
unsigned int end_va_compressed_rw = 0;

/* DL PAGER Versioning*/
const char dlpager_version[] = DLPAGER_VERSION;


/*
 * dlpager_main
 *
 * DL pager thread. It creates two regions to create a shadow mapping to the
 * swap pool (physical) for maintenance. One regions is RW to help loading the
 * page and the other regions is to help invalidate I-cache.
 */
void dlpager_main (void *pArg)
{
    int i, ret;
    qurt_sysevent_pagefault_t pf_data;
    unsigned int seg_base, seg_size;

    /* Segment (not loaded) base address and size */
    seg_base = DLPAGER_ALIGN_DOWN ((unsigned int)&__swapped_segments_start__, PAGE_SIZE_4K);
    seg_size = DLPAGER_ALIGN_UP (((unsigned int)&__swapped_segments_end__ - seg_base), PAGE_SIZE_4K);

    for (i = 0; i < dlpager_attr.num_swap_pools; i++) {
        unsigned int pool_size = dlpager_attr.swap_pool[i].size;
        unsigned int page_size = dlpager_attr.swap_pool[i].page_size;

        /* swap pool don't need to be bigger than the segment */
        if (pool_size > seg_size) {
            pool_size = seg_size;
        }

        /* Initialize swap memory */
        dlpager_swapmem_create(pool_size);
        
        /* Invalidate the pool in both I-cache and D-cache */
        dlpager_swapmem_invalidate();

        /* Initialize swap pool for paging */
        init_active_page_pool (i, pool_size,
		       	       page_size, seg_base, seg_size);

        /*Removing the RW mappings as the swap pools have been configured*/
        dlpager_swapmem_removerw();
    }

    while (1) {
        ret = qurt_exception_wait_pagefault (&pf_data);
        assert(ret == QURT_EOK);
  
        DL_DEBUG("receive page fault info: threadID=%x, faulting address=%x\n",
                 pf_data.thread_id, pf_data.fault_addr);

        if ((pf_data.fault_addr < seg_base) ||
            pf_data.fault_addr >= (seg_base + seg_size))
        {
            DL_DEBUG ("VA 0x%x out of range\n");
            continue;
        }

        dlpager_swapmem_addrw();
        memload_fault_handler (pf_data.fault_addr, pf_data.thread_id); 
        dlpager_swapmem_removerw();
		
        /* resume the faultiing thread */
        ret = qurt_thread_resume(pf_data.thread_id);
        assert( ret == QURT_EOK );
    }
}

/* 
 * dlpager_init
 *
 * Initialization function to start pager thread. Input parameters decides
 * the configuration of swap pool size, page size etc.
 */
DLPAGER_CONSTRUCTOR int dlpager_init (void)
{
    qurt_thread_attr_t thread_attr;
    int i, rc, status;
    void *pStack;
    unsigned int page_size;

    dlpager_q6zip_init();

    /* No need to start pager, if swap segments are not available */
    if (&__swapped_segments_start__ == &__swapped_segments_end__) {
        return 0;
    }

    rc = dlpager_get_attr (&dlpager_attr);

    assert (rc == 0);

    assert (dlpager_attr.num_swap_pools <= DLPAGER_MAX_SWAP_POOLS);

    /* If there are no swap pools, there is no point in starting DL Pager */
    if (dlpager_attr.num_swap_pools == 0) {
        DL_DEBUG("No need to start DL Pager\n");
	return -1;
    }

    for (i = 0; i < dlpager_attr.num_swap_pools; i++) {
        for (page_size = PAGE_SIZE_4K; page_size < PAGE_SIZE_4M; page_size *= 4) {
            if (dlpager_attr.swap_pool[i].page_size <= page_size) {
                break;
            }
        }
        if (dlpager_attr.swap_pool[i].page_size != page_size) {
            /* Not a valid page size of Hexagon processor */
            return -1;
        }
        dlpager_attr.swap_pool[i].size = DLPAGER_ALIGN_UP (dlpager_attr.swap_pool[i].size, page_size);
        DL_DEBUG("swap pool %d size = 0x%0x, page size = 0x%x\n", i, dlpager_attr.swap_pool[i].size, dlpager_attr.swap_pool[i].page_size);

    }

    /* Demand paging Thread */
    qurt_thread_attr_init (&thread_attr);
    pStack = malloc (DLPAGER_MAIN_STACK_SIZE);
    assert (pStack != NULL);
    qurt_thread_attr_set_name (&thread_attr, "DLPager_main");
    qurt_thread_attr_set_stack_addr (&thread_attr, pStack);
    qurt_thread_attr_set_stack_size (&thread_attr, DLPAGER_MAIN_STACK_SIZE);
    qurt_thread_attr_set_priority (&thread_attr, DLPAGER_MAIN_PRIO);
    status = qurt_thread_create (&dlpager_tid, &thread_attr, dlpager_main, (void *)0);
    assert(status == 0);
    DL_DEBUG(" Starting DL main Thread , id=%d \n", dlpager_tid);

    return 0;
}

