#===============================================================================
#
# STARTUP Scons
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2013 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary

# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.

# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.9.1/securemsm/aostlm/qdi_client/build/aostlm_qdi.scons#1 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 09/25/14   wt     Initial version.
#===============================================================================
from glob import glob
from os.path import join, basename

Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/aostlm/qdi_client"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

env.Append(CPPPATH = "${BUILD_ROOT}/core/api/securemsm/aostlm")
env.Append(CPPPATH = "${BUILD_ROOT}/core/securemsm/aostlm/core/inc")
env.Append(CPPPATH = "${BUILD_ROOT}/core/securemsm/aostlm/platform/inc")
env.Append(CPPPATH = "${BUILD_ROOT}/core/securemsm/aostlm/qdi/inc")


#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
    'DAL',
    'DEBUGTOOLS',
    'HAL',
    'MPROC',
    'SECUREMSM',
    'SERVICES',
    'STORAGE',
    'SYSTEMDRIVERS',

    # needs to be last also contains wrong comdef.h
    'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

####################################################################################
#
#  AOSTLM QDI Driver Client Enablement
#
#    AOSTLM QDI Driver Client is only 'on' for certain targets.  It is 'off' for SIM
#    and any targets not in the enabled list.  When AOSTLM is 'off', all license
#    requests are granted automatically.
#
####################################################################################

#  Set to true to turn AOSTLM QDI Driver Client off for EVERYTHING
env.Replace(DISABLE_AOSTLM_QDI = 'FALSE')

#  The enabled targets.  Add new targets as necessary
AOSTLM_QDI_ENABLED_TARGETS = [
   'msm8994'
]

#
#   Do not change the remainder of these lines
#
if env.get('CHIPSET') not in AOSTLM_QDI_ENABLED_TARGETS:
   env.Replace(DISABLE_AOSTLM_QDI = 'TRUE')

if ARGUMENTS.get('SIM') == "1" or ARGUMENTS.get('SIM') == "TRUE":
   env.Replace(DISABLE_AOSTLM_QDI = 'TRUE')

#
#  Inform whether AOSTLM QDI is enabled or not
#
if env.get('DISABLE_AOSTLM_QDI') == "TRUE":
   env.PrintInfo("AOSTLM QDI Driver Client Disabled for this build")
else :
   env.PrintInfo("AOSTLM QDI Driver Client Enabled for this build")

#-------------------------------------------------------------------------------
# Sources
#-------------------------------------------------------------------------------

if env.get('DISABLE_AOSTLM_QDI') == "TRUE":
    AOSTLM_QDI_SRC = [
	    '${BUILDPATH}/src/aostlm_qdi_client_stub.c', 
    ]

else:
    AOSTLM_QDI_SRC = [
            '${BUILDPATH}/src/aostlm_qdi_client.c',
    ]

# Sources that should never be shared
AOSTLM_QDI_CLEAN_SOURCES = env.FindFiles(['*.c'], '${BUILD_ROOT}/core/securemsm/aostlm/qdi_client/src')

#-------------------------------------------------------------------------------
# Add RC INIT
#-------------------------------------------------------------------------------

RCINIT_BUILD_TAG = ['CORE_MODEM', 'CORE_QDSP6_SW']

#
# AOSTLM QDI Diag Init
#
RCINIT_INIT_AOSTLM_QDI_DIAG = {
   'sequence_group'     : 'RCINIT_GROUP_1',
   'init_name'          : 'AOSTLM_QDI_DIAG',
   'init_function'      : 'aostlm_qdi_driver_client_startup',
}


if 'USES_RCINIT' in env:
   env.AddRCInitFunc( RCINIT_BUILD_TAG, RCINIT_INIT_AOSTLM_QDI_DIAG )

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

ADD_IMGS = ['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
            'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
            'APPS_IMAGE',   'CBSP_APPS_IMAGE',
            'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
            'AVS_ADSP_USER']

env.AddBinaryLibrary(ADD_IMGS, '${BUILDPATH}/aostlm_qdi_client', AOSTLM_QDI_SRC)

#-------------------------------------------------------------------------------
# Clean Sources
#-------------------------------------------------------------------------------
env.CleanPack(ADD_IMGS, AOSTLM_QDI_CLEAN_SOURCES)

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

