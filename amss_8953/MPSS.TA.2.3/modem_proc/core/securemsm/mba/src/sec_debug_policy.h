/**
@file sec_debug_policy.h
@brief Trustzone Fuse Provisioning debug policy specific definitions/routines

This file contains the target specific information for debug policy Provisioning

*/

/*=============================================================================
                              EDIT HISTORY
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


  $Header: //components/rel/core.mpss/3.9.1/securemsm/mba/src/sec_debug_policy.h#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $

 when           who         what, where, why
 --------       ---         --------------------------------------------------
 2014/07/08     st          Initial version
=============================================================================*/

#ifndef SEC_DEBUG_POLICY_H
#define SEC_DEBUG_POLICY_H

#define DBG_POLICY_ENABLE_ONLINE_CRASH_DUMPS       0
#define DBG_POLICY_ENABLE_OFFLINE_CRASH_DUMPS      1
#define DBG_POLICY_DISABLE_AUTHENTICATION          2
#define DBG_POLICY_ENABLE_JTAG                     3
#define DBG_POLICY_ENABLE_LOGGING                  4

#define DBG_POLICY_HASH_DIGEST_SIZE_SHA256        32
#define DBG_POLICY_ID_ARRAY_SIZE                  32
#define DBG_POLICY_CERT_ARRAY_SIZE                8

// A policy can apply to a range of msm serial numbers.
// This value limits that range to a default value.
#define DBG_POLICY_SERIAL_NUM_RANGE_LIMIT         1
#define DBG_POLICY_ELF_IMAGE_ID                   0x200
#define DP_ELF_HASH_OFFSET	                  0x1000 // TODO: remove
#define DP_ELF_CONTENT_OFFSET	                  0x3000 // TODO: remove

#define DBG_POLICY_REVISION_NUMBER                1

typedef struct __attribute__((__packed__))  
    {
        char magic[4];
        uint32 size;		
        uint32 revision;		
        uint32 serial_num_start;		
        uint32 serial_num_end;		
        uint32 reserved;		
        union {
        struct {
                uint64 enable_online_crash_dumps   :  1;
                uint64 enable_offline_crash_dumps  :  1;
                uint64 disable_authentication      :  1;
                uint64 enable_jtag                 :  1;
                uint64 enable_logs                 :  1;
                uint64 reserved_bits               : 43; // reserved for QCT
                uint64 oem_reserved_bits           : 16; // reserved for OEM
				}flags;
        uint64 flags_value;
        };
        uint32 image_id_count;
        uint32 image_id_array[DBG_POLICY_ID_ARRAY_SIZE];
        uint32 root_cert_hash_count;
        uint8  root_cert_hash_array[DBG_POLICY_CERT_ARRAY_SIZE][DBG_POLICY_HASH_DIGEST_SIZE_SHA256];
} dbg_policy_t;

#endif
