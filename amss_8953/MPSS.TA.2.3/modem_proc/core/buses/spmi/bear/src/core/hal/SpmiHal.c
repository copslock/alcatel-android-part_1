/**
 * @file:  SpmiHal.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/09/29 21:52:15 $
 * $Header: //components/rel/core.mpss/3.9.1/buses/spmi/bear/src/core/hal/SpmiHal.c#1 $
 * $Change: 9128810 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiHal.h"
#include "SpmiOs.h"

//******************************************************************************
// Global Data
//******************************************************************************

void* pmicArbHwioBase = NULL;

//******************************************************************************
// Public API Functions
//******************************************************************************

Spmi_Result SpmiHal_Init()
{
    if(pmicArbHwioBase != NULL) {
        return SPMI_SUCCESS;
    }

    return SpmiOs_GetPmicArbBaseAddr( &pmicArbHwioBase );
}
