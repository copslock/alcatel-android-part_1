/***********************************************************************
 * fs_signed_img_param.c
 *
 * Parameters needed to verify signed image.
 * Copyright (C) 2011,2013,2016 QUALCOMM Technologies, Inc.
 *
 * When the file system image is signed, there are some parameters
 * needed to verify the authenticity of the image. This file implements
 * the methods for retrieving these parameters.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.9.1/storage/fs_tar/src/fs_signed_img_param.c#2 $ $DateTime: 2016/08/23 05:49:11 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2016-08-22   as    Updating SW_ID for sec tools usage.
2013-01-29   nr    Migrate to new signed image authentication apis.
2011-06-18   wek   Create. Add secure TAR signing/verification.

===========================================================================*/

#include "fs_signed_img_param.h"

#ifndef FS_SIGNED_IMG_ROOT_HASH
  #define FS_SIGNED_IMG_ROOT_HASH \
   {0xb5, 0x3f, 0xb2, 0x3d, 0x19, 0x53, 0xde, 0xcb, \
    0x95, 0x92, 0x8f, 0xe6, 0x57, 0x55, 0x6c, 0xea, \
    0x6e, 0xda, 0xb3, 0x44, 0x4d, 0xc7, 0x08, 0xc0, \
    0x19, 0x05, 0x7c, 0xba, 0xf8, 0xc6, 0x2d, 0x4a}
#endif



#ifndef FS_SIGNED_IMG_MSM_HW_ID
  #define FS_SIGNED_IMG_MSM_HW_ID     0x0000000000000000
#endif

#ifndef FS_SIGNED_IMG_SOFTWARE_ID
  #define FS_SIGNED_IMG_SOFTWARE_ID  0x000000000000001B
#endif


static uint8 fs_root_hash[] = FS_SIGNED_IMG_ROOT_HASH;

uint8* fs_sign_get_root_hash (uint32 *hash_len)
{
  *hash_len = sizeof (fs_root_hash);
  return &fs_root_hash[0];
}

void fs_sign_get_msm_id (uint64 *msm_id)
{
  *msm_id = FS_SIGNED_IMG_MSM_HW_ID;
}

void fs_sign_get_software_version_id (uint64 *sw_version)
{
  *sw_version = FS_SIGNED_IMG_SOFTWARE_ID;
}
