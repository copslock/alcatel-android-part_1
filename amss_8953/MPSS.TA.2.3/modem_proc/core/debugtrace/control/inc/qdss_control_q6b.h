#ifndef QDSS_CONTROL_Q6B_H
#define QDSS_CONTROL_Q6B_H
/**
   @file qdss_control_q6b.h
   QUALCOMM Debug Subsystem (QDSS) controls for debug agent for Q6B.
 */
/*=============================================================================
   Copyright (c) 2015 Qualcomm Technologies, Inc.
   All rights reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
   $Header: //components/rel/core.mpss/3.9.1/debugtrace/control/inc/qdss_control_q6b.h#1 $

   when       who     what, where, why
   --------   ---    -----------------------------------------------------------
   01/26/15   lht    Initial revision.

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include <stdio.h>
#include "comdef.h"
#include "qdss_control.h"

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/
/*=========================================================================
   ETM
 =========================================================================*/
/*-------------------------------------------------------------------------*/
/**
   Get the current state of ETM on Q6B

   @param[out] state  ETM state 0 (disabled) or 1 (enabled)

   @return   QDSS_CONTROL_RVAL_xxx
 */
int qdss_control_q6b_get_etm(uint8 *state);

/*-------------------------------------------------------------------------*/
/**
   Enable or disable ETM on Q6B

   @param[in]  state  0 (disable) or 1 (enable)

   @return   QDSS_CONTROL_RVAL_xxx
 */
int qdss_control_q6b_set_etm(uint8 state);

/*-------------------------------------------------------------------------*/
/**
  @brief Get the ETM's configuration on Q6B

  @param [in ] pget_str       : Pointer to the string of the
                                 configuration being requested.
         [out] presp_str      : Pointer to string for the configuration
                                 setting
         [in ] resp_max_len   : Maximum number of bytes at presp_str.

  @return   QDSS_CONTROL_RVAL_...
 */
int qdss_control_q6b_get_etm_config(const char *pget_str,
                                    char *presp_str,
                                    size_t resp_max_len);

/*-------------------------------------------------------------------------*/
/**
  @brief  Set the ETM's configuration on Q6B

  @param [in ] pset_str : Pointer to the string with the
                           configuration to be set.

  @return   QDSS_CONTROL_RVAL...
 */
int qdss_control_q6b_set_etm_config(const char *pset_str);


#endif /* QDSS_CONTROL_Q6B_H */

