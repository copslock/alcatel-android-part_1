#ifndef QDSS_CTRL_PRIV_H
#define QDSS_CTRL_PRIV_H

/*=============================================================================

FILE:         qdss_ctrl_priv.h

DESCRIPTION:  Private data structures used by the qdss ctrl subsystem

=============================================================================*/
/*=============================================================================
  Copyright (c) 2013 - 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "DALSys.h"
#include "npa.h"
#include "tracer.h"

/*---------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ------------------------------------------------------------------------*/

#define QDSS_CTRL_SUCCESS          0
#define QDSS_CTRL_ERROR            1

/*---------------------------------------------------------------------------
 * Type Declarations
 * ------------------------------------------------------------------------*/

struct qdss_ctrl_s {
   npa_client_handle npa_client;
   DALSYSSyncHandle hSync;
   tracer_client_handle_t hTracer;
   uint32 test_data_count;
   boolean clk_enabled;
   uint8 etm_state;
   uint8 etm_state_rpm;
   npa_client_handle snoc_npa_client;
   npa_client_handle bimc_npa_client;
};

extern struct qdss_ctrl_s qdss_ctrl_ctxt;

#endif /* QDSS_CTRL_PRIV_H */

