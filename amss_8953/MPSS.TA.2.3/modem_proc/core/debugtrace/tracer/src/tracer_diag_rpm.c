/*===========================================================================

  FILE: tracer_diag_rpm.c

  OVERVIEW:     Tracer's interface to diag packet service; proxy for RPM

  DEPENDENCIES:

===========================================================================*/
/*===========================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/
/*===========================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/tracer/src/tracer_diag_rpm.c#1 $
===========================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "diagpkt.h"
#include "diagcmd.h"

#include "tracer_diag.h"
#include "tracer_cfgrpm.h"

/*---------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ------------------------------------------------------------------------*/

/* Modem acts as proxy for RPM */
#define MODEM  1
#if (EVENT_SUBSYSTEM == 1)
   #define TRACER_RPM_DIAG_PROC_ID   QDSSDIAG_PROCESSOR_RPM
#else
   #define TRACER_RPM_DIAG_PROC_ID   0
#endif
#undef MODEM

/*---------------------------------------------------------------------------
 * Type Declarations
 * ------------------------------------------------------------------------*/
PACK(void *) tracer_diag_pkt_rpm_handler_ext(PACK(void *) pRqstPkt, uint16 pkt_len);

/*---------------------------------------------------------------------------
 * Static Variable Definitions
 * ------------------------------------------------------------------------*/

// Diag packet service - callback table.
static const diagpkt_user_table_entry_type tracer_diag_pkt_rpm_tbl[] =
{
   { TRACER_RPM_DIAG_PROC_ID | TRACER_DIAG_CTRL_RPMSWE,  // range: from
     TRACER_RPM_DIAG_PROC_ID | TRACER_DIAG_CTRL_RPMSWE,  // range: to
     tracer_diag_pkt_rpm_handler_ext }                   // handler fcn
};

/*---------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * ------------------------------------------------------------------------*/
#define TRACER_RESP_PKT_SET(_result) \
            pRespPkt = (tracer_diag_pkt_resp *) diagpkt_subsys_alloc( \
               DIAG_SUBSYS_QDSS, \
               pHdrPkt->subsysCmdCode, \
               sizeof(tracer_diag_pkt_resp)); \
            if (NULL != pRespPkt) \
            { \
               pRespPkt->result = _result; \
            }

#define TRACER_ALLOCDIAGRESP(_resp_t) \
           (_resp_t *) diagpkt_subsys_alloc(DIAG_SUBSYS_QDSS, \
           pHdrPkt->subsysCmdCode, sizeof(_resp_t))

/*-------------------------------------------------------------------------*/
PACK(void *) tracer_diag_pkt_rpm_handler_ext(PACK(void *) pRqstPkt,
                                             uint16 pkt_len)
{
   tracer_diag_pkt_hdr *pHdrPkt;
   tracer_diag_pkt_resp *pRespPkt = NULL;
   PACK(void *)pResp = NULL;
   tracer_cmdresp_t trc_ret;

   if (NULL != pRqstPkt)
   {
      pHdrPkt = (tracer_diag_pkt_hdr *)pRqstPkt;

      switch (pHdrPkt->subsysCmdCode & 0x0FF)
      {
         case TRACER_DIAG_CTRL_RPMSWE:
            if (pkt_len < sizeof(tracer_diag_rpmswe_ctrl))
            {
               pResp = diagpkt_err_rsp(DIAG_BAD_LEN_F, pRqstPkt, pkt_len);
            }
            else
            {
               trc_ret = tracer_cfgrpm_swe(
                  (uint32)(((tracer_diag_rpmswe_ctrl *)pRqstPkt)->sink),
                  (uint32)(((tracer_diag_rpmswe_ctrl *)pRqstPkt)->evtGroup),
                  ((tracer_diag_rpmswe_ctrl *)pRqstPkt)->evtBitmask
                  );
               TRACER_RESP_PKT_SET(trc_ret);
               pResp = (void *)pRespPkt;
            }
            break;

         default:
            pResp = diagpkt_err_rsp(DIAG_BAD_CMD_F, pRqstPkt, pkt_len);
         break;
      }

      if (NULL != pResp)
      {
         diagpkt_commit(pResp);
         pResp = NULL;
      }
   }
   return (pResp);
}

/*---------------------------------------------------------------------------
 * Externalized Function Definitions
 * ------------------------------------------------------------------------*/

// Initialize interface with diag packet service
void tracer_diag_rpm_init(void)
{
#if TRACER_RPM_DIAG_PROC_ID > 0
  DIAGPKT_DISPATCH_TABLE_REGISTER (
     (diagpkt_subsys_id_type) DIAG_SUBSYS_QDSS, tracer_diag_pkt_rpm_tbl);
#endif
  return;
}

