#ifndef QDSS_QMI_CTRL_SVC_Q6B_H
#define QDSS_QMI_CTRL_SVC_Q6B_H
/*

  File: qdss_qmi_service_q6b.h

  QDSS QMI Control Service for Q6B (proxy for second modem)

*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/qmi/ctrlsvc_q6b/src/qdss_qmi_ctrl_svc_q6b.h#1 $

  when       who     what, where, why
  --------   ---    -----------------------------------------------------------
  01/26/15   lht    Initial release

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qmi_csi.h"
#include "qmi_csi_target_ext.h"

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/
qmi_csi_service_handle qdss_qmi_ctrl_svc_q6b_init(qmi_csi_os_params *os_params);


#endif /* QDSS_QMI_CTRL_SVC_Q6B_H */

