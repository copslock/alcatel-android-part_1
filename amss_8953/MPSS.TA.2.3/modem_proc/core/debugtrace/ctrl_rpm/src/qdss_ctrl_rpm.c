/*=============================================================================

FILE:         qdss_ctrl_rpm.c

DESCRIPTION:  Implements handlers for diag commands to configure qdss on
              RPM
=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_rpm/src/qdss_ctrl_rpm.c#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qdss_control.h"
#include "qdss_ctrl_rpm_diag.h"

/*---------------------------------------------------------------------------
 * Externalized Function Definitions
 * ------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/

/**
  @brief  Handles the QDSS_CTRL_FILTER_ETM diag command

  @return 0 if successful, error code otherwise
 */
int qdss_ctrl_filter_etm_rpm2_handler(qdss_ctrl_filter_etm_req *pReq,
                                      int req_len,
                                      qdss_ctrl_filter_etm_rsp *pRsp,
                                      int rsp_len)
{
   int nErr = QDSS_CONTROL_RVAL_UNKNOWN_ERR;

   nErr = qdss_control_set_etm_rpm(pReq->state);
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
/**
  @brief  Handles the QDSS_CTRL_FILTER_ETM_RPM diag command

  @return 0 if successful, error code otherwise
 */
int qdss_ctrl_filter_etm_rpm_rpm2_handler(qdss_ctrl_filter_etm_rpm_req *pReq,
                                          int req_len,
                                          qdss_ctrl_filter_etm_rpm_rsp *pRsp,
                                          int rsp_len)
{
   int nErr = QDSS_CONTROL_RVAL_UNKNOWN_ERR;

   nErr = qdss_control_set_etm_rpm(pReq->state);
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
/**
  @brief Initializes the QDSS control subsystem.

  This is called from RC init.

  @return None
 */
void qdss_ctrl_rpm_init(void)
{
   qdss_ctrl_rpm_diag_init();
}

