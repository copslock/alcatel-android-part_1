/*=============================================================================

FILE:         qdss_ctrl_rpm_diag.c

DESCRIPTION:

=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_rpm/src/qdss_ctrl_rpm_diag.c#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qdss_ctrl_rpm_diag.h"

/*---------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * ------------------------------------------------------------------------*/

#define QDSS_HANDLE_DIAG_CMD(cmd)                              \
   if (pkt_len < sizeof(cmd##_req)) {                          \
      pRsp = diagpkt_err_rsp(DIAG_BAD_LEN_F, pReq, pkt_len);   \
   }                                                           \
   else {                                                      \
      pRsp =  diagpkt_subsys_alloc(DIAG_SUBSYS_QDSS,           \
                                   pHdr->subsysCmdCode,        \
                                   sizeof(cmd##_rsp));         \
      if (NULL != pRsp) {                                      \
         cmd##_rpm2_handler((cmd##_req *)pReq,                 \
                            pkt_len,                           \
                            (cmd##_rsp *)pRsp,                 \
                            sizeof(cmd##_rsp));                \
      }                                                        \
   }


/*---------------------------------------------------------------------------
 * Externalized Function Definitions
 * ------------------------------------------------------------------------*/

PACK(void *) qdss_ctrl_rpm_diag_pkt_handler(PACK(void *) pReq, uint16 pkt_len)
{
   qdss_ctrl_diag_pkt_hdr *pHdr;
   PACK(void *)pRsp = NULL;

   if (NULL != pReq)    {
      pHdr = (qdss_ctrl_diag_pkt_hdr*)pReq;

      switch (pHdr->subsysCmdCode & 0x0FF) {

      case QDSS_CTRL_FILTER_ETM:
         QDSS_HANDLE_DIAG_CMD(qdss_ctrl_filter_etm);
         break;

      case QDSS_CTRL_FILTER_ETM_RPM:
         QDSS_HANDLE_DIAG_CMD(qdss_ctrl_filter_etm_rpm);
         break;

      default:
         pRsp = diagpkt_err_rsp(DIAG_BAD_CMD_F, pReq, pkt_len);
         break;
      }

      if (NULL != pRsp) {
         diagpkt_commit(pRsp);
         pRsp = NULL;
      }
   }
   return (pRsp);
}

/*-------------------------------------------------------------------------*/
// Diag packet service - callback tables.
static const diagpkt_user_table_entry_type qdss_ctrl_rpm_diag_pkt_tbl[] =
   {
      { QDSS_CTRL_RPM_DIAG_PROC_ID | QDSS_CTRL_FILTER_ETM, // range: from
        QDSS_CTRL_RPM_DIAG_PROC_ID | QDSS_CTRL_FILTER_ETM, // range: to
        qdss_ctrl_rpm_diag_pkt_handler },                  // handler fcn

      { QDSS_CTRL_RPM_DIAG_PROC_ID | QDSS_CTRL_FILTER_ETM_RPM, // range: from
        QDSS_CTRL_RPM_DIAG_PROC_ID | QDSS_CTRL_FILTER_ETM_RPM, // range: to
        qdss_ctrl_rpm_diag_pkt_handler }                       // handler fcn

   };

/*-------------------------------------------------------------------------*/
// Initialize interface with diag packet service
void qdss_ctrl_rpm_diag_init(void)
{
#if QDSS_CTRL_RPM_DIAG_PROC_ID > 0
  DIAGPKT_DISPATCH_TABLE_REGISTER (
     (diagpkt_subsys_id_type) DIAG_SUBSYS_QDSS, qdss_ctrl_rpm_diag_pkt_tbl);
#endif
  return;
}

