#ifndef QDSS_CTRL_RPM_DIAG_H
#define QDSS_CTRL_RPM_DIAG_H

/*=============================================================================

FILE:         qdss_ctrl_rpm_diag.h

DESCRIPTION:

=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_rpm/src/qdss_ctrl_rpm_diag.h#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/
#include "qdss_ctrl_diag.h"

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/

int qdss_ctrl_filter_etm_rpm2_handler(qdss_ctrl_filter_etm_req *pReq,
                                      int req_len,
                                      qdss_ctrl_filter_etm_rsp *pRsp,
                                      int rsp_len);

int qdss_ctrl_filter_etm_rpm_rpm2_handler(qdss_ctrl_filter_etm_rpm_req *pReq,
                                          int req_len,
                                          qdss_ctrl_filter_etm_rpm_rsp *pRsp,
                                          int rsp_len);

/* ----------------------------------------------------------------------- */

void qdss_ctrl_rpm_diag_init(void);

#endif /* QDSS_CTRL_RPM_DIAG_H */

