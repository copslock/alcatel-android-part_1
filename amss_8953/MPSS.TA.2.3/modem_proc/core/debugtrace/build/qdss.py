#=================================================================================
#  File: qdss.py
#
#  Python Script for debugtrace
#
#  Copyright (c) 2015 Qualcomm Technologies, Inc.
#  All rights reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#---------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.9.1/debugtrace/build/qdss.py#1 $
#
#=================================================================================

# Templates for calls:
# qdss.BuildTags(['rpm', 'apps', 'modem', 'adsp', 'wcn', 'sensor', 'sensor2', 'audio', 'venus', 'gss', 'vpu'])
# qdss.BuildTags(['modem', 'adsp', 'wcn'])
def BuildTags(image_list):
    # List of build tags for image_tags keys given in image_list
    tag_names = []
    image_tags = {
        'rpm'          : ['RPM_IMAGE', 'RPM', 'rpm'],
        'apps'         : ['APPS_PROC', 'APPS_IMAGE', 'CBSP_APPS_IMAGE'],
        'modem'        : ['CORE_MODEM', 'MODEM_IMAGE', 'CBSP_MODEM_IMAGE'],
        'adsp'         : ['CORE_ADSP', 'CORE_USER_PD'], #'AVS_ADSP'?
        'wcn'          : ['CORE_WCN', 'WCN_IMAGE', 'CBSP_WCN_IMAGE'],
        'gss'          : ['CORE_GSS'],
        'sensor'       : ['CORE_SLPI_ROOT'],
        'audio'        : ['CORE_AVS_ADSP_USER','CORE_QDSP6_AUDIO_PD'],
        'sensor2'      : ['CORE_SSC_ADSP_USER', 'CORE_SSC_SLPI_USER', 'CORE_QDSP6_SENSOR_PD'],
        'venus'        : ['VENUS_VSS'],
        'vpu'          : ['VPU-TBD'],
        }
    for ii in image_list:
        image_key = ii
        tag_names.extend(image_tags[image_key])
    return tag_names


