#ifndef QDSS_CTRL_Q6B_ETM_H
#define QDSS_CTRL_Q6B_ETM_H
/**
   @file qdss_ctrl_q6b_etm.h

   QUALCOMM Debug Subsystem (QDSS) controls (acting proxy) for Q6B's ETM

 */
/*=============================================================================
   Copyright (c) 2015 Qualcomm Technologies, Inc.
   All rights reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
   $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_q6b/src/qdss_ctrl_q6b_etm.h#1 $

   when       who     what, where, why
   --------   ---    -----------------------------------------------------------
   01/26/15   lht    Initial revision.

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include <stdio.h>
#include "comdef.h"

/*---------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ------------------------------------------------------------------------*/

// ETM configurable parameter identifiers.
#define QCTRL_Q6B_PARAM_ID_DSP_ETM_NONE    0
#define QCTRL_Q6B_PARAM_ID_DSP_ETM_MODE    1
#define QCTRL_Q6B_PARAM_ID_DSP_ETM_ROUTE   2
#define QCTRL_Q6B_PARAM_ID_DSP_ETM_FILTER  3
#define QCTRL_Q6B_PARAM_ID_DSP_ETM_UNKNOWN 0xFF

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/

void qdss_ctrl_q6b_etm_init(void);

int qdss_ctrl_q6b_etm_get_param(uint16 param_id, uint32 *pval);
int qdss_ctrl_q6b_etm_set_param(uint16 param_id, uint32 val);

int qdss_ctrl_q6b_etm_get_config(const char *pget_str,
                                 char *presp_str,
                                 size_t resp_max_len);
int qdss_ctrl_q6b_etm_set_config(const char *pset_str);

/** Enable the ETM. */
int qdss_ctrl_q6b_etm_enable(void);

/** Disabled the ETM. */
int qdss_ctrl_q6b_etm_disable(void);


#endif /* QDSS_CTRL_Q6B_ETM_H */
