/*=============================================================================

FILE:         qdss_ctrl_q6b.c

DESCRIPTION:  Implements handlers for diag commands to configure qdss on
              Q6B.
=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_q6b/src/qdss_ctrl_q6b.c#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "ClockDefs.h"

#include "qdss_q6b_control.h"
#include "qdss_control_q6b.h"
#include "qdss_ctrl_q6b_diag.h"
#include "qdss_ctrl_q6b_etm.h"
#include "qdss_ctrl_priv.h"

/*---------------------------------------------------------------------------
 * Static Variable Definitions
 * ------------------------------------------------------------------------*/

struct qdss_ctrl_s qdss_ctrl_q6b_ctxt;

/*---------------------------------------------------------------------------
 * Function Definitions
 * ------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/**
  @brief  Handles the QDSS_CTRL_FILTER_ETM diag command

  @return 0 if successful, error code otherwise
 */
int qdss_ctrl_filter_etm_q6b_handler(qdss_ctrl_filter_etm_req *pReq,
                                     int req_len,
                                     qdss_ctrl_filter_etm_rsp *pRsp,
                                     int rsp_len)
{
   int nErr = QDSS_CONTROL_RVAL_UNKNOWN_ERR;

   nErr = qdss_control_q6b_set_etm(pReq->state);
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
int qdss_control_q6b_get_etm(uint8 *state)
{
   *state = qdss_ctrl_q6b_ctxt.etm_state;
   return QDSS_CONTROL_RVAL_SUCCESS;
}

/*-------------------------------------------------------------------------*/
int qdss_control_q6b_set_etm(uint8 state)
{
   int rval = QDSS_CONTROL_RVAL_UNKNOWN_ERR;

   if ((0 == qdss_ctrl_q6b_ctxt.hSync) || (0 == qdss_ctrl_q6b_ctxt.npa_client))
   {
      return QDSS_CONTROL_RVAL_SYNCH_ERR;
   }

   DALSYS_SyncEnter(qdss_ctrl_q6b_ctxt.hSync);

   if (state)
   {
      if (!qdss_ctrl_q6b_ctxt.clk_enabled)
      {
         npa_issue_required_request(qdss_ctrl_q6b_ctxt.npa_client,
                                    CLOCK_QDSS_LEVEL_DEBUG);
         qdss_ctrl_q6b_ctxt.clk_enabled = TRUE;
      }

      /* State: D0 bit serves the enable or disable ETM,
         D1 - D7 bits serves ETM configuration mode */
      if (1 < state)    // Request from Diag.
      {  // State right shifted 1 time to get the mode value.
         qdss_ctrl_q6b_etm_set_param(QCTRL_Q6B_PARAM_ID_DSP_ETM_MODE,
                                     (unsigned int)state >> 1);
      }  // else simple request from QMI/Debug Agent or Diag
         // with the default mode.
      rval = qdss_ctrl_q6b_etm_enable();
   } else
   {
      if (qdss_ctrl_q6b_ctxt.clk_enabled)
      {
         rval = qdss_ctrl_q6b_etm_disable();
         qdss_ctrl_q6b_ctxt.clk_enabled = FALSE;
         npa_issue_required_request(qdss_ctrl_q6b_ctxt.npa_client,
                                    CLOCK_QDSS_LEVEL_OFF);
      } else
      {
         rval = QDSS_CONTROL_RVAL_SUCCESS;
      }
   }

   if (QDSS_CONTROL_RVAL_SUCCESS == rval)
   {
      qdss_ctrl_q6b_ctxt.etm_state = state;
   }

   DALSYS_SyncLeave(qdss_ctrl_q6b_ctxt.hSync);
   return rval;
}

/*-------------------------------------------------------------------------*/
// Debug Agent QMI QDSSC message
int qdss_control_q6b_get_etm_config(const char *pget_str,
                                    char *presp_str,
                                    size_t resp_max_len)
{
   return qdss_ctrl_q6b_etm_get_config(pget_str,
                                       presp_str,
                                       resp_max_len);
}

/*-------------------------------------------------------------------------*/
// Debug Agent QMI QDSSC message
int qdss_control_q6b_set_etm_config(const char *pset_str)
{
   return qdss_ctrl_q6b_etm_set_config(pset_str);
}

/*-------------------------------------------------------------------------*/
/**
  @brief  Handles the QDSS_CTRL_ETM_GET_PARAM diag command

  @return 0 if successful, error code otherwise
 */
int qdss_ctrl_etm_get_param_q6b_handler(qdss_ctrl_etm_get_param_req *pReq,
                                        int req_len,
                                        qdss_ctrl_etm_get_param_rsp *pRsp,
                                        int rsp_len)
{
   int nErr = QDSS_CONTROL_RVAL_UNKNOWN_ERR;;
   uint32 getval;
   pRsp->config_id = pReq->config_id;
   pRsp->param_id = pReq->param_id;
   pRsp->resv = 0;
   if (QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_ID != pReq->config_id)
   {
      nErr = QDSS_CONTROL_RVAL_INVALID_ARG;
      pRsp->val = 0;
   } else
   {
      nErr = qdss_ctrl_q6b_etm_get_param(pReq->param_id, &getval);
      pRsp->val = getval;
   }
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
/**
   @brief  Handles the QDSS_CTRL_SET_ETM_PARAM diag command

   @return 0 if successful, error code otherwise
 */
int qdss_ctrl_etm_set_param_q6b_handler(qdss_ctrl_etm_set_param_req *pReq,
                                        int req_len,
                                        qdss_ctrl_etm_set_param_rsp *pRsp,
                                        int rsp_len)
{
   int nErr = QDSS_CONTROL_RVAL_UNKNOWN_ERR;
   pRsp->config_id = pReq->config_id;
   pRsp->param_id = pReq->param_id;
   if (QDSS_Q6B_CTRL_ETM_CONFIG_FORMAT_ID != pReq->config_id)
   {
      nErr = QDSS_CONTROL_RVAL_INVALID_ARG;
   } else
   {
      nErr = qdss_ctrl_q6b_etm_set_param(pReq->param_id, pReq->val);
   }
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
int qdss_ctrl_filter_etm_rpm_q6b_handler(qdss_ctrl_filter_etm_rpm_req *pReq,
                                         int req_len,
                                         qdss_ctrl_filter_etm_rpm_rsp *pRsp,
                                         int rsp_len)
{
   int rval = QDSS_CONTROL_RVAL_NOT_SUPPORTED;
   pRsp->result = rval;
   return rval;
}
/*-------------------------------------------------------------------------*/
/**
  @brief Initializes the QDSS control subsystem.

  This is called from RC init.

  @return None
 */
void qdss_ctrl_q6b_init(void)
{
   int nErr = 1;

   qdss_ctrl_q6b_etm_init();
   qdss_ctrl_q6b_diag_init();

   qdss_ctrl_q6b_ctxt.hSync = 0;
   qdss_ctrl_q6b_ctxt.hTracer = 0;
   qdss_ctrl_q6b_ctxt.test_data_count = 0;
   qdss_ctrl_q6b_ctxt.clk_enabled = FALSE;
   qdss_ctrl_q6b_ctxt.etm_state = 0;
   qdss_ctrl_q6b_ctxt.etm_state_rpm = 0;
   qdss_ctrl_q6b_ctxt.snoc_npa_client = 0;
   qdss_ctrl_q6b_ctxt.bimc_npa_client = 0;

   nErr = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                            &qdss_ctrl_q6b_ctxt.hSync, NULL);
   if (0 == nErr)
   {
      qdss_ctrl_q6b_ctxt.npa_client =
         npa_create_sync_client("/clk/qdss", "qdss", NPA_CLIENT_REQUIRED);
   }
   return;
}

