#===============================================================================
#
# LTE QMI Scons
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/lte.mpss/6.4/qmi/build/lte_qmi.scons#1 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 08/03/15   psv     Initial version.
#===============================================================================
Import('env')

# Enable warnings -> errors for all, except LLVM toolchain (6.x.x) during migration
import os
if not os.environ.get('HEXAGON_RTOS_RELEASE').startswith('6'):
	env = env.Clone(HEXAGONCC_WARN = "${HEXAGONCC_WARN} -Werror")
	env = env.Clone(HEXAGONCXX_WARN = "${HEXAGONCCXX_WARN} -Werror")

from glob import glob
from os.path import join, basename

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_LTE_RRC",
])

#-------------------------------------------------------------------------------
# Necessary Public and Restricted API's
#-------------------------------------------------------------------------------
env.RequirePublicApi([
    'HWENGINES',
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'DAL',
    'POWER',
    'BUSES',
    'MPROC',
    'KERNEL',                             # needs to be last 
    ], area='core')

env.RequirePublicApi([
        'PROSE',
        ],
        area='UTILS')

env.RequirePublicApi(['LTE_QMI'], area='QMIMSGS')

# Need to get access to LTE protected headers
env.RequireProtectedApi([
    'LTE',
    ])


env.RequirePublicApi('QTF', area='mob') 
#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
LTE_QMI_SOURCES = ['${BUILDPATH}/' + basename(fname)
                   for fname in glob(join(env.subst(SRCPATH), '*.c'))]

import re

# STM Files
for stmname in glob(join(env.subst(SRCPATH), '*.stm')):
    match = re.search('([^\\\/]*)\.stm', stmname)
    match.group(1)
    env.STM2('${BUILDPATH}/__' + match.group(1) + '_int.c',
             '${BUILDPATH}/' + match.group(1) + '.stm')
    LTE_QMI_SOURCES.append('${BUILDPATH}/__' + match.group(1) + '_int.c')

env.Append(CPPPATH = '${BUILDPATH}')
env.Append(CPPPATH = '${LTE_ROOT}/qmi/build/${BUILDPATH}')

# Compile the LTE QMI source and convert to a binary library
env.AddBinaryLibrary(['MODEM_MODEM', 'MOB_LTE'], '${BUILDPATH}/lte_qmi', LTE_QMI_SOURCES, pack_exception=['USES_COMPILE_LTE_L3_L2_PROTECTED_LIBS', 'USES_FEATURE_LTE_PACKBUILD_GENERATE_PROTECTED_LIBS', 'USES_CUST_1'])


#-------------------------------------------------------------------------------
# avoid run-time errors when trying to send MSGR messages 
#-------------------------------------------------------------------------------


env.LoadSoftwareUnits()
