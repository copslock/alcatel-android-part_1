#ifndef LTE_QMI_TASK_H
#define LTE_QMI_TASK_H
/*==============================================================================

                LTE_QMI_TASK.H

DESCRIPTION
  This file contains the declarations needed in LTE_QMI Task.

Copyright (c) 2013 - 2015 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
==============================================================================*/

/*==============================================================================

  $Header: 
  $DateTime: 
  $Author: 
 
  when        who    what, where, why
  --------    ---    -------------------------------------------------------------
  07/18/15    psv     created file.
==============================================================================*/

/*==============================================================================

                          INCLUDE FILES FOR MODULE

==============================================================================*/

#include <comdef.h>
#include <pthread.h>
#include <msgr.h>
#include <lte_assert.h>

/*==============================================================================

                           INTERNAL TYPE DEFINITIONS

==============================================================================*/

   


/*===========================================================================

  FUNCTION:  lte_qmi_task_init

===========================================================================*/
/*!
    @brief
    This function is a initialization function for the LTE QMI Task.

    @detail
    Initialization consists of:
    - Initialization LTE QMI control attributes
    - Initialization stack size and priority
    - Starting the LTE QMI Task

    @return
    Thread ID

    @note

    @see related_function()

*/
/*=========================================================================*/
EXTERN pthread_t lte_qmi_task_init
( 
  int priority  /*!< thread priority passed in by app mgr */
);


#endif /* LTE_QMI_TASK_H */
