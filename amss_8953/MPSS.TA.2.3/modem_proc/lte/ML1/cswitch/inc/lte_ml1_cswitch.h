/*@file
lte_ml1_tunemgr.h

@brief
This module contains the external functions of RF Manager

@detail
This module contains the external functions of RF Manager

*/

/*===========================================================================

  Copyright (c) 2010-2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

  EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/17/16   ymu     Extend Cswitch RF tuning estimation time
10/06/15   bm      created
===========================================================================*/

#ifndef LTE_ML1_CSWITCH_H
#define LTE_ML1_CSWITCH_H

/*===========================================================================

  INCLUDE FILES

===========================================================================*/

#include "lte_variation.h"
#include "intf_sys.h"
#include "intf_async.h"
#include "lte_ml1_comdef.h"
#include "lte_ml1_common.h"
#include <lte_ml1_log.h>
#include "lte_cphy_msg.h"
#include <lte_ml1_mem.h>
#include "lte_ml1_tunemgr.h"
#include "lte_ml1_tunemgr_intf.h"
#include "lte_ml1_dlm_types.h"

#ifdef FEATURE_CSWITCH_MODULE

/*===========================================================================

                   Data types

===========================================================================*/

/* Time uinits are in milli sec */
#define LTE_ML1_CSWITCH_ASAP_SCHED_MARGIN_TIME  2
#define LTE_ML1_CSWITCH_TRM_ACQ_TIME            2
#define LTE_ML1_CSWITCH_TUNE_TIME_RX_ONLY       15
#define LTE_ML1_CSWITCH_TUNE_TIME_TX_INC        28
#define LTE_ML1_CSWITCH_DL_RESUME_TIME          2
#define LTE_ML1_CSWITCH_CHEST_START_TIME        2
#define LTE_ML1_CSWITCH_CHEST_STOP_TIME         4
#define LTE_ML1_CSWITCH_CHEST_TIME              (LTE_ML1_CSWITCH_CHEST_STOP_TIME +\
                                                 LTE_ML1_CSWITCH_CHEST_START_TIME)

/* The below margin times are worst case margins that CSWITCH module
 * might need to perform its operations (depending on requested 
 * operation type) before calling client's cswitch_done_cb at start
 * of client usable time */

/* Note that use of the below macros is optional for client, it may
 * send the request any time it know that a request needs to be 
 * scheduled some time in the future; cswitch module with schedule
 * an ASAP request during lte_ml1_cswitch_sched_req() if it
 * does not have enough margin to try and finish operations before
 * worst case start time, otherwise, if there is enough margin, 
 * cswitch will schedule a timed request (with a calculated
 * start SFN).
 * */
/* To be used with lte_ml1_cswitch_req_init_params_for_tune() API */
#define LTE_ML1_CSWITCH_TUNE_REQ_MARGIN_TIME  (LTE_ML1_CSWITCH_ASAP_SCHED_MARGIN_TIME + LTE_ML1_CSWITCH_TRM_ACQ_TIME + \
                                               LTE_ML1_CSWITCH_TUNE_TIME_TX_INC )

/* To be used with lte_ml1_cswitch_req_init_params_for_cell_switch() API */
#define LTE_ML1_CSWITCH_CELL_SWITCH_REQ_MARGIN_TIME  (LTE_ML1_CSWITCH_ASAP_SCHED_MARGIN_TIME + LTE_ML1_CSWITCH_TRM_ACQ_TIME + \
                             LTE_ML1_CSWITCH_TUNE_TIME_TX_INC + LTE_ML1_CSWITCH_DL_RESUME_TIME +\
                             LTE_ML1_CSWITCH_CHEST_TIME)

/* To be used with lte_ml1_cswitch_req_init_params_for_cell_switch() API */
#define LTE_ML1_CSWITCH_CELL_SWITCH_REQ_MARGIN_TIME_NO_CHEST  (LTE_ML1_CSWITCH_ASAP_SCHED_MARGIN_TIME + LTE_ML1_CSWITCH_TRM_ACQ_TIME + \
                             LTE_ML1_CSWITCH_TUNE_TIME_TX_INC + LTE_ML1_CSWITCH_DL_RESUME_TIME)

/* Maximum number of carriers that CSWITCH module can tune */
#define LTE_ML1_CSWITCH_MAX_CC LTE_ML1_RFMGR_MAX_CC

/*! Handle to be used by client for any cswitch operation on request
 * already queued with cswitch module */
typedef struct lte_ml1_cswitch_int_req_s* lte_ml1_cswitch_handle_type;

typedef enum
{
  LTE_ML1_CSWITCH_STATUS_UNINITIALIZED,
  LTE_ML1_CSWITCH_STATUS_SUCCESS,
  LTE_ML1_CSWITCH_STATUS_INVALID_PARAM,
  LTE_ML1_CSWITCH_STATUS_NOT_SUPPORTED,
  LTE_ML1_CSWITCH_STATUS_RESOURCE_BUSY,
  LTE_ML1_CSWITCH_STATUS_ERROR
}lte_ml1_cswitch_status_type;

/*! Client provided timings on when to start/stop the cell switch procedure */
typedef struct
{
  /*! Start time of the operation (usable time) in rtc */
  uint64 start_time_rtc;

  /*! Worst case start time, if trigger happens after worst_case_start_time
      the schedule will be aborted. Value in rtc
   */
  uint64 worst_case_start_time_rtc;

  /* Min usable time for this operation. Used for min activation window
   * for scheduler to do arbitration and conflict detection with other
   * objects. Value in rtc */
  uint64 min_usable_time_rtc;

}lte_ml1_cswitch_timing_info_type;


typedef enum
{
  LTE_ML1_CSWITCH_PROC_STATUS_STARTED,
  /*! aborted due to start callback arriving after worst_case_start_time */
  LTE_ML1_CSWITCH_PROC_STATUS_ABORTED_MISSED_TIME,
  /*! aborted due to scheduler aborting the cswitch obj */
  LTE_ML1_CSWITCH_PROC_STATUS_ABORTED_SCHED_ABORT,
  LTE_ML1_CSWITCH_PROC_STATUS_STOPPED
}lte_ml1_cswitch_proc_status_type;

typedef void (*lte_ml1_cswitch_cb_type)(lte_ml1_cswitch_handle_type handle, void *user_data);

/*! Callbacks to be registered by the client with cswitch module */
typedef struct 
{
  /* Cell switch procedure is finished (cswitch/freq_tune done) */
  lte_ml1_cswitch_cb_type switch_done;

  /* cell switch procedure finished, client may proceed with new
   * tune params set */
  lte_ml1_cswitch_cb_type switch_back_done;

  /* Cell switch procedure got aborted due to some reason */  
  lte_ml1_cswitch_cb_type abort;

  /* Cell switch procedure got cancelled by client and cswitch module
   * has done handling the cancel request */  
  lte_ml1_cswitch_cb_type cancel_cnf;  
}lte_ml1_cswitch_cb_info_type;

typedef enum
{
  LTE_ML1_CSWITCH_OP_CELL_SWITCH,
  LTE_ML1_CSWITCH_OP_TUNE_ONLY,
  LTE_ML1_CSWITCH_OP_TRM_ACQ_ONLY 
}lte_ml1_cswitch_operation_type;


/*! Structure that contains freq. related info - freq and bw */
typedef struct 
{
  /* freq/bandwidth */
  lte_earfcn_t                  freq;
  lte_bandwidth_idx_e           bw;
}lte_ml1_cswitch_tune_info_type;

/*! Structure that contains cell related info - rx freq, bw, cell_id */
typedef struct 
{
  /* Rx freq/bandwidth */
  lte_earfcn_t                  freq;
  lte_bandwidth_idx_e           bw;
  /* target Cell ID  */
  lte_phy_cell_id_t             cell_id;  
}lte_ml1_cswitch_cell_info_type;

/* Freq info to which to tune */
typedef struct
{
  /* magic_number for this structure */
  uint32                        magic_number;
  /*! Requested chain bitmask (see 
   * LTE_ML1_RFMGR_TRM_RX_BOTH/LTE_ML1_RFMGR_TRM_TX_MASK ) */
  lte_ml1_trm_rfmgr_res_mask_t  required_res;

  /* RF devices for the requested carrier; will be filled in by
   * cswitch module itself */
  rfm_device_enum_type          target_dev[LTE_ML1_RFMGR_TRM_CHAIN_MAX];
  
  /* Rx freq/bandwidth */
  lte_earfcn_t                  rx_freq;
  lte_bandwidth_idx_e           rx_bw;

  /* Tx freq/bandwidth */
  lte_earfcn_t                  tx_freq;
  lte_bandwidth_idx_e           tx_bw;

  /* target Cell ID - optional (required for cell switch) 
   * (not needed for tune) */
  lte_phy_cell_id_t             cell_id;
}lte_ml1_cswitch_freq_req_type;




/*
                                          trigger_switch_back
                                                    +
                                                    |
                                                    v

           +-----------------------------------------------------------+
           |             |          |               |                  |
           | cell switch | warmup   | Usable time   | cell switch (back|
           | (tune/slam/ |          | (for client)  | to orig values)  |
           |  dl dis/en) |          |               |                  |
           +-----------------------------------------------------------+
           ^                        ^                                  ^
           |                        |                                  |
           |                        |                                  |
           +                        +                                  +
       schdlr obj               switch_done                      switch_back_done
       start cb

*/

typedef struct 
{
  boolean clock_change;
  lte_ml1_mcpm_req_s mcpm_req;
}lte_ml1_cswitch_clock_info_type;

typedef struct 
{
  /* Param used internally and set when client has initialized
   * the params with default settings */
  uint32                            magic_number;
 
  /*! Type of client making the request */
  lte_ml1_cswitch_client_type       client;

  /* TRM priority required for LTE-D clients to specify with what
   * priority should cswitch module request the TRM chains for LTE-D */
  trm_reason_enum_t                 trm_priority;

  /* Whether we need to wait for channel estimation before invoking
   * start cb to client */
  boolean                           chest_covered;

  /*! Scheduling info */
  lte_ml1_cswitch_timing_info_type  timing_info;

  /* Callback pointers - from cswitch back to client */
  lte_ml1_cswitch_cb_info_type      cbs;

  /* Object to be used to protect tune/cell switch operation -
   * this is to be specified by the client based on client
   * decided friendliness and priority */
  lte_ml1_schdlr_obj_id_e           sched_obj_id;
  lte_ml1_schdlr_priority_e         sched_obj_priority;

  /*************************************************************/
  /* Following params should NOT be set be client directly but
   * only by using APIs exported by cswitch module to set them */

  /*! freq info */
  lte_ml1_cswitch_freq_req_type     freq_info[LTE_ML1_CSWITCH_MAX_CC];

  /* target cell params */
  lte_ml1_rat_lte_cell_struct_s     target_cell;

  /* type of operation CSWITCH/TUNE-only */
  lte_ml1_cswitch_operation_type    op_type;  

  /* Client needs to info if clock bump up is needed */
  lte_ml1_cswitch_clock_info_type   clock_info;
}lte_ml1_cswitch_req_type;
  
/*===========================================================================

  FUNCTION DEFINITION

===========================================================================*/

/******************** Called internally from ML1 ***************************/

/*===========================================================================

  lte_ml1_cswitch_init

===========================================================================*/
/*!
    @brief
    This function initialize cswitch Module. This function is only supposed to
    be called at the powerup.

    @detail
    cswitch init

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_init
(
  lte_mem_instance_type instance,
  msgr_client_t         *client_ptr,  /*!< Message router client pointer   */
  msgr_id_t             mq_id         /*!< Lower layer message queue       */
);

/*===========================================================================

  lte_ml1_cswitch_deinit

===========================================================================*/
/*!
    @brief
    This function de-initializes cswitch Module. This function is called when
    ML1 stacks is being de-inited

    @detail
    cswitch init

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_deinit
(
  lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_cswitch_handle_msgs

===========================================================================*/
/*!
    @brief
    Handles the message received from ML1 manager

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_cswitch_handle_msgs
(
  lte_mem_instance_type instance,
  /* Message contents */
  msgr_hdr_s       *msg_ptr,
  /* Message length */
  uint32           msg_len
);

/************************* Client API **************************************/

/*===========================================================================

  lte_ml1_cswitch_clock_req_init

===========================================================================*/
/*!
  @brief
  This function initializes the client provided request structure 
  (lte_ml1_cswitch_req_type) with default params. This must be called before
  call to lte_ml1_cswitch_sched_req()

  @detail
  cswitch request init

  @return
  none

*/
/*=========================================================================*/
void lte_ml1_cswitch_clock_req_init
(
 lte_mem_instance_type    instance,
 lte_ml1_cswitch_req_type *req,
 uint8 max_prb_num,
 uint8 num_rx,
 uint8 num_tx,
 lte_l1_tdd_ul_dl_cfg_index_e subframe_assign
);

/*===========================================================================

  lte_ml1_cswitch_sched_req_init

===========================================================================*/
/*!
    @brief
    This function initializes the client provided request structure 
    (lte_ml1_cswitch_req_type) with default params. This must be called before
    call to lte_ml1_cswitch_sched_req()

    @detail
    cswitch request init

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_sched_req_init
(
  lte_mem_instance_type    instance,
  lte_ml1_cswitch_req_type *req
);


/*===========================================================================

  lte_ml1_cswitch_req_init_params_for_tune

===========================================================================*/
/*!
    @brief
    This function initializes the client provided request structure 
    with tune params

    @detail
    cswitch request init with tune info

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_req_init_params_for_tune
(
  lte_mem_instance_type          instance,
  /*! request structure that needs to be filled in with tune related
   * details */
  lte_ml1_cswitch_req_type       *req,
  /* Number of DL carriers to tune */
  uint32                         num_dl_carriers,
  /* DL carrier tune info */
  lte_ml1_cswitch_tune_info_type *dl_tune_info,
  /* Number of UL carriers to tune */
  uint32                         num_ul_carriers,
  /* UL carrier tune info */
  lte_ml1_cswitch_tune_info_type *ul_tune_info
);

/*===========================================================================

  lte_ml1_cswitch_req_init_params_for_cell_switch

===========================================================================*/
/*!
    @brief
    This function initializes the client provided request structure 
    with cell switch params

    @detail
    cswitch request init with cell info

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_req_init_params_for_cell_switch
(
  lte_mem_instance_type          instance,
  /*! request structure that needs to be filled in with tune related
   * details */
  lte_ml1_cswitch_req_type       *req,
  /* Number of DL carriers to tune */
  uint32                         num_dl_carriers,
  /* DL carrier tune info */
  lte_ml1_cswitch_cell_info_type *dl_cell_info,
  /* Number of UL carriers to tune */
  uint32                         num_ul_carriers,
  /* UL carrier tune info */
  lte_ml1_cswitch_tune_info_type *ul_tune_info,
  /* target cell info */
  lte_ml1_rat_lte_cell_struct_s  *target_cell
);

/*===========================================================================

  lte_ml1_cswitch_trigger_switch_back

===========================================================================*/
/*!
    @brief
    Client MUST call this function to let cswitch know when to switch back to
    the original freq/cell values. cswitch will invoke switch_back_done 
    callback provided by the client when it is done switching to original 
    freq values.

    @detail
    cswitch request init with cell info

    @return
    none

*/
/*=========================================================================*/
void lte_ml1_cswitch_trigger_switch_back
(
  lte_mem_instance_type          instance,
  /* Handle asociated with the request */
  lte_ml1_cswitch_handle_type    handle
);

/*===========================================================================

  lte_ml1_cswitch_switch_cancel

===========================================================================*/
/*!
  @brief
  Client MUST call this function to Cancel an already schduled cswitch request.

  @detail
  Handle to the ealrier request is the input

  @return
  none

*/
/*=========================================================================*/
void lte_ml1_cswitch_switch_cancel
(
 lte_mem_instance_type          instance,
 /* Handle asociated with the request */
 lte_ml1_cswitch_handle_type    handle
);

/*===========================================================================

  lte_ml1_cswitch_sched_req

===========================================================================*/
/*!
    @brief
    Schedule a tune request with cswitch module. The request param has details
    about scheduler timing and target freq params
    NOTE that this API should always be called from the MGR task context and
    not from GM task context.

    @detail
    place a cswitch req

    @return
    lte_ml1_cswitch_status_type

*/
/*=========================================================================*/
lte_ml1_cswitch_status_type lte_ml1_cswitch_sched_req
(
  lte_mem_instance_type        instance,

  /*! This structure contains info on when to schedule
   * freq switch and what params (freq/bw) to use to
   * tune to new freq. This should be allocated by the client
   * and maintained throughout the validity of the request;
   * i.e. until it gets abort or switch_back_done callback
   * from cswitch module */
  lte_ml1_cswitch_req_type     *req,

  /* User data associated with the request, will be used as a param when
   * cswitch module invokes the switch_done/switch_back_done/abort cb */
  void                         *user_data,

  /* client should use this handle for any operations related
   * to this request while calling cswitch API. Client provides
   * the address where to save the handle */
  lte_ml1_cswitch_handle_type *handle_ptr  /* return value */
);

/*===========================================================================

  lte_ml1_cswitch_is_operation_complete

===========================================================================*/
/*!
    @brief
    Client may use this API to query cswitch if the req associated with the
    given handle has been serviced (client may destroy the req if this
    API returns true)

    @detail

    @return
    boolean TRUE: Request associated with given handle is serviced
            FALSE: Request associated with given handle is not serviced

*/
/*=========================================================================*/
boolean lte_ml1_cswitch_is_operation_complete
(
  lte_mem_instance_type        instance,
  lte_ml1_cswitch_handle_type  handle
);

/*===========================================================================

                        lte_ml1_cswitch_intf_find_cell

===========================================================================*/
/*!
    @brief
    This function returns the pointer of the item for the specific frequency
    number and the cell ID

    @detail
    This function returns the pointer of the item for the specific frequency
    number and the cell ID.

    If no such cell is in CSWITCH DB, it returns NULL which will eventually Assert

    @return
    The pointer to the BPLMN DB item if it is found in the DB. 
    NULL if it is not in the DB

*/
/*=========================================================================*/
lte_ml1_rat_lte_cell_struct_s *lte_ml1_cswitch_intf_find_cell
( 
  lte_mem_instance_type instance,
  /*! the frequency (earfcn) of the cell */
  lte_earfcn_t      req_freq,

  /*! the cell ID */
  lte_phy_cell_id_t req_cell_id
);

#endif /*#ifdef FEATURE_CSWITCH_MODULE*/

#endif //LTE_ML1_CSWITCH_H

