#! /usr/bin/env python
import time
import sys
import struct
import math
from collections import Counter
from multiprocessing import cpu_count, Manager

from compress_process import CompressionProcess


#Development parameters
DEV = 0  #enable to create intermediate dict.bin and compressed.bin
PRELOADED_DICT = 0  #enable to use pregenerated dict.bin
INFO_ENABLED = 1	#enable to print info statements



#Algorithm parameters
DICTIONARY1_SIZE =1024
DICTIONARY2_SIZE =4096
DICTIONARY_SIZE  =DICTIONARY1_SIZE+DICTIONARY2_SIZE
DICTIONARY1_BITS = int(math.ceil(math.log(float(DICTIONARY1_SIZE),2)))
DICTIONARY2_BITS = int(math.ceil(math.log(float(DICTIONARY2_SIZE),2)))
BLOCK_SIZE = 1024
LOOKBACK_LEN = 512
LOOKBACK_BITS = int(math.ceil(math.log(float(LOOKBACK_LEN),2)))

def info(arg):
	if INFO_ENABLED:
		print arg


def chunks(l, n, size=BLOCK_SIZE):
	""" Divide l amongst n groups, making sure first n-1 groups are multiples of BLOCK_SIZE
	"""
	wordsUsed = 0
	threads = n
	for i in xrange(n-1):
		blocksPerThread = int(math.ceil(((len(l)-wordsUsed) / float(threads)) / size))
		threads -= 1
		yield l[wordsUsed:wordsUsed+blocksPerThread*size]
		wordsUsed += blocksPerThread*size
	yield l[wordsUsed:]

		
def compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
	threadSafe = Manager()
	start = time.time()
	instrList=[]
	instrListSubset=[]
	dictionary={}

	if not input:
		if (len(sys.argv) < 2):
			print "Usage: " + sys.argv[0] + " <file-to-compress> [<compression-type>] [<subset-start>] [<subset-size>]"
			exit(-2)

		inputFile = sys.argv[1]

		type = 'f'
		subset_start = 0
		subset_size = 0xFFFFFFFF

		if (len(sys.argv) > 2):
			type = sys.argv[2][0:1]
		if (len(sys.argv) > 3):
			subset_start = int(sys.argv[3], 16) + 1
		if (len(sys.argv) > 4):
			subset_size = int(sys.argv[4], 16)

		with open(inputFile,"rb") as f:
			word = f.read(4)
			bytes_read = 4
			while word:
				word = struct.unpack('I', word)[0]
				instrList.append(word)
				if bytes_read >= subset_start and bytes_read <= (subset_start+subset_size):
					instrListSubset.append(word)
				word = f.read(4)
				if len(word)!=4:
					info("Warning file not a multiple of words: len(last_read)=%d bytes_read=%d"%(len(word),bytes_read))
					break
				bytes_read += 4

	else:
		for word in (input[i:i+4] for i in range(0,len(input),4)):
			if len(word) == 4:
				instrList.append( struct.unpack('I',word)[0] )
		instrListSubset = instrList

			
	fileReadTime=time.time()-start
	###########################################################################
	start = time.time()

	dict_list = [struct.pack('I', 0)]*DICTIONARY_SIZE

	if PRELOADED_DICT:
		with open("dict.bin","rb") as f:
			idx = 0
			word = f.read(4)
			while word and len(word) == 4:
				word = struct.unpack('I', word)[0]
				dictionary[word] = idx
				dict_list[idx] = struct.pack('I', word & 0xFFFFFFFF)
				idx += 1

	else:
		countDict = Counter(instrList)

		for n,(word,count) in enumerate(countDict.most_common(DICTIONARY_SIZE)):
			dictionary[word] = n
			dict_list[n] = struct.pack('I', word & 0xFFFFFFFF)

		if DEV:
			with open("dict.bin","w") as f:
				for word in dict_list:
					f.write(word)

	
	dictionaryTime=time.time()-start
	###########################################################################
	start = time.time()


	n_blocks = 0
	n_threads = int((cpu_count()*3)/2)
	#n_threads = 1
	processes = []
	#Launch a thread for each chunk of blocks
	for n,instrChunk in enumerate(chunks(instrListSubset,n_threads,page_size/4)):
		chunkBlocks = len(instrChunk)/float(BLOCK_SIZE)
		n_blocks += math.ceil(chunkBlocks)
		if len(instrChunk) > 0:
			#print "Thread", n, "assigned",  chunkBlocks, "blocks"
			q = threadSafe.list()
			statsWords = threadSafe.dict()
			statsBits = threadSafe.dict()
			p = CompressionProcess(DICTIONARY1_SIZE, DICTIONARY1_BITS,DICTIONARY2_SIZE, DICTIONARY2_BITS, BLOCK_SIZE, LOOKBACK_LEN, LOOKBACK_BITS, instrChunk, dictionary, None, None, q, statsWords,statsBits)
			processes.append((p,q,statsWords,statsBits))
			p.start()
	#print "----"

	v_addrs = []  #list of block starting addresses
	va = 0
	num_blocks_i = int(n_blocks)
	print "Num blocks = " + str(num_blocks_i)
	if(num_blocks_i%2 == 0):
		#we need to ensure we have odd number of blocks 
		#so that va starts at an 8-byte aligned address
		va = int(VA_start + 2 + 2 + 4 * DICTIONARY_SIZE + 4 * num_blocks_i + 4) #2 bytes for n_blocks, 2 for 0, 4 per dict entry, 4 per block start addr
	else:
		va = int(VA_start + 2 + 2 + 4 * DICTIONARY_SIZE + 4 * num_blocks_i)

	print "Start address of compressed stream " + str(va)
	#Join the threads and process results
	compressed_text = []
	statsWordsCounter = Counter()
	statsBitsCounter = Counter()
	for n,(p,q,statsWords,statsBits) in enumerate(processes):
		p.join()
		for block in q:				
			if(va%8 != 0):
				print "We have a problem. Start addr not DWORD aligned " + str(va)
				sys.exit(0)
			v_addrs.append( struct.pack('I',va) )			
			for word in block:
				compressed_text.append(word)
			va += 4 * len(block)							
		print "Thread", n, "compressed",  len(q), "blocks"

		#consolidate thread statistics
		for (stat,count) in statsWords.items():
			statsWordsCounter[stat] += count
		for (stat,count) in statsBits.items():
			statsBitsCounter[stat] += count
	print "----"

	if DEV:
		with open("compressed.bin", "wb") as f:
			for word in compressed_text:
				f.write(word)   

	compressTime = time.time()-start
   ############################################################################

	print "File Read Time:", fileReadTime,"s"
	print "Dictionary Time:", dictionaryTime, "s"
	print "Compression Time:", compressTime, "s"

	#build meta data
	metadata = [struct.pack("H",n_blocks), struct.pack("H",0)]
	metadata += dict_list
	metadata += v_addrs
	if(num_blocks_i%2 == 0):
		metadata += struct.pack("I",0)
	metadata += compressed_text

	#print compression stats
	info("################################################################################################")
	info("                                        Type     #Words    #Bits   #bitsCoded #WordsSaved Ratio")
	wordsUncompressed=0
	wordsCompressed=0
	for (stat,wordCount) in statsWordsCounter.most_common():
		bitCount=statsBitsCounter[stat]
		bitsPerWord=0
		if bitCount!=0:
			bitsPerWord=int(float(bitCount)/float(wordCount))
		wordsSaved=wordCount-(bitCount/32)
		ratio=float(bitCount/32)/float(wordCount)
		wordsUncompressed+=wordCount
		wordsCompressed+=((bitCount+31)/32)
		info('symb2freq["%20s"] = %10d #%10d %10d %10d  %2.4f'%(stat,wordCount,bitCount,bitsPerWord,wordsSaved,ratio))
	#print("==================================")
	#for (stat,bitCount) in statsBitsCounter.most_common():
		#info('symb2freq["%20s"] = %10d'%(stat,bitCount))

	totalRatio=float(wordsCompressed)/float(wordsUncompressed)
	info("############################################################################")
	info("Uncompressed=%2.2fMB Compressed=%2.2fMB totalRatio %2.4f"%(wordsUncompressed/250000.0,wordsCompressed/250000.0,totalRatio))
	info("Code length assigned relative to word frequency")
	info("Search order is assigned relative to final compression ratio")
	info("############################################################################")

	return ''.join(metadata)  #joins list elements together as string with no spaces

if __name__ == '__main__':
	compress()
