#!usr/bin/env python

################################################################################
# This script is called by q6zip_crash_rw.cmm and supposed to find required
# python file and copy them into C:\Temp folder for q6zip_crash_rw.cmm to use.
################################################################################

import os
import sys
import shutil
import glob
import imp


def main(): #{{{
  if(len(sys.argv) != 3):
    print "ERROR: invalid usage of q6zip_crash_rw_hd.py!"
    print "This script is supposed to be called by q6zip_crash_rw.cmm only."
    sys.exit(1)

  # assume we always have this python file delivered in same folder with
  # q6zip_crash_rw.cmm file.
  build_root = sys.argv[2]
  if "modem_proc" in build_root: 
    # non-uniform &MPSS_BUILD_ROOT causes issue here.
    build_root = build_root.split('modem_proc')[0]
  path_modem = os.path.join(build_root, "modem_proc")
  use_perf = True # by default we assume we are using core module.
  if sys.argv[1] == "core":
    use_perf = False

  files_to_copy = []
  # find required files from right module and copy them into user local dir.
  if use_perf:
    print "uses perf module"
    fpath = os.path.join( path_modem,
                          "perf",
                          "q6zip",
                          "core",
                          "compressor")

    #q6zip_versions = imp.load_source("q6zip_versions",
    #                                 os.path.join( fpath,
    #                                               "q6zip_versions.py"))

    #rw_ver = q6zip_versions.RW_VERSION

    rw_ver = sys.argv[1]
    py_f1 = os.path.join( fpath,
                          "rw",
                          rw_ver,
                          "bin",
                          "rw_py_compress.py")

    py_f2 = os.path.join( fpath,
                          "debug",
                          "rw_decompress_file.py") # need be added here.

  else: # use core.
    # In newer PL, core module has q6zip_versions.py but on older PL, it only
    # has q6zip_constants.py. Before we clean up this, we have to complicate our
    # logic to detect the right files to be copied.
    print "uses core module"
    fpath = os.path.join( path_modem,
                          "core",
                          "kernel",
                          "dlpager",
                          "compressor")

    qcf = os.path.join(fpath, "q6zip_constants.py")
    old_pl = os.path.isfile(qcf)
    if old_pl: # TH2.x and older
      py_f1 = os.path.join(fpath, "rw_py_compress.py")
      py_f2 = os.path.join(fpath, "rw_decompress_file.py")
      py_f3 = qcf
      files_to_copy.append(py_f3)
    else: # TA, AT and newer
      q6zip_versions = imp.load_source("q6zip_versions",
                                       os.path.join( fpath,
                                                     "q6zip_versions.py"))
      rw_ver = q6zip_versions.RW_VERSION
      py_f1 = os.path.join( fpath,
                            "rw",
                            rw_ver,
                            "bin",
                            "rw_py_compress.py")
      py_f2 = os.path.join( fpath,
                            "rw",
                            "rw_decompress_file.py") # need be added here.

  cmm_f = os.path.join( fpath, "debug", "q6zip_crash_rw.cmm")
  files_to_copy.extend([py_f1, py_f2, cmm_f])

  for f in files_to_copy:
    if  not os.path.isfile(f):
      print "ERROR: required python script not delivered in MPSS build."
      sys.exit(1)

  usr_dir = "C:\\Temp" # windows only. And always assume it's already present.
  if not os.path.exists(usr_dir):
    # don't create this folder by py script (http://stackoverflow.com/a/273227).
    print "ERROR: cannot find 'C:\Temp' directory on this PC."
    sys.exit(1)

  # copy required files from MPSS build to user directory.
  try:
    for f in files_to_copy:
      # we use copyfile instead of copy: http://stackoverflow.com/a/11835950
      #shutil.copy(f, usr_dir)
      shutil.copyfile(f, os.path.join(usr_dir, os.path.basename(f)))
  except shutil.Error: # copy same file is not needed.
    print "skip copying exactly same files."
    pass
  except IOError as e:
    print "ERROR: %s" % e
    sys.exit(1)

  print "Done."
  return 0
#}}}

if __name__ == "__main__":
  main()
