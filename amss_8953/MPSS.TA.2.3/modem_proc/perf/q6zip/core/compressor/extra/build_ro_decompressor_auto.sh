#!/usr/bin/env bash

#set -o verbose

curr_platf=`uname -o`

find ../ -type f -name 'q6zip_uncompress.c' | while read line; do
    echo "compiling '$line'"
    src_path="$(dirname $line)"
    src_file="${line##*/}" # extract file name.
    inc_path="$(echo $src_path | sed 's/src/inc/')"
    bin_path="$(echo $src_path | sed 's/src/bin/')"
    bin_file="${src_file%.*}.o" # http://stackoverflow.com/a/2664746
    if [ "$curr_platf" == "Cygwin" ]; then
        i686-pc-mingw32-gcc -c -mwindows $line -I$inc_path -o $bin_path/$bin_file && \
        i686-pc-mingw32-gcc -o $bin_path/q6zip_ro_decompressor.exe -mwindows q6zip_file_interface.c $bin_path/$bin_file -I$inc_path
    elif [ "$curr_platf" == "GNU/Linux" ]; then
        gcc -c $line -I$inc_path -o $bin_path/$bin_file && \
        gcc -o $bin_path/q6zip_ro_decompressor q6zip_file_interface.c $bin_path/$bin_file -I$inc_path
    fi
done

exit 0
