#pragma once
#include "q6zip_test.h"

#define Q6ZIP_TEST_VECTORS_RW_MAX 23

extern q6zip_test_vectors_index_t const RW_VECTORS_INDEX[ Q6ZIP_TEST_VECTORS_RW_MAX ];

