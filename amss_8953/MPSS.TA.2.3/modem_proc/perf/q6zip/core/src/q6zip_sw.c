#include "q6_cache.h"
#include "q6zip_clk.h"
#include "q6zip_context.h"
#include "q6zip_log.h"
#include "q6zip_request.h"
#include "q6zip_request_private.h"
#include "q6zip_sw.h"
#include "q6zip_sw_worker.h"
#include "q6zip_types.h"
#include "q6zip_uncompress.h"
#include "qurt.h"
#include "rw_compress.h"
#include <stringl.h>

/** @brief Define this (below) to enable Q6ZIP_SW to respond synchronously */
#undef Q6ZIP_SW_USES_SYNC_WORKER

/** @brief Define this (below) to enable q6zip logs */
#undef Q6ZIP_SW_LOGGING_ENABLED

/** @brief Maximum number of outstanding requests with SW implementation of
           Q6ZIP */
#define Q6ZIP_SW_MAX_OUTSTANDING_REQUESTS 2

/** @brief Type for Q6ZIP SW module specific state */
typedef struct
{
    /** @brief Dictionary for readonly decompress operation */
    q6zip_iovec_t dict;

    /** @brief Callback to be invoked upon completion of a request */
    q6zip_completed_cb_t completed;

    qurt_sem_t sem;

    /** @brief *NEXT* sequence number to be used */
    uint8 next_seqno;

} q6zip_sw_module_t;

static q6zip_sw_module_t q6zip_sw_module;

static inline void q6zip_sw_log_request (q6zip_request_t * request)
{
    #if defined( Q6ZIP_SW_LOGGING_ENABLED )
    q6zip_context_t * context = q6zip_request_private_get_context( request );

    context->meta.sw.seqno = q6zip_sw_module.next_seqno;
    q6zip_log_put(
        qurt_sysclock_get_hw_ticks(),
        context->meta.sw.seqno,
        0,
        0,
        request->priority == Q6ZIP_FIXED_PRIORITY_HIGH,
        Q6ZIP_LOG_TYPE_REQ,
        request->algo,
        request->src.len >> 2 );
    q6zip_sw_module.next_seqno++;
    #endif
}

static inline void q6zip_sw_log_response (
    q6zip_request_t * request,
    q6zip_response_t const * response
)
{
    #if defined( Q6ZIP_SW_LOGGING_ENABLED )
    q6zip_context_t * context = q6zip_request_private_get_context( request );

    q6zip_log_put(
        qurt_sysclock_get_hw_ticks(),
        context->meta.sw.seqno,
        0,
        0,
        request->priority == Q6ZIP_FIXED_PRIORITY_HIGH,
        Q6ZIP_LOG_TYPE_RSP,
        request->algo,
        response->len >> 2 );

    #if defined( Q6ZIP_LOG_ASYNC_FLUSH )
    q6zip_log_flush();
    #endif

    #endif
}

static void q6zip_sw_process_request (q6zip_request_t * request)
{
    q6zip_context_t * context = q6zip_request_private_get_context( request );
    int dst_len;

    context->impl = Q6ZIP_IMPL_SW;
    context->response.error = Q6ZIP_ERROR_NONE;

    q6zip_sw_log_request( request );

    switch ( request->algo )
    {
        case Q6ZIP_ALGO_UNZIP_RO:
            dst_len = (int)request->dst.len;
            q6zip_uncompress( request->dst.ptr, &dst_len, request->src.ptr, request->src.len, q6zip_sw_module.dict.ptr );
            context->response.len = dst_len;

            qurt_mem_cache_clean ((qurt_addr_t)request->dst.ptr,4096,QURT_MEM_CACHE_FLUSH_INVALIDATE, QURT_MEM_DCACHE);

            break;

        case Q6ZIP_ALGO_ZIP_RW:
            dczero( (uint32_t)request->dst.ptr, request->dst.len );
            l2fetch_buffer( request->src.ptr, request->src.len );
            /* @warning Input length is in *WORDS* but output is in *BYTES*! */
            context->response.len = deltaCompress( request->src.ptr, request->dst.ptr, request->src.len >> 2 );
            qurt_mem_cache_clean( (qurt_addr_t) request->src.ptr, request->src.len, QURT_MEM_CACHE_FLUSH_INVALIDATE, QURT_MEM_DCACHE );
            break;

        case Q6ZIP_ALGO_UNZIP_RW:
            l2fetch_buffer( request->src.ptr, request->src.len );
            /* @warning Input length is in *WORDS* but output is in *BYTES*! */
            context->response.len = deltaUncompress( request->src.ptr, request->dst.ptr, request->dst.len >> 2 );

            qurt_mem_cache_clean ((qurt_addr_t)request->dst.ptr,4096,QURT_MEM_CACHE_FLUSH_INVALIDATE, QURT_MEM_DCACHE);
            break;

        case Q6ZIP_ALGO_MEMCPY:
            memscpy( request->dst.ptr, request->dst.len, request->src.ptr, request->src.len );
            qurt_mem_cache_clean( (qurt_addr_t)request->dst.ptr, 4096, QURT_MEM_CACHE_FLUSH_INVALIDATE, QURT_MEM_DCACHE );
            break;

        default:
            context->response.error = Q6ZIP_ERROR_BAD_OPERATION;
            /* @todo anandj Progamming error. Abort! */
            break;
    }

    q6zip_sw_log_response( request, &context->response );

    qurt_sem_up( &q6zip_sw_module.sem );

    q6zip_sw_module.completed( request, &context->response );
}

static void q6zip_sw_init (
    void *                 ro_dict,
    unsigned int           ro_dict_len,
    q6zip_initialized_cb_t initialized,
    q6zip_completed_cb_t   completed
)
{
    q6zip_sw_module.dict.ptr = ro_dict;
    q6zip_sw_module.dict.len = ro_dict_len;

    q6zip_sw_module.completed = completed;

    qurt_sem_init_val( &q6zip_sw_module.sem, Q6ZIP_SW_MAX_OUTSTANDING_REQUESTS );

    #if !defined( Q6ZIP_SW_USES_SYNC_WORKER )
    /* Initialize worker(s) that will initiate the unzip/zip work */
    q6zip_sw_worker_init( q6zip_sw_process_request );
    #endif

    initialized( TRUE );
}

static q6zip_request_t * q6zip_sw_get_request (void)
{
    qurt_sem_down( &q6zip_sw_module.sem );
    return q6zip_request_alloc();
}

static q6zip_error_t q6zip_sw_submit (q6zip_request_t ** request)
{
    Q6ZIP_ASSERT( *request );

    if ( !request || !*request )
    {
        return Q6ZIP_ERROR_NULL_PTR;
    }

    q6zip_clk_check_vote();

    #if defined( Q6ZIP_SW_USES_SYNC_WORKER )
    q6zip_sw_process_request( *request );
    return Q6ZIP_ERROR_NONE;
    #else
    /* Let (one of) the worker do the work */
    return q6zip_sw_worker_submit( *request );
    #endif
}

static q6zip_error_t q6zip_sw_submit_pair (q6zip_request_pair_t * pair)
{
    q6zip_error_t error;

    Q6ZIP_ASSERT( pair );
    Q6ZIP_ASSERT( pair->first );
    Q6ZIP_ASSERT( pair->second );

    if ( !pair || !pair->first || !pair->second )
    {
        return Q6ZIP_ERROR_NULL_PTR;
    }

    /* @todo anandj submit_pair() may not be needed. It's being kept around for
       completeness. Once CRD confirms that it is not required, remove it! */

    error = q6zip_sw_submit( &pair->first );
    if ( error )
    {
        return error;
    }

    return q6zip_sw_submit( &pair->second );;
}

static void q6zip_sw_fini (void)
{
    #if !defined( Q6ZIP_SW_USES_SYNC_WORKER )
    q6zip_sw_worker_fini();
    #endif
}

q6zip_iface_t const Q6ZIP_SW = {

    q6zip_sw_init,
    q6zip_sw_get_request,
    q6zip_sw_submit,
    q6zip_sw_submit_pair,
    q6zip_sw_fini

};
