#include "q6zip_clk.h"
#include "q6zip_context.h"
#include "q6zip_log.h"
#include "q6zip_module.h"

void q6zip_module_init (void)
{
    /* @todo anandj Disable q6zip_clk until modem crash/unresponsiveness
       is fixed
    q6zip_clk_init();*/
    q6zip_context_module_init();
    q6zip_log_init();
}

void q6zip_module_fini (void)
{
    q6zip_context_module_fini();
}

