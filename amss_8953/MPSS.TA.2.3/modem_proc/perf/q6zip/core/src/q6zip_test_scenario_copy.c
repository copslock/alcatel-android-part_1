#include "comdef.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test_scenario_copy.h"
#include <stringl.h>

#define PAGE_SHIFT 12
#define PAGE_SIZE ( 1 << PAGE_SHIFT )
#define NUM_PAGES 16

typedef PACK( struct )
{
    uint8 control;

    uint8 num_requests_per_burst:7;
    uint8 compare:1;

    uint16 num_bursts;

} q6zip_test_scenario_copy_conf_t;

typedef struct
{
    q_link_type   link;
    q6zip_iovec_t src;
    q6zip_iovec_t dst;
    boolean       compare;

} node_t;

typedef struct
{
    struct
    {
        q_type queue;
        node_t nodes[ NUM_PAGES ];
    } buffer;

} q6zip_test_scenario_copy_module_t;

/** @brief Configuration */
static q6zip_test_scenario_copy_conf_t conf;

/* Source pages for IPA DMA */
static unsigned char src_pages[ NUM_PAGES * PAGE_SIZE ] __attribute__(( aligned( 4 ) ));

/* Destination pages for IPA DMA */
static unsigned char dst_pages[ NUM_PAGES * PAGE_SIZE ] __attribute__(( aligned( 4 ) ));

static q6zip_test_scenario_copy_module_t module;

static void initialize (q6zip_test_scenario_t * scenario)
{
    unsigned int i;

    q_init( &module.buffer.queue );

    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( module.buffer.nodes ); i++ )
    {
        /* Associate source and destination pages */
        module.buffer.nodes[ i ].src.ptr = &src_pages[ i * PAGE_SIZE ];
        module.buffer.nodes[ i ].src.len = PAGE_SIZE;

        memset( module.buffer.nodes[ i ].src.ptr, i, PAGE_SIZE );

        module.buffer.nodes[ i ].dst.ptr = &dst_pages[ i * PAGE_SIZE ];
        module.buffer.nodes[ i ].dst.len = PAGE_SIZE;

        memset( module.buffer.nodes[ i ].dst.ptr, 0, PAGE_SIZE );

        q_put( &module.buffer.queue, &module.buffer.nodes[ i ].link );
    }
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
    q6zip_test_scenario_copy_conf_t const * confp = (q6zip_test_scenario_copy_conf_t *)msg;

    /* Local copy of configuration */
    memscpy( &conf, sizeof( conf ), confp, sizeof( *confp ) );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "CONFG: copy. compare=%u num_bursts=%u num_requests_per_burst=%d",
           conf.compare, conf.num_bursts, conf.num_requests_per_burst );
}

static void start (q6zip_test_scenario_t * scenario)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    node_t * node;
    q6zip_error_t error;
    unsigned int num_requests;
    unsigned int num_bursts;

    num_bursts = num_requests = 0;
    while ( !scenario->data.stopped && num_bursts < conf.num_bursts)
    {
        num_requests = 0;
        while ( !scenario->data.stopped && num_requests < conf.num_requests_per_burst )
        {
            request = q6zip->get_request();
            Q6ZIP_ASSERT( request );

            node = (node_t *)q_get( &module.buffer.queue );
            Q6ZIP_ASSERT( node );

            request->src.ptr = node->src.ptr;
            request->src.len = node->src.len;
            request->dst.ptr = node->dst.ptr;
            request->dst.len = node->dst.len;
            request->algo    = Q6ZIP_ALGO_MEMCPY;
            request->priority = Q6ZIP_FIXED_PRIORITY_HIGH;
            request->user.other = node;

            error = q6zip->submit( &request );

            if ( error )
            {
                q_put( &module.buffer.queue, &node->link );
                q6zip_request_free( &request );
            }

            num_requests++;

            q6zip_test_wait( 50000 );
        }

        num_bursts++;
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "LEAVE: copy. num_bursts=%u num_requests_per_burst=%u",
           num_bursts, num_requests, 0 );
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    node_t * node = request->user.other;
    Q6ZIP_ASSERT( !response->error );

    if ( conf.compare )
    {
        ASSERT( response->len == PAGE_SIZE );
        ASSERT( memcmp( request->dst.ptr, request->src.ptr, request->src.len ) == 0 );
    }

    q_put( &module.buffer.queue, &node->link );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_copy =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
