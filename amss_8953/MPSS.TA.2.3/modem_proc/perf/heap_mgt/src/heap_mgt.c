/**
  @file heap_mgt.c

  @brief  
*/
/*
  Copyright (c) 2013 QUALCOMM Technologies Incorporated.
          All Rights Reserved.
    Qualcomm Confidential and Proprietary
*/

/*===========================================================================
                      EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/perf.mpss/1.0.4/heap_mgt/src/heap_mgt.c#1 $
  $DateTime: 2016/02/09 16:18:42 $


  when        who     what, where, why
  ---------   ---     ---------------------------------------------------------------------------
 08/31/2015   skotalwa added new file

===========================================================================*/


#include "heap_mgt.h"

#ifdef HEAP_QUOTA_BUDGET

extern void heap_quota_exceeded_cb(mem_heap_type *heap_ptr,uint32 tid,size_t max_used);
 
quota_debug_info_s quota_debug_info; 

/********************************************************
 FUNCTION: heap_mgt_init(void)
 Init function to for heap quota budget.
 
 @param[in]
  - N/A.  
 
 @return
  - N/A.
 ********************************************************/
void heap_mgt_init(void)
{
	

   if((int)(memheap_register_quota_exceed_cb(&modem_mem_heap, heap_quota_exceeded_cb))==0)
   {
       MSG_HIGH("memheap_register_quota_exceed_cb successful", 0,0,0);
   }
   else 
   {
       MSG_HIGH("memheap_register_quota_exceed_cb un-successful", 0,0,0);
   }
  return;
}
/********************************************************
 FUNCTION: heap_quota_exceeded_cb(void)
 call back function when heap quota is exceeded.
 
 @param[in]
  - N/A.  
 
 @return
  - N/A.
 ********************************************************/
void heap_quota_exceeded_cb(mem_heap_type *heap_ptr, uint32 tid, size_t max_used)
{
uint32 client_id;
char *task_name; 
client_id = tid;
uint32 size = max_used;
int result ;
char string [100];
int str_len;

 result = memheap_get_task_name(&modem_mem_heap, client_id, &task_name);

 quota_debug_info.callback_exceeded_quota = TRUE;

 #ifdef ERR_FATAL_AUX_MSG
   str_len = snprintf(string, 100, "heap quota exceeded task is %s", task_name );
   ERR_FATAL_AUX_MSG("Crash is due to heap limit exceeded by task. check coredump.err.aux_msg for details", string, str_len) ;
 #else
   ERR_FATAL(" heap quota of task_id %d exceeded with current usage %d ",client_id,size,0); 
 #endif


return;
}
#endif /*HEAP_QUOTA_BUDGET*/
