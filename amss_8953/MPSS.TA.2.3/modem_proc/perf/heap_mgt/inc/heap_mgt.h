
/********************************************//**
*\file heap_mgt.h
*\brief Common to heap_mgt.c and heap_mgt.h

*\copyright (c) 2006 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/perf.mpss/1.0.4/heap_mgt/inc/heap_mgt.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/07/15   skotalwa  heap quota    

===========================================================================*/



#include "customer.h"
#include "comdef.h"
#include "target.h"
#include "atomic_ops.h"
#include "modem_mem.h"        /* External interface to modem_mem.c */
#include "memheap.h"

extern mem_heap_type modem_mem_heap;

/*debug info for heap quota*/
typedef struct 
{
   boolean callback_registration_failed;
   boolean callback_exceeded_quota;
 }quota_debug_info_s;
 
 /********************************************************
 FUNCTION: heap_mgt_init(void)
 Init function to for heap quota budget.
 
 @param[in]
  - N/A.  
 
 @return
  - N/A.
 ********************************************************/
void heap_mgt_init(void);

