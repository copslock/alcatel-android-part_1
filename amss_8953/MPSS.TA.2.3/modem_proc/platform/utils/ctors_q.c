#include <stdio.h>

static int count = -1;
__attribute__((constructor)) static void ctor(void) {
      printf("ctor\n");
      count++;
}

__attribute__((destructor)) static void dtor(void) {
      printf("dtor\n");
}


int main(void) {
   return count;
}
