#include "allocator.h"
#include "uthash.h"
#include "verify.h"
#include "stdlib_IRealloc.h"
#include "shared.h"

#include <stdio.h>

#if !defined(__NIX)

// TODO need to support for mingw
static int rv = 1;
static int myctor(void) {
   rv--;
   return 0;
}
static int mydtor(void) {
   rv--;
   printf("dtor\n");
   return 0;
}

SHARED_OBJECT_API_ENTRY(myctor, mydtor)

struct hash {
   UT_hash_handle hh;
   int key;
};

int uthash_test(void) {
   struct hash* group = 0;
   struct hash item = {0}, *ptr = &item;
   int key = 1;
   item.key = 1;
   HASH_ADD_INT(group, key,   ptr);
   HASH_DELETE_IF(hh, group,  ptr);
   HASH_DELETE_IF(hh, group,  ptr);
   HASH_ADD_INT(group, key,   ptr);
   HASH_FIND_INT(group, &key,    ptr);
   assert(ptr == &item);
   HASH_DELETE_IF(hh, group,  ptr);
   HASH_FIND_INT(group, &key,    ptr);
   assert(ptr == 0);
   return 0;
}

int main(void) {
   int nErr = 0;
   nErr += uthash_test();
   nErr += rv;
   return nErr;
}

#else
int main(void) {
   return 0;
}
#endif
