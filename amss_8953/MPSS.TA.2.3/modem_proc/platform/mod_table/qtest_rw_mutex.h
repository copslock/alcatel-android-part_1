#define RW_MUTEX_T uint32 
#define RW_MUTEX_CTOR(mut) mut = 0 
#define RW_MUTEX_LOCK_READ(mut) \
   do {\
      assert(STD_BIT_TEST(&mut, 1) == 0); \
      assert(STD_BIT_TEST(&mut, 2) == 0); \
      STD_BIT_SET(&mut, 1); \
   } while (0)

#define RW_MUTEX_UNLOCK_READ(mut) \
   do {\
      assert(STD_BIT_TEST(&mut, 1)); \
      assert(STD_BIT_TEST(&mut, 2) == 0); \
      STD_BIT_CLEAR(&mut, 1); \
   } while (0)

#define RW_MUTEX_LOCK_WRITE(mut) \
   do {\
      assert(STD_BIT_TEST(&mut, 1) == 0); \
      assert(STD_BIT_TEST(&mut, 2) == 0); \
      STD_BIT_SET(&mut, 2); \
   } while (0)

#define RW_MUTEX_UNLOCK_WRITE(mut) \
   do {\
      assert(STD_BIT_TEST(&mut, 1) == 0); \
      assert(STD_BIT_TEST(&mut, 2)); \
      STD_BIT_CLEAR(&mut, 2); \
   } while (0)

#define RW_MUTEX_DTOR(mut) mut = 0


