#include "dlw.h"
#include "qurt_rw_mutex.h"
#include "mod_table.h"
#include "mod_table_cache.h"
#include "platform_libs.h"

//#ifndef VERIFY_PRINT_INFO
//#define VERIFY_PRINT_INFO
//#endif
//
//#ifndef VERIFY_PRINT_ERROR
//#define VERIFY_PRINT_ERROR
//#endif
//
//#define FARF_LOW           1
#include "HAP_farf.h"
#define LOGL(args...) FARF(LOW, args)

#include "mod_table_imp.h"

// gcc bug
#pragma GCC diagnostic ignored "-Wmissing-braces"

// Externed RPC process lookup table
extern int adsp_pls_add_lookup(uint32 type, uint32 key, int size, int (*ctor)(void* ctx, void* data), void* ctx, void (*dtor)(void*), void** ppo);
extern int adsp_pls_lookup(uint32 type, uint32 key, void** ppo);

// mod_table object
static struct static_mod_table static_mod_table_obj = {0};

// Externed RPC cache functions
extern int fastrpc_get_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t* attr);
extern int fastrpc_set_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t attr) ;

/**
  * register a static component for invocations
  * this can be called at any time including from a static constructor
  * 
  * overrides will be tried first, then dynamic modules, then regular 
  * static modules.
  *
  * name, name of the interface to register
  * pfn, function pointer to the skel invoke function
  *
  * for example:
  *   __attribute__((constructor)) static void my_module_ctor(void) {
  *      mod_table_register_static("my_module", my_module_skel_invoke);
  *   }
  *
  */
int mod_table_register_static_override(const char* name, int(*pfn)(uint32 sc, remote_arg* pra)) {	
   if(0 == static_mod_table_ctor_imp(&static_mod_table_obj)) {
      return static_mod_table_register_static_override_imp(&static_mod_table_obj, name, pfn);
   }
   return -1;
}

/**
  * register a static component for invocations
  * this can be called at any time including from a static constructor
  *
  * name, name of the interface to register
  * pfn, function pointer to the skel invoke function
  *
  * for example:
  *   __attribute__((constructor)) static void my_module_ctor(void) {
  *      mod_table_register_static("my_module", my_module_skel_invoke);
  *   }
  *
  */
int mod_table_register_static(const char* name, int(*pfn)(uint32 sc, remote_arg* pra)) {	
   if(0 == static_mod_table_ctor_imp(&static_mod_table_obj)) {
      return static_mod_table_register_static_imp(&static_mod_table_obj, name, pfn);
   }
   return -1;
}

/**
  * get and set the modules prefered caching attributes for input and output buffers
  * this can only be called from the invoking thread.
  *
  * @param bInBuf, if TRUE the value is set for input buffers, if FALSE for output buffers
  * @param attr, one of qurts attributes
  * 
  */
 
int mod_table_set_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t attr) {
   return fastrpc_set_cache_attr(bInBuf, attr);
}

int mod_table_get_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t* attr) {
   return fastrpc_get_cache_attr(bInBuf, attr);
}

/**
 * Open a module and get a handle to it
 *
 * in_name, name of module to open
 * handle, Output handle
 * dlerr, Error String (if an error occurs)
 * dlerrorLen, Length of error String (if an error occurs)
 * pdlErr, Error identifier
 */
int mod_table_open(const char* in_name, remote_handle* handle, char* dlerr, int dlerrorLen, int* pdlErr) {
   int nErr = 0;
   struct open_mod_table* pomt = 0;
   VERIFY(0 == adsp_pls_add_lookup((uint32)open_mod_table_ctor_imp, 0, sizeof(*pomt), open_mod_table_ctor_imp, (void*)&static_mod_table_obj, open_mod_table_dtor_imp, (void**)&pomt));
   VERIFY(0 == (nErr = open_mod_table_open_imp(pomt,in_name,handle,dlerr,dlerrorLen,pdlErr)));
bail:
   return nErr;
}
/**
 * invoke a handle in the mod table
 *
 * handle, handle to invoke
 * sc, scalars, see remote.h for documentation.
 * pra, args, see remote.h for documentation.
 */
int mod_table_invoke(remote_handle handle, uint32 sc, remote_arg* pra) {	
   int nErr = 0;
   struct open_mod_table* pomt = 0;
   VERIFY(0 == adsp_pls_add_lookup((uint32)open_mod_table_ctor_imp, 0, sizeof(*pomt), open_mod_table_ctor_imp, (void*)&static_mod_table_obj, open_mod_table_dtor_imp, (void**)&pomt));
   VERIFY(0 == (nErr = open_mod_table_handle_invoke(pomt, handle, sc, pra)));
bail:
   return nErr;
}

/**
 * Closes a handle in the mod table
 *
 * handle, handle to close
 * errStr, Error String (if an error occurs)
 * errStrLen, Length of error String (if an error occurs)
 * pdlErr, Error identifier
 */
int mod_table_close(remote_handle handle, char* errStr, int errStrLen, int* pdlErr) {
   int nErr = 0;
   struct open_mod_table* pomt = 0;
   VERIFY(0 == adsp_pls_lookup((uint32)open_mod_table_ctor_imp, 0, (void**)&pomt));
	VERIFY(0 == (nErr = open_mod_table_close_imp(pomt, handle, errStr,errStrLen,pdlErr)));
bail:
	return nErr;
}

/**
 * internal use only
 */
int mod_table_register_const_handle(remote_handle handle, const char* in_name, int(*pfn)(uint32 sc, remote_arg* pra)) {
   if(0 == static_mod_table_ctor_imp(&static_mod_table_obj)) {
      return static_mod_table_register_const_handle_imp(&static_mod_table_obj, handle, in_name, pfn);
   }
   return -1;
}

// Constructor and destructor
static int mod_table_ctor(void) {
	return static_mod_table_ctor_imp(&static_mod_table_obj);
}
static void mod_table_dtor(void) {
	static_mod_table_dtor_imp(&static_mod_table_obj);
	return;
}

PL_DEFINE(mod_table, mod_table_ctor, mod_table_dtor);
