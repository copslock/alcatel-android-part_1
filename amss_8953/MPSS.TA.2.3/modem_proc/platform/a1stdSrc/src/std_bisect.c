#include "AEEstd.h"

int std_bisect(const void* vpElems, int nNumElems, int nElemWidth, 
               const void* vpElem, 
               int (*pfnCompare)(void*, const void*, const void*),
               void* pCompareCx)
{
   const byte* pElems = vpElems;
   const byte* pElem = vpElem;
   int nMax = nNumElems, nMin = 0;

   while (nMax > nMin) {
      int nIx = (nMin + nMax) / 2;
      const void* pTest = pElems + (nIx * nElemWidth);
      int cmp = pfnCompare(pCompareCx, pElem, pTest);
      if (cmp == 0 ) {
         return nIx;
      } else if (cmp < 0 ) {
         nMax = ((nIx == nMax) ? nMax - 1 : nIx);
      } else {
         nMin = ((nIx == nMin) ? nMin + 1 : nIx);
      }
   }
   return nMax;
}

