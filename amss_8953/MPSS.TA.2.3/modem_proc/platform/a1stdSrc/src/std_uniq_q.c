#include "std_uniq.c"

static int cmp_int32(void* _, const void* aa, const void* bb)
{
   return *((int32*)aa) == *((int32*)bb) ? 0 : (*((int32*)aa) < *((int32*)bb) ? -1 : 1);
}

int main(void) 
{
   int nErrs = 0;
   int ii;
   int rv[] =  {0,1,2,3,4,5,6,7,8,9};
   int src[] = {0,1,1,2,2,3,4,4,5,5,5,6,7,8,9,9,9};
   int size = std_uniq(src, STD_ARRAY_SIZE(src), sizeof(src[0]), cmp_int32, 0);
   nErrs += size == STD_ARRAY_SIZE(rv) ? 0 : 1;
   for (ii = 0; ii < STD_ARRAY_SIZE(rv); ++ii) {
      nErrs += rv[ii] == src[ii] ? 0 : 1;
   }

   size = std_uniq(rv, STD_ARRAY_SIZE(rv), sizeof(rv[0]), cmp_int32, 0);
   nErrs += size == STD_ARRAY_SIZE(rv) ? 0 : 1;
   for (ii = 0; ii < STD_ARRAY_SIZE(rv); ++ii) {
      nErrs += rv[ii] == src[ii] ? 0 : 1;
   }

   return nErrs;
}
