/*==============================================================================

File:
   AEEstdtime.h

Description:
   Time conversion algorithms

        Copyright (c) 2008 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
==============================================================================*/

#include "AEEStdDef.h"
#include "AEEStdErr.h"
#include "AEEstd.h"
#include "AEEstdtime.h"

#define SECSINDAY  86400

/* Other Epochs can be found here: 
   http://en.wikipedia.org/wiki/Epoch_%28reference_date%29 */

/*==============================================================================

JulianDayToStdTime

Description:
   This procedure converts a specified Julian Date into a Gregorian Calendar
   Date.    

   This formula is taken from the astronomical concept of "Julian Date"
   (not to be confused with the Julian Calendar).  The Julian Date is
   the number of days since January 1, 4713BC, and give astronomers an
   easy way to calculate the time difference between two dates.  
   There exist formulas from converting a Gregorian calendar date into the
   appropriate Julian day number; this implements one of these
   after making an adjustment to the epoch, to base our times on
   6 January 1980.

   In particular, this formula is explained:
   http://www.tondering.dk/claus/cal/node3.html#sec-julianPeriod

Prototype:
   void JulianDayToStdTime(uint32 u32Days, AEEDateTime* pdt)

Parameters:
   u32Days: Julian day
   pdt: AEEDateTime structure corresponding to the Julian Day specified

Return Value:
   None

Comments:
   Zeros mm:hh:ss

=============================================================================*/
static void JulianDayToDateTime(uint32 u32Days, AEEDateTime* pdt)
{
   // Horrible variable names, but used to be consistent with the 
   // formula docs.
   //
   // The following implements the formula exactly as documented at:
   // http://www.tondering.dk/claus/cal/node3.html#sec-julianPeriod
   //
   // Note that parenthesis are very important to get the integer 
   // division correct.  For instance, 12*(m/10) is intended, rather
   // than (12*m)/10 which would normally be used to obtain a more
   // mathematically correct result.
   //
   uint32 a,b,c,d,e,m;

   a = u32Days + 32044;
   b = (4*a + 3)/146097;
   c = a - (146097*b)/4;
   d = (4*c + 3)/1461;
   e = c - (1461*d)/4;
   m = (5*e+2)/153;
   
   pdt->wDay = (uint16)((e - (153*m + 2)/5) + 1);
   pdt->wMonth = (uint16)((m + 3) - 12*(m/10));
   pdt->wYear = (uint16)(((100*b + d) - 4800) + m/10);
   pdt->wWeekDay = (uint16)(u32Days % 7);
}

/*=============================================================================

DateTimeToJulianDay

Description:
   This procedure converts a specified Gregorian Calendar Date into a 
   Julian Date.    

   This formula is taken from the astronomical concept of "Julian Date"
   (not to be confused with the Julian Calendar).  The Julian Date is
   the number of days since January 1, 4713BC, and give astronomers an
   easy way to calculate the time difference between two dates.  
   There exist formulas from converting a Gregorian calendar date into the
   appropriate Julian day number; this implements one of these
   after making an adjustment to the epoch, to base our times on
   6 January 1980.

   In particular, this formula is explained:
   http://www.tondering.dk/claus/cal/node3.html#sec-julianPeriod

Prototype:
   int StdTimeToJulianDay(AEEDateTime dt)

Parameters:
   dt: date and time

Return Value:
   Julian day

Comments:
   Ignores mm:hh:ss

=============================================================================*/
static int DateTimeToJulianDay(const AEEDateTime* pdt)
{
   uint32 u32Days;

   // Horrible variable names, but used to be consistent with the 
   // formula docs.
   //
   // The following implements the formula exactly as documented at:
   // http://www.tondering.dk/claus/cal/node3.html#sec-julianPeriod
   //
   uint32 a,y,m;

   a = (14 - pdt->wMonth)/12;
   y = pdt->wYear + 4800 - a;
   m = pdt->wMonth + 12*a - 3;

   u32Days = ((pdt->wDay + ((153*m + 2)/5) + (365*y + y/4 + y/400 - y/100)) -
              32045);

   return u32Days;
}

AEEResult stdtime_AddSeconds(const AEEDateTime* cpdtAddend, int nSecs, 
                                    AEEDateTime* pdtSum)
{
   uint32 dwSecsSum;
   uint32 dwDays;
   boolean bSubtract = FALSE;

   // Convert Time to seconds
   dwSecsSum = (cpdtAddend->wHour*60*60) + (cpdtAddend->wMinute*60) + cpdtAddend->wSecond;

   if (nSecs < 0) {
      // Subtracting time.
      // Since we are dealing with uint32s, there's no possibility of an
      // underflow.
      uint32 dwSecs = (uint32)(-nSecs);
      if (dwSecsSum > dwSecs) {
         // Subtracting less than a day. No need to modify the Julian Day Number
         dwSecsSum -= dwSecs;
      } else {
         // Subtracting more than a day. Need to adjust the Julian Day Number as well
         dwSecsSum = dwSecs - dwSecsSum;
         bSubtract = TRUE;
      }
   } else {
      // Adding time
      // This is the only scenario where we can possibly overflow when
      //    dwSecsSum + nSecs > MAX_UINT32
      // The max value that dwSecsSum can ever have is SECSINDAY. Thus for this
      // condition to be valid, nSecs > MAX_UINT32 - SECSINDAY. That's
      // impossible!
      dwSecsSum += (uint32)nSecs;
   }

   dwDays = DateTimeToJulianDay(cpdtAddend);
   if (bSubtract) {
      // Subtract the number of days first
      dwDays -= (dwSecsSum / SECSINDAY);
      // If we have any more seconds to be subtracted, then first make it one more 
      // day back and then find out how many seconds remain.
      // For example, if there are 100 more seconds that remain to be
      // subtracted, we first push the day back by one and then set the number
      // of seconds to be SECSINDAY - 100;
      if ((dwSecsSum %= SECSINDAY) != 0 ) {
         dwDays--;
         dwSecsSum = SECSINDAY - dwSecsSum;
      }
   } else {
      // Add the number of days first
      dwDays += (dwSecsSum / SECSINDAY);
      // Find out how many seconds remain
      dwSecsSum %= SECSINDAY;
   }

   // Convert the Julian days into Gregorian Date
   JulianDayToDateTime(dwDays, pdtSum);  

   // Convert the seconds last
   pdtSum->wSecond = (uint16)(dwSecsSum % 60);
   dwSecsSum /= 60;
   pdtSum->wMinute = (uint16)(dwSecsSum % 60);
   pdtSum->wHour = (uint16)(dwSecsSum / 60);

   return AEE_SUCCESS;
}

AEEResult stdtime_GetDiffSeconds(const AEEDateTime *cpdtMinuend,
                                 const AEEDateTime *cpdtSubtrahend, int *pnDifference)
{
   uint32 dwDaysMinuend;
   uint32 dwDaysSubtrahend;
   uint32 dwSecsMinuend;
   uint32 dwSecsSubtrahend;
   uint32 dwDaysDiff = 0;
   uint32 dwSecsDiff = 0;

   dwDaysMinuend = DateTimeToJulianDay(cpdtMinuend);
   dwDaysSubtrahend = DateTimeToJulianDay(cpdtSubtrahend);

   dwSecsMinuend = (cpdtMinuend->wHour*60*60) + (cpdtMinuend->wMinute*60) +
                   cpdtMinuend->wSecond;
   dwSecsSubtrahend = (cpdtSubtrahend->wHour*60*60) + (cpdtSubtrahend->wMinute*60) +
                      cpdtSubtrahend->wSecond;

   dwDaysDiff = (dwDaysMinuend - dwDaysSubtrahend)*SECSINDAY;
   dwSecsDiff = dwSecsMinuend - dwSecsSubtrahend;

   /* OVFERFLOW CONDITIONS
      ====================
    
      Consider two dates X and Y:
         X = {X.d, X.s}
         Y = {Y.d, Y.s}, where .d stands for days and .s stands for seconds
      When computing the difference between the two dates X and Y, overflow can
      occur if X > Y and:
            X - Y > MAX_INT32
         => (X.d - Y.d) + (X.s - Y.s) > MAX_INT32
      
      That can happen if:
            A. (X.d - Y.d) > MAX_INT32
         OR B. (X.s - Y.s) > MAX_INT32 :Impossible as both X.s, Y.s < MAX_INT32
         OR C. (X.d - Y.d) > MAX_INT32 - (X.s - Y.s)
      
      Now, X > Y, iff.
            1. (X.d > Y.d)
         OR 2. (X.d == Y.d) && (X.s > Y.s)
      In (2) above, overflow condition (C) would then translate to
            0 > MAX_INT32 - (X.s - Y.s)
      which is impossible as (X.s - Y.s) < MAX_INT32
      
      Thus, overflow occurs if:
            X.d > Y.d
        AND (X.d - Y.d) > MAX_INT32 - (X.s - Y.s)
   */   

   if ((dwDaysMinuend > dwDaysSubtrahend) && 
       (dwDaysDiff > MAX_INT32 - dwSecsDiff)) {
         return STD_OVERFLOW;
      }

   /* UNDERFLOW CONDITIONS
      ====================
    
      When computing the difference, underflow can occur if X < Y and:
            X - Y < MIN_INT32
         => (X.d - Y.d) + (X.s - Y.s) < MIN_INT32
         => (X.d - Y.d) < MIN_INT32 - (X.s - Y.s)   ... (A)
      
      Now, X < Y iff.
            1. X.d < Y.d
         OR 2. (X.d == Y.d) && (X.s < Y.s)
      In (2) above, the underflow condition (A) would then translate to
            0 < MIN_INT32 - (X.s - Y.s)
      which is impossible since (X.s - Y.s) > MIN_INT32 always
      
      Thus, underflow occurs if:
            X.d < Y.d
        AND (X.d - Y.d) < MIN_INT32 - (X.s - Y.s)
   */

   if ((dwDaysMinuend < dwDaysSubtrahend) &&
       (dwDaysDiff < MIN_INT32 - dwSecsDiff)) {
      return STD_UNDERFLOW;
   }

   *pnDifference = dwDaysDiff + dwSecsDiff;

   return AEE_SUCCESS;
}

