// these functions are here to maintain binary compatibility with older 
//  versions of a1std.lib that export these function as externs.
// 
// newer releases have std_swapl() defined and std_swaps() to be static 
//  inlines in AEEstd.h.
unsigned long std_swapl(unsigned long ul)
{
   ul = ((ul & 0x00ff00ff) << 8) | ((ul>>8) & 0x00ff00ff);
   return (ul >> 16) | (ul << 16);
}

// A couple more instructions on ARM9:
//
// unsigned long std_swapl(unsigned long ul)
// {
//    return (((ul & 0x0ff) << 24) + ((ul & 0x0ff00) << 8) +
//            ((ul & 0x0ff0000) >> 8) + ((ul & 0xff000000) >> 24));
// }

