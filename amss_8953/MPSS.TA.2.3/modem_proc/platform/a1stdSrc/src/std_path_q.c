#include <string.h>
#include <stdio.h>

#include "AEEstd.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

// check equality, tolerating NULLs
#define STREQ(a,b) \
   ((a) == (b) || ((a) != NULL && (b) != NULL && 0 == std_strcmp(a,b)))

#define TEST(x) \
   do { if (!(x)) { \
      printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
      errs++; \
   } } while (0)

static int std_makepath_test(void)
{
   int i;
   int errs = 0;

   static const struct {
      const char* cpszDir;
      const char* cpszFile;
      const char* cpszOut;
      int         nRet;
   } makepath_tests[] = {
      { "0",  "1",  "0/1",  3,},
      { "0/", "1",  "0/1",  3,},
      { "0",  "/1", "0/1",  3,},
      { "0/", "/1", "0/1",  3,},
      { "/",  "/1", "/1",   2,},
      { "/",  "1",  "/1",   2,},
      { "",   "1",  "1",    1,},
      { "",   "",   "",     0,},
      { "0",  "",   "0/",   2,},
      { "01", "2",  "01/",  4,},
      { "01", "23", "01/",  5,},
      { "0123", "", "012",  5,},
   };

   for (i = 0; i < STD_ARRAY_SIZE(makepath_tests); i++) {
      int     nRet;
      char    szOut[5];

      szOut[sizeof(szOut)-1] = '\0';
      nRet = std_makepath(makepath_tests[i].cpszDir, 
                          makepath_tests[i].cpszFile, 
                          szOut, sizeof(szOut)-1);

      TEST(std_strlen(szOut) <= 3);
      TEST(nRet == makepath_tests[i].nRet);
      TEST(!std_strcmp(szOut, makepath_tests[i].cpszOut));
   }

   return errs;
}

static int std_splitpath_test(void)
{
   int i;
   int errs = 0;

   static const struct {
      const char* cpszPath;
      const char* cpszDir;
      const char* cpszOut;
   } splitpath_tests[] = {
      { "",                    "",      ""},
      { "",                    "/",     ""},
      { "/",                   "",      ""},
      { "/a",                  "",      "a"},
      { "/",                   "/",     ""},
      { "/dir1",               "",      "dir1"},
      { "/d",                  "d",     0,},
      { "/d",                  "/",     "d"},
      { "/d/",                 "/d",    ""},
      { "/d/f",                "/",     "d/f"},
      { "/d/f",                "/d",    "f"},
      { "/d/f",                "/d/",   "f"},
      { "/d/f/",               "/d",    "f/"},
      { "/d/f/",               "/d/f",  ""},
      { "/dir1/dir2/file.ext", "/dir1", "dir2/file.ext"},
      { "dir1/dir2",           "/dir1/", 0},
      { "/dir1///",            "/dir1",  "//"},
      { "/dir1//",             "/dir1/", "/"},
      { "/dir1/",              "/dir1//", ""},
      { "/dir1/dir2",          "/dir1//", 0},
      { "",                    "/dir",   0},
      { "//",                  "",       "/" },
      { "//",                  "/",      "/" },
      { "//",                  "//",     "" },
      { "/d/f",                "/d/f",   ""},
      { "/dir/file",           "/d",     0},
      { "/dir/file",           "/di",    0},
      { "/dir/file",           "/dir",   "file"},
      { "dir/file",            "dir",    "file"},
      { "dir/file",            "dir/file", ""}
   };

   for (i = 0; i < STD_ARRAY_SIZE(splitpath_tests); i++) {
      char *szOut;

      szOut = std_splitpath(splitpath_tests[i].cpszPath, 
                            splitpath_tests[i].cpszDir);

      if ( ! STREQ(szOut, splitpath_tests[i].cpszOut) ) {
         printf(__FILENAME__ ":%d: test #%d failed: %s != %s\n", __LINE__, 
                i+1, szOut, splitpath_tests[i].cpszOut);
      }
   }

   return errs;
}

static int std_cleanpath_test(void)
{
   int i;
   int errs = 0;

   static const struct {
      const char* cpszPath;
      const char* cpszClean;
   } cleanpath_tests[] = {
      { "",           "",   },
      { "/",          "/",  },

      /* "here"s, mostly alone */
      { "./",         "/",  },
      { "/.",         "/",  },
      { "/./",        "/",  },
      /* "up"s, mostly alone */
      { "..",         "",   },
      { "/..",        "/",  },
      { "../",        "/",  },
      { "/../",       "/",  },
                            
      /* fun with x */    
      { "x/.",        "x",   },
      { "x/./",       "x/",  },
      { "x/..",       "",    },
      { "/x/..",      "/",   },
      { "x/../",      "/",   },
      { "/x/../",     "/",   },
      { "/x/../..",   "/",   },
      { "x/../..",    "",    },
      { "x/../../",   "/",   },
      { "x/./../",    "/",   },
      { "x/././",     "x/",  },
      { "x/.././",    "/",   },
      { "x/../.",     "",    },
      { "x/./..",     "",    },
      { "../x",       "/x",  },
      { "../../x",    "/x",  },
      { "/../x",      "/x",  },
      { "./../x",     "/x",  },
                            
      /* double slashes */ 
      { "//",         "/",   },
      { "///",        "/",   },
      { "////",       "/",   },
      { "x//x",       "x/x", },
   };

      for (i = 0; i < STD_ARRAY_SIZE(cleanpath_tests); i++) {
         char buf[16];
         char* pcRet;

         std_strlcpy(buf, cleanpath_tests[i].cpszPath, sizeof(buf));

         pcRet = std_cleanpath(buf);

         TEST(!std_strcmp(buf, cleanpath_tests[i].cpszClean));
         TEST(pcRet == buf);
      }

      return errs;
   }

static int std_basename_test(void)
{
   int i;
   int errs = 0;

   static const struct {
      const char* cpszPath;
      const char* cpszBase;
   } basename_tests[] = {
      { "",           "",  },
      { "/",          "",  },
      { "x",          "x", },
      { "/x",         "x", },
      { "y/x",        "x", },
      { "/y/x",       "x", },
   };

   for (i = 0; i < STD_ARRAY_SIZE(basename_tests); i++) {
      char* pcRet;

      pcRet = std_basename(basename_tests[i].cpszPath);

      TEST(!std_strcmp(pcRet, basename_tests[i].cpszBase));
   }

   return errs;
}

int main(void)
{
   return 
   std_makepath_test() +
   std_splitpath_test() +
   std_cleanpath_test() +
   std_basename_test();
}

