#include <string.h>
#include <stdio.h>

#include "AEEstd.h"
#include "AEEstdtime.h"
#include "AEEStdErr.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define SECSINMIN      60
#define SECSINHOUR     (60 * SECSINMIN)
#define SECSINDAY      (24 * SECSINHOUR)
#define SECSINYEAR     (365 * SECSINDAY)
#define SECSINLEAPYEAR (SECSINYEAR + SECSINDAY)

#define TEST_START(x) \
   bFail = FALSE; TEST(x);

#define TEST(x) \
   do { \
      if (!(x)) { \
         bFail = TRUE; \
         printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
         errs++; \
      } \
   } while (0)

#define MsgSeconds(string, s) \
   do { \
         char gDay[][7] = { {'M','O','N','\0'}, {'T','U','S','\0'}, \
                            {'W','E','D','\0'}, {'T','H','U','\0'}, \
                            {'F','R','I','\0'}, {'S','A','T','\0'}, \
                            {'S','U','N','\0'} }; \
         AEEDateTime __dt; \
         (void)stdtime_AddSeconds(&dtGPSEpoch, s, &__pst); \
         printf("%s %d s -> (%s) %02d/%02d/%d %02d:%02d:%02d UTC\n", \
                   (string), (s), \
                   gDay[(__dt).wWeekDay], (__dt).wMonth, (__dt).wDay, \
                   (__dt).wYear, (__dt).wHour, (__dt).wMinute, (__dt).wSecond); \
   } while (0)

#define MsgDate(string, dt) \
   do { \
         char gDay[][7] = { {'M','O','N','\0'}, {'T','U','S','\0'}, \
                            {'W','E','D','\0'}, {'T','H','U','\0'}, \
                            {'F','R','I','\0'}, {'S','A','T','\0'}, \
                            {'S','U','N','\0'} }; \
         int nSecs; \
         (void)stdtime_GetDiffSeconds(&dtGPSEpoch, &(dt), &nSecs); \
         printf("%s %d s -> (%s) %02d/%02d/%d %02d:%02d:%02d UTC\n", \
                   (string), (nSecs), \
                   gDay[(dt).wWeekDay], (dt).wMonth, (dt).wDay, \
                   (dt).wYear, (dt).wHour, (dt).wMinute, (dt).wSecond); \
   } while (0)

#define SET_DATE_VALUE(dt, y, mo, d, h, mi, s, w) \
   STD_ZEROAT(&dt); \
   dt.wYear = y; \
   dt.wMonth = mo; \
   dt.wDay = d; \
   dt.wHour = h; \
   dt.wMinute = mi; \
   dt.wSecond = s; \
   dt.wWeekDay = w; 

static int stdtime_test(void)
{
   int errs = 0;
   boolean bFail = FALSE;

   AEEDateTime dt;
   AEEDateTime dt2;
   int seconds;

   // Test the baseline...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(0 == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test "negative" sts
   {
      AEEDateTime dt3;
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      SET_DATE_VALUE(dt, 1980, 1, 5, 23, 59, 59, 5);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2));
      TEST(-1 == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }

      SET_DATE_VALUE(dt, 2010, 3, 26, 0, 10, 0, 4);
      SET_DATE_VALUE(dt3, 2010, 3, 25, 23, 56, 40, 3);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt3, &dt, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dt, seconds, &dt2));
      TEST(-800 == seconds);
      TEST(0 == std_memcmp(&dt3, &dt2, sizeof(dt2)));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }
   }

   // Test "negative" year
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1979;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 5;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(-1 * SECSINYEAR == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }   

   // Test the year increment (leap year)...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1981;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 1;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(SECSINLEAPYEAR == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the year increment (non-leap year)...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1982;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 2;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(SECSINLEAPYEAR + SECSINYEAR == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the month increment...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 2;
      dt.wDay = 6;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 2;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(SECSINDAY*31 == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the day increment...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 7;
      dt.wHour = 0;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 0;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(SECSINDAY == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the hour increment
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 10;
      dt.wMinute = 0;
      dt.wSecond = 0;
      dt.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(10*SECSINHOUR == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the minute increment...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 10;
      dt.wMinute = 10;
      dt.wSecond = 0;
      dt.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(10*SECSINHOUR+10*SECSINMIN == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Test the second increment...
   {
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 10;
      dt.wMinute = 10;
      dt.wSecond = 7;
      dt.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
      (void)stdtime_AddSeconds(&dtGPSEpoch, seconds, &dt2);

      TEST_START(36607 == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // 1 second
   {
      AEEDateTime dt3;

      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      STD_ZEROAT(&dt3);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 6;
      dt.wHour = 23;
      dt.wMinute = 50;
      dt.wSecond = 50;
      dt.wWeekDay = 6;

      dt3.wYear = 1980;
      dt3.wMonth = 1;
      dt3.wDay = 6;
      dt3.wHour = 23;
      dt3.wMinute = 50;
      dt3.wSecond = 49;
      dt3.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dt3, &seconds);
      (void)stdtime_AddSeconds(&dt3, seconds, &dt2);

      TEST_START(1 == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }
   } 


   // Test the second increemnt (both times have hours) positive
   {
      AEEDateTime dt3;

      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      STD_ZEROAT(&dt3);
      
      dt.wYear = 1980;
      dt.wMonth = 1;
      dt.wDay = 7;
      dt.wHour = 23;
      dt.wMinute = 59;
      dt.wSecond = 59;
      dt.wWeekDay = 0;

      dt3.wYear = 1980;
      dt3.wMonth = 1;
      dt3.wDay = 6;
      dt3.wHour = 23;
      dt3.wMinute = 59;
      dt3.wSecond = 59;
      dt3.wWeekDay = 6;

      (void)stdtime_GetDiffSeconds(&dt, &dt3, &seconds);
      (void)stdtime_AddSeconds(&dt3, seconds, &dt2);

      TEST_START(SECSINDAY == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }
   } 

   // Test the second increemnt (both times have hours) negative
   {
      // Test a difference of one day
      AEEDateTime dt3;

      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      STD_ZEROAT(&dt3);
      
      SET_DATE_VALUE(dt, 1980, 1, 7, 23, 59, 59, 0);
      SET_DATE_VALUE(dt3, 1980, 1, 6, 23, 59, 59, 6);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt3, &dt, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dt, seconds, &dt2));
      TEST_START(-1 * SECSINDAY == seconds);
      TEST(0 < std_memcmp(&dt, &dt3, sizeof(dt)));
      if (bFail) {
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }

      // Test a difference of less than a day
      SET_DATE_VALUE(dt, 2010, 3, 26, 0, 0, 0, 4);
      SET_DATE_VALUE(dt3, 2010, 3, 25, 10, 0, 0, 3);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt3, &dt, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dt, seconds, &dt2));
      TEST(-50400 == seconds);
      TEST(0 == std_memcmp(&dt3, &dt2, sizeof(dt3)));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }

      // Test a difference of just more than a day
      SET_DATE_VALUE(dt, 2010, 3, 26, 0, 0, 0, 4);
      SET_DATE_VALUE(dt3, 2010, 3, 24, 23, 59, 58, 2);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt3, &dt, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dt, seconds, &dt2));
      TEST(-1 * (SECSINDAY+2) == seconds);
      TEST(0 == std_memcmp(&dt3, &dt2, sizeof(dt3)));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }

      // Test a difference of more than two days
      SET_DATE_VALUE(dt, 2010, 3, 26, 0, 10, 0, 4);
      SET_DATE_VALUE(dt3, 2010, 3, 22, 23, 59, 58, 0);
      TEST_START(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt3, &dt, &seconds));
      TEST(AEE_SUCCESS == stdtime_AddSeconds(&dt, seconds, &dt2));
      TEST(-259802 == seconds);
      TEST(0 == std_memcmp(&dt3, &dt2, sizeof(dt3)));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }

      SET_DATE_VALUE(dt, 2010, 3, 26, 0, 0, 0, 4);
      (void)stdtime_AddSeconds(&dt, -86402, &dt2);
   }  

   // Test large days...
   {
      AEEDateTime dt3;

      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      STD_ZEROAT(&dt3);

      dt.wYear = 2100;
      dt.wMonth = 3;
      dt.wDay = 4;
      dt.wHour = 11;
      dt.wMinute = 55;
      dt.wSecond = 0;
      dt.wWeekDay = 3;

      dt3.wYear = 2050;
      dt3.wMonth = 1;
      dt3.wDay = 1;
      dt3.wHour = 0;
      dt3.wMinute = 0;
      dt3.wSecond = 0;
      dt3.wWeekDay = 4;

      (void)stdtime_GetDiffSeconds(&dt, &dt3, &seconds);
      (void)stdtime_AddSeconds(&dt3, seconds, &dt2);

      TEST_START(1583236500UL == seconds);
      TEST(0 == std_memcmp(&dt, &dt2, sizeof(dt)));

      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
      }
   }

   // Year 2100 is tricky, because Gregorian != Gregorian considering it leap....
   {
      AEEDateTime dt3;

      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);
      STD_ZEROAT(&dt3);
      
      dt.wYear = 2100;
      dt.wMonth = 2;
      dt.wDay = 28;
      dt.wHour = 02;
      dt.wMinute = 15;
      dt.wSecond = 45;
      dt.wWeekDay = 6;

      dt3.wYear = 2099;
      dt3.wMonth = 1;
      dt3.wDay = 1;
      dt3.wHour = 0;
      dt3.wMinute = 0;
      dt3.wSecond = 0;
      dt3.wWeekDay = 3;

      (void)stdtime_GetDiffSeconds(&dt, &dt3, &seconds);
      seconds += SECSINDAY;
      (void)stdtime_AddSeconds(&dt3, seconds, &dt2);

      TEST_START((dt2.wYear == 2100) && (dt2.wMonth == 3) &&
                 (dt2.wDay == 1) && (dt2.wHour == 2) &&
                 (dt2.wMinute == 15) && (dt2.wSecond == 45) &&
                 (dt2.wWeekDay == 0));

      if (bFail) {
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }
   }

   // Test really large days (overflow, actually)
   {
      AEEDateTime dt3;
      STD_ZEROAT(&dt);
      STD_ZEROAT(&dt2);

      TEST_START(AEE_SUCCESS == stdtime_AddSeconds(&dtGPSEpoch, MAX_INT32, &dt));
      // This call should not overflow
      TEST(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      TEST(seconds == MAX_INT32);
      // Now lets make this overflow
      dt.wSecond++;
      TEST(STD_OVERFLOW == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         printf("Seconds: %d\n", seconds);
      }
            
      TEST_START(AEE_SUCCESS == stdtime_AddSeconds(&dtGPSEpoch, MIN_INT32, &dt));
      // This call should not underflow
      TEST(AEE_SUCCESS == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      TEST(seconds == MIN_INT32);
      // Now lets make this underflow
      dt.wSecond--;
      TEST(STD_UNDERFLOW == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         printf("Seconds: %d\n", seconds);
      }

      SET_DATE_VALUE(dt, 2116, 2, 12, 6, 28, 16, 6);
      TEST_START(STD_OVERFLOW == stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds));
      if (bFail) {
         MsgDate("GSP Epoch: ", dtGPSEpoch);
         MsgDate("dt: ", dt);
         printf("Seconds: %d\n", seconds);
      }

      // Should not overflow as we handle all internal calculations as uint32
      SET_DATE_VALUE(dt, 1980, 1, 6, 12, 0, 0, 0);
      SET_DATE_VALUE(dt3, 2048, 1, 24, 3, 14, 8, 4);
      TEST_START(AEE_SUCCESS == stdtime_AddSeconds(&dt, MAX_INT32-(12*60*60)+1, &dt2));
      TEST(0 == std_memcmp(&dt3, &dt2, sizeof(dt2)));
      if (bFail) {
         MsgDate("dt: ", dt);
         MsgDate("dt2: ", dt2);
         MsgDate("dt3: ", dt3);
      }
   }

   // Test a large number of sts (Convert to Gregorian and Back)
   {
      uint32 current = 0;

      while(current < (0x7FFFFFFF - 86501)) {
         (void)stdtime_AddSeconds(&dtGPSEpoch, current, &dt);
         (void)stdtime_GetDiffSeconds(&dt, &dtGPSEpoch, &seconds);
         TEST(current == seconds);
         current += 86507;
      }
   }

   return errs;
}

int main(void)
{
   return stdtime_test();
}

