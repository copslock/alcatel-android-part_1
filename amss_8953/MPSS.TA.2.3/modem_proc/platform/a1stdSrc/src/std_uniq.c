#include "AEEstd.h"

int std_uniq(void* vpElems, int nNumElems, int nElemWidth, 
             int (*pfnCompare)(void*, const void*, const void*),
             void* pCompareCx)
{
   char* pElems = vpElems;
   char* pElemsDst = vpElems;
   char* pElemsEnd = pElems + (nNumElems * nElemWidth);
   int   nNumUniq = nNumElems == 0 ? 0 : 1;

   while (pElems < pElemsEnd) {

      if (0 != pfnCompare(pCompareCx, pElems, pElemsDst)) {
         pElemsDst += nElemWidth;
         std_memmove(pElemsDst, pElems, nElemWidth);
         nNumUniq += 1;
      }

      pElems += nElemWidth;
   }

   return nNumUniq;
}
