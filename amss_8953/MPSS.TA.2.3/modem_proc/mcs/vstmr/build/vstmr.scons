#===============================================================================
#
# VSTMR Scons
#
# Copyright (c) 2015 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary

# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.

# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/mcs.mpss/5.0/vstmr/build/vstmr.scons#7 $
#
#===============================================================================
from glob import glob
from os.path import join, basename

Import('env')

if 'USES_MCS_VSTMR' not in env:
   Return()

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Necessary API's for test purposes
#-------------------------------------------------------------------------------
env.RequirePublicApi(['QTF'], area='MOB')

#-------------------------------------------------------------------------------
# Add flags for FW standalone
#-------------------------------------------------------------------------------
if 'USES_STANDALONE_FW' in env:
  env.Append(CPPDEFINES = "FEATURE_VSTMR_STANDALONE")

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
VSTMR_SRC  = ['${BUILDPATH}/' + basename(fname)
             for fname in glob(join(env.subst(SRCPATH), '*.c'))]
VSTMR_SRC += ['${BUILDPATH}/' + basename(fname)
             for fname in glob(join(env.subst(SRCPATH), '*.cpp'))]

# Compile the vstmr source and convert to a library
env.AddBinaryLibrary(['MODEM_MODEM','MOB_MCS_VSTMR'],
                     '${BUILDPATH}/vstmr',
                     VSTMR_SRC,
                     pack_exception=['USES_CUSTOMER_GENERATE_LIBS', 'USES_COMPILE_L1_OPT_AC_PROTECTED_LIBS'])

#-------------------------------------------------------------------------------
# Load test units
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

#-------------------------------------------------------------------------------
# RCInit...
#-------------------------------------------------------------------------------

if 'USES_MODEM_RCINIT' in env:
   RCINIT_IMG = ['MODEM_MODEM']
   env.AddRCInitFunc(           # Code Fragment in TMC: NO
    RCINIT_IMG,                 
    {
     'sequence_group'             : 'RCINIT_GROUP_3',                   # required
     'init_name'                  : 'vstmr_init',                       # required
     'init_function'              : 'vstmr_init',                       # required
     'dependencies'               : ['mcpm'],
     'policy_optin'               : ['default', 'ftm', ],
    })
   env.AddRCInitTask(
    RCINIT_IMG,
    {
     'sequence_group'             : 'RCINIT_GROUP_3',
     'thread_name'                : 'vstmr_epoch',
     'thread_type'                : 'RCINIT_TASK_POSIX',
     'thread_entry'               : 'vstmr_epoch_task',
     'stack_size_bytes'           : '4096',
     'priority_amss_order'        : 'VSTMR_PRI_ORDER',
     'policy_optin'               : ['default', 'ftm', ],
    })
