/*!
  @file
  fws_qsh.h

  @brief
  FWS QSH internal header.

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

$Header: 
===========================================================================*/
#ifndef FWS_QSH_H
#define FWS_QSH_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "qsh.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! client major version */
#define FWS_QSH_VER_MAJOR 1

/*! client minor version */
#define FWS_QSH_VER_MINOR 0

/*===========================================================================

                    FUNCTION PROTOTYPES

===========================================================================*/
#ifdef FEATURE_QSH_DUMP
void fws_qsh_dump_collect_cb(qsh_client_cb_params_s * qsh_params);
#endif

#endif /* FWS_QSH_H */
