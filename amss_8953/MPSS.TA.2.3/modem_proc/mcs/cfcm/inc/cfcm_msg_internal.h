/*!
  @file
  cfcm_msg_internal.h

  @brief
  Internal CFCM related UMIDs.

  @author
  rohitj

*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/cfcm/inc/cfcm_msg_internal.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/04/15   rj      CFCM re-Design changes
11/21/14   rj      Adding MDM_TEMP and VDD_PEAK_CURR monitors
11/20/14   rj      Log Packet support added in CFCM
10/17/14   rj      Added supoort for Thermal RunAway Monitor
09/24/14   rj      initial version
==============================================================================*/

#ifndef CFCM_MSG_INTERNAL_H
#define CFCM_MSG_INTERNAL_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "cfcm.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief monitor state change indication message format
*/
typedef struct
{
  uint32           cpu_load;      /*!< cpu_load */
  uint32           max_cpu_freq;  /*!< MAX CPU Freq in KHz */
  uint32           curr_cpu_freq; /*!< Current CPU Freq in KHz */
} cfcm_cpu_load_type_s;

typedef struct
{
  uint32           state;  /*!< pa_state */
} cfcm_npa_node_type_s;

typedef struct
{
  uint8           level;  /*!< Thermal RunAway state */
} cfcm_bw_monitor_type_s;

typedef struct
{
  uint32                       client_mask; /*!< clients for which this indication is valid */
  cfcm_cmd_e                   cmd;         /*!< CFCM Command */
  cfcm_dsm_mem_level_e         level;       /*!< operation that triggered the event */
  dsm_mempool_id_enum_type     pool_id;     /*!< pool triggered the event */
  dsm_mem_level_enum_type      event;       /*!< level reached */
  dsm_mem_op_enum_type         op;          /*!< operation that triggered the event */
} cfcm_dsm_monitor_type_s;

/*----------------------------------------------------------------------------
  Union for all monitor data type
----------------------------------------------------------------------------*/
typedef union
{

  /* CPU load % Data */
  cfcm_cpu_load_type_s          cpu_info;

  /* Thermal PA/RunAway/MDM Temp/VDD Peak Curr monitor Data */
  cfcm_npa_node_type_s          npa_node;

  /* DSM monitor Data */
  cfcm_dsm_monitor_type_s       dsm;

  /* Bus Bandwidth monitor Data */
  cfcm_bw_monitor_type_s        bw;

} cfcm_monitor_data;

/*! @brief monitor state change indication message format
*/
typedef struct
{
  /*!< standard msgr header */
  msgr_hdr_struct_type     hdr;

  /* What Monitor data (commands/monitor-input) sent with this indication */
  cfcm_monitor_e           monitor_id;

  /* Union of monitor data sent */
  cfcm_monitor_data        monitor_data;
} cfcm_monitor_ind_msg_s;


/*=============================================================================

                        MSGR UMID Definitions

=============================================================================*/

/*! @brief These Indications are used for CFCM Flow Control 
 */
enum 
{
  /*! MCS_CFCM_MONITOR_IND msg which will 
           be used by CFCM to post monitor indications internally */
  MSGR_DEFINE_UMID( MCS, CFCM, IND, MONITOR,
                    CFCM_MONITOR_ID, cfcm_monitor_ind_msg_s)

};

/*==============================================================================

                    INTERNAL FUNCTION PROTOTYPES

==============================================================================*/



#endif /* CFCM_MSG_INTERNAL_H */
