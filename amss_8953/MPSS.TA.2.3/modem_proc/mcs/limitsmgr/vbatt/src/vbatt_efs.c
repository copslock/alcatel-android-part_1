/*!
  @file
  vbatt_efs.c

  @brief
  This file implements reading and looking up VBATT limits from EFS
*/

/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/limitsmgr/vbatt/src/vbatt_efs.c#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
05/29/15   tl      Initial revision

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "vbatt_i.h"
#include "vbatt_efs.h"
#include "lmtsmgr_task.h"
#include "lmtsmgr_translation.h"
#include "lmtsmgr_diag.h"
#include "fs_public.h"
#include "fs_errno.h"
#include "queue.h"
#include "mcsprofile.h"

/*=============================================================================
                                   TYPEDEFS
=============================================================================*/

typedef struct
{
  q_link_type           link;
  vbatt_record_type     record;
} vbatt_queue_record_type;

/*=============================================================================
                              INTERNAL VARIABLES
=============================================================================*/

#if !defined( FEATURE_MCS_TABASCO )

q_type vbatt_efs_records;

#endif /* !FEATURE_MCS_TABASCO */

/*=============================================================================
                              INTERNAL FUNCTIONS
=============================================================================*/

#if !defined( FEATURE_MCS_TABASCO )

/*=============================================================================

  FUNCTION:  vbatt_efs_read

=============================================================================*/
/*!
    @brief
    Reads the VBATT records from the specified EFS file for the specified tech

    @param[in]  file    The file name to read
    @param[in]  tech    The technology corresponding to the file name

    @return
    None
*/
/*===========================================================================*/
static void vbatt_efs_read(const char *file, cxm_tech_type tech)
{
  int32 fd;
  int result, i;
  int count = 0;
  int16 version;
  vbatt_efs_record_type efs_record;
  vbatt_queue_record_type *queue_record;

  fd = efs_open(file, O_RDONLY, ALLPERMS);
  if(fd < 0)
  {
    LMTSMGR_MSG_2(ERROR, "Error %d opening EFS file for tech %d", fd, tech);
    return;
  }

  /* Read the EFS version number */
  result = efs_read(fd, &version, sizeof(version));
  if(result <= 0 || result != sizeof(version))
  {
    LMTSMGR_MSG_2(ERROR, "Error %d reading EFS version for tech %d",
        efs_errno, tech);
    efs_close(fd);
    return;
  }

  if(version != VBATT_EFS_VERSION)
  {
    LMTSMGR_MSG_3(ERROR,
        "EFS version mismatch for tech %d: Expected version %d read version %d",
        tech, VBATT_EFS_VERSION, version);
    efs_close(fd);
    return;
  }

  /* Read the EFS records */
  for(;;)
  {
    result = efs_read(fd, &efs_record, sizeof(efs_record));

    /* Check to see if the read failed. If it did, then determine
     * why it failed. */
    if(result <= 0)
    {
      LMTSMGR_MSG_1(HIGH, "EFS error %d while reading VBATT record",
          efs_errno);
      break;
    }

    /* If read passed, check if we read the expected number of bytes */
    if(result != sizeof(efs_record))
    {
      LMTSMGR_MSG_1(MED, "Could not read a complete record of size %d",
                 sizeof(efs_record));
      break;
    }

    /* Allocate memory for the new record */
    queue_record =
      modem_mem_alloc(sizeof(vbatt_queue_record_type), MODEM_MEM_CLIENT_MCS);
    if(queue_record == NULL)
    {
      LMTSMGR_MSG_2(HIGH, "Out of memory allocating %d bytes for tech %d",
                 sizeof(vbatt_queue_record_type), tech);
      break;
    }

    q_link(queue_record, &queue_record->link);

    /* Copy the data from EFS to memory */
    queue_record->record.band =
      lmtsmgr_trans_rf_bands_to_sys_bands(tech, efs_record.band);
    for(i = 0; i < VBATT_STAGES_MAX; i++)
    {
      queue_record->record.stages[i].tx_power_limit =
        efs_record.stages[i].tx_power_limit;
      queue_record->record.stages[i].voltage_up =
        efs_record.stages[i].voltage_up;
      queue_record->record.stages[i].voltage_down =
        efs_record.stages[i].voltage_down;
    }
    queue_record->record.time_hysteresis = efs_record.time_hysteresis;

    /* Insert the new record into the queue */
    q_put(&vbatt_efs_records, &queue_record->link);

    count++;
  }

  efs_close(fd);

  LMTSMGR_MSG_2(LOW, "Read %d record(s) for tech %d", count, tech);
}

/*=============================================================================

  FUNCTION:  vbatt_lookup_func

=============================================================================*/
/*!
    @brief
    Comparator function for vbatt_lookup() to look up a VBATT record in the
    queue by the specified band

    @param[in]  item_ptr        Pointer to the record, which is expected to be
                                of type vbatt_queue_record_type
    @param[in]  compare_val     Pointer to the band to compare the record to,
                                which is expected to be of type
                                sys_band_class_e_type

    @return
    Non-zero if item_ptr matches compare_val; zero otherwise
*/
/*===========================================================================*/
static int vbatt_lookup_func(void *item_ptr, void *compare_val)
{
  const vbatt_queue_record_type *record;
  const sys_band_class_e_type *compare_band;

  ASSERT(item_ptr);
  ASSERT(compare_val);

  record = (const vbatt_queue_record_type *)item_ptr;
  compare_band = (const sys_band_class_e_type *)compare_val;

  return (record->record.band == *compare_band);
}

/*=============================================================================

  FUNCTION:  vbatt_validate_record

=============================================================================*/
/*!
    @brief
    Checks the contents of the record to verify that it is internally
    consistent

    @param[in]  record  The vbatt record to validate

    @return
    TRUE if the record meets the validation criteria; FALSE otherwise
*/
/*===========================================================================*/
static boolean vbatt_validate_record(const vbatt_record_type *record)
{
  if(record->stages[0].voltage_down > record->stages[0].voltage_up)
  {
    LMTSMGR_MSG_3(ERROR, "Band %d: stage 1 voltage_down %d > voltage_up %d",
        record->band,
        record->stages[0].voltage_down,
        record->stages[0].voltage_up);
    return FALSE;
  }

  if(record->stages[1].voltage_down > record->stages[1].voltage_up)
  {
    LMTSMGR_MSG_3(ERROR, "Band %d: stage 2 voltage_down %d > voltage_up %d",
        record->band,
        record->stages[1].voltage_down,
        record->stages[1].voltage_up);
    return FALSE;
  }

  if(record->stages[1].voltage_up > record->stages[0].voltage_up)
  {
    LMTSMGR_MSG_3(ERROR,
        "Band %d: stage 2 voltage_up %d > stage 1 voltage_up %d",
        record->band,
        record->stages[1].voltage_up,
        record->stages[0].voltage_up);
    return FALSE;
  }

  if(record->stages[1].voltage_down > record->stages[0].voltage_down)
  {
    LMTSMGR_MSG_3(ERROR,
        "Band %d: stage 2 voltage_down %d > stage 1 voltage_down %d",
        record->band,
        record->stages[1].voltage_down,
        record->stages[0].voltage_down);
    return FALSE;
  }

  return TRUE;
}

#endif /* !FEATURE_MCS_TABASCO */

/*=============================================================================
                               PUBLIC FUNCTIONS
=============================================================================*/

/*=============================================================================

  FUNCTION:  vbatt_efs_init

=============================================================================*/
/*!
    @brief
    Initializes this module's internal data structures and reads VBATT records
    from EFS for the supported techs LTE and GSM.

    @return
    None
*/
/*===========================================================================*/
void vbatt_efs_init(void)
{
#if !defined( FEATURE_MCS_TABASCO )
  q_init(&vbatt_efs_records);

  /* Read vbatt records for LTE */
  vbatt_efs_read(LIMITSMGR_VBATT_LTE_LIMIT, CXM_TECH_LTE);
  /* Read vbatt records for all GSM subscriptions */
  vbatt_efs_read(LIMITSMGR_VBATT_GSM_LIMIT, CXM_TECH_GSM1);
#endif /* !FEATURE_MCS_TABASCO */
}

/*=============================================================================

  FUNCTION:  vbatt_efs_deinit

=============================================================================*/
/*!
    @brief
    Frees all memory allocated by vbatt_efs_init().

    @return
    None
*/
/*===========================================================================*/
void vbatt_efs_deinit(void)
{
#if !defined( FEATURE_MCS_TABASCO )
  q_link_type *link;

  /* Free all memory used by the queue */
  while((link = q_get(&vbatt_efs_records)))
  {
    modem_mem_free(link, MODEM_MEM_CLIENT_MCS);
  }

  q_destroy(&vbatt_efs_records);
#endif /* !FEATURE_MCS_TABASCO */
}

#if !defined( FEATURE_MCS_TABASCO )

/*=============================================================================

  FUNCTION:  vbatt_lookup

=============================================================================*/
/*!
    @brief
    Looks up a VBATT record for the specified band.

    @param[in]  band    The tech/band to look up

    @return
    A pointer to the VBATT record if the band could be found; NULL otherwise.
*/
/*===========================================================================*/
const vbatt_record_type * vbatt_lookup(sys_band_class_e_type band)
{
  const vbatt_queue_record_type *queue_link;
  const vbatt_record_type *record = NULL;

  MCSMARKER(VBATT_LOOKUP_I);

  queue_link = (const vbatt_queue_record_type *)
    q_linear_search(&vbatt_efs_records, vbatt_lookup_func, &band);

  if(queue_link)
  {
    if(vbatt_validate_record(&queue_link->record))
    {
      vbatt_log_record(&queue_link->record, TRUE);
      record = &queue_link->record;
    }
    else
    {
      vbatt_log_record(&queue_link->record, FALSE);
    }
    LMTSMGR_MSG_2(LOW, "Found record %x for band %d", record, band);
  }
  else
  {
    LMTSMGR_MSG_1(MED, "Record for band %d not found", band);
  }

  MCSMARKER(VBATT_LOOKUP_O);

  return record;
}

#endif /* !FEATURE_MCS_TABASCO */
