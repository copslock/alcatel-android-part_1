
/*!
  @file
  wwan_coex_chnl_handler.c

  @brief
  This file implements the channel conflict of LIMTSMGR COEX module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/limitsmgr/wwan_coex/src/wwan_coex_chnl_handler.c#9 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
08/26/15   mb      Add calls to log chnl cflt
08/13/15   jm      Fix improper check for Max QTA Device Pairs in QTA gap info
08/04/15   sg      Fix for getting the right freq ID for QCTA conflicts
06/16/15   jm      Add history buffer for conflict checks
06/16/15   jm      Ignore dummy RF devices in channel conflict table
04/13/15   sg      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "mcs_variation.h"
#include "comdef.h"
#include "cxm.h"
#include "lmtsmgr_task.h"
#include "lmtsmgr_i.h"
#include "lmtsmgr_msgr.h"
#include "wwan_coex.h"
#include "wwcoex_conflict_table.h"
#include "wwan_coex_chnl.h"
#include "lmtsmgr_translation.h"
#include "stringl.h"

/*=============================================================================

                            GLOBAL

=============================================================================*/

extern  wwcoex_chnl_cflt_table_type wwcoex_chnl_cflt_tbls[WWCOEX_MAX_CHNL_CFLT_TBLS];

extern  wwcoex_query_cflt_hist_type wwcoex_cflt_chk_hist[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];
boolean cxm_qta_client[TRM_CLIENT_MAX];

cxm_qta_info_type wwcoex_qta_info_hist[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];

uint8 wwcoex_qta_info_hist_index = 0;


/*=============================================================================

                                FUNCTIONS

=============================================================================*/

/*============================================================================

FUNCTION CXM_GET_CHNL_CFLT_TBL

DESCRIPTION
  Function used to get a free table for QTA data
  
DEPENDENCIES
  None

RETURN VALUE
  int32 - Index of the table

SIDE EFFECTS
  None

============================================================================*/
static int32 cxm_get_free_chnl_cflt_tbl()
{
  uint32 i;
  int32 index = -1;

  for(i=0;i<WWCOEX_MAX_CHNL_CFLT_TBLS;i++)
  {
    if(wwcoex_chnl_cflt_tbls[i].is_valid == FALSE)
    {
      index = i;
      break;
    }
  }

  return index;
}

/*============================================================================

FUNCTION CXM_GET_CARRIER_FREQ_ID

DESCRIPTION
  Function used to get CA freq IDs for a given tech.
  
DEPENDENCIES
  None

RETURN VALUE
  Boolean
    TRUE - if the parameters are valid
    FALSE - if the parameters are invalid

SIDE EFFECTS
  None

============================================================================*/
static boolean cxm_get_carrier_freq_id
(
  cxm_tech_type   clid,
  uint16          *freqid,
  cxm_carrier_e   type
)
{
  uint32 i, count;
  lmtsmgr_tech_link_type  *link;

  if((clid <= CXM_TECH_DFLT_INVLD) ||
     (clid >= CXM_TECH_MAX))
  {
    return FALSE;
  }
  
  count = lmtsmgr.tech_state[clid].currList.num_entries;
  link  = lmtsmgr.tech_state[clid].currList.links;
    
  for(i=0;i<count;i++)
  {
    if((type == CXM_CARRIER_SCC_0) &&
       (link[i].freqInfo.link_info.type == CXM_LNK_TYPE_CA1))
    {
      *freqid = link[i].freqInfo.freqid;
    }
    else if((type == CXM_CARRIER_SCC_1) &&
            (link[i].freqInfo.link_info.type == CXM_LNK_TYPE_CA2))
    {
      *freqid = link[i].freqInfo.freqid;
    }
  }

  return TRUE;
}


/*============================================================================

FUNCTION CXM_QTA_UPDATE_INFO

DESCRIPTION
  Function used by tech TRM to update QTA related parameters to COEX.
  
DEPENDENCIES
  None

RETURN VALUE
  Boolean
    TRUE - if the parameters are valid
    FALSE - if the parameters are invalid

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_qta_update_info
(
  cxm_qta_info_type   *qta_info
)
{
  boolean is_valid_table;
  uint32 i,curr_tbl_idx;
  cxmfw_rat_type  idle_tech_fw, data_tech_fw;
  cxm_tech_type   data_tech;
  trm_client_type client_type;
  cxm_qta_association_type  *qta_params;
  wwcoex_chnl_cflt_result_type *arr1,*arr2;
  rfm_device_enum_type data_dev, idle_dev;
  uint16 freqid=0xFFFF;
  int32 new_tbl_idx = -1;
  
  /* check if input pointer is valid */
  ASSERT(qta_info != NULL);
  
  memscpy(&wwcoex_qta_info_hist[wwcoex_qta_info_hist_index], sizeof(cxm_qta_info_type),
          qta_info, sizeof(cxm_qta_info_type));

  /* Circular buffer reset */
  wwcoex_qta_info_hist_index = ((wwcoex_qta_info_hist_index + 1) % WWCOEX_MAX_CHNL_QUERY_HIST_SIZE);

  /* Validate num_entries */
  if ((qta_info->num_entries == 0) ||
      (qta_info->num_entries > CXM_QTA_MAX_DEVICE_PAIR))
  {
    LMTSMGR_MSG_1(HIGH, "WWCOEX: Invalid num_entries %d in QTA info!", qta_info->num_entries);
    // Send signal to log qta info before returning. 
    lmtsmgr_set_sigs(LMTSMGR_WWCOEX_CFLT_TBL_SIG);
    return FALSE;
  }

  /* Extract the idle tech ID from input */
  idle_tech_fw = lmtsmgr_trans_trm_client_to_fwcxm_client(qta_info->idle_clid);
  data_tech_fw = lmtsmgr_trans_trm_client_to_fwcxm_client(qta_info->data_clid);
  data_tech    = lmtsmgr_trans_trm_to_cxm_client(qta_info->data_clid);

  /* Validate tech ID */
  if((idle_tech_fw == CXM_FW_NOTECH) || 
     (data_tech_fw == CXM_FW_NOTECH))
  {
    LMTSMGR_MSG_2(HIGH, "WWCOEX: Invalid idle_tech_fw %d data_tech_fw %d!", 
                  idle_tech_fw, data_tech_fw);
    // Send signal to log qta info before returning. 
    lmtsmgr_set_sigs(LMTSMGR_WWCOEX_CFLT_TBL_SIG);
    return FALSE;
  }

  /* Handle start of QTA gap */
  if(qta_info->is_qta_start)
  {
    /* Find a free table. This API should always return valid index */
    new_tbl_idx = cxm_get_free_chnl_cflt_tbl();
    ASSERT(new_tbl_idx >= 0);

    /* Populate the table with data tech and idle information */
    wwcoex_chnl_cflt_tbls[new_tbl_idx].tech1 = (uint8)data_tech_fw;
    wwcoex_chnl_cflt_tbls[new_tbl_idx].tech2 = (uint8)idle_tech_fw;
    wwcoex_chnl_cflt_tbls[new_tbl_idx].is_valid = TRUE;

    /* Loop through the QTA entries and mark all applicable
       client types. This will be used to infer scenarios */
    for (i=0;i<qta_info->num_entries;i++)
    {
      qta_params = &(qta_info->qta_params[i]);
      client_type = qta_params->data_client_type;
      
      if(client_type < TRM_CLIENT_MAX)
      {
        cxm_qta_client[client_type] = TRUE;
      }
    }
    
    /* Loop through the QTA entries and add them to the table */
    for (i=0;i<qta_info->num_entries;i++)
    {
      qta_params = &(qta_info->qta_params[i]);
      data_dev = qta_params->data_dev;
      idle_dev = qta_params->idle_dev;
      client_type = qta_params->data_client_type;

      ASSERT(data_dev < RFM_MAX_DEVICES);
      ASSERT(idle_dev < RFM_MAX_DEVICES);

      /* Ignore dummy devices: channel conflict table
         only accounts for valid RF devices (0-7) */
      if(data_dev >= WWCOEX_MAX_RF_DEV ||
         idle_dev >= WWCOEX_MAX_RF_DEV)
      {
        LMTSMGR_MSG_2(HIGH, "Channel conflict for data-dev: %d idle-dev: %d ignored",
                      data_dev, idle_dev);
        continue;
      }
      
      arr1 = &(wwcoex_chnl_cflt_tbls[new_tbl_idx].arr[data_dev][idle_dev]);
      arr2 = &(wwcoex_chnl_cflt_tbls[new_tbl_idx].arr[idle_dev][data_dev]);

      /* If there are no partial conflicts
         then create channel conflict between the 2 channel for all freq IDs 
         identify the case as channel or filter conflict */
      if(qta_params->is_partial_conflict == FALSE)
      {
        /* If there are only diversity entry and not the
           corresponding primary then it is filter conflict */
        if((client_type == TRM_CLIENT_DIVERSITY) &&
           (cxm_qta_client[TRM_CLIENT_PRIMARY] == FALSE))
        {
          arr1->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
        }
        else if((client_type == TRM_CLIENT_CA_DIVERSITY) &&
                (cxm_qta_client[TRM_CLIENT_CA] == FALSE))
        {
          arr1->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
        }
        else if((client_type == TRM_CLIENT_CA1_DIVERSITY) &&
                (cxm_qta_client[TRM_CLIENT_CA1] == FALSE))
        {
          arr1->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_FILTER;
        }
        else
        {
          arr1->cflt_type = WWCOEX_CHNL_CFLT_LEGACY;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_LEGACY;
        }
      }
      /* If there are partial conflicts then also note the
         carrier frequency ID */
      else
      {
        if(client_type == TRM_CLIENT_PRIMARY_TX)
        {
          continue;
        }
        else if((client_type == TRM_CLIENT_CA) ||
                (client_type == TRM_CLIENT_CA_DIVERSITY))
        {
          cxm_get_carrier_freq_id(data_tech,
                                  &freqid,
                                  CXM_CARRIER_SCC_0);
          
          arr1->cflt_type = WWCOEX_CHNL_CFLT_CARRIER;
          arr1->freq_id = freqid;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_CARRIER;
          arr2->freq_id = freqid;
        }
        else if((client_type == TRM_CLIENT_CA1) ||
                (client_type == TRM_CLIENT_CA1_DIVERSITY))
        {
          cxm_get_carrier_freq_id(data_tech,
                                  &freqid,
                                  CXM_CARRIER_SCC_1);
          
          arr1->cflt_type = WWCOEX_CHNL_CFLT_CARRIER;
          arr1->freq_id = freqid;
          arr2->cflt_type = WWCOEX_CHNL_CFLT_CARRIER;
          arr2->freq_id = freqid;
        }
        else
        {
          /* Only QCTA can have partial conflicts */
          ASSERT(0);
        }
      }
    }
  }

  /* Handle end of QTA gap */
  else
  {
    /* Get the table corresponding to the tech Ids
       Always expected to return a valid table */
    is_valid_table = wwcoex_is_chnl_cflt_table_valid(
                       (uint32)idle_tech_fw,
                       (uint32)data_tech_fw,
                       &curr_tbl_idx);
    ASSERT(is_valid_table == TRUE);

    /* Clear the contents of the table */
    memset(&(wwcoex_chnl_cflt_tbls[curr_tbl_idx]),
               0, sizeof(wwcoex_chnl_cflt_table_type));
    
    /* clear QTA clients active mask */
    memset((void *)cxm_qta_client,0,
           sizeof(cxm_qta_client));
  }
  // Send signal to log qta info before returning. 
  lmtsmgr_set_sigs(LMTSMGR_WWCOEX_CFLT_TBL_SIG);
  return TRUE;
}


/*=============================================================================

  FUNCTION:  wwan_coex_spur_list_init

=============================================================================*/
/*!
    @brief
    Initialize spur related entries
 
    @details
      Initialize spur related entries
      
    @return
     void

*/
/*===========================================================================*/
void wwan_coex_chnl_cflt_init()
{

  /* Memset handles */
  memset((void *)wwcoex_chnl_cflt_tbls,0,sizeof(wwcoex_chnl_cflt_tbls));

  memset((void *)cxm_qta_client,0,sizeof(cxm_qta_client));
  
  memset(&wwcoex_qta_info_hist[0],0,sizeof(wwcoex_qta_info_hist));
  
  memset(&wwcoex_cflt_chk_hist[0],0,sizeof(wwcoex_cflt_chk_hist));

  return;
}
