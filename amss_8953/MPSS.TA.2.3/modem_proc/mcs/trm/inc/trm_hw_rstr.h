#ifndef TRM_HW_RSTR_H
#define TRM_HW_RSTR_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

           Transceiver Resource Manager HW Restriction Header File

GENERAL DESCRIPTION

  This file provides management of the HW restriction in support
  CRAT concurrency


EXTERNALIZED FUNCTIONS

  None

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  TRM HW restriction EFS should be available.


  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.


=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=



==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/mcs.mpss/5.0/trm/inc/trm_hw_rstr.h#5 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ---------------------------------------------------------
08/13/2015   sg      Added support to prioritize between LTE SCELLs
07/24/2015   sg      Added support to distinguish intraband vs. interband UL
06/29/2015   sg      Added support for RXFE NB
03/26/2015   sg      Initial revision

============================================================================*/


/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "trm.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

/* Number of clients that can be simultanesously queried */
#define TRM_HW_RSTR_MAX_QUERY   2
#define TRM_HW_RSTR_EFS         "/nv/item_files/mcs/trm/trm_hw_rstr"

/* Client IDs used for HW restrictions */
typedef enum
{
  TRM_HW_RSTR_CLIENT_TOTAL,
  TRM_HW_RSTR_CLIENT_LTE,
  TRM_HW_RSTR_CLIENT_LTE_CA1_INTER_DL,
  TRM_HW_RSTR_CLIENT_LTE_CA2_INTER_DL,
  TRM_HW_RSTR_CLIENT_LTE_CA1_INTRA_DL,
  TRM_HW_RSTR_CLIENT_LTE_CA2_INTRA_DL,
  TRM_HW_RSTR_CLIENT_LTE_CA1_INTER_UL,
  TRM_HW_RSTR_CLIENT_LTE_CA1_INTRA_UL,
  TRM_HW_RSTR_CLIENT_GSM,
  TRM_HW_RSTR_CLIENT_CDMA,
  TRM_HW_RSTR_CLIENT_WCDMA,
  TRM_HW_RSTR_CLIENT_WCDMA_CA1_INTER_DL,
  TRM_HW_RSTR_CLIENT_TDSCDMA, 
  TRM_HW_RSTR_CLIENT_MAX
}trm_hw_rstr_client_type;

/* Reason IDs used for HW restrictions */
typedef enum
{
  TRM_HW_RSTR_REASON_PRI,
  TRM_HW_RSTR_REASON_DLCA,
  TRM_HW_RSTR_REASON_ULCA,
  TRM_HW_RSTR_REASON_MAX
}trm_hw_rstr_reason_type;

/* Structure used for storing HW restrictions */
typedef struct
{
  trm_hw_rstr_client_type   clid;
  uint32                    cpu;
  uint32                    ccs;
  uint32                    vpe;
  uint32                    rxfe_nb;
}trm_hw_rstr_type;

/* Structure used for querying HW restrictions from scheduler */
typedef struct
{
  trm_client_enum_t             clid;
  uint64                        dev_sharing_bitmask;
  trm_reason_enum_t             reason;
  trm_unlock_any_prio_enum_type prio;
}trm_hw_rstr_input_type;

/* Query structure used by TRM scehduler */
typedef struct
{
  trm_hw_rstr_input_type    client[TRM_HW_RSTR_MAX_QUERY];
}trm_hw_rstr_query_type;

/* HW restriction in EFS */
typedef PACK(struct)
{
  uint32                    clid;
  uint32                    cpu;
  uint32                    ccs;
  uint32                    vpe;
}trm_hw_rstr_efs_type;


/*============================================================================

                            FUNCTION DECLARATIONS

============================================================================*/

/*============================================================================

FUNCTION TRM_QUERY_HW_RESTRICTION

DESCRIPTION
  This function is used to query HW restriction between a pair of clients
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE - if there are HW restriction
  FALSE - if there are no HW restriction

SIDE EFFECTS
  None

============================================================================*/
boolean trm_query_hw_rstr
(
  trm_hw_rstr_query_type *query
);


/*============================================================================

FUNCTION TRM_INIT_HW_RESTRICTION

DESCRIPTION
  This function initializes the HW restrictions that are applicable.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_init_hw_rstr(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* TRM_HW_RSTR_H */

