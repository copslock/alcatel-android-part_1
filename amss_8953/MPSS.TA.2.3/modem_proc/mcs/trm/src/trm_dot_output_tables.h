#ifndef TRM_DOT_OUTPUT_TABLES_H
#define TRM_DOT_OUTPUT_TABLES_H

/*===========================================================================

            T R M   D O T   O U T P U T   T A B L E S   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/trm/src/trm_dot_output_tables.h#3 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "trm_dot_init.h"
#include "trm_device_ordering.h"
#include "rfm_device_types.h"
#include "list.h"

#define TRM_MAX_BAND_GROUPS 245
/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/
typedef struct trm_dot_band_group_node_s
{
  list_link_type link;
  rfm_bands_bitmask band_mask;
  uint32 bg_index;
}trm_dot_band_group_node;

typedef struct trm_dot_device_order_set_node_s
{
  trm_device_order_set_list_type dev_order_set;
  uint8 index;
  struct trm_dot_device_order_set_node_s *next_ptr;
}trm_dot_device_order_set_node;

typedef struct trm_dot_standalone_table_node_s
{
  trm_standalone_dot_type standalone_dot_entry;
  struct trm_dot_standalone_table_node_s *next_ptr;
} trm_dot_standalone_table_node;

typedef struct 
{
  list_link_type link;
  trm_concurrency_dot_type conc_dot_entry;
} trm_dot_concurrency_table_node;

typedef struct
{
  trm_rat_group_type rat_group1;
  boolean is_tdd1;
  trm_rat_group_type rat_group2;
  boolean is_tdd2;
  uint32  band;
  boolean is_stdalone_tbl;
}trm_dot_query_band_group_input;

void trm_dot_get_band_group_node
(
  trm_rat_group_type rat_group1,
  boolean is_tdd1,
  trm_rat_group_type rat_group2,
  boolean is_tdd2,
  trm_dot_band_group_node** bg_node_ptr,
  uint32 *bg_index,
  boolean is_stdalone_tbl
);

uint8 trm_dot_add_device_order_set_to_list
(
  list_type *dev_list,
  uint32 set_num
);

void trm_dot_find_band_in_band_group
(
  trm_dot_query_band_group_input *bg_query_input,
  trm_dot_band_group_node** bg_node_ptr,
  uint32 *bg_index
);

uint32 trm_dot_get_first_band_from_group
(
  trm_rat_group_type rat_group1,
  boolean is_tdd1,
  trm_rat_group_type rat_group2,
  boolean is_tdd2,
  uint8 bg_idx,
  boolean is_stdalone_tbl 
);

void trm_dot_add_band_to_band_group
(
  trm_band_t band,
  rfm_bands_bitmask* band_mask
);

trm_dot_standalone_table_node* trm_dot_get_standalone_table_node(void);

void trm_dot_add_entry_to_concurrency_table
(
  uint64 conc_mask,
  uint32 t1_set_num,
  uint32 t2_set_num
);

uint32 trm_dot_get_max_stdalone_band_groups
(
  trm_rat_group_type rat_group,
  boolean tdd_flag
);

uint32 trm_dot_get_device_order_set_tbl
( 
  trm_device_order_set_list_type** dev_order_set_tbl_ptr
);

uint32 trm_dot_get_standalone_tbl 
( 
  trm_standalone_dot_type** stdalone_tbl_ptr
);

uint32 trm_dot_get_concurrency_tbl 
( 
  trm_concurrency_dot_type **conc_tbl_ptr
);

void trm_dot_output_band_groups(void);
void trm_dot_destroy_all_band_group_lists
(
  void
);

#endif /* TRM_DOT_OUTPUT_TABLES_H */
