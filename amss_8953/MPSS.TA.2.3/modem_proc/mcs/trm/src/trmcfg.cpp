/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

            T R A N S C E I V E R   R E S O U R C E   M A N A G E R

                Transceiver Resource Manager Configuration File

GENERAL DESCRIPTION

  This file supports configuration Transceiver Resource Manager


EXTERNALIZED FUNCTIONS

  None
  

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/mcs.mpss/5.0/trm/src/trmcfg.cpp#59 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
11/27/2015   pap     Adding changes for eMBMS.
10/30/2015   pap     Changes to correct priority for LTE CA clients in case of 
                     Sleep.
10/13/2015   sr      Make 1X Traffic incompatible with DO modes (CR: 914527)
09/24/2015   sr      Set eMBMS reason priority to 50 (CR: 913187)
09/24/2015   pap     Add changes to make L+W DR only when appropriate NV is set.
09/15/2015   mn      Added compatibility between HDR diversity and 1x (CR: 908172).
09/01/2015   sk      Make BG TRAFFIC for 1x valid(897417)
09/01/2015   mn      TRM does not block concurrency between LTE HORxD and GSM clients.
                      (CR: 899799).
08/27/2015   mn      TRM does not block concurrency between TDS Tx and 1x Rx
                      (CR: 897811).
08/25/2015   mn      Disallow concurrency between GSM and GPRS clients 
                      (CR: 895398).
08/24/2015   rj      Added support for CTCH (Broadcast MultiCast) channel
08/11/2015   sg      Allow IRAT + Data
08/03/2015   pap     Block IRAT + IRAT concurrency.
07/28/2015   sg      1X Acess and WCDMA Access are not compatible with LTE idle
07/21/2015   pap     Add changes to enable CM_INV reason for 1x.
07/20/2015   ag      Made HDR Access and 1X compatible in SHDR mode 
07/19/2015   sg      Remove 1X connected with any LTE idle concurrency
07/14/2015   mn      Added 1x and HDR diversity compatibilities in 
                      SHDR mode (CR: 870819).
07/10/2015   mn      Set DR compatibility for x + y technologies on 
                      Tabasco 2.0. (CR: 853466).
06/26/2015   mn      Block HDR + 1x IRAT concurrency (CR: 853521).
06/26/2015   rj      Added support for W CA Device Ordering
06/18/2015   mn      Block ACCESS + ACCESS concurrency (CR: 856500).
06/17/2015   sp      Added ACQ_CELL_SELECT AND ACQ_CELL_SELECT_INV DR reasons 
06/15/2015   sk      CM Inv made DR compatible(852789) 
06/10/2015   mn      W + 1x should always run in SR mode even if DR is enabled 
                      through NV. (CR: 851452).
05/19/2015   sp      Removed reasons and made other TRM-U fixes/cleanup (CR: 827122)
04/28/2015   mn      UMTS1 and UMTS2 should be compatible only in DR-DSDS 
                      mode. (CR: 829486).
04/14/2015   sp      Mapped old TRM reasons to new TRM-U reason priorities (for Phase 1)
03/19/2015   mn      HDR ACQ_DIV and ACCESS_DIV should not be DR capable (CR: 808564).
03/12/2015   sp      Added tech-based restriction for DR mode 
02/18/2015   sk      Update log v8 for eMBMS logging(796676) 
01/13/2015   mn      Increased the Priority LTE Total Control (TRM_LTE client). 
                      (CR: 778286).
01/28/2014   sk      Made ACQUISITION_HI DR incompatible(786126) 
01/13/2015   sk      WLAN priority made lower than CA priorities(728239) 
12/24/2014   mn      Enable TRM_LOW_LAT_TRAFFIC for use by LTE. (CR: 769352).
12/11/2014   mn      LTE EMBMS functional changes (CR: 769538).
11/14/2014   sr      Add a SLEEP Diversity reason for LTE to use for diversity
                     to avoid sending wrong unlocks due to PAM (CR: 751597) 
11/04/2014   sr      Make High Priority Signalling DR capable (CR:747380) 
11/04/2014   sr      Making LTE Diversity compatible with all other div clients
                     (CR: 747740).
11/04/2014   sr      LTE Div should be compatible with only DR reasons (CR:727672).
10/21/2014   sk      Div modes made compatible to DR modes only in DRDSDS(743095) 
10/24/2014   mn      Support for LTE HO RxD (CR: 744539)
10/20/2014   sk      Fix memory issue on offtarget(734841)
10/11/2014   mn      Increase LTE CA priority above LTE CA SEC (CR: 735178).
09/29/2014   mn      Added a new TRM_DIVERSITY_IRAT_MEASUREMENT reason (CR: 712921).
09/07/2014   sr      Make LTE Access a DR Capable reason(CR: 712328)
09/04/2014   ag      Made LTE TOTAL CONTROL > DO to avoid 1x2L reselect failure
08/12/2014   sr      IRAT should be made DR compatible for LTE(714342)
08/12/2014   ag      Made G ACQ_MAX > G RESELECT/_HI
08/05/2014   mn      Increase the SUBS_CAP priority to be above GSM RESELECT 
                       (CR: 700737).
07/23/2014   sr      GERAN using deprecated priority causing L2G resel/redir/CCO 
                     failure in DR-DSDS mode (700576)
07/23/2014   mn      Adding new GSM secondary clients (CR: 696634).
07/17/2014   mn      TRM should map 1x and GSM2 to chain 2 when LTE is holding 
                      both chain 0 and chain 1 (CR: 695108).
07/15/2014   sk      Added missing compatibility in SGLTE/SGTDS case (695500)
07/11/2014   mn      All diversity modes should be compatible with other modes 
                      in DR-DSDS mode (CR: 691656).
07/08/2014   mn      TRM: TDSCDMA secondary to be made compatible with GSM in 
                      DR mode. (CR: 690332).
06/15/2014   sk      WLAN antenna sharing changes (615092)
05/21/2014   mn      Changed 1x ACQ_INV mode to TRM_MODE_1X_ACQ. (CR: 668618).
05/19/2014   mn      Made D2L measurements incompatible with GSM SLTE modes
                      for SVLTE + G DSDA mode (CR: 667375).
05/16/2014   sk      G SLTe mode made compatible with W div for DSDA(664431)
03/25/2014   sk      New Reason for LTE high priority signalling(653561)
05/05/2014   sk      Remove incompatibility between W Div and G2 in DSDA 660131) 
04/20/2014   sk      LTE sleep priorityfor sec client cahnged (CR: 649238)
04/09/2014   sk      Changes to support 1x acq throttling (CR: 626286)
04/09/2014   sr      LTE Paging Priority should be 185 (CR: 644158) 
04/01/2014   mn      SLTE + CA support (CR: 616403).
04/03/2014   sk      New Reason for LTE emergency SIB Reads
03/20/2014   mn      Made the new SLTE GSM modes compatible with WCDMA diversity 
                      mode. (CR: 635616).
02/27/2014   sr      SLTE Changes.
03/12/2014   mn      Adding support for connected mode WTR hopping.
03/04/2014   mn      Made W div compatible with GSM client (CR: 616239).
02/24/2014   mn      Enabled CHANNEL_MAINTENANCE reason for HDR.
02/13/2014   sk      Added channel_maintenance_inv reason
01/23/2014   mn      Lowering the priority of SUBS_PRIO request to prevent 
                     unlock callbacks and denials of W/T in single SIM mode.
01/20/2014   sk      Added compatibility between TDS_DIV and GPS
01/03/2013   sk      Change the priorities for CA client to the lowest
01/05/2014   sr      In PBR, increments within 100 ms are redundant(CR:591479)
01/03/2013   sk      Reset the compatibility bitmask before setting new 
11/26/2013   sr      Change for Dime OMRD (CR:  589736)
11/26/2013   mn      Opp. SGLTE/SGTDS/SVLTE +G DSDA support.
11/25/2013   sr      PBR Algorithm Implementation
10/29/2013   sk      IRAT client priority changed to maximum
09/12/2013   sk      L+G DSDS support
08/29/2013   rj      Adding TRM support for G2W TRM_LTA_SUPPORTED
05/21/2013   mn      Adding TSTS support.
05/14/2013   rj      Added support for SGLTE in TRM
03/25/2013   rj      Updating TDSCDMA priority for Triton
02/19/2012   sr      HDR Diversity not working during acquisition.(CR:454030)
02/06/2012   sr      DO is not able acquire div when LTE is active.(CR: 449718)
02/04/2012   sr      Add missing GPS compatibilities(CR:447595)
02/04/2012   sr      Add a new priority TRM_ACQ_DIVERSITY for HDR(CR:439864)
12/04/2012   sr      Set 1X Div and HDR Div compatible for SVDO.
11/27/2012   mn      Triton DSDA changes.
11/26/2012   rj      Additional changes for adding TRM_CM client ID.
10/03/2012   mn      Nikel DSDS Merge.
08/13/2012   ag      Added new reason for non-urgent 1x rude wakeup
04/02/2012   ag      Make HDR IRAT < 1X DEMOD and 1X ACQ
02/16/2012   ag      Make DO IRAT compatible witk LTE and LTE div > 1x page
01/13/2012   ag      TRM Feature Cleanup  
01/10/2012   ag      Added two new diversity reasons 
12/20/2011   ag      Make GPS and HDR compatible in SVDO mode and LTE div>1xdiv
12/07/2011   ag      Remove (2,6) NV check for DO acq/idle with 1x idle compat.
11/09/2011   ns      Increase GPS priority > 1x acq and demod on secondary 
08/01/2011   sr      Made MODE_1X compatible with MODE_HDR_IDLE and MODE_HDR
08/01/2011   sr      Make 1x acquisition and DO compatible
08/15/2011   sg      Added support for TDSCDMA Layer1
06/14/2011   ag      Remove DORB featurization to make MCDO_TRAFFIC same as LPT.
06/09/2011   ag      Make DO mc_traffic < 1x acq
05/31/2011   ag      Make priority of interlock div same as diversity for 1x.
04/21/2011   ag      Added an API to check whether SHDR mode is supported.
04/11/2011   sg      Modified compatability setting for MCDO and 1X
02/10/2011   sg      Added support for ET and HDR-IRAT measurements
01/20/2011   sg      Added compatability for MCDO and 1X for DOrB 
11/22/2010   ag      Added new reason SMALL_SCI_CONTINUATION for HDR. 
10/18/2010   ag      Merged DSDS functionality.
08/23/2010   ag      Merged GSM/WCDMA reasons.
06/17/2010   ag      Added mode TRM_MODE_HDR_MCDO_CONNECTED to make the new 
                     MC_TRAFFIC reason incompatible with 1X in SHDR.
06/02/2010   hh      Added priority value of 50 to TRM_SYSTEM_MEASUREMENT 
                     reason in 1x secondary 
06/01/2010   hh      Added new reason TRM_SYSTEM_MEASUREMENT for 1x system 
                     measurement changes.
04/06/2010   sm      Added Support for PAM functionality
11/13/2009   ag      Added modem_mcs_defs.h for GENESIS modem.  
11/16/2009   ag      Changed GPS demod priority to 155 for non-FTS mode.
11/12/2009   ag      Fix KW error - array out of bounds.
10/19/2009   ag      Changed HDR acq. priority to 111 for non-FTS mode. 
08/28/2009   ag      Fixed some medium lint warnings.
07/31/2009   ag      Fixed lint warnings 
07/21/2009   ag      Merged INTLOCK and other Q6 changes from //depot
27/03/2009   ns      Clean up to support SCMM and 8650 builds only
03/13/2009   ag      Added a change for off target build (TRM_BYTES_PER_CLIENT)
02/17/2009   cpk     Added support for Off Target support
10/21/2008   aps     Lower HDR Acquisition priority
09/18/2008   rkc     Added TRM_DEMOD_PAGE_CONTINUATION to trmcfg tables.
09/09/2008   sg      Fix lint/klocwork warnings
08/28/2008   adw     Added FEATURE_MCS_TRM to featurize TRM for ULC.
06/06/2008   sg      Added high priority BCMCS support.
05/22/2008   sg      Integrated changes from old depot 
09/21/2007   grl     Added full time SHDR support.
08/01/2007   ebb     Updated TRM_BYTES_PER_CLIENT
05/24/2007   ebb     Changed TRM_BYTES_PER_CLIENT to account for 64 bit additions
                     in client struct.
10/17/2006   cab     Added TRM_DEMOD_SMALL_SCI_PAGE reason.
                     1x acq and hi pri HDR traffic always compatible
09/25/2006   cab     Allowed 1x acq simultaneous with HDR high pri traffic
09/15/2006   cab     Integrated UMTS
07/25/2006   cab     Lowered HDR access priority to support access hybrid
06/07/2006   cab     Allowed GPS to run concurrently with HDR HPT
06/06/2006   pa      Featurize 1x acq on secondary chain for 6800B/7500B
06/06/2006   pa      Use lower priority than GPS for 1x acq on secondary chain.
06/06/2006   pa      Enable 1x acquisition on secondary chain
05/02/2006   cab     Added ifdef's for offline compilation and testing
04/20/2006   grl     Increased the priority of GPS above 1x paging for 6800.
12/02/2005   awj     Added priority for 1x secondary demod commands
11/21/2005   cab     Moved NV item id's to compile time assign outside library
11/08/2005   grl     Lowered priority of broadcast so that 1x acq can run.
10/21/2005   grl     Added SGPS-DO compatibility and HDR div compatibility.
09/10/2005   grl     Added broadcast access/traffic reasons.
08/29/2005   grl     Added support for dynamic compatibility masks.
07/19/2005   grl     Added support for SGPS+1x acq and removed S-idle-idle.
07/11/2005   grl     Added TRM config mask bit for SHDR enabling.
06/21/2005   ajn     Linting changes.
06/14/2005   ajn     Mode table compile-time initialized.
06/13/2005   grl     Added support for 1x to be active on both RX1 and TXRX1.
05/26/2005   ajn     Code review comment changes
03/28/2005   ajn     Added GPS_TDM reason, and client/reason compatibility
03/02/2005   ajn     Initial AMSS version

=============================================================================*/



/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "mcs_variation.h"
#include <customer.h>
#include "trmcfg.h"
#include "stringl.h"
extern "C"
{
    #include "modem_mcs_defs.h"
    #include <err.h>
}
#include "trm_config_handler.h"

/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/

/*-----------------------------------------------------------------------------
  Locally defined TRM CFG related features
-----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
  Number of clients in trm_client_enum_t
-----------------------------------------------------------------------------*/

const uint32                  TRM::num_clients = TRM_MAX_CLIENTS;



/*-----------------------------------------------------------------------------
  NV item identifiers
-----------------------------------------------------------------------------*/

const nv_items_enum_type      TRM::nv_trm_config = NV_TRM_CONFIG_I;
const nv_items_enum_type      TRM::nv_gps_rf_config = NV_GPS_RF_CONFIG_I;

/*-----------------------------------------------------------------------------
  Client mode identifiers
-----------------------------------------------------------------------------*/

typedef enum
{
  /*-----------
    1X modes
  -----------*/

  /* General mode for no special restrictions */
  TRM_MODE_1X,

  TRM_MODE_1X_START = TRM_MODE_1X,

  TRM_MODE_1X_DR_CAPABLE,

  /* Acquisition may be incompatible with GPS */
  TRM_MODE_1X_ACQ,

  /* 1X in diversity */
  TRM_MODE_1X_DIV,

  /* 1X inter-rat measurement mode */
  TRM_MODE_1X_IRAT_MEASUREMENT,

  TRM_MODE_1X_ACCESS_MODE,

  TRM_MODE_1X_TRAFFIC,

  TRM_MODE_1X_END = TRM_MODE_1X_TRAFFIC,

  /*-----------
    HDR modes
  -----------*/

  /* General mode for no special restrictions */
  TRM_MODE_HDR,

  TRM_MODE_HDR_START = TRM_MODE_HDR,

  /* HDR in a traffic call */
  TRM_MODE_HDR_CONNECTED,

  /* HDR in idle  */
  TRM_MODE_HDR_IDLE,

  /* HDR in diversity */
  TRM_MODE_HDR_DIV,

  /* HDR in diversity */
  TRM_MODE_HDR_DIV_DR_CAPABLE,

  /* HDR in small SCI idle operation */
  TRM_MODE_HDR_SMALL_SCI_IDLE,

  /* HDR performing IRAT measurements on LTE */
  TRM_MODE_HDR_IRAT_MEASUREMENT,

  TRM_MODE_HDR_ACCESS_MODE,

  TRM_MODE_HDR_END = TRM_MODE_HDR_ACCESS_MODE,

  /*-----------
    UMTS1 modes
  -----------*/

  /* General mode for no special restrictions */
  TRM_MODE_UMTS1,

  TRM_MODE_UMTS1_START = TRM_MODE_UMTS1,

  /* WCDMA in diversity */
  TRM_MODE_UMTS1_DIV,

  /* Dual receive */
  TRM_MODE_UMTS1_DR_CAPABLE,

  TRM_MODE_UMTS1_ACCESS_MODE,

  TRM_MODE_UMTS1_IRAT_MEASUREMENT,

  TRM_MODE_UMTS1_CTCH_MODE,

  TRM_MODE_UMTS1_END = TRM_MODE_UMTS1_CTCH_MODE,

  /*----------- 
    UMTS_CA modes
  -----------*/

  /* Carrier aggregation */
  TRM_MODE_UMTS1_CA,

  TRM_MODE_UMTS_CA_START = TRM_MODE_UMTS1_CA,

  TRM_MODE_UMTS_CA_IRAT_MEASUREMENT,

  TRM_MODE_UMTS_CA_END = TRM_MODE_UMTS_CA_IRAT_MEASUREMENT,

  /*-----------
    UMTS2 modes
  -----------*/

  /* General mode for no special restrictions */
  TRM_MODE_UMTS2,

  TRM_MODE_UMTS2_START = TRM_MODE_UMTS2,

  /* WCDMA in diversity */
  TRM_MODE_UMTS2_DIV,

  /* Dual receive */
  TRM_MODE_UMTS2_DR_CAPABLE,

  /* Carrier aggregation */
  TRM_MODE_UMTS2_CA,

  TRM_MODE_UMTS2_ACCESS_MODE,

  TRM_MODE_UMTS2_IRAT_MEASUREMENT,

  TRM_MODE_UMTS2_CTCH_MODE,

  TRM_MODE_UMTS2_END = TRM_MODE_UMTS2_CTCH_MODE,

  /*-----------
    GSM1 modes
  -----------*/

  /* General mode for GSM1 and GSM2 clients */
  TRM_MODE_GSM1,

  TRM_MODE_GSM1_START = TRM_MODE_GSM1,

  /* GSM Modes which are capable of SLTE */
  TRM_MODE_GSM1_SLTE_CAPABLE,

  TRM_MODE_GSM1_DR_CAPABLE,

  TRM_MODE_GSM1_DIV,

  TRM_MODE_GSM1_ACCESS_MODE,

  TRM_MODE_GSM1_IRAT_MEASUREMENT,

  TRM_MODE_GSM1_END = TRM_MODE_GSM1_IRAT_MEASUREMENT,

  /*-----------
    GSM2 modes
  -----------*/

  /* General mode for GSM1 and GSM2 clients */
  TRM_MODE_GSM2,

  TRM_MODE_GSM2_START = TRM_MODE_GSM2,

  /* GSM Modes which are capable of SLTE */
  TRM_MODE_GSM2_SLTE_CAPABLE,

  TRM_MODE_GSM2_DR_CAPABLE,

  TRM_MODE_GSM2_DIV,

  TRM_MODE_GSM2_ACCESS_MODE,

  TRM_MODE_GSM2_IRAT_MEASUREMENT,

  TRM_MODE_GSM2_END = TRM_MODE_GSM2_IRAT_MEASUREMENT,

  /*-----------
    TDSCDMA modes
  -----------*/

  /* TDSCDMA general mode*/
  TRM_MODE_TDSCDMA,

  TRM_MODE_TDSCDMA_START = TRM_MODE_TDSCDMA,

  TRM_MODE_TDSCDMA_DR_CAPABLE,
  
  /* TDSCDMA in diversity mode*/
  TRM_MODE_TDSCDMA_DIV,

  TRM_MODE_TDSCDMA_ACCESS_MODE,

  TRM_MODE_TDSCDMA_IRAT_MEASUREMENT,

  TRM_MODE_TDSCDMA_END = TRM_MODE_TDSCDMA_IRAT_MEASUREMENT,

  /*-----------
    LTE modes
  -----------*/

  /* LTE general mode*/
  TRM_MODE_LTE,

  TRM_MODE_LTE_START = TRM_MODE_LTE,
  
  /* LTE Modes which are capable of SLTE */
  TRM_MODE_LTE_SLTE_CAPABLE,

  TRM_MODE_LTE_DR_CAPABLE,
  
  /* LTE in diversity mode*/
  TRM_MODE_LTE_DIV,
  
  TRM_MODE_LTE_ACCESS_MODE,

  TRM_MODE_LTE_IRAT_MEASUREMENT,

  TRM_MODE_LTE_END = TRM_MODE_LTE_IRAT_MEASUREMENT,

  /*-----------
    CM modes
  -----------*/ 
  TRM_MODE_CM,  

  /*-----------
    Mode that is compatible with everything
  -----------*/

  TRM_MODE_ALL_COMPATIBLE,

  /*-----------
    GPS modes
  -----------*/

  TRM_MODE_GPS = TRM_MODE_ALL_COMPATIBLE,

  /*-----------
    WLAN modes
  -----------*/
  TRM_MODE_WLAN = TRM_MODE_ALL_COMPATIBLE,

  TRM_MODE_WLAN_START = TRM_MODE_WLAN,

  TRM_MODE_WLAN_END = TRM_MODE_WLAN,

  /* Number of client modes */
  TRM_NUM_MODES,

  /*-----------
    IRAT client modes
  -----------*/
  TRM_MODE_IRAT = TRM_NUM_MODES,

} trm_mode_id_enum_t;

/*-----------------------------------------------------------------------------
  Client mode id to bit-masks
-----------------------------------------------------------------------------*/

#define TRM_MASK_FROM_MODE(id)          ( (uint64)1 << (id) )

   
/*-----------------------------------------------------------------------------
  Returns the selected Simultaneous 1x HDR Mode
-----------------------------------------------------------------------------*/

#define TRMCFG_MSK_MODE_S1XHDR(mask)      \
  ( ( trmcfg_mode_s1xhdr_enum_type )( mask & 0x3 ) )


/* The standard S1X/HDR mode is off, meaning legacy hybrid mode. */
#define TRMCFG_MODE_S1XHDR_DEFAULT TRMCFG_MODE_S1XHDR_DISABLED

/*-----------------------------------------------------------------------------
  TRM Library memory requirements
-----------------------------------------------------------------------------*/

/* Space required by all clients, in bytes */
#define TRM_BYTES             (TRM_BYTES_PER_CLIENT * TRM_MAX_CLIENTS)

/* Space required by all client, in qwords */
#define TRM_QWORDS            ((TRM_BYTES+7)/8)

/*-----------------------------------------------------------------------------
  TRM Config static, non-constant data
-----------------------------------------------------------------------------*/
struct trmcfg_t
{
  /*-------------------------------------------------------------------------
    Mode to compatible modes table
  -------------------------------------------------------------------------*/
  trm_compatible_mask_t       compatible[ TRM_NUM_MODES ];

  /*-------------------------------------------------------------------------
    Back up of mode compatibilities
  -------------------------------------------------------------------------*/
  trm_compatible_mask_t       compatibility_backup[ TRM_MAX_CLIENTS][ TRM_NUM_MODES ];

  /*-------------------------------------------------------------------------
    Clients that are always incompatible with each other
  -------------------------------------------------------------------------*/
  trm_compatible_mask_t       client_incompatibility[ TRM_MAX_CLIENTS ];

  /*-------------------------------------------------------------------------
    Memory buffer - Storage for TRM library, uint64 aligned
  -------------------------------------------------------------------------*/
  uint64                      buffer[ TRM_QWORDS ];
};


/* TRMCFG's static, non-constant data storage */
static trmcfg_t               trmcfg;



/*-----------------------------------------------------------------------------
  TRM Memory Buffer
-----------------------------------------------------------------------------*/

/* Location and size of storage */
TRM::Memory                   TRM::memory =
{
  /* Location of storage */
  (uint8 *) trmcfg.buffer,

  /* Size of storage */
  sizeof(trmcfg.buffer)
};

/*-----------------------------------------------------------------------------
  Set Compatibility Information Storage for Debug purposes.
-----------------------------------------------------------------------------*/
TRM::trm_compatibility_info_type set_compatibility_info;

/*-----------------------------------------------------------------------------
  Client log code

  These values are used by the TRM log parser to identify clients.
  The values must not change.
-----------------------------------------------------------------------------*/

enum trmlog_client_enum_t
{
  TRMLOG_1X                   = 0,
  TRMLOG_1X_SECONDARY         = 1,
  TRMLOG_HDR                  = 2,
  TRMLOG_HDR_SECONDARY        = 3,
  TRMLOG_GPS                  = 4,
  TRMLOG_UMTS                 = 5,
  TRMLOG_UMTS_SECONDARY       = 6,
  TRMLOG_GSM1                 = 7,
  TRMLOG_GSM2                 = 8,
  TRMLOG_WCDMA                = 9,
  TRMLOG_TDSCDMA              = 10,
  TRMLOG_TDSCDMA_SECONDARY    = 11,
  TRMLOG_UMTS_CA              = 12,
  TRMLOG_UMTS_CA_SECONDARY    = 13,
  TRMLOG_LTE                  = 14,
  TRMLOG_LTE_SECONDARY        = 15,
  TRMLOG_LTE_CA               = 16,
  TRMLOG_LTE_CA_SECONDARY     = 17,
  TRMLOG_CM                   = 18,
  TRMLOG_GSM3                 = 19,
  TRMLOG_GSM_SECONDARY        = 20,
  TRMLOG_RF                   = 21,
  TRMLOG_IRAT                 = 22,
  TRMLOG_WLAN                 = 23,
  TRMLOG_GPRS1                = 24,
  TRMLOG_GPRS2                = 25,
  TRMLOG_LTE_CA1              = 26,
  TRMLOG_LTE_CA1_SECONDARY    = 27,
  TRMLOG_GSM1_SECONDARY       = 28,
  TRMLOG_GSM2_SECONDARY       = 29,
  TRMLOG_GSM3_SECONDARY       = 30,
  TRMLOG_LTE_HO_SECONDARY1    = 31,
  TRMLOG_LTE_HO_SECONDARY2    = 32,
  TRMLOG_LTE_EMBMS1           = 33,
  TRMLOG_LTE_EMBMS2           = 34,
  TRMLOG_IRAT2                = 35,
  TRMLOG_UMTS2                = 36,
  TRMLOG_UMTS2_SECONDARY      = 37,
  
   /* Tx clients: rxtx split */
  TRMLOG_1X_TX                = 38,
  TRMLOG_HDR_TX               = 39,
  TRMLOG_UMTS_TX              = 40,
  TRMLOG_UMTS2_TX             = 41,
  TRMLOG_UMTS_CA_TX           = 42,
  TRMLOG_GSM1_TX              = 43,
  TRMLOG_GSM2_TX              = 44,
  TRMLOG_TDSCDMA_TX           = 45,
  TRMLOG_LTE_TX               = 46,
  TRMLOG_LTE_CA_TX            = 47,
  TRMLOG_LTE_CA1_TX           = 48,
  TRMLOG_GPRS1_TX             = 49,
  TRMLOG_GPRS2_TX             = 50,
  TRMLOG_IRAT_TX              = 51,
  TRMLOG_IRAT2_TX             = 52,
  TRMLOG_WLAN_SECONDARY       = 53,
  TRMLOG_LTE_CA_4_RX0         = 54,
  TRMLOG_LTE_CA_4_RX1         = 55, 
  TRMLOG_LTE_CA1_4_RX0        = 56,
  TRMLOG_LTE_CA1_4_RX1        = 57,
  TRMLOG_LTED                 = 58,
  TRMLOG_LTED_SECONDARY       = 59,
  TRMLOG_LTED_TX              = 60,

  TRMLOG_MAX                    = TRMLOG_LTED_TX
};



/*=============================================================================

                   CLIENT/REASON -> PRIORITY & MODE MAPPING

  Conditional compilation may be used to remove some of the following arrays,
  to conserve memory, if a specific target does not need them.

=============================================================================*/


/*-----------------------------------------------------------------------------
  Priority/Mode for a given client/reason pair
-----------------------------------------------------------------------------*/

typedef struct
{
  /* Priority for given client/reason */
  trm_pri_t                   pri;

  /* Mode for the the current client/reason */
  trm_mode_id_enum_t               mode;
}
trmcfg_pri_mode_t;


#define TRMU_IRAT_CLIENT                250
#define TRMU_DR_TRAFFIC                 90
#define TRMU_TRAFFIC                    90
#define TRMU_TECH_CHANGE                85
#define TRMU_CHANNEL_MAINTENANCE_INV    80
#define TRMU_PAGE                       70
#define TRMU_ACCESS                     60
#define TRMU_CHANNEL_MAINTENANCE        50
#define TRMU_ACQUISITION_INV            40
#define TRMU_BG_TRAFFIC                 30
#define TRMU_ACQUISITION                20
#define TRMU_DIVERSITY                  15
#define TRMU_WLAN_CLIENT                10
#define TRMU_SLEEP                      05

/*-----------------------------------------------------------------------------
  TRM_1X 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_1x[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_1X_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_1X },
  /* TRM_BROADCAST_ACCESS */          { TRMU_BG_TRAFFIC,            TRM_MODE_1X },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_1X },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_1X_TRAFFIC },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_1X_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { TRMU_PAGE,                  TRM_MODE_1X_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_1X_ACQ },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_1X_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_1X },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_1X },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_1X },
  /* TRM_RESELECT */                  { BAD,                        TRM_MODE_1X },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_1X_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_1X_ACQ },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_1X },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_1X_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_1X },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_1X },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_1X_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_1X },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_1X_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_1X_DR_CAPABLE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_1X }, 
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_1X },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_1X },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_1X_DR_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_1X },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_1X_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_1X },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_1X },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_1X },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_1X },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_1X_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_1X_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_1X },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_1X },
};


/*-----------------------------------------------------------------------------
  TRM_1X_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_1x_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_1X_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_1X },
  /* TRM_BROADCAST_ACCESS */          { TRMU_BG_TRAFFIC,            TRM_MODE_1X },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_1X },
  /* TRM_TRAFFIC */                   { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_1X_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { TRMU_PAGE,                  TRM_MODE_1X_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_1X_ACQ },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY,             TRM_MODE_1X_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_1X },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_1X },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_1X },
  /* TRM_RESELECT */                  { BAD,                        TRM_MODE_1X },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD,                        TRM_MODE_1X },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION_INV */           { BAD,                        TRM_MODE_1X_ACQ },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_1X },
  /* TRM_BG_TRAFFIC */                { BAD,                        TRM_MODE_1X_DIV },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_1X },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_1X },
  /* TRM_IRAT_MEASUREMENT */          { BAD,                        TRM_MODE_1X_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY,             TRM_MODE_1X },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { TRMU_DIVERSITY,             TRM_MODE_1X_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_1X_DR_CAPABLE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_1X }, 
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_1X },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_1X },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV,  TRM_MODE_1X },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_1X },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ TRMU_DIVERSITY,             TRM_MODE_1X_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_1X },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_1X },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_1X },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_1X },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_1X },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_1X_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD,                        TRM_MODE_1X_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_1X },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_1X },
};


/*-----------------------------------------------------------------------------
  TRM_HDR 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_hdr[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_HDR_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_HDR_CONNECTED },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_HDR },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_HDR },
  /* TRM_TRAFFIC */                   { TRMU_BG_TRAFFIC,            TRM_MODE_HDR_CONNECTED },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_HDR_IDLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_HDR },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_HDR_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_HDR },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_HDR },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_PAGE,                  TRM_MODE_HDR_SMALL_SCI_IDLE },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_PAGE,                  TRM_MODE_HDR_IDLE },
  /* TRM_RESELECT */                  { BAD,                        TRM_MODE_HDR },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_HDR_IDLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_HDR },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_HDR },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_HDR_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_HDR },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ TRMU_PAGE,                  TRM_MODE_HDR_SMALL_SCI_IDLE },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_HDR_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_HDR },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_HDR_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_HDR }, 
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_HDR },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_HDR },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_HDR_IDLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_HDR },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_HDR_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_HDR },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_HDR },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_HDR },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_HDR },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_HDR_IDLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_HDR_IDLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_HDR },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_HDR },
};


/*-----------------------------------------------------------------------------
  TRM_HDR_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_hdr_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_HDR_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_HDR_CONNECTED },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_HDR },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_HDR },
  /* TRM_TRAFFIC */                   { BAD,                        TRM_MODE_HDR_CONNECTED },
  /* TRM_DEMOD_PAGE */                { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION */               { BAD,                        TRM_MODE_HDR },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY,             TRM_MODE_HDR_DIV_DR_CAPABLE },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_HDR },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_HDR },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_RESELECT */                  { BAD,                        TRM_MODE_HDR },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION_INV */           { BAD,                        TRM_MODE_HDR },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_HDR },
  /* TRM_BG_TRAFFIC */                { BAD,                        TRM_MODE_HDR_DIV_DR_CAPABLE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_HDR },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_IRAT_MEASUREMENT */          { BAD,                        TRM_MODE_HDR_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY,             TRM_MODE_HDR },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_HDR_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_HDR }, 
  /* TRM_ACQ_DIVERSITY */             { TRMU_DIVERSITY,             TRM_MODE_HDR_DIV },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_HDR },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD,                        TRM_MODE_HDR },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_HDR },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_HDR_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_HDR },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_HDR },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_HDR },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_HDR },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_HDR },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD,                        TRM_MODE_HDR_IDLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_HDR },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_HDR },
};


/*-----------------------------------------------------------------------------
  TRM_GPS 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_gps[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_GPS },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_GPS },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_GPS },
  /* TRM_LOCATION_FIX */              { TRMU_CHANNEL_MAINTENANCE, TRM_MODE_GPS },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_GPS */                 { TRMU_CHANNEL_MAINTENANCE, TRM_MODE_GPS },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_GPS },
  /* TRM_DIVERSITY */                 { BAD, TRM_MODE_GPS },
  /* TRM_GPS_TDM */                   { TRMU_CHANNEL_MAINTENANCE, TRM_MODE_GPS },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_GPS },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_GPS },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_GPS },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE, TRM_MODE_GPS },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_GPS },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_GPS },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_GPS },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_GPS },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_GPS },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_GPS },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_GPS },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_GPS },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_GPS },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_GPS },  
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_GPS },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_GPS },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_GPS },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_GPS },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_GPS },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_GPS },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_GPS },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_GPS },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_GPS },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_GPS },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_GPS },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_GPS },
  /* TRM_CTCH */                      { BAD, TRM_MODE_GPS },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_GPS },
};


/*-----------------------------------------------------------------------------
  TRM_UMTS1
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts1[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_UMTS1_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_UMTS1 },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_UMTS1 },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_UMTS1_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_UMTS1_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_UMTS1 },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS1_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_UMTS1_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_UMTS1_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_UMTS1},
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_CTCH */                      { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS1_CTCH_MODE },
  /* TRM_CTCH_INV */                  { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_UMTS1_CTCH_MODE },
};

/*-----------------------------------------------------------------------------
  TRM_UMTS2
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts2[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_UMTS2_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_UMTS2 },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_UMTS2 },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_UMTS2_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_UMTS2_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_UMTS2 },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS2_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_UMTS2_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_UMTS2_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_UMTS2},
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_CTCH */                      { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_UMTS2_CTCH_MODE },
  /* TRM_CTCH_INV */                  { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_UMTS2_CTCH_MODE },
};

/*-----------------------------------------------------------------------------
  TRM_UMTS1_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts1_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC, TRM_MODE_UMTS1 },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_UMTS1 },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_UMTS1 },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_UMTS1 },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_UMTS1_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_UMTS1 },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_UMTS1 },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_UMTS1 },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_UMTS1 },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_UMTS1 },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_UMTS1 },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_UMTS1 },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_UMTS1 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_UMTS1 },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_UMTS1_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY, TRM_MODE_UMTS1 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_UMTS1_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_UMTS1 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_UMTS1 },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_UMTS1 },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_UMTS1 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_UMTS1 },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_UMTS1 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_UMTS1_IRAT_MEASUREMENT},
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_UMTS1 },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_UMTS1 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_UMTS1 },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_UMTS1 },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_UMTS1 },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_UMTS1_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_UMTS1_CTCH_MODE },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_UMTS1_CTCH_MODE },
};

/*-----------------------------------------------------------------------------
  TRM_UMTS2_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts2_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC, TRM_MODE_UMTS2 },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_UMTS2 },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_UMTS2 },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_UMTS2 },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_UMTS2_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_UMTS2 },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_UMTS2 },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_UMTS2 },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_UMTS2 },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_UMTS2 },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_UMTS2 },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_UMTS2 },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_UMTS2 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_UMTS2 },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_UMTS2_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY, TRM_MODE_UMTS2 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_UMTS2_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_UMTS2 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_UMTS2 },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_UMTS2 },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_UMTS2 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_UMTS2 },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_UMTS2 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_UMTS2_IRAT_MEASUREMENT},
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_UMTS2 },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_UMTS2 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_UMTS2 },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_UMTS2 },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_UMTS2 },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_UMTS2_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_UMTS2_CTCH_MODE },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_UMTS2_CTCH_MODE },
};

/*-----------------------------------------------------------------------------
  TRM_GSM11 and TRM_GPRS1 
-----------------------------------------------------------------------------*/
static const trmcfg_pri_mode_t  trmcfg_pri_mode_gsm1[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_GSM1_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_GSM1 },
  /* TRM_BROADCAST_ACCESS */          { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_GSM1 },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_GSM1 },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_GSM1 },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_GSM1 },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_GSM1 },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_GSM1 },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_DEMOD_MAX_SENS */            { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_DEMOD_CBCH */                { TRMU_ACQUISITION,           TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE_INV */            { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_GSM1_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_GSM1 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_GSM1 },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM1_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_GSM1 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_GSM1},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_GSM1 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_GSM1 },
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_GSM1 },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_GSM1 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM1_SLTE_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_GSM1 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_GSM1_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_GSM1 },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_GSM1 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_GSM1 },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_GSM1 },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_GSM1 },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_GSM1 },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_GSM1 },
};

/*-----------------------------------------------------------------------------
  TRM_GSM1_SECONDARY, TRM_GSM11_SECONDARY, TRM_GSM12_SECONDARY, TRM_GSM13_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_gsm1_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_GSM1 },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_GSM1 },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_GSM1 },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_GSM1 },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_GSM1 },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_GSM1_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_GSM1 },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_GSM1 },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_GSM1 },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_GSM1 },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_GSM1 },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_GSM1 },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_GSM1 },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_GSM1 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_GSM1 },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_GSM1_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_GSM1 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_GSM1 },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_GSM1 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_GSM1 },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_GSM1 },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_GSM1 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_GSM1 },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_GSM1 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_GSM1_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_GSM1 },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_GSM1 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_GSM1 },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_GSM1 },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_GSM1 },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_GSM1_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_GSM1 },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_GSM1 },
};

/*-----------------------------------------------------------------------------
  TRM_GSM2 and TRM_GPRS2 
-----------------------------------------------------------------------------*/
static const trmcfg_pri_mode_t  trmcfg_pri_mode_gsm2[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_GSM2_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_GSM2 },
  /* TRM_BROADCAST_ACCESS */          { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_GSM2 },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_GSM2 },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_GSM2 },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_GSM2 },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_GSM2 },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_GSM2 },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_DEMOD_MAX_SENS */            { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_DEMOD_CBCH */                { TRMU_ACQUISITION,           TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE_INV */            { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_GSM2_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_GSM2 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_GSM2 },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_GSM2_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_GSM2 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_GSM2},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_GSM2 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_GSM2 },
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_GSM2 },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_GSM2 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_GSM2_SLTE_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_GSM2 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_GSM2_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_GSM2 },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_GSM2 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_GSM2 },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_GSM2 },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_GSM2 },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_GSM2 },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_GSM2 },
};

/*-----------------------------------------------------------------------------
  TRM_GSM2_SECONDARY, TRM_GSM21_SECONDARY, TRM_GSM22_SECONDARY, TRM_GSM23_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_gsm2_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_GSM2 },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_GSM2 },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_GSM2 },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_GSM2 },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_GSM2 },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_GSM2_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_GSM2 },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_GSM2 },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_GSM2 },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_GSM2 },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_GSM2 },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_GSM2 },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_GSM2 },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_GSM2 },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_GSM2 },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_GSM2_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_GSM2 },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_GSM2 },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_GSM2 },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_GSM2 },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_GSM2 },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_GSM2 },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_GSM2 },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_GSM2 },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_GSM2_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_GSM2 },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_GSM2 },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_GSM2 },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_GSM2 },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_GSM2 },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_GSM2_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_GSM2 },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_GSM2 },
};

/*-----------------------------------------------------------------------------
  TRM_TDSCDMA 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_tdscdma[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_TDSCDMA_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_TDSCDMA },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_TDSCDMA },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_TDSCDMA_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_TOTAL_CONTROL */             { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_TDSCDMA_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_TDSCDMA_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_TDSCDMA_DIV },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_SLEEP */                     { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_TDSCDMA_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_EMBMS */                     { BAD,                        TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD,                        TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_EMBMS_LOW */                 { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_DR_TRAFFIC */                { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_TDSCDMA },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_TDSCDMA },
};


/*-----------------------------------------------------------------------------
  TRM_TDSCDMA_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_tdscdma_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_TDSCDMA },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_TDSCDMA },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_TDSCDMA },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_TDSCDMA_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_TDSCDMA },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_TDSCDMA },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_TDSCDMA },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_TDSCDMA },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_TDSCDMA },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_TDSCDMA },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_TDSCDMA },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_TDSCDMA },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_TDSCDMA_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_TDSCDMA_DIV },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_TDSCDMA},
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_TDSCDMA},
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_TDSCDMA },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_TDSCDMA },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_TDSCDMA },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_TDSCDMA },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_TDSCDMA_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_TDSCDMA },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_TDSCDMA },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_TDSCDMA },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_TDSCDMA },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_TDSCDMA },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_TDSCDMA_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_TDSCDMA },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_TDSCDMA },
};

/*-----------------------------------------------------------------------------
  TRM_UMTS_CA 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts1_ca[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DIVERSITY */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_UMTS1_CA  },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC, TRM_MODE_UMTS1_CA },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_UMTS1_CA },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_UMTS_CA_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_UMTS_CA_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_EMBMS */                     { BAD, TRM_MODE_UMTS1_CA },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CTCH */                      { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_UMTS1_CA },
};


/*-----------------------------------------------------------------------------
  TRM_UMTS_CA_SECONDARY 
-----------------------------------------------------------------------------*/
static const trmcfg_pri_mode_t  trmcfg_pri_mode_umts1_ca_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_UMTS1_CA },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_UMTS1_CA },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_UMTS1_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_UMTS1_CA},
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_UMTS1_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_EMBMS */                     { BAD, TRM_MODE_UMTS1_CA },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CTCH */                      { BAD, TRM_MODE_UMTS1_CA },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_UMTS1_CA },
};

/*-----------------------------------------------------------------------------
  TRM_LTE
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_lte[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_ACCESS,                TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_TRAFFIC,               TRM_MODE_LTE_SLTE_CAPABLE },
  /* TRM_BROADCAST_ACCESS */          { BAD,                        TRM_MODE_LTE },
  /* TRM_LOCATION_FIX */              { BAD,                        TRM_MODE_LTE },
  /* TRM_TRAFFIC */                   { TRMU_TRAFFIC,               TRM_MODE_LTE_SLTE_CAPABLE },
  /* TRM_DEMOD_PAGE */                { TRMU_PAGE,                  TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { BAD,                        TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_GPS */                 { BAD,                        TRM_MODE_LTE },
  /* TRM_ACQUISITION */               { TRMU_ACQUISITION,           TRM_MODE_LTE },
  /* TRM_DIVERSITY */                 { BAD,                        TRM_MODE_LTE_DIV },
  /* TRM_GPS_TDM */                   { BAD,                        TRM_MODE_LTE },
  /* TRM_TOTAL_CONTROL */             { TRMU_TRAFFIC,          TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE */            { BAD,                        TRM_MODE_LTE  },
  /* TRM_DEMOD_MAX_SENS */            { BAD,                        TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD,                        TRM_MODE_LTE },
  /* TRM_RESELECT */                  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD,                        TRM_MODE_LTE },
  /* TRM_ACQUISITION_INV */           { TRMU_ACQUISITION_INV,       TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_INV */            { BAD,                        TRM_MODE_LTE },
  /* TRM_BG_TRAFFIC */                { TRMU_BG_TRAFFIC,            TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD,                        TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD,                        TRM_MODE_LTE },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD,                        TRM_MODE_LTE },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD,                        TRM_MODE_LTE_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD,                        TRM_MODE_LTE},
  /* TRM_SUBS_CAP_CHANGE*/            { BAD,                        TRM_MODE_LTE},
  /* TRM_ACQ_DIVERSITY */             { BAD,                        TRM_MODE_LTE },
  /* TRM_SLEEP */                     { TRMU_SLEEP,                 TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_CHANNEL_MAINTENANCE_INV, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE,           TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD,                        TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD,                        TRM_MODE_LTE },
  /* TRM_EMBMS */                     { TRMU_CHANNEL_MAINTENANCE,   TRM_MODE_LTE_DR_CAPABLE },  
  /* TRM_EMBMS_INV */                 { TRMU_DR_TRAFFIC,            TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_SINGLE_RX_CA */              { BAD,                        TRM_MODE_LTE },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD,                        TRM_MODE_LTE },
  /* TRM_EMBMS_LOW */                 { TRMU_BG_TRAFFIC,            TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DR_TRAFFIC */                { TRMU_DR_TRAFFIC,            TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_ACQUISITION,           TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_ACQUISITION_INV,       TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD,                        TRM_MODE_LTE },
  /* TRM_CTCH_INV */                  { BAD,                        TRM_MODE_LTE },
};


/*-----------------------------------------------------------------------------
  TRM_LTE_SECONDARY 
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_lte_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_LTE },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_LTE },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_LTE },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_LTE },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_LTE_DIV },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_LTE },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_LTE },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_LTE },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_LTE },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_LTE },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_DIVERSITY, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY, TRM_MODE_LTE },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_LTE_DIV},
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_LTE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_LTE},
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_LTE },
  /* TRM_SLEEP */                     { TRMU_SLEEP, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_LTE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_LTE },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { TRMU_SLEEP, TRM_MODE_LTE_DIV },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { TRMU_DR_TRAFFIC, TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_LTE },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_LTE },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_LTE },
  /* TRM_DR_TRAFFIC */                { TRMU_DR_TRAFFIC, TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_LTE },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_LTE },
};

/*-----------------------------------------------------------------------------
  TRM_LTE_CA
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_lte_ca[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_DIVERSITY, TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_ACCESS_URGENT */             { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_LTE },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_LTE },
  /* TRM_TRAFFIC */                   { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_PAGE */                { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION */               { TRMU_DIVERSITY, TRM_MODE_LTE },
  /* TRM_DIVERSITY */                 { BAD, TRM_MODE_LTE },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_LTE },
  /* TRM_TOTAL_CONTROL */             { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_LTE },
  /* TRM_RESELECT */                  { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION_INV */           { TRMU_DIVERSITY, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_LTE },
  /* TRM_BG_TRAFFIC */                { TRMU_DIVERSITY, TRM_MODE_LTE_ACCESS_MODE },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_LTE },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_DIVERSITY, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_LTE },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_LTE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_LTE },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_LTE },
  /* TRM_SLEEP */                     { TRMU_SLEEP, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_LTE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_LTE },
  /* TRM_TECH_CHANGE                */{ TRMU_DIVERSITY, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_LTE },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_LTE },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_LTE },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_LTE },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_LTE },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_LTE },
};

/*-----------------------------------------------------------------------------
  TRM_LTE_CA_SECONDARY
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_lte_ca_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_LTE },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_LTE },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_LTE },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_LTE },
  /* TRM_DIVERSITY */                 { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_LTE },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_LTE },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_LTE },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_LTE },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_LTE },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_DIVERSITY, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_DIVERSITY, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_LTE },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_LTE },
  /* TRM_SUBS_CAP_CHANGE*/            { BAD, TRM_MODE_LTE },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_LTE },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_LTE },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_LTE },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_LTE },
  /* TRM_TECH_CHANGE                */{ TRMU_TECH_CHANGE, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_LTE_IRAT_MEASUREMENT },
  /* TRM_SLEEP_DIVERSITY */           { TRMU_SLEEP, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_LTE },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_LTE },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_LTE },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_LTE },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_LTE_DR_CAPABLE },
  /* TRM_CTCH */                      { BAD, TRM_MODE_LTE },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_LTE },
};


/*-----------------------------------------------------------------------------
  TRM_CM
-----------------------------------------------------------------------------*/

static const trmcfg_pri_mode_t  trmcfg_pri_mode_cm[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { BAD, TRM_MODE_CM },
  /* TRM_ACCESS_URGENT */             { BAD, TRM_MODE_CM },
  /* TRM_BROADCAST_ACCESS */          { BAD, TRM_MODE_CM },
  /* TRM_LOCATION_FIX */              { BAD, TRM_MODE_CM },
  /* TRM_TRAFFIC */                   { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_PAGE */                { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_BROADCAST */           { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_GPS */                 { BAD, TRM_MODE_CM },
  /* TRM_ACQUISITION */               { BAD, TRM_MODE_CM },
  /* TRM_DIVERSITY */                 { BAD, TRM_MODE_CM },
  /* TRM_GPS_TDM */                   { BAD, TRM_MODE_CM },
  /* TRM_TOTAL_CONTROL */             { BAD, TRM_MODE_CM },
  /* TRM_SMALL_SCI_PAGE */            { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_MAX_SENS */            { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { BAD, TRM_MODE_CM },
  /* TRM_RESELECT */                  { BAD, TRM_MODE_CM },
  /* TRM_CHANNEL_MAINTENANCE */       { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_CBCH */                { BAD, TRM_MODE_CM },
  /* TRM_ACQUISITION_INV */           { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_PAGE_INV */            { BAD, TRM_MODE_CM },
  /* TRM_BG_TRAFFIC */                { BAD, TRM_MODE_CM },
  /* TRM_MC_TRAFFIC */                { BAD, TRM_MODE_CM },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ BAD, TRM_MODE_CM },
  /* TRM_IRAT_MEASUREMENT */          { BAD, TRM_MODE_CM },
  /* TRM_ENVELOPE_TRACKING */         { BAD, TRM_MODE_CM },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { BAD, TRM_MODE_CM },
  /* TRM_DEMOD_PAGE_NONURGENT */      { BAD, TRM_MODE_CM },
  /* TRM_SUBS_CAP_CHANGE*/            { TRMU_TECH_CHANGE, TRM_MODE_CM },
  /* TRM_ACQ_DIVERSITY */             { BAD, TRM_MODE_CM },
  /* TRM_SLEEP */                     { BAD, TRM_MODE_CM },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { BAD, TRM_MODE_CM },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { BAD, TRM_MODE_CM },
  /* TRM_TECH_CHANGE                */{ BAD, TRM_NUM_MODES },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ BAD, TRM_MODE_CM },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_CM },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_CM },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_CM },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_CM },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_CM },
  /* TRM_ACQUISITION_CELL_SELECT */   { BAD, TRM_MODE_CM },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ BAD, TRM_MODE_CM },
  /* TRM_CTCH */                      { BAD, TRM_MODE_CM },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_CM },
};

/*-----------------------------------------------------------------------------
  TRM_IRAT, inorder to make IRAT client non-premptible, priority is set to 255
-----------------------------------------------------------------------------*/
static const trmcfg_pri_mode_t  trmcfg_pri_mode_irat[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ACCESS_URGENT */             { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_BROADCAST_ACCESS */          { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_LOCATION_FIX */              { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_TRAFFIC */                   { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_PAGE */                { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_BROADCAST */           { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_GPS */                 { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ACQUISITION */               { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DIVERSITY */                 { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_GPS_TDM */                   { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_TOTAL_CONTROL */             { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_MAX_SENS */            { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_RESELECT */                  { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_CBCH */                { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ACQUISITION_INV */           { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_PAGE_INV */            { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_BG_TRAFFIC */                { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_MC_TRAFFIC */                { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DEMOD_PAGE_NONURGENT */      { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_SUBS_CAP_CHANGE */           { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ACQ_DIVERSITY */             { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_SLEEP */                     { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_TECH_CHANGE                */{ TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_IRAT },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_IRAT },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_IRAT },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_IRAT },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_IRAT },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_IRAT_CLIENT,   TRM_MODE_IRAT },
  /* TRM_CTCH */                      { BAD, TRM_MODE_IRAT },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_IRAT },
};

/*-----------------------------------------------------------------------------
  TRM_WLAN, inorder to make WLAN client always premptible, priority is set to 8
-----------------------------------------------------------------------------*/
static const trmcfg_pri_mode_t  trmcfg_pri_mode_wlan[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACCESS_URGENT */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_BROADCAST_ACCESS */          { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_LOCATION_FIX */              { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TRAFFIC */                   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_BROADCAST */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_GPS */                 { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION */               { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY */                 { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_GPS_TDM */                   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TOTAL_CONTROL */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_MAX_SENS */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_RESELECT */                  { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_CBCH */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION_INV */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_INV */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_BG_TRAFFIC */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_MC_TRAFFIC */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_NONURGENT */      { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SUBS_CAP_CHANGE */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQ_DIVERSITY */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SLEEP */                     { TRMU_SLEEP,         TRM_MODE_WLAN },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TECH_CHANGE                */{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_WLAN },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_WLAN },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_WLAN },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_WLAN },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_WLAN },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_CTCH */                      { BAD, TRM_MODE_WLAN },
  /* TRM_CTCH_INV */                  { BAD, TRM_MODE_WLAN },
};

static const trmcfg_pri_mode_t  trmcfg_pri_mode_wlan_secondary[TRM_REASON_MAX] = 
{
  /* TRM_ACCESS        */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACCESS_URGENT */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_BROADCAST_ACCESS */          { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_LOCATION_FIX */              { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TRAFFIC */                   { TRMU_BG_TRAFFIC,    TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_BROADCAST */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_GPS */                 { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION */               { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY */                 { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_GPS_TDM */                   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TOTAL_CONTROL */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SMALL_SCI_PAGE */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_MAX_SENS */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_CONTINUATION */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_RESELECT */                  { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_CHANNEL_MAINTENANCE */       { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_CBCH */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION_INV */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_INV */            { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_BG_TRAFFIC */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_MC_TRAFFIC */                { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SMALL_SCI_PAGE_CONTINUATION*/{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_IRAT_MEASUREMENT */          { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ENVELOPE_TRACKING */         { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY_LOWCMTHRESH */     { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DEMOD_PAGE_NONURGENT */      { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SUBS_CAP_CHANGE */           { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQ_DIVERSITY */             { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SLEEP */                     { TRMU_SLEEP,         TRM_MODE_WLAN },
  /* TRM_CHANNEL_MAINTENANCE_INV */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_HIGH_PRIORITY_SIGNALLING */  { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_TECH_CHANGE                */{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_DIVERSITY_IRAT_MEASUREMENT */{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_SLEEP_DIVERSITY */           { BAD, TRM_MODE_WLAN },
  /* TRM_EMBMS */                     { BAD, TRM_NUM_MODES },  
  /* TRM_EMBMS_INV */                 { BAD, TRM_NUM_MODES },
  /* TRM_SINGLE_RX_CA */              { BAD, TRM_MODE_WLAN },
  /* TRM_SINGLE_RX_CA_DIV */          { BAD, TRM_MODE_WLAN },
  /* TRM_EMBMS_LOW */                 { BAD, TRM_MODE_WLAN },
  /* TRM_DR_TRAFFIC */                { BAD, TRM_MODE_WLAN },
  /* TRM_ACQUISITION_CELL_SELECT */   { TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
  /* TRM_ACQUISITION_CELL_SELECT_INV*/{ TRMU_WLAN_CLIENT,   TRM_MODE_WLAN },
};
/*=============================================================================

                             CLIENT CONSTANT DATA

=============================================================================*/

/*-----------------------------------------------------------------------------
  Client const data structure type
-----------------------------------------------------------------------------*/

typedef struct
{
  /* Log code for the client */
  trmlog_client_enum_t           log_code;

  /* Number of reasons */
  trm_reason_enum_t              reasons;

  /* Mode for the the current client/reason */
  const trmcfg_pri_mode_t *      pri_mode;

  /* Start Mode */
  trm_mode_id_enum_t             start_mode;

  /* End Mode */
  trm_mode_id_enum_t             end_mode;
}
trmcfg_client_t;

/*-----------------------------------------------------------------------------
  Array of all diversity modes
-----------------------------------------------------------------------------*/
static const trm_mode_id_enum_t trmcfg_div_modes[] =
{
  TRM_MODE_1X_DIV,
  TRM_MODE_HDR_DIV_DR_CAPABLE,
  TRM_MODE_UMTS1_DIV,
  TRM_MODE_UMTS2_DIV,
  TRM_MODE_TDSCDMA_DIV,
  TRM_MODE_GSM1_DIV,
  TRM_MODE_GSM2_DIV,
  TRM_MODE_LTE_DIV,
};

/*-----------------------------------------------------------------------------
  Array of all dr modes
-----------------------------------------------------------------------------*/
static const trm_mode_id_enum_t trmcfg_dr_modes[] =
{
  TRM_MODE_1X_DR_CAPABLE,
  TRM_MODE_1X_IRAT_MEASUREMENT,
  TRM_MODE_UMTS1_DR_CAPABLE,
  TRM_MODE_GSM1_SLTE_CAPABLE,
  TRM_MODE_GSM1_DR_CAPABLE,
  TRM_MODE_UMTS2_DR_CAPABLE,
  TRM_MODE_GSM2_SLTE_CAPABLE,
  TRM_MODE_GSM2_DR_CAPABLE,
  TRM_MODE_TDSCDMA_DR_CAPABLE,
  TRM_MODE_LTE_DR_CAPABLE,
  TRM_MODE_HDR_IDLE,
  TRM_MODE_HDR_IRAT_MEASUREMENT,
  TRM_MODE_HDR_SMALL_SCI_IDLE,
  TRM_MODE_UMTS1_CTCH_MODE,
  TRM_MODE_UMTS2_CTCH_MODE,
};

static const trm_mode_id_enum_t trmcfg_access_modes[] =
{
  TRM_MODE_1X_ACCESS_MODE,
  TRM_MODE_HDR_ACCESS_MODE,
  TRM_MODE_UMTS1_ACCESS_MODE,
  TRM_MODE_UMTS2_ACCESS_MODE,
  TRM_MODE_GSM1_ACCESS_MODE,
  TRM_MODE_GSM2_ACCESS_MODE,
  TRM_MODE_TDSCDMA_ACCESS_MODE,
  TRM_MODE_LTE_ACCESS_MODE,
};

/*-----------------------------------------------------------------------------
  Array of all IRAT modes
-----------------------------------------------------------------------------*/
static const trm_mode_id_enum_t trmcfg_irat_modes[] =
{
  TRM_MODE_1X_IRAT_MEASUREMENT,
  TRM_MODE_HDR_IRAT_MEASUREMENT,
  TRM_MODE_UMTS1_IRAT_MEASUREMENT,
  TRM_MODE_UMTS2_IRAT_MEASUREMENT,
  TRM_MODE_GSM1_IRAT_MEASUREMENT,
  TRM_MODE_GSM2_IRAT_MEASUREMENT,
  TRM_MODE_TDSCDMA_IRAT_MEASUREMENT,
  TRM_MODE_LTE_IRAT_MEASUREMENT,
};

/*-----------------------------------------------------------------------------
  Client const data structures

  Conditional compilation may be used to remove some of the following array
  structures, to conserve memory, if a specific target does not need them.
-----------------------------------------------------------------------------*/

static const trmcfg_client_t trmcfg_client[] =
{
  /* TRM_1X */
  {
    TRMLOG_1X,
    TRM_REASON_MAX,
    trmcfg_pri_mode_1x,
    TRM_MODE_1X_START, 
    TRM_MODE_1X_END
  },

  /* TRM_1X_SECONDARY */
  {
    TRMLOG_1X_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_1x_secondary,
    TRM_MODE_1X_START, 
    TRM_MODE_1X_END
  },

  /* TRM_HDR */
  {
    TRMLOG_HDR,
    TRM_REASON_MAX,
    trmcfg_pri_mode_hdr,
    TRM_MODE_HDR_START, 
    TRM_MODE_HDR_END
  },

  /* TRM_HDR_SECONDARY */
  {
    TRMLOG_HDR_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_hdr_secondary,
    TRM_MODE_HDR_START, 
    TRM_MODE_HDR_END
  },

  /* TRM_GPS */
  {
    TRMLOG_GPS,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gps,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_UMTS */
  {
    TRMLOG_UMTS,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1,
    TRM_MODE_UMTS1_START, 
    TRM_MODE_UMTS1_END
  },

  /* TRM_UMTS_SECONDARY */
  {
    TRMLOG_UMTS_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1_secondary,
    TRM_MODE_UMTS1_START, 
    TRM_MODE_UMTS1_END
  },

  /* TRM_GSM1*/
  {
    TRMLOG_GSM1,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm1,
    TRM_MODE_GSM1_START, 
    TRM_MODE_GSM1_END
  },

  /* TRM_GSM2*/
  {
    TRMLOG_GSM2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm2,
    TRM_MODE_GSM2_START, 
    TRM_MODE_GSM2_END
  },

  /* TRM_WCDMA*/
  {
    TRMLOG_WCDMA,
    TRM_REASON_MAX,
    NULL,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_TDSCDMA */
  {
    TRMLOG_TDSCDMA,
    TRM_REASON_MAX,
    trmcfg_pri_mode_tdscdma,
    TRM_MODE_TDSCDMA_START, 
    TRM_MODE_TDSCDMA_END
  },

  /* TRM_TDSCDMA_SECONDARY */
  {
    TRMLOG_TDSCDMA_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_tdscdma_secondary,
    TRM_MODE_TDSCDMA_START, 
    TRM_MODE_TDSCDMA_END
  },

  /* TRM_UMTS_CA */
  {
    TRMLOG_UMTS_CA,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1_ca,
    TRM_MODE_UMTS_CA_START, 
    TRM_MODE_UMTS_CA_END
  },

  /* TRM_UMTS_SECONDARY */
  {
    TRMLOG_UMTS_CA_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1_ca_secondary,
    TRM_MODE_UMTS1_START, 
    TRM_MODE_UMTS1_END
  },


  /* TRM_LTE */
  {
    TRMLOG_LTE,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },


  /* TRM_LTE_SECONDARY */
  {
    TRMLOG_LTE_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_CA */
  {
    TRMLOG_LTE_CA,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_CA_SECONDARY */
  {
    TRMLOG_LTE_CA_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },
 
  /* TRM_CM */
  {
    TRMLOG_CM,
    TRM_REASON_MAX,
    trmcfg_pri_mode_cm,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_GSM3*/
  {
    TRMLOG_GSM3,
    TRM_REASON_MAX,
    NULL,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_GSM_SECONDARY*/
  {
    TRMLOG_GSM_SECONDARY,
    TRM_REASON_MAX,
    NULL,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_RF */
  {
    TRMLOG_RF,
    TRM_REASON_MAX,
    NULL,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

    /* TRM_IRAT */
  {
    TRMLOG_IRAT,
    TRM_REASON_MAX,
    trmcfg_pri_mode_irat,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

    /* TRM_WLAN */
  {
    TRMLOG_WLAN,
    TRM_REASON_MAX,
    trmcfg_pri_mode_wlan,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_GPRS1*/
  {
    TRMLOG_GPRS1,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm1,
    TRM_MODE_GSM1_START, 
    TRM_MODE_GSM1_END
  },

  /* TRM_GPRS2*/
  {
    TRMLOG_GPRS2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm2,
    TRM_MODE_GSM2_START, 
    TRM_MODE_GSM2_END
  },

  /* TRM_LTE_CA1 */
  {
    TRMLOG_LTE_CA1,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_CA1_SECONDARY */
  {
    TRMLOG_LTE_CA1_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_GSM1_SECONDARY*/
  {
    TRMLOG_GSM1_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm1_secondary,
    TRM_MODE_GSM1_START, 
    TRM_MODE_GSM1_END
  },

  /* TRM_GSM2_SECONDARY*/
  {
    TRMLOG_GSM2_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm2_secondary,
    TRM_MODE_GSM2_START, 
    TRM_MODE_GSM2_END
  },

  /* TRM_GSM3_SECONDARY*/
  {
    TRMLOG_GSM3_SECONDARY,
    TRM_REASON_MAX,
    NULL,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_LTE_HO_SECONDARY1 */
  {
    TRMLOG_LTE_HO_SECONDARY1,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },  

  /* TRM_LTE_HO_SECONDARY2 */
  {
    TRMLOG_LTE_HO_SECONDARY2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_EMBMS1 */
  {
    TRMLOG_LTE_EMBMS1,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_EMBMS2 */
  {
    TRMLOG_LTE_EMBMS2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },
  
   /* TRM_IRAT2 */
  {
    TRMLOG_IRAT2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_irat,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_UMTS2 */
  {
    TRMLOG_UMTS2,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts2,
    TRM_MODE_UMTS2_START, 
    TRM_MODE_UMTS2_END
  },

  /* TRM_UMTS_SECONDARY2 */
  {
    TRMLOG_UMTS2_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts2_secondary,
    TRM_MODE_UMTS2_START, 
    TRM_MODE_UMTS2_END
  },

  /*rxtx split: tx clients*/

  /* TRM_1X_TX */
  {
    TRMLOG_1X_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_1x,
    TRM_MODE_1X_START, 
    TRM_MODE_1X_END
  },

  /* TRM_HDR_TX */
  {
    TRMLOG_HDR_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_hdr,
    TRM_MODE_HDR_START, 
    TRM_MODE_HDR_END
  },

  /* TRM_UMTS_TX */
  {
    TRMLOG_UMTS_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1,
    TRM_MODE_UMTS1_START, 
    TRM_MODE_UMTS1_END
  },

    /* TRM_UMTS2_TX */
  {
    TRMLOG_UMTS2_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts2,
    TRM_MODE_UMTS2_START, 
    TRM_MODE_UMTS2_END
  },

  /* TRM_UMTS_CA_TX */
  {
    TRMLOG_UMTS_CA_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_umts1_ca,
    TRM_MODE_UMTS1_START, 
    TRM_MODE_UMTS1_END
  },

  /* TRM_GSM1_TX*/
  {
    TRMLOG_GSM1_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm1,
    TRM_MODE_GSM1_START, 
    TRM_MODE_GSM1_END
  },

  /* TRM_GSM2_TX*/
  {
    TRMLOG_GSM2_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm2,
    TRM_MODE_GSM2_START, 
    TRM_MODE_GSM2_END
  },

    /* TRM_TDSCDMA */
  {
    TRMLOG_TDSCDMA_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_tdscdma,
    TRM_MODE_TDSCDMA_START, 
    TRM_MODE_TDSCDMA_END
  },

    /* TRM_LTE_TX */
  {
    TRMLOG_LTE_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_CA_TX */
  {
    TRMLOG_LTE_CA_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_CA1_TX */
  {
    TRMLOG_LTE_CA1_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_ca,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },
  
   /* TRM_GPRS1_TX*/
  {
    TRMLOG_GPRS1_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm1,
    TRM_MODE_GSM1_START, 
    TRM_MODE_GSM1_END
  },

  /* TRM_GPRS2*/
  {
    TRMLOG_GPRS2_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_gsm2,
    TRM_MODE_GSM2_START, 
    TRM_MODE_GSM2_END
  },

  /* TRM_IRAT */
  {
    TRMLOG_IRAT_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_irat,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_IRAT2_TX */
  {
    TRMLOG_IRAT2_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_irat,
    TRM_NUM_MODES,
    TRM_NUM_MODES
  },

  /* TRM_WLAN_SECONDARY */
  {
    TRMLOG_WLAN_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_wlan_secondary,
    TRM_MODE_WLAN_START,
    TRM_MODE_WLAN_END
  },

   /* TRM_LTE_CA_4_RX0 */
   {
      TRMLOG_LTE_CA_4_RX0,
      TRM_REASON_MAX,
      trmcfg_pri_mode_lte_secondary,
      TRM_MODE_LTE_START,
      TRM_MODE_LTE_END
   },

   /* TRM_LTE_CA_4_RX1 */
   {
      TRMLOG_LTE_CA_4_RX1,
      TRM_REASON_MAX,
      trmcfg_pri_mode_lte_secondary,
      TRM_MODE_LTE_START,
      TRM_MODE_LTE_END
   },
   /* TRM_LTE_CA1_4_RX0 */
   {
      TRMLOG_LTE_CA1_4_RX0,
      TRM_REASON_MAX,
      trmcfg_pri_mode_lte_secondary,
      TRM_MODE_LTE_START,
      TRM_MODE_LTE_END
   },

   /* TRM_LTE_CA_4_RX1 */
   {
      TRMLOG_LTE_CA1_4_RX1,
      TRM_REASON_MAX,
      trmcfg_pri_mode_lte_secondary,
      TRM_MODE_LTE_START,
      TRM_MODE_LTE_END
   },
  /* TRM_LTED */
  {
    TRMLOG_LTED,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTE_SECONDARY */
  {
    TRMLOG_LTED_SECONDARY,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte_secondary,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },

  /* TRM_LTED_TX */
  {
    TRMLOG_LTED_TX,
    TRM_REASON_MAX,
    trmcfg_pri_mode_lte,
    TRM_MODE_LTE_START, 
    TRM_MODE_LTE_END
  },


};




/*=============================================================================

                            FUNCTION DECLARATIONS

=============================================================================*/

static void trmcfg_set_clients_compatible
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
);

static void trmcfg_set_clients_incompatible
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
);

static void trmcfg_disallow_concurrency
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
);

/*=============================================================================

FUNCTION TRM::pri

DESCRIPTION
  Return the priority for a given client/reason pair.
  
DEPENDENCIES
  None

RETURN VALUE
  Priority

SIDE EFFECTS
  None

=============================================================================*/

trm_pri_t TRM::pri
(
  /* Client requesting the priority */
  trm_client_enum_t           client,

  /* Reason for the client's request for an RF chain */
  trm_reason_enum_t           reason
)
{

  trm_pri_t  priority = BAD;
/*---------------------------------------------------------------------------*/

  /* Out of Range check for client id */
  if ( ((uint32)client) < TRM_MAX_CLIENTS )
  {
    if ( ( trmcfg_client[ client ].pri_mode != NULL ) && reason < trmcfg_client[ client ].reasons )
    {
      priority = trmcfg_client[ client ].pri_mode[ reason ].pri;
    }
    else
    {
      ERR_FATAL("Client %d: Invalid reason %d", (int)client, (int)reason, 0);
    }
  }
  else
  {
    ERR_FATAL("Invalid client %d", (int)client, 0, 0);
  }

  return priority;

} /* TRM::pri( trm_client_enum_t, trm_reason_enum_t) */



/*=============================================================================

FUNCTION TRM::get_compatibility

DESCRIPTION
  Retrieve the compatibility mask for a given client/reason pair.
  
DEPENDENCIES
  None

RETURN VALUE
  A bitmask representing all the compatibilities of a given client/reason pair.

SIDE EFFECTS
  None

=============================================================================*/

trm_compatible_mask_t TRM::get_compatibility
(
  /* Client requesting the compatibility masks */
  trm_client_enum_t           client,

  /* Reason for the client's request */
  trm_reason_enum_t           reason
)
{
  /* Operating "Mode" for the client/reason */
  trm_mode_id_enum_t               mode_id;

  /* A bitmask representing the compatibility of the client/reason pair */
  trm_compatible_mask_t       compatibility = 0;

/*---------------------------------------------------------------------------*/

  /* Out of Range check for client id */
  if ( ((uint32) client) < TRM_MAX_CLIENTS )
  {
    /* Sanity check the reason */
    if ( reason < trmcfg_client[ client ].reasons )
    {
      /* Look up mode from client/reason pair */
      mode_id = trmcfg_client[ client ].pri_mode[ (int)reason ].mode;

      /* Sanity check the mode_id */
      if ( mode_id > TRM_NUM_MODES )
      {
        ERR_FATAL("Invalid mode_id %d for client %d", (int)mode_id, 
                (int)client, 0 );
      }

      if ( mode_id != TRM_NUM_MODES ) 
      {
        /* Look up compatible modes for the mode id */
        compatibility = trmcfg.compatible[ mode_id ];
      }
    }
    else
    {
      ERR_FATAL("Client %d: Invalid reason %d", (int)client, (int)reason, 0); 
    }
  }
  else 
  {
    ERR_FATAL("Invalid client - %d", (int)client, 0, 0);
  }
  return compatibility;

} /* TRM::get_compatibility( client, reason ) */

/*=============================================================================

FUNCTION TRM::is_concurrency_disallowed

DESCRIPTION
  Returns if concurrency is disallowed between a pair of clients.
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE if concurrency is disallowed or FALSE otherwise.

SIDE EFFECTS
  None

=============================================================================*/

boolean TRM::is_concurrency_disallowed
(
  trm_client_enum_t           client1,
  trm_client_enum_t           client2
)
{
  ASSERT(IS_VALID_CLIENT(client1));
  ASSERT(IS_VALID_CLIENT(client2));

  return (trmcfg.client_incompatibility[client1] & ((uint64) 1 << client2)) ? TRUE : FALSE;
}

/*=============================================================================

FUNCTION TRM::set_dr_mode

DESCRIPTION
  Sets the Dual Recieve compatibilities of the client
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

void TRM::set_dr_mode
(
  trm_client_enum_t          client_id,
  trm_dual_receive_enum_type dr_mode
)
{
  ASSERT(IS_VALID_CLIENT(client_id));
  const trmcfg_client_t& client = trmcfg_client[client_id];

    if ((client.start_mode >= TRM_NUM_MODES) || (client.end_mode >= TRM_NUM_MODES))
    {
      MSG_4(MSG_SSID_DFLT, MSG_LEGACY_ERROR,
      "Ignoring client %d setting DR mode to %d, incorrect start mode %d or end mode %d", 
           client_id,
           dr_mode,
           client.start_mode,
           client.end_mode);
         
    return ;
    }
    
  switch ( dr_mode )
    {
  case TRM_DUAL_RECEIVE_ENABLED:
      /* Restore the back up of the mode compatibilities */    
      for ( uint8 loop_cntr = client.start_mode; loop_cntr <= client.end_mode; ++loop_cntr )
      {
        trmcfg.compatible[ loop_cntr ] = trmcfg.compatibility_backup[ client_id][ loop_cntr ];
      }
    break;

  case TRM_DUAL_RECEIVE_DISABLED:
    /* Take a back up of the mode compatibilities */
    memscpy
    (
      &trmcfg.compatibility_backup[client_id][0], 
      sizeof(trmcfg.compatibility_backup[client_id][0])*TRM_NUM_MODES,
      &trmcfg.compatible, 
      sizeof(trmcfg.compatible[0])*TRM_NUM_MODES
    );

    /* Reset compatibilities for this client's modes */
    for ( uint8 loop_cntr = client.start_mode; loop_cntr <= client.end_mode; ++loop_cntr )
    {
      trmcfg.compatible[ loop_cntr ] = 0;
    }

    /* Set compatibilities with its own modes */
    trmcfg_set_clients_compatible(client_id, client_id);
    break;

   default:
      break;
  }
}

/*=============================================================================

FUNCTION TRM::get_compatibility_mode

DESCRIPTION
  Retrieve the compatibility mode for a given client/reason pair.
  
DEPENDENCIES
  None

RETURN VALUE
  The compatibility mode of a given client/reason pair.

SIDE EFFECTS
  None

=============================================================================*/

trm_compatible_mask_t TRM::get_compatibility_mode
(
  /* Client requesting the compatibility masks */
  trm_client_enum_t           client,

  /* Reason for the client's request */
  trm_reason_enum_t           reason
)
{
  /* Operation "Mode" for the client/reason */
  trm_mode_id_enum_t               mode_id;

  /* A bitmask representing the client mode */
  trm_compatible_mask_t       mode = 0;

/*---------------------------------------------------------------------------*/

  /* Out of Range check for client id */
  if ( ((uint32) client) < TRM_MAX_CLIENTS )
  {
    if ( ( trmcfg_client[ client ].pri_mode != NULL ) && ( reason < trmcfg_client[ client ].reasons ) )
    {
      /* Look up mode from client/reason pair */
      mode_id = trmcfg_client[ client ].pri_mode[ reason ].mode;

      /* Convert mode id into a mode bitmask */
      mode = (trm_compatible_mask_t)TRM_MASK_FROM_MODE( mode_id );
    }
    else
    {
      ERR_FATAL("Client %d: Invalid reason %d", (int)client, (int)reason, 0); 
    }
  }
  else
  {
    ERR_FATAL("Invalid client - %d", (int)client, 0, 0);
  }

  return mode;

} /* TRM::get_compatibility_mode( client, reason ) */

/*=============================================================================

FUNCTION TRM::client_log_id

DESCRIPTION
  Return the log code for a client.
  
DEPENDENCIES
  None

RETURN VALUE
  Priority

SIDE EFFECTS
  None

=============================================================================*/

uint8 TRM::client_log_id
(
  /* Client log code is needed for */
  trm_client_enum_t client
)
{

/*---------------------------------------------------------------------------*/

  /* Added for KW issue Buffer Overflow warning */
  if (((uint32)client) < TRM_MAX_CLIENTS) 
  {
    return (uint8)(trmcfg_client[ client ].log_code);
  }

  /* If we get an invalid client, log it anyway
       so that we dont break parsing            */
  return (uint8)(TRMLOG_MAX+1) ;

} /* TRM::client_log_id( trm_client_enum_t ) */



/*=============================================================================

FUNCTION TRMCFG_SET_COMPATIBLE

DESCRIPTION
  Indicate modes are compatible (can run simultaneously) 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

static void trmcfg_set_compatible
(
  /* Primary client identifier */
  trm_mode_id_enum_t               mode_1_id,

  /* Secondary client identifier */
  trm_mode_id_enum_t               mode_2_id
)
{

/*---------------------------------------------------------------------------*/

  ASSERT(mode_1_id < TRM_NUM_MODES);
  ASSERT(mode_2_id < TRM_NUM_MODES);

  /* Set mode_1 as compatible with mode_2, and vis-versa */
  trmcfg.compatible[ mode_1_id ] |= (trm_compatible_mask_t)
                                       TRM_MASK_FROM_MODE( mode_2_id );
  trmcfg.compatible[ mode_2_id ] |= (trm_compatible_mask_t)
                                       TRM_MASK_FROM_MODE( mode_1_id );

} /* trmcfg_set_compatible( mode_1_id, mode_2_id ) */

/*=============================================================================

FUNCTION TRMCFG_SET_ALL_COMPATIBLE

DESCRIPTION
  Indicate modes are compatible (can run simultaneously) 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

static void trmcfg_set_all_compatible
(
  /* Primary client identifier */
  trm_mode_id_enum_t               mode_id
)
{
  int i = 0;

/*---------------------------------------------------------------------------*/

  ASSERT(mode_id < TRM_NUM_MODES);

  /* Set all modes compatible with the input mode and vice versa */
  trmcfg.compatible[ mode_id ] = ((((uint64)1)<<TRM_NUM_MODES) - 1);
  
  for ( i = 0; i < TRM_NUM_MODES; i++ )
  {
    trmcfg.compatible[ i ] |= (trm_compatible_mask_t)
                                       TRM_MASK_FROM_MODE( mode_id );  
  }  
  
} /* trmcfg_set_compatible( mode_1_id, mode_2_id ) */

/*=============================================================================

FUNCTION TRMCFG_SET_CLIENTS_COMPATIBLE

DESCRIPTION
  Indicate modes are compatible (can run simultaneously) for a pair of clients
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

static void trmcfg_set_clients_compatible
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
)
{
/*---------------------------------------------------------------------------*/

  ASSERT(IS_VALID_CLIENT(client1_id));
  ASSERT(IS_VALID_CLIENT(client2_id));

  const trmcfg_client_t& client1 = trmcfg_client[client1_id];
  const trmcfg_client_t& client2 = trmcfg_client[client2_id];

  /* Go over client 1 and client 2 modes and set them compatible with each other */
  for ( uint8 inner_loop1 = client1.start_mode; inner_loop1 <= client1.end_mode; ++inner_loop1 )
  {
    if ( inner_loop1 >= TRM_NUM_MODES )
    {
      break;
    }

    for ( uint8 inner_loop2 = client2.start_mode; inner_loop2 <= client2.end_mode; ++inner_loop2 )
    {
      if ( inner_loop2 >= TRM_NUM_MODES )
      {
        break;
      }
      trmcfg_set_compatible( (trm_mode_id_enum_t) inner_loop1, (trm_mode_id_enum_t) inner_loop2 );
      trmcfg_set_compatible( TRM_MODE_ALL_COMPATIBLE,          (trm_mode_id_enum_t) inner_loop2 );
    }

    trmcfg_set_compatible( (trm_mode_id_enum_t) inner_loop1, TRM_MODE_ALL_COMPATIBLE );
  }
  
} /* trmcfg_set_compatible( mode_1_id, mode_2_id ) */

/*=============================================================================

FUNCTION TRMCFG_SET_INCOMPATIBLE

DESCRIPTION
  Indicate modes are uncompatible (can't run simultaneously) 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

static void trmcfg_set_incompatible
(
  /* Primary client identifier */
  trm_mode_id_enum_t               mode_1_id,

  /* Secondary client identifier */
  trm_mode_id_enum_t               mode_2_id
)
{

/*---------------------------------------------------------------------------*/

  ASSERT(mode_1_id < TRM_NUM_MODES);
  ASSERT(mode_2_id < TRM_NUM_MODES);

  /* Set mode_1 as compatible with mode_2, and vis-versa */
  trmcfg.compatible[ mode_1_id ] &= ~((trm_compatible_mask_t)
                                        TRM_MASK_FROM_MODE( mode_2_id ));
  trmcfg.compatible[ mode_2_id ] &= ~((trm_compatible_mask_t)
                                        TRM_MASK_FROM_MODE( mode_1_id ));

} /* trmcfg_set_incompatible( mode_1_id, mode_2_id ) */

/*=============================================================================

FUNCTION TRMCFG_SET_CLIENTS_INCOMPATIBLE

DESCRIPTION
  Indicate modes are incompatible (can run simultaneously) for a pair of clients
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

static void trmcfg_set_clients_incompatible
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
)
{

/*---------------------------------------------------------------------------*/

  ASSERT(IS_VALID_CLIENT(client1_id));
  ASSERT(IS_VALID_CLIENT(client2_id));

  const trmcfg_client_t& client1 = trmcfg_client[client1_id];
  const trmcfg_client_t& client2 = trmcfg_client[client2_id];

  /* Go over client 1 and client 2 modes and set them compatible with each other */
  for ( uint8 inner_loop1 = client1.start_mode; inner_loop1 <= client1.end_mode; ++inner_loop1 )
  {
    if ( inner_loop1 >= TRM_NUM_MODES )
    {
      break;
    }

    for ( uint8 inner_loop2 = client2.start_mode; inner_loop2 <= client2.end_mode; ++inner_loop2 )
    {
      if ( inner_loop2 >= TRM_NUM_MODES )
      {
        break;
      }
      trmcfg_set_incompatible( (trm_mode_id_enum_t) inner_loop1, (trm_mode_id_enum_t) inner_loop2 );
    }
  }
  
} /* trmcfg_set_compatible( mode_1_id, mode_2_id ) */


/*=============================================================================

FUNCTION trmcfg_disallow_concurrency

DESCRIPTION
  Sets the clients incompatible so that they will not be allowed to operate concurrently.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
static void trmcfg_disallow_concurrency
(
  trm_client_enum_t    client1_id,
  trm_client_enum_t    client2_id
)
{
  ASSERT(IS_VALID_CLIENT(client1_id));
  ASSERT(IS_VALID_CLIENT(client2_id));

  /* Set client1 as incompatible with client, and vis-versa */
  trmcfg.client_incompatibility[ client1_id ] |= (trm_compatible_mask_t)
                                       TRM_MASK_FROM_MODE( client2_id );
  trmcfg.client_incompatibility[ client2_id ] |= (trm_compatible_mask_t)
                                       TRM_MASK_FROM_MODE( client1_id );
}

/*============================================================================

FUNCTION TRM::IS_SHDR_ENABLED

DESCRIPTION
  Checks if SHDR is enabled or not.
    
DEPENDENCIES
  None
  
RETURN VALUE
  TRUE if SHDR is enabled. FALSE otherwise.

SIDE EFFECTS
  None

============================================================================*/

boolean TRM::is_shdr_enabled
( 
  uint32 mask
)
{
  /* Mode of s1x/hdr operation */
  trmcfg_mode_s1xhdr_enum_type  s1xhdr_mode;
  boolean val = FALSE;
  
  /* Grab the mode S1XHDR field from the mask */
  s1xhdr_mode = TRMCFG_MSK_MODE_S1XHDR( mask );

  if ( (s1xhdr_mode == TRMCFG_MODE_S1XHDR_BCMCS_ONLY) || 
       (s1xhdr_mode == TRMCFG_MODE_S1XHDR_ENABLED ) )
  {
    /* Simultaneous 1x and BCMCS mode (Traffic uses hybrid mode) OR 
     Simultaneous 1x and HDR mode is ON */
    val = TRUE;
  }
  
  return val;

}

/*============================================================================

FUNCTION TRM::ENABLE_MODE

DESCRIPTION
  Enable the given TRM mode if possible.
    
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void TRM::enable_mode
( 
  trm_mode_enum_t mode
)
{

/*--------------------------------------------------------------------------*/

  switch ( mode )
  {
    case TRM_MODE_SIMUL_1XIDLE_HDRTC:

      /* 1X (non-diversity) & HDR connected */
      trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_HDR_CONNECTED );
      
      trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_CONNECTED );

      break;
  }

} /* TRM::enable_mode( mode ) */



/*============================================================================

FUNCTION TRM::DISABLE_MODE

DESCRIPTION
  Disable the given TRM mode.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void TRM::disable_mode
( 
  trm_mode_enum_t mode
)
{

/*--------------------------------------------------------------------------*/

  switch ( mode )
  {
    case TRM_MODE_SIMUL_1XIDLE_HDRTC:
      /* 1X (non-diversity) & HDR connected */
      trmcfg_set_incompatible( TRM_MODE_1X,      TRM_MODE_HDR_CONNECTED );
      trmcfg_set_incompatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_CONNECTED );
      break;
  }

} /* TRM::disable_mode( mode ) */



/*=============================================================================

FUNCTION TRM::set_compatible_clients

DESCRIPTION
  Identify clients which can run in parallel using seperate RF chains 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

void TRM::set_compatible_clients
(
  /* Set Compatibility Information */
  const trm_compatibility_info_type info

)
{

  /* Mode of s1x/hdr operation */
  trmcfg_mode_s1xhdr_enum_type  s1xhdr_mode;
  uint8 num_clients = TRM_ARR_SIZE(trmcfg_client);

/*---------------------------------------------------------------------------*/

  TRM_COMPILE_ASSERT(TRM_ARR_SIZE(trmcfg_client) == TRM_MAX_CLIENTS);

  /* Store the information into global storage for debugging */
  set_compatibility_info.independent_chains  =  info.independent_chains;
  set_compatibility_info.modes_enabled         =  info.modes_enabled;
  set_compatibility_info.shdr_mode_mask        =  info.shdr_mode_mask;

  /* reset the compatibility array first */
  memset( trmcfg.compatible, 0, sizeof(trmcfg.compatible) );
  memset( trmcfg.client_incompatibility, 0, sizeof(trmcfg.client_incompatibility) );

  /* Set all client's modes compatible with its own modes */
  for ( uint8 loop_cntr = 0; loop_cntr < num_clients; ++loop_cntr )
  {
    trmcfg_set_clients_compatible((trm_client_enum_t)loop_cntr, (trm_client_enum_t)loop_cntr);
  }

  /*------------------------------------------------------------------------
    Supported simultaneous modes
  ------------------------------------------------------------------------*/

  if ( info.independent_chains )
  {
    /* All 1X modes are compatible with any other 1x mode. */
    trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_1X );
    trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_1X );
    trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_1X_DR_CAPABLE );
    trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_1X_IRAT_MEASUREMENT);
    trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_1X_IRAT_MEASUREMENT);
    trmcfg_set_compatible( TRM_MODE_1X_ACQ,  TRM_MODE_1X_IRAT_MEASUREMENT);
    trmcfg_set_compatible( TRM_MODE_1X_IRAT_MEASUREMENT, TRM_MODE_1X_IRAT_MEASUREMENT);
    trmcfg_set_compatible( TRM_MODE_1X_IRAT_MEASUREMENT, TRM_MODE_1X_DIV );
    trmcfg_set_compatible( TRM_MODE_ALL_COMPATIBLE, TRM_MODE_CM);
    
    /*---------------------------------------------------------------------
      Dual Subscription Dual Active Mode
     --------------------------------------------------------------------*/
    if ( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_DSDA_IS_ENABLED) )
    {
      /* DSDA is on */
      trmcfg_set_all_compatible( TRM_MODE_GSM1 );
      trmcfg_set_all_compatible( TRM_MODE_GSM1_SLTE_CAPABLE );
      trmcfg_set_all_compatible( TRM_MODE_GSM1_DR_CAPABLE );
      
      trmcfg_set_incompatible( TRM_MODE_GSM1, TRM_MODE_CM );
      trmcfg_set_incompatible( TRM_MODE_GSM1_SLTE_CAPABLE, TRM_MODE_CM );
      trmcfg_set_incompatible( TRM_MODE_GSM1_DR_CAPABLE, TRM_MODE_CM );

      /* DSDA is on */
      trmcfg_set_all_compatible( TRM_MODE_GSM2 );
      trmcfg_set_all_compatible( TRM_MODE_GSM2_SLTE_CAPABLE );
      trmcfg_set_all_compatible( TRM_MODE_GSM2_DR_CAPABLE );
      
      trmcfg_set_incompatible( TRM_MODE_GSM2, TRM_MODE_CM );
      trmcfg_set_incompatible( TRM_MODE_GSM2_SLTE_CAPABLE, TRM_MODE_CM );
      trmcfg_set_incompatible( TRM_MODE_GSM2_DR_CAPABLE, TRM_MODE_CM );

    }

    /*---------------------------------------------------------------------
      Simultaneous 1x and LTE modes
     --------------------------------------------------------------------*/
    if ( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_SVLTE_IS_ENABLED) )
    {
      /* SVLTE is on */

      /* Set 1X and LTE compatible */
      trmcfg_set_clients_compatible(TRM_1X, TRM_LTE);

      /* Set HDR and LTE compatible */
      trmcfg_set_clients_compatible(TRM_HDR, TRM_LTE);

      /* DO IRAT should be incompatible with GSM modes for SVLTE+G,
         this will be made compatible for DSDA and if SVLTE is also
         enabled following will reset the compatiblity */
      trmcfg_set_incompatible( TRM_MODE_HDR_IRAT_MEASUREMENT, TRM_MODE_GSM1 );
      trmcfg_set_incompatible( TRM_MODE_HDR_IRAT_MEASUREMENT, TRM_MODE_GSM1_SLTE_CAPABLE );
      trmcfg_set_incompatible( TRM_MODE_HDR_IRAT_MEASUREMENT, TRM_MODE_GSM2 );
      trmcfg_set_incompatible( TRM_MODE_HDR_IRAT_MEASUREMENT, TRM_MODE_GSM2_SLTE_CAPABLE );
    }
    else if( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_1X_SLTE_IS_ENABLED) )
    {
       trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,     TRM_MODE_LTE_SLTE_CAPABLE);
       trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,     TRM_MODE_LTE_DR_CAPABLE);
       trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,     TRM_MODE_LTE_DIV);
       trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE, TRM_MODE_LTE_ACCESS_MODE);
    }
  
    /*---------------------------------------------------------------------
      Simultaneous GSM and LTE/TDS modes
     --------------------------------------------------------------------*/
    if ( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_SGLTE_SGTDS_IS_ENABLED) )
    {
      /* SGLTE is on */

      /* Set TDS and GSM1 compatible */
      trmcfg_set_clients_compatible(TRM_TDSCDMA, TRM_GSM1);

      /* Set LTE and GSM1 compatible */
      trmcfg_set_clients_compatible(TRM_LTE, TRM_GSM1);
    }
    else if ( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_GSM_SLTE_IS_ENABLED) )
    {
      trmcfg_set_compatible( TRM_MODE_GSM1_SLTE_CAPABLE,     TRM_MODE_LTE_SLTE_CAPABLE);
      trmcfg_set_compatible( TRM_MODE_GSM1_SLTE_CAPABLE,     TRM_MODE_LTE_DR_CAPABLE);
      trmcfg_set_compatible( TRM_MODE_GSM1_SLTE_CAPABLE,     TRM_MODE_LTE_DIV);
    }
  
    if ( TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_DR_DSDS_IS_ENABLED) )
    {
      /* We are in DR-DSDS mode. We need to enable following compatible:
          Idle, IRAT, Data Call, Sleep, HP Broadcast, Broadcast Access,
          Reselection, SCI and Channel Maintenance.*/

      /* Set diversity modes compatibility */
      for ( uint8 loopcntr = 0; loopcntr < TRM_ARR_SIZE(trmcfg_dr_modes); loopcntr++ )
      {
        /* set all div mode compatible with DR modes */
        for(uint8 innercnt1 =0; innercnt1 < TRM_ARR_SIZE(trmcfg_dr_modes); innercnt1++)
        {
          trmcfg_set_compatible(trmcfg_dr_modes[loopcntr], trmcfg_dr_modes[innercnt1]);
        }
      }

      /* Set diversity modes compatibility */
      for ( uint8 loopcntr = 0; loopcntr < TRM_ARR_SIZE(trmcfg_div_modes); loopcntr++ )
      {
        /* set all div mode compatible with DR modes */
        for(uint8 innercnt1 =0; innercnt1 < TRM_ARR_SIZE(trmcfg_dr_modes); innercnt1++)
        {
          trmcfg_set_compatible(trmcfg_div_modes[loopcntr], trmcfg_dr_modes[innercnt1]);
        }
  
        /* set all div mode compatible with other div modes */
        for(uint8 innercnt2 =0; innercnt2 < TRM_ARR_SIZE(trmcfg_div_modes); innercnt2++)
        {
          trmcfg_set_compatible(trmcfg_div_modes[loopcntr], trmcfg_div_modes[innercnt2]);
        }
      }

      /* Set access modes compatibility */
      for ( uint8 loopcntr = 0; loopcntr < TRM_ARR_SIZE(trmcfg_access_modes); loopcntr++ )
      {
        /* set all access mode compatible with DR modes */
        for(uint8 innercnt1 =0; innercnt1 < TRM_ARR_SIZE(trmcfg_dr_modes); innercnt1++)
        {
          trmcfg_set_compatible(trmcfg_access_modes[loopcntr], trmcfg_dr_modes[innercnt1]);
        }

        /* set all access mode compatible with div modes */
        for(uint8 innercnt1 =0; innercnt1 < TRM_ARR_SIZE(trmcfg_div_modes); innercnt1++)
        {
          trmcfg_set_compatible(trmcfg_access_modes[loopcntr], trmcfg_div_modes[innercnt1]);
        }
      }

      /* IRAT modes are set here for DR-DSDS cases */
  
      /* IRAT is compatabile with all DR modes */
      for( uint8 loop_cnt = 0; loop_cnt < TRM_ARR_SIZE(trmcfg_dr_modes); loop_cnt++ )
      {
        for(uint8 inner_loop_cnt = 0; inner_loop_cnt < TRM_ARR_SIZE(trmcfg_irat_modes); inner_loop_cnt++ )
        {
          trmcfg_set_compatible( trmcfg_dr_modes[loop_cnt], trmcfg_irat_modes[inner_loop_cnt]);
        }
      }

      /* IRAT is compatible with all Access Modes */
      for( uint8 loop_cnt = 0; loop_cnt < TRM_ARR_SIZE(trmcfg_access_modes); loop_cnt++ )
      {
        for(uint8 inner_loop_cnt = 0; inner_loop_cnt < TRM_ARR_SIZE(trmcfg_irat_modes); inner_loop_cnt++ )
        {
          trmcfg_set_compatible( trmcfg_access_modes[loop_cnt], trmcfg_irat_modes[inner_loop_cnt]);
        }
      }

      /* IRAT is compatible with all Diversity modes */
      for( uint8 loop_cnt = 0; loop_cnt < TRM_ARR_SIZE(trmcfg_div_modes); loop_cnt++ )
      {
        for(uint8 inner_loop_cnt = 0; inner_loop_cnt < TRM_ARR_SIZE(trmcfg_irat_modes); inner_loop_cnt++ )
        {
          trmcfg_set_compatible( trmcfg_div_modes[loop_cnt], trmcfg_irat_modes[inner_loop_cnt]);
        }
      }

      /* IRAT is made incompatible with IRAT modes */      
      for( uint8 loop_cnt = 0; loop_cnt < TRM_ARR_SIZE(trmcfg_irat_modes); loop_cnt++ )
      {
        for(uint8 inner_loop_cnt = loop_cnt; inner_loop_cnt < TRM_ARR_SIZE(trmcfg_irat_modes); inner_loop_cnt++ )
        {
          trmcfg_set_incompatible( trmcfg_irat_modes[loop_cnt], trmcfg_irat_modes[inner_loop_cnt] );
        }
      }

      if( !TRM_IS_BIT_ENABLED(info.modes_enabled, TRM_DR_W_AND_L_IS_ENABLED) )
      {
        /* make all the LTE modes incompatible with all W modes */
        trmcfg_set_clients_incompatible(TRM_UMTS, TRM_LTE);
        trmcfg_set_clients_incompatible(TRM_UMTS2, TRM_LTE);
      }

      /* W Access cannot be compatible with LTE idle */         
      trmcfg_set_incompatible(TRM_MODE_UMTS1_ACCESS_MODE, 
                              TRM_MODE_LTE_DR_CAPABLE);
      trmcfg_set_incompatible(TRM_MODE_UMTS2_ACCESS_MODE, 
      	                      TRM_MODE_LTE_DR_CAPABLE);

      trmcfg_set_incompatible(TRM_MODE_UMTS1_ACCESS_MODE, 
     	                      TRM_MODE_LTE_DIV);
      trmcfg_set_incompatible(TRM_MODE_UMTS2_ACCESS_MODE, 
                              TRM_MODE_LTE_DIV);

      /* 1X Access cannot be compatible with LTE idle */
      trmcfg_set_incompatible(TRM_MODE_1X_ACCESS_MODE, 
                              TRM_MODE_LTE_DR_CAPABLE);
      trmcfg_set_incompatible(TRM_MODE_1X_ACCESS_MODE, 
                              TRM_MODE_LTE_DIV);

      trmcfg_set_compatible(TRM_MODE_HDR_DIV, TRM_MODE_1X_ACQ);    
      trmcfg_set_compatible(TRM_MODE_HDR_DIV_DR_CAPABLE, TRM_MODE_1X_ACQ);

      trmcfg_set_clients_incompatible(TRM_UMTS, TRM_1X);
      trmcfg_set_clients_incompatible(TRM_UMTS2, TRM_1X);
      
      uint32 sub2_tech_supported_bitmask = trm_config_handler_get_tech_supported_bitmask(SYS_MODEM_AS_ID_2);
      uint8 srlte_dsds_dual_multi_mode = trm_config_handler_get_srlte_dsds_dual_multi_mode();

#ifdef FEATURE_MCS_TABASCO
      /* If LTE is on sub2, and SRLTE DSDS dual mode is enabled, it means 1x and LTE are on separate subs and we need to
         disable 1x and GSM DR */
      if( IS_DUAL_MULTIMODE(srlte_dsds_dual_multi_mode) && TRM_MSK_BIT_ON(sub2_tech_supported_bitmask, SYS_SYS_MODE_LTE) ) 
      {
        trmcfg_set_clients_incompatible(TRM_GSM2, TRM_1X);
        trmcfg_set_clients_incompatible(TRM_GSM1, TRM_1X);
      }

      /* On TA, x + 1x are not compatible (Except for G + 1x) */
      trmcfg_set_clients_incompatible(TRM_LTE, TRM_1X);
      trmcfg_set_clients_incompatible(TRM_LTE_CA, TRM_1X);
      trmcfg_set_clients_incompatible(TRM_UMTS, TRM_UMTS2);
      trmcfg_set_clients_incompatible(TRM_LTE,  TRM_UMTS2);
      trmcfg_set_clients_incompatible(TRM_LTE,  TRM_UMTS);
#endif

      trmcfg_set_clients_incompatible(TRM_TDSCDMA, TRM_1X);
      trmcfg_set_clients_incompatible(TRM_TDSCDMA, TRM_UMTS);
      trmcfg_set_clients_incompatible(TRM_TDSCDMA, TRM_UMTS2);
      
      const trmcfg_client_t& client_hdr = trmcfg_client[TRM_HDR];

      /* Set 1x IRAT measurement incompatible with all HDR modes */
      for ( uint8 loop_cntr = client_hdr.start_mode; loop_cntr <= client_hdr.end_mode; ++loop_cntr )
      {
        trmcfg_set_incompatible( (trm_mode_id_enum_t) loop_cntr, TRM_MODE_1X_IRAT_MEASUREMENT );
      }      

      /* W/TDS Access/W-BG Traffic cannot be compatible with W-CTCH mode */         
      trmcfg_set_incompatible(TRM_MODE_UMTS1_ACCESS_MODE, 
      	                      TRM_MODE_UMTS2_CTCH_MODE);
      trmcfg_set_incompatible(TRM_MODE_UMTS2_ACCESS_MODE, 
      	                      TRM_MODE_UMTS1_CTCH_MODE);

      trmcfg_set_incompatible(TRM_MODE_TDSCDMA_ACCESS_MODE, 
                              TRM_MODE_UMTS1_CTCH_MODE);
      trmcfg_set_incompatible(TRM_MODE_TDSCDMA_ACCESS_MODE, 
      	                      TRM_MODE_UMTS2_CTCH_MODE);

      /* CTCH + CTCH should be SR */         
      trmcfg_set_incompatible(TRM_MODE_UMTS1_CTCH_MODE, 
                              TRM_MODE_UMTS2_CTCH_MODE);

      /* CTCH Mode should be SR with all LTE reasons */
      const trmcfg_client_t& client_lte = trmcfg_client[TRM_LTE];
      for ( uint8 loop_cntr1 = client_lte.start_mode; loop_cntr1 <= client_lte.end_mode; ++loop_cntr1 )
      {
        trmcfg_set_incompatible( (trm_mode_id_enum_t) loop_cntr1, TRM_MODE_UMTS1_CTCH_MODE );
        trmcfg_set_incompatible( (trm_mode_id_enum_t) loop_cntr1, TRM_MODE_UMTS2_CTCH_MODE );
      }

    }
    else
    {
      /* DR-DSDS is off */

      /* Grab the mode S1XHDR field from the TRM_CONFIG mask */
      s1xhdr_mode = TRMCFG_MSK_MODE_S1XHDR( info.shdr_mode_mask );

      if ( s1xhdr_mode == TRMCFG_MODE_S1XHDR_UNSET )
      {
        /* If the mode has not been explicitly set, set the mode to the 
           compile time default mode. */
        s1xhdr_mode = TRMCFG_MODE_S1XHDR_DEFAULT;
      }

      /* Now set the compatibilities appropriately based on the SHDR mode
         selected. */

      if ( s1xhdr_mode == TRMCFG_MODE_S1XHDR_ENABLED  )
      {
        /* 1X (non-diversity) & HDR broadcast */
        //trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_HDR_BROADCAST );

        /* 1X (non-diversity) & HDR connected */
       // trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_HDR_CONNECTED );
        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_CONNECTED );
        //trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_HDR_BG_CONNECTED );

        /* MCDO and 1X can be compatible only for DOrB SSMA PLs and 
           only when when SHDR is enabled.
           In all other targets/modes MCDO is not compatible with 1X */
        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE, TRM_MODE_HDR_ACCESS_MODE );

        /* 1X acq & HDR idle */
        trmcfg_set_compatible( TRM_MODE_1X_ACQ,  TRM_MODE_HDR_IDLE );

        /* 1X acq & HDR connected */
        trmcfg_set_compatible( TRM_MODE_1X_ACQ,  TRM_MODE_HDR_CONNECTED);
        trmcfg_set_compatible( TRM_MODE_1X_ACQ,  TRM_MODE_HDR_ACCESS_MODE);
        
        /* 1X (non diversity, non acquisition) & HDR idle */
        trmcfg_set_compatible( TRM_MODE_1X,      TRM_MODE_HDR_IDLE );
        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_IDLE );

        /* 1X (non diversity, non acquisition) & HDR Acquisition */
        trmcfg_set_compatible( TRM_MODE_1X,  TRM_MODE_HDR);
        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,  TRM_MODE_HDR);

        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_DIV );
        trmcfg_set_compatible( TRM_MODE_1X_ACQ,             TRM_MODE_HDR_DIV );
        trmcfg_set_compatible( TRM_MODE_1X,                 TRM_MODE_HDR_DIV );
        trmcfg_set_compatible( TRM_MODE_1X_DIV,             TRM_MODE_HDR_DIV );
        trmcfg_set_compatible( TRM_MODE_1X_DIV,             TRM_MODE_HDR_CONNECTED );
        trmcfg_set_compatible( TRM_MODE_1X_DIV,             TRM_MODE_HDR_IDLE );
        trmcfg_set_compatible( TRM_MODE_1X_DIV,             TRM_MODE_HDR);
        
        trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,      TRM_MODE_HDR_DIV_DR_CAPABLE );
        trmcfg_set_compatible( TRM_MODE_1X_ACQ,             TRM_MODE_HDR_DIV_DR_CAPABLE );
        trmcfg_set_compatible( TRM_MODE_1X,                 TRM_MODE_HDR_DIV_DR_CAPABLE );
        trmcfg_set_compatible( TRM_MODE_1X_DIV,             TRM_MODE_HDR_DIV_DR_CAPABLE );

        const trmcfg_client_t& client_hdr = trmcfg_client[TRM_HDR];

        /* Set 1x IRAT measurement incompatible with all HDR modes */
        for ( uint8 loop_cntr = client_hdr.start_mode; loop_cntr <= client_hdr.end_mode; ++loop_cntr )
        {
          trmcfg_set_incompatible( (trm_mode_id_enum_t) loop_cntr, TRM_MODE_1X_IRAT_MEASUREMENT );
        }
      }

      /* 1X (non-diversity) & HDR idle */
      trmcfg_set_compatible( TRM_MODE_1X,  TRM_MODE_HDR_SMALL_SCI_IDLE );
      trmcfg_set_compatible( TRM_MODE_1X_DR_CAPABLE,  TRM_MODE_HDR_SMALL_SCI_IDLE );

      /* Set UMTS compatible with UMTS_CA */
      trmcfg_set_clients_compatible(TRM_UMTS, TRM_UMTS_CA);

      /* Set UMTS2 compatible with UMTS_CA */
      trmcfg_set_clients_compatible(TRM_UMTS2, TRM_UMTS_CA);
    
    } /* if DR_DSDS bit is on */

  } /* if independent chains ... */

  /* Incompatibility between G Rx clients */
  trmcfg_disallow_concurrency(TRM_GSM1, TRM_GPRS1);
  trmcfg_disallow_concurrency(TRM_GSM2, TRM_GPRS2);
  
  /* Incompatibility between G Tx clients */
  trmcfg_disallow_concurrency(TRM_GSM1_TX, TRM_GPRS1_TX);
  trmcfg_disallow_concurrency(TRM_GSM2_TX, TRM_GPRS2_TX);
  
  /* Incompatibility between G Rx and Tx clients */
  trmcfg_disallow_concurrency(TRM_GSM1, TRM_GPRS1_TX);
  trmcfg_disallow_concurrency(TRM_GSM2, TRM_GPRS2_TX);
  trmcfg_disallow_concurrency(TRM_GSM1_TX, TRM_GPRS1);
  trmcfg_disallow_concurrency(TRM_GSM2_TX, TRM_GPRS2);
  
  /* Disallow concurrency between LTE HO_RxD and GSM clients */
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM1);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM1_SECONDARY);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM2);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM2_SECONDARY);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GPRS1);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GPRS2);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM1_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GSM2_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GPRS1_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY1, TRM_GPRS2_TX);
  
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM1);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM1_SECONDARY);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM2);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM2_SECONDARY);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GPRS1);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GPRS2);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM1_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GSM2_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GPRS1_TX);
  trmcfg_disallow_concurrency(TRM_LTE_HO_SECONDARY2, TRM_GPRS2_TX);
  
  
} /* TRM::set_compatible_clients( independent_chains ) */

/*=============================================================================

FUNCTION TRM::is_dr_reason

DESCRIPTION
  Returns true if the reason is DR capable for the associated client
  
DEPENDENCIES
  None

RETURN VALUE
  Priority

SIDE EFFECTS
  None

=============================================================================*/
boolean TRM::is_dr_reason 
(
  trm_client_enum_t client,
  trm_reason_enum_t reason
)
{
  /* Operation "Mode" for the client/reason */
  trm_mode_id_enum_t               mode_id;

  /* A bitmask representing the client mode */
  trm_compatible_mask_t       mode_mask = 0;

  boolean is_dr = FALSE;

  /* Out of Range check for client id and reason*/
  if ( ((uint32)client) < TRM_MAX_CLIENTS && reason < trmcfg_client[ client ].reasons)
  {
      /* Look up mode from client/reason pair */
      mode_id = trmcfg_client[ client ].pri_mode[ reason ].mode;
      /* Convert mode id into a mode bitmask */
      mode_mask = (trm_compatible_mask_t)TRM_MASK_FROM_MODE( mode_id );

      if (IS_DR_MODE(mode_mask))
      {
          is_dr = TRUE;
      }
  }
  else
  {
    ERR_FATAL("Invalid client: %d or client reason", (int)client, 0, 0);
  }

  return is_dr;
} /* TRM::dr_reason() */

