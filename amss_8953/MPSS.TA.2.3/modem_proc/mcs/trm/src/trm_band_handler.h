#ifndef TRM_BAND_HANDLER_H
#define TRM_BAND_HANDLER_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

              Transceiver Resource Manager Band Handler Header File

GENERAL DESCRIPTION

  This file provides some declarations for TRM band handler.


EXTERNALIZED FUNCTIONS

  None


REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/mcs.mpss/5.0/trm/src/trm_band_handler.h#8 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
10/06/2015   mn      Support of restricted IRAT/IFS bands (CR: 916871).
02/11/2015   sk      Changes to log operating mode info(791956)
12/11/2014   sk      Band compatibility check enhancement changes(768580)
11/25/2014   sr      Cache support for quicker band concurrency check(CR:754184).
09/29/2014   mn      Added a new TRM_DIVERSITY_IRAT_MEASUREMENT reason (CR: 712921).
08/12/2014   mn      Support for a unified IRAT mechanism which works across all 
                      TRM modes (CR: 705286).
08/04/2014   mn      Increase the size of the concurrency restriction hash 
                      table (CR: 703878).
08/01/2014   mn      Restriction in the size of the hash table for concurrency 
                      restrictions in TRM causes a crash. (CR: 702823).
07/17/2014   mn      TRM should map 1x and GSM2 to chain 2 when LTE is holding 
                      both chain 0 and chain 1 (CR: 695108).
04/11/2014   mn      TRM re-factoring. 

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "customer.h"
#include "modem_mcs_defs.h"

extern "C"
{
  #include "trm.h"
}

#include "trmi.h"
#include "queue.h"

#define TRM_CONCURRENCY_RESTRICTIONS_MAX_SIZE      800

#define TRM_BAND_MASK_INDEX(band)                         ( band < 64 ) ? 0 : ( band < 128 ) ? 1 : 2
#define TRM_BAND_MASK_BIT_POS(band)                       ( band < 64 ) ? band : ( band < 128 ) ? (band-64) : (band-128)

/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/

typedef struct trm_band_concurrency_index_entry_type
{
  /* Link */
  q_link_type                             link;

  /* Pointer to the concurrency restriction table entry */
  const rfm_concurrency_restriction_type* table_entry;
} trm_band_concurrency_index_entry_type;


/*-----------------------------------------------------------------------------
  TRM Client Band Class

    Class which handles band class used/requested by clients.

-----------------------------------------------------------------------------*/
class TRMClientBandClass
{
  private:
    trm_band_t bands[MAX_BAND_INFO];
    uint32     num_bands;

  public:
    TRMClientBandClass(void);
    ~TRMClientBandClass(void);

    void operator = (TRMClientBandClass     other_band_class);
    void operator = (trm_freq_input_type*   freq_info);
    boolean operator != (trm_freq_input_type  other_band);
    boolean operator == (trm_band_t  other_band);
    trm_band_t operator[](uint8 i) const ;
    void get_freq_info(trm_freq_input_type* freq_info);
    trm_band_t  get_home_band(void);
    void get_log_info(trm_log_band_info_type* log_info) const;
};

/*----------------------------------------------------------------------------
  Band class information - 8 bit struct
----------------------------------------------------------------------------*/
typedef struct
{
  TRMClientBandClass      bc_current;    /* Current Band          */
  TRMClientBandClass      bc_requested;  /* Requested Band        */
  TRMClientBandClass      bc_previous;   /* Previous Band         */
  trm_bc_enum_t           bc_state;      /* Band Class State Enum */
  trm_client_enum_t       bc_waiting_on; /* Client waiting on ... */
  boolean                 bc_registered;
  boolean                 reserved;      /* Reserved */
  /* Until this, logging have been modified to work as 16 bytes.
     The modification was needed to keep it backward compatible
     with NikeL where trm_band_t was 32 bits while it is 16 bits
     on Dime. Any future modifications would need either 8 byte
     boundary rule to work seamlessly w.r.t. logging. */
} trm_bc_info_type;

/*----------------------------------------------------------------------------
  Transceiver RF mode mapping information
----------------------------------------------------------------------------*/
typedef struct
{
  /* RF Mode for this object */
  rfm_mode_enum_type rfmode;

  /* Band class information */
  trm_bc_info_type   bc_info;
}trm_rf_mode_map_type;

/*----------------------------------------------------------------------------
  Frequency Information
----------------------------------------------------------------------------*/
typedef struct
{
  /* Band Information */
  trm_band_t            band;

  /* Band allowed flag */
  boolean               primary_allowed;

  boolean               diversity_allowed;

  rfm_device_enum_type  primary_device;

  rfm_device_enum_type  diversity_device;

} trm_band_handler_freq_info_type;

/*----------------------------------------------------------------------------
  Frequency Input Information
----------------------------------------------------------------------------*/
typedef struct
{
  /* Number of bands */
  uint32                          num_bands;

  /* Band Information */
  trm_band_handler_freq_info_type bands[MAX_BAND_INFO];
} trm_band_handler_freq_input_type;

/*----------------------------------------------------------------------------
  Client1 info for compatibility check
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t                 id;
  trm_client_enum_t                 measured_client;
  trm_band_handler_freq_input_type  freq_input;
  rfm_device_enum_type              rf_device;
} trm_band_compatibility_client1_input_type;

/*----------------------------------------------------------------------------
  Activity compatibility check input time
----------------------------------------------------------------------------*/
typedef struct
{
  trm_band_compatibility_client1_input_type client1_info;
  trm_client_enum_t                         client2;
}trm_band_compatibility_check_input_type;

/*----------------------------------------------------------------------------
  Transceiver RF mode mapping information
----------------------------------------------------------------------------*/
typedef struct
{
  /* Chain whose capability needs to be checked */
  trm_group             group;

  /* client requesting band info */
  trm_client_enum_t     client;

  /* resource type being requested */
  trm_resource_enum_t   resource;

  /* Number of bands */
  uint32                num_bands;

  /* Band information */
  trm_band_handler_freq_info_type bands[TRM_DEVICE_MAPPING_INPUT_SIZE];

} trm_chain_band_capability_input_type;

/*----------------------------------------------------------------------------
  Band concurrency restriction entry
----------------------------------------------------------------------------*/
typedef struct
{
  rfm_bands_bitmask*   band_group[RFM_MAX_DEVICES];
}trm_device_band_restrictions_entry_type;

/*-----------------------------------------------------------------------------
  TRM Band Handler class.

    Class which handles band compatibility checks.

-----------------------------------------------------------------------------*/
class TRMBandHandler
{
private:
  trm_device_band_restrictions_entry_type* band_restrictions[RFM_MAX_DEVICES][SYS_BAND_CLASS_MAX];
  
public:
  TRMBandHandler(void);

  ~TRMBandHandler(void);

  void de_initialize(void);

  void initialize(rfm_devices_configuration_type* config);
  void set_band_restriction(rfm_device_enum_type device1, sys_band_class_e_type band1, uint32 device_group2, rfm_bands_bitmask band_group2);

  boolean is_band_compatible( trm_client_enum_t client1, trm_client_enum_t client2 );
  boolean is_band_compatible( trm_band_compatibility_check_input_type* input );

  boolean check_conc_restriction( trm_group            group1,
                                  trm_freq_input_type* freq_info1,
                                  trm_group            group2,
                                  trm_freq_input_type* freq_info2 );

  boolean are_bands_supported_on_chain
  (
    trm_chain_band_capability_input_type *input
  );

   boolean are_irat_bands_supported_on_chain
   (
      trm_band_handler_freq_input_type     *input,
      boolean                              is_primary,
      trm_group                            group
   );
  boolean check_band_mask
  (
    uint64 *device_band_map,
    uint64 band
  );

  boolean is_diversity_sharing_mode( trm_client_enum_t client1, 
             trm_client_enum_t client2 );

  boolean is_irat_restriction_band(trm_band_t band);
};

/*============================================================================

Member Function trm_band_handler_initialize_freq_input

Description
  Translation function from trm_freq_input_type to trm_band_handler_freq_input_type
  
Dependencies
  None

Return Value
  None

Side Effects
  None

============================================================================*/
void trm_band_handler_initialize_freq_input
(
  trm_freq_input_type*              input,
  trm_band_handler_freq_input_type* output
);

#endif /* TRM_BAND_HANDLER_H */
