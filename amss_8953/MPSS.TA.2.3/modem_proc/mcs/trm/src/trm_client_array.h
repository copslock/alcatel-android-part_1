#ifndef TRM_CLIENT_ARRAY_H
#define TRM_CLIENT_ARRAY_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

              Transceiver Resource Manager Client Array Header File

GENERAL DESCRIPTION

  This file provides some declarations of TRMClient Array handling.


EXTERNALIZED FUNCTIONS

  None


REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/mcs.mpss/5.0/trm/src/trm_client_array.h#6 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
07/07/2015   mn      Inform rx_operation and as_id to RF (CR: 780579).
07/01/2015   mn      Handle data tech passing all devices in START QTA (CR: 864477)
05/13/2015   sk      Support for device sharing
12/25/2014   mn      TRM should use relative position in the order list for priority
                      comparisons. (CR: 774749).
10/20/2014   mn      Earliest client overshadows an incompatible higher priority 
                      client (CR: 741714).
04/11/2014   mn      TRM re-factoring. 

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "customer.h"
#include "modem_mcs_defs.h"
#include "trmi.h"

extern "C"
{
  #include "trm.h"
}

/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/

/*----------------------------------------------------------------------------
  Client Array
----------------------------------------------------------------------------*/

class TRMClientArray
{
protected:

#ifdef TEST_FRAMEWORK
  #error code not present
#else
  trm_client_enum_t         client[ TRM_ACTIVE_CLIENTS_MAX ];
#endif /* TEST_FRAMEWORK */

  /* Number of clients in the array */
  uint8                     num;

public:
  TRMClientArray() : num( 0 ) 
  {
    int i;

      for (i=0; i<TRM_ACTIVE_CLIENTS_MAX; i++ )
    {
      client[i] = TRM_NO_CLIENT; 
    } 

    clear();
  } 

  /* Number of clients in the array */                     
  inline uint32                    length()          const
  {
    return num;
  }

  /* Clear the contents of the array */
  virtual void                      clear()
  {
    num = 0;
  }

    /* Retrieve client at the indicated position */
    trm_client_enum_t                  operator[](int i) const;

  /* Finds a client in the array. Returns -1 if not found */
    virtual int8                       find(trm_client_enum_t client_to_find);

    /* Add the client to the array */
    virtual void                       append( trm_client_enum_t client_id );

  /* Remove the client, if present in the array */
    virtual void                       remove( trm_client_enum_t client_id );
};

/*----------------------------------------------------------------------------
  TRM Client Order Array
----------------------------------------------------------------------------*/

class TRMClientOrderArray : public TRMClientArray
{
  int8                      indices[TRM_MAX_CLIENTS];

public:
  virtual void              clear();

  /* Insert the client at the appropriate position */
  void                      insert( trm_client_enum_t client_id );

  /* Update the client's position in the array by removing and re-inserting */
  void                      update( trm_client_enum_t client_id );

  /* Returns true if client1 is higher in priority than client2 */
  boolean                   compare(trm_client_enum_t client1, trm_client_enum_t client2);

  /* Finds a client in the array. Returns -1 if not found */
  int8                      find(trm_client_enum_t client_to_find);

  /* Remove the client, if present in the array */
  void                      remove( trm_client_enum_t client_id );
};

/* TRM feature array is the list of client that has active features,
	this list has same attributes as conflict array */
typedef TRMClientArray      TRMConflictArray, TRMFeatureArray;

#endif /* TRM_CLIENT_ARRAY_H */
