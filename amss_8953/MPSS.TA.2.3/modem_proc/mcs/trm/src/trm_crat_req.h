#ifndef TRM_CRAT_REQ_H
#define TRM_CRAT_REQ_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

              Transceiver Resource Manager Internal Header File

GENERAL DESCRIPTION

  This file provides some common definitions for trm.cpp & trm_crat_req.cpp


EXTERNALIZED FUNCTIONS

  None


REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/mcs.mpss/5.0/trm/src/trm_crat_req.h#2 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
03/09/2016   ag      Fix memory leaks   
05/14/2014   rj      Intial Version

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "customer.h"
#include "modem_mcs_defs.h"
#include "trm_dot_init.h"

extern "C"
{
  #include "trm.h"
}


/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/


/*----------------------------------------------------------------------------
  CRAT Requirement Table information
----------------------------------------------------------------------------*/
typedef enum
{
  /* CRAT Requirement Table Invalid Enum */
  TRM_CRAT_REQU_TABLE_INVALID = -1,

  /* CRAT Requirement Table StandAlone BEST Tx Enum */
  TRM_CRAT_REQU_TABLE_SA_BEST_TX,

  /* CRAT Requirement Table StandAlone BEST Rx Enum */
  TRM_CRAT_REQU_TABLE_SA_BEST_RX,

  /* CRAT Requirement Table StandAlone ANY Rx Enum */
  TRM_CRAT_REQU_TABLE_SA_ANY_RX,

  /* CRAT Requirement Table Two Tech Concurrency ANY Rx Enum */
  TRM_CRAT_REQU_TABLE_CC_ANY_RX,

  /* CRAT Requirement Table MAX Enum */
  TRM_CRAT_REQU_TABLE_MAX
}trm_crat_requ_tables_enum_type;

/*----------------------------------------------------------------------------
  CRAT Requirement State information
----------------------------------------------------------------------------*/
typedef enum
{
  /* CRAT Requirement State Invalid Enum */
  TRM_CRAT_REQU_STATE_INVALID = -1,

  /* CRAT Requirement DSDA State Enum */
  TRM_CRAT_REQU_STATE_DSDA,

  /* CRAT Requirement non-DSDA State Enum */
  TRM_CRAT_REQU_STATE_NON_DSDA,

  /* CRAT Requirement DR-MODE State Enum */
  TRM_CRAT_REQU_STATE_DRMODE,

  /* CRAT Requirement non-DR-MODE State Enum */
  TRM_CRAT_REQU_STATE_NON_DRMODE,

  /* CRAT Requirement State MAX Enum */
  TRM_CRAT_REQU_STATE_MAX
}trm_crat_requ_state_enum_type;

/*----------------------------------------------------------------------------
  CRAT Priority Enum
  This enum represents different types of priority
----------------------------------------------------------------------------*/
typedef enum
{
  /* Invalid CRAT Prio Enum */
  TRM_CRAT_PRIO_INVALID = -1,

  /* Priority of preferred Tx from RFC Requirement */
  TRM_CRAT_PRIO_PREFER_TX_FROM_RFC,

  /* Priority of preferred Rx from RFC Requirement */
  TRM_CRAT_PRIO_PREFER_RX_FROM_RFC,

  /* Priority of preferred Tx & PRx on same Antenna */
  TRM_CRAT_PRIO_PREFER_TX_PRX_ON_SAME_ANTENNA,

  /* Priority of Tie Rx & Tx to same WTR */
  TRM_CRAT_PRIO_TIE_RX_TX_TO_SAME_WTR,

  /* Priority of USE DGLNA */
  TRM_CRAT_PRIO_USE_DGLNA,

  /* Priority of preferred Rx from RFC Requirement for SUBx */
  TRM_CRAT_PRIO_PREFER_RX_FROM_RFC_FOR_SUBx,

  /* Priority of preferred Rx from RFC Requirement for SUBa */
  TRM_CRAT_PRIO_PREFER_RX_FROM_RFC_FOR_SUBa,

  /* Priority of Tie Both Techs to same WTR */
  TRM_CRAT_PRIO_TIE_TECHS_TO_SAME_WTR,

  /* Priority of Avoiding Hot Swapping for SUBx + TECHy */
  TRM_CRAT_PRIO_AVOID_HOT_SWAPPING_SUBx_TECHy,

  /* PRxonP during full concurrency flag */
  TRM_CRAT_PRIO_PRX_ON_P_FULL_CONNC,

  /* PRxonD during full concurrency flag using DGLNA flag */
  TRM_CRAT_PRIO_PRX_ON_D_FULL_CONNC_DGLNA_FLAG,

  TRM_CRAT_PRIO_MAX
} trm_crat_priority_enum_type;

/*----------------------------------------------------------------------------
  TRM CRAT info on Sub's Rx Resource Request
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_CRAT_SUB_RX_RES_INVALID = -1,
  TRM_CRAT_SUB_RES_BEST_RX,
  TRM_CRAT_SUB_RES_ANY_RX,
  TRM_CRAT_SUB_RX_RES_MAX
}trm_crat_sub_rx_res_enum_type;

/*----------------------------------------------------------------------------
  TRM CRAT StandAlone Priority Info
----------------------------------------------------------------------------*/

typedef struct
{
  /* Type of Priority */
  trm_crat_priority_enum_type  type; 

  /* Priority Value */
  int                          priority; 
}trm_crat_sa_client_prio_info;

/*----------------------------------------------------------------------------
  TRM CRAT Concurrency Priority Info
----------------------------------------------------------------------------*/

typedef struct
{
  /* Type of Priority */
  trm_crat_priority_enum_type  type; 

  /* Priority Value */
  int                          priority; 

  /* PRx ON during full concurrency (based on DGLNA ON or not) */
  boolean                      pRxOn_flag; 

  /* This shows whether current node is for SUBa */
  boolean  is_SUBa;  

}trm_crat_cc_client_prio_info;

/*----------------------------------------------------------------------------
  TRM CRAT Concurrency Priority Info
----------------------------------------------------------------------------*/

typedef struct TrmCratClientPrioList
{
  /* Client CallBack function */
  trm_crat_cc_client_prio_info prio_node;

  /* Pointer to next node in this linked list */
  struct TrmCratClientPrioList*      pNext;
  
}trm_crat_client_prio_list;

/*----------------------------------------------------------------------------
  Transceiver client information
----------------------------------------------------------------------------*/
class TRMCratClientPrioRowNode
{
  /* The client who's structure this is */
  trm_crat_requ_tables_enum_type         tbl_type;

  /* Head node of Client Prio List */
  trm_crat_client_prio_list*             head;

  /* SUBa node of Client Prio List - will be used to access element faster  */
  trm_crat_client_prio_list*             head_SUBa;

  /* Current node of Client Prio List - will be used to get Next node */
  trm_crat_client_prio_list*             curr_node;

  /* PRxonP node of Client Prio List - will be used to access element faster */
  trm_crat_client_prio_list*             PRxonP_node;

  /* PRxonD node of Client Prio List - will be used to access element faster */
  trm_crat_client_prio_list*             PRxonD_node;

  boolean add_sa_node(trm_crat_cc_client_prio_info*          new_prio);

  void add_cc_node(trm_crat_cc_client_prio_info* new_prio);

  //trm_crat_client_prio_list* get_SUBa_node_start();

  trm_crat_client_prio_list* get_SUBx_node_end();

public:

  /* Next Priority for Stand Alone */
  TRMCratClientPrioRowNode(void);

  /* Initialize Current Node to Head for client query start */
  void  init_curr_node();

  /* Next Priority for Stand Alone */
  boolean  add(trm_crat_cc_client_prio_info*          new_prio, 
                 trm_crat_requ_tables_enum_type         tbl);

  /* Next Priority for Stand Alone */
  trm_crat_sa_client_prio_info  next_priority();

  /* Next Priority for Concurrency */
  trm_crat_cc_client_prio_info  next_priority(boolean is_dglna_on);

  void destroy_list();
};

/*----------------------------------------------------------------------------
  TRM CRAT Priority Info per CRAT Table
----------------------------------------------------------------------------*/

typedef struct
{
  /* Next Priority for Stand Alone */
  TRMCratClientPrioRowNode*       trm_crat_node;
}trm_crat_requ_node_obj;

/*----------------------------------------------------------------------------
  TRM CRAT Priority Info per CRAT Table
----------------------------------------------------------------------------*/

typedef struct
{
  /* Next Priority for Stand Alone with Max limit as TRM_CRAT_SUB_RX_RES_MAX*/
  trm_crat_requ_node_obj       cc_node[TRM_CRAT_SUB_RX_RES_MAX];

}trm_crat_cc_prio_table_format;

/*----------------------------------------------------------------------------
  TRM CRAT Priority Info per CRAT Table
----------------------------------------------------------------------------*/

typedef struct
{
  /* Next Priority for Stand Alone with Max limit as TRM_CRAT_REQU_STATE_MAX*/
  trm_crat_requ_node_obj       sa_info[TRM_CRAT_REQU_STATE_MAX];

  /* Next Priority for Concurrency */
  trm_crat_cc_prio_table_format       cc_info[TRM_DOT_DDS_MAX][TRM_DOT_RAT_MAX];
}trm_crat_prio_info_per_subs_tech;

/*----------------------------------------------------------------------------
  TRM CRAT Priority Info per CRAT Table
----------------------------------------------------------------------------*/

typedef struct
{
  /* Data Structure for CRAT Table */
  trm_crat_prio_info_per_subs_tech node[TRM_DOT_DDS_MAX][TRM_DOT_RAT_MAX];
}trm_crat_prio_info_per_table;


/*----------------------------------------------------------------------------
  TRM CRAT Requirement Info
----------------------------------------------------------------------------*/

typedef struct
{
  /* Data Structure for CRAT Table */
  trm_crat_prio_info_per_table        crat_table[TRM_CRAT_REQU_TABLE_MAX];

}trm_crat_requ_info;

/*----------------------------------------------------------------------------
  Input information for TRM CRAT Stand Alone Scenairo 
----------------------------------------------------------------------------*/
typedef struct
{
  /* This gives Type of SUB - DDS/non-DDS */
  trm_dot_dds_enum_type           sub_type;

  /* TECH info for the */
  trm_dot_rat_enum_type           tech_id;

  /* This gives Type of State - DRMODE/non-DRMODE */
  trm_crat_requ_state_enum_type   state_type;

} trm_crat_prio_obj_sa_input_info;

/*----------------------------------------------------------------------------
  Input information for TRM CRAT Concurrency Priority table 
----------------------------------------------------------------------------*/
typedef struct
{
  /* This gives Type of SUB - DDS/non-DDS */
  trm_dot_dds_enum_type           sub1_type;

  /* TECH1 info for the */
  trm_dot_rat_enum_type           tech1_id;

  /* TRM CRAT info on Sub's Rx Resource Request */
  trm_crat_sub_rx_res_enum_type   sub1_rx_res;

  /* This gives Type of SUB - DDS/non-DDS */
  trm_dot_dds_enum_type           sub2_type;

  /* TECH2 info for the */
  trm_dot_rat_enum_type           tech2_id;

  /* Choose between PRxonP during full concurrency flag with/out DGLNA */
  //boolean                         dglna_on;

} trm_crat_prio_obj_cc_input_info;


/*----------------------------------------------------------------------------
  Union for all set state input data
----------------------------------------------------------------------------*/
typedef union
{
  /* Stand Alone Data */
  trm_crat_prio_obj_sa_input_info   sa_data;

  /* Concurrency Data */
  trm_crat_prio_obj_cc_input_info   cc_data;
} trm_crat_prio_data_input_u;

/*----------------------------------------------------------------------------
  Input information for TRM CRAT Priority Object 
----------------------------------------------------------------------------*/
typedef struct
{
  /* TECH1 info for the */
  trm_crat_requ_tables_enum_type        table_type;

  /* Union for all set state input data */
  trm_crat_prio_data_input_u            data_in;

} trm_crat_prio_input_info;

/*----------------------------------------------------------------------------
  OutPut information for TRM CRAT Priority Object 
----------------------------------------------------------------------------*/
typedef struct
{
  /* Table type to be used */
  trm_crat_requ_tables_enum_type        table_type;

  TRMCratClientPrioRowNode*       trm_crat_node;
} trm_crat_prio_output_info;


/*============================================================================

FUNCTION TRM_CRAT_GET_PRIORITY_OBJ

DESCRIPTION
  This API is required to get object for Priority of client
  
DEPENDENCIES
  None

RETURN VALUE
  Object for priority for the RAT in Device Allocation

SIDE EFFECTS
  None

============================================================================*/
void trm_crat_get_priority_obj
(
/* Input Structure for Request */
trm_crat_prio_input_info  *input,

/* Output Structure from Request Processing */
trm_crat_prio_output_info *output
);

/*============================================================================

FUNCTION TRM_CRAT_REQU_PARSE_INIT

DESCRIPTION
  This API is to initialize and parse CRAT Requirement document
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_crat_requ_parse_init( void );

/*============================================================================

FUNCTION TRM_CRAT_RESET_PRIORITY_OBJ_TO_HEAD

DESCRIPTION
  This API will set object to head of Linked List Node
  
DEPENDENCIES
  Caller should get valid Node from TRM_CRAT 

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_crat_reset_priority_obj_to_head
(
/* Input Structure for Request */
  TRMCratClientPrioRowNode*       trm_crat_node
);

/*============================================================================

FUNCTION TRM_CRAT_DELETE_OBJS

DESCRIPTION
  This API will clear all the memory and delete the CRAT Req objects
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_crat_delete_objs
(
  void
);

#endif /* TRM_CRAT_REQ_H */
