/*!
  @file
  coex_nv.c

  @brief
  Implementation of APIs to manage for CXM's connection/transport interfaces

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

                                 ( CXM )

                   Co-Existence Manager Source File

GENERAL DESCRIPTION

  This file provides Interface to the Connection Manager's NV items


EXTERNALIZED FUNCTIONS

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/cxm/src/coex_nv.c#8 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
10/13/15   btl     Add cxm_mode NV item
08/22/15   cdh     Spreadsheet to victim table autogen support, version 10 NV
02/20/14   tak     Updated to version 8 NV
09/30/13   tak     Updated to version 7 NV
12/21/12   btl     Port Optimize efs init writes at boot if file exists and
                     cxm_uart baud rate powerup fix
09/27/12   cab     Cleaned up compiler warning for unused return value
09/06/12   cab     Set default values, remove assert
07/20/12   cab     Extend featurization to deal with lte deps
07/18/12   cab     Updated NV versioning and protocols
07/10/12   cab     Adding some messaging
05/16/12   cab     Initial revision

==========================================================================*/



/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/
#include <IxErrno.h>
#include <comdef.h>
#include <stringl.h>
#include "fs_public.h"
#include "msg.h"
#include "coex_nv.h"
#include "coex_algos.h"
#include "cxm_utils.h"
#include <wci2_uart.h>
#include "npa.h"
#ifdef FEATURE_COEX_USE_NV
#include "coex_victim_table.h"
#endif

/*=============================================================================

                         INTERNAL DEFINES

=============================================================================*/
/* error code for NV read as described in fs_public.h */
#define CXM_NV_READ_ERROR -1

/*=============================================================================

                       INTERNAL LOCAL HELPER FUNCTIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  coex_nv_set_default

=============================================================================*/
/*!
    @brief
    This method maintains the internal mapping between the indication method
    id and offset of the message in the message map.

    @return
    int32 offset
*/
/*===========================================================================*/
static void coex_nv_set_default (
  coex_config_params_v10 *coex_nv
)
{
  int32 fd;

/*--------------------------------------------------------------------------*/

  /*set coex params to default values*/
  CXM_MSG_0( HIGH, "Setting NV to default values" );
  set_coex_params();

  fd = efs_open( COEX_CONFIG_DATA_FILE_PATH, O_CREAT|O_WRONLY|O_TRUNC, 
                 ALLPERMS );
  if ( fd < 0 )
  {
    /* cannot open the file for writing, what gives? */
    CXM_MSG_0( ERROR, "Cannot write data to NV" );
  }
  else
  {
    /* write default data to file */
    efs_write( fd, coex_nv, sizeof(coex_config_params_v10) );
    efs_close( fd );
  }

}

/*=============================================================================

  FUNCTION:  coex_nv_read_mode

=============================================================================*/
/*!
    @brief
    Read the NV EFS item containing the CXM Protocol, if it exists, from NV.

    @return
    Protocol if item is present, else -1.
*/
/*===========================================================================*/
void coex_nv_read_mode(
  coex_config_params_v10 *coex_nv
)
{
  uint16 efs_data;
  int32  fd;
  /*-----------------------------------------------------------------------*/
  fd = efs_open( CXM_MODE_EFS_FILE_PATH, O_RDONLY, ALLPERMS );
  if ( fd < 0 )
  {
    /* could not read the existing file */
  }
  else
  {
    efs_read( fd, &efs_data, sizeof(efs_data) );
    efs_close( fd );
    coex_nv->cxm_coex_protocol = (uint8) efs_data;
    CXM_MSG_1( HIGH, "NV Protocol set: %d", efs_data);
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_nv_read_config_data

=============================================================================*/
/*!
    @brief

    @return
*/
/*===========================================================================*/
void coex_nv_read_config_data (
  coex_config_params_v10 *coex_nv
)
{
  uint8 version = 0;
  int32 fd;
  /*-----------------------------------------------------------------------*/
  fd = efs_open( COEX_CONFIG_DATA_FILE_PATH, O_RDONLY, ALLPERMS );
  if ( fd < 0 )
  {
    /* could not read the existing file */

    /* create default values and open file to write */
    efs_mkdir( COEX_CONFIG_DATA_FILE_DIR, ALLPERMS );
    coex_nv_set_default(coex_nv);
  }
  else
  {
    efs_read( fd, &version, sizeof(uint8) );
    efs_close( fd );

    CXM_MSG_2( HIGH, "NV version %d found, expected verson %d", version, 
               CXM_CONFIG_VERSION );

    if ( version == CXM_CONFIG_VERSION )
    {
      /* read data from file */
      fd = efs_open( COEX_CONFIG_DATA_FILE_PATH, O_RDONLY, ALLPERMS );
      efs_read( fd, coex_nv, sizeof(coex_config_params_v10) );
      efs_close( fd );
    }
    else
    {
      coex_nv_set_default(coex_nv);
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_nv_create_conf_file

=============================================================================*/
/*!
    @brief
    Create a .conf file for backing up the efs item

    @return
    void
*/
/*===========================================================================*/
void coex_nv_create_conf_file( const char *conf_path, const char *item_path )
{
  int32               fd;
  char                eol = '\n';
  struct fs_stat      conf_file_det;
  size_t              data_size;
  int                 file_stat_result;
  /*------------------------------------------------------------------------*/
  file_stat_result = efs_stat(conf_path, &conf_file_det);
  data_size = (size_t)( strlen(item_path) + sizeof(eol) );

  /* If the conf file does not exist or if the size of the existing conf file is
     not what we expect, create a new conf file */
  if ( file_stat_result != 0 || data_size != conf_file_det.st_size )
  {
    /* Create conf file for backing up EFS files, erase it if it already exists */
    fd = efs_open( conf_path, O_CREAT|O_WRONLY|O_TRUNC, ALLPERMS );

    if (fd < 0)
    {
      CXM_MSG_1( ERROR, "Error opening EFS config file %d", fd );
    }
    else
    {
      /* Write the conf file */
      efs_write( fd, item_path, strlen(item_path) );
      efs_write( fd, &eol, sizeof(eol) );
      efs_close( fd );
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_nv_init

=============================================================================*/
/*!
    @brief

    @return
*/
/*===========================================================================*/
void coex_nv_init (
  coex_config_params_v10 *coex_nv
)
{
  /*--------------------------------------------------------------------------*/
#ifdef FEATURE_COEX_USE_NV
  /* obtain or create coex data in NV */
  coex_nv_create_conf_file(CXM_EFS_CONF_FILE_PATH, COEX_CONFIG_DATA_FILE_PATH);
  coex_nv_read_config_data(coex_nv);
#endif
  /*-----------------------------------------------------------------------*/
  /* Read the data from the EFS Protocol item file, if it exists. */
  coex_nv_create_conf_file(CXM_MODE_EFS_CONF_FILE_PATH, CXM_MODE_EFS_FILE_PATH);
  coex_nv_read_mode(coex_nv);

  return;
}

/*=============================================================================

  FUNCTION:  coex_init_victim_table

=============================================================================*/
/*!
    @brief

    @return
*/
/*===========================================================================*/
void coex_init_victim_table(
  coex_config_params_v10 *coex_nv,
  coex_conflict_type     *victim_tbl
)
{
#ifdef FEATURE_COEX_USE_NV
  uint8                   i, j; 
  uint8                   num_static_confl = CXM_COEX_VT_VALID_STATIC_SIZE;
  /* note that num_conflicts is the number of conflicts from NV only, at this point */
  uint8                   nv_num_confl = coex_nv->num_conflicts;
  int32                   fd;
  size_t                  size;
  fs_ssize_t              read_bytes;
  uint32                  total_read = 0;
/*--------------------------------------------------------------------------*/

  /* Since this is autogenerated, make sure it was properly generated */
  CXM_ASSERT( victim_tbl != NULL );
  /* If this fails, the victim table translation tool needs an update */
  CXM_ASSERT( coex_nv->num_conflicts <= ((CXM_COEX_VT_MAX_ENTRIES)) )

  if( COEX_SYS_ENABLED( CXM_SYS_BHVR_VICTIM_TABLE ) )
  {
    fd = efs_open( COEX_CONFIG_DATA_FILE_PATH, O_RDONLY, ALLPERMS );
    if ( fd < 0 )
    {
      /* File wouldn't open, so defaulting to whatever came from the spreadsheet */
      CXM_MSG_0( ERROR, "Config data failed to open; ignoring NV conflicts" );
      return;
    }
    else
    {
      /* scan through the config params at the beginning of the file */
      size = sizeof(coex_config_params_v10);
      while ( (size - total_read) > 0 )
      {
        read_bytes = efs_read( fd, ((uint8 *)coex_nv + total_read), (size - total_read) );
        CXM_ASSERT( read_bytes != CXM_NV_READ_ERROR );
        total_read += (uint32)read_bytes;          
      }

      if( COEX_SYS_ENABLED( CXM_SYS_BHVR_NV_ONLY_VICT_TBL ) )
      {
        /* victim table comes from EFS exclusively, so ignore autogenerated data */
        CXM_MSG_0( HIGH, "Ignoring spreadsheet victim table and using EFS data" );

        // first, clear the victim table of entries from spreadsheet
        memset( victim_tbl, 0, CXM_COEX_VT_MAX_ENTRIES * sizeof(coex_conflict_type) );

        /* reset total_read and read the victim table */
        total_read = 0;
        size = coex_nv->num_conflicts * sizeof(coex_conflict_type);
        while ( (size - total_read) > 0 )
        {
          read_bytes = efs_read( fd, ((uint8 *)victim_tbl + total_read), (size - total_read) );
          CXM_ASSERT( read_bytes != CXM_NV_READ_ERROR );
          total_read += (uint32)read_bytes;          
        }

        if( !COEX_SYS_ENABLED( CXM_SYS_BHVR_PROCESS_SAWLESS_CONFLICTS ) )
        {
          // iterate through victim table and remove SAWless entries
          for( i = 0; i < coex_nv->num_conflicts; i++ )
          {
            if( victim_tbl[i].reason == COEX_CONFLICT_SAWLESS )
            {
              // Replace SAWless conflict with conflict of other type, if possible
              for( j = nv_num_confl - 1; j >= i && j < CXM_COEX_VT_MAX_ENTRIES; j-- )
              {
                if( j == i )
                {
                  /* We may have already replaced entry i, so check if it is still a SAWless conflict */
                  if( victim_tbl[i].reason == COEX_CONFLICT_SAWLESS )
                  {
                    // Nothing to replace with, so just clear the entry
                    memset( &(victim_tbl[i]), 0, sizeof(coex_conflict_type) );
                    nv_num_confl--;
                  }
                  /* In the case where j == i == 0, avoid decrementing j (underflow)*/
                  break;
                }
                else if( victim_tbl[j].reason != COEX_CONFLICT_SAWLESS )
                {
                  // Overwrite conflict i with conflict j and exit inner loop
                  memscpy( &(victim_tbl[i]), sizeof(coex_conflict_type), &(victim_tbl[j]), sizeof(coex_conflict_type) );
                  memset( &(victim_tbl[j]), 0, sizeof(coex_conflict_type) );
                  nv_num_confl--;
                  break;
                }
                else
                {
                  // Entry j is also SAWless, so clear it
                  memset( &(victim_tbl[j]), 0, sizeof(coex_conflict_type) );
                  nv_num_confl--;
                }
              }
            }
          }
        }
        /* update the total number of conflicts with number post-processing */
        coex_nv->num_conflicts = nv_num_confl;

      } /* EFS victim table exclusively (old method) */

      /* else, read spreadsheet and NV conflicts */
      else
      {
        /* read autogenerated conflicts first, then a limited number from NV */

        /* limit the number of conflicts to read from NV */
        if( coex_nv->num_conflicts > CXM_COEX_VT_MAX_NV_ENTRIES )
        {
          CXM_MSG_0( HIGH, "Limiting number of victim table entries from NV" );
          coex_nv->num_conflicts = CXM_COEX_VT_MAX_NV_ENTRIES;
        }

        if( !COEX_SYS_ENABLED( CXM_SYS_BHVR_PROCESS_SAWLESS_CONFLICTS ) )
        {
          /* iterate through autogen victim table and remove SAWless entries */
          for( i = 0; i < CXM_COEX_VT_VALID_STATIC_SIZE; i++ )
          {
            if( victim_tbl[i].reason == COEX_CONFLICT_SAWLESS )
            {
              /* Replace SAWless conflict with conflict of other type, if possible */
              for( j = num_static_confl - 1; j >= i && j < CXM_COEX_VT_MAX_ENTRIES; j-- )
              {
                if( j == i )
                {
                  /* We may have already replaced entry i, so check if it is still a SAWless conflict */
                  if( victim_tbl[i].reason == COEX_CONFLICT_SAWLESS )
                  {
                    /* Nothing to replace with, so just clear the entry */
                    memset( &(victim_tbl[i]), 0, sizeof(coex_conflict_type) );
                    num_static_confl--; 
                  }
                  /* In the case where j == i == 0, avoid decrementing j (underflow)*/
                  break;
                }
                else if( victim_tbl[j].reason != COEX_CONFLICT_SAWLESS )
                {
                  /* Replace entry i with entry j and exit inner loop */
                  memscpy( &(victim_tbl[i]), sizeof(coex_conflict_type), &(victim_tbl[j]), sizeof(coex_conflict_type) );
                  memset( &(victim_tbl[j]), 0, sizeof(coex_conflict_type) );
                  num_static_confl--;
                  break;
                }
                else
                {
                  /* Entry j is also SAWless, so clear it */
                  memset( &(victim_tbl[j]), 0, sizeof(coex_conflict_type) );
                  num_static_confl--;
                }
              }
            }
          }
        }

        /* Handle optional conflicts from NV */
        if( num_static_confl >= 255 )
        {
          /* No room for any entries from NV */
          coex_nv->num_conflicts = 255;
        }
        else if( ((uint32)(coex_nv->num_conflicts) + (uint32)(num_static_confl)) >= 255 )
        {
          /* Read entries from NV until victim table is full */
          /* reset total_read and read the victim table */
          total_read = 0;
          size = (255 - num_static_confl) * sizeof(coex_conflict_type);
          while ( (size - total_read) > 0 )
          {
            read_bytes = efs_read( fd, ((uint8 *)(&(victim_tbl[num_static_confl])) + total_read), (size - total_read) );
            CXM_ASSERT( read_bytes != CXM_NV_READ_ERROR );
            total_read += (uint32)read_bytes;          
          }
        
          coex_nv->num_conflicts = 255;
        }
        else
        {
          /* Read maximum number of optional NV entries */
          /* reset total_read and read the victim table */
          total_read = 0;
          size = coex_nv->num_conflicts * sizeof(coex_conflict_type);
          while ( (size - total_read) > 0 )
          {
            read_bytes = efs_read( fd, ((uint8 *)(&(victim_tbl[num_static_confl])) + total_read), (size - total_read) );
            CXM_ASSERT( read_bytes != CXM_NV_READ_ERROR );
            total_read += (uint32)read_bytes;          
          }
          /* We know this won't overflow due to the else if case */
          coex_nv->num_conflicts += num_static_confl;
        }
      } // else, read from both spreadsheet and EFS

      efs_close( fd );
    } // else, fd >= 0

  } // CXM_SYS_BHVR_VICTIM_TABLE

#endif /* FEATURE_COEX_USE_NV */
  return;
}


