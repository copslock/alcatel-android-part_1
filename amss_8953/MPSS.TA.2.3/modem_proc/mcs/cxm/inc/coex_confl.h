#ifndef __COEX_CONFL_H__
#define __COEX_CONFL_H__
/*!
  @file
  coex_confl.h

  @brief
  This file contains the process of finding and choosing an active WCN/WWAN
  conflict.

*/
/*===========================================================================
 *
  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/cxm/inc/coex_confl.h#5 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
05/15/15   btl     Initial version

==========================================================================*/
/*=============================================================================

                      INCLUDES

=============================================================================*/
#include <comdef.h>
#include <cxm.h>
#ifdef FEATURE_COEX_USE_NV
#include "coex_victim_table.h"
#endif /* FEATURE_COEX_USE_NV */

/*=============================================================================

                      DEFINES AND TYPEDEFS

=============================================================================*/
#ifdef FEATURE_COEX_USE_NV
#define COEX_CONFL_INVALID_INDEX -1
#define COEX_CONFL_INDEX_MAX      (CXM_COEX_VT_MAX_ENTRIES - 1)
#define COEX_CONFL_INDEX_MIN      0
#endif /* FEATURE_COEX_USE_NV */
#define CXM_MAX_ACTIVE_CONFS      8
#define CXM_MAX_OVLP_CONFS        32

/* wlan modes in order of priority consideration for conflict matching */
typedef enum
{
  /* index corresponding to CXM_WLAN_HIGH_PRIO_TYPE conflict */
  COEX_WCN_MODE_HP   = 0,

  /* index corresponding to CXM_WLAN_CONN_TYPE conflict */
  COEX_WCN_MODE_CONN = 1,

  /* do not use as index -- max (array sizer) */
  COEX_WCN_MODE_MAX  = 2

} coex_wcn_mode_e;

/* conflict match options in order of priority (best match at end) */
typedef enum
{
  /* no match -- conflict inactive */
  COEX_MATCH_NONE             = 0,

  /* partial overlap between operating freq and conflict freq ranges */
  COEX_MATCH_PART_OVERLAP     = 1,

  /* match operating center freq within conflict definition */
  COEX_MATCH_CF               = 2,

  /* complete overlap between operating freq and conflict freq ranges */
  COEX_MATCH_COMPLETE_OVERLAP = 3

} coex_conflict_match_e;

/* for storing active and overlap conflicts to be passed to WLAN */
typedef struct
{
  /* Index of conflict */ 
  uint8                 index;
  /* Row number (from spreadsheet) of conflict */
  uint16                xl_row_num;
  /* Tech that triggered this row */ 
  cxm_tech_type         tech;
  /* Carrier IDs of conflict */ 
  cxm_carrier_e         carrier;
  /* Conflict match type */
  coex_conflict_match_e match;
} cxm_confl_s;

/* conflicts for all active techs */
typedef struct
{
  uint8 num_best;
  cxm_confl_s     best_indx[CXM_MAX_ACTIVE_CONFS];
  uint8 num_ovlp;
  cxm_confl_s     ovlp_indx[CXM_MAX_OVLP_CONFS];
} cxm_tot_cofl_s;

/* passed into conflict finding functions, returns containing new indices,
 * or COEX_CONFL_INVALID_INDEX */
typedef struct
{
  cxm_tech_type     wwan_tech;
  cxm_wcn_tech_type wcn_tech;
  int16             active_index[CXM_CARRIER_MAX];
  uint16            active_row_num[CXM_CARRIER_MAX];
} coex_confl_retval;

/* potential/active conflicts for all active techs */
typedef struct
{
  boolean         wan_updated;
  boolean         wcn_updated;
  cxm_tot_cofl_s  tot_confls;
} coex_confl_total_retval;

/* callback to return new active conflict indices */
typedef void (*coex_confl_active_cb)( coex_confl_retval *retval );

/* callback to return total conflict info (aggregated for all techs) */
typedef void (*coex_confl_total_cb)( coex_confl_total_retval *retval );

/* wcn bands iterator work callback -- provided by coex_confl */
typedef void (*coex_confl_wcn_work_cb)( 
  cxm_wcn_tech_type   tech, 
  coex_wcn_mode_e     mode,
  coex_band_type_v01 *band
);

/* wcn bands iterator -- coex_algos must provide */
typedef void (*coex_confl_wcn_band_itr_cb)(
  cxm_wcn_tech_type           tech,
  coex_wcn_mode_e             mode,
  coex_confl_wcn_work_cb      work
);

/*=============================================================================

                      EXTERNAL FUNCTION DECLARATIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  coex_confl_init

=============================================================================*/
/*!
    @brief
    Construct the data structures used in finding (a) conflict(s) in 
    victim table-based coex operation.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_init(
  coex_config_params_v10 *params,
  coex_conflict_type     *victim_tbl
);

/*=============================================================================

  FUNCTION:  coex_confl_deinit

=============================================================================*/
/*!
    @brief
    Free any memory used by coex conflict processing

    @return
    void
*/
/*===========================================================================*/
void coex_confl_deinit( void );

/*=============================================================================

  FUNCTION:  coex_confl_set_cb

=============================================================================*/
/*!
    @brief
    Set a callback that the conflict processing algorithm will call with
    the result. Multiple callbacks are allowed.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_set_cb(
  coex_confl_active_cb tech_cb,
  coex_confl_total_cb  total_cb
);

/*=============================================================================

  FUNCTION:  coex_confl_set_wcn_band_iterator

=============================================================================*/
/*!
    @brief
    Callback that the conflict processing algorithm will call to iterate
    over all the WCN bands, passing each WCN band in turn for processing.
    This enables us to support multiple WCN band formats and separate the data

    @return
    void
*/
/*===========================================================================*/
void coex_confl_set_wcn_band_iterator(
  coex_confl_wcn_band_itr_cb  wcn_iterator
);

/*=============================================================================

  FUNCTION:  coex_confl_update_wwan_tech

=============================================================================*/
/*!
    @brief
    Update the list of possible conflicts that apply to the given tech.
    This will in turn call coex_find_conflict to pick the best conflict,
    if any, out of the list of active conflicts in each tech

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table

    @return
    none
*/
/*===========================================================================*/
void coex_confl_update_wwan_tech(
  cxm_tech_type      tech,
  cxm_tech_data_s   *band_info
);

/*=============================================================================

  FUNCTION:  coex_confl_update_wcn_tech

=============================================================================*/
/*!
    @brief
    Update the list of possible conflicts that apply to the given tech.
    This will in turn call coex_find_conflict to pick the best conflict,
    if any, out of the list of active conflicts in each tech

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table

    @return
    none
*/
/*===========================================================================*/
void coex_confl_update_wcn_tech(
  cxm_wcn_tech_type tech,
  uint32            mode_mask
);

#endif /*__COEX_CONFL_H__*/
