
#ifndef LOCAL_COMDEF_H
#define LOCAL_COMDEF_H

/*============================================================================*/
/** @file  
 * This module has definition and declaration related to MCAL WCDMA search
 * driver module APIs
 */
/*============================================================================*/

/*============================================================================
Copyright (c) 2006 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
============================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/wcdma.mpss/8.2.3/l1/offline/src/local_comdef.h#1 $
$DateTime: 2016/02/25 15:55:24 $
$Author: pwbldsvc $

when        who     what, where, why
--------    ---     --------------------------------------------------------

===========================================================================*/

#endif /* LOCAL_COMDEF_H */
