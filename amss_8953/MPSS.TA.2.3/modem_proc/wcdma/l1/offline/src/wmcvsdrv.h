#ifndef WMCVSDRV_H
#define WMCVSDRV_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         WCDMA MCVS Driver interface

GENERAL DESCRIPTION

  This file contains the source code for the WCDMA Modem Power Abstraction
  layer implementation.


Copyright (c) 2000-2015 by Qualcomm Technologies, Inc. All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/wcdma.mpss/8.2.3/l1/offline/src/wmcvsdrv.h#3 $
$DateTime: 2016/06/17 04:06:12 $
$Author: pwbldsvc $

when           who      what, where, why
--------       ----     ---------------------------------------------------------- 
06/17/16       kr       Need Q6A to be at 288 with FBRX in voice call for 8940
03/01/16       kr       Make sure Q6A runs at>=288 MHZ with WTR3925[FBRX enabled] 
02/12/16       kr       Release MCVS with value other than 0 for q6B[Jacala specific]
1/8/16         kr       New clk plan for Jacala
12/18/15       kr       Send Q6B Clk Cfg command in a QTA gap for IDLE mode [max of IDLE / Data SUB]
11/04/15       kr       FR 30623: Searcher MCVS optimization,Support for LOW SVS for srch cycle count
10/27/15       kr       BACKOUT--FR 30623: Searcher MCVS optimization,Support for LOW SVS for srch cycle count
10/27/15       kr       FR 30623: Searcher MCVS optimization,Support for LOW SVS for srch cycle count
09/03/15       rs       Fixing compiler warnings for Atlas
08/03/15       Kr       Handle race condition in IRP+MCVS  clock losering scenario
07/22/15       kr       Reset CME client info once QICE is released
07/17/15       sks      ONly access QICE clks using CME's TxD and RxD status.
07/17/15       rsr      Added New HAndling for MCVS + CME.
07/08/14       rsr      Add current q6 and vpe speed api
07/02/15       bj       Increase the split acq step1+NASTT step lock duration from  80 to 85 ms 
                        and boost the clock level to NOM for NASTT search in ACQ state to avoid 1x page miss.
06/19/15       kr       TA.2.0 Enable LOW SVS for Eldarian
06/15/15       rs       BOLT W+W changes
06/12/15       sks      TH - MCVS Changes for using single VU in DC in DS mode.
06/10/15       kr       send MCVS request for Q6A=384 from L1 whenever QICE is enabled
06/03/15       kr       TA featurization
01/19/15       sks      Global re-org for W+W
12/18/14       sks      CPC LS changes
12/05/14       psr      Thor compilation fixes
12/01/14       stk      FR24685:UMTS MCVS for Thor Modem
10/1/14        stk      Fixed the NV bug for clock bumpup and added support for THOR clk bump up via NV
09/16/14       stk      JO SW-FW interface change to support Q6 clock speeds 
08/08/14       sks      Initial Version. MCPM/MCVS code cleanup and JO MCVS 

 ===========================================================================*/

#include "rex.h"
#include "mcpm_api.h"
#include "l1const.h"

#ifdef FEATURE_WCDMA_MCVS

#if defined(FEATURE_WCDMA_BOLT_MODEM) && !(defined(FEATURE_WCDMA_THOR_MODEM))

/* ******************************************** */
/*                MACROS                        */
/* ******************************************** */
#define MAX_CARRIERS 4
#define WMCVS_PERCENT_UTIL_HIGH   85
#define WMCVS_PERCENT_UTIL_LOW    80
#define WMCVS_SRCH_PERIOD360      36
#define WMCVS_SRCH_PERIOD180      18
#define MAX_PRIORITY_SRCH_CLIENT_PERIOD_180  4
#define MAX_PRIORITY_SRCH_CLIENT             6


/* ******************************************** */
/*                ENUM DECLARATIONS             */
/* ******************************************** */

/* MCVS clients */
typedef enum
{
  WCDMA_WMCPM_MCVSDRV_VPE_PULLIN_UPDATE,      /* 0 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE,             /* 1 */
  WCDMA_WMCPM_MCVSDRV_Q6CLK_UPDATE,           /* 2 */
  WCDMA_WMCPM_MCVSDRV_SRCHCLK_UPDATE,         /* 3 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE_CME,         /* 4 */
}wmcvsdrv_client_type;

/*VPE speed for MCVS in Khz*/
typedef enum
{
  WMCVS_VPE_NULL=0, 
  WMCVS_VPE_285K=285000,  /*SVS2 - CLK SPEED 1 FOR SEARCHER CLIENT */
  WMCVS_VPE_499K= 499000, /*SVS - CLK SPEED 2 FOR SEARCHER CLIENT */
  WMCVS_VPE_666K=666000,  /*NOM for BOLT and THOR - CLK SPEED 3 FOR SEARCHER CLIENT */
  WMCVS_VPE_768K= 768000, /*TURBO - CLK SPEED 4 FOR SEARCHER CLIENT */
  WMCVS_VPE_800K= 800000  /*TURBO SPEED FOR THOR */
} wmcpmdrv_mcvs_vpe_speedType;

/*q6 speed for MCVS in Khz*/
typedef enum
{
  WMCVS_Q6CLK_NULL=0,   
  WMCVS_Q6CLK_144K= 144000,
  WMCVS_Q6CLK_288K= 288000,
  WMCVS_Q6CLK_307_2K= 307200, /*SVS2*/
  WMCVS_Q6CLK_518_4K= 518400, /*SVS*/
  WMCVS_Q6CLK_614_4K= 614400, /*NOM for BOLT*/
  WMCVS_Q6CLK_652_8K=652800,  /*NOM for THOR*/
  WMCVS_Q6CLK_710_4K = 729600, /*TURBO*/
  WMCVS_Q6CLK_850K = 850000   /*TURBO for THOR*/
} wmcpmdrv_mcvs_q6clk_speedType;

/* ENUMs that map to the offline MCPM speed types. needed for comparing against the current speed sent to FW in order to abort searches if necessary.*/
typedef enum
{   
   W_OFFLINE_INVALID = MCPM_CLK_NULL,   
   W_OFFLINE_CLK_19_2M = MCPM_CLK_19_2M,
   W_OFFLINE_CLK_72M = MCPM_CLK_72M,
   W_OFFLINE_143M = MCPM_CLK_143M,
   W_OFFLINE_144M = MCPM_CLK_144M,
   W_OFFLINE_250M = MCPM_CLK_250M,
   W_OFFLINE_333M = MCPM_CLK_333M,
   W_OFFLINE_384_5M = MCPM_CLK_384_5M,
   W_OFFLINE_399M = MCPM_CLK_399M,
} wmcpmdrv_mcpm_offline_clk_speed_type;

/* ******************************************** */
/*               COMMON GLOBAL VARIABLES        */
/* ******************************************** */

/* offline clock speed enums no longer needed */
typedef struct 
{
   /*# SHO fingers */
   uint8 num_aset_fingers[MAX_CARRIERS];
      /*# SHO cells */
   uint8 num_sho_cells[MAX_CARRIERS];
   /*number of neighbors*/
   uint8 num_neighbor_cells[MAX_CARRIERS];
   /*Total number of fingers*/
   uint8 tot_num_of_fingers[MAX_CARRIERS];
   /* total number of cells will be used for PULL in*/
   uint8 total_num_cells;
   /*TxD is present or not*/	  
   boolean status_txd[MAX_CARRIERS];
   /*PCCPCH  info in DCH */
   boolean pccpch_enabled ;
}wmcpmdrv_mcvs_vpe_update_struct_type;

typedef struct
{
  wmcpmdrv_mcvs_q6clk_speedType wmcpmdrv_mcvs_int_q6clk_speed;
  uint32 wmcvs_debug_bumpup_vpe_offline;
  mcpm_mcvsrequest_parms_type wmcpmdrv_wmcvs_request_params;
  wmcpmdrv_mcvs_vpe_update_struct_type wmcpmdrv_mcvs_vpe_update_params;
  uint8 wmcpmdrv_mcvs_cme_mask;
  uint16 wmcpm_mcvs_int_srch_period;
  wmcpmdrv_mcvs_vpe_speedType wmcpmdrv_mcvs_int_vpe_speed;
}wmcvsdrv_ext_cntrl_params_type;

extern wmcvsdrv_ext_cntrl_params_type wmcvsdrv_ext_cntrl_params[WCDMA_NUM_SUBS];

/*
SVS switching condition for R99 :
No-TD :
   (NUM OF SHO CELLS < 5 and 2*SHO_CELLS + ASET FINGERS > 11) OR
   (TOTAL NUMBER OF FINGERS > = 11) OR
   (NUM OF SHO CELLS = 4) OR
   (PCCPCH DECODING ON (EITHER SERVING OR NEIGHBOR))
With-TD :
   (NUM OF SHO CELLS < 5 and 2*SHO_CELLS + ASET FINGERS > 10) OR
   (TOTAL NUMBER OF FINGERS > = 9) OR
   (NUM OF SHO CELLS = 4) OR
   (PCCPCH DECODING ON (EITHER SERVING OR NEIGHBOR))
*/


#define WMCVS_VPE_PARAM_STATUS_R99_SVS_CARR(carr_idx) (((!wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx])\
                                             && (((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] < 5)\
                                             && ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] <<1)\
                                           + (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx])\
                                            > (11)))\
                                           ||\
                                           (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= (11))\
                                           ||\
                                           (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] ==4)\
                                           ||( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled)))\
                                           ||\
                                           ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx]) &&\
                                           (((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] < 5)\
                                           && ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] <<1)\
                                           + (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[carr_idx])\
                                            > (10)))\
                                           ||\
                                           ( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx]>= (9))\
                                           ||\
                                           (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] ==4)\
                                           ||( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled))))


/* SVS switching condition for HS:
No-TD:
    (NUM OF SHO CELLS >=2 AND <=4) OR
    (NUM OF SHO CELLS = 1 AND ASET FINGERS >=8) OR
    (QSET >=2) OR
    (PCCPCH DECODING ON (SERVING OR NEIGHBOR))
With-TD:
    (NUM OF SHO CELLS >=2 AND <=4) OR
    (NUM OF SHO CELLS = 1 AND NEIGHBOR CELLS >=2) OR
    (NUM OF SHO CELLS = 1 AND NEIGHBOR CELLS < 2 AND RxD on AND (ASET fingers >=6 OR QSET >=2 )) OR
    (NUM OF SHO CELLS = 1 AND NEIGHBOR CELLS < 2 AND RxD off AND (ASET fingers >=7 OR QSET >=2 )) OR 
    (PCCPCH DECODING ON (EITHER SERVING OR NEIGHBOR))
*/

#define WMCVS_VPE_PARAM_STATUS_HS_SVS_CARR(carr_idx) ((!wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx]\
                                                          &&((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] >= 2 && wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] <= 4 )\
                                                          ||((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >=8 ) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 1))\
                                                          ||(wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_cme_mask &( 2<<carr_idx))\
                                                          ||(wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled)))\
                                                          ||\
                                                          (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx]\
                                                          &&((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] >= 2 && wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] <= 4 )\
                                                          ||(wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 1  && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] >= 2))\
                                                          ||((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 1) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] < 2) && IS_RXD_ACTIVE() && ( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= 6 || (IS_QICE_ACTIVE() && !IS_EQ_ACTIVE()) ))\
                                                          ||((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] == 1) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_neighbor_cells[carr_idx] < 2) && !IS_RXD_ACTIVE() && ( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx] >= 7 || (IS_QICE_ACTIVE() && !IS_EQ_ACTIVE())))\
                                                          )\
                                                          )\
                                           ||( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.pccpch_enabled))

#define WMCVS_VPE_PARAM_STATUS_R99_SVS() (WMCVS_VPE_PARAM_STATUS_R99_SVS_CARR(0) || WMCVS_VPE_PARAM_STATUS_R99_SVS_CARR(1))

#define WMCVS_VPE_PARAM_STATUS_HS_SVS() (WMCVS_VPE_PARAM_STATUS_HS_SVS_CARR(0) || WMCVS_VPE_PARAM_STATUS_HS_SVS_CARR(1))

/* NOM switching condition for both R99 and HS
NUM OF SHO CELLS = 5 or 6
*/

#define WMCVS_VPE_PARAM_STATUS_NOM_CARR(carr_idx) (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx] >= 5)

#define WMCVS_VPE_PARAM_STATUS_NOM() (WMCVS_VPE_PARAM_STATUS_NOM_CARR(0) || WMCVS_VPE_PARAM_STATUS_NOM_CARR(1))


/*
Q6 switching condition to 288 MHz for VOICE + CM - not used currently.
With no TD:
      (NUM OF SHO CELLS = 1 AND ASET FINGERS > 9) OR
      (NUM OF SHO CELLS = 2 AND ASET FINGERS > 7) OR
      (NUM OF SHO CELLS = 3 AND ASET FINGERS > 5)
With TD:
       (NUM OF SHO CELLS = 1 AND ASET FINGERS > 8) OR
      (NUM OF SHO CELLS = 2 AND ASET FINGERS > 6) OR
      (NUM OF SHO CELLS = 3 AND ASET FINGERS > 4)
*/
#define WMCVS_Q6_PARAM_STATUS() ((!wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[0]\
                                   && (((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 1) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 9))\
                                   || ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 2) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 7))\
                                   || ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 3) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 5))))\
                                      ||\
                                   (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[0]\
                                   && (((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 1) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 8))\
                                   || ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 2) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 6))\
                                   || ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0] == 3) && (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_aset_fingers[0] > 4))))\
                                )

#define WMCVS_VPE_PARAM_STATUS_PULLIN()  ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.total_num_cells > 4) )

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_vpe_checks

DESCRIPTION
  This function checks MCVS BUMP UP/DOWN criteria for Pure MCVS VPE update calls

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_vpe_checks(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_vpe_update_in_mcvs_param

DESCRIPTION
function to send MCVS request when n ot already running at desired VPE frequency

PARAMETERS
VPE frequencywanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call at max will take 100usec
===========================================================================*/
void wmcpmdrv_vpe_update_in_mcvs_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_vpe_update_in_mcpm_param

DESCRIPTION
Update  MCVS params to send alongwith MCPM request
 
PARAMETERS
vpe_speed

RETURN VALUE
NONE

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_vpe_update_in_mcpm_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);


/*===========================================================================
                          COMMON LOCAL/EXTERN Function Declaration
============================================================================*/
/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_config_wrapper

DESCRIPTION
  Wrapper function to send MCVS_Config_modem with W globals set and reset. 
  This W global variable will be checked in the offline_clk_change_cb() when offline clk changes. 
  
PARAMETERS
  None.

RETURN VALUE

SIDE EFFECTS
  None. No SRCH abort should happen through MCVS, since searcher module takes care of it. 
===========================================================================*/
LOCAL void wmcpmdrv_mcvs_config_modem_wrapper(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcvsdrv_clear_mvcs_db()

DESCRIPTION
  memset WMCVS request_parms and all globals to zero.
  This will be called during W stack init and de-init
 
PARAMETERS
  None.
 
RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcvsdrv_clear_wmvcs_db(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_SRCH_PERIODICITY

DESCRIPTION
function to return the new/updated srch period.
 
PARAMETERS
  None.

RETURN VALUE
updated srch period

SIDE EFFECTS
  None.
===========================================================================*/
uint16 wmcpmdrv_mcvs_update_srch_periodicity(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_SRCH_CLK_TO_SRCH

DESCRIPTION
function to return the offline clk freq in Mhz.
 
PARAMETERS
  None.

RETURN VALUE
offline clk freq in Mhz

SIDE EFFECTS
  None.
===========================================================================*/
uint8 wmcpmdrv_mcvs_update_srch_clk_to_srch(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_triage_param_update

DESCRIPTION
  Wrapper function for retrieving TxD Status from WL1

PARAMETERS
  None.

RETURN VALUE
  TxD_OFF/TxD_ON/TxD_DC

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_triage_param_update(wmcpmdrv_mcvs_vpe_update_struct_type* vpe_params, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_Pullin_cell_cnt

DESCRIPTION
  Gets the cell cnt from demod during pull in procedure.
 
PARAMETERS
 global VPE struct Address

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_pullin_cell_cnt(wmcpmdrv_mcvs_vpe_update_struct_type *vpe_params, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcvs_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
pure MCVS req

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcvs_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcpm_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
MCVS embedded in mcpm req.

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcpm_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_get_cme_status

DESCRIPTION
  This function gets CME mask for QIce and updates MCVS internal global variable.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_get_cme_status(wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCVSDRV_REQUEST_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to request for power resources.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wmcvsdrv_request_client_power_config(wmcvsdrv_client_type client, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCVSDRV_RELINQUISH_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to relinquish for power resources.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wmcvsdrv_relinquish_client_power_config(wmcvsdrv_client_type client, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_get_mcpm_offline_clk_speed

DESCRIPTION
Called from inside the clock change callback to find out the new speed.

PARAMETERS
none

RETURN VALUE
none

SIDE EFFECTS
none 
 
===========================================================================*/
void wmcpmdrv_get_mcpm_offline_clk_speed(wsub_id_e_type wsub_id);

#else /* THOR Modem */

/* ******************************************** */
/*                  CONSTANTS                   */
/* ******************************************** */
#define MAX_CARRIERS                         2
#define WMCVS_PERCENT_UTIL_HIGH              85
#define WMCVS_PERCENT_UTIL_LOW               80
#define WMCVS_SRCH_PERIOD360                 36
#define WMCVS_SRCH_PERIOD180                 18
#define MAX_PRIORITY_SRCH_CLIENT_PERIOD_180  4
#define MAX_PRIORITY_SRCH_CLIENT             6

/* ******************************************** */
/*       STRUCTURES & ENUM DECLARATIONS         */
/* ******************************************** */
typedef enum
{
   MCVS_STANDALONE,
   MCVS_THROUGH_MCPM
}wmcvsdrv_mcvs_req_type;

/* MCVS clients */
typedef enum
{
  WCDMA_WMCPM_MCVSDRV_VPE_PULLIN_UPDATE,      /* 0 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE,             /* 1 */
  WCDMA_WMCPM_MCVSDRV_Q6CLK_UPDATE,           /* 2 */
  WCDMA_WMCPM_MCVSDRV_SRCHCLK_UPDATE,         /* 3 */
  WCDMA_WMCPM_MCVSDRV_VPE_UPDATE_CME,         /* 4 */
  WCDMA_WMCPM_MCVSDRV_CLIENT_Q6A_BOOST,       /* 5 */
  WCDMA_WMCPM_MCVSDRV_CLIENT_Q6A_MCVS,        /* 6 */
  WCDMA_WMCPM_MCVSDRV_Q6B_NASTT_UPDATE        /* 7 */
}wmcvsdrv_client_type;

#ifdef FEATURE_WCDMA_TABASCO_MODEM
/*used for Scalar Q6*/
typedef enum
{
#ifdef FEATURE_WCDMA_TABASCO_14NM_MODEM 
//For Jacala
  WMCVS_Q6CLK_NULL = 0, 
  WMCVS_Q6ACLK_LOW_CORNER1 = 288000, /*Non Corner*/
  WMCVS_Q6ACLK_SVS_LOW = 326400, /*LOW SVS*/
  WMCVS_Q6ACLK_SVS = 480000, /* SVS*/
  WMCVS_Q6ACLK_SVS_PLUS = 556800, /* SVS+*/  
  WMCVS_Q6ACLK_NOM = 691000, /* NOM*/
  WMCVS_Q6ACLK_NOM_PLUS = 748800, /* NOM+*/
  WMCVS_Q6ACLK_TURBO =  844800 /* TURBO*/ 
#else//Sahi
  WMCVS_Q6CLK_NULL = 0, 
  WMCVS_Q6CLK_SVS_LOW = 230400, /* SVS-*/
  WMCVS_Q6CLK_LOW_CORNER1 = 288000, /*Non Corner*/
  WMCVS_Q6CLK_SVS = 384000, /* SVS*/
  WMCVS_Q6CLK_SVS_PLUS = 480000, /* SVS+*/  
  WMCVS_Q6CLK_NOM = 576000, /* NOM*/
  WMCVS_Q6CLK_NOM_PLUS = 614000, /* NOM+*/
  WMCVS_Q6CLK_TURBO =  691200 /* TURBO*/    
#endif
} wmcpmdrv_mcvs_q6clk_speedType;

/*used for Vector Q6*/
typedef enum
{
#ifdef FEATURE_WCDMA_TABASCO_14NM_MODEM 
//For Jacala
   WMCVS_Q6BCLK_INVALID = 0,  
   WMCVS_Q6BCLK_REL = 19200,  
   WMCVS_Q6BCLK_SVS_LOW = 288000, /*  LOW SVS-*/
   WMCVS_Q6BCLK_SVS = 460800,  /* SVS*/
   WMCVS_Q6BCLK_SVS_PLUS = 518800,  /* SVS+*/
   WMCVS_Q6BCLK_NOM = 652800, /* NOM*/
   WMCVS_Q6BCLK_NOM_PLUS = 710000, /* NOM+*/
   WMCVS_Q6BCLK_TURBO = 806400   /*TURBO*/
#else
   W_Q6_INVALID = MCPM_CLK_NULL,   
   W_Q6_19_2M = 19200,   
   W_Q6_115M = 115200,
   W_Q6_211M = 211200, /* SVS-*/
   W_Q6_364M = 364800,  /* SVS*/
   W_Q6_460M = 460800,  /* SVS+*/
   W_Q6_556M = 556800, /* NOM*/
   W_Q6_576M = 576000, /* NOM+*/
   W_Q6_652M = 652800   /*TURBO*/
#endif
} wmcpmdrv_mcvs_q6vclk_speedtype;

#else
typedef enum
{
  WMCVS_Q6CLK_NULL  = 0,
  WMCVS_Q6CLK_SVS2  = 345600,
  WMCVS_Q6CLK_SVS   = 499200,
  WMCVS_Q6CLK_NOM   = 652800,
  WMCVS_Q6CLK_TURBO = 844800,
} wmcpmdrv_mcvs_q6clk_speedType;

typedef enum
{
  WMCVS_VPE_CLK_NULL    = 0, 
  WMCVS_VPE_CLK_SVS2    = 285000,  
  WMCVS_VPE_CLK_SVS     = 499000, 
  WMCVS_VPE_CLK_NOM     = 666000,  
  WMCVS_VPE_CLK_TURBO   = 799000, 
} wmcpmdrv_mcvs_vpe_speedType;

/* ENUMs that map to the offline MCPM speed types. needed for comparing against the current speed sent to FW in order to abort searches if necessary.*/
typedef enum
{   
   WMCVS_OFFLINE_INVALID = MCPM_CLK_NULL,   
   WMCVS_OFFLINE_19_2M   = MCPM_CLK_19_2M,
   WMCVS_OFFLINE_72M     = MCPM_CLK_72M,
   WMCVS_OFFLINE_143M    = MCPM_CLK_143M,
   WMCVS_OFFLINE_144M    = MCPM_CLK_144M,
   WMCVS_OFFLINE_250M    = MCPM_CLK_250M,
   WMCVS_OFFLINE_333M    = MCPM_CLK_333M,
   WMCVS_OFFLINE_384_5M  = MCPM_CLK_384_5M,
   WMCVS_OFFLINE_399M    = MCPM_CLK_399M,
} wmcpmdrv_mcpm_offline_clk_speed_type;

#endif
/* offline clock speed enums no longer needed */
typedef struct 
{
   /*# SHO fingers */
   uint8 num_aset_fingers[MAX_CARRIERS];
      /*# SHO cells */
   uint8 num_sho_cells[MAX_CARRIERS];
   /*number of neighbors*/
   uint8 num_neighbor_cells[MAX_CARRIERS];
   /*Total number of fingers*/
   uint8 tot_num_of_fingers[MAX_CARRIERS];
   /* total number of cells will be used for PULL in*/
   uint8 total_num_cells[MAX_CARRIERS];
   /*TxD is present or not*/	  
   boolean status_txd[MAX_CARRIERS];
   /*PCCPCH  info in DCH */
   boolean pccpch_enabled ;
}wmcpmdrv_mcvs_vpe_update_struct_type;

typedef struct
{ 
#ifdef FEATURE_WCDMA_TABASCO_MODEM 
  wmcpmdrv_mcvs_q6vclk_speedtype wmcpmdrv_mcvs_int_q6clk_speed;
  uint32 wmcpmdrv_mcvs_int_q6_scalar_clk_speed ;
  uint32 wmcvsdrv_int_client_bmask;
#else
  wmcpmdrv_mcvs_q6clk_speedType wmcpmdrv_mcvs_int_q6clk_speed;
  wmcpmdrv_mcvs_vpe_speedType wmcpmdrv_mcvs_int_vpe_speed;
#endif
  uint32 wmcvs_debug_bumpup_vpe_offline;
  mcpm_mcvsrequest_parms_type wmcpmdrv_wmcvs_request_params;
  wmcpmdrv_mcvs_vpe_update_struct_type wmcpmdrv_mcvs_vpe_update_params;
  uint16 wmcpm_mcvs_int_srch_period;

#ifdef FEATURE_WCDMA_TABASCO_MODEM
  uint8 wmcvsdrv_srch_cycle_count;
#endif
}wmcvsdrv_ext_cntrl_params_type;

extern wmcvsdrv_ext_cntrl_params_type wmcvsdrv_ext_cntrl_params[WCDMA_NUM_SUBS];


#ifdef FEATURE_WCDMA_TABASCO_MODEM

#define WMCVS_GET_MAX_VAL(a, b) ((a >= b) ? (a) : (b))
#define WMCVSDRV_CLIENT_MASK_VPE_UPDATE_CME             (0x1 << (int32)WCDMA_WMCPM_MCVSDRV_VPE_UPDATE_CME)
#define IS_VPE_CME_ACTIVE()                             (wmcvsdrv_ext_cntrl_params[wsub_id].wmcvsdrv_int_client_bmask & WMCVSDRV_CLIENT_MASK_VPE_UPDATE_CME)
#define WMCVSDRV_FING_STEP 3
#define WMCVDRV_CELL_STEP 2
#define WMCVSDRV_SET_INT_CLIENT_BMSK(client) \
do{ \
    wmcvsdrv_ext_cntrl_params[wsub_id].wmcvsdrv_int_client_bmask |= ((0x1) << client); \
}while(0)

#define WMCVSDRV_RESET_INT_CLIENT_BMSK(client) \
do{ \
    wmcvsdrv_ext_cntrl_params[wsub_id].wmcvsdrv_int_client_bmask &= ~((0x1) << client); \
}while(0)

#define WMCVS_SUM_SHO_CELLS(carr_idx)    ((carr_idx == 0) ? (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[0]) : ( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[1]))
#define WMCVS_SUM_TOT_FINGERS(carr_idx)  ((carr_idx == 0) ? (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[0]) : ( wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[1]))
#define WMCVS_MAX_SRCHR_MODES 3

#else
/* VPE SPEED type for the auto-gen table */
typedef enum
{
  WMCVS_VPE_INVALID, /* Always have INVALID at 0th index since the compute_vpe_speed() uses MAX logic */
  WMCVS_VPE_SVS2,
  WMCVS_VPE_SVS,
  WMCVS_VPE_NOM,
  WMCVS_VPE_TURBO
} wmcpmdrv_mcvs_vpe_enumType;

/* ******************************************** */
/*               COMMON GLOBAL VARIABLES        */
/* ******************************************** */

/* ******************************************** */
/*           MCVS SWITCHING ALGORITHM           */
/* ******************************************** */
/* RxD status is the same across both carriers*/

#define WMCVS_GET_ASET_CNT(carr_idx)   (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.num_sho_cells[carr_idx])
#define WMCVS_GET_TOTAL_ASET_CNT()     (WMCVS_GET_ASET_CNT(0) + WMCVS_GET_ASET_CNT(1))

#define WMCVS_GET_FING_CNT(carr_idx)   (wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.tot_num_of_fingers[carr_idx])
#define WMCVS_GET_TOTAL_FING_CNT()     (WMCVS_GET_FING_CNT(0) + WMCVS_GET_FING_CNT(1))

/* LSB two bits for pri carr and next two bits for sec carr. So right shift by 2 for Secondary only */
#define WMCVS_GET_QSET_SIZE(carr_idx)  ((carr_idx == 0) ? (wmcvsdrv_int_cntrl_params[wsub_id].wmcvsdrv_cme_struct.cme_mcvs_qset_size_status & (0x3)) : ((wmcvsdrv_int_cntrl_params[wsub_id].wmcvsdrv_cme_struct.cme_mcvs_qset_size_status>>2) & (0x3)))
/* Accumulate all QSET Itr per carrier  */
#define WMCVS_GET_TOTAL_QSET_SIZE()    WMCVS_GET_QSET_SIZE(0) + WMCVS_GET_QSET_SIZE(1)
#define WMCVS_GET_QSET_ITER(carr_idx)  ((wmcvsdrv_int_cntrl_params[wsub_id].wmcvsdrv_cme_struct.cme_mcvs_qset_iteration_status[carr_idx]      & (0x3))+\
                                        ((wmcvsdrv_int_cntrl_params[wsub_id].wmcvsdrv_cme_struct.cme_mcvs_qset_iteration_status[carr_idx]>>2) & (0x3))+\
                                        ((wmcvsdrv_int_cntrl_params[wsub_id].wmcvsdrv_cme_struct.cme_mcvs_qset_iteration_status[carr_idx]>>4) & (0x3)))

#define WMCVS_GET_TOTAL_QSET_ITER()    WMCVS_GET_QSET_ITER(0) + WMCVS_GET_QSET_ITER(1)

/* below TD macro only used for TH's compute VPE function. accessing only rake's TD parameter here.
   For accessing QICE clk table, only CME's TD status will be accessed.*/
#define WMCVS_GET_TXD_STATUS(carr_idx) ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.status_txd[carr_idx]))

/* Look up table indices */
#define WMCVS_GET_FING_CNT_IDX(carr_idx)  WMCVS_GET_FING_CNT(carr_idx)-1
#define WMCVS_GET_QSET_SIZE_IDX(carr_idx) WMCVS_GET_QSET_SIZE(carr_idx)-1
#define WMCVS_GET_QSET_ITER_IDX(carr_idx) WMCVS_GET_QSET_ITER(carr_idx)-1

#define WMCVS_GET_TOTAL_ASET_IDX()      WMCVS_GET_TOTAL_ASET_CNT() - 2
#define WMCVS_GET_TOTAL_FING_IDX()      WMCVS_GET_TOTAL_FING_CNT() - 1
#define WMCVS_GET_TOTAL_QSET_SIZE_IDX()   WMCVS_GET_TOTAL_QSET_SIZE() - 1
#define WMCVS_GET_TOTAL_QSET_ITER_IDX()   WMCVS_GET_TOTAL_QSET_ITER() - 1

/*
  Q6 will require bump up to 499.2 MHz when any of the following conditions are met in DC-HSDPA:
  Either of the carriers has ASET > 1 
  Either of the carriers has QSET > 1
*/ 
#define WMCVS_Q6_PARAM_STATUS_SVS_CARR()             (IS_DC_ACTIVE() && ((WMCVS_GET_ASET_CNT(0) > 1) || (WMCVS_GET_QSET_ITER(0) > 1) || (WMCVS_GET_ASET_CNT(1) > 1) || (WMCVS_GET_QSET_ITER(1) > 1)))

#define WMCVS_VPE_PARAM_STATUS_PULLIN()              ((wmcvsdrv_ext_cntrl_params[wsub_id].wmcpmdrv_mcvs_vpe_update_params.total_num_cells[0] > 4) )  //Is it needed for THOR ? To, Check with Abhinav ???

/* Call the HS_SVS per carrier seperately.
   SC Final Clk speed will be based on MAX[HS(c0),Rake(c0)]
   DC Final Clk speed will be based on MAX[MAX[HS(c0),Rake(c0)], MAX[HS(c1),Rake(c1)]]
 */
#endif
/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_COMPUTE_VPE_SPEED

DESCRIPTION
  This function checks MCVS BUMP UP/DOWN criteria for Pure MCVS VPE update calls

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef FEATURE_WCDMA_TABASCO_MODEM
void wmcvsdrv_compute_q6_change(wmcvsdrv_mcvs_req_type req_type, wsub_id_e_type wsub_id);
#else
void wmcpmdrv_mcvs_compute_vpe_speed(wmcvsdrv_mcvs_req_type req_type, wsub_id_e_type wsub_id);
#endif

/*===========================================================================
FUNCTION     wmcpmdrv_vpe_update_in_mcvs_param

DESCRIPTION
function to send MCVS request when n ot already running at desired VPE frequency

PARAMETERS
VPE frequencywanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call at max will take 100usec
===========================================================================*/
#ifdef FEATURE_WCDMA_TABASCO_MODEM
void wmcpmdrv_q6clk_update_in_mcvs_param(uint32 q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
#else
void wmcpmdrv_vpe_update_in_mcvs_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
#endif

/*===========================================================================
FUNCTION     wmcpmdrv_vpe_update_in_mcpm_param

DESCRIPTION
Update  MCVS params to send alongwith MCPM request
 
PARAMETERS
vpe_speed

RETURN VALUE
NONE

SIDE EFFECTS
  None.
===========================================================================*/
#ifdef FEATURE_WCDMA_TABASCO_MODEM
void wmcpmdrv_q6clk_update_in_mcpm_param(uint32 q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
#else
void wmcpmdrv_vpe_update_in_mcpm_param(wmcpmdrv_mcvs_vpe_speedType vpe_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
#endif

/*===========================================================================
                          COMMON LOCAL/EXTERN Function Declaration
============================================================================*/
/*===========================================================================
FUNCTION     wmcvsdrv_clear_mvcs_db()

DESCRIPTION
  memset WMCVS request_parms and all globals to zero.
  This will be called during W stack init and de-init
 
PARAMETERS
  None.
 
RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcvsdrv_clear_wmvcs_db( wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_SRCH_PERIODICITY

DESCRIPTION
function to return the new/updated srch period.
 
PARAMETERS
  None.

RETURN VALUE
updated srch period

SIDE EFFECTS
  None.
===========================================================================*/
uint16 wmcpmdrv_mcvs_update_srch_periodicity( wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCPMDRV_MCVS_UPDATE_SRCH_CLK_TO_SRCH

DESCRIPTION
function to return the offline clk freq in Mhz.
 
PARAMETERS
  None.

RETURN VALUE
offline clk freq in Mhz

SIDE EFFECTS
  None.
===========================================================================*/
uint8 wmcpmdrv_mcvs_update_srch_clk_to_srch( wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_triage_param_update

DESCRIPTION
  Wrapper function for retrieving TxD Status from WL1

PARAMETERS
  None.

RETURN VALUE
  TxD_OFF/TxD_ON/TxD_DC

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_triage_param_update(wmcpmdrv_mcvs_vpe_update_struct_type* vpe_params , wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_mcvs_get_Pullin_cell_cnt

DESCRIPTION
  Gets the cell cnt from demod during pull in procedure.
 
PARAMETERS
 global VPE struct Address

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
void wmcpmdrv_mcvs_get_pullin_cell_cnt(wmcpmdrv_mcvs_vpe_update_struct_type *vpe_params, wsub_id_e_type wsub_id);

#ifdef FEATURE_WCDMA_TABASCO_MODEM
#else
/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcvs_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
pure MCVS req

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcvs_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
/*===========================================================================
FUNCTION     wmcpmdrv_q6clk_update_in_mcpm_param

DESCRIPTION
function to send MCVS request when not already running at desired Q6 frequency
MCVS embedded in mcpm req.

PARAMETERS
Q6 frequency wanted

RETURN VALUE
None

SIDE EFFECTS
Blocking call for x usec
===========================================================================*/
void wmcpmdrv_q6clk_update_in_mcpm_param(wmcpmdrv_mcvs_q6clk_speedType q6_speed,mcvs_request_type req_type, wsub_id_e_type wsub_id);
#endif
/*===========================================================================
FUNCTION     WMCVSDRV_REQUEST_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to request for power resources.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wmcvsdrv_request_client_power_config(wmcvsdrv_client_type client, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     WMCVSDRV_RELINQUISH_CLIENT_POWER_CONFIG

DESCRIPTION
  Wrapper function for W MCPM clients to relinquish for power resources.

PARAMETERS
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.
===========================================================================*/
extern void wmcvsdrv_relinquish_client_power_config(wmcvsdrv_client_type client, wsub_id_e_type wsub_id);

/*===========================================================================
FUNCTION     wmcpmdrv_get_mcpm_offline_clk_speed

DESCRIPTION
Called from inside the clock change callback to find out the new speed.

PARAMETERS
none

RETURN VALUE
none

SIDE EFFECTS
none 
 
===========================================================================*/
#ifdef FEATURE_WCDMA_TABASCO_MODEM
void wmcpmdrv_get_mcpm_clk_speed( wsub_id_e_type wsub_id);
uint8 wmcvsdrv_modulo(uint8 x,uint8 y);
extern void wmcvsdrv_q6Aclk_update_in_mcvs_param(uint32 q6Aspeed_req,mcvs_request_type req_type,wsub_id_e_type wsub_id);
extern void wmcvsdrv_check_Q6A(wsub_id_e_type wsub_id);
void wmcvsdrv_clear_mcvs_cme_info(wsub_id_e_type wsub_id);
uint32 wmcvsdrv_compute_q6b_for_qice_params(uint8 *qset_size, uint8 *qset_iter, boolean *txd_status, boolean *rxd_status,
	                                    uint8 *srch_cycle_count,uint8 *local_num_cells, wsub_id_e_type wsub_id);

extern void wmcvsdrv_send_clk_cfg_qta_gap(wsub_id_e_type wsub_id);

#ifdef FEATURE_WCDMA_TABASCO_14NM_MODEM
#define MCPM_Q6B_BASE_VALUE_IN_MHZ (WMCVS_Q6BCLK_SVS/1000)
#else
#define MCPM_Q6B_BASE_VALUE_IN_MHZ (W_Q6_364M/1000)
#endif
#else
void wmcpmdrv_get_mcpm_offline_clk_speed( wsub_id_e_type wsub_id);
#endif

#ifdef FEATURE_WCDMA_THOR_MODEM
/*===========================================================================
FUNCTION     WMCVSDRV_RET_VPE

DESCRIPTION
  Wrapper function to return last requested VPE speed

PARAMETERS
  None.

RETURN VALUE
  Absolute VPE speed in KHz.

SIDE EFFECTS
  None.
===========================================================================*/
extern wmcpmdrv_mcvs_vpe_speedType wmcvsdrv_ret_vpe(wsub_id_e_type wsub_id);
/*===========================================================================
FUNCTION     WMCVSDRV_RET_Q6

DESCRIPTION
  Wrapper function to return last requested Q6 speed

PARAMETERS
  None.

RETURN VALUE
  Absolute Q6 speed in KHz.

SIDE EFFECTS
  None.
===========================================================================*/
extern wmcpmdrv_mcvs_q6clk_speedType wmcvsdrv_ret_q6(wsub_id_e_type wsub_id);
#endif

/*===========================================================================
FUNCTION     wmcvsdrv_get_qset_txd_status

DESCRIPTION
  This function reads the structure from the CME module that has details on the CME TxD status and QSET size across all carriers.

PARAMETERS
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
extern void wmcvsdrv_get_qset_txd_status( wsub_id_e_type wsub_id);

extern void wmcvsdrv_ext_cntrl_params_init( void );
extern void wmcvsdrv_int_cntrl_params_init( void );

extern uint8 wmcvsdrv_compute_vpe_for_qice_params(uint8 *qset_size, uint8 *qset_iter, uint8 *txd_status, uint8 *rxd_status, wsub_id_e_type wsub_id);

#endif /* defined (FEATURE_WCDMA_BOLT_MODEM) && !(defined(FEATURE_WCDMA_THOR_MODEM)) */ 

#endif /*FEATURE_WCDMA_MCVS*/

#endif /* ifndef WMCVSDRV_H */
