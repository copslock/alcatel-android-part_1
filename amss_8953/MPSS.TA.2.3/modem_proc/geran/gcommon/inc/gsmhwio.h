#ifndef __GSMHWIO_H__
#define __GSMHWIO_H__
/*===========================================================================

  Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


$Header: //components/rel/geran.mpss/7.2/gcommon/inc/gsmhwio.h#1 $ $DateTime: 2015/12/03 03:35:15 $ $Author: pwbldsvc $

when       who       what, where, why
--------   ---       --------------------------------------------------------
14/03/12   pg        Inital version

===========================================================================*/ 

#include "geran_variation.h"
#include "msm.h"
#endif
