/*============================================================================
  @file rr_cxm_state.c

  @brief This module implements a state machine that manages RR-CXM state indication feature for 
  multi-SIM and associated interface functions.

   MCS-CxM is kept informed of GERAN states like connected, idle, rach etc.

                Copyright (c) 2015 QUALCOMM Technologies, Inc.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //components/rel/geran.mpss/7.2/grr/src/rr_cxm_state.c#1 $ */
/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_GSM_CXM_L3_STATE
#error code not present
#endif /* FEATURE_GSM_CXM_L3_STATE */
