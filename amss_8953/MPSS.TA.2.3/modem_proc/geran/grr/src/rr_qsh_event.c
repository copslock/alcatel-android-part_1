/*!
 * \file rr_qsh_event.c
 *
 * This module contains functionality to interface to QSH via EVENTs.
 *
 *              Copyright (c) 2016 Qualcomm Technologies, Inc.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 */
/* $Header: //components/rel/geran.mpss/7.2/grr/src/rr_qsh_event.c#5 $ */
/* $DateTime: 2016/07/26 04:13:53 $$Author: pwbldsvc $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"

#if defined(FEATURE_QSH_EVENT_NOTIFY_TO_QSH) || defined(FEATURE_QSH_EVENT_NOTIFY_HANDLER)
#include "qsh.h"
#include "rr_qsh_event.h"
#include "geran_multi_sim.h"
#include "rr_defs.h"
#include "rr_nv.h"
#include "sys.h"
#include "qsh_util.h"
#include "rr_gprs_defs.h"
#include "rr_l1_send.h"
#include "rr_conn_establish.h"
#include "rr_general.h"
#include "rr_mac_send.h"
#include "rr_diag.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
  
typedef struct
{
  uint32 event_bitmap;
}rr_qsh_event_data_t;

/*----------------------------------------------------------------------------
 * Variable Definitions
 * -------------------------------------------------------------------------*/
rr_qsh_event_data_t rr_qsh_event_data[NUM_GERAN_DATA_SPACES];

boolean rr_event_id = FALSE;

static uint32 rr_qsh_failure_event_enum[] = {
                                               RR_QSH_EVENT_ASSIGNMENT_FAILURE, 
                                               RR_QSH_EVENT_HANDOVER_FAILURE,                        
                                               RR_QSH_EVENT_DTM_ASSIGNMENT_FAILURE,           
                                               RR_QSH_EVENT_G2W_HO_FAILURE,                         
                                               RR_QSH_EVENT_RESEL_FAILURE,                               
                                               RR_QSH_EVENT_CON_REL_L2_RESET,                                          
                                               RR_QSH_EVENT_RACH_FAILURE,                               
                                               RR_QSH_EVENT_MPLMN_TIMEOUT,                               
                                               RR_QSH_EVENT_G2X_RESEL_FAILED,                           
                                               RR_QSH_EVENT_GSM_SYS_INFO_TIMER_EXPIRY,          
                                               RR_QSH_EVENT_GPRS_SYS_INFO_TIMER_EXPIRY,    
                                               RR_QSH_EVENT_RLF,                                            
                                               RR_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE
                                             };

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/

/*===========================================================================
   FUNCTION     rr_qsh_execute_reset_handler
  
   DESCRIPTION
     Triggers a reset in a handler functionality in RR
  
   DEPENDENCIES
     None.
  
   PARAMETERS
   gas_id, handler_cfg
  
   RETURN VALUE
     None.
  
   SIDE EFFECTS
  
  ===========================================================================*/

void rr_qsh_execute_reset_handler( rr_qsh_dtf_handler_id_enum_T event_id, gas_id_t gas_id)
{

  MSG_GERAN_HIGH_1_G("QSH DEBUG: Entered rr_qsh_execute_reset_handler, event_id: %d ", event_id);

  switch ( event_id ) 
  {
    case RR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ:
    {
      rrdiag_set_simulate_procedure_mask ((rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ), 0, gas_id);
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ:
    {
      rrdiag_set_simulate_procedure_mask ((rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ), 0, gas_id);
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ, 0, gas_id);
      break;    
    }
   
    case RR_QSH_EVENT_NOTIFY_SI13_INTERRUPT:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_SI13_INTERRUPT, 0, gas_id);
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^ 
                                              GRR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT, 0, gas_id);
      break;    
    }
   
    case RR_QSH_EVENT_NOTIFY_LAST_SI_READ:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_LAST_SI_READ, 0, gas_id); 
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY, 0, gas_id); 
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY:
    {
      rrdiag_set_simulate_procedure_mask ( rrdiag_get_simulate_procedure_mask(gas_id) ^
                                             GRR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY, 0, gas_id); 
      break;    
    }

    case RR_QSH_EVENT_NOTIFY_INVALID:
    {
      rrdiag_test_reset(gas_id);
      break;
    }

    default:
    break;

  }
}

/*===========================================================================
   FUNCTION     rr_qsh_reset_scell_all_irat_neighbours
  
   DESCRIPTION
     Resets the L and W neighbours to zero.
  
   DEPENDENCIES
     None.
  
   PARAMETERS
    scell_info_ptr
  
   RETURN VALUE
     None.
  
   SIDE EFFECTS
  
  ===========================================================================*/

static void rr_qsh_reset_scell_all_irat_neighbours(gprs_scell_info_t *scell_info_ptr)
{
  scell_info_ptr->gsm.utran_neighbor_list.valid_data = FALSE;// W neighbour as 0
  scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.num_of_cells = 0;

  scell_info_ptr->lte_neighbour_list.count = 0; // L neighbour is 0
}

/*===========================================================================
   FUNCTION     rr_qsh_wcdma_cell_update_req
  
   DESCRIPTION
     Triggers a wcdma cell update request with L or W neighbours
  
   DEPENDENCIES
     None.
  
   PARAMETERS
     gas_id, num_neigh, rr_qsh_neigh_type;
  
   RETURN VALUE
     None.
  
   SIDE EFFECTS
  
  ===========================================================================*/

 void rr_qsh_wcdma_cell_update_req( uint8 num_neigh, rr_qsh_neigh_type neigh_type, gas_id_t gas_id)
{
  gprs_scell_info_t *scell_info_ptr;

  scell_info_ptr = rr_gprs_get_scell_info(gas_id);
  RR_NULL_CHECK_FATAL(scell_info_ptr);

  rr_qsh_reset_scell_all_irat_neighbours(scell_info_ptr);

  if(!rr_gsm_only(gas_id))
  {
    MSG_GERAN_LOW_0_G("QSH DEBUG: rr_qsh_wcdma_cell_update_req L/W enabled");

    if (neigh_type == RR_QSH_RAT_NEIGH_IS_WCDMA)
    {
      scell_info_ptr->gsm.utran_neighbor_list.valid_data = TRUE;  //W neighbour as 1
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.num_of_cells = 1;
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.cell_list[0].cell_code = 2448;
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.cell_list[0].diversity = FALSE;
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.cell_list[0].uarfcn.uarfcn = 9662;
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.cell_list[0].uarfcn.rat_type = RR_UTRAN_FDD;
 
      if (num_neigh == 0)
      {
        scell_info_ptr->gsm.utran_neighbor_list.valid_data = FALSE;// W neighbour as 0
        scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.num_of_cells = 0;
      }
    }
    else
    {
      scell_info_ptr->lte_neighbour_list.count = 1; // L neighbour is 1
      scell_info_ptr->lte_neighbour_list.entries[0].frequency.earfcn = 300;
      scell_info_ptr->lte_neighbour_list.entries[0].frequency.measurement_bandwidth = 3;
      scell_info_ptr->lte_neighbour_list.entries[0].priority = 7;
      scell_info_ptr->lte_neighbour_list.entries[0].thresh_high = 30;
      scell_info_ptr->lte_neighbour_list.entries[0].thresh_low = 10;
      scell_info_ptr->lte_neighbour_list.entries[0].qrxlevmin = 8;
      scell_info_ptr->lte_neighbour_list.entries[0].not_allowed_cells.bitmap[0] = (uint8) 363;
      scell_info_ptr->lte_neighbour_list.entries[0].measurement_control = TRUE;

      if (num_neigh == 0)
      {
        scell_info_ptr->lte_neighbour_list.count = 0; // L neighbour is 0
      }
    }

    rr_send_mph_wcdma_cell_update_list_req(scell_info_ptr, gas_id);
  }
  else
  {
    MSG_GERAN_ERROR_0_G("QSH DEBUG: GSM only enabled");
  }
}

/*===========================================================================
   FUNCTION     rr_qsh_execute_handler
  
   DESCRIPTION
     Triggers the appropriate event handler functionality in RR
  
   DEPENDENCIES
     None.
  
   PARAMETERS
   gas_id, handler_cfg
  
   RETURN VALUE
     None.
  
   SIDE EFFECTS
  
  ===========================================================================*/

static void rr_qsh_execute_handler(gas_id_t gas_id,  qsh_client_event_notify_params_s handler_cfg)
{
  uint8 num_neigh = 0;
  gprs_scell_info_t *scell_info_ptr;

  MSG_GERAN_HIGH_1_G("QSH DEBUG: Entered rr_qsh_execute_handler event_id: %d ", handler_cfg.id);

  switch(handler_cfg.id)  
  {
    case RR_QSH_EVENT_NOTIFY_L1_DS_ABORT_IND:  
    {  
      rr_send_mph_l1_ds_abort_ind(RR_DS_ABORT_TO_NULL, MPH_POWER_SCAN_CNF, FALSE, gas_id);
      break;
    }
    case  RR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }
    case  RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }
    case  RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }
  
    case RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_L:
    {
      num_neigh = 1;
      rr_qsh_wcdma_cell_update_req( num_neigh, RR_QSH_RAT_NEIGH_IS_LTE, gas_id);  

      rr_qsh_execute_reset_handler( RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_L, gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_W:
    {
      num_neigh = 1;
      rr_qsh_wcdma_cell_update_req( num_neigh, RR_QSH_RAT_NEIGH_IS_WCDMA, gas_id);  

      rr_qsh_execute_reset_handler( RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_W, gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_WCDMA_UPDATE_REQ_NO_IRAT:
    {
      scell_info_ptr = rr_gprs_get_scell_info(gas_id);
      RR_NULL_CHECK_FATAL(scell_info_ptr);

      scell_info_ptr->lte_neighbour_list.count = 0; // L neighbour is 0
      scell_info_ptr->gsm.utran_neighbor_list.valid_data = FALSE;// W neighbour as 0
      scell_info_ptr->gsm.utran_neighbor_list.wcdma_cell_list.num_of_cells = 0;

      rr_send_mph_wcdma_cell_update_list_req(scell_info_ptr, gas_id);  

      rr_qsh_execute_reset_handler( RR_QSH_EVENT_NOTIFY_WCDMA_UPDATE_REQ_NO_IRAT, gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
               GRR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY, rrdiag_get_simulate_parameter(gas_id), gas_id);    
      break;
    }

    case RR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
             GRR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_SI13_INTERRUPT:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_SI13_INTERRUPT, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_LAST_SI_READ:
    {
      rrdiag_set_simulate_procedure_mask( rrdiag_get_simulate_procedure_mask(gas_id)|
              GRR_QSH_EVENT_NOTIFY_LAST_SI_READ, rrdiag_get_simulate_parameter(gas_id), gas_id);
      break;
    }

    case RR_QSH_EVENT_NOTIFY_RESET:
    {
      rr_qsh_execute_reset_handler( RR_QSH_EVENT_NOTIFY_INVALID, gas_id);
      break;
    }  

    default:
      break;

  }
}

void rr_qsh_event_handler_config_perform_cb_action(qsh_client_cb_params_s *cb_params_ptr)
{
  gas_id_t gas_id;
  qsh_client_event_notify_params_s handler_cfg;
  qsh_client_cfg_s *config_ptr = &cb_params_ptr->action_params.cfg;
  
  gas_id = geran_map_nas_id_to_gas_id ( cb_params_ptr->action_params.event_notify.subs_id );
  handler_cfg = cb_params_ptr->action_params.event_notify;
   
  rr_qsh_execute_handler(gas_id, handler_cfg);

  // Indicate call-back action done
  {
    qsh_client_action_done_s action_done;

  /* Notify QSH that the required action is handled */
    qsh_client_action_done_init(&action_done);

    action_done.cb_params_ptr = cb_params_ptr;
    action_done.action_mode_done = QSH_ACTION_MODE_DONE_ASYNC;

    qsh_client_action_done(&action_done);
  }

  return;
}

/*!
 * \brief Performs the EVENT CONFIG call-back action
 *
 * \param cb_params_ptr (in)
 */
__attribute__((section(".uncompressible.text")))
void rr_qsh_event_config_perform_cb_action(qsh_client_cb_params_s *cb_params_ptr)
{
  // Short-cut to config params
  qsh_client_cfg_s *config_ptr = &cb_params_ptr->action_params.cfg;

  // Extract action, event, and gas_id from the config params
  qsh_event_action_e action = (uint8)(config_ptr->cmd_code & 0x000000FF);
  rr_qsh_event_id_t event = (uint8)((config_ptr->cmd_code >> 8) & 0x000000FF);
  gas_id_t gas_id = geran_map_nas_id_to_gas_id((sys_modem_as_id_e_type)((config_ptr->cmd_code >> 16) & 0x000000FF));

  uint32 rr_failure_events = (sizeof(rr_qsh_failure_event_enum)/sizeof(uint32));

  if ((action == QSH_EVENT_ACTION_ENABLE) && (RR_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE == event))
  {
    while (rr_failure_events--)
    {
      rr_qsh_event_data[gas_id].event_bitmap |=  (1 << rr_qsh_failure_event_enum[rr_failure_events]);
    }
    rr_event_id = TRUE;
  }  
  else if ((action == QSH_EVENT_ACTION_DISABLE) && (RR_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE == event))
  {
    if (rr_event_id == TRUE)
    {
      while (rr_failure_events--)
      {
        rr_qsh_event_data[gas_id].event_bitmap &=  ~(1 << rr_qsh_failure_event_enum[rr_failure_events]);	    
      }
    }
  }
  else if (action == QSH_EVENT_ACTION_ENABLE)
  {
    rr_qsh_event_data[gas_id].event_bitmap |=  (1 << event);
  }
  else if (action == QSH_EVENT_ACTION_DISABLE)
  {
    rr_qsh_event_data[gas_id].event_bitmap &= ~(1 << event);
  }

  // Indicate call-back action done
  {
    qsh_client_action_done_s action_done;

    qsh_client_action_done_init(&action_done);

    action_done.cb_params_ptr = cb_params_ptr;
    action_done.action_mode_done = QSH_ACTION_MODE_DONE_ASYNC;

    qsh_client_action_done(&action_done);
  }

  return;
}

/*!
 * \brief Notifies an event to QSH if the event notification was enabled by QSH
 *
 * \param event_id(in), gas_id (in)
 */
__attribute__((section(".uncompressible.text")))
void rr_qsh_event_notify(rr_qsh_event_id_t event_id, const gas_id_t gas_id)
{
  qsh_client_event_notify_params_s  event_notify_params;

  /* Check if QSH has enabled corresponding event id */
  if(rr_qsh_event_data[gas_id].event_bitmap & (1 << event_id))
  {
    qsh_client_event_notify_init(&event_notify_params);
    
    event_notify_params.client = QSH_CLT_GRR;

    if(rr_nv_qsh_debug_enabled(RR_GAS_ID_TO_AS_ID))
    {
      MSG_GERAN_HIGH_1_G("GRR QSH: Event Notified: %d", event_id);
    }	
    if (rr_event_id == TRUE)
    {
      event_id = RR_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE;
    }

    event_notify_params.id = event_id;
    event_notify_params.event_data = NULL;
    event_notify_params.subs_id = geran_map_gas_id_to_nas_id(gas_id);
    
    qsh_client_event_notify(&event_notify_params);
  }
}

/*!
 * \brief Maps OTA failure message to QSH event and notifies QSH the same
 *
 * \param msg_type(in), gas_id (in)
 */
__attribute__((section(".uncompressible.text")))
void rr_qsh_event_notify_from_ota_failure_msg(uint8 msg_type, const gas_id_t gas_id)
{
  switch(msg_type)
  {
    case ASSIGNMENT_FAILURE :
    {
       rr_qsh_event_notify(RR_QSH_EVENT_ASSIGNMENT_FAILURE, gas_id);
       break;
    }

    case HANDOVER_FAILURE  :
    {
       rr_qsh_event_notify(RR_QSH_EVENT_HANDOVER_FAILURE, gas_id);
       break;
    }

    case DTM_ASSIGNMENT_FAILURE :
    {
      rr_qsh_event_notify(RR_QSH_EVENT_DTM_ASSIGNMENT_FAILURE, gas_id);
      break;
    }
    
    default:
      break;
  }
}

/*!
 * \brief Initialisation function called from RR-QSH when event notification functionality is required.
 *
  * \param void
 */
__attribute__((section(".uncompressible.text")))
void rr_qsh_event_init(void)
{
  int as_index;
  
  for (as_index = 0; as_index < NUM_GERAN_DATA_SPACES; as_index++)
  {
    /* By default all the GRR events are disabled */
    rr_qsh_event_data[as_index].event_bitmap =  0 ;
  }
}

#endif // FEATURE_QSH_EVENT_NOTIFY_TO_QSH

/* EOF */
