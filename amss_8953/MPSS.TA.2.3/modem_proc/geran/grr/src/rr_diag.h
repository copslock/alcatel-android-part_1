/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            RR Diag Module

GENERAL DESCRIPTION:
   This file contains the prototype definitions and declaratiosn for RR Diag module.

*============================================================================*/

#ifndef RR_DIAG_H
#define RR_DIAG_H

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"

#if defined(FEATURE_QSH_EVENT_NOTIFY_TO_QSH) || defined(FEATURE_QSH_EVENT_NOTIFY_HANDLER)

#include "geran_dual_sim_g.h"
#include "geran_multi_sim.h"


/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *--------------------------------------------------------------------------*/

#define GRR_SIMULATE_NONE                                0x00
#define GRR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ 0x01
#define GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ  0x02
#define GRR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ  0x04
#define GRR_QSH_EVENT_NOTIFY_SI13_INTERRUPT              0x08
#define GRR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT              0x10
#define GRR_QSH_EVENT_NOTIFY_LAST_SI_READ                0x20
#define GRR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY          0x40
#define GRR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY          0x80

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 *--------------------------------------------------------------------------*/
/**
  @brief Sets the mask bits.

  @return void
*/
extern void rrdiag_set_simulate_procedure_mask(const uint8 simu_pro, const uint8 simu_para, gas_id_t gas_id);

/**
  @brief Returns the mask bits that were set earlier.

  @return mask bits set
*/
extern uint8 rrdiag_get_simulate_procedure_mask( gas_id_t gas_id);

/**
  @brief Returns the simulation parameter that was set earlier.

  @return parameter set early.
*/
extern uint8 rrdiag_get_simulate_parameter( gas_id_t gas_id);

/**
  @brief sets the value of simulation parameter.

  @return void
*/
extern void rrdiag_set_simulate_parameter(uint8 param, gas_id_t gas_id);

/**
  @brief resets the mask bits to default value.

  @return void
*/
extern void rrdiag_test_reset( gas_id_t gas_id );

#endif //defined(FEATURE_QSH_EVENT_NOTIFY_TO_QSH) || defined(FEATURE_QSH_EVENT_NOTIFY_HANDLER)

#endif
/*EOF*/

