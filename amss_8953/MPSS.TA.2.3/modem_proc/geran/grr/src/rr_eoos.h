/*==============================================================================

                             R R   EOOS

GENERAL DESCRIPTION

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2007-2015 Qualcomm Technologies, Inc.

$Header: //components/rel/geran.mpss/7.2/grr/src/rr_eoos.h#1 $

==============================================================================*/


/*==============================================================================

                      INCLUDE FILES FOR MODULE

==============================================================================*/

#include "customer.h"
#include "comdef.h"

#include "geran_variation.h"
