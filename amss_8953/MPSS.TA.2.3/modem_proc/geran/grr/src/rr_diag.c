/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            RR Diag Module

GENERAL DESCRIPTION:
   This file is used by QSH to register the qsh event handlers in a dispatch table, which are used to trigger the handlers at appropriate places.

*============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"

#if defined(FEATURE_QSH_EVENT_NOTIFY_TO_QSH) || defined(FEATURE_QSH_EVENT_NOTIFY_HANDLER)

#include "rr_diag.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *--------------------------------------------------------------------------*/

uint8 rrdiag_cached_rr_simu_procedure[NUM_GERAN_DATA_SPACES] = {INITIAL_VALUE(GRR_SIMULATE_NONE)};

uint8 rrdiag_cached_rr_simu_parameter[NUM_GERAN_DATA_SPACES]= {INITIAL_VALUE(0)};

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 *--------------------------------------------------------------------------*/

/*===========================================================================
Function: rrdiag_get_simulate_procedure_mask
Description:
       Return rr_DTF MASK
============================================================================*/
uint8 rrdiag_get_simulate_procedure_mask( gas_id_t gas_id)
{
  return ( rrdiag_cached_rr_simu_procedure[check_gas_id(gas_id)] );
}

/*===========================================================================
Function: rrdiag_get_simulate_parameter
Description:
       Return rr_DTF Parameter
============================================================================*/
uint8 rrdiag_get_simulate_parameter( gas_id_t gas_id)
{
  return ( rrdiag_cached_rr_simu_parameter[check_gas_id(gas_id)] );
}

/*===========================================================================
Function: rrdiag_set_simulate_parameter
Description:
       Set RR_DTF Parameter
============================================================================*/
void rrdiag_set_simulate_parameter(uint8 param, gas_id_t gas_id)
{
  rrdiag_cached_rr_simu_parameter[check_gas_id(gas_id)] = param;
}

/*===========================================================================
Function: rrdiag_set_simulate_procedure_mask
Description:
       Set RR_DTF MASK
============================================================================*/
void rrdiag_set_simulate_procedure_mask(const uint8 simu_pro, const uint8 simu_para, gas_id_t gas_id)
{
  gas_id = check_gas_id(gas_id);

  rrdiag_cached_rr_simu_procedure[gas_id] = simu_pro;
  rrdiag_cached_rr_simu_parameter[gas_id] = simu_para;

  MSG_GERAN_HIGH_2_G("rrdiag rr_simu_pro 0x0%x rr_simu_para %d",rrdiag_cached_rr_simu_procedure[gas_id],rrdiag_cached_rr_simu_parameter[gas_id]);
}

/*
  clears rrdiag test settings
*/

void rrdiag_test_reset( const gas_id_t gas_id )
{
  rrdiag_set_simulate_procedure_mask( GRR_SIMULATE_NONE, 0, gas_id);
}

#endif
/*EOF*/

