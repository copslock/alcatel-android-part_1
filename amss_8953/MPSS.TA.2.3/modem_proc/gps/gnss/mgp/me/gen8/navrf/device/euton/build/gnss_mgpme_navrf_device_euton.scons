# -------------------------------------------------------------------------------- #
#       G N S S _ M G P M E _ N A V R F _ D E V I C E _ E U T O N . S C O N S
#
# DESCRIPTION
#   SCons file for the GNSS MGP ME Nav RF subsystem.
#
#
# INITIALIZATION AND SEQUENCING REQUIREMENTS
#   None.
#
#
#  Copyright (c) 2013 - 2014, 2016 Qualcomm Atheros, Inc.
#  Qualcomm Atheros Confidential and Proprietary. All Rights Reserved. 
#  Copyright (c) 2015 Qualcomm Technologies, Inc.
#  Qualcomm Technologies Confidential and Proprietary. All Rights Reserved. 
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
# --------------------------------------------------------------------------------- #

# --------------------------------------------------------------------------------- #
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to this file.
# Notice that changes are listed in reverse chronological order.
#
# $Header: //components/rel/gnss8.mpss/7.5.4/gnss/mgp/me/gen8/navrf/device/euton/build/gnss_mgpme_navrf_device_euton.scons#1 $
# $DateTime: 2016/07/05 04:36:26 $
# 
# when          who     what, where, why
# ----------    ---     ------------------------------------------------------------
# 2016/03/03    rb      Added support for 8953 and 8976
# 2013/09/24    rs      Initial Version
#
# --------------------------------------------------------------------------------- #

#--------------------------------------------------------------------------------
# Import and clone the SCons environment
#--------------------------------------------------------------------------------
Import('env')
from glob import glob
from os.path import join, basename

#--------------------------------------------------------------------------------
# Check USES flags and return if library isn't needed
#--------------------------------------------------------------------------------
if 'USES_CGPS' not in env:
   Return()

if 'USES_GNSS_SA' in env:
   Return()
   
#--------------------------------------------------------------------------------
# Only build this for certain targets
#--------------------------------------------------------------------------------   
if env['CHIPSET'] not in ['mdm9x35',
                          'mdm9x45',
                          'mdm9x55',  # Gen9 Target - Gen8C support is for RUMI
                          'msm8994',
                          'msm8996',
						  'msm8952',
						  'msm8953',
						  'msm8976',
						  'msm8940',
                         ]:
   env.CleanPack('CLEANPACK_TARGET', env.FindFiles('*', '..'))
   Return()

#--------------------------------------------------------------------------------
# Setup Debug preferences 
#--------------------------------------------------------------------------------

if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG     = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG     = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG     = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG     = "-g")

#--------------------------------------------------------------------------------
# Setup source PATH
#--------------------------------------------------------------------------------
SRCPATH = '../src'
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#--------------------------------------------------------------------------------
# Name of the subsystem to which this unit belongs
#--------------------------------------------------------------------------------
LIB_TARGET = '${BUILDPATH}/' + 'gnss_mgpme_navrf_device_euton'

# Set MSG_BT_SSID_DFLT for legacy MSG macros by removing previous version and adding new
# Definition
#--------------------------------------------------------------------------------
env.Replace(CPPDEFINES = [x for x in env['CPPDEFINES'] if not x.startswith("MSG_BT_SSID_DFLT=")] +
                                         ["MSG_BT_SSID_DFLT=MSG_SSID_MGPME"]) 

#--------------------------------------------------------------------------------
# Images that this VU is added
#--------------------------------------------------------------------------------
IMAGES = ['MODEM_MODEM']

#--------------------------------------------------------------------------------
# Generate the library and add to an image
#--------------------------------------------------------------------------------
BINARY_LIB_SOURCES = ['${BUILDPATH}/' + basename(fname)
                      for fname in glob(join(env.subst(SRCPATH), '*.c'))]

#--------------------------------------------------------------------------------
# Ship our code as binary library and add it to the Modem image
#--------------------------------------------------------------------------------
env.AddBinaryLibrary(IMAGES, (LIB_TARGET + '_pvt'), BINARY_LIB_SOURCES)

