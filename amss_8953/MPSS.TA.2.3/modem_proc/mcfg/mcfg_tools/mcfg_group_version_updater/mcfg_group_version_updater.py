#******************************************************************************
#* $Header: //components/rel/mcfg.mpss/7.0/mcfg_tools/mcfg_group_version_updater/mcfg_group_version_updater.py#5 $
#* $DateTime: 2016/02/11 12:16:01 $
#*
#* 
#******************************************************************************
#*
#* Copyright (c) 2015 Qualcomm Technologies, Inc.
#* All rights reserved.
#* Qualcomm Technologies, Inc. Confidential and Proprietary.
#*
#******************************************************************************
# Fixed location for this script: mcfg_gen/scripts/mcfg_group_version_updater.py

# Terminology
# ===========
# Carrier/main config are used interchangeably, as opposed to group configs

# MBN regeneration process for tech teams
# ==========================================

# * In excel workbook "Summary" tab
#   - Select the checkboxes for all groups that have been updated
#   - Click on "Update all referencing configs"

# This will run a VBA macro that calls mcfg_group_version_updater.py
# Macro will pass in as an argument a list of paths to the selected xml group files that have been updated

# Script will find all references to the provided group xml paths in carrier xml, and update the appropriate carrier excel/xml/mbn

# Update/regenerate process for a config
# * Increment MCFG_VERSION by 1 
# * Add Revision notes row  -should this be done? Perhaps group excel should have its own manually updated revision notes
# * Regenerate XML and MBN

# Requirements
# * No spreadsheets may have the same name
# * Excel workbook file naming conventions should be maintained
# * Carrier XML should have file name format of mcfg_sw_gen_[Non_VoLTE].xml
#   - Bracketed portion should be spreadsheet name in excel workbook

# Style - group xml filenames should be decriptive
# e.g. ims_att_VoLTE.xml not ims.xml

# Questions
# Should QC Version be updated too?

import sys
import os
import re
import subprocess
import argparse
import xml.etree.ElementTree as ET
from datetime import datetime
from collections import OrderedDict
import numbers

import openpyxl  # 3rd party open source library under MIT license

# for debugging only
from pprint import pprint, pformat

# Explicitly comparing wb file names to these pre-defined conventional names
# Allows the code to prevent accidentally updating 'MCFG_SW_Items_List_Macro.xlsm - Copy'
# instead of 'MCFG_SW_Items_List_Macro.xlsm',
CONVENTIONAL_WORKBOOK_NAMES = {
    'MCFG_SW_Items_List_Macro.xlsm': 'mcfg_sw',
    'MCFG_HW_Items_List_Macro.xlsm': 'mcfg_hw',
}

# Use this regex to match MCFG workbooks. Would like naming for groups to be as flexible as possible
MCFG_WKBK = re.compile(r'(^(MCFG_([SH]W))[\w\+]+(GROUP|MACRO).xlsm)', flags=re.IGNORECASE)
MCFG_GROUP_PATH = re.compile(r'.*mcfg.*groups')
MCFG_GROUP_WKBK = re.compile(r'(MCFG_[SH]W\w*GROUP_MACRO.xlsm)', flags=re.IGNORECASE)

# Group version column in carrier workbook
GROUP_VERSION_COLUMN_INDEX = 1
NV_ITEM_TYPE_COLUMN_INDEX = 2
EFS_FILE_SOURCE_PATH_COLUMN_INDEX = 7
# TRL Record Column Indices
DATA_FIELD_COLUMN_INDEX = 0
DATA_VALUE_COLUMN_INDEX = 5
# Column of revision number in revision notes sheet
REVISION_NUMBER_COLUMN_INDEX = 1
CR_COLUMN_INDEX = 4

# Constants for conventional meta data sheet names in excel workbook, just in case they are ever renamed
SUMMARY_WORKSHEET_NAME = 'Summary'
REVISION_NOTES_WORKSHEET_NAME = 'Revision History'
META_WORKSHEET_NAMES = [SUMMARY_WORKSHEET_NAME, REVISION_NOTES_WORKSHEET_NAME]

def find_node_abs_path(abs_path, node):
    head = abs_path
    tail = 'default'

    while (head and tail) and (tail != node):
        (head,tail) = os.path.split(head)

    if not head or not tail:
        print "Path '%s' does not contain node '%s'", abs_path, node
        return None

    # Reattach node to the path returned
    return os.path.join(head, tail)

def get_root_dir():
    '''
    Returns the build_root directory. Assumes build_root is parent directory of modem_proc
    '''
    cwd = os.path.dirname(os.path.realpath(__file__))
    modem_proc_dir = find_node_abs_path(cwd, 'modem_proc')
    if modem_proc_dir == None:
        print "Parent directory 'modem_proc' not found. Please include all MCFG related folders under parent directory named 'modem_proc'."
        sys.exit()
    else:
        build_root = os.path.dirname(modem_proc_dir)

    return build_root

WORKSPACE_ROOT = get_root_dir()
MCFG_ROOT = os.path.join(WORKSPACE_ROOT, 'modem_proc', 'mcfg')
MCFG_BUILD_DIR = os.path.join(MCFG_ROOT, 'build')

GROUP_NODE_ROOTS = ['groups', 'mcfg_group']

def parse_arguments():
    'This function parses command line arguments'
    # python mcfg_group_version_updater.py --configs=mcfg_sw:IMS_ATT_VoLTE --workbook_dir=C:\workspaces\mcfg.mpss.5.0.ln_grouping_project_JO\modem_proc\mcfg\mcfg_gen\groups\ims\
    # convert to modem_proc\mcfg\mcfg_gen\groups\ims\mcfg_sw_gen_IMS_ATT_VoLTE.xml
    parser = argparse.ArgumentParser(description='Find all main configs that reference a group and update it')
    parser.add_argument('--configs', required=True,
                       help='Comma separated list of configs to update in following format: mcfg_sw:IMS_ATT_VoLTE')
    parser.add_argument('--workbook_dir', required=True,
                       help='Path to group workbook dir')
    parser.add_argument('--list_only', action='store_true', dest='list_only',
                        help='List all carriers that reference the group, but don\'t update them')
    parser.add_argument('--log_cr', action='store_true', dest='log_change_request',
                        help='Log the change request used to include an update in a spreadsheet. Valid for windows only.')
    parser.add_argument('--force_log_cr', action='store_true', dest='force_log_cr', default=False,
                        help='Force a log entry to be created even if previous entry is same as new entry')
    parser.add_argument('--force_ref_gen', action='store_true', dest='force_ref_gen', default=False,
                        help='Force generation of reference configs even if versions have not changed')
    parser.add_argument('--cr', dest='change_request',
                        help='Change request id. Used to manually set CR string in revision notes if log_change_request flag is set.')
    args = parser.parse_args()

    parsed_args = {
        'group_identifiers': [],
        'list_only': args.list_only,
        'log_change_request': args.log_change_request,
        'change_request': args.change_request,
        'force_log_cr': args.force_log_cr,
        'force_ref_gen': args.force_ref_gen,
    }

    configs = args.configs.split(',')
    rel_path_to_group_xml_dir = get_relpath_from_dir(args.workbook_dir, "modem_proc")
    for config in configs:
        config_type, config_name = config.split(':')
        group_xml_filename = '%s_gen_%s.xml' % (config_type, config_name)
        rel_path_to_group_xml = os.path.join(rel_path_to_group_xml_dir, group_xml_filename)
        parsed_args['group_identifiers'].append(rel_path_to_group_xml)


    # Nice feature to have - truncate absolute paths into relative ones from $BUILD_ROOT
    # C:\....\modem_proc\...  --> modem_proc\....

    return parsed_args

def get_group_version_from_group_xml (file_path):
    '''
    Get version of group from actual group xml file
    Input: Path to xml file
    Output: Actual group version (not from carrier configs)
    '''
    NV_CONFIG_DATA = 'NvConfigurationData'

    tree = ET.parse(file_path)
    root = tree.getroot()
    group_paths = []
    for child in root:
        if child.tag == NV_CONFIG_DATA:
            version = child.attrib.get('version')
            return version


def get_group_references_in_xml (file_path):
    '''
    Get list of groups referenced in an xml
    Input: Path to xml file
    Output: List of groups referenced in the xml
    '''
    GROUP_FILE = 'GroupFile'

    tree = None
    try:
        tree = ET.parse(file_path)
    except:
        print "Could not parse XML file: \n\t", file_path
        sys.exit(1)

    root = tree.getroot()
    group_paths = []
    for child in root:
        if child.tag == GROUP_FILE:
            buildPath = child.attrib.get('buildPath')
            # normalizing slashes to unix style for easy comparison
            buildPath = os.path.normcase(buildPath)  
            group_paths.append(buildPath)
    return group_paths



def get_group_references_in_pl(group_identifer):
    '''
    Search through all carrier xml and find all references to a group xml file path
    Input:
      * Path to root dir for main xml
      * Relative path to group xml to search for as listed in main xml
    Output: A list of paths to carrier xml files that reference the given group
    '''

    group_references = []
    skip_dirs = [ 'build', 'scripts', 'source']
    skip_file = re.compile('mcfg_.*devcfg.xml')

    xml_file_name_pattern = re.compile('mcfg_[sh]w_gen_[\w\+\-]+.xml')

    #group_root = findmatch(GROUP_NODE_ROOTS, group_identifier)

    # Root nodes for directories containing NV group & carrier configuration XMLs
    abs_path_group_id = os.path.join(WORKSPACE_ROOT, group_identifier)
    group_root = find_node_abs_path(abs_path_group_id, 'groups')
    mcfg_gen = os.path.join(MCFG_ROOT, "mcfg_gen")

    mcfg_xml_roots = [ group_root, mcfg_gen ]
    for mcfg_xml_root in mcfg_xml_roots:
        for root, dirs, files in os.walk(mcfg_xml_root):
            #Prune directories that should not be searched
            dirs[:] = [dir for dir in dirs if dir not in skip_dirs ]

            for file_name in files:
                if xml_file_name_pattern.match(file_name) and not skip_file.match(file_name):
                    xml_path = os.path.join(root, file_name)
                    #print xml_path
                    groups_in_xml = get_group_references_in_xml(xml_path)  # list of groups in xml
                    for group in groups_in_xml:
                        if group.lower() == group_identifier.lower():
                            if xml_path not in group_references: # In case groups is under mcfg_gen, prevent multiple appends of same file
                                group_references.append(xml_path)
    #only XMLs that reference specific group identifier are returned
    return group_references
            

def structure_pending_updates(group_references):
    '''
    Input: Array of paths to carrier xml files that reference a specific group
    Output: Hash that maps from paths of excel workbooks that needs to be updated
         to list of config names that will be updated in that workbook
    '''

    struct = OrderedDict()

    for group_path in group_references:
        # carrier_folder_path = path to mcfg\mcfg_gen\generic\NA\ATT
        carrier_folder_path, xml_file_name = os.path.split(group_path)

        # workbook_found = False
        # only one workbook per config type (mcfg_sw vs mcfg_hw) is present in each folder
        for file_name in os.listdir(carrier_folder_path):
            if re.match(MCFG_WKBK, file_name):
                carrier_workbook_path = os.path.join(carrier_folder_path, file_name)

        # This line retrives everything after 'mcfg_sw_gen_' in xml filename as '_'separated list
        # 'mcfg_sw_gen_Non_VoLTE.xml' -> ['mcfg', 'sw', 'gen', 'Non', 'VoLTE.xml'] -> ['Non', 'VoLTE.xml']
        spreadsheet_name = xml_file_name.split('_') [3:]
        # ['Non', 'VoLTE.xml'] -> 'Non_VoLTE.xml'
        spreadsheet_name = '_'.join(spreadsheet_name)
        # 'Non_VoLTE.xml' -> ['Non_VoLTE', 'xml'] -> 'Non_VoLTE'
        spreadsheet_name = spreadsheet_name.split('.')[0]

        # Add spreadsheet_name to appropriate carrier bin, adding carrier if necessary
        if carrier_workbook_path:
            if carrier_workbook_path not in struct:
                # first time carrier is encountered
                struct[carrier_workbook_path] = [spreadsheet_name,]
            else:
                # all other times
                struct[carrier_workbook_path].append(spreadsheet_name)
    return struct


def get_relpath_from_dir (complete_path, ancestor_dir):
    # C:\workspaces\mcfg.mpss.5.0.ln_grouping_project_JO\modem_proc\mcfg\mcfg_gen\generic\NA\ATT
    #  Only trim down to generic\NA\ATT\
    dirs = complete_path.split(os.sep)
    # retain parent folder to mcfg by comparing second list element. first element will typically be 'modem_proc'
    while (len(dirs) > 1) and (dirs[0] != ancestor_dir):
        dirs.pop(0)
    if len(dirs) <= 1:
        print "Relative path to %s from %s not found", ancestor_dir, complete_path
        return None
    else:
        return os.path.join(*dirs)


def get_partial_path (complete_path, dir_levels):
    # C:\workspaces\mcfg.mpss.5.0.ln_grouping_project_JO\modem_proc\mcfg\mcfg_gen\generic\NA\ATT
    #  Only trim down to generic\NA\ATT\
    folder_stack = []
    for level in range(dir_levels):
        folder_name = os.path.split(complete_path)[1]
        complete_path = os.path.split(complete_path)[0]
        folder_stack.append(folder_name)
    reversed_path = []
    for level in range(dir_levels):
        folder_name = folder_stack.pop()
        reversed_path.append(folder_name)

    return os.path.join(*reversed_path)


# Delete multi-line string to enable advanced versioning mechanism
#"""

def load_sheets(wb, sheet_names=[]):
    '''
    This function returns a map of sheet names to sheet object
    If a list of sheet_names is provided, those will be loaded. Otherwise, all sheets are loaded.
    '''
    sheets = {}
    sheet_names = sheet_names if sheet_names else wb.get_sheet_names()
    for sheet_name in sheet_names: # -> ['Summary', 'Non_VoLTE', 'VoLTE', 'Revision History']
        sheet = wb.get_sheet_by_name(sheet_name)
        sheets[sheet_name] = sheet
    return sheets

def update_sheet_version(sheets, carrier_sheet_name):
    '''
    Input: Hash map of all sheets, name of carrier sheet to be updated
    Result: Mcfg version found and updated, regardless of it being located in carrier sheet or summary
    Note: This function modifies the workbook, but does not save it. This must be done after func call
    '''
    VER_WIDTH = 8 # Width of the version string (minus 0x prefix)
    updated_mcfg_version = ''
    for row in sheets[carrier_sheet_name].rows:
        column_A = str(row[DATA_FIELD_COLUMN_INDEX].value)
        if column_A.lower() == 'mcfg_version': 
            mcfg_version_cell = row[DATA_VALUE_COLUMN_INDEX]
            if mcfg_version_cell.value.startswith('='):  # if reference to another cell in summary
                referenced_cell_info = mcfg_version_cell.value[1:]  # '=Summary!C13'  -> 'Summary!C13'
                reference_sheet_name, cell_coord = referenced_cell_info.split('!')  # 'Summary!C13'  -> ['Summary', 'C13']
                mcfg_version_cell = sheets[SUMMARY_WORKSHEET_NAME][cell_coord]

            updated_mcfg_version = str('0x%0*X' % (VER_WIDTH, (int(mcfg_version_cell.value, 16) + 1))) # to base 10, +1, then back to base 16
            updated_mcfg_version = updated_mcfg_version[0:2].lower() + updated_mcfg_version[2:].upper()  # 0x lowercase, but rest of alpha chars uppercase (cosmetic)
            print "Updating mcfg version from %s to %s" % (mcfg_version_cell.value, updated_mcfg_version)
            mcfg_version_cell.value = updated_mcfg_version
        elif column_A.lower() == 'qc_version':
            #assumes MCFG_Version has been updated before arriving to row with QC_version
            qc_version_cell = row[DATA_VALUE_COLUMN_INDEX]
            qc_version_cell.value = updated_mcfg_version
    return mcfg_version_cell.value

def update_group_version(sheets, carrier_sheet_name, group_identifier):
    '''
    Input: Hash map of all sheets, name of carrier sheet to be updated, rel path to group xml
    Result: Group version found and updated

    Supported group version types:
    * An arbitrary amount of words separated by periods with a number at the end (ims.att_volte.5)
    * Just a number (6)

    Note: This function modifies the workbook, but does not save it. This must be done after func call
    '''
    actual_group_xml_version = get_group_version_from_group_xml(os.path.join(WORKSPACE_ROOT, group_identifier))

    for row in sheets[carrier_sheet_name].rows:
        #print row[EFS_FILE_SOURCE_PATH_COLUMN_INDEX]
        column_C = str(row[NV_ITEM_TYPE_COLUMN_INDEX].value)  # nv item type will be 'group' for group rows


        if row[EFS_FILE_SOURCE_PATH_COLUMN_INDEX].value:
           # print row[EFS_FILE_SOURCE_PATH_COLUMN_INDEX].value.encode('ascii','ignore')
            column_H = row[EFS_FILE_SOURCE_PATH_COLUMN_INDEX].value.encode('ascii','ignore')
        else:
            column_H = None

        if column_C.lower() == 'group' and column_H == group_identifier:
            group_version_cell = row[GROUP_VERSION_COLUMN_INDEX]
            original_group_version = group_version_cell.value


            if actual_group_xml_version.lower() == original_group_version.lower():
                print "Group version in '%s' already matches group_version of %s in '%s'" % (carrier_sheet_name, actual_group_xml_version, group_identifier)
                print 'No carrier update needed'
            else:
                group_version_cell.value = actual_group_xml_version
                print "Updating '%s' expected group version from %s to %s" % (carrier_sheet_name, original_group_version, group_version_cell.value)
                return group_version_cell.value


def update_group_wkbk_summary(summary, group_identifier):
    '''
    Input: Hash map of all sheets, name of carrier sheet to be updated, rel path to group xml
    Result: Group version found and updated

    Supported group version types:
    * An arbitrary amount of words separated by periods with a number at the end (ims.att_volte.5)
    * Just a number (6)

    Note: This function modifies the workbook, but does not save it. This must be done after func call
    '''

    SUMMARY_KEYWORD_COLUMN = 0
    summary_title_found = False
    summary_title_row = None
    group_ver_column = None
    group_ver_row = None

    # Find the row containing list of group names in workbook...
    for i, row in enumerate(summary.rows):
        if summary_title_found:
            break
        else:
            if str(row[SUMMARY_KEYWORD_COLUMN].value) == "Summary":
                summary_title_found = True
                summary_title_row = i
                group_names_row = summary_title_row + 1
                group_ver_row = group_names_row + 2

    # Find the group column heading equal to group identifier...
    for j, group_name in enumerate(summary.rows[group_names_row]):
        if str(group_name.value).lower() == group_identifier.lower():
            group_ver_column = j
            break

    try:
        config_ver_cell = summary.rows[group_ver_row][group_ver_column]
    except:
        print "No group found in Summary listing for %s" % group_identifier

    curr_config_ver = config_ver_cell.value
    ver_components = curr_config_ver.split('.')
    #Increment the numeric portion of the version. Assumed to be last element of '.' separated version
    ver_components[-1]  = int(ver_components[-1]) + 1
    ver_components[-1]  = str(ver_components[-1])
    new_config_ver = ".".join(ver_components)
    config_ver_cell.value = new_config_ver
    return config_ver_cell.value


def add_revision_notes_entry(revision_notes_sheet, config_name, mcfg_version, group_id, group_ver, CR):
    previous_row_was_entry = False
    revision_numbers = []
    last_row_index = len(list(revision_notes_sheet.rows)) - 1

    # Navigate to last row of the reivision notes
    for i, row in enumerate(revision_notes_sheet.rows):
        revision_number_cell = row[REVISION_NUMBER_COLUMN_INDEX]
        revision_number = revision_number_cell.value
        if isinstance(revision_number, numbers.Number) and (i != last_row_index):
            previous_row_was_entry = True
            revision_numbers.append(revision_number)
        elif previous_row_was_entry:
            if (i == last_row_index):
                #If last row avaiable was an entry, then append a new blank row to sheet
                revision_notes_sheet.append([])
            break
        else:
            pass#print 'ELSE ', repr(revision_number), type(revision_number)

    if group_ver == None:
        update_note = '%s same version as before' % (group_id)
    else:
        update_note = '%s updated to ver %s' % (group_id, group_ver)

    entry = [
        revision_numbers[-1] + 1,
        datetime.now().strftime('%m/%d/%Y'),
        'Group update in %s' % config_name,
        CR, # CR #
        update_note,
        '%s: %s' % (config_name, mcfg_version)
    ]

    for i, item in enumerate(entry):
        row[i + 1].value = item

def update_ref_xmls(configs_to_update):
    #stub function -- will update gruop versions in XMLs directly
    return

def update_workbook(excel_workbook_path, configs_to_update, group_identifier, cr):
    wb = openpyxl.load_workbook(excel_workbook_path, keep_vba=True)
    sheets = load_sheets(wb, configs_to_update + META_WORKSHEET_NAMES) # Also loads meta sheets by default (summary+revision)
    summary_sheet = sheets[SUMMARY_WORKSHEET_NAME]
    for config_name in configs_to_update:
        print '\n',
        print config_name
        print '=' * len(config_name)
        #import pdb; pdb.set_trace()
        group_version = update_group_version(sheets, config_name, group_identifier)
        #TODO: take this out when done testing
        #if not group_version:
        #    group_version = "test1"
        if group_version or args['force_log_cr']:
            if MCFG_GROUP_WKBK.search(excel_workbook_path):
                mcfg_version = update_group_wkbk_summary(summary_sheet, config_name)
                #TODO: immediately regenerate the XML after updating workbook
            else:
                mcfg_version = update_sheet_version(sheets, config_name)
            add_revision_notes_entry(sheets[REVISION_NOTES_WORKSHEET_NAME], config_name, mcfg_version, group_identifier, group_version, cr)

    try:
        wb.save(excel_workbook_path)
    except IOError, tb:
        print "Failed to save changes to workbook"
        print tb
        print "If you have the main/carrier workbook open that needs to be updated, please close it and run again"
        sys.exit(1)

    return group_version

def call_build_mcfgs(excel_workbook_file_path, configs_to_update, build_options):

    excel_workbook_file_name = os.path.basename(excel_workbook_file_path)
    match = MCFG_WKBK.match(excel_workbook_file_name)
    excel_workbook_type = match.group(2).lower()
    config_type = match.group(3).lower()
    if not excel_workbook_type:
        sys.exit("Incorrect workbook naming convention, unable to rebuild main mbn")

    if "win" in sys.platform:
        command = os.path.join(MCFG_BUILD_DIR, 'build_mcfgs.exe ')
    else:
        command = 'perl ' + os.path.join(MCFG_BUILD_DIR, 'build_mcfgs.pl ')

    command += '--configs='
    for i, config_name in enumerate(configs_to_update):
        if i > 0:
            command += ','
        command += '%s:%s' % (excel_workbook_type, config_name)
    command += build_options
    command += '--' + config_type + '_cfgs_workbook=' + excel_workbook_file_path + ' '
    command += '--source-dir='

    excel_workbook_file_dir = os.path.split(excel_workbook_file_path)[0]

    partial_carrier_dir_path = get_relpath_from_dir(excel_workbook_file_dir, "mcfg_gen")

    partial_carrier_dirs = partial_carrier_dir_path.split(os.sep)
    partial_carrier_dirs.pop(0)
    command += os.path.join(*partial_carrier_dirs)

    print "\nRunning '%s'" % command
    exit_code = subprocess.call(command, shell=True)
    print "Exit code :%d" % exit_code
#"""

def find_wkbk(group_id):
    abs_path_group_id = os.path.join(WORKSPACE_ROOT, group_identifier)
    abs_path_group_dir = os.path.dirname(abs_path_group_id)
    excel_wkbk = None

    for file in os.listdir(abs_path_group_dir):
        if MCFG_WKBK.search(file):
            excel_wkbk = os.path.join(abs_path_group_dir, file)

    return excel_wkbk


def get_last_CR_revision(excel_wkbk, load_option="rw"):

    if load_option == "read_only":
        try:
            wb = openpyxl.load_workbook(excel_wkbk, keep_vba=True, read_only=True)
        except:
            print "could not open configuration workbook: '%s'", excel_wkbk
    else:
        try:
            wb = openpyxl.load_workbook(excel_wkbk, keep_vba=True)
        except:
            print "could not open configuration workbook: '%s'", excel_wkbk


    #Scroll to last entry in revision notes and collect CR data
    previous_row_was_entry = False
    revision_numbers = []
    CR_data = "TBD"
    rev_sheet = wb.get_sheet_by_name(REVISION_NOTES_WORKSHEET_NAME) # Also loads meta sheets by default (summary+revision)
    last_row_index = len(list(rev_sheet.rows)) - 1

    for i, row in enumerate(rev_sheet.rows):
        revision_number_cell = row[REVISION_NUMBER_COLUMN_INDEX]
        revision_number = revision_number_cell.value
        if isinstance(revision_number, numbers.Number) and (i != last_row_index):
            #print repr(revision_number), type(revision_number)
            previous_row_was_entry = True
            previous_row = row
        elif previous_row_was_entry:
            if (i == last_row_index):
                CR_data = row[CR_COLUMN_INDEX].value
            else:
                CR_data = previous_row[CR_COLUMN_INDEX].value
            break
        else:
            pass#print 'ELSE ', repr(revision_number), type(revision_number)

    return CR_data

def regeneration_needed(version_update, forced_regeneration, forced_CR_update):
    if version_update:
        print "Regenerating due to version update"
    if forced_regeneration:
        print "Regenerating due force generation option turned on"
    if forced_CR_update:
        print "Regenerating due forced CR update option turned on"
    if (ver_update != None) or forced_regeneration or forced_CR_update:
        return True
    else:
        print "No updates present -- regeneration skipped"
        return False

if __name__ == '__main__':
    args = parse_arguments()
    group_identifiers = args['group_identifiers']

    # group_identifiers = groups getting referenced by other XMLs
    for group_identifier in group_identifiers:
        # Returns array of paths to xml files that reference the group
        group_references = get_group_references_in_pl(group_identifier)

        # Add parent groups to list of group ids that are referenced. This ensures
        # configurations using a parent group to reference a child subgroup are updated as well
        # Note: no checks are currently done to prevent circular references in groups
        for reference in group_references:
            if "groups" in reference:
                group_identifiers.append(get_relpath_from_dir(reference, "modem_proc"))

        # Generates hash that maps from paths to excel workbook that needs to be updated
        # to list of config names that will be updated in that workbook
        # Ex:
        # {
        #   '..NA\ATT\MCFG_SW_Items_List_Macro.xlsm': ['VoLTE', 'Non_VoLTE']
        # }
        #TODO: change function to list carrier name with XML references instead of workbooks
        pending_updates = structure_pending_updates(group_references)

        if args['list_only']:
            print "If you update+regen %s, please also regen the following configs referencing this group" % group_identifier
            print "-" * 72
            #TODO: change function to use list of XML references instead of workbooks
            for workbook_path in pending_updates:
                configs_to_update = pending_updates[workbook_path]
                print '* ' + get_partial_path(workbook_path, 3)
                for config in configs_to_update:
                    print '\t- %s' % config
        else:
            change_request = args['change_request']
            # If option to record change request has been selected, but no CR# provided
            if args['log_change_request'] and (args['change_request'] == None):
                if "win" in sys.platform:
                    excel_workbook_path = find_wkbk(group_identifier)

                    change_request = get_last_CR_revision(excel_workbook_path, "read_only")
            #TODO: change function to use list of XML references instead of workbooks
            for excel_workbook_path in pending_updates:
                ver_update = None
                configs_to_update = pending_updates[excel_workbook_path]
                update_ref_xmls(configs_to_update)

                if "win" in sys.platform:
                    prev_CR_log = get_last_CR_revision(excel_workbook_path)

                    # Only update workbook if CR# isn't already listed as last entry
                    if args['force_log_cr'] or (prev_CR_log != change_request):
                        #print excel_workbook_path
                        ver_update = update_workbook(excel_workbook_path, configs_to_update, group_identifier, change_request)
                    else:
                        print "Latest CR entry in rev notes of destination workbook %s is already %s" % (os.path.basename(excel_workbook_path), change_request)

                    if regeneration_needed(ver_update, args['force_ref_gen'], args['force_log_cr']):
                        # Regenerate just the source files for group XMLs -- remove once update_ref_xmls() is in place
                        if MCFG_GROUP_WKBK.search(excel_workbook_path):
                            call_build_mcfgs(excel_workbook_path, configs_to_update, ' --force-regenerate --sources-only ')
                        #TODO: depend on group XML paths instead of workbook path
                        if not MCFG_GROUP_WKBK.search(excel_workbook_path):
                            call_build_mcfgs(excel_workbook_path, configs_to_update, ' --force-regenerate --force-rebuild ')
                else:
                    if regeneration_needed(ver_update, args['force_ref_gen'], args['force_log_cr']):
                        #TODO: depend on group XML paths instead of workbook path
                        if not MCFG_GROUP_WKBK.search(excel_workbook_path):
                            call_build_mcfgs(excel_workbook_path, configs_to_update, ' --force-rebuild -xml ')
