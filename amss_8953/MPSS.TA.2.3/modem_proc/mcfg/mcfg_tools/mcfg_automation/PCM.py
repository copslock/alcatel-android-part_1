#!/usr/bin/env python2.6

# DESCRIPTION
#   Prism

# COPYRIGHT
#   All Rights Reserved.  QUALCOMM Proprietary

#   Export of this technology or software is regulated by the U.S. Government.
#   Diversion contrary to U.S. law prohibited.

#   Copyright (c) 2014, QUALCOMM Technologies, Inc.

#   All data and information contained in or disclosed by this document is
#   confidential and propriety information of QUALCOMM Technologies, Inc. and all
#   rights therein are expressly reserved. By accepting this material the
#   recipient agrees that this material and the information contained
#   therein is held in confidence and in trust and will not be used
#   copied, reproduced in whole or in part, nor its contents revealed
#   in any manner to others without the express written permission
#   of QUALCOMM Technologies, Inc.
#
#   Author: Monte Misa

import getpass
from suds import client
from suds.transport import https

class PCM(object):
    """Prism base class"""

    # If your service account is whitelisted and impersonating, you must provide an ApplicationSource value.
    app_source = "" # Put your own application source if applicable

    client = None
    userid = None
    pw = None

    def __init__(self,passwd= None):
        url = "http://pcm/PcmService/PcmWebService.svc?singleWsdl"

        if PCM.client is None:
            PCM.userid = getpass.getuser()
            
            if not passwd:
                passwd = getpass.getpass()
            
            PCM.pw = passwd
            
            PCM.client = client.Client(url, transport=https.WindowsHttpAuthenticated(username=PCM.userid, password=passwd))

            ## Do this to get the namespace info, web method info, class info, etc.
            #print PCM.client


    def getPackageWarehouse(self, SIR):
        GetSoftwareImageComponentCompositionsRequest = PCM.client.factory.create('ns0:GetSoftwareImageComponentCompositionsRequest')
        # print GetSoftwareImageComponentCompositionsRequest

        CompositionType = PCM.client.factory.create('ns0:CompositionType')
        # print CompositionType

        GetSoftwareImageComponentCompositionsRequest["SoftwareImageName"] = SIR
        GetSoftwareImageComponentCompositionsRequest["IsLatest"] = True
        GetSoftwareImageComponentCompositionsRequest["CompositionType"] = "Current"

        GetSoftwareImageComponentCompositionsResponse = PCM.client.service.GetSoftwareImageComponentCompositions(GetSoftwareImageComponentCompositionsRequest)
        if not GetSoftwareImageComponentCompositionsResponse['ComponentCompositions']:
            return None
        for temp in GetSoftwareImageComponentCompositionsResponse['ComponentCompositions'][0][0]['ComponentCompositionAssociations'][0]:
            if "mcfg.mpss" in temp['Component']['Name']:
                return temp['Component']['Name']



