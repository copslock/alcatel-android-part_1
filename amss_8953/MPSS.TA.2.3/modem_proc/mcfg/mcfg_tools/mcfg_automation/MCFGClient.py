#******************************************************************************
#* $Header: //components/rel/mcfg.mpss/7.0/mcfg_tools/mcfg_automation/MCFGClient.py#2 $
#* $DateTime: 2016/02/11 12:16:01 $
#*
#* 
#******************************************************************************
#*
#* Copyright (c) 2015 Qualcomm Technologies, Inc.
#* All rights reserved.
#* Qualcomm Technologies, Inc. Confidential and Proprietary.
#*
#******************************************************************************
#===============================================================================
# IIICCBClient.py - Bhavesh Patel
#===============================================================================

'''
MCFGClient.py

This script does the back end processing to login to JIRA and get the JIRA response using REST API.
'''

import urllib2, base64, cookielib, json
import urllib
import sys
import re
import time
import pdb


JIRA_PRODUCTION_TOOLS_SERVER = """https://jira.qualcomm.com/jira"""
JIRA_REST_SEARCH = """/rest/api/2/search?jql=%s&startAt=%d&maxResults=%d&fields=%s"""
JIRA_ADD_COMMENT = """/rest/api/2/issue/%s/comment"""
JIRA_PUT_VALUES = """/rest/api/2/issue/%s"""
JIRA_CHANGE_STATUS = """/rest/api/2/issue/%s/transitions"""

JRESP_CODE_GET_SUCCESS = 200
JRESP_CODE_NO_CONTENT = 204
JRESP_CODE_BAD_REQUEST = 400
JRESP_CODE_NOT_FOUND = 404

def readAuthFile(auth_file):
    pw = None
    user= None
    try:
        with open(auth_file, 'r') as f:
            user, pw = f.readline().strip().split("::")
    except:
        print "Unable to read/process file: " + str(auth_file)

    return user, pw

def login(user='', pw=''):
    import getpass

    if len(user) is 0:
        user = getpass.getuser()
    print "Username: " + user

    if len(pw) is 0:
        pw = getpass.getpass()
    else:
        print "Password provided."

    return user, pw

class JIRA:
    def __init__ (self, user, pw):
        self.user = user
        self.local_ticket_cache = {}
        self.pw = pw
        cj = cookielib.CookieJar()
        self.urlopener = urllib2.build_opener(RedirectHandler, urllib2.HTTPCookieProcessor(cj))
        self.urlbasestr = base64.encodestring('%s:%s' % (user, pw)).replace('\n', '')

    def getJIRAIssues(self, query, jira_interval, jira_fields, jira_release, jira_report, server=JIRA_PRODUCTION_TOOLS_SERVER):
        jresp = None
        startAt = 0
        jira_key = '' # Ticket number
        rest_query = server + JIRA_REST_SEARCH
        jira_cnt = 0 # Total JIRAs processed
        techpub_cnt = 0 # Tech pub request count
        aris_image_rel_cnt = 0 # Aris/Image release count
        idl_review_cnt = 0 # IDL review count
        pw_promotion_cnt = 0 # Total PW promotions
        cpl_integ_cnt = 0 # Total CPL integrations
        while True:
            jresp = self.getResponse(rest_query % (urllib.quote_plus(query), startAt, jira_interval, jira_fields))
            if jresp is None: # Unable to process JIRA request
                return 0
            if 'issues' not in jresp.keys(): # Issues field not in JIRA response.
                return 0
            if len(jresp['issues']) == 0: # No matching issues found.
                return -1
            
            issuesCnt = len(jresp['issues']) # total issues in the response.
            cnt = 0 # access individual issues
            while issuesCnt != 0:
                if jresp['issues'][cnt].has_key('key'):
                    jira_key = jresp['issues'][cnt]['key']
                    # print "Processing Jira: " + jira_key
                    jira_release[jira_key] = jresp['issues'][cnt]['fields']
                    
                    jira_cnt += 1
                    cnt += 1
                    issuesCnt -= 1
            
            if len(jresp['issues']) < jira_interval:
                break
            else:
                startAt += len(jresp['issues'])
        
        jira_report.extend([idl_review_cnt, aris_image_rel_cnt, pw_promotion_cnt, cpl_integ_cnt, techpub_cnt])
        print "Total JIRA processed: " + str(jira_cnt) + "\n"
        return 1

    def getResponse(self, req):  
        jresp = None
        reqqq = urllib2.Request(JIRA_PRODUCTION_TOOLS_SERVER)
        reqqq.add_header("Authorization", "Basic %s" % self.urlbasestr)
        r = self.urlopener.open(reqqq)

        request = urllib2.Request(req)
        response = self.openRequest(request)
        
        if response is None:
            print "Nonetype response"
            return None

        if response.code is not JRESP_CODE_GET_SUCCESS:
            print "Failed to get response from server"
            print response.info()
            return None
        
        jresp = json.load(response)
        return jresp


    def postResponse(self, req,data):  
        jresp = None

        request = urllib2.Request(req)
        request.add_data(data)
        request.add_header("Accept", "application/json")
        request.add_header('X-Atlassian-token' , 'no-check')

        request.add_header("Content-type","application/json")

        reqqq = urllib2.Request(JIRA_PRODUCTION_TOOLS_SERVER)
        reqqq.add_header("Authorization", "Basic %s" % self.urlbasestr)
        r = self.urlopener.open(reqqq)


        response = self.openRequest(request)

        if response is None:
            print "Nonetype response"
            return None

        if response.code > 300:
            print "Failed to get response from server"
            print response.info()
            return None
        
        jresp = json.load(response)
        return jresp


    def putResponse(self, req, data):
    	jresp = None
    	request = urllib2.Request(req)
    	request.add_data(data)
    	request.add_header("Accept", "application/json")
        request.add_header('X-Atlassian-token' , 'no-check')

        request.add_header("Content-type","application/json")
        request.get_method = lambda: 'PUT'

        reqqq = urllib2.Request(JIRA_PRODUCTION_TOOLS_SERVER)
        reqqq.add_header("Authorization", "Basic %s" % self.urlbasestr)
        r = self.urlopener.open(reqqq)

        response = self.openRequest(request)

        if response is None:
            print "Nonetype response"
            return None

        if response.code > 300:
            print "Failed to get response from server"
            print response.info()
            return None

        jresp = response
        return jresp


    def openRequest(self, request):
        response = None
        num_retries = 0
        max_retries = 5

        while num_retries < max_retries:
            try:
            	strn = "Basic %s" % self.urlbasestr
                request.add_header("Authorization", "Basic %s" % self.urlbasestr)
                response = self.urlopener.open(request)
                break
            except urllib2.HTTPError as e:
                print "Error %s" % e.read()
                num_retries += 1
                print "Retry... %d" % num_retries

        return response

    def addComment(self,key,comment):
    	rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_ADD_COMMENT
    	values = {}
    	values['body'] = comment
    	data = json.dumps(values)
    	jresp = self.postResponse(rest_query %(key),data)
    	return (jresp)

    def addLabel(self,key,label):
    	rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_PUT_VALUES
    	values = {"update": {"labels" : [{"add":label}]}}
    	data = json.dumps(values)
    	jresp = self.putResponse(rest_query %(key), data)

    	return (jresp)

    def moveToResolved(self,key):
        pdb.set_trace()
        rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_PUT_VALUES
        jresp = self.getResponse(rest_query %(key))
        status = jresp['fields']['status']['id']

        rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_CHANGE_STATUS

        if status == '10088' or status == '10251': # Withdrawn or Awaiting User Response
            #Reopen - open - review - approved - resolved
            values =  {"transition" : { "id" : "4"} } #Reopen
            data = json.dumps(values)
            jresp = self.postResponse(rest_query%(key),data)
            print jresp
            status = '4'

        if status == '4': #Reopen
            values =  {"transition":{"id" : "1"} } #Open
            data = json.dumps(values)
            jresp = self.postResponse(rest_query%(key),data)
            print jresp
            status = '1'        
        
        if status == '1': #Open
            values =  {"transition":{"id" : "10135"} } #In Review
            data = json.dumps(values)
            jresp = self.postResponse(rest_query%(key),data)
            print jresp
            status = '10135'
        
        # Approved - Resolved
        if status == '10002' or status == '10135': #In Review or Monitor/OnHold
            values = {"transition":{"id" : "10082"} } #Approved
            data = json.dumps(values)
            jresp = self.postResponse(rest_query%(key),data)
            print jresp
            status = '10082'        

        if status == '10082':
            values = {"transition":{"id" : "5"} } #Resolved
            data = json.dumps(values)
            jresp = self.postResponse(rest_query%(key),data)
            print jresp
        
        return (jresp)

    def changeAssignee(self,key,assignee):
    	rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_PUT_VALUES +"/assignee"
    	values = {"name" : assignee}
    	data = json.dumps(values)
    	jresp = self.putResponse(rest_query %(key), data)

    	return (jresp)

    def getIssue(self,key):
        rest_query = JIRA_PRODUCTION_TOOLS_SERVER + "/rest/api/2/issue/" + key
        jresp = self.getResponse(rest_query)
        return (jresp)

    def getAttachments(self, key):
        issue = self.getIssue(key)
        return issue['fields']['attachment']

    def openAttachment(self, uri):
        reqqq = urllib2.Request(JIRA_PRODUCTION_TOOLS_SERVER)
        reqqq.add_header("Authorization", "Basic %s" % self.urlbasestr)
        r = self.urlopener.open(reqqq)

        req = uri
        return self.urlopener.open(req)

    def updateDescription(self,key,description):
        rest_query = JIRA_PRODUCTION_TOOLS_SERVER + JIRA_PUT_VALUES
        values = {"update": {"description" : [{"set":description}]}}
        data = json.dumps(values)
        jresp = self.putResponse(rest_query %(key), data)
        return (jresp)








class RedirectHandler(urllib2.HTTPRedirectHandler):

	def redirect_request(self, req, fp, code, msg, headers, newurl):
		return urllib2.HTTPRedirectHandler.redirect_request(self,req,fp,code,msg,headers,newurl)
	def http_error_302(self,req,fp,code,msg,headers):
		return urllib2.HTTPRedirectHandler.http_error_302(self,req,fp,code,msg,headers)
	http_error_301 = http_error_303 = http_error_307 = http_error_302
