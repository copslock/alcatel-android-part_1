#ifndef __VS_TASK_H__
#define __VS_TASK_H__

/*
  ============================================================================

   Copyright (C) 2014-2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

  $Header: //components/rel/avs.mpss/6.2.1/vsd/inc/protected/vs_task.h#2 $
  $DateTime: 2015/11/04 02:19:45 $
  $Author: pwbldsvc $
*/

#define TASK_PRIORITY( task_pri_order ) ( 255 - task_pri_order )

/* VS High Priority Task */
#define VS_HIGH_TASK_NAME ( "VS_HIGH" )
#define VS_HIGH_TASK_STACK_SIZE ( 2 * 1024 )

/* VS Medium Priority Task */
#define VS_MED_TASK_NAME ( "VS_MED" )
#define VS_MED_TASK_STACK_SIZE ( 2 * 1024 )

/* VS Low Priority Task */
#define VS_LOW_TASK_NAME ( "VS_LOW" )
#define VS_LOW_TASK_STACK_SIZE ( 4 * 1024 )

/* MVS Task */
#define MVS_TASK_NAME ( "MVS" )
#define MVS_TASK_STACK_SIZE ( 4 * 1024 )

/* MVA Task */
#define MVA_TASK_NAME ( "MVA" )
#define MVA_TASK_STACK_SIZE ( 2 * 1024 )

/* IVA Task */
#define IVA_TASK_NAME ( "IVA" )
#define IVA_TASK_STACK_SIZE ( 2 * 1024 )

/* GVA Task */
#define GVA_TASK_NAME ( "GVA" )
#define GVA_TASK_STACK_SIZE ( 2 * 1024 )

/* WVA Task */
#define WVA_TASK_NAME ( "WVA" )
#define WVA_TASK_STACK_SIZE ( 2 * 1024 )

/* TVA Task */
#define TVA_TASK_NAME ( "TVA" )
#define TVA_TASK_STACK_SIZE ( 2 * 1024 )

/* CVA Task */
#define CVA_TASK_NAME ( "CVA" )
#define CVA_TASK_STACK_SIZE ( 2 * 1024 )

/* VAGENT Task */
#define VAGENT_TASK_NAME ( "VAGENT" )
#define VAGENT_TASK_STACK_SIZE ( 2 * 1024 )

/* VOCSVC Task */
#define VOCSVC_TASK_NAME ( "VOCSVC" )
#define VOCSVC_TASK_STACK_SIZE ( 1024 )

#ifdef AVS_MPSS_JO

/* AVS thread priorities specific to JO images. */
#define VS_HIGH_TASK_PRIORITY ( 171 )
#define VS_MED_TASK_PRIORITY ( 170 )
#define VS_LOW_TASK_PRIORITY ( 167 )
#define MVS_TASK_PRIORITY ( 170 )  
#define IVA_TASK_PRIORITY ( 171 )
#define GVA_TASK_PRIORITY ( 171 )
#define MVA_TASK_PRIORITY ( 171 )
#define WVA_TASK_PRIORITY ( 171 )
#define TVA_TASK_PRIORITY ( 171 )
#define CVA_TASK_PRIORITY ( 171 )
#define VOCSVC_TASK_PRIORITY ( 89 )
#define VAGENT_TASK_PRIORITY ( 171 )

#else

/* AVS thread priorities specific to Thor/Tabasco and all future chipsets. */
#define VS_HIGH_TASK_PRIORITY ( 181 )
#define VS_MED_TASK_PRIORITY ( 173 )
#define VS_LOW_TASK_PRIORITY ( 136 )
#define MVS_TASK_PRIORITY ( 173 )
#define IVA_TASK_PRIORITY ( 181 )
#define GVA_TASK_PRIORITY ( 181 )
#define MVA_TASK_PRIORITY ( 171 )
#define WVA_TASK_PRIORITY ( 181 )
#define TVA_TASK_PRIORITY ( 181 )
#define CVA_TASK_PRIORITY ( 181 )
#define VOCSVC_TASK_PRIORITY ( 89 )
#define VAGENT_TASK_PRIORITY ( 181 )

#endif /* AVS_MPSS_JO */

#endif /* __VS_TASK_H__ */

