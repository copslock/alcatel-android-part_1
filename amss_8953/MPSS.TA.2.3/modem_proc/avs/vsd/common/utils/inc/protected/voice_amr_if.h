#ifndef __VOICE_AMR_IF_H__
#define __VOICE_AMR_IF_H__

/**
  @file  voice_amr_if.h
  @brief This is the public header file that clients of GVA should include.
         This file includes all other GVA public header files and contains
         single entry point into the GVA.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //components/rel/avs.mpss/6.2.1/vsd/common/utils/inc/protected/voice_amr_if.h#1 $
  $Author: pwbldsvc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------


  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/

/* SYSTEM UTILS APIs. */
#include "dsmutil.h"
#include "mmdefs.h"

/* VSD APIs*/
#include "amrsup.h"
#include "vs.h"

/****************************************************************************
  VOICE AMR DEFINES
****************************************************************************/


/****************************************************************************
  VOICE AMR STRUCTURE DEFINATION
****************************************************************************/

/**
 * DSM Watermark vased AMR queue.
 */
typedef struct voice_amr_dsm_queue_t voice_amr_dsm_queue_t;

struct voice_amr_dsm_queue_t {

  dsm_watermark_type wm_a;
  dsm_watermark_type wm_b;
  dsm_watermark_type wm_c;
  q_type queue_a;
  q_type queue_b;
  q_type queue_c;
};


/**
 * AMR Logical channels state type.
 */
typedef struct voice_amr_chan_state_t voice_amr_chan_state_t;

struct voice_amr_chan_state_t {

  boolean has_chan_a; 
    /**< channel A exists */
  uint8_t lca;
    /**< class A logical channel ID. */

  boolean has_chan_b;
    /**< channel B exists */
  uint8_t lcb;                    
    /**< class B logical channel ID. */

  boolean has_chan_c;  
    /**< channel C exists */
  uint8_t lcc;                  
    /**< class C logical channel ID. */

};

/**
 * Buffer allocation of AMR core speech data
 */
typedef struct voice_amr_speech_buffer_t voice_amr_speech_buffer_t;

struct voice_amr_speech_buffer_t {

  uint8 data_a[ AMRSUP_CLASS_A_BYTES ];
  uint8 data_b[ AMRSUP_CLASS_B_BYTES ];
  uint8 data_c[ AMRSUP_CLASS_C_BYTES ];
};

/**
 * Voice AMR frame info.
 */
typedef struct voice_amr_frame_info_t voice_amr_frame_info_t;

struct voice_amr_frame_info_t {

  amrsup_frame_type frame_type;
  amrsup_mode_type mode;
};

/****************************************************************************
 * VOICE AMR UTILITY ROUTINES                                               *
 ****************************************************************************/

/**
 * Initialize AMR core speech buffer.
 */
extern void voice_amr_init_core_speech (
  amrsup_core_speech_type     *amr_speech,
  voice_amr_speech_buffer_t  *speech_buf
);


/**
 * This function will determine number of bytes of AMR vocoder frame length
 * based on the frame type and frame rate.
 * 
 * This returns number of bytes of AMR frame.
 */
extern uint32_t voice_amr_frame_len (
  amrsup_frame_type frame_type,
  amrsup_mode_type amr_mode
);


/**
 * This function will determine number of bits of AMR vocoder frame length
 * based on the frame type and frame rate.
 * 
 * This returns number of bits of AMR frame.
 */
extern uint32_t voice_amr_frame_len_bits (
 amrsup_frame_type frame_type,
 amrsup_mode_type amr_mode
);


/**
 * This function processes uplink data and transports it over the DSM queue
 * initialized in the mvssup_wcdma_set_ul_channel or 
 * mvssup_tdscdma_set_ul_channel.
 */
extern uint32_t voice_amr_ul_processing (
   vs_voc_buffer_t* vs_buffer,
   voice_amr_chan_state_t* ul_chan_state,
   voice_amr_dsm_queue_t* amr_dsm_q
 );



/**
 * This function retrieves downlink data from the DSM queue initialized in the
 * mvssup_wcdma_set_dl_channel or mvssup_tdscdma_set_dl_channel and processes 
 * it for vocoder use.
 */
extern uint32_t voice_amr_dl_processing (
  vs_voc_buffer_t* vs_buffer,
  voice_amr_dsm_queue_t* amr_dsm_q
 );

#endif /* __VOICE_AMR_IF_H__ */

