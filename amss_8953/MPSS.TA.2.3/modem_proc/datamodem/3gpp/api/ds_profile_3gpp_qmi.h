/******************************************************************************
  @file    ds_profile_3gpp_qmi.h
  @brief   

  DESCRIPTION
  Tech specific 3GPP Profile Management through QMI, header file

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. 
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
****************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/3gpp/api/ds_profile_3gpp_qmi.h#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/04/11   gs      First version of the header file.
===========================================================================*/
#ifndef DS_PROFILE_3GPP_QMI_H
#define DS_PROFILE_3GPP_QMI_H

#include "comdef.h"


#endif /* DS_PROFILE_3GPP_QMI_H */
