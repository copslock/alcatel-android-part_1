/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

   3 G   D A T A   S E R V I C E S   T H R O U G H P U T   M A N A G E R

GENERAL DESCRIPTION
  This software unit contains functions for throughput estimation manager.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS


  Copyright (c) 2001 - 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath: L:/src/asw/MM_DATA/vcs/ds3gtimer.c_v   1.1   13 Aug 2002 16:24:52   sramacha  $
  $Header: //components/rel/data.mpss/3.4.3.1/3gpp/dsmgr/src/ds3gtputmgr.c#3 $ $DateTime: 2016/07/03 23:37:08 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/07/14   vm     Initial version

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

#include "rex.h"
#include "data_msg.h"
#include "ds3gmgr.h"
#include "ds3gtimer.h"
#include "ds3gtputmgr.h"
#include "ps_sys_conf.h"
#include "ps_sys_event.h"
#include "ds_3gpp_hdlr.h"


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/
/*---------------------------------------------------------------------------
  Default value to refresh actual throughput timer value (in msec)
  -------------------------------------------------------------------------*/
#define DS3GTPUTMGR_REFRESH_ACTUAL_TPUT_TIMER_DEF_VAL      250

/*---------------------------------------------------------------------------
  Macro to get timer id for subscription id
    timer_id per subs_id = (subs_id * 1000)+timer_id
---------------------------------------------------------------------------*/
#define DS3GTPUTMGR_TIMER_ID_FOR_SUBS_ID(subs_id, timer_id) \
          ((unsigned long)(subs_id*1000) + timer_id)

/*---------------------------------------------------------------------------
  Macro to get subscription id from timer id for 
    subs_id = (timer_id / 1000)
---------------------------------------------------------------------------*/
#define DS3GTPUTMGR_SUBS_ID_FROM_TIMER_ID(timer_id) (timer_id/1000)

/*---------------------------------------------------------------------------
  Macro to get timer enum from timer id for 
    subs_id = (timer_id / 1000)
---------------------------------------------------------------------------*/
#define DS3GTPUTMGR_TIMER_ENUM_FROM_TIMER_ID(timer_id,subs_id) \
    (timer_id - 1000*subs_id)

/*---------------------------------------------------------------------------
  Struct for DS_CMD_DS3GTPUTMGR_TIMER_EXPIRED
---------------------------------------------------------------------------*/
typedef struct
{
  ds3gsubsmgr_subs_id_e_type  subs_indx;
  unsigned long               timer_id;
} ds3gtputmgr_timer_expired_cmd_type;

/*---------------------------------------------------------------------------
  Global variable for TIMER critical sections.
---------------------------------------------------------------------------*/
static rex_crit_sect_type  ds3gtputmgr_crit_sect = {{0}};

static ds3gtputmgr_uldl_throughput_info_s  
         ds3gtputmgr_uldl_throughput_info[DS3GSUBSMGR_SUBS_ID_MAX];

static ds3gtputmgr_dl_est_reporting_status_info_s 
         ds3gtputmgr_dl_est_reporting_status_info[DS3GSUBSMGR_SUBS_ID_MAX];

/*---------------------------------------------------------------------------
  Variable to store time interval to send Throughput Info Indication
  periodically
---------------------------------------------------------------------------*/
static uint32 ds3gtputmgr_ind_interval[DS3GSUBSMGR_SUBS_ID_MAX] = {0};

#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*---------------------------------------------------------------------------

  Variable to store uplink throughput reporting
---------------------------------------------------------------------------*/
static ds3gtputmgr_uplink_throughput_info_s 
         ds3gtputmgr_uplink_throughput_info[DS3GSUBSMGR_SUBS_ID_MAX];
#endif
/*---------------------------------------------------------------------------
  Variable to store time interval to refresh actual throughput for all
  active calls periodically
---------------------------------------------------------------------------*/
static uint32 ds3gtputmgr_actual_tput_interval[DS3GSUBSMGR_SUBS_ID_MAX] = {0};

/*---------------------------------------------------------------------------
  Timer objects for Throughput Info Indication 
---------------------------------------------------------------------------*/
static rex_timer_type ds3gtputmgr_tput_info_timer[DS3GSUBSMGR_SUBS_ID_MAX];

/*---------------------------------------------------------------------------
  Timer object to refresh actual throughput
---------------------------------------------------------------------------*/
static rex_timer_type ds3gtputmgr_refresh_actual_tput_timer[DS3GSUBSMGR_SUBS_ID_MAX];

/*---------------------------------------------------------------------------
  Timer object to estimated downlink throughput timer
---------------------------------------------------------------------------*/
static rex_timer_type ds3gtputmgr_est_dl_tput_timer[DS3GSUBSMGR_SUBS_ID_MAX];

/*---------------------------------------------------------------------------
  Number of times the throughput info timer expired
  and waiting to be processed in DS cmd  queue
---------------------------------------------------------------------------*/
static uint16 ds3gtputmgr_tput_info_tmr_exp_cnt[DS3GSUBSMGR_SUBS_ID_MAX] = {0};

/*---------------------------------------------------------------------------
  Number of times the refresh actual throughput info timer expired
  and waiting to be processed in DS cmd  queue
---------------------------------------------------------------------------*/
static uint16 ds3gtputmgr_rfrsh_act_tput_tmr_exp_cnt[DS3GSUBSMGR_SUBS_ID_MAX]
                                                                       = {0};

/*---------------------------------------------------------------------------
  Number of times the refresh actual throughput info timer expired
  and waiting to be processed in DS cmd  queue
---------------------------------------------------------------------------*/
static uint16 ds3gtputmgr_uldl_throughput_exp_cnt[DS3GSUBSMGR_SUBS_ID_MAX] = {0};


/*===========================================================================
                    Forward declarations/prototypes
===========================================================================*/
/*===========================================================================

                      INTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_TIMER_EXP_CNT

DESCRIPTION   For a particulat timer ID, this function returns the number of 
              times the timer has expired before being processed
 
DEPENDENCIES  None

RETURN VALUE  uint16 - Timer expiry count

SIDE EFFECTS  None
===========================================================================*/
uint16 ds3gtputmgr_get_timer_exp_cnt
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  ds3g_timer_enum_type        timer_id
)
{
  uint16  ret_val = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if((timer_id != DS3G_TIMER_REPORT_TPUT_INFO_IND) &&
     (timer_id != DS3G_TIMER_REFRESH_ACTUAL_TPUT) &&
     (timer_id != DS3G_TIMER_ULDL_THROUGHPUT))
  {
    return ret_val;
  }

  rex_enter_crit_sect(&ds3gtputmgr_crit_sect);
  switch( timer_id )
  {
    case DS3G_TIMER_REPORT_TPUT_INFO_IND:
      ret_val = ds3gtputmgr_tput_info_tmr_exp_cnt[ds3g_subs_id];
      break;

    case DS3G_TIMER_REFRESH_ACTUAL_TPUT:
      ret_val = ds3gtputmgr_rfrsh_act_tput_tmr_exp_cnt[ds3g_subs_id];
      break;

     case DS3G_TIMER_ULDL_THROUGHPUT:
       ret_val = ds3gtputmgr_uldl_throughput_exp_cnt[ds3g_subs_id];
       break;


    default:
      break;
  }
  rex_leave_crit_sect(&ds3gtputmgr_crit_sect);

  return  ret_val;
} /* ds3gtputmgr_get_timer_exp_cnt() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_INC_TIMER_EXP_CNT

DESCRIPTION   For a particulat timer ID, this function increments the timer 
              expiry count by one
 
DEPENDENCIES  None

RETURN VALUE  TRUE - Timer Expiry count has been incremented successfully

SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_inc_timer_exp_cnt
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  ds3g_timer_enum_type        timer_id
)
{
  boolean  ret_val = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if((timer_id != DS3G_TIMER_REPORT_TPUT_INFO_IND) &&
     (timer_id != DS3G_TIMER_REFRESH_ACTUAL_TPUT) &&
     (timer_id != DS3G_TIMER_ULDL_THROUGHPUT))
  {
    return ret_val;
  }

  /*-------------------------------------------------------------------------
    Increment the timer expiry count
  -------------------------------------------------------------------------*/
  rex_enter_crit_sect(&ds3gtputmgr_crit_sect);
  switch( timer_id )
  {
    case DS3G_TIMER_REPORT_TPUT_INFO_IND:
      ds3gtputmgr_tput_info_tmr_exp_cnt[ds3g_subs_id]++;
      ret_val = TRUE;
      break;

    case DS3G_TIMER_REFRESH_ACTUAL_TPUT:
      ds3gtputmgr_rfrsh_act_tput_tmr_exp_cnt[ds3g_subs_id]++;
      ret_val = TRUE;
      break;

    case DS3G_TIMER_ULDL_THROUGHPUT:
      ds3gtputmgr_uldl_throughput_exp_cnt[ds3g_subs_id]++;
      ret_val = TRUE;
      break;
    default:
      ret_val = FALSE;
      break;
  }
  rex_leave_crit_sect(&ds3gtputmgr_crit_sect);

  return ret_val;
} /* ds3gtputmgr_inc_timer_exp_cnt() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_CLR_TIMER_EXP_CNT

DESCRIPTION   For a particulat timer ID, this function resets the timer 
              expiry count. The count will be cleared, when the command posted
              for timer expiry gets processed
 
DEPENDENCIES  None

RETURN VALUE  TRUE - Timer Expiry count has been cleared successfully

SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_clr_timer_exp_cnt
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  ds3g_timer_enum_type        timer_id
)
{
  boolean  ret_val = FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if((timer_id != DS3G_TIMER_REPORT_TPUT_INFO_IND) &&
     (timer_id != DS3G_TIMER_REFRESH_ACTUAL_TPUT) &&
     (timer_id != DS3G_TIMER_ULDL_THROUGHPUT))
  {
    return ret_val;
  }

  /*------------------------------------------------------------------------- 
    Increment the timer expiry count
    -------------------------------------------------------------------------*/
  rex_enter_crit_sect(&ds3gtputmgr_crit_sect);
  switch( timer_id )
  {
    case DS3G_TIMER_REPORT_TPUT_INFO_IND:
      ds3gtputmgr_tput_info_tmr_exp_cnt[ds3g_subs_id] = 0;
      ret_val = TRUE;
      break;

    case DS3G_TIMER_REFRESH_ACTUAL_TPUT:
      ds3gtputmgr_rfrsh_act_tput_tmr_exp_cnt[ds3g_subs_id] = 0;
      ret_val = TRUE;
      break;
    case DS3G_TIMER_ULDL_THROUGHPUT:
      ds3gtputmgr_uldl_throughput_exp_cnt[ds3g_subs_id] = 0;
      ret_val = TRUE;
      break;
    default:
      ret_val = FALSE;
      break;
  }
  rex_leave_crit_sect(&ds3gtputmgr_crit_sect);
  return  ret_val;
} /* ds3gtputmgr_clr_timer_exp_cnt() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_TIMER_CB

DESCRIPTION   This callback function isregistered with timer to
              notify of timer expirations. This function posts a command to
              DS task upon timer expiry with the timer ID for the expired timer

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_timer_cb
(
  unsigned long  timer_id_per_subs
)
{
  ds_cmd_type                         *cmd_ptr = NULL;
  ds3g_timer_enum_type                 timer_id;
  uint16                               timer_exp_cnt = 0;
  ds3gsubsmgr_subs_id_e_type           ds3g_subs_id;
  ds3gtputmgr_timer_expired_cmd_type  *timer_exp_cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ds3g_subs_id = DS3G_SUBS_ID_FROM_TIMER_ID(timer_id_per_subs);
  timer_id = (ds3g_timer_enum_type)DS3G_TIMER_ENUM_FROM_TIMER_ID(timer_id_per_subs,ds3g_subs_id);
  timer_exp_cnt = ds3gtputmgr_get_timer_exp_cnt(ds3g_subs_id, timer_id_per_subs);

  DATA_3GMGR_MSG2(MSG_LEGACY_MED, "Timer Id:%d expired. Exp cnt %d",
                  timer_id, timer_exp_cnt);

  /*------------------------------------------------------------------------- 
   Check the number of times the timer has expired previously since the last
   time the timer expiry processing was done. If the timer has expired more
   than once, then probably there is already a command waiting in the queue
   to be processed. In that case we do not have to post command to DS task,
   if the command is already in the queue and waiting to be processed
  -------------------------------------------------------------------------*/
  if( timer_exp_cnt > 0 )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_MED, "Timer expiry cmd for timer id:%d "
                    "already in cmd_q. Skip posting cmd again",
                    timer_id);
    ds3gtputmgr_inc_timer_exp_cnt(ds3g_subs_id, timer_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Command already not in command queue. Post the command to DS task
  -------------------------------------------------------------------------*/
  cmd_ptr = ds_allocate_cmd_buf(sizeof(ds3gtputmgr_timer_expired_cmd_type));
  if ( (cmd_ptr == NULL) || (cmd_ptr->cmd_payload_ptr == NULL) )
  {
    ASSERT(0);
    return;
  }

  /*-------------------------------------------------------------------------
    Increment the timer expiry count before posting
  -------------------------------------------------------------------------*/
  ds3gtputmgr_inc_timer_exp_cnt(ds3g_subs_id, timer_id);

  /*-------------------------------------------------------------------------
    Send a DS_TIMER_EXPIRED_CMD to the DS task, and indicate which timer
    expired.
  -------------------------------------------------------------------------*/
  cmd_ptr->hdr.cmd_id = DS_CMD_DS3GTPUTMGR_TIMER_EXPIRED;
  timer_exp_cmd_ptr
    = (ds3gtputmgr_timer_expired_cmd_type*)cmd_ptr->cmd_payload_ptr;
  timer_exp_cmd_ptr->subs_indx = ds3g_subs_id;
  timer_exp_cmd_ptr->timer_id  = timer_id;
  ds_put_cmd( cmd_ptr );
  return;
} /* ds3gtputmgr_timer_cb() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_TPUT_INFO_IND_INTERVAL

DESCRIPTION   This function returns the frequency (in msec) the throughput 
              Information Indication is generated periodically for the
              given subscription ID

DEPENDENCIES  None

RETURN VALUE  uint32 - Refresh Interval in (msec)
 
              0       - If the timer is not running (or) if the subscription
                        ID is invalid
 
SIDE EFFECTS  None
===========================================================================*/
uint32 ds3gtputmgr_get_tput_info_ind_interval
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  uint32  interval = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    interval = ds3gtputmgr_ind_interval[ds3g_subs_id];
  }
  return interval;
} /* ds3gtputmgr_get_tput_info_ind_interval() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_TPUT_INFO_IND_INTERVAL

DESCRIPTION   This function configures the timer interval to generate 
              throughput information Ind periodically. 

DEPENDENCIES  None

RETURN VALUE  TRUE  - If Throughput Info Ind interval has been set for 
                      the Subscription ID
              FALSE - Otherwise
 
SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_set_tput_info_ind_interval
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint32                      interval
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                  "Subs ID: %d Set Throughput Info Ind Timer to %d msec",
                  ds3g_subs_id, interval);
  ds3gtputmgr_ind_interval[ds3g_subs_id] = interval;
  return TRUE;
}/* ds3gtputmgr_set_tput_info_ind_interval() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_ACTUAL_TPUT_REFRESH_INTERVAL

DESCRIPTION   For the current subscription ID, this function sets
              sets the time interval to periodically refresh 
              the actual throughput numbers for all bearers/RLP's for all
              active calls

DEPENDENCIES  None

RETURN VALUE  TRUE  - If Actual throughput refresh interval has been set for 
                      the Subscription ID
              FALSE - Otherwise
 
 
SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_set_actual_tput_refresh_interval
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint32                      interval
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH, 
                  "Subs ID: %d Setting Time to refresh actual  throughput "
                  "every %d msec", ds3g_subs_id,interval);
  ds3gtputmgr_actual_tput_interval[ds3g_subs_id] = interval;
  return TRUE;
} /* ds3gtputmgr_set_actual_tput_refresh_interval() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_ESTIMATED_DL_TPUT_INTERVAL

DESCRIPTION   This function sets the 
              multiplier for t_acumulate that is used to calculate t_report

DEPENDENCIES  None

RETURN VALUE  TRUE  - If Throughput Info Ind interval has been set for 
                      the Subscription ID
              FALSE - Otherwise
 
SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_set_est_dl_tput_requested_interval
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  uint32 report_interval
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }

  if(report_interval == 
     ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].requested_interval)
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_ERROR,
                "old report_interval and new report_interval is same %d for subs %d",
                report_interval, ds3g_subs_id);
    return FALSE;
  }

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                  "Subs ID: %d Set Throughput Info Ind Timer to %d msec",
                  ds3g_subs_id, report_interval);

  ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].requested_interval = 
                        report_interval;
  return TRUE;
}/* ds3gtputmgr_set_estimated_dl_tput_interval */


/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_EST_DL_TPUT_ACTUAL_INTERVAL

DESCRIPTION   This function sets actual reporting inveral if that's different
              from what client requested
 
DEPENDENCIES  None

RETURN VALUE  None
 
SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_est_dl_tput_actual_interval
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  uint32 report_interval
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return;
  }

  if(report_interval != 
     ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].actual_interval)
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                    "set_actual_iterval: old %d new %d ",
                    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].actual_interval,
                    report_interval);


    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].actual_interval = 
                        report_interval;
    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].
        update_reporting_status = TRUE;
  }

}/* ds3gtputmgr_set_est_dl_tput_actual_interval */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_EST_DL_TPUT_ACTUAL_INTERVAL

DESCRIPTION   This function sets actual reporting inveral if that's different
              from what client requested
 
DEPENDENCIES  None

RETURN VALUE  None
 
SIDE EFFECTS  None
===========================================================================*/
uint32  ds3gtputmgr_get_est_dl_tput_actual_interval
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return 0;
  }

  return ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].                      actual_interval;   

}/* ds3gtputmgr_set_est_dl_tput_actual_interval */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_EST_DL_TPUT_REPORT_STATUS

DESCRIPTION   This function sets the reporting status for the subs_id
              

DEPENDENCIES  None

RETURN VALUE  None
 
SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_est_dl_tput_report_status
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  ds3gtputmgr_est_reporting_status_e_type report_status
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return ;
  }

  if(report_status != 
     ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].reporting_status)
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                    "set_reporting_status: old %d new %d ",
                    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].reporting_status,
                    report_status);

    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].reporting_status = 
                        report_status;
    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].
        update_reporting_status = TRUE;
  }

}/* ds3gtputmgr_set_est_dl_tput_report_status */


/*===========================================================================
FUNCTION      DS3GTPUTMGR_POST_REPORT_STATUS_IND

DESCRIPTION   This function posts the reporting status indication to PS 
              framework for subs_id

DEPENDENCIES  None

RETURN VALUE  None
 
SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_post_report_status_ind
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  ps_sys_thrpt_status_type tput_report_status;
  int16                             dss_errno = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return ;
  }

  if (ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].
        update_reporting_status == TRUE) 
  {
    tput_report_status.actual_interval = 
        ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].actual_interval;
    tput_report_status.thrpt_status = 
        (ps_sys_thrpt_status_reason_enum_type)ds3gtputmgr_dl_est_reporting_status_info
        [ds3g_subs_id].reporting_status;

    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                "lce report_status: %d for subs %d ",
                tput_report_status.thrpt_status, ds3g_subs_id);

    ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                         PS_SYS_CONF_DL_THROUGHPUT_INTERVAL_STATUS_CHANGE,
                         (ps_sys_subscription_enum_type)
                           ds3gsubsmgr_subs_id_ds3g_to_ds(ds3g_subs_id),
                         &tput_report_status, &dss_errno);
    ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].
        update_reporting_status = FALSE;
  }
  
}

/*==============================================================================
FUNCTION      DS3GTPUTMGR_UPDATE_ACTUAL_THPUT_REFRESH_INTERVAL

DESCRIPTION   * Given the current subscription ID, this utility function 
                estimates the duration to periodically refresh the actual
                throughput rate for all the active data calls for the current
                subscription based on the timer duration of the throughput Info
                Indication timer
              * Starts the periodic refresh actual throughput Timer

DEPENDENCIES  None 
 
RETURN VALUE  1   -   Timer updated and started 
              0   -   Timer stopped
              -1  -   Either timer value not updated (or) Actual throughput
                      Timer start attempt failed
 
SIDE EFFECTS  None
===========================================================================*/
int ds3gtputmgr_update_actual_thput_refresh_interval
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id
)
{
  int                   ret_val                       = -1;
  uint32                throughput_info_ind_duration  =  0;
  uint32                refresh_interval              =  0;
  ds3g_timer_enum_type  timer_id                      =  DS3G_TIMER_MIN;
  uint8                 expiry_count                  =  0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d", ds3g_subs_id);
    return ret_val;
  }

  /*------------------------------------------------------------------------- 
    Calculate the duration to refresh actual throughput based on the
    duration of reporting throughput Info Indication
    -------------------------------------------------------------------------*/
  throughput_info_ind_duration = 
                          ds3gtputmgr_get_tput_info_ind_interval(ds3g_subs_id);
  if (throughput_info_ind_duration == 0)
  {
    refresh_interval = 0;
  } 
  else if (throughput_info_ind_duration < DS3GTPUTMGR_REFRESH_ACTUAL_TPUT_TIMER_DEF_VAL)
  {
    refresh_interval = throughput_info_ind_duration;
  } 
  else
  {
    refresh_interval = DS3GTPUTMGR_REFRESH_ACTUAL_TPUT_TIMER_DEF_VAL;
  }
  /*-------------------------------------------------------------------------
    Update the new the actual throughput Timer duration
  -------------------------------------------------------------------------*/
  if (FALSE == ds3gtputmgr_set_actual_tput_refresh_interval(ds3g_subs_id,
                                                           refresh_interval))
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_MED, "DS 3G subs id %d Failed setting actual "
                                    "t'put refresh duration",ds3g_subs_id,
                                    refresh_interval);
    return ret_val;
  }

  /*-------------------------------------------------------------------------
    Acquire the timer ID to start the timer based on the subscription ID
  -------------------------------------------------------------------------*/
  timer_id = DS3G_TIMER_ID_FOR_SUBS_ID(ds3g_subs_id, 
                                              DS3G_TIMER_REFRESH_ACTUAL_TPUT);

  /*-------------------------------------------------------------------------
    Stop timer if the duration is 0 else start the timer to refresh
    actual throughput
  -------------------------------------------------------------------------*/
  if (refresh_interval == 0)
  {
    ds3g_timer_stop_ex(DS3G_TIMER_REFRESH_ACTUAL_TPUT,
                       ds3g_subs_id);

    DATA_3GMGR_MSG1(MSG_LEGACY_HIGH, "Stopping Timer %d",timer_id);

    ret_val = 0;
  }
  else
  {
    /*-----------------------------------------------------------------------
      Actual throughput timer duration changed. Run the timer with
      the new duration
    -----------------------------------------------------------------------*/
    ds3g_timer_start_ex(DS3G_TIMER_REFRESH_ACTUAL_TPUT, 
                     refresh_interval,
                     ds3g_subs_id);

    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH, "Started Periodic timer %d for %d seconds",
                    timer_id,refresh_interval);

    /*-----------------------------------------------------------------------
      Set the expiry count to 0. This will initialize the bearer throughput
      stats on the Mode Handlers
    -----------------------------------------------------------------------*/
    ds3g_refresh_actual_throughput(ds3g_subs_id, expiry_count);
    ret_val = 1;
  }

  return ret_val;
} /* ds3gtputmgr_update_actual_thput_refresh_interval() */

/*==============================================================================
FUNCTION      DS3GTPUTMGR_UPDATE_TPUT_INFO_IND_INTERVAL

DESCRIPTION   This is a callback function registered with PS per subscription 
              ID, that gets triggered, when an active client sets a timer
              value 'x' (msec) to periodically report throughput Info indication
              When
              'x' != 0   the throughput Info indication timer is started to
              periodically expire and generate Indication
              'x'  = 0   The throughput Info Indication timer is stopped, if
              already running ==> Reporting throughput Indication for the
              current subscription ID is stopped
 
DEPENDENCIES  None 
 
RETURN VALUE  None

===========================================================================*/
void ds3gtputmgr_update_tput_info_ind_interval
(
   ps_sys_tech_enum_type           tech_type,
   ps_sys_event_enum_type          event_name,
   ps_sys_subscription_enum_type   ps_subs_id,
   void                           *event_info_ptr,
   void                           *user_data_ptr
)
{
  uint32                      timer_val = 0;
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  ds3g_timer_enum_type        timer_id = DS3G_TIMER_MIN;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Validate Arguments
  -------------------------------------------------------------------------*/
  if(event_info_ptr == NULL)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "NULL ptr passed. Return");
    return;
  }

  if(event_name != PS_SYS_EVENT_UPDATED_THROUGHPUT_TIMER)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Invalid Event %d. Ignoring");
    return;
  }

  /*-------------------------------------------------------------------------
    Get DS 3G SUB ID from PS Subscription ID
  -------------------------------------------------------------------------*/
  ds3g_subs_id
    = ds3gsubsmgr_subs_id_ds_to_ds3g((ds_sys_subscription_enum_type)ps_subs_id);

  if( !ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id) )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid subs_id %d", ps_subs_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Get the timer value to be updated
  -------------------------------------------------------------------------*/
  timer_val = *(uint32 *)event_info_ptr;
  if(FALSE == ds3gtputmgr_set_tput_info_ind_interval(ds3g_subs_id,timer_val))
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_ERROR, 
                    "Can't set timer duration %d on ds3g subs id %d",
                    timer_val, ds3g_subs_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Update the timer to refresh actual throughput timer value, based on the
    new throughput Info Ind timer duration
  -------------------------------------------------------------------------*/
  if( ds3gtputmgr_update_actual_thput_refresh_interval(ds3g_subs_id) < 0 )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,
                    "Can't update actual tput refresh timer");
  }

  /*-------------------------------------------------------------------------
    Acquire the timer ID to start based on the subscription ID
  -------------------------------------------------------------------------*/
  timer_id = DS3G_TIMER_ID_FOR_SUBS_ID(ds3g_subs_id, 
                                              DS3G_TIMER_REPORT_TPUT_INFO_IND);

  /*-------------------------------------------------------------------------
    Decide whether to start (or) stop the timer.
    Start the timer  - (If) the new timer duration received is non zero
    Stop the timer  - Otherwise
  -------------------------------------------------------------------------*/
  if(timer_val == 0)
  {
    ds3g_timer_stop_ex(DS3G_TIMER_REPORT_TPUT_INFO_IND,
                    ds3g_subs_id);
  }
  else
  {
    /*-----------------------------------------------------------------------
      Start the timer to report Throughput Info indication
    -----------------------------------------------------------------------*/
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,
                    "Starting Timer %d to periodically expire every %d msec",
                    timer_id,timer_val);
    ds3g_timer_start_ex(DS3G_TIMER_REPORT_TPUT_INFO_IND, 
                     timer_val,
                     ds3g_subs_id);
  }
  return;
} /* ds3gtputmgr_update_tput_info_ind_interval() */

/*==============================================================================
FUNCTION      DS3GTPUTMGR_UPDATE_DL_TPUT_EST_IND_INFO

DESCRIPTION   * Given the current subscription ID, this function updates t_report
              and prepares MH to the reporting
 
SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_update_dl_tput_est_ind_info
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

   ds_3gpp_downlink_throughput_hdlr((sys_modem_as_id_e_type)ds3g_subs_id, TRUE);

} /* ds3gtputmgr_update_dl_tput_est_ind_info */

/*==============================================================================
FUNCTION      DS3GTPUTMGR_UPDATE_DL_TPUT_EST_INFO_IND_INTERVAL

DESCRIPTION   This is a callback function registered with PS per subscription 
              ID, for t_accumulate multiplier change from the client side
 
DEPENDENCIES  None 
 
RETURN VALUE  None

===========================================================================*/
void ds3gtputmgr_update_dl_tput_est_info_ind_interval
(
  ps_sys_tech_enum_type           tech_type,
  ps_sys_event_enum_type          event_name,
  ps_sys_subscription_enum_type   ps_subs_id,
  void                           *event_info_ptr,
  void                           *user_data_ptr
)
{
  uint32                      requested_interval = 0;
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-------------------------------------------------------------------------
    Validate Arguments
  -------------------------------------------------------------------------*/
  if (event_info_ptr == NULL)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, 
                    "NULL Argument. Cannot update"
                    "dl estimation throughput Info Ind Interval");
    return;
  }

  if (event_name != PS_SYS_EVENT_UPDATED_DL_THROUGHPUT_TIMER)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "Ignoring Event %d. Not updating "
                                      "dl estimationthroughput Info timer");
    return;
  }

  /*-------------------------------------------------------------------------
    Get DS 3G SUB ID from PS Subscription ID
  -------------------------------------------------------------------------*/
  ds3g_subs_id
    = ds3gsubsmgr_subs_id_ds_to_ds3g((ds_sys_subscription_enum_type)ps_subs_id);

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Get the timer value to be updated
  -------------------------------------------------------------------------*/
  requested_interval = *(uint32 *)event_info_ptr;

  if( ds3gtputmgr_set_est_dl_tput_requested_interval(ds3g_subs_id,
                                                 requested_interval) == FALSE)
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_ERROR, 
                    "Can't set timer duration %d on ds3g subs id %d",
                    requested_interval,ds3g_subs_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Update the timer to refresh actual throughput timer value, based on the
    new throughput Info Ind timer duration
  -------------------------------------------------------------------------*/
  ds3gtputmgr_update_dl_tput_est_ind_info(ds3g_subs_id);

  return;
} /* ds3gtputmgr_update_dl_tput_est_info_ind_interval() */

/*===========================================================================

                      EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================
FUNCTION      DS3GTPUTMGR_INIT

DESCRIPTION   This function initializes the ds3g throughput mgr services. 
              This function must be called once, at Data Services Task startup.

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_init( void )
{
  /*-------------------------------------------------------------------------
    Initialize critical section
  -------------------------------------------------------------------------*/
  rex_init_crit_sect(&ds3gtputmgr_crit_sect);
  return;
}/* ds3gtputmgr_init() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_PROCESS_INIT_COMPLETE_CMD

DESCRIPTION   This function registers for PS sys events and timer objects

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_process_init_complete_cmd( void )
{
  int16                          dss_errno = 0;
  ds3gsubsmgr_subs_id_e_type     ds3g_subs_indx = 0;
  ds_sys_subscription_enum_type  ds_subs_id;

  /*-------------------------------------------------------------------------
    Initialize timer interval and register for PS_SYS event per subscription
  -------------------------------------------------------------------------*/
  for( ds3g_subs_indx = 0;
       ds3g_subs_indx < DS3GSUBSMGR_SUBS_ID_MAX;
       ds3g_subs_indx++ )
  {
    ds_subs_id = ds3gsubsmgr_subs_id_ds3g_to_ds(ds3g_subs_indx);
  /*-----------------------------------------------------------------------
    Register Througput Information Ind Timer Expiry CB
  -----------------------------------------------------------------------*/
    memset(&ds3gtputmgr_tput_info_timer[ds3g_subs_indx],0,sizeof(rex_timer_type));

    ds3g_timer_register_ex((void *)&ds3gtputmgr_tput_info_timer[ds3g_subs_indx],
                       ds3gtputmgr_timer_cb,
                       DS3G_TIMER_REPORT_TPUT_INFO_IND,
                       ds3g_subs_indx);
    ds3g_timer_configure_periodicity_ex(DS3G_TIMER_REPORT_TPUT_INFO_IND, 
                                        TRUE,
                                        ds3g_subs_indx);

  /*-----------------------------------------------------------------------
    Register Timer to Actual Throughput Timer Expiry CB
  -----------------------------------------------------------------------*/
    memset(&ds3gtputmgr_refresh_actual_tput_timer[ds3g_subs_indx],0,sizeof(rex_timer_type));
    ds3g_timer_register_ex((void *)&ds3gtputmgr_refresh_actual_tput_timer[ds3g_subs_indx],
                       ds3gtputmgr_timer_cb,
                       DS3G_TIMER_REFRESH_ACTUAL_TPUT,
                       ds3g_subs_indx);

    ds3g_timer_configure_periodicity_ex(DS3G_TIMER_REFRESH_ACTUAL_TPUT, 
                                     TRUE,
                                     ds3g_subs_indx);

  /*-------------------------------------------------------------------------
    REGISTER TIMER TO SEND ESTIMATED DL THROUGHPUT TIMER EXPIRY CB
  -------------------------------------------------------------------------*/
    ds3g_timer_register_ex((void *)&ds3gtputmgr_est_dl_tput_timer[ds3g_subs_indx],
                       ds3gtputmgr_timer_cb,
                       DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION,
                       ds3g_subs_indx);

    ds3g_timer_configure_periodicity_ex(DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION,
                                     TRUE,
                                     ds3g_subs_indx);

    /*-----------------------------------------------------------------------
      Initialize Both Throughput Info Indication refresh actual throughput
      timer duration
    -----------------------------------------------------------------------*/
    ds3gtputmgr_actual_tput_interval[ds3g_subs_indx] = 0;
    ds3gtputmgr_ind_interval[ds3g_subs_indx] = 0;
    memset(&ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_indx],0,
           sizeof(ds3gtputmgr_dl_est_reporting_status_info_s));

    /*-----------------------------------------------------------------------
      Register for interested System Events soon after Init is complete
    -----------------------------------------------------------------------*/
    if( 0 > ps_sys_event_reg_ex(PS_SYS_TECH_ALL,
                                PS_SYS_EVENT_UPDATED_THROUGHPUT_TIMER,
                                (ps_sys_subscription_enum_type)ds_subs_id,
                                ds3gtputmgr_update_tput_info_ind_interval,
                                NULL,
                                &dss_errno) )
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,
                      "Couldn't reg for UPDATED_TPUT_TIMER event Err:%d",
                      dss_errno);
    }

    if (0 > ps_sys_event_reg_ex(PS_SYS_TECH_ALL,
                                PS_SYS_EVENT_UPDATED_DL_THROUGHPUT_TIMER,
                                (ps_sys_subscription_enum_type)ds_subs_id,
                                ds3gtputmgr_update_dl_tput_est_info_ind_interval,
                                NULL,
                                &dss_errno))
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,
                      "Couldn't reg for UPDATED_DL_TPUT_TIMER event Err:%d",
                      dss_errno);
    }
  }
  return;
} /* ds3gtputmgr_process_init_complete_cmd() */

/*===========================================================================
FUNCTION      DS3G_GET_ACTUAL_THROUGHPUT_REFRESH_INTERVAL

DESCRIPTION   For the given subscription ID, this function returns the time 
              interval(in msec), the actual throughput rate gets refreshed
              for every bearer/RLP depending on the current sys mode and
              the number of active calls 

DEPENDENCIES  The throughput Info event timer should be running to refresh 
              actual throughput for all bearers/RLP's  

RETURN VALUE  uint32 - Refresh Interval in (msec)
 
              0       - If the timer is not running (or) if the subscription
                        ID is invalid
 
SIDE EFFECTS  None
===========================================================================*/
uint32 ds3gtputmgr_get_actual_tput_refresh_interval
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id
)
{
  uint32  interval = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    interval = ds3gtputmgr_actual_tput_interval[ds3g_subs_id];
  }
  return interval;
} /* ds3g_get_actual_throughput_refresh_interval */


/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_ESTIMATED_DL_TPUT_INTERVAL

DESCRIPTION   For the given subscription ID, this function returns the 
              multiplier for t_report

DEPENDENCIES  

RETURN VALUE  uint32 - T_accumulate multiplier 

SIDE EFFECTS  None
===========================================================================*/
uint32 ds3gtputmgr_get_estimated_dl_tput_requested_interval
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  uint32  interval = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    interval = ds3gtputmgr_dl_est_reporting_status_info[ds3g_subs_id].
        requested_interval;
  }
  return interval;
}/* ds3gtputmgr_get_estimated_dl_tput_interval */


/*===========================================================================
FUNCTION      DS3GTPUTMGR_PROCESS_TIMER_EXPIRED_CMD

DESCRIPTION   This function processes the DS_CMD_DS3GTPUTMGR_TIMER_EXPIRED. 
              It determines which timer has expired, and calls the appropriate
              function to handle the timer expiry.

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_process_timer_expired_cmd
(
  ds_cmd_type  *cmd_ptr
)
{
  ds3g_timer_enum_type                 timer_id = DS3G_TIMER_MIN;
  uint16                               timer_exp_cnt = 0;
  ds3gsubsmgr_subs_id_e_type           ds3g_subs_id;
  ds3gtputmgr_timer_expired_cmd_type  *timer_exp_cmd_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if( ( cmd_ptr == NULL ) || (cmd_ptr->cmd_payload_ptr == NULL) )
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "NULL cmd_ptr or payload ptr passed");
    return;
  }

  if(cmd_ptr->hdr.cmd_id != DS_CMD_DS3GTPUTMGR_TIMER_EXPIRED)
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid command:%d",
                    cmd_ptr->hdr.cmd_id);
    return;
  }

  timer_exp_cmd_ptr
    = (ds3gtputmgr_timer_expired_cmd_type*)cmd_ptr->cmd_payload_ptr;
  timer_id = timer_exp_cmd_ptr->timer_id;
  ds3g_subs_id = timer_exp_cmd_ptr->subs_indx;

  /*-------------------------------------------------------------------------
    Check if the timer id is valid
  -------------------------------------------------------------------------*/
  if( (timer_id < DS3G_TIMER_REPORT_TPUT_INFO_IND) ||
      (timer_id > DS3G_TIMER_UPLINK_THROUGHPUT) )
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,
                    "Invalid timer_id %d to process timer expiry", timer_id);
    return;
  }

  /*-------------------------------------------------------------------------
    Cache the timer expiry count before clearing
  -------------------------------------------------------------------------*/
  timer_exp_cnt = ds3gtputmgr_get_timer_exp_cnt(ds3g_subs_id, timer_id);
  ds3gtputmgr_clr_timer_exp_cnt(ds3g_subs_id, timer_id);
  DATA_3GMGR_MSG2(MSG_LEGACY_MED, "timer_id:%d expiry_count:%d",
                  timer_id, timer_exp_cnt);

  /*-------------------------------------------------------------------------
    Call the appropriate function, based on the timer that expired.
  -------------------------------------------------------------------------*/
  switch(timer_id)
  {
    case DS3G_TIMER_REPORT_TPUT_INFO_IND:
      ds3g_report_throughput_info(ds3g_subs_id);
      break;

    case DS3G_TIMER_REFRESH_ACTUAL_TPUT:
      ds3g_refresh_actual_throughput(ds3g_subs_id, timer_exp_cnt);
      break;

    case DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION:
       ds3g_send_downlink_throughput_estimation_ind(ds3g_subs_id);
       break;

    case DS3G_TIMER_ULDL_THROUGHPUT:                       
      ds3gtputmgr_report_uldl_throughput_request(ds3g_subs_id,timer_exp_cnt);
      break;
#ifdef FEATURE_DATA_RAVE_SUPPORT 
    case DS3G_TIMER_UPLINK_THROUGHPUT:
      ds3gtputmgr_report_uplink_throughput_response(ds3g_subs_id);
      break;
#endif
    default:
      break;
  }

  return;
} /* ds3gtputmgr_process_timer_expired_cmd() */
/*==============================================================================
FUNCTION      DS3GGTPUTMGR_UPDATE_ULDL_THROUGHPUT_INFO_IND_INTERVAL

DESCRIPTION   This is a callback function registered with PS per subscription 
              ID, that gets triggered, when an active client sets a timer
              value 'x' (msec) to periodically report uld dl throughput
              Info indication  When  'x' != 0   the throughput Info indication
              timer is started to periodically expire and generate Indication
 
              'x'  = 0   The throughput Info Indication timer is stopped, if
              already running ==> Reporting throughput Indication for the
              current subscription ID is stopped
 
DEPENDENCIES  None 
 
RETURN VALUE  None
===========================================================================*/
void ds3gtputmgr_update_uldl_throughput_info_interval
(
  ps_sys_tech_enum_type                          tech_type,
  ps_sys_event_enum_type                         event_name,
  ps_sys_subscription_enum_type                  ps_subs_id,
  void                                          *event_info_ptr,
  void                                          *user_data_ptr
)
{
  uint32                       frequency      = 0;
  ds3gsubsmgr_subs_id_e_type   ds3g_subs_id   = DS3GSUBSMGR_SUBS_ID_INVALID;
  ds_cmd_type                 *cmd_ptr = NULL;
  ds3gtputmgr_reporting_info  *cmd_info_ptr = NULL;
  /*----------------------------------------------------------------------- 
    Validate Arguments
    -----------------------------------------------------------------------*/
  if (event_info_ptr == NULL)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_LOW, "NULL ptr passed");
    return;
  }
  if (event_name != PS_SYS_EVENT_UL_DL_THROUGHPUT_INFO_FREQ)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_LOW, "Ignoring Event");
    return;
  }

  /*----------------------------------------------------------------------- 
    Get DS 3G SUB ID from PS Subscription ID
    -----------------------------------------------------------------------*/
  ds3g_subs_id = ds3gsubsmgr_subs_id_ds_to_ds3g((ds3gsubsmgr_subs_id_e_type)
                                                ps_subs_id);
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return;
  }

  frequency = *(uint32 *)event_info_ptr;


  /*-----------------------------------------------------------------
      Post a command.
  -----------------------------------------------------------------*/
  cmd_ptr = ds_allocate_cmd_buf(sizeof(ds3gtputmgr_reporting_info));
  if( (cmd_ptr == NULL) || (cmd_ptr->cmd_payload_ptr == NULL) )
  {
    ASSERT(0);
    return;
  }

  cmd_ptr->hdr.cmd_id = DS_CMD_PROCESS_ULDL_TIMER_CMD;
  cmd_info_ptr = (ds3gtputmgr_reporting_info*)cmd_ptr->cmd_payload_ptr;

  cmd_info_ptr->frequency = frequency;
  cmd_info_ptr->ds3g_subs_id = ds3g_subs_id;

  DS_3GPP_MSG1_HIGH("Posted Process uplink downlink timer command %d",
                             frequency);

  ds_put_cmd_ext(cmd_ptr);

} /* ds3gtputmgr_update_uldl_throughput_info_interval */
/*===========================================================================
FUNCTION      DS3G_PROCESS_ULDL_TIMER_CMD

DESCRIPTION   This function is called to process the uplink downlink timer 
              command 

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3g_process_uldl_timer_cmd
(
  const ds_cmd_type  *cmd_ptr
)
{
  ds3gtputmgr_reporting_info        *data_block_ptr = NULL;
  uint32                            frequency = 0;
  ds3gsubsmgr_subs_id_e_type        ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  uint32                            old_frequency = 0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if( (cmd_ptr == NULL) || (cmd_ptr->cmd_payload_ptr == NULL) )
  {
    return;
  }

  data_block_ptr
    = (ds3gtputmgr_reporting_info *)cmd_ptr->cmd_payload_ptr;

  frequency =  data_block_ptr->frequency;
  ds3g_subs_id = data_block_ptr->ds3g_subs_id;
  old_frequency = ds3gtputmgr_get_uldl_throughput_frequency(ds3g_subs_id);
  /*-------------------------------------------------------------------------
    Get the timer value to be updated
    -------------------------------------------------------------------------*/

  if ((frequency >0) && 
      (old_frequency >0) &&
      (frequency != old_frequency)
       )
  {
    /*Set Frequency to zero, stop timer, inform lower layers*/
    ds3gtputmgr_set_uldl_throughput_frequency(ds3g_subs_id,0);
    ds3gtputmgr_update_tput_uldl_timer(ds3g_subs_id);
  }
  
  if (FALSE == ds3gtputmgr_set_uldl_throughput_frequency(ds3g_subs_id,
                                                         frequency))
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_ERROR,
                    "Can't set frequency %d on ds3g subs id %d",
                    frequency,ds3g_subs_id);
    return;
  }
   
  /*Set Frequency, start timer and inform lower layers*/
  ds3gtputmgr_update_tput_uldl_timer(ds3g_subs_id);
  return;
} /* ds3g_process_uldl_timer_cmd */
/*===========================================================================
FUNCTION      DS3GTPUTMGR_ULDL_TIMER_INTIT

DESCRIPTION   This function registers for PS sys events and timer objects related 
to throughput reporting ul dl 

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_uldl_timer_init(void)
{
  int16                   dss_errno                 = 0;
  uint8                   index                     = 0;

  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - */
  for (index = 0; index < DS3GSUBSMGR_SUBS_ID_MAX;index++) 
  {
    ds3gtputmgr_uldl_throughput_info[index].
      achievable_lte_in_progress = FALSE;
    ds3gtputmgr_uldl_throughput_info[index].
      achievable_wcdma_in_progress = FALSE;
    ds3gtputmgr_uldl_throughput_info[index].timer_count = 0;
    ds3gtputmgr_uldl_throughput_info[index].frequency  = 0;

    /*---------------------------------------------------------------------- 
      REGISTER THROUGPUT INFORMATION IND TIMER EXPIRY CB
      ---------------------------------------------------------------------*/
    memset(&(ds3gtputmgr_uldl_throughput_info[index].uldl_throughput_timer),0,sizeof(rex_timer_type));
    ds3g_timer_register_ex((void *)&(ds3gtputmgr_uldl_throughput_info[index].uldl_throughput_timer),
                         ds3gtputmgr_timer_cb,
                         DS3G_TIMER_ULDL_THROUGHPUT,
                         index);

    ds3g_timer_configure_periodicity_ex(DS3G_TIMER_ULDL_THROUGHPUT, 
                                        TRUE,
                                        index);

    /*------------------------------------------------------------------------- 
      Register for interested System Events soon after Init is complete
      -------------------------------------------------------------------------*/
    if (0 > ps_sys_event_reg_ex(PS_SYS_TECH_ALL,
                                PS_SYS_EVENT_UL_DL_THROUGHPUT_INFO_FREQ,
                                (ps_sys_subscription_enum_type)
                                  ds3gsubsmgr_subs_id_ds3g_to_ds(index),
                                ds3gtputmgr_update_uldl_throughput_info_interval,
                                NULL,
                                &dss_errno))
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Couldn't reg for UL_DL Throughput Info"
                    "event Err : (%d)", dss_errno);
    }
  }
}
#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*==============================================================================
FUNCTION      DS3GGTPUTMGR_UPLINK_THROUGHPUT_INFO_IND_INTERVAL

DESCRIPTION   This is a callback function registered with PS per subscription 
              ID, that gets triggered, when an active client sets a timer
              value 'x' (msec) to periodically report Uplink throughput
              Info indication  When  'x' != 0   the throughput Info indication
              timer is started to periodically expire and generate Indication
 
              'x'  = 0   The throughput Info Indication timer is stopped, if
              already running ==> Reporting throughput Indication for the
              current subscription ID is stopped
 
DEPENDENCIES  None 
 
RETURN VALUE  None
===========================================================================*/
void ds3gtputmgr_uplink_throughput_info_interval
(
  ps_sys_tech_enum_type                          tech_type,
  ps_sys_event_enum_type                         event_name,
  ps_sys_subscription_enum_type                  ps_subs_id,
  void                                          *event_info_ptr,
  void                                          *user_data_ptr
)
{
  uint32                       uplink_reporting_period      = 0;
  uint32                       old_uplink_throughput_reporting = 0;
  ds3gsubsmgr_subs_id_e_type   ds3g_subs_id   = DS3GSUBSMGR_SUBS_ID_INVALID;

  /*----------------------------------------------------------------------- 
    Validate Arguments
  -----------------------------------------------------------------------*/

  if ((event_info_ptr == NULL)|| 
      (event_name != PS_SYS_EVENT_UPDATED_UL_THROUGHPUT_TIMER))
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR, "NULL Argument or Incorrect Timer. "
              "Cannot update UL DLthroughput Info Ind Interval");
    return;
  }

  /*----------------------------------------------------------------------- 
    Get DS 3G SUB ID from PS Subscription ID
    -----------------------------------------------------------------------*/
  ds3g_subs_id = ds3gsubsmgr_subs_id_ds_to_ds3g((ds3gsubsmgr_subs_id_e_type)
                                                ps_subs_id);

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

  old_uplink_throughput_reporting = 
    ds3gtputmgr_get_uplink_throughput_reporting(ds3g_subs_id);

  /*-------------------------------------------------------------------------
    Get the timer value to be updated
    -------------------------------------------------------------------------*/

  uplink_reporting_period = *(uint32 *)event_info_ptr;

  /* old reporting period is same as new reporting period */
  if (uplink_reporting_period == old_uplink_throughput_reporting)
  {
    return;
  }

  /* old reporting period is different from new reporting period */
  if ((uplink_reporting_period >0) && 
      (old_uplink_throughput_reporting >0) &&
      (uplink_reporting_period != old_uplink_throughput_reporting)
       )
  {
    ds3gtputmgr_set_uplink_throughput_reporting(ds3g_subs_id,0);
    ds3gtputmgr_update_uplink_throughput_timer(ds3g_subs_id,TRUE);
  }
  
  if (FALSE == ds3gtputmgr_set_uplink_throughput_reporting(
                 ds3g_subs_id,uplink_reporting_period))
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_ERROR, "Can't set Uplink throughput "
                   "reporting %d on ds3g subs id %d",
                    uplink_reporting_period,ds3g_subs_id);
    return;
  }
  else
  {
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH, "Set Uplink throughput reporting "
                    "%d on ds3g subsid %d",
                    uplink_reporting_period,ds3g_subs_id);

  }
   
  /*Set Frequency, start timer and inform lower layers*/
  ds3gtputmgr_update_uplink_throughput_timer(ds3g_subs_id,TRUE);
  return;
}

/*===========================================================================
FUNCTION      DS3GTPUTMGR_UPLINK_THROUGHPUT_INTIT

DESCRIPTION   This function registers for PS sys events and initializes 
              the uplink throughput reporting variables 

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_uplink_throughput_init(void)
{
  int16                   dss_errno                 = 0;
  uint8                   index                     = 0;

  /* - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - -  */
  for (index = 0; index < DS3GSUBSMGR_SUBS_ID_MAX;index++) 
  {
    memset(&ds3gtputmgr_uplink_throughput_info[index],
          0,sizeof(ds3gtputmgr_uplink_throughput_info_s));
    /*---------------------------------------------------------------------- 
     Register for interested System Events soon after Init is complete
     ------------------------------------------------------------------------*/
    if (0 > ps_sys_event_reg_ex(PS_SYS_TECH_ALL,
                               PS_SYS_EVENT_UPDATED_UL_THROUGHPUT_TIMER,
                               (ps_sys_subscription_enum_type)
                                  ds3gsubsmgr_subs_id_ds3g_to_ds(index),
                               ds3gtputmgr_uplink_throughput_info_interval,
                               NULL,
                               &dss_errno))
    {
      DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Couldn't reg for Uplink Throughput "
                      "Timer event Err : (%d)", dss_errno);
      continue;
    }

    ds3g_timer_register_ex(
       (void *)&(ds3gtputmgr_uplink_throughput_info[index].uplink_throughput_timer),
      ds3gtputmgr_timer_cb,
      DS3G_TIMER_UPLINK_THROUGHPUT,
      index);

    ds3g_timer_configure_periodicity_ex(DS3G_TIMER_UPLINK_THROUGHPUT,
                                       TRUE,
                                       index);
        
  }
}
/*==============================================================================
FUNCTION      DS3GTPUMGR_UPDATE_UPLINK_THROUGHPUT_TIMER

DESCRIPTION   This functions is invoked by modehandler in wcdma and lte mode 
              during call transition. This inturn will clear the timer and
              inform lower layer if needed/ start the timer and inform the
              upper layer
 
DEPENDENCIES  None 
 
RETURN VALUE  None
===========================================================================*/
void ds3gtputmgr_update_uplink_throughput_timer
(
   ds3gsubsmgr_subs_id_e_type ds3g_subs_id_reporting,
   boolean                    tput_module
)
{
  uint32                       reporting_timer = 0;
  ds3gsubsmgr_subs_id_e_type   ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  ds3g_timer_enum_type         timer_id     = DS3G_TIMER_UPLINK_THROUGHPUT;
  uint32                       actual_reporting_timer =0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ds3g_subs_id   = ds3g_subs_id_reporting;

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }
  /*-----------------------------------------------------------------------
    Get the timer value to be updated
    -----------------------------------------------------------------------*/
  reporting_timer = ds3gtputmgr_get_uplink_throughput_reporting(ds3g_subs_id);
  /*Start Timer and Inform Lower layer*/


  /*If reporting timer is greater than zero or the client is setting 
    reporting timer to zero */
  if (reporting_timer > 0 ||
      tput_module )
  {
    actual_reporting_timer = ds_3gpp_hdlr_configure_uplink_timer(
                              timer_id, 
                              ds3gsubsmgr_subs_id_ds3g_to_cm(ds3g_subs_id),
                              reporting_timer);

    if(actual_reporting_timer > 0)
    {
      ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
        actual_throughput_reporting = actual_reporting_timer;
    }

    if(reporting_timer == 0)
    {
      ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
        actual_throughput_reporting = 0;
    }

    /*If LTE is not in progress and wcdma is not in progress; 
      set the data rate and confidence to invalid
      and report as the last report*/
    ds3gtputmgr_report_uplink_throughput_status_response(ds3g_subs_id);

    if ( ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_confidence 
         != DS3G_UL_INVALID_CONFIDENCE)
    {
        ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
          uplink_confidence = DS3G_UL_INVALID_CONFIDENCE;
        ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
          uplink_data_rate_kbps = DS3G_UL_INVALID_THROUGHPUT;
        ds3gtputmgr_report_uplink_throughput_response(ds3g_subs_id);
    }
  }  
}
#endif
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_ULDL_THROUGHPUT_FRQEUENCY

DESCRIPTION   This function configures frequency to generate 
              uldl throughput information Ind periodically. 

DEPENDENCIES  None

RETURN VALUE  TRUE  - If Throughput Info Ind interval has been set for 
                      the Subscription ID
              FALSE - Otherwise
 
SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_set_uldl_throughput_frequency
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  uint32 frequency
)
{
  /*-------------------------------------------------------------------------
    Is the Subs ID Valid?
  -------------------------------------------------------------------------*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }
  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"Subs ID: %d Set Throughput Info Frequency"
                                  "to %d", ds3g_subs_id, frequency);
  ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].frequency = frequency;
  return TRUE;
}/* ds3gtputmgr_set_throughput_info_ind_interval() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_ULDL_THROUGHPUT_FREQUENCY

DESCRIPTION   This function returns the uldl frequency for the given ds3g 
              subs_id 

DEPENDENCIES  None

RETURN VALUE  uint32 - Frequency
 
              0       - If the timer is not running (or) if the subscription
                        ID is invalid
 
SIDE EFFECTS  None
===========================================================================*/
uint32 ds3gtputmgr_get_uldl_throughput_frequency
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  uint32          frequency = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    frequency = ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].frequency;
  }
  return frequency;
} 

#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_UPLINK_THROUGHPUT_REPORTING

DESCRIPTION   This function configures uplink reporting period to 
              generate uplink throughput information Ind periodically. 

DEPENDENCIES  None

RETURN VALUE  TRUE  - If Uplink Throughput Info Ind interval has been set for 
                      the Subscription ID
              FALSE - Otherwise
 
SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_set_uplink_throughput_reporting
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  uint32                     uplink_throughput_reporting
)
{
  /*-------------------------------------------------------------------------
    Is the Subs ID Valid?
  -------------------------------------------------------------------------*/
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    return FALSE;
  }
  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"Subs ID: %d Set Uplink Throughput Info Reporting"
                  "to %d", ds3g_subs_id, uplink_throughput_reporting);
  ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
    uplink_throughput_reporting = uplink_throughput_reporting;
  return TRUE;
}/* ds3gtputmgr_set_throughput_info_ind_interval() */

/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_UPLINK_THROUGHPUT_REPORTING

DESCRIPTION   This function gets the uplink reporting value for the given ds3g 
              subs_id 

DEPENDENCIES  None

RETURN VALUE  uint32 - Frequency
 
              0       - If the timer is not running (or) if the subscription
                        ID is invalid
 
SIDE EFFECTS  None
===========================================================================*/
uint32 ds3gtputmgr_get_uplink_throughput_reporting
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  uint32          uplink_throughput_reporting = 0;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
 if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
 {
   uplink_throughput_reporting = 
     ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_throughput_reporting;
 }
  return uplink_throughput_reporting;
} 
#endif

/*===========================================================================
FUNCTION      DS3GTPUTMGR_RESET_THROUGHPUT_INFO_TIMER_COUNT

DESCRIPTION   This function resets uldl throughput timer count for the 
              corresponding ds3g subs id

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_reset_uldl_throughput_timer_count
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].timer_count=0;
  }

  return;
} 

/*===========================================================================
FUNCTION      DS3GTPUTMGR_RESET_WCDMA_ACHIEVABLE_IN_PROGRESS

DESCRIPTION   This function resets wcdma achievable  variable which
              tracks whether the achievable throughput timer is running for wcdma
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_reset_wcdma_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
      achievable_wcdma_in_progress=  FALSE;
  }

  return;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_RESET_LTE_ACHIEVABLE_IN_PROGRESS

DESCRIPTION   This function resets lte achievable   variable which
              tracks whether the achievable throughput timer is running for lte
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_reset_lte_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
      achievable_lte_in_progress= FALSE;
  }

  return;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_WCDMA_ACHIEVABLE_IN_PROGRESS

DESCRIPTION   This function sets wcdma achievable   variable which
              tracks whether the achievable throughput timer is running for wcdma
              rat on a particular subscription


DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_wcdma_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
      achievable_wcdma_in_progress= TRUE;
  }

  return;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_LTE_ACHIEVABLE_IN_PROGRESS

DESCRIPTION    This function sets lte achievable   variable which
              tracks whether the achievable throughput timer is running for lte
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_lte_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
      achievable_lte_in_progress= TRUE;
  }

  return;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_LTE_ACHIEVABLE_IN_PROGRESS

DESCRIPTION   This function gets lte achievable   variable which
              tracks whether the achievable throughput timer is running for
              lte rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_get_lte_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  boolean ret_val= FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ret_val = ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
                achievable_lte_in_progress;
  }

  return ret_val;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_WCDMA_ACHIEVABLE_IN_PROGRESS

DESCRIPTION   This function gets wcdma achievable   variable which
              tracks whether the achievable throughput timer is running for
              wcdma rat for a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
boolean  ds3gtputmgr_get_wcdma_achievable_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  boolean ret_val= FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ret_val = ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
                achievable_wcdma_in_progress;
  }

  return ret_val;
} 

#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_WCDMA_UPLINK_IN_PROGRESS

DESCRIPTION   This function sets wcdma uplink  variable which
              tracks whether the uplink throughput timer is running for wcdma
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_wcdma_uplink_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  boolean                    uplink_status
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
      wcdma_in_progress= uplink_status;
  }
  return;
} 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_LTE_UPLINK_IN_PROGRESS

DESCRIPTION   This function sets lte uplink  variable which
              tracks whether the uplink throughput timer is running for lte
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_lte_uplink_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
    boolean                  uplink_status
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
      lte_in_progress= uplink_status;
  }

  return;
} 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_LTE_UPLINK_IN_PROGRESS

DESCRIPTION   This function gets lte uplink   variable which
              tracks whether the uplink throughput timer is running for
              lte rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds3gtputmgr_get_lte_uplink_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  boolean ret_val= FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ret_val = ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
                lte_in_progress;
  }

  return ret_val;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_WCDMA_UPLINK_IN_PROGRESS

DESCRIPTION   This function gets wcdma uplink   variable which
              tracks whether the uplink throughput timer is running for
              wcdma rat for a particular subscription

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean  ds3gtputmgr_get_wcdma_uplink_in_progress
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  boolean ret_val= FALSE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ret_val = ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].
                wcdma_in_progress;
  }

  return ret_val;
}
 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_UPLINK_THROUGHPUT_RATE_PARAMS

DESCRIPTION   This function sets lte uplink  variable which
              tracks whether the uplink throughput timer is running for lte
              rat on a particular subscription

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_uplink_throughput_rate_params
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint32  uplink_datarate_kbps,
  uint8  confidence
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id) &&
       (ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].lte_in_progress ||
         ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].wcdma_in_progress))
  {
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_data_rate_kbps = 
      uplink_datarate_kbps;
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_confidence = 
      confidence;

    if (ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].wcdma_in_progress)
    {
      ds3gtputmgr_report_uplink_throughput_response(ds3g_subs_id);
    }

  }

  
  return;
} 
#endif
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_ULDL_THROUGHPUT_TIMER_COUNT

DESCRIPTION   This function sets uldl throughput timer count for the corresponding
              ds3g subs id

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_set_uldl_throughput_timer_count
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id,
  uint64                     new_timer_count
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].
      timer_count= new_timer_count;
  }

  return;
} 


/*===========================================================================
FUNCTION      DS3GTPUTMGR_GET_ULDL_THROUGHPUT_TIMER_COUNT

DESCRIPTION   This function gets uldl throughput timer count for the 
              corresponding ds3g subs id

DEPENDENCIES  None

RETURN VALUE None

SIDE EFFECTS  None
===========================================================================*/
uint64 ds3gtputmgr_get_uldl_throughput_timer_count
(
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id
)
{
  uint64          timer_count = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    timer_count = ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].timer_count;
  }

  return timer_count;
} 

/*==============================================================================
FUNCTION      DS3GTPUTMGR_UPDATE_TPUT_ULDL_TIMER

DESCRIPTION   This functions is invoked by modehandler in wcdma and lte mode 
              during call transition. This inturn will clear the timer and
              inform lower layer if needed/ start the timer and inform the
              upper layer
 
DEPENDENCIES  None 
 
RETURN VALUE  None
===========================================================================*/
void ds3gtputmgr_update_tput_uldl_timer
(
   ds3gsubsmgr_subs_id_e_type ds3g_subs_id_reporting
)
{
  uint32                       frequency      = 0;
  ds3gsubsmgr_subs_id_e_type   ds3g_subs_id   = DS3GSUBSMGR_SUBS_ID_INVALID;
  ds3g_timer_enum_type         timer_id       = DS3G_TIMER_ULDL_THROUGHPUT;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  ds3g_subs_id   = ds3g_subs_id_reporting;

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }
  /*-------------------------------------------------------------------------
    Get the timer value to be updated
    -------------------------------------------------------------------------*/
  frequency = ds3gtputmgr_get_uldl_throughput_frequency(ds3g_subs_id);

  if (&(ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].uldl_throughput_timer) != NULL)
  {
    /*Start Timer and Inform Lower layer*/
    ds_3gpp_hdlr_configure_uldl_timer(
       timer_id,
       ds3gsubsmgr_subs_id_ds3g_to_cm(ds3g_subs_id),
       frequency);
  }
}
/*===========================================================================
FUNCTION      DS3GTPUTMGR_REPORT_ULDL_THROUGHPUT_REQUEST

DESCRIPTION   This function invokes the mode handlers to request the report for 
              the uplink downlink achievable data rate and allowed data rate
              during the current sampling interval

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_report_uldl_throughput_request
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint16                      timer_expiry_count
)
{
  uint32                    frequency  = 0; 
  uint16                    reminder = 0; 
  sys_modem_as_id_e_type    subs_id = SYS_MODEM_AS_ID_NONE;
  uint64                    timer_count =0;
  boolean                   query_achievable= FALSE;

  frequency = ds3gtputmgr_get_uldl_throughput_frequency(ds3g_subs_id);
  subs_id = ds3gsubsmgr_subs_id_ds3g_to_cm(ds3g_subs_id);

  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_HIGH,"Invalid DS 3G sub ID %d to report tput",
                    ds3g_subs_id);
    return;
  }  

  if (frequency == 0)
  {
    DATA_3GMGR_MSG0(MSG_LEGACY_ERROR,"frequency is zero(0)");
    return;
  }  

  timer_count = ds3gtputmgr_get_uldl_throughput_timer_count(ds3g_subs_id);
  timer_count = timer_count + timer_expiry_count;
  ds3gtputmgr_set_uldl_throughput_timer_count(ds3g_subs_id,timer_count);
  reminder = timer_count%frequency;

  if (reminder ==0 )
  {
     query_achievable = TRUE;
  }

  ds_3gpp_hdlr_query_uldl_throughput_request(query_achievable,
                                             ds3gsubsmgr_subs_id_ds3g_to_cm
                                               (ds3g_subs_id),
                                             timer_expiry_count);
    
  return;
} 

/*===========================================================================
FUNCTION      DS3GTPUTMGR_REPORT_ULDL_THROUGHPUT_RESPONSE

DESCRIPTION   This function is invoked by modehandler to report the achievable 
              and allowed  data rate from lte rats to ps framework

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void  ds3gtputmgr_report_uldl_throughput_response
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint32  achievable_ul_data_rate,
  uint32  achievable_dl_data_rate
)
{
  ps_sys_ul_dl_throughput_info_type *report_p  = NULL;
  int16                             dss_errno           = 0;  


  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

  report_p = (ps_sys_ul_dl_throughput_info_type*)modem_mem_alloc(
                                        sizeof(ps_sys_ul_dl_throughput_info_type),
                                        MODEM_MEM_CLIENT_DATA);

  if (report_p != NULL)
  {
    report_p->ul_achievable_throughput = achievable_ul_data_rate;
    report_p->dl_achievable_throughput = achievable_dl_data_rate;
    report_p->ul_actual_throughput = 
      ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_ul_data_rate;
    report_p->dl_actual_throughput = 
      ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_dl_data_rate;
    DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"Achievable Throughput Info ul_rate %d "
                                    "dl rate %d",
                    achievable_ul_data_rate,
                    achievable_dl_data_rate);

    if (0 > ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                               PS_SYS_CONF_UL_DL_THROUGHPUT_INFO,
                               (ps_sys_subscription_enum_type)
                                 ds3gsubsmgr_subs_id_ds3g_to_ds(ds3g_subs_id),
                               (void*)report_p,
                               &dss_errno))
    {
        DS_3GPP_MSG0_HIGH("Sending Throughput Info Ind failed");
    }
    modem_mem_free(report_p, MODEM_MEM_CLIENT_DATA);
  }
}
/*===========================================================================
FUNCTION      DS3GTPUTMGR_REPORT_ULDL_CONFIGURED_THROUGHPUT_RESPONSE

DESCRIPTION   This function is invoked to report the configurable data rate 
              for wcdma/lte rats to ps framework


DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void  ds3gtputmgr_report_uldl_configured_throughput_response
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id,
  uint32  uplink_configured_kbps,
  uint32  downlink_configured_kbps
)
{
  ps_sys_configured_throughput_info_type  ps_configured_throughput_info;
  int16                            ps_error = DS_ENOERR;

  ps_configured_throughput_info.ul_configured_throughput = 
    uplink_configured_kbps;
  ps_configured_throughput_info.dl_configured_throughput = 
    downlink_configured_kbps;

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"Configured Throughput Info ul_rate"
            " %d dl rate %d",uplink_configured_kbps,downlink_configured_kbps);

  (void)ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                           PS_SYS_CONF_CONFIGURED_UL_DL_THROUGHPUT_INFO,
                            (ps_sys_subscription_enum_type)
                              ds3gsubsmgr_subs_id_ds3g_to_ds(ds3g_subs_id),
                            (void*)(&ps_configured_throughput_info),
                            &ps_error
                             );
}
#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_REPORT_UPLINK_THROUGHPUT_RESPONSE

DESCRIPTION   This function is invoked to report the uplink data rate 
              and confidence for wcdma/lte rats to ps framework


DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void  ds3gtputmgr_report_uplink_throughput_response
(
  ds3gsubsmgr_subs_id_e_type  ds3g_subs_id
)
{
  ps_sys_ul_throughput_info_type          ul_tput_info;
  int16                                   ps_error = DS_ENOERR;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

  ul_tput_info.uplink_allowed_rate = 
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_data_rate_kbps;
  ul_tput_info.confidence_level = 
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].uplink_confidence;

  DATA_3GMGR_MSG2(MSG_LEGACY_HIGH,"Reporting Uplink Throughput Info"
            " %d confidence %d",ul_tput_info.uplink_allowed_rate,
             ul_tput_info.confidence_level);

  (void)ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                      PS_SYS_CONF_UL_THROUGHPUT_INFO,
                      (ps_sys_subscription_enum_type)
                      ds3g_subs_id,
                      &ul_tput_info, 
                      &ps_error);
}
/*===========================================================================
FUNCTION      DS3GTPUTMGR_REPORT_UPLINK_THROUGHPUT_STATUS_RESPONSE

DESCRIPTION   This function is invoked to report the uplink data rate 
              and confidence for wcdma/lte rats to ps framework


DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void  ds3gtputmgr_report_uplink_throughput_status_response
(
  ds3gsubsmgr_subs_id_e_type                ds3g_subs_id
)
{
  ps_sys_thrpt_status_type    ul_tput_status;
  int16                       ps_error = DS_ENOERR;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
    return;
  }

  ul_tput_status.actual_interval = 
    ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].actual_throughput_reporting;
  ul_tput_status.thrpt_status    =  
    (ps_sys_thrpt_status_reason_enum_type)
      ds3gtputmgr_current_uplink_status(ds3g_subs_id);

  (void)ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                      PS_SYS_CONF_UL_THROUGHPUT_INTERVAL_STATUS_CHANGE,
                      (ps_sys_subscription_enum_type)
                      ds3g_subs_id,
                      &ul_tput_status, 
                      &ps_error);

}
/*===========================================================================
FUNCTION      DS3GTPUTMGR_CURRENT_UPLINK_STATUS

DESCRIPTION   This function is invoked to get the current uplink status 
              
DEPENDENCIES  None

RETURN VALUE  ds3gtputmgr_est_reporting_status_e_type

SIDE EFFECTS  None
===========================================================================*/
ds3gtputmgr_est_reporting_status_e_type ds3gtputmgr_current_uplink_status
(
   ds3gsubsmgr_subs_id_e_type                           ds3g_subs_id
)
{

  sys_sys_mode_e_type                     current_mode = SYS_SYS_MODE_NONE;
  ds3gtputmgr_est_reporting_status_e_type uplink_tput_status;
  sys_modem_as_id_e_type                  cm_subs_id   = SYS_MODEM_AS_ID_NONE;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",
                    ds3g_subs_id);
    return DS3G_REPORT_DISABLED_UNSUPPORTED_RAT;
  }

  cm_subs_id = ds3gsubsmgr_subs_id_ds3g_to_cm(ds3g_subs_id);
  current_mode = ds3g_get_current_network_mode_ex(cm_subs_id);


  if (ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].lte_in_progress ||
      ds3gtputmgr_uplink_throughput_info[ds3g_subs_id].wcdma_in_progress )
  {
      return DS3G_REPORT_ENABLED;
  }

  if ((current_mode == SYS_SYS_MODE_LTE) || 
      (current_mode == SYS_SYS_MODE_WCDMA))
  {
    uplink_tput_status = ds_3gpp_hdlr_get_current_uplink_status(
                          ds3gsubsmgr_subs_id_ds3g_to_cm(ds3g_subs_id));
  }
  else
  {
    uplink_tput_status = DS3G_REPORT_DISABLED_UNSUPPORTED_RAT;
  }
  return uplink_tput_status;
}
#endif
/*===========================================================================
FUNCTION      DS3GTPUTMGR_STOP_ULDL_THROUGHPUT_TIMER

DESCRIPTION   This function stops the uldl timer and resets other statistics 
              related to uldl for each subs_id 

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_stop_uldl_throughput_timer
(
   ds3g_timer_enum_type           timer_id,
   ds3gsubsmgr_subs_id_e_type   ds3g_subs_id
)
{
   if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
   {
     DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
     return;
   }
   ds3gtputmgr_reset_uldl_throughput_timer_count(ds3g_subs_id);
   ds3g_timer_stop_ex(timer_id,ds3g_subs_id);
   ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_ul_data_rate = 
    0; 
  ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_dl_data_rate = 
    0;
}

#ifdef FEATURE_DATA_RAVE_SUPPORT 
/*===========================================================================
FUNCTION      DS3GTPUTMGR_STOP_UPLINK_THROUGHPUT_TIMER

DESCRIPTION   This function stops the uldl timer and resets other statistics 
              related to uldl for each subs_id 

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds3gtputmgr_stop_uplink_throughput_timer
(
   ds3g_timer_enum_type           timer_id,
   ds3gsubsmgr_subs_id_e_type   ds3g_subs_id
)
{
   if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
   {
     DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",ds3g_subs_id);
     return;
   }

   ds3g_timer_stop_ex(timer_id,ds3g_subs_id);
}
#endif
/*===========================================================================
FUNCTION      DS3GTPUTMGR_SET_ACTUAL_DATARATES

DESCRIPTION   This function is invoked to set the actual uplink/downlink 
              data rates  for  each subscription


DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/

void ds3gtputmgr_set_actual_datarates
(
   ds3gsubsmgr_subs_id_e_type   ds3g_subs_id,
   uint32                       average_ul_data_rate,
   uint32                       average_dl_data_rate
)
{ 
  if (!ds3gsubsmgr_is_ds3g_subs_index_valid(ds3g_subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR, "Invalid DS3G sub ID %d",
                    ds3g_subs_id);
    return;
  }

  ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_ul_data_rate = 
    average_ul_data_rate; 
  ds3gtputmgr_uldl_throughput_info[ds3g_subs_id].actual_dl_data_rate = 
    average_dl_data_rate;

  
  DS_3GPP_MSG2_HIGH("UL actual Throughput values %d DL Actual "
                    "Throughput Values %d",
                    average_ul_data_rate,
                    average_dl_data_rate);
}

/*===========================================================================
FUNCTION DS3GTPUTMGR_START_DL_TPUT_EST_TIMER

DESCRIPTION
  This function starts reporting timer for that subscriptions

  
PARAMETERS
  subs_id - Subscription

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/ 

void ds3gtputmgr_start_dl_tput_est_timer
(
   sys_modem_as_id_e_type subs_id
)
{
  uint32 timer_interval = 0;
  ds3gsubsmgr_subs_id_e_type ds3g_subs_id = DS3GSUBSMGR_SUBS_ID_INVALID;
  /*--------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return;
  }

  ds3g_subs_id = ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id);

  timer_interval = ds3gtputmgr_dl_est_reporting_status_info
                       [ds3g_subs_id].actual_interval;

  ds3g_timer_start_ex(DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION, 
                      timer_interval, ds3g_subs_id);

  DATA_3GMGR_MSG2(MSG_LEGACY_LOW,"DL estimated tput timer started for  CM sub id %d is with:  %d"
                    ,subs_id, timer_interval);

}/*ds3gtputmgr_start_dl_tput_est_timer*/


/*===========================================================================
FUNCTION DS3G_STOP_DL_TPUT_EST_TIMER

DESCRIPTION
  This function stops reporting timer for that subscriptions

  
PARAMETERS
  subs_id - Subscription

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds3g_stop_dl_tput_est_timer
(
   sys_modem_as_id_e_type subs_id
)
{
  /*--------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return;
  }


  ds3g_timer_stop_ex(DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION,
                  ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id));

  DATA_3GMGR_MSG1(MSG_LEGACY_HIGH,"DL estimated tput timer stopped for  CM sub id %d",
                    subs_id);
}/*ds3g_stop_dl_tput_est_timer*/


/*===========================================================================
FUNCTION DS3G_IS_DL_TPUT_EST_TIMER_RUNNING

DESCRIPTION
  This function checks if reporting timer is running for that subscriptions

  
PARAMETERS
  subs_id - Subscription

DEPENDENCIES
  None.

RETURN VALUE
  boolean - TRUE is timer is running, FALSE otherwise

SIDE EFFECTS
  None.
===========================================================================*/ 
boolean ds3g_is_dl_tput_est_timer_running
(
   sys_modem_as_id_e_type subs_id
)
{
  boolean ret_val = FALSE;
  /*--------------------------------------------------*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return ret_val;
  }

  ret_val = ds3g_timer_get_status_ex(DS3G_TIMER_DOWNLINK_THROUGHPUT_ESTIMATION,
                                  ds3gsubsmgr_subs_id_cm_to_ds3g(subs_id));

  DATA_3GMGR_MSG2(MSG_LEGACY_LOW,
                  "DL estimated tput timer for CM sub id %d is running:%d",
                    subs_id, ret_val);

  return ret_val;
} /* ds3g_is_dl_tput_est_timer_running() */

/*===========================================================================
FUNCTION DS3GTPUTMGR_CHECK_AND_UPDATE_DL_TPUT_REPORTING_TIMER

DESCRIPTION
  This function checks if reporting timer is needed to started or stoppedd based
  on technology and user preference

  
PARAMETERS
  subs_id - Subscription
  reporting_frequency - client reported reporting frequency
  need_to_start - whether reporting timer is actually needed or not

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/  
void ds3gtputmgr_check_and_update_dl_tput_reporting_timer
(
   sys_modem_as_id_e_type subs_id,
   uint32 reporting_interval,
   boolean need_to_report
)
{
   /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
   if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return;
  }

  if (need_to_report == TRUE) 
  {
     if (ds3g_is_dl_tput_est_timer_running(subs_id)) 
     {
        DATA_3GMGR_MSG1(MSG_LEGACY_LOW,"Report time already running for CM sub ID %d",subs_id);
     }

#ifdef FEATURE_DATA_LTE
     ds_eps_bearer_cntxt_reset_to_current_dl_byte_count(subs_id);
#endif
     ds3gtputmgr_start_dl_tput_est_timer(subs_id);
  }
  else if (ds3g_is_dl_tput_est_timer_running(subs_id))
  {
     ds3g_stop_dl_tput_est_timer(subs_id);
  }

}/*ds3gtputmgr_check_and_update_dl_tput_reporting_timer */


/*===========================================================================
FUNCTION DS3G_POST_DOWNLINK_THROUGHPUT_ESTIMATION_IND

DESCRIPTION
  This function posts the downlink throughput estimation to PS framework

  
PARAMETERS
  subs_id - Subscription
  ps_sys_dl_throughput_info_type - dl_tput_info

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds3g_post_downlink_throughput_estimation_ind
(
   ps_sys_dl_throughput_info_type dl_tput_info,
   sys_modem_as_id_e_type subs_id
)
{
  int16                             dss_errno = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return;
  }
  ps_sys_conf_set_ex(PS_SYS_TECH_ALL,
                      PS_SYS_CONF_DL_THROUGHPUT_INFO,
                      (ps_sys_subscription_enum_type)
                      ds3gsubsmgr_subs_id_cm_to_ds(subs_id),
                      &dl_tput_info, &dss_errno);
  return;
}


/*===========================================================================
FUNCTION ds3gtputmgr_process_downlink_throughput_estimation_ind

DESCRIPTION
  This function posts the downlink throughput estimation to PS framework

  
PARAMETERS
  sys_modem_as_id_e_type subs_id
  uint32 data_rate
  uint8 confidence_level
  boolean freeze_flag
  boolean moved_out

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/ 
void ds3gtputmgr_process_downlink_throughput_estimation_ind
(
   sys_modem_as_id_e_type subs_id,
   uint32 data_rate,
   uint8 confidence_level,
   boolean freeze_flag,
   boolean moved_out
)
{
  ps_sys_dl_throughput_info_type dl_tput_info;
  memset(&dl_tput_info,0,sizeof(ps_sys_dl_throughput_info_type));
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (!ds3gsubsmgr_is_subs_id_valid(subs_id))
  {
    DATA_3GMGR_MSG1(MSG_LEGACY_ERROR,"Invalid CM sub ID %d",subs_id);
    return;
  }

  dl_tput_info.downlink_allowed_rate = data_rate;
  dl_tput_info.confidence_level = confidence_level;
  dl_tput_info.is_suspended = moved_out;


  ds3g_post_downlink_throughput_estimation_ind(dl_tput_info, subs_id);
}



