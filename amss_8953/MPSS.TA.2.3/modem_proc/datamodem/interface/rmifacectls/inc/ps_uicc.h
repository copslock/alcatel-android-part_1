#ifndef _PS_UICC_H
#define _PS_UICC_H
/*===========================================================================

                              P S _ U I C C . H
                   
DESCRIPTION
  The header file for UICC external/common declarations.

EXTERNALIZED FUNCTIONS
  
  ps_uicc_powerup_init()
    This function performs power-up initialization for the UICC sub-system

  ps_uicc_sio_cleanup()
    This function is used to close the SIO stream

Copyright (c) 2008-2009 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/data.mpss/3.4.3.1/interface/rmifacectls/inc/ps_uicc.h#1 $
  $Author: pwbldsvc $ $DateTime: 2016/02/19 14:49:57 $
===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#endif /* _PS_UICC_H */
