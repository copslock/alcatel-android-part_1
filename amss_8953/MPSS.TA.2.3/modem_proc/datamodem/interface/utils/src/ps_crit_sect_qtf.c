/*==========================================================================*/
/*!
  @file 
  ps_crit_sect.c

  @brief
  This file provides QTF specific critical section implementation.

  Copyright (c) 2009 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
*/
/*==========================================================================*/
/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/interface/utils/src/ps_crit_sect_qtf.c#1 $
  $DateTime: 2016/02/19 14:49:57 $$Author: pwbldsvc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2009-12-03 hm  Created module.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "datamodem_variation.h"
#include "comdef.h"
#include "ps_crit_sect.h"
#include "ps_system_heap.h"
#include "amssassert.h"

#if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE)
#error code not present
#endif


/*===========================================================================

                          PUBLIC FUNCTION DEFINITIONS

===========================================================================*/
#if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE)
#error code not present
#endif /* if defined(TEST_FRAMEWORK) && defined(FEATURE_QUBE) */


