/*===========================================================================
  FILE: ds_Sock_RecvECNInfo.cpp

  OVERVIEW: This file provides implementation of the RecvECNInfo class.

  DEPENDENCIES: None

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/interface/dssock/src/ds_Sock_RecvECNInfo.cpp#1 $
  $DateTime: 2016/02/19 14:49:57 $$Author: pwbldsvc $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------
  2015-03-02 ad Created module

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "ds_Utils_StdErr.h"

#include "ds_Sock_RecvECNInfo.h"
#include "ds_Utils_DebugMsg.h"
#include "ps_system_heap.h"


using namespace ds::Sock;
using namespace ds::Error;


/*===========================================================================

                     PUBLIC MEMBER FUNCTIONS

===========================================================================*/
void * RecvECNInfo::operator new
(
  unsigned int numBytes
)
throw()
{

  void * buf = NULL;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  PS_SYSTEM_HEAP_MEM_ALLOC(buf, numBytes, void *);
  return buf; 
   
} /* RecvECNInfo::operator new() */

void RecvECNInfo::operator delete
(
  void *  bufPtr
)
throw()
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  if (0 == bufPtr)
  {
    LOG_MSG_ERROR_0("delete(): "
                      "NULL ptr");
    ASSERT( 0);
    return;
  }
  
  PS_SYSTEM_HEAP_MEM_FREE(bufPtr);
  return;
} /* RecvECNInfo::operator delete() */


RecvECNInfo::RecvECNInfo
(
  unsigned char  _recvECNHandle
) :refCnt(1),recvECNHandle( _recvECNHandle)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return;

} /* RecvECNInfo::RecvECNInfo() */


ds::ErrorType CDECL RecvECNInfo::GetAncID
(
  ds::Sock::AncDataIDType *  ancIDPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (0 == ancIDPtr)
  {
    LOG_MSG_INVALID_INPUT_0("RecvECNInfo::GetAncID(): "
                            "NULL arg");
    return QDS_EFAULT;
  }

  *ancIDPtr = AncData::RECV_ECN_INFO;
  return AEE_SUCCESS;

} /* RecvECNInfo::GetAncID() */


ds::ErrorType CDECL RecvECNInfo::SetAncID
(
  ds::Sock::AncDataIDType  ancID
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return AEE_SUCCESS;

} /* RecvECNInfo::SetAncID() */


ds::ErrorType CDECL RecvECNInfo::GetRecvECNHandle
(
  unsigned char *  recvECNHandlePtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_1("RecvECNInfo::GetRecvIFHandle(): "
                           "Obj 0x%x", this);

  if (0 == recvECNHandlePtr)
  {
    LOG_MSG_INVALID_INPUT_1("RecvECNInfo::GetRecvIFHandle(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  *recvECNHandlePtr = recvECNHandle;

  LOG_MSG_FUNCTION_EXIT_1("RecvECNInfo::GetRecvIFHandle(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* RecvECNInfo::GetRecvIFHandle() */


ds::ErrorType CDECL RecvECNInfo::QueryInterface
(
  AEEIID   iid,
  void **  objPtrPtr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_FUNCTION_ENTRY_2("RecvECNInfo::QueryInterface(): "
                           "Obj 0x%x iid %d", this, iid);

  if (0 == objPtrPtr)
  {
    LOG_MSG_INVALID_INPUT_1("RecvECNInfo::QueryInterface(): "
                            "NULL arg, obj 0x%x", this);
    return QDS_EFAULT;
  }

  switch (iid)
  {
    case AEEIID_IAncDataPriv:
    {
      *objPtrPtr = static_cast <IAncDataPriv *> ( this);
      (void) AddRef();
      break;
    }

    case AEEIID_IRecvECNInfoPriv:
    {
      *objPtrPtr = this;
      (void) AddRef();
      break;
    }

    case AEEIID_IQI:
    {
      *objPtrPtr = static_cast <IQI *> ( this);
      (void) AddRef();
      break;
    }

    default:
    {
      *objPtrPtr = 0;
      LOG_MSG_INVALID_INPUT_2("RecvECNInfo::QueryInterface(): "
                              "Unknown iid %d, obj 0x%x", iid, this);
      return AEE_ECLASSNOTSUPPORT;
    }
  } /* switch */

  LOG_MSG_FUNCTION_EXIT_1("RecvECNInfo::QueryInterface(): "
                          "Success, obj 0x%x", this);
  return AEE_SUCCESS;

} /* RecvECNInfo::QueryInterface() */

