/*===========================================================================

                    DS_QMI_OTDT_STUBS.C

DESCRIPTION
  Stubs for DS QMI ON TARGET DATA TEST, in case that this framework is not 
  enabled.

EXTERNALIZED FUNCTIONS

  
Copyright (c) 2012 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

EDIT HISTORY FOR FILE

$Header: //components/rel/data.mpss/3.4.3.1/interface/qmidata/src/ds_qmi_otdt_stubs.c#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/02/12    aa     Created module
===========================================================================*/

/*===========================================================================

                         INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"


/*===========================================================================

                    EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/
int ds_qmi_otdt_init
(
  void
)
{
  return 0;
}

