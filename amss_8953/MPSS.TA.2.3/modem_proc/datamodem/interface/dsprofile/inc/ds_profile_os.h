/******************************************************************************
  @file    ds_profile_os.h
  @brief   Operating System specific header

  DESCRIPTION
  This header defines API for OS specific logging, locking mechanisms. 

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (C) 2009-2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. 
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/interface/dsprofile/inc/ds_profile_os.h#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/09   mg      Created the module. First version of the file.
===========================================================================*/

#ifndef DS_PROFILE_OS_H
#define DS_PROFILE_OS_H

#include "datamodem_variation.h"

  #include "customer.h"
  #include "ds_profile_os_amss.h"

#endif /* DS_PROFILE_OS_H */
