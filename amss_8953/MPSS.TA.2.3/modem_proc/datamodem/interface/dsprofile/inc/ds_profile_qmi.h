/******************************************************************************
  @file    ds_profile_qmi.h
  @brief   DS PROFILE - QMI related code, which is not technology specific

  DESCRIPTION
      DS PROFILE - QMI related code, which is not technology specific

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  N/A

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
  ---------------------------------------------------------------------------
******************************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/interface/dsprofile/inc/ds_profile_qmi.h#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/14/12   gs      First version 
===========================================================================*/
#ifndef DS_PROFILE_QMI_QNX_H
#define DS_PROFILE_QMI_QNX_H


#endif /* DS_PROFILE_QMI_QNX_H_ */

