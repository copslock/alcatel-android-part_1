/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                             P S _ D P M . I P A C

GENERAL DESCRIPTION
  This is the implementation of the Data Path Manager module

Copyright (c) 2014 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/data.mpss/3.4.3.1/interface/dpm/src/ps_dpm_ipa.c#4 $
  $Author: pwbldsvc $ $DateTime: 2016/06/10 03:07:10 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
===========================================================================*/
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                                INCLUDE FILES

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

#include "ps_dpm_hw.h"
#include "ps_dpm_opt_defs.h"
#include "ps_rm_defs.h"
#include "ps_rm_svc.h"
#include "ps_logging_diag.h"
#include "ps_logging.h"
#include "log.h"   /*For log_status() */
#include "ps_dpm.h"

#include "ipa.h"
#include "ipa_ipfltr.h"

#include "ds_qmi_qos.h"
#include "ds_qmi_wds.h"

/*===========================================================================

                           DEFINES

===========================================================================*/



/*===========================================================================

                           STATIC DATA DECLARATIONS

===========================================================================*/

typedef ipa_acc_dpl_cb_fn_param_s  ps_dpm_hw_dpl_cb_fn_param_type;

/*-----------------------------------------------------------------------
  Indicates if hardware dpl logging is enabled
-----------------------------------------------------------------------*/
boolean ps_dpm_hw_dpl_enabled = FALSE;

/*-----------------------------------------------------------------------
  Indicates if hardware dpl logging is available. It doesnt mean that
  it has been enabled. DPM is master and it controls whether to enable
  or disable logging.
-----------------------------------------------------------------------*/
boolean ps_dpm_hw_dpl_available = FALSE;

/*===========================================================================

                           STATIC DATA DECLARATIONS

===========================================================================*/

void ps_dpmi_hw_wait_for_ev_cback
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  (void) ps_rm_wait((rex_sigs_type) (1 << PS_RM_HW_EV_CBACK_SIGNAL));

  return;
} /* ps_dpmi_hw_wait_for_ev_cback() */

/*===========================================================================

                             INTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================
FUNCTION   ps_dpm_hw_dpl_cb()

DESCRIPTION
  This function is registered with hardware at powerup and is called by
  hardware to indicate that dpl on hardware is enabling.

DEPENDENCIES
  None.

PARAMETERS
  cb_param : Callback parameters

RETURN VALUE
  None

SIDE EFFECTS
  None.
===========================================================================*/
static void ps_dpm_hw_dpl_cb
(
  ps_dpm_hw_dpl_cb_fn_param_type * cb_param
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

   LOG_MSG_INFO1_1("ps_dpm_hw_dpl_cb : Hardware logging is available %d",
                    cb_param->is_available);

  /*-----------------------------------------------------------------------
     if hardware indicates that dpl is available then set 
     ps_dpm_hw_dpl_available to TRUE. DPL logging would available when
     log codes are enabled in qxdm
  -----------------------------------------------------------------------*/

  if (TRUE == cb_param->is_available)
  {
    ps_dpm_hw_dpl_available = TRUE;


    if (TRUE == log_status( LOG_DATA_PROTOCOL_LOGGING_NETWORK_IP_RM_TX_80_BYTES_C) ||
        TRUE == log_status( LOG_DATA_PROTOCOL_LOGGING_NETWORK_IP_RM_RX_80_BYTES_C) ||
        TRUE == log_status( LOG_DATA_PROTOCOL_LOGGING_NETWORK_IP_UM_TX_80_BYTES_C) ||
        TRUE == log_status( LOG_DATA_PROTOCOL_LOGGING_NETWORK_IP_UM_RX_80_BYTES_C))
    {    
      ps_dpm_hw_control_dpl_logging(DPL_PACKET_PARTIAL_LENGTH);
    }
  }
  else
  {
    /*-----------------------------------------------------------------------
    Indicates that hardware dpl logging is unavailable. 
    -----------------------------------------------------------------------*/
    ps_dpm_hw_dpl_available = FALSE;
    ps_dpm_hw_dpl_enabled   = FALSE;
  }
  
} /* ps_dpm_hw_dpl_cb */

static ipa_wan_bearer_tech_e ps_dpmi_get_ipa_tech
(
  ps_sys_rat_ex_enum_type    sys_rat
)
{
  ipa_wan_bearer_tech_e ipa_bearer_tech;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  switch(sys_rat)
  {
    case PS_SYS_RAT_EX_3GPP_LTE:
      ipa_bearer_tech =  IPA_WAN_BEARER_TECH_LTE;
      break;

    case PS_SYS_RAT_EX_3GPP_WCDMA:
      ipa_bearer_tech = IPA_WAN_BEARER_TECH_UMTS;
      break;

    default:
      ipa_bearer_tech = IPA_WAN_BEARER_TECH_OTHER;
      break;
  }
  return ipa_bearer_tech;
}

static void ps_dpmi_ipa_ev_cback
(
  ipa_wan_ds_cb_event_e  ipa_event_name,
  uint8                  uid
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_2("ps_dpmi_ipa_ev_cback(): uid %d event %d",
                  uid, ipa_event_name);

  switch (ipa_event_name)
  {
    case IPA_WAN_DS_CB_EVENT_BEARER_DEREGISTERED:
    case IPA_WAN_DS_CB_EVENT_BEARER_SUSPENDED:
    case IPA_WAN_DS_CB_EVENT_BEARER_ACTIVATED:
    {
      PS_RM_SET_SIGNAL(PS_RM_HW_EV_CBACK_SIGNAL);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_dpmi_ipa_ev_cback(): Unknown event");
      break;
    }
  }

  return;
} /* ps_dpmi_ipa_ev_cback() */

#ifdef FEATURE_DATA_PS_464XLAT
static void ps_dpmi_ipa_clat_ev_cback
(
  ipa_clat_cfg_cb_evt_e  ipa_event_name,
  ipa_clat_handle_t      ipa_clat_handle,
  void                 * cb_data
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  LOG_MSG_INFO1_2("ps_dpmi_ipa_clat_ev_cback(): ipa clat hndl %d event %d",
                  ipa_clat_handle, ipa_event_name);

  switch (ipa_event_name)
  {
    case IPA_CLAT_CFG_CB_EVT_REGISTERED:
    case IPA_CLAT_CFG_CB_EVT_DEREGISTERED:
    case IPA_CLAT_CFG_CB_EVT_RECONFIGURED:
    case IPA_CLAT_CFG_CB_EVT_SIO_ASSOCIATED:
    {
      PS_RM_SET_SIGNAL(PS_RM_HW_EV_CBACK_SIGNAL);
      break;
    }

    default:
    {
      LOG_MSG_ERROR_0("ps_dpmi_ipa_clat_ev_cback(): Unknown event");
      break;
    }
  }

  return;
} /* ps_dpmi_ipa_clat_ev_cback() */
#endif /* FEATURE_DATA_PS_464XLAT */

static void ps_dpmi_ipa_ul_stats_cback
(
  uint32                   subs_id,
  uint8                    uid,
  ipa_wan_ds_ul_stats_s  * stats_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if (stats_ptr == NULL)
  {
    return;
  }

  qmi_qos_get_fc_stats(
            &stats_ptr->num_qos_flow_enable,
            &stats_ptr->num_qos_flow_disable);

  qmi_wds_get_fc_stats(
            &stats_ptr->num_wds_flow_enable,
            &stats_ptr->num_wds_flow_disable);

} /* ps_dpmi_ipa_ul_stats_cback() */

 
/*===========================================================================

                             EXTERNAL FUNCTIONS

===========================================================================*/
int ps_dpm_hw_get_iface_stat_adjustment
(
  uint8                            uid,
  ps_iface_type                  * iface_ptr,  
  ps_dpm_hw_stats_info_type      * stats_ptr
)
{ 
  ipa_err_code_e             ipa_err_code;
  ipa_wan_bidir_stats_s      stats;
  int                        ret_val = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  LOG_MSG_INFO3_2("ps_dpm_hw_get_iface_stat_adjustment() : iface_ptr 0x%x:%d ", 
                  PS_IFACE_IS_VALID(iface_ptr)?iface_ptr->name:0,
                  PS_IFACE_IS_VALID(iface_ptr)?iface_ptr->instance:0);
  
  memset(stats_ptr, 0, sizeof(ps_dpm_hw_stats_info_type));
  
  do
  {
    ipa_err_code = ipa_wan_ds_get_bearer_stats(uid, &stats);   

    if (IPA_SUCCESS != ipa_err_code)
    {
      LOG_MSG_INFO3_2("ps_dpm_hw_get_iface_stat_adjustment(): "
                      "Couldn't get ipa stats uid %d  err %d",
                      uid, ipa_err_code);
      ret_val = -1;                
      break;
    }
                        
    if(PS_IFACE_IS_ADDR_FAMILY_V4(iface_ptr) == TRUE)
    {
      stats_ptr->dl_stats.num_ipv4_bytes += stats.dl_stats.num_ipv4_bytes;
      stats_ptr->dl_stats.num_ipv4_pkts += (uint32)stats.dl_stats.num_ipv4_pkts;
    }
    else
    {
      stats_ptr->dl_stats.num_ipv6_bytes += stats.dl_stats.num_ipv6_bytes;
      stats_ptr->dl_stats.num_ipv6_pkts += (uint32)stats.dl_stats.num_ipv6_pkts;
    }
   
    return ret_val;
  } while(0);  
  
  LOG_MSG_INFO3_0("ps_dpm_hw_get_iface_stat_adjustment() : Stat adj failed ");                 
  return ret_val;                  
} /* ps_dpm_hw_get_iface_stat_adjustment */


int ps_dpm_hw_reg_bearer
(
  uint8                      uid,
  dsm_watermark_type	   * l2_to_ps_wm_ptr,
  dsm_watermark_type       * hw_to_ps_wm_ptr,
  sio_stream_id_type         sio_stream_id,
  ps_sys_rat_ex_enum_type    rat  
)
{
#ifndef TEST_FRAMEWORK
  ipa_err_code_e      ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*-----------------------------------------------------------------------
    Register bearer with IPA
  -----------------------------------------------------------------------*/
  ipa_err_code = ipa_wan_ds_register_bearer
                 (
                   uid,
                   sio_stream_id,
                   l2_to_ps_wm_ptr,
                   hw_to_ps_wm_ptr,
                   NULL,
                   ps_dpmi_get_ipa_tech(rat)
                 );

  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_hw_reg_bearer(): "
                    "Couldn't reg bearer with IPA, err %d", ipa_err_code);
    return -1;
  }
  
#endif /* TEST_FRAMEWORK */  
  return 0;
} /* ps_dpm_hw_reg_bearer */

int ps_dpm_hw_dereg_bearer
(
  uint8                     uid,
  sio_port_id_type          sio_port_id,
  boolean                   is_data_path_bridged
)
{ 
/* Parameters sio_port_id,is_data_path_bridged are not used in case of IPA */
#ifndef TEST_FRAMEWORK
  ipa_err_code_e   ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ipa_err_code = ipa_wan_ds_deregister_bearer(uid);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_hw_dereg_bearer(): Couldn't dereg bearer "
                    "with IPA, err %d", ipa_err_code);
    ASSERT(0);
    return -1;
  }
  
  LOG_MSG_INFO1_0("ps_dpm_hw_dereg_bearer(): Waiting for ipa Dereg cb");
                  
  ps_dpmi_hw_wait_for_ev_cback();

#endif /* TEST_FRAMEWORK */    
  LOG_MSG_INFO1_1("ps_dpm_hw_dereg_bearer(): De-registered bearer uid %d",
                  uid);  
  
  return 0;
} /* ps_dpm_hw_dereg_bearer() */

int16 ps_dpm_hw_bridge_bearer
(
  uint8                            uid,
  sio_port_id_type                 sio_port_id  
)
{
  int16                  ret_val = -1;    
#ifndef TEST_FRAMEWORK  
  ipa_err_code_e         ipa_err_code;
#endif /* TEST_FRAMEWORK */  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  do
  {
#ifndef TEST_FRAMEWORK
    /*-----------------------------------------------------------------------
      Bridge bearer with IPA
    -----------------------------------------------------------------------*/
    ipa_err_code = ipa_wan_ds_config_dl_bridge(uid, IPA_DL_BRIDGE_STATE_BRIDGE);
    if (IPA_SUCCESS != ipa_err_code)
    {
      LOG_MSG_ERROR_1("ps_dpm_hw_bridge_bearer(): "
                      "Couldn't bridge bearer with IPA, err%d", ipa_err_code);
      break;
    }
    
#endif /* TEST_FRAMEWORK */  
    ret_val = 0;
  } while (0);

  return ret_val;
} /* ps_dpm_hw_bridge_bearer() */


int16 ps_dpm_hw_unbridge_bearer
(
  uint8                            uid,
  sio_port_id_type                 sio_port_id 
)
{  
#ifndef TEST_FRAMEWORK  
  ipa_err_code_e  ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ipa_err_code =
    ipa_wan_ds_config_dl_bridge(uid, IPA_DL_BRIDGE_STATE_UNBRIDGE);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_2("ps_dpm_hw_unbridge_bearer(): Couldn't unbridge "
                    "uid %d with IPA, err %d",
                    uid, ipa_err_code);
    ASSERT(0);
    return -1;
  }
#endif /* TEST_FRAMEWORK */                
  return 0;
} /* ps_dpm_hw_unbridge_bearer() */


int ps_dpm_hw_config_dpl
(
  uint8                      uid,
  uint16                     dpl_pkt_len_v4,
  uint16                     dpl_pkt_len_v6,
  uint32                     dpm_um_handle,
  ps_iface_type            * iface_ptr
)
{
#ifndef TEST_FRAMEWORK 
  uint16                dpl_pkt_len;
  ipa_err_code_e        ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ( dpl_pkt_len_v6 < dpl_pkt_len_v4)
  {
    dpl_pkt_len = dpl_pkt_len_v4;
  }
  else
  {
    dpl_pkt_len = dpl_pkt_len_v6;
  }    

  if (0 != dpl_pkt_len)
  {
    ipa_err_code =
      ipa_wan_ds_config_dpl(uid, 
                            iface_ptr->dpl_net_cb.recv_dpl_id.ifname, 
                            dpl_pkt_len, 
                            dpm_um_handle);

    if (IPA_SUCCESS != ipa_err_code)
    {
      LOG_MSG_ERROR_2("ps_dpm_hw_config_dpl(): "
                      "Couldn't config DPL on uid %d with IPA, err %d",
                      uid, ipa_err_code);
      return -1;
    }
  }
  else
  {
    LOG_MSG_INFO2_1("ps_dpmi_config_dpl_with_hw(): "
                    "Not configuring DPL with IPA on uid %d, as DPL is not "
                    "enabled", uid);
  }
#endif /* TEST_FRAMEWORK */
  return 0;
} /* ps_dpm_hw_config_dpl() */

void ps_dpm_hw_dereg_dpl
(
  uint8                      uid
)
{
#ifndef TEST_FRAMEWORK
  ipa_err_code_e        ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
	 
  ipa_err_code =
    ipa_wan_ds_config_dpl(uid, 0xDE, 0, 0);
	 
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_2("ps_dpmi_dereg_dpl_with_ipa(): "
                    "Couldn't dereg DPL on uid %d with IPA, err %d",
                    uid, ipa_err_code);                    
  }
#endif /* TEST_FRAMEWORK */  
} /* ps_dpm_hw_dereg_dpl() */

int ps_dpm_hw_suspend_bearer
(
  uint8   uid
)
{
  ipa_err_code_e  ipa_err_code;
  ipa_wan_bearer_proc_state_e  bearer_state;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ipa_err_code = ipa_wan_ds_get_bearer_proc_state(uid, &bearer_state);

  if (IPA_SUCCESS == ipa_err_code &&
      IPA_WAN_BEARER_PROC_STATE_SUSPEND != bearer_state)
  {
    ipa_err_code =
      ipa_wan_ds_set_bearer_proc_state(uid, IPA_WAN_BEARER_PROC_STATE_SUSPEND);
    if (IPA_SUCCESS != ipa_err_code)
    {
      LOG_MSG_ERROR_2("ps_dpm_hw_suspend_bearer(): Couldn't suspend "
                      "bearer with uid %d with IPA, err %d",
                       uid, ipa_err_code);
      ASSERT(0);
      return -1;
    }
  
#ifndef TEST_FRAMEWORK
   ps_dpmi_hw_wait_for_ev_cback();
#endif
 }
  
  LOG_MSG_INFO1_3("ps_dpm_hw_suspend_bearer(): Suspend bearer with uid %d"
                  "ipa previous bearer state 0x%x, ipa_err_code %d",
                   uid, bearer_state, ipa_err_code);
                 
  return 0;
} /* ps_dpm_hw_suspend_bearer() */

void ps_dpm_hw_reg_event_cb
(
  void
)
{
  ipa_err_code_e  ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ipa_err_code = ipa_wan_ds_register_event_cb(ps_dpmi_ipa_ev_cback);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_init(): Couldn't register func with IPA, err %d",
                    ipa_err_code);
    ASSERT(0);
  }

  ipa_err_code = ipa_wan_register_ds_ul_stats_cb_fn(ps_dpmi_ipa_ul_stats_cback);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("Couldn't register UL stats func with IPA, err %d",
                    ipa_err_code);
  }

#ifdef FEATURE_DATA_PS_464XLAT
  ipa_err_code = ipa_clat_register_evt_cb(ps_dpmi_ipa_clat_ev_cback, NULL);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_init(): Couldn't register CLAT func with IPA, err %d",
				    ipa_err_code);
    ASSERT(0);
  }
#endif /* FEATURE_DATA_PS_464XLAT */
} /* ps_dpm_hw_reg_event_cb() */

void ps_dpm_hw_activate_bearer
(
  uint8   uid
)
{
  ipa_err_code_e  ipa_err_code;
  ipa_wan_bearer_proc_state_e  bearer_state;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ipa_err_code = ipa_wan_ds_get_bearer_proc_state(uid, &bearer_state);

  if (IPA_SUCCESS == ipa_err_code &&
      IPA_WAN_BEARER_PROC_STATE_ACTIVE != bearer_state)
  {
  ipa_err_code =
    ipa_wan_ds_set_bearer_proc_state(uid, IPA_WAN_BEARER_PROC_STATE_ACTIVE);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_2("ps_dpm_hw_activate_bearer(): Couldn't activate "
                    "bearer with uid %d with IPA, err %d",
                     uid, ipa_err_code);
    ASSERT(0);
    return;
  }
#ifndef TEST_FRAMEWORK
  ps_dpmi_hw_wait_for_ev_cback();
#endif
  }
  
  LOG_MSG_INFO1_3("ps_dpm_hw_activate_bearer(): Activate bearer with uid %d"
                  "ipa previous bearer state 0x%x, ipa_err_code %d",
                   uid, bearer_state, ipa_err_code);

  return;
} /* ps_dpm_hw_activate_bearer() */

void ps_dpm_hw_dpl_powerup_init
(
  void
)
{
  ipa_err_code_e     ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*-----------------------------------------------------------------------
     Register callback with hardware. Hardware would indicate if dpl is
     available on hardware or not through the callback registered
  -----------------------------------------------------------------------*/
  ipa_err_code = ipa_acc_dpl_reg_cb (ps_dpm_hw_dpl_cb);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_hw_dpl_powerup_init(): "
                    "Couldn't reg cb dpl with IPA, err %d", ipa_err_code);
    ASSERT(0);                    
  }
} /* ps_dpm_hw_dpl_powerup_init */

void ps_dpm_hw_log_dpl_pkt
(
  ps_iface_type                * iface_ptr,
  dsm_item_type                * rx_pkt,
  ps_dpm_hw_dpl_pkt_direction    pkt_direction
)
{
  ipa_err_code_e            ipa_err_code;
  ipa_acc_dpl_dir_e         ipa_pkt_dir;
  ps_iface_type           * base_iface_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  
  base_iface_ptr = PS_IFACE_GET_BASE_IFACE(iface_ptr);
  if (NULL == base_iface_ptr)
  {
    return;
  }
  
  if (TRUE == ps_dpm_hw_dpl_enabled)
  {
    if (PS_DPM_HW_DPL_PKT_DIRECTION_UL == pkt_direction)
    {
      ipa_pkt_dir = IPA_ACC_DPL_DIR_UL;
    }
    else
    {
      ipa_pkt_dir = IPA_ACC_DPL_DIR_DL;
    }

    /*-----------------------------------------------------------------------
       Log the packet with hardware
    -----------------------------------------------------------------------*/
    ipa_err_code = ipa_acc_dpl_tx (ipa_pkt_dir,
                                   base_iface_ptr->dpl_net_cb.recv_dpl_id.ifname,
                                   (uint16) dsm_length_packet (rx_pkt),
                                   rx_pkt);
    if (IPA_SUCCESS != ipa_err_code)
    {
      LOG_MSG_ERROR_1("ps_dpm_hw_log_dpl_pkt(): "
                      "Couldn't reg dpl with IPA, err %d", ipa_err_code);
    } 
  }  
                     
} /* ps_dpm_hw_log_dpl_pkt */

void ps_dpm_hw_config_dpl_sio
(
  sio_stream_id_type  sio_stream_id,
  uint8               ifname,
  uint16              num_bytes
)
{
  ipa_err_code_e            ipa_err_code;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ipa_err_code = ipa_sio_config_dpl (sio_stream_id, ifname, num_bytes);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_hw_log_dpl_pkt(): "
                    "Couldn't reg dpl sio stream id with IPA, err %d", 
                    ipa_err_code);
  }  
} /* ps_dpm_hw_config_dpl_sio */

void ps_dpm_hw_control_dpl_logging
(
  uint32         pkt_len
)
{
  ipa_err_code_e        ipa_err_code;
  ipa_acc_dpl_state_e   ipa_dpl_state = IPA_ACC_DPL_STATE_DISABLE;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
   LOG_MSG_INFO1_1(" ps_dpm_hw_control_dpl_logging pkt_len %d", pkt_len);

  /*-----------------------------------------------------------------------
    Change in qxdm log code indicates whether to enable or disable dpl
  -----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------
    1. Check if ipa already registered
    2. Check if its registered then check if log codes suggest only
       partial length if so enable hardware logging else disable hardware
       logging
  -----------------------------------------------------------------------*/   
  if (TRUE == ps_dpm_hw_dpl_available)
  {
    if (DPL_PACKET_PARTIAL_LENGTH == pkt_len)
    {
      ipa_dpl_state = IPA_ACC_DPL_STATE_ENABLE;
      ps_dpm_hw_dpl_enabled = TRUE;
    }
    else
    {
      ipa_dpl_state = IPA_ACC_DPL_STATE_DISABLE;
      ps_dpm_hw_dpl_enabled = FALSE;    
    }
  }

  ipa_err_code = ipa_acc_dpl_configure (ipa_dpl_state);
  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_2("ps_dpm_hw_control_dpl_logging(): "
                    "Couldn't reg/dereg dpl with IPA, err %d state %d", 
                     ipa_err_code, ipa_dpl_state);
    ASSERT(0);                    
  }

  /*-----------------------------------------------------------------------
    Go through all dpm rm info and dpm um info bearers and enable or disable
    logging
  -----------------------------------------------------------------------*/
  ps_dpm_control_dpl_hw_logging(pkt_len);

} /* ps_dpm_hw_control_dpl_logging */

/*===========================================================================
FUNCTION   PS_DPM_HW_REG_DPL_WMK

DESCRIPTION
  This function is used to register the DPM DPL WM with IPA

DEPENDENCIES
  None.

PARAMETERS
  ps_dpm_dpl_wmk_ptr - Pointer to ps_dpm_dpl_wmk

RETURN VALUE
  None
===========================================================================*/
void ps_dpm_hw_reg_dpl_wmk
(
  dsm_watermark_type *dpl_wmk_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ipa_wan_ds_register_dpl_wm (dpl_wmk_ptr);
} /* ps_dpm_hw_reg_dpl_wmk() */

/*===========================================================================
FUNCTION   PS_DPM_HW_DPL_WMK_SIG_HANDLER()

DESCRIPTION
  Signal handler for PS_DPM_DPL_SIGNAL

DEPENDENCIES
  None.

PARAMETERS
  sig: the Signal that is handled
  user_data_ptr: NOT USED

RETURN VALUE
  TRUE: no more processing to be done
  FALSE: Needs to be called again.

SIDE EFFECTS
  None.
===========================================================================*/
boolean ps_dpm_hw_dpl_sig_handler
(
  ps_sig_enum_type    sig,
  void              * user_data_ptr
)
{
   dsm_item_type         * item_ptr = NULL;
   ps_dpm_um_info_type   * dpm_um_info_ptr = NULL;
   ps_iface_type         * iface_ptr = NULL;
   uint8                   version;
   dsm_watermark_type     *dpl_wmk_ptr = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

   dpl_wmk_ptr = ps_dpmi_dpl_get_wmk();
   item_ptr = dsm_dequeue( dpl_wmk_ptr );
   if ( item_ptr == NULL )
   {
     return TRUE;
   }

   dpm_um_info_ptr = ps_dpmi_get_dpm_um_info_by_handle
                     (
                       (int32)item_ptr->app_field
                     );
   if (NULL == dpm_um_info_ptr)
   {
     LOG_MSG_INFO1_1 ("ps_dpm_dpl_sig_handler(): Invalid UM DPM info handle %d", 
                      (int32)item_ptr->app_field);
     dsm_free_packet( &item_ptr);
     return FALSE;   
   }
   
   if (!dsm_peek_byte(item_ptr, 0, &version))
   {
     LOG_MSG_INFO1_0 ("ps_dpm_dpl_sig_handler(): Couldnt retrieve version of ip pkt");
     dsm_free_packet( &item_ptr);
     return FALSE;      
   }
   
   if (PS_DPM_OPT_V4_VERSION == (version & PS_DPM_OPT_IP_VERSION_MASK))
   {
     iface_ptr = dpm_um_info_ptr->v4_iface_ptr;
   }
   else if (PS_DPM_OPT_V6_VERSION == (version & PS_DPM_OPT_IP_VERSION_MASK))
   {
     iface_ptr = dpm_um_info_ptr->v6_iface_ptr;
   }
   
   if ( !PS_IFACE_IS_VALID( iface_ptr) )
   {
     LOG_MSG_INFO1_0 ("ps_dpm_dpl_sig_handler(): Invalid iface in dsm item");
     dsm_free_packet( &item_ptr);
     return FALSE;
   }

   PS_DPM_GLOBAL_STATS_INC(iface_ptr);
   
   DPL_LOG_NETWORK_RX_PACKET(iface_ptr, item_ptr, DPL_IID_NETPROT_IP);

   dsm_free_packet( &item_ptr);
   return FALSE;
} /* ps_dpm_hw_dpl_sig_handler() */

boolean ps_dpm_hw_pkt_info_is_filter_result_set
(
  dsm_item_type           * dsm_item_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return 
    (boolean)((ps_dpm_dsm_app_field_info_type *)(&((dsm_item_ptr)->app_field)))->is_filter_result_valid;
} /* ps_dpm_hw_pkt_info_is_filter_result_set */

uint16 ps_dpm_hw_pkt_info_get_filter_result
(
  dsm_item_type           * dsm_item_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return 
    (uint16)((ps_dpm_dsm_app_field_info_type *)(&((dsm_item_ptr)->app_field)))->filter_result;
} /* ps_dpm_hw_pkt_info_get_filter_result */

uint8 ps_dpm_hw_pkt_info_get_version
(
  dsm_item_type           * dsm_item_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return 
    (uint8)((ps_dpm_dsm_app_field_info_type *)(&((dsm_item_ptr)->app_field)))->ip_version;
} /* ps_dpm_hw_pkt_info_get_version */

boolean ps_dpm_hw_pkt_is_version_ipv4
(
  dsm_item_type           * dsm_item_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return (IPA_IP_TYPE_IPV4 == PS_DPM_HW_PKT_INFO_GET_VERSION(dsm_item_ptr));
} /* ps_dpm_hw_pkt_is_version_ipv4 */

boolean ps_dpm_hw_pkt_is_version_ipv6
(
  dsm_item_type           * dsm_item_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return (IPA_IP_TYPE_IPV6 == PS_DPM_HW_PKT_INFO_GET_VERSION(dsm_item_ptr));
} /* ps_dpm_hw_pkt_is_version_ipv6 */

void ps_dpm_hw_offload_checksum
(
  void
)
{
  ipa_err_code_e      ipa_err_code = IPA_SUCCESS;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
#ifndef TEST_FRAMEWORK
  ipa_err_code = ipa_offload_checksum();

  if (IPA_SUCCESS != ipa_err_code)
  {
    LOG_MSG_ERROR_1("ps_dpm_hw_offload_checksum() : Checksum offload"
                    " fail ipa err %d", ipa_err_code);    
    ASSERT(0);
  }
  
  return;
#endif /* TEST_FRAMEWORK */
} /* ps_dpm_hw_offload_checksum() */

#ifdef FEATURE_DATA_PS_464XLAT
static ipa_clat_prefix_len_e ps_dpm_hw_get_ipa_prefix_len
(
  uint8   prefix_len
)
{
   ipa_clat_prefix_len_e ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_MAX;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  switch(prefix_len)
  {
    case 96:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_96;
      break;
    
    case 64:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_64;
      break;
      
    case 56:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_56;
      break;

    case 48:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_48;
      break;
      
    case 40:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_40;
      
    case 32:
      ipa_clat_prefix_len = IPA_CLAT_V6_PREFIX_LEN_32;
      break; 

    default:      
      break;    
  }
  
  return ipa_clat_prefix_len;
} /* ps_dpm_hw_get_ipa_prefix_len */

int ps_dpm_hw_clat_reg_pdn_context
(
  ps_dpm_clat_reg_pdn_cntxt_info_type       * clat_reg_info_ptr,
  ps_dpm_clat_hw_handle_type                * clat_hw_handle
)
{
  ipa_err_code_e                        ipa_err_code = IPA_SUCCESS;
  ipa_clat_pdn_cntxt_info_type          ipa_clat_pdn_cntxt;
  ps_dpm_error_enum_type                dpm_error_info = PS_DPM_SUCCESS;
  ipa_clat_handle_t                     ipa_clat_handle; 
  ps_iface_type                       * um_base_iface_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {  
    /* check if CLAT is supported */
    if(ipa_query_capability() & IPA_CAPABILITY_CLAT_DL)
    {   

      if (NULL == clat_reg_info_ptr->iface_ptr)
      {
        dpm_error_info = PS_DPM_ERROR_ARG_INVALID;
        break;
      }
  
      um_base_iface_ptr = PS_DPM_GET_BASE_IFACE(clat_reg_info_ptr->iface_ptr);
      if (!PS_IFACE_IS_VALID(um_base_iface_ptr))
      {
        dpm_error_info = PS_DPM_ERROR_IFACE_INVALID;
        break;
      }

      /* Register with hardware only for umts iface */
      if (UMTS_IFACE != PS_IFACE_GET_NAME(um_base_iface_ptr))
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
        break;
      }
      /* copy the dpm clat context to ipa clat context */
      ipa_clat_pdn_cntxt.v4_src_addr = 
        clat_reg_info_ptr->v4_client_addr;
      ipa_clat_pdn_cntxt.v6_dev_prefix_len = 
        ps_dpm_hw_get_ipa_prefix_len(clat_reg_info_ptr->v6_dev_prefix_len);
      ipa_clat_pdn_cntxt.v6_plat_prefix_len = 
        ps_dpm_hw_get_ipa_prefix_len(clat_reg_info_ptr->v6_plat_prefix_len);      
      ipa_clat_pdn_cntxt.dev_addr_match_bits =
        clat_reg_info_ptr->v6_dev_addr_prefix_len;
      ipa_clat_pdn_cntxt.mtu = PS_IFACE_GET_MTU(clat_reg_info_ptr->iface_ptr);
      
      memscpy(&(ipa_clat_pdn_cntxt.v6_dev_prefix),
              sizeof(struct ps_in6_addr),
              &(clat_reg_info_ptr->v6_dev_addr),
              sizeof(struct ps_in6_addr));
              
      memscpy(&(ipa_clat_pdn_cntxt.v6_plat_prefix),
              sizeof(struct ps_in6_addr),
              &(clat_reg_info_ptr->v6_plat_prefix),
              sizeof(struct ps_in6_addr)); 

      ipa_err_code = ipa_clat_register(&ipa_clat_pdn_cntxt,
                                       &ipa_clat_handle);
      if (IPA_SUCCESS != ipa_err_code)
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_FAILED;    
        ASSERT(0);
        break;
      }                

      *clat_hw_handle = (ps_dpm_clat_hw_handle_type)ipa_clat_handle;    
      ps_dpmi_hw_wait_for_ev_cback();
    }
    else
    {
      dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
      break;
    }
    
    return 0;
  } while(0);
 
  LOG_MSG_ERROR_2("ps_dpm_hw_clat_reg_pdn_context(): Failed registering "
                  "clat context with hardware err %d and ipa err %d",
                   dpm_error_info, ipa_err_code);    
  return -1;
}/* ps_dpm_hw_clat_reg_pdn_context */

int ps_dpm_hw_clat_reg_global_context
(
  ps_dpm_clat_global_cntxt_info_type   * clat_global_cntxt_info
)
{
  ipa_err_code_e                         ipa_err_code = IPA_SUCCESS;
  ipa_clat_global_cntxt_info_type        ipa_global_cntxt;
  ps_dpm_error_enum_type                 dpm_error_info = PS_DPM_SUCCESS;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {  
    /* check if CLAT is supported */
    if(ipa_query_capability() & IPA_CAPABILITY_CLAT_DL)
    { 
      /*copy the dpm global context to ipa global context */
      ipa_global_cntxt.v4_client_subnet_mask =
        clat_global_cntxt_info->v4_client_subnet_mask;
      ipa_global_cntxt.v4_client_subnet_addr = 
        clat_global_cntxt_info->v4_client_subnet_addr;
      ipa_global_cntxt.tos_ignore_bit_flag =
        clat_global_cntxt_info->tos_ignore_bit_flag;
      ipa_global_cntxt.tos_override_val =
        clat_global_cntxt_info->tos_override_val;          

      ipa_err_code = ipa_clat_global_register(&ipa_global_cntxt);

      if (IPA_SUCCESS != ipa_err_code)
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_FAILED;    
        ASSERT(0);
        break;
      }

      /*-----------------------------------------------------------------------
        For XLAT, indicate to IPA that UL static filters are applied after 
        CLAT gloabal register is called
      -----------------------------------------------------------------------*/
      ipa_ipfltr_apply_ul_static_rules_done();
    }
    else
    {
      dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
      break;
    }  
    
    return 0;    
  } while(0);

  LOG_MSG_ERROR_2("ps_dpm_hw_clat_reg_global_context(): Failed registering "
                  "clat context with dpm err %d and ipa err %d",
                   dpm_error_info, ipa_err_code);    
  return -1;
}/* ps_dpm_hw_clat_reg_global_context */

int ps_dpm_hw_clat_assoc_sio_stream
(
  ps_dpm_clat_hw_handle_type           * clat_hw_handle,
  sio_stream_id_type                     sio_stream_id
)
{
  ipa_err_code_e                         ipa_err_code = IPA_SUCCESS;
  ps_dpm_error_enum_type                 dpm_error_info = PS_DPM_SUCCESS;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {  
    /* check if CLAT is supported */
    if(ipa_query_capability() & IPA_CAPABILITY_CLAT_DL)
    {    
      if (NULL == clat_hw_handle)
      {
        dpm_error_info = PS_DPM_ERROR_ARG_INVALID; 
        break;
      }

      ipa_err_code = ipa_clat_assoc_sio_stream(*(ipa_clat_handle_t *)clat_hw_handle, 
                                               sio_stream_id);

      if (IPA_SUCCESS != ipa_err_code)
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_FAILED;    
        ASSERT(0);
        break;
      }
      
      ps_dpmi_hw_wait_for_ev_cback();
    }
    else
    {
      dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
      break;
    }  
    
    return 0;    
  } while(0);

  LOG_MSG_ERROR_2("ps_dpm_hw_clat_assoc_sio_stream(): Failed registering "
                  "clat context with dpm err %d and ipa err %d",
                   dpm_error_info, ipa_err_code);    
  return -1;
}/* ps_dpm_hw_clat_assoc_sio_stream */

int ps_dpm_hw_clat_rereg_pdn_context
(
  ps_dpm_clat_reg_pdn_cntxt_info_type       * clat_reg_info_ptr,
  ps_dpm_clat_hw_handle_type                * clat_hw_handle
)
{
  ipa_err_code_e                        ipa_err_code = IPA_SUCCESS;
  ipa_clat_pdn_cntxt_info_type          ipa_clat_pdn_cntxt;
  ps_dpm_error_enum_type                dpm_error_info = PS_DPM_SUCCESS;
  ps_iface_type                       * um_base_iface_ptr;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {  
    /* check if CLAT is supported */
    if(ipa_query_capability() & IPA_CAPABILITY_CLAT_DL)
    {   
      if (NULL == clat_reg_info_ptr->iface_ptr)
      {
        dpm_error_info = PS_DPM_ERROR_ARG_INVALID;
        break;
      }
  
      um_base_iface_ptr = PS_DPM_GET_BASE_IFACE(clat_reg_info_ptr->iface_ptr);
      if (!PS_IFACE_IS_VALID(um_base_iface_ptr))
      {
        dpm_error_info = PS_DPM_ERROR_IFACE_INVALID;
        break;
      }

      /* Register with hardware only for umts iface */
      if (UMTS_IFACE != PS_IFACE_GET_NAME(um_base_iface_ptr))
    {   
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
        break;
      }

      /* copy the dpm clat context to ipa clat context */              
      ipa_clat_pdn_cntxt.v4_src_addr = 
        clat_reg_info_ptr->v4_client_addr;
      ipa_clat_pdn_cntxt.v6_dev_prefix_len = 
        ps_dpm_hw_get_ipa_prefix_len(clat_reg_info_ptr->v6_dev_prefix_len);
      ipa_clat_pdn_cntxt.v6_plat_prefix_len = 
        ps_dpm_hw_get_ipa_prefix_len(clat_reg_info_ptr->v6_plat_prefix_len);        
      ipa_clat_pdn_cntxt.dev_addr_match_bits =
        clat_reg_info_ptr->v6_dev_addr_prefix_len;        
      
      memscpy(&(ipa_clat_pdn_cntxt.v6_dev_prefix),
              sizeof(struct ps_in6_addr),
              &(clat_reg_info_ptr->v6_dev_addr),
              sizeof(struct ps_in6_addr));
              
      memscpy(&(ipa_clat_pdn_cntxt.v6_plat_prefix),
              sizeof(struct ps_in6_addr),
              &(clat_reg_info_ptr->v6_plat_prefix),
              sizeof(struct ps_in6_addr));             

      ipa_err_code = ipa_clat_reconfigure(*(ipa_clat_handle_t *)clat_hw_handle,
			                  &ipa_clat_pdn_cntxt);
      if (IPA_SUCCESS != ipa_err_code)
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_FAILED;    
        ASSERT(0);
        break;
      }                
      
      ps_dpmi_hw_wait_for_ev_cback();       
    }
    else
    {
      dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_NOT_SUPPORTED;
      break;
    }
    
    return 0;
  } while(0);
 
  LOG_MSG_ERROR_2("ps_dpm_hw_clat_rereg_pdn_context(): Failed registering "
                  "clat context with hardware err %d and ipa err %d",
                   dpm_error_info, ipa_err_code);    
  return -1;
}/* ps_dpm_hw_clat_rereg_pdn_context */

void ps_dpm_hw_clat_dereg_pdn_context
(
  ps_dpm_clat_hw_handle_type       * clat_hw_handle
)
{
  ipa_err_code_e           ipa_err_code = IPA_SUCCESS;
  ps_dpm_error_enum_type   dpm_error_info = PS_DPM_SUCCESS;  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  do
  {
    if (NULL != clat_hw_handle)
    {
      ipa_err_code = ipa_clat_deregister(*(ipa_clat_handle_t *)clat_hw_handle);
      if (IPA_SUCCESS != ipa_err_code)
      {
        dpm_error_info = PS_DPM_ERROR_HARDWARE_OP_FAILED;  
        ASSERT(0); 
        break;        
      }      

      ps_dpmi_hw_wait_for_ev_cback();      
    }

    else
    {
      dpm_error_info = PS_DPM_ERROR_ARG_INVALID;  
      break;
    }
    
    return;
  } while(0);
  
  LOG_MSG_ERROR_2("ps_dpm_hw_clat_dereg_pdn_context(): Failed registering "
                  "clat context with dpm err %d and ipa err %d",
                   dpm_error_info, ipa_err_code);     
  return;
} /* ps_dpm_hw_clat_dereg_pdn_context */
#endif /* FEATURE_DATA_PS_464XLAT */
/**
  @brief  Registers CB with HW.
             call back would be invoked in two cases
             1)when continuous invalid packet count
             reaches threshold.
             2)first valid packet after invalid count threshold.
   
  @param[in] error_pkt_threshold:   threshold count
  @param[in] err_ind_cb_fn:            call back to be invoked by HW
   
  @return  NA
  
  @code
  @endcode
*/
void ps_dpm_hw_register_bearer_err_ind
(
  uint32                           err_pkt_threshold,
  void*                            err_ind_cb_fn_ptr
)
{
#ifndef TEST_FRAMEWORK
  ipa_wan_register_bearer_err_ind( err_pkt_threshold,
                                 ( ipa_wan_bearer_err_ind_cb_type)err_ind_cb_fn_ptr);
#endif
}
	
/**
  @brief  Deregisters CB with HW.
   
  @return  NA
  
  @code
  @endcode
*/
void ps_dpm_hw_deregister_bearer_err_ind
(
  void
)
{
#ifndef TEST_FRAMEWORK
  //ipa_wan_deregister_bearer_err_ind();
#endif
}