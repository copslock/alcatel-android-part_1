#ifndef DS_LTED_H
#define DS_LTED_H
/*===========================================================================
 
                             DS_LTED.H

DESCRIPTION
  Header file of DS LTE-D service module.
 
Copyright (c) 2015 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/lted/inc/ds_lted.h#5 $
  $DateTime: 2016/07/01 07:54:27 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/13/16   hr      Adding support for request queue
01/01/16   hr      TLS-PSK with GBA and provisioning for key generation
08/18/15   hr      Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY
#include "ds_lted_msg.h"
#include "ds_lted_ext_msg.h"

/*===========================================================================
                      EXTERNAL FUNCTIONS DEFINITIONS
===========================================================================*/

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/*===========================================================================
FUNCTION      DS_LTED_INIT

DESCRIPTION   Initializes the DS LTE-D services module
 
PARAMETERS    None
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_init
(
  void
);

#ifdef __cplusplus
}
#endif /* __cplusplus */

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_DSS_NET_CB_EVENT

DESCRIPTION   Dssnet callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_dss_net_cb_event
(
  ds_lted_net_cb_event_info_type *dss_net_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_ATTACH_NET_CB_EVENT

DESCRIPTION   Attach PDN dssnet callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_attach_net_cb_event
(
  ds_lted_net_cb_event_info_type *attach_net_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_HTTP_CB_EVENT

DESCRIPTION   HTTP callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_http_cb_event
(
  ds_lted_http_cb_event_info_type *http_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_RETRY_TIMER_CB_EVENT

DESCRIPTION   Retry timer expiry callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_retry_timer_cb_event
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_PS_SYS_CB_EVENT

DESCRIPTION   PS SYS event callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_ps_sys_cb_event
(
  ds_lted_ps_sys_cb_event_info_type *ps_sys_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_TLS_PSK_WAIT_EVENT

DESCRIPTION   PSK wait event processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_tls_psk_wait_event
(
  ds_lted_psk_wait_event_info_type *psk_wait_event_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_PROCESS_GBA_CB_EVENT

DESCRIPTION   GBA callback processor
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_process_gba_cb_event
(
  ds_lted_gba_cb_event_info_type *gba_cb_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_NV_REFRESH

DESCRIPTION   Process NV refresh for DS LTE-D module
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_nv_refresh
(
  ds_sys_subscription_enum_type subs_id
);

/*===========================================================================
FUNCTION      DS_LTED_REC_POST_REQ_EVENT_HDLR

DESCRIPTION   This function handles DS_APPSRV_LTE_D_POST_START_REQ to
              post LTE-D Client specified byte stream to LTE-D Server.
 
DEPENDENCIES  None

RETURN VALUE  TRUE  if processing was successful
              FALSE otherwise

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_recv_post_req_event_hdlr
(
  ds_lted_recv_post_req_event_info_type *ev_info_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_POST_REQ_HDLR

DESCRIPTION   This function handles DS_APPSRV_LTE_D_POST_START_REQ MSG in 
              DSMSGRRECV task 
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_post_req_hdlr
(
  ds_appsrv_lte_d_post_req_s *req_ptr
);

#endif /* FEATURE_LTE_DISCOVERY */
#endif /* DS_LTED_H */
