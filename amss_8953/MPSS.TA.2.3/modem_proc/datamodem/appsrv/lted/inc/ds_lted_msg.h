#ifndef DS_LTED_MSG_H
#define DS_LTED_MSG_H
/*===========================================================================

                            DS_LTED_MSG.h

DESCRIPTION
 Header file of LTE-D message factory and dispatcher.

 Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/lted/inc/ds_lted_msg.h#4 $
  $DateTime: 2016/07/01 07:54:27 $ $Author: pwbldsvc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/13/16    hr      Adding support for request queue
01/01/16    hr      TLS-PSK with GBA and provisioning for key generation
08/18/15    hr      Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY

#include "dssocket_defs.h"
#include "ps_sys.h"
#include "ps_sys_event.h"
#include "ds_http_types.h"
#include "gba_lib.h"
#include "ds_lted_ext_msg.h"

/*===========================================================================
                       MACRO AND TYPE DEFINITIONS
===========================================================================*/
typedef enum
{
  DS_LTED_MSG_ID_MIN = 0,
  DS_LTED_DSS_NET_CB_EVENT = DS_LTED_MSG_ID_MIN,
  DS_LTED_ATTACH_NET_CB_EVENT,
  DS_LTED_HTTP_CB_EVENT,
  DS_LTED_RETRY_TIMER_CB_EVENT,
  DS_LTED_PS_SYS_CB_EVENT,
  DS_LTED_PSK_WAIT_EVENT,
  DS_LTED_GBA_CB_EVENT,
  DS_LTED_PROVISION_HTTP_CB_EVENT,
  DS_LTED_PROVISION_REFRESH_TIMER_CB_EVENT,
  DS_LTED_PROVISION_RETRY_TIMER_CB_EVENT,
  DS_LTED_PROVISION_SMS_WAIT_TIMER_CB_EVENT,
  DS_LTED_PROVISION_SMS_RECEIVED_EVENT,
  DS_LTED_RECV_POST_REQ_EVENT,
  DS_LTED_MSG_ID_MAX
}ds_lted_msg_id_enum_type;

typedef struct
{
  sint15            nethandle;
  dss_iface_id_type iface_id;
  sint15            err;
}ds_lted_net_cb_event_info_type;

typedef struct
{
  uint32  session_id;
  uint32  request_id;
  sint15  error;
  uint16  http_status;
  uint8  *content;
  uint32  content_size;
  ds_http_response_header *header_info;
  uint32  num_headers;
}ds_lted_http_cb_event_info_type;

typedef struct
{
  ps_sys_tech_enum_type   tech_type;
  ps_sys_event_enum_type  event_name;
  void                   *event_info_ptr;
}ds_lted_ps_sys_cb_event_info_type;

typedef struct
{
  uint32                            http_req_id;
  ds_http_block_tls_psk_info_s_type tls_psk_info;
}ds_lted_psk_wait_event_info_type;

typedef struct
{
  uint32                  lted_req_id;
  gba_result_enum_type    status;
  gba_response_data_type *resp_ptr;
}ds_lted_gba_cb_event_info_type;

typedef struct
{
  uint32                     lted_req_id;
  ds_appsrv_lte_d_post_req_s post_req;
}ds_lted_recv_post_req_event_info_type;

/*===========================================================================
                      EXTERNAL FUNCTIONS DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION     DS_LTED_MSG_POST()

DESCRIPTION  Factory LTE-D internal commands

RETURN VALUE TRUE  if successful 
             FALSE otherwise 

DEPENDENCIES None

SIDE EFFECTS None
===========================================================================*/
boolean ds_lted_msg_post
(
  ds_lted_msg_id_enum_type  msg_id,
  void                     *msg_ptr
);

/*===========================================================================
FUNCTION      DS_LTED_INIT

DESCRIPTION   Initializes the DS LTE-D internal command processing function
 
PARAMETERS    None
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_msg_init
(
  void
);

#endif /* FEATURE_LTE_DISCOVERY */
#endif/* DS_LTED_MSG_H */
