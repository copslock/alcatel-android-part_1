#ifndef DS_LTED_CONFIG_H
#define DS_LTED_CONFIG_H
/*===========================================================================
 
                          DS_LTED_CONFIG.H

DESCRIPTION
  Header file of DS LTE-D configuration management module.
 
Copyright (c) 2015 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/lted/inc/ds_lted_config.h#5 $
  $DateTime: 2016/09/01 17:21:39 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/01/16   hr      TLS-PSK with GBA and provisioning for key generation
08/18/15   hr      Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY

#include "comdef.h"
#include "dssocket.h"
#include "mmgsdilib_common.h"
#include "gba_lib.h"

/*===========================================================================
                           MACRO AND TYPE DEFINITIONS
===========================================================================*/
#define DS_LTED_MAX_URI_LEN 100

typedef enum
{
  DS_LTED_ESM_SOFT_FAILURE_SHORT_RECOVERY = 0,
  DS_LTED_ESM_SOFT_FAILURE_LONG_RECOVERY,
  DS_LTED_ESM_HARD_FAILURE,
  DS_LTED_ESM_PERM_FAILURE
}ds_lted_esm_failure_type_e_type;

/*===========================================================================
                      EXTERNAL FUNCTIONS DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DS_LTED_CFG_INIT

DESCRIPTION   Initializes the DS LTE-D services configuration, called during 
              bootup and NV refresh EVENT
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_cfg_init
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_COMPOSE_NAF_URI_WITH_MCC_MNC

DESCRIPTION   Forms the LTE-D NAF URI based on MCC and MNC read from USIM
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_cfg_compose_naf_uri_with_mcc_mnc 
(
  nv_ehrpd_imsi_type *imsi_data
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_READ_PROSE_FUNC

DESCRIPTION   Reads NAF address from UIM PROSE function file
 
DEPENDENCIES  Subscription needs be ready

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_lted_cfg_read_prose_func
(
  mmgsdi_session_type_enum_type session_type
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_PDN_PROFILE_ID
 
DESCRIPTION   Returns the LTE-D PDN profile ID
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_pdn_profile_id
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_NAF_URI
 
DESCRIPTION   Returns the LTE-D NAF uri
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
char* ds_lted_cfg_get_naf_uri
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_N_PC3
 
DESCRIPTION   Returns the maxumum number of HTTP soft failure retries
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint8 ds_lted_cfg_get_N_pc3
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_PC3
 
DESCRIPTION   Returns the HTTP soft failure retry duration
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_pc3
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_SHORT
 
DESCRIPTION   Returns the PDN level short term ESM failure retry durantion
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_short
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_LONG
 
DESCRIPTION   Returns the PDN level long term ESM failure retry durantion
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_long
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_PERMANENT_BARRING
 
DESCRIPTION   Returns the permanent barring timer duration
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_permanent_barring
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_DS_PROVISION_ENABLED
 
DESCRIPTION   Returns if the DS LTE-D provision enabled
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_lted_cfg_get_ds_provision_enabled
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_ICP_URI
 
DESCRIPTION   Returns the LTE-D ICP uri
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
char* ds_lted_cfg_get_icp_uri
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_N_PROVISION
 
DESCRIPTION   Returns the maxumum number of provision soft failure retries
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint8 ds_lted_cfg_get_N_provision
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_PROVISION
 
DESCRIPTION   Returns the provision soft failure retry duration
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_provision
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_PROVISION_REFRESH
 
DESCRIPTION   Returns the provision refresh duration
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_provision_refresh
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_PROVISION_SMS_WAIT
 
DESCRIPTION   Returns the provision SMS wait timer
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_provision_sms_wait
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_PROVISION_SMS_PORT_NUM
 
DESCRIPTION   Returns the provision SMS port number
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_provision_sms_port_num
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_MAX_RETRY_ROUND
 
DESCRIPTION   Returns the max number of soft failure retry rounds
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_max_retry_round
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_GAP
 
DESCRIPTION   Returns T_gap for which retry round count can be cleared
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_gap
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_TCP
 
DESCRIPTION   Returns T_tcp socket and TCP level timeout for PC3 requests
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_tcp
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_T_throttling
 
DESCRIPTION   Sets the soft failure throttling timer
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_lted_cfg_get_T_throttling
(
  void
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_IS_HTTP_HARD_FAILURE

DESCRIPTION   This function returns if the specific http status is 
              treated as hard failure for LTE-D 
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_lted_cfg_is_http_hard_failure
(
  uint16  http_status
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_GET_ESM_FAILURE_TYEP

DESCRIPTION   This function returns type of failure for the specific esm 
              failure 
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
ds_lted_esm_failure_type_e_type ds_lted_cfg_get_esm_failure_type
(
  dss_net_down_reason_type reason
);

/*===========================================================================
FUNCTION      DS_LTED_CFG_IS_GBA_HARD_FAILURE 
 
DESCRIPTION   This function returns if the GBA result is treated as hard 
              failure for LTE-D 
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
boolean ds_lted_cfg_is_gba_hard_failure
(
  gba_result_enum_type gba_result
);

#endif /* FEATURE_LTE_DISCOVERY */
#endif /* DS_LTED_CONFIG_H */
