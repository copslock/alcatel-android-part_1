#ifndef DS_APPSRV_WMS_IF_H
#define DS_APPSRV_WMS_IF_H
/*===========================================================================
 
                          DS_APPSRV_WMS_IF.C

DESCRIPTION
  Header file for interface between appsrv and wms
 
Copyright (c) 2016 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/utils/inc/ds_appsrv_wms_if.h#1 $
  $DateTime: 2016/03/24 02:59:44 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/04/16   hr      Initial version
===========================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "datamodem_variation.h"

#include "comdef.h"
#include "wms.h"
#include "ds_appsrv_task.h"

/*===========================================================================
                           MACRO AND TYPE DEFINITIONS
===========================================================================*/
typedef struct
{
  boolean is_activated;
  wms_cl_parse_msg_cb_type parse_msg_cb;
  wms_msg_event_cb_type    msg_event_cb;
}ds_appsrv_wms_if_client_info_type;

/*===========================================================================
                         EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_INIT

DESCRIPTION   Initiates APPSRV WMS interface
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_init
(
  ds_appsrv_module_type module_id
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_RELEASE

DESCRIPTION   Initiates APPSRV WMS interface
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_release
(
  ds_appsrv_module_type module_id
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_ACTIVATE

DESCRIPTION   Activate WMS service for the module so that callbacks can be 
              received. If APPSRV WMS interface is not activated, will go
              ahead and activate itself.
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_activate
(
  ds_appsrv_module_type module_id
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_CLIENT_DEACTIVATE

DESCRIPTION   Deactivate WMS service for the module so that callbacks will 
              not be invoked. Deactivates APPSRV WMS interface if no module
              is active for WMS service anymore
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_client_deactivate
(
  ds_appsrv_module_type module_id
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_REG_PARSE_MSG_CB

DESCRIPTION   Register message parsing callback with APPSRV WMS interface
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_reg_parse_msg_cb
(
  ds_appsrv_module_type    module_id,
  wms_cl_parse_msg_cb_type msg_parsing_cb
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_REG_MSG_CB

DESCRIPTION   Register message event callback with APPSRV WMS interface
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_client_err_e_type ds_appsrv_wms_if_reg_msg_cb
(
  ds_appsrv_module_type module_id,
  wms_msg_event_cb_type msg_event_cb
);

/*===========================================================================
FUNCTION      DS_APPSRV_WMS_IF_MSG_ACK

DESCRIPTION   Wrapper for wms_msg_ack
 
DEPENDENCIES  None

RETURN VALUE  wms_client_err_e_type: error code

SIDE EFFECTS  None
===========================================================================*/
wms_status_e_type ds_appsrv_wms_if_msg_ack
(
  wms_cmd_cb_type            cmd_cb,
  const void                *user_data,
  const wms_ack_info_s_type *ack_info_ptr
);

#endif /* DS_APPSRV_WMS_IF_H */
