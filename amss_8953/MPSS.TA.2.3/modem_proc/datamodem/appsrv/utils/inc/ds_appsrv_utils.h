#ifndef DS_APPSRV_UTILS_H
#define DS_APPSRV_UTILS_H
/*===========================================================================

                           DS_APPSRV_UTILS.H

DESCRIPTION
   
   Collection of utility functions being used by various modules in APPSRV.
                
EXTERNALIZED FUNCTIONS

 Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================
                       EDIT HISTORY FOR FILE
  $Header: //components/rel/data.mpss/3.4.3.1/appsrv/utils/inc/ds_appsrv_utils.h#2 $
  $Author: pwbldsvc $  $DateTime: 2016/03/17 17:21:44 $
  
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/17/15   hr      Initial version.
===========================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "comdef.h"
#include "datamodem_variation.h"

#include "ds_sys.h"
/*===========================================================================
                   EXTERNAL DEFINITIONS AND TYPES
===========================================================================*/

/*-----------------------------------------------------------------
  Gives whether data is enabled or not while UE is in roaming.
  is_data_enabled = TRUE  : data is enabled while UE is in roaming
  is_data_enabled = FALSE : data is disabled while UE is in roaming
---------------------------------------------------------------------*/
typedef struct
{
  ds_sys_subscription_enum_type subs_id;
  boolean                       is_data_enabled;
}ds_appsrv_data_roaming_setting_s_type;



/*===========================================================================
                    EXTERNALIZED FUNCTIONS DEFINITIONS
===========================================================================*/

#ifdef __cplusplus
extern "C"
{
#endif
/*===========================================================================
FUNCTION      DS_APPSRV_UTILS_INIT

DESCRIPTION   This function initializes all APPSRV common services.
              The function is called during power up.
 
PARAMETERS    None 
 
DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_appsrv_utils_init
(
  void
);

/*===========================================================================
FUNCTION      DS_APPSRV_EFS_ITEM_FILE_PATH_SIZE

DESCRIPTION   Returns the total APPSRV EFS item file paths size
 
PARAMETERS    None
 
DEPENDENCIES  None

RETURN VALUE  Total APPSRV EFS item file paths size

SIDE EFFECTS  None
===========================================================================*/
uint32 ds_appsrv_efs_item_file_path_size
(
  void
);

/*===========================================================================
FUNCTION      DS_APPSRV_UPDATE_DATA_CONFIG_INFO

DESCRIPTION   Puts all the required nv item file paths in the 
              data_config_info.conf file. Would be called during powerup.
 
DEPENDENCIES  None

RETURN VALUE  length of string added to buffer for success. 
              -1 for failure.

SIDE EFFECTS  None
===========================================================================*/
int32 ds_appsrv_update_data_config_info
(
  char   *file_paths_buf
);

#ifdef __cplusplus
}
#endif

#endif /* DS_APPSRV_UTILS_H */
