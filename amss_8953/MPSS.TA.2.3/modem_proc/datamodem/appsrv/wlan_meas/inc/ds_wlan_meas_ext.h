#ifndef DS_WLAN_MEAS_EXT_H
#define DS_WLAN_MEAS_EXT_H
/*===========================================================================

                 WLAN_MEAS_EXT HEADER FILE

DESCRIPTION
  This file contains data declarations and function prototypes 
  
EXTERNALIZED FUNCTIONS 

 Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
4/30/15   youjunf      created
===========================================================================*/


/*===========================================================================
                           INCLUDE FILES
===========================================================================*/


/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/


/*===========================================================================
FUNCTION      DS_WLAN_MEAS_INIT

DESCRIPTION   This function initializes wlan meas module.

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/

void ds_wlan_meas_init(void);

#endif /* DS_WLAN_MEAS_EXT_H */

