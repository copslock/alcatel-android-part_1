#ifndef DS_WLAN_MEAS_CMD_HDLR_H
#define DS_WLAN_MEAS_CMD_HDLR_H
/*===========================================================================
                      DS_WLAN_MEAS_CMD_HDLR.H

DESCRIPTION
 cmd hdlr header file for wlan meas.

EXTERNALIZED FUNCTIONS
 Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who                            what, where, why
--------    -------                ----------------------------------------
05/01/15    Youjunf                          First version of file
===========================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "ps_sys.h"


/*===========================================================================
                   EXTERNAL DEFINITIONS AND TYPES
===========================================================================*/

/*---------------------------------------------------------------------------
  Enum for wlan meas internal cmd types
---------------------------------------------------------------------------*/
typedef enum
{ 
  DS_WLAN_MEAS_CMD_ID_MIN, 
  DS_WLAN_MEAS_CMD_WLAN_SYS_CHG = DS_WLAN_MEAS_CMD_ID_MIN,
  DS_WLAN_MEAS_CMD_ID_MAX
}ds_wlan_meas_cmd_id_enum_type;


/*-------------------------------------------------------------------------
wlan connection status type
-------------------------------------------------------------------------*/
typedef ps_sys_wlan_status_type  ds_wlan_meas_wlan_status_type;


typedef union
{
  ds_wlan_meas_wlan_status_type  wlan_status;
}ds_wlan_meas_cmd_content_u_type;


/*---------------------------------------------------------------------------
 wlan meas internal cmd structure
---------------------------------------------------------------------------*/
typedef struct
{
  ds_wlan_meas_cmd_id_enum_type    cmd_id;
  ds_wlan_meas_cmd_content_u_type  cmd_content;
}ds_wlan_meas_cmd_type;

/*===========================================================================
            EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================
FUNCTION      DS_WLAN_MEAS_CMD_INIT

DESCRIPTION   This function initializes wlan meas cmd module.

PARAMETERS    None

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_wlan_meas_cmd_init(void);


/*===========================================================================
FUNCTION DS_WLAN_MEAS_CMD_POST

DESCRIPTION
  Posts a wlan meas internal cmd.

DEPENDENCIES
  None

PARAMETERS
  cmd_id          - id of the command to be posted 
  cmd_content_ptr - pointer to the cmd content to be send along with the DS cmd


RETURN VALUE  None
  
SIDE EFFECTS  None
===========================================================================*/
void ds_wlan_meas_cmd_post(ds_wlan_meas_cmd_id_enum_type   cmd_id,
                           void                            *cmd_content_ptr);

/*===========================================================================
FUNCTION      DS_WLAN_MEAS_PROCESS_CMDS

DESCRIPTION   This function processes wlan meas commands. 

DEPENDENCIES  wlan meas should be initialized prior to calling this 
              function.
 
RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ds_wlan_meas_process_cmds(uint32 *data_ptr);


#endif /* DS_WLAN_MEAS_CMD_HDLR */