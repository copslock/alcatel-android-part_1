/*==============================================================================

                        ds_XMLDecoder_LTED.h

GENERAL DESCRIPTION
  Decoder for LTED

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/21/14    ml     Created file/Initial version.
==============================================================================*/
#ifndef DS_XML_DECODER_LTED_H
#define DS_XML_DECODER_LTED_H

#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY
#include "ds_XMLDecoder.h"

#include "ds_ASString.h"
#include "ds_ASVector.h"
#include "ds_ASAssignType.h"

namespace Appsrv
{
namespace XML
{

class XMLElement;





class ds_lted_MonitoringPolicy
{
public:
  ASString plmn;
  uint32   validity_timer;

  ds_lted_MonitoringPolicy(const XMLElement* elem, bool& flag);
  ~ds_lted_MonitoringPolicy();

private:
  void parse_plmn(const XMLElement* elem, bool& flag);
  void parse_validity_timer(const XMLElement* elem, bool& flag);
};


class ds_lted_AnnouncingPolicy
{
public:
  ASString                    plmn;
  uint32                      validity_timer;
  Utils::ASAssignType<uint32> range;

  ds_lted_AnnouncingPolicy(const XMLElement* elem, bool& flag);
  ~ds_lted_AnnouncingPolicy();

private:
  void parse_plmn(const XMLElement* elem, bool& flag);
  void parse_validity_timer(const XMLElement* elem, bool& flag);
  void parse_range(const XMLElement* elem, bool& flag);
};



class ds_lted_Provision
{
public:
  ASVector<ds_lted_MonitoringPolicy*> monitoring_policy;
  ASVector<ds_lted_AnnouncingPolicy*> announcing_policy;
  ASVector<ASString*>                 con_ref;
  ASString                            tls_psk_password;
  const XMLElement*                   ext;

  ds_lted_Provision(const XMLElement* elem, bool& flag);
  ~ds_lted_Provision();

private:
  void parse_monitoring_policy(const XMLElement* elem, bool& flag);
  void parse_announcing_policy(const XMLElement* elem, bool& flag);
  void parse_con_ref(const XMLElement* elem, bool& flag);
  void parse_ext(const XMLElement* elem, bool& flag);
};






/*===========================================================================
CLASS XMLDecoder_LTED

DESCRIPTION
  Decoder for LTE-D. policy will be null if decode failed
===========================================================================*/
class XMLDecoder_LTED : private XMLDecoder
{
public:
  /*===========================================================================
  FUNCTION XMLDecoder_LTED CONSTRUCTOR

  DESCRIPTION
    LTE-D decoder constructor. It will decode the given xml content first. Then
    if the root is not NULL, it will decode it into an LTE-D structure. policy
    will be NULL if decoding fails.

  PARAMETERS
    xml_content - The XML content to decode. If this is the only given parameter
                  client MUST guarantee that it is a null terminating c-string.
    len         - Length of xml_content. Client must guarantee that xml_content
                  size is at least len.

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
  ===========================================================================*/
  XMLDecoder_LTED(const char* xml_content);
  XMLDecoder_LTED(const char* xml_content, uint32 len);
  ~XMLDecoder_LTED();

protected:
  virtual void clean();

private:
  void parse_xml();


public:
  ds_lted_Provision* provision;
};


} /* namespace XML */
} /* namespace Appsrv */


#endif /* FEATURE_LTE_DISCOVERY */
#endif /* DS_XML_DECODER_LTED_H */
