/*==============================================================================

                        ds_XMLDecoder_LTED.cpp

GENERAL DESCRIPTION
  XML parser wrapper for LTE-D

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/21/14    ml     Created file/Initial version.
==============================================================================*/
#include "datamodem_variation.h"

#ifdef FEATURE_LTE_DISCOVERY
#include "ds_XMLDecoder_LTED.h"

#include "ds_XMLElement.h"
#include "ds_XMLSchemaValidator.h"
#include "ds_XMLUtility.h"

#include "ds_appsrv_mem.h"
#include "data_msg.h"
#include <cstdlib> // strtoul


namespace
{

const char ELEM_NAME_MONITORING_POLICY[] = "MonitoringPolicy";
const char ELEM_NAME_ANNOUNCING_POLICY[] = "AnnouncingPolicy";
const char ELEM_NAME_TO_CON_REF[]        = "ToConRef";
const char ELEM_NAME_PLMN[]              = "PLMN";
const char ELEM_NAME_VALIDITY_TIMER[]    = "ValidityTimerT4005";
const char ELEM_NAME_RANGE[]             = "Range";
const char ELEM_NAME_CON_REF[]           = "ConRef";
const char ELEM_NAME_EXT[]               = "Ext";
const char ELEM_NAME_TLS_PSK_PASSWORD[]  = "Password"; // TODO: TLS PSK provision tag name undecided

}


namespace Appsrv
{
namespace XML
{


/* ==========================================================================
 *                            ds_lted_MonitoringPolicy
 * ========================================================================== */
ds_lted_MonitoringPolicy::ds_lted_MonitoringPolicy(const XMLElement* elem, bool& flag)
: validity_timer(0)
{
  typedef void (ds_lted_MonitoringPolicy::*elem_parser)(const XMLElement*, bool&);

  ASVector<elem_parser> parse_fcn_ptrs;
  XMLElementValidator   element_validator(elem, flag, XMLElementCondition(NULL, true, true, 2, 2));

  // Flag will be set if current element is invalid.
  if(flag) return;


  // Add schema rules
  element_validator.addRule(XMLElementCondition(ELEM_NAME_PLMN, true, false));
  parse_fcn_ptrs.push_back(&ds_lted_MonitoringPolicy::parse_plmn);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_VALIDITY_TIMER, true, false));
  parse_fcn_ptrs.push_back(&ds_lted_MonitoringPolicy::parse_validity_timer);


  // Iterate child elements against the schema rules
  ds_xml_iterate_elements(this, element_validator, parse_fcn_ptrs, flag);
}



ds_lted_MonitoringPolicy::~ds_lted_MonitoringPolicy()
{ }



void ds_lted_MonitoringPolicy::parse_plmn(const XMLElement* elem, bool& flag)
{
  plmn = elem->get_text();
}



void ds_lted_MonitoringPolicy::parse_validity_timer(const XMLElement* elem, bool& flag)
{
  validity_timer = strtoul(elem->get_text().c_str(), NULL, 10);
}





/* ==========================================================================
 *                            ds_lted_AnnouncingPolicy
 * ========================================================================== */
ds_lted_AnnouncingPolicy::ds_lted_AnnouncingPolicy(const XMLElement* elem, bool& flag)
: validity_timer(0)
{
  typedef void (ds_lted_AnnouncingPolicy::*elem_parser)(const XMLElement*, bool&);

  ASVector<elem_parser> parse_fcn_ptrs;
  XMLElementValidator   element_validator(elem, flag, XMLElementCondition(NULL, true, true, 2, 3));

  // Flag will be set if current element is invalid.
  if(flag) return;


  // Add schema rules
  element_validator.addRule(XMLElementCondition(ELEM_NAME_PLMN, true, false));
  parse_fcn_ptrs.push_back(&ds_lted_AnnouncingPolicy::parse_plmn);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_VALIDITY_TIMER, true, false));
  parse_fcn_ptrs.push_back(&ds_lted_AnnouncingPolicy::parse_validity_timer);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_RANGE, false, false));
  parse_fcn_ptrs.push_back(&ds_lted_AnnouncingPolicy::parse_range);


  // Iterate child elements against the schema rules
  ds_xml_iterate_elements(this, element_validator, parse_fcn_ptrs, flag);
}



ds_lted_AnnouncingPolicy::~ds_lted_AnnouncingPolicy()
{ }



void ds_lted_AnnouncingPolicy::parse_plmn(const XMLElement* elem, bool& flag)
{
  plmn = elem->get_text();
}



void ds_lted_AnnouncingPolicy::parse_validity_timer(const XMLElement* elem, bool& flag)
{
  validity_timer = strtoul(elem->get_text().c_str(), NULL, 10);
}



void ds_lted_AnnouncingPolicy::parse_range(const XMLElement* elem, bool& flag)
{
  range = strtoul(elem->get_text().c_str(), NULL, 10);
}



/* ==========================================================================
 *                            ds_lted_Provision
 * ========================================================================== */
 ds_lted_Provision::ds_lted_Provision(const XMLElement* elem, bool& flag)
{
  typedef void (ds_lted_Provision::*elem_parser)(const XMLElement*, bool&);

  ASVector<elem_parser> parse_fcn_ptrs;
  XMLElementValidator   element_validator(elem, flag, XMLElementCondition(NULL, true, true, 0, 4));

  // Flag will be set if current element is invalid.
  if(flag) return;


  // Add schema rules
  element_validator.addRule(XMLElementCondition(ELEM_NAME_MONITORING_POLICY, false, true, 1));
  parse_fcn_ptrs.push_back(&ds_lted_Provision::parse_monitoring_policy);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_ANNOUNCING_POLICY, false, true, 1));
  parse_fcn_ptrs.push_back(&ds_lted_Provision::parse_announcing_policy);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_TO_CON_REF, false, true, 1));
  parse_fcn_ptrs.push_back(&ds_lted_Provision::parse_con_ref);

  element_validator.addRule(XMLElementCondition(ELEM_NAME_EXT, false, true, 0));
  parse_fcn_ptrs.push_back(&ds_lted_Provision::parse_ext);


  // Iterate child elements against the schema rules
  ds_xml_iterate_elements(this, element_validator, parse_fcn_ptrs, flag);
}



ds_lted_Provision::~ds_lted_Provision()
{
  clear_ptr_vector(monitoring_policy);
  clear_ptr_vector(announcing_policy);
  clear_ptr_vector(con_ref);
}




void ds_lted_Provision::parse_monitoring_policy(const XMLElement* elem, bool& flag)
{
  for(uint32 i = 0; i < elem->children_size(); i++)
  {
    if(!ds_xml_add_obj_to_vector(
                                 new(ds_appsrv_alloc<ds_lted_MonitoringPolicy>(), APPSRV_MEM) ds_lted_MonitoringPolicy(elem->get_child(i), flag),
                                 monitoring_policy,
                                 flag,
                                 ELEM_NAME_MONITORING_POLICY
                                 ))
    {
      return;
    }
  }
}



void ds_lted_Provision::parse_announcing_policy(const XMLElement* elem, bool& flag)
{
  for(uint32 i = 0; i < elem->children_size(); i++)
  {
    if(!ds_xml_add_obj_to_vector(
                                 new(ds_appsrv_alloc<ds_lted_AnnouncingPolicy>(), APPSRV_MEM) ds_lted_AnnouncingPolicy(elem->get_child(i), flag),
                                 announcing_policy,
                                 flag,
                                 ELEM_NAME_ANNOUNCING_POLICY
                                 ))
    {
      return;
    }
  }
}



void ds_lted_Provision::parse_con_ref(const XMLElement* elem, bool& flag)
{
  for(uint32 i = 0; i < elem->children_size(); i++)
  {
    const XMLElement* child = elem->get_child(i);
    if(NULL == child)
    {
      XMLDecoderException::get_instance().raise(flag, ERR_MSG_MALLOC_OBJ, ELEM_NAME_TO_CON_REF);
      return;
    }

    if(child->get_tag() != ELEM_NAME_CON_REF)
    {
      XMLDecoderException::get_instance().raise(flag, ERR_MSG_MALLOC_OBJ, ELEM_NAME_CON_REF);
      return;
    }

    ASString* conref_str = new(ds_appsrv_alloc<ASString>(), APPSRV_MEM) ASString(child->get_text());
    if(!con_ref.push_back(conref_str))
    {
      ds_appsrv_free(conref_str);
      XMLDecoderException::get_instance().raise(flag, ERR_MSG_MALLOC_VEC, ELEM_NAME_TO_CON_REF);
    }
  }
}



void ds_lted_Provision::parse_ext(const XMLElement* elem, bool& flag)
{
  ext = elem;

  for(uint32 i = 0; i < ext->children_size(); ++i)
  {
    const XMLElement* child = ext->get_child(i);

    if(child->get_tag() == ELEM_NAME_TLS_PSK_PASSWORD)
    {
      // TODO: Convert to uint8 if necessary. Currently copies raw text
      tls_psk_password = child->get_text();
      break;
    }
  }
}




/* ==========================================================================
 *                            XMLDecoder_LTED
 * ========================================================================== */
XMLDecoder_LTED::XMLDecoder_LTED(const char* xml_content)
: XMLDecoder(xml_content), provision(NULL)
{
  parse_xml();
}


XMLDecoder_LTED::XMLDecoder_LTED(const char* xml_content, uint32 len)
: XMLDecoder(xml_content, len), provision(NULL)
{
  parse_xml();
}


XMLDecoder_LTED::~XMLDecoder_LTED()
{
  XMLDecoder_LTED::clean();
}



void XMLDecoder_LTED::clean()
{
  if(NULL != provision)
    ds_appsrv_free(provision);

  provision = NULL;
}




void XMLDecoder_LTED::parse_xml()
{
  if(flag || NULL == root) return;
  provision = new(ds_appsrv_alloc<ds_lted_Provision>(), APPSRV_MEM) ds_lted_Provision(root, flag);

  if(NULL == provision)
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_LOW, "XMLDecoder_ANDSF::decode - Failed to allocate memory for ANDSF management object");
    XMLDecoderException::get_instance().raise(flag, ERR_MSG_MALLOC, "LTE-D Provision");
  }
  else if(flag)
  {
    DATA_APPSRV_MSG_SPRINTF_2(
                              MSG_LEGACY_ERROR,
                              "XMLDecoder::XMLDecoder - XMLDecoderException : %s; %s",
                              XMLDecoderException::get_instance().what(),
                              XMLDecoderException::get_instance().get_details()
                              );
    clean();
    XMLDecoder::clean();
  }
}



} /* namespace XML */
} /* namespace Appsrv */


#endif /* FEATURE_LTE_DISCOVERY */
