#ifndef DS_EPC_PDN_HO_SUB_SM_H
#define DS_EPC_PDN_HO_SUB_SM_H
/*===========================================================================

                           DS EPC PDN State Machine
DESCRIPTION 
  This header file contains enums for the input to the dlci state machine.
  Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/data.mpss/3.4.3.1/3gpp2/epchandoff/inc/ds_epc_pdn_ho_sub_sm.h#1 $
  $Author: pwbldsvc $ $DateTime: 2016/02/19 14:49:57 $

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/31/11    fjia    Created Module
===========================================================================*/
#include "customer.h"
#include "ds_epc_sm_defs.h"

/*===========================================================================
                      INCLUDE FILES FOR MODULE
===========================================================================*/

/*===========================================================================
                      DEFINITIONS  FOR THE MODULE
===========================================================================*/


/*===========================================================================
                      PUBLIC DATA DECLARATIONS
===========================================================================*/
#if 0
typedef enum
{
  EPC_PDN_HO_SM_START_EV = 0,
  EPC_PDN_HO_SM_STOP_EV,
  EPC_PDN_HO_SM_HO_EV,
  EPC_PDN_HO_SM_PREREG_EV,
  EPC_PDN_HO_SM_SRAT_RCVRD_EV,
  EPC_PDN_HO_IF_TRAT_UP_EV,
  EPC_PDN_HO_IF_TRAT_DOWN_EV,
  EPC_PDN_HO_TMR_SRAT_TMR_EXP_EV,
  EPC_PDN_HO_WAIT_TRAT_DOWN_EV  
}epc_pdn_ho_sub_sm_input_event_type; 
#endif 

/*===========================================================================
                      PUBLIC FUNCTION DECLARATIONS
===========================================================================*/


#endif /* DS_EPC_PDN_HO_SUB_SM_H */


