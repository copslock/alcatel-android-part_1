

#ifndef DS3G_EXT_H
#define DS3G_EXT_H
/*===========================================================================

                              D S 3 G   E X T H

                            H E A D E R   F I L E

DESCRIPTION
  This is the external header file for lower layers to use DS defined API and
  structures

  Copyright (c) 2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.  
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/api/public/ds3g_ext.h#1 $ $DateTime: 2016/02/19 14:49:57 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/23/15   sa     Initial creation.

===========================================================================*/


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/


/*===========================================================================

                      PUBLIC ENUM/STRUCT DECLARATIONS

===========================================================================*/

typedef enum
{
  DS3G_TX_POWER_BIN_1,                        /* x <= 0dBm */ 
  DS3G_TX_POWER_BIN_2,                        /* 0dBm < x <= 8dBm */ 
  DS3G_TX_POWER_BIN_3,                        /* 8dBm < x <= 15dBm */ 
  DS3G_TX_POWER_BIN_4,                        /* 15dBm < x <= 20dBm */ 
  DS3G_TX_POWER_BIN_5,                        /* x > 20dBm */ 
  DS3G_TX_POWER_BIN_MAX                       /*  MAX */ 
}ds3g_tx_power_bin_e_type;


#endif /* DS3G_EXT_H */
