#ifndef DS_LTED_EXT_MSG_H
#define DS_LTED_EXT_MSG_H
/*===========================================================================
 
                             DS_LTED_EXT_MSG.H

DESCRIPTION
  This file defines the interface to DS LTE-D service module
 
Copyright (c) 2015 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/data.mpss/3.4.3.1/api/public/ds_lted_ext_msg.h#3 $
  $DateTime: 2016/06/03 12:46:12 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/18/15   hr      Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"
#include "msgr.h"
#include "msgr_ds.h"
#include "ds_sys.h"
#include "nv_items.h"
#include "ds_http_types.h"

/*===========================================================================
                  MACROS, TYPEDEFS AND VARIABLES
===========================================================================*/
#define DS_APPSRV_LTE_D_POST_START_REQ_ID        0x01
#define DS_APPSRV_LTE_D_REQ_ID_IND_ID            0x02
#define DS_APPSRV_LTE_D_POST_FRAGMENT_REQ_ID     0x03
#define DS_APPSRV_LTE_D_POST_RESULT_IND_ID       0x04
#define DS_APPSRV_LTE_D_CANCEL_POST_REQ_ID       0x05
#define DS_APPSRV_LTE_ATTACHED_IND_ID            0x06
#define DS_APPSRV_SEND_SRVREQ_REQ_ID             0x07
#define DS_APPSRV_SRVREQ_RESULT_IND_ID           0x08
#define DS_APPSRV_RESET_ACTIVE_FLAG_REQ_ID       0x09
#define DS_APPSRV_LTE_D_PROVISION_REQ_ID         0x0a
#define DS_APPSRV_LTE_D_PROVISION_RESULT_IND_ID  0x0b
#define DS_APPSRV_IMSI_DATA_CHANGED_IND_ID       0x0c
#define DS_APPSRV_SET_LTE_D_PSK_REQ_ID           0x0d
#define DS_APPSRV_LTE_D_PSK_EXPIRY_IND_ID        0x0e

#define DS_APPSRV_MAX_LTE_D_REQ_FRAGMENT_CNT 10
#define DS_APPSRV_MAX_LTE_D_PSK_KEY_LEN      DS_HTTP_TLS_PSK_KEY_MAX_LEN

typedef struct 
{
  msgr_hdr_s    msg_hdr;                     /* Message router header */
  msgr_attach_s dsm_attach;                  /* Byte stream to be sent */
  char          apn_str[DS_SYS_MAX_APN_LEN]; /* APN for LTE-D PDN, if NULL DS will use pre-configured LTE-D profile */
  uint32        apn_len;                     /* Length of the APN string */
  boolean       is_last;                     /* Is last fragment for post request */
}ds_appsrv_lte_d_post_req_s;

typedef struct
{
  msgr_hdr_s msg_hdr;    /* Message router header */
  uint32     request_id; /* The request ID DS LTE-D service created */
}ds_appsrv_lte_d_post_req_id_ind_s;

typedef struct
{
  msgr_hdr_s    msg_hdr;    /* Message router header */
  msgr_attach_s dsm_attach; /* Byte stream to be sent */
  uint32        request_id; /* Request ID */
  boolean       is_last;    /* Is last fragment for post request */
}ds_appsrv_lte_d_post_fragment_req_s;

typedef enum
{
  DS_APPSRV_LTE_D_ERR_NONE,
  DS_APPSRV_LTE_D_LOCAL_ERR,
  DS_APPSRV_LTE_D_GBA_ERR, 
  DS_APPSRV_LTE_D_GBA_PERM_ERR, /* only 403 is counted as hard failure in GBA */
  DS_APPSRV_LTE_D_TLS_ERR,
  DS_APPSRV_LTE_D_TLS_PERM_ERR,
  DS_APPSRV_LTE_D_HTTP_ERR, 
  DS_APPSRV_LTE_D_HTTP_PERM_ERR, 
  DS_APPSRV_LTE_D_PDN_CONN_ERR, 
  DS_APPSRV_LTE_D_PDN_CONN_PERM_ERR,
  DS_APPSRV_LTE_D_PROVISION_IN_PROG,
  DS_APPSRV_LTE_D_PROVISION_PERM_ERR
}ds_appsrv_lte_d_err_e_type;

typedef struct
{
  msgr_hdr_s                 msg_hdr;          /* Message router header */
  msgr_attach_s              dsm_attach;       /* Byte stream received */
  ds_appsrv_lte_d_err_e_type err_code;         /* Error code */
  uint16                     http_status_code; /* HTTP server error status code */
  uint32                     req_id;           /* Request ID */
}ds_appsrv_lte_d_post_result_ind_s;

typedef struct
{
  msgr_hdr_s                 msg_hdr; /* Message router header */
  uint32                     req_id;  /* Request ID */
}ds_appsrv_lte_d_cancel_post_req_s;

typedef struct
{
  msgr_hdr_s msg_hdr;   /* Message router header */
  boolean    is_succ;   /* Is service request sent successfully */
  uint8      emm_cause; /* Reserved */
}ds_appsrv_srvreq_result_ind_s;

typedef struct
{
  msgr_hdr_s                 msg_hdr;  /* Message router header */
  ds_appsrv_lte_d_err_e_type err_code; /* Error code */
}ds_appsrv_lte_d_provision_result_ind_s;

typedef struct
{
  msgr_hdr_s         msg_hdr;   /* Message router header */
  nv_ehrpd_imsi_type imsi_data; /* IMSI data */
}ds_appsrv_imsi_data_changed_ind_s;

typedef struct
{
  msgr_hdr_s msg_hdr;                                  /* Message router header */
  uint8      psk_key[DS_APPSRV_MAX_LTE_D_PSK_KEY_LEN]; /* PSK data */
  uint16     psk_key_len;                              /* PSK len */
}ds_appsrv_set_lte_d_psk_req_s;

enum
{
  MSGR_DEFINE_UMID(DS, APPSRV, REQ, LTE_D_POST_START,
                   DS_APPSRV_LTE_D_POST_START_REQ_ID,
                   ds_appsrv_lte_d_post_req_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, LTE_D_REQ_ID,
                   DS_APPSRV_LTE_D_REQ_ID_IND_ID,
                   ds_appsrv_lte_d_post_req_id_ind_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, LTE_D_POST_FRAGMENT,
                   DS_APPSRV_LTE_D_POST_FRAGMENT_REQ_ID,
                   ds_appsrv_lte_d_post_fragment_req_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, LTE_D_POST_RESULT,
                   DS_APPSRV_LTE_D_POST_RESULT_IND_ID,
                   ds_appsrv_lte_d_post_result_ind_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, LTE_D_CANCEL_POST,
                   DS_APPSRV_LTE_D_CANCEL_POST_REQ_ID,
                   ds_appsrv_lte_d_cancel_post_req_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, LTE_ATTACHED,
                   DS_APPSRV_LTE_ATTACHED_IND_ID,
                   msgr_hdr_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, SEND_SRVREQ,
                   DS_APPSRV_SEND_SRVREQ_REQ_ID,
                   msgr_hdr_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, SRVREQ_RESULT,
                   DS_APPSRV_SRVREQ_RESULT_IND_ID,
                   ds_appsrv_srvreq_result_ind_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, RESET_ACTIVE_FLAG,
                   DS_APPSRV_RESET_ACTIVE_FLAG_REQ_ID,
                   msgr_hdr_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, LTE_D_PROVISION,
                   DS_APPSRV_LTE_D_PROVISION_REQ_ID,
                   msgr_hdr_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, LTE_D_PROVISION_RESULT,
                   DS_APPSRV_LTE_D_PROVISION_RESULT_IND_ID,
                   ds_appsrv_lte_d_provision_result_ind_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, IMSI_DATA_CHANGED,
                   DS_APPSRV_IMSI_DATA_CHANGED_IND_ID,
                   ds_appsrv_imsi_data_changed_ind_s),

  MSGR_DEFINE_UMID(DS, APPSRV, REQ, SET_LTE_D_PSK,
                   DS_APPSRV_SET_LTE_D_PSK_REQ_ID,
                   ds_appsrv_set_lte_d_psk_req_s),

  MSGR_DEFINE_UMID(DS, APPSRV, IND, LTE_D_PSK_EXPIRY,
                   DS_APPSRV_LTE_D_PSK_EXPIRY_IND_ID,
                   msgr_hdr_s)
};

#endif /* DS_LTED_EXT_MSG_H */
