/*!
  @file
  prose_defs.h

  @brief
  This file contains the PROSE LTE_QMI interface declarations.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/api/prose/prose_defs.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_DEFS_H
#define PROSE_DEFS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <comdef.h>

#include <sys.h>
#include <sys_plmn_selection.h>

#define PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE 50

#define PROSE_MONITORING_POLICY_LIST_MAX_SIZE 50

#define PROSE_DEDICATED_APN_NAME_MAX_SIZE 100

#define PROSE_MAX_TIMR_DUR_MIN 525600

#define PROSE_MIN_TIMR_DUR_MIN 1


typedef enum
{
  PROSE_DISC_CATEGORY_INVALID = 0,
  PROSE_DISC_CATEGORY_HIGH,
  PROSE_DISC_CATEGORY_MEDIUM,
  PROSE_DISC_CATEGORY_LOW,
  PROSE_DISC_CATEGORY_VERY_LOW,
  PROSE_DISC_CATEGORY_MAX
} prose_disc_category_e;

typedef enum
{
  PROSE_DISC_TYPE_INVALID = 0,
  PROSE_DISC_TYPE_OPEN,
  PROSE_DISC_TYPE_RESTRICTED
} prose_disc_type_e;

typedef enum
{
  PROSE_DISC_ANNOUNCING_POLICY_RANGE_INVALID = 0,
  PROSE_DISC_ANNOUNCING_POLICY_RANGE_SHORT,
  PROSE_DISC_ANNOUNCING_POLICY_RANGE_MEDIUM,
  PROSE_DISC_ANNOUNCING_POLICY_RANGE_LONG,
  PROSE_DISC_ANNOUNCING_POLICY_RANGE_RESERVED
} prose_disc_announcing_policy_range_e;


#define PROSE_APP_ID_SIZE 2048
typedef struct { uint32 length; char byte[PROSE_APP_ID_SIZE]; } prose_pa_id_s;

#define PROSE_OS_ID_SIZE 16 
typedef struct { unsigned char byte[PROSE_OS_ID_SIZE]; } prose_os_id_s;

#define PROSE_OS_APP_ID_SIZE 256
typedef struct { uint32 length; char byte[PROSE_OS_APP_ID_SIZE]; } prose_os_app_id_s;

#define PROSE_APP_CODE_SIZE 23
typedef struct { unsigned char byte[PROSE_APP_CODE_SIZE]; } prose_app_code_s;

#define PROSE_DISC_KEY_SIZE 16
typedef struct { unsigned char byte[PROSE_DISC_KEY_SIZE]; } prose_disc_key_s;

typedef struct
{
  sys_plmn_id_s_type plmn;

  uint32 t4005;

  prose_disc_announcing_policy_range_e range;

} prose_disc_announcing_policy_list_info_s;

typedef struct
{
  sys_plmn_id_s_type plmn;

  uint32 t4005;

} prose_disc_monitoring_policy_list_info_s;


#ifdef __cplusplus
}
#endif

#endif /* PROSE_DEFS_H */
