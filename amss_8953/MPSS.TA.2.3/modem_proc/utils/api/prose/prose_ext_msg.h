/*!
  @file
  prose_ext_msg.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/api/prose/prose_ext_msg.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_EXT_MSG_H
#define PROSE_EXT_MSG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <msgr.h>
#include <appmgr.h>
#include "prose_defs.h"

#define MSGR_PROSE_DISC    MSGR_TECH_MODULE( MSGR_TECH_PROSE, 0x01 )

typedef enum
{
  PROSE_QMI_RESULT_SUCCESS = 0,
  PROSE_QMI_RESULT_IN_PROGRESS,
  PROSE_QMI_RESULT_FAILURE,
  PROSE_QMI_RESULT_ABORTED,
  PROSE_QMI_RESULT_PC3_FAILURE,
  PROSE_QMI_RESULT_PC5_FAILURE,
  PROSE_QMI_RESULT_PC3_FAILURE_INVALID_APP,
  PROSE_QMI_RESULT_PC3_FAILURE_UNKNOWN_PA_ID,
  PROSE_QMI_RESULT_PC3_FAILURE_UE_AUTHORIZATION,
  PROSE_QMI_RESULT_PC3_FAILURE_SCOPE_VIOLATION_PA_ID,
  PROSE_QMI_RESULT_ERR_UNKNOWN
} prose_qmi_result_e;

typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_id_s os_id;

  int32 announcing_policy_list_size;

  prose_disc_announcing_policy_list_info_s announcing_policy_list[PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE];

  int32 monitoring_policy_list_size;

  prose_disc_monitoring_policy_list_info_s monitoring_policy_list[PROSE_MONITORING_POLICY_LIST_MAX_SIZE];

  char dedicated_apn_name[PROSE_DEDICATED_APN_NAME_MAX_SIZE];
} prose_disc_set_config_req_s;

typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_qmi_result_e result;

} prose_disc_set_config_rsp_s;

typedef struct
{
  msgr_hdr_struct_type msg_hdr;

} prose_disc_get_config_req_s;

typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_id_s os_id;

  int32 announcing_policy_list_size;

  prose_disc_announcing_policy_list_info_s announcing_policy_list[PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE];

  int32 monitoring_policy_list_size;

  prose_disc_monitoring_policy_list_info_s monitoring_policy_list[PROSE_MONITORING_POLICY_LIST_MAX_SIZE];

  char dedicated_apn_name[PROSE_DEDICATED_APN_NAME_MAX_SIZE];
} prose_disc_get_config_rsp_s;

/*! @brief Data Structure for Discovery Set Category Request
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_disc_category_e category;

} prose_disc_set_category_req_s;

/*! @brief Data Structure for Discovery Set Category Response
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

} prose_disc_set_category_rsp_s;


/*! @brief Data Structure for Discovery Get Category Request
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

} prose_disc_get_category_req_s;

/*! @brief Data Structure for Discovery Get Category Response 
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_disc_category_e category;

} prose_disc_get_category_rsp_s;

/*! @brief Data Structure for Discovery Terminate Request
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

} prose_disc_terminate_req_s;


/*! @brief Data Structure for Discovery Terminate Response 
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

} prose_disc_terminate_rsp_s;

/*! @brief Data Structure for Discovery Get Service Status 
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

} prose_disc_get_service_status_req_s;

/*! @brief Data Structure for Discovery Get Service Status 
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;
  
  boolean publish_allowed;

  boolean subscribe_allowed;

} prose_disc_get_service_status_rsp_s;


/*! @brief Data Structure for Discovery Publish Request
 *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  msgr_attach_s dsm_attach;

  prose_os_app_id_s os_app_id;
  
  prose_pa_id_s pa_id;

  uint32        duration;

  prose_disc_type_e disc_type;

  uint32        request_time;

} prose_disc_publish_req_s;


/*! @brief Data Structure for Discovery Publish Response
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;
  
  prose_os_app_id_s os_app_id;

  prose_pa_id_s pa_id;

  prose_qmi_result_e result;

} prose_disc_publish_rsp_s;


/*! @brief Data Structure for Discovery Publish Cancel Request
 *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;
  
  prose_pa_id_s pa_id;

} prose_disc_publish_cancel_req_s;


/*! @brief Data Structure for Discovery Publish Cancel Response
 *  *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

} prose_disc_publish_cancel_rsp_s;


/*! @brief Data Structure for Discovery Subscribe Request
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_pa_id_s pa_id;

  uint32        duration;

  prose_disc_type_e disc_type;

  uint32        request_time;

  boolean       metadata_flag;

} prose_disc_subscribe_req_s;


/*! @brief Data Structure for Discovery Subscribe Response
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_pa_id_s pa_id;

  prose_qmi_result_e result;

} prose_disc_subscribe_rsp_s;


/*! @brief Data Structure for Discovery Subscribe Cancel Request
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_pa_id_s pa_id;

} prose_disc_subscribe_cancel_req_s;

/*! @brief Data Structure for Discovery Subscribe Cancel Response
 *  */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

} prose_disc_subscribe_cancel_rsp_s;


/*! @brief Data Structure for Broadcast Notification Indication
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  boolean publish_allowed;

  boolean subscribe_allowed;

} prose_disc_broadcast_notification_ind_s;


/*! @brief Data Structure for Match Event Indication
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  msgr_attach_s dsm_attach;

  prose_os_app_id_s os_app_id;
  
  prose_pa_id_s pa_id;

  prose_pa_id_s matched_pa_id;
  
  boolean match_event_status;
  
} prose_disc_match_event_ind_s;


/*! @brief Data Structure for Notification Indication
 */
typedef struct
{
  msgr_hdr_struct_type msg_hdr;

  prose_os_app_id_s os_app_id;

  prose_pa_id_s pa_id;

  prose_qmi_result_e result;

} prose_disc_notification_ind_s;


typedef struct
{
  msgr_hdr_struct_type msg_hdr;

} prose_disc_dummy_s;


/*! @brief External requests that Prose receives
 * */
enum
{
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, SET_CONFIG, 0x00, prose_disc_set_config_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, GET_CONFIG, 0x01, prose_disc_get_config_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, SET_CATEGORY, 0x02, prose_disc_set_category_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, GET_CATEGORY, 0x03, prose_disc_get_category_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, TERMINATE, 0x04, prose_disc_terminate_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, GET_SERVICE_STATUS, 0x05, prose_disc_get_service_status_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, PUBLISH, 0x06, prose_disc_publish_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, PUBLISH_CANCEL, 0x07, prose_disc_publish_cancel_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, SUBSCRIBE, 0x08, prose_disc_subscribe_req_s),
  MSGR_DEFINE_UMID(PROSE, DISC, REQ, SUBSCRIBE_CANCEL, 0x09, prose_disc_subscribe_cancel_req_s),
};

/*! @brief External responses that Prose throws
 *  * */
enum
{
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, SET_CONFIG, 0x00, prose_disc_set_config_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, GET_CONFIG, 0x01, prose_disc_get_config_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, SET_CATEGORY, 0x02, prose_disc_set_category_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, GET_CATEGORY, 0x03, prose_disc_get_category_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, TERMINATE, 0x04, prose_disc_terminate_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, GET_SERVICE_STATUS, 0x05, prose_disc_get_service_status_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, PUBLISH, 0x06, prose_disc_publish_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, PUBLISH_CANCEL, 0x07, prose_disc_publish_cancel_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, SUBSCRIBE, 0x08, prose_disc_subscribe_rsp_s),
  MSGR_DEFINE_UMID(PROSE, DISC, RSP, SUBSCRIBE_CANCEL, 0x09, prose_disc_subscribe_cancel_rsp_s),
};

/*! @brief External indications that Prose throws
 *  * */
enum
{
  MSGR_DEFINE_UMID(PROSE, DISC, IND, BROADCAST_NOTIFICATION, 0x00, prose_disc_broadcast_notification_ind_s),
  MSGR_DEFINE_UMID(PROSE, DISC, IND, MATCH_EVENT, 0x01, prose_disc_match_event_ind_s),
  MSGR_DEFINE_UMID(PROSE, DISC, IND, NOTIFICATION, 0x02, prose_disc_notification_ind_s),
};


/*! @brief Supervisory messages that Prose receives or throws
 * */
enum
{
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, LOOPBACK, MSGR_ID_LOOPBACK,
      msgr_spr_loopback_s),
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, LOOPBACK_REPLY, MSGR_ID_LOOPBACK_REPLY,
      msgr_spr_loopback_reply_s),
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, THREAD_READY, APPMGR_ID_THREAD_READY,
      none),
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, THREAD_KILL, APPMGR_ID_THREAD_KILL,
      none),

  MSGR_DEFINE_UMID(PROSE, DISC, SPR, INT_MSG, 5, none),
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, EXT_MSG, 6, none),
  MSGR_DEFINE_UMID(PROSE, DISC, SPR, STIMULI, 7, none),
};

/*! @brief Enumeration of TMRI id nums
 *  * */
/*********************************************************************
 *  DO NOT CHANGE THE ORDER OF THE ENUM BELOW - USED FOR EVENT LOGGING.
 *   ALWAYS ADD YOUR NEW TIMER AT THE BOTTOM
 *   *********************************************************************/
typedef enum
{
  DISC_TMRI_T4000 = 0x0, /* do not change */
  DISC_TMRI_MAX          /*!< Max value */
} prose_disc_tmri_type_e;


/*! @brief Timers maintained by the Prose procedures
 *  * */
enum
{
  /* Internal Timers */
  PROSE_DISC_TMRI_FIRST = MSGR_UMID_BASE(MSGR_PROSE_DISC, MSGR_TYPE_TMRI),

  MSGR_DEFINE_UMID(PROSE, DISC, TMRI, T4000, DISC_TMRI_T4000, prose_disc_tmri_s),

  PROSE_DISC_TMRI_MAX,
  PROSE_DISC_TMRI_LAST = PROSE_DISC_TMRI_MAX - 1
};


#ifdef __cplusplus
}
#endif

#endif /* PROSE_EXT_MSG_H*/
