/*!
  @file
  prose_test_qmi_config_multiple.cpp

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/tests/prose_test_qmi_config_multiple.cpp#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

extern "C"
{
#include "prose_ext_msg.h"
#include "dsm_item.h"
#include "dsm_init.h"
}

#include "tf_stub.h"
#include "TestFramework.h"
#include "prose_qtf.h"

static msgr_umid_type umid_list[] =
{
  LTE_RRC_PROSE_CFG_REQ,
  PROSE_DISC_SET_CONFIG_RSP,
  PROSE_DISC_BROADCAST_NOTIFICATION_IND,
};

TF_DEFINE_TEST_CASE(COMPONENT, ProseTestQmiConfigMultiple);


void ProseTestQmiConfigMultiple::Setup()
{
  prose_qtf_setup(umid_list, sizeof(umid_list)/sizeof(umid_list[0]));  
}

void ProseTestQmiConfigMultiple::Test()
{
  byte *buf_ptr;
  uint32 buf_len;

  prose_disc_set_config_req_s config_req;
  msgr_hdr_s attach_ind;
  lte_rrc_prose_cfg_req_s *cfg_req_ptr;
  prose_disc_set_config_rsp_s *cfg_rsp_ptr;

  dsm_item_type *dsm_ptr = NULL;
  msgr_attach_struct_type *att_ptr;

  TF_MSG("Start of test");

  sys_plmn_id_s_type plmn1 = { { 0x01, 0xF0, 0x01 } };
  sys_plmn_id_s_type plmn2 = { { 0x01, 0xF0, 0x02 } };

  msgr_init_hdr(&config_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SET_CONFIG_REQ);
  config_req.announcing_policy_list_size = 1;
  config_req.announcing_policy_list[0].plmn = plmn1;
  config_req.announcing_policy_list[0].range = PROSE_DISC_ANNOUNCING_POLICY_RANGE_MEDIUM;
  config_req.announcing_policy_list[0].t4005 = 1;
  config_req.monitoring_policy_list_size = 2;
  config_req.monitoring_policy_list[0].plmn = plmn1;
  config_req.monitoring_policy_list[0].t4005 = 1;
  config_req.monitoring_policy_list[1].plmn = plmn2;
  config_req.monitoring_policy_list[1].t4005 = 3;
  memcpy(config_req.dedicated_apn_name, "TEST_APN", 9);
  memset(&config_req.os_id, 0, sizeof(config_req.os_id));
  *(uint16*)(&config_req.os_id.byte) = 4023;
  prose_qtf_send_msg((byte*)&config_req, sizeof(config_req));

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.num_plmns == 1);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[2] == 0x0F);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.num_plmns == 2);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[2] == 0x0F);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[0] == 0x02);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[2] == 0x0F);
  
  TF_MSG("Checking for PROSE_DISC_SET_CONFIG_RSP");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_set_config_rsp_s) == buf_len);
  cfg_rsp_ptr = (prose_disc_set_config_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SET_CONFIG_RSP == cfg_rsp_ptr->msg_hdr.id);
  
  msgr_init_hdr(&attach_ind, MSGR_PROSE_DISC, DS_APPSRV_LTE_ATTACHED_IND);
  prose_qtf_send_msg((byte*)&attach_ind, sizeof(attach_ind));

  lte_rrc_lted_available_ind_s lted_avail;
  msgr_init_hdr(&lted_avail.msg_hdr, MSGR_PROSE_DISC, LTE_RRC_LTED_AVAILABLE_IND);
  lted_avail.tx_enabled = TRUE;
  lted_avail.rx_enabled = TRUE;
  lted_avail.tx_on_serving = TRUE;
  prose_qtf_send_msg((byte*)&lted_avail, sizeof(lted_avail));

  TF_MSG("Checking for PROSE_DISC_BROADCAST_NOTIFICATION_IND");

  prose_disc_broadcast_notification_ind_s *notif_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_broadcast_notification_ind_s) == buf_len);
  notif_ind_ptr = (prose_disc_broadcast_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_BROADCAST_NOTIFICATION_IND == notif_ind_ptr->msg_hdr.id);
  TF_ASSERT(notif_ind_ptr->publish_allowed == TRUE);
  TF_ASSERT(notif_ind_ptr->subscribe_allowed == TRUE);

  sys_plmn_id_s_type plmn3 = { { 0x01, 0xF0, 0x03 } };
  sys_plmn_id_s_type plmn4 = { { 0x01, 0xF0, 0x04 } };

  msgr_init_hdr(&config_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SET_CONFIG_REQ);
  config_req.announcing_policy_list_size = 1;
  config_req.announcing_policy_list[0].plmn = plmn3;
  config_req.announcing_policy_list[0].range = PROSE_DISC_ANNOUNCING_POLICY_RANGE_MEDIUM;
  config_req.announcing_policy_list[0].t4005 = 1;
  config_req.monitoring_policy_list_size = 2;
  config_req.monitoring_policy_list[0].plmn = plmn3;
  config_req.monitoring_policy_list[0].t4005 = 1;
  config_req.monitoring_policy_list[1].plmn = plmn4;
  config_req.monitoring_policy_list[1].t4005 = 3;
  memcpy(config_req.dedicated_apn_name, "TEST_APN", 9);
  memset(&config_req.os_id, 0, sizeof(config_req.os_id));
  *(uint16*)(&config_req.os_id.byte) = 4023;
  prose_qtf_send_msg((byte*)&config_req, sizeof(config_req));

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.num_plmns == 1);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[0] == 0x03);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.plmn[0].mnc[2] == 0x0F);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.num_plmns == 2);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[0] == 0x03);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[2] == 0x0F);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[0] == 0x04);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[2] == 0x0F);
  
  TF_MSG("Checking for PROSE_DISC_SET_CONFIG_RSP");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_set_config_rsp_s) == buf_len);
  cfg_rsp_ptr = (prose_disc_set_config_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SET_CONFIG_RSP == cfg_rsp_ptr->msg_hdr.id);
  
  TF_SLEEP(60000);

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.num_plmns == 0);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.num_plmns == 2);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[0] == 0x03);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[2] == 0x0F);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[0] == 0x04);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[1].mnc[2] == 0x0F);

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  TF_ASSERT(cfg_req_ptr->lted_tx_plmn.num_plmns == 0);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.num_plmns == 1);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[0] == 0x01);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mcc[2] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[0] == 0x04);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[1] == 0x00);
  TF_ASSERT(cfg_req_ptr->lted_rx_plmn.plmn[0].mnc[2] == 0x0F);

  prose_qtf_wait_for_done();

  prose_qtf_check_no_more_msgs();
}

void ProseTestQmiConfigMultiple::Teardown()
{
  prose_qtf_teardown();
}

