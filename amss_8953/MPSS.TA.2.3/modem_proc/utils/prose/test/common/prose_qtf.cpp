/*!
  @file
  prose_qtf.cpp

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/common/prose_qtf.cpp#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#include "TestFramework.h"

#include <qtf.h>
#ifndef FEATURE_MCS_QTF_ON_TARGET
#include "qtf_rcinit.h"
#include <qtf_tmc.h>
#endif /* FEATURE_MCS_QTF_ON_TARGET */

extern "C"
{
#include <assert.h>
#include <msgr.h>
#include <msgr_lte.h>
#include <msgr_types.h>
#include <appmgr.h>
#include <pthread.h>
#include <mqueue.h>
#include <signal.h>

#include <dsm_item.h>
#include <dsm_init.h>
}

#include "prose_qtf.h"
#include <qtf_efs.h>

#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK */


// Maximum number of internal signals in the signal queue (both ext and int)
#define PROSE_QTF_MAX_NUM_SIGNALS     100
#define PROSE_QTF_DEFAULT_MSGQ_PRIO   1

// Maximum number of external msg ids (terminated outside RRC) the 
// state machine under test can throw
#define PROSE_QTF_MAX_NUM_EXT_MSGIDS  100

/*
   We can bring this back once CS bug is fixed
#define PROSE_QTF_EXT_MB_SIZE  (PROSE_QTF_MAX_NUM_EXT_MSGIDS * sizeof(lte_rrc_ext_msg_u))
*/
#define PROSE_QTF_EXT_MB_SIZE 5000

static prose_qtf_s prose_qtf;

/*===========================================================================

FUNCTION:  prose_qtf_msgq_init

===========================================================================*/
/*!
  @brief
  Init routine for a test message queue. Creates and inits all necessary objects.

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_msgq_init(prose_qtf_msgq_s *msgq, char *q_name)
{
  pthread_mutexattr_t mutex_attr;

  /*-----------------------------------------------------------------------*/

  ASSERT(msgq != NULL);

  /*-----------------------------------------------------------------------*/

  // Create an instance of a signal queue for received msg signals
  ASSERT(msgr_client_create(&msgq->client) == E_SUCCESS);

  ASSERT(msgr_client_add_mq(q_name,
        &msgq->client,
        PROSE_QTF_DEFAULT_MSGQ_PRIO,
        PROSE_QTF_MAX_NUM_SIGNALS,
        sizeof(msgr_hdr_s),
        &msgq->mb_id) == E_SUCCESS);

  // Create mutex object to synchronize access to msgq
  ASSERT(pthread_mutexattr_init(&mutex_attr) == 0);
  ASSERT(pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE) == 0);
  ASSERT(pthread_mutex_init(&msgq->mutex, &mutex_attr) == 0);

  // Initialize the internal message queue
  prose_qtf_queue_reset(msgq);

} /* prose_qtf_msgq_init() */


/*===========================================================================

FUNCTION:  prose_qtf_msgq_deinit

===========================================================================*/
/*!
  @brief
  Deinit routine for a test message queue. Deallocates all necessary objects.

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_msgq_deinit
(
 prose_qtf_msgq_s *msgq   /*!< Ptr to the msgq to initialize */
 )
{
  /*-----------------------------------------------------------------------*/

  ASSERT(msgq != NULL);

  /*-----------------------------------------------------------------------*/

  ASSERT(msgr_client_delete(&msgq->client) == E_SUCCESS);

  ASSERT(pthread_mutex_destroy(&msgq->mutex) == 0);

} /* prose_qtf_msgq_deinit() */

/*===========================================================================

FUNCTION:  prose_qtf_setup

===========================================================================*/
/*!
  @brief
  Sets up the RRC test framework

  @detail
  The following actions are performed
  - Message router is initialized
  - RRC single threaded apartment is created and initialized
  - RRC mailboxes are created
  - UMIDs are registered with the message router
  - Dispatcher table is initialized

  @return
  None.
  */
/*=========================================================================*/
  void prose_qtf_setup
(
 msgr_umid_type *ext_msg_id_list,   /*!< Array of external UMIDs to trap */
 uint8 ext_msg_id_cnt      /*!< Num of UMIDs in above array */
 )
{
  prose_qtf_s *priv = &prose_qtf;
  lte_errno_e ret_status;
  pthread_mutexattr_t mutex_attr;
  msgr_id_t mb_id;

  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/

  // Create a msgr client and test message queue
  // InitTestResources();

#ifndef FEATURE_MCS_QTF_ON_TARGET
  // Create and init the RRC thread
  // appmgr_test_init(PROSE_DEFAULT_APP);
  // ASSERT(appmgr_test_spawn_thread("PROSE", MSGR_PROSE_DISC, prose_task_init, 1) != (qthread_t) 0);

  qtf_rcinit_add_task("prose");
  qtf_rcinit_start();

#endif /* FEATURE_MCS_QTF_ON_TARGET */

  // Create signal queue for receiving signal when input stimuli is processed
  ASSERT(msgr_client_create(&priv->input_proc_client) == E_SUCCESS);

  ASSERT(msgr_client_add_mq("Prose UTF Stimuli",
        &priv->input_proc_client,
        PROSE_QTF_DEFAULT_MSGQ_PRIO,
        PROSE_QTF_MAX_NUM_STIMULI,
        sizeof(msgr_hdr_s),
        &mb_id) == E_SUCCESS);

  ASSERT(msgr_register(MSGR_PROSE_DISC,
        &priv->input_proc_client,
        mb_id,
        PROSE_DISC_STIMULI_SPR) == E_SUCCESS);

  // Initialize the stimuli sent list
  priv->num_stimuli = 0;

  // Create mutex object to synchronize access to stimuli list
  ASSERT(pthread_mutexattr_init(&mutex_attr) == 0);
  ASSERT(pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE) == 0);
  ASSERT(pthread_mutex_init(&priv->input_proc_mutex, &mutex_attr) == 0);

  // Initialize the internal and external message queues
  prose_qtf_msgq_init(&priv->int_msgq, "Prose UTF IntS");
  ASSERT(msgr_register(
        MSGR_PROSE_DISC,
        &priv->int_msgq.client,
        priv->int_msgq.mb_id,
        PROSE_DISC_INT_MSG_SPR) == E_SUCCESS);

  prose_qtf_msgq_init(&priv->ext_msgq, "Prose UTF ExtS");
  ASSERT(msgr_register(
        MSGR_PROSE_DISC,
        &priv->ext_msgq.client,
        priv->ext_msgq.mb_id,
        PROSE_DISC_EXT_MSG_SPR) == E_SUCCESS);

  // If the test wants to capture any external messages
  if (ext_msg_id_cnt > 0)
  {
    // Create a mailbox for receiving external messages
    ASSERT(msgr_client_create(&priv->ext_msgq_client) == E_SUCCESS);

    ASSERT(msgr_client_add_mq("Prose UTF ExtM",
          &priv->ext_msgq_client,
          PROSE_QTF_DEFAULT_MSGQ_PRIO,
          PROSE_QTF_MAX_NUM_EXT_MSGIDS,
          sizeof(prose_qtf_ext_msg_u),
          &priv->ext_msgq_mb_id) == E_SUCCESS);

    priv->ext_msgq_mb_exists = TRUE;

    ret_status = msgr_register_block(
        MSGR_PROSE_DISC,
        &priv->ext_msgq_client,
        priv->ext_msgq_mb_id,
        ext_msg_id_list,
        ext_msg_id_cnt);
    ASSERT(ret_status == E_SUCCESS);
  }
  else
  {
    priv->ext_msgq_mb_exists = FALSE;
  }


#ifndef FEATURE_MCS_QTF_ON_TARGET
  // Unblock the RRC thread
  // appmgr_test_thread_run();
#endif /* FEATURE_MCS_QTF_ON_TARGET */


  //qtf_efs_initialize("modem_prose");  

} /* prose_qtf_setup() */



/*===========================================================================

FUNCTION:  prose_qtf_setup_task

===========================================================================*/
/*!
  @brief
  Sets up the RRC test framework

  @detail
  The following actions are performed
  - Message router is initialized
  - RRC single threaded apartment is created and initialized
  - RRC mailboxes are created
  - UMIDs are registered with the message router
  - Dispatcher table is initialized

  @return
  None.
  */
/*=========================================================================*/
void prose_qtf_setup_task()
{
  // Create a msgr client and test message queue
  // InitTestResources();

  //Initialize EFS for test
  qtf_efs_initialize("modem_prose");

#ifndef FEATURE_MCS_QTF_ON_TARGET
  // Create and init the RRC thread
  // appmgr_test_init(PROSE_DEFAULT_APP);
  // ASSERT(appmgr_test_spawn_thread("PROSE", MSGR_PROSE_DISC, prose_task_init, 1) != (qthread_t) 0);

  qtf_rcinit_add_task("prose");
  qtf_rcinit_start();

  // Start the RRC thread
  // appmgr_test_thread_run();
#endif /* FEATURE_MCS_QTF_ON_TARGET */

  // Wait till RRC task is ready
#if 0
  while(FALSE == lte_rrc_task_is_initialized())
  {
    TF_SLEEP(10);
  }
#endif

} /* prose_qtf_setup_task() */




/*===========================================================================

FUNCTION:  prose_qtf_teardown

===========================================================================*/
/*!
  @brief
  Performs cleanup of the RRC test framework

  @detail
  The following actions are performed
  - RRC deinit is called
  - RRC STA is released
  - Any needed signal queues, signal objects are released

  @return
  None.
  */
/*=========================================================================*/
void prose_qtf_teardown(void)
{
  prose_qtf_s *priv = &prose_qtf;

#ifndef FEATURE_MCS_QTF_ON_TARGET
  // Stop the RRC thread
  // appmgr_test_teardown(PROSE_DEFAULT_APP);
  qtf_rcinit_teardown();

#endif /* FEATURE_MCS_QTF_ON_TARGET */

  // Remove the test resources (including stopping all threads)
  // TeardownTestResources();  

  // Destroy the input-processed-events related signal objects
  ASSERT(pthread_mutex_destroy(&priv->input_proc_mutex) == 0);

  // Destroy the msgr client created for input-processed-events
  ASSERT(msgr_client_delete(&priv->input_proc_client) == E_SUCCESS);

  // Release the ext msg mb if present
  if (priv->ext_msgq_mb_exists == TRUE)
  {
    ASSERT(msgr_client_delete(&priv->ext_msgq_client) == E_SUCCESS);
  }

  // Destroy the internal and external message queues
  prose_qtf_msgq_deinit(&priv->int_msgq);
  prose_qtf_msgq_deinit(&priv->ext_msgq);

  //Delete EFS test tree
  qtf_efs_teardown();

} /* prose_qtf_teardown() */


/*===========================================================================

FUNCTION:  prose_qtf_teardown_task

===========================================================================*/
/*!
  @brief
  Performs cleanup of the RRC test framework

  @detail
  The following actions are performed
  - RRC deinit is called
  - RRC STA is released
  - Any needed signal queues, signal objects are released

  @return
  None.
  */
/*=========================================================================*/
void prose_qtf_teardown_task(void)
{
#ifndef FEATURE_MCS_QTF_ON_TARGET
  // Stop the RRC thread
  // appmgr_test_teardown(PROSE_DEFAULT_APP);
  qtf_rcinit_teardown();

#endif /* FEATURE_MCS_QTF_ON_TARGET */

  // Remove the test resources (including stopping all threads)
  // TeardownTestResources();  

  //Delete EFS test tree
  qtf_efs_teardown();

} /* prose_qtf_teardown_task() */


/*===========================================================================

FUNCTION:  prose_qtf_process_input

===========================================================================*/
/*!
  @brief
  Common handler for processing any input sent to the common test state machine

  @return
  Current state. Common test state machine has only one state.
  */
/*=========================================================================*/
void prose_qtf_process_input(msgr_hdr_s *msg_ptr)
{
  prose_qtf_s *priv = &prose_qtf;
  boolean match_found;
  int i, j;
  msgr_hdr_s int_sig;

  ASSERT(msg_ptr != NULL);

  match_found = FALSE;

  // Enter the critical section, all mods to the stimuli list are atomic
  ASSERT(pthread_mutex_lock(&priv->input_proc_mutex) == 0);

  // Check to see if the incoming message is really a stimuli we had sent
  for (i = 0; i < priv->num_stimuli; i++)
  {
    if (priv->stimuli_list[i] == msg_ptr->id)
    {
      // Incoming message matches a stimuli
      // Delete it from the stimuli list (copy all subsequent array elements
      // one position up)
      for (j = i+1; j < priv->num_stimuli; j++)
      {
        ASSERT(j < PROSE_QTF_MAX_NUM_STIMULI);
        priv->stimuli_list[j - 1] = priv->stimuli_list[j];
      }

      // Decrement the stimuli count
      priv->num_stimuli--;

      match_found = TRUE;
      break;
    }
  }

  // If we received a legitimate message
  if (match_found == FALSE)
  {
    // Copy to internal message queue
    prose_qtf_queue_put(&priv->int_msgq, (byte*)msg_ptr, sizeof(prose_qtf_msg_u));

    TF_MSG("Rcvd msg, q_len=%u, head=%u, tail=%u", 
        priv->int_msgq.queue.num_items, priv->int_msgq.queue.head_index,
        priv->int_msgq.queue.tail_index);

    // Signal the test thread (event on int msgq)
    msgr_init_hdr(&int_sig, MSGR_PROSE_DISC, PROSE_DISC_INT_MSG_SPR);
    ASSERT(msgr_send(&int_sig, sizeof(int_sig)) == E_SUCCESS);
  }

  // Else, we received a loopbacked stimuli
  else
  {
    // Signal the test thread (received a stimuli)
    msgr_init_hdr(&int_sig, MSGR_PROSE_DISC, PROSE_DISC_STIMULI_SPR);
    ASSERT(msgr_send(&int_sig, sizeof(int_sig)) == E_SUCCESS);
  }

  // Leave the critical section
  ASSERT(pthread_mutex_unlock(&priv->input_proc_mutex) == 0);

} /* prose_qtf_process_input() */


/*===========================================================================

FUNCTION:  prose_qtf_get_next_int_msg

===========================================================================*/
/*!
  @brief
  Gets the next message received by the unit test framework. This call blocks
  indefinitely.

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_get_next_int_msg(byte **buf_ptr, uint32 *buf_len)
{
  prose_qtf_s *priv = &prose_qtf;
  msgr_hdr_s int_sig;
  uint32 rcv_msg_size;

  /*-----------------------------------------------------------------------*/

  ASSERT(buf_ptr != NULL);

  /*-----------------------------------------------------------------------*/

  // Wait for stimuli to be processed
  prose_qtf_wait_for_done();

  // Block on the signal queue
  ASSERT(msgr_receive(&priv->int_msgq.client, (uint8*) &int_sig, sizeof(int_sig),
        &rcv_msg_size) == E_SUCCESS);

  prose_qtf_queue_get(&priv->int_msgq, buf_ptr, buf_len);

} /* prose_qtf_get_next_int_msg() */


/*===========================================================================

FUNCTION:  prose_qtf_wait_for_next_ext_msg

===========================================================================*/
/*!
  @brief
  This call blocks indefinitely till UTF receives an ext message from RRC.
  The msg is left untouched in the UTF internal queue. 

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_wait_for_next_ext_msg
(
)
{
  prose_qtf_s *priv = &prose_qtf;
  prose_qtf_msg_u rcv_msg;
  uint32 rcv_msg_size;
  lte_errno_e status;

  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/

  // Wait for stimuli to be processed
  prose_qtf_wait_for_done();

  if (priv->ext_msgq.queue.num_items == 0)
  {
    // Block on external msgq
    status = msgr_receive(&priv->ext_msgq_client, 
        (uint8*) &rcv_msg,
        sizeof(rcv_msg),
        &rcv_msg_size);
    ASSERT(status == E_SUCCESS);

    // Copy to internal message queue
    prose_qtf_queue_put(&priv->ext_msgq, (byte*) &rcv_msg, rcv_msg_size);
  }

} /* prose_qtf_wait_for_next_ext_msg() */



/*===========================================================================

FUNCTION:  prose_qtf_get_next_ext_msg

===========================================================================*/
/*!
  @brief
  Gets the next message received by the unit test framework. This call blocks
  indefinitely.

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_get_next_ext_msg
(
 byte   **buf_ptr,     /*!< Ptr to the message buffer */
 uint32 *buf_len       /*!< Length of the buffer in bytes */
 )
{
  prose_qtf_s *priv = &prose_qtf;

  /*-----------------------------------------------------------------------*/

  ASSERT(buf_ptr != NULL);

  /*-----------------------------------------------------------------------*/

  prose_qtf_wait_for_next_ext_msg();
  prose_qtf_queue_get(&priv->ext_msgq, buf_ptr, buf_len);

  uint32 *umid_id = (uint32*)(*buf_ptr);

  TF_MSG("\nretrieved external umid = 0x%x\n",*umid_id);

} /* prose_qtf_get_next_ext_msg() */



/*===========================================================================

FUNCTION:  prose_qtf_send_msg

===========================================================================*/
/*!
  @brief
  Sends a test stimuli to the state machine under test.

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_send_msg
(
 byte      *msg_ptr,   /*!< Ptr to msg buffer to send */
 uint32    msg_len     /*!< Size of the msg in bytes */
 )
{
  prose_qtf_s *priv = &prose_qtf;
  uint32 msg_id;
  msgr_hdr_s *msg_hdr_ptr;

  /*-----------------------------------------------------------------------*/

  ASSERT(msg_ptr != NULL);
  ASSERT(msg_len > 0);

  /*-----------------------------------------------------------------------*/

  // Retrive the UMID from the msg being sent
  msg_hdr_ptr = (msgr_hdr_s*)(void*) msg_ptr;
  msg_id = msg_hdr_ptr->id;

  // Enter the critical section, all mods to the stimuli list are atomic
  ASSERT(pthread_mutex_lock(&priv->input_proc_mutex) == 0);

  // Store the sent stimuli id
  priv->stimuli_list[priv->num_stimuli] = msg_id;
  priv->num_stimuli++;

  // Leave the critical section
  ASSERT(pthread_mutex_unlock(&priv->input_proc_mutex) == 0);

  ASSERT(msgr_send((msgr_hdr_struct_type*)(void*) msg_ptr, msg_len) == E_SUCCESS);

} /* prose_qtf_send_msg() */


/*===========================================================================

FUNCTION:  prose_qtf_wait_for_done

===========================================================================*/
/*!
  @brief
  Blocks till all the stimuli sent to the state-machine-under-test have been
  processed.

  @detail
  - The stimuli should have been sent using prose_qtf_send_msg
  - If any other messages are received by the unit test framework prior to
  the complete notification message, this function will ASSERT
  - In other words, this function should be called only when the state machine
  under test is not expected to send any explicit messages back to the test code

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_wait_for_done()
{
  prose_qtf_s *priv = &prose_qtf;
  boolean done = FALSE;
  msgr_hdr_s int_sig;
  uint32 rcv_msg_size;
  lte_errno_e status;

  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/

  while (1)
  {
    // If all the stimuli have already been processed, we can return immediately
    // Enter the critical section, all mods to the stimuli list are atomic
    ASSERT(pthread_mutex_lock(&priv->input_proc_mutex) == 0);

    if (priv->num_stimuli == 0)
    {
      // All stimuli that were sent have been processed, we can return
      // Drain the signal queue
      do
      {
        status = msgr_receive_nonblock(&priv->input_proc_client, 
            (uint8*) &int_sig, 
            sizeof(int_sig),
            &rcv_msg_size);

      } while (status == E_SUCCESS);

      ASSERT(status == E_NO_DATA);
      done = TRUE;
    }

    // Leave the critical section
    ASSERT(pthread_mutex_unlock(&priv->input_proc_mutex) == 0);

    if (done == TRUE) break;

    // Block on the input proc signal queue
    ASSERT(msgr_receive(&priv->input_proc_client, (uint8*) &int_sig, sizeof(int_sig),
          &rcv_msg_size) == E_SUCCESS);

  }


} /* prose_qtf_wait_for_done() */


/*===========================================================================

FUNCTION:  prose_qtf_reset

===========================================================================*/
/*!
  @brief
  Resets the unit test framework.

  @note
  This call blocks till all the sent stimuli have been processed.

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_reset
(
)
{
  prose_qtf_s *priv = &prose_qtf;

  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/

  prose_qtf_wait_for_done();

  // Reset the int and ext msg queues
  prose_qtf_queue_reset(&priv->int_msgq);
  prose_qtf_queue_reset(&priv->ext_msgq);

  // Reset the stimuli list
  priv->num_stimuli = 0;


} /* prose_qtf_reset() */


/*===========================================================================

FUNCTION:  prose_qtf_get_queue_counts

===========================================================================*/
/*!
  @brief
  Returns the number of currently queued msgs in the internal and external
  queues.

  @note
  This function returns the current snapshot of the queue. The queue count
  can increase if more messages are subsequently received from the state 
  machine under test.

  @return
  None
  */
/*=========================================================================*/
  void prose_qtf_get_queue_counts
(
 uint8 *int_queue_count,  /*!< Number of elements in the internal queue */
 uint8 *ext_queue_count   /*!< Number of elements in the external queue */
 )
{
  prose_qtf_s *priv = &prose_qtf;

  /*-----------------------------------------------------------------------*/

  ASSERT(int_queue_count != NULL);
  ASSERT(ext_queue_count != NULL);

  /*-----------------------------------------------------------------------*/

  *int_queue_count = priv->int_msgq.queue.num_items;
  *ext_queue_count = priv->ext_msgq.queue.num_items;

} /* prose_qtf_get_queue_counts() */


/*===========================================================================

FUNCTION:  prose_qtf_queue_reset

===========================================================================*/
/*!
  @brief
  Initializes the internal message queue

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_queue_reset
(
 prose_qtf_msgq_s *msgq   /*!< Ptr to the msgq to initialize */  
 )
{
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/

  // Enter the critical section, all mods to the msg queue are atomic
  ASSERT(pthread_mutex_lock(&msgq->mutex) == 0);

  msgq->queue.num_items = 0;
  msgq->queue.num_outstanding = 0;
  msgq->queue.head_index = 0;
  msgq->queue.tail_index = 0;

  // Leave the critical section
  ASSERT(pthread_mutex_unlock(&msgq->mutex) == 0);

} /* prose_qtf_queue_reset() */


/*===========================================================================

FUNCTION:  prose_qtf_queue_put

===========================================================================*/
/*!
  @brief
  Adds a msg to the tail of the message queue

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_queue_put
(
 prose_qtf_msgq_s *msgq,   /*!< Ptr to the msgq to initialize */  
 byte    *buf_ptr,                  /*!< Ptr to the msg buffer */
 uint32  buf_len                    /*!< Length of the buffer */
 )
{
  uint8 put_index;

  /*-----------------------------------------------------------------------*/

  ASSERT(buf_ptr != NULL);
  ASSERT(buf_len != 0);

  // Guard against buffer overflow
  ASSERT(buf_len <= sizeof(prose_qtf_queue_item_s));

  /*-----------------------------------------------------------------------*/

  // Enter the critical section, all mods to the msg queue are atomic
  ASSERT(pthread_mutex_lock(&msgq->mutex) == 0);

  ASSERT((msgq->queue.num_items + msgq->queue.num_outstanding) < PROSE_QTF_QUEUE_LEN);
  msgq->queue.num_items++;

  put_index = msgq->queue.tail_index;
  msgq->queue.tail_index = (put_index + 1) % PROSE_QTF_QUEUE_LEN;

  memcpy(msgq->queue.items[put_index].data, buf_ptr, buf_len);
  msgq->queue.items[put_index].len = buf_len;

  TF_MSG("Queued msg, q_len=%u, head=%u, tail=%u", 
      msgq->queue.num_items, msgq->queue.head_index,
      msgq->queue.tail_index);

  // Leave the critical section
  ASSERT(pthread_mutex_unlock(&msgq->mutex) == 0);

} /* prose_qtf_queue_put() */


/*===========================================================================

FUNCTION:  prose_qtf_queue_get

===========================================================================*/
/*!
  @brief
  Dequeue a msg from the head of the queue

  @note
  msg_queue.num_items is deliberately not been decremented in this function
  This forces the caller to call init after all buffers have been used

  @return
  None
  */
/*=========================================================================*/
void prose_qtf_queue_get
(
 prose_qtf_msgq_s *msgq, /*!< Ptr to the msgq to initialize */  
 byte   **buf_ptr,         /*!< <OUT> Ptr to the msg buffer, NULL if queue is empty */
 uint32 *buf_len           /*!< <OUT> Length of buffer in bytes */
 )
{
  /*-----------------------------------------------------------------------*/

  ASSERT(buf_ptr != NULL);

  /*-----------------------------------------------------------------------*/

  // Enter the critical section, all mods to the msg queue are atomic
  ASSERT(pthread_mutex_lock(&msgq->mutex) == 0);

  if (msgq->queue.num_items == 0)
  {
    *buf_ptr = NULL;
  }
  else
  {
    *buf_ptr = (byte*) msgq->queue.items[msgq->queue.head_index].data;
    *buf_len = msgq->queue.items[msgq->queue.head_index].len;
    msgq->queue.head_index = (msgq->queue.head_index + 1) % PROSE_QTF_QUEUE_LEN;
    msgq->queue.num_items--;

    TF_MSG("Delivered msg, q_len=%u, head=%u, tail=%u", 
        msgq->queue.num_items, msgq->queue.head_index,
        msgq->queue.tail_index);
  }

  // Leave the critical section
  ASSERT(pthread_mutex_unlock(&msgq->mutex) == 0);

} /* prose_qtf_queue_get() */


/*===========================================================================

FUNCTION:  prose_qtf_check_no_more_msgs

===========================================================================*/
/*!
  @brief
  ASSERTs if there are any messages received by UTF (internal or external)
  in the next 100ms. Can be used by test code to verify that the module
  under test has stabilized.

  @return
  None.
  */
/*=========================================================================*/
void prose_qtf_check_no_more_msgs()
{
  prose_qtf_s *priv = &prose_qtf;
  prose_qtf_msg_u rcv_msg;
  uint32 rcv_msg_size;
  lte_errno_e status;

  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/

  // Sleep for 100ms..
  CTestFramework::YieldProc(100);

  // If msgs were received on the int_msgq, the queue count will have been
  // updated by now
  ASSERT(priv->int_msgq.queue.num_items == 0);
  ASSERT(priv->ext_msgq.queue.num_items == 0);

  // Make non-blocking call to receive on ext msgq
  if(priv->ext_msgq_client != 0)
  {
    status = msgr_receive_nonblock(&priv->ext_msgq_client, 
        (uint8*) &rcv_msg,
        sizeof(rcv_msg),
        &rcv_msg_size);
    ASSERT(status == E_NO_DATA);
  }

} /* prose_qtf_check_no_more_msgs() */

