/*!
  @file
  prose_qtf.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/common/prose_qtf.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_QTF_H
#define PROSE_QTF_H

#include <msgr_umid.h>

#include "prose_ext_msg.h"
#include "ds_lted_ext_msg.h"
#include "lte_mac_msg.h"
#include "lte_rrc_ext_msg.h"

void prose_qtf_setup(msgr_umid_type *ext_msg_id_list, uint8 ext_msg_id_cnt);

void prose_qtf_teardown(void);

void prose_qtf_setup_task(void);

void prose_qtf_teardown_task(void);

void prose_qtf_send_msg(byte *msg_ptr, uint32 msg_len);

void prose_qtf_get_next_int_msg(byte **buf_ptr, uint32 *buf_len);

void prose_qtf_get_next_ext_msg(byte **buf_ptr, uint32 *buf_len);

void prose_qtf_wait_for_next_ext_msg(void);

void prose_qtf_wait_for_done(void);

void prose_qtf_reset(void);

void prose_qtf_get_queue_counts(uint8 *int_queue_count, uint8 *ext_queue_count);

void prose_qtf_check_no_more_msgs(void);

void* prose_qtf_get_payload_ptr(byte *msg_ptr, uint32 msg_len);

void prose_qtf_process_input(msgr_hdr_s *msg_ptr);

typedef union
{
  prose_disc_publish_req_s publish_req;
  prose_disc_publish_cancel_req_s pub_cancel_req;
  prose_disc_subscribe_req_s subs_req;
  prose_disc_subscribe_cancel_req_s subs_cancel_req;
  prose_disc_set_config_req_s set_config;
  prose_disc_get_config_req_s get_config;
  prose_disc_set_category_req_s set_cat;
  prose_disc_get_category_req_s get_cat;
  prose_disc_terminate_req_s terminate_req;
  ds_appsrv_lte_d_post_req_id_ind_s post_req_id;
  ds_appsrv_lte_d_post_result_ind_s post_result;
  ds_appsrv_srvreq_result_ind_s srvreq_id;
  lte_mac_pac_add_cnf_s pac_add;
  lte_mac_filter_add_cnf_s filter_add;
  lte_mac_pac_match_rpt_ind_s match_rpt;
  lte_mac_pac_tx_status_rpt_ind_s tx_status_rpt;
  lte_rrc_lted_available_ind_s lted_avail;
  lte_rrc_lted_not_available_ind_s lted_not_avail;
  lte_rrc_conn_rel_ind_s conn_rel_ind;
  lte_rrc_lted_plmn_earfcn_ind_s plmn_earfcn_ind;
} prose_ext_msg_u;

typedef union
{
  msgr_hdr_s                 msgr_hdr;
  prose_disc_set_config_rsp_s set_config_rsp;
  prose_disc_get_config_rsp_s get_config_rsp;
  prose_disc_set_category_rsp_s set_cat_rsp;
  prose_disc_terminate_rsp_s terminate_rsp;
  prose_disc_get_service_status_rsp_s get_service_status_rsp;
  prose_disc_publish_rsp_s   publish_rsp;
  prose_disc_publish_cancel_rsp_s publish_cancel_rap;
  prose_disc_subscribe_rsp_s subscribe_rsp;
  prose_disc_subscribe_cancel_rsp_s subscribe_cancel_rsp;
  prose_disc_match_event_ind_s match_event;
  prose_disc_broadcast_notification_ind_s broadcast_notif;
  prose_disc_notification_ind_s notif;
} prose_qtf_test_msg_u;

typedef union
{
  prose_ext_msg_u         ext_msg;
  prose_qtf_test_msg_u    test_msg;
} prose_qtf_ext_msg_u;

typedef union
{
  msgr_hdr_s          msgr_hdr;
  prose_qtf_ext_msg_u ext_msg;

} prose_qtf_msg_u;

// Todo: Change prose_ext_msg_u to an enum of ext msg
#define PROSE_QTF_QUEUE_SLOT_SIZE (sizeof(prose_qtf_ext_msg_u))

/*! @brief Static circular queue item
*/
typedef struct
{
  // Note we define a uint32 array to get the data buffer word-aligned
  uint32 data[(PROSE_QTF_QUEUE_SLOT_SIZE / 4) + 1];
  uint32 len;

} prose_qtf_queue_item_s;


// Maximum number of simultaneous messages expected to be received 
// from the state machine under test
#define PROSE_QTF_QUEUE_LEN           100

/*! @brief Static circular queue
*/
typedef struct
{
  /*!< Number of items in the queue */
  uint8 num_items;

  /*!< Number of queue items still being used (not yet freed) */
  uint8 num_outstanding;

  /*!< Array of ptrs to queue items */
  prose_qtf_queue_item_s items[PROSE_QTF_QUEUE_LEN];

  /*!< Index to one more than the last item (index to push next item) */
  uint8 tail_index;

  /*!< Index to the next item to pop */
  uint8 head_index;

} prose_qtf_queue_s;


/*! @brief Encapsulates elements required for buffering received messages
*/
typedef struct
{
  /*!< Signal queue for rcvd msg events */
  msgr_client_t client;

  /*!< Mutex to synchronize access to message queue */
  pthread_mutex_t mutex;

  /*!< Queue of received messages */
  prose_qtf_queue_s queue;

  /*!< Mailbox id */
  msgr_id_t mb_id;

} prose_qtf_msgq_s;


// Maximum number of stimuli that can be simultaneously sent to the 
// state machine under test
#define PROSE_QTF_MAX_NUM_STIMULI     100

/*! @brief Typedef of variables internal to module prose_qtf.cpp
*/
typedef struct
{
  /*!< Critical section to synch access to stimuli list */
  pthread_mutex_t   input_proc_mutex;

  /*!< Queue for storing list of stimuli already sent */
  msgr_client_t     input_proc_client;
  uint32            stimuli_list[PROSE_QTF_MAX_NUM_STIMULI];
  uint8             num_stimuli;            /*!< Number of stimuli already sent */

  prose_qtf_msgq_s int_msgq;      /*!< Queue of received internal messages */
  prose_qtf_msgq_s ext_msgq;      /*!< Queue of received external messages */

  msgr_client_t    ext_msgq_client;    /*!< Mailbox to receive external messages */
  msgr_id_t        ext_msgq_mb_id;     /*!< Mailbox id for external msgq */
  boolean          ext_msgq_mb_exists; /*!< Flag to indicate if ext msg mb was allocated */

} prose_qtf_s;


void prose_qtf_queue_put(prose_qtf_msgq_s *msgq, byte *buf_ptr, uint32 buf_len);

void prose_qtf_queue_get(prose_qtf_msgq_s *msgq, byte **buf_ptr, uint32 *buf_len);

void prose_qtf_queue_reset(prose_qtf_msgq_s *msgq);

#endif /* PROSE_QTF_H */

