/*!
  @file
  prose_states.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/states/inc/prose_states.h#7 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_STATES_H
#define PROSE_STATES_H

#include "state.h"
#include "prose_event_handler.h"

class StateMachine;

class ProseRootState : public State, public ProseEventHandler
{
  public:
    static ProseRootState *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_disc_publish_req(const prose_disc_publish_req_s*, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_req(const prose_disc_subscribe_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_announce_rsp(const AnnounceRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_reject_rsp(const RejectRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pac_add_cnf(const lte_mac_pac_add_cnf_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_cancel_req(const prose_disc_publish_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_monitor_rsp(const MonitorRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_filter_add_cnf(const lte_mac_filter_add_cnf_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pac_match_rpt_ind(const lte_mac_pac_match_rpt_ind_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pac_tx_status_rpt_ind(const lte_mac_pac_tx_status_rpt_ind_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_cancel_req(const prose_disc_subscribe_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_match_ack(const MatchAck_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_match_rej(const MatchReject_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_conn(const void*, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_xml_failure(const void*, StateMachine *sm);
    virtual prose_event_status_e on_pre_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_pre_t4002_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4002_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4004_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4006_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_hangover_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseRootState() : State("ProseRootState", NULL) {}
    ProseRootState(const ProseRootState&);
    ProseRootState& operator=(const ProseRootState&);

    static ProseRootState *instance_;
};

class ProseAwaitPAC : public State, public ProseEventHandler
{
  public:
    static ProseAwaitPAC *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_pc3_announce_rsp(const AnnounceRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_reject_rsp(const RejectRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_xml_failure(const void*, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_cancel_req(const prose_disc_publish_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_req(const prose_disc_publish_req_s*, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseAwaitPAC(State *parent) : State("ProseAwaitPAC", parent) {}
    ProseAwaitPAC(const ProseAwaitPAC&);
    ProseAwaitPAC& operator=(const ProseAwaitPAC&);

    static ProseAwaitPAC *instance_;
};

class ProseAwaitConn : public State, public ProseEventHandler
{
  public:
    static ProseAwaitConn *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_conn(const void*, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_cancel_req(const prose_disc_publish_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_req(const prose_disc_publish_req_s*, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_pre_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseAwaitConn(State *parent) : State("ProseAwaitConn", parent) {}
    ProseAwaitConn(const ProseAwaitConn&);
    ProseAwaitConn& operator=(const ProseAwaitConn&);

    static ProseAwaitConn *instance_;
};

class ProseTransmitPAC : public State, public ProseEventHandler
{
  public:
    static ProseTransmitPAC *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_pac_add_cnf(const lte_mac_pac_add_cnf_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pac_tx_status_rpt_ind(const lte_mac_pac_tx_status_rpt_ind_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_cancel_req(const prose_disc_publish_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_publish_req(const prose_disc_publish_req_s*, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_pre_t4000_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseTransmitPAC(State *parent) : State("ProseTransmitPAC", parent) {}
    ProseTransmitPAC(const ProseTransmitPAC&);
    ProseTransmitPAC& operator=(const ProseTransmitPAC&);

    static ProseTransmitPAC *instance_;
};

class ProseAwaitFilters : public State, public ProseEventHandler
{
  public:
    static ProseAwaitFilters *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_pc3_monitor_rsp(const MonitorRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_reject_rsp(const RejectRsp_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_xml_failure(const void*, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_cancel_req(const prose_disc_subscribe_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_req(const prose_disc_subscribe_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4002_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseAwaitFilters(State *parent) : State("ProseAwaitFilters", parent) {}
    ProseAwaitFilters(const ProseAwaitFilters&);
    ProseAwaitFilters& operator=(const ProseAwaitFilters&);

    static ProseAwaitFilters *instance_;
};

class ProseMonitorPAC : public State, public ProseEventHandler
{
  public:
    static ProseMonitorPAC *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_filter_add_cnf(const lte_mac_filter_add_cnf_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pac_match_rpt_ind(const lte_mac_pac_match_rpt_ind_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_cancel_req(const prose_disc_subscribe_cancel_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_disc_subscribe_req(const prose_disc_subscribe_req_s *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm);
    virtual prose_event_status_e on_pre_t4002_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4004_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t4002_expiry(const void *msg, StateMachine *sm);
    virtual prose_event_status_e on_t_hangover_expiry(const void*, StateMachine *sm);
    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm);

  private:
    explicit ProseMonitorPAC(State *parent) : State("ProseMonitorPAC", parent) {}
    ProseMonitorPAC(const ProseMonitorPAC&);
    ProseMonitorPAC& operator=(const ProseMonitorPAC&);

    static ProseMonitorPAC *instance_;
};

class ProseAwaitMatchReport : public State, public ProseEventHandler
{
  public:
    static ProseAwaitMatchReport *get_instance();

    static void destroy();

  protected:
    virtual void on_initialize(StateMachine *sm);
    virtual void on_entry(StateMachine *sm);
    virtual void on_exit(StateMachine *sm);
    virtual void on_terminate(StateMachine *sm);

    virtual prose_event_status_e on_pc3_match_ack(const MatchAck_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_pc3_match_rej(const MatchReject_info *msg_ptr, StateMachine *sm);
    virtual prose_event_status_e on_xml_failure(const void*, StateMachine *sm);

  private:
    explicit ProseAwaitMatchReport(State *parent) : State("ProseAwaitMatchReport", parent) {}
    ProseAwaitMatchReport(const ProseAwaitMatchReport&);
    ProseAwaitMatchReport& operator=(const ProseAwaitMatchReport&);

    static ProseAwaitMatchReport *instance_;
};

#endif /* PROSE_STATES_H */
