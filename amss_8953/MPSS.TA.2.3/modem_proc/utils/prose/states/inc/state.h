/*!
  @file
  states.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/states/inc/state.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef STATE_H
#define STATE_H

#include <assert.h>

class StateMachine;

enum prose_event_status_e {EVENT_HANDLED, EVENT_NOT_HANDLED};

class State
{
  public:

    const char *get_state_name() const
    {
      return state_name_;
    }

    template <typename EVENT_HANDLER, typename EVENT>
      bool process_event(const EVENT* event, prose_event_status_e (EVENT_HANDLER::*event_handler)(const EVENT*, StateMachine*), StateMachine* sm, bool state_is_handler = false)
      {
        bool handled = false;

        EVENT_HANDLER *state = state_is_handler ? reinterpret_cast<EVENT_HANDLER*>(this) :
          dynamic_cast<EVENT_HANDLER*>(this);

        if (state != 0)
        {
          handled = (state->*event_handler)(event, sm) == EVENT_HANDLED;
        }
        return handled;
      }

  protected:
    explicit State(const char *name, State *parent) : state_name_(name), parent_(parent), level_(parent == 0 ? 1 : parent->level_ + 1), magic_(CTOR_MAGIC_VALUE)
    {
      ASSERT(!parent || parent->magic_ == CTOR_MAGIC_VALUE);
    }

    virtual ~State() { const_cast<int&>(magic_) = 0;}

    virtual void on_entry(StateMachine *sm) {};

    virtual void on_exit(StateMachine *sm) {};

    virtual void on_initialize(StateMachine *sm) {};

    virtual void on_terminate(StateMachine *sm) {};

  private:
    State(const State&);
    State& operator=(const State&);

    const char *state_name_;

    State* const parent_;

    const int level_;

    const int magic_;

    static const int CTOR_MAGIC_VALUE = 0xDEADBEEF;

    friend class StateMachine;
};

#endif /* STATE_H */
