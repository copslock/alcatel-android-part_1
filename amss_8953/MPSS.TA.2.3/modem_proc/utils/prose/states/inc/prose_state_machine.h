/*!
  @file
  prose_state_machine.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/states/inc/prose_state_machine.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_STATE_MACHINE_H
#define PROSE_STATE_MACHINE_H

#include "state_machine.h"

#include "prose_event_handler.h"
#include "prose_timer_pool.h"

class ProseStateMachine : public StateMachine
{
  public:

    ProseStateMachine(State *initial_state): StateMachine(initial_state) {}

    virtual ~ProseStateMachine() {}

    void initialize() { start(); }
    void terminate() {}

    void send_event(const prose_disc_publish_req_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_disc_publish_req);
    }

    void send_event(const prose_disc_publish_cancel_req_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_disc_publish_cancel_req);
    }

    void send_event(const prose_disc_subscribe_req_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_disc_subscribe_req);
    }

    void send_event(const prose_disc_subscribe_cancel_req_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_disc_subscribe_cancel_req);
    }

    void send_event(const AnnounceRsp_info *msg)
    {
      process_event(msg, &ProseEventHandler::on_pc3_announce_rsp);
    }

    void send_event(const MonitorRsp_info *msg)
    {
      process_event(msg, &ProseEventHandler::on_pc3_monitor_rsp);
    }

    void send_event(const RejectRsp_info *msg)
    {
      process_event(msg, &ProseEventHandler::on_pc3_reject_rsp);
    }

    void send_event(const MatchAck_info *msg)
    {
      process_event(msg, &ProseEventHandler::on_pc3_match_ack);
    }

    void send_event(const MatchReject_info *msg)
    {
      process_event(msg, &ProseEventHandler::on_pc3_match_rej);
    }

    void send_event(const lte_mac_pac_add_cnf_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_pac_add_cnf);
    }

    void send_event(const lte_mac_filter_add_cnf_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_filter_add_cnf);
    }

    void send_event(const lte_mac_pac_match_rpt_ind_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_pac_match_rpt_ind);
    }

    void send_event(const lte_mac_pac_tx_status_rpt_ind_s *msg)
    {
      process_event(msg, &ProseEventHandler::on_pac_tx_status_rpt_ind);
    }

    void send_event_conn()
    {
      process_event((void*)0, &ProseEventHandler::on_conn);
    }

    void send_event_auth_change()
    {
      process_event((void*)0, &ProseEventHandler::on_auth_change);
    }

    void send_event_xml_failure(uint32 msg)
    {
      process_event((void*)msg, &ProseEventHandler::on_xml_failure);
    }

    void send_event_pre_t4000_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_pre_t4000_expiry);
    }

    void send_event_t4000_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t4000_expiry);
    }

    void send_event_pre_t4002_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_pre_t4002_expiry);
    }

    void send_event_t4002_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t4002_expiry);
    }

    void send_event_t4004_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t4004_expiry);
    }

    void send_event_t4006_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t4006_expiry);
    }

    void send_event_t_hangover_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t_hangover_expiry);
    }

    void send_event_t_session_expiry(TimerPool::timer_id msg)
    {
      process_event((void*)&msg, &ProseEventHandler::on_t_session_expiry);
    }

  private:
    ProseStateMachine(const ProseStateMachine&);
    ProseStateMachine& operator=(const ProseStateMachine&);
};

#endif /* PROSE_STATE_MACHINE_H */
