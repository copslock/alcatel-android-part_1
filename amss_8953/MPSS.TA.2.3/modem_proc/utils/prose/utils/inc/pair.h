/*!
  @file
  pair.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/utils/inc/pair.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PAIR_H
#define PAIR_H

template<typename T1, typename T2>
class pair
{
  public:
    pair() :
      first(), second()
    {}
    pair(const pair<T1, T2>& src) :
      first(src.first), second(src.second)
    {}
    template<typename U1, typename U2>
    pair(const pair<U1, U2>& src) :
      first(src.first), second(src.second)
    {}
    pair<T1, T2>& operator=(const pair<T1, T2>& rhs)
    {
      if (this != &rhs)
      {
        first = rhs.first;
        second = rhs.second;
      }
      return *this;
    }
    pair(const T1& a, const T2& b) :
      first(a), second(b)
    {}
    T1 first;
    T2 second;
};

template<typename T1, typename T2>
inline bool operator==(const pair<T1, T2>& a, const pair<T1, T2>& b)
{
  return a.first == b.first && a.second == b.second;
}

template<typename T1, typename T2>
inline bool operator!=(const pair<T1, T2>& a, const pair<T1, T2>& b)
{
  return a.first != b.first || a.second != b.second;
}

template<typename T1, typename T2>
pair<T1, T2> make_pair(const T1& a, const T2& b)
{
  return pair<T1, T2>(a, b);
}


#endif /* PAIR_H */
