/*!
  @file
  hash_map.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/utils/inc/hashmap.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef HASHMAP_H
#define HASHMAP_H

#include "list.h"
#include "pair.h"

template<typename T>
class DefaultHash
{
  public:
    unsigned operator()(const T& key) const
    {
      unsigned bytes = sizeof(key);
      unsigned result = 0;
      for (unsigned i = 0; i< bytes; i++)
      {
        unsigned char b = *((unsigned char*)&key + i);
        result += b;

      }
      return result;
    }
};

template<typename T>
struct Equals 
{
  bool operator()(const T& left, const T& right) const
  {
    return (left == right);
  }
};


template<typename Key, typename T, typename Compare = Equals<Key>, typename Hash = DefaultHash<Key> >
class hashmap
{
  public: 
    typedef Key key_type;
    typedef T mapped_type;
    typedef pair<const Key, T> value_type;
    typedef hashmap<Key, T, Compare, Hash> hashmap_type;

  private:
    typedef list<value_type> ListType;

  public:
    explicit hashmap(const Compare& comp = Compare(), unsigned num_buckets = 101,
        const Hash& hash = Hash()) :
      size_(0),
      comp_(comp),
      num_buckets_(num_buckets),
      hash_(hash)
    {
      // assert(num_buckets > 0);
      elems_ = mem_alloc<ListType>(num_buckets_);
    }

    ~hashmap()
    {
      mem_free<ListType>(elems_, num_buckets_);
      elems_ = 0;
      num_buckets_ = 0;
      size_ = 0;
    }

    hashmap(const hashmap<Key, T, Compare, Hash>& src) :
      size_(src.size_),
      comp_(src.comp_),
      num_buckets_(src.num_buckets_),
      hash_(src.hash_)
    {
      elems_ = mem_alloc<ListType>(src.num_buckets_);
      for (unsigned i = 0; i < src.num_buckets_; i++)
      {
        elems_[i] = src.elems_[i];
      }
    }

    hashmap<Key, T, Compare, Hash>& operator=(const hashmap<Key, T, Compare, Hash>& rhs)
    {
      if (this != &rhs)
      {
        mem_free<ListType>(elems_, num_buckets_);
        size_ = rhs.size_;
        num_buckets_ = rhs.num_buckets_;
        comp_ = rhs.comp_;
        hash_ = rhs.hash_;
        elems_ = mem_alloc<ListType>(rhs.num_buckets_);
        for (unsigned i = 0; i < rhs.num_buckets_; i++)
          elems_[i] = rhs.elems_[i];
      }
      return *this;
    }

    unsigned erase(const key_type& x)
    {
      unsigned bucket;
      typename ListType::iterator it = find_element(x, bucket);
      if (it != elems_[bucket].end())
      {
        elems_[bucket].erase(it);
        size_--;
        return 1;
      }
      else
        return 0;
    }

    T& operator[](const key_type& x)
    {
      iterator found = find(x);
      if (found == end())
      {
        insert(pair<const Key, T>(x, T()));
        found = find(x);
      }
      return found->second;
    }

    bool empty() const { return size_ == 0; }
    unsigned size() const { return size_; }

    void clear()
    {
      for (unsigned i = 0; i < num_buckets_; i++)
        elems_[i].clear();
      size_ = 0;
    }

    class const_iterator
    {
      public:
        typedef typename hashmap_type::ListType::const_iterator list_iterator_type;

        const_iterator() :
          bucket_index_(0),
          list_iterator_(list_iterator_type()),
          hashmap_(0)
      {}

        const_iterator(unsigned bucket, list_iterator_type list_it, 
            const hashmap_type* in_hashmap) :
          bucket_index_(bucket),
          list_iterator_(list_it),
          hashmap_(in_hashmap)
      {}

        const value_type& operator*() const
        { return *list_iterator_; }

        const value_type* operator->() const
        { return &(*list_iterator_); }

        const_iterator& operator++()
        { increment(); return *this; }

        const_iterator operator++(int)
        { const_iterator it = *this; increment(); return it; }

        // const_iterator<HashMap>& operator--();
        // const_iterator<HashMap> operator--(int);

        bool operator==(const const_iterator& rhs) const
        { 
          return bucket_index_ == rhs.bucket_index_ &&
            hashmap_ == rhs.hashmap_ &&
            list_iterator_ == rhs.list_iterator_;

        }
        bool operator!=(const const_iterator& rhs) const
        { return !(*this == rhs); }

      protected:
        unsigned bucket_index_;
        list_iterator_type list_iterator_;
        const hashmap_type* hashmap_;

        void increment()
        {
          ++list_iterator_;
          if (list_iterator_ == hashmap_->elems_[bucket_index_].end())
          {
            for (unsigned i = bucket_index_+1; i < hashmap_->num_buckets(); i++)
            {
              if (!hashmap_->elems_[i].empty())
              {
                list_iterator_ = hashmap_->elems_[i].begin();
                bucket_index_ = i;
                return;
              }
            }
            bucket_index_ = hashmap_->num_buckets() - 1;
            list_iterator_ = hashmap_->elems_[bucket_index_].end();
          }
        }

        void decrement();
        friend class hashmap;
    };

    class iterator : public const_iterator
    {
      public:
        typedef typename hashmap_type::ListType::iterator list_iterator_type;

        iterator() : const_iterator()
      {}

        iterator(unsigned bucket, list_iterator_type list_it, hashmap_type* in_hashmap) :
          const_iterator(bucket, list_it, in_hashmap)
      {}

        value_type& operator*()
        { return const_cast<value_type&>(*const_iterator::list_iterator_); }

        value_type* operator->()
        { return const_cast<value_type*>(&(*const_iterator::list_iterator_)); }

        iterator& operator++()
        { const_iterator::increment(); return *this; }

        iterator operator++(int)
        { iterator it = *this; const_iterator::increment(); return it; }

        // iterator<HashMap>& operator--();
        // iterator<HashMap> operator--(int);
        friend class hashmap;
    };

    iterator begin()
    {
      if (size_ == 0)
        return end();
      for (unsigned i = 0; i < num_buckets_; ++i)
      {
        if (!elems_[i].empty())
          return iterator(i, elems_[i].begin(), this);
      }
      return end();
    }

    iterator end()
    {
      unsigned bucket = num_buckets_ - 1;
      return iterator(bucket, elems_[bucket].end(), this);
    }

    const_iterator begin() const
    {
      return const_cast<hashmap_type*>(this)->begin();
    }

    const_iterator end() const
    {
      return const_cast<hashmap_type*>(this)->end();
    }

    pair<iterator, bool> insert(const value_type& x)
    {
      unsigned bucket;
      typename ListType::iterator it = find_element(x.first, bucket);
      bool inserted = false;
      if (it == elems_[bucket].end())
      {
        size_++;
        it = elems_[bucket].insert(elems_[bucket].end(), x);
        inserted = true;
      }
      return make_pair(iterator(bucket, it, this), inserted);
    }

    iterator find(const key_type& x)
    {
      unsigned bucket;
      typename ListType::iterator it = find_element(x, bucket);
      if (it == elems_[bucket].end())
      {
        return end();
      }
      return iterator(bucket, it, this); //&(*it);
    }

    const_iterator find(const key_type& x) const
    {
      unsigned bucket;
      typename ListType::const_iterator it = 
        const_cast<hashmap_type*>(this)->find_element(x, bucket);
      if (it == elems_[bucket].end())
      {
        return end();
      }
      return const_iterator(bucket, it, this); //&(*it);
    }

    iterator erase(iterator pos)
    {
      iterator n = pos;
      ++n;
      elems_[pos.bucket_index_].erase(pos.list_iterator_);
      --size_;
      return n;
    }

  private:
    ListType *elems_;
    unsigned size_;
    Compare comp_;
    unsigned num_buckets_;
    Hash hash_;

    unsigned num_buckets() const { return num_buckets_; }

    typename ListType::iterator find_element(const key_type& x, unsigned& bucket) const
    {
      bucket = hash_(x) % num_buckets_;
      for (typename list<pair<const Key, T> >::iterator
          it = elems_[bucket].begin(); it != elems_[bucket].end(); ++it)
      {
        if (comp_(it->first, x))
        {
          return it;
        }
      }
      return elems_[bucket].end();
    }
};

#endif /* HASHMAP_H */

