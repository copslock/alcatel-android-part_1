/*!
  @file
  mem.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/utils/inc/mem.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_MEM_H
#define PROSE_MEM_H

#include "stringl.h"
#include "modem_mem.h"

#define PROSE_NEW(type_name)                  (new(mem_alloc<type_name >()) type_name)
#define PROSE_NEW_1(type_name,arg1)           (new(mem_alloc<type_name >()) type_name(arg1))
#define PROSE_NEW_2(type_name,arg1,arg2)      (new(mem_alloc<type_name >()) type_name(arg1,arg2))
#define PROSE_NEW_3(type_name,arg1,arg2,arg3) (new(mem_alloc<type_name >()) type_name(arg1,arg2,arg3))
#define PROSE_DELETE(type_name,obj)           (mem_free<type_name >(obj))

#define PROSE_NEW_ARR(type_name,count)        (mem_alloc<type_name >(count))
#define PROSE_DELETE_ARR(type_name,obj,count) (mem_free<type_name >(obj,count))

#define PROSE_MALLOC(size)                    (mem_alloc(size))
#define PROSE_REALLOC(ptr,size)               (mem_realloc(ptr,size))
#define PROSE_STRDUP(str)                     (mem_strdup(str))
#define PROSE_FREE(obj)                       (mem_free(obj)) 

inline
void* operator new(unsigned size, void* alloc) throw()
{
  return alloc;
}

inline
void operator delete(void* obj, void* alloc) throw()
{}

// namespace prose 

static inline
void* mem_alloc(unsigned obj_size)
{
  return modem_mem_alloc(obj_size, MODEM_MEM_CLIENT_UTILS);
}

static inline
void* mem_realloc(void* ptr, unsigned obj_size)
{
  return modem_mem_realloc(ptr, obj_size, MODEM_MEM_CLIENT_UTILS);
}

static inline
void* mem_alloc(unsigned obj_size, unsigned array_size)
{
  if (!obj_size || !array_size)
    return NULL;
  return modem_mem_alloc(obj_size * array_size, MODEM_MEM_CLIENT_UTILS);
}

void mem_free(void*& obj) throw();

static inline char* mem_strdup(const char *s)
{
  unsigned l = 1 + strlen(s);
  char* p = static_cast<char*>(mem_alloc(l));
  return p ? strncpy(p, s, l) : NULL;
}

/* Note: Does not call constructor. Use placement new for constructor call. */
template<typename T>
T* mem_alloc() throw()
{
  return static_cast<T*>(modem_mem_alloc(sizeof(T), MODEM_MEM_CLIENT_UTILS));
}

/* Note: Loops through the array and calls the default constructor. */
template<typename T>
T* mem_alloc(unsigned array_size) throw()
{
  if (!array_size)
    return NULL;

  T* obj = static_cast<T*>(modem_mem_alloc(sizeof(T) * array_size, MODEM_MEM_CLIENT_UTILS));
  if (obj)
  {
    for (unsigned i = 0; i < array_size; ++i)
    {
      new((void*)&(obj[i])) T();
    }
  }
  return obj;
}

template<typename T>
void mem_free(T*& obj) throw()
{
  if (obj)
  {
    obj->~T();
    modem_mem_free((void*)obj, MODEM_MEM_CLIENT_UTILS);
  }
  obj = NULL;
}

template<typename T>
void mem_free(T*& obj, unsigned array_size) throw()
{
  if (obj)
  {
    for(unsigned i = 0; i < array_size; ++i)
    {
      obj[i].~T();
    }
    modem_mem_free((void*)obj, MODEM_MEM_CLIENT_UTILS);
  }
  obj = NULL;
}

/* Specialization for void type */
/* Allocators return NULL because void has size 0 */
template<>
inline void* mem_alloc() throw()
{
  return NULL;
}

template<>
inline void* mem_alloc(unsigned int array_size) throw()
{
  return NULL;
}

/* Deallocator specialization for void since destructor can't be called on void type */
template<>
inline void mem_free(void*& obj) throw()
{
  if (obj)
  {
    modem_mem_free(obj, MODEM_MEM_CLIENT_UTILS);
  }
  obj = NULL;
}

/* Deallocator specialization for void since destructor can't be called on void type */
template<>
inline void mem_free(void*& obj, unsigned int array_size) throw()
{
  if (obj)
  {
    modem_mem_free(obj, MODEM_MEM_CLIENT_UTILS);
  }
  obj = NULL;
}

#endif /* PROSE_MEM_H */

