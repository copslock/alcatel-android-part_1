/*!
  @file
  object_pool.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/utils/inc/object_pool.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef OBJECT_POOL_H
#define OBJECT_POOL_H

#include "list.h"

template<class T>
class ObjectPool
{
  public:
    ObjectPool(unsigned object_count = default_size_)
    {
      if (object_count == 0) return;
      pool_ = mem_alloc<T>(object_count);
      ASSERT(pool_);
      for (unsigned i = 0; i < object_count; i++)
      {
        free_list_.push_back(&pool_[i]);
      }
      object_count_ = object_count;
    }

    ~ObjectPool()
    {
      mem_free<T>(pool_, object_count_);
      free_list_.clear();
      pool_ = NULL;
    }

    T* acquire_object()
    {
      T *object = NULL;
      if (pool_ && !free_list_.empty())
      {
        object = free_list_.front();
        free_list_.pop_front();
      }
      return object;
    }

    void release_object(T* object)
    {
      if (pool_)
        free_list_.push_front(object);
    }

    T* operator[](unsigned n)
    {
      T *object = NULL;
      if (pool_ && n < object_count_)
        object = &pool_[n];
      return object;
    }

  private:

    ObjectPool(const ObjectPool<T>&);
    ObjectPool<T>& operator=(const ObjectPool<T>&);

    void allocate_pool();

    static const unsigned default_size_;

    unsigned object_count_;

    list<T*> free_list_;

    T* pool_;
    
};

#endif /* OBJECT_POOL_H */
