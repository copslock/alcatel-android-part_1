/*!
  @file
  MatchAck_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/MatchAck_info.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef MatchAck_info_H
#define MatchAck_info_H

#include "xml_list.h"
#include "xml_element.h"

class anyExtType;

class MatchAck_info : public XmlElement
{
  private:
    int        transaction_id_;
    char       *prose_application_id_;
    int        validity_timer_t4004_;
    char       *metadata_;
    char       *metadata_index_mask_;
    anyExtType *anyExt_;
    slist      *any_;
    int        match_report_refresh_timer_t4006_;
    slist      *anyAttribute_;

    void clear_transaction_id();
    void clear_prose_application_id();
    void clear_validity_timer_t4004();
    void clear_metadata();
    void clear_metadata_index_mask();
    void clear_anyExt();
    void clear_any();
    void clear_match_report_refresh_timer_t4006();
    void clear_anyAttribute();

  public:
    MatchAck_info()
    {
      transaction_id_ = 0;
      prose_application_id_ = NULL;
      validity_timer_t4004_ = 0;
      metadata_ = NULL;
      metadata_index_mask_ = NULL;
      anyExt_ = NULL;
      any_ = NULL;
      match_report_refresh_timer_t4006_ = 0;
      anyAttribute_ = NULL;
    }

    virtual ~MatchAck_info();

    MatchAck_info(const MatchAck_info&);
    MatchAck_info& operator=(const MatchAck_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_prose_application_id(const char* prose_application_id_val)
    {
      if (prose_application_id_val != NULL)
      {
        prose_application_id_ = PROSE_STRDUP(prose_application_id_val);
      }
      return true;
    }

    char* get_prose_application_id() const
    {
      return prose_application_id_;
    }

    bool set_validity_timer_t4004(int validity_timer_t4004_val)
    {
      validity_timer_t4004_ = validity_timer_t4004_val;
      return true;
    }

    int get_validity_timer_t4004() const
    {
      return validity_timer_t4004_;
    }

    bool set_metadata(const char* metadata_val)
    {
      if (metadata_val != NULL)
      {
        metadata_ = PROSE_STRDUP(metadata_val);
      }
      return true;
    }

    char* get_metadata() const
    {
      return metadata_;
    }

    bool set_metadata_index_mask(const char* metadata_index_mask_val)
    {
      if (metadata_index_mask_val != NULL)
      {
        metadata_index_mask_val = PROSE_STRDUP(metadata_index_mask_val);
      }
      return true;
    }

    char* get_metadata_index_mask() const
    {
      return metadata_index_mask_;
    }

    bool set_anyExt(anyExtType* anyExt_val)
    {
      if (anyExt_val != NULL)
      {
        anyExt_ = anyExt_val;
      }
      return true;
    }

    anyExtType* get_anyExt() const
    {
      return anyExt_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_match_report_refresh_timer_t4006(int match_report_refresh_timer_t4006_val)
    {
      match_report_refresh_timer_t4006_ = match_report_refresh_timer_t4006_val;
      return true;
    }

    int get_match_report_refresh_timer_t4006() const
    {
      return match_report_refresh_timer_t4006_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"MatchAck_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"MatchAck-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* MatchAck_info_H */

