/*!
  @file
  xml_common.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_common.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlCommon_H
#define XmlCommon_H

#define XML_SCHEMA_URL                    "http://www.w3.org/2001/XMLSchema-instance"
#define XML_DOUBLE_TILE_ESCAPE_STR        "/%7E%7E/"

//Default Encoding
#define DEFAULT_ENCODING "ISO-8859-15"

//Default Version for XML Document if nothing is specified.
#define DEFAULT_VERSION "1.0"

#define UTF8_ENCODING "UTF-8"
// char constants
#define XML_NULL_CHAR     '\0'
#define XML_BEGIN_TAG     '<'
#define XML_END_TAG       '>'
#define XML_DBLQUOTE_CHAR '\"'
#define XML_SLASH_CHAR    '/'
#define XML_BLANK_SPACE   ' '
#define XML_TAB           '\t'
#define XML_LINE_FEED     '\r'
#define XML_NEW_LINE      '\n'
#define XML_QUESTION_CHAR '?'
#define XML_SNGLQUOTE_CHAR '\''
#define XML_EQUALS        "="
#define XML_QUOTE         "\""
#define XML_SNGL_QUOTE    "\'"	
#define XML_QUOTES        "\"\'"
#define XML_WHITESPACES   " \t\r\n"

typedef enum e_XmlStrRef
{
  XML_NULL = 0,
  XML_USSD_DATA_STR, //ussd-data
  XML_USSD_STR, //ussd-string
  XML_LANGUAGE_STR, //language
  XML_USSD_ERROR_CODE_STR,//error-code
  XML_XMLNS_STR,
  XML_VERSION_NUM_STR,
  XML_ENCODING_STR,
  XML_PROSE_DISCOVERY_MESSAGE_STR, // prose-discovery-message
  XML_DISCOVERY_REQUEST_STR, // DISCOVERY_REQUEST
  XML_MESSAGE_EXT_STR, // message-ext
  XML_DISC_REQ_STR, // discovery-request
  XML_TRANSACTION_ID_STR, // transaction-ID
  XML_COMMAND_STR, // command
  XML_UE_IDENTITY_STR, // UE-identity
  XML_PROSE_APPLICATION_ID_STR, // ProSe-Application-ID
  XML_APPLICATION_IDENTITY_STR, // application-identity
  XML_DISCOVERY_ENTRY_ID_STR, // discovery-entry-ID
  XML_REQUESTED_TIMER_STR, // Requested-Timer
  XML_METADATA_STR, // metadata
  XML_OS_APP_ID_STR, // OS-ID
  XML_OS_ID_STR, // OS-App-ID
  XML_MCC_STR, // MCC
  XML_MNC_STR, // MNC
  XML_MSIN_STR, // MSIN
  XML_MATCH_REPORT_STR, // MATCH_REPORT
  XML_MATCH_REP_STR, // match-report
  XML_PROSE_APPLICATION_CODE_STR, // ProSe-Application-Code
  XML_MONITORED_PLMN_ID_STR, // Montiored-PLMN-ID
  XML_VPLMN_ID_STR, // VPLMN-ID
  XML_MIC_STR, // MIC
  XML_UTC_BASED_COUNTER_STR, // UTC-based-counter
  XML_METADATA_FLAG_STR, // Metadata-flag
  XML_PLMN_INFO_MCC_STR, // mcc
  XML_PLMN_INFO_MNC_STR, // mnc

  /* NameSapce Related elements*/

  XML_NAMESPACE_XSI_STR, /* xsi namespace */
  XML_SCHEMALOCATION_STR, /* schemaLocation */
  XML_SCHEMA_INSTANCE_URL_STR, /* "http://www.w3.org/2001/XMLSchema-instance" */
  XML_NAMESPACE_CP_STR, /* cp namespace */
  XML_NS_PROSE_DISCOVERY_STR, // "urn:3GPP:ns:ProSe:Discovery:2014"
  XML_NS_COPYCONTROL_STR, // "urn:ietf:params:xml:ns:copycontrol"
  XML_MAX_NUM
} XML_STR_REF;


class XmlCommonArray
{
  private:

    static const char *xml_str_ref_[XML_MAX_NUM];
    static XmlCommonArray *instance_;

    XmlCommonArray(){};

  public:
    ~XmlCommonArray(){};

    static XmlCommonArray* create_instance();

    static void release_instance();

    const char* get_str(XML_STR_REF str_ref)
    {
      return xml_str_ref_[str_ref];
    }
};

//string table for the XMLDocGenerator.
static const char *arrXmlStringTable[] = {
    "xml",
    "version",
    "encoding",
    "ENTITY",
    "xmlns",  
    " ",     //SPACE
    "" ,     //EMPTY
    "<",	   //LT
    ">",	   //GT
    "&lt",   //&LT
    "&gt",   //&GT
    "=",     //EQ
    "?",     //QMARK
    "!",     //EXMARK
    "\"",	   //DOUBLEQUOTE
    "\'",    //SINGLEQUOTE
    "-", 	   //DASH
    "<!--",  //START_XML_COMMENT
    "-->",   //END_XML_COMMENT
    ":"	,    //COLON
    "/",     //SLASH
    "(",     //BROPEN
    ")",     //BRCLOSE
    "\n"     //NEWLINE
};

//enum for the XMLDocGenerator to be used with arrXmlStringTable
enum STRING_TABLE{
    XML, 
    VERSION, 
    ENCODING, 
    ENTITY, 
    XMLNS,
    SPACE,
    EMPTY,
    LT, 
    GT,
    _LT,
    _GT,
    EQ, 
    QMARK, 
    EXMARK,
    DOUBLEQUOTE,
    SINGLEQUOTE,
    DASH,
    START_XML_COMMENT,
    END_XML_COMMENT,
    COLON,
    SLASH,
    BROPEN,
    BRCLOSE,
    NEWLINE
};

#endif /* XmlCommon_H */

