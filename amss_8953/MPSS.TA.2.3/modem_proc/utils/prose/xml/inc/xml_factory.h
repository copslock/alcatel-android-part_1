/*!
  @file
  xml_factory.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_factory.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlFactory_H
#define XmlFactory_H

class XmlContext;

typedef enum eXMLSchemaID
{
  XML_SCHEMA_PROSE_DISCOVERY_MESSAGE,
} XML_SCHEMA_ID;

class XmlFactory
{
  public:
    static XmlContext* newInstance(XML_SCHEMA_ID eXmlSchemaId);
    static void deleteContext(XmlContext* pXmlSchemaConetxt);
};

#endif /* XmlFactory_H */

