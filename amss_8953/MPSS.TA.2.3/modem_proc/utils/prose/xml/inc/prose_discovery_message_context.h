/*!
  @file
  prose_discovery_message_context.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/prose_discovery_message_context.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef prose_discovery_message_context_H
#define prose_discovery_message_context_H

#include "xml_object.h"
#include "xml_context.h"
#include "xml_encoder.h"
#include "xml_common.h"

class XmlCommonArray;
class prose_discovery_message;
class prose_direct_discovery_request;
class DiscMsgExtType;
class DiscReq_info;
class IMSI_info;
class AppID_info;
class anyExtType;
class slist;
class prose_direct_discovery_match_report;
class PLMN_info;

class prose_discovery_message_context : public XmlContext
{
  public:
    XmlMarshaller* createMarshaller();
    XmlUnMarshaller* createUnMarshaller();
};

class prose_discovery_message_marshaller : public XmlMarshaller, public XmlEncoder
{
  private:
    char* xml_msg_;
    uint16 size_;
    XmlCommonArray* common_array_;

  public:

    prose_discovery_message_marshaller();
    ~prose_discovery_message_marshaller();

    const char* Marshall(const XmlObject* obj, uint16 *len);

    bool add_IMSI_info_to_xml(IMSI_info *);
    bool add_AppID_info_to_xml(AppID_info *);
    bool add_PLMN_info_to_xml(PLMN_info *obj, XML_STR_REF element_name);
    bool add_DiscReq_info_to_xml(list<XmlElement*> *obj);
    bool add_MatchRep_info_to_xml(list<XmlElement*> *obj);
    bool add_anyExtType_to_xml(anyExtType *);
    bool add_any_to_xml(slist *);
    bool add_prose_direct_discovery_request_to_xml(prose_direct_discovery_request *);
    bool add_prose_direct_discovery_match_report_to_xml(prose_direct_discovery_match_report *obj);
    bool add_DiscMsgExtType_to_xml(DiscMsgExtType *);
    bool add_prose_discovery_message_to_xml(prose_discovery_message *);
};

class prose_discovery_message_unmarshaller : public XmlUnMarshaller
{
  public:

    prose_discovery_message_unmarshaller(){};
    ~prose_discovery_message_unmarshaller(){};

    XmlObject* UnMarshall(const char* pstring);
};

#endif /* prose_discovery_message_context_H */
