/*!
  @file
  xml_decoder.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_decoder.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlDecoder_H
#define XmlDecoder_H

#define XML_INCOMPLETE  1
#define XML_SUCCESS     0
#define XML_ERROR       -1

#define SET_DELIM(m, c)         ((m)[(c) >> 3] |= (1 << ((c) & 7)))
#define IS_DELIM(m, c)          ((m)[(c) >> 3] & (1 << ((c) & 7)))
#define DELIM_TABLE_SIZE        32

#include "../../utils/inc/list.h"

class XmlDecoderSchema;

class Decoder
{
  public:
    Decoder();
    virtual ~Decoder()
    {
      reset_xml_decoder();
    }

    int decode(const char *xml_str, int count);

    void set_xml_decoder_schema(XmlDecoderSchema *xml_decoder_schema)
    {
      xml_decoder_schema_ = xml_decoder_schema;
    }

    void reset_xml_decoder();

    void init_xml_decoder();

  private:
    Decoder(const Decoder&);
    Decoder& operator=(const Decoder&);

    enum DecoderState
    {
      PARSE_INIT,
      PARSE_CONTINUE,
      PARSE_ERROR,
      PARSE_COMPLETE
    };

    int              decode_attributes(char *n_strAttrList, bool self_ending);
    char*            tokenize(char *n_pString,const char *n_pDelims, char **n_ppNewStr);

    void             unescape_xml(char* n_pString);
    void             unescape_xml_string(char* n_pString, char* n_pSequence, char n_iCharacter);
    void             unescape_xml_string(char* haystack, unsigned int haystack_length, char* needle, char replacement);

    int              validate_attribute_list(char* attr_list);
    char*            trim_spaces(char* n_str);
    char*            buf_str(const char* haystack, unsigned int haystack_length, const char* needle, unsigned int needle_length);
    unsigned int     contract_buffer(char* s, unsigned int slen);

    static char     *RemoveQuotes(char *n_strValue);
    static char     *LocateBeginElement(char *n_strDecode);
    static char     *LocateEndElement(char *n_strDecode);
    static char     *SkipSpaces(char *n_strBegin);
    static bool      IsEmailFormat(char *n_str);
    static void      TrimRightSingle(char* n_pStr);

    DecoderState      decode_state_;
    list<char*>       parser_stack_;
    char             *decode_str_;
    XmlDecoderSchema *xml_decoder_schema_;
};

#endif /* XmlDecoder_h */
