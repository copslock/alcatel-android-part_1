/*!
  @file
  DiscFilter_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/DiscFilter_info.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef DiscFilter_info_H
#define DiscFilter_info_H

#include "xml_list.h"
#include "xml_element.h"

class DiscFilter_info : public XmlElement
{
  private:
    char  *prose_application_code_;
    list<char*> *prose_application_mask_;
    int   ttl_timer_t4002_;
    slist *any_;
    slist *anyAttribute_;

    DiscFilter_info(const DiscFilter_info&);

    void clear_prose_application_code();
    void clear_prose_application_mask();
    void clear_ttl_timer_t4002();
    void clear_any();
    void clear_anyAttribute();

  public:
    DiscFilter_info()
    {
      prose_application_code_ = NULL;
      prose_application_mask_ = NULL;
      ttl_timer_t4002_ = 0;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~DiscFilter_info();

    DiscFilter_info& operator=(const DiscFilter_info&);

    bool set_prose_application_code(const char* prose_application_code_val)
    {
      if (prose_application_code_val != NULL)
      {
        prose_application_code_ = PROSE_STRDUP(prose_application_code_val);
      }
      return true;
    }

    char* get_prose_application_code() const
    {
      return prose_application_code_;
    }

    bool set_prose_application_mask(const char *prose_application_mask_val);

    bool set_prose_application_mask(list<char*> *prose_application_mask_val)
    {
      if (prose_application_mask_val != NULL)
      {
        prose_application_mask_ = prose_application_mask_val;
      }
      return true;
    }

    list<char*>* get_prose_application_mask() const
    {
      return prose_application_mask_;
    }

    bool set_ttl_timer_t4002(int ttl_timer_t4002_val)
    {
      ttl_timer_t4002_ = ttl_timer_t4002_val;
      return true;
    }

    int get_ttl_timer_t4002() const
    {
      return ttl_timer_t4002_;
    }

    bool set_any(slist *any)
    {
      any_ = any;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute)
    {
      anyAttribute_ = anyAttribute;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"DiscFilter_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"DiscFilter-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif // DiscFilter_info_H

