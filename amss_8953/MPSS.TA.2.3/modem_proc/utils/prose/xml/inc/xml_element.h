/*!
  @file
  xml_element.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_element.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlElement_H
#define XmlElement_H

#include <assert.h>

#include "../../utils/inc/list.h"
#include "xml_attribute.h"
#include "prose_diag.h"

class XmlElement
{
  private:
    XmlElement(const XmlElement&);
    XmlElement& operator=(const XmlElement&);

  public:
    XmlElement(){};
    virtual ~XmlElement(){};
    virtual char* ClassName() = 0;
    virtual char* ClassType() = 0;
    virtual char* TagName() = 0;
    virtual unsigned char Valiadate() = 0;

    virtual bool  ProcessUnMarshall(char* n_Element,char* n_Value, XmlElement* n_Object)
    {
      return true;
    }
    virtual bool  ProcessUnMarshallAttribute(char* n_Attribute, char* n_Value, XmlAttribute* n_Object)
    {
      return true;
    }
};

class AnyType : public XmlElement
{
  private:
    char* any_;

    AnyType(const AnyType&);

    void clear_any()
    {
      if (any_ != NULL)
      {
        PROSE_FREE(any_);
        any_ = NULL;
      }
    }

  public:
    AnyType()
    {
      any_ = NULL;
    }

    virtual ~AnyType()
    {
      clear_any();
    }

    AnyType& operator=(const AnyType &rhs)
    {
      if (this == &rhs)
        return *this;

      clear_any();
      if (rhs.any_)
      {
        set_any(rhs.get_any());
      }
      return *this;
    }

    bool set_any(char* name)
    {
      if (name != NULL)
      {
        any_ = PROSE_STRDUP(name);
      }
      return true;
    }

    char* get_any() const
    {
      return any_;
    }
    bool ProcessUnMarshall(char* n_Element, char* n_Value, XmlElement* n_Object)
    {
      return true;
    }

    bool ProcessUnMarshallAttribute(char* n_Element, char* n_Value, XmlAttribute* n_Object)
    {
      return true;
    }

    char* ClassName() { return (char*)"AnyType"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"AnyType"; }
    unsigned char Valiadate(){ return 0; }
};

#endif /* XmlElement_H */

