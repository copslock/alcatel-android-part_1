/*!
  @file
  prose_discovery_message.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/prose_discovery_message.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef prose_discovery_message_H
#define prose_discovery_message_H

#include "xml_list.h"
#include "xml_element.h"
#include "xml_object.h"

class DiscMsgExtType;
class prose_direct_discovery_request;
class prose_direct_discovery_response;
class prose_direct_discovery_match_report;
class prose_direct_discovery_match_report_ack;

class prose_discovery_message : public XmlObject
{
  private:
    enum 
    {
      UNDEFINED,
      DISCOVERY_REQUEST,
      DISCOVERY_RESPONSE,
      MATCH_REPORT,
      MATCH_REPORT_ACK,
      UE_REGISTRATION_REQUEST,
      UE_REGISTRATION_RESPONSE,
      APPLICATION_REGISTRATION_REQUEST,
      APPLICATION_REGISTRATION_RESPONSE,
      PROXIMITY_REQUEST,
      PROXIMITY_RESPONSE,
      PROXIMTIY_ALERT,
      UE_DEREGISTRATION_REQUEST,
      UE_DEREGISTRATION_RESPONSE,
      CANCEL_PPROXIMTY_REQUEST,
      CANCEL_PROXIMITY_RESPONSE,
      PROXIMITY_REQUEST_VALIDATION,
      PROXIMTIY_REQUEST_VALIDATION_RESPONSE,
      MESSAGE_EXT,
      ANY
    } choice1_;

    union
    {
      prose_direct_discovery_request          *discovery_request_;
      prose_direct_discovery_response         *discovery_response_;
      prose_direct_discovery_match_report     *match_report_;
      prose_direct_discovery_match_report_ack *match_report_ack_;
      DiscMsgExtType                          *message_ext_;
      slist                                   *any_;
    };

    prose_discovery_message(const prose_discovery_message&);

    void clear_discovery_request();
    void clear_discovery_response();
    void clear_match_report();
    void clear_match_report_ack();
    void clear_message_ext();
    void clear_any();

  public:
    prose_discovery_message()
    {
      choice1_ = UNDEFINED;
      discovery_request_ = NULL;
      message_ext_ = NULL;
      any_ = NULL;
    }

    virtual ~prose_discovery_message();

    prose_discovery_message& operator=(const prose_discovery_message&);

    bool set_discovery_request(prose_direct_discovery_request *discovery_request_val)
    {
      if (discovery_request_val != NULL)
      {
        discovery_request_ = discovery_request_val;
        choice1_ = DISCOVERY_REQUEST;
      }
      return true;
    }

    prose_direct_discovery_request* get_discovery_request() const
    {
      return (choice1_ == DISCOVERY_REQUEST) ? discovery_request_ : NULL;
    }

    bool set_discovery_response(prose_direct_discovery_response *discovery_response_val)
    {
      if (discovery_response_val != NULL)
      {
        discovery_response_ = discovery_response_val;
        choice1_ = DISCOVERY_RESPONSE;
      }
      return true;
    }

    prose_direct_discovery_response* get_discovery_response() const
    {
      return (choice1_ == DISCOVERY_RESPONSE) ? discovery_response_ : NULL;
    }

    bool set_match_report(prose_direct_discovery_match_report *match_report_val)
    {
      if (match_report_val != NULL)
      {
        match_report_ = match_report_val;
        choice1_ = MATCH_REPORT;
      }
      return true;
    }

    prose_direct_discovery_match_report* get_match_report() const
    {
      return (choice1_ == MATCH_REPORT) ? match_report_ : NULL;
    }

    bool set_match_report_ack(prose_direct_discovery_match_report_ack *match_report_ack_val)
    {
      if (match_report_ack_val != NULL)
      {
        match_report_ack_ = match_report_ack_val;
        choice1_ = MATCH_REPORT_ACK;
      }
      return true;
    }

    prose_direct_discovery_match_report_ack* get_match_report_ack() const
    {
      return (choice1_ == MATCH_REPORT_ACK) ? match_report_ack_ : NULL;
    }

    bool set_message_ext(DiscMsgExtType* message_ext_val)
    {
      if (message_ext_val != NULL)
      {
        message_ext_ = message_ext_val;
        choice1_ = MESSAGE_EXT;
      }
      return true;
    }

    DiscMsgExtType* get_message_ext() const
    {
      return (choice1_ == MESSAGE_EXT) ? message_ext_ : NULL;
    }

    bool set_any(slist *any_val)
    {
      if (any_val != NULL)
      {
      any_ = any_val;
      choice1_ = ANY;
      }
      return true;
    }

    slist* get_any() const
    {
      return (choice1_ == ANY) ? any_ : NULL;
    }
            
    virtual bool IsRootElement()
    {
      return true;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"prose_discovery_message"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"prose-discovery-message"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* prose_discovery_message_H */

