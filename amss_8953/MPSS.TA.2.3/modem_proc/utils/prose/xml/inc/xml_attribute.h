/*!
  @file
  xml_attribute.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_attribute.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlAttribute_H
#define XmlAttribute_H

#include "mem.h"

class XmlAttribute
{
  public:
    XmlAttribute(){};

    virtual ~XmlAttribute(){};

    virtual char* ClassName() = 0;

    virtual char* ClassType() = 0;

    virtual char* TagName() = 0;

    virtual unsigned char Validate() = 0;
};


class AttributeValuePair : public XmlAttribute
{
  private:
    char* attr_name_;
    char* attr_value_;

    AttributeValuePair(AttributeValuePair const& src);

  public:
    AttributeValuePair() {
      attr_name_ = NULL;
      attr_value_ = NULL;
    }

    virtual ~AttributeValuePair() {
      if (attr_name_ != NULL)
      {
        PROSE_FREE(attr_name_);
        attr_name_ = NULL;
      }
      if (attr_value_ != NULL)
      {
        PROSE_FREE(attr_value_);
        attr_value_ = NULL;
      }
    }

    bool set_attr_name(char* element_name) {
      if (element_name != NULL)
      {
        attr_name_ = PROSE_STRDUP(element_name);
      }
      return true;
    }

    char* get_attr_name() {
      return attr_name_;
    }

    bool set_attr_value(char* element_name) {
      if (element_name != NULL)
      {
        attr_value_ = PROSE_STRDUP(element_name);
      }
      return true;
    }

    char* get_attr_value() {
      return attr_value_;
    }

    AttributeValuePair& operator=(AttributeValuePair const& rhs)
    {
      if (this == &rhs)
        return *this;

      if (attr_name_ != NULL)
      {
        PROSE_FREE(attr_name_);
        attr_name_ = NULL;
      }
      attr_name_ = PROSE_STRDUP(rhs.attr_name_);

      if (attr_value_ != NULL)
      {
        PROSE_FREE(attr_value_);
        attr_value_ = NULL;
      }
      attr_value_ = PROSE_STRDUP(rhs.attr_value_);
      return *this;
    }

    virtual char* ClassName(){ return (char*)"AttributeValuePair"; }
    virtual char* ClassType(){ return (char*)"XMLAttribute"; }
    virtual char* TagName(){ return (char*)"AttributeValuePair"; }
    virtual unsigned char Validate(){ return 0; }
};



#endif /* XmlAttribute_H */
