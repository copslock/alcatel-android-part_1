/*!
  @file
  prose_direct_discovery_request.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/prose_direct_discovery_request.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef prose_direct_discovery_request_H
#define prose_direct_discovery_request_H

#include "xml_list.h"
#include "xml_element.h"

class anyExtType;
class DiscReq_info;

class prose_direct_discovery_request : public XmlElement
{
  private:
    list<XmlElement*>        *discovery_request_;
    anyExtType   *anyExt_;
    slist        *any_;
    slist        *anyAttribute_;

    prose_direct_discovery_request(const prose_direct_discovery_request&);

    void clear_discovery_request();
    void clear_anyExt();
    void clear_any();
    void clear_anyAttribute();

  public:
    prose_direct_discovery_request()
    {
      discovery_request_ = NULL;
      anyExt_ = NULL;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~prose_direct_discovery_request();

    prose_direct_discovery_request& operator=(const prose_direct_discovery_request&);

    bool set_discovery_request(DiscReq_info *discovery_request_val);

    bool set_discovery_request(list<XmlElement*> *discovery_request_val)
    {
      if (discovery_request_val != NULL)
      {
        discovery_request_ = discovery_request_val;
      }
      return true;
    }

    list<XmlElement*>* get_discovery_request() const
    {
      return discovery_request_;
    }

    bool set_anyExt(anyExtType* anyExt_val)
    {
      if (anyExt_val != NULL)
      {
        anyExt_ = anyExt_val;
      }
      return true;
    }

    anyExtType* get_anyExt() const
    {
      return anyExt_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"prose_direct_discovery_request"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"prose-direct-discovery-request"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* prose_direct_discovery_request_H */

