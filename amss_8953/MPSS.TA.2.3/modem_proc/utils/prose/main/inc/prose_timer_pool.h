/*!
  @file
  prose_timer_pool.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_timer_pool.h#9 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_TIMER_POOL_H
#define PROSE_TIMER_POOL_H

#include "hashmap.h"
#include "list.h"
#include "object_pool.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "rex.h"
#ifdef __cplusplus
}
#endif

class Mutex
{
  public:
    Mutex()       { rex_init_crit_sect(&mutex_); }
    ~Mutex()      { rex_del_crit_sect(&mutex_); }
    void lock()   { rex_enter_crit_sect(&mutex_); }
    void unlock() { rex_leave_crit_sect(&mutex_); }
  private:
    rex_crit_sect_type mutex_;
};

class ScopedLock
{
  public:
    ScopedLock(Mutex& mutex) : mutex_(mutex) { mutex_.lock(); }
    ~ScopedLock() { mutex_.unlock(); }
  private:
    Mutex &mutex_;
};

class TimerPool
{
  public:
    typedef uint32 timer_id;

    static TimerPool* get_instance();
    virtual ~TimerPool();

    static void destroy();

    static uint64 now();

    void init(uint32 max_timers = 1000);

    void deinit();

    enum timer_type_e 
    {
      INVALID = 0,
      PRE_T4000,
      T4000,
      PRE_T4002,
      T4002,
      T4004,
      T4005,
      T_HANGOVER,
      T_SESSION,
      T_PC3_BATCH
    };

    timer_id create(uint64 when, uint64 period, timer_type_e type, uint32 session_id = 0);
    bool remove(timer_id id);
    bool exists(timer_id id);
    timer_type_e get_type(timer_id id);
    uint64 get_remaining_time(timer_id id);

    void timer_callback();

  private:
    TimerPool();
    TimerPool(TimerPool const&);
    TimerPool& operator=(TimerPool const&);

    struct TimerInstance
    {
      TimerInstance(timer_id id = 0)
        : id_(id), timestamp_(0), period_(0), 
          type_(INVALID), session_id_(0),
          running_(false)
      {}

      timer_id id_;
      uint64 timestamp_;
      uint64 period_;
      timer_type_e type_;
      uint32 session_id_;
      bool running_;
      
      TimerInstance(TimerInstance const& r);
      TimerInstance& operator=(TimerInstance const& r);
    };

    void delete_from_pool(const TimerInstance* timer);
    void insert_into_pool(const TimerInstance* timer);

    void dispatch(timer_type_e type, timer_id id, uint32 session_id);

    static TimerPool* instance_;

    Mutex sync_;

    ObjectPool<TimerInstance> *timers_;
    uint32 max_timers_;

    typedef hashmap<timer_id, TimerInstance*> TMap;
    TMap timer_map_;

    typedef list<const TimerInstance*> TList;
    typedef list<TList*> TPool;
    TPool timer_pool_;

    timer_type timer_type_inst_;

    timer_id id_counter_;

    timer_id get_next_id() { return id_counter_++ % 0xFFFFFFFF; }

    static void timer_callback(timer_cb_data_type data);
};

#endif /* PROSE_TIMER_POOL_H */

