/*!
  @file
  prose_session_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_session_info.h#15 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_SESSION_INFO_H
#define PROSE_SESSION_INFO_H

#include "prose_state_machine.h"
#include "prose_data_manager.h"
#include "prose_timer_pool.h"

class ProseSessionInfo : public ProseStateMachine
{
  public:

    enum SessionType { NONE = 0, ANNOUNCE, MONITOR };

    ProseSessionInfo();

    virtual ~ProseSessionInfo() {}

    void initialize(SessionType);
    void finalize();

    void cleanup();

    bool in_use() const
    {
      return in_use_;
    }

    void set_session_id(unsigned session_id)
    {
      session_id_ = session_id;
    }

    unsigned get_session_id() const
    {
      return session_id_;
    }

    SessionType get_session_type() const
    {
      return type_;
    }

    const uint32& touch() const
    {
      return touch_time_;
    }

    uint32& touch()
    {
      return touch_time_;
    }

    const prose_pa_id_s& get_pa_id() const
    {
      return pa_id_;
    }

    const prose_os_app_id_s& get_os_app_id() const
    {
      return os_app_id_;
    }

    uint32 get_request_time() const
    {
      return request_time_;
    }

    uint8 get_num_mac_sessions() const
    {
      return mac_session_id_.size();
    }

    void set_mac_session_id(uint32 mac_session_id);

    void set_mac_session_id(uint8 num_ids, const uint32 *mac_session_id);

    void set_disc_req_info(const prose_os_app_id_s& app_id, const prose_pa_id_s& pa_id, uint32 duration, prose_disc_type_e disc_type, uint32 request_time, const char *metadata = 0, uint32 metadata_size = 0, bool metadata_flag = false);

    void modify_disc_req_info(uint32 request_time, const char *metadata = 0, uint32 metadata_size = 0);

    void set_announce_info(const char* prose_app_code, const char* disc_key, int t4000, int de_id);

    bool set_discovery_filters(list<XmlElement*>* filters, int de_id);

    void add_filter(char* pac, list<char*>* pam, int t4002);

    int set_match_info(int trans_id, const char* pa_id_str, int t4004, int t4006, const char* metadata, const char* metadata_index_mask, prose_app_code_s *out_pac);

    int set_match_reject(int trans_id, int rej_value);

    int set_match_failure(int trans_id);

    void convert_matched_pac(const uint8 *pac, prose_app_code_s* out_pac);

    void send_pc3_announce_request(const char* metadata = 0, uint32 metadata_size = 0);

    void send_pc3_monitor_request();

    void send_pc3_match_request(const prose_app_code_s& pac, uint32 mic, uint32 utc_time, uint32 freq, bool metadata_update);

    void send_pc3_cancel_request();

    void send_mac_pac_add_req();

    void send_mac_filter_add_req();

    void send_match_event_ind(const prose_app_code_s& pac, const char* metadata = 0);

    void send_notification_ind(prose_qmi_result_e result);

    bool check_auth();

    void publish_cancel();

    void subscribe_cancel();

    void subscribe_cancel(TimerPool::timer_id id);
    
    bool fetch_announce_data_from_cache();

    bool fetch_monitor_data_from_cache();

    bool fetch_match_data_from_cache(const prose_app_code_s& pac, bool* metadata_update = 0);

    void reset_announce_data_from_cache();

    void reset_announce_data_from_cache(TimerPool::timer_id id);

    void reset_monitor_data_from_cache();

    void reset_monitor_data_from_cache(TimerPool::timer_id id);

    void reset_match_data_from_cache(TimerPool::timer_id id);

    void match_event_end(TimerPool::timer_id id);

    void cleanup_timer(TimerPool::timer_id id);

  private:

    static const uint32 PRE_FETCH_TIME;
    static const uint32 MATCH_HANGOVER_TIME;

    bool in_use_;
    uint32 session_id_;
    SessionType type_;
    uint32 touch_time_;

    list<uint32> mac_session_id_;
    list<ProseDataManager::FilterElem>::const_iterator filter_iter_;

    prose_os_app_id_s os_app_id_;
    prose_pa_id_s pa_id_;
    uint32 duration_;
    prose_disc_type_e disc_type_;
    uint32 request_time_;
    bool metadata_flag_;

    const ProseDataManager::PacInfo* pac_info_;
    const ProseDataManager::FilterInfo* filter_info_;
    list<pair<prose_app_code_s, int> > unacked_pac_list_;
    list<pair<prose_app_code_s, int> > metadata_req_list_;

    struct TimerData
    {
      TimerPool::timer_id id_;
      const void *data_;
      explicit TimerData(TimerPool::timer_id id) : id_(id) {}
      explicit TimerData(TimerPool::timer_id id, const void *data) : id_(id), data_(data) {}
      private:
      //TimerData(TimerData const&);
      TimerData& operator=(TimerData const&);
    };
    list<TimerData> timer_list_;

    struct MetadataIndexer
    {
      prose_app_code_s mask_;
      prose_app_code_s previous_val_;
      bool pending_;
    };
    list<MetadataIndexer> metadata_index_list_;

    bool cleanup_hangover_timer(const prose_app_code_s* pac_ptr);

    void send_match_end_event_ind(const prose_app_code_s& matched_pac);
};

#endif /* PROSE_SESSION_INFO_H */
