/*!
  @file
  prose_config_manager.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_config_manager.h#7 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_CONFIG_MANAGER_H
#define PROSE_CONFIG_MANAGER_H

#include "hashmap.h"

#include "prose_dispatcher.h"
#include "prose_timer_pool.h"

template<>
struct Equals<prose_os_app_id_s> 
{
  bool operator()(const prose_os_app_id_s& id1, const prose_os_app_id_s& id2) const
  {
    return memcmp(&id1, &id2, sizeof(prose_os_app_id_s)) == 0;
  }
};

class ProseSessionInfo;

class ProseConfigManager
{
  public:
    static ProseConfigManager* get_instance();

    virtual ~ProseConfigManager();

    static void destroy();

    void set_config(const prose_os_id_s *os_id, int32 ann_list_size, const prose_disc_announcing_policy_list_info_s *ann_list, int32 mon_list_size, const prose_disc_monitoring_policy_list_info_s *mon_list, const char *apn_name);

    void get_config(prose_os_id_s *os_id, int32 *ann_list_size, prose_disc_announcing_policy_list_info_s *ann_list, int32 *mon_list_size, prose_disc_monitoring_policy_list_info_s *mon_list, char *apn_name);

    void set_category(const prose_os_app_id_s *app_id, prose_disc_category_e cat);

    prose_disc_category_e get_category(const prose_os_app_id_s *app_id);

    const prose_os_id_s& get_os_id() const
    {
      return os_id_;
    }

    const sys_plmn_id_s_type* get_imsi_plmn() const
    {
      return &imsi_plmn_id_;
    }

    const uint8* get_imsi_msin(uint8 *len) const
    {
      *len = num_msin_digits_;
      return imsi_msin_;
    }

    void set_plmn_earfcn_mapping(uint32 earfcn, const lte_rrc_plmn_s& plmn);

    const sys_plmn_id_s_type* get_plmn_earfcn_mapping(uint32 earfcn);

    void process_attach_event();

    void process_srvreq_event(bool status, uint8 emm_cause);

    void process_imsi(const nv_ehrpd_imsi_type& imsi);

    void process_conn_rel(bool rlf);

    void process_t4005_expiry(TimerPool::timer_id id);

    void get_service_status(boolean* publish_allowed, boolean* subscribe_allowed);

    void set_lted_available(boolean tx, boolean rx, boolean tx_on_serving,
        const lte_rrc_plmn_s& plmn);

    void set_lted_unavailable(bool non_oos);

    const char* get_apn_name() const { return apn_name_; }

    const sys_plmn_id_s_type *get_camped_plmn() const;

    bool tx_on_camped_cell() const;

    bool tx_available() const { return tx_available_; }

    bool rx_available() const { return rx_available_; }

    void add_rx_session(ProseSessionInfo* session);

    void delete_rx_session(ProseSessionInfo* session);

    void add_tx_session(ProseSessionInfo* session);

    void delete_tx_session(ProseSessionInfo* session);

    void send_srvreq_req();

    enum States
    {
      UNAVAILABLE = 0,
      LTED_IDLE,
      ATTACHED,
      AVAILABLE,
      CONNECTED,
    };

    States get_state() const { return state_; }

    bool extract_plmn(const prose_pa_id_s& pa_id, sys_plmn_id_s_type& sys_plmn_id);

    bool check_announce_authorization(const prose_pa_id_s& pa_id);
    
    bool check_monitor_authorization(const prose_pa_id_s& pa_id);

    static const uint8 NUM_MSIN_DIGITS = 10;

  private:
    ProseConfigManager();
    ProseConfigManager(const ProseConfigManager&);
    ProseConfigManager& operator=(const ProseConfigManager&);

    void clear_config();

    bool set_state(States new_state);

    sys_plmn_id_s_type rrc_plmn_to_sys_plmn_id(const lte_rrc_plmn_s& plmn_id);

    void sys_plmn_id_to_rrc_plmn(const sys_plmn_id_s_type *sys_plmn_list, int32 list_size,
        lte_rrc_plmn_list_s& rrc_plmn_list);

    bool compare_plmns(const sys_plmn_id_s_type& pa_id_plmn, const sys_plmn_id_s_type& sys_plmn);

    void send_prose_cfg_req();
    void send_broadcast_notification();
    void send_ded_cfg_req();

    static ProseConfigManager *instance_;
    prose_os_id_s os_id_;
    int32 announce_list_size_;
    sys_plmn_id_s_type announce_plmn_list_[PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE];
    prose_disc_announcing_policy_range_e announce_range_[PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE];
    int32 monitor_list_size_;
    sys_plmn_id_s_type monitor_plmn_list_[PROSE_ANNOUNCING_POLICY_LIST_MAX_SIZE];
    char apn_name_[PROSE_DEDICATED_APN_NAME_MAX_SIZE];

    sys_plmn_id_s_type camped_plmn_;
    bool tx_on_camped_cell_;
    bool tx_available_;
    bool rx_available_;
    sys_plmn_id_s_type imsi_plmn_id_;
    uint8 imsi_msin_[NUM_MSIN_DIGITS];
    uint8 num_msin_digits_;

    hashmap<prose_os_app_id_s, prose_disc_category_e> app_category_map_;

    hashmap<uint32, sys_plmn_id_s_type> earfcn_plmn_map_;

    States state_;

    list<ProseSessionInfo*> rx_sessions_;
    list<ProseSessionInfo*> tx_sessions_;

    struct T4005CallbackData
    {
      enum Type { ANNOUNCE, MONITOR };
      explicit T4005CallbackData(TimerPool::timer_id id, sys_plmn_id_s_type plmn, Type type)
        : id_(id), plmn_(plmn), type_(type)
      {}
      TimerPool::timer_id id_;
      sys_plmn_id_s_type plmn_;
      Type type_;
      private:
      T4005CallbackData(T4005CallbackData const&);
      T4005CallbackData& operator=(T4005CallbackData const&);
    };
    list<T4005CallbackData*> callback_list_;
};

#endif /* PROSE_CONFIG_MANAGER_H */
