/*!
  @file
  prose_session_repository.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_session_repository.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_SESSION_REPOSITORY_H
#define PROSE_SESSION_REPOSITORY_H

#include "hashmap.h"
#include "object_pool.h"

#include "prose_defs.h"
#include "prose_session_info.h"

#if 0
template<>
class DefaultHash<pair<prose_pa_id_s,prose_os_app_id_s> >
{
  unsigned operator()(const pair<prose_pa_id_s,prose_os_app_id_s>& id)
  {
    unsigned result = 0;
    for (unsigned i = 0; i < id.first.length; i++)
      result += id.first.byte[i];
    for (unsigned i = 0; i < id.second.length; i++)
      result += id.second.byte[i];
    return result;
  }
};
#endif

class ProseSessionRepository
{
  public:

    static ProseSessionRepository* get_instance();
        
    virtual ~ProseSessionRepository() {}

    static void destroy();

    void init(int max_sessions = 100);

    void deinit();

    ProseSessionInfo *start_session(ProseSessionInfo::SessionType);

    ProseSessionInfo *lookup_session(unsigned session_id);

    void add_pa_id(const prose_pa_id_s& pa_id, const prose_os_app_id_s& app_id, ProseSessionInfo* session);

    ProseSessionInfo* lookup_pa_id(const prose_pa_id_s& pa_id, const prose_os_app_id_s& app_id);

    void remove_pa_id(const prose_pa_id_s& pa_id, const prose_os_app_id_s& app_id);

    void add_mac_session(uint32 mac_session_id, ProseSessionInfo* session);

    const list<ProseSessionInfo*>* lookup_mac_session(uint32 mac_session_id);

    int num_prose_sessions(uint32 mac_session_id);

    void remove_mac_session(uint32 mac_session_id, ProseSessionInfo* session);

    void end_session(unsigned session_id);

    void send_connected_ind();

    void send_auth_change_ind();

    void terminate(const prose_os_app_id_s *os_app_id);

    void cleanup();

  private:

    ProseSessionRepository();

    ObjectPool<ProseSessionInfo> *sessions_;

    unsigned get_next_session_id()
    {
      return session_id_counter_++;
    }

    typedef hashmap<unsigned, ProseSessionInfo*> SessionMap;
    SessionMap session_map_;

    typedef hashmap<pair<prose_pa_id_s,prose_os_app_id_s>, ProseSessionInfo*> PaIdMap;
    PaIdMap pa_id_map_;

    typedef hashmap<uint32, list<ProseSessionInfo*> > MacSessionMap;
    MacSessionMap mac_session_map_;

    unsigned max_sessions_;

    unsigned session_id_counter_;

    static ProseSessionRepository *instance_;
};

#endif /* PROSE_SESSION_REPOSITORY_H */

