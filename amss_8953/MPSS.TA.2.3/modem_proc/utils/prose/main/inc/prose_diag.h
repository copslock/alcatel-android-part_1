/*!
  @file
  prose_data_manager.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_diag.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_DIAG_H
#define PROSE_DIAG_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <msg.h>
#ifdef __cplusplus
}
#endif

#ifdef WIN32
#define __func__ __FUNCTION__
#endif

/* Diag level Masks  (xx_ss_mask) for the Main Module */
#define PROSE_MAIN_MASK  MSG_MASK_8 
#define PROSE_MAIN_LOW   (PROSE_MAIN_MASK | MSG_LEGACY_LOW)
#define PROSE_MAIN_MED   (PROSE_MAIN_MASK | MSG_LEGACY_MED)
#define PROSE_MAIN_HIGH  (PROSE_MAIN_MASK | MSG_LEGACY_HIGH)
#define PROSE_MAIN_ERROR (PROSE_MAIN_MASK | MSG_LEGACY_ERROR)

/* Diag level Masks  (xx_ss_mask) for the UTILS Module */
#define PROSE_UTILS_MASK  MSG_MASK_9 
#define PROSE_UTILS_LOW   (PROSE_UTILS_MASK | MSG_LEGACY_LOW)
#define PROSE_UTILS_MED   (PROSE_UTILS_MASK | MSG_LEGACY_MED)
#define PROSE_UTILS_HIGH  (PROSE_UTILS_MASK | MSG_LEGACY_HIGH)
#define PROSE_UTILS_ERROR (PROSE_UTILS_MASK | MSG_LEGACY_ERROR)

/* Diag level Masks  (xx_ss_mask) for the XML Module */
#define PROSE_XML_MASK  MSG_MASK_10 
#define PROSE_XML_LOW   (PROSE_XML_MASK | MSG_LEGACY_LOW)
#define PROSE_XML_MED   (PROSE_XML_MASK | MSG_LEGACY_MED)
#define PROSE_XML_HIGH  (PROSE_XML_MASK | MSG_LEGACY_HIGH)
#define PROSE_XML_ERROR (PROSE_XML_MASK | MSG_LEGACY_ERROR)

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#define PROSE_MSG(xx_ss_mask, xx_fmt)                                 \
  MSG(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt)

#define PROSE_MSG_1(xx_ss_mask, xx_fmt, a)                            \
  MSG_1(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a)

#define PROSE_MSG_2(xx_ss_mask, xx_fmt, a, b)                         \
  MSG_2(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b)

#define PROSE_MSG_3(xx_ss_mask, xx_fmt, a, b, c)                      \
  MSG_3(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c)

#define PROSE_MSG_4(xx_ss_mask, xx_fmt, a, b, c, d)                   \
  MSG_4(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c, d)

#define PROSE_MSG_5(xx_ss_mask, xx_fmt, a, b, c, d, e)                \
  MSG_5(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c, d, e)

#define PROSE_MSG_6(xx_ss_mask, xx_fmt, a, b, c, d, e, f)             \
  MSG_6(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c, d, e, f)

#define PROSE_MSG_7(xx_ss_mask, xx_fmt, a, b, c, d, e, f, g)          \
  MSG_7(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c, d, e, f, g)

#define PROSE_MSG_8(xx_ss_mask, xx_fmt, a, b, c, d, e, f, g, h)       \
  MSG_8(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c, d, e, f, g, h)

#define PROSE_MSG_SPRINTF_1(xx_ss_mask, xx_fmt, a)                    \
  MSG_SPRINTF_1(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a)

#define PROSE_MSG_SPRINTF_2(xx_ss_mask, xx_fmt, a, b)                 \
  MSG_SPRINTF_2(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b)

#define PROSE_MSG_SPRINTF_3(xx_ss_mask, xx_fmt, a, b, c)                 \
  MSG_SPRINTF_3(MSG_SSID_LTE_RRC, (xx_ss_mask), xx_fmt, a, b, c)


#define PROSE_MSG_MED(xx_fmt)                                         \
  MSG(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt)

#define PROSE_MSG_1_MED(xx_fmt, a)                                    \
  MSG_1(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a)

#define PROSE_MSG_2_MED(xx_fmt, a, b)                                 \
  MSG_2(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b)

#define PROSE_MSG_3_MED(xx_fmt, a, b, c)                              \
  MSG_3(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c)

#define PROSE_MSG_4_MED(xx_fmt, a, b, c, d)                           \
  MSG_4(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c, d)

#define PROSE_MSG_5_MED(xx_fmt, a, b, c, d, e)                        \
  MSG_5(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c, d, e)

#define PROSE_MSG_6_MED(xx_fmt, a, b, c, d, e, f)                     \
  MSG_6(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c, d, e, f)

#define PROSE_MSG_7_MED(xx_fmt, a, b, c, d, e, f, g)                  \
  MSG_7(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c, d, e, f, g)

#define PROSE_MSG_8_MED(xx_fmt, a, b, c, d, e, f, g, h)               \
  MSG_8(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b, c, d, e, f, g, h)


#define PROSE_MSG_HIGH(xx_fmt)                                        \
  MSG(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt)

#define PROSE_MSG_1_HIGH(xx_fmt, a)                                   \
  MSG_1(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a)

#define PROSE_MSG_2_HIGH(xx_fmt, a, b)                                \
  MSG_2(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b)

#define PROSE_MSG_3_HIGH(xx_fmt, a, b, c)                             \
  MSG_3(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c)

#define PROSE_MSG_4_HIGH(xx_fmt, a, b, c, d)                          \
  MSG_4(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d)

#define PROSE_MSG_5_HIGH(xx_fmt, a, b, c, d, e)                       \
  MSG_5(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e)

#define PROSE_MSG_6_HIGH(xx_fmt, a, b, c, d, e, f)                    \
  MSG_6(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e, f)

#define PROSE_MSG_7_HIGH(xx_fmt, a, b, c, d, e, f, g)                 \
  MSG_7(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e, f, g)

#define PROSE_MSG_8_HIGH(xx_fmt, a, b, c, d, e, f, g, h)              \
  MSG_8(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e, f, g, h)


#define PROSE_MSG_FATAL(xx_fmt)                                       \
  MSG(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt)

#define PROSE_MSG_1_FATAL(xx_fmt, a)                                  \
  MSG_1(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a)

#define PROSE_MSG_2_FATAL(xx_fmt, a, b)                               \
  MSG_2(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b)

#define PROSE_MSG_3_FATAL(xx_fmt, a, b, c)                            \
  MSG_3(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c)

#define PROSE_MSG_4_FATAL(xx_fmt, a, b, c, d)                         \
  MSG_4(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c, d)

#define PROSE_MSG_5_FATAL(xx_fmt, a, b, c, d, e)                      \
  MSG_5(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c, d, e)

#define PROSE_MSG_6_FATAL(xx_fmt, a, b, c, d, e, f)                   \
  MSG_6(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c, d, e, f)

#define PROSE_MSG_7_FATAL(xx_fmt, a, b, c, d, e, f, g)                \
  MSG_7(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c, d, e, f, g)

#define PROSE_MSG_8_FATAL(xx_fmt, a, b, c, d, e, f, g, h)             \
  MSG_8(MSG_SSID_LTE_RRC, MSG_LEGACY_FATAL, xx_fmt, a, b, c, d, e, f, g, h)


#define PROSE_MSG_SPRINTF_1_LOW(xx_fmt, a)                            \
  MSG_SPRINTF_1(MSG_SSID_LTE_RRC, MSG_LEGACY_LOW, xx_fmt, a)

#define PROSE_MSG_SPRINTF_2_LOW(xx_fmt, a, b)                         \
  MSG_SPRINTF_2(MSG_SSID_LTE_RRC, MSG_LEGACY_LOW, xx_fmt, a, b)

#define PROSE_MSG_SPRINTF_1_MED(xx_fmt, a)                            \
  MSG_SPRINTF_1(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a)

#define PROSE_MSG_SPRINTF_2_MED(xx_fmt, a, b)                         \
  MSG_SPRINTF_2(MSG_SSID_LTE_RRC, MSG_LEGACY_MED, xx_fmt, a, b)


#define PROSE_MSG_SPRINTF_1_HIGH(xx_fmt, a)                            \
  MSG_SPRINTF_1(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a)

#define PROSE_MSG_SPRINTF_2_HIGH(xx_fmt, a, b)                         \
  MSG_SPRINTF_2(MSG_SSID_LTE_RRC, MSG_LEGACY_HIGH, xx_fmt, a, b)


/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

#endif /* PROSE_DIAG_H */
