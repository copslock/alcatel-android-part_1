/*!
  @file
  prose_pc3_manager.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_pc3_manager.h#10 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_PC3_MANAGER_H
#define PROSE_PC3_MANAGER_H

#include "hashmap.h"

#include "prose_defs.h"
#include "ds_lted_ext_msg.h"

#include "prose_timer_pool.h"

class XmlContext;
class prose_discovery_message;

class ProsePC3Manager
{
  public:
    static ProsePC3Manager* get_instance();

    virtual ~ProsePC3Manager();

    void deinit();

    static void destroy();

    void send_discovery_announce_req(uint32 session_id, const prose_os_app_id_s& app_id, const prose_pa_id_s& pa_id, uint32 de_id, uint32 request_time, const char* metadata, uint32 metadata_size);

    void send_discovery_update_metadata_req(uint32 session_id, const prose_os_app_id_s& app_id, const prose_pa_id_s& pa_id, uint32 de_id, uint32 request_time, const char* metadata, uint32 metadata_size);

    void send_discovery_monitor_req(uint32 session_id, const prose_os_app_id_s& app_id, const prose_pa_id_s& pa_id, uint32 de_id, uint32 request_time);

    int send_match_report(uint32 session_id, const prose_app_code_s& pac, uint32 mic, uint32 utc_time, const sys_plmn_id_s_type& mplmn, const sys_plmn_id_s_type& rplmn);

    void set_req_id(uint32 req_id);

    void send_cancel_req(uint32 session_id);

    void process_response(const ds_appsrv_lte_d_post_result_ind_s* rsp);

    errno_enum_type hexbinary_encode(const unsigned char hex[], uint32 size, char* str, uint32 str_len);

    errno_enum_type hexbinary_decode(const char* str, unsigned char hex[], uint32 size);

    void reverse_memcpy(unsigned char *dest, const unsigned char *src, uint32 size);

    void enable_batch_requests();

    void process_batch_timer_expiry(TimerPool::timer_id id);

  private:
    ProsePC3Manager();
    ProsePC3Manager(const ProsePC3Manager&);
    ProsePC3Manager& operator=(const ProsePC3Manager&);

    static const uint32 MAX_OFFSET;

    unsigned get_next_trans_id()
    {
      return trans_id_counter_++;
    }

    void send_discovery_req(uint32 session_id, int command, const prose_os_app_id_s& app_id, const prose_pa_id_s& pa_id, uint32 de_id, uint32 request_time, const char* metadata = 0, uint32 metadata_size = 0);

    void send_ds_appsrv_post_start_req(prose_discovery_message* msg);

    void dispatch(const prose_discovery_message* msg);

    void process_xml_failure(const list<uint8>* transids);

    bool check_clock(const char* str, uint32 offset);

    uint8 trans_id_counter_;

    list<uint8>* transids_;

    list<list<uint8>*> unacked_transid_list_;

    XmlContext *xml_context_;

    static ProsePC3Manager *instance_;

    hashmap<uint8,uint32> transid_sessionid_map_;

    hashmap<uint32,list<uint8>*> reqid_transid_map_;

    bool batch_requests_;

    prose_discovery_message *prev_msg_;

    TimerPool::timer_id tid_;

    enum
    {
      DISCOVERY_REQ_RESERVED = 0,
      DISCOVERY_REQ_ANNOUNCE,
      DISCOVERY_REQ_MONITOR,
      DISCOVERY_REQ_QUERY,
      DISCOVERY_REQ_RESPONSE,
      DISCOVERY_REQ_UPDATE_METADATA,
      DISCOVERY_REQ_UNUSED
    };

    char temp_str_[PROSE_APP_ID_SIZE + 1];

    int get_mcc(const sys_plmn_id_s_type* plmn);

    int get_mnc(const sys_plmn_id_s_type* plmn);
};

#endif /* PROSE_PC3_MANAGER_H */
