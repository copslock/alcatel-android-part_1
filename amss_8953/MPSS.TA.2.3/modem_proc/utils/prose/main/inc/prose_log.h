/*!
  @file
  prose_log.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_log.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_LOG_H
#define PROSE_LOG_H

#include <comdef.h>
#include <log.h>
#include "prose_defs.h"

/*! @brief Log packet version 1 */
#define PROSE_LOG_PACKET_VER_1 1

/*! @brief PROSE log enumeration type. Each enumeration will be mapped to a unique
 * log packet/log code.
 */
typedef enum
{
  PROSE_LOG_DISC_REQ_INFO_MSG_ID = 0x0000, /*!< Discovery request message */
  PROSE_LOG_DISC_ANNOUNCE_INFO_MSG_ID = 0x0001, /*!< LTE-D Announce Information*/
  PROSE_LOG_DISC_MONITOR_INFO_MSG_ID = 0x0002, /*!< LTE-D Monitor Information */
  PROSE_LOG_DISC_MATCH_INFO_MSG_ID = 0x0003,/*!< LTE-D Match Ack Information from the server */
  PROSE_LOG_OUTGOING_PC3_MSG_ID = 0x0004, /*!< PC3 message sent from UE to Prose Server */
  PROSE_LOG_INCOMING_PC3_MSG_ID = 0x0005, /*!< PC3 message sent from Prose Server to UE */
  PROSE_LOG_MAX_ID
} prose_log_id_e;

/*! @brief PROSE log control structure
 */
typedef struct
{
  uint16 log_code;
  uint16 max_size;
  uint16 write_index;
  uint16 buf_size;
  uint8 *write_buf;
  uint8 *read_buf;
} prose_log_ctl_s;

/*! @brief PROSE Timer Status enum type.
 */
typedef enum
{
  PROSE_LOG_TIMER_STARTED = 0,      /*!< Timer was started */
  PROSE_LOG_TIMER_STOPPED = 1,      /*!< Timer is stoppped by Prose module*/
  PROSE_LOG_TIMER_EXPIRED = 2,      /*!< Timer expired */
  PROSE_LOG_TIMER_MAX_STATUS
} prose_log_timer_status_e;

#ifdef _WIN32
#pragma pack(push,1) // Save previous, and turn on 1 byte alignment
#endif

/*! @brief PROSE Timer Status event structure
 * */
typedef PACK(struct)
{
  uint8 timer;  /*!< Timer Name */
  uint64 remain_time;  /*!< Remaining time in sec */
  uint8 status;  /*!< timer status:  Started, Stopped or Expired - prose_log_timer_status_e*/
  uint32 id; /*!< Session Id */
} prose_log_timer_status_event_s;


/*! @brief PROSE Discovery Session Start event structure
 */
typedef PACK(struct)
{
  uint32 id;              /*!< Session Id */
  
  uint8 type;             /*!< Type 1 - Announce
                                    2 - MOnitor 
                           */
  uint32 duration;        /*!< Session Duration */
  uint32 request_time;    /*!< Requested time */
  uint8  metadata_flag;   /*!< Metadata flag 0-False/1-True */
} prose_log_disc_session_start_event_s;

/*! @brief PROSE Discovery Session Stop event structure
 */
typedef PACK(struct)
{
  uint32 id;              /*!< Session Id */
} prose_log_disc_session_stop_event_s;

/*! @brief PROSE Status event structure
 */
typedef PACK(struct)
{
  uint32 id;              /*!< Session Id */
  uint8 status;
} prose_log_disc_session_status_event_s;

/*! @brief PROSE Service Status event structure
 */
typedef PACK(struct)
{
  uint8 service_available; /*!< LTED Available 0-False/1-True */
} prose_log_service_status_event_s;

#define PROSE_LOG_OS_APP_ID_SIZE 256
#define PROSE_LOG_PA_ID_SIZE 2048
#define PROSE_LOG_PAC_SIZE 23
#define PROSE_LOG_KEY_SIZE 16
#define PROSE_LOG_METADATA_SIZE 128 
#define PROSE_LOG_MAX_FILTERS 5 
#define PROSE_LOG_MAX_PC3_MSG_SIZE 4096 

/*! @brief This structure is for logging Prose Discovery Request from QMI
*/
typedef PACK(struct)
{
  uint8 log_packet_ver; /*!< Log packet version. Range: 0...255. Version 24 */
  uint8 type;                                      /*! Type 1 - Announce
                                                            2 - MOnitor
                                                      */
  uint32 id;              /*!< Session Id */
  uint32 os_app_id_size;  /*!< OS App ID size */
  uint8 os_app_id[PROSE_LOG_OS_APP_ID_SIZE];       /*!< OS App ID field valid until os_app_id_size */
  uint32 pa_id_size;      /*!< PA ID size */
  uint8 pa_id[PROSE_LOG_PA_ID_SIZE];             /*!< PA Id field valid until pa_id_size*/
  uint32 metadata_size;   /*!< Metadata size */
  uint8 metadata[PROSE_LOG_METADATA_SIZE];         /*!< Metadata field valid until metadata_size */
} prose_log_disc_req_info_s;

/*! @brief This structure is for logging Prose Announce Info used for the session
*/
typedef PACK(struct)
{
  uint8 log_packet_ver;                      /*!< Log packet version. Range: 0...255. Version 24*/
  uint32 id;              /*!< Session Id */
  uint8 pac[PROSE_LOG_PAC_SIZE];             /*!< PAC */
  uint8 key[PROSE_LOG_KEY_SIZE];             /*!< Key */
  uint64 validity_time;   /*!< Validity time */
} prose_log_disc_announce_info_s;

/*! @brief This structure is for logging Prose Monitor Info used for the session
*/
typedef PACK(struct)
{
  uint8 log_packet_ver; /*!< Log packet version. Range: 0...255. Version 24*/
  uint32 id;                /*!< Session Id */
  uint32 num_filters;       /*!< Num filters */
  PACK(struct) {
    uint8 pac[PROSE_LOG_PAC_SIZE];             /*!< PAC */
    uint32 num_masks;                          /*!< Num Masks */
    uint8 mask[PROSE_LOG_MAX_FILTERS][PROSE_LOG_PAC_SIZE];        /*!< Mask valid for only num_masks*/
    uint64 validity_time;   /*!< Validity time */
    uint8 installed_flag;                                         /*!< Installed 0-No / 1-Yes */
  } filter[PROSE_LOG_MAX_FILTERS];  /*!< FILTER ARRAY valid for num_filters*/
} prose_log_disc_monitor_info_s;

/*! @brief This structure is for logging Prose Match Info being used
*/
typedef PACK(struct)
{
  uint8 log_packet_ver; /*!< Log packet version. Range: 0...255. Version 24 */
  uint32 id;              /*!< Session Id */
  uint8 pac[PROSE_LOG_PAC_SIZE];             /*!< PAC */
  uint32 pa_id_size;      /*!< PA ID size */
  uint8 pa_id[PROSE_LOG_PA_ID_SIZE];         /*!< PA Id valid until pa_id_size*/
  uint32 metadata_size;   /*!< Metadata size */
  uint8 metadata[PROSE_LOG_METADATA_SIZE];   /*!< Metadata valid until metadata_size*/
  uint64 validity_time;   /*!< Validity time */
} prose_log_disc_match_info_s;

/*! @brief This structure is for logging Prose Outgoing PC3 Message
*/
typedef PACK(struct)
{
  uint8 log_packet_ver;    /*!< Log packet version. Range: 0...255. Version 24 */
  uint32 msg_size;        /*!< Msg size */
  uint8 msg[PROSE_LOG_MAX_PC3_MSG_SIZE];             /*!< Msg valid for msg_size*/
} prose_log_outgoing_pc3_msg_s;

/*! @brief This structure is for logging Prose Outgoing PC3 Message
*/
typedef PACK(struct)
{
  uint8 log_packet_ver; ///< Log packet version. Range: 0...255. Version 24
  uint8 ds_err_code;      /*!< DS Error Code */
  uint8 status_code;      /*!< Status Code */
  uint32 msg_size;        /*!< Msg size */
  uint8 msg[PROSE_LOG_MAX_PC3_MSG_SIZE];             /*!< Msg valid for msg_size*/
} prose_log_incoming_pc3_msg_s;

#ifdef _WIN32
#pragma pack(pop) // Revert alignment to what it was previously
#endif

extern void prose_log_disc_req_info(uint8 type, uint32 id, const prose_os_app_id_s *os_app_id,
    const prose_pa_id_s *pa_id, const char *metadata, uint32 metadata_size);

extern void prose_log_disc_announce_info(uint32 id, const prose_app_code_s *pac,
    const prose_disc_key_s *key, uint64 validity_time);

extern void prose_log_disc_monitor_info(uint32 id, uint32 num_filters, 
    const prose_app_code_s *filter, uint8 num_masks, 
    const prose_app_code_s *mask, uint64 validity_time, uint32 macid);

extern void prose_log_disc_match_info(uint32 id, const prose_app_code_s *pac,
    const prose_pa_id_s *pa_id, const char *metadata, uint32 metadata_size, uint64 validity_time);

extern void prose_log_outgoing_pc3_msg(uint32 msg_size, const char *msg);

extern void prose_log_incoming_pc3_msg(uint8 err_code, uint8 status_code, uint32 msg_size,
    const char *msg);


extern uint8 *prose_log_alloc
(
  prose_log_id_e log_id,
  uint16 size
);

extern void prose_log_commit
(
  prose_log_id_e log_id
);

extern void prose_log_init
(
  prose_log_id_e log_id
);

extern boolean prose_log_check_enable
(
  prose_log_id_e log_id
);

#endif /* PROSE_LOG_H */

