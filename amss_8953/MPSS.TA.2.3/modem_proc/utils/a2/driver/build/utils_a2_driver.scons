#===============================================================================
#
# Utils A2 Driver
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014 by Qualcomm Technologies, Inc.
# All Rights Reserved.
# Qualcomm Technologies, Inc Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who       what, where, why
# --------   ---       --------------------------------------------------------
# 05/27/15   mm        Added include paths to support ATF
# 03/02/15   xl       Cleanup SCONSCOP Violation
# 11/20/13   mm       Added policy_optin key/value pair to tasks
# 04/19/13   ca       Added MOB_UTILS tag
# 04/08/13   md       Reduced stack size
# 07/30/12   rp       Added A2 Log Task
# 11/26/12   hangdong Updated MPROC with dep on CORE
# 03/07/12   ars      RCInit: Added cpu_affinity plus some changes
# 02/13/12   ars      RCInit changes
# 11/21/10   ae       Initial version.
#===============================================================================
Import('env')
env = env.Clone()

if 'USES_GNSS_SA' in env:
    Return()
from glob import glob
from os.path import join, basename
import os

# recursively searches the directory and adds to include path
def search(dir):
    env.Append(CPPPATH = [dir])
    for item in os.listdir(dir):
        if os.path.isdir(os.sep.join([dir, item])):
            search(os.sep.join([dir, item]))

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

# set to True in order to enable ATF
a2_atf_enabled = False

if a2_atf_enabled:
    # define feature
    env.Append(CPPDEFINES = [ 'FEATURE_A2_ATF' ])

    # path to ATF framework/test files
    path_test_atf = env.subst('${UTILS_ROOT}/a2/driver/test_atf')

    if os.path.isdir(path_test_atf):
        # recursively add subdirectories to preprocessor path            
        search(path_test_atf)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_A2",
])

#-------------------------------------------------------------------------------
# Necessary Public and Restricted API's
#-------------------------------------------------------------------------------
env.RequirePublicApi([
    'HWENGINES',
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'WIREDCONNECTIVITY',
    'STORAGE',
    'DAL',
    'POWER',
    'BUSES',
    'SECUREMSM',
    'MPROC',
    'KERNEL',                             # needs to be last 
    ], area='core')

env.RequirePublicApi([
  'MCFG',
       ],
       area='MCFG')

# ------------------------------------------------------------------------------
# UTILS A2 DRIVER API
#-------------------------------------------------------------------------------
env.PublishPrivateApi('UTILS_A2_TEST',['${UTILS_ROOT}/a2/driver/test/inc'])

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
UTILS_A2_DRIVER_SOURCES = ['${BUILDPATH}/' + basename(fname)
                   for fname in glob(join(env.subst(SRCPATH), '*.c'))]

# Compile the UTILS A2 Driver source and convert to a binary library
env.AddBinaryLibrary(['MODEM_MODEM','MOB_UTILS'], '${BUILDPATH}/utils_a2_driver', UTILS_A2_DRIVER_SOURCES, pack_exception=['USES_CUSTOMER_GENERATE_LIBS'])

# RC Init Task Dictionary
RCINIT_TASK_A2 = {
            'thread_name'          : 'a2',
            'sequence_group'       : env.subst('$MODEM_DRIVER'),
            'stack_size_bytes'     : env.subst('$A2_STKSZ'),
            'priority_amss_order'  : 'A2_PRI_ORDER',
            'thread_entry'         : 'a2_task_main',
            'cpu_affinity'         : 'REX_ANY_CPU_AFFINITY_MASK',
            'policy_optin'         : ['default', 'ftm', ]
    }
    
RCINIT_TASK_A2_LOG = {
            'thread_name'          : 'a2_log',
            'sequence_group'       : 'RCINIT_GROUP_3',
            'stack_size_bytes'     : '8192',
            'priority_amss_order'  : 'SHARED_IDLE_SERVICE_PRI_ORDER',
            'thread_entry'         : 'a2_log_task_main',
            'cpu_affinity'         : 'REX_ANY_CPU_AFFINITY_MASK',
            'policy_optin'         : ['default', 'ftm', ]
    }

# Add tasks to RCInit
if 'USES_MODEM_RCINIT' in env:
            env.AddRCInitTask(['MODEM_MODEM','MOB_UTILS'], RCINIT_TASK_A2)

if 'USES_RCINIT' in env:
            env.AddRCInitTask(['MODEM_MODEM','MOB_UTILS'], RCINIT_TASK_A2_LOG)

      
#-------------------------------------------------------------------------------
# Look for test build files
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()
