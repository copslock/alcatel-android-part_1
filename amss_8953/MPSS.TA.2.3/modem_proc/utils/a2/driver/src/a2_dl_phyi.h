#ifndef LTE_A2_DL_PHYI_H
#define LTE_A2_DL_PHYI_H

/*!
  @file a2_dl_phyi.h

  @brief
   The internal interface to the downlink phy portion of the a2 driver for LTE.

  @ingroup a2_int_interface
*/

/*==============================================================================

  Copyright (c) 2016 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/a2/driver/src/a2_dl_phyi.h#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/21/16   vt      CR 1014681: DL UL Debug code
10/08/14   ca      CR: 735597, Resolved compilation errors for TABASCO
09/05/14   mm      Added signal to shutdown procedure
08/20/14   ca      CR: 711304, Added message handling for 
                   A2_DL_PHY_SET_DEC_INTR_REQ
08/05/14   mm      Added a2_dl_phy_shut_down()
05/09/14   mm      Naming cleanup; removed unused stat num_intr_in_hw_off_state
05/07/14   ca      Added stats for num tbs dropped.
03/28/14   mm      Moved structs from a2_dl_phy.c for QSH
01/13/14   ca      DSDA feature is implemented.
11/08/13   mm      Added support for LTE cat-6/CA
10/08/13   ar      Initial Checkin


==============================================================================*/


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <a2_dl_phy.h>
#include <a2_common.h>
#include <a2_dbg.h>
#include <a2_taskq.h>
#include <a2_poweri.h>
#include <a2_status_util.h>
#include <comdef.h>
#include <qurt.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*!@brief macro to define debug TB data size */
#define A2_DL_PHY_DBG_TB_DATA_SIZE  32

#ifdef FEATURE_A2_SW_WORKAROUND_LTE_DL_PHY_CRC

/*! @brief crc size in bytes */
#define A2_DL_PHY_LTE_CRC_SIZE_IN_BYTES 3

#endif /* FEATURE_A2_SW_WORKAROUND_LTE_DL_PHY_CRC */

/* Max entries in DL PHY profiling table - specify power of 2 */
#define A2_DL_PHY_MAX_PROF_POW 6
#define A2_DL_PHY_MAX_PROF_CNT (1 << A2_DL_PHY_MAX_PROF_POW)
#define A2_DL_PHY_MAX_PROF_CNT_MASK (0xffffffff >> (32 - A2_DL_PHY_MAX_PROF_POW))

/* Max entries in default tech IRQ vector table - specify power of 2 */
#define A2_DL_PHY_MAX_IRQ_POW 3
#define A2_DL_PHY_MAX_IRQ_CNT (1 << A2_DL_PHY_MAX_IRQ_POW)
#define A2_DL_PHY_MAX_IRQ_CNT_MASK (0xffffffff >> (32 - A2_DL_PHY_MAX_IRQ_POW))

/*! @brief Enum for signal masks */
typedef enum
{
  A2_DL_PHY_SIGNAL_MASK_SHUTDOWN = 0x1
} a2_dl_phy_signal_mask_e;

/* Enum for possible tag ids */
typedef enum
{
  A2_DL_PHY_TAG_ID_SHUTDOWN_SIGNAL = 0x0,
  A2_DL_PHY_TAG_ID_MAX
} a2_dl_phy_tag_id_e;

/*!
   @brief Structure to hold a2 sub id information such as,
   the inst bf and demback bf.
*/
typedef struct
{
  /*! variable to store instance bf for a given sub id 
      the set bits represent hardware resources allocated for this sub id */
  a2_hw_inst_bf_t   hw_inst_bf;
  
  /*! variable to store dem back bf for a given sub id 
      the set bits indicate the combinations of dembacks used for this sub id */
  a2_demback_bf_t   demback_bf;

  /*! decob interrupt type config flag 
    TRUE - enable; FALSE - disable */
  boolean           lte_imm_intr_flag;
} a2_dl_phy_sub_id_info_s;

/*!
   @brief
    Structure to hold cached info of all the tasks written by this module
*/
typedef struct
{
  hwtask_a2_ts_db_vbuf_cfg_t          vbuf_cfg_for_dob_0;
  hwtask_a2_ts_db_vbuf_cfg_1_t        vbuf_cfg_for_dob_1;
  hwtask_a2_ts_dma_irq_t              config_init_irq;
  hwtask_a2_dl_phy_ts_config_init_t   dl_phy0_config_init;
  hwtask_a2_dl_phy_1_ts_config_init_t dl_phy1_config_init;
} a2_dl_phy_cached_tasks_s;

typedef struct
{
  hwtask_a2_ts_dma_tag_t              tag;
} a2_dl_phy_cached_tasks_inst_s;


typedef struct
{
  uint32  get_data_time;
  /*Statusq_rd ptr */
  uint32  statusq_rd_ptr;
  a2_dl_phy_tb_info_t meta_info;  
  /*TB data */
  uint8  (*tb_data)[A2_DL_PHY_DBG_TB_DATA_SIZE];
}a2_dl_phy_hw_profiling_s;

  
/*! @brief structure to debug unexpected interrupts */
typedef struct
{
  uint32        irq_vector;
  a2_timetick_t timetick;
} a2_dl_phy_irq_dbg_s;

/*!
   @brief
    Structure to hold statistics common to all LTE instances
*/
typedef struct
{
  /*!< num DMA IRQ interrupts received */
  uint32                  num_dma_irq_intr;
  
  /*!< num of times mac called a2_dl_phyi_get_data() */
  uint32                  num_get_data_calls;
  
  /*!< num times LTE MAC called a2_dl_phyi_set_dec_interrupt_type() and A2 was 
       in LTE tech */
  uint32                  num_intr_cfg_in_lte_tech;
  
  /*!< num times LTE MAC called a2_dl_phyi_set_dec_interrupt_type() but A2
       wasn't in LTE tech */
  uint32                  num_intr_cfg_in_non_lte_tech;
  
  /*!< num of intr received when DL PHY is in default tech */
  uint32                  num_intr_in_def_tech;
  
  /*!< IRQ #s of intr received when DL PHY is in default tech */
  a2_dl_phy_irq_dbg_s     irq_vector_in_def_tech[A2_DL_PHY_MAX_IRQ_CNT];
  
  uint8                   irq_vector_in_def_tech_index;
  
  /*!< num of times get data is skipped because DL PHY tech is DEFAULT */
  uint32                  num_get_data_calls_skipped;
  
  /*!< num TBs flushed during dl phy shyt down */
  uint32                  num_tbs_dropped_no_mac_cb;
} a2_dl_phy_stats_common_s;

/*!
   @brief
    Structure to hold statistics specific to LTE instances
*/
typedef struct
{
  /*!< num transport block interrupts received */
  uint32    num_trblk_intr;
  
  /*!< num tranport blocks received */
  uint32    num_trblk_rcvd;
  
  /*!< num tranport block bytes received */
  uint32    num_trblk_bytes_rcvd;
  
  /*!< num invalid tbs received */
  uint32    num_invalid_trblk_rcvd;
  
  /*!< num invalid tb bytes received */
  uint32    num_invalid_trblk_bytes_rcvd;

  /*!< num DMA IRQ interrupts received after switching the dl phy intr mode
       from timer to immediate */
  uint32    num_config_init_intr;

  /*!< num interrupts received with no callback set */
  uint32    num_intr_rcvd_no_cb;

  /*!< num of times a2_dl_phy_process_statusq was called on this instance and 
       no TBs were received */
  uint32    num_process_statusq_calls_no_tbs;
  /*!< number of times dl phy  statusQ is not empty during dl phy shut down */
  uint32    num_dl_phy_statusq_not_empty_at_shutdown;
  
  /*!< num of times taskq/statusq rd/wr ptr mismatched at unset_technology() */
  uint32    num_taskq_rd_wr_mismatch;
  uint32    num_statusq_rd_wr_mismatch;
  
  /* num of times shutdown blocked to wait for rd/wr ptrs */
  uint32    num_shutdown_blocked;
  
  /* max latency in us waiting for rd/wr ptrs */
  uint32    max_shutdown_latency_us;

#ifdef FEATURE_A2_SW_WORKAROUND_DL_PHY_INVALID_STATUS
  uint32    wrong_frag_dst_len_cnt;
  uint32    wrong_frag_dst_addr_cnt;
#endif
  /* HW profiling stats for 1 TB dma */
  a2_dl_phy_hw_profiling_s    profiling[A2_DL_PHY_MAX_PROF_CNT];
  /* currnet index pointing into hw_profiling_info array */
  uint8        profiling_idx;

  /*Max tb count */
  uint32  max_tb_count;
} a2_dl_phy_stats_lte_s;


/*!
   @brief
    Data structure common to both LTE instances
*/
typedef struct
{

  a2_dl_phy_cached_tasks_s         cached_tasks;

  a2_dl_phy_tb_available_cb        tb_recd_cb;
  
  a2_dl_phy_stats_common_s         stats;
  /*! @a2 dl phy look up table of instance bf and demback bf from sub id */
  a2_dl_phy_sub_id_info_s          sub_id_info_tbl[A2_SUB_ID_MAX];
  
  /* signal for shutdown */
  qurt_signal_t                    shutdown_signal;
  
} a2_dl_phy_common_s;

/* Struct to store the aggregation info for LTE-D*/
typedef struct
{
   /*Store the aggregation TB address*/
   void*  agg_tb_start_addr;

   /*Store the total Aggregated length*/
   uint32  agg_tb_length;

   /*backup the lsw*/
   uint32 sw_info_lsw_backup;

   /*backup the msw*/
   uint32 sw_info_msw_backup;
}a2_dl_phy_lted_agg_info_s;
/*!
   @brief
    Data structure specific to LTE instance
*/
typedef struct
{
  a2_taskq_s                        taskq;
  a2_taskq_status_s                 statusq;
  
  a2_dl_phy_cached_tasks_inst_s     cached_tasks;
  
  a2_status_util_dsm_chain_info_s   partial_dsm_chain_info;

  /*! last statusq frag ptr processed by the DL PHY */
  dsm_item_type*                    last_frag_proc_by_dl_phy;

  uint32                            tb_len_in_bytes;
  void                             *tb_addr;
  
  a2_dl_phy_stats_lte_s             stats;

  /*! technology for which a2 dl phy has been configured to */
  a2_technology_e                   curr_technology;

  /*! a2 power collapse related debug info */
  a2_pc_dbg_s                       pc_dbg;
  
  /*! a variable to store sub_id for a particular demback_id */
  a2_sub_id_t                       sub_id;

  /*! a variable to store aggregation info for LTE-D*/
  a2_dl_phy_lted_agg_info_s         lted_agg_info;

} a2_dl_phy_inst_s;

/*! @brief
     DL PHY top level structure */
typedef struct
{
  a2_dl_phy_common_s    common;
  a2_dl_phy_inst_s      inst[A2_HW_INST_ID_MAX];
} a2_dl_phy_s;

/*! @brief define enumuration for DL PHY VBUF selection. Maps to the vbuf_sel 
   field in hwtask_a2_ts_a2_mp_vbuf_cfg_t. Can take atmost 8 values */
typedef enum
{
  /*! VBUF Undefined */
  A2_DL_PHY_VBUF_CFG_SEL_UNDEFINED,
  /*! LTE 0 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_LTE_0,
  /*! LTE 1 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_LTE_1,
  /*! W 0 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_WCDMA_0,
  /*! W 1 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_WCDMA_1,
  /*! TD 0 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_TDSCDMA_0,
  /*! TD 1 VBUF */
  A2_DL_PHY_VBUF_CFG_SEL_TDSCDMA_1,
  A2_DL_PHY_VBUF_CFG_SEL_MAX
} a2_dl_phy_vbuf_cfg_sel_e;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  a2_dl_phy_get_top_lvl_ptr

==============================================================================*/
/*!
  @brief
  Returns a pointer to the top-level data structure.
*/
/*============================================================================*/
const a2_dl_phy_s * a2_dl_phy_get_top_lvl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_get_taskq

==============================================================================*/
  /*!
    @brief
    This function returns the pointer to a2 dl phy taskq. The taskq instance is
    decided by the argument demback id */  
/*============================================================================*/
a2_taskq_s* a2_dl_phy_get_taskq(a2_demback_id_e demback_id);

/*==============================================================================

  FUNCTION:  a2_dl_phy_get_statusq


==============================================================================*/
  /*!
    @brief
    This function returns the pointer to a2 dl phy statusq. The taskq instance
    is decided by the argument demback id */  
/*============================================================================*/
a2_taskq_status_s* a2_dl_phy_get_statusq(a2_demback_id_e demback_id);


/*==============================================================================

  FUNCTION:  a2_dl_phy_set_technology

==============================================================================*/
/*!
  @brief
  Set the technology that is going to use A2 DL PHY HW and perform necessary
  HW initialization.

  @caller
  HSPA: whenever handover to HSPA is initiated and/or whenever WCDMA cell search
        starts
  LTE: whenever UE starts LTE cell search

  @todo - need to see from when and where to call this function. Critical from
   HSPA<->LTE hand-off. Need to make sure that task bring up doesn't happen in
   parallel. If yes then there is a need to sync up between LTE and HSPA.
   Probable mutex to guarantee mutual exclusion.
*/
/*============================================================================*/
void a2_dl_phy_set_technology
(
  a2_sub_id_t     sub_id, /* subscriber id */
  a2_technology_e technology, /* technology */
  a2_hw_inst_bf_t inst_bf,
  a2_demback_bf_t demback_bf
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_unset_technology

==============================================================================*/
/*!
  @brief
  Set the technology that is going to use A2 DL PHY HW and perform necessary
  HW initialization.

  @caller
  HSPA: whenever handover to HSPA is initiated and/or whenever WCDMA cell search
        starts
  LTE: whenever UE starts LTE cell search

  @todo - need to see from when and where to call this function. Critical from
   HSPA<->LTE hand-off. Need to make sure that task bring up doesn't happen in
   parallel. If yes then there is a need to sync up between LTE and HSPA.
   Probable mutex to guarantee mutual exclusion.
*/
/*============================================================================*/
void a2_dl_phy_unset_technology
(
  a2_sub_id_t     sub_id /* subscriber id */
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_shut_down

==============================================================================*/
/*!
  @brief
  Verifies that taskq/statusq have been processed.
*/
/*============================================================================*/
void a2_dl_phy_shut_down
(
  void
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_hw_init

==============================================================================*/
/*!
    @brief
    initializes the HW related registers for DL PHY block

    @return
    None
*/
/*============================================================================*/
void a2_dl_phy_hw_init
(

  a2_sub_id_t     sub_id, /* subscriber id */
  a2_technology_e tech /* tech to boot a2 up in */
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_init

==============================================================================*/
/*!
    @brief
    Initializes the SW structures of DL PHY block

    @return
    None
*/
/*============================================================================*/
void a2_dl_phy_init ( void );

/*==============================================================================

  FUNCTION:  a2_dl_phy_deinit

==============================================================================*/
/*!
    @brief
    De-initializes the downlink peripheral block

    @return
    None
*/
/*============================================================================*/
extern void a2_dl_phy_deinit(void);

/*==============================================================================

  FUNCTION:  a2_dl_phy_ist_cb

==============================================================================*/
/*!
  @brief
  Interrupt handler for A2 DL PHY module.

*/
/*============================================================================*/
int a2_dl_phy_ist_cb ( void* arg );

/*==============================================================================

  FUNCTION:  a2_dl_phy_hspa_init

==============================================================================*/
/*!
    @brief
    Initializes variables associated with hspa init block

    @caller
    A2 Init (during A2 driver initialization)

    @return
    None
*/
/*============================================================================*/
void a2_dl_phy_hspa_init ( void );

/*==============================================================================

  FUNCTION:  a2_dl_phy_hspa_get_status

==============================================================================*/
/*!
    @brief
    Reads the status from the status queue. Calls appropriate call back
    functions based on the status/tag received.

    @note
    Takes care of necessary cache invaldiation before processing status queue.

    @caller
    "HSPA Header Read ISR" (primary) and/or "MAC DL HS"

    @return
    boolean: TRUE if any status is read from the status queue. FALSE if status
             is not yet updated.
*/
/*============================================================================*/
boolean a2_dl_phy_hspa_get_status
(
  a2_demback_id_e demback_id
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_hspa_unset_technology

==============================================================================*/
/*!
    @brief
    cleans up DL PHY HSPA state during a2 power collapse

    @return
    TRUE if shut down was successful
    FALSE otherwise
*/
/*============================================================================*/
void a2_dl_phy_hspa_unset_technology
(
  a2_demback_id_e demback_id
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_update_sub_id

==============================================================================*/
/*!
    @brief
    updates the sub id to instance id, demback id map.
    This fucniton is called when power_proc_req() is being processed.

    @return
    returns void
*/
/*============================================================================*/

void a2_dl_phy_add_sub_id
(
  a2_sub_id_t         sub_id,     /*!< sub id in the system */
  a2_hw_inst_bf_t     inst_bf, /*!< a2 instacne id  */
  a2_demback_bf_t     demback_bf_arg,  /*!< a2 demback id  */
  a2_technology_e     tech
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_hspa_add_sub_id

==============================================================================*/
/*!
    @brief
    updates the sub id to instance bf, demback bf table.
    This fucniton is called when power_proc_req() is being processed.

    @return
    returns void
*/
/*============================================================================*/
void a2_dl_phy_hspa_add_sub_id
(
  a2_sub_id_t         sub_id,     /*!< sub id in the system */
  a2_hw_inst_bf_t     inst_bf, /*!< a2 instacne id  */
  a2_demback_bf_t     demback_bf  /*!< a2 demback id  */
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_remove_sub_id

==============================================================================*/
/*!
    @brief
    removes the sub id info from sub id to instance bf, demback bf table.

    @return
    returns void
*/
/*============================================================================*/
void a2_dl_phy_remove_sub_id
(
  a2_sub_id_t         sub_id     /*!< sub id in the system */
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_validate_tech

==============================================================================*/
/*!
  @brief
  To validate the current technology, after the set technology is called.
  */
/*============================================================================*/
void a2_dl_phy_validate_tech
(
  a2_technology_e technology, /* technology */
  a2_demback_bf_t demback_bf /* dem back id */
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_get_sub_id_info_tbl_ptr
==============================================================================*/
/*!
  @brief
  To get the handle to the sub_id_to_inst_id_tbl table
  */
/*============================================================================*/
a2_dl_phy_sub_id_info_s* a2_dl_phy_get_sub_id_info_tbl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_get_demback_id_to_sub_id_ptr
==============================================================================*/
/*!
  @brief
  To get the handle to the sub id variable for a given demback id
  */
/*============================================================================*/
a2_sub_id_t* a2_dl_phy_get_demback_id_to_sub_id_ptr
(
  a2_demback_id_e demback_id
);

/*==============================================================================

  FUNCTION:  a2_dl_phy_set_boot_up_state
==============================================================================*/
/*!
  @brief
  To set the boot up state for taskqs
  */
/*============================================================================*/
void a2_dl_phy_set_boot_up_state
(
  boolean boot_up_state
);

/*==============================================================================

  FUNCTION:  a2_dl_phyi_process_set_dec_intr_type

==============================================================================*/
/*!
  @brief
  Set DL PHY data interrupt type: IMMEDIATE or PERIODIC for LTE technology

  @detail
  Note that this function is used in A2 context.
*/
/*============================================================================*/

void  a2_dl_phyi_process_set_dec_intr_type
(
  a2_sub_id_t      sub_id,        /* subscriber id in the system */
  boolean          imm_interrupt /* immedite interrupt value */  
);


#endif /* LTE_A2_DL_PHYI_H */

