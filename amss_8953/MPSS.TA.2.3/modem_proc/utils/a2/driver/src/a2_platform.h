#ifndef A2_PLATFORM_H
#define A2_PLATFORM_H
/*!
  @file a2_platform.h

  @brief
   A file defining platform specific parameters/functionality.

*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/a2/driver/src/a2_platform.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
12/30/14   mm      Updated IRQ #s for Tabasco
10/30/14   ca      CR: 735597, Resolving compilation errors for TABASCO
08/15/14   ca      CR:710338, Added new interrupt vectors for THOR
01/13/14   ca      DSDA feature is implemented.
12/04/13   mm      Fixed DL PHY 1 timer bug
07/11/13   ca      A2 rumi bring up IRQ map fix 
04/26/13   yjz     Add A2_DAL_DBG_ERROR_IRQ
03/20/13   ars     CR465878: Added checksum offload feature 
08/08/12   ars     DIME PHY changes after RUMI validation 
04/11/12   bn      Added in support for DIME
10/06/11   ar      added a2 power collapse support
10/04/09   sm      Remove feature FEATURE_A2_VIRTIO and moved the macro to .c 
                   file
06/12/09   sah     Pull out unused interupt numbers.
05/14/09   yg      Split the A2 initialization for Espresso into Memory
                   Mapping section and A2 Init section.
05/14/09   sah     Add in some new espresso initialization, featurization,
                   and split init.
05/13/09   sah     Fix build break and featurize espresso code.
05/13/09   yg      Added stub function:a2_platform_espresso_init for
                   Espresso related A2 initialization.
03/02/09   sah     Initial Checkin

==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

/*==============================================================================

                                MACROS

==============================================================================*/


/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @DAL Interrupt Vector Numbers */
/* todo - these values need to be changed FEATURE_TABASCO_MODEM*/ 
#define A2_DAL_FRAG_MOD_IRQ         153  /* FRAG MODEM, a2_evt[0] */
#define A2_DAL_TIMER0_IRQ           174  /* Timer 0, a2_evt[2] */
#define A2_DAL_TIMER1_IRQ           175  /* Timer 1, a2_evt[3] */
#define A2_DAL_TIMER2_IRQ           176  /* Timer 2, a2_evt[4] */
#define A2_DAL_TIMER3_IRQ           177  /* Timer 3, a2_evt[5] */
#define A2_DAL_DECOB0_IRQ           178  /* DL PHY for LTE case, a2_evt[6] */
#define A2_DAL_UL_PHY0_IRQ          198  /* UL PHY, a2_evt[7] */
#define A2_DAL_DL_PHY0_IRQ          199  /* DL PHY for WCDMA case, a2_evt[8] */
#define A2_DAL_UL_SEC0_IRQ          200  /* UL SEC, a2_evt[9] */
#define A2_DAL_UL_PHY1_IRQ          201  /* UL PHY, a2_evt[10] */
#define A2_DAL_DL_PHY1_IRQ          202  /* DL PHY for WCDMA case, a2_evt[11] */
#define A2_DAL_UL_SEC1_IRQ          138  /* UL SEC, a2_evt[12] */
#define A2_DAL_DECOB1_IRQ           154  /* DL PHY for LTE case, a2_evt[1] */
#define A2_DAL_DBG_ERROR_IRQ        125  /* ERROR IRQ*/
/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*===========================================================================

  FUNCTION:  a2_platform_init

===========================================================================*/
/*!
    @brief
    Initializes the A2 blocks for that specific platform

    @return
    None
*/
/*=========================================================================*/
void a2_platform_init( void );

/*===========================================================================

  FUNCTION:  a2_platform_sync_mcdma

===========================================================================*/
/*!
    @brief
    Finishes syncing the mcdma hw logical channels.

    @return
    None
*/
/*=========================================================================*/
void a2_platform_sync_mcdma(void);

/*===========================================================================

  FUNCTION:  a2_init

===========================================================================*/
/*!
    @brief
    Initializes the A2 driver

    This function does not start up the a2 task however.

*/
/*=========================================================================*/
void a2_init( void );

/*==============================================================================

  FUNCTION:  a2_deinit

==============================================================================*/
/*!
    @brief
    De-initializes the A2 driver

*/
/*============================================================================*/
void a2_deinit(void);

/*===========================================================================

  FUNCTION:  a2_platform_deregister_interrupts

===========================================================================*/
/*!
    @brief
    Deregisters all interrupts for a specific platform.

    @return
    None
*/
/*=========================================================================*/
void a2_platform_deregister_interrupts
(void);

#endif /* A2_PLATFORM_H */

