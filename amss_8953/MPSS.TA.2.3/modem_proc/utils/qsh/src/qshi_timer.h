/*!
  @file
  qshi_timer.h

  @brief
  QSH internal header file qsh timer related functionality.
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/qsh/src/qshi_timer.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/15/15   vd      CR835881: Increase the size of the metric timestamp to uint32
                   and change the granularity to 1 msec
02/13/15   vd      Initial version               
==============================================================================*/

#ifndef QSHI_TIMER_H
#define QSHI_TIMER_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <queue.h>
#include <queue_services.h>
#include <timer.h>
#include <qshi_util.h>
#include <qsh_client.h>
#include <qsh_types.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! re-enable to debug timer */
//#define QSH_TIMER_DEBUG_MSG

#define QSH_TIMER_METRIC_Q_SIZE             20
#define QSH_TIMER_METRIC_INFO_SIZE          200
#define QSH_TIMER_EXPIRY_MSEC               50
/*! this is to address the scenario where the timer expires just less  
  than QSH_TIMER_EXPIRY_MSEC */
#define QSH_TIMER_EXPIRY_TOLERANCE_MSEC     1
#define QSH_TIMER_USEC_TO_MSEC              1000

/*! @brief for maintaing metric timer info in a queue. 
*/
typedef struct
{
  /* @todo: ensure its a lockless queue */
  q_type         timer_q;
  /*! indicate if the queue is active and in use */
  boolean        active;
  /*! timeout value for this queue in msec */
  uint16         timeout_ms;
  /*! elapsed time in msec for this queue */
  uint16         elapsed_time_msec;
  /*! last time this queue was activate */
  qsh_timetick_t last_activated;
} qsh_timer_metric_q_s;

/*! @brief declare metric timer info struct
*/
typedef struct qsh_timer_metric_info_s qsh_timer_metric_info_s;

/*!
  @brief
  Type to represent internal metric configuration stats.
*/
typedef struct
{
  uint16  start_count;
  uint16  stop_count;
} qshi_metric_cfg_stats_s;

/*!
  @brief
  Type to represent internal top-level metric configuration.
*/
typedef struct
{
  /*! metric id */
  qsh_metric_id_t             id;

  /*! action */
  qsh_metric_action_e         action;
  
  /*! subscriber id */
  sys_modem_as_id_e_type      subs_id;
  
  /*! flag to indicate when FIFO wraparound happens: if flag is FALSE, the
      first element of FIFO should be ignored */
  boolean                     wrap_around_flag;
  
  /*! client */
  uint8                       client;
  
  /*! period in ms */
  uint16                      sampling_period_ms;
  
  /*! FIFO for elements */
  qsh_metric_fifo_s           fifo;

  /*! pointer to configuration specific to this metric */
  void *                      cfg_data_ptr;

  /*! stats */
  qshi_metric_cfg_stats_s     stats; 
  
  /*! cookie/timer info ptr */
  qsh_timer_metric_info_s *   timer_info_ptr; 
} qshi_timer_metric_cfg_s;

/*! @brief create a timer link union. Easy to traverse
*/
typedef union
{
  q_link_type               link;
  qsh_timer_metric_info_s*  timer_info;
} qsh_timer_link_u;

/*! @brief for maintaing metric timer info
*/
struct qsh_timer_metric_info_s
{
  qsh_timer_link_u       timer_link;
  /*! maintain the pointer to the metric cfg */
  qshi_timer_metric_cfg_s*     metric_cfg;
  /*! pointer to the metric queue where the element belongs to */
  qsh_timer_metric_q_s*  metric_q_ptr;
};

/*! @brief 
    Top-level data structure for timer stats.
*/
typedef struct
{
  /*! max sampling period in msec */
  uint16  max_sampling_period_ms;
  /*! min sampling period in msec */
  uint16  min_sampling_period_ms;
  /*! total num of metric added */
  uint16  metric_add_count;
  /*! total num of metric removed */
  uint16  metric_remove_count;
} qsh_timer_stats_s;

/*! @brief 
  Top-level data structure.
*/
typedef struct
{
   /*! to indicate if the timer is active */
   boolean                 active;
   /*! current sampling period */
   uint16                  sampling_period_msec;
   /*! current timestamp in msec. It is mostly used for 
     timestamping timer expiry based metric collection */
   uint32                  timestamp_msec;
   /*! last time the timer expired */
   qsh_timetick_t          last_activated;
   /*! timer datatype for QSH timers used primarily 
     for non-deferrable timers */
   timer_group_type        timer_group;
   timer_type              timer;
   qsh_timer_metric_q_s    timer_metric_q[QSH_TIMER_METRIC_Q_SIZE];
   /*! No. of queues used so far */
   uint16                  timer_metric_q_count;
   /*! free queue for the timer */
   qsh_timer_metric_q_s    timer_metric_free_q;
   /*! array of metric timer info */
   qsh_timer_metric_info_s timer_metric_info[QSH_TIMER_METRIC_INFO_SIZE];
   qsh_timer_stats_s       timer_stats;
} qsh_timer_s;

/*==============================================================================

                         EXTERNAL VARIABLES

==============================================================================*/

extern qsh_timer_s qsh_timer;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================

  FUNCTION:  qsh_timer_expiry_process

==============================================================================*/
/*!
  @brief
  Process timer expiry.

  @return
  None
 
*/
/*============================================================================*/
void qsh_timer_expiry_process(void);

/*==============================================================================

  FUNCTION:  qsh_timer_init

==============================================================================*/
/*!
  @brief
  Init QSH timer.

  @return
  None
*/
/*============================================================================*/
void qsh_timer_init(void);

/*==============================================================================

  FUNCTION:  qsh_timer_add

==============================================================================*/
/*!
  @brief
  Adds a new metric cfg to the appropriate timer queue .

  @return
  None
*/
/*============================================================================*/
void qsh_timer_add(qshi_timer_metric_cfg_s * metric_cfg);

/*==============================================================================

  FUNCTION:  qsh_timer_remove

==============================================================================*/
/*!
  @brief
  Removes a new metric cfg from the appropriate timer queue .

  @return
  None
*/
/*============================================================================*/
void qsh_timer_remove(qshi_timer_metric_cfg_s * metric_cfg);

/*==============================================================================

  FUNCTION:  qsh_timer_find_duplicate_metric_cfg

==============================================================================*/
/*!
  @brief
  Ensure same metric cfg ptr is not used twice

  @return
  None
 
*/
/*============================================================================*/
void qsh_timer_find_duplicate_metric_cfg(qshi_timer_metric_cfg_s *metric_cfg);

/*==============================================================================

  FUNCTION:  qsh_timer_get_timestamp

==============================================================================*/
/*!
  @brief
  Gets the last updated timestamp (msec) during timer expiry. 

  @return
  Timestamp in msec
 
*/
/*============================================================================*/
uint32 qsh_timer_get_timestamp(void);

#endif /* QSHI_TIMER_H */
