/*!
  @file
  binary_heap.h

  @brief
  header file for binary heap functionality.
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================
                      GENERAL DESCRIPTION

  A binary heap is a simple data structure used for logically storing and re-
  trieving data blocks, based on a given priority order without
  physically copying the actual client data elements.
  
  The binary heap is implemented using an array. 
  MIN heap: The first element in the array is always minimum.
  MAX Heap: The first element in the array is always maximum.
  In MIN binary heap, the parent node is always smaller than its two child nodes.
  
  left child is calculated using (2 * parent_idx + 1) and 
  right child is calculated using (2* parent_idx + 2).
  parent child is calculated using (child_idx/2).
  A binary heap does not mention any relationship between left and right 
  children of a given parent.
  
                Visual representation of binary MIN heap array.          
   +----------+   +---------+   +-----------+   +-----------+   +-----------+
   |Min ele(0)|-->|Parent(1)|-->| ran ele(2)|-->| child_1(3)|-->| child_2(4)|
   +----------+   +---------+   +-----------+   +-----------+   +-----------+

  Inserting in a Min heap:
        10(Min)                         3(Min)
       /  \    ---> (add 3) =====>     / \     
      12   16                         10  16
                                     /
                                    12
  Extracting Min from a MIn heap:
        3(Min)                              12                10(Min)
       / \                                  / \               / \
      10  16  ----> (1.extract min  --->   10  16 ---->     12  16
     /               2.Insert last
    12                 element at root.
                     3.heapify on root.)

                     

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/qsh/src/binary_heap.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
10/11/15   ca      CR:920884, QSH rule man phase 3.
07/20/15   ca      Initial check in.               
==============================================================================*/

#ifndef BINARY_HEAP_H
#define BINARY_HEAP_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <comdef.h>
#include "err.h"
#include <assert.h>
#ifdef FEATURE_POSIX
#include <pthread.h>
#endif
#include <rex.h>
#include <qurt.h>
#include <msg.h>

/*!
FOLLOWING CODE ILLUSTRATES THE USAGE OF BINARY HEAP LIBRARY.

Assumption: heapifying element is of size is uint32.
Note: If the heapifying element is of size uint16 then corresponding APIs shud 
be used.

// client struct type 
typedef struct
{ 
  // The first element should always be of type binary_heap_element_32
  // The first element should always be the element on which heapifying is done
  binary_heap_element_32 prio;
  // Random data
  uint32 a;
  // Random data
  char b;
  // Random data
  char c;
  // Random data
  uint32 d;
}client_struct_s;

client_binary_heap_test_fn()
{
  // Pointer to heap handle. This is retuned by binary_heap_init()
  // And this needs to be passed as an argument to 
  // Binary_heap_insert(), binary_heap_delete(), binary_heap_deinit()APIs 
  void* heap_handle = NULL;;
  
  // An object to client struct for which binary heap is created.
  // Note: Binary heap is created only for the pointers of the  client objects.
  client_struct_s client_custom_obj;
  client_struct_s client_custom_obj2;
  
  // A pointer pointing to extracted element from binary min heap 
  client_struct_s* min_extracted_element;
  
  // Init client obj.
  client_custom_obj.prio = 30;
  client_custom_obj.a = 100;
  client_custom_obj.b = 'c';
  client_custom_obj.c = 'V';
  client_custom_obj.d = 999;
  
  // Init another client obj	
  client_custom_obj2.prio = 50;
  client_custom_obj2.a = 200;
  client_custom_obj2.b = 'h';
  client_custom_obj2.c = 'i';
  client_custom_obj2.d = 111;
  
  // An object to the confg params of binary heap. 
  // Clients will make sure that this object is present till 
  // Binary_heap_init() fn is returned. binary heap lib is NOT making a copy of 
  // The heap_cfg 
  binary_heap_cfg_s heap_cfg;
  
  // At this point binary heap lib will update the client's heap_cfg with some 
  // Default params.	
  binary_heap_cfg_init(&heap_cfg);
  
  heap_cfg.cap = 20;
  heap_cfg.type = BINARY_HEAP_TYPE_MIN;
  // At this point binary heaplib will allocate the memory for binary heap array 
  // Of pointers. Clients should not modify memory pointing to "heap_handle".
  heap_handle =  = binary_heap_init(&heap_cfg);
  
  // Inserting into binary heap
  binary_heap_insert_32(heap_handle,&client_custom_obj);
  
  // Inserting into binary heap
  binary_heap_insert_32(heap_handle,&client_custom_obj2);
  
  // Extracting min from binary heap
  min_extracted_element = (client_struct_s*)binary_heap_extract_32(heap_handle);
  
  // Delete the node from binary heap 
  binary_heap_delete_32(heap_handle,&client_custom_obj2);
  
  // Deallocate the memory for binary heap 
  // After this function is called do not use "heap_handle"
  binary_heap_deinit(heap_handle);	

} */

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*==============================================================================
	
                    EXTERNAL FUNCTION PROTOTYPES
	
==============================================================================*/
/*==============================================================================
		
                    INTERNAL DEFINITIONS AND TYPES
		
==============================================================================*/
#ifdef FEATURE_POSIX
  typedef pthread_mutex_t    binary_heap_crit_sect_s;
#else
  typedef rex_crit_sect_type binary_heap_crit_sect_s;
#endif

/*! 1. This heap element on which heapifying is done should always be the 
       first element in the client structure 
    2. Clients should use the binary_heap_element_64 type as heapifying element.
    Ex: #include "binary_heap.h"
    typedef struct
    {
      binary_heap_element_64  heapifying_element;
      ...
      ...
      ...
      ...
    }client_struct_s
    */    
typedef uint64 binary_heap_element_64;

/*! 1. This heap element on which heapifying is done should always be the 
       first element in the client structure 
    2. Clients should use the binary_heap_element_32 type as heapifying element.
    Ex: #include "binary_heap.h"
    typedef struct
    {
      binary_heap_element_32  heapifying_element;
      ...
      ...
      ...
      ...
    }client_struct_s
    */    
typedef uint32 binary_heap_element_32;

/*! 1. This heap element on which heapifying is done should always be the 
       first element in the client structure 
    2. As of now only uint32 is supported for heapifying element.
    3. Clients should use the binary_heap_element_16 type as heapifying element.
    Ex: #include "binary_heap.h"
    typedef struct
    {
      binary_heap_element_16  heapifying_element;
      ...
      ...
      ...
      ...
    }client_struct_s
    */
typedef uint16 binary_heap_element_16;


/*! 
  An enum type to describe lock or lockless heap */
typedef enum
{
  /* Heap is protected by lock */
  BINARY_HEAP_LOCK_TYPE_LOCK = 0x0,
  /* binary heap is not protected with the lock*/
  BINARY_HEAP_LOCK_TYPE_LOCK_LESS,
}binary_heap_lock_type_e;

/*! An enum type to describe heap type */
typedef enum 
{
 /*! binary min heap */
  BINARY_HEAP_TYPE_MIN = 0x0,
  /*! binary max heap */
  BINARY_HEAP_TYPE_MAX,
  /*! invalid binary heap */
  BINARY_HEAP_TYPE_NONE,  
}binary_heap_type_e;

/*! typedef for all the err codes */
typedef enum
{
  BINARY_HEAP_RESULT_SUCESS,
  BINARY_HEAP_RESULT_EXCEED_MAX_CAP,
} binary_heap_result_e;

/*! Astructure to hold the config info of the binary heap. This info is 
    provided by the clients */
typedef struct
{
  /*! Client recommended max elements in the binary heap.
      Initially a heap array is allocated wiht cap size. it is best if this 
      recommended size is as close as to the run time binary heap size. 
      if actual size (run time size )is greater than this cap, then a new heap 
      array which is double the size of cap is allocated and all the elements 
      are copied from original heap array to new heap array. This might not be 
      time effiencent.*/
  uint32                     cap;

  /*! Maximum capacity of the heap beyond which the heap needs to drop elements */
  uint32                     max_cap;

  /*! MIN or MAX heap */
  binary_heap_type_e         type;
}binary_heap_cfg_s;

/*==============================================================================

FUNCTION:  binary_heap_cfg_init

==============================================================================*/
/*!
  @brief
  This function iinitializes the client's binary_heap_cfg_s struct with default 
  params. After returning from this function, client is expected to fill 
  binary_heap_cfg_s with actual config params and then call binary_heap_init() 
  fn.
*/
/*============================================================================*/
void binary_heap_cfg_init(binary_heap_cfg_s* heap_cfg);

/*==============================================================================

FUNCTION:  binary_heap_init

==============================================================================*/
/*!
  @brief
  1. This funtion validates the parameters of binary_heap_cfg_s.
  2. This function allocates mem for heap array and returns a void* ptr 
  (32 bit address). 
  3. Clients are expected to store this returned address and pass this 
  as an arg while calling 
  binary_heap_deinit(void* heap_handle)
  binary_heap_insert_32(void* heap_handle, void* client_element)
  binary_heap_extract_32(void* heap_handle);
*/
/*============================================================================*/
void* binary_heap_init(binary_heap_cfg_s* heap_cfg);

/*==============================================================================

FUNCTION:  binary_heap_insert_64

==============================================================================*/
/*!
@brief
  1. This function will insert the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint64.
  3. T(n) = O(logn) for each element.
*/
/*============================================================================*/
binary_heap_result_e binary_heap_insert_64(void* heap_handle, void* client_element);

/*==============================================================================

FUNCTION: binary_heap_extract_64

==============================================================================*/
/*!
  @brief
  1. This function returns the top (min) of the binary heap and heapifies the 
  whole binary heap to re assign the heap array values 
  2. The heapifying element is of size uint64.
  3. T(n) = O(logn).
*/
/*============================================================================*/
void* binary_heap_extract_64(void* heap_handle);

/*==============================================================================

FUNCTION: binary_heap_delete_64

==============================================================================*/
/*!
  @brief
  1. This function will delete the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint64.
  3. this function takes care of the holes in binary heap array after deleting 
  ptr to cliend node from binary heap 
  4. T(n) = O(n) for each element.
/*============================================================================*/
void binary_heap_delete_64(void* heap_handle, void* client_element);


/*==============================================================================

FUNCTION:  binary_heap_insert_32

==============================================================================*/
/*!
@brief
  1. This function will insert the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint32.
  3. T(n) = O(logn) for each element.
*/
/*============================================================================*/
binary_heap_result_e binary_heap_insert_32(void* heap_handle, void* client_element);

/*==============================================================================

FUNCTION: binary_heap_extract_32

==============================================================================*/
/*!
  @brief
  1. This function returns the top (min) of the binary heap and heapifies the 
  whole binary heap to re assign the heap array values 
  2. The heapifying element is of size uint32.
  3. T(n) = O(logn).
*/
/*============================================================================*/
void* binary_heap_extract_32(void* heap_handle);

/*==============================================================================

FUNCTION: binary_heap_delete_32

==============================================================================*/
/*!
  @brief
  1. This function will delete the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint32.
  3. this function takes care of the holes in binary heap array after deleting 
  ptr to cliend node from binary heap 
  4. T(n) = O(n) for each element.
/*============================================================================*/
void binary_heap_delete_32(void* heap_handle, void* client_element);

/*==============================================================================

FUNCTION:  binary_heap_deinit

==============================================================================*/
/*!
@brief
  1. This function will frees the memory for the heap array
  2.After this function is called, do not use heap_handle.
*/
/*============================================================================*/
void binary_heap_deinit(void* heap_handle);

/*==============================================================================

FUNCTION:  binary_heap_peek

==============================================================================*/
/*!
@brief
  Returns the max/min element in the heap without removing it
*/
/*============================================================================*/
void* binary_heap_peek(void* heap_handle);

/*==============================================================================

FUNCTION:  binary_heap_insert_16

==============================================================================*/
/*!
@brief
  1. This function will insert the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint16.
  3. T(n) = O(logn) for each element.
*/
/*============================================================================*/
binary_heap_result_e binary_heap_insert_16(void* heap_handle, void* client_element);

/*==============================================================================

FUNCTION: binary_heap_extract_16

==============================================================================*/
/*!
  @brief
  1. This function returns the top (min) of the binary heap and heapifies the 
  whole binary heap to re assign the heap array values 
  2. The heapifying element is of size uint16.
  3. T(n) = O(logn).
*/
/*============================================================================*/
void* binary_heap_extract_16(void* heap_handle);

/*==============================================================================

FUNCTION: binary_heap_delete_16

==============================================================================*/
/*!
  @brief
  1. This function will delete the pointer to client_element at approprate index
  of the binary heap array.
  2. The heapifying element is of size uint16.
  3. this function takes care of the holes in binary heap array after deleting 
  ptr to cliend node from binary heap 
  4. T(n) = O(n) for each element.
/*============================================================================*/
void binary_heap_delete_16(void* heap_handle, void* client_element);

/*==============================================================================

  FUNCTIONS:  binary_heap_crit_sect_init

==============================================================================*/
/*!
  @brief
  Functions to init a mutex.

  @return
  None
*/
/*============================================================================*/

static inline void binary_heap_crit_sect_init( binary_heap_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX
   pthread_mutexattr_t attr;
   ASSERT( lock_ptr != NULL );

   if( pthread_mutexattr_init(&attr) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_init",0,0,0);
   }
   if( pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_settype",0,0,0);
   }
   if( pthread_mutex_init(lock_ptr, &attr) != 0 )
   {
     ERR_FATAL("failed on thread_mutex_init",0,0,0);
   }
   if(pthread_mutexattr_destroy(&attr)!=0)
   {
     MSG_HIGH(
      "WARNING: Potential memory leak. Could not destroy pthread_mutex_attr",
      0,0,0);
   }

#else
  rex_init_crit_sect( (rex_crit_sect_type*) lock_ptr );
#endif
}
/*==============================================================================

  FUNCTIONS:  binary_heap_crit_sect_deinit

==============================================================================*/
/*!
  @brief
  Functions to deinit a mutex.

  @return
  None
*/
/*============================================================================*/

static inline void binary_heap_crit_sect_deinit( binary_heap_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX

  ASSERT( lock_ptr != NULL );

  if( pthread_mutex_destroy(lock_ptr) !=0 )
  {
    ERR_FATAL("Couldn't destroy pthread mutex",0,0,0);
  }

#endif /* FEATURE_POSIX */
}

/*==============================================================================

  FUNCTIONS:  binary_heap_crit_sect_enter
              

==============================================================================*/
/*!
  @brief
  Functions to enter a critical section.

  @return
  None
*/
/*============================================================================*/
static inline void binary_heap_crit_sect_enter
(
  binary_heap_crit_sect_s *   crit_sect_ptr
)
{
  ASSERT(crit_sect_ptr != NULL);
  
#ifdef FEATURE_POSIX
  ASSERT(pthread_mutex_lock(crit_sect_ptr) == 0);
#else
  rex_enter_crit_sect((rex_crit_sect_type *) crit_sect_ptr);
#endif

} /* qsh_crit_sect_enter() */

/*==============================================================================

  FUNCTIONS:  binary_heap_crit_sect_leave
              

==============================================================================*/
/*!
  @brief
  Functions to leave a critical section.

  @return
  None
*/
/*============================================================================*/


static inline void binary_heap_crit_sect_leave
(
  binary_heap_crit_sect_s *   crit_sect_ptr
)
{
  ASSERT(crit_sect_ptr != NULL);
  
#ifdef FEATURE_POSIX
  ASSERT(pthread_mutex_unlock(crit_sect_ptr) == 0);
#else
  rex_leave_crit_sect((rex_crit_sect_type *) crit_sect_ptr);
#endif

} /* qsh_crit_sect_leave() */

/*==============================================================================

FUNCTION: binary_heap_get_heap_start_addr

==============================================================================*/
/*!
  @brief:
  This function returns the start address of binary heap.
  IN: heap_handle
*/
/*============================================================================*/
uint32** binary_heap_get_heap_start_addr
(
  void*    heap_handle
);

/*==============================================================================

FUNCTION: binary_heap_get_heap_curr_idx

==============================================================================*/
/*!
  @brief:
  This function returns the current size / index of binary heap.
  IN: heap_handle
*/
/*============================================================================*/
uint32 binary_heap_get_heap_curr_idx
(
  void*    heap_handle
);

#endif /* BINARY_HEAP_H */
