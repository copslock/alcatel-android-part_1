#ifndef RFCOMMON_CORE_SAR_H
#define RFCOMMON_CORE_SAR_H

/*!
   @file
   rfcommon_core_sar.h

   @brief
      This file contains prototype declarations and definitions necessary to
      be used within RF core module.

   @details

*/

/*===========================================================================
Copyright (c) 2011 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/08 09:54:43 $ 
$Author: mplcsds1 $
$Header: //components/rel/rfa.mpss/4.3/rf/common/rf/core/inc/rfcommon_core_sar.h#4 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/15/16   dyc     SAR forced antenna support
08/12/11    ap      Fix warnings
08/11/11    ap     Modify Get SAR State API to remove parameter 
03/20/11   lcl     Initial version
============================================================================*/ 
#include "rfm_common_sar.h"
#include "rfm_mode_types.h"

/* SAR value indicator for ASDiv to use other antenna */
#define RF_SAR_FORCE_OTHER_ANTENNA 0xFFFE

/*===========================================================================

                           FUNCTION PROTOTYPES

===========================================================================*/

/*--------------------------------------------------------------------------*/
extern void rfcommon_core_sar_set_state(rfm_common_sar_state_type sar_state);

/*--------------------------------------------------------------------------*/
extern rfm_common_sar_state_type rfcommon_core_sar_get_state(void);

/*--------------------------------------------------------------------------*/
extern void rfcommon_core_sar_set_overrride(rfm_common_sar_override_type override);

#endif /* RFCOMMON_CORE_SAR_H */
