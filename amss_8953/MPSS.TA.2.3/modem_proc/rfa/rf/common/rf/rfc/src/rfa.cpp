/*!
   @file
   rfa.cpp

   @brief
   This file contains implementation the RFA class.
*/

/*===========================================================================

Copyright (c) 2011-12 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2015/11/23 13:11:23 $ $Author: pwbldsvc $
$Header: //components/rel/rfa.mpss/4.3/rf/common/rf/rfc/src/rfa.cpp#1 $

when       who     what, where, why
------------------------------------------------------------------------------- 
05/09/12   sr      Initial version.

============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "rfa.h"
#include "rfcommon_msg.h"
#include "modem_mem.h"

// Constructor 
rfa::rfa() 
{

}

// Destructor
rfa::~rfa()
{

}

void* rfa::operator new(size_t size)
{
  return modem_mem_alloc(size,MODEM_MEM_CLIENT_RFA);
}

void rfa::operator delete(void* ptr)
{
  modem_mem_free(ptr,MODEM_MEM_CLIENT_RFA);
}
