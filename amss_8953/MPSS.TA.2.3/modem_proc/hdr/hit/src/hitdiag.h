#ifndef _HITDIAG_H
#define _HITDIAG_H

/*===*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                  HIT   DIAG  HEADER    FILE

GENERAL DESCRIPTION
 This file contains types and declarations associated with the HIT diag 
 processing. 

EXTERNALIZED FUNCTIONS
    hitdiag_init
    
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
01/15/2013   smd     Featurized hit cmd and hit diag.
05/07/2007   vh      Changed dynamic memory allocation to static one
03/28/2007   vh      Created

==========================================================================*/

#endif /* _HITDIAG_H */
