#ifndef _HITCMD_H
#define _HITCMD_H

/*===*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                  HIT COMMAND SERVICE UTILITY HEADER FILE

GENERAL DESCRIPTION
 This file contains types and declarations associated with the HIT command 
 processing. 

EXTERNALIZED FUNCTIONS
    hitcmd_alloc
    hitcmd_param_ptr
    hitcmd_send_rsp_pkt
    hitcmd_send_status
    hitcmd_refresh
    
REGIONAL FUNCTIONS
    None
    
    Copyright (c) 2006, 2007 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

============================================================================

                        EDIT HISTORY FOR MODULE

$Header: //components/rel/hdr.mpss/3.2.6/hit/src/hitcmd.h#1 $ $DateTime: 2015/09/30 15:19:37 $ $Author: pwbldsvc $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     -------------------------------------------------------
01/15/2013   smd     Featurized hit cmd and hit diag.
05/07/2007   vh      Changed dynamic memory allocation to static one
03/28/2007   vh      Created

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/
#include "comdef.h"
#endif /* _HITCMD_H */
