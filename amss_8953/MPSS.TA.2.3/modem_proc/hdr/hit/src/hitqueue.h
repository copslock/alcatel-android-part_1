#ifndef _HITQUEUE_H
#define _HITQUEUE_H

/*===*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

      H I T   Q U E U E    S E R V I C E S    H E A D E R    F I L E

GENERAL DESCRIPTION
 This file contains types and declarations associated with the HIT Queue
 Services.

EXTERNALIZED FUNCTIONS
    hitqueue_init
    hitqueue_put
    hitqueue_get
    
REGIONAL FUNCTIONS
    None
    
  Copyright (c) 2006, 2007 by Qualcomm Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/hdr.mpss/3.2.6/hit/src/hitqueue.h#1 $ $DateTime: 2015/09/30 15:19:37 $ $Author: pwbldsvc $

when         who     what, where, why
----------   ---     --------------------------------------------------------
01/15/2013   smd     Featurized hit cmd and hit diag.
05/07/2007   rmg     Changed dynamic memory allocation to static one
03/28/2007   vh      Created

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/
#include "rex.h"
#endif /* _HITQUEUE_H */
