#ifndef LPA_HTTP_H
#define LPA_HTTP_H
/*===========================================================================


            L P A   H T T P   H E A D E R


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2016 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/uim.mpss/5.1/lpa/src/lpa_http.h#3 $ $DateTime: 2016/05/19 16:24:36 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/19/16   av      Route HTTP request to QMI HTTP
05/15/16   ll      HTTP Chunking
05/15/16   av      LPA should wait for DS session open cb before sending post
03/28/16   av      Initial revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "queue.h"
#include "time_types.h"
#include "ds_http_types.h"

#include "lpalib.h"
#include "lpa.h"
#include "lpa_lpd.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

#define LPA_HTTP_HEADER_NAME_MAX_LEN     32
#define LPA_HTTP_HEADER_VALUE_MAX_LEN    256
#define LPA_SESSION_ID_NULL              0xFFFFFFFF
#define LPA_MAX_PROFILE_SIZE             0x100000  /*1 MB*/

/* Custom headers that can be specified by the client */
typedef struct
{
  char name [LPA_HTTP_HEADER_NAME_MAX_LEN+1];
  char value[LPA_HTTP_HEADER_VALUE_MAX_LEN+1];
} lpa_http_response_header_type;


typedef enum {
  LPA_HTTP_RSP,
  LPA_HTTP_QMI_HTTP_RSP
} lpa_http_cmd_enum_type;

typedef struct {
   uint32                         session_id;
   uint32                         request_id;
   sint15                         error;
   uint16                         http_status;
   uint32                         content_size;
   uint8                        * content_ptr;
   uint32                         num_headers;
   lpa_http_response_header_type* header_info_ptr;
} lpa_http_response_data_type;

typedef struct {
   qmi_uim_http_service_registry_id_type service_id;
   qmi_uim_http_transaction_result_type  http_status;
   uint16                                content_size;
   uint8                               * content_ptr;
   uint16                                num_headers;
   lpa_http_response_header_type       * header_info_ptr;
} lpa_http_qmi_http_response_data_type;

/* ----------------------------------------------------------------------------
   STRUCTURE:      LPA_HTTP_CMD_TYPE

   DESCRIPTION:
     Contains information for handling LPA HTTP commands
-------------------------------------------------------------------------------*/
typedef struct {
  q_link_type                            link;
  lpa_http_cmd_enum_type                 cmd_type;
  union {
    lpa_http_response_data_type          rsp_data;
    lpa_http_qmi_http_response_data_type qmi_http_rsp_data;
  } data;
} lpa_http_rsp_type;

/*=============================================================================

                       FUNCTION PROTOTYPES

=============================================================================*/

/*===========================================================================
FUNCTION LPA_HTTP_HANDLE_CMD_SIG

DESCRIPTION
  This function, called in lpa_main, is called to process HTTP event

DEPENDENCIES
  LPA task must be finished initialization.

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_http_handle_cmd_sig (
  void
);

/*===========================================================================
   FUNCTION:      LPA_HTTP_INIT

   DESCRIPTION:
     LPA HTTP init function called during task init

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     None

   SIDE EFFECTS:
     None
===========================================================================*/
void lpa_http_init (
  void
);

/*===========================================================================
   FUNCTION:      LPA_HTTP_CLEANUP

   DESCRIPTION:
     LPA HTTP cleanup function called to clean up refrerence data

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     None

   SIDE EFFECTS:
     None
===========================================================================*/
lpa_result_enum_type lpa_http_cleanup(
  lpa_slot_id_enum_type             slot_id
);

/*===========================================================================
FUNCTION LPA_HTTP_BUILD_AND_SEND_INITIATE_AUTH

DESCRIPTION
  This function is used to send Initiate Auth request to SMDP

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  lpa_result_enum_type

SIDE EFFECTS
  None
===========================================================================*/
lpa_result_enum_type lpa_http_build_and_send_initiate_auth (
  lpa_lpd_initiate_auth_req_type                *initiate_auth_req_ptr
);

/*===========================================================================
FUNCTION LPA_HTTP_BUILP_AND_SEND_GET_BPP

DESCRIPTION
  This function is used to send Get Bound Profile Package request to SMDP

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  lpa_result_enum_type

SIDE EFFECTS
  None
===========================================================================*/
lpa_result_enum_type lpa_http_build_and_send_get_bpp (
  lpa_lpd_get_bpp_req_type                *get_bpp_req_ptr
);

/*===========================================================================
FUNCTION LPA_HTTP_BUILP_AND_SEND_HANDLE_INSTALLATION_RESULT

DESCRIPTION
  This function is used to send the Handle Installation Result to SMDP

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  lpa_result_enum_type

SIDE EFFECTS
  None
===========================================================================*/
lpa_result_enum_type lpa_http_build_and_send_handle_installation_result (
  lpa_lpd_handle_installation_result_req_type         *profile_installation_result_req_ptr
);
#endif /* LPA_HTTP_H */

