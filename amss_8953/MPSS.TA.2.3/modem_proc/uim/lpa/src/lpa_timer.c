/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


            L P A   T I M E R   F U N C T I O N S


GENERAL DESCRIPTION

  This source file contains the functions for setting/clearing timers.

                        COPYRIGHT INFORMATION

Copyright (c) 2016 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/uim.mpss/5.1/lpa/src/lpa_timer.c#2 $ $DateTime: 2016/03/30 22:57:22 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/30/16    av     Fixed Klocwork errors 
03/28/16    av     Initial revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#include "rex.h"

#include "lpa.h"
#include "lpa_platform.h"
#include "lpa_timer.h"
#include "lpa_cmd.h"

/*=============================================================================

                       DATA DECLARATIONS

=============================================================================*/

#define LPA_SANITY_TIMER_VALUE             300000 /* 5 minutes */

static rex_timer_type                      lpa_sanity_timer[LPA_NUM_SLOTS];

/*=============================================================================

                       FUNCTION PROTOTYPES

=============================================================================*/

/*===========================================================================
FUNCTION LPA_TIMER_CLEANUP

DESCRIPTION
  Undefines the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_cleanup (
  lpa_slot_id_enum_type          slot_id
)
{
  if(slot_id < LPA_NUM_SLOTS)
  {
    (void)rex_undef_timer(&lpa_sanity_timer[slot_id]);
  }
} /* lpa_timer_cleanup */


/*===========================================================================
FUNCTION LPA_TIMER_CLEAR

DESCRIPTION
  Function clears the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_clear (
  lpa_slot_id_enum_type          slot_id
)
{
  if(slot_id < LPA_NUM_SLOTS)
  {
    (void)rex_clr_timer(&lpa_sanity_timer[slot_id]);
  }
} /* lpa_timer_clear */


/*===========================================================================
FUNCTION LPA_TIMER_SET

DESCRIPTION
  Function sets the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  lpa_result_enum_type

SIDE EFFECTS
  None
===========================================================================*/
lpa_result_enum_type lpa_timer_set (
  lpa_slot_id_enum_type          slot_id
)
{
  uint32                 timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_1;

  switch(slot_id)
  {
    case LPA_SLOT_1:
      timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_1;
      break;
    case LPA_SLOT_2:
      timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_2;
      break;
    default:
      return LPA_GENERIC_ERROR;
  }

  (void)rex_clr_sigs(rex_self(), timer_sig);
  (void)rex_set_timer(&lpa_sanity_timer[slot_id], LPA_SANITY_TIMER_VALUE);

  return LPA_SUCCESS;
} /* lpa_timer_set */


/*===========================================================================
FUNCTION LPA_TIMER_HANDLE_SANITY_TIMER_EXPIRE_SIG

DESCRIPTION
  This function, called in lpa_task, is called to process sanity timer
  expiration signal for a given slot.

DEPENDENCIES
  lpa_task must have finished initialization.

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_handle_sanity_timer_expire_sig (
  lpa_slot_id_enum_type               slot_id
)
{
  uint32                 timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_1;

  /* Send the response and clear the timer */
  LPA_MSG_HIGH_0("LPA_SANITY_TIMER_EXPIRE_SIG");

  if(slot_id == LPA_SLOT_1)
  {
    timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_1;
  }
  else if(slot_id == LPA_SLOT_2)
  {
    timer_sig = LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_2;
  }
  else
  {
    return;
  }

   /* Clear the signal */
  (void)rex_clr_sigs(rex_self(), timer_sig);

  lpa_send_response(LPA_ERROR_COMMAND_TIMEOUT,
                    NULL,
                    slot_id,
                    TRUE);
} /* lpa_timer_handle_sanity_timer_expire_sig */


/*===========================================================================
FUNCTION LPA_TIMER_INIT

DESCRIPTION
  Function initializes and defines the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_init (
  void
)
{
  rex_def_timer (&lpa_sanity_timer[LPA_SLOT_1], UIM_LPA_TCB, LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_1);
  rex_def_timer (&lpa_sanity_timer[LPA_SLOT_2], UIM_LPA_TCB, LPA_SANITY_TIMER_EXPIRE_SIG_SLOT_2);
} /* lpa_timer_init */

