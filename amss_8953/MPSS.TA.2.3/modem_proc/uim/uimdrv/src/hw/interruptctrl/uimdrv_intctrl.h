#ifndef UIMDRV_INTCTRL_H
#define UIMDRV_INTCTRL_H
/*============================================================================
  FILE:        uimdrv_intctrl.h

  OVERVIEW:    Contains the implementation of the UARTDM class for uim drivers.

  DEPENDENCIES: N/A

                Copyright (c) 2012-2014 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //components/rel/uim.mpss/5.1/uimdrv/src/hw/interruptctrl/uimdrv_intctrl.h#1 $
$DateTime: 2016/03/21 16:46:25 $
$Author: pwbldsvc $

when        who        what, where, why
------      ----       -----------------------------------------------------------
07/20/15    ks         Consolidating UIM bring-up changes
09/18/14    akv        UIMDRV UT enhancements
11/27/13    akv        Initialization of hw enumeration based on dal chip family
10/08/13    nmb        HW header file cleanup
07/12/13    nmb        Global Reorganization
07/12/13    akv        HW Enumeration - modifying target specific hardware values 
                       and organizing them in value-key pairs
12/05/12    js         UIM Parallel Processing changes
10/26/12    akv/ssr    HW Enumeration changes
============================================================================*/
#include "comdef.h"
#include "uimdrv_main.h"

/* Initialize interrupt controller */
void initialize_intctrl(uim_instance_global_type *uim_ptr);

#endif /* UIMDRV_INTCTRL_H */
