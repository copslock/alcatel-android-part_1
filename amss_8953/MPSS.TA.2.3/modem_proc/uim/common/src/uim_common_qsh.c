/*============================================================================
  FILE:         uim_common_qsh.c

  OVERVIEW:     The file defines the UIM QSH event logging module

  DEPENDENCIES: N/A

                Copyright (c) 2016 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/uim.mpss/5.1/common/src/uim_common_qsh.c#2 $

when       who      what, where, why
--------   ---      -----------------------------------------------------------
08/04/16   bcho     Support added for QSH generic failure event
07/01/16   bcho     Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_variation.h"
#include "uim_msg.h"
#include "uim_common_qsh.h"
#include "mmgsdilib_p.h"

/*===========================================================================

            DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/

#ifdef FEATURE_QSH_EVENT_METRIC

/* Array to store QSH event status */
static boolean uim_common_qsh_event_enabled_array[USIM_QSH_EVENT_MAX] = {0};


/*=============================================================================

                          UIM COMMON FUNCTIONS
                     Definitions used throughout UIM

=============================================================================*/

/*==========================================================================
FUNCTION UIM_COMMON_QSH_IS_FAILURE_EVENT

DESCRIPTION
  To check whether QSH event is failure event or not

DEPENDENCIES
  None

RETURN VALUE
  Boolean

SIDE EFFECTS
  None
==========================================================================*/
static boolean uim_common_qsh_is_failure_event(
  usim_qsh_event_e qsh_event
)
{
  switch(qsh_event)
  {
    case USIM_QSH_EVENT_CARD_ERROR:
    case USIM_QSH_EVENT_RECOVERY_TRIGGERED:
      return TRUE;
    /* All other cases are handled as non-failures */
    default:
      break;
  }
  return FALSE;
}/* uim_common_qsh_is_failure_event */


/*==========================================================================
FUNCTION UIM_COMMON_QSH_CB

DESCRIPTION
  This is callback function registered with QSH module to set status of UIM QSH events

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
==========================================================================*/
static void uim_common_qsh_cb(
  qsh_client_cb_params_s* qsh_cb_params_ptr
)
{
  if(qsh_cb_params_ptr == NULL ||
     qsh_cb_params_ptr->action != QSH_ACTION_CFG)
  {
    return;
  }

  switch(qsh_cb_params_ptr->action_mode)
  {
    case QSH_ACTION_MODE_SYNC_REQUIRED:
    case QSH_ACTION_MODE_SYNC_OPTIONAL:
      uim_common_qsh_process_action_event_config_params(qsh_cb_params_ptr,
                                                        TRUE);
      break;

    case QSH_ACTION_MODE_ASYNC_REQUIRED:
      (void)mmgsdi_qsh_action_event_config(qsh_cb_params_ptr);
      break;

    default:
      break;
  }
}/* uim_common_qsh_cb */
#endif /*FEATURE_QSH_EVENT_METRIC */

/*==========================================================================
FUNCTION UIM_COMMON_QSH_PROCESS_ACTION_EVENT_CONFIG_PARAMS

DESCRIPTION
  This function processes QSH event config action

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
==========================================================================*/
void uim_common_qsh_process_action_event_config_params(
  qsh_client_cb_params_s* qsh_cb_params_ptr,
  boolean                 is_mode_sync
)
{
#ifdef FEATURE_QSH_EVENT_METRIC
  qsh_client_action_done_s cb_done;
  qsh_event_action_e       action     = QSH_EVENT_ACTION_DISABLE;
  usim_qsh_event_e         event_type = USIM_QSH_EVENT_MAX;

  if(qsh_cb_params_ptr == NULL)
  {
    return;
  }

  memset(&cb_done, 0x00, sizeof(cb_done));

  action = qsh_cb_params_ptr->action_params.event_cfg.action;
  event_type = (usim_qsh_event_e)(qsh_cb_params_ptr->action_params.event_cfg.id);

  if(event_type < USIM_QSH_EVENT_MAX)
  {
    uim_common_qsh_event_enabled_array[event_type] = (action == QSH_EVENT_ACTION_ENABLE) ? TRUE : FALSE;
  }

  /*  QSH should be intimated that our event configuration is updated */
  qsh_client_action_done_init(&cb_done);

  cb_done.cb_params_ptr = qsh_cb_params_ptr;

  cb_done.action_mode_done = is_mode_sync ? QSH_ACTION_MODE_DONE_SYNC : QSH_ACTION_MODE_DONE_ASYNC;
  qsh_client_action_done(&cb_done);
#else
  (void)qsh_cb_params_ptr;
  (void)is_mode_sync;
#endif /* FEATURE_QSH_EVENT_METRIC */
}/* uim_common_qsh_process_action_event_config_params */


/*==========================================================================
FUNCTION UIM_COMMON_QSH_IS_EVENT_ENABLED

DESCRIPTION
  This function provides status of UIM QSH events

DEPENDENCIES
  None

RETURN VALUE
  Boolean

SIDE EFFECTS
  None
==========================================================================*/
boolean uim_common_qsh_is_event_enabled(
  usim_qsh_event_e qsh_event
)
{
#ifdef FEATURE_QSH_EVENT_METRIC
  if(qsh_event < USIM_QSH_EVENT_MAX)
  {
    /* If DEBUG_ANY_FAILURE event is enabled, returns TRUE; if not, fallback to
       failure events' status */
    if(uim_common_qsh_is_failure_event(qsh_event) == FALSE)
    {
      return uim_common_qsh_event_enabled_array[qsh_event];
    }
    else
    {
      return uim_common_qsh_event_enabled_array[USIM_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE] ||
             uim_common_qsh_event_enabled_array[qsh_event];
    }
  }
#else
  (void) qsh_event;
#endif /* FEATURE_QSH_EVENT_METRIC */
  return FALSE;
}/* uim_common_qsh_is_event_enabled */


/*==========================================================================
FUNCTION UIM_COMMON_QSH_INIT

DESCRIPTION
  This function registers UIM with QSH module 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
==========================================================================*/
void uim_common_qsh_init(
  void
)
{
#ifdef FEATURE_QSH_EVENT_METRIC
  qsh_client_reg_s uim_client_reg;

  memset(&uim_client_reg, 0x00, sizeof(uim_client_reg));

  /* Initialize uim client */
  qsh_client_reg_init(&uim_client_reg);

  /*Populate UIM client info to QSH */
  uim_client_reg.client = QSH_CLT_USIM;
  uim_client_reg.client_cb_ptr = uim_common_qsh_cb;
  uim_client_reg.major_ver = USIM_QSH_MAJOR_VER;
  uim_client_reg.minor_ver = USIM_QSH_MINOR_VER;
  uim_client_reg.cb_action_support_mask = QSH_ACTION_CFG;

  qsh_client_reg(&uim_client_reg);
#endif /* FEATURE_QSH_EVENT_METRIC */
}/* uim_common_qsh_init */


/*==========================================================================
FUNCTION UIM_COMMON_QSH_EVENT_NOTIFY

DESCRIPTION
  This function notifies QSH module for UIM event.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
==========================================================================*/
void uim_common_qsh_event_notify(
  usim_qsh_event_e qsh_event
)
{
#ifdef FEATURE_QSH_EVENT_METRIC
  qsh_client_event_notify_params_s  event_notify_params;

  memset(&event_notify_params, 0x00, sizeof(event_notify_params));

  if(qsh_event >= USIM_QSH_EVENT_MAX)
  {
    return;
  }

  qsh_client_event_notify_init(&event_notify_params);

  /* For failure events, notify with DEBUG_ANY_FAILURE if it is enabled.
     Note that even if failure events are enabled we use DEBUG_ANY_FAILURE in
     above scenario */
  if((uim_common_qsh_is_failure_event(qsh_event)) &&
     uim_common_qsh_event_enabled_array[USIM_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE])
  {
    qsh_event = USIM_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE;
  }

  event_notify_params.client = QSH_CLT_USIM;
  event_notify_params.id = qsh_event;
  event_notify_params.event_data = NULL;

  QSH_LOG(QSH_CLT_USIM, QSH_CAT_EVENT, QSH_MSG_TYPE_HIGH, "UIM event 0x%x",
          event_notify_params.id);

  qsh_client_event_notify(&(event_notify_params));
#else
  (void)qsh_event;
#endif /* FEATURE_QSH_EVENT_METRIC */
}/* uim_common_qsh_event_notify */
