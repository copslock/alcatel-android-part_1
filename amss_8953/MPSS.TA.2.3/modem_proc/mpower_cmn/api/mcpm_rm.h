#ifndef __MCPM_RM_H__
#define __MCPM_RM_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R

                           H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the interface functions and definitions for MCPM
    resource manager (RM).


  EXTERNALIZED FUNCTIONS
    MCPM_RM_Init
    MCPM_RM_Apply_Configuration
    MCPM_RM_Register_Callback
    MCPM_RM_Update_Wakeup_Time

  INITIALIZATION AND SEQUENCING REQUIREMENTS
    Invoke the MCPM_RM_Init function to initialize the MCPM RM.

        Copyright (c) 2017 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mpower_cmn.mpss/1.0.1/api/mcpm_rm.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------- 
05/24/16   sz      Added API to get tech client pending state.  
03/29/16   ys      Deferred Async Implementation.
03/24/16   yz      Initial common code version and it's based on CL 10130557 (for TA) and CL 10130735 (for AT) 
                   on Thor: //components/rel/mpower.mpss/5.0/mcpm/inc/mcpm_rm.h#18


==========================================================================*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/

/*
 * Includes in the local directory.
 */
#include "mcpm_drv.h"
#include "mcpm_trace.h"

/*
 * Includes outside of the local directory.
 */
#include <mcpm_api.h>
#include <mcpm_utils.h>
#include <DDIClock.h>      /* public API interface to DAL */

/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/
/* ============================================================================
**    Externs
** ==========================================================================*/

/* =========================================================================
**  Function : MCPM_Get_DAL_Handle
** =========================================================================*/
/**
  This function returns the DAL handle required to make DAL API calls.

  @param
  None.

  @return
  DalDeviceHandle

  @dependencies
  TBD.

*/

extern DalDeviceHandle * MCPM_Get_DAL_Handle(void);

/* ============================================================================
**    Definitions
** ==========================================================================*/

/*
 * Resource flag bits
 *
 *  Resource type flags:
 *  MCPM_RM_RESRC_FLAG_REG_RESOURCE             - regular (non schedulable) resource.
 *  MCPM_RM_RESRC_FLAG_REG_SPECIAL_RESOURCE     - regular (non schedulable) special resource.
 *                                                (this resource is called by RM as long as the new
 *                                                 muxed value is different from the last committed one.
 *                                                 Resource internally decides on what to do based on
 *                                                 UP or DOWN direction flag that RM passes to it.)
 *  MCPM_RM_RESRC_FLAG_SCHED_RESOURCE           - schedulable resource.
 *  MCPM_RM_RESRC_FLAG_TRANS_RESOURCE           - resource commit uses npa transactions, so commit
 *                                                must be called every time regardless of the value
 *                                                being committed.
 *
 *  Resource location flags:
 *  MCPM_RM_RESRC_FLAG_LOCAL_PROCESSOR          - resource is local.
 *  MCPM_RM_RESRC_FLAG_REMOTE_PROCESSOR         - resource is remote.
 *
 *  Resource ownership flags:
 *  MCPM_RM_RESRC_FLAG_OTHER_OWNED              - resource is owned by other team.
 *  MCPM_RM_RESRC_FLAG_MCPM_OWNED               - resource is owned by MCPM team.
 *
 *  Misc flags
 *  MCPM_RM_RESRC_FLAG_NOASYNC                  - resource cannot be commited in async SRR thread.
 *  MCPM_RM_RESRC_FLAG_NPA_INTERFACE            - resource interface is NPA.
 *  MCPM_RM_MUX_MCVS_IN_LIGHT_SLEEP             - resource to be muxed in light sleep.
 *  MCPM_RM_NO_MCVS_IN_LIGHT_SLEEP              - resource which is MCVS resoure and not MCVS-allowed in light sleep, i.e. its MCVS request will not honored in light sleep
 *
 * Resource Type Flags:
 * MCPM_RM_RESRC_FLAG_VECTOR_TYPE               - indicates resource is a NPA vector resource and may require custom mux
 *
 */
#define MCPM_RM_RESRC_FLAG_REG_RESOURCE             0x00000001
#define MCPM_RM_RESRC_FLAG_REG_SPECIAL_RESOURCE     0x00000002
#define MCPM_RM_RESRC_FLAG_SCHED_RESOURCE           0x00000004
#define MCPM_RM_RESRC_FLAG_TRANS_RESOURCE           0x00000008
#define MCPM_RM_RESRC_MULTI_TECH_MUX_RESOURCE       0x00000010

#define MCPM_RM_RESRC_FLAG_OTHER_OWNED              0x00000100
#define MCPM_RM_RESRC_FLAG_MCPM_OWNED               0x00000200

#define MCPM_RM_RESRC_FLAG_DEFERRED_ASYNC           0x00001000
#define MCPM_RM_RESRC_FLAG_EXPLICIT_COMPLETE_SCHED  0x00002000

#define MCPM_RM_RESRC_FLAG_LOCAL_PROCESSOR          0x00010000
#define MCPM_RM_RESRC_FLAG_REMOTE_PROCESSOR         0x00020000

#define MCPM_RM_RESRC_FLAG_NPA_INTERFACE            0x00100000
#define MCPM_RM_RESRC_FLAG_VECTOR_TYPE              0x00200000

#define MCPM_RM_MUX_MCVS_IN_LIGHT_SLEEP             0x00400000
#define MCPM_RM_NO_MCVS_IN_LIGHT_SLEEP              0x00800000

#define MCPM_RESRC_UNIT_KHZ                         "KHz"
#define MCPM_RESRC_UNIT_KBS                         "KB/s"
#define MCPM_RESRC_UNIT_MBS                         "MB/s"
#define MCPM_RESRC_UNIT_USEC                        "usec"

#define MCPM_RM_RESRC_NO_UPDATE                     MCPM_RESRC_CFG_VALUE_DC

/* ============================================================================
**    Typedefs
** ==========================================================================*/

/*
 * MCPM_RM_Resrc_State_Type
 *
 * MCPM resource state type.
 */
typedef uint32 MCPM_RM_Resrc_StateType;


/*
 * MCPM_RM_Resrc_DataType
 *
 * MCPM resource data type.
 */
typedef void * MCPM_RM_Resrc_DataType;


/*
 * MCPM_RM_Resrc_Init_FuncType
 *
 * MCPM resource init function type.
 */
typedef void (*MCPM_RM_Resrc_Init_FuncType)(const MCPM_Drv_CtxtType *pDrvCtxt);


/*
 * MCPM_RM_Resrc_CallbackType
 *
 * MCPM resource callback type.
 */
typedef void (*MCPM_RM_Resrc_Callback_FuncType) (void);


/*
 * MCPM_RM_ResrcType
 *
 * RM resource type.  Each resource will use this to identify its type.
 *
 * VOLTAGE_RAIL - This is a voltage rail/regulator resource.
 * SYSTEM_BUS   - This is a system bus resource.
 * MODEM_BUS    - This is a modem bus resource.
 * MODEM_BLOCK  - This is a modem block resource.
 * MODEM_CLOCK  - This is a modem clock resource.
 * MODEM_RF     - This is a modem RF resource.
 * MODEM_FW     - This is a modem FW resource.
 * MODEM_SYSTEM - This is one of various modem resources
 */
typedef enum
{
  VOLTAGE_RAIL,
  SYSTEM_BUS,
  MODEM_PLL,
  MODEM_BUS,
  MODEM_BLOCK,
  MODEM_CLOCK,
  MODEM_RF,
  MODEM_FW,
  MODEM_SYSTEM,
  MCPM_ENUM_32BITS(RESRC_TYPE)
} MCPM_RM_ResrcType;


/*
 * MCPM_RM_MuxType
 *
 * RM mux type.  Each resource will use this to identify its mux operation.
 *
 * NO_MUX_PLUGIN     - This resource has no muxing requirements
 * OR_MUX_PLUGIN     - This resource requires OR mux.
 * MIN_MUX_PLUGIN    - This resource requires MIN mux. If used, you most likely
 *                     will want to define a non-zero ResetState
 * MAX_MUX_PLUGIN    - This resource requires MAX mux.
 * SUM_MUX_PLUGIN    - This resource requires SUM mux.
 * CUSTOM_MUX_PLUGIN - This resource requires a customize mux which resource will
 *                     implement and RM will call during mux operation.
 */
typedef enum
{
  NO_MUX_PLUGIN,
  OR_MUX_PLUGIN,
  MIN_MUX_PLUGIN,
  MAX_MUX_PLUGIN,
  SUM_MUX_PLUGIN,
  CUSTOM_MUX_PLUGIN,
  MCPM_ENUM_32BITS(MUX_PLUGIN)
} MCPM_RM_MuxType;


/*
 * MCPM_RM_RequestType
 *
 * RM request type.
 *
 * NO_REQ                     - No operation needed.
 * OVERWRITE_ALL_REQ          - RM will overwrite with new request config.
 * OVERWRITE_NON_DC_REQ       - RM will overwrite with new non-DC request config.
 * MUX_REQ                    - RM will operate mux with new request config.
 */
typedef enum
{
  NO_REQ,
  OVERWRITE_ALL_REQ,
  OVERWRITE_NON_DC_REQ,
  MUX_REQ,
  MCPM_ENUM_32BITS(REQ_TYPE)
} MCPM_RM_RequestType;


/*
 * MCPM_RM_Resrc_Callback_TrigType
 *
 * RM callback trigger type.  Each resource will use this to identify its callback trigger type.
 *
 * TRIG_BEFORE_STATE_CHANGE   - Trigger before resource state changes.
 * TRIG_AFTER_STATE_CHANGE    - Trigger after resource state changes.
 */
typedef enum
{
  TRIG_BEFORE_STATE_CHANGE,
  TRIG_AFTER_STATE_CHANGE,
  MCPM_ENUM_32BITS(TRIG_TYPE)
} MCPM_RM_Resrc_Callback_TrigType;

/*
 * MCPM_Translate_Type
 *
 * The types of translation for resource translate functions.
 * TRANSLATE_FROM_MCPM   - Translate from an MCPM value.
 * TRANSLATE_TO_MCPM     - Translate to an MCPM value.
 */
typedef enum
{
  TRANSLATE_FROM_MCPM,
  TRANSLATE_TO_MCPM,
} MCPM_Translate_Type;

/*
 * MCPM_RM_Resrc_Request_ParamsType
 *
 * Additional request parameters resources may need.
 *
 *
 * bTechClientsMuxNeeded  - Muxing across clients can be skipped if it is FALSE.
 * bCommitSwitch      - Commit switch - 1 (ON/Increase) or 0 (OFF/Decrease)
 * bSleepRequest      - Whether this is a sleep request.
 * tWakeupTime        - Scheduled wakeup time.
 * bPowerDownRequest  - Whether this is a power down request.
 * bSkipSRRWakeup     - If SRR commits can be skipped on wakeup (i.e. all
 *                      scheduled requests have been applied)
 * pmReqCfgParams     - Pointer to MCPM_Config_Modem request parameters.
 * eRequest           - MCPM request type.
 */
typedef struct
{
  boolean                 bTechClientsMuxNeeded;
  boolean                 bCommitSwitch;
  boolean                 bSleepRequest;
  uint64                  tWakeupTime;
  boolean                 bPowerDownRequest;
  boolean                 bSkipSRRWakeup;
  mcpm_request_parms_type *pmReqCfgParams;
  mcpm_request_type       eRequest;
} MCPM_RM_Resrc_Request_ParamsType;

/*
 * MCPM_RM_Resrc_InfoType
 *
 * Resource information
 *
 * State       - Resource state.
 */
typedef struct
{
  MCPM_RM_Resrc_StateType    State[MCPM_NUM_RESRC];
} MCPM_RM_Resrc_InfoType;

/*
 * MCPM_RM_Resrc_ClientType
 *
 * MCPM resource client type.
 *
 * PendingState    - Array of pending client resource states.
 * PrevState       - Array of previous client resource states.
 * CurrState       - Array of current client resource states.
 */
typedef struct
{
  MCPM_RM_Resrc_StateType  PendingState[MCPM_NUM_RESRC];
  MCPM_RM_Resrc_StateType  PrevState[MCPM_NUM_RESRC];
  MCPM_RM_Resrc_StateType  CurrState[MCPM_NUM_RESRC];
}MCPM_RM_Resrc_ClientType;


/*
 * MCPM_RM_Resrc_Callback_InfoType
 *
 * Resource callback information.
 *
 * fCallback      - Callback function.
 * eTrigType      - Callback function trigger type.
 */
typedef struct
{
  MCPM_RM_Resrc_Callback_FuncType         fCallback;
  MCPM_RM_Resrc_Callback_TrigType         eTrigType;
}MCPM_RM_Resrc_Callback_InfoType;


/*
 * MCPM_RM_Resrc_Tech_InfoType
 *
 * Tech specific resource state and associated callback function (if any).
 *
 * ClientState - Array of resouce client states.
 * PrevState   - Array of resource overall previous states.
 * CurrState   - Array of resource overall current states.
 * mCallback   - Array of resource callback information.
 */
typedef struct
{
  MCPM_RM_Resrc_ClientType         ClientState[MCPM_NUM_RESRC_CLIENTS];
  MCPM_RM_Resrc_StateType          PrevState[MCPM_NUM_RESRC];
  MCPM_RM_Resrc_StateType          CurrState[MCPM_NUM_RESRC];
  MCPM_RM_Resrc_Callback_InfoType  mCallback[MCPM_NUM_RESRC];
} MCPM_RM_Resrc_Tech_InfoType;


/*
 * MCPM_RM_Resrc_CtrlType
 *
 * Set of function pointers used to control a resource.
 *
 * Mux_Resource     - Mux set of two values for a given resource.
 * Commit_Resource  - Commit a value to a given resource.
 * Compare_Resource   - Customize compare for a given resource.
 * Callback_Resource  - Customize callback for a given resource.
 * Custom_Resource    - Special customized function for a given resource.
 * Translate_Resource - Translate between MCPM resource values in RM and external(e.g. VCS) resource values.
 * Custom_Lock        - Customize lock for a given resource.
 */
typedef struct
{
  MCPM_RM_Resrc_StateType  (*Mux_Resource)        ( mcpm_tech_type                    eTech,
                                                    MCPM_RM_Resrc_StateType           State1,
                                                    MCPM_RM_Resrc_StateType           State2,
                                                    MCPM_Mux_Stage_Type                     muxStage);

  MCPM_RM_Resrc_StateType  (*Commit_Resource)     ( mcpm_tech_type                    eTech,
                                                    MCPM_RM_Resrc_StateType           State,
                                                    const MCPM_RM_Resrc_Request_ParamsType  *pmResrcReqParams,
                                                    MCPM_RM_Resrc_DataType            *pResrcData);

  boolean                  (*Compare_Resource)    ( MCPM_RM_Resrc_StateType           TechState1,
                                                    MCPM_RM_Resrc_StateType           TechState2);


  MCPM_RM_Resrc_StateType  (*Callback_Resource)   ( mcpm_tech_type                    eTech,
                                                    MCPM_RM_Resrc_StateType           TechPrevState,
                                                    MCPM_RM_Resrc_StateType           TechCurrState,
                                                    const MCPM_RM_Resrc_Request_ParamsType  *mResrcReqParams,
                                                    MCPM_RM_Resrc_Callback_FuncType   fCallback);

  void                     (*Custom_Resource)     ( mcpm_tech_type                          eTech);

  uint32                   (*Translate_Resource)  ( MCPM_Resrc_IDType                       ResourceID,
                                                    MCPM_RM_Resrc_StateType                 State,
                                                    MCPM_Translate_Type                     TranslateType);

  void                     (*Custom_Lock)         ( boolean                                 bEnterMuxCommit);

} MCPM_RM_Resrc_CtrlType;


/*
 * MCPM_RM_Resrc_PropertiesType
 *
 * Resource properties.
 *
 * szName         - Resource name.
 * szUnit         - Resource unit.
 * eType          - Resource type.
 * eMuxType       - Resource mux type.
 * pmCtrlType     - Resource control type.
 * nFlags         - Flags set for this resource.
 * ResetState     - Resource reset state (only used for resource which has
 *                  non-zero cold-boot value). For resources which use the MIN
 *                  mux plugin, you will probably want to give a non-zero value
 *                  for this.
 * BlkEnableState - Modem block resource enable (only used for HW block/register
 *                  which has special enable value - i.e., BCR register) state.
 * pData          - Pointer to resource specific data (or NULL).
 */
typedef struct
{
  const char                    *szName;
  const char                    *szUnit;
  const MCPM_RM_ResrcType       eType;
  const MCPM_RM_MuxType         eMuxType;
  const MCPM_RM_Resrc_CtrlType  *pmCtrlType;
  const uint32                  nFlags;
  MCPM_RM_Resrc_StateType       ResetState;
  MCPM_RM_Resrc_StateType       BlkEnableState;
  MCPM_RM_Resrc_DataType        pData;
} MCPM_RM_Resrc_PropertiesType;


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/

/* =========================================================================
**  Function : MCPM_RM_Enter_Atomic_Section
** =========================================================================*/
/**
  This function enters RM atomic setcion - resource lock and other necessary locks.

  @param
  nResrcID   [in]  Resource ID.

  @return
  None.

  @dependencies
  None.

*/
void MCPM_RM_Enter_Atomic_Section
(
  MCPM_Resrc_IDType       nResrcID
);


/* =========================================================================
**  Function : MCPM_RM_Leave_Atomic_Section
** =========================================================================*/
/**
  This function leaves RM atomic setcion - resource lock and other necessary locks.

  @param
  nResrcID   [in]  Resource ID.

  @return
  None.

  @dependencies
  None.

*/
void MCPM_RM_Leave_Atomic_Section
(
  MCPM_Resrc_IDType       nResrcID
);


/* =========================================================================
**  Function : MCPM_RM_Install_Resource
** =========================================================================*/
/**
  Installs a resource.

  This function installs a resource.  RM adds the resource into a lookup table
  and updates other variables accordingly.

  @param
  pResrcProperties   [in]  Pointer to a resource's properties.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Install_Resource
(
  MCPM_RM_Resrc_PropertiesType *pResrcProperties
);


/* =========================================================================
**  Function : MCPM_RM_Init
** =========================================================================*/
/**
  Initializes MCPM resource manager (RM).

  This function initializes MCPM resource manager and all the resources managed
  by it.

  @param
  pDrvCtxt - Pointer to the driver context.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Init (const MCPM_Drv_CtxtType *pDrvCtxt);


/* =========================================================================
**  Function : MCPM_RM_Get_Resrc_ID
** =========================================================================*/
/**
  Returns an ID if a given resource (string name) is found.

  This funciton returns an ID if a given resource (string name) is found.

  @param
  szResrcName       [in] Resource string name.

  @return
  MCPM_Resrc_IDType - Return resource ID (if found), ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_Resrc_IDType MCPM_RM_Get_Resrc_ID
(
  const char *szResrcName
);


/* =========================================================================
**  Function : MCPM_RM_Get_Resrc_Committed_State
** =========================================================================*/
/**
  Returns resource's current committed state.

  This funciton returns resource's current committed state.

  @param
  eResrcID               [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType - Resource committed state if resource ID found, ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Resrc_Committed_State
(
  MCPM_Resrc_IDType eResrcID
);


/* =========================================================================
**  Function : MCPM_RM_Get_Client_Prev_Resrc_State
** =========================================================================*/
/**
  Returns previous resource state for a given tech client.

  This funciton returns previous resource state for a given tech client.

  @param
  eTech                   [in] Requesting tech.
  eClient                 [in] Requesting client.
  nResrcID                [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType  - Tech's resource state if the input params are valid,
                             ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Client_Prev_Resrc_State
(
  mcpm_tech_type          eTech,
  MCPM_Resrc_ClientType   eClient,
  MCPM_Resrc_IDType       nResrcID
);


/* =========================================================================
**  Function : MCPM_RM_Get_Client_Resrc_State
** =========================================================================*/
/**
  Returns current resource state for a given tech client.

  This funciton returns current resource state for a given tech client.

  @param
  eTech                   [in] Requesting tech.
  eClient                 [in] Requesting client.
  nResrcID                [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType  - Tech's resource state if the input params are valid,
                             ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Client_Resrc_State
(
  mcpm_tech_type          eTech,
  MCPM_Resrc_ClientType   eClient,
  MCPM_Resrc_IDType       nResrcID
);


/* =========================================================================
**  Function : MCPM_RM_Get_Resrc_State
** =========================================================================*/
/**
  Returns current resource state for a given tech.

  This funciton returns current resource state for a given tech.

  @param
  eTech                   [in] Requesting tech.
  nResrcID                [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType  - Tech's resource state if the input params are valid,
                             ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Resrc_State
(
  mcpm_tech_type          eTech,
  MCPM_Resrc_IDType       nResrcID
);

/* =========================================================================
**  Function : MCPM_RM_Get_Pending_Resrc_State
** =========================================================================*/
/**
  Returns pending resource state for a given tech.

  This funciton returns the pending resource state for a given tech.

  @param
  eTech                   [in] Requesting tech.
  nResrcID                [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType  - Tech's pending resource state if the input params are valid,
                             ERR_FATAL otherwise.

  @dependencies
  None.

*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Pending_Resrc_State
(
  mcpm_tech_type          eTech,
  MCPM_Resrc_IDType       nResrcID
);

/* =========================================================================
**  Function : MCPM_RM_Get_Overall_Resrc_State
** =========================================================================*/
/**
  Returns current resource state for all the techs (muxed state)

  @param
  nResrcID                [in] Resource ID.

  @return
  MCPM_RM_Resrc_StateType  - Tech's resource state if the input params are valid,
                             ERR_FATAL otherwise.

  @dependencies
  None.

*/
MCPM_RM_Resrc_StateType MCPM_RM_Get_Overall_Resrc_State
(
  MCPM_Resrc_IDType       nResrcID
);

/* =========================================================================
**  Function : MCPM_RM_Request_Client_Resrc_State
** =========================================================================*/
/**
  Requests new resource state for a given tech client.

  This funciton requests new resource state for a given tech client.
  The requested resource state is considered a pending request.  This request
  will be applied when one of the mux+commit functions is called.

  @param
  eTech                   [in] Requesting tech.
  eClient                 [in] Requesting client.
  nResrcID                [in] Resource ID.
  State                   [in] State to set to.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Request_Client_Resrc_State
(
  mcpm_tech_type          eTech,
  MCPM_Resrc_ClientType   eClient,
  MCPM_Resrc_IDType       nResrcID,
  MCPM_RM_Resrc_StateType State
);


/* =========================================================================
**  Function : MCPM_RM_Request_Resrc_States
** =========================================================================*/
/**
  Requests tech config_modem client resource states.

  This funciton updates resource states with new requests for a tech config_modem
  client. The requested resource states are considered part of pending requests.  This
  request will be applied when MCPM_RM_Mux_Commit_Resrc_States function is called.

  @param
  eTech                 [in]  Requesting tech.
  pResrcConfig          [in]  Pointer to the input configuration array.
  nConfigLength         [in]  Length of the input configuration array.
  MCPM_RM_RequestType       [in]  RM request type.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Request_Resrc_States
(
  mcpm_tech_type                     eTech,
  const MCPM_RM_Resrc_StateType      *pResrcConfig,
  uint32                             nConfigLength,
  MCPM_RM_RequestType                eReqType
);


/* =========================================================================
**  Function : MCPM_RM_Mux_Commit_Resrc_States
** =========================================================================*/
/**
  Muxes and commits pending tech config_modem client configuration to all the
  resources (if required).

  This function muxes and commits pending tech config_modem client configuration
  to all the resources (if required).  The function goes through
  all the resource states and performs a mux operation.  The mux result is
  then committed by invoking resource specific commit function.

  @param
  eTech                 [in]  Requesting tech.
  pmResrcReqParams      [in]  Pointer to the request parameters.

  @return
  None.

  @dependencies
  MCPM_RM_Request_Resrc_States or MCPM_RM_Request_Client_Resrc_State must be called
  before calling this function to request for required resource state(s).

*/

void MCPM_RM_Mux_Commit_Resrc_States
(
  mcpm_tech_type                         eTech,
  MCPM_RM_Resrc_Request_ParamsType       *pmResrcReqParams
);


/* =========================================================================
**  Function : MCPM_RM_Mux_Commit_MCVS_Resrc_States
** =========================================================================*/
/**
  Muxes and commits pending tech non-config_modem client configuration to all
  the resources (if required).

  This function muxes and commits pending tech non-config_modem client
  configuration to all the resources (if required).  The function goes through
  all the resource states and performs a mux operation.  The mux result is
  then committed by invoking resource specific commit function.

  @param
  eTech                 [in]  Requesting tech.
  pmResrcReqParams      [in]  Pointer to the request parameters.

  @return
  None.

  @dependencies
  MCPM_RM_Request_Client_Resrc_State must be called before calling this function
  to request for required resource state(s).

*/

void MCPM_RM_Mux_Commit_MCVS_Resrc_States
(
  mcpm_tech_type                         eTech,
  MCPM_RM_Resrc_Request_ParamsType *pmResrcReqParams
);


/* =========================================================================
**  Function : MCPM_RM_Register_Callback
** =========================================================================*/
/**
  Registers a callback function for a given resource.

  This funciton registers a callback function for a given resource.  This
  callback is called when a resource state changes.

  @param
  eTech       [in] Requesting tech.
  nResrcID    [in] Resource ID.
  fCallback   [in] Pointer to a callback function.
  eTrigType   [in] Resource callback function trigger condition.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Register_Callback
(
  mcpm_tech_type                      eTech,
  MCPM_Resrc_IDType                   nResrcID,
  MCPM_RM_Resrc_Callback_FuncType     fCallback,
  MCPM_RM_Resrc_Callback_TrigType     eTrigType
);


/* =========================================================================
**  Function : MCPM_RM_Schedule_Remote_Resources_No_Async
** =========================================================================*/
/**
  Updates wakeup times for schedulable resources.

  This funciton updates wakeup times for schedulable resources.

  NOTE: This should be used with care in situations where Async SRR could be
  occurring, as it doesn't check the Async SRR flags before attempting to
  reschedule. MCPM_RM_Schedule_Remote_Resources() should be used if possible.

  @param
  eTech             [in] Requesting tech.
  tNewWakeupTime    [in] New wakeup time.
  bAsyncFlag        [in] Flag to indicate if it's called by async thread.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Schedule_Remote_Resources_Async 
(
  mcpm_tech_type eTech,
  uint64         tNewWakeupTime,
  boolean        bAsyncFlag,
  boolean        bAsyncDeferred
);


/* =========================================================================
**  Function : MCPM_RM_Schedule_Remote_Resources
** =========================================================================*/
/**
  Schedules resources after checking if there's currently a pending Async
    request. The check protects against concurrent access of structures.

  NOTE: This function is preferred over MCPM_RM_Schedule_Remote_Resources_No_Async()
    in most cases, as it adds little overhead and provides some security against
    concurrent accesses. MCPM_RM_Schedule_Remote_Resources_Async() should
    only be used if there's an issue with checking the Async flag in the context
    it's called from.

  @param eTech             [in] Requesting tech.
  @param tNewWakeupTime    [in] New wakeup time.

  @return
  None

  @dependencies
  TBD.

*/
void MCPM_RM_Schedule_Remote_Resources
(
  mcpm_tech_type eTech,
  uint64         tNewWakeupTime
);


/* =========================================================================
**  Function : MCPM_RM_Update_Last_Committed_State
** =========================================================================*/
/**
  Updates last committed state of a given resource.

  This funciton updates last committed state of a given resource.

  @param
  pmResrcProperties   [in] Resource property address (for security check).
  nResrcID    [in] Resource ID.
  State       [in] State to update to.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_RM_Update_Last_Committed_State
(
  const MCPM_RM_Resrc_PropertiesType  *pmResrcProperties,
  MCPM_Resrc_IDType        nResrcID,
  MCPM_RM_Resrc_StateType  State
);

/* =========================================================================
**  Function : MCPM_Resrc_HW_Init
** =========================================================================*/
/**
  Contains any resource HW initialization as required.

  This function contains any resource HW initialization as required.

  @param
  None.

  @return
  None.

  @dependencies
  TBD.

*/

void MCPM_Resrc_HW_Init (void);

/* =========================================================================
**  Function : MCPM_Resrc_HW_Deinit
** =========================================================================*/
/**
  Contains any resource HW deinitalization as required.

  This function contains any resource HW deinitalization as required.

  @param
  None.

  @return
  None.

  @dependencies
  TBD.

*/

void MCPM_Resrc_HW_Deinit (void);
/* =========================================================================
**  Function : MCPM_RM_Sched_Reqs_Complete
** =========================================================================*/
/**
  Checks if a tech has any scheduled requests pending by looping through all
  resources and checking SRR "Pending" flag.

  @param
  eTech    [in] Tech to check.

  @return
  None.

  @dependencies
  None.

*/
boolean MCPM_RM_Sched_Reqs_Complete
(
  mcpm_tech_type          eTech
);


/* =========================================================================
**  Function : MCPM_Get_Resource_Properties
** =========================================================================*/
/**
  Returns a copy of the properties data structure for a given resource.
  @param
  nResrcID [in] Resource ID to get properties for

  @return
  Pointer to MCPM_RM_Resrc_PropertiesType for nResrcID
  The Property might be NULL, so please check the return to further use it.

  @dependencies
  None
*/
MCPM_RM_Resrc_PropertiesType * MCPM_Get_Resource_Properties
(
  MCPM_Resrc_IDType        nResrcID
);
/* =========================================================================
**  Function : MCPM_RM_Get_Client_PendingState
** =========================================================================*/
/*
  Returns a copy of the pending state for a given resource of a given client.
 
  @param
  eTech    [in] Tech to get pending state for.
  eClient  [in] eClient to get pending state for.
  nResrcID [in] Resource ID to get pending state for.

  @return
  Pending state of the resource.

  @dependencies
  None
*/

MCPM_RM_Resrc_StateType MCPM_RM_Get_Client_PendingState
(
  mcpm_tech_type             eTech,
  MCPM_Resrc_ClientType      eClient,
  MCPM_Resrc_IDType          nResrcID
);

#endif /* !MCPM_RM_H */

