#ifndef __MCPM_NV_CFG_H__
#define __MCPM_NV_CFG_H__
/*=========================================================================


     M O D E M   C L O C K   A N D   P O W E R   M A N A G E R
                  
                     N V  H E A D E R   F I L E



GENERAL DESCRIPTION

  This file contains the implementation of the the Modem Clock and Power
  Manager NV configuration functions.

PUBLIC EXTERNALIZED FUNCTIONS
  

INITIALIZATION AND SEQUENCING REQUIREMENTS
  Invoke the MCPMDRV_Init function to initialize the Modem Clock and Power
  Manager Driver.



    Copyright (c) 2017 by QUALCOMM Technologies, Inc.  All Rights Reserved.

==========================================================================*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mpower_cmn.mpss/1.0.1/api/mcpm_nv_cfg.h#1 $

when         who      what, where, why
--------     ---      --------------------------------------------------------
07/31/15     ls       Added MVC feature (phase 1 with Boot-safe CPR).
06/18/15     hg       Added changes for common code cleanup.
01/30/15     ls       Added multi-tech LPR support in power debug feature. 
05/05/15     mk       Added support for modem sensor interface.
01/06/15     ls       Added light sleep support in power debug feature.
11/13/14     sz       Removed the NV for ulog because MCPM reads diag NV to disable ulog.
10/28/14     sz       Added disable ulog NV item.
10/10/14     ls       Added MCPM CPU boost support for FW.
09/05/14     sc       Added support for CMCC power optimization feature.
02/19/14     ls       Ported latest power debug changes from Dime.
01/23/14     bd       Added PMU Logging Support
01/15/14     sr       Added new trace points for profiling.
02/12/13     vs       Ported from Dime release branch. 

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

/*
 * Includes outside of the local directory.
 */
#include <comdef.h>
#include <mcpm_api.h>

/*==========================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/


/*=========================================================================
      Constants and Macros
==========================================================================*/

#define MCPM_NV_IS_SET                      0x1
#define MCPM_UTILS_PWRDBG_RESERV_ITEMS      4
#define MCPM_UTILS_NVITEM_RESERV_ITEMS      3
#define MCPM_UTILS_PMU_RESERV_ITEMS         2
#define MCPM_MAX_LENGTH_EFS_VALUE           4
#define MCPM_MAX_POWER_OPTIMIZATION_VALUE   5
#define MCPM_AMD_RESERV_ITEMS               6


/*=========================================================================
      Typedefs
==========================================================================*/

/* Legacy MCPM NV item, size is fixed to 16 bytes*/
/*
 * MCPM_NV_ITEM_T 
 *  
 * MCPM NV items.  
 */

typedef struct 
{
  uint8  MCPM_NV_QDSS_ENABLE;  
  uint8  MCPM_NV_GPIO_Profile_Port;
  uint8  MCPM_NV_UnitTest_Ctrl;
  uint8  MCPM_NV_FW_Clk_Boost_Mode;
  uint32 MCPM_NV_UnitTest_Commands;
  uint32 MCPM_NV_UnitTest_reserved;  
  uint8  MCPM_NV_Trace_Enable;

  uint8  reserved[MCPM_UTILS_NVITEM_RESERV_ITEMS];
} MCPM_NV_ITEM_T;


/*
 * MCPM_NV_TIMER_TYPE_ENUM 
 *  
 * MCPM NV timer type.  
 */

typedef enum 
{
  MCPM_NO_TIMER,
  MCPM_REX_TIMER,
  MCPM_DEFERRABLE_TIMER,
  MCPM_NON_DEFERRABLE_TIMER
} MCPM_NV_TIMER_TYPE_ENUM;


/*
 * MCPM_NV_ACTION_ENUM 
 *  
 * MCPM NV action.  
 */

typedef enum 
{
  MCPM_Crash_UE,
  MCPM_Print_F3_Msg
} MCPM_NV_ACTION_ENUM;


/*
 * MCPM_NV_POWER_DEBUG_MODE_ENUM 
 *  
 * MCPM NV power debug mode.  
 */

typedef enum
{
  MCPM_PWRDBG_DEFAULT_MODE,
  MCPM_PWRDBG_BOOTUP,
  MCPM_PWRDBG_CHK_BOOTUP_PC,
  MCPM_PWRDBG_LPR,
  MCPM_PWRDBG_MULTI_TECH,
  MCPM_PWRDBG_MULTI_TECH_LPR
} MCPM_NV_POWER_DEBUG_MODE_ENUM; 


/*
 * MCPM_NV_PwrDbg_T 
 *  
 * MCPM NV power debug type.  
 */

typedef struct
{
  MCPM_NV_POWER_DEBUG_MODE_ENUM MCPM_NV_PwrDbg_Mode;
  MCPM_NV_TIMER_TYPE_ENUM MCPM_NV_PwrDbg_Timer_Type;
  MCPM_NV_ACTION_ENUM MCPM_NV_PwrDbg_Timer_Expires_Action;
  uint8 MCPM_NV_PwrDbg_Tech;

  uint32 MCPM_NV_PwrDbg_Set_Timer;
  uint32 MCPM_NV_PwrDbg_SleepNum;
  uint32 MCPM_NV_PwrDbg_WakeupNum;
  uint32 MCPM_NV_PwrDbg_VoiceNum;
  uint32 MCPM_NV_PwrDbg_DataNum;
  uint32 MCPM_NV_PwrDbg_LightSleepNum;

  uint8 reserved[MCPM_UTILS_PWRDBG_RESERV_ITEMS];
} MCPM_NV_PwrDbg_T; 


/*
 * MCPM_NV_PwrOpt_T
 *
 * MCPM NV for power optimization.
 */

typedef struct 
{
  /*
   * Every bit in MCPM_NV_Tech_List corresponds to a L1 tech in the order 
   * as defined in mcpm_api.h.
   */
  uint32 MCPM_NV_Tech_List;
  
  /* 
   * Fields corresponding to each L1 tech designating the degree of aggressive
   * power saving.
   */
  uint8 MCPM_NV_Tech_level[MCPM_NUM_RAT_TECH];
} MCPM_NV_PwrOpt_T;


/*
 * MCPM_NV_PMU_T 
 *  
 * MCPM NV PMU Config type.  
 */

typedef struct
{
  uint32   PMU_SAMPLE_DELAY_US;
  boolean  PMU_TRACE_EN;
  boolean  PMU_CPP_ONLY;
  uint8    reserved[MCPM_UTILS_PMU_RESERV_ITEMS];
} MCPM_NV_PMU_T;


/*
 * Modem AMD interface NV configuration
 */

typedef struct
{
  uint8   MCPM_AMD_ENABLE;
  uint8   MCPM_AMD_FAKE_STATIONARY_MOBILITY;
  uint8   reserved[MCPM_AMD_RESERV_ITEMS];
} MCPM_NV_AMD_T;


/*
 * MCPM NV configuration type.
 */

typedef struct
{
  MCPM_NV_ITEM_T   mcpm_nv_cfg_info;
  MCPM_NV_PwrDbg_T mcpm_nv_pwrdbg_info;
  MCPM_NV_PwrOpt_T mcpm_nv_pwropt_info;
  MCPM_NV_PMU_T    mcpm_nv_pmu_info;
  MCPM_NV_AMD_T    mcpm_nv_amd_info;
}MCPM_NV_CFG_T;


/*
 * MCPM_NV_FW_CLK_BOOST_MODE_ENUM 
 *  
 * MCPM FW clk boost mode.  
 */

typedef enum
{
  MCPM_FW_CLK_BOOST_DEFAULT_MODE,
  MCPM_FW_CLK_BOOST_NOM,
  MCPM_FW_CLK_BOOST_TURBO
} MCPM_NV_FW_CLK_BOOST_MODE_ENUM; 


/*=========================================================================
      Prototypes
==========================================================================*/



/*=========================================================================
      Variables
==========================================================================*/

/*==========================================================================
               FUNCTION DEFINITIONS FOR MODULE
==========================================================================*/

/*===========================================================================
FUNCTION     MCPM_Read_NV_Items

DESCRIPTION  This function reads the NV items from EFS and loads into a data 
             structure. 

DEPENDENCIES MCPM_NV_Init must be called before calling this function.

PARAMETERS   None.

RETURN VALUE Returns NV items.

SIDE EFFECTS None.
===========================================================================*/

MCPM_NV_CFG_T MCPM_Read_NV_Items(void);


/* =========================================================================
**  Function : MCPM_EFS_Ctrl
** =========================================================================*/
/**
  This function reads and returns EFS file content. The content can be used
  to enable/disable SW flags.

  @param
  pFilePath           [in] path to the EFS file

  @return
    File content in uint8.

  @dependencies
  None.
 
*/

uint8 MCPM_EFS_Ctrl(const char * pFilePath);


/*===========================================================================
FUNCTION     MCPM_NV_Init

DESCRIPTION  This function initializes MCPM NV items.

DEPENDENCIES None.

PARAMETERS   None.

RETURN VALUE None.

SIDE EFFECTS None.
===========================================================================*/

void MCPM_NV_Init(void);


#endif /* __MCPM_NV_CFG_H__ */

