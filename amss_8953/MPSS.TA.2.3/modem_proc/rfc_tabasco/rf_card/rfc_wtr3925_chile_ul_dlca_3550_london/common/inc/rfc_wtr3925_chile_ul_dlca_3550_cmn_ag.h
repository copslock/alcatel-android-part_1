
#ifndef RFC_WTR3925_CHILE_UL_DLCA_3550_CMN_AG
#define RFC_WTR3925_CHILE_UL_DLCA_3550_CMN_AG

#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: xmlautogen.exe
Generated from: V5.23.1000 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2016 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/rfc_tabasco.mpss/2.0/rf_card/rfc_wtr3925_chile_ul_dlca_3550/common/inc/rfc_wtr3925_chile_ul_dlca_3550_cmn_ag.h#23 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_common.h"



typedef enum
{
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PA_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PA_RANGE,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_ASM_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_TUNER_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_TX_TX_RF_ON0,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_TX_RX_RF_ON0,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_ASM_TRIGGER,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_TX_TX_TRIGGER,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_OFF_TX_RX_TX_TRIGGER,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PA_TRIGGER,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_OFF_TX_RX_TX_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_TX_TX_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TIMING_PAPM_MULTISLOT_CTL,
  RFC_WTR3925_CHILE_UL_DLCA_3550_PA_ON_10,
  RFC_WTR3925_CHILE_UL_DLCA_3550_PA1_R2,
  RFC_WTR3925_CHILE_UL_DLCA_3550_PA0_R1,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_01,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_09,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_02,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_11,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_04,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_17,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_15,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_05,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_20,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_06,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_14,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_07,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_16,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_08,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RF_PATH_SEL_21,
  RFC_WTR3925_CHILE_UL_DLCA_3550_GPDATA0_0,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE1_CLK,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE1_DATA,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE2_CLK,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE2_DATA,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE4_CLK,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE4_DATA,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE5_CLK,
  RFC_WTR3925_CHILE_UL_DLCA_3550_RFFE5_DATA,
  RFC_WTR3925_CHILE_UL_DLCA_3550_INTERNAL_GNSS_BLANK,
  RFC_WTR3925_CHILE_UL_DLCA_3550_INTERNAL_GNSS_BLANK_CONCURRENCY,
  RFC_WTR3925_CHILE_UL_DLCA_3550_TX_GTR_TH,
  RFC_WTR3925_CHILE_UL_DLCA_3550_PA_IND,
  RFC_WTR3925_CHILE_UL_DLCA_3550_SIG_NUM,
  RFC_WTR3925_CHILE_UL_DLCA_3550_SIG_INVALID,
}wtr3925_chile_ul_dlca_3550_sig_type;



#ifdef __cplusplus

#include "rfc_common_data.h"

class rfc_wtr3925_chile_ul_dlca_3550_cmn_ag:public rfc_common_data
{
  public:
    uint32 sig_info_table_get(rfc_signal_info_type **rfc_info_table);
    rfc_phy_device_info_type* get_phy_device_cfg( void );
    rfc_logical_device_info_type* get_logical_device_cfg( void );
    boolean get_logical_path_config(rfm_devices_configuration_type* dev_cfg);
    const rfm_devices_configuration_type* get_logical_device_properties( void );
    boolean get_cmn_properties(rfc_cmn_properties_type **ptr);
    static rfc_common_data * get_instance(rf_hw_type rf_hw);
    boolean rfc_get_remapped_device_info
    (
      rfc_cal_device_remap_info_type *source_device_info,
      rfc_cal_device_remap_info_type *remapped_device_info
    );
  protected:
    rfc_wtr3925_chile_ul_dlca_3550_cmn_ag(rf_hw_type rf_hw);
};

#endif   /*  __cplusplus  */


#ifdef __cplusplus
}
#endif



#endif



