
#ifndef RFC_MSM_SIGNAL_INFO_AG
#define RFC_MSM_SIGNAL_INFO_AG


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.10.211 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2014 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/rfc_tabasco.mpss/2.0/target/msm8909/inc/rfc_msm_signal_info_ag.h#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_msm_typedef.h" 



/* Revision: V5.10.211 */
#define RFC_ENCODED_REVISION 0x050A00D3 

#define RFC_SIG_LIST_END 0xFFFF

#define RFC_MSM_SIG_INFO_REV 0x000A0000



typedef enum
{
  RFC_MSM_SIG_NUM,
  RFC_MSM_SIG_INVALID = 0xFFFFFFFF,
}rfc_msm_signal_type;


extern uint32 rfc_msm_sig_info_table_get(rfc_msm_signal_info_type **msm_info_table);

typedef enum
{
  LTE_TX_ON_UL_SF_SMPS_PGM,
  LTE_TX_ON_UL_SF_PA_CTL_ON,
  LTE_TX_ON_QPOET_NonTr_To_QPOET_TR,
  LTE_TX_ON_PRACH_OR_SRS_ONLY_PA_CTL_ON,
  LTE_TX_ON_PRACH_OR_SRS_ONLY_SMPS_PGM,
  LTE_TX_CONT_UL_SF_PA_CTL_ON,
  LTE_TX_CONT_UL_SF_SMPS_PGM,
  LTE_TX_CONT_POST_UE_SRS_PA_CTL_ON,
  LTE_TX_CONT_POST_UE_SRS_SMPS_PGM,
  LTE_TX_CONT_PRE_UE_SRS_PA_CTL_ON,
  LTE_TX_CONT_PRE_UE_SRS_SMPS_PGM,
  LTE_TX_OFF_NULL_SF_PA_CTL_OFF,
  LTE_TX_OFF_CELL_SRS_PA_CTL_OFF,
  LTE_TX_SMPS_EN_LOW,
  LTE_TX_SMPS_EN_HIGH,
  LTE_TX_ON_UL_SF_ASM_TX,
  LTE_TX_ON_PRACH_OR_SRS_ONLY_ASM_TX,
  LTE_TX_OFF_NULL_SF_ASM_RX,
  LTE_TX_OFF_CELL_SRS_ASM_RX,
  RFC_TIMING_PARAMS_NUM
}rfc_timing_param_type;


#ifdef __cplusplus
}
#endif



#endif


