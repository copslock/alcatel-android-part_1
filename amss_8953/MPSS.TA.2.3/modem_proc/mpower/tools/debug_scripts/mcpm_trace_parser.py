#  ====================================================================================================================
#
#  Copyright (c) 2014 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ====================================================================================================================
#
#  $Header: //components/rel/mpower.mpss/7.3/tools/debug_scripts/mcpm_trace_parser.py#1 $
#
#  when       who     what, where, why 
#  --------   ---     -------------------------------------------------------------------------------------------------
#  09/11/14   sr      Updated script to remove unwanted trace events from being displayed.
#  09/05/14   sr      Updated script to perform a stricter check for data3 parameter.
#  08/28/14   sr      Updated script to display output with MCPM top lock removed.
#  08/21/14   sr      Added support to display multithreaded events correctly.
#  07/21/14   sr      Added support to display unaccounted time for all trace events.
#  06/24/14   sr      Initial version.
# 
#  ====================================================================================================================
# Script for parsing mcpm_trace variable
# Usage:
# Open command prompt window.
# Go to the directory where mcpm_trace_parser.py is located.
# Syntax:
#   mcpm_trace_parser.py <input file> <output directory>
#   <input file>:                   Input text file containing output of 'mcpm_trace' variable from Trace32.
#   <output directory>:             Directory where output file needs to be stored. If nothing is specified, output
#                                   will be stored at C:\Temp
# Output:
#   mcpm_profiling_results.txt:     Stored in the same directory from where syntax is executed.
# Example:
#   mcpm_trace_parser.py trace.txt C:\Dropbox\Bolt\results
#######################################################################################################################

# Import standard modules.
import sys
import re
import os.path


# Class for storing information of each entry in mcpm_trace variable.
class Trace:
    def __init__(self, line):
        # Store the trace_type info.
        self.trace_type = re.findall(r'(?<=\btrace_type =\s)(\w+)', line)
        # Store the request_type info.
        self.request_type = re.findall(r'(?<=\brequest_type =\s)(\w+)', line)
        # Store the timestamp info.
        self.ts = re.findall(r'(\d+)(?!.*\d)', line)
        # Store the mcvs_req_type info.
        self.data3 = re.findall(r'(?<=\bdata3 =\s)(\d+)', line)


# Function to return event_type for a trace_type input data.
def get_event_type(trace_type):
    event_type = ''
    is_mcpm_event = re.search('MCPM', trace_type)
    if is_mcpm_event is not None:
        # If input data is MCPM_START_REQ or MCPM_END_REQ, return MCPM as event_type.
        event_type = 'MCPM'
    else:
        # For all other input data, remove _START/_END from input data to get event_type.
        is_start_evt = re.search('START', trace_type)
        if is_start_evt is not None:
            event = re.split('_START', trace_type)
            event_type = event[0]
        is_end_evt = re.search('END', trace_type)
        if is_end_evt is not None:
            event = re.split('_END', trace_type)
            event_type = event[0]
    return event_type


# Main function.
def main():
    if len(sys.argv) == 3:
        # Use user-specified output directory if provided.
        parent_dir = sys.argv[2]
    else:
        # If output directory is not provided, store output in default location.
        parent_dir = 'C:\Temp'

    # Open output file for writing.
    mcpm_output_file = os.path.join(parent_dir, 'mcpm_profiling_results.txt')
    g = open(mcpm_output_file, 'w')

    # Initialize variables and lists.
    trace_obj_list = []
    trace_obj_list_mcpm = []
    trace_obj_list_mcvs = []
    mcpm_start_times = []
    mcpm_end_times = []
    mcvsconfig_start_times = []
    mcvsconfig_end_times = []
    mcvsrelease_start_times = []
    mcvsrelease_end_times = []
    mcpm_mcvs_start_requests = []
    mcpm_times = []
    mcvsconfig_times = []
    mcvsrelease_times = []

    # Open input file for reading.
    f = open(sys.argv[1], 'r')

    # Create list of all trace objects containing MCPM and MCVS requests in input file.
    for line in f:
        trace_obj = Trace(line)
        cond = trace_obj.trace_type != [] and trace_obj.ts[0] != '0'
        if cond is True:
            if trace_obj.data3[0] != '1' and trace_obj.data3[0] != '2' and trace_obj.data3[0] != '3' and \
                    trace_obj.data3[0] != '4':
                trace_obj_list_mcpm.append(trace_obj)
                print("mcpm ", line )
                print("trace_obj ",trace_obj.data3[0])
            else:
                trace_obj_list_mcvs.append(trace_obj)
                print("mcvs ", line )
                print("trace_obj ",trace_obj.data3[0])

    # Total number of entries present in input file.
    trace_obj_list_mcpm_len = len(trace_obj_list_mcpm)
    trace_obj_list_mcvs_len = len(trace_obj_list_mcvs)
    # Sort the entries in trace_obj_list_mcpm w.r.t. timestamp.
    trace_obj_list_mcpm = sorted(trace_obj_list_mcpm, key=lambda trace: trace.ts[0])
    trace_obj_list_mcvs = sorted(trace_obj_list_mcvs, key=lambda trace: trace.ts[0])

    # List of all timestamps for MCPM events.
    count_mcpm_list = 0
    while True:
        if trace_obj_list_mcpm[count_mcpm_list].trace_type[0] == 'MCPM_START_REQ':
            # Store the timestamp for MCPM_START_REQ events along with corresponding request_type.
            trace_obj_start = trace_obj_list_mcpm[count_mcpm_list]
            mcpm_start_times.append(trace_obj_start.ts[0])
            count_per_mcpm_request = count_mcpm_list
            while True:
                trace_obj_end = trace_obj_list_mcpm[count_per_mcpm_request]
                # Store the timestamp for MCPM_END_REQ events for the current request_type.
                if trace_obj_end.trace_type[0] == 'MCPM_END_REQ' and trace_obj_end.request_type[0] == \
                        trace_obj_start.request_type[0]:
                    mcpm_end_times.append(trace_obj_end.ts[0])
                    break
                count_per_mcpm_request += 1
                if count_per_mcpm_request == len(trace_obj_list_mcpm):
                    break
        count_mcpm_list += 1
        if count_mcpm_list == len(trace_obj_list_mcpm):
            break

    # Remove outliers from the list of start and end times and obtain list of MCPM event times.
    if len(mcpm_start_times) > 0 and len(mcpm_end_times) > 0:
        if int(mcpm_end_times[0]) < int(mcpm_start_times[0]):
            mcpm_start_times.remove(mcpm_start_times[0])
            mcpm_end_times.remove(mcpm_end_times[0])
    mcpm_times = zip(mcpm_start_times, mcpm_end_times)

    # List of all timestamps for MCVS_CONFIG or MCVS_RELEASE events.
    for trace_obj in trace_obj_list_mcvs:
        if trace_obj.trace_type[0] == 'MCVS_CONFIG_START':
            # Store the timestamp only.
            mcvsconfig_start_times.append(trace_obj.ts[0])
        elif trace_obj.trace_type[0] == 'MCVS_RELEASE_START':
            mcvsrelease_start_times.append(trace_obj.ts[0])
        elif trace_obj.trace_type[0] == 'MCVS_CONFIG_END':
            mcvsconfig_end_times.append(trace_obj.ts[0])
        elif trace_obj.trace_type[0] == 'MCVS_RELEASE_END':
            mcvsrelease_end_times.append(trace_obj.ts[0])

    # Remove outliers from the list of start and end times and obtain list of MCVS_CONFIG and MCVS_RELEASE event
    # times.
    if len(mcvsconfig_start_times) > 0 and len(mcvsconfig_end_times) > 0:
        if int(mcvsconfig_end_times[0]) < int(mcvsconfig_start_times[0]):
            mcvsconfig_end_times.remove(mcvsconfig_end_times[0])
        if len(mcvsconfig_start_times) - len(mcvsconfig_end_times) == 1:
            mcvsconfig_start_times.remove(mcvsconfig_start_times[-1])
    mcvsconfig_times = zip(mcvsconfig_start_times, mcvsconfig_end_times)

    if len(mcvsrelease_start_times) > 0 and len(mcvsrelease_end_times) > 0:
        if int(mcvsrelease_end_times[0]) < int(mcvsrelease_start_times[0]):
            mcvsrelease_end_times.remove(mcvsrelease_end_times[0])
        if len(mcvsrelease_start_times) - len(mcvsrelease_end_times) == 1:
            mcvsrelease_start_times.remove(mcvsrelease_start_times[-1])
    mcvsrelease_times = zip(mcvsrelease_start_times, mcvsrelease_end_times)

    mcpm_mcvs_times = mcpm_times + mcvsconfig_times + mcvsrelease_times

    # Sort the list in order of start time.
    mcpm_mcvs_times = sorted(mcpm_mcvs_times, key=lambda times: times[0])
    mcpm_mcvs_start_times = [start_time for (start_time, end_time) in mcpm_mcvs_times]
    mcpm_mcvs_end_times = [end_time for (start_time, end_time) in mcpm_mcvs_times]

    # Obtain the combined list of MCPM and MCVS trace objects.
    trace_obj_list_combined = trace_obj_list_mcpm + trace_obj_list_mcvs

    # Sort the entries in trace_obj_list_combined w.r.t. timestamp.
    trace_obj_list_combined = sorted(trace_obj_list_combined, key=lambda trace: trace.ts[0])

    # Obtain the MCPM request or tech corresponding to each valid start time.
    count = 0
    for trace_obj in trace_obj_list_combined:
        if trace_obj.ts[0] == mcpm_mcvs_times[count][0]:
            is_mcvs_request = re.search('MCVS', trace_obj.trace_type[0])
            if is_mcvs_request is not None:
                # Tech corresponding to current MCVS request.
                tech = re.split('_', trace_obj.request_type[0])
                mcpm_mcvs_start_requests.append(tech[1])
            else:
                mcpm_mcvs_start_requests.append(trace_obj.request_type[0])
            count += 1
            if count == len(mcpm_mcvs_times):
                break

    # Obtain the list of tuples, each tuple containing start time, end time, corresponding MCPM request and flag to
    # check if it is a MCVS request or not.
    mcpm_times_info = zip(mcpm_mcvs_start_times, mcpm_mcvs_end_times, mcpm_mcvs_start_requests)

    # Display output.
    prev_evt_start = 0
    prev_evt_end = 0
    unacc_time = 0

    # Counter to keep track of position of current MCPM/MCVS request.
    count_mcpm = 0
    count_mcvs = 0
    count = 0

    for info in mcpm_times_info:
        # Check if current info is a MCVS info.
        is_mcpm_info = re.search('MCPM', info[2])       
        if is_mcpm_info is None:
            trace_obj_list = trace_obj_list_mcvs
            count = count_mcvs
            trace_obj_list_len = trace_obj_list_mcvs_len
        else:
            trace_obj_list = trace_obj_list_mcpm
            count = count_mcpm
            trace_obj_list_len = trace_obj_list_mcpm_len

        # Increment 'count' till the current MCPM_START_REQ is reached.
        while trace_obj_list[count].ts[0] != info[0]:
            count += 1

        # Initialize list for storing all instances of event_type for current pair of MCPM_START_REQ and MCPM_END_REQ.
        events = []
        # Counter to keep track of position of current event_type
        count_per_info = count
        space_count = 40
        inner_events_exist = False
        inner_events_end = []
        inner_events_end_ts = []
        start_count = 0
        end_count = 0
        rm_start_count = 0
        # Display MCPM request type or tech for MCVS requests.
        g.write(info[2])
        g.write('\n')
        # Check if mcpm_request_type for current info is a SLEEP request.
        is_sleep_request = re.search('SLEEP', info[2])
        # Display unaccounted durations for non SLEEP and non MCVS requests only.
        if is_sleep_request is None:
            g.write('\nEVENT\t\t\t\t\tDURATION(usecs)\t\t\t\t\tUNACCOUNTED DURATION(usecs)\n\n')
        else:
            g.write('\nEVENT\t\t\t\t\tDURATION(usecs)\n\n')
        while True:
            # If current trace_obj is not a MCVS request and request_type matches the desired mcpm_request_type,
            # execute below code.
            if trace_obj_list[count_per_info].data3[0] != '1' and trace_obj_list[count_per_info].data3[0] != '2' and \
                    trace_obj_list[count_per_info].data3[0] != '3' and trace_obj_list[count_per_info].data3[0] != '4':
                compare_value = trace_obj_list[count_per_info].request_type[0]
            else:
                tech = re.split('_', trace_obj_list[count_per_info].request_type[0])
                compare_value = tech[1]
            if compare_value == info[2]:
                event_type = get_event_type(trace_obj_list[count_per_info].trace_type[0])
                # If current event_type has not been printed already, execute the below statements.
                if not event_type in events:
                    # Check if current mcpm_request_type is a SLEEP request.
                    is_sleep_request = re.search('SLEEP', trace_obj_list[count_per_info].request_type[0])
                    events.append(event_type)
                    # Obtain the duration for event_type inside the current MCPM request.
                    if event_type == 'MCPM' or event_type == 'MCVS_CONFIG' or event_type == 'MCVS_RELEASE':
                        event_type_duration = str((int(info[1]) - int(info[0])) * 10 / 192)
                        start_count = count_per_info
                        start_time = int(info[0])
                        end_time = int(info[1])
                        count_per_event_type = count_per_info
                        while True:
                            if trace_obj_list[count_per_event_type].ts[0] == info[1]:
                                end_count = count_per_event_type
                                break
                            else:
                                count_per_event_type += 1
                    else:
                        count_per_event_type = count_per_info
                        start_time = trace_obj_list[count_per_event_type].ts[0]
                        start_count = count_per_event_type
                        end_time = ''
                        end_req = ''
                        end_req = end_req.join([event_type, '_END'])
                        while True:
                            trace_obj_per_event = trace_obj_list[count_per_event_type]
                            if trace_obj_per_event.trace_type[0] == end_req:
                                if trace_obj_per_event.data3[0] != '1' and trace_obj_per_event.data3[0] != '2' and \
                                        trace_obj_per_event.data3[0] != '3' and trace_obj_per_event.data3[0] != '4':
                                    # For MCPM requests, check if event_type end corresponds to current request_type.
                                    if trace_obj_per_event.request_type[0] == info[2]:
                                        end_time = trace_obj_per_event.ts[0]
                                        end_count = count_per_event_type
                                        break
                                    else:
                                        count_per_event_type += 1
                                else:
                                    end_time = trace_obj_per_event.ts[0]
                                    end_count = count_per_event_type
                                    break
                            else:
                                count_per_event_type += 1
                        # Store the start count of RM_MUX_COMMIT event for SLEEP requests.
                        if event_type == 'RM_MUX_COMMIT' and is_sleep_request is not None:
                            rm_start_count = start_count
                        # Calculate the duration for current event_type.
                        event_type_duration = str((int(end_time) - int(start_time)) * 10 / 192)

                    # If the previous event_type displayed contains inner event_types as well, increment the spacing.
                    if inner_events_exist and start_count < inner_events_end[-1]:
                        space_count += 5
                        unacc_time = str((int(start_time) - int(prev_evt_start)) * 10 / 192)
                        inner_events_exist = False
                    else:
                        unacc_time = str((int(start_time) - int(prev_evt_end)) * 10 / 192)
                    # For SLEEP requests, don't apply regular indentation logic for SRR resources.
                    if (is_sleep_request is not None) and (0 < rm_start_count < start_count):
                        if event_type == 'MPLL0':
                            event_type_display = ''.ljust(55-40)+event_type.ljust(55)
                        else:
                            event_type_display = ''.ljust(50-40)+event_type.ljust(50)
                    else:
                        event_type_display = ''.ljust(space_count-40)+event_type.ljust(space_count)
                    # Display current event_type and duration.
                    g.write(event_type_display)
                    g.write(event_type_duration)
                    # Store the values for current event times for calculating unaccounted time between current event
                    # and next event.
                    prev_evt_start = start_time
                    prev_evt_end = end_time
                    # Current event_type to be displayed contains inner event_types, store the values for inner
                    # event_type variables.
                    if end_count - start_count > 1:
                        # Check if inner event_types contain any MCPM_START_REQ from another thread.
                        tmp_count = start_count
                        for itn in range(start_count+1,end_count):
                            if (trace_obj_list[itn].trace_type[0] != 'MCPM_START_REQ'):
                                # For MCPM requests, check against request_type. For MCVS requests, check against tech
                                # type.
                                if (is_mcpm_info is not None) and (trace_obj_list[itn].request_type[0] == info[2]):
                                    tmp_count += 1
                                else:
                                    tech_request_type = re.split('_', trace_obj_list[itn].request_type[0])
                                    if tech_request_type[1] == info[2]:
                                        tmp_count += 1
                        # Don't mark inner_events_exist as True if there are no valid inner events.
                        if tmp_count != start_count:
                            inner_events_exist = True
                            inner_events_end.append(end_count)
                            inner_events_end_ts.append(end_time)
                    # If current event_type is the last inner event_type, decrement space_count and initialize the
                    # values for inner event_type variables to default values.
                    if len(inner_events_end) > 0:
                        if inner_events_end[-1] - end_count == 1:
                            if (is_sleep_request is None) and not (event_type == 'MCPM') and not (event_type == 'MCVS_C\
                                ONFIG') and not (event_type == 'MCVS_RELEASE'):
                                result = int(unacc_time)+((int(inner_events_end_ts[-1]) - int(end_time)) * 10 / 192)
                                unacc_time = str(result)
                                prev_evt_end = int(inner_events_end_ts[-1])
                            inner_events_end_ts.pop()
                            inner_events_end.pop()
                            space_count -= 5
                    # Display unaccounted time
                    if (is_sleep_request is None) and not (event_type == 'MCPM') and not (event_type == 'MCVS_CONFIG') \
                            and not (event_type == 'MCVS_RELEASE'):
                        unacc_time_display = str(unacc_time).rjust(100-len(event_type_display)-len(event_type_duration))
                        g.write(unacc_time_display)
                    g.write('\n')
            count_per_info += 1
            # If counter to keep track of position of current event_type has reached end of mcpm_trace, go to the next
            # MCPM request.
            if count_per_info == trace_obj_list_len:
                break
            # If there are no more pending event_types in current MCPM request, go to next MCPM request.
            if trace_obj_list[count_per_info].ts[0] == info[1]:
                break
        # Check if mcpm_request_type for current info is a SLEEP request.
        is_sleep_request = re.search('SLEEP', info[2])
        # Display unaccounted time b/w last event_type and end of MCPM request for non SLEEP and non MCVS requests.
        if is_sleep_request is None:
            unacc_time = str((int(info[1]) - int(prev_evt_end)) * 10 / 192)
            unacc_time_display = str(unacc_time).rjust(100)
            g.write(unacc_time_display)

        g.write('\n___________________________________________________________________________________________________')
        g.write('_____________________________\n\n')
        if is_mcpm_info is None:
            count_mcvs = count
        else:
            count_mcpm = 0

    f.close()
    g.close()

# Call main() function to begin the program
if __name__ == '__main__':
    main()