#===============================================================================
#
# Multimode SD SCons 
#
# GENERAL DESCRIPTION
#    SCons build script
#
# Copyright (c) 2011 by Qualcomm Technologies INCORPORATED.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/mmcp.mpss/7.3.1/mmode/sd/build/mmode_sd.scons#1 $
#  $DateTime: 2016/03/24 12:47:03 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#06/05/12   ns      Added ECALL in Restricted APIs
#03/13/12   ds      Added WCDMA and AUDIO APIs
#09/29/11   gm      RC init changes
#09/15/10   pm      Initial file
#
#===============================================================================
# from glob import glob
# from os.path import join, basename

Import('env')

if 'USES_WPLT' in env:
   Return()

# --------------------------------------------------------------------------- #
# Turn off/on debug if requested 			      
# --------------------------------------------------------------------------- # 
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG = "-g")

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = '../src'

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
    'MSG_BT_SSID_DFLT=MSG_SSID_SD',
])

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
# Construct the list of source files by looking for *.c
#SD_C_SOURCES = ['${BUILDPATH}/' + basename(fname)
#                 for fname in glob(join(env.subst(SRCPATH), '*.c'))]


SD_C_SOURCES = [
	'${BUILDPATH}/prl.c',
	'${BUILDPATH}/prldiag.c',
	'${BUILDPATH}/puzl.c',
	'${BUILDPATH}/sd.c',
	'${BUILDPATH}/sdnv.c',
	'${BUILDPATH}/sdsr.c',
	'${BUILDPATH}/sdss.c',
	'${BUILDPATH}/sdssscr.c',
	'${BUILDPATH}/sdprl.c',
	'${BUILDPATH}/sdefs.c',
	'${BUILDPATH}/sdmmss.c',
	'${BUILDPATH}/sdtask.c',
	'${BUILDPATH}/sdcmd.c',
]

# Add our library to the ModemApps image
env.AddLibrary( ['MODEM_MODEM', 'MOB_MMODE'], '${BUILDPATH}/mmode_sd', [SD_C_SOURCES] )


# Build image for which this task belongs
RCINIT_SD = ['MODEM_MODEM', 'MOB_MMODE']

# RC Init Function Dictionary
RCINIT_INIT_SD = {
	    'sequence_group'      : env.subst('$MODEM_UPPERLAYER'),
	    'init_name'           : 'sd_init',
	    'init_function'       : 'sd_init_before_app_task_start',
      'policy_optin'        : ['default', 'ftm', ],
    }
    
# RC Init Task Dictionary
RCINIT_TASK_SD = {
      'thread_name'         : 'sd',
      'sequence_group'      : env.subst('$MODEM_UPPERLAYER'),
      'stack_size_bytes'    : env.subst('$SD_STKSZ'),
      'priority_amss_order' : 'SD_PRI_ORDER',
      'stack_name'          : 'sd_stack',
      'thread_entry'        : 'sd_task',
      'tcb_name'            : 'sd_tcb',
      'cpu_affinity'        : env.subst('$MODEM_CPU_AFFINITY'),
      'policy_optin'        : ['default', 'ftm', ],
    }

# Add init function to RCInit
if 'USES_MODEM_RCINIT' in env:
  env.AddRCInitFunc(RCINIT_SD, RCINIT_INIT_SD)
  env.AddRCInitTask(RCINIT_SD, RCINIT_TASK_SD)

