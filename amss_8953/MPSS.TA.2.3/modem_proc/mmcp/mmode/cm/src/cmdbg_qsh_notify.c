/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

           C A L L   M A N A G E R   QSH D E B U G   M O D U L E

GENERAL DESCRIPTION
  This module contains the debug related functionality of call manager.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


Copyright (c) 2015 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/mmcp.mpss/7.3.1/mmode/cm/src/cmdbg_qsh_notify.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/04/15   SKK      Initial release.

===========================================================================*/

/*lint -save -e656 -e641
** Turn off enum to int / operationuses compatible enums
*/

/**--------------------------------------------------------------------------
** Includes
** --------------------------------------------------------------------------
*/
#include "mmcp_variation.h"

#ifdef FEATURE_QSH_EVENT_NOTIFY_HANDLER
#error code not present
#endif //FEATURE_QSH_EVENT_NOTIFY_HANDLER
