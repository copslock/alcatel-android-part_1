#ifndef CM_QSH_INT_H
#define CM_QSH_INT_H
/*===========================================================================

     C A L L   M A N A G E R   D E B U G   H E A D E R   F I L E

DESCRIPTION
  This header file contains debug macros and definitions necessary to
  interface with cmdbg_qsh_notify.c


Copyright (c) 2015 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/



/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/mmcp.mpss/7.3.1/api/cm_qsh_int.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/04/15   SKK      Initial release.

===========================================================================*/
#include "mmcp_variation.h"

#ifdef FEATURE_QSH_EVENT_NOTIFY_HANDLER
#error code not present
#endif /* FEATURE_QSH_EVENT_NOTIFY_HANDLER */
#endif /* CM_QSH_INT_H */

