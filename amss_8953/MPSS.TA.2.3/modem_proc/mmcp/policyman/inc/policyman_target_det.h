#ifndef _POLICYMAN_TARGET_DET_H_
#define _POLICYMAN_TARGET_DET_H_

/**
  @file policyman_target_det.h

  @brief  Determination of whether we are building for target or Windows (unit
          test environment).
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //components/rel/mmcp.mpss/7.3.1/policyman/inc/policyman_target_det.h#1 $
  $DateTime: 2016/03/24 12:47:03 $
  $Author: pwbldsvc $
*/

#endif /* _POLICYMAN_TARGET_DET_H_ */
