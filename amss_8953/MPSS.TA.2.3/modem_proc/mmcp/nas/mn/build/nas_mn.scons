#===============================================================================
#
# NAS MN SCons 
#
# GENERAL DESCRIPTION
#    SCons build script
#
# Copyright (c) 2010 - 2011 by Qualcomm Technologies INCORPORATED.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/mmcp.mpss/7.3.1/nas/mn/build/nas_mn.scons#1 $
#  $DateTime: 2016/03/24 12:47:03 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 04/12/11   pm      Set MSG_BT_SSID_DFLT to map legacy messages to your SSID name
# 09/15/10   pm      Initial file
#
#===============================================================================
#from glob import glob
#from os.path import join, basename

Import('env')

if 'USES_GSM' not in env and ('USES_UMTS' not in env or 'USES_WPLT' in env):
   Return()

# --------------------------------------------------------------------------- #
# Turn off/on debug if requested 			      
# --------------------------------------------------------------------------- # 
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG = "")

if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG = "-g")

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = '../src'

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
    'MSG_BT_SSID_DFLT=MSG_SSID_NAS_MN',
])

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
#MN_C_SOURCES = ['${BUILDPATH}/' + basename(fname)
#                 for fname in glob(join(env.subst(SRCPATH), '*.c'))]

MN_C_SOURCES = [
	'${BUILDPATH}/mn_cc_msg_util.c',
	'${BUILDPATH}/mn_cc_processing.c',
	'${BUILDPATH}/mn_get_cnm_cc_msgs.c',
	'${BUILDPATH}/mn_get_cnm_ss_msgs.c',
	'${BUILDPATH}/mn_handle_test.c',
	'${BUILDPATH}/mn_initialise_mn_data.c',
	'${BUILDPATH}/mn_process_cm_cc_msgs.c',
	'${BUILDPATH}/mn_process_cm_ss_cc_msgs.c',
	'${BUILDPATH}/mn_process_cm_ss_msgs.c',
	'${BUILDPATH}/mn_process_cnm_cc_msgs.c',
	'${BUILDPATH}/mn_process_cnm_sms_msgs.c',
	'${BUILDPATH}/mn_process_smrl_msgs.c',
	'${BUILDPATH}/mn_put_cm_cc_msgs.c',
	'${BUILDPATH}/mn_put_cm_ss_msgs.c',
	'${BUILDPATH}/mn_put_cnm_cc_msgs.c',
	'${BUILDPATH}/mn_put_cnm_sms_msgs.c',
	'${BUILDPATH}/mn_put_cnm_ss_msgs.c',
	'${BUILDPATH}/mn_put_uasms_msgs.c',
	'${BUILDPATH}/mn_route_cc_msgs.c',
	'${BUILDPATH}/mn_route_mn_msgs.c',
	'${BUILDPATH}/mn_route_sms_msgs.c',
	'${BUILDPATH}/mn_route_ss_msgs.c',
	'${BUILDPATH}/mn_send_message.c',
	'${BUILDPATH}/mn_sms_msg_util.c',
	'${BUILDPATH}/mn_sms_processing.c',
	'${BUILDPATH}/mn_ss_processing.c',
	'${BUILDPATH}/mn_ss_cc_processing.c',
	'${BUILDPATH}/mn_tch_connection.c',
	'${BUILDPATH}/mn_timers.c',
	'${BUILDPATH}/mnglobal.c',
	'${BUILDPATH}/mnutils.c',
]

# Add our library to the ModemApps image
env.AddLibrary( ['MODEM_MODEM'], '${BUILDPATH}/nas_mn', [MN_C_SOURCES] )

