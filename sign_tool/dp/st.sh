#!/bin/bash

if( test $# -le 2 ); then
    echo
    echo "./st.sh path project xmlfile"
    echo "For example ./st.sh ../ idol4s amss"
    echo "path 参数是amss工程路径或者android工程路径"
    echo "project 参数必须是android out/target/product/ 目录下的目录名， 这样才能找到aboot文件"
    echo "xmlfile 就是当前目录下CSCconfig_×.xml文件，你可以自己定义文件组合，目前这个参数支持all, amss, bootimage, lk, modem, rpm, tz, venus, wcnss"
    echo "如果签名时，没找到相关文件，你可以check这个脚本20行开始的那些文件路径定义"
    echo
    exit 254
fi

#######################################################################
#  File path define:                                                  #
#  If you can't find file, you can check these file paths in your code#
#######################################################################

platform=amss_8996

sbl1=$platform/boot_images/QcomPkg/Msm8996Pkg/Bin64/xbl.elf
pmic=$platform/boot_images/QcomPkg/Msm8996Pkg/Bin64/pmic.elf
MPRG8916=$platform/boot_images/QcomPkg/Msm8996Pkg/Bin64/prog_emmc_firehose_8996_ddr.elf

tz=$platform/trustzone_images/build/ms/bin/IADAANAA/tz.mbn
hyp=$platform/trustzone_images/build/ms/bin/IADAANAA/hyp.mbn
devcfg=$platform/trustzone_images/build/ms/bin/IADAANAA/devcfg.mbn

rpm=$platform/rpm_proc/build/ms/bin/AAAAANAAR/rpm.mbn

mba=$platform/modem_proc/build/ms/bin/8996.gen.prod/mba.mbn
qdsp6sw=$platform/modem_proc/build/ms/bin/8996.gen.prod/qdsp6sw.mbn


wcnss=$platform/wcnss_proc/build/ms/bin/8976/reloc/wcnss.mbn
dsp2=$platform/adsp_proc/adsp_proc/obj/qdsp6v5_ReleaseG/adsp.mbn

cmnlib=$platform/trustzone_images/build/ms/bin/IADAANAA/cmnlib.mbn
cmnlib64=$platform/trustzone_images/build/ms/bin/IADAANAA/cmnlib64.mbn
keymaster=$platform/trustzone_images/build/ms/bin/IADAANAA/keymaster.mbn
widevine=$platform/trustzone_images/build/ms/bin/IADAANAA/widevine.mbn
#cppf
sampleapp=$platform/trustzone_images/build/ms/bin/IADAANAA/cppf.mbn
#fingerpr
tzapps=$platform/trustzone_images/build/ms/bin/IADAANAA/tclapp.mbn
#alipay
sps=$platform/trustzone_images/build/ms/bin/IADAANAA/alipay2.mbn
#mdtp
sbl2=$platform/trustzone_images/build/ms/bin/IADAANAA/mdtp.mbn
#tqs 
sbl3=$platform/trustzone_images/build/ms/bin/IADAANAA/dhsecapp.mbn
isdbtmm=$platform/trustzone_images/build/ms/bin/IADAANAA/isdbtmm.mbn
efstar=$platform/trustzone_images/build/ms/bin/IADAANAA/qmpsecap.mbn
playready=$platform/trustzone_images/build/ms/bin/IADAANAA/securemm.mbn
synaf=$platform/trustzone_images/build/ms/bin/IADAANAA/synafp64.mbn
fingergprinter=$platform/trustzone_images/build/ms/bin/IADAANAA/fingerprint64.mbn
sampleapp64=$platform/trustzone_images/build/ms/bin/IADAANAA/smplap64.mbn
firehose=$platform/trustzone_images/build/ms/bin/IADAANAA/smplap32.mbn
hdcp2=$platform/trustzone_images/build/ms/bin/IADAANAA/gptest.mbn

slpi=$platform/slpi_proc/obj/qdsp6v5_ReleaseG/slpi.mbn

dgfx_microcode=vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8996/system/etc/firmware/
gfx_microcode=$dgfx_microcode/a530_zap.elf

dvenus=$platform/venus_proc/build/bsp/asic/build/PROD/mbn/reloc/signed/
venus=$dvenus/venus.mbn
appsbl=out/target/product/$2/emmc_appsboot.mbn

#boot=out/target/product/$2/newboot.img
#recovery=out/target/product/$2/newrecovery.img
#tar=out/target/product/$2/newstudy.tar
######################################################################
## File path define end                                              #
######################################################################

echo Clean output ...
rm -fr output/*

chmod 777 CSCconfig.xml
cp -f CSCconfig_$3.xml CSCconfig.xml

osbl1=output/sbl1/sbl1.mbn
omrpg=output/emmcbld/MPRG8916.mbn
otz=output/qsee/tz.mbn
ohyp=output/qhee/hyp.mbn
orpm=output/rpm/rpm.mbn
omba=output/mba/mba.mbn
oqdsp6sw=output/qdsp6sw/qdsp6sw.mbn
owcnss=output/wcnss/wcnss.mbn
odsp2=output/dsp2/dsp2.mbn

ocmnlib=output/cmnlib/cmnlib.mbn
okeymaster=output/keymaster/keymaster.mbn
owidevine=output/widevine/widevine.mbn
osampleapp=output/sampleapp/sampleapp.mbn
otzapps=output/tzapps/tzapps.mbn
osps=output/sps/sps.mbn
osbl2=output/sbl2/sbl2.mbn
osbl3=output/sbl3/sbl3.mbn
oisdbtmm=output/isdbtmm/isdbtmm.mbn
oplayready=output/playready/playready.mbn

ovenus=output/venus/venus.mbn
vf=output/venus/
oappsbl=output/appsbl/appsbl.mbn

opmic=output/pmic/pmic.mbn
oslpi=output/slpi/slpi.mbn
odevcfg=output/devcfg/devcfg.mbn
osynaf=output/synaf/synaf.mbn
ofingergprinter=output/fingergprinter/fingergprinter.mbn
osampleapp64=output/sampleapp64/sampleapp64.mbn
ogfx_microcode=output/gfx_microcode/gfx_microcode.mbn
gfx=output/gfx_microcode/

ocmnlib64=output/cmnlib64/cmnlib64.mbn
oefstar=output/efstar/efstar.mbn
ofirehose=output/firehose/firehose.mbn
ohdcp2=output/hdcp2/hdcp2.mbn


oboot=output/boot/boot.mbn
orecovery=output/recovery/recovery.mbn
otar=output/tar/tar.mbn



output=(osbl1 omrpg otz ohyp orpm omba oqdsp6sw owcnss odsp2 ocmnlib okeymaster owidevine osampleapp otzapps osps osbl2 osbl3 oisdbtmm oplayready ovenus oappsbl opmic oslpi odevcfg osynaf ofingergprinter osampleapp64 ogfx_microcode ocmnlib64 oefstar ofirehose ohdcp2)
input=(sbl1 MPRG8916 tz hyp rpm mba qdsp6sw wcnss dsp2 cmnlib keymaster widevine sampleapp tzapps sps sbl2 sbl3 isdbtmm playready venus appsbl pmic slpi devcfg synaf fingergprinter sampleapp64 gfx_microcode cmnlib64 efstar firehose hdcp2)

pil=pil-splitter.py

#saboot=out/target/product/$2/emmc_appsboot.mbn
#svenus=vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8916_32/system/etc/firmware/venus.mbn
#sboot=out/target/product/$2/boot.img
#srecovery=out/target/product/$2/recovery.img
#star=out/target/product/$2/tar.mbn

port=18952
flag=0
sip="172.16.11.210" #"127.0.0.1" #  
pro="\/8996"

case $2 in
    msm8996 | idol4s_cn | idol4s_tmo)
        pro="\/8996"
        IP=`ifconfig | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'` 
        echo "IP --> "$IP""
        [[ "$IP" =~ "172.16" ]] && port=18963

        echo "port, flag --> $port, $flag"
        
        ;;
    *)
        echo "Not support project $2"
        exit 251
        ;;
esac

case $3 in
    CU)
        qdsp6sw=modem_proc/mcfg/configs/mcfg_sw/generic/China/CU/Commercial/OpenMkt/mcfg_sw.mbn
        ;;

    CT)
        qdsp6sw=modem_proc/mcfg/configs/mcfg_sw/generic/China/CT/Commercial/OpenMkt/mcfg_sw.mbn
        ;;

    CMCC)
        qdsp6sw=modem_proc/mcfg/configs/mcfg_sw/generic/China/CMCC/Commercial/Volte_OpenMkt/mcfg_sw.mbn
        ;;
    dp)
        ./Client2
        chmod 777 -R output/
        echo
        echo "you can find your dp file in output/dp/dp.mbn tar file!!!!!"
        echo
        exit 0
        ;;
esac

sed -i -e s'/servport="[^"]*"/servport="'$port'"/' ./CSCconfig.xml

sed -i -e s'/project name="[^"]*"/project name="'$pro'"/' ./CSCconfig.xml

sed -i -e s'/servip="[^"]*"/servip="'$sip'"/' ./CSCconfig.xml

evenus=177
etar=177

i=0
j=0
for img in ${input[@]};
    do
        fl=$1/${!img}
        if test -e $fl; then
	    grep -r "$img path=\"" ./CSCconfig.xml >> /dev/null
	    if test $? -eq 0 ; then
		if test $img = gfx_microcode ; then
	    	    evenus=$j
                elif test $img = tar ; then
                    etar=$j
                fi            
            	sed -i -e s':'"$img"' path="[^"]*":'"$img"' path="'$fl'":' ./CSCconfig.xml  
		echo image $fl will be signed          
		of=${output[$i]}
		routput[$j]=${!of}
		rinput[$j]=$fl
		j=$(($j+1))
	    fi
        else
            echo "Can't find file $fl"
            fl=
            sed -i -e s':'"$img"' path="[^"]*":'"$img"' path="'$fl'":' ./CSCconfig.xml
        fi
        i=$(($i+1))
    done
if test $j -eq 0; then
    echo
    echo In this path $1, the sign tool can not find any image to sign, please check $1 and file path define!!!
    echo
    exit 255
fi

echo Sign start: $j $etar $evenus
sign(){
i=0
while( test $i -le 10 )
do
	i=$(($i+1))
	echo Sign count $i	
	./Client2
	if test $? -eq 0 ; then
		echo "Sign successfully"
		break
	fi
	
        if test $i -eq 2; then
                echo "Sign failed"
                exit 254
        fi

	sleep $i
	echo "Sign again"
        chmod -R 777 output/
        echo Clean output ...
        rm -fr output/*
        if test $flag -eq 1 ; then
          flag=0
          port=$(($port+1))
          #sed -i -e s'/servport="[^"]*"/servport="'$port'"/' ./CSCconfig.xml
          echo "port, flag --> $port, $flag"
        fi
done
}
sign
chmod -R 777 output/
i=0
while ( test $i -lt $j )
do
if test -e ${routput[$i]} ; then
    python elf.py ${routput[$i]}
    if( test $? -ne 0 ); then
      exit 252
    fi
    if test $i -eq $evenus ; then
        cp -fr $pil $gfx
        cd $gfx
        python $pil gfx_microcode.mbn a530_zap
        mv gfx_microcode.mbn a530_zap.elf
        rm -f $pil
	cd -
        echo "cp -fr $gfx/* $1/$dgfx_microcode"
	cp -fr $gfx/* $1/$dgfx_microcode
    elif test $i -eq $etar ; then
        cp -fr ${routput[$i]} tool/
        cd tool
        python efs_image_create.py -o tar_end.mbn efs_image_meta.bin tar.mbn
        cd -
        cp -fr tool/tar_end.mbn ${rinput[$i]}
    else
        cp -fr ${routput[$i]} ${rinput[$i]}
    fi
else 
    echo ${routput[$i]} is not exist and the image ${rinput[$i]} may sign error
    exit 253
fi
i=$(($i+1))
done

echo Sign done!!!

