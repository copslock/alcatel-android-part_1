#===============================================================================
#
# DAL CONFIG Lib
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/dev/tz.bf/4.0/abhianan.TZ.BF.4.0.DEVCFGFWK_HYPERVISOR/trustzone_images/core/dal/config/build/SConscript#1 $
#  $DateTime: 2014/09/18 12:22:25 $
#  $Author: abhianan $
#  $Change: 6620110 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when         who     what, where, why
# --------     ---     ---------------------------------------------------------
# 10/31/2014   aa      Create
#
#===============================================================================
Import('env')
import os
env = env.Clone()

#-------------------------------------------------------------------------------
# Load dal config builders
#-------------------------------------------------------------------------------
env.Tool('dalconfig_builder', toolpath = ['.'])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${DAL_ROOT}/config"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External paths, NOTE: DALConfig is a special case as it may require any
# ".h" file which may not be a public API
#-------------------------------------------------------------------------------

EXTERNAL_API = [
   'MODEM_PMIC',                  #pm.h
   'MODEM_API',
]
env.RequireExternalApi(EXTERNAL_API)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'HAL',
   'BUSES',
   'HWENGINES',
   'SYSTEMDRIVERS',
   'SYSTEMDRIVERS_DALCONFIG',
   'DEBUGTOOLS',
   'SERVICES',
   'APTTESTS',
   'KERNEL_MEMORYMANAGER',
   'KERNEL'
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
if env.GetUsesFlag('USES_DEVCFG') is True:
   DEVCFG_SOURCES = []
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG,
   {
      'devcfg_xml' : ['${BUILD_ROOT}/core/dal/config/tz/dalsystem_tz.xml']
   })
   
   dal_gen_src = ['${BUILDPATH}/DALConfig_tz.c', '${BUILDPATH}/devcfg_data_tz.c']
   devcfg_gen_xml = '${BUILDPATH}/DevCfg_tz_master.xml'
   devcfg_gen_env = '${BUILDPATH}/DevCfg_tz_env.txt'
   DALConfig_xml = env.DevcfgBuilder([devcfg_gen_xml,devcfg_gen_env], None, DATA_COLLECT=DEVCFG_IMG)
   DALConfig_src = env.DALConfigSrcBuilder(dal_gen_src, [DALConfig_xml[0]])
   DEVCFG_SOURCES.extend(DALConfig_src)
   if (os.path.exists(env.RealPath(devcfg_gen_env))) :
      lines = [line.strip() for line in open(env.RealPath(devcfg_gen_env), 'r')]
      env.get('CPPPATH').extend(lines) 
      
   # Create DALModDir file
   dal_mod_src = ['${BUILDPATH}/DALModDir_tz.c']
   DALModDir_src = env.DALModDirSrcBuilder(dal_mod_src, [DALConfig_xml[0]])
   DEVCFG_SOURCES.extend(DALModDir_src)

   dalconfig_obj = env.Object(DEVCFG_SOURCES)

   # add explicit dependency so SCons know that when ever XML changes so does the
   # objs have to get re-compiled
   env.Depends (dalconfig_obj, DALConfig_xml)

   # Add Libraries to image
   env.AddLibrary(DEVCFG_IMG, '${BUILDPATH}/DALConfig_tz', dalconfig_obj)
   
   # Force linker to add pad file in TZ Code Image to reserve section
   # devcfg_pad_obj = env.Object(['${BUILD_ROOT}/core/dal/config/src/devcfg_pad.c'])
   # env.AddObjsToImage(DEVCFG_IMG, devcfg_pad_obj)
