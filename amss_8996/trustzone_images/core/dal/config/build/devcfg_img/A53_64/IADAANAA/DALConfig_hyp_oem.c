#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "dalconfig.h"
#include "dalconfig.h"
#include "customer.h"
#include "customer.h"
#include "custiadaanaaa.h"
#include "custiadaanaaa.h"
#include "targiadaanaaa.h"
#include "custiadaanaaa.h"
#include "custiadaanaaa.h"
#include "custremoteapis.h"
#include "custiadaanaaa.h"
#include "custtarget.h"
#include "custiadaanaaa.h"
#include "custsdcc.h"
#include "custiadaanaaa.h"
#include "custsurf.h"
#include "custiadaanaaa.h"
#include "custdiag.h"
#include "custiadaanaaa.h"
#include "custefs.h"
#include "custiadaanaaa.h"
#include "custpmic.h"
#include "custiadaanaaa.h"
#include "custsio_8660.h"
#include "custiadaanaaa.h"
#include "custsec.h"
#include "custsec.h"
#include "custsfs.h"
#include "custsec.h"
#include "custiadaanaaa.h"
#include "customer.h"
#include "dalconfig.h"
#include "QupACCommonIds.h"
#include "comdef.h"
#include "com_dtypes.h"
#include "target.h"
#include "customer.h"
#include "custremoteapis.h"
#include "custtarget.h"
#include "custsdcc.h"
#include "custsurf.h"
#include "custdiag.h"
#include "custefs.h"
#include "custpmic.h"
#include "custsio_8660.h"
#include "custsec.h"
#include "custsfs.h"
#include "armasm.h"
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#ifndef DAL_CONFIG_IMAGE_APPS 
#define DAL_CONFIG_IMAGE_APPS 
#endif 
extern void * pm_hyp_spmi_cfg;
extern void * pm_hyp_spmi_cfg_sz;

const DALSYSPropStructTblType DALPROP_StructPtrs_hyp_oem[] =  {
	 {sizeof(void *), &pm_hyp_spmi_cfg},
	 {sizeof(void *), &pm_hyp_spmi_cfg_sz},
	{0, 0 } 
 };
const uint32 DALPROP_PropBin_hyp_oem[] = {

			0x00000818, 0x00000020, 0x000002c0, 0x000002e4, 0x000002e4, 
			0x00000001, 0x00000001, 0x000007e4, 0x49524550, 0x49204850, 
			0x50470044, 0x72204f49, 0x65676e61, 0x5f575200, 0x45434341, 
			0x4c5f5353, 0x00545349, 0x505f5349, 0x49535245, 0x4e455453, 
			0x45500054, 0x48504952, 0x534c425f, 0x55513150, 0x47525f50, 
			0x444e495f, 0x535f5845, 0x54524154, 0x52455000, 0x5f485049, 
			0x50534c42, 0x50555132, 0x5f47525f, 0x45444e49, 0x54535f58, 
			0x00545241, 0x49524550, 0x425f4850, 0x3150534c, 0x54524155, 
			0x5f47525f, 0x45444e49, 0x54535f58, 0x00545241, 0x49524550, 
			0x425f4850, 0x3250534c, 0x54524155, 0x5f47525f, 0x45444e49, 
			0x54535f58, 0x00545241, 0x5f435353, 0x50534c42, 0x5f505551, 
			0x495f4752, 0x5845444e, 0x4154535f, 0x53005452, 0x425f4353, 
			0x5550534c, 0x5f545241, 0x495f4752, 0x5845444e, 0x4154535f, 
			0x50005452, 0x50495245, 0x50475f48, 0x525f4f49, 0x4e495f47, 
			0x5f584544, 0x52415453, 0x554e0054, 0x45505f4d, 0x48504952, 
			0x534c425f, 0x4f435f50, 0x00534552, 0x5f4d554e, 0x50534c42, 
			0x4e494d5f, 0x524f4349, 0x4e005345, 0x535f4d55, 0x4d5f4353, 
			0x43494e49, 0x5345524f, 0x52455000, 0x5f485049, 0x425f5353, 
			0x3150534c, 0x534c425f, 0x41425f50, 0x4142204d, 0x50004553, 
			0x50495245, 0x53535f48, 0x534c425f, 0x425f3250, 0x5f50534c, 
			0x204d4142, 0x45534142, 0x43535300, 0x534c425f, 0x4c425f50, 
			0x425f5053, 0x42204d41, 0x00455341, 0x49524550, 0x535f4850, 
			0x4c425f53, 0x5f315053, 0x504d4142, 0x5f455049, 0x5f444953, 
			0x45444e49, 0x54535f58, 0x00545241, 0x49524550, 0x535f4850, 
			0x4c425f53, 0x5f325053, 0x504d4142, 0x5f455049, 0x5f444953, 
			0x45444e49, 0x54535f58, 0x00545241, 0x5f435353, 0x50534c42, 
			0x4d41425f, 0x45504950, 0x4449535f, 0x444e495f, 0x535f5845, 
			0x54524154, 0x52455000, 0x5f485049, 0x425f5353, 0x5f50534c, 
			0x554d4d53, 0x534e495f, 0x434e4154, 0x53530045, 0x4c425f43, 
			0x535f5053, 0x5f554d4d, 0x54534e49, 0x45434e41, 0x534c4200, 
			0x485f3150, 0x5f4b4c43, 0x454d414e, 0x534c4200, 0x485f3250, 
			0x5f4b4c43, 0x454d414e, 0x6c614400, 0x00766e45, 0x67726154, 
			0x66437465, 0x69640067, 0x6c626173, 0x6d735f65, 0x615f756d, 
			0x70730063, 0x635f696d, 0x6e6e6168, 0x635f6c65, 0x73006766, 
			0x5f696d70, 0x6e616863, 0x5f6c656e, 0x5f676663, 0x657a6973, 
			0x00000000, 0x5f636367, 0x70736c62, 0x68615f31, 0x6c635f62, 
			0x6367006b, 0x6c625f63, 0x5f327073, 0x5f626861, 0x006b6c63, 
			0x00000001, 0x00008996, 0x00002070, 0x02800000, BLSP1_QUP0, 
			0x0880000a, 0x02010003, 0x00000003, 0x08800015, 0x00000300, 
			0x02800024, 0x00000000, 0xff00ff00, 0x02800000, BLSP1_UART0, 
			0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 
			0x00000000, 0xff00ff00, 0x02800000, BLSP1_QUP1, 0x0880000a, 
			0x000000-01, 0x08800015, 0x00000300, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP1_UART1, 0x0880000a, 0x000000-01, 
			0x08800015, 0x00000300, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, BLSP1_QUP2, 0x0880000a, 0x2f2e2d03, 0x00000030, 
			0x08800015, 0x00000300, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, BLSP1_UART2, 0x0880000a, 0x000000-01, 0x08800015, 
			0x00000600, 0x02800024, 0x00000000, 0xff00ff00, 0x02800000, 
			BLSP1_QUP3, 0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 
			0x02800024, 0x00000000, 0xff00ff00, 0x02800000, BLSP1_UART3, 
			0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 
			0x00000000, 0xff00ff00, 0x02800000, BLSP1_QUP4, 0x0880000a, 
			0x000000-01, 0x08800015, 0x00000600, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP1_UART4, 0x0880000a, 0x000000-01, 
			0x08800015, 0x00000600, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, BLSP1_QUP5, 0x0880000a, 0x000000-01, 0x08800015, 
			0x00000300, 0x02800024, 0x00000000, 0xff00ff00, 0x02800000, 
			BLSP1_UART5, 0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 
			0x02800024, 0x00000000, 0xff00ff00, 0x02800000, BLSP2_QUP0, 
			0x0880000a, 0x000000-01, 0x08800015, 0x00000300, 0x02800024, 
			0x00000000, 0xff00ff00, 0x02800000, BLSP2_UART0, 0x0880000a, 
			0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP2_QUP1, 0x0880000a, 0x06050403, 
			0x00000007, 0x08800015, 0x00000300, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP2_UART1, 0x0880000a, 0x000000-01, 
			0x08800015, 0x00000300, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, BLSP2_QUP2, 0x0880000a, 0x33323103, 0x00000034, 
			0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, BLSP2_UART2, 0x0880000a, 0x000000-01, 0x08800015, 
			0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 0x02800000, 
			BLSP2_QUP3, 0x0880000a, 0x0a090803, 0x0000000b, 0x08800015, 
			0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 0x02800000, 
			BLSP2_UART3, 0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 
			0x02800024, 0x00000000, 0xff00ff00, 0x02800000, BLSP2_QUP4, 
			0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 
			0x00000000, 0xff00ff00, 0x02800000, BLSP2_UART4, 0x0880000a, 
			0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP2_QUP5, 0x0880000a, 0x57565503, 
			0x00000058, 0x08800015, 0x00000300, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, BLSP2_UART5, 0x0880000a, 0x000000-01, 
			0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, SSC_QUP0, 0x0880000a, 0x000000-01, 0x08800015, 
			0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 0x02800000, 
			SSC_UART0, 0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 
			0x02800024, 0x00000000, 0xff00ff00, 0x02800000, SSC_QUP1, 
			0x0880000a, 0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 
			0x00000000, 0xff00ff00, 0x02800000, SSC_UART1, 0x0880000a, 
			0x000000-01, 0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 
			0xff00ff00, 0x02800000, SSC_QUP2, 0x0880000a, 0x000000-01, 
			0x08800015, 0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 
			0x02800000, SSC_UART2, 0x0880000a, 0x000000-01, 0x08800015, 
			0x0000ff00, 0x02800024, 0x00000000, 0xff00ff00, 0x02800032, 
			0x0000000d, 0x02800051, 0x0000000d, 0x02800070, 0x00000007, 
			0x02800090, 0x00000007, 0x028000b0, 0x0000000a, 0x028000cb, 
			0x00000007, 0x028000e7, 0x00000000, 0x02800102, 0x00000002, 
			0x02800118, 0x00000006, 0x0280012b, 0x00000003, 0x0280013d, 
			0x07544000, 0x0280015b, 0x07584000, 0x02800179, 0x01e84000, 
			0x02800190, 0x00000080, 0x028001b8, 0x000000c0, 0x028001e0, 
			0x00000008, 0x02800201, 0x00000018, 0x0280021e, 0x00000019, 
			0x11800235, 0x00000000, 0x11800245, 0x00000012, 0xff00ff00, 
			0x02800255, 0x80000002, 0x1480025c, 0x00000000, 0xff00ff00, 
			0x02800266, 0x00000000, 0xff00ff00, 0x12800276, 0x00000000, 
			0x12800287, 0x00000001, 0xff00ff00 };



const StringDevice driver_list_hyp_oem[] = {
			{"/dev/buses/qup/blsp1_qup0",2346008095u, 752, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart0",2428797227u, 792, NULL, 0, NULL },
			{"/dev/buses/qup/blsp1_qup1",2346008096u, 828, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart1",2428797228u, 864, NULL, 0, NULL },
			{"/dev/buses/qup/blsp1_qup2",2346008097u, 900, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart2",2428797229u, 940, NULL, 0, NULL },
			{"/dev/buses/qup/blsp1_qup3",2346008098u, 976, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart3",2428797230u, 1012, NULL, 0, NULL },
			{"/dev/buses/qup/blsp1_qup4",2346008099u, 1048, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart4",2428797231u, 1084, NULL, 0, NULL },
			{"/dev/buses/qup/blsp1_qup5",2346008100u, 1120, NULL, 0, NULL },
			{"/dev/buses/uart/blsp1_uart5",2428797232u, 1156, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup0",2385143488u, 1192, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart0",3720265196u, 1228, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup1",2385143489u, 1264, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart1",3720265197u, 1304, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup2",2385143490u, 1340, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart2",3720265198u, 1380, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup3",2385143491u, 1416, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart3",3720265199u, 1456, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup4",2385143492u, 1492, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart4",3720265200u, 1528, NULL, 0, NULL },
			{"/dev/buses/qup/blsp2_qup5",2385143493u, 1564, NULL, 0, NULL },
			{"/dev/buses/uart/blsp2_uart5",3720265201u, 1604, NULL, 0, NULL },
			{"/dev/buses/qup/ssc_qup0",4266220230u, 1640, NULL, 0, NULL },
			{"/dev/buses/uart/ssc_uart0",2125900082u, 1676, NULL, 0, NULL },
			{"/dev/buses/qup/ssc_qup1",4266220231u, 1712, NULL, 0, NULL },
			{"/dev/buses/uart/ssc_uart1",2125900083u, 1748, NULL, 0, NULL },
			{"/dev/buses/qup/ssc_qup2",4266220232u, 1784, NULL, 0, NULL },
			{"/dev/buses/uart/ssc_uart2",2125900084u, 1820, NULL, 0, NULL },
			{"BLSP_GLOBAL_PROP",3648527686u, 1856, NULL, 0, NULL },
			{"/ac/smmu",4045178921u, 2040, NULL, 0, NULL },
			{"/pmic/hyp/cfg",2933554588u, 2052, NULL, 0, NULL }
};
