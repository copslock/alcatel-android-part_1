#include "DALSysTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_hyp_oem[];

extern const uint32 DALPROP_PropBin_hyp_oem[];

extern const StringDevice driver_list_hyp_oem[];


const DALProps DALPROP_PropsInfo_hyp_oem = {(const byte*)DALPROP_PropBin_hyp_oem, DALPROP_StructPtrs_hyp_oem, 33, driver_list_hyp_oem};
