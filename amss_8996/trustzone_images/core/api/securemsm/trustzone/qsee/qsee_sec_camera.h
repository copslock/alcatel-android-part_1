#ifndef QSEE_SECCAM_H
#define QSEE_SECCAM_H

/**
@file qsee_sec_camera.h
@brief Provide Secure Display functionality
*/

/*===========================================================================
   Copyright (c) 2016 by QUALCOMM, Technology Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.tz/1.0.1/api/securemsm/trustzone/qsee/qsee_sec_camera.h#4 $
  $DateTime: 2016/05/23 18:25:32 $
  $Author: pwbldsvc $

when       who      what, where, why
--------   ---      ------------------------------------
05/12/16   ak       Add streaming status control functions
04/25/16   ak       Add CCI address space access functions
04/18/16   sn       Initial version
===========================================================================*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Get the current secure camera session ID
 *
 * @param [out] the current secure camera session ID
 *
 * @return \c 0 on success; -1 on failure. \c
 */
int qsee_sec_camera_get_session(uint32_t *sessionID);

/**
 * Mark streaming session start
 *
 * @param [in] the current secure camera session ID
 *
 * @return \c 0 on success; -1 on failure. \c
 */
int qsee_sec_camera_start_streaming(uint32_t sessionID);

/**
 * Mark streaming session end
 *
 * @param [in] the current secure camera session ID
 *
 * @return \c 0 on success; -1 on failure. \c
 */
int qsee_sec_camera_stop_streaming(uint32_t sessionID);

/**
 * Write 32-bit data to CCI address space.
 *
 * @param [in] offset of the CCI register
 * @param [in] data to write
 *
 * @return \c 0 on success; -1 on failure. \c
 */
int qsee_sec_camera_cci_write(uint32_t offset, uint32_t data);

/**
 * Read 32-bit data from CCI address space.
 *
 * @param [in] offset of the CCI register
 * @param [out] returned data
 *
 * @return \c 0 on success; -1 on failure. \c
 */
int qsee_sec_camera_cci_read(uint32_t offset, uint32_t *data);


#endif /* QSEE_SECCAM_H */

