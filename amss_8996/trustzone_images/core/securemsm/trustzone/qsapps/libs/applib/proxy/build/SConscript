#===============================================================================
#
# ctzl_proxy
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2010-2016 by Qualcomm Technologies, Inc.  All Rights Reserved.
#
#-------------------------------------------------------------------------------
#
#  $Header:  $
#  $DateTime: $
#  $Author:  $
#  $Change:  $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 07/03/16   dc     Updated Copyright
# 19/12/15   dc     Added QPAY app
# 08/19/15   ng     Added secure_ui_sample64
# 07/16/14   tp     Changed DRM sconscripts to use the prxy_services
#                   library instead of linking to qsee_services.
# 10/17/13   ib    Add SECUREMM
# 05/26/13   sn    Add SECUREUI
# 10/18/12   ah     initial version
#===============================================================================
Import('env')
env = env.Clone()


#-------------------------------------------------------------------------------
# Set search path for securemsm subsystem files
#-------------------------------------------------------------------------------
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/libs/applib/common/src")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/libs/services/src")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/secmath/shared/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/secrsa/shared/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/secrsa/shared/src")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/ecc/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/core/ecc/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/ecies/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/core/ecies/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/aes/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/core/aes/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/sha/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/core/sha/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/environment/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/des/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/shared/ecc_generic/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/unifiedcrypto/core/ecc_generic/inc")
env.Append(CCFLAGS = " -O3  -D_TZ_PROXY_LIB ")

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'SERVICES',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsapps/libs/applib/proxy/"

#-------------------------------------------------------------------------------
# Let Scons build the object files in $(BUILDPATH} and don't copy over files
#-------------------------------------------------------------------------------
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------



#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
SECMATH_SRC_SHARED = [
  '${BUILDPATH}/src/prxy_secmath.c',
  '${BUILDPATH}/src/prxy_secrsa.c',
  '${BUILDPATH}/src/prxy_services.c',
  '${BUILDPATH}/src/prxy_ecc.c',
  '${BUILDPATH}/src/prxy_qsee_shim.c',
  '${BUILDPATH}/src/prxy_ufaes.c',
  '${BUILDPATH}/src/prxy_ufdes.c',
  '${BUILDPATH}/src/prxy_ufsha.c',
]

# This depends on the random number generator provided by TZ
env.Append(CPPPATH = "${COREBSP_ROOT}/api/dal")
env.Append(CPPPATH = "${COREBSP_ROOT}/api/securemsm/crypto")
env.Append(CPPPATH = "${COREBSP_ROOT}/api/securemsm/trustzone/qsee")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/secmath/env/sampleapp/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/secrsa/env/sampleapp/inc")
env.Append(CPPPATH = "${COREBSP_ROOT}/securemsm/trustzone/qsee/include")


# SECMATH_PRIV_HEADERS = env.GlobFiles('${BUILDPATH}/secmath/shared/src/*.h')
# SECMATH_PRIV_HEADERS += [
#   '${BUILDPATH}/secmath/shared/src/secmath_BIGINT_isprime.c',
# ]
#-------------------------------------------------------------------------------
# Add Libraries to images in the secboot3 boot architecture. Note we don't
# add the library into SBL1 image as SBL1 uses PBL secboot library instead
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

env.AddBinaryLibrary(['KEYMASTER_IMAGE',
                      'GPSAMPLE', 'GPTEST2', 'GPTEST_IMAGE', 'TTAARI1', 'TTACAPI1', 'TTACAPI2', 'TTACAPI3', 'TTACAPI4', 'TTACAPI5', 'TTACRP1', 'TTADS1', 'TTATIME1', 'TTATCF1', 'TTATCF2', 'TTATCF3',
                      'TTATCF4', 'TTATCF5', 'SAMPLEAPP_IMAGE', 'TCLAPP_IMAGE', 'SAMPLEAPP64_IMAGE', 'SYNAFP_IMAGE', 'SYNAFP64_IMAGE', 'ASSURANCETEST_IMAGE', 'ASSURANCETEST64_IMAGE', 'SECUREUISAMPLE_IMAGE', 'SECUREUISAMPLE64_IMAGE', 'APTTESTAPP_IMAGE', 'APTCRYPTOTESTAPP_IMAGE', 'FIDOCONFIG_IMAGE',
                      'APTTESTAPP64_IMAGE', 'APTCRYPTOTESTAPP64_IMAGE', 'SECUREMM_IMAGE', 'MDTP_IMAGE', 'WIDEVINE_IMAGE', 'PLAYREADY_IMAGE', 'LKSECAPP_IMAGE', 'LKSECAPP64_IMAGE', 'SSMAPP_IMAGE', 'FINGERPRINT_IMAGE', 'FINGERPRINT64_IMAGE', 'RETSTAPP_IMAGE', 'VOICEPRINT_IMAGE', 'IRIS_IMAGE',
                      'SKELETON_IMAGE', 'CRIKEYMGMTAPP_IMAGE', 'CRIKEYMGMTAPP64_IMAGE', 'APTLKSECAPP_IMAGE', 'APTLKSECAPP64_IMAGE', 'CHAMOMILE_IMAGE', 'FIDOCRYPTO_IMAGE', 'SAMPLEAUTH_IMAGE', 'SECOTACL_IMAGE', 'SAMPLEEXTAUTH_IMAGE', 'QMPSECAPP_IMAGE', 'FIDOSUI_IMAGE', 'DHSECAPP_IMAGE', 'PR_3_0_IMAGE',
                      'QPAY_IMAGE', 'QPAY64_IMAGE'],
   '${BUILDPATH}/tzlib_proxy',
   SECMATH_SRC_SHARED)
#env.CleanPack(['KEYMASTER_IMAGE', 'GPTEST_IMAGE', 'TTAARI1', 'TTACAPI1', 'TTACAPI2', 'TTACAPI3', 'TTACAPI4', 'TTACAPI5', 'TTACRP1', 'TTADS1', 'TTATIME1', 'TTATCF1', 'TTATCF2', 'TTATCF3', 'TTATCF4', 'TTATCF5', 'SAMPLEAPP_IMAGE', 'TCLAPP_IMAGE', 'SAMPLEAPP64_IMAGE','SECUREMM_IMAGE', 'MDTP_IMAGE', 'SECOTACL_IMAGE', 'WIDEVINE_IMAGE', 'PLAYREADY_IMAGE', 'LKSECAPP_IMAGE', 'SSMAPP_IMAGE', 'RETSTAPP_IMAGE', 'CHAMOMILE_IMAGE', 'FIDOCRYPTO_IMAGE', 'FIDOSUI_IMAGE', 'DHSECAPP_IMAGE', 'PR_3_0_IMAGE', 'QPAY_IMAGE', 'QPAY64_IMAGE'], SECMATH_PRIV_HEADERS)
