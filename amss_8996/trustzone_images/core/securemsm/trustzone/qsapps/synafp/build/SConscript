#===========================================================================
#  Copyright (c) 2011-2014 QUALCOMM Incorporated.
#  All Rights Reserved.
#  Qualcomm Confidential and Proprietary
#===========================================================================
#
# App Core
#
# GENERAL DESCRIPTION
#    build script
#                      EDIT HISTORY FOR FILE
#
#  This section contains schedulerents describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 01/13/14    wt     Created
#===============================================================================
Import('env')
env = env.Clone()

if env.has_key('USES_NO_CP'):
	env.Append(CCFLAGS = ' -DUSES_NO_CP ')


#env.Append(ARMCC_OPT = " --enum_is_int  --interface_enums_are_32_bit ")
#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/trustzone/qsapps/synafp/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------

# do not generate thumb code for inline assembler code
#env.Append(ARMCC_OPT = ' -marm')

env.RequireRestrictedApi('SMPLSERV')
env.RequireRestrictedApi('SMPLCERT')
env.RequireRestrictedApi('BIOMETRIC')
env.RequirePrivateApi('SECUREMSM')

INC_PATH = [
   "${INC_ROOT}/core/api/boot/qfprom/",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/synafp/inc/${CHIPSET}",
   "${INC_ROOT}/core/api/services",
   "${INC_ROOT}/core/api/securemsm/trustzone/qsee",
   "${INC_ROOT}/core/api/securemsm/trustzone/gp",
   "${INC_ROOT}/core/api/kernel/libstd/stringl",
   "${INC_ROOT}/core/securemsm/accesscontrol/api",
   "${INC_ROOT}/core/kernel/smmu/v2/inc/",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/synafp/src",
   "${INC_ROOT}/core/securemsm/secrsa/shared/inc",
   "${INC_ROOT}/core/securemsm/secmath/shared/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/ecc/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/aes/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/aes/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/shared/sha/inc",
   "${INC_ROOT}/core/securemsm/unifiedcrypto/core/sha/inc",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/services/src",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/applib/common_applib/inc",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/applib/gp/inc",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/applib/qsee/inc",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/applib/qsee/src",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/synafp/inc",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/synafp/inc/vtp",
   "${INC_ROOT}/core/securemsm/trustzone/qsee/mink/include",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/biometric/inc",
]

# Logging APIs
env.PublishPrivateApi('SSE_LOG_API', [
   '${INC_ROOT}/core/securemsm/sse/log/inc',
])

# Common includes
env.PublishPrivateApi('SSE_COMMON_API', [
   '${INC_ROOT}/core/securemsm/sse/common/include',
])

# Secure Touch includes
env.PublishPrivateApi('SSE_SECURE_TOUCH_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/common/include',
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/layout/include',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/controller/inc',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_QSEE_API', [
   '${INC_ROOT}/core/securemsm/sse/qsee/SecureTouch/drTs/include',
])
env.Append(CPPPATH = INC_PATH)

# Secure UI includes
env.PublishPrivateApi('SSE_SECURE_UI_QSEE_API', [
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUI/inc",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUILib/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUILib/lib/include",
])

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------   
if env.has_key('USES_NO_CP'):
	APP_CORE_ENTRY_SOURCES = [      
        '${BUILDPATH}/app_main.c',
        '${BUILDPATH}/qsee_wrapper_spi.c',
]

else:
  APP_CORE_ENTRY_SOURCES = [      
        '${BUILDPATH}/app_main.c',
        '${BUILDPATH}/qsee_wrapper_spi.c',
]


#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddObject('SYNAFP_IMAGE', APP_CORE_ENTRY_SOURCES)

#-------------------------------------------------------------------------------
# Add metadata to image
#-------------------------------------------------------------------------------
md = {
   'appName':    'synafp',
   'privileges': ['default',
                  'I2C',
                  'OEMUnwrapKeys',
                  'CertValidate',
                  'SPI',              
                  'TLMM',
                  'SecureDisplay',
                  'IntMask',
                  'OEMBuf',
                  'TransNSAddr',
                 ],
   'memoryType':  'Unprotected',
}

env.AddSecureAppMetadata('SYNAFP_IMAGE', md )

#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------
NOSHIP_SOURCES = SRCPATH + "/app_content_protection_noship.c"
env.CleanPack('SYNAFP_IMAGE', NOSHIP_SOURCES)

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
executionEngine_lib = env.File('${BUILDPATH}/validity_lib32/libexecutionEngine.a')
env.AddLibsToImage('SYNAFP_IMAGE', executionEngine_lib)

#scriptEngine_lib = env.File('${BUILDPATH}/validity_lib32/libscriptEngine.a')
#env.AddLibsToImage('SYNAFP_IMAGE', scriptEngine_lib )

event_lib = env.File('${BUILDPATH}/validity_lib32/libvcsEvent.a')
env.AddLibsToImage('SYNAFP_IMAGE', event_lib)

vfmUnpack_lib = env.File('${BUILDPATH}/validity_lib32/libvfmUnpack.a')
env.AddLibsToImage('SYNAFP_IMAGE', vfmUnpack_lib)

vfm_lib = env.File('${BUILDPATH}/validity_lib32/libvfm.a')
env.AddLibsToImage('SYNAFP_IMAGE', vfm_lib)

scs_lib = env.File('${BUILDPATH}/validity_lib32/libvcsScs.a')
env.AddLibsToImage('SYNAFP_IMAGE', scs_lib)

pal_lib = env.File('${BUILDPATH}/validity_lib32/libvcsPal.a')
env.AddLibsToImage('SYNAFP_IMAGE', pal_lib)

dbg_lib = env.File('${BUILDPATH}/validity_lib32/libvcsDbg.a')
env.AddLibsToImage('SYNAFP_IMAGE', dbg_lib)

template_lib = env.File('${BUILDPATH}/validity_lib32/libvcsTemplate.a')
env.AddLibsToImage('SYNAFP_IMAGE', template_lib)

vcsConfig_lib = env.File('${BUILDPATH}/validity_lib32/libvcsConfig.a')
env.AddLibsToImage('SYNAFP_IMAGE', vcsConfig_lib)

matcher_lib = env.File('${BUILDPATH}/validity_lib32/libvcsMatcher.a')
env.AddLibsToImage('SYNAFP_IMAGE', matcher_lib)

#vcsmMatcher4_lib = env.File('${BUILDPATH}/validity_lib32/libvcsmMatcher4.a')
#env.AddLibsToImage('SYNAFP_IMAGE', vcsmMatcher4_lib)

libBMF_r3g_lib = env.File('${BUILDPATH}/validity_lib32/libBMF_r3g.a')
env.AddLibsToImage('SYNAFP_IMAGE',libBMF_r3g_lib)

image_lib = env.File('${BUILDPATH}/validity_lib32/libvcsImage.a')
env.AddLibsToImage('SYNAFP_IMAGE', image_lib)

dliUtil_lib = env.File('${BUILDPATH}/validity_lib32/libvcsIrDliUtil.a')
env.AddLibsToImage('SYNAFP_IMAGE', dliUtil_lib)

dliRt_lib = env.File('${BUILDPATH}/validity_lib32/libvcsIrDliRT.a')
env.AddLibsToImage('SYNAFP_IMAGE', dliRt_lib)

algCommon_lib = env.File('${BUILDPATH}/validity_lib32/libvcsAlgCommon.a')
env.AddLibsToImage('SYNAFP_IMAGE', algCommon_lib)

#authnrAdrFpHal_lib = env.File('${BUILDPATH}/validity_lib32/libauthnrAdrFpHal.a')
#env.AddLibsToImage('SYNAFP_IMAGE', authnrAdrFpHal_lib)

#authnrAliPay_lib = env.File('${BUILDPATH}/validity_lib32/libauthnrAliPay.a')
#env.AddLibsToImage('SYNAFP_IMAGE', authnrAliPay_lib)


 
env.LoadSoftwareUnits()
