/*! @file qsee_wrapper_spi.c
*******************************************************************************

** This file contains the QSEE SPI interface functions.
**
** Copyright 2011-2015 Synaptics Sensors, Inc. All Rights Reserved.
**
**/

#include "com_dtypes.h"
#include "qsee_wrapper_spi.h" 
#include "qsee_spi.h"
#include "qsee_log.h"
#include "qsee_timer.h"
#define SPI_DEVICE_ID QSEE_SPI_DEVICE_9

//qsee_spi_config_t spi_config = {0, QSEE_SPI_CLK_IDLE_LOW, QSEE_SPI_INPUT_FIRST_MODE, QSEE_SPI_CS_ACTIVE_LOW, QSEE_SPI_CS_KEEP_ASSERTED, QSEE_SPI_CLK_NORMAL, 8, 0, 0};
qsee_spi_config_t spi_config = {9600000, QSEE_SPI_CLK_IDLE_LOW, QSEE_SPI_INPUT_FIRST_MODE, QSEE_SPI_CS_ACTIVE_LOW, QSEE_SPI_CS_KEEP_ASSERTED, QSEE_SPI_CLK_NORMAL, 8, 1, 0};

#if 0 
{
    .max_freq = 0,
    .spi_clk_polarity = QSEE_SPI_CLK_IDLE_HIGH;
    .spi_shift_mode = QSEE_SPI_OUTPUT_FIRST_MODE;
    .spi_cs_polarity = QSEE_SPI_CS_ACTIVE_HIGH;
    .spi_cs_mode = QSEE_SPI_CS_KEEP_ASSERTED;
    .spi_clk_always_on = QSEE_SPI_CLK_NORMAL;
    .spi_bits_per_word = 8;
    .hs_mode = 0;
    .loopback = 0;
};
#endif
/**
 * Open the device, and perform some HW intialization \n
 *
 *
 * @return 0 on success, negative on failure
 */
 
 #if 0
void mdelay(unsigned long long x)
{
    unsigned long long time_base = qsee_get_uptime();
	unsigned long long time_out;
	while (1) 
	{
        time_out = qsee_get_uptime();
		if ((time_out - time_base) >=x )
			break;
	}

}
#endif
 
 int qsee_wrapper_spi_open(void)
 {
 	int retval = 0;
    QSEE_LOG(QSEE_LOG_MSG_ERROR," Entered qsee_wrapper_spi_open xxxxxyyyy1212\n");
#if 1
 	retval = qsee_spi_open(SPI_DEVICE_ID);
 	if(retval != 0)
    {
 		//printk("qsee_spi_open: retval = %d\n", retval);
        QSEE_LOG(QSEE_LOG_MSG_ERROR,"SPI Open Error\n");
    }
#endif
 	return 0;
 }

/**
 * Reads data from SPI bus.
 *
 * @param[in] p_read_info Read buffer information
 *
 * @return 0 on success, negative on failure
 */
 int qsee_wrapper_spi_read(qsee_wrapper_spi_transaction_info_t* p_read_info)
 {
 	int retval = 0;
 	qsee_spi_transaction_info_t read_info;

        QSEE_LOG(QSEE_LOG_MSG_ERROR," Entered qsee_wrapper_spi_read\n");
#if 1
	read_info.buf_addr = p_read_info->buf_addr;
	read_info.buf_len = p_read_info->buf_len;
	
 	retval = qsee_spi_read(SPI_DEVICE_ID ,&spi_config, &read_info);
	//mdelay(50);
 	if(retval != 0)
        {
        
 	        QSEE_LOG(QSEE_LOG_MSG_ERROR,"qsee_spi_read failed\n");
                //printk("qsee_spi_read: retval = %d\n", retval);
        }
#endif
 	return 0;
}

/**
 * Writes data on SPI bus.
 *
 * @param[in] p_write_info Write buffer information
 *
 * @return 0 on success, negative on failure
 */
int qsee_wrapper_spi_write(qsee_wrapper_spi_transaction_info_t* p_write_info)
{
	int retval = 0;
 	qsee_spi_transaction_info_t write_info;

	QSEE_LOG(QSEE_LOG_MSG_ERROR," Entered qsee_wrapper_spi_write\n");
#if 1	
	write_info.buf_addr = p_write_info->buf_addr;
	write_info.buf_len = p_write_info->buf_len;
	
 	retval = qsee_spi_write(SPI_DEVICE_ID ,&spi_config, &write_info);
	//mdelay(50);
 	if(retval != 0)
        {
         QSEE_LOG(QSEE_LOG_MSG_ERROR,"qsee_spi_write failed\n");
 		//printk("qsee_spi_write: retval = %d\n", retval);
        }
#endif
 	return 0;
		
}

/**
 * Writes data on SPI bus.
 *
 * @param[in] p_write_info Write buffer information
 * @param[in] p_read_info Read buffer information
 *
 * @return 0 on success, negative on failure
 */
int qsee_wrapper_spi_full_duplex(qsee_wrapper_spi_transaction_info_t* p_write_info, qsee_wrapper_spi_transaction_info_t* p_read_info)
{
	int retval = 0;
	int i = 0;
 	qsee_spi_transaction_info_t read_info;
 	qsee_spi_transaction_info_t write_info;

	#if 0
	static uint8 write_data[256];//__attribute__((aligned(64)));
	static uint8 read_data[256];//__attribute__((aligned(64)));
	for (i = 0; i < 256; i++) {
		write_data[i] =0;
		read_data[i] = 0;
	}
	#endif
	
	QSEE_LOG(QSEE_LOG_MSG_ERROR," Entered qsee_wrapper_spi_full_duplex\n");
#if 1
	read_info.buf_addr = p_read_info->buf_addr;
	read_info.buf_len = p_read_info->buf_len;
	write_info.buf_addr = p_write_info->buf_addr;
	write_info.buf_len = p_write_info->buf_len;
	#if 0
	read_data = (uint8 *)read_info.buf_addr;
	write_data = (uint8 *)write_info.buf_addr;

	QSEE_LOG(QSEE_LOG_MSG_ERROR,"qsee_spi_full_duplex write_data is: (buf_len is %d)", write_info.buf_len);
	for (i = 0;i<10;i++)
		QSEE_LOG(QSEE_LOG_MSG_ERROR,"0x%0x", write_data[i]);
	QSEE_LOG(QSEE_LOG_MSG_ERROR,"/n");
	#endif
	retval = qsee_spi_full_duplex(SPI_DEVICE_ID ,&spi_config, &write_info, &read_info);
	//mdelay(50);
	if(retval != 0)
        {
         QSEE_LOG(QSEE_LOG_MSG_ERROR,"qsee_spi_full_duplex Failed");
        }
	#if 0
	else {
		QSEE_LOG(QSEE_LOG_MSG_ERROR,"qsee_spi_full_duplex read_data is:(buf_len is %d)", read_info.buf_len);
		for (i = 0;i<read_info.buf_len;i++)
			QSEE_LOG(QSEE_LOG_MSG_ERROR,"0x%0x", read_data[i]);
        QSEE_LOG(QSEE_LOG_MSG_ERROR,"/n");

	}
	#endif
#endif
	return 0;
	
	
}

/**
 * Close the client access to the spi device.
 *
 *
 *
 * @return 0 on success, negative on failure
 */

int qsee_wrapper_spi_close(void)
{
	int retval = 0;
        
    QSEE_LOG(QSEE_LOG_MSG_ERROR," qsee_wrapper_spi_close entered\n");	
	retval = qsee_spi_close(SPI_DEVICE_ID);
 	if(retval != 0)
    {
     QSEE_LOG(QSEE_LOG_MSG_ERROR," qsee_spi_close failed\n");
    }
 	return 0;
}
