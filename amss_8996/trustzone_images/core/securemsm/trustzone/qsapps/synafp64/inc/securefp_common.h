 /*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        Secure Finger Print Common Header

GENERAL DESCRIPTION
  Contains common typedef and command-response struct

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#ifndef __securefp_common_h_
#define __securefp_common_h_

#include "vcsTypes.h"

#define VCS_API
#define MAX_TEMPLATE_SIZE		30

//typedef unsigned char uint8_t;
//typedef unsigned short uint16_t;
//typedef unsigned int uint32_t;

typedef uint32_t vcsUint32_t;
typedef uint8_t vcsUint8_t;

typedef vcsUint32_t vcsResult_t;


typedef struct vcsTlBlobData_s
{
    vcsUint32_t     length;
    vcsUint32_t     pData;
} vcsTlBlobData_t;

typedef struct vfm_auth_req_s {
    vcsUint32_t   cmd_id;   /* command id */
    vcsTlBlobData_t input_blob;           /* input blob data */
//	vcsUint8_t    *mem_pa;              /* ion buffer pointer for spi driver */
} vfm_auth_req_t;

typedef struct vfm_auth_rsp_s {
    vcsUint32_t cmd_id;   /* command id */
    vcsUint32_t ret;                    /* return value */
	vcsTlBlobData_t output_blob;          /* output blob data */
} vfm_auth_rsp_t;

#endif /* __securefp_common_h_ */

