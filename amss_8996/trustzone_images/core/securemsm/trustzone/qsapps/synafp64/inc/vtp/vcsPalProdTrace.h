/*! @file vcsPalProdTrace.h
*******************************************************************************
**
**  This file contains the USDK Event Tracing interface.
**
**  Copyright 2008 Validity Sensors, Inc. All Rights Reserved.
*/

#ifndef __vcsPalProdTrace_h__
#define __vcsPalProdTrace_h__

#if VCS_FEATURE_PRODTRACE && !DBG

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

    extern void VCS_API palProdTraceInit(void);
    extern void VCS_API palProdTraceDeinit(void);
    extern void  VCS_API palProdTraceStart(void);
    extern void  VCS_API palProdTraceStop(void);
    extern void  VCS_API palTraceEvent(
        char*       fileName,
        vcsUint32_t lineNumber,
        vcsUint8_t  level
        );
    extern void VCS_API palProdTraceSetLevel(vcsUint32_t level);

# define PAL_TRACE_EVENT(L) {                                           \
    palTraceEvent(__FILE__, __LINE__, L);                               \
    }

/*
*   Interface to trace events.
*/

#define PAL_PRDTRACE_LEVEL_REG_VAL_NAME              "evTraceEnableLevel"

#define PAL_TRACE_LEVEL_NONE        0   // Tracing is not on
#define PAL_TRACE_LEVEL_FATAL       1   // Abnormal exit or termination
#define PAL_TRACE_LEVEL_ERROR       2   // Severe errors that need logging
#define PAL_TRACE_LEVEL_WARNING     3   // Warnings such as allocation failure
#define PAL_TRACE_LEVEL_INFORMATION 4   // Includes non-error cases(e.g.,Entry-Exit)
#define PAL_TRACE_LEVEL_VERBOSE     5   // Detailed traces from intermediate steps


# define PAL_PRODTRACE_INIT()         palProdTraceInit()
# define PAL_PRODTRACE_DEINIT()       palProdTraceDeinit()

# define PAL_PRODTRACE_START()        palProdTraceStart()
# define PAL_PRODTRACE_STOP()         palProdTraceStop()

/* Setting trace level */
#define PAL_PRODTRACE_SET_LEVEL(L)    palProdTraceSetLevel(L)

/* Trace error messages */
# define PAL_PRODTRACE_ERROR()        PAL_TRACE_EVENT(PAL_TRACE_LEVEL_ERROR)

/* Trace warning messages */
# define PAL_PRODTRACE_WARNING()      PAL_TRACE_EVENT(PAL_TRACE_LEVEL_WARNING)

/* Trace informational messages */
# define PAL_PRODTRACE_INFO()         PAL_TRACE_EVENT(PAL_TRACE_LEVEL_INFORMATION)

/* Trace detailed information messages */
# define PAL_PRODTRACE_VERBOSE()      PAL_TRACE_EVENT(PAL_TRACE_LEVEL_VERBOSE)

#ifdef __cplusplus
    }
#endif /* __cplusplus */

#else /*VCS_FEATURE_PRODTRACE&& !DBG*/

/*
*   Event Tracing dummy interface.
*/
# define PAL_PRODTRACE_INIT()
# define PAL_PRODTRACE_DEINIT()
# define PAL_PRODTRACE_START()
# define PAL_PRODTRACE_STOP()
# define PAL_PRODTRACE_ERROR()
# define PAL_PRODTRACE_WARNING()
# define PAL_PRODTRACE_INFO()
# define PAL_PRODTRACE_VERBOSE()

#endif /* VCS_FEATURE_PRODTRACE && !DBG*/

#if !VCS_FEATURE_PRODTRACE && !DBG
#define PAL_PRODTRACE_SET_LEVEL(L)
#endif /* !VCS_FEATURE_PRODTRACE && !DBG*/



#endif /* __vcsPalProdTrace_h__ */

