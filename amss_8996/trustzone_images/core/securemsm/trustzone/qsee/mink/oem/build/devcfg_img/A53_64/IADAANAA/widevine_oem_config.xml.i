<driver name="NULL">
  <global_def>
    <var_seq name="drm_data_store_path" type=DALPROP_DATA_TYPE_STRING>
      "/data/misc/qsee/"
    </var_seq>
  </global_def>
  <device id="widevine">
<!--
    Not applicable to Widevine
-->
    <props name="license_data_store_path" type=DALPROP_ATTR_TYPE_STRING_PTR>
      drm_data_store_path
    </props>
<!--
    Not applicable to Widevine
-->
    <props name="disable_security_stream_check" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
    Not applicable to Widevine
-->
    <props name="get_IV_constraint" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
    Not applicable to Widevine
-->
    <props name="use_legacy_hdmi_hdcp_check" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
    HDCP1.4 OEM config
    Warning!
    To OEM, please do NOT change this value. if you change, premium DRM content can be exposed.
-->
    <props name="use_oem_external_hdcp" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
    customers need to config "provision_constraint_flag" to have single time provisioning
    feature with drm_prov_finalize().
    if "provision_constraint_flag" = 1, key provisioning can be done only once after calling drm_prov_finalize().
    if "provision_constraint_flag" = 0, there is no such constraint
-->
    <props name="provision_constraint_flag" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
   The maximum HDCP 2.x version that OEM can support
   we define
   1 is NOT supported.
   2 is HDCP2.0;
   3 is HDCP2.1;
   4 is HDCP2.2;
-->
    <props name="maximum_hdcp_2x_capability" type=DALPROP_ATTR_TYPE_UINT32>
      4
    </props>
<!--
   This feature for Widevine debug only
   if g_widevine_kcb_logging_flag is set (=1), every OEMCrypto_SelectKey will log
   key control block info: including magic, duration, nonce, control bits (4 bytes).
   Note, this feature should be only turned on for a debugging purpose. Turning on
   this feature adds more loggings and downgrades performance.
   0: Disable
   1: Enable
-->
    <props name="enable_kcb_logging_flag" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
<!--
   0: DSI-0 is being used for External display
   1: DSI-1 is being used for External display
   NOTE: This will not configure DSI-0/DSI-1 for external display but it is only for informing widevine
   about which DSI interface is configured for external display.
-->
    <props name="external_display_dsi_intf_num" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>
<!--
   0: Allow Content through DSI-0 and DSI-1. It will still block HDCP2.x content on DSI interface that is configured for external display.
   1: Block Content on DSI interface configured for external display but allow through local display.
   2: Block both DSI-0 and DSI-1
   Below is the behaviour for various combinations of external_display_dsi_intf_num and block_content_thru_dsi
   if external_display_dsi_intf_num=1 and block_content_thru_dsi = 0 then
   Secure content is allowed through DSI-0 and DSI-1. But HDCP2.X content is still blocked on DSI-1 as it is external display.
   if external_display_dsi_intf_num=1 and block_content_thru_dsi = 1 then
   Secure content is allowed through DSI-0 as it is local display but it is blocked on DSI-1 as it is external display
   if external_display_dsi_intf_num=1 and block_content_thru_dsi = 2 then
   Secure content is blocked on both DSI-0 and DSI-1. This means no video seen on both local and external display.
   if external_display_dsi_intf_num=0 and block_content_thru_dsi = 0 then
   Secure content is allowed through DSI-0 and DSI-1. But HDCP2.X content is still blocked on DSI-0 as it is external display.
   if external_display_dsi_intf_num=0 and block_content_thru_dsi = 1 then
   Secure content is allowed through DSI-1 as it is local display but it is blocked on DSI-0 as it is external display
   if external_display_dsi_intf_num=0 and block_content_thru_dsi = 2 then
   Secure content is blocked on both DSI-0 and DSI-1. This means no video seen on both local and external display.
-->
    <props name="block_content_thru_dsi" type=DALPROP_ATTR_TYPE_UINT32>
      2
    </props>
  </device>
</driver>
