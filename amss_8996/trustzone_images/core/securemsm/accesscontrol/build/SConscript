#===============================================================================
#
# AC Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014-2015 by Qualcomm Technologies, Inc.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.tz/1.0.1/securemsm/accesscontrol/build/SConscript#3 $
#  $DateTime: 2015/08/21 17:51:48 $
#  $Author: pwbldsvc $
#  $Change: 8870019 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 04/03/15   rs      Moved SMEM partition protections to Hypervisor
# 02/09/12   PS      Initial release
#===============================================================================
Import('env')

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
env = env.Clone()

# Additional defines
env.Append(CPPDEFINES = ["FEATURE_LIBRARY_ONLY"])   


if env['MSM_ID'] in ['8996']:
    env.Replace(AC_MSM_ID = '8996')
    env.Replace(AC_FAMILY_ID = 'honeybadger')
elif env['MSM_ID'] in ['8956']:
    env.Replace(AC_MSM_ID = '8956')
    env.Replace(AC_FAMILY_ID = 'bear')
elif env['MSM_ID'] in ['8952']:
    env.Replace(AC_MSM_ID = '8952')
    env.Replace(AC_FAMILY_ID = 'bear')
elif env['MSM_ID'] in ['8953']:
    env.Replace(AC_MSM_ID = '8953')
    env.Replace(AC_FAMILY_ID = 'bear')
elif env['MSM_ID'] in ['8937']:
    env.Replace(AC_MSM_ID = '8937')
    env.Replace(AC_FAMILY_ID = 'bear')
else:
   Return();
      
SRCPATH = "../../accesscontrol"
CBSP_APIS = []

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 
IMAGES_HYP = ['HYPERVISOR_IMAGE']
IMAGES_TZ = ['TZOS_IMAGE']

if env.has_key('TZOS_IMAGE'):
    env.Replace(AC_IMG = 'tz')
elif env.has_key('HYPERVISOR_IMAGE') or env.has_key('DAL_DEVCFG_OEM_IMG'):
    env.Replace(AC_IMG = 'hyp')
else:
    Return();

if 'tzbsp_with_test_svc' in env:
  if env['tzbsp_with_test_svc'] == 1:
      print "With Test is turned on"
      env.Append(CCFLAGS = " -DTZBSP_WITH_TEST_SVC ")

if 'tzbsp_with_mmu' in env:
  if env['tzbsp_with_mmu'] == 1:
      print "With MMU is turned on"
      env.Append(CCFLAGS = " -DTZBSP_WITH_MMU ")


#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
env.PublishPrivateApi('AC', [
   '${INC_ROOT}/core/securemsm/accesscontrol/src/components/smmu',
   '${INC_ROOT}/core/securemsm/accesscontrol/src/components/xpu',
   '${INC_ROOT}/core/securemsm/accesscontrol/src/components',
   '${INC_ROOT}/core/securemsm/accesscontrol/src',
   '${INC_ROOT}/core/securemsm/accesscontrol/src/tz',
   '${INC_ROOT}/core/securemsm/accesscontrol/api',
   '${INC_ROOT}/core/securemsm/accesscontrol/cfg/${AC_MSM_ID}',
   '${INC_ROOT}/core/securemsm/accesscontrol/cfg/${AC_MSM_ID}/${AC_IMG}',
   '${INC_ROOT}/core/securemsm/accesscontrol/src/components/vmidmt'
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS += [
    'BUSES',
    'HAL',
    'DAL',
    'MPROC',
    'SYSTEMDRIVERS',
    'SERVICES',
    'SECUREMSM',
    'MINK',
    'KERNEL',
    'TZCHIPSET',
    'TZLIBARMV8',
    'AC',
]

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)

#-------------------------------------------------------------------------------
# HYP Sources, libraries
#-------------------------------------------------------------------------------
AC_SOURCES_RAW_HYP = env.GlobFiles('../../accesscontrol/src/hyp/AccessControlHyp.c', posix=True)
AC_SOURCES_RAW_HYP += env.GlobFiles('../../accesscontrol/src/components/smmu/*.c', posix=True)
AC_SOURCES_RAW_HYP += env.GlobFiles('../../accesscontrol/cfg/${AC_MSM_ID}/*.c', posix=True)
AC_SOURCES_RAW_HYP += env.GlobFiles('../../accesscontrol/src/*.c', posix=True)
AC_SOURCES_RAW_HYP = [path.replace(SRCPATH, '${BUILDPATH}') for path in AC_SOURCES_RAW_HYP]


#-------------------------------------------------------------------------------
# TZ Sources, libraries
#-------------------------------------------------------------------------------
AC_SOURCES_RAW_TZ = env.GlobFiles('../../accesscontrol/src/tz/AccessControlTz.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/src/tz/${AC_MSM_ID}/AccessControlTzTarget.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/src/tz/${AC_FAMILY_ID}/AccessControlTzFamily.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/src/components/xpu/*.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/src/*.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/src/components/vmidmt/*.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/cfg/${AC_MSM_ID}/tz/*.c', posix=True)
AC_SOURCES_RAW_TZ += env.GlobFiles('../../accesscontrol/cfg/${AC_MSM_ID}/*.c', posix=True)
AC_SOURCES_RAW_TZ = [path.replace(SRCPATH, '${BUILDPATH}') for path in AC_SOURCES_RAW_TZ]

#-------------------------------------------------------------------------------
# XML files
#-------------------------------------------------------------------------------
#SPMI_CONFIG_FILE_XML = env.GlobFiles('../../spmi/config/${SPMI_MSM_ID}/*${SPMI_MSM_ID}*.xml', posix=True)
#SPMI_CONFIG_FILE_XML = ', '.join(SPMI_CONFIG_FILE_XML) #convert list to string

#if SPMI_CONFIG_FILE_XML: #try including XML only if it is present in the build 
#   if 'USES_DEVCFG' in env:
#      env.AddDevCfgInfo(['DAL_DEVCFG_IMG'], 
#      {
#          'devcfg_xml'    : SPMI_CONFIG_FILE_XML
#      })

#-------------------------------------------------------------------------------
# Bear targets don't have Hyp.
#-------------------------------------------------------------------------------
SMMU_CONFIG = 'smmu_config.xml'
XPU_CONFIG = 'xpu_config.xml'
if 'USES_NOAC' in env:
  SMMU_CONFIG = 'smmu_config_noac.xml'
  XPU_CONFIG = 'xpu_config_noac.xml'
  
if env['MSM_ID'] not in ['8952','8953', '8956', '8976', '8935', '8937']:
  SMMU_CONFIG_FILE_XML = env.GlobFiles('../../accesscontrol/cfg/${AC_MSM_ID}/hyp/'+SMMU_CONFIG, posix=True)
  SMMU_CONFIG_FILE_XML = ', '.join(SMMU_CONFIG_FILE_XML) #convert list to string
  if 'USES_DEVCFG' in env:
    DEVCFG_IMG = ['DAL_DEVCFG_OEM_HYP_IMG']
    env.AddDevCfgInfo(DEVCFG_IMG,
    {
       'devcfg_xml' : [SMMU_CONFIG_FILE_XML]
    })

XPU_CONFIG_FILE_XML = env.GlobFiles('../../accesscontrol/cfg/${AC_MSM_ID}/tz/'+XPU_CONFIG, posix=True)
XPU_CONFIG_FILE_XML = ', '.join(XPU_CONFIG_FILE_XML) #convert list to string

if 'USES_DEVCFG' in env:
  DEVCFG_IMG = ['DAL_DEVCFG_OEM_QSEE_IMG']
  env.AddDevCfgInfo(DEVCFG_IMG,
  {
    'devcfg_xml' : [XPU_CONFIG_FILE_XML]
  })


#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary(IMAGES_TZ, '${BUILDPATH}/AC', AC_SOURCES_RAW_TZ)

# For some reason AddBinaryLibrary causes HYP syscalls defined in code to not
# get linked into the HYP_WITH_TEST_IMAGE
#env.AddBinaryObject(IMAGES_HYP, AC_SOURCES_RAW_HYP)
env.AddBinaryLibrary(IMAGES_HYP, '${BUILDPATH}/AC', AC_SOURCES_RAW_HYP)

#-------------------------------------------------------------------------------
# Remove unused target files during the clean process
#-------------------------------------------------------------------------------
#Full_File_List=env.FindFiles(['*'], '${INC_ROOT}/core/buses/spmi/config/', posix=True)
#Full_File_List+=env.FindFiles(['*'], '${INC_ROOT}/core/buses/spmi/scripts/', posix=True)
#Using_File_List=env.FindFiles(['*'], '${INC_ROOT}/core/buses/spmi/config/${SPMI_MSM_ID}', posix=True)
#Using_File_List+=env.FindFiles(['pmic_arb_spmi_cfg_tzos.xml'], '${INC_ROOT}/core/buses/spmi/config/', posix=True)
#Using_File_List+=env.FindFiles(['*'], '${INC_ROOT}/core/buses/spmi/scripts/${SPMI_MSM_ID}', posix=True)
#Removed_File_List=list(set(Full_File_List).difference(Using_File_List))
#env.CleanPack(IMAGES, Removed_File_List)
