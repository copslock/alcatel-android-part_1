#===============================================================================
#
# qpay  build script
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2010 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/apps.tz/1.0.1/bsp/trustzone/qsapps/qpay/build/SConscript#2 $
#  $DateTime: 2016/01/12 15:07:25 $
#  $Author: pwbldsvc $
#  $Change: 9710929 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
#===============================================================================
import os
Import('env')

env = env.Clone()

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
# alias - First alias is always the target then the other possible aliases
aliases = ['qpay', 'qpay32', 'all' ]

env.InitImageVars(
   alias_list = aliases,       # list of aliases, unique name index [0]
   proc = 'scorpion',          # proc settings
   config = 'apps',            # config settings
   build_tags = ['APPS_PROC',
      'QPAY_IMAGE'],  # list of build tags for sub lib scripts
   tools = [
      "${BUILD_ROOT}/core/bsp/build/scripts/scl_builder.py",
      "${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py",
      "buildspec_builder.py",
      "${BUILD_ROOT}/tools/build/scons/sectools/sectools_builder.py",
   ],
)

if not env.CheckAlias():
   Return()

#------------------------------------------------------------------------------
# Init default values for this PROC/Image
#------------------------------------------------------------------------------

#===============================================================================
# qsapp build rules
#===============================================================================

#------------------------------------------------------------------------------
# Configure and load in USES and path variables
#------------------------------------------------------------------------------
env.InitBuildConfig()

#---------------------------------------------------------------------------
# Load in the tools scripts
#---------------------------------------------------------------------------
env.LoadToolScript("llvm", toolpath = ['${BUILD_SCRIPTS_ROOT}'])
env.LoadToolScript('apps_defs', toolpath = ['${BUILD_SCRIPTS_ROOT}'])

#------------------------------------------------------------------------------
# Add extension flags for qsapp
#------------------------------------------------------------------------------
if env['BUILD_VER'] == "":
   env.Replace(BUILD_VER = '0')

#----------------------------------------------------------------------------
# Enable Stack protection
#----------------------------------------------------------------------------
env.Append(CCFLAGS = " -fstack-protector")
env.Append(CCFLAGS = " -Wno-incompatible-library-redeclaration ")
env.Append(CFLAGS = "-Werror ")

# TODO Not sure why this is needed. qsapp must be added somewhere,
# otherwise a CPU without TZ instructions is being used.
# env.Replace(ARM_CPU_SCORPION = '7')
env.Append(CFLAGS = ' -fPIC') # ' --apcs=/ropi/rwpi --lower_ropi --lower_rwpi')
# To Do
#env.Append(ASFLAGS = ' -fPIC') # --apcs=/ropi/rwpi ')

env.Append(CFLAGS = "-Werror ")

env.Append(CPPDEFINES = [
   "QPAY",
   "BUILD_BOOT_CHAIN",
   "BUILD_BOOT_CHAIN_SPBL",
   "BOOT_LOADER",
   "BOOT_WATCHDOG_DISABLED",
   "FLASH_NAND_SINGLE_THREADED",
   "FLASH_CLIENT_BOOT",
   "FLASH_USE_DM_PAGES",
   "FEATURE_HS_USB_BASIC",
   "BOOT_SBL_H=\\\"boot_comdef.h\\\"",
   "BOOT_CUSTSBL_H=\\\"custsbl.h\\\"",
   "BOOT_MODULE_BUILD_VERSION=" + env['BUILD_VER'],
   "FEATURE_USES_TURBO",
   "RUMIBUILD",
])

#------------------------------------------------------------------------------
# Decide which build steps to perform
#------------------------------------------------------------------------------
# Regular build steps (no filter) is build everything, once a user starts
# using filters we have to make decisions depending on user input.
#
# The LoadAUSoftwareUnits function will take care of filtering subsystem, units,
# etc.  This is to take care of what steps to enable disable from the top level
# script, such as building files specify in this script i.e. quartz, stubs, etc.

do_local_files = True
do_link = True
do_post_link = True

# Get user input from command line
filter_opt = env.get('FILTER_OPT')

# Limit build processing based on filter option
if filter_opt is not None:
   do_link = False
   do_post_link = False

   if not env.FilterMatch(os.getcwd()):
      do_local_files = False

#-------------------------------------------------------------------------------
# Libraries Section
#-------------------------------------------------------------------------------
apps_libs, apps_objs = env.LoadAUSoftwareUnits('apps')
core_libs, core_objs = env.LoadAUSoftwareUnits('core')
qsapp_units = [core_objs, core_libs, apps_objs, apps_libs]

if do_local_files:
   #============================================================================
   # qsapp Environment
   #============================================================================

   #----------------------------------------------------------------------------
   # Begin building qsapp
   #----------------------------------------------------------------------------
   env.Replace(TARGET_NAME = 'qpay')
   env.Replace(QSAPP_ROOT = '${BUILD_ROOT}/apps/securemsm/trustzone/qsapps/${TARGET_NAME}')

   #----------------------------------------------------------------------------
   # Generate Scatter Load File (SCL)
   #----------------------------------------------------------------------------
   target_scl = env.SclBuilder('${SHORT_BUILDPATH}/${TARGET_NAME}',
      '${QSAPP_ROOT}/build/${TARGET_NAME}.ld')

   qsapp_units.extend(target_scl)

if do_link:
   #----------------------------------------------------------------------------
   # Generate qsapp ELF
   #----------------------------------------------------------------------------
   libs_path = env['INSTALL_LIBPATH']

   arm_libs = [
      File(env.SubstRealPath('${MUSL32PATH}/lib/libc.a')),
      File(env.SubstRealPath('${LLVMLIB}/libclang_rt.builtins-arm.a'))
   ]

   env.Append(LINKFLAGS = "-shared -Bsymbolic")
   # Extend core library to include standard libraries
   core_libs.extend(arm_libs)
   qsapp_elf  = env.Program('${SHORT_BUILDPATH}/${TARGET_NAME}',
      source=[core_objs, apps_objs], LIBS=[core_libs, apps_libs], LIBPATH=libs_path)

   env.Depends(qsapp_elf , target_scl)

   env.Clean(qsapp_elf , env.subst('${SHORT_BUILDPATH}/${TARGET_NAME}.map'))
   env.Clean(qsapp_elf , env.subst('${SHORT_BUILDPATH}/${TARGET_NAME}.sym'))

if do_post_link:
   #----------------------------------------------------------------------------
   # Generate qsapp MBN
   #----------------------------------------------------------------------------
   qsapp_pbn = env.InstallAs('${SHORT_BUILDPATH}/${TARGET_NAME}.pbn',
      qsapp_elf )

   install_root = env.subst('${MBN_ROOT}')
   image_name = "${TARGET_NAME}"
   install_unsigned_root = env.SectoolGetUnsignedInstallPath(install_base_dir = install_root)
   env.Replace(MBN_FILE = os.path.join(install_unsigned_root, image_name))

   qsapp_mbn = env.MbnBuilder('${SHORT_BUILDPATH}/${TARGET_NAME}',
      qsapp_pbn, IMAGE_TYPE="${TARGET_NAME}", MBN_TYPE="elf",
      IMAGE_ID=4, FLASH_TYPE="sdcc")

   #----------------------------------------------------------------------------
   # Sectools signing
   #----------------------------------------------------------------------------
   pil_base_dir = '${BUILD_ROOT}/build/ms/bin/PIL_IMAGES/SPLITBINS_${QC_SHORT_BUILDPATH}'
   sectools_signed_mbn = env.SectoolBuilder(
            target_base_dir = '${SHORT_BUILDPATH}',
            source=qsapp_mbn,
            sign_id=image_name,
            msmid = env.subst('${MSM_ID}'),
            sectools_install_base_dir = install_root,
            install_file_name = image_name + ".mbn",
            config='${BUILD_ROOT}/apps/bsp/trustzone/qsapps/build/secimage.xml',
            pilsplitter_target_base_dir=pil_base_dir)

   #-------------------------------------------------------------------------
   # Build files for PIL driver
   #-------------------------------------------------------------------------
   env.LoadToolScript('pil_splitter_builder', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
   unsigned_pil_dir = env.SectoolGetUnsignedInstallPath(install_base_dir = pil_base_dir)
   qsapp_pil = env.PilSplitterBuilder(os.path.join(unsigned_pil_dir, '${TARGET_NAME}.mdt'), qsapp_mbn)

   env.Depends(qsapp_pil, qsapp_mbn)

   #============================================================================
   # Define units that will be built
   #============================================================================
   qsapp_units = env.Alias ('arm11_qsapp_units', [
      qsapp_elf ,
      qsapp_mbn,
      qsapp_pil,
      sectools_signed_mbn
   ])

# Add aliases
for alias in aliases:
   env.Alias(alias, qsapp_units)
