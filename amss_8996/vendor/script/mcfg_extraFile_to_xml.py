#!/usr/bin/python

import sys
import os
import xml.dom.minidom as minidom
import struct


def usage():
    print "\n--------------------------------------------------------"
    print "Usage: \nPlease launch script with a nv xml path. Ex:"
    print sys.argv[0]+" xmlpath"
    print "\n"
    sys.exit(-1)

def parse_Group(buildpath):
    domgroup = minidom.parse(buildpath)
    GroupFileItems = domgroup.documentElement
    for groupfileitem in GroupFileItems._get_childNodes():
        if groupfileitem.nodeType == minidom.Node().ELEMENT_NODE:
            if groupfileitem.tagName == 'NvConfigurationData' and len(doc.getElementsByTagName('NvConfigurationData')) > 0:
                continue
            elif groupfileitem.tagName == 'GroupFile':
                parse_Group(groupfileitem.getAttribute('buildPath'))
            else:
                doc.lastChild.appendChild(groupfileitem)

def main():

    if len(sys.argv) < 2:
        print "\nPlease input one of nv xml."
        usage()

    impl = minidom.getDOMImplementation()
    global doc
    doc = impl.createDocument(None, "NvData", None)

    parse_Group(sys.argv[1])

    xmlStr = doc.toprettyxml(indent="", newl="", encoding='utf-8')
    xmlStr = xmlStr.replace('\t', '').replace('\n', '')
    dom = minidom.parseString(xmlStr)

    NvFileList = dom.getElementsByTagName("NvEfsFile")
    for nvfile in NvFileList:
        buildpath = nvfile.getAttribute('buildPath')
        try:
            fs = open(buildpath,'rb')
            c = fs.read()
            filesize = os.path.getsize(buildpath)
            if filesize < 0:
                print "ERROR: %s file size is invalid" % buildpath
                sys.exit(1)
            nvfile.setAttribute('filesize',str(filesize))
            data = struct.unpack(str(filesize)+'B',c)
            targetValue = ''.join(map(lambda x : "%02x" % x, data))
            nvfile.setAttribute('targetValue',targetValue)
        except IOError, err:
            print "Error: %s" % err
            sys.exit(1)
        except:
            print "Error when parse %s file." % buildpath
            sys.exit(1)
        finally:
            fs.close()

    NewXmlFile = os.path.splitext(sys.argv[1])[0]

    f = open(NewXmlFile+'_new.xml','w')
    dom.writexml(f, indent="\n", addindent="\t", encoding='utf-8')
    f.close()
    print "Generate: "+NewXmlFile+"_new.xml"


if __name__ == '__main__':
    main()
