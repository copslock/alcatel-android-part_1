#!/bin/bash

if [ -z $1 ] ; then
    echo "Didn't specify cpu chip type, exiting now ..."
    exit 1
fi

CPU_CHIP_TYPE=$1

buildid_8996=(8996.gen.prod AAAAANAAR IADAANAA AAAAAAAA AAAAAAAA)

if [ "$CPU_CHIP_TYPE" == "8996" ] ; then
    buildid=(${buildid_8996[@]})
    NON_CPU_CHIP_TYPE="8939"
else
    echo "CPU chip type error, exiting now..."
    exit 1    
fi

Modem_BuildId=${buildid[0]}
Rpm_BuildId=${buildid[1]}
Tz_BuildId=${buildid[2]}
adsp_BuildId=${buildid[3]}
slpi_BuildId=${buildid[4]}

TopPath=$(pwd)

CommBPath=$TopPath/common/build
if [ "$CPU_CHIP_TYPE" == "8996" ] ; then
    CommOPath=$TopPath/common/build/emmc
else
    echo "CPU chip type error, exiting now..."
    exit 1
fi

ModemOPath=$TopPath/modem_proc/build/ms/bin/$Modem_BuildId
BootOPath=$TopPath/boot_images/QcomPkg/Msm8996Pkg/Bin64
RpmOPath=$TopPath/rpm_proc/build/ms/bin/$Rpm_BuildId
TzOPath=$TopPath/trustzone_images/build/ms/bin/$Tz_BuildId
AdspOPath=$TopPath/adsp_proc/adsp_proc/build/dynamic_signed
CommTlPath=$TopPath/common/sectools/resources/build/fileversion2

copy_exit()
{
    local modname=$1
    echo "==================== Can't find one of $modname target files,please check ======================"
    exit 0
}

########--------prepareimage-------############

pushd $CommBPath > /dev/null

# modem images and elf backup
if [ -f "$CommOPath/bin/asic/NON-HLOS.bin" ] && [ -f "$CommOPath/bin/BTFM.bin" ]; then
    cp -f $CommOPath/bin/asic/NON-HLOS.bin ./NON-HLOS.bin
    cp -f $CommOPath/bin/BTFM.bin ./BTFM.bin
    if [ -f "$TopPath/modem_proc/build/ms/orig_MODEM_PROC_IMG_${Modem_BuildId}Q.elf" ]; then
        cp -f $TopPath/modem_proc/build/ms/orig_MODEM_PROC_IMG_${Modem_BuildId}Q.elf ./orig_MODEM_PROC_IMG_${Modem_BuildId}Q.elf
    fi
else
    copy_exit "modem"
fi

# boot images and elf backup
if [ -f "$BootOPath/xbl.elf" ] && [ -f "$BootOPath/pmic.elf" ]; then
    cp -f $BootOPath/xbl.elf ./xbl.elf
    cp -f $BootOPath/pmic.elf ./pmic.elf
    if [ -f "$TopPath/boot_images/Build/Msm8996_Loader/RELEASE_LLVMLINUX/AARCH64/QcomPkg/XBLLoader/XBLLoader/DEBUG/XBLLoader.dll" ]; then
        cp -f $TopPath/boot_images/Build/Msm8996_Loader/RELEASE_LLVMLINUX/AARCH64/QcomPkg/XBLLoader/XBLLoader/DEBUG/XBLLoader.dll ./XBLLoader.dll
    fi
    if [ -f "$TopPath/boot_images/Build/Msm8996_Loader/RELEASE_LLVMLINUX/AARCH64/QcomPkg/Msm8996Pkg/Library/XBLRamDumpLib/XBLRamDumpLib/DEBUG/XBLRamDump.dll" ]; then
        cp -f $TopPath/boot_images/Build/Msm8996_Loader/RELEASE_LLVMLINUX/AARCH64/QcomPkg/Msm8996Pkg/Library/XBLRamDumpLib/XBLRamDumpLib/DEBUG/XBLRamDump.dll ./XBLRamDump.dll
    fi
else
    copy_exit "boot"
fi

# rpm image and elf backup
if [ -f "$RpmOPath/rpm.mbn" ]; then
    cp -f $RpmOPath/rpm.mbn ./rpm.mbn
    if [ -f "$TopPath/rpm_proc/core/bsp/rpm/build/${CPU_CHIP_TYPE}/RPM_${Rpm_BuildId}.elf" ]; then
        cp -f $TopPath/rpm_proc/core/bsp/rpm/build/${CPU_CHIP_TYPE}/RPM_${Rpm_BuildId}.elf ./RPM_${Rpm_BuildId}.elf
    fi
else
    copy_exit "rpm"
fi

# trustzone images and elf bakcup
if [ -f "$TzOPath/tz.mbn" ] &&
   [ -f "$TzOPath/hyp.mbn" ] &&
   [ -f "$TzOPath/devcfg.mbn" ] &&
   [ -f "$TzOPath/keymaster.mbn" ] &&
   [ -f "$TzOPath/cmnlib.mbn" ] &&
   [ -f "$TzOPath/cmnlib64.mbn" ]; then
    cp -f $TzOPath/tz.mbn ./tz.mbn
    cp -f $TzOPath/hyp.mbn ./hyp.mbn
    cp -f $TzOPath/devcfg.mbn ./devcfg.mbn
    cp -f $TzOPath/keymaster.mbn ./keymaster.mbn
    cp -f $TzOPath/cmnlib.mbn ./cmnlib.mbn
    cp -f $TzOPath/cmnlib64.mbn ./cmnlib64.mbn
    if [ -f "$TopPath/trustzone_images/core/bsp/monitor/build/$Tz_BuildId/mon.elf" ]; then
        cp -f $TopPath/trustzone_images/core/bsp/monitor/build/$Tz_BuildId/mon.elf ./mon.elf
    fi
    if [ -f "$TopPath/trustzone_images/core/bsp/qsee/build/$Tz_BuildId/qsee.elf" ]; then
        cp $TopPath/trustzone_images/core/bsp/qsee/build/$Tz_BuildId/qsee.elf ./qsee.elf
    fi
    if [ -f "$TopPath/trustzone_images/core/bsp/hypervisor/build/$Tz_BuildId/hyp.elf" ]; then
        cp $TopPath/trustzone_images/core/bsp/hypervisor/build/$Tz_BuildId/hyp.elf ./hyp.elf
    fi
else
    copy_exit "tz"
fi

# adsp image and elf backup
if [ -f $AdspOPath/adspso.bin ]; then
    cp -f $AdspOPath/adspso.bin ./adspso.bin
    if [ -f "$TopPath/adsp_proc/adsp_proc/build/ms/M${CPU_CHIP_TYPE}${adsp_BuildId}Q1234.elf" ]; then
        cp $TopPath/adsp_proc/adsp_proc/build/ms/M${CPU_CHIP_TYPE}${adsp_BuildId}Q1234.elf ./M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234_adsp.elf
    fi
    if [ -f "$TopPath/adsp_proc/adsp_proc/build/ms/M${CPU_CHIP_TYPE}${adsp_BuildId}Q1234_AUDIO.elf" ]; then
        cp $TopPath/adsp_proc/adsp_proc/build/ms/M${CPU_CHIP_TYPE}${adsp_BuildId}Q1234_AUDIO.elf ./M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234_AUDIO.elf
    fi
else
    copy_exit "adsp"
fi

# slpi elf backup
if [ -f "$TopPath/slpi_proc/build/ms/M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234.elf" ]; then
    cp $TopPath/slpi_proc/build/ms/M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234.elf ./M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234.elf
fi
if [ -f "$TopPath/slpi_proc/build/ms/M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234_SENSOR.elf" ]; then
    cp $TopPath/slpi_proc/build/ms/M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234_SENSOR.elf ./M${CPU_CHIP_TYPE}${slpi_BuildId}Q1234_SENSOR.elf
fi

# sec file
if [ -f "$CommTlPath/sec.dat" ]; then
    cp -f $CommTlPath/sec.dat ./sec.dat
else
    copy_exit "sec.dat"
fi

# gpt and raw program
if [ -f "$CommOPath/gpt_backup0.bin" ] &&
   [ -f "$CommOPath/gpt_main0.bin" ] &&
   [ -f "$CommOPath/patch0.xml" ] &&
   [ -f "$CommOPath/rawprogram0.xml" ]; then
    cp -f $CommOPath/gpt_backup0.bin ./gpt_backup0.bin
    cp -f $CommOPath/gpt_main0.bin ./gpt_main0.bin
    cp -f $CommOPath/patch0.xml ./patch0.xml
    cp -f $CommOPath/rawprogram0.xml ./rawprogram0.xml
else
    copy_exit "gpt"
fi

popd > /dev/null
