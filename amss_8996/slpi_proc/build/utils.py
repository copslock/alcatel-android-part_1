#===============================================================================
#                    Copyright 2009 QUALCOMM Technologies Incorporated.
#                           All Rights Reserved.
#                         QUALCOMM Proprietary/GTDR
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/build.slpi/1.0/utils.py#10 $
#  $DateTime: 2015/10/28 15:48:36 $
#  $Change: 9310635 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
#   when       who            what, where, why
# --------   ---        ---------------------------------------------------------
#   09/29/15   sm        Merging in changes to delete 'obj' directory on clean and rebuild.
#   08/25/15   hw        Update python path to fix compiling issue on Linux
#   06/19/14   agurujee  Update split file name to SLPI
#   06/12/13   corinc    Script to call the QC-SCons build System for Badger    

#===============================================================================
import os
import fnmatch
import sys
import datetime
import shutil

import os
import subprocess
def create_timestamp(env):
   print "==================Creating timestamp and q6_build_version header files=================="
   print env['BUILD_VERSION']
   str_build_version = "#define QDSP6_BUILD_VERSION " + env.subst('$BUILD_VERSION') 
   file = open('./q6_build_version.h', 'w')
   file.write(str_build_version)
   file.close()
   
   timestamp_dir = os.getcwd()
   print "\n\n Time Stamp working directory is %s" % timestamp_dir

   current_time = datetime.datetime.now()
   str_timestamp_qcom = "const char qcom_timestampstring[] = \"QCOM time:" + current_time.strftime("Q6_BUILD_TS_%a_%b_%d_%H:%M:%S_PST_%Y_") + env.subst('$BUILD_VERSION') + "\";"
   if os.path.exists('./qcom_timestamp.h'):
      print "qcom_timestamp header file already generated from CRM build, check the build version from existing header file"
   else:
      file = open('./qcom_timestamp.h', 'w')
      file.write(str_timestamp_qcom)
      file.close()
   
   str_timestamp_engg = "const char engg_timestampstring[] = \"ENGG time:" + current_time.strftime("Q6_BUILD_TS_%a_%b_%d_%H:%M:%S_PST_%Y_") + env.subst('$BUILD_VERSION') + "\";"   
   file = open('./engg_timestamp.h', 'w')
   file.write(str_timestamp_engg)
   file.close()
   
#   current_time = date | sed 's/ /_/g'
#   file.write(current_time)

def init_chipset(env):
  
  #print env.Dump()
  print "current PLATFORM is " + env['PLATFORM']
  chipset=env.subst('$CHIPSET')
  print chipset
  build_flavor=env.subst('$BUILD_FLAVOR')
  print build_flavor
  build_act=env.subst('$BUILD_ACT')
  print build_act
  build_flags=env.subst('$BUILD_FLAGS')
  print build_flags
  build_verbose=env.subst('$BUILD_VERBOSE')
  print build_verbose
  build_component=env.subst('$BUILD_COMPONENT')
  print build_component
  build_filter=env.subst('$BUILD_FILTER')
  print build_filter
  build_sconsargs=env.subst('$BUILD_SCONSARGS')
  print build_sconsargs  
  

  if env['PLATFORM'] == 'posix':
     env.Replace(SCONS_CMD_SCRIPT = "python build.py")
  else:
     env.Replace(SCONS_CMD_SCRIPT = "python build.py")

  if build_flavor=='spd':
     if chipset=='msm8x26':
        env.Replace(IMAGE_ALIAS = "msm8974_SPD")
     elif chipset=='msm8962':
        env.Replace(IMAGE_ALIAS = "apq8084_SPD")
     else:   
        env.Replace(IMAGE_ALIAS = "${CHIPSET}_SPD")
  elif build_flavor=='mpd':
     if chipset=='msm8x26':
        env.Replace(IMAGE_ALIAS = "msm8974_MPD")
     elif chipset=='msm8962':
        env.Replace(IMAGE_ALIAS = "apq8084_MPD")
     else:
        env.Replace(IMAGE_ALIAS = "${CHIPSET}_MPD")   
  else:
     print "Unknown BUILD_FLAVOR!!!"
     sys.exit(0)

  if build_component:
     build_component = build_component.replace(',', ' ')
     env.Replace(IMAGE_ALIAS = build_component)
    
  if build_filter:
     env.Replace(SCONS_FLAVOR_ACT_CMD_FILTER = "${SCONS_CMD_SCRIPT} ${IMAGE_ALIAS} --filter="+build_filter)    
     scons_flavor_act_cmd_filter=env.subst('$SCONS_FLAVOR_ACT_CMD_FILTER')     
     env.Replace(SCONS_CMD = scons_flavor_act_cmd_filter+" BUILD_ID=AAAAAAAA BUILD_VER=1234 -j 8")
  else:
     env.Replace(SCONS_CMD = "${SCONS_CMD_SCRIPT} ${IMAGE_ALIAS} BUILD_ID=AAAAAAAA BUILD_VER=1234 -j 8")
  
  if build_verbose:
     env.Replace(SCONS_FLAVOR_CMD = "cd build/ms && ${SCONS_CMD} --verbose="+build_verbose)
  else:
     env.Replace(SCONS_FLAVOR_CMD = "cd build/ms && ${SCONS_CMD} --verbose=1")
     
  if build_sconsargs:
     opts_sconsargs = build_sconsargs.replace(',', ' ')
     env.Replace(SCONS_FLAVOR_CMD_ARGS = "${SCONS_FLAVOR_CMD} "+opts_sconsargs)
     scons_flavor_act_cmd_args=env.subst('$SCONS_FLAVOR_CMD_ARGS')
     env.Replace(SCONS_FLAVOR_CMD = scons_flavor_act_cmd_args)
  if os.path.exists('./obj'):
      splitelf_dir = './obj'
      shutil.rmtree(splitelf_dir)
  if build_act=='clean':
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD} -c")
     print "clean engg_timestamp"
     engg_timestamp_file = './engg_timestamp.h'
     os.remove(engg_timestamp_file)
     print "clean split images"
  elif build_act=='SIM':
     print "for build_act is SIM, pass SIM=1 to SCons build cmd"
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD} SIM=1")

     print "generating osam cfg file"
     #str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     #import pdb; pdb.set_trace() 
     if build_flags != 'CHECKSIMBOOT':
        if env.get('PLATFORM') in ["Windows_NT","windows","win32","cygwin"] :
           #This is a hack as pw works on windows and testing is done on linux
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"       
        else:
           print "sds"
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     else:
        if env.get('PLATFORM') in ["Windows_NT","windows","win32","cygwin"] :
           str_osam_path = "..\core\kernel\qurt\install_again\ADSPv5MP\debugger\cygwin\qurt_model.dll"       
        else:
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     print str_osam_path      
     #import pdb; pdb.set_trace()       
     dir_obj_ReleaseG = './obj/qdsp6v5_ReleaseG/'
     if not os.path.exists(dir_obj_ReleaseG):
        os.makedirs(dir_obj_ReleaseG)
     file = open(dir_obj_ReleaseG + 'osam.cfg', 'w')
     file.write(str_osam_path)
     file.close()
  elif build_act=='klocwork':
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD}")
  else:
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD}")
  
  if build_flags:
     #env.Replace(SCONS_FLAVOR_ACT_CMD_FLAGS = "${SCONS_FLAVOR_ACT_CMD} USES_FLAGS="+build_flags)
     env.Replace(SCONS_FLAVOR_ACT_CMD_FLAGS = "${SCONS_FLAVOR_ACT_CMD}")
     scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD_FLAGS')
     env.Replace(SCONS_FLAVOR_ACT_CMD = scons_flavor_act_cmd)
 

def execute_buildcmd(env):
   print "==================delete the elfs from previous build if exist=================="
   dspelf_file = './slpi.elf'
   if os.path.exists(dspelf_file):
      os.remove(dspelf_file)
   adsp_link_dir = './build/bsp/slpi_root/build/AAAAAAAA/'
   if os.path.exists(adsp_link_dir):
      adsp_link_files = os.listdir(adsp_link_dir)
      print adsp_link_files
      os.chdir(adsp_link_dir)
      for file in adsp_link_files:
         os.remove(file)
      os.chdir('../../../../..')

   print "==================execute the build startup command=================="
   print "print the SCONS_FLAVOR_ACT_CMD in env"
   print env['SCONS_FLAVOR_ACT_CMD']
   scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
   print "print the SCONS_FLAVOR_ACT_CMD command when use it"
   print scons_flavor_act_cmd
   cwd_dir = os.getcwd()
   print "\n\nexecute the build working directory is %s" % cwd_dir
   os.system(scons_flavor_act_cmd)
   cwd_dir = os.getcwd()
   print "\n\nCurrent working directory is %s" % cwd_dir

def split_proc(env):
   # If .\slpi.elf exists, run pil-splitter.py on binaries.
   if os.path.exists('./obj/qdsp6v5_ReleaseG/slpi.mbn'):
		# Define directories that were generated by sectools_builder.py.
		if env['PLATFORM'] == 'posix':
			sectool_images = [
				os.path.dirname('/signed/firmware/image/'),
				os.path.dirname('/unsigned/firmware/image/'),
				os.path.dirname('/signed_encrypted/firmware/image/')
			]
			object_dir = os.path.dirname("obj/qdsp6v5_ReleaseG/")
			slpimbn = "/slpi.mbn";
			pythonbin = "/pkg/qct/software/python/2.7.6/bin/python"; 
		else:
			sectool_images = [
				os.path.dirname('./signed/firmware/image/'),
				os.path.dirname('./unsigned/firmware/image/'),
				os.path.dirname('./signed_encrypted/firmware/image/')
			]
			object_dir = os.path.dirname("./obj/qdsp6v5_ReleaseG/")
			slpimbn = "\slpi.mbn";
			pythonbin = "python"
		# Get the current directory.
		slpi_root_dir = os.getcwd()
		for binary_dir in sectool_images:
         # Check if the directory exist.  If not, create it.
			if not os.path.exists(object_dir + binary_dir):
				os.makedirs(object_dir + binary_dir)
         
         # Run pil-splitter.py on the requested binary.  Change directory to 
         # the binary directory, then issue:
         # "python [Location of pil-splitter.py] [Location of MBN] slpi".
			print "**INFO: Executing pil-splitter.py on " + binary_dir + " binaries."
			split_cmd = "cd " + slpi_root_dir + "/" + object_dir + binary_dir + "/ && python " + slpi_root_dir + "/qdsp6/scripts/pil-splitter.py " + slpi_root_dir + "/" + object_dir + slpimbn + " slpi"
			os.system(split_cmd)

   # If .\slpi.elf does not exist, exit.
   else:
      print "Unable to find slpi.elf.  Exiting."
      sys.exit(0)
	 
def elf_extractor_proc(env):
   print "================== Running hexagon elf extractor script ==================="
   env.Replace(ELF_EXTRACTOR_PROC_CMD = "python ./qdsp6/scripts/hexagon_elf_extractor.py --target=adsp --elf=./build/bsp/slpi_root/build/AAAAAAAA/SLPI_ROOT_AAAAAAAAQ.elf")
   elf_extractor_proc_cmd=env.subst('$ELF_EXTRACTOR_PROC_CMD')
   os.system(elf_extractor_proc_cmd)

def memory_proc(env):

   build_flavor=env.subst('$BUILD_FLAVOR')
   chipset_name=env.subst('$CHIPSET')
   target_name=chipset_name[3:]
   tools_version_used = os.environ.get('HEXAGON_RTOS_RELEASE', None)   
   tools_major_version_used = int(tools_version_used[0])      
   elf_name_normal="".join(['M',target_name,'AAAAAAAAQ1234.elf'])
   elf_name_normal=os.path.join("./build/ms/",elf_name_normal)
   
   elf_name_sensor="".join(['M',target_name,'AAAAAAAAQ1234_SENSOR.elf'])
   elf_name_sensor=os.path.join("./build/ms/",elf_name_sensor)   


   print "================== Running memory profile script ==================="
   if build_flavor=='spd':
          env.Replace(MEMORY_PROC_CMD2 = "python ./qdsp6/scripts/show_memory_qurt.py ./build/bsp/slpi_root/build/AAAAAAAA/SLPI_ROOT_AAAAAAAAQ.elf ${CHIPSET}")   
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD2')
          show_memory_qurt_flag = os.system(memory_proc_cmd2)     
   elif build_flavor=='mpd':
          env.Replace(MEMORY_PROC_CMD2 = "python ./qdsp6/scripts/show_memory_qurt.py ./build/bsp/multi_pd_img/build/AAAAAAAA/bootimage.pbn ${CHIPSET} %s"%(tools_major_version_used))       
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD2')
          os.system(memory_proc_cmd2)
   if os.path.exists('./build/bsp/slpi_root/build/AAAAAAAA/SLPI_ROOT_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/Image_Break_Down.py ${CHIPSET} ./build/bsp/slpi_root/build/AAAAAAAA/SLPI_ROOT_AAAAAAAAQ.elf.map CommonPD %s"%(tools_major_version_used))   
          memory_proc_cmd1=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd1)
   if os.path.exists('./build/bsp/ssc_slpi_user/build/AAAAAAAA/SSC_SLPI_USER_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/Image_Break_Down.py ${CHIPSET} ./build/bsp/ssc_slpi_user/build/AAAAAAAA/SSC_SLPI_USER_AAAAAAAAQ.elf.map SensorsPD %s"%(tools_major_version_used))   
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd2)
   if chipset_name!='msm8996':
    if env['PLATFORM'] == 'posix':
      env.Replace(HEXAGON_ANALYZER_BACKEND_CMD = "hexagon-analyzer-backend")
    else:
      env.Replace(HEXAGON_ANALYZER_BACKEND_CMD = "hexagon-analyzer-backend.exe")

   if os.path.exists('./build/bsp/slpi_root/build/AAAAAAAA/SLPI_ROOT_AAAAAAAAQ.elf.map'):
      env.Replace(MEMORY_PROC_CMD3 = ("$HEXAGON_ANALYZER_BACKEND_CMD --aidfile ./qdsp6/tools/input/libcAidFile.txt --elffile %s -o ./build/ms > ./build/ms/hexagon_StackAnalyzer_Log.txt "%elf_name_normal))
   memory_proc_cmd3=env.subst('$MEMORY_PROC_CMD3')
   os.system(memory_proc_cmd3)
         
   if os.path.exists('./build/ms/RA_FunctionStackSizes.csv'):
		if os.path.exists('./build/ms/RA_FunctionStackSizes_normal.csv'):
				os.remove('./build/ms/RA_FunctionStackSizes_normal.csv')
		os.renames('./build/ms/RA_FunctionStackSizes.csv','./build/ms/RA_FunctionStackSizes_normal.csv')
		  
   if os.path.exists('%s'%elf_name_sensor):
		env.Replace(MEMORY_PROC_CMD4 = ("$HEXAGON_ANALYZER_BACKEND_CMD --aidfile ./qdsp6/tools/input/libcAidFile.txt --elffile %s -o ./build/ms > ./build/ms/hexagon_StackAnalyzer_Log.txt "%elf_name_sensor))
		memory_proc_cmd4=env.subst('$MEMORY_PROC_CMD4')
		os.system(memory_proc_cmd4)
		
   os.system("python ./qdsp6/tools/stack_size.py %s %s"%(build_flavor,elf_name_sensor))	


#Initialize environmentn 
def Init(env):

   os.chdir('../')
   env_init_dir = os.getcwd()
   print "\n\n Env init working directory is %s" % env_init_dir

   #InitChipset method
   env.AddMethod(init_chipset,"InitChipset")

   #create timestamp
   env.AddMethod(create_timestamp, "CreateTimestamp")

   #execute the build startup command
   env.AddMethod(execute_buildcmd, "ExecuteBuildCmd")

   #execute the split_proc command
   env.AddMethod(split_proc, "SplitProc")

   #execute the memory_proc command
   env.AddMethod(memory_proc, "MemoryProc")

   #execute the hexagon_elf_extractor command
   env.AddMethod(elf_extractor_proc, "ElfExtractorProc")
    
   env.AddMethod(check_sim_boot, "CheckSimBoot")
def check_sim():
   print "Starting sim command"
   sim_cmd="cd avs && qdsp6-sim --simulated_returnval --mv5c_256 ../slpi.elf --simulated_returnval --rtos ../obj/qdsp6v5_ReleaseG/osam.cfg --symfile ../build/ms/M8974AAAAAAAAQ1234_reloc.elf --symfile ../build/ms/M8974AAAAAAAAQ1234_SENSOR_reloc.elf --cosim_file ./q6ss.cfg"
   os.system(sim_cmd)
   sys.exit(0)


def check_sim_boot(env):
   path =".."
   name="check_sim_boot.txt"
   full_path = os.path.join(path, name)
   print "Starting sim command"
   sim_cmd='cd avs && qdsp6-sim --simulated_returnval --mv5c_256 ../slpi.elf --simulated_returnval --rtos ../obj/qdsp6v5_ReleaseG/osam.cfg --symfile ../build/ms/M8974AAAAAAAAQ1234_reloc.elf --symfile ../build/ms/M8974AAAAAAAAQ1234_SENSOR_reloc.elf --cosim_file ./q6ss.cfg  > %s 2>&1' % full_path
   os.system(sim_cmd)
   if os.path.isfile('check_sim_boot.txt'):
       f=open('check_sim_boot.txt', 'r')
       check_msgs=['CreateEliteStaticService Begin','CreateEliteStaticService End','StartEliteStaticService start','StartEliteStaticService end','Ratio to Real Time']
       count=0
       for msg in check_msgs:
          f.seek(0, 0)
          count =0
          for line in f:
             if msg in line:
                count=count+1
          if count == 0:
             print msg+"not created"
             print "Failed in testing the sim image, check check_sim_boot.txt for more info"
             sys.exit(-1)
       print "======================= Simulation Tests Passed  =========================="
   else:
       print "======== check_sim_boot.txt is not found ================"
       sys.exit(-1)
  
