#===============================================================================
#
# QMIMSGS SLPI Root SCons Image
#
# GENERAL DESCRIPTION
#    This is the Image SCons to generate the QMIMSGS Root PD *.ELF, specific to Sensors.
#
# Copyright (c) 2009-2016 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/build.slpi/1.0/bsp/qmimsgs_root_libs/build/qmimsgs_root_libs.scons#7 $
#  $DateTime: 2016/01/29 15:46:51 $
#  $Change: 9815294 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 26/01/16   ps      Add 8998_MPD support.
# 03/23/15   sm      Update ListFileBuilder API to AddListFile.
# 02/06/15   sm      Update to allow SPD and MPD core only compilation.
# 01/20/15   sm      Add new alias tag to enable core only compilation.
# 12/15/14   sm      Removed 'adsp' from the configuration.
# 10/04/14   sm      Removed kernel configuration from scripts.
# 09/24/14   sm      Cleaned up revision for 8996.
#
#===============================================================================

Import('env')
import os
import string

#------------------------------------------------------------------------------
# Init image vars 
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Init aliases array.
# first alias (0th elemten in array) is always unique name, there should 
# never be two images with the same unique name
aliases = ['qmimsgs_root_libs', 'msm8996_MPD', 'msm8998_MPD', 'SPD_core_drivers_only','MPD_core_drivers_only']

#------------------------------------------------------------------------------
# Init environment variables

# Define the builders required for this image.
build_tools = ['buildspec_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/swe_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/sleep_lpr_builder.py']

# Define the build tags defined for this image.
build_tags = ['QMIMSGS_ADSP', 'QMIMSGS_SLPI_ROOT']

env.InitImageVars(
   alias_list=aliases,
   proc='qdsp6',
   config='slpi',
   plat='qurt',
   build_tags = build_tags,
   tools = build_tools
)

# Append a 'Q' to the Build ID accordingly.
env.Replace(BUILD_ID=env.subst('${BUILD_ID}Q'))
env.Replace(CUST_H=string.lower(env.subst('CUST${BUILD_ID}.H')))

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
if not env.CheckAlias():
   Return()

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.InitBuildConfig()

# Set USES Flags.
env.Replace(USES_DEVCFG = 'yes')
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_IMG')
env.AddUsesFlags('USES_MPD')
env.AddUsesFlags('USES_QURTOS_IMG') #this is the flag to be used for mpd builds to compile for guest os
env.Append(CPPDEFINES="SENSOR_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_SENSOR.pbn\\\"")
env.Append(CPPDEFINES="AUDIO_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO.pbn\\\"")
env.Append(CPPDEFINES=["MULTI_PD_BUILD"])
env.AddUsesFlags('USES_RCINIT_PLAYBOOK')

#---------------------------------------------------------------------------
# Load in the tools-set init scripts
#---------------------------------------------------------------------------

#Add ADSP Image specific linker inputs here
env.LoadToolScript('hexagon_adsp', toolpath = ['${BUILD_ROOT}/build'])


if 'USES_MALLOC_WRAPPER_ENABLED' in env:
    env.Append(CFLAGS='-DMALLOC_WRAPPER ')

#---------------------------------------------------------------------------
# Libs/Objs
#---------------------------------------------------------------------------
image_libs = []
image_objs = []

#---------------------------------------------------------------------------
# Libraries Section
#---------------------------------------------------------------------------

# Load the 'qmimsgs' SCons files.
if env.PathExists("${BUILD_ROOT}/qmimsgs"):
   au_items = env.LoadAreaSoftwareUnits('qmimsgs')
   image_libs.extend(au_items['LIBS'])
   image_objs.extend(au_items['OBJS'])

# Add the libraries and objects to the image.
image_units = [image_objs, image_libs]

#--- RCINIT Playbook Extension, Library Specific Details -------------------

PLAYLISTS = [ ] # NONE USED

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${SHORT_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_RCINIT' in env and 'USES_RCINIT_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYLIST OUTPUT THIS LIBRARY
   rcinit_out_rcpl = playbook_env.RealPath('${SHORT_BUILDPATH}/rcinit_playlist.rcpl')
   playbook_env.AddRCInitPlaylist(build_tags, rcinit_out_rcpl)
   playbook_env.AddArtifact(build_tags, rcinit_out_rcpl)
   playbook_env.Depends(build_tags, rcinit_out_rcpl)     # Manage explicit detail outside of AU
   image_units.append(rcinit_out_rcpl)                   # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS LIBRARY
   rcinit_out_txt = playbook_env.RealPath('${SHORT_BUILDPATH}/rcinit_playlist.txt')
   playbook_env.AddRCInitPlaybook(build_tags, rcinit_out_txt, None)
   playbook_env.AddArtifact(build_tags, rcinit_out_txt)
   playbook_env.Depends(rcinit_out_txt, rcinit_out_rcpl) # Manage explicit detail outside of AU
   image_units.append(rcinit_out_txt)                    # Manage explicit detail outside of AU

#--- RCINIT Playbook Extension, Library Specific Details -------------------

#------------------------------------------------------------------------------
# Putting the image together
#------------------------------------------------------------------------------

if 'IMAGE_BUILD_LOCAL_FILES' in env:
   #-------------------------------------------------------------------------
   # Local Files
   #-------------------------------------------------------------------------
   
   # this is where local files are created, for example link scripts (lcs)
   # for qdsp6 like images, or scatter load files (scl) for amr like images.
   local_itmes= []
   
   image_units += local_itmes
      
if 'IMAGE_BUILD_LINK' in env:
   #-------------------------------------------------------------------------
   # Link image
   #-------------------------------------------------------------------------
   
   # Generate the Listing File for this SCons Image.
   image_lf = env.AddListFile("${SHORT_BUILDPATH}/${TARGET_NAME}.lf", image_objs, LIBS=image_libs,
         add_header=False, relative_path="${BUILD_ROOT}/build/ms", posix=True)
   
if 'IMAGE_BUILD_POST_LINK' in env:
   #-------------------------------------------------------------------------
   # Post process image
   #-------------------------------------------------------------------------
   
   # this is where any aditional rules after linking are done.

   #=========================================================================
   # Define targets needed 
   #
   image_units += [
      image_lf,
   ]

#=========================================================================
# Finish up...
env.BindAliasesToTargets(image_units)
