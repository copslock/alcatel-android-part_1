#===============================================================================
#
# CoreBSP HEXAGON tool rules
#
# GENERAL DESCRIPTION
#    HEXAGON Tools definitions
#
# Copyright (c) 2009-2016 by QUALCOMM Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/build.slpi/1.0/hexagon_adsp.py#9 $
#  $DateTime: 2016/03/23 13:00:40 $
#  $Author: pwbldsvc $
#  $Change: 10127679 $
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who         what, where, why
# --------   ---         ---------------------------------------------------------
# 03/22/16   rp          Remove stack protector compiler option for 8998.
# 02/19/16   sm          Add platform_root_libs back into 8996 and 8998.
# 08/25/15   hw          Add -D switch for fixing a customer compiling issue
# 05/08/14   corinc      Check the tool version for the correct bin path
#===============================================================================
import sys
import os
import string
import SCons.Action
from SCons.Script import *


   
#------------------------------------------------------------------------------
# Hooks for Scons
#------------------------------------------------------------------------------
def exists(env):
   return env.Detect('hexagon_adsp')

def generate(env):
   #===============================================================================
   # load hexagon.py to environment
   #===============================================================================
   env.Tool('hexagon', toolpath = ['${BUILD_ROOT}/tools/build/scons/scripts'])

   #===============================================================================
   # figure out what tools set will be using
   #===============================================================================
   # The following environment variables must be defined prior to using this make
   # file: GNUPATH, QCTOOLS. In addition the PATH must be updated for
   # the GCC tools.

   hexagon_root = os.environ.get('HEXAGON_ROOT', None)   
   hexagon_image_entry = os.environ.get('HEXAGON_IMAGE_ENTRY', '0xf0000000')   
   hexagon_rtos_release = os.environ.get('HEXAGON_RTOS_RELEASE', None)
   q6version = os.environ.get('HEXAGON_Q6VERSION', None)
   hexagon_major_tool_ver = int(hexagon_rtos_release[0])
      
   # init variables to environment
   env.Replace(QDSP6_RELEASE_DIR = "${HEXAGON_ROOT}/${HEXAGON_RTOS_RELEASE}")
   
   if not env.PathExists("${QDSP6_RELEASE_DIR}"):
      env.PrintError("-------------------------------------------------------------------------------")
      env.PrintError("*** HEXAGON tools are not present")
      env.PrintError("*** Please install HEXAGON tools")
      env.PrintError("*** Currently setup for RTOS: ${HEXAGON_RTOS_RELEASE}")
      env.PrintError("*** Missing RTOS PATH: ${QDSP6_RELEASE_DIR}")
      env.PrintError("*** Recommended locations:")
      env.PrintError("***      Windows: C:\Qualcomm\HEXAGON_Tools")
      env.PrintError("***      Linux: HOME/Qualcomm/HEXAGON_Tools (OR) /pkg/qct/software/hexagon/releases/tools")
      env.PrintError("***      Note: If installed at other location, please update \'Software Paths & other definitions\' section in \'build.py\' accordingly")
      env.PrintError("***            OR set HEXAGON_ROOT and HEXAGON_RTOS_RELEASE environment variables accordingly")
      env.PrintError("***               Eg: set HEXAGON_ROOT=C:\Qualtools\Hexagon")
      env.PrintError("***                   set HEXAGON_RTOS_RELEASE=5.1.04")
      env.PrintError("-------------------------------------------------------------------------------")
      Exit(1)

   #----------------------------------------------------------------------------
   # common defines for all components
   #----------------------------------------------------------------------------

   env.Append(CFLAGS='-D__qdsp6__=1')
   env.Append(ARFLAGS = ' -D ')
   env.Append(CFLAGS = '-G0')

# Include the following include path for 8996.  The latest kernel release for 8998
   # no longer requires this flag.
   if env['CHIPSET'] in ['msm8996']:
      env.Append(CFLAGS='-fstack-protector')

   if 'BUILD_BAREBONE' in env:
      env.Append(CFLAGS='-DBAREBONE_ADSP=1')

   if 'BUILD_SLPI' in env:
      env.Append(CFLAGS='-DSENSOR_LPI=1')
   if not env.CheckAlias(alias_list=['MPD_core_drivers_only','SPD_core_drivers_only']): 
      env.AddUsesFlags('USES_PLATFORM_FS')
    
   if ARGUMENTS.get('SIM') == "1" or ARGUMENTS.get('SIM') == "TRUE":
      env.AddUsesFlags(['USES_AVS_TEST', 'USES_SENSORS_INTEGRATION_ENABLED'])
   else:
      env.AddUsesFlags(['USES_ADSPPM_INTEGRATION_ENABLED', 'USES_SENSORS_INTEGRATION_ENABLED','USES_VIDEO_INTEGRATION_ENABLED'])

   #-------------------------------------------------------------------------------
   # HEXAGON LINK, very special procedure
   #-------------------------------------------------------------------------------

   if 'USES_MEMOPT' in env:
      env.Replace(CHIPSET_DIR = "${CHIPSET}MEMOPT")
   else:
      env.Replace(CHIPSET_DIR = "${CHIPSET}")
   
   #Add ADSP Image specific linker inputs here
   env.LoadToolScript('qdsp6_defs_adsp', toolpath = ['${BUILD_ROOT}/build/ms'])
   #G0 flag is required so that it is uniform. kernel libs are compiled with G0
   if hexagon_major_tool_ver <= 6:
      env.Replace(QDSP6_RELEASE_LIB_DIR="${QDSP6_RELEASE_DIR}/dinkumware/lib/${Q6VERSION}/G0")
      env.Replace(LINKER_FILE="elite.linker")
#      env.Replace(QDSP6BIN = "${QDSP6_RELEASE_DIR}/gnu/bin")
#      print 'QDSP6_RELEASE_LIB_DIR = dinkumware/lib'
   else: # LLVM (version >= 7) path for the Q6 released library path
      env.Replace(QDSP6_RELEASE_LIB_DIR="${QDSP6_RELEASE_DIR}/Tools/target/hexagon/lib/${Q6VERSION}/G0")
      env.Replace(LINKER_FILE="elite_llvm.linker")
      env.Replace(QDSP6BIN = "${QDSP6_RELEASE_DIR}/Tools/bin")
      # temporary adding the hexgon warning flags for LLVM, -Wno-cast-align
      env.Replace(HEXAGON_WARN = "-Wall -Wpointer-arith -Wno-cast-align")
#      print 'QDSP6_RELEASE_LIB_DIR = Tools/target/hexagon'
   # builder to support dynamic linking 
   if 'USES_PLATFORM_FS' in env:
      env.LoadToolScript('platform_builders', toolpath = ['${BUILD_ROOT}/platform/build'])

   #-------------------------------------------------------------------------------
   # Software tool and environment definitions
   #-------------------------------------------------------------------------------

   env.PrintInfo("Print from hexagon_adsp.py version")
   env.PrintInfo("HEXAGON_TOOLS       = ${HEXAGON_TOOLS}")
   env.PrintInfo("HEXAGON_RTOS_REL    = ${HEXAGON_RTOS_RELEASE}")
   env.PrintInfo("Q6VERSION           = ${Q6VERSION}")      
   env.PrintInfo("HEXAGON_ROOT        = %s" % env.RealPath("${HEXAGON_ROOT}"))
   env.PrintInfo("QDSP6BIN            = ${QDSP6BIN}")
   env.PrintInfo("HEXAGON_IMAGE_ENTRY = ${HEXAGON_IMAGE_ENTRY}")   
   #============================================================================
   #print env.Dump()
