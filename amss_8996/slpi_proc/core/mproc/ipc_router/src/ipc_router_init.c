/*===========================================================================

                    I P C    R O U T E R   I N I T

   This file does the node and OS specific initialization of IPC Router
   and sets up all the links.

  ---------------------------------------------------------------------------
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.slpi/1.0/mproc/ipc_router/src/ipc_router_init.c#4 $
$DateTime: 2015/04/29 09:04:31 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
==========================================================================*/

/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include "ipc_router_core.h"
#include "ipc_router_xal.h"
#include "ipc_router_xal_smdl.h"
#include "ipc_router_xal_glink.h"
#include "DALSys.h"
#include "DALSysTypes.h"

/* This file is just a template. Each target must maintain its own version of ipc_router_init.c */

#define IPC_ROUTER_DEFAULT_PROCESSOR_ID 5

#define IPC_ROUTER_XAL_GLINK_PRIO 150
#define IPC_ROUTER_XAL_GLINK_STACK_SIZE (1024 * 4)

extern void ipc_router_qdi_init(void);

static int ipc_router_inited = 0;

static unsigned int ipc_router_get_processor_id(void)
{
  DALSYS_PROPERTY_HANDLE_DECLARE(hSpmDevCfg);
  DALSYSPropertyVar prop;
  DALResult result;

  result = DALSYS_GetDALPropertyHandleStr("/dev/core/mproc/ipc_router", hSpmDevCfg);
  if(result != DAL_SUCCESS)
  {
    return IPC_ROUTER_DEFAULT_PROCESSOR_ID;
  }
  result = DALSYS_GetPropertyValue( hSpmDevCfg, "local_processor_id", 0, &prop );
  if(result != DAL_SUCCESS)
  {
    return IPC_ROUTER_DEFAULT_PROCESSOR_ID;
  }
  return (unsigned int)prop.Val.dwVal;
}

extern void qsocket_init(void);

void ipc_router_init(void)
{
  if(ipc_router_inited)
    return;
  ipc_router_inited = 1;
  /* Initialize router and start transports */
  ipc_router_core_init(ipc_router_get_processor_id());

  qsocket_init();

#ifdef FEATURE_IPC_ROUTER_QDI_DRIVER
  ipc_router_qdi_init();
#endif

  /* Link to Apps */
  {
    static ipc_router_xal_smdl_param_type param = {"IPCRTR", 
                                                   SMD_APPS_SSC, 
                                                   SMD_STANDARD_FIFO,
                                                   SMDL_OPEN_FLAGS_MODE_PACKET, 
                                                   FALSE};
    ipc_router_xal_start_xport(&ipc_router_xal_smdl, (void *)&param, 0, "APPS");
  }

  /* Link to Modem */
  {
    static ipc_router_xal_glink_param_type param = {
      "SMEM", "mpss", "IPCRTR", 0, 
      IPC_ROUTER_XAL_GLINK_PRIO,
      IPC_ROUTER_XAL_GLINK_STACK_SIZE,
      {{128,16}, {512, 8}, {1024+128, 5}, {0, 0}}};

    ipc_router_xal_start_xport(&ipc_router_xal_glink, (void *)&param, 0, "MPSS");
  }

  /* Link to Lpass */
  {
    static ipc_router_xal_glink_param_type param = {
      "SMEM", "lpass", "IPCRTR", 0, 
      IPC_ROUTER_XAL_GLINK_PRIO,
      IPC_ROUTER_XAL_GLINK_STACK_SIZE,
      {{128,16}, {512, 8}, {1024+128, 5}, {8192+128, 1}}};

    ipc_router_xal_start_xport(&ipc_router_xal_glink, (void *)&param, 0, "ADSP");
  }
}

