#===============================================================================
#
# G-LINK SSR Component Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Qualcomm Technologies, Inc. Confidential and Proprietary.
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.slpi/1.0/mproc/glink/ssr/build/glink_ssr.scons#6 $
#  $DateTime: 2015/07/10 09:31:08 $
#  $Author: pwbldsvc $
#  $Change: 8562913 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 03/05/15   bc      Update build tag from deprecated ones
# 12/11/14   rs      Support link notifications
# 07/27/14   rs      Initial version for G-Link SSR component.
#
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
if env.IsTargetEnable('MISSIONROM_IMAGE'):
  GLINK_BUILD_ROOT = "${BUILD_ROOT}/missionrom/core/mproc/glink"
else:
  GLINK_BUILD_ROOT = "${BUILD_ROOT}/core/mproc/glink"

SRCPATH = GLINK_BUILD_ROOT + "/ssr/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
#  Publish Private APIs
#-------------------------------------------------------------------------------

# Images that will have full glink features
FULL_GLINK_FEAUTRE_IMAGES = ['CORE_SLPI_ROOT', 'CORE_ADSP_ROOT', 'CORE_MPSS_ROOT',
                             'CBSP_APPS_IMAGE', 'APPS_IMAGE']

OS_INC_PATH = GLINK_BUILD_ROOT+"/os/inc"
CORE_INC_PATH = GLINK_BUILD_ROOT+"/core/inc"
SSR_INC_PATH = GLINK_BUILD_ROOT+"/ssr/inc"

env.PublishPrivateApi('MPROC_GLINK', [
   OS_INC_PATH,
   SSR_INC_PATH,
   CORE_INC_PATH
] )

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'BOOT',
   'DEBUGTOOLS',
   'HAL',
   'DAL',
   'HWENGINES',
   'MPROC',
   'MEMORY',
   'POWER',
   'SERVICES',
   'SYSTEMDRIVERS',
   'WIREDCONNECTIVITY',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)
env.RequireProtectedApi(['MPROC_GLINK'])


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
GLINK_SSR_CORE = [
   '${BUILDPATH}/glink_ssr.c',
   '${BUILDPATH}/glink_ssr_config.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
# Add Image-specific Libraries, whose sources should be packed out.
env.AddBinaryLibrary(['WCN_IMAGE', 'CORE_SLPI_ROOT', 'CORE_ADSP_ROOT',
                      'CORE_MPSS_ROOT', 'CBSP_APPS_IMAGE', 'APPS_IMAGE'],
                     '${BUILDPATH}/glink_ssr',
                     [GLINK_SSR_CORE])

                     
#-------------------------------------------------------------------------------
# CleanPack (Remove) all remaining files for peripheral processors
#-------------------------------------------------------------------------------
PACK_OUT = env.FindFiles(['*.h', '*.c', '*.s'], SRCPATH)
# Don't include RPM in this list, because it already does we want automatically.
env.CleanPack(['CORE_SLPI_ROOT', 'CORE_ADSP_ROOT',
               'CORE_MPSS_ROOT', 'WCN_IMAGE'], PACK_OUT)

#-------------------------------------------------------------------------------
# RCINIT Task and Init Fields and Initialization
#-------------------------------------------------------------------------------
RCINIT_INIT_GLINK_SSR_START = {
  'sequence_group'             : 'RCINIT_GROUP_0',           # required
  'init_name'                  : 'glink_ssr',                # required
  'init_function'              : 'glink_ssr_init',           # required
  'dependencies'               : [ 'dalsys', 'glink' ]
}

if 'USES_RCINIT' in env:
  env.AddRCInitFunc( ['CORE_SLPI_ROOT', 'CORE_ADSP_ROOT', 'CORE_MPSS_ROOT'],
                     RCINIT_INIT_GLINK_SSR_START )

