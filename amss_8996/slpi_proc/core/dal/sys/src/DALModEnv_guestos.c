/*==============================================================================
@file  DALModEnv_guestos.c

Initialization functions on Guest OS

Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
==============================================================================*/
#include "DALSys.h"
#include "DALStdDef.h"
#include "DALSysCmn.h"
#include "DALStdErr.h"
#include "DALFramework.h"

extern void DevCfg_Init(void);

void _DALSYS_InitMod(DALSYSConfig *pCfg)
{
   DevCfg_Init();
}
