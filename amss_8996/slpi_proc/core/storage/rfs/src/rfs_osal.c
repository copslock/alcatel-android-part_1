/***********************************************************************
 * rfs_osal.c
 *
 * OS abstraction layer implemenation.
 * Copyright (C) 2014 QUALCOMM Technologies, Inc.
 *
 * Using this file allows us to compile RFS API code without any sort of
 * dependency on underlying operating system.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.slpi/1.0/storage/rfs/src/rfs_osal.c#1 $ $DateTime: 2014/06/27 13:53:49 $ $Author: coresvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2014-01-27   dks   Create

===========================================================================*/

#include "rfs_config_i.h"
#include "rfs_osal.h"

#if defined (FEATURE_RFS_COMPILE_WITH_REX)

void
rfs_osal_init_crit_sect (rfs_osal_crit_sect_type *cs)
{
  rex_init_crit_sect (cs);
}

void
rfs_osal_enter_crit_sect (rfs_osal_crit_sect_type *cs)
{
  rex_enter_crit_sect (cs);
}

void
rfs_osal_leave_crit_sect (rfs_osal_crit_sect_type *cs)
{
  rex_leave_crit_sect (cs);
}

#endif
