/***********************************************************************
 * rfs_osal.h
 *
 * OS abstraction layer.
 * Copyright (C) 2014 QUALCOMM Technologies, Inc.
 *
 * Using this file allows us to compile RFS API code without any sort of
 * dependency on underlying operating system.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.slpi/1.0/storage/rfs/src/rfs_osal.h#1 $ $DateTime: 2014/06/27 13:53:49 $ $Author: coresvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2014-04-04   dks   Featurize compilation
2014-01-27   dks   Create

===========================================================================*/

#ifndef __RFS_OSAL_H__
#define __RFS_OSAL_H__

#include "rfs_config_i.h"
#include "comdef.h"

#if defined (FEATURE_RFS_COMPILE_WITH_REX)

#include "rex.h"

typedef rex_crit_sect_type    rfs_osal_crit_sect_type;

void rfs_osal_init_crit_sect (rfs_osal_crit_sect_type *cs);

void rfs_osal_enter_crit_sect (rfs_osal_crit_sect_type *cs);

void rfs_osal_leave_crit_sect (rfs_osal_crit_sect_type *cs);

#endif

#endif /* not __RFS_OSAL_H__ */
