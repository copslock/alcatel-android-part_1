#ifndef USLEEP_TRANS_H
#define USLEEP_TRANS_H
/*==============================================================================
  FILE:         uSleep_trans.h

  OVERVIEW:     Types & prototypes for transition uSleep functions

  DEPENDENCIES: None
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/transition/uSleep_trans.h#6 $
$DateTime: 2015/04/28 18:18:47 $
==============================================================================*/
#include "DALStdDef.h"
#include "sleep_stats_global.h"
#include "qurt.h"
#include "npa.h"

/*==============================================================================
                              INTERNAL VARIABLES
 =============================================================================*/
extern qurt_signal_t g_uSleepExitSignal;
extern qurt_signal_t g_uSleepEnterSignal;

/*==============================================================================
                            DEFINITIONS & TYPES
 =============================================================================*/
/* Transtion interrupts */
#define USLEEP_SPM_ISLAND_ENTRY_IRQ   29 /* enter_island_mode_irq */
#define USLEEP_SPM_ISLAND_EXIT_IRQ    11 /* q6ss_spm_trig_done_irq[1] */

/* NPA client type that prevents uSleep entry based on latency and other
 * internal restrictions */
#define USLEEP_CLIENT_NODE_INTERNAL   NPA_CLIENT_CUSTOM1  /* Internal uSleep client */
#define USLEEP_CLIENT_NODE_CLOCK_CTL  NPA_CLIENT_CUSTOM2  /* System drivers clock client */
#define USLEEP_CLIENT_NODE_AON_CTL    NPA_CLIENT_CUSTOM3  /* System drivers AON client */

/** 
 * uSleep_transition_mode
 * 
 * @brief Enum to indicate which RPM data set(s) to send and how to wait for the
 *        response
 */
typedef enum uSleep_transition_mode_s
{
  UIMAGE_ENTER  = 0,  /* Performs island entry steps */
  UIMAGE_EXIT,        /* Performs island exit steps */ 
  UIMAGE_EXIT_FAST    /* Performs island exit steps required in fatal error cases */
}uSleep_transition_mode;

/** 
 * uSleep_transition_profiling
 * 
 * @brief Data structure for tracking the mode transition times
 */
typedef struct uSleep_transition_profiling_s
{
  /* Timestamp of last uSleep entry point */
  uint64 entry_start_time;

  /* Timestamp of last uSleep exit point */
  uint64 exit_start_time;

  /* Current value of the time to enter uSleep */
  uint32 entry_latency;

  /* Current value of the time to exit uSleep */
  uint32 exit_latency;

  /* Statistics for transition times */
  sleepStats_generic  entry_stats;
  sleepStats_generic  exit_stats;

  /* Total entries into island mode */
  uint32 total_entries;

  /* Adjustement match value */
  uint32 adjust_count;
}uSleep_transition_profiling;

/*==============================================================================
                        NORMAL MODE FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * uSleepTrans_setupIslandEntry
 * 
 * @brief Performs the final stages of island entry immediatly before island 
 *        mode is actually entered.        
 */
void uSleepTrans_setupIslandEntry(void);

/**
 * uSleepTrans_completeTransitionToNormalMode
 * 
 * @brief Completes the island mode exit process.
 */
void uSleepTrans_completeTransitionToNormalMode(void);

/**
 * uSleepTrans_initialize
 *
 * @brief Intializes some of the internal objects which are used to interact
 *        with surrounding systems especially Sleep.
 *  
 * @Note This assumes that the CXO vote has been setup via normal sleep code 
 *       initilization and will never be removed from the active/sleep sets. 
 */
void uSleepTrans_initialize(void);

/*==============================================================================
                        ISLAND FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * uSleepTrans_islandControl
 * 
 * @brief Enters or exits island mode
 * 
 * @param mode: Enum to perform entry or exit transition actions
 */
void uSleepTrans_islandControl(uSleep_transition_mode mode);

/** 
 * uSleepTrans_transitionToNormalOperation
 * 
 * @brief Finishes the tranistion to normal operational mode once the system 
 *        has started the exit process and is able to return to single threaded
 *        mode 
 */
void uSleepTrans_transitionToNormalOperation(void);

/**
 * uSleepTrans_islandEntryHandler
 *
 * @brief ISR for the interrupt generated at the completion of island entry
 */
void uSleepTrans_islandEntryHandler(uint32 param);

/**
 * uSleepTrans_islandExitHandler
 *
 * @brief ISR for the interrupt generated at the completion of island exit
 */
void uSleepTrans_islandExitHandler(uint32 param);

/** 
 * uSleepTrans_performNormalModeTransition
 * 
 * @brief Starts the transition from island mode to normal mode
 */
uint32 uSleepTrans_performNormalModeTransition(void);

/** 
 * uSleepTrans_performIslandTransition
 * 
 * @brief uSleep function in island mode that handles the normal mode transition
 */
uint32 uSleepTrans_performIslandTransition(void);

/** 
 * uSleepTrans_updateTimes 
 *  
 * @brief If necessary, will update the transition latency values to enter and 
 *        exit uSleep.
 *  
 * @note This must be called in normal operational mode. 
 */ 
void uSleepTrans_updateTimes(void);

/** 
 * uSleepTrans_transitionNotify
 * 
 * @brief Notifies registered clients of a normal operational <-> island 
 *        transition.
 *  
 * @param state:  Enum of entry or exit transition 
 */
void uSleepTrans_transitionNotify(uSleep_state_notification state);

/**
 * uSleepTrans_setTime
 * 
 * @brief Sets the time it takes to transition in or out of uSleep
 *  
 * @param entryTicks: Time in ticks of entry transition
 * @param exitTicks:  Time in ticks of exit transition
 */
void uSleepTrans_setTime(uint32 entryTicks, uint32 exitTicks);

/**
 * uSleepTrans_getTime
 * 
 * @brief Returns the current value of the uSleep latency required to enter 
 *        and exit the mode 
 */
uint32 uSleepTrans_getTime(void);

/** 
 * uSleepTrans_notifyComplete
 * 
 * @brief Notifies uSleep that the island transition has been completed.
 *  
 * @param entry: TRUE   -> Island entry is complete
 *               FALSE  -> Island exit is complete
 */
void uSleepTrans_notifyComplete(uint8 entry);

#endif /* USLEEP_TRANS_H */

