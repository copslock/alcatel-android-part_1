/*==============================================================================
  FILE:         uSleep_trans.c

  OVERVIEW:     This file provides uSleep transition functions that are located
                in DDR memory space

  DEPENDENCIES: None
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/transition/ddr/uSleep_trans.c#10 $
$DateTime: 2015/05/18 16:12:17 $
==============================================================================*/
#include <stdlib.h>
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "uSleepi.h"
#include "uSleep_util.h"
#include "uSleep_ddr_log.h"
#include "uSleep_spm.h"
#include "uSleep_timer.h"
#include "uSleep_node.h"
#include "uSleep_os.h"
#include "sleep_os.h"
#include "sleep_pmi.h"
#include "rpmclient.h"
#include "qurt.h"
#include "rpm.h"
#include "spm.h"

/*==============================================================================
                           EXTERNAL VARIABLES
 =============================================================================*/
extern uSleep_callback_data g_uSleepCallbacks;
extern qurt_mutex_t         g_uSleepStateMutex;
extern uSleep_global_data   g_uSleepData;

/*==============================================================================
                             INTERNAL VARIABLES
 =============================================================================*/
/* Results from the RPM call to force the sync of the various sets before
 * entering island mode */
static uint32 g_rpmSleepSetSync;
static uint32 g_rpmNextActiveSetResult;

/*==============================================================================
                       INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/**
 * uSleepTrans_resetTimes
 * 
 * @brief Resets transition statistic values
 * 
 * @param transPtr: Pointer to transition data 
 */
static void uSleepTrans_resetTimes(uSleep_transition_profiling *transPtr) 
{
  memset(&transPtr->entry_stats, 0, sizeof(sleepStats_generic));
  memset(&transPtr->exit_stats, 0, sizeof(sleepStats_generic));
  
  transPtr->entry_stats.min  = UINT64_MAX;
  transPtr->exit_stats.min   = UINT64_MAX;

  return;
}

/**
 * uSleepTrans_programDDRWakeupTimeInternal
 * 
 * @brief Creates a uTimer based on the previously set value.  The timer is used to exit 
 *        island mode in order to service the normal mode timer.  
 */
static void uSleepTrans_programDDRWakeupTimeInternal(void)
{
  uint64 wakeup = uSleep_getDDRWakeupTimeInternal();

  uSleepDDRLog_printf(USLEEP_DDR_LOG_LEVEL_TRANSITION, (1 * 2),
                      "Set DDR exit timer (Value: 0x%llx)",
                      ULOG64_DATA(wakeup));
  
  uSleepTimer_setValue(wakeup);
  return;
}

/*==============================================================================
                       EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/*
 * uSleepTrans_setDDRWakeupTimeInternal
 */
void uSleepTrans_setDDRWakeupTimeInternal(uint64 wakeupTimer)
{
  /* Backoff the normal operational deadline by the time it takes to exit
   * uSleep */
  g_uSleepData.ddr_wakeup_timer = 
    wakeupTimer - g_uSleepData.transition_data.exit_latency;

  /* Create a uTimer based on the normal mode timer to trigger exit */
  uSleepTrans_programDDRWakeupTimeInternal();

  return;
}

/*
 * uSleepTrans_setupIslandEntry
 */
void uSleepTrans_setupIslandEntry(void)
{
  /* Disable the RPM interrupt so the sync calls do not cause us to
   * exit uImage with the ACK interrupt */
  rpm_mask_interrupt(true);

  /* Flush the sleep/next active sets */
  g_rpmSleepSetSync         = rpm_force_sync(RPM_SLEEP_SET);
  g_rpmNextActiveSetResult  = rpm_force_sync(RPM_NEXT_ACTIVE_SET);

  return;
}

/*
 * uSleepTrans_completeTransitionToNormalMode
 */
void uSleepTrans_completeTransitionToNormalMode(void)
{
  /* Set sleep function pointer to main uSleep perform function so we may  
   * re-enter island mode */
  uSleepOS_setIdleFunctionPtr(uSleepOS_mainEntry);

  /* Set PMI handler back to main image handler */
  sleepPMI_setHandlerFunctionPTR(sleepOS_PMIPerformer);
  
  /* Use NAS result if valid, else SS */
  rpm_churn_queue(0 != g_rpmNextActiveSetResult ? g_rpmNextActiveSetResult : g_rpmSleepSetSync);

  /* Reenable RPM interrupt that was disabled from the set sync on island entry */
  rpm_mask_interrupt(false);

  /* Call notification CB's when in normal operational mode */
  uSleepTrans_transitionNotify(USLEEP_STATE_EXIT);

  return;
}

/*
 * uSleepTrans_transitionNotify
 */
void uSleepTrans_transitionNotify(uSleep_state_notification state)
{
  uSleep_callback_list      *guestFunction;
  uSleep_user_callback_list *userFunction;

  /* Notify registered client callback functions of uImage transition */
  guestFunction = g_uSleepCallbacks.callback_list;
  userFunction  = g_uSleepCallbacks.user_callback_list;

  uSleepDDRLog_printf(USLEEP_DDR_LOG_LEVEL_TRANSITION, 1,
                      "Transition notification (State: %s)",
                      USLEEP_STATE_ENTER == state ? "Entry" : "Exit");

  /* Call guest OS notification functions */
  while(NULL != guestFunction)
  {
    guestFunction->callback(state);
    guestFunction = guestFunction->next;
  }

  /* Call user OS notification functions. This will actually signal
   * a thread to run in user space and wait for it to finish calling all user
   * CB's for that particular PD */
  while(NULL != userFunction)
  {
    userFunction->callback(state, userFunction->signals);
    userFunction = userFunction->next;
  }

  uSleepDDRLog_printf(USLEEP_DDR_LOG_LEVEL_TRANSITION, 0,
                      "Notification complete");

  return;
}

/* 
 * uSleepTrans_updateTimes
 */ 
void uSleepTrans_updateTimes(void)
{
  uSleep_transition_profiling *transPtr = uSleep_getProfilingDataPtr();

  if(transPtr->adjust_count < 4)
  {
    /* Initialize data values */
    uSleepTrans_resetTimes(transPtr);

    /* Set initial adjustment match value */
    transPtr->adjust_count = 4;
  }
  else if(transPtr->entry_stats.count >= transPtr->adjust_count)
  {
    uint32 enterAvg;
    uint32 exitAvg;
    uint64 enterTotal = transPtr->entry_stats.total;
    uint64 exitTotal  = transPtr->exit_stats.total;

    /* Compute average transition enter and exit latency */
    enterAvg  = (uint32)(enterTotal / transPtr->entry_stats.count);
    exitAvg   = (uint32)(exitTotal / transPtr->exit_stats.count);

    /* Update transition times */
    uSleepTrans_setTime(enterAvg, exitAvg);

    /* Reset the data for the next adjustment, but keep current min/max */
    transPtr->entry_stats.count = 0;
    transPtr->entry_stats.total = 0;
    transPtr->exit_stats.count = 0;
    transPtr->exit_stats.total = 0;

    /* Set next adjustment */
    if(transPtr->adjust_count < 0x000000400)
    {
      transPtr->adjust_count = transPtr->adjust_count << 2;
    }
  }

  return;
}

/*
 * uSleepTrans_setTime
 */
void uSleepTrans_setTime(uint32 entryTicks,
                         uint32 exitTicks)
{
  uSleep_transition_profiling *dataPtr = uSleep_getProfilingDataPtr();
  
  uSleepDDRLog_printf(USLEEP_DDR_LOG_LEVEL_TRANSITION, 2,
                      "Transition time update request "
                      "(enter: %d) "
                      "(exit: %d)",
                      entryTicks,
                      exitTicks);

  dataPtr->entry_latency = entryTicks;
  dataPtr->exit_latency = exitTicks;

  return;
}

/*
 * uSleepTrans_getTime
 */
uint32 uSleepTrans_getTime(void)
{
  uSleep_transition_profiling *dataPtr = uSleep_getProfilingDataPtr();

  return (dataPtr->entry_latency + dataPtr->exit_latency);
}

/*
 * uSleepTrans_initialize
 */
void uSleepTrans_initialize(void)
{
  /* Initializing the mutex for uSleep resource request */
  qurt_mutex_init(&g_uSleepStateMutex);

  /* Setup required hardware transition data */
  uSleepSPM_setupSequenceTransition();

  return;
}

