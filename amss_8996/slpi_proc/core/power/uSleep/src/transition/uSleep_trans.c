/*==============================================================================
  FILE:         uSleep_trans.c

  OVERVIEW:     This file provides uSleep transition functions that are located
                in island memory space

  DEPENDENCIES: Object file generated from source is marked as island section
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/transition/uSleep_trans.c#14 $
$DateTime: 2016/07/01 05:25:07 $
==============================================================================*/
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "uSleep.h"
#include "uSleepi.h"
#include "uSleep_trans.h"
#include "uSleep_timer.h"
#include "uSleep_target.h"
#include "uSleep_util.h"
#include "uSleep_lpr.h"
#include "uSleep_os.h"
#include "uSleep_log.h"
#include "uSleep_spm.h"
#include "island_mgr.h"
#include "uInterruptController.h"
#include "uSleep_HALhwio.h"
#include "HALhwio.h"

/*==============================================================================
                             GLOBAL VARIABLES
 =============================================================================*/
/* Signal used to block caller when uSleep exit is requested */
qurt_signal_t g_uSleepExitSignal;

/* Signal used to block a call to exit while in the process of entering island */
qurt_signal_t g_uSleepEnterSignal;

/*==============================================================================
                       INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/**
 * uSleepTrans_waitForHardwareExit
 *
 * @brief Used to poll for the hardware exit status in error cases where the
 *        exit interrupt can not be used.
 */ 
void uSleepTrans_waitForHardwareExit(void)
{
  uint32 state;

  do
  {
    if(UINTERRUPT_ERROR == 
       uInterruptController_IsInterruptPending(USLEEP_SPM_ISLAND_EXIT_IRQ, &state))
    {
      uSleepOS_haltOnError();
    }
  }while(state != 1);

  /* Complete exit path */
  uSleepTrans_notifyComplete(FALSE);

  return;
}

/**
 * uSleepTrans_triggerIslandTransition
 *
 * @brief Triggers hardware to begin the island enter or exit process 
 *  
 * @param triggerType: Enter or exit option
 */ 
static void uSleepTrans_triggerIslandTransition(uSleep_spm_soft_trigger triggerType)
{
  uint32 status;
  uint32 timeoutCount = 0;

  uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1, 
                   "Begin bus transition (Mode: %s)",
                   USLEEP_SW_TRIGGER_ENTER == triggerType ? "Isolation" : "Deisolation");

  CORE_VERIFY(triggerType < USLEEP_SW_TRIGGER_LAST);

  if(USLEEP_SW_TRIGGER_ENTER == triggerType)
  {
    /* Ensure SW trigger status is not active. */
    CORE_VERIFY(0 == HWIO_IN(SSC_QDSP6SS_SPM_TRIG_STATUS));

    HWIO_OUT(SSC_QDSP6SS_SPM_SW_TRIG, 0x01);
	
	/* Add an initial read of the status to ensure trigger setting is flushed (per HPG) */
	status = HWIO_IN(SSC_QDSP6SS_SPM_TRIG_STATUS);

    /* Wait for AXI isolation to complete before returning on island entry */
    while(0 != HWIO_IN(SSC_QDSP6SS_SPM_TRIG_STATUS))
    {
      CORE_LOG_VERIFY((++timeoutCount < 768),
                       uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 0, 
                                        " ERROR: Isolation timeout"));
    }

    uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1, 
                     " Sequence complete (count: %d)",
                     timeoutCount);
  }
  else
  {
    /* Set exit bit */
    HWIO_OUTF(SSC_SSC_IEC_CTL, EXIT_ISLAND_MODE_TRIG, 0x01);

    /* Add an initial read of the status to ensure trigger setting is flushed (per HPG) */
    status = HWIO_IN(SSC_QDSP6SS_SPM_TRIG_STATUS);
  }

  return;
}

/*==============================================================================
                       EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/*
 * uSleepTrans_islandControl
 */
void uSleepTrans_islandControl(uSleep_transition_mode mode)
{
  int32 entryCount = 5;

  if(UIMAGE_EXIT_FAST != mode)
  {
    /* Configure the power mode to enter during transtion waits */
    uSleepTarget_configureIslandTransitionPowerMode(TRUE);
  }

  if(UIMAGE_ENTER == mode)
  {
    /* Reset the entry signal so we can block any possible exit call until uSleep has fully 
     * entered */
    qurt_signal_clear(&g_uSleepEnterSignal, 0x01);

    /* Perform the setup required to enter island mode */
    uSleepTrans_setupIslandEntry();

    /* OS island entry - This will update TLBs and other OS specific island
     * requirements */
    CORE_VERIFY(ISLAND_MGR_EOK == island_mgr_island_enter());

    /* Set HW entry wait state  */
    uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_STAGE1_ENTRY);

    /* Trigger AXI bus isolation and begin island entry */
    uSleepTrans_triggerIslandTransition(USLEEP_SW_TRIGGER_ENTER);

    /* Wait for island entry complete interrupt. Give it a few tries before erroring */
    do
    {
      uSleepOS_enterIdle();
      qurt_power_wait_for_idle();
    }while((--entryCount > 0) &&
           (uSleep_getStateInternal() == uSLEEP_INTERNAL_STATE_STAGE1_ENTRY));

    /* Should be fully transitioned to island mode at this point */
    CORE_VERIFY(entryCount > 0);

    /* Reconfigure for standard island mode operation */
    uSleepTarget_configureIslandTransitionPowerMode(FALSE);
  }
  else
  {
    /* Set HW exit wait state and change sleep function pointer to continue exit routines */
    if(UIMAGE_EXIT_FAST == mode)
    {
      uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_FATAL_EXIT);
    }
    else
    {
      uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_STAGE1_EXIT);
      uSleepOS_setIdleFunctionPtr(uSleepTrans_performNormalModeTransition);
    }

    /* Trigger AXI bus isolation and begin island entry */
    uSleepTrans_triggerIslandTransition(USLEEP_SW_TRIGGER_EXIT);

    if(UIMAGE_EXIT_FAST == mode)
    {
      /* In error case, wait here for hardware exit to complete */
      uSleepTrans_waitForHardwareExit();
    }
  }
 
  return;
}

/*
 * uSleepTrans_transitionToNormalOperation
 */
void uSleepTrans_transitionToNormalOperation(void)
{
  /* Finish exiting island mode in STM context */
  if(ISLAND_MGR_EOK != island_mgr_island_exit())
  {
    /* Exit is broke, nothing more we can do here. */
    uSleepOS_haltOnError();
  }
  
  /* Finish the transition */
  uSleepTrans_completeTransitionToNormalMode();

  return;
}

/* 
 * uSleepTrans_performNormalModeTransition
 */
uint32 uSleepTrans_performNormalModeTransition(void)
{
  uint32 exitCount = 0;
  
  /* Wait for island exit complete interrupt */
  do
  {
    qurt_power_wait_for_idle();

    if(uSleep_getStateInternal() != uSLEEP_INTERNAL_STATE_REQ_EXIT)
    {
      uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1,
                     " Waiting for exit int (count: %d)",
                     exitCount);

      uSleepOS_enterIdle();
    }
    else
    {
      break;
    }
  }while(++exitCount < 10);

  /* Should be fully transitioned to normal mode at this point */
  CORE_VERIFY((exitCount < 10) && 
              (uSleep_getStateInternal() == uSLEEP_INTERNAL_STATE_REQ_EXIT));

  return 0; 
}

/* 
 * uSleepTrans_performIslandTransition
 */
uint32 uSleepTrans_performIslandTransition(void)
{
  uSleep_transition_profiling *transProfileData = uSleep_getProfilingDataPtr();

  /* Function should entered once, only from the island entry request stage */
  CORE_VERIFY(uSleep_getStateInternal() == uSLEEP_INTERNAL_STATE_FULL_ENTRY);

  /* Enter island mode by doing the AXI bus isolation which will be complete when
   * this returns */
  uSleepTrans_islandControl(UIMAGE_ENTER);

  /* Island entry is complete */
  uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_ACTIVE);

  uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1,
                   "Island transition done (Ret: %s)",
                   USLEEP_RETENTION_DISABLE == 
                    uSleep_getLowPowerModeInternal() ? "On" : "Off");

  /* Record the uSleep entry transition time */
  sleepStats_updateValue(&transProfileData->entry_stats,
                         uTimetick_Get() - transProfileData->entry_start_time);

  /* Let others know entry is done */
  qurt_signal_set(&g_uSleepEnterSignal, 0x01);

  return 0;
}

/*
 * uSleepTrans_islandEntryHandler
 */
void uSleepTrans_islandEntryHandler(uint32 param)
{
  uint32 nState;

  /* Clear interrupt source */
  HWIO_OUTF(SSC_SSC_IEC_CTL, ENTER_ISLAND_MODE_IRQ_CLR, 0x1);

  CORE_VERIFY(uSleep_getStateInternal() == uSLEEP_INTERNAL_STATE_STAGE1_ENTRY);

  /* Complete island entry */
  uSleepTrans_notifyComplete(TRUE);

  /* ensure source interrupt is clear before exiting ISR */
  do
  {
    CORE_VERIFY(uInterruptController_IsInterruptPending(USLEEP_SPM_ISLAND_ENTRY_IRQ, &nState) == UINTERRUPT_SUCCESS);
  }while(nState != 0);

  return;
}

/*
 * uSleepTrans_islandExitHandler
 */
void uSleepTrans_islandExitHandler(uint32 param)
{
  CORE_VERIFY(uSleep_getStateInternal() == uSLEEP_INTERNAL_STATE_STAGE1_EXIT);

  uSleepTrans_notifyComplete(FALSE);

  return;
}

/*
 * uSleepTrans_notifyComplete
 */
void uSleepTrans_notifyComplete(uint8 entry)
{
  uSleep_internal_state state = uSleep_getStateInternal();

  if(TRUE == entry)
  {
    CORE_VERIFY(uSLEEP_INTERNAL_STATE_STAGE1_ENTRY == state);

    /* HW Island entry is complete, set to SW finish state and main uSleep function */
    uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_STAGE2_ENTRY);
    
    uSleepOS_setIdleFunctionPtr(uSleepOS_worker);
  }
  else
  {
    if(uSLEEP_INTERNAL_STATE_FATAL_EXIT == state)
    {
      /* In cases of fatal error, exit island immediatly after island HW has
       * finished it's transition. */
      uSleepOS_setIdleFunctionPtr(uSleepOS_performError);  

      /* Exit immediately */
      if(ISLAND_MGR_EOK != island_mgr_island_exit())
      {
        /* Fatal error exit is broke, nothing more we can do here. */
        uSleepOS_haltOnError();
      }
    }
    else
    {
      CORE_VERIFY(uSLEEP_INTERNAL_STATE_STAGE1_EXIT == state);

      /* Hardware island exit is complete, set request state to finish
       * the normal exit process. */
      uSleepOS_setIdleFunctionPtr(uSleepOS_worker);
      uSleep_setStateInternal(uSLEEP_INTERNAL_STATE_REQ_EXIT);
    }
  }

  return;
}

