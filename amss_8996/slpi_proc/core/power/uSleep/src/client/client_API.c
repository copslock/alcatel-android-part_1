/*==============================================================================
  FILE:         client_API.c

  OVERVIEW:     This file provides uSleep API functions for guest PD that are
                located in the ISLAND section

  DEPENDENCIES: None
  
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/uSleep/src/client/client_API.c#12 $
$DateTime: 2016/01/12 19:14:03 $
==============================================================================*/
#include "comdef.h"
#include "CoreVerify.h"
#include "uSleep.h"
#include "uSleepi.h"
#include "sleep_stats_global.h"
#include "sleep_pmi.h"
#include "uSleep_log.h"
#include "uSleep_util.h"
#include "uSleep_timer.h"
#include "uSleep_trans.h"
#include "uSleep_os.h"
#include "qurt.h"
#include "smd.h"
#include "island_mgr.h"
#include "uInterruptController.h"

/*==============================================================================
                           TYPE & MACRO DEFINITIONS
 =============================================================================*/
/* Macros for locking/unlocking the mutex around uSleep API's that will change
 * internal states */ 
#define USLEEP_LOCK()  qurt_mutex_lock(&g_uSleepMutex);
#define USLEEP_FREE()  qurt_mutex_unlock(&g_uSleepMutex);

/*==============================================================================
                             INTERNAL VARIABLES
 =============================================================================*/
/* API lock */
qurt_mutex_t g_uSleepMutex;

/*==============================================================================
                        EXTERNAL FUNCTION PROTOTYPES
 =============================================================================*/
void uSleepTrans_waitForHardwareExit(void);

/*==============================================================================
                       EXTERNAL GUEST SECTION FUNCTIONS
 =============================================================================*/
/*
 * uSleep_exit
 */
uint32 uSleep_exit(void)
{
  uSleep_internal_state       state;
  uint32                      pendingInt    = qurt_system_vid_get();
  uSleep_transition_profiling *profileData  = uSleep_getProfilingDataPtr();
  qurt_thread_t               taskID        = qurt_thread_get_id();

  /* Since we are modifying the uSleep state to exit, only allow one client to
   * do that at a time */
  uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 2,
                   " Get exit mutex (Task: 0x%x) (VID: %u)",
                   taskID, pendingInt);

  USLEEP_LOCK();

  state = uSleep_getStateInternal();

  /* Check if we are in the middle of trying to enter island mode and someone * 
   * called exit */
  if(state & (uSLEEP_INTERNAL_STATE_STAGE1_ENTRY |
              uSLEEP_INTERNAL_STATE_STAGE2_ENTRY |
              uSLEEP_INTERNAL_STATE_FULL_ENTRY))
  {
    uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1,
                     " Waiting for entry completion (Task: 0x%x)", taskID);

    /* Wait for entry to complete before exiting */
    qurt_signal_wait(&g_uSleepEnterSignal, 0x01, QURT_SIGNAL_ATTR_WAIT_ALL);

    /* Update the internal state after waiting */
    state = uSleep_getStateInternal();
  }

  /* If we are currently active, turn on DDR and wait for full exit to happen */
  if(state & uSLEEP_INTERNAL_STATE_ACTIVE)
  {
    /* Record the start of the exit sequence */
    profileData->exit_start_time = uTimetick_Get();

    uSleepLog_QDSSPrintf(USLEEP_LOG_LEVEL_PROFILING, 
                         USLEEP_EXIT_START_NUM_ARGS,
                         USLEEP_EXIT_START_STR, 
                         USLEEP_EXIT_START,
                         pendingInt);

    /* Clear the existing uTimer that was created based on the first normal
     * mode non-deferrable timer. */
    uSleepTimer_clearTimer();

    /* Reset the exit signal so we can block the caller until uSleep has fully 
     * exited */
    qurt_signal_clear(&g_uSleepExitSignal, 0x01);

    /* Exit island mode */
    uSleepTrans_islandControl(UIMAGE_EXIT);

    /* Block until the system has fully transitioned back to normal operational
     * mode */
    qurt_signal_wait(&g_uSleepExitSignal, 0x01, QURT_SIGNAL_ATTR_WAIT_ALL);
  }
  else if(0 == (state & uSLEEP_INTERNAL_STATE_IN_DDR))
  {
    /* Can not be in a transition state when calling this function */
    uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 0,
                     " ERROR: exit called while in transition");

    uSleepOS_haltOnError();
  }

  uSleepLog_printf(USLEEP_LOG_LEVEL_TRANSITION, 1, 
                   " Give exit mutex (0x%x)",
                   taskID);

  USLEEP_FREE();

  return USLEEP_SUCCESS;
}

/*
 * uSleep_setLpmMode
 */
uint32 uSleep_setLowPowerMode(uSleep_power_mode mode)
{
  USLEEP_LOCK();
  uSleep_setLowPowerModeInternal(mode);
  USLEEP_FREE();

  return USLEEP_SUCCESS;
}

/*
 * uSleep_exitOnError
 */
void uSleep_exitOnError(void)
{
  uSleep_internal_state state;
  uint32                count     = 0;
  uint32                intState  = 0;

  state = uSleep_getStateInternal();

  switch(state)
  {
    /* At beginning stage of entry -
     * Nothing to do as the normal exit request was made before (or while in) call
     * to kernel entry */ 
    case uSLEEP_INTERNAL_STATE_FULL_ENTRY:
    {
      return;
    }

    /* Island entry transition is in progress -
     * This stage is after kernel entry, but before final transition is complete
     * In true island case, sleep will be waiting for hardware entry completion interrupt and
     * must wait for the completion interrupt before allowing exit */ 
    case uSLEEP_INTERNAL_STATE_STAGE1_ENTRY:
    {
      do
      {
        /* Wait for entry completion interrupt - can not start exit sequence before entry is complete.
         * The count value used has no special significance and is simply a large enough value to 
         * give the interrupt a chance to fire before declaring (forced watchdog) a complete failure 
         * in the exit process. */
        if((UINTERRUPT_SUCCESS != 
            uInterruptController_IsInterruptPending(USLEEP_SPM_ISLAND_ENTRY_IRQ, &intState)) ||
           (++count > 131072))
        {
          uSleepLog_QDSSPrintf(USLEEP_LOG_LEVEL_TRANSITION, 
                               USLEEP_CRIT_ERR_NUM_ARGS,
                               USLEEP_CRIT_ERR_STR, 
                               USLEEP_CRIT_ERR,
                               state, intState, count);

          /* Does not return */
          uSleepOS_haltOnError();
        }
      } while(intState != 0);

      /* Initiate the exit */
      uSleepTrans_islandControl(UIMAGE_EXIT_FAST);
      break;
    }

    /* Standard exit request is in progress -
     * This stage is before kernel exit call
     * In true island case, sleep is waiting for hardware exit completion interrupt */
    case uSLEEP_INTERNAL_STATE_STAGE1_EXIT:
    {
      /* Call the remaining critical error handler routines (path after exit sequence triggered) */
      uSleepTrans_waitForHardwareExit();
      break;
    }

    /* Island has completed the entry transition - 
     * Call the exit transition, island should be completly exited when this returns */ 
    case uSLEEP_INTERNAL_STATE_STAGE2_ENTRY:
    case uSLEEP_INTERNAL_STATE_ACTIVE:
    {
      uSleepTrans_islandControl(UIMAGE_EXIT_FAST);
      break;
    }

    /* Invalid state - function should not have been called */
    default:
    {
      uSleepLog_QDSSPrintf(USLEEP_LOG_LEVEL_TRANSITION, 
                           USLEEP_CRIT_ERR_NUM_ARGS,
                           USLEEP_CRIT_ERR_STR, 
                           USLEEP_CRIT_ERR,
                           state, intState, count);

      /* does not return */
      uSleepOS_haltOnError();
    }
  }

  return;
}

/*
 * uSleep_getCriticalExitTime
 */
uint64 uSleep_getCriticalExitTime(void)
{
  uSleep_internal_state state;
  uint64                exitTime = -1; /* Default to no critical exit time */
  
  state = uSleep_getStateInternal();

  /* If we are currently active, return the actual exit timer value */
  if(state & (uSLEEP_INTERNAL_STATE_ACTIVE        |
              uSLEEP_INTERNAL_STATE_FULL_ENTRY    |
              uSLEEP_INTERNAL_STATE_STAGE1_ENTRY  |
              uSLEEP_INTERNAL_STATE_STAGE2_ENTRY))
  {
    /* Return actual critical exit time */
    exitTime = uSleep_getDDRWakeupTimeInternal();
  }
  else if(state & (uSLEEP_INTERNAL_STATE_STAGE1_EXIT  |
                   uSLEEP_INTERNAL_STATE_REQ_EXIT     |
                   uSLEEP_INTERNAL_STATE_FULL_EXIT))
  {
    /* Exit has been requested, if a client asks, return 0 to indicate
     * they should not begin any work */
    exitTime = 0;
  }

  return exitTime;
}

/*
 * uSleep_isExitRequested
 */
boolean uSleep_isExitRequested(void)
{
  boolean               rtnVal  = FALSE;
  uSleep_internal_state state   = uSleep_getStateInternal();
  
  if(state & uSLEEP_INTERNAL_STATE_REQ_EXIT)
  {
    rtnVal = TRUE;
  }

  return rtnVal;
}
