#ifndef SLEEP_STATS_GLOBAL_H
#define SLEEP_STATS_GLOBAL_H
/*==============================================================================
  FILE:         sleep_stats_global.h
  
  OVERVIEW:     Internal functions & types for global interest statistic data
 
  DEPENDENCIES: User should not include this file directly and should use 
                the master file "sleep_stats.h"
                
                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/inc/sleep_stats_global.h#2 $
$DateTime: 2015/03/25 23:54:25 $
==============================================================================*/
#include "DALStdDef.h"
#include "sleep.h"

/*==============================================================================
                                TYPE DEFINITIONS
 =============================================================================*/
/**
 * RPM_master_stats
 *
 * @brief RPM data fields for shutdown & wakeup
 */ 
typedef struct RPM_master_stats_s
{
  uint32 active_cores;                    /* Bitmask of active cores - core 0 = bit 0 */
  uint32 num_shutdowns;                   /* Number of times all cores have power collapsed */
  uint64 shutdown_req;                    /* Timestamp of last shutdown of all cores */
  uint64 wakeup_ind;                      /* Timestamp of last master wakeup ind */
  uint64 bringup_req;                     /* Timestamp of last bring up req */
  uint64 bringup_ack;                     /* Timestamp of last bring up ack */
  uint32 wakeup_reason;                   /* 0 = rude wakeup, 1 = scheduled wakeup */
  uint32 last_sleep_transition_duration;  /* Time taken (in ticks) for the last W->S transition */
  uint32 last_wake_transition_duration;   /* Time taken (in ticks) for the last S->W transition */
  uint32 xo_count;                        /* Shows how many times the SS has power collapsed 
                                           * and voted for XO shutdown */
  uint64 xo_last_entered_at;              /* Last timestamp the SS power collapsed and voted 
                                           * for XO shutdown */
  uint64 xo_last_exited_at;               /* Last timestamp the SS exited PC with vote for 
                                           * XO shutdown and woken up by RPM */
  uint64 xo_accumulated_duration;         /* Shows how long the SS has been in a sleep mode 
                                           * since it has booted */
  uint32 reserved[6];
}RPM_master_stats;

/**
 * sleepStats_generic
 *
 * @brief Basic statistics data structure
 */
typedef struct sleepStats_generic_s
{
  uint32  count;  /* measurement count */
  uint64  total;  /* measurement total */
  uint64  max;    /* measurement maximum value */
  uint64  min;    /* measurement minimum value*/
}sleepStats_generic;

/**
 * sleepStats_generic_signed
 *
 * @brief Basic SIGNED statistics data structure
 */
typedef struct sleepStats_generic_signed_s
{
  uint32  count;  /* measurement count */
  int64   total;  /* measurement total */
  int64   max;    /* measurement maximum value */
  int64   min;    /* measurement minimum value*/
}sleepStats_generic_signed;

/*==============================================================================
                              FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * sleepStats_getRpmDataPtr
 *
 * @brief Returns the RPM shared memory statistics location
 */
RPM_master_stats *sleepStats_getRpmDataPtr(void) USLEEP_CODE_SECTION;

/** 
 * sleepStats_updateValue
 *  
 * @brief Update data fields of the generic statistics structure
 *        
 * @param stats: Pointer to generic data structure to update
 * @param value: Value that will be used for the update
 */
void sleepStats_updateValue(sleepStats_generic *stats, uint64 value) USLEEP_CODE_SECTION;

/** 
 * sleepStats_updateSignedValue
 *  
 * @brief Updates the data fields for the generic SIGNED statistics structure
 *        
 * @param stats: Pointer to generic SIGNED data structure to update
 * @param value: Value that will be used for the update
 */
void sleepStats_updateSignedValue(sleepStats_generic_signed *stats,
                                  int64                     value);

#endif /*SLEEP_STATS_GLOBAL_H*/

