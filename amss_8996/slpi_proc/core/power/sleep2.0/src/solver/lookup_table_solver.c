/*==============================================================================
  FILE:         lookup_table_solver.c
 
  OVERVIEW:     This file provides the sleep solver implementation.  This is
                used by the sleep task when the CPU is idle, to gather the
                available low power modes that can be entered.

  DEPENDENCIES: None

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/src/solver/lookup_table_solver.c#4 $
$DateTime: 2015/04/28 18:18:47 $
==============================================================================*/
#include <limits.h>
#include <stdlib.h>
#include "CoreVerify.h"
#include "CoreTime.h"
#include "sleep_log.h"
#include "sleep_lpri.h"
#include "lookup_table_solver.h"
#include "synthTypes.h"
#include "SleepLPR_synth_modes.h"
#include "synthRegistry.h"

/*==============================================================================
                            INTERNAL FUNCTIONS
 =============================================================================*/
/**
 * sleepLUTSolver_findElement
 *
 * @brief This function finds the lookup table element that 
 *        contains a duration range that satisfies the sleep
 *        duration passed in, and returns a pointer to that
 *        element.  This function can be optimized/modified
 *        independently of the main solver function.
 *
 * @param mode_duration:   Sleep duration to find the lookup 
 *                         table element for.
 *  
 * @param ftable_idx:      Frequency table index based on
 *                         current core frequency 
 * 
 * @param lutEntryIdx[out]:Index to the lookup table element which
 *                         was return. Valid only if lookup table
 *                         element was non null else -1.
 *  
 * @return Pointer to the lookup table element whose duration 
 *         range contains the sleep duration passed in.
 */
static sleep_mLUT_elem *sleepLUTSolver_findElement
( 
  uint64  mode_duration,
  uint32  lutIdx,
  int32  *lutEntryIdx
)
{
  /* Keep track of the most recently used duration.  This should help
   * with the case where the same duration range is hit over and over
   * again. */
  static uint32     mru_mode_idx  = 0;
  static sleep_mLUT *mru_lut      = 0;
  sleep_mLUT_elem   *mru_mode, *last_mode, *comp_mru_mode;
  sleep_mLUT        *pModeLUT;
  int32             end_idx       = 0;

  CORE_VERIFY_PTR( lutEntryIdx );

  pModeLUT  = SleepLPR_power_lookup_table[lutIdx];
  mru_mode  = &pModeLUT->element[mru_mode_idx];
  last_mode = &pModeLUT->element[pModeLUT->num_elements - 1];
  *lutEntryIdx = -1;                    /* Invalid until entry found. */  

  /*Set comparison mode to one idx lower (higher duration)*/
  if(mru_mode_idx == 0)
  {
    comp_mru_mode = mru_mode;
  }
  else
  {
    comp_mru_mode = &pModeLUT->element[mru_mode_idx - 1];
  }

  /* Check the last recently used entry for a match. */
  if(pModeLUT == mru_lut)
  {
    /* If given duration > most recently used duration and most recent duration
     * is max or given duration is <= next higher duration 
     */ 
    if(mode_duration >= mru_mode->duration_thresh &&
       (mru_mode_idx == 0 || mode_duration < comp_mru_mode->duration_thresh))
    {
      /* Found a match */
      *lutEntryIdx = (int32)mru_mode_idx;
      sleepStats_updateValue(&mru_mode->element_stats, mode_duration);
      return mru_mode;
    }
    /* Check for not enough time for any mode */
    else if(mode_duration < last_mode->duration_thresh)
    {
      return NULL;
    }
  }
  else
  {
    /* Running at different frequency from last cached value (different LUT ptr)
     *  - Need to update cache values */
    mru_lut   = pModeLUT;
    last_mode = &pModeLUT->element[pModeLUT->num_elements - 1];
  }
  
  mru_mode_idx  = pModeLUT->num_elements / 2;
  mru_mode      = &pModeLUT->element[mru_mode_idx];

  /* Check for not enough time for any mode in new frequency table */
  if(mode_duration < last_mode->duration_thresh)
  {
    return NULL;
  }

  /* Use the MRU info to determine which half of lookup table to traverse.
   * The search will start at index 0 or current index + 1 and increment until 
   * it finds the correct entry or errors.
   */
  if( mode_duration >= mru_mode->duration_thresh )
  {
    end_idx       = mru_mode_idx + 1;
    mru_mode_idx  = 0;
  }
  else
  {
    mru_mode_idx++;
    end_idx = pModeLUT->num_elements;
  }
  
  /* Traverse the rest of the table to find the duration that matches. */
  for(; mru_mode_idx < end_idx; mru_mode_idx++ )
  {
    if( mode_duration >= pModeLUT->element[mru_mode_idx].duration_thresh)
    {
      /* Found it, return this element. */
      *lutEntryIdx = (int32)mru_mode_idx;
      sleepStats_updateValue(&pModeLUT->element[mru_mode_idx].element_stats, 
                             mode_duration);
      return ( &pModeLUT->element[mru_mode_idx] );
    }
  }

  /* We should never make it this far. If we do, some logic failed above that 
   * should have caught the case were there is not enough time to do any modes.
   */
   sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 2*1, 
                    "Invalid duration (Value: 0x%llx)", 
                    ULOG64_DATA(mode_duration));

  CORE_VERIFY( 0 );
  return NULL;
}

/**
 * @brief This function outputs threshold deadline when the mode selected by
 *        lookup table solver may no longer be optimal. 
 *
 * @param input       : Structure containing solver input parameters.
 * @param output      : Structure containing various output data from solver.
 * @param lutEntryIdx : Index of the entry in look up table that contains
 *                      mode meeting input restriction. 
 *
 * @note This is meant to be an internal function and assumes valid input
 *       parameters. If that changes, we may need to verify them.
 */
void sleepLUTSolver_setThresholdDeadline
(
  sleep_solver_input  *input,
  sleep_solver_output *output,
  int32                lutEntryIdx
)
{
  uint32 lutIdx = input->fLUT->mLUT_idx; /* Index of the look up table */
  uint32 thresholdIdx;                   /* Index of threshold duration entry */

  sleep_mLUT_elem *lutEntry;
  sleep_mLUT *lut = SleepLPR_power_lookup_table[lutIdx];

  /* Currently we have simple implementation of setting threshold for every
   * duration change. So this is 1:1 mapping. But if we optimize this,
   * thresholdEntryIdx may not be same as lutEntryIdx */
  thresholdIdx = lutEntryIdx;

  lutEntry = &lut->element[thresholdIdx];

  /* Setting the threshold deadline. */
  output->threshold_deadline = output->ref_deadline - lutEntry->duration_thresh;

}

/**
 * sleepLUTSolver_initialize
 *
 * @brief Init function for the lookup table solver.  This 
 *        function populates the mode_to_enter field in the
 *        lookup table, per the synthLPRM's registered.
 *
 * @return Returns non-zero on error.
 */
static uint32 sleepLUTSolver_initialize( void )
{
  sleep_synth_lpr *registry;
  uint32          freqIdx, durIdx, modeIdx;
  uint32          mode_index;

  registry = synthRegistry_getRegistry();

  /* There will always be at least 1 table */
  for(freqIdx = 0; freqIdx < g_SleepNumOfFreqs; freqIdx++)
  {
    sleep_mLUT *pTable = SleepLPR_power_lookup_table[freqIdx]; 

    for(durIdx = 0; durIdx < pTable->num_elements; durIdx++)
    {
      pTable->element[durIdx].element_stats.min = UINT64_MAX;

      for( modeIdx = 0; modeIdx < SLEEP_LPR_NUM_SYNTHESIZED_MODES; modeIdx++)
      {
        if(-1 == pTable->element[durIdx].mode[modeIdx].mode_idx) 
        {
          pTable->element[durIdx].mode[modeIdx].mode_ptr = NULL; 
        }
        else
        {
          mode_index = pTable->element[durIdx].mode[modeIdx].mode_idx;
          pTable->element[durIdx].mode[modeIdx].mode_ptr = 
            &registry->modes[mode_index];
        }
      }
    }
  }

  return 0;
}

/**
 * sleepLUTSolver_mainFunction
 *
 * @brief This function finds the right low power mode to enter, 
 *        based on a pre-determined lookup table.  Given
 *        duration and latency restrictions, the low power mode
 *        is chosen from the table.
 *
 * @param input:  List of constraints that are used by the solver 
 *                to select modes.
 * @param output: List of output data from the solver -- 
 *                contains the list of modes to enter, as well as
 *                the backoff time that should be used.
 */
static void sleepLUTSolver_mainFunction
(
  sleep_solver_input  *input,
  sleep_solver_output *output
)
{
  int32             i;
  int32             lutEntryIdx;        /* Index of chosen entry in LUT */
  uint64            modeDuration;
  uint64            softDeadline;
  uint64            currentTime;
  uint64            hardDuration;
  uint32            modeEnterExitLat  = 0;
  sleep_synth_lprm  *modeChosen       = NULL;
  sleep_mLUT_elem   *lookupElement    = NULL;
  sleep_fLUT_node   *fLUT             = input->fLUT;

  /* Set default values for output */
  output->selected_synthLPRM = NULL;
  output->threshold_deadline = 0;

  if(SLEEP_ENABLED == synthLPR_getStatus(synthRegistry_getRegistry()))
  {
    /* Get current time to be used for determination of
     * soft deadline and hard duration */
    currentTime = CoreTimetick_Get64();

    softDeadline = currentTime + input->soft_duration;

    /* Calculate hard duration from first hard deadline */
    hardDuration = input->hard_deadlines.minimum - currentTime;

    /* Compare soft deadline against the first of all the hard deadlines
     * to expire, which is the minimum value in the data structure */
    if(input->hard_deadlines.minimum < softDeadline)
    {
      output->ref_deadline  = input->hard_deadlines.minimum;
      modeDuration          = hardDuration;
    }
    else
    {
     /* Use reference deadline based on soft duration - best case as it is
      * just a hint and we are not aware of exact time of wakeup event. */
      output->ref_deadline  = softDeadline;
      modeDuration          = input->soft_duration;
    }

    sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_ATS, SLEEP_ENTER_SOLVER_NUM_ARGS,
                         SLEEP_ENTER_SOLVER_STR, SLEEP_ENTER_SOLVER,
                         fLUT->frequency, 
                         ULOG64_DATA(hardDuration),
                         ULOG64_DATA(input->soft_duration),
                         input->latency_budget );

    /* Find the lookup table entry for this duration. */
    lookupElement = 
      sleepLUTSolver_findElement(modeDuration, fLUT->mLUT_idx, &lutEntryIdx);

    if(lookupElement != NULL)
    {
      sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 3,
                       "Solver table (mLUT: %d) (Duration: %lld)",
                       fLUT->mLUT_idx,
                       ULOG64_DATA(lookupElement->duration_thresh));

      /* Traverse the modes for the duration range found, and find the one
       * that fits the current criteria. */
      for( i = 0; i < SLEEP_LPR_NUM_SYNTHESIZED_MODES; i++ )
      {
        modeChosen = lookupElement->mode[i].mode_ptr;

        /* If we hit a NULL mode, then we've reached the end of the mode list,
         * and didn't find a mode to enter. */
        if( NULL == modeChosen )
        {
          break;
        }

        /* Skip if mode is disabled or if input attr mask does not match */
        if( ( SLEEP_DISABLED == modeChosen->mode_status ) || 
            ( ( input->synth_attr_mask & modeChosen->attr ) == 0 ) )
        {
          continue;
        }

        modeEnterExitLat = synthLPRM_getLPRMLatency( modeChosen, 
                                                     fLUT->mLUT_idx);

        /* Does the mode's enter and exit latency fit in the expected sleep
         * duration, and not violate the latency budget? */
        if( modeEnterExitLat > modeDuration ||
            modeEnterExitLat > input->latency_budget )
        {
          sleepLog_printf( SLEEP_LOG_LEVEL_INFO, 3 + (2*1),
                           "Mode skipped (name: \"%s\") "
                           "(reason: \"Insufficient Time\") "
                           "(Synth latency: 0x%08x) "
                           "(latency budget: 0x%08x) (duration: 0x%llx)",
                           modeChosen->name, 
                           modeEnterExitLat, 
                           input->latency_budget, 
                           ULOG64_DATA(modeDuration) );
          continue;
        }

        /* If we made it this far, then we found the right mode.
         * Record its info, fill output data and return. */
        output->selected_synthLPRM = modeChosen;
        sleepLUTSolver_setThresholdDeadline( input, output, lutEntryIdx );

        sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_ATS, 
                             SLEEP_SOLVER_MODE_CHOSEN_NUM_ARGS,
                             SLEEP_SOLVER_MODE_CHOSEN_STR, 
                             SLEEP_SOLVER_MODE_CHOSEN,
                             modeChosen->name,
                             ULOG64_DATA( output->ref_deadline),
                             ULOG64_DATA( output->threshold_deadline ) );

        /* Prepare the data to return. */
        output->backoff_ptr     = synthLPRM_getBackOffTime(modeChosen, fLUT);
        output->enter_exit_time = modeEnterExitLat;
        break;
      }
    }
  }

  /* We should not hit this on Q6 with ATS and threshold timer but should
   * probably keep it for commonality and the fact that solver by itself is
   * not aware mode latencies. */
  if( NULL == output->selected_synthLPRM )
  {
    sleepLog_printf( SLEEP_LOG_LEVEL_ATS, 0, "No mode chosen" );
  }

  return;
}

sleep_solver_type sleep_lookup_table_solver =
{
  sleepLUTSolver_initialize,
  NULL, /* no deinit function */
  sleepLUTSolver_mainFunction,
  "Lookup Table"
};

