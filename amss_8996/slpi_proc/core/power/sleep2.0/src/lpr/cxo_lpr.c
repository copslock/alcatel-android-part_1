/*============================================================================
  FILE:         cxo_lpr.c

  OVERVIEW:     This file provides the LPR definition for the CXO clock 
                low-power modes.

  DEPENDENCIES: None

                Copyright (c) 2012-2014 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/src/lpr/cxo_lpr.c#3 $
$DateTime: 2014/09/09 15:38:00 $
==============================================================================*/

#include "sleep_lpr.h"
#include "kvp.h"
#include "rpmclient.h"
#include "vmpm.h"
#include "sleep_target.h"
#include "CoreVerify.h"
#include "DDIChipInfo.h"
#include "DDIPlatformInfo.h"
#include "DALDeviceId.h"
#include "DDITimetick.h"

/*==============================================================================
                              INTERNAL VARIABLES
 =============================================================================*/

/**
 * @brief DAL handle for time tick driver to sync local QTimer with global
 *        QTimer. This is SSC specific requirement whenever we wakeup from
 *        xo shutdown or exit from island mode.
 */
static DalDeviceHandle *g_DALTimetickHandle;

/*==============================================================================
                              INTERNAL FUNCTIONS
 =============================================================================*/

/** 
 * CXOShutdownLPR_initializeTarget
 * 
 * @brief Performs target/platform specific initialization for a cxo
 *        shutdown lpr.
 */
static void CXOShutdownLPR_initializeTarget(void)
{
  /* Add target specific code if any */
  return;
}

/*==============================================================================
                               GLOBAL FUNCTIONS
 =============================================================================*/
/** 
 * CXOShutdownLPR_initialize
 * 
 * @brief Performs required initialization for cxo lpr.
 */
void CXOShutdownLPR_initialize(void)
{
  kvp_t *cxoActiveKVP = kvp_create(4 * 3);   /* Active Set KVP for CXO */
  kvp_t *cxoSleepKVP  = kvp_create(4 * 3);   /* Sleep set KVP for CXO */

  const unsigned int cxoEnableKey = 0x62616e45; /* Enab key in little endian */
  const unsigned int cxoSleepReq  = 0;          /* CXO request for Sleep Set */
  const unsigned int cxoActiveReq = 1;          /* CXO request for Active Set */

  /* Preparing and sending one time active set vote for CXO */
  kvp_put(cxoActiveKVP, cxoEnableKey, 
          sizeof(cxoActiveReq), (void *)&cxoActiveReq);
  rpm_post_request(RPM_ACTIVE_SET, RPM_CLOCK_0_REQ, 0, cxoActiveKVP);

  /* Preparing and sending Sleep set vote for cxo.
   * Since for cxo, we are not clearing our vote upon exiting from Sleep, it
   * is sufficient to send the Sleep set vote only once */
  kvp_put(cxoSleepKVP, cxoEnableKey, sizeof(cxoSleepReq), (void *)&cxoSleepReq);
  rpm_post_request(RPM_SLEEP_SET, RPM_CLOCK_0_REQ, 0, cxoSleepKVP);

  /* Target specific initialization - if any */
  CXOShutdownLPR_initializeTarget();

  /* Obtain a DAL handle to time tick driver to sync local qtimer with global
   * one. */
  DalTimetick_Attach("SystemTimer", &g_DALTimetickHandle);
  CORE_VERIFY_PTR( g_DALTimetickHandle );

  /* Relesing KVPs */
  kvp_destroy( cxoActiveKVP );
  kvp_destroy( cxoSleepKVP );

  return;
}

/** 
 * CXOShutdownLPR_enter 
 *  
 * @brief Function that gets called when Master is voting for cxo shutdown 
 *        during a Sleep cycle.
 *
 * @param wakeup_tick: Absolute time tick (in 19.2 Mhz) when Master is 
 *                     expected to wake up.
 */
void CXOShutdownLPR_enter(uint64 wakeup_tick)
{
  return;
}

/** 
 * CXOShutdownLPR_exit 
 *  
 * @brief Exit function which gets called while exiting from Sleep on master 
 *        side for a Sleep cycle when master voted for cxo shutdown.
 */
void CXOShutdownLPR_exit( void )
{
  /* Syncing local Qtimer with global Qtimer. */
  CORE_DAL_VERIFY( DalTimetick_SyncSSCTimer( g_DALTimetickHandle ) );

  return;
}

