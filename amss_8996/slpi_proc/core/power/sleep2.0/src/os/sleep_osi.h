#ifndef SLEEP_OSI_H
#define SLEEP_OSI_H
/*==============================================================================
  FILE:           sleep_osi.h

  OVERVIEW:       This file contains sleep internal declarations of functions 
                  that are used in the common main sleep subsystem

  DEPENDENCIES: None

                Copyright (c) 2013-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.slpi/1.0/power/sleep2.0/src/os/sleep_osi.h#3 $
$DateTime: 2015/02/02 09:52:25 $
==============================================================================*/
#include "DALStdDef.h"
#include "sleep.h"
#include "sleep_os.h"

/*==============================================================================
                           GLOBAL FUNCTION DECLARATIONS
 =============================================================================*/
/**
 * sleepOS_configIdleMode
 * 
 * @brief This function configures how processor idle is handled within sleep.
 * 
 * @param idleMode: Idle time behavior/configuration for next cycle.
 */
void sleepOS_configIdleMode(sleepOS_IdleModeType idleMode);

/**
 * sleepOS_getFrequency
 *
 * @brief A query function to return cpu clock frequency in KHz.
 *
 * @return CPU frequency in KHz.
 */
uint32 sleepOS_getFrequency(void);

/**
 * sleepOS_mainTask
 *
 * @brief This is the main sleep task function
 *  
 * @note Function is located in island memory section.
 */
void sleepOS_mainTask(void *ignored) USLEEP_CODE_SECTION;

#endif /* SLEEP_OSI_H */

