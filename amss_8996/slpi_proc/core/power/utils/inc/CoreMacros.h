/*==============================================================================
@file CoreMacros.h

Common Macros that are used throughout Core

Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

$Header: //components/rel/core.slpi/1.0/power/utils/inc/CoreMacros.h#4 $
==============================================================================*/
#ifndef COREMACROS_H
#define COREMACROS_H

#ifndef MAX
#define MAX( a, b ) ( ((a)>(b)) ? (a) : (b) )
#endif

#ifndef MIN
#define MIN( a, b ) ( ((a)<(b)) ? (a) : (b) )
#endif


#endif
