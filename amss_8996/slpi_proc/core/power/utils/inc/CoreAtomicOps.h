/*==============================================================================
@file CoreAtomicOps.h

Atomic increment and decrement

Copyright (c) 2010-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
      
$Header: //components/rel/core.slpi/1.0/power/utils/inc/CoreAtomicOps.h#4 $
==============================================================================*/
#ifndef COREATOMICOPS_H
#define COREATOMICOPS_H

#ifdef WIN8EA
#include "stdint.h"
#else
#include <stdint.h>
#endif

/**
   @brief Core_AtomicInc - Atomic increment
 
   Increments by one the value of the specified 32-bit variable
   as an atomic operation. 
  
   @param pCount : pointer to the variable to be incremented
   @return unsigned : the resulting incremented value
 */
unsigned Core_AtomicInc( volatile uint32_t * pCount );

/**
   @brief Core_AtomicDec - Atomic decrement
 
   Decrements by one the value of the specified 32-bit variable 
   as an atomic operation. 
  
   @param pCount : pointer to the variable to be decremented
   @return unsigned : the resulting decremented value
 */
unsigned Core_AtomicDec( volatile uint32_t * pCount );
#endif /* COREATOMICOPS_H */
