/*===========================================================================

FILE:         BSPspm.c

DESCRIPTION:  This file contains initializing/obtaining target specific SPM 
              data and related functions. Depending on the target, these 
              data can be obtained in different way like through device 
              config or by direct initialization.


Copyright (c) 2010-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

$Header: //components/rel/core.slpi/1.0/power/spm/src/hal/BSPspm.c#3 $

============================================================================*/


/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "spm.h"
#include "BSPspm.h"
#include "CoreVerify.h"

/* -----------------------------------------------------------------------
**                           GLOBAL TARGET DATA
** ----------------------------------------------------------------------- */

/**
 * g_spmNumCores
 */
unsigned int g_spmNumCores USPM_DATA_SECTION = 0;

/**
 * g_spmBspData
 */
BSP_spm_ConfigDataType *g_spmBspData = NULL;

/**
 * g_spmCmdSeqArray
 */
BSP_spm_CmdSequenceType (*g_spmCmdSeqArray)[SPM_NUM_LOW_POWER_MODES] = NULL;
    

/* -----------------------------------------------------------------------
**                         Functions  
** ----------------------------------------------------------------------- */

/**
 * BSP_spm_InitTargetData
 */
void BSP_spm_InitTargetData( void )
{
  DALSYS_PROPERTY_HANDLE_DECLARE(hSpmDevCfg);
  DALSYSPropertyVar prop;

    CORE_DAL_VERIFY(DALSYS_GetDALPropertyHandleStr("/dev/core/power/spm", 
                                                   hSpmDevCfg));
  
    CORE_DAL_VERIFY(DALSYS_GetPropertyValue( hSpmDevCfg, "spm_num_cores", 0, 
                                             &prop));
    g_spmNumCores = (unsigned int)prop.Val.dwVal;
  
    CORE_DAL_VERIFY(DALSYS_GetPropertyValue( hSpmDevCfg, "spm_bsp_data", 0, 
                                             &prop)); 
    g_spmBspData = (BSP_spm_ConfigDataType *)prop.Val.pStruct;
  
    CORE_DAL_VERIFY(DALSYS_GetPropertyValue( hSpmDevCfg, "spm_cmd_seq_info_array",
                                             0, &prop));
    g_spmCmdSeqArray = 
      (BSP_spm_CmdSequenceType (*)[SPM_NUM_LOW_POWER_MODES])prop.Val.pStruct;

  return;
}

/**
 * BSP_spm_GetNumSupportedLPMs
 */
uint32 BSP_spm_GetNumSupportedLPMs( uint32 coreNum )
{
  int i = 0;

  CORE_VERIFY( coreNum < g_spmNumCores );
  CORE_VERIFY_PTR( g_spmCmdSeqArray );

  while ( i < SPM_NUM_LOW_POWER_MODES ) 
  {
    if ( 0 == g_spmCmdSeqArray[coreNum][i].len )
    {
      break;
    }
    i++;
  }

  return i;
}
 
