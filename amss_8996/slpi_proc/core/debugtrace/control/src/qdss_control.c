/*=============================================================================

FILE:         qdss_control.c

DESCRIPTION:  Implements handlers for diag comamnds to configure and control
              SSC QDSS.

================================================================================
              Copyright (c) 2012 Qualcomm Technologies Incorporated.
                         All Rights Reserved.
                QUALCOMM Proprietary and Confidential
==============================================================================*/
#include "qdss_control_priv.h"
#include "qdss_control.h"
#include "qdss_diag.h"
#include "DALDeviceId.h"
#include "qdss.h"
#include "qdss_tfunnel.h"
#include "qdss_ts.h"
#include "tracer.h"
#include "tracer_event_ids.h"
#include "tracer_micro.h"

struct qdss_control qdss;


/*-------------------------------------------------------------------------*/

/**
  @brief  Handles the QDSS_QUERY_STATUS diag command

  Memory for request and response packets is already allocated
  before calling this function. This applies for all diag command handlers below

  @param                 pReq [in ] :  request packet
  @param              req_len [in ] :  request packet length
  @param                 pRsp [in ] :  response packet
  @param              rsp_len [in ] :  response packet length

  @return 0 if successful, error code otherwise
 */
int qdss_query_status_handler(qdss_query_status_req *pReq,
                              int req_len,
                              qdss_query_status_rsp *pRsp,
                              int rsp_len)
{

   int nErr;

   TRY(nErr,qdss_control_get_sink(&pRsp->trace_sink));
   TRY(nErr,qdss_control_get_stm(&pRsp->stm_enabled));
   TRY(nErr,qdss_control_get_hwe(&pRsp->hw_events_enabled));

   CATCH(nErr) {}
   return nErr;
}

/*-------------------------------------------------------------------------*/

/**
  @brief  Handles the QDSS_FILTER_STM diag command

  @return 0 if successful, error code otherwise
 */
int qdss_filter_stm_handler(qdss_filter_stm_req *pReq,
                          int req_len,
                          qdss_filter_stm_rsp *pRsp,
                          int rsp_len)
{
   int nErr;

   if (pReq->state) {
      TRY(nErr,qdss_stm_trace_enable());
   }
   else {
      TRY(nErr,qdss_stm_trace_disable());
   }

   CATCH(nErr){}
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/

/**
  @brief  Handles the QDSS_FILTER_HWEVENTS_ENABLE diag command

  @return 0 if successful, error code otherwise
 */
int qdss_filter_hwevents_handler(qdss_filter_hwevents_req *pReq,
                          int req_len,
                          qdss_filter_hwevents_rsp *pRsp,
                          int rsp_len)

{
   int nErr;

   TRY(nErr,qdss_control_set_hwe(pReq->state));

   CATCH(nErr){}

   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/

/**
  @brief  Handles the QDSS_FILTER_HWEVENTS_CONFIGURE diag command

  @return 0 if successful, error code otherwise
 */
int qdss_filter_hwevents_configure_handler(qdss_filter_hwevents_configure_req *pReq,
                          int req_len,
                          qdss_filter_hwevents_configure_rsp *pRsp,
                          int rsp_len)

{
   int nErr;

   TRY(nErr,qdss_control_set_hwe_reg(pReq->register_addr,
                                     pReq->register_value));

   CATCH(nErr) {}
   pRsp->result = nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
int qdss_filter_hwevents_set_detect_mask_handler(qdss_filter_hwevents_set_detect_mask_req *pReq,
                        int req_len,
                        qdss_filter_hwevents_set_detect_mask_rsp *pRsp,
                        int rsp_len)
{
   int nErr = QDSS_HWEVENT_UNKNOWN_ERR;
   TRY(nErr,qdss_control_set_hwe_detect_mask(pReq->hwe_mask));
   CATCH(nErr) {}
   pRsp->result=nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
int qdss_filter_hwevents_set_trigger_mask_handler(qdss_filter_hwevents_set_trigger_mask_req *pReq,
                        int req_len,
                        qdss_filter_hwevents_set_trigger_mask_rsp *pRsp,
                        int rsp_len)
{
   int nErr = QDSS_HWEVENT_UNKNOWN_ERR;
   TRY(nErr,qdss_control_set_hwe_trigger_mask(pReq->hwe_mask));
   CATCH(nErr) {}
   pRsp->result=nErr;
   return nErr;
}

/*-------------------------------------------------------------------------*/
void qdss_usleep_notification(uSleep_state_notification state)
{
   switch(state) {
   case USLEEP_STATE_ENTER:
      QDSS_IslandEnter();
      break;
   case USLEEP_STATE_EXIT:
      QDSS_IslandExit();
      break;
   default:
      //do nothing
      break;
   }
}

/*-------------------------------------------------------------------------*/
void qdss_register_usleep(void) 
{
   //TODO: Need to time enter exit latencies.
   qdss.huSleep=uSleep_registerNotificationCallback(300,600,qdss_usleep_notification);
}

/*-------------------------------------------------------------------------*/
int qdss_enable_local_etb(void) 
{
   int nErr;
   int tmc_mode = TMC_MODE_CIRCULAR_BUFFER;
   
   if ( (qdss.current_trace_sink == QDSS_TRACESINK_CBUF) ||
        (qdss.current_trace_sink == QDSS_TRACESINK_HWFIFO)) {
      tmc_mode = qdss.current_trace_sink;
   }
   else {
      qdss.current_trace_sink = QDSS_TRACESINK_AUTO;
      tmc_mode = TMC_MODE_CIRCULAR_BUFFER;
   }
   TRY(nErr,DalTMC_SetMode(qdss.hTMC,tmc_mode));
   TRY(nErr,DalTMC_EnableTrace(qdss.hTMC));

   CATCH(nErr){}

   return nErr;
}

/*-------------------------------------------------------------------------*/

/**
  @brief  Ensure QDSS HW is ready to be configure

 This function votes QDSS clocks on, attach to drivers and init hardware
 If this is already done this function returns success

  @return 0 if successful, error code otherwise
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_STATE = A handle is not valid.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
 */
int qdss_ensure_hw_ready(void)
{
   int nErr;


   if ((NULL == qdss.hTMC)||(NULL == qdss.hFunnel)||(NULL == qdss.hSTMCfg)||
       (NULL == qdss.hHWEvent) || (NULL==qdss.huSleep))
      {

      //Make sure clocks are turned on.
      TRY(nErr,QDSSOn());

      if (NULL == qdss.hTMC) {
         TRY(nErr, DAL_TMCDeviceAttach("DALDEVICEID_TMC_SLPI",&qdss.hTMC));
         TRY(nErr, qdss_enable_local_etb());
      }

      if (NULL == qdss.hFunnel) {
         TRY(nErr,DAL_TFunnelDeviceAttach(DALDEVICEID_TFUNNEL_0,&qdss.hFunnel));
      }

      if (NULL == qdss.hSTMCfg) {
         TRY(nErr,DAL_STMCfgDeviceAttach("DALDEVICEID_STM_CONFIG_SLPI",&qdss.hSTMCfg));
      }

      if (NULL == qdss.hHWEvent) {
         TRY(nErr,DAL_HWEventDeviceAttach("DALDEVICEID_HWEVENT",&qdss.hHWEvent));
      }

      if (NULL== qdss.huSleep) {
         qdss_register_usleep();
      }

      TRY(nErr,qdss_csr_init());

   }

   nErr = QDSS_CONTROL_SUCCESS;

   CATCH(nErr){
      if (DAL_ERROR==nErr) {
         nErr = QDSS_CONTROL_BAD_STATE;
      }
      else if (DAL_ERROR_DEVICE_ACCESS_DENIED==nErr)
      {
         nErr = QDSS_CONTROL_FUSE_BLOWN;
      }
   }

   return nErr;
}


int qdss_control_on_bootup(void);

/*-------------------------------------------------------------------------*/

#define QDSS_INVALID_TRACE_SINK  0xFF

/**
  @brief Initializes the QDSS control subsystem.

  This is called from RC init. QDSS HW is not touched or clocked.
  That happens when explicitly turned on by DIAG commands

  @return None
 */
void qdss_control_init(void)
{
   int nErr;
   qdss_diag_init();
   qdss.hTracer = 0;
   qdss.hTMC = 0;
   qdss.hFunnel = 0;
   qdss.hSTMCfg = 0;
   qdss.hSync = 0;
   qdss.current_trace_sink = QDSS_TRACESINK_CBUF;
   qdss.bTraceSinkEnabled = FALSE;
   TRY(nErr,DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, &qdss.hSync, NULL));
   qdss.ts.timer_us = 0;
   qdss.npa_client = npa_create_sync_client("/clk/qdss", "qdss_slpi_stm", NPA_CLIENT_REQUIRED);
   
   TRY(nErr,qdss_control_on_bootup());

   CATCH(nErr) {
   }
   IGNORE(nErr);
}

/* Below are qdss control interface functions. Definitions of these
 * are in qdss_contro.h
 */

/*-------------------------------------------------------------------------*/
/*
   Return:
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_STATE = A handle is not valid.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
      QDSS_CONTROL_DEVICE_ERROR = STM driver error.
*/
int qdss_control_get_stm(uint8 *state)
{
   uint32 uStatus = 0;
   int nErr;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());

   THROW_IF(nErr,DAL_SUCCESS !=
            DalSTMCfg_STMStatus(qdss.hSTMCfg,STM_EN,&uStatus),
            QDSS_CONTROL_DEVICE_ERROR);

   *state =  (uStatus)?1:0;

   CATCH(nErr){}

   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}

/*-------------------------------------------------------------------------*/
/*
   Return:
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_STATE = A handle is not valid.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
      QDSS_CONTROL_DEVICE_ERROR = STM driver error.
*/
int qdss_control_set_stm(uint8 state)
{
   int nErr;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());

   //disable first, so enable would actually be a re-enable
   //This is to ensure ASYNC gets generated anytime this is called.
   THROW_IF(nErr,DAL_SUCCESS !=
            DalSTMCfg_STMControl(qdss.hSTMCfg,STM_EN,0),
            QDSS_CONTROL_DEVICE_ERROR);

   THROW_IF(nErr,DAL_SUCCESS !=
            DalSTMCfg_STMControl(qdss.hSTMCfg,STM_EN,state),
            QDSS_CONTROL_DEVICE_ERROR);

   CATCH(nErr){}

   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}

/*-------------------------------------------------------------------------*/
/*
   Return:
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_STATE = A handle is not valid.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
      QDSS_CONTROL_DEVICE_ERROR = STM driver error.
*/
int qdss_control_get_hwe(uint8 *state)
{
   uint32 uStatus = 0;
   int nErr;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());

   THROW_IF(nErr,DAL_SUCCESS !=
            DalSTMCfg_STMStatus(qdss.hSTMCfg,STM_HW_EVT_EN,&uStatus),
            QDSS_CONTROL_DEVICE_ERROR);

   *state =  (uStatus)?1:0;

   CATCH(nErr){}

   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}


/*-------------------------------------------------------------------------*/
/*
   Return:
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_STATE = A handle is not valid.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
      QDSS_CONTROL_DEVICE_ERROR = STM driver error.
*/
int qdss_control_set_hwe(uint8 state)
{
  int nErr;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());
   if (state) {
      //sync timestamps before enabling HW events
      qdss_csr_timestamp_sync();
   }
   else {
      //make sure we resync for next session
      qdss_csr_timestamp_sync_renable();
   }

   THROW_IF(nErr,DAL_SUCCESS !=
            DalSTMCfg_STMControl(qdss.hSTMCfg,STM_HW_EVT_EN,state),
            QDSS_CONTROL_ERROR);

   /*Enable default pre-compiled HW events if state==1*/
   if (state==0x1)
   {
      THROW_IF(nErr,DAL_SUCCESS !=
               DAL_HWEvent_ConfigurePreset(qdss.hHWEvent),
               QDSS_CONTROL_DEVICE_ERROR);
   }

   CATCH(nErr){}

   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}

/*-------------------------------------------------------------------------*/
/*
   Return:
      QDSS_CONTROL_SUCCESS = Successful.
      QDSS_CONTROL_BAD_PARAM = Invalid address.
      QDSS_CONTROL_FUSE_BLOWN = QDSS fuse is blown.
      QDSS_CONTROL_ERROR = Failed.
*/
int qdss_control_set_hwe_reg(unsigned long addr, unsigned long val)
{
   int nErr;
   uint8 ret_code =(uint8) QDSS_HWEVENT_UNKNOWN_ERR;;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);

   TRY(nErr,qdss_ensure_hw_ready());

   TRY(nErr,DAL_HWEvent_SetRegister(qdss.hHWEvent, addr, val,
                                    &ret_code));

   nErr = QDSS_HWEVENT_CONFIG_SUCCESS;
   CATCH(nErr) {}

   DALSYS_SyncLeave(qdss.hSync);
   return (QDSS_HWEVENT_CONFIG_SUCCESS == ret_code) ? QDSS_CONTROL_SUCCESS :
          (QDSS_HWEVENT_FUSE_CHECK_ERR == ret_code) ? QDSS_CONTROL_FUSE_BLOWN :
          (QDSS_HWEVENT_ADDR_CHECK_ERR == ret_code) ? QDSS_CONTROL_BAD_PARAM :
                                                      QDSS_CONTROL_ERROR;
}

/*-------------------------------------------------------------------------*/
int qdss_control_set_hwe_detect_mask(uint32 bit_mask)
{
   int nErr;
   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());

   THROW_IF(nErr, DAL_SUCCESS !=
            DalSTMCfg_STMControl(qdss.hSTMCfg,STM_HW_EVT_ENABLE_MASK,bit_mask),
            QDSS_CONTROL_ERROR);
   nErr = QDSS_CONTROL_SUCCESS;

   CATCH(nErr) {}
   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}

/*-------------------------------------------------------------------------*/
int qdss_control_set_hwe_trigger_mask(uint32 bit_mask)
{
   int nErr;
   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,qdss_ensure_hw_ready());

   THROW_IF(nErr, DAL_SUCCESS !=
            DalSTMCfg_STMControl(qdss.hSTMCfg,STM_HW_EVT_TRIGGER_MASK,bit_mask),
            QDSS_CONTROL_ERROR);
   nErr = QDSS_CONTROL_SUCCESS;

   CATCH(nErr) {}
   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}
/*-------------------------------------------------------------------------*/

boolean qdss_stm_trace_is_enabled (void) 
{
   uint8 stm_enabled = 0;

   if ((NULL == qdss.hTMC)||(NULL == qdss.hFunnel)||(NULL == qdss.hSTMCfg)||
       (DAL_SUCCESS!=qdss_control_get_stm(&stm_enabled)) ||
       (0==stm_enabled)) {
      return FALSE;
   }
   return TRUE;

}

extern void tracer_set_local_stm_enable(boolean enable);


/*-------------------------------------------------------------------------*/

#define _CLOCK_QDSS_LEVEL_LOW  3 // ClockDefs.h not up-to-date

int qdss_stm_trace_enable(void)
{
   int nErr = 0;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);
   TRY(nErr,QDSSOn());

   if (NULL!= qdss.npa_client) {
      //An additional fixed frequency vote for STM.
      npa_issue_required_request(qdss.npa_client, _CLOCK_QDSS_LEVEL_LOW); 
   }

   TRY(nErr,qdss_ensure_hw_ready());
   TRY(nErr,qdss_enable_local_etb());
   TRY(nErr, qdss_tfunnel_enable_port(TFUNNEL_SSC_STM_PORT));
   qdss_csr_timestamp_sync();
   TRY(nErr, qdss_control_set_stm(1)); //1 for enable


   //Drain the initial sync event out
   QDSS_OpenSSCFunnel();
   ETBSwitchToHWFIFO();
   QDSSLogSyncEvents(QDSS_SSC_TS_SYNC);
   QDSSEndMarker(QDSS_SSC_ETB_NORMAL_MODE_START);
   ETBSwitchToCBUF();
   QDSS_CloseSSCFunnel();
   QDSSModeSwitchMarker(QDSS_SSC_ETB_NORMAL_MODE_START);
   QDSSLogSyncEvents(QDSS_SSC_TS_SYNC);



   CATCH(nErr) {}

   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}

/*-------------------------------------------------------------------------*/

int qdss_stm_trace_disable(void)
{
   int nErr = 0;

   if (0==qdss.hSync) {
      return QDSS_CONTROL_BAD_STATE;
   }

   DALSYS_SyncEnter(qdss.hSync);

   if (qdss_stm_trace_is_enabled()) {
      //Drain the final sync event out
      QDSS_OpenSSCFunnel();
      ETBSwitchToHWFIFO();
      QDSSLogSyncEvents(QDSS_SSC_TS_SYNC);
      QDSSEndMarker(QDSS_SSC_ETB_NORMAL_MODE_END);
      ETBSwitchToCBUF();
      QDSS_CloseSSCFunnel();


      TRY(nErr, qdss_tfunnel_disable_port(TFUNNEL_SSC_STM_PORT));
      TRY(nErr, qdss_control_set_stm(0)); //0 for disable
   }
   CATCH(nErr) {}     

   qdss_csr_timestamp_sync_renable();
   DALSYS_SyncLeave(qdss.hSync);
   return nErr;
}



/*-------------------------------------------------------------------------*/
 
void QDSS_IslandEnter(void)
{
   if (0==qdss.hSync) {
      return;
   }

   DALSYS_SyncEnter(qdss.hSync);

   if (qdss_stm_trace_is_enabled()) {
      tracer_event_simple(QDSS_ISLAND_ENTER);
      QDSS_OpenSSCFunnel();
      ETBSwitchToHWFIFO();
      QDSSLogSyncEvents(QDSS_SSC_TS_SYNC);
      QDSSEndMarker(QDSS_SSC_ETB_NORMAL_MODE_END);
      ETBSwitchToCBUF();
      QDSS_CloseSSCFunnel();
      QDSSModeSwitchMarker(QDSS_SSC_ETB_ISLAND_MODE_START);
   }
   DALSYS_SyncLeave(qdss.hSync);

}

/*-------------------------------------------------------------------------*/
uint32  HAL_qdss_etfetb_GetWritePtr();

void QDSS_IslandExit(void)
{
   uint32 etb_write_ptr = 0;

   if (0==qdss.hSync) {
      return;
   }

   DALSYS_SyncEnter(qdss.hSync);

   if (qdss_stm_trace_is_enabled()) {
      etb_write_ptr = HAL_qdss_etfetb_GetWritePtr();
      QDSS_OpenSSCFunnel();
      ETBSwitchToHWFIFO();
      UTRACER_EVENT(QDSS_ISLAND_EXIT,etb_write_ptr);
      QDSSLogSyncEvents(QDSS_SSC_TS_SYNC);
      QDSSEndMarker(QDSS_SSC_ETB_ISLAND_MODE_END);
      ETBSwitchToCBUF();
      QDSS_CloseSSCFunnel();
      QDSSModeSwitchMarker(QDSS_SSC_ETB_NORMAL_MODE_START);
   }

   DALSYS_SyncLeave(qdss.hSync);
}

/*-------------------------------------------------------------------------*/

//Uncomment below line to enable sensor ETM TRACE on boot-up
//#define QDSS_ENABLE_SENSOR_ETM_ON_BOOTUP  1

//Uncomment below line to enable sensor ETM after a given delay
//#define QDSS_ENABLE_SENSOR_ETM_DEFERRED  1


int qdss_control_on_bootup(void)
{
   int nErr = QDSS_CONTROL_SUCCESS;

#ifdef QDSS_ENABLE_SENSOR_ETM_ON_BOOTUP
   TRY(nErr,qdss_control_set_sink(QDSS_TRACESINK_AUTO));
   TRY(nErr,qdss_control_set_etm(1));
   CATCH(nErr) {}
#elif QDSS_ENABLE_SENSOR_ETM_DEFERRED
   qdss.ts.timer_us = 30000000;
   qdss_ts_start_timer();
#endif

   return nErr;
}

