/*
===========================================================================
*/
/**
  @file ClockSSC.c 
  
  Main entry point for the MSM8996 SSC clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2012 - 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.slpi/1.0/systemdrivers/clock/hw/msm8996/src/ClockSSC.c#12 $
  $DateTime: 2016/02/03 20:40:36 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  09/07/11   dcf     Created.
 
  ====================================================================
*/ 



/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "DDIChipInfo.h"
#include "ClockDriver.h"
#include "ClockSSC.h"
#include "busywait.h"
#include "ClockSWEVT.h"
#include "ClockSSCCPU.h"
#include "ClockSSCHWIO.h"
#include "uClock.h"

/*=========================================================================
      Macros
==========================================================================*/

#define CLOCK_ID_CPU   "ssc_q6core"

/*=========================================================================
      Type Definitions
==========================================================================*/

uint32 Clock_nHWIOBaseSSC;

/*=========================================================================
      Data
==========================================================================*/

static ClockImageCtxtType Clock_ImageCtxt;

/* =========================================================================
      Functions
==========================================================================*/

extern DALResult Clock_UpdateWarmupTime
(
  ClockDrvCtxt *pDrvCtxt,
  VCSCornerType eVRegLevel
);

extern boolean uClock_Init(void);
//extern DALResult Clock_DoCPUTests(ClockDrvCtxt* pDrvCtxt);

/* =========================================================================
**  Function : Clock_DetectBSPVersion
** =========================================================================*/
/**
  Detects which BSP configuration to use for the current HW.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if a valid configuration was not found, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_DetectBSPVersion
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockImageCtxtType     *pImageCtxt;
  ClockCPUPerfConfigType *pCPUPerfConfig;
  uint32                  i;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Detect which CPU BSP data to use for this HW version.                 */
  /*-----------------------------------------------------------------------*/

  pCPUPerfConfig = pImageCtxt->pBSPConfig->pCPUPerfConfig;
  for (i = 0; i < pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs; i++)
  {
    if (Clock_IsBSPSupported(&pCPUPerfConfig[i].HWVersion) == TRUE)
    {
      pImageCtxt->CPUCtxt.PerfConfig.HWVersion = pCPUPerfConfig[i].HWVersion;
      pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel = pCPUPerfConfig[i].anPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel = pCPUPerfConfig[i].nMaxPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMinPerfLevel = pCPUPerfConfig[i].nMinPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nNumPerfLevels = pCPUPerfConfig[i].nNumPerfLevels;

      break;
    }
  }

  if (i == pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs)
  {
    return DAL_ERROR;
  }

  return DAL_SUCCESS;

} /* END of Clock_DetectBSPVersion */


/* =========================================================================
**  Function : ClockStub_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult ClockStub_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */

/* =========================================================================
**  Function : Clock_InitCPUConfig
** =========================================================================*/
/**
  Initializes current configuration of CPU clock
 
  This function is invoked at driver initialization to initialize the current
  CPU configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_InitCPUConfig
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult            eRes;
  ClockNodeType       *pClock;
  ClockSourceNodeType *pSource;
  ClockImageCtxtType  *pImageCtxt;
  ClockCPUConfigType  *pConfig;
  uint32               nPL, nConfig;
  uint32               nSourceIndex;
  HAL_clk_SourceType   eSubSource;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get CPU clock ID.                                                     */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_CPU,
      (ClockIdType *)&pImageCtxt->CPUCtxt.pClock);
  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }

  pClock = pImageCtxt->CPUCtxt.pClock;

  /*-----------------------------------------------------------------------*/
  /* Find the max performance level.                                       */
  /*-----------------------------------------------------------------------*/

  nPL = pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel-1;
  nConfig = pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel[nPL];
  pConfig = &pImageCtxt->pBSPConfig->pCPUConfig[nConfig];

  /*-----------------------------------------------------------------------*/
  /* Configure the CPU to the max performance level.                       */
  /*-----------------------------------------------------------------------*/

  HAL_clk_ConfigClockMux(pClock->pDomain->HALHandle, &pConfig->Mux.HALConfig);

  /*-----------------------------------------------------------------------*/
  /* Update state.                                                         */
  /*-----------------------------------------------------------------------*/

  nSourceIndex = pDrvCtxt->anSourceIndex[pConfig->Mux.HALConfig.eSource];
  if (nSourceIndex == 0xFF)
  {
    return DAL_ERROR;
  }

  pSource = &pDrvCtxt->aSources[nSourceIndex];
  if (pSource == NULL)
  {
    return DAL_ERROR;
  }

  pClock->pDomain->pSource = pSource;
  pImageCtxt->CPUCtxt.pConfig = pConfig;
  pClock->pDomain->pActiveMuxConfig = &pConfig->Mux;

  /*-----------------------------------------------------------------------*/
  /* Connect the active frequency config.                                  */
  /*-----------------------------------------------------------------------*/

  pSource->pActiveFreqConfig = pConfig->Mux.pSourceFreqConfig;
  eSubSource = pConfig->Mux.pSourceFreqConfig->HALConfig.eSource;
  if(eSubSource != HAL_CLK_SOURCE_NULL)
  {
    nSourceIndex = pDrvCtxt->anSourceIndex[eSubSource];
    if( nSourceIndex != 0xFF )
    {
      pSource->pSource = &pDrvCtxt->aSources[nSourceIndex];
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Ensure that the CPU core clock/domain/source reference counts are 1.  */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, (ClockIdType)pImageCtxt->CPUCtxt.pClock);

  /*-----------------------------------------------------------------------*/
  /* Initialize the DCVS module.                                           */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitDCVS(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Temporary:  Update the voltage warmup time.  This will be moved to    */
  /* VCS once details are worked out.                                      */
  /*-----------------------------------------------------------------------*/

  Clock_UpdateWarmupTime(pDrvCtxt, pConfig->Mux.eVRegLevel);

  return DAL_SUCCESS;

} /* END Clock_InitCPUConfig */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult eRes;
  uint32 nIdx, nSourceIndex;
  HAL_clk_SourceType eSubSource;
  ClockPropertyValueType PropVal = NULL;
  ClockSourceInitType* pInitSources = NULL;
  HAL_clk_HWIOBaseType *pHWIOBases = NULL;
  ClockImageBSPConfigType* pBSPConfig = NULL;

  /*-----------------------------------------------------------------------*/
  /* Get the CPU Configurations.                                           */
  /*-----------------------------------------------------------------------*/

  if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfig", &PropVal))
  {
    return(DAL_ERROR);
  }

  pBSPConfig = (ClockImageBSPConfigType*)PropVal;

  /*
   * Check for Istari Pro here.
   */
  if (DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MSM8996SG)
  {
    Clock_ImageCtxt.pBSPConfig = &pBSPConfig[1];
  }
  else
  {
    for (nIdx = 0; nIdx < 2; nIdx++)
    {
      if (Clock_IsBSPSupported(&pBSPConfig[nIdx].HWVersion) == TRUE)
      {
        Clock_ImageCtxt.pBSPConfig = &pBSPConfig[nIdx];
        break;
      }
    }
  }

  if (Clock_ImageCtxt.pBSPConfig == NULL)
  {
    return(DAL_ERROR);
  }

  pDrvCtxt->pImageCtxt = &Clock_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Attach to the VCS DAL.                                                */
  /*-----------------------------------------------------------------------*/

  eRes = DAL_StringDeviceAttach("VCS", &Clock_ImageCtxt.hVCS);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to attach to VCS DAL: %d", eRes);
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6SS HWIO Base address.                                */
  /*-----------------------------------------------------------------------*/

  if(Clock_nHWIOBaseSSC == 0)
  {
    eRes = Clock_GetPropertyValue("ClockHWIOBase", &PropVal);

    if (eRes != DAL_SUCCESS)
    {
      return(eRes);
    }

    pHWIOBases = (HAL_clk_HWIOBaseType*)PropVal;

    if (pHWIOBases != NULL)
    {
      Clock_MapHWIORegion(
        pHWIOBases->nPhysAddr, pHWIOBases->nSize, &Clock_nHWIOBaseSSC);

      /*
       * If we are unable to map a virtual address, assume the physical one.
       */
      if(Clock_nHWIOBaseSSC == NULL)
      {
        Clock_nHWIOBaseSSC = pHWIOBases->nPhysAddr;
      }
    }
    else
    {
      /*
       * We were unable to get the base address.
       */
      return(DAL_ERROR);
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Detect the BSP version to use.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectBSPVersion(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Failed to detect the BSP version.");
    return eRes;
  }  

  /*-----------------------------------------------------------------------*/
  /* Connect the active frequency configuration for the main PLL. This has */
  /* already been configured and enabled pre-main so this step ensures SW  */
  /* has correct data structures initialized.  Additionally, set a         */
  /* suppressible reference count on this source.                          */
  /*-----------------------------------------------------------------------*/

  nIdx = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_SSCPLL0];

  if ((DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(3,0)) && 
      (DalChipInfo_ChipFamily() != DALCHIPINFO_FAMILY_MSM8996SG))
  {
    pDrvCtxt->aSources[nIdx].pActiveFreqConfig = 
      &pDrvCtxt->aSources[nIdx].pBSPConfig->pSourceFreqConfig[0];
  }
  else
  {
    pDrvCtxt->aSources[nIdx].pActiveFreqConfig = 
      &pDrvCtxt->aSources[nIdx].pBSPConfig->pSourceFreqConfig[1];
    
  }
  pDrvCtxt->aSources[nIdx].nReferenceCountSuppressible++;

  eSubSource = 
    pDrvCtxt->aSources[nIdx].pBSPConfig->pSourceFreqConfig->HALConfig.eSource;

  if( eSubSource != HAL_CLK_SOURCE_NULL )
  {
    nSourceIndex = pDrvCtxt->anSourceIndex[eSubSource];
    if(nSourceIndex != 0xFF)
    {
      pDrvCtxt->aSources[nIdx].pSource = &pDrvCtxt->aSources[nSourceIndex];
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the XO module.                                             */
  /*-----------------------------------------------------------------------*/

  Clock_InitXO(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize CPU core clock frequency configuration.                    */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitCPUConfig(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect CPU core clock configuration.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* May need to update the calibration frequency config as it may be      */
  /* different between chip revisions and needs to be consisten with test  */
  /* environments.  Default is set to V3.                                  */
  /*-----------------------------------------------------------------------*/

  if ((DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(3, 0)) && 
       (DalChipInfo_ChipFamily() != DALCHIPINFO_FAMILY_MSM8996SG))
  {

    if(DAL_SUCCESS != Clock_GetPropertyValue("ClockCPUCalCfgV2", &PropVal))
    {
      return(DAL_ERROR);
    }

    nSourceIndex = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_SSCPLL1];
    if((nSourceIndex != 0xFF) && (PropVal != NULL))
    {
      pDrvCtxt->aSources[nSourceIndex].pBSPConfig->pCalibrationFreqConfig =
        (ClockSourceFreqConfigType*)PropVal;
    }
    else
    {
      return DAL_ERROR;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6PLL if applicable to this chipset.                   */
  /*-----------------------------------------------------------------------*/

  if(DAL_SUCCESS == Clock_GetPropertyValue("ClockSourcesToInit", &PropVal))
  {
    pInitSources = (ClockSourceInitType*)PropVal;

    if (pInitSources != NULL)
    {
      nIdx = 0;
      while (pInitSources[nIdx].eSource != HAL_CLK_SOURCE_NULL)
      {
        Clock_InitSource(pDrvCtxt,
                         pInitSources[nIdx].eSource,
                         pInitSources[nIdx].nFreqHz);
        nIdx++;
      }
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Now that NPA is initialized, allow Q6 scaling by the power manager.   */
  /*-----------------------------------------------------------------------*/

  Clock_EnableDCVS(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize the NPA Voltage NPA node.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_InitVdd(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize the micro clock interface.                                 */
  /*-----------------------------------------------------------------------*/

  if (FALSE == uClock_Init())
  {
    return DAL_ERROR;
  }

  //(void)Clock_DoCPUTests(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_ProcessorSleep
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorSleep
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
)
{

  return DAL_SUCCESS;

} /* END Clock_ProcessorSleep */


/* =========================================================================
**  Function : Clock_ProcessorRestore
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorRestore
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
) 
{

  /*
   * Nothing to do here. HW_VOTE does SPMCTL override handling.
  */

  return DAL_SUCCESS;

} /* END Clock_ProcessorRestore */


/* =========================================================================
**  Function : Clock_AdjustSourceFrequency
** =========================================================================*/
/*
  See DDIClock.h
*/
DALResult Clock_AdjustSourceFrequency
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockSourceType eSource,
   int32           nDeltaLAlpha
)
{
  return(DAL_ERROR);

} /* Clock_AdjustSourceFrequency */


DALResult Clock_SelectClockSource
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockIdType     nClock,
   ClockSourceType eSource
)
{

  if(eSource == CLOCK_SOURCE_PRIMARY)
  {
    return(DAL_SUCCESS);
  }
  return(DAL_ERROR);

} /* Clock_SelectClockSource */


/* =========================================================================
**  Function : Clock_GetImageCtxt
** =========================================================================*/
/*
  See ClockSSC.h
*/

ClockImageCtxtType* Clock_GetImageCtxt(void)
{
  return(&Clock_ImageCtxt);
}

/*
 * Unused APIs are stubbed out here.
 */

DALResult Clock_ImageBIST
(
  ClockDrvCtxt  *pDrvCtxt,
  boolean       *bBISTPassed,
  uint32        *nFailedTests
)
{
  return(DAL_ERROR);
}

DALResult Clock_LoadNV
(
  ClockDrvCtxt  *pDrvCtxt
)
{
  return(DAL_ERROR_NOT_SUPPORTED);
}

