/*=========================================================================

                         Busywait 

GENERAL DESCRIPTION
  This file contains the implementation of the busywait() function for
  hardware based blocking waits.

EXTERNALIZED FUNCTIONS
  busywait

INITIALIZATION AND SEQUENCING REQUIREMENTS
 None

Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
  $Header: //components/rel/core.slpi/1.0/systemdrivers/busywait/src/busywait.c#2 $
  $DateTime: 2015/04/03 15:05:45 $
  $Author: pwbldsvc $
  $Change: 7816328 $

when       who     what, where, why 
--------   ---     --------------------------------------------------------- 
11/23/11   pbitra     Initial version.

==========================================================================*/ 

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

#include "DALSys.h"
#include "HALhwio.h"
#include "busywaitTimerHWIO.h"
#include "busywait.h"


/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      Constants and Macros 
==========================================================================*/

#define BUSYWAIT_XO_FREQUENCY_IN_KHZ 20000

/*=========================================================================
      Typedefs
==========================================================================*/


/*=========================================================================
      Variables
==========================================================================*/
static boolean IsBusywaitEnabled = 0;

/*==========================================================================

                         FUNCTIONS FOR MODULE

==========================================================================*/

/*==========================================================================

  FUNCTION      BUSYWAIT_INIT

  DESCRIPTION   This function enables the Busywait Counter.

  PARAMETERS    None

  DEPENDENCIES  None

  RETURN VALUE  None

  SIDE EFFECTS  None

==========================================================================*/

void busywait_init(void)
{
  if (! IsBusywaitEnabled)
  {
     /* Enable the Busywait Counter. */
       HWIO_SSC_IEC_BSY_WAIT_OUTM(SSC_MCC_REGS_REG_BASE, \
          HWIO_SSC_IEC_BSY_WAIT_BUSY_WAIT_EN_BMSK,\
          (1 << HWIO_SSC_IEC_BSY_WAIT_BUSY_WAIT_EN_SHFT));

      IsBusywaitEnabled = 1;
  }
}

/*==========================================================================

  FUNCTION      BUSYWAIT

  DESCRIPTION   This function pauses the execution of a thread for a
                specified time.

  PARAMETERS    pause_time_us - Time to pause in microseconds

  DEPENDENCIES  None

  RETURN VALUE  None.

  SIDE EFFECTS  The Busywait counter will be enabled and left running to 
                support nested busywait calls, i.e. to support 
                multithreading and/or ISRs.

==========================================================================*/

void busywait (uint32 pause_time_us)
{
  uint32 start_count = 0;
  uint64 delay_count = 0;

  if (IsBusywaitEnabled)
  {
    start_count = HWIO_SSC_IEC_BUSY_WAIT_CNTR_VAL0_IN(SSC_MCC_REGS_REG_BASE);
    /*
     * Perform the delay.
     */
    delay_count = (pause_time_us * (uint64)BUSYWAIT_XO_FREQUENCY_IN_KHZ)/1000;
    while ((HWIO_SSC_IEC_BUSY_WAIT_CNTR_VAL0_IN(SSC_MCC_REGS_REG_BASE) 
                                         - start_count) < delay_count);
  }
}

