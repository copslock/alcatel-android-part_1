#===============================================================================
#                    Copyright 2014-2015 Qualcomm Technologies Incorporated.
#                           All Rights Reserved.
#                      Qualcomm Confidential and Proprietary
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.slpi/1.0/systemdrivers/InterruptController/build/interruptcontroller.scons#7 $
#  $DateTime: 2015/06/08 12:32:36 $
#  $Author: pwbldsvc $
#  $Change: 8312848 $
#
#===============================================================================
# DAL InterruptController Lib
#-------------------------------------------------------------------------------
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/systemdrivers/InterruptController"
SRCPATHSCRIPTS = env['BUILD_ROOT']+'/core/systemdrivers/InterruptController/scripts'
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

INTC_IMAGES = ['CORE_SLPI_ROOT', 'CORE_SLPI_USER']
INTC_IMAGES_ROOT = ['CORE_SLPI_ROOT']

#-------------------------------------------------------------------------------
# Source Code
#-------------------------------------------------------------------------------

env.PublishPrivateApi('DAL_INTERRUPTCONTROLLER', [
   "${INC_ROOT}/core/api/kernel/libstd/stringl",
   "${INC_ROOT}/core/api/kernel/qurt",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src",
   "${INC_ROOT}/core/systemdrivers/InterruptController/inc",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src/qurt",
   "${INC_ROOT}/core/systemdrivers/InterruptController/src/qurt/uimage",
   "${INC_ROOT}/core/systemdrivers/hal/aonint/inc",
])

env.Replace(SRC_DIR='qurt')
env.Append(CPPDEFINES = ["DALINTERRUPT_LOG"])
env.Append(CPPDEFINES = ["INTERRUPT_LOG_ENTRIES=0"])
env.Append(CPPDEFINES = ["DALINTERRUPT_MPM_WAKEUP"])
env.Append(CPPPATH = [
   "${INC_ROOT}/core/api/kernel/qurt",
 ])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DEBUGTOOLS',
   'MPROC',
   'POWER',
   'DEBUGTRACE',
   # needs to be last also contains wrong comdef.h      
   'KERNEL',
]

CBSP_RESTRICTED_API = [
   'DAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DEBUGTOOLS',
   'MPROC',
   'POWER',
   # needs to be last also contains wrong comdef.h      
   'KERNEL',
   'SYSTEMDRIVERS_HAL_AONINT_INC',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_RESTRICTED_API)


#-------------------------------------------------------------------------------
# Source Code
#-------------------------------------------------------------------------------
DAL_INTERRUPT_CONTROLLER_SOURCES = [
   '${BUILDPATH}/src/DALInterruptControllerInfo.c',
   '${BUILDPATH}/src/DALInterruptControllerFwk.c',
   '${BUILDPATH}/src/qurt/DALInterruptController.c',
   '${BUILDPATH}/src/utils/DALInterruptController_utils.c'
]

if 'USES_SENSOR_IMG' in env:
  DAL_INTERRUPT_CONTROLLER_SOURCES.extend(['${BUILDPATH}/config/8996/InterruptConfigData.c'])

AONINT_SOURCES = [
   '${BUILDPATH}/src/qurt/AonInt.c',
]

if 'USES_SENSOR_IMG' in env:
  INTERRUPT_UIMAGE = [
    '${BUILDPATH}/src/qurt/uimage/uInterruptControllerStub.c'
  ]
else:
  INTERRUPT_UIMAGE = [
    '${BUILDPATH}/src/qurt/uimage/uInterruptController.c',
  ]
  INTERRUPT_ISLAND_UIMAGE = [
    '${BUILDPATH}/src/qurt/uimage/uInterruptControllerIsland.c',
    '${BUILDPATH}/src/qurt/uimage/uAonInt.c',
  ]  

if ( 'USES_ISLAND' in env ) and ( 'USES_SENSOR_IMG' not in env ):
  uint_island_lib = env.AddLibrary( INTC_IMAGES_ROOT, 
                      '${BUILDPATH}/InterruptController/uimage/uintkernelIsland',
                      INTERRUPT_ISLAND_UIMAGE )
  env.AddIslandLibrary( INTC_IMAGES_ROOT, uint_island_lib )

env.AddLibrary( INTC_IMAGES, 
                '${BUILDPATH}/InterruptController/uimage/uintkernel',
                INTERRUPT_UIMAGE )

try:
  env.AddCMMScripts ('SLPI', [SRCPATHSCRIPTS], { 'InterruptController.cmm' : ' Interrupt Controller', 'InterruptLog.cmm' : ' Interrupt Log' }, 'DAL')
except:
  pass
#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddLibrary(INTC_IMAGES, '${BUILDPATH}/DALInterruptController', DAL_INTERRUPT_CONTROLLER_SOURCES)
env.AddLibrary(INTC_IMAGES_ROOT, '${BUILDPATH}/AONInt', AONINT_SOURCES)
   
if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG','DEVCFG_CORE_QDSP6_SENSOR_SW']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
      '8996_xml' : ['${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/InterruptController.xml',
                    '${BUILD_ROOT}/core/systemdrivers/InterruptController/config/8996/InterruptConfigData.c'],                     
   })

if 'USES_CORESLEEP_TOOL' in env:
    xml_dir = env['BUILD_ROOT'] + '/core/systemdrivers/InterruptController/config/8996/lpr'
    env.AddCoreSleepLPR(INTC_IMAGES_ROOT, {'sleep_lpr_xml' : [xml_dir]})

if 'USES_RCINIT' in env:
  RCINIT_IMG = INTC_IMAGES_ROOT
  env.AddRCInitFunc(       # Code Fragment in TMC: NO
  RCINIT_IMG,              # define TMC_RCINIT_INIT_GPIOINT_INIT
  {
    'sequence_group'             : 'RCINIT_GROUP_0',      # required
    'init_name'                  : 'AonInt',              # required
    'init_function'              : 'AonInt_Init',         # required
    'dependencies'               : ['dalsys','npa']
  })



