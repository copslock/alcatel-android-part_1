/*
==============================================================================

FILE:         VCSBSP.c

DESCRIPTION:
  This file contains VCS bsp data for DAL based driver.

==============================================================================

                             Edit History

$Header: //components/rel/core.slpi/1.0/systemdrivers/vcs/config/msm8996/VCSBSP.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------- 
01/22/14   lil     Created.

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/


#include "comdef.h"
#include "VCSBSP.h"
#include "pmapp_npa.h"


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Data Declarations
==========================================================================*/

/*
 *  VCS_RailCornerConfigsCX
 *
 *  Set of rail corner configurations.
 */
static VCSRailCornerConfigType VCS_CornerConfigCX[] =
{
  {
    .eCornerMin        = VCS_CORNER_LOW_MINUS,
    .eCornerMax        = VCS_CORNER_TURBO,
    .eCornerInit       = VCS_CORNER_NOMINAL,
    .pVoltageRange     = NULL, /* We do not know about CX's voltage table */
    .nNumVoltageRanges = 0,    /* We do not directly manage CX voltages */
    .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
  }
};


/*
 * VCS_RailConfigs
 *
 * Set of rail configurations.
 */
static VCSRailConfigType VCS_RailConfigs[] =
{
  {
    .eRail             = VCS_RAIL_CX,
    .szName            = VCS_NPA_RESOURCE_VDD_CX,
    .pCornerConfig     = VCS_CornerConfigCX,
    .nNumCornerConfigs = ARR_SIZE(VCS_CornerConfigCX),
    .szNameDependency  = PMIC_NPA_GROUP_ID_RAIL_SSC_CX,
    .bEnableCPR        = FALSE,
    .bEnableDVS        = TRUE
  }
};


/*
 * Set of LDO_Q6 corner voltages.
 */
static VCSCornerVoltageType VCS_LDOCornerVoltageLPASSQ6[] =
{
  {
    .eCorner     = VCS_CORNER_LOW_MINUS,
    .nVoltageUV  = 775000
  },
};


/*
 * LDO_Q6 configuration data.
 *
 * Note: This structure is a starting point for what we think the LDO
 * usage should be. This may change in the future.
 */
static VCSLDOConfigType VCS_LDOConfigLPASSQ6 =
{
  .eLDO               = HAL_LDO_Q6,
  .szName             = VCS_NPA_RESOURCE_LDO_LPASS_Q6,
  .nHeadroom          = 1,
  .pCornerVoltage     = VCS_LDOCornerVoltageLPASSQ6,
  .nNumCornerVoltages = ARR_SIZE(VCS_LDOCornerVoltageLPASSQ6),
  .bEnable            = FALSE
};


/*
 * List of CPUs.
 */
static VCSCPUConfigType VCS_CPUConfigs[] =
{
  {
    .eCPU       = CLOCK_CPU_LPASS_Q6,
    .szName     = "/clk/cpu",
    .eRail      = VCS_RAIL_CX,
    .pLDOConfig = &VCS_LDOConfigLPASSQ6,
  },
};


/*
 * Corner mapping from VCS enum to PMIC enum
 */
uint32 VCS_CornerPMICMap[VCS_CORNER_NUM_OF_CORNERS] =
{
  PMIC_NPA_MODE_ID_CORE_RAIL_OFF,          // VCS_CORNER_OFF
  PMIC_NPA_MODE_ID_CORE_RAIL_RETENTION,    // VCS_CORNER_RETENTION
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW_MINUS,    // VCS_CORNER_LOW_MINUS
  PMIC_NPA_MODE_ID_CORE_RAIL_LOW,          // VCS_CORNER_LOW
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,      // VCS_CORNER_LOW_PLUS
  PMIC_NPA_MODE_ID_CORE_RAIL_NOMINAL,      // VCS_CORNER_NOMINAL
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO,        // VCS_CORNER_NOMINAL_PLUS
  PMIC_NPA_MODE_ID_CORE_RAIL_TURBO         // VCS_CORNER_TURBO
};


/*
 * VCS Log Default Configuration.
 */
const VCSLogType VCS_LogDefaultConfig[] =
{
  {
    /* .nLogSize = */ 4096,
  }
};


/*
 *  VCS_BSPConfig
 *
 *  List and length of Rail and CPU configurations.
 */
const VCSBSPConfigType VCS_BSPConfig =
{
  .pRailConfig     = VCS_RailConfigs,
  .nNumRailConfigs = ARR_SIZE(VCS_RailConfigs),
  .pCPUConfig      = VCS_CPUConfigs,
  .nNumCPUConfigs  = ARR_SIZE(VCS_CPUConfigs),
  .pnCornerPMICMap = VCS_CornerPMICMap
};

