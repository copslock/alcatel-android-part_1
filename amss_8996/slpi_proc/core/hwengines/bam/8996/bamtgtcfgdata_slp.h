#ifndef BAMTGTCFGDATA_SLP_H_
#define BAMTGTCFGDATA_SLP_H_

/**
  @file bamtgtcfgdata_slp.h
  @brief
  This file contains configuration data for the BAM driver for the 
  8996 Sensor system.

*/
/*
===============================================================================

                             Edit History

 $Header: //components/rel/core.slpi/1.0/hwengines/bam/8996/bamtgtcfgdata_slp.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/22/15   ss      Updated sensor blsp bam EE index.
12/05/14   ss      Created for Bam support in SLP image.

===============================================================================
                   Copyright (c) 2014-2015 QUALCOMM Technologies Inc.
                          All Rights Reserved.
                        Qualcomm Confidential and Proprietary.
===============================================================================
*/

/** Configs supported for
   BLSP_SLC 
   BLSP1 
   BLSP2 
   BAM_TGT_CFG_LAST 
 */

#define BAM_CNFG_BITS_VAL 0xFFFFF004

const bam_target_config_type  bam_tgt_config[] = {
    {                    // BLSP1_BAM
      /* .bam_pa     */  0x07544000,
      /* .options    */  (BAM_TGT_CFG_SHARABLE|BAM_TGT_CFG_NO_INIT),
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  3,
      /* .sec_config */  NULL,
      /* .size       */  BAM_MAX_MMAP
    },
    {                    // BLSP2_BAM
      /* .bam_pa     */  0x07584000,
      /* .options    */  (BAM_TGT_CFG_SHARABLE|BAM_TGT_CFG_NO_INIT),
      /* .cfg_bits   */  BAM_CNFG_BITS_VAL,
      /* .ee         */  3,
      /* .sec_config */  NULL,
      /* .size       */  BAM_MAX_MMAP
    },
    {                    // BLSP_SSC_BAM
     /* .bam_pa     */   0x01E84000, 
     /* .options    */   (BAM_TGT_CFG_SHARABLE|BAM_TGT_CFG_NO_INIT),
     /* .cfg_bits   */   BAM_CNFG_BITS_VAL,   
     /* .ee         */   3,
     /* .sec_config */   NULL,
     /* .size       */   BAM_MAX_MMAP
    },
    {                    //dummy config
      /* .bam_pa     */  BAM_TGT_CFG_LAST,
      /* .options    */  0,
      /* .cfg_bits   */  0,
      /* .ee         */  0,
      /* .sec_config */  NULL,
      /* .size       */  0
    },
};

#endif

