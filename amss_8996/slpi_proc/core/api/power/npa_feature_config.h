/*==============================================================================
@file npa_feature_config.h

NPA Features. Customize this file for codebases that need less or specific
options enabled.

  Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
           All Rights Reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary.

$Header: //components/rel/core.slpi/1.0/api/power/npa_feature_config.h#2 $
============================================================================*/

#ifndef NPA_FEATURE_CONFIG_H
#define NPA_FEATURE_CONFIG_H

/* Define to remove events from NPA */
//#define DEFEATURE_NPA_EVENTS

/* Define to remove async requests from NPA */
//#define DEFEATURE_NPA_ASYNC_CLIENTS

/* Define to remove all WL related things from NPA */
#define DEFEATURE_NPA_WORKLOOPS

/* Define to remove transactions from NPA */
//#define DEFEATURE_NPA_TRANSACTIONS

#ifdef DEFEATURE_NPA_WORKLOOPS
#define DEFEATURE_NPA_EVENTS
#define DEFEATURE_NPA_ASYNC_CLIENTS
#endif

#endif // NPA_FEATURE_CONFIG_H





