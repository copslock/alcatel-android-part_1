#ifndef _I2CBSP_H_
#define _I2CBSP_H_
/*=============================================================================

  @file   I2cBsp.h

   This file contains the types for the platform services.
 
Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

  ===========================================================================*/
/* $Header: //components/rel/core.slpi/1.0/buses/i2c/src/drv/I2cBsp.h#2 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cTypes.h"
#include "I2cDriverTypes.h"
/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/
//TODO: remove this global
extern const uint32              i2cDeviceNum;

/*-------------------------------------------------------------------------
 * Externalized Function Definitions
 * ----------------------------------------------------------------------*/

void I2cBsp_Init(void);
void I2cBsp_DeInit(void);


#endif /* _I2CBSP_H_ */

