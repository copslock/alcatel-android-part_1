#include "Uart.h"

static UartHandle serial_port;

void uartlite_test(void)
{
   char msg[] = "TX works!!!";
   Uart_init(&serial_port,UART_SECOND_PORT);

   Uart_transmit(serial_port, msg,11);
   Uart_deinit(serial_port);
}

