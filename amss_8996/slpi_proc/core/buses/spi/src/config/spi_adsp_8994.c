/*=============================================================================

  FILE:     spi_adsp_8994.c

  OVERVIEW: Contains target specific SPI configuration for 8994 ADSP 
 
Copyright (c) 2009-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.slpi/1.0/buses/spi/src/config/spi_adsp_8994.c#2 $
  $DateTime: 2015/04/03 15:05:45 $$Author: pwbldsvc $

  When        Who    What, where, why
  --------    ---    -----------------------------------------------------------
  2014/04/01  vk     Initial version

  ===========================================================================*/
#include "SpiDevicePlatSvc.h"
#include "SpiDriverTypes.h"
#include "DALDeviceId.h"
#include "DDIHWIO.h"
#include "DDITlmm.h"

//Vin - make sure this goes in uImage memory
spiDevice spiDevices[SPIPD_DEVICE_COUNT] ATTRIBUTE_ISLAND_DATA = {
   { NULL, 1, 0 }, //device handle, core number, current status
   { NULL, 9, 0 },
   { NULL, 12, 0 }
};

SpiDevicePlat_DevCfg Spi_DeviceCfg[SPIPD_DEVICE_COUNT] ATTRIBUTE_ISLAND_DATA;

#define SPI_PERIPH_SS_BASE_PHYS_ADDR 0xF9800000

const uint32 SpiQupPhysAddrOffset[] ATTRIBUTE_ISLAND_DATA = {0x123000, 0x124000, 0x125000, 0x126000, 0x127000, 0x128000, 0x163000, 0x124000, 0x125000, 0x126000, 0x127000, 0x128000};

char *SpiClks[] ATTRIBUTE_ISLAND_DATA = { "gcc_blsp1_qup1_spi_apps_clk", "gcc_blsp1_qup1_spi_apps_clk",
   "gcc_blsp1_qup2_spi_apps_clk", "gcc_blsp1_qup3_spi_apps_clk", "gcc_blsp1_qup4_spi_apps_clk",
   "gcc_blsp1_qup5_spi_apps_clk", "gcc_blsp1_qup6_spi_apps_clk", "gcc_blsp2_qup1_spi_apps_clk",
   "gcc_blsp2_qup2_spi_apps_clk", "gcc_blsp2_qup3_spi_apps_clk", "gcc_blsp2_qup4_spi_apps_clk",
   "gcc_blsp2_qup5_spi_apps_clk", "gcc_blsp2_qup6_spi_apps_clk" };

const uint32 SpiTxBamPipeNum[] ATTRIBUTE_ISLAND_CONST = {12, 14, 16, 18, 20, 22, 12, 14, 16, 18, 20, 22};
const uint32 SpiRxBamPipeNum[] ATTRIBUTE_ISLAND_CONST = {13, 15, 17, 19, 21, 23, 13, 15, 17, 19, 21, 23};

const uint32 SpiGpioClk[] ATTRIBUTE_ISLAND_CONST = {0x2006C031, 0x2006C071, 0x2006C0B2, 0x2006C142, 0x2006C183, 0x2006C1C1, 0x2006C2C1, 0x2006C301, 0x2006C342, 0x2006C382, 0x2006C541, 0x2006C581};
const uint32 SpiGpioCS[] ATTRIBUTE_ISLAND_CONST = {0x2006C021, 0x2006C061, 0x2006C0A2, 0x2006C132, 0x2006C172, 0x2006C1B1, 0x2006C2B1, 0x2006C2F1, 0x2006C332, 0x2006C372, 0x2006C531, 0x2006C571};
const uint32 SpiClkGpioMISO[] ATTRIBUTE_ISLAND_CONST = {0x2006C011, 0x2006C051, 0x2006C091, 0x2006C122, 0x2006C162, 0x2006C1A2, 0x2006C2A1, 0x2006C2E1, 0x2006C322, 0x2006C363, 0x2006C522, 0x2006C561};
const uint32 SpiClkGpioMOSI[] ATTRIBUTE_ISLAND_CONST = {0x2006C001, 0x2006C041, 0x2006C081, 0x2006C112, 0x2006C152, 0x2006C193, 0x2006C291, 0x2006C2D1, 0x2006C312, 0x2006C353, 0x2006C512, 0x2006C551};

static DalDeviceHandle  ATTRIBUTE_ISLAND_DATA *pClkHandle;
static DalDeviceHandle  ATTRIBUTE_ISLAND_DATA *pTlmmHandle;


// Call from RCInit
void SpiInit(void)
{
   int32 i;
   SpiDevicePlat_DevCfg *tgtCfg;
   DALResult dalRes;
   uint8 *periph_base = NULL;
   DalDeviceHandle *phDalHWIO = NULL;

   memset(Spi_DeviceCfg, 0, sizeof(Spi_DeviceCfg));

   dalRes = DAL_DeviceAttach(DALDEVICEID_HWIO, &phDalHWIO);
   if ((DAL_SUCCESS != dalRes) || (NULL == phDalHWIO))
   {
      return;
   }
   if (DAL_SUCCESS != DalHWIO_MapRegion(phDalHWIO, "PERIPH_SS", &periph_base))
   {
      return;
   }

   if (NULL == pClkHandle)
   {
      dalRes = DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &pClkHandle);
      if ((DAL_SUCCESS != dalRes) || (NULL == pClkHandle))
      {
         return;
      }
   }
   if (NULL == pTlmmHandle)
   {
      if (DAL_SUCCESS != DAL_DeviceAttachEx(NULL, DALDEVICEID_TLMM, DALTLMM_INTERFACE_VERSION, &pTlmmHandle))
      {
         return;
      }

      if (DAL_SUCCESS != DalDevice_Open(pTlmmHandle, DAL_OPEN_SHARED))
      {
         DAL_DeviceDetach(pTlmmHandle);
         pTlmmHandle = NULL;
         return;
      }
   }

   for (i = 0; i < SPIPD_DEVICE_COUNT; i++)
   {
      tgtCfg = &(Spi_DeviceCfg[i]);
      tgtCfg->uQupCoreNum = spiDevices[i].qup_core_num;
      tgtCfg->bBamSupported = 0;
      tgtCfg->bamXferMinBytes = 64;
      tgtCfg->bInterruptBased = 0;
      tgtCfg->bTcsrInterruptRequired = 0;
      tgtCfg->gpioConfigured = 0;
      tgtCfg->pQupAppClkName = SpiClks[tgtCfg->uQupCoreNum];
      tgtCfg->pQupHClkName = ((tgtCfg->uQupCoreNum <= 6) ? "gcc_blsp1_ahb_clk" : "gcc_blsp2_ahb_clk");
      tgtCfg->qupPhysBlockAddr = SPI_PERIPH_SS_BASE_PHYS_ADDR + SpiQupPhysAddrOffset[tgtCfg->uQupCoreNum];
      tgtCfg->qupVirtBlockAddr = (uint32)periph_base + SpiQupPhysAddrOffset[tgtCfg->uQupCoreNum];
      tgtCfg->tcsrVirtBlockAddr = 0;
      tgtCfg->uTcsrInterruptBitMask = 0;
      tgtCfg->uTcsrInterruptBitShift = 0;
      tgtCfg->spiBamCfg.uBamBaseAddr = ((tgtCfg->uQupCoreNum <= 6) ? (SPI_PERIPH_SS_BASE_PHYS_ADDR + 0x104000) : (SPI_PERIPH_SS_BASE_PHYS_ADDR + 0x144000));
      tgtCfg->spiBamCfg.uBamBlspId = (tgtCfg->uQupCoreNum <= 6) ? 1 : 2;
      tgtCfg->spiBamCfg.uBamInterruptId = 0; //not using BAM interrupt
      tgtCfg->spiBamCfg.uBamRxPipeNum = SpiTxBamPipeNum[tgtCfg->uQupCoreNum];
      tgtCfg->spiBamCfg.uBamTxPipeNum = SpiTxBamPipeNum[tgtCfg->uQupCoreNum];
      tgtCfg->pClkHandle = pClkHandle;
      tgtCfg->pTlmmHandle = pTlmmHandle;
      tgtCfg->spiGpio.gpio_sig_spi_clk = SpiGpioClk[tgtCfg->uQupCoreNum];
      tgtCfg->spiGpio.gpio_sig_spi_cs = SpiGpioCS[tgtCfg->uQupCoreNum];
      tgtCfg->spiGpio.gpio_sig_spi_miso = SpiClkGpioMISO[tgtCfg->uQupCoreNum];
      tgtCfg->spiGpio.gpio_sig_spi_mosi = SpiClkGpioMOSI[tgtCfg->uQupCoreNum];
   }
}

