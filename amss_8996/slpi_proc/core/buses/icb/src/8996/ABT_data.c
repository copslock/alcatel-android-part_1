/*==============================================================================

FILE:      ABT_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.slpi/1.0/buses/icb/src/8996/ABT_data.c#3 $ 
$DateTime: 2015/04/03 15:05:45 $
$Author: pwbldsvc $
$Change: 7816328 $ 

When        Who    What, where, why
----------  ---    ----------------------------------------------------------- 
2014/10/03  tb     Ported for SSC
2013/11/14  tb     Added support for multiple enable/status registers
2013/04/16  pm     Added slot for interrupt priority  
2012/10/04  av     Support for disabling ABT 
2012/05/31  av     Created
 
Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
        All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
==============================================================================*/
#include "ABTimeout.h"
#include "ABTimeout_HWIO.h"

/*============================================================================
                      TARGET AND PLATFORM SPECIFIC DATA
============================================================================*/

/* Base address for devices */
#define ABT_SSC_CFG_BASE_ADDR  SSC_CFG_AHBE_BUS_TIMEOUT_REG_BASE 

/* Bit Mask for ABT Slaves */
#define ABT_SSC_CFG_BMSK   HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_0_SHFT

/* ABT Slave CLK Name */
#define ABT_SSC_CFG_CLK   "scc_ahb_timeout_clk"

/* Timeout Interrupt Register Address */
#define ABT_TIMEOUT_SLAVE_GLB_EN       HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR
#define ABT_TIMEOUT_SLAVE_LCL_GLB_EN       HWIO_SSC_SSC_CFG_BUS_CONFIG_ADDR
#define ABT_TIMEOUT_SLAVE_LCL_GLB_BMSK       HWIO_SSC_SSC_CFG_BUS_CONFIG_AHBE_GLOBAL_EN_BMSK

/* SSC Summary Interrupt Vectors */
#define ABT_SSC_INTR_VECTOR      ABT_NO_INTERRUPT

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/* ABT Configuration Data*/
ABT_slave_info_type ABT_cfgdata[] = 
{ 
//ABT_SLAVE_INFO(  name, sl_en, int_en, to_val, local, shared)
  ABT_SLAVE_INFO(SSC_CFG,  TRUE,   TRUE,   0xFF, TRUE, TRUE),
};

/* ABT interrupt enable array */
void *intrEnable[] =
{
};

/* ABT interrupt status array */
void *intrStatus[] =
{
};

/* ABT Platform Data type */
ABT_platform_info_type ABT_platform_info =
{
    "SSC",                                    /* Image name */
    intrEnable,                               /* INTR Enable array */
    intrStatus,                               /* INTR Status Register array */
    (void*)ABT_TIMEOUT_SLAVE_GLB_EN,          /* ABT Slave global en address */
    ABT_SSC_INTR_VECTOR,                      /* SSC Summary Interrupt Vector */
    NULL,                                     /* No interrupt priority needed on SSC */
    sizeof(intrEnable)/sizeof(intrEnable[0]), /* Number of interrupt enable/status registers */
    (void*)ABT_TIMEOUT_SLAVE_LCL_GLB_EN,      /* ABT Slave local global en address */
    ABT_TIMEOUT_SLAVE_LCL_GLB_BMSK,           /* ABT Slave local global bit mask */
};


/* ABT Configuration Property Data*/
ABT_propdata_type ABT_propdata =  
{
    /* Length of the config data array */
    sizeof(ABT_cfgdata)/sizeof(ABT_slave_info_type), 
    /* Pointer to config data array */ 
    ABT_cfgdata,
    /* Pointer to platform info data */ 
    &ABT_platform_info                                    
};

