#ifndef __ABTIMEOUT_HWIO_H__
#define __ABTIMEOUT_HWIO_H__
/*
===========================================================================
*/
/**
  @file ABTimeout_HWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    SSC_CFG_AHBE_BUS_TIMEOUT
    TCSR_TCSR_REGS
    SSC_MCC_REGS

  'Include' filters applied: BASE[SSC_DATA_AHBE_BUS_TIMEOUT] BASE[SSC_CFG_AHBE_BUS_TIMEOUT] 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.slpi/1.0/buses/icb/src/8996/ABTimeout_HWIO.h#1 $
  $DateTime: 2014/10/15 17:16:49 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: SSC_CFG_AHBE_BUS_TIMEOUT
 *--------------------------------------------------------------------------*/

#define SSC_CFG_AHBE_BUS_TIMEOUT_REG_BASE                          (SSC_BASE      + 0x006c5000)

/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_REGS
 *--------------------------------------------------------------------------*/

#define TCSR_TCSR_REGS_REG_BASE                                                                                                                       (CORE_TOP_CSR_BASE      + 0x000a0000)

#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00000800)

#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00008020)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_0_BMSK                                                                                       0x200
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_0_SHFT                                                                                         0x9

/*----------------------------------------------------------------------------
 * MODULE: SSC_MCC_REGS
 *--------------------------------------------------------------------------*/

#define SSC_MCC_REGS_REG_BASE                                                      (SSC_BASE      + 0x00600000)

#define HWIO_SSC_SSC_CFG_BUS_CONFIG_ADDR                                           (SSC_MCC_REGS_REG_BASE      + 0x00004000)
#define HWIO_SSC_SSC_CFG_BUS_CONFIG_AHBE_GLOBAL_EN_BMSK                                  0x40
#define HWIO_SSC_SSC_CFG_BUS_CONFIG_AHBE_GLOBAL_EN_SHFT                                   0x6

#define HWIO_SSC_SSC_CFG_BUS_STATUS_ADDR                                           (SSC_MCC_REGS_REG_BASE      + 0x00004004)
#define HWIO_SSC_SSC_CFG_BUS_STATUS_AHBE_STATUS_BMSK                                      0x7
#define HWIO_SSC_SSC_CFG_BUS_STATUS_AHBE_STATUS_SHFT                                      0x0

#endif /* __ABTIMEOUT_HWIO_H__ */
