#! /usr/bin/python

''' New version of write_eip_info, writes header, variable length records, and string table '''
'''
    Top level header (fixed length, one only):
      uint32_t magic_number (0x32504945)
      uint16_t version_number
      uint16_t number_of_apps
    Second level header (fixed length, one per app):
      uint16_t first_memsection_number
      uint16_t number_of_memsections
      uint16_t entry_memsection_number
      uint16_t string_table_offset_of_app_name
    Third level header (fixed length, one per memsection):
      uint32_t virtual_address_start
      uint32_t physical_address_start
      uint32_t length_in_bytes
      uint32_t permissions
    String table:
      Zero terminated strings for app names
      (App names which are restartable are prepended with '>' greater than sign)
      (App names which are not-restartable are not prepended)
'''
def _build_eip_info(f, infiles, island_moves, opt_mappings, seg_optimize):
    import struct
    from qurt import aligndown, alignup
    for i in infiles:
        segs = []
        for q in i.phdr:
            p = q.outcopy
            ''' Add the memory described in this program header '''
            virt_start = aligndown(p.p_vaddr, 0x1000)
            virt_end = alignup(p.p_vaddr + p.p_memsz, 0x1000)
            virt_offset = p.p_paddr - p.p_vaddr
            virt_exec = ((p.p_flags & 1) != 0)
            virt_write = ((p.p_flags & 2) != 0)
            if virt_start != virt_end and p.p_type == 1:
                segs += [(virt_start, virt_end, virt_offset, virt_exec, virt_write, False)]
        done, segs = (False, sorted(segs))
        while not done:
            done, segs = seg_optimize(segs)
        i.segs = segs
    if opt_mappings:
        # New style of doing things; use the optimized mappings we already computed
        for asid in range(1,len(infiles)):
            segs = []
            for M in opt_mappings:
                if M.ASID == asid:
                    segs += [(M.vpage << 12, (M.vpage+M.size) << 12, (M.ppage-M.vpage) << 12, (M.X != 0), (M.W != 0), M.island)]
            infiles[asid].segs = segs
    top_hdr = (0x32504945,1,len(infiles))
    eip_data = struct.pack('<LHH', *top_hdr)
    strtab = ''
    for m in island_moves:
        strtab += struct.pack('<LLL', *m)
    strtab += struct.pack('<L', 0)
    secno = 0
    for i in infiles:
        segs = i.segs
        second_hdr = (secno, len(segs), secno, len(strtab))
        eip_data += struct.pack('<HHHH', *second_hdr)
        secno += len(segs)
        if i.restart:
            strtab += '>'
        strtab += i.appname + '\0'
    for i in infiles:
        segs = i.segs
        segs = sorted(segs, key=lambda s: (s[5], s[0]+s[2]))
        for s in segs:
            perms = 1           # Read permission
            ''' Add in exec and write permissions '''
            if s[3]:
                perms += 4        # Execute permission
            if s[4]:
                perms += 2        # Write permission
            if s[5]:
                perms += 0x100    # Island status
            third_hdr = (s[0], s[0]+s[2], s[1]-s[0], perms)
            eip_data += struct.pack('<LLLL', *third_hdr)
    eip_data += strtab
    return eip_data

class ElfInfoPatch:
    def __init__(self):
        #
        # List here the symbols we need to extract from the build
        #
        # To declare a symbol as optional (weak), prepend the name
        #  of the symbol with [w] with no intervening white space.
        #  For example:  [w]Optional_Symbol
        #
        # Weak symbols that don't exist in the build are reported
        #  as having an address of zero.
        #
        self.symbols_spec = '''
            QURTK_phys_island
            QURTK_tlb_dump
            TLB_LAST_REPLACEABLE_ENTRY
            qurtos_mmap_table
            QURTK_tlblock_entries
            QURTK_tlb_dump_relocs
            qurtos_boot_mappings
            QURTK_quick_load_MMU
            qurtos_xml_passthrough
            qurtos_virt_reserved
            [w]qurtos_ramfs_directory
            [w]qurtos_extra_instructions
            [w]qurtos_slots
            [w]qurtos_tlb_reclaim
            [w]pool_configs
        '''
        self.flags_spec = '''
            CONFIG_ADVANCED_MEMORY
        '''
        self.symbols = self.symbols_spec.split()
        self.flags = self.flags_spec.split()
        self.symbols.sort()
        self.flags.sort()
        self.total_words = len(self.symbols)+1
    def Signature(self):
        #
        # For now, have the signature be the actual strings themselves
        #
        r = '\n'.join(self.flags+['']+self.symbols)
        while len(r)%4:
            r+='\n'
        return r
    def FindSig(self, cfg, addr, size):
        #
        #  Class method which is supposed to find and return the
        #   signature string found in the .start section.
        #
        #  The signature string will always:
        #   1.  Start with the string "CONFIG_"
        #   2.  Consist of only printable characters (Python includes newlines as printable)
        #   3.  Have a length which is a multiple of 4
        #   4.  Occur exactly twice within the .start section
        #   5.  Have the first instance in .start be preceded by four zero bytes which are 8-byte-aligned
        #   6.  Have both instances in .start be 4-byte aligned
        #   7.  Except for 0-3 trailing "padding" newlines, have exactly one
        #        instance of two newlines in a row.
        #
        #  Within those constraints, return the longest such signature
        #   which works.  If two tie for the longest such signature
        #   which works, use the one found earliest in the section.
        #
        #  Note that ties should never happen, but we're prepared to behave deterministically.
        #
        #  The reason we go to this trouble is to allow for parsing an ELF which was compiled
        #   against a different version of this file -- one which might export more, fewer,
        #   or different symbols.  This won't happen often during normal builds, but can
        #   be very useful for debugging.
        #

        import string
        zeroes = '\0\0\0\0'
        sig_start = 'CONFIG_'
        sig_start_with_zeroes = zeroes+sig_start
        start_contents = cfg[(addr,size)]
        try_these = start_contents.split(sig_start_with_zeroes)
        i = 0
        best_return = (0,)
        while i+1 < len(try_these):
            i += 1
            # Reconstruct the portion up to the signature we're going to try
            #  and the portion after the signature.
            before = sig_start_with_zeroes.join(try_these[:i])+zeroes
            after = sig_start + sig_start_with_zeroes.join(try_these[i:])
            if len(before) % 8 != 4:
                continue                  # Violates #6 above

            try_siglen = 8                # Base size from which to start trying
            while True:
                try_siglen += 4           # Enforce #3 above
                try_sig = after[:try_siglen]
                stripped = try_sig.rstrip('\n')
                fsplit = stripped.split('\n\n')
                if len(try_sig) < try_siglen:
                    break                 # Not enough chars, and longer won't work
                if [c for c in try_sig if not c in string.printable]:
                    break                 # Violates #2 above, and longer won't work
                if len(stripped.split('\n\n\n')) != 1:
                    break                 # Violates #7 above, and longer won't work
                if len(fsplit) != 2:
                    continue              # Violates #7 above, but longer might work
                start_splits = start_contents.split(try_sig)
                if len(start_splits) != 3:
                    continue              # Violates #4 above, but longer might work
                if not start_splits[0].endswith(zeroes):
                    continue              # Violates #5 above, but longer might work
                if len(start_splits[1]) % 4 != 0:
                    continue              # Violates #6 above, but longer might work?

                # One last thing -- the space between the two signatures must have
                #  at least 4 bytes for every symbol after the double-newline, plus
                #  an extra 4 bytes for the flags
                # Check that here.

                needed_length = (len(fsplit[1].split('\n'))+1)*4
                have_length = len(start_splits[1])
                if have_length < needed_length:
                    continue              # Not enough data, but longer might work?

                eip_offset = len(before)-4
                eip_size = have_length+try_siglen*2+4

                if eip_size % 8 != 0:
                    continue              # Not 8-byte-aligned, but longer might work?

                if try_siglen > best_return[0]:
                    best_return = (len(try_sig), try_sig, eip_offset, eip_size, start_splits[1][:needed_length])

        if best_return == (0,):
            raise Exception('Could not find a valid ELF Info Patch area')

        return best_return

    def Parse(self, cfg, addr, size):
        import struct
        (_, sig, offset, total_size, data) = self.FindSig(cfg, addr, size)
        temp = sig.split('\n\n')
        flagnames = temp[0].split('\n')
        symbolnames = temp[1].split('\n')
        temp = struct.unpack('<'+'L'*(len(data)/4), data)
        bitpos = 1
        for x in flagnames:
            if temp[0] & bitpos:
                setattr(cfg, x, True)
            else:
                setattr(cfg, x, False)
            bitpos *= 2
        ix = 1
        for x in symbolnames:
            if x.startswith('[w]'):
                setattr(cfg, x[3:], temp[ix])
            else:
                setattr(cfg, x, temp[ix])
            ix += 1
        for x in self.flags:
            if not hasattr(cfg, x):
                setattr(cfg, x, False)
        for x in self.symbols:
            if x.startswith('[w]'):
                x = x[3:]
            if not hasattr(cfg, x):
                setattr(cfg, x, 0)
        #
        # Now fill in the legacy variables
        #
        cfg.eip_addr_phys_island = cfg.QURTK_phys_island
        cfg.eip_addr_boot_maps = cfg.qurtos_boot_mappings
        cfg.eip_addr_tlb_dump = cfg.QURTK_tlb_dump
        cfg.eip_size_tlb_dump = 1+cfg.TLB_LAST_REPLACEABLE_ENTRY
        cfg.eip_addr_tlblock = cfg.QURTK_tlblock_entries
        cfg.eip_addr_mmap_table = cfg.qurtos_mmap_table
        cfg.eip_addr_tlb_relocs = cfg.QURTK_tlb_dump_relocs
        cfg.eip_addr_quick_mmu = cfg.QURTK_quick_load_MMU
        cfg.eip_size = total_size
        cfg.eip_location = addr + offset

        addr = cfg.qurtos_virt_reserved
        vr = []
        while True:
            z = cfg[(addr,'<l')][0]
            if z < 0:
                break
            vr.append(z)
            addr += 4
        cfg.vrlist = zip(vr[::2],vr[1::2])

        # Go read XML passthrough

        if cfg.qurtos_xml_passthrough:
            sz = struct.unpack('<L', cfg[(cfg.qurtos_xml_passthrough, 4)])[0]
            xml = cfg[(cfg.qurtos_xml_passthrough+4,sz)]
        else:
            xml = '<image></image>'
        from xml.dom.minidom import parseString
        cfg.xml = parseString(xml.decode('utf-8'))
        cfg.eip = self
        self.cfg = cfg
        return (cfg, cfg.xml.documentElement.childNodes)

    def Build_Asm(self, outfilename, req_eipsize, flags):
        bitpos = 1
        flags_numeric = 0
        for x in self.flags:
            if ('-D'+x) in flags:
                flags_numeric += bitpos
            bitpos *= 2
        sig = self.Signature()
        #
        # Calculate total space used for our contents (minus padding)
        #  4 bytes for zero word at the beginning
        #  4 bytes for the flags word
        #  4 bytes for each symbol we need
        #  2 signatures
        #
        used_space = 4 + 4 + len(self.symbols)*4 + len(sig)*2
        eipsize = req_eipsize
        if eipsize < used_space:
            eipsize = used_space
        while eipsize % 8:
            eipsize += 1
        if eipsize != req_eipsize:
            print 'Adjusting EIP size from requested %s to actual %s' % (req_eipsize, eipsize)
        unused_space = eipsize - used_space
        fo = open(outfilename, 'w')
        fo.write('/* This file is automatically generated */\n')
        fo.write('#ifdef GENERATE_ELF_INFO_PATCH\n')
        fo.write('  .align 8\n')
        fo.write('ELF_INFO_PATCH:\n')
        fo.write('  .word 0\n')
        for x in sig:
            fo.write('  .byte %u\n' % ord(x))
        fo.write('  .word %u\n' % flags_numeric)
        for x in self.symbols:
            if x.startswith('[w]'):
                fo.write('  .weak %s\n' % x[3:])
                fo.write('  .word %s\n' % x[3:])
            else:
                fo.write('  .word %s\n' % x)
        fo.write('  .space %u,0\n' % unused_space)
        for x in sig:
            fo.write('  .byte %u\n' % ord(x))
        fo.write('#endif /* GENERATE_ELF_INFO_PATCH */\n')
        fo.close()
        return 0

    def writeback(self, *args):
        data = _build_eip_info(*args)
        if len(data) > self.cfg.eip_size:
            raise Exception('ELF Info Patch area overflow.  Cannot complete image build.')
        if len(data) < self.cfg.eip_size:
            data += '\177' * (self.cfg.eip_size - len(data))
        self.cfg[(self.cfg.eip_location,len(data))] = data

if __name__ == '__main__':
    from sys import argv
    ret = 1
    if 'build_eip' in argv[1:2]:
        ret = ElfInfoPatch().Build_Asm(argv[2], int(argv[3]), argv[4:])
    raise SystemExit(ret)
