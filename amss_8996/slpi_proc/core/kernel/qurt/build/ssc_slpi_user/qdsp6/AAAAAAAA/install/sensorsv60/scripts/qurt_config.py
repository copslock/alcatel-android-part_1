#!/usr/bin/env python

class qurt_config:
    def genheader_subcommand(self, arglist):
        from lib.genheader import genheader_cmd
        return genheader_cmd(arglist)
    def update_subcommand(self, arglist):
        from lib.merge import merge_cmd
        return merge_cmd(arglist)
    def usage(self):
        cmds = sorted([z.rsplit('_',1)[0] for z in dir(self) if z.endswith('_subcommand')])
        str = 'First argument must be one of:\n  ' + ', '.join(cmds)
        raise Exception(str)
    def run_command(self, argv):
        from traceback import format_exc as tbstr
        progname = argv[0]
        try:
            print ' '.join(argv)
            raw_args = argv[1:]
            args = [s for s in raw_args if not s == '--traceback']
            if args == raw_args:
                tbstr = None
            try:
                subfunc = getattr(self, '%s_subcommand' % args[0])
            except StandardError:
                self.usage()
            return subfunc(args[1:])
        except (SystemExit, KeyboardInterrupt):
            raise
        except Exception, err:
            if tbstr:
                print tbstr()
            print '%s: Error:\n*** %s' % (progname, err)
        except:
            raise
        return 1
    def main(self):
        import sys
        sys.exit(self.run_command(sys.argv))

qurt_config().main()    # Never returns

# Signatures of the files that this depends on
# 13c566d93b3bdf9a9a45f778e17c18c1 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\cust_config_template.c
# 73a289a6a2027ed72dc9646d357f9333 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\default_build_config.def
# 1633b5b0296de87a86466ec793dd9a6f Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\static_build_config.def
# fb029620d16a7ed5f97289883f996027 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\qurt_tlb_unlock.xml
# 206b9e12f5643aab9b5c41da144c95f3 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\__init__.py
# 992309777e70b115ee88d151ceea0f4d Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\build_qurt_config.py
# 9dbe95fb17059e23e02557c064bf4bee Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\build_xml.py
# f9aeef6595136b822a22295d22a024f8 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\elf_info_patch.py
# 92948080d10d75e5f94f4777d21bb646 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\ezxml.py
# 476321ba022da3266d0982e4e716cad1 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\genheader.py
# c083132b08dbc9fe4f0e2ad37e4cab81 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\interrupt_xml.py
# 5931ff5d95e2ffa6ccc8d55f945fd82a Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\kernel_xml.py
# 7409922476b0ca823d9047c5e7c6651c Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\machine_xml.py
# 0dcdb7dcc6e15226b8ea7e4febe611ae Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\memsection_xml.py
# 8d2f2a0df1c87bbd05ff68f33b22c1a1 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\merge.py
# 4ffa9ed630bd335beb3517b8494d7c1b Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\parse_build_params.py
# 77c942c618e3edec510cc51245a617d3 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\parse_spec.py
# 31733f66f57d2783f3b7e99338fdb9ae Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\physpool_xml.py
# 94ea469cffbb6a63290a6412f2ca6361 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\program_xml.py
# 525c26ef529ce3fa69bba266b5182ec2 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\lib\qurt.py
# 19ade0db6e2584c1aa3e2c3fa51b0250 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\build_params.txt
# fce4c4833376a752a5911fddefbc0af4 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\Input\cust_config.c
# a27c7b4b1a2c26627a3fbfa6434e7317 Z:\b\slpi_proc\core\kernel\qurt\build\ssc_slpi_user\qdsp6\AAAAAAAA\install\sensorsv60\scripts\qurt-image-build.py
