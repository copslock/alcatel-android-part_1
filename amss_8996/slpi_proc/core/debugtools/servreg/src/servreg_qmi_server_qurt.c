/*
#============================================================================
#  Name:
#    servreg_qmi_notifier_server.c
#
#  Description:
#    Service Registry notifier file for root image. This module serves as the end-point
#    of communication via QMI.
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "qurt.h"
#include "msg.h"
#include "qmi_csi.h"
#include "qmi_csi_target_ext.h"
#include "rcinit.h"

#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_notifier.h"
#include "servreg_qmi_notifier_server.h"
#include "servreg_qmi_notifier_client.h"

#define SERVREG_QMI_NOTIF_SERVER_WAIT_MASK                0x20

static struct
{
   qurt_thread_t tid;

} servreg_qmi_notif_req_server_internal;

qurt_anysignal_t servreg_qmi_signal_qurt;

extern qmi_csi_service_handle servreg_qmi_server_init(qmi_csi_os_params *os_params);

/** =====================================================================
 * Thread:
 *     servreg_qmi_server_req_task
 *
 * Description:
 *     Service Registry QMI notifier server task
 *
 * Parameters:
 *    param: Task init parameter
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_qmi_server_req_task(void *argv __attribute__((unused)))
{
   uint32_t q_mask;
   qmi_csi_os_params os_params;
   qmi_csi_service_handle servreg_qmi_server_handle;

   qurt_anysignal_init(&servreg_qmi_signal_qurt);
   os_params.signal = &servreg_qmi_signal_qurt;
   os_params.sig = SERVREG_QMI_NOTIF_SERVER_WAIT_MASK;

   servreg_qmi_server_handle = servreg_qmi_server_init(&os_params);
   servreg_qmi_notif_client_init();

   /* Block for start signal */
   rcinit_handshake_startup();    

   /* Task forever loop */
   for (;;)
   {
      q_mask = qurt_anysignal_wait(&servreg_qmi_signal_qurt, SERVREG_QMI_NOTIF_SERVER_WAIT_MASK);
      if (q_mask & SERVREG_QMI_NOTIF_SERVER_WAIT_MASK)
      {
         qurt_anysignal_clear(&servreg_qmi_signal_qurt, SERVREG_QMI_NOTIF_SERVER_WAIT_MASK);
         qmi_csi_handle_event(servreg_qmi_server_handle, &os_params);
      }
   }
}

/** =====================================================================
 * Function:
 *     servreg_qmi_notif_task_init
 *
 * Description:
 *     Initialization function for OS specific task to handle QMI request messages
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qmi_notif_task_init(void)
{
   qurt_thread_attr_t attr;
   static unsigned long stack[SERVREG_QMI_NOTIF_SERVER_TASK_STACK/sizeof(unsigned long)];

   qurt_thread_attr_init(&attr);
   qurt_thread_attr_set_name(&attr, SERVREG_QMI_NOTIF_REQ_SERVER_TASK_NAME);
   qurt_thread_attr_set_stack_addr(&attr, stack);
   qurt_thread_attr_set_stack_size(&attr, sizeof(stack));
   qurt_thread_attr_set_priority(&attr, qurt_thread_get_priority(qurt_thread_get_id()) - 1);
   qurt_thread_attr_set_affinity(&attr, QURT_THREAD_ATTR_AFFINITY_DEFAULT);

   qurt_thread_create(&servreg_qmi_notif_req_server_internal.tid, &attr, servreg_qmi_server_req_task, NULL);

   return;
}
