#ifndef SERVREG_QDI_H
#define SERVREG_QDI_H
/*
#============================================================================
#  Name:
#    servreg_qdi.h 
#
#  Description:
#    Implements QDI layer for Service registry that goes to the root image
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdlib.h"
#include "qurt.h"
#include "qurt_qdi_driver.h"   /* Including header only in this file */

#if defined(__cplusplus)
extern "C"
{
#endif

#define SERVREG_QDI_DEVICE_PATH_LEN                    32                             /* INTERNAL, Maximum Device Path Length */
#define SERVREG_QDI_DEVICE_NAME_LEN                    QURT_MAX_NAME_LEN              /* INTERNAL, Maximum Device Basename Length */
#define SERVREG_QDI_DEVICE                             "/dev/servnotif"               /* INTERNAL, Device Prefix */
#define SERVREG_QDI_MAX_PDS                            2
#define SERVREG_QDI_MASK_VALUE                         0x1

#define SERVREG_QDI_WORKER_WAIT                        (0+QDI_PRIVATE)
#define SERVREG_QDI_REG_REMOTE_LISTENER                (1+QDI_PRIVATE)
#define SERVREG_QDI_DEREG_REMOTE_LISTENER              (2+QDI_PRIVATE)
#define SERVREG_QDI_FREE_REMOTE_HANDLE                 (3+QDI_PRIVATE)
#define SERVREG_QDI_SET_REMOTE_ACK                     (4+QDI_PRIVATE)
#define SERVREG_QDI_GET_REMOTE_HANDLE                  (5+QDI_PRIVATE)
#define SERVREG_QDI_CREATE_QMI_ENTRY                   (6+QDI_PRIVATE)

/* Localized Type Declarations */
struct servnotif_qdi_device_s
{
   qurt_qdi_obj_t qdiobj;                                                            /* Required to be first */
   uint32_t pid;
};
typedef struct servnotif_qdi_device_s servnotif_qdi_device_t, * servnotif_qdi_device_p;

/* Localized Type Declarations */
struct servnotif_qdi_device_data_s
{
   uint32_t pid;
   /* sr_mon_handle that represents the PD itself i.e soc/domain/subdomain */
   SERVREG_MON_HANDLE sr_mon_handle;
   qurt_anysignal_t sig_t;
   uint32_t sig_m;                                                               /* Client supplied */
   /* Head node of the Service Registry Notifier node linked list for this user pd device */
   SERVREG_QDI_NOTIF_HANDLE servreg_qdi_notif_node_list_head;
   
};
typedef struct servnotif_qdi_device_data_s servnotif_qdi_device_data_t, * servnotif_qdi_device_data_p;

/* Localized Type Declarations */
struct servnotif_qdi_pid_state_s
{
   SERVREG_BOOL pid_connected;
   uint32_t pid;
};
typedef struct servnotif_qdi_pid_state_s servnotif_qdi_pid_state_t, * servnotif_qdi_pid_state_p;

/** =====================================================================
 * Function:
 *     servreg_qdi_invoke
 *
 * Description: 
 *     This gives the canonical form for the arguments to a QDI
 *     driver invocation function.
 * 
 * Parameters:
 *     client_handle  : QDI handle which represents the client
 *                      which made this QDI request.  
 *
 *     qurt_qdi_obj_t : Points at the qdi_object_t structure
 *                      on which this QDI request is being made.
 *
 *     int method     : The integer QDI method which represents
 *                      the request type.
 *
 *     qurt_qdi_arg_t a1 to a3 :  The first three general purpose arguments
 *                                to the invocation function are passed in
 *                                these slots.
 *
 *     qurt_qdi_arg_t a4 to a9 :   Arguments beyond the first three are
 *                                 passed on the stack.
 * Returns:
 *     -1 for failure and 0 for success
 * =====================================================================  */
int servreg_qdi_invoke(int client_handle,
                       qurt_qdi_obj_t* obj,
                       int servnotif_qdi_method,
                       qurt_qdi_arg_t a1,
                       qurt_qdi_arg_t a2,
                       qurt_qdi_arg_t a3,
                       qurt_qdi_arg_t a4,
                       qurt_qdi_arg_t a5,
                       qurt_qdi_arg_t a6,
                       qurt_qdi_arg_t a7,
                       qurt_qdi_arg_t a8,
                       qurt_qdi_arg_t a9
                      );

/** =====================================================================
 * Function:
 *     servreg_qdi_init
 *
 * Description:
 *     Initialization function. Registers a QDI device with the generic QDI object
 *
 * Parameters:
 *     None
 * Returns:
 *     None
 * =====================================================================  */
void servreg_qdi_init(void);

#if defined(__cplusplus)
}
#endif

#endif