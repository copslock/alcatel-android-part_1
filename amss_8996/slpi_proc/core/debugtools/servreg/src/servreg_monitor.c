/*
#============================================================================
#  Name:
#    servreg_monitor.c 
#
#  Description:
#    Service Registry Monitor(mon) feature. 
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdarg.h"
#include "stdlib.h"
#include <stringl/stringl.h>
#include "comdef.h"             /* Definitions for byte, word, etc.     */
#include "err.h"
#include "queue.h"
#include "timer.h"
#include "rcesn.h"
#include "rcecb.h"

#include "servreg_internal.h"
#include "servreg_locator.h"
#include "servreg_localdb.h"
#include "servreg_monitor.h"
#include "servreg_notifier.h"

/* Service Registry Monitor node list structure */
struct servreg_mon_node_s
{
   /* Service Registry monitor signature that is unique to this handle = 0xccccdddd */
   uint32_t mon_signature;

   /* Handle for the rcesn block that sends out notifications when there is a service state change */
   RCESN_HANDLE rcesn_handle;

   /* Service name */
   SERVREG_NAME service_name;

   /* Represents the number of clients who have alloc the monitor handle */
   uint32_t handle_count;

   /* Represents the number of listeners who have registered to get the service state change notifications */
   uint32_t listener_ref_count;

   /* Set this prameter to TRUE if you want the state change notification of this service to by synchronous */
   SERVREG_BOOL servreg_sync;

   /* Trasaciton_id refers to the number of times the state has changed. This number is required to set the correct ack */
   uint32_t transaction_id;

   /* state_q represents the q pointer with pending state of the services that has to be set */
   /* The next state in the q is set only after all the ACK's from the previous state have arrived */
   q_type *state_q;

   /* state_sync_q represents the q pointer with registrants who have registered to get sync complete notification */
   /* Entry from this queue is removed and put in the state_q only when the service state change occurs */
   q_type *state_sync_q;

   /* Pointer to the next monitor node */
   struct servreg_mon_node_s* next;
};
typedef struct servreg_mon_node_s servreg_mon_node_t, * servreg_mon_node_p;

/* Service Registry Service state entry structure */
struct servreg_mon_queue_s
{
   /* Link for the next queue */
   q_link_type link;

   /* Represents the number of pending ACK's that we are expecting from the listeners */
   uint32_t ack_pending_count;

   /* Service state for this queue entry */
   SERVREG_SERVICE_STATE curr_state;

   /* Rcecb handle that reperesents all the registrants who have registered for sync complete notification */
   RCECB_HANDLE rcecb_handle;

   /* Timeout value monitors the time taken for the ACK's to be received */
   uint32_t timeout;
};
typedef struct servreg_mon_queue_s servreg_mon_queue_t, *servreg_mon_queue_p;

#define SERVREG_MON_ACK_TIMEOUT_COUNT         5
#define SERVREG_MON_ACK_TIME_CHK_SEC          1

/* Type casts as accessor functions */
#define sr_mon_node2sr_mon_handle(x)        ((SERVREG_MON_HANDLE)x)
#define sr_mon_handle2sr_mon_node(x)        ((servreg_mon_node_p)x)

/* Pool Allocations MON node */
struct servreg_mon_node_pool_s
{
   struct servreg_mon_node_s servreg_mon_node_pool[SERVREG_MON_NODE_POOL_SIZE];
   struct servreg_mon_node_pool_s * next;
};
typedef struct servreg_mon_node_pool_s servreg_mon_node_pool_t, * servreg_mon_node_pool_p;

/* Internal structure */
struct servreg_mon_node_internal_s
{
   servreg_mon_node_pool_p servreg_mon_node_pool_head_p;
   servreg_mon_node_p servreg_mon_node_pool_free_p;
   servreg_mutex_t mutex;
   servreg_mutex_t mutex_create;
   SERVREG_BOOL dynamic_use;
   unsigned long init_flag;
};

struct servreg_mon_node_internal_s servreg_mon_node_internal;
servreg_mon_node_pool_t servreg_mon_node_pool_static;
servreg_mon_node_p servreg_mon_node_hashtab[SERVREG_MON_NODE_HASHTABLE_BUCKETS];

/* Ack timer specific */
static timer_type servreg_mon_ack_timer = { 0 };
static timer_group_type servreg_group = { 0 };

/* Static Functions defined in this file */
static servreg_mon_node_p servreg_mon_node_pool_init(void);
static servreg_mon_node_p servreg_mon_node_pool_alloc(void);
static servreg_mon_node_p servreg_mon_node_pool_free(servreg_mon_node_p servreg_mon_node_p);
/* Hash Functions */
static servreg_mon_node_p servreg_mon_node_hashtab_get(servreg_hash_t hash);
static void servreg_mon_node_hashtab_put(servreg_mon_node_p sr_mon_node, servreg_hash_t hash);
static void servreg_mon_node_hashtab_delete(servreg_mon_node_p sr_mon_node_del, servreg_hash_t hash);
static servreg_hash_t servreg_mon_node_nmehash(SERVREG_NAME const name);
static void servreg_mon_node_internal_init(void);

//static SERVREG_MON_HANDLE servreg_get_sr_mon_handle(SERVREG_NAME const name);
static SERVREG_MON_HANDLE servreg_create_mon_node(SERVREG_NAME sr_name, RCESN_HANDLE rc_handle);
static void servreg_service_queue(SERVREG_MON_HANDLE sr_mon_handle);
static int servreg_compare_state(void* item_ptr, void* compare_val);
static void servreg_mon_ack_timer_cb(timer_cb_data_type);

/** =====================================================================
 * Function:
 *     servreg_mon_node_pool_init
 *
 * Description:
 *     Initializes the memory pool for monitor node structure
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_mon_node_p : Returns the first free monitor node space from the pool
 * =====================================================================  */
static servreg_mon_node_p servreg_mon_node_pool_init(void)
{
   servreg_mon_node_pool_p next_pool = SERVREG_NULL;

   if (SERVREG_NULL == servreg_mon_node_internal.servreg_mon_node_pool_head_p)
   {
      next_pool = &servreg_mon_node_pool_static;
   }
   else if (SERVREG_TRUE == servreg_mon_node_internal.dynamic_use)
   {
      next_pool = (servreg_mon_node_pool_p)servreg_malloc(sizeof(servreg_mon_node_pool_t));
   }

   if (SERVREG_NULL != next_pool)
   {
      int i;

      for (i = 0; i < SERVREG_MON_NODE_POOL_SIZE; i++)
      {
         if (i != (SERVREG_MON_NODE_POOL_SIZE - 1))
         {
            next_pool->servreg_mon_node_pool[i].next = &(next_pool->servreg_mon_node_pool[i + 1]);
         }
         else
         {
            next_pool->servreg_mon_node_pool[i].next = servreg_mon_node_internal.servreg_mon_node_pool_free_p;
         }

         next_pool->servreg_mon_node_pool[i].mon_signature = SERVREG_MON_SIGNATURE;
         next_pool->servreg_mon_node_pool[i].rcesn_handle = SERVREG_NULL;
         next_pool->servreg_mon_node_pool[i].service_name = SERVREG_NULL;
         next_pool->servreg_mon_node_pool[i].handle_count = 0;
         next_pool->servreg_mon_node_pool[i].listener_ref_count = 0;
         next_pool->servreg_mon_node_pool[i].servreg_sync = SERVREG_FALSE;
         next_pool->servreg_mon_node_pool[i].transaction_id = 0;
         next_pool->servreg_mon_node_pool[i].state_q = NULL;
      }

      servreg_mon_node_internal.servreg_mon_node_pool_free_p = &(next_pool->servreg_mon_node_pool[0]);
      next_pool->next = servreg_mon_node_internal.servreg_mon_node_pool_head_p;
      servreg_mon_node_internal.servreg_mon_node_pool_head_p = next_pool;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_mon_node_pool_init() malloc failed");
      return SERVREG_NULL;
   }

   return servreg_mon_node_internal.servreg_mon_node_pool_free_p;
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_pool_alloc
 *
 * Description:
 *     Gives the first available free and allocated space from the memory
 *
 * Parameters:
 *     None
 *
 * Returns:
 *    servreg_mon_node_p : the first available free and allocated space from the memory
 * =====================================================================  */
static servreg_mon_node_p servreg_mon_node_pool_alloc(void)
{
   servreg_mon_node_p ret;
   servreg_mon_node_p sr_mon_node;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   if (SERVREG_NULL == servreg_mon_node_internal.servreg_mon_node_pool_free_p)
   {
      sr_mon_node = servreg_mon_node_pool_init();
   }
   else
   {
      sr_mon_node = servreg_mon_node_internal.servreg_mon_node_pool_free_p;
   }

   if (SERVREG_NULL != sr_mon_node)
   {
      servreg_mon_node_internal.servreg_mon_node_pool_free_p = sr_mon_node->next;
      sr_mon_node->next = SERVREG_NULL;
      ret = sr_mon_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_mon_node_pool_alloc() alloc failed");
      ret = SERVREG_NULL;
   }

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_pool_free
 *
 * Description:
 *     Reclaims back the sr_mon_node to the memory pool
 *
 * Parameters:
 *     sr_mon_node : space to be reclaimed back
 *
 * Returns:
 *    servreg_mon_node_p : The next available free space in the memory pool
 * =====================================================================  */
static servreg_mon_node_p servreg_mon_node_pool_free(servreg_mon_node_p sr_mon_node)
{
   servreg_mon_node_p ret = SERVREG_NULL;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   if(SERVREG_NULL != sr_mon_node)
   {
      sr_mon_node->rcesn_handle = SERVREG_NULL;
      servreg_free(sr_mon_node->service_name);
      sr_mon_node->handle_count = 0;
      sr_mon_node->listener_ref_count = 0;
      sr_mon_node->servreg_sync = SERVREG_FALSE;
      sr_mon_node->transaction_id = 0;

      sr_q_entry = (servreg_mon_queue_p)q_get(sr_mon_node->state_q);
      while(SERVREG_NULL != sr_q_entry)
      {
         servreg_free(sr_q_entry);
         sr_q_entry = (servreg_mon_queue_p)q_get(sr_mon_node->state_q);
      }

      //q_destroy(sr_mon_node->state_q);
      servreg_free(sr_mon_node->state_q);

      sr_mon_node->next = servreg_mon_node_internal.servreg_mon_node_pool_free_p;
      servreg_mon_node_internal.servreg_mon_node_pool_free_p = sr_mon_node;
      ret = sr_mon_node;
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_mon_node_pool_free() sr_mon_node is NULL and cannot be freed");
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_hashtab_get
 *
 * Description:
 *     Given the hash value, get the corresponding sr monitor node
 *
 * Parameters:
 *     hash : hash value which is based on the service name
 *
 * Returns:
 *     servreg_mon_node_p : sr monitor node that corresponds to the hash value
 * =====================================================================  */
static servreg_mon_node_p servreg_mon_node_hashtab_get(servreg_hash_t hash)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   sr_mon_node = servreg_mon_node_hashtab[hash];

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));

   return sr_mon_node;
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_hashtab_put
 *
 * Description:
 *     Put the sr monitor node to the hash table with the given hash value
 *
 * Parameters:
 *     sr_mon_node : sr monitor node to be put into the hash table
 *     hash        : hash value associated with the sr montior node
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_mon_node_hashtab_put(servreg_mon_node_p sr_mon_node, servreg_hash_t hash)
{
   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   /*  Insert to head of list for that hash value */
   sr_mon_node->next = servreg_mon_node_hashtab[hash];

   /* Update head for that hash value */
   servreg_mon_node_hashtab[hash] = sr_mon_node;

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_hashtab_delete
 *
 * Description:
 *     Delete the sr monitor node from the hash table with the given hash value
 *
 * Parameters:
 *     sr_mon_node : sr monitor node to be put into the hash table
 *     hash        : hash value associated with the sr montior node
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_mon_node_hashtab_delete(servreg_mon_node_p sr_mon_node_del, servreg_hash_t hash)
{
   servreg_mon_node_p sr_mon_node_curr = SERVREG_NULL, sr_mon_node_prev = SERVREG_NULL;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   /* Get the head of the list */
   sr_mon_node_curr = servreg_mon_node_hashtab[hash];

   while(SERVREG_NULL != sr_mon_node_curr)
   {
      if(sr_mon_node_curr == sr_mon_node_del)
      {
         if(SERVREG_NULL == sr_mon_node_prev)
         {
            /* Delete the first node */
            servreg_mon_node_hashtab[hash] = sr_mon_node_curr->next;
         }
         else
         {
            sr_mon_node_prev->next = sr_mon_node_curr->next;
         }

         sr_mon_node_curr->next = SERVREG_NULL;

         break; /* exit while() loop */
      }

      sr_mon_node_prev = sr_mon_node_curr;
      sr_mon_node_curr = sr_mon_node_curr->next;
   }

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));

   return;
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_nmehash
 *
 * Description:
 *     Function calculated the hash value given a name
 *
 * Parameters:
 *     name : name given to calculate the hash value
 *
 * Returns:
 *     hash : hash value calculated for the given name
 * =====================================================================  */
static servreg_hash_t servreg_mon_node_nmehash(SERVREG_NAME const name)
{
   SERVREG_NAME s = name;
   servreg_hash_t hash = 0;

   while ('\0' != *s)
   {
      servreg_hash_t temp;

      hash = (hash << 4) + (servreg_hash_t)(*s);
      if (0 != (temp = hash & 0xf0000000))
      {
         hash ^= (temp >> 24);
      }
      hash &= (~temp);
      s++;
   }
   return hash % (sizeof(servreg_mon_node_hashtab) / sizeof(servreg_mon_node_p));
}

/** =====================================================================
 * Function:
 *     servreg_mon_node_internal_init
 *
 * Description:
 *     Initialization of the internal memory pools and other internals
 *     for sr monitor nodes
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_mon_node_internal_init(void)
{
   servreg_mutex_init_dal(&(servreg_mon_node_internal.mutex));
   servreg_mutex_init_dal(&(servreg_mon_node_internal.mutex_create));

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex));

   servreg_mon_node_internal.dynamic_use = TRUE;
   secure_memset(&servreg_mon_node_hashtab, 0, sizeof(servreg_mon_node_hashtab));
   servreg_mon_node_pool_init();

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex));
}

/** =====================================================================
 * Function:
 *     servreg_create_mon_node
 *
 * Description:
 *     Creates a mon node with the given sr_name and rcesn handle
 *
 * Parameters:
 *    sr_name : domain+service or just domain name
 *    rc_handle : rcesn handle
 *
 * Returns:
 *    SERVREG_MON_HANDLE : handle to the mon node
 *
 * Note :
 *    No need of locks as this function will be called internally only by servreg_alloc_monitor_handle()
 * =====================================================================  */
static SERVREG_MON_HANDLE servreg_create_mon_node(SERVREG_NAME sr_name, RCESN_HANDLE rc_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   /* Check if the MON node exists */
   sr_mon_handle = servreg_get_sr_mon_handle(sr_name);

   if(SERVREG_NULL == sr_mon_handle)
   {
      /* Allocate a new mon node from the node pool */
      sr_mon_node = servreg_mon_node_pool_alloc();

      if(SERVREG_NULL != sr_mon_node && RCESN_NULL != rc_handle)
      {
         /* In Servreg Local Db servreg_concat() allocates memory for sr_name */
         sr_mon_node->service_name = sr_name;
         sr_mon_node->rcesn_handle = rc_handle;
         sr_mon_node->handle_count = 1;
         sr_mon_node->listener_ref_count = 0;
         sr_mon_node->servreg_sync = SERVREG_FALSE;
         sr_mon_node->transaction_id = 0;

         sr_mon_node->state_q = (q_type *)servreg_malloc(sizeof(q_type));
         /* Initialize the queue */
         q_init(sr_mon_node->state_q);

         sr_mon_node->state_sync_q = (q_type *)servreg_malloc(sizeof(q_type));
         /* Initialize the queue */
         q_init(sr_mon_node->state_sync_q);

         sr_mon_node->next = SERVREG_NULL;
         servreg_mon_node_hashtab_put(sr_mon_node, servreg_mon_node_nmehash(sr_mon_node->service_name));
         if(rcesn_signal_handle(rc_handle, SERVREG_SERVICE_STATE_UNINIT)){;}

         sr_mon_handle = sr_mon_node2sr_mon_handle(sr_mon_node);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_create_mon_node() sr_mon_node is NULL \n");
      }
   }
   else
   {
      sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);
      sr_mon_node->handle_count = sr_mon_node->handle_count + 1;
   }

   return sr_mon_handle;
}

/** =====================================================================
 * Function:
 *     servreg_service_queue
 *
 * Description:
 *     Function services the state queue for the given monitor handle.
 *     It will set the next pending state of the service.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     None
 * =====================================================================  */
static void servreg_service_queue(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);
   sr_q_entry = (servreg_mon_queue_p)q_check(sr_mon_node->state_q);

   if(SERVREG_NULL != sr_q_entry)
   {
      /* Update pending ack with new ref count */
      // sr_q_entry->ack_pending_count = sr_mon_node->listener_ref_count;

      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_service_queue() setting next state in queue\n");

      /* Signal all the listeners that the state of the service has changed for the first entry in the queue */
      if(rcesn_signal_handle(sr_mon_node->rcesn_handle, sr_q_entry->curr_state)){;}
      sr_mon_node->transaction_id = sr_mon_node->transaction_id + 1;
      
      if(sr_mon_node->listener_ref_count == 0)
      {
         /* Remove from the queue if no listeners attached to it */
         sr_q_entry = (servreg_mon_queue_p)q_get(sr_mon_node->state_q);
         servreg_free(sr_q_entry);

         /* Get the next entry in the queue*/
         sr_q_entry = (servreg_mon_queue_p)q_check(sr_mon_node->state_q);
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_service_queue() no pending states in queue \n");
   }

   return;
}

/** =====================================================================
 * Function:
 *     servreg_compare_state
 *
 * Description:
 *     Callback function given to the queue API's
 *     Function compares the state of q_entry state and the one passes in as a parameter
 *
 * Parameters:
 *     item_ptr : pointer to the item in the queue
 *     compare_val : service state value to be compared against
 *
 * Returns:
 *     1 : if state matches
 *     0 : if state does not match
 * =====================================================================  */
static int servreg_compare_state(void* item_ptr, void* compare_val)
{
   int ret = 0;

   if(((servreg_mon_queue_p)item_ptr)->curr_state == *((uint32_t *)compare_val))
   {
      ret = 1;
   }
   else
   {
      ret = 0;
   }

   return ret;
}

/** =====================================================================
* Function:
*     servreg_mon_ack_timer_cb
*
* Description:
*     Timer driven function to check the state of the ACKs for all the monitor
*     nodes
*
* Parameters:
*     unused
*
* Returns:
*     none
* =====================================================================  */
static void servreg_mon_ack_timer_cb(timer_cb_data_type unused)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;
   uint32_t i = 0;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   //MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_mon_ack_timer_cb() checking ACKs ");

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));

   for(i = 0; i < SERVREG_MON_NODE_HASHTABLE_BUCKETS; i++)
   {
      sr_mon_node = servreg_mon_node_hashtab[i];

      while(SERVREG_NULL != sr_mon_node)
      {
         sr_q_entry = (servreg_mon_queue_p)q_check(sr_mon_node->state_q);

         /* An entry in the queue suggests that the head q_entry state is set and its awaiting for the ACKs */
         if(SERVREG_NULL != sr_q_entry)
         {
            /* Decrement the timeout value */
            sr_q_entry->timeout = sr_q_entry->timeout - 1;
            
            if(sr_q_entry->timeout == 0)
            {
               //ERR_FATAL( "SERVREG_MON: in servreg_mon_ack_timer_cb() all ACKs not received for state = \n", sr_q_entry->curr_state, 0, 0);

               MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_mon_ack_timer_cb() Acks not received, processing next state ");

               /* All the ACK's not received within the certain timeframe, forcefully send out sync complete notif */
               if(RCECB_NULL != sr_q_entry->rcecb_handle)
               {
                  /* Callback function called even if all the ACK's are not received */
                  if(rcecb_signal_handle(sr_q_entry->rcecb_handle)){;}
               }

               /* Remove from the Queue after timeout even if all local ACK's are not received */
               sr_q_entry = (servreg_mon_queue_p)q_get(sr_mon_node->state_q);
               servreg_free(sr_q_entry);

               sr_mon_handle = sr_mon_node2sr_mon_handle(sr_mon_node);

               /* Process the next entry in the queue */
               servreg_service_queue(sr_mon_handle);
            }
         }
         sr_mon_node = sr_mon_node->next;
      }
   }

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return;
}

/** =====================================================================
 * Function:
 *     servreg_get_sr_mon_handle
 *
 * Description:
 *     Checks if a MON node already exists with the given name.
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" name
 *
 * Returns:
 *    SERVREG_MON_HANDLE : handle to the mon node
 *
 * Note :
 *    No need of locks as this function will be called internally only by servreg_create_mon_node()
 * =====================================================================  */
SERVREG_MON_HANDLE servreg_get_sr_mon_handle(SERVREG_NAME const name)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   sr_mon_node = servreg_mon_node_hashtab_get(servreg_mon_node_nmehash(name));

   while(SERVREG_NULL != sr_mon_node)
   {
      /* If they dont have the same address */
      if(sr_mon_node->service_name != name)
      {
         int len = servreg_nmelen(sr_mon_node->service_name);
         if(servreg_nmecmp(sr_mon_node->service_name, name, len) == 0 )
         {
            sr_mon_handle = sr_mon_node2sr_mon_handle(sr_mon_node);
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_get_sr_mon_handle() handle = 0x%x ", sr_mon_handle);
            break;
         }
      }
      else
      {
         sr_mon_handle = sr_mon_node2sr_mon_handle(sr_mon_node);
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_get_sr_mon_handle() handle = 0x%x", sr_mon_handle);
         break;
      }
      sr_mon_node = sr_mon_node->next;
   }

   return sr_mon_handle;
}

/** =====================================================================
 * Function:
 *     servreg_set_transaction_id
 *
 * Description:
 *     Set the current transaction_id.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 * =====================================================================  */
void servreg_set_transaction_id(SERVREG_MON_HANDLE sr_mon_handle, uint32_t sr_transaction_id)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         sr_mon_node->transaction_id =  sr_transaction_id;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_set_transaction_id() transaction_id = %d\n ", sr_transaction_id);
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_set_transaction_id() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_MON: sr_mon_node is NULL \n", 0, 0, 0);
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ;
}

/** =====================================================================
 * Function:
 *     servreg_alloc_monitor_handle
 *
 * Description:
 *     Given the domain + service name, this functions returns a handle
 *
 * Parameters:
 *     domain : "soc/domain/subdomain" info
 *     service : "provider/service" info. This field can be SERVREG_NULL also.
 *
 * Returns:
 *     SERVREG_MON_HANDLE : Opaque handle to a service state
 *                          Check for return value and if it is NOT NULL
 * =====================================================================  */
SERVREG_MON_HANDLE servreg_alloc_monitor_handle(SERVREG_NAME domain, SERVREG_NAME service)
{
   SERVREG_NAME servreg_name = SERVREG_NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;
   RCESN_HANDLE rcesn_handle = RCESN_NULL;

   if(SERVREG_SUCCESS == servreg_name_check(domain, service))
   {
      if(SERVREG_TRUE == servreg_is_local(domain))
      {
         servreg_name = servreg_concat(domain, service);
      }
      else
      {
         servreg_name = servreg_concat(domain, SERVREG_NULL);
      }

      if(SERVREG_NULL != servreg_name)
      {
         servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));

         /* First create the rcesn node */
         rcesn_handle = rcesn_create_name(servreg_name);

         if(RCESN_NULL != rcesn_handle)
         {
            /* Then Create the MON node */
            sr_mon_handle = servreg_create_mon_node(servreg_name, rcesn_handle);

            if(SERVREG_NULL != sr_mon_handle)
            {
               MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_alloc_monitor_handle() handle = 0x%x ", sr_mon_handle);
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: servreg_alloc_monitor_handle() handle is null");
            }
         }
         else
         {
            ERR_FATAL( "SERVREG_MON: in servreg_alloc_monitor_handle() rcesn_handle not created \n", 0, 0, 0);
         }

         servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_alloc_monitor_handle() servreg_name is NULL \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_alloc_monitor_handle() invalid domain or/and service name \n");
   }

   return sr_mon_handle;
}

/** =====================================================================
 * Function:
 *     servreg_free_monitor_handle
 *
 * Description:
 *     Free the monitor handle that is associated to an event mapped by domain+service name
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 *
 * Note : 
 *     If clients are no longer interested in the service AND they have de-registered their listeners
 *     using servreg_deregister_listener(), they can free the handle by calling servreg_free_monitor_handle().
 * =====================================================================  */
SERVREG_RESULT servreg_free_monitor_handle(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         sr_mon_node->handle_count = sr_mon_node->handle_count - 1;

         if(sr_mon_node->handle_count == 0)
         {
             /* If its a remote service */
            if(SERVREG_TRUE != servreg_is_domain_local(sr_mon_node->service_name))
            {
               /* via GLINK and/or QDI */
               ret = servreg_free_remote_handle(sr_mon_node->service_name, sr_mon_handle);
            }

            /* Delete the entry from the hash table */
            servreg_mon_node_hashtab_delete(sr_mon_node, servreg_mon_node_nmehash(sr_mon_node->service_name));

            /* Reclaim back the mon node to free pool */
            if(SERVREG_NULL != servreg_mon_node_pool_free(sr_mon_node))
            {
               ret = SERVREG_SUCCESS;
               MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_free_monitor_handle() handle freed\n ");
            }
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_free_monitor_handle() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_free_monitor_handle() sr_mon_handle is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }

   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_set_state
 *
 * Description:
 *     Function sets the state of the service
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *     state         : State of the service. See enum SERVREG_SERVICE_STATE for options
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_set_state(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE state)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ///* You can change the state only if its a local domain service */
         //if(SERVREG_TRUE == servreg_is_domain_local(sr_mon_node->service_name))
         //{
            if(SERVREG_TRUE == sr_mon_node->servreg_sync)
            {
               /* Check if an entry with the same state is present in the state_sync_q, if yes then delete it from state_sync_q and put in the state_q*/
               sr_q_entry = (servreg_mon_queue_p)q_linear_delete_new(sr_mon_node->state_sync_q, servreg_compare_state, &state, NULL, NULL);

               if(SERVREG_NULL != sr_q_entry)
               {
                  /* Reset the ack pending count */
                  sr_q_entry->ack_pending_count = sr_mon_node->listener_ref_count;
               }
               else
               {
                  sr_q_entry = (servreg_mon_queue_p)servreg_malloc(sizeof(servreg_mon_queue_t));

                  if(SERVREG_NULL != sr_q_entry)
                  {
                     sr_q_entry->ack_pending_count = sr_mon_node->listener_ref_count;
                     sr_q_entry->curr_state = state;
                     /* Dont create an rcecb entry until registrants to get sync complete notif register */
                     sr_q_entry->rcecb_handle = RCECB_NULL;
                     sr_q_entry->timeout = SERVREG_MON_ACK_TIMEOUT_COUNT;
                  }
                  else
                  {
                     ERR_FATAL("SERVREG_MON: in servreg_set_state() malloc failed \n", 0, 0, 0);
                  }
               }

               /* Check if its the first entry in the queue */
               if(NULL == (servreg_mon_queue_p)q_check(sr_mon_node->state_q))
               {
                  /* Signal all the listeners that the state of the service has changed if first entry in the queue*/
                  sr_mon_node->rcesn_handle = rcesn_signal_handle(sr_mon_node->rcesn_handle, state);

                  if (RCESN_NULL != sr_mon_node->rcesn_handle)
                  {
                     sr_mon_node->transaction_id = sr_mon_node->transaction_id + 1;
                     ret = SERVREG_SUCCESS;

                     if(sr_mon_node->listener_ref_count != 0)
                     {
                        /* Put the sr_q_entry into the queue and remove it only after you get all the ACK's back */
                        q_put(sr_mon_node->state_q, q_link(sr_q_entry, &sr_q_entry->link));
                     }
                     else
                     {
                        servreg_free(sr_q_entry);
                     }

                     MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_set_state() success, sync service name and state = 0x%x\n ", state);
                  }
                  else
                  {
                     MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_state() sr_mon_node->rcesn_handle not created \n");
                     ret = SERVREG_FAILURE;
                  }
               }
               else
               {
                  /* Put the sr_q_entry into the tail of the queue and process it only after the previous entry in the Queue has got all the ACK's back */
                  q_put(sr_mon_node->state_q, q_link(sr_q_entry, &sr_q_entry->link));

                  ret = SERVREG_SUCCESS;

                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_set_state() in queue to be set, sync service name and state = 0x%x\n ", state);
               }
            }
            else
            {
               /* Signal all the listeners that the state of the service has changed if not a sync service */
               sr_mon_node->rcesn_handle = rcesn_signal_handle(sr_mon_node->rcesn_handle, state);

               if (RCESN_NULL == sr_mon_node->rcesn_handle)
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_state() sr_mon_node->rcesn_handle not created \n");
                  ret = SERVREG_FAILURE;
               }
               else
               {
                  sr_mon_node->transaction_id = sr_mon_node->transaction_id + 1;
                  ret = SERVREG_SUCCESS;
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: servreg_set_state() success, service name and state = 0x%x\n ", state);
               }
            }
         //}
         //else
         //{
         //   MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_state() sr_mon_handle has invalid signature \n");
         //   ret = SERVREG_INVALID_PARAM;
        // }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_state() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_state() sr_mon_node is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_service_name
 *
 * Description:
 *     Given the monitor handle, the function returns the service name 
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     service_name or SERVREG_NULL
 * =====================================================================  */
SERVREG_NAME servreg_get_service_name(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_NAME ret = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ret = sr_mon_node->service_name;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_service_name() name = %s ", *ret);
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_get_service_name() sr_mon_handle has invalid signature \n");
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_get_service_name() sr_mon_node is NULL \n");
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_set_synchronous
 *
 * Description:
 *     Function sets the service state to be synchronous 
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_set_synchronous(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         sr_mon_node->servreg_sync = SERVREG_TRUE;
         ret = SERVREG_SUCCESS;
         MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_set_synchronous() service set as sync ");
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_synchronous() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_synchronous() sr_mon_node is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_synchronous
 *
 * Description:
 *     Function to get the sync state of the service
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *    TRUE if the sync state is set to TRUE, else FALSE
 * =====================================================================  */
SERVREG_BOOL servreg_get_synchronous(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_BOOL ret = SERVREG_FALSE;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ret = sr_mon_node->servreg_sync;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_synchronous() service sync state = %d ", ret);
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_get_synchronous() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_MON: in servreg_get_synchronous() sr_mon_node is NULL \n", 0, 0, 0);
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_pending_acks
 *
 * Description:
 *     Function gets the pending ack count for the service state
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *     state         : The service state for which the pending ack is requested
 * Returns:
 *     Ack count
 * =====================================================================  */
uint32_t servreg_get_pending_acks(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE state)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;
   uint32_t ret = 0xFFFFFFFF;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         sr_q_entry = (servreg_mon_queue_p)q_linear_search(sr_mon_node->state_q, servreg_compare_state, &state);

         if(SERVREG_NULL != sr_q_entry)
         {
            ret = sr_q_entry->ack_pending_count;
            MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_pending_acks() service pending ack = %d ", ret);
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_get_pending_acks() state does not exist \n");
         }
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_get_pending_acks() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_MON: sr_mon_node is NULL \n", 0, 0, 0);
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_register_listener
 *
 * Description:
 *     Register a listener to get event notifications for a service it is interested in
 *
 * Parameters:
 *     sr_mon_handle        : Handle to an existing service state which is mapped by domain + service 
 *                            or just domain name. This is the service that the listener is interested in.
 *     servreg_signal_type  : NHLOS specific notification signal type information
 *     signal               : NHLOS specific notification signal & mask information
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_listener(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SIGEX_TYPE servreg_signal_type, SERVREG_SIGEX servreg_signal)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   /* Initialized as SERVREG_SERVICE_STATE_UNINIT */
   uint32_t curr_remote_state = 0xFFFFFFFF, remote_transaction_id = 0;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));

   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         if(servreg_signal_type == SERVREG_SIGEX_TYPE_SIGQURT)
         {
            sr_mon_node->rcesn_handle = SERVREG_RCESN_REGISTER_HANDLE_QURT(sr_mon_node->rcesn_handle, (RCESN_FNSIG_COMPARE)rcesn_compare_ne_prev, servreg_signal);
         }
         else
         {
            sr_mon_node->rcesn_handle = SERVREG_RCESN_REGISTER_HANDLE_REX(sr_mon_node->rcesn_handle, (RCESN_FNSIG_COMPARE)rcesn_compare_ne_prev, servreg_signal);
         }

         if (RCESN_NULL != sr_mon_node->rcesn_handle)
         {
            sr_mon_node->listener_ref_count = sr_mon_node->listener_ref_count + 1;

            if(SERVREG_TRUE != servreg_is_local(sr_mon_node->service_name))
            {
               /* If its a remote service, then the service is automatically treated as synchronous service */
               sr_mon_node->servreg_sync = SERVREG_TRUE;
            }

            /* If its a remote service */
            if(SERVREG_TRUE != servreg_is_domain_local(sr_mon_node->service_name))
            {
               /* via GLINK and/or QDI */
               ret = servreg_register_remote_listener(sr_mon_node->service_name, sr_mon_handle, &curr_remote_state, &remote_transaction_id);

               if(SERVREG_SUCCESS == ret)
               {
                  /* Update the current remote state and remote transaction_id */
                  if(rcesn_setstatecurr_handle(sr_mon_node->rcesn_handle, curr_remote_state)){;}
                  sr_mon_node->transaction_id = remote_transaction_id;
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_register_listener() client registered for remote service\n ");
               }
               else
               {
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_listener() client registration failed for remote service ret = %d\n ", ret);
               }
            }
            else
            {
               /* Only a local service */
               ret = SERVREG_SUCCESS;
               MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_register_listener() client registered for local service\n ");
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_listener() sr_mon_node->rcesn_handle not created \n");
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_listener() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_listener() sr_mon_node is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_deregister_listener
 *
 * Description:
 *     De-Register a listener to get event notifications for a service
 *
 * Parameters:
 *     sr_mon_handle        : Handle to an existing service state which is mapped by domain + service 
 *                            or just domain name
 *     servreg_signal_type  : NHLOS specific notification signal type information
 *     signal               : NHLOS specific notification signal & mask information
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 *
 * Note : 
 *     If clients are no longer interested in the service AND they have de-registered their listeners
 *     using servreg_deregister_listener(), they can free the handle by calling servreg_free_monitor_handle().
 * =====================================================================  */
SERVREG_RESULT servreg_deregister_listener(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SIGEX_TYPE servreg_signal_type, SERVREG_SIGEX servreg_signal)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         if(servreg_signal_type == SERVREG_SIGEX_TYPE_SIGQURT)
         {
            sr_mon_node->rcesn_handle = SERVREG_RCESN_UNREGISTER_HANDLE_QURT(sr_mon_node->rcesn_handle, (RCESN_FNSIG_COMPARE)rcesn_compare_ne_prev, servreg_signal);
         }
         else
         {
            sr_mon_node->rcesn_handle = SERVREG_RCESN_UNREGISTER_HANDLE_REX(sr_mon_node->rcesn_handle, (RCESN_FNSIG_COMPARE)rcesn_compare_ne_prev, servreg_signal);
         }

         if (RCESN_NULL != sr_mon_node->rcesn_handle)
         {
            if(sr_mon_node->listener_ref_count != 0)
            {
               sr_mon_node->listener_ref_count = sr_mon_node->listener_ref_count - 1;

               /* If its registered as a remote service, de-register it */
               if(SERVREG_TRUE != servreg_is_domain_local(sr_mon_node->service_name))
               {
                  /* via GLINK and/or QDI */
                  ret = servreg_deregister_remote_listener(sr_mon_node->service_name, sr_mon_handle);

                  if(SERVREG_SUCCESS == ret)
                  {
                     MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_deregister_listener() client deregistered for remote service\n ");
                  }
                  else
                  {
                     MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_listener() client deregistration failed for remote service ret = %d\n ", ret);
                  }
               }
               else
               {
                  /* Only a local service */
                  ret = SERVREG_SUCCESS;
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_deregister_listener() client deregistered for local service\n ");
               }
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_listener() no listeners registered to de-register them\n");
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_listener() sr_mon_node->rcesn_handle not created \n");
            ret = SERVREG_FAILURE;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_listener() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_listener() sr_mon_node is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_listener_ref_count
 *
 * Description:
 *     Get the listener reference count for the given montior handle
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     Listener ref count
 * =====================================================================  */
uint32_t servreg_get_listener_ref_count(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   uint32_t ret = 0;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ret = sr_mon_node->listener_ref_count;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_listener_ref_count() ref count = %d\n ", ret);
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_get_listener_ref_count() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_MON: sr_mon_node is NULL \n", 0, 0, 0);
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_service_curr_state
 *
 * Description:
 *     Get the current state of a service that the listener is interested in
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     SERVREG_SERVICE_STATE : Returns the state of the service. 
 *                             Refer to the enum SERVREG_SERVICE_STATE for list of states
 * =====================================================================  */
SERVREG_SERVICE_STATE servreg_get_service_curr_state(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_SERVICE_STATE ret = SERVREG_SERVICE_STATE_UNINIT;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ret = rcesn_getstatecurr_handle(sr_mon_node->rcesn_handle);
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_service_curr_state() curr state = 0x%x\n ", ret);
      }
      else
      {
         ERR_FATAL( "SERVREG_MON: in servreg_get_service_curr_state() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL( "SERVREG_MON: in servreg_get_service_curr_state() sr_mon_node is NULL \n", 0, 0, 0);
   }
   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_get_transaction_id
 *
 * Description:
 *     Get the current transaction_id.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     Current transaction if for that service
 * =====================================================================  */
uint32_t servreg_get_transaction_id(SERVREG_MON_HANDLE sr_mon_handle)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   uint32_t ret = 0;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         ret = sr_mon_node->transaction_id;
         MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_get_transaction_id() transaction_id = %d\n ", ret);
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_get_transaction_id() sr_mon_handle has invalid signature \n", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("SERVREG_MON: sr_mon_node is NULL \n", 0, 0, 0);
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_set_ack
 *
 * Description:
 *     Function increases the ack count by one for that service
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_set_ack(SERVREG_MON_HANDLE sr_mon_handle, uint32_t transaction_id)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));

   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         if(SERVREG_TRUE == sr_mon_node->servreg_sync && transaction_id == sr_mon_node->transaction_id)
         {
            sr_q_entry = (servreg_mon_queue_p)q_check(sr_mon_node->state_q);

            if(SERVREG_NULL != sr_q_entry)
            {
               sr_q_entry->ack_pending_count = sr_q_entry->ack_pending_count - 1;

               if(sr_q_entry->ack_pending_count == 0)
               {
                  if(RCECB_NULL != sr_q_entry->rcecb_handle)
                  {
                     /* Callback function called only after all the ACK's are received */
                     if(rcecb_signal_handle(sr_q_entry->rcecb_handle)){;}
                  }

                  /* If its a remote service */
                  if(SERVREG_TRUE != servreg_is_domain_local(sr_mon_node->service_name))
                  {
                     /* via GLINK and/or QDI */
                     ret = servreg_set_remote_ack(sr_mon_node->service_name, sr_mon_handle, sr_q_entry->curr_state, sr_mon_node->transaction_id);
                  }
                  else
                  {
                     ret = SERVREG_SUCCESS;
                     MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_set_ack() one ack set, pending acks = %d", sr_q_entry->ack_pending_count);
                  }

                  /* Remove from the Queue when all the local ACK's are received */
                  sr_q_entry = (servreg_mon_queue_p)q_get(sr_mon_node->state_q);
                  servreg_free(sr_q_entry);

                  /* Process the next entry in the queue */
                  servreg_service_queue(sr_mon_handle);
               }
               else
               {
                  ret = SERVREG_SUCCESS;
                  MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_set_ack() one ack set, pending acks = %d", sr_q_entry->ack_pending_count);
               }
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_ack() sending ACK before state change notification sent out \n");
               //ERR_FATAL("SERVREG_MON: in servreg_set_ack() sending ACK before state change notification sent out ", 0, 0, 0);
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_ack() cannot set ACK if service is not synchronous or transaciton id invalid. Doing nothing. \n");
            ret = SERVREG_SUCCESS;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_ack() sr_mon_handle has invalid signature \n");
         //ERR_FATAL("SERVREG_MON: in servreg_set_ack() sr_mon_handle has invalid signature ", 0, 0, 0);
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_set_ack() sr_mon_node is NULL \n");
      //ERR_FATAL("ERVREG_MON: in servreg_set_ack() sr_mon_node is NULL ", 0, 0, 0);
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_register_sync_notif_cb
 *
 * Description:
 *     Register with a callback function to get sync notification for that service state
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *     state         : The service state for which the client wants to get the sync notification
 *                     Should preferably be SERVREG_SERVICE_STATE_DOWN or SERVREG_SERVICE_STATE_UP
 *     callback      : Callback function registered
 *     cb_p1         : Callback function parameter. Can be set to sr_mon_handle
 *     cb_p2         : Callback function parameter 2. Can be set to the service state.
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_register_sync_notif_cb(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE state, SERVREG_FNSIG_CALLBACK const callback, SERVREG_CB_PARAM cb_p1, SERVREG_CB_PARAM cb_p2)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         if(SERVREG_NULL != callback)
         {
            sr_q_entry = (servreg_mon_queue_p)q_linear_search(sr_mon_node->state_sync_q, servreg_compare_state, &state);

            /* If state_sync_q entry not found create a new one */
            if(SERVREG_NULL == sr_q_entry)
            {
               sr_q_entry = (servreg_mon_queue_p)servreg_malloc(sizeof(servreg_mon_queue_t));

               if(SERVREG_NULL != sr_q_entry)
               {
                  sr_q_entry->ack_pending_count = sr_mon_node->listener_ref_count;
                  sr_q_entry->curr_state = state;
                  sr_q_entry->rcecb_handle = rcecb_create_name(sr_mon_node->service_name);
                  sr_q_entry->timeout = SERVREG_MON_ACK_TIMEOUT_COUNT;
               }
               else
               {
                  ERR_FATAL("SERVREG_MON: in servreg_register_sync_notif_cb() malloc failed \n", 0, 0, 0);
               }
            }

            if(RCECB_NULL != sr_q_entry->rcecb_handle)
            {
               /* Event indicates sync completes */
               sr_q_entry->rcecb_handle = rcecb_register_parm2_handle(sr_q_entry->rcecb_handle, (RCECB_CALLBACK_FNSIG_P2)callback, (RCECB_PARM)cb_p1, (RCECB_PARM)cb_p2);

               /* Put the sync notif registrant in the sync q */
               q_put(sr_mon_node->state_sync_q, q_link(sr_q_entry, &sr_q_entry->link));
               ret = SERVREG_SUCCESS;
               MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_register_sync_notif_cb() client registered to get sync notification \n");
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_sync_notif_cb() rcecb_handle is null \n");
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_sync_notif_cb() callback is NULL \n");
            ret = SERVREG_INVALID_PARAM;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_sync_notif_cb() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_register_sync_notif_cb() sr_mon_node is NULL \n");
      ret = SERVREG_INVALID_HANDLE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_deregister_sync_notif_cb
 *
 * Description:
 *     De-Register to get sync notification for that service and de-register the callback function
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *     state         : The service state for which the client wants to dereigster to get sync notification
 *                     Should preferably be SERVREG_SERVICE_STATE_DOWN or SERVREG_SERVICE_STATE_UP
 *     callback      : callback function de-registered
 *     cb_p1         : Callback function parameter. Can be set to sr_mon_handle
 *     cb_p2         : Callback function parameter 2. Can be set to the service state.
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
SERVREG_RESULT servreg_deregister_sync_notif_cb(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_SERVICE_STATE state, SERVREG_FNSIG_CALLBACK const callback, SERVREG_CB_PARAM cb_p1, SERVREG_CB_PARAM cb_p2)
{
   servreg_mon_node_p sr_mon_node = SERVREG_NULL;
   SERVREG_RESULT ret = SERVREG_FAILURE;
   servreg_mon_queue_p sr_q_entry = SERVREG_NULL;

   sr_mon_node = sr_mon_handle2sr_mon_node(sr_mon_handle);

   servreg_mutex_lock_dal(&(servreg_mon_node_internal.mutex_create));
   if(SERVREG_NULL != sr_mon_node)
   {
      if(SERVREG_MON_SIGNATURE == sr_mon_node->mon_signature)
      {
         if(SERVREG_NULL != callback)
         {
            sr_q_entry = (servreg_mon_queue_p)q_linear_search(sr_mon_node->state_q, servreg_compare_state, &state);

            if(SERVREG_NULL == sr_q_entry)
            {
               /* If registrant not found in the state_q then find it in the state_sync_q */
               sr_q_entry = (servreg_mon_queue_p)q_linear_search(sr_mon_node->state_sync_q, servreg_compare_state, &state);
            }

            if(SERVREG_NULL != sr_q_entry)
            {
               if(RCECB_NULL != sr_q_entry->rcecb_handle)
               {
                  /* Event indicates sync completes */
                  sr_q_entry->rcecb_handle = rcecb_unregister_parm2_handle(sr_q_entry->rcecb_handle, (RCECB_CALLBACK_FNSIG_P2)callback, (RCECB_PARM)cb_p1, (RCECB_PARM)cb_p2);

                  ret = SERVREG_SUCCESS;
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_deregister_sync_notif_cb() client deregistered to get sync notification \n");
               }
               else
               {
                  MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_sync_notif_cb() rcecb_handle is null \n");
                  ret = SERVREG_FAILURE;
               }
            }
            else
            {
               MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_sync_notif_cb() sr_q_entry is NULL \n");
               ret = SERVREG_FAILURE;
            }
         }
         else
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_sync_notif_cb() callback is NULL \n");
            ret = SERVREG_INVALID_PARAM;
         }
      }
      else
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_sync_notif_cb() sr_mon_handle has invalid signature \n");
         ret = SERVREG_INVALID_HANDLE;
      }
   }
   else
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR, "SERVREG_MON: in servreg_deregister_sync_notif_cb() sr_mon_node is NULL \n");
      ret = SERVREG_FAILURE;
   }
   servreg_mutex_unlock_dal(&(servreg_mon_node_internal.mutex_create));

   return ret;
}

/** =====================================================================
 * Function:
 *     servreg_monitor_init
 *
 * Description:
 *     Initialization function for Service Registry Monitor feature.
 *     Service Registry will register itself as a service and sets its state
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_monitor_init(void)
{
   SERVREG_NAME domain = SERVREG_NULL;
   SERVREG_MON_HANDLE sr_mon_handle = SERVREG_NULL;

   /* Static memory pool allocation init */
   servreg_mon_node_internal_init();

   domain = servreg_get_local_domain();

   if(SERVREG_NULL != domain)
   {
      /* Register the state of the PD as UP with Service Registry */
      sr_mon_handle = servreg_alloc_monitor_handle(domain, SERVREG_NULL);

      if(SERVREG_NULL != sr_mon_handle)
      {
         /* Always set the pd service itself as synchronous */
         if(SERVREG_SUCCESS != servreg_set_synchronous(sr_mon_handle))
         {
            ERR_FATAL("SERVREG_MON: in servreg_monitor_init() servreg_set_synchronous() failed", 0, 0, 0);
         }

         if(SERVREG_SUCCESS == servreg_set_state(sr_mon_handle, SERVREG_SERVICE_STATE_UP))
         {
            MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH, "SERVREG_MON: in servreg_monitor_init() success \n");
         }
         else
         {
            ERR_FATAL("SERVREG_MON: in servreg_monitor_init() servreg_set_state() failed", 0, 0, 0);
         }
      }
      else
      {
         ERR_FATAL("SERVREG_MON: in servreg_monitor_init() sr_mon_handle NULL", 0, 0, 0);
      }
   }

   /* Set timer to check for the ACK state as deferrable timer to avoid unnecessary wakeup of Q6 */
   timer_group_set_deferrable(&servreg_group, TRUE);

   if(TE_SUCCESS != timer_def_osal(&servreg_mon_ack_timer,
                                  &servreg_group,
                                  TIMER_FUNC1_CB_TYPE,
                                  servreg_mon_ack_timer_cb, NULL))
   {
       ERR_FATAL("SERVREG_MON: in servreg_monitor_init() Timer def failed", 0, 0, 0);
   }

   if(TE_SUCCESS != timer_set_64(&servreg_mon_ack_timer,
                                SERVREG_MON_ACK_TIME_CHK_SEC,
                                SERVREG_MON_ACK_TIME_CHK_SEC,
                                T_SEC))
   {
       ERR_FATAL("SERVREG_MON: in servreg_monitor_init() Timer set failed", 0, 0, 0);
   }

   return;
}

