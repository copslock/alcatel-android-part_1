/** vi: tw=128 ts=3 sw=3 et
@file tms_utils_os_pitex_qurt.c
@brief This file contains the API for the TMS Utilities, API 0.2.0
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2014 QUALCOMM Technologies Incorporated.
All rights reserved.
QUALCOMM Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.slpi/1.0/debugtools/utils/src/tms_utils_os_pitex_qurt.c#1 $
$DateTime: 2014/09/25 11:54:23 $
$Change: 6661231 $
$Author: pwbldsvc $
===============================================================================*/

#if !defined(TMS_UTILS_EXCLUDE_MSG_SWEVT)
#include "msg.h"
#endif

#if !defined(TMS_UTILS_EXCLUDE_TRACER_SWEVT)
#include "tracer.h"
#include "tms_utils_tracer_swe.h"
#endif

// Function must remain reentrant and not utilize NHLOS or external library calls which
// are not reentrant or potentially cause any type of NHLOS blocking to occur.

#include "stdint.h"
#include "tms_utils.h"

#include "qurt.h"

#define TMS_OS_PITEX_COOKIE 0x0c0cc0c0

typedef struct tms_utils_os_pitex_s
{
   qurt_mutex_t mutex;                       // mutex implementation

   uint32_t cookie;                          // cookie protection to detect 'corruption'

} tms_utils_os_pitex_t, *tms_utils_os_pitex_p;

/**
API, Priority Inheritance Mutex, Required Client Supplied Resource Storage Size

Client caller is responsible to manage supplied resource storage.

@return        size_t                        Required Client Supplied Resource Storage Size (Bytes)
*/
size_t tms_utils_os_pitex_sz(void)
{
   return sizeof(tms_utils_os_pitex_t);      // supply to client the required size for resource storage
}

/**
API, Priority Inheritance Mutex, Initialize

Client caller is responsible to manage supplied resource storage.

tms_os_pitex tex;
tms_os_store tex_s;
if (NULL == (tex_s = (tms_os_store)malloc(tms_utils_os_pitex_sz()))) not_initialized;
if (NULL == (tex = tms_utils_os_pitex_init(tex_s, tms_utils_os_pitex_sz()))) not_initialized;

@param[in]     tms_os_store                  Client Supplied Resource Storage Address
@param[in]     size_t                        Client Supplied Resource Storage Size
@return        tms_os_pitex                  Opaque Mutex Handle
*/
tms_os_pitex tms_utils_os_pitex_init(tms_os_store tex_s, size_t tex_sz)
{
   tms_os_pitex rc = NULL;

   // check arguments

   if (NULL != tex_s && sizeof(tms_utils_os_pitex_t) == tex_sz)
   {
      tms_utils_os_pitex_p tex_p = (tms_utils_os_pitex_p)tex_s;

      qurt_pimutex_init(&tex_p->mutex);

      // initialize cookie

      tex_p->cookie = TMS_OS_PITEX_COOKIE;

      rc = tex_p;
   }

   return rc;
}

/**
API, Priority Inheritance Mutex, Destroy

Client caller is responsible to manage supplied resource storage.

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pitex_destroy(tex)) failure;
if (tex_s) free(tex_s);

@param[in]     tms_os_pitex                  Opaque Mutex Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pitex_destroy(tms_os_pitex tex)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != tex)
   {
      tms_utils_os_pitex_p tex_p = (tms_utils_os_pitex_p)tex;

      // destroy cookie

      tex_p->cookie = ~TMS_OS_PITEX_COOKIE;

      qurt_pimutex_destroy(&tex_p->mutex);

      rc = TMS_UTILS_STATUS_SUCCESS;
   }

   return rc;
}

/**
API, Priority Inheritance Mutex, Trylock

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pitex_trylock(tex)) failure;

@param[in]     tms_os_pitex                  Opaque Mutex Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pitex_trylock(tms_os_pitex tex)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != tex)
   {
      tms_utils_os_pitex_p tex_p = (tms_utils_os_pitex_p)tex;

      // check cookie intact

      if (TMS_OS_PITEX_COOKIE == tex_p->cookie)
      {
         // context obtains mutex

         if (0 == qurt_pimutex_try_lock(&tex_p->mutex))
         {
            rc = TMS_UTILS_STATUS_SUCCESS;
         }
      }
   }

   return rc;
}

/**
API, Priority Inheritance Mutex, Lock

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pitex_lock(tex)) failure;

@param[in]     tms_os_pitex                  Opaque Mutex Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pitex_lock(tms_os_pitex tex)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != tex)
   {
      tms_utils_os_pitex_p tex_p = (tms_utils_os_pitex_p)tex;

      // check cookie intact

      if (TMS_OS_PITEX_COOKIE == tex_p->cookie)
      {
         // context obtains mutex

         qurt_pimutex_lock(&tex_p->mutex);

         rc = TMS_UTILS_STATUS_SUCCESS;
      }
   }

   return rc;
}

/**
API, Priority Inheritance Mutex, Unlock

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pitex_unlock(tex)) failure;

@param[in]     tms_os_pitex                  Opaque Mutex Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pitex_unlock(tms_os_pitex tex)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != tex)
   {
      tms_utils_os_pitex_p tex_p = (tms_utils_os_pitex_p)tex;

      // check cookie intact

      if (TMS_OS_PITEX_COOKIE == tex_p->cookie)
      {
         // context obtains mutex

         qurt_pimutex_unlock(&tex_p->mutex);

         rc = TMS_UTILS_STATUS_SUCCESS;
      }
   }

   return rc;
}
