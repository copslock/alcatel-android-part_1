/** vi: tw=128 ts=3 sw=3 et
@file tms_utils_os_pisem_qurt.c
@brief This file contains the API for the TMS Utilities, API 0.2.0
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2014 QUALCOMM Technologies Incorporated.
All rights reserved.
QUALCOMM Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.slpi/1.0/debugtools/utils/src/tms_utils_os_pisem_qurt.c#1 $
$DateTime: 2014/09/25 11:54:23 $
$Change: 6661231 $
$Author: pwbldsvc $
===============================================================================*/

#if !defined(TMS_UTILS_EXCLUDE_MSG_SWEVT)
#include "msg.h"
#endif

#if !defined(TMS_UTILS_EXCLUDE_TRACER_SWEVT)
#include "tracer.h"
#include "tms_utils_tracer_swe.h"
#endif

// Function must remain reentrant and not utilize NHLOS or external library calls which
// are not reentrant or potentially cause any type of NHLOS blocking to occur.

#include "stdint.h"
#include "tms_utils.h"

#include "qurt.h"

#define TMS_OS_PISEM_COOKIE 0x0b0bb0b0

typedef struct tms_utils_os_pisem_s
{
   qurt_cond_t cond;                         // semaphore cond/mutex implementation

   qurt_mutex_t mutex;                       // semaphore cond/mutex implementation

   uint32_t count;                           // semaphore value

   uint32_t cookie;                          // cookie protection to detect 'corruption'

} tms_utils_os_pisem_t, *tms_utils_os_pisem_p;

/**
API, Priority Inheritance Semaphore, Required Client Supplied Resource Storage Size

Client caller is responsible to manage supplied resource storage.

@return        size_t                        Required Client Supplied Resource Storage Size (Bytes)
*/
size_t tms_utils_os_pisem_sz(void)
{
   return sizeof(tms_utils_os_pisem_t);      // supply to client the required size for resource storage
}

/**
API, Priority Inheritance Semaphore, Initialize

Client caller is responsible to manage supplied resource storage.

tms_os_pisem sem;
tms_os_store sem_s;
if (NULL == (sem_s = (tms_os_store)malloc(tms_utils_os_pisem_sz()))) not_initialized;
if (NULL == (sem = tms_utils_os_pisem_init(sem_s, tms_utils_os_pisem_sz()))) not_initialized;

@param[in]     tms_os_store                  Client Supplied Resource Storage Address
@param[in]     size_t                        Client Supplied Resource Storage Size
@return        tms_os_pisem                  Opaque Semaphore Handle
*/
tms_os_pisem tms_utils_os_pisem_init(tms_os_store sem_s, size_t sem_sz)
{
   tms_os_pisem rc = NULL;

   // check arguments

   if (NULL != sem_s && sizeof(tms_utils_os_pisem_t) == sem_sz)
   {
      tms_utils_os_pisem_p sem_p = (tms_utils_os_pisem_p)sem_s;

      qurt_pimutex_init(&sem_p->mutex);

      qurt_cond_init(&sem_p->cond);

      sem_p->count = 0;

      // initialize cookie

      sem_p->cookie = TMS_OS_PISEM_COOKIE;

      rc = sem_p;
   }

   return rc;
}

/**
API, Priority Inheritance Semaphore, Destroy

Client caller is responsible to manage supplied resource storage.

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pisem_destroy(sem)) failure;
if (sem_s) free(sem_s);

@param[in]     tms_os_pisem                  Opaque Semaphore Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pisem_destroy(tms_os_pisem sem)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != sem)
   {
      tms_utils_os_pisem_p sem_p = (tms_utils_os_pisem_p)sem;

      // destroy cookie

      sem_p->cookie = ~TMS_OS_PISEM_COOKIE;

      sem_p->count = 0;

      qurt_cond_destroy(&sem_p->cond);

      qurt_pimutex_destroy(&sem_p->mutex);

      rc = TMS_UTILS_STATUS_SUCCESS;
   }

   return rc;
}

/**
API, Priority Inheritance Semaphore, Wait

Wait for resource available from a semaphore; block calling context as required

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pisem_wait(sem)) failure;

@param[in]     tms_os_pisem                  Opaque Semaphore Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pisem_wait(tms_os_pisem sem)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != sem)
   {
      tms_utils_os_pisem_p sem_p = (tms_utils_os_pisem_p)sem;

      // check cookie intact

      if (TMS_OS_PISEM_COOKIE == sem_p->cookie)
      {
         // context obtains mutex

         qurt_pimutex_lock(&sem_p->mutex);

         while (0 == sem_p->count)
         {
            // context releases mutex via cond wait

            qurt_cond_wait(&sem_p->cond, &sem_p->mutex);

            // context acquires mutex via cond wait
         }

         sem_p->count--;

         // context releases mutex

         qurt_pimutex_unlock(&sem_p->mutex);

         rc = TMS_UTILS_STATUS_SUCCESS;
      }
   }

   return rc;
}

/**
API, Priority Inheritance Semaphore, Post

Post resource available to a semaphore; arrange to release next context waiting

if (TMS_UTILS_STATUS_SUCCESS > tms_utils_os_pisem_post(sem)) failure;

@param[in]     tms_os_pisem                  Opaque Semaphore Handle
@return        TMS_UTILS_STATUS              Operation Result
*/
TMS_UTILS_STATUS tms_utils_os_pisem_post(tms_os_pisem sem)
{
   TMS_UTILS_STATUS rc = TMS_UTILS_STATUS_ERROR;

   // check arguments

   if (NULL != sem)
   {
      tms_utils_os_pisem_p sem_p = (tms_utils_os_pisem_p)sem;

      // check cookie intact

      if (TMS_OS_PISEM_COOKIE == sem_p->cookie)
      {
         // context obtains mutex

         qurt_pimutex_lock(&sem_p->mutex);

         if (0 == sem_p->count)
         {
            // "wake all", let the scheduler sort out by priority
            // next context to acquire mutex upon release by this
            // context

            qurt_cond_broadcast(&sem_p->cond);
         }

         sem_p->count++;

         // context releases mutex

         qurt_pimutex_unlock(&sem_p->mutex);

         rc = TMS_UTILS_STATUS_SUCCESS;
      }
   }

   return rc;
}
