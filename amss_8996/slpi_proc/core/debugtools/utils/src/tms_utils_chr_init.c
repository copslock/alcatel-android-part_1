/** vi: tw=128 ts=3 sw=3 et
@file tms_utils_chr.c
@brief This file contains the API for the TMS Utilities, API 0.2.0
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2014 QUALCOMM Technologies Incorporated.
All rights reserved.
QUALCOMM Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.slpi/1.0/debugtools/utils/src/tms_utils_chr_init.c#1 $
$DateTime: 2014/09/25 11:54:23 $
$Change: 6661231 $
$Author: pwbldsvc $
===============================================================================*/

#if defined(TMS_UTILS_TRACER_SWEVT)
#include "tracer.h"
#include "tms_utils_tracer_swe.h"
#endif

// Code Size: ~0.3KB Optimized
// Stack Resource: ~32B Maximum Single Routine

// Function must remain reentrant and not utilize NHLOS or external library calls which
// are not reentrant or potentially cause any type of NHLOS blocking to occur.

#include "stdint.h"
#include "tms_utils.h"

size_t tms_utils_chr_init(int8_t* out_buf_p, size_t out_buf_sz, const int8_t chr)
{
   int8_t* out_p = out_buf_p;

   while (out_buf_sz && out_p)
   {
      *out_buf_p = chr;

      out_buf_sz--, out_p++;
   }

   if (out_buf_sz && out_p)
   {
      *out_p = '\0';

      return out_p - out_buf_p;
   }

   if (out_p && out_p - out_buf_p)
   {
      *(out_p - 1) = '\0';

      return out_p - out_buf_p;
   }

   return 0;
}
