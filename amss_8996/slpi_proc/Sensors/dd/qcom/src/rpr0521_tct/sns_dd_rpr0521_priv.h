/*===========================================================================

         S E N S O R S    AMBIENT LIGHT & PROXIMITY SENSOR    D R I V E R  

DESCRIPTION

  Defines the interface exposed by the ROHM ALS and Proximity driver
  RPR0521

********************************************************************************
* Copyright (c) 2012, Avago Technologies.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of Avago nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************

===========================================================================*/
/*-----------------------------------------------------------------------------
 * Copyright (c) 2012 - 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
  -----------------------------------------------------------------------------*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/core/pkg/dsps/rel/2.2/adsp_proc/Sensors/dd/qcom/src/sns_dd_rpr0521_priv.h#2 $


when         who     what, where, why
--------     ---     ------------------------------------------------------
24-Mar-2014  am      First Draft
==========================================================================*/

#ifndef _SNSDRPR0521PRIV_H
#define _SNSDRPR0521PRIV_H

#include "fixed_point.h"
#include "sns_ddf_util.h"
#include "msg_diag_service.h"

/*=======================================================================

                  INTERNAL DEFINITIONS

========================================================================*/
#ifndef NULL
#define NULL  0
#endif
//zxz add for idol4s cn calibration
#define FEATURE_TCTNB_RPR0521_CALIBRATION

#undef  ALSPRX_EN_DISTANCE
#define ALSPRX_DEBUG_REG
#undef  ALSPRX_DEBUG_TIMER
#undef  ALSPRX_DEBUG_ONE_SENSOR
#undef  ALSPRX_DEBUG_BASIC

#define SNS_DD_RPR0521_ID 			0x0A

#define SNS_DD_ALSPRX_DFLT_MODE               SNS_NORMAL_MODE
#define SNS_DD_ALSPRX_SUPPORTED_MODE          (1 << SNS_NORMAL_MODE | 1 << SNS_SLEEP_MODE)
#define SNS_DD_ALSPRX_DATA_READY_MS           50       /* after issue a command */
#define SNS_DD_ALSPRX_DATA_READY_US           50000  /* after PRX integration */

#define SNS_DD_ALS_DFLT_MILLI_LUX             100000  /* default value for the last sampled mlux */

#define SNS_DD_RPR0521_ALS_MAX_LUX       65535
//Mod-Begin- by TCTNB.ZXZ task-1747027,2016/03/04,FAE-sunlong modify ,task-1839022,2016/03/17,zhaohanhan confirm change
/* TCTNB.ZXZ task-2205307,2016/05/26,FAE-sunlong and zhaohanhan test and confirm*/
	#define SNS_DD_RPR0521_PRX_NEAR_THRESHOLD    25
	#define SNS_DD_RPR0521_PRX_FAR_THRESHOLD    15
//Mod-End- by TCTNB.ZXZ
#define SNS_DD_RPR0521_PRX_THESHOLD          300

#define SNS_DD_RPR0521_PRX_CAL_THRESHOLD     2000

#define SNS_DD_RPR0521_PILT_INIT             SNS_DD_RPR0521_PRX_FAR_THRESHOLD
#define SNS_DD_RPR0521_PIHT_INIT             SNS_DD_RPR0521_PRX_NEAR_THRESHOLD
#define SNS_DD_RPR0521_PILT_NEAR             SNS_DD_RPR0521_PRX_FAR_THRESHOLD
/*[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-977279, 2015/12/09, change 0x3FF=1023 to 0xFFF=4095 ,if not when near rawdata >1023 ,will continuous report interrupt*/
#define SNS_DD_RPR0521_PIHT_NEAR             0xFFF
/*[BUFFIX]-Mod-Begin- by TCTNB.ZXZ*/
#define SNS_DD_RPR0521_PILT_FAR              0
#define SNS_DD_RPR0521_PIHT_FAR              SNS_DD_RPR0521_PRX_NEAR_THRESHOLD

#define SNS_DD_RPR0521_ALS_CHANGE_THRESHOLD  0.2 /* any change of +/- 20% will cause an interrupt */
#define SNS_DD_RPR0521_AILT                  (1 - SNS_DD_RPR0521_ALS_CHANGE_THRESHOLD)
#define SNS_DD_RPR0521_AIHT                  (1 + SNS_DD_RPR0521_ALS_CHANGE_THRESHOLD)

#ifdef ADSP_HWCONFIG_L
#define SNS_DD_RPR0521_ALS_GA                105
#define SNS_DD_RPR0521_ALS_COE_B             187
#else
#define SNS_DD_RPR0521_ALS_GA                48
#define SNS_DD_RPR0521_ALS_COE_B             223
#endif

#define SNS_DD_RPR0521_ALS_COE_C             70
#define SNS_DD_RPR0521_ALS_COE_D             142
#define SNS_DD_RPR0521_ALS_DF                52

/* Register Addresses define */
#define SNS_DD_RPR0521_ID_ADDR		0x40
#define SNS_DD_RPR0521_ENABLE_ADDR	0x41
#define SNS_DD_RPR0521_ALS_ADDR		0x42
#define SNS_DD_RPR0521_PRX_ADDR		0x43
#define SNS_DD_RPR0521_PDATAL_ADDR	0x44
#define SNS_DD_RPR0521_PDATAH_ADDR	0x45
#define SNS_DD_RPR0521_ADATA0L_ADDR	0x46
#define SNS_DD_RPR0521_ADATA0H_ADDR	0x47
#define SNS_DD_RPR0521_ADATA1L_ADDR	0x48
#define SNS_DD_RPR0521_ADATA1H_ADDR	0x49
#define SNS_DD_RPR0521_STATUS_ADDR	0x4A
#define SNS_DD_RPR0521_PIHTL_ADDR	0x4B
#define SNS_DD_RPR0521_PIHTH_ADDR	0x4C
#define SNS_DD_RPR0521_PILTL_ADDR	0x4D
#define SNS_DD_RPR0521_PILTH_ADDR	0x4E
#define SNS_DD_RPR0521_AIHTL_ADDR	0x4F
#define SNS_DD_RPR0521_AIHTH_ADDR	0x50
#define SNS_DD_RPR0521_AILTL_ADDR	0x51
#define SNS_DD_RPR0521_AILTH_ADDR	0x52
/* Register Value define : STATUS */
#define SNS_DD_RPR0521_PINT_STATUS         0x80  /* PRX interrupt status */
#define SNS_DD_RPR0521_AINT_STATUS         0x40  /* ALS interrupt status */

/* attributes for als data type */
/* The res and accuracy for ALS are not static values. They're calculated from the previous data */ 
#define SNS_DD_ALS_PWR                        175      /* unit of uA */
#define SNS_DD_ALS_RES                        FX_FLTTOFIX_Q16(0.01)   /* unit of this data type is lux and under open air (no cover glass and black ink */
#define SNS_DD_ALS_LO_PWR                     4       /* unit of uA */
#define SNS_DD_ALS_ACCURACY                   FX_FLTTOFIX_Q16(0.01)   /* unit of this data type is lux and under open air (no cover glass and black ink */

/* attributes for proximity data type */
#define SNS_DD_PRX_PWR                      12675   /* unit of uA */ /* LED peak current + ADC current */
#define SNS_DD_PRX_RES                      FX_FLTTOFIX_Q16(0.001)   /* unit of this data type is meter */
#define SNS_DD_PRX_ACCURACY                 1      /* unit of this data type unit which is mm */

/* attribute for NV items */
#define SNS_DD_VISIBLE_TRANS_RATIO            25
#define SNS_DD_IR_TRANS_RATIO                 60
#define SNS_DD_DC_OFFSET                      10      /* unit of ADC count */
#define SNS_DD_PRX_THRESH_NEAR                80      /* unit of ADC count */
#define SNS_DD_PRX_THRESH_FAR                 50      /* unit of ADC count */
#define SNS_DD_PRX_FACTOR                     200
#define SNS_DD_ALS_FACTOR                     100

#define SNS_DD_PRXDIST_TB_MAX_VALUE           1023    /* 10 bits */
#define SNS_DD_RPR0521_NUM_SENSOR_TYPES        2


#define SNS_DD_ALS_CALIBRATED_LUX             300 /* 300 calibrated lux */

//add for ALS outdoor
#define JUDGE_OUTDOOR     10000
#define COEF_OUTDOOR    7

#ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
	#define DEFAULT_CROSSTALK   4095-SNS_DD_RPR0521_PRX_NEAR_THRESHOLD
#endif

//grace modify in 2014.4.11 begin
#define ALS_EN	(0x1 << 7)
#define PS_EN		(0x1 << 6)
#define BOTH_STANDBY	(0)
#define PS10MS		(0x1) /*modified by TCTSH tao-yu, PR-977004, 2015/11/25*/
#define PS40MS		(0x2)
#define PS100MS		(0x3)
#define ALS100MS_PS50MS	(0x5)
#define BOTH100MS	(0x6)
#define MODE_NONUSE         (0)
#define MODE_PROXIMITY      (1)
#define MODE_ILLUMINANCE    (2)
#define MODE_BOTH           (3)

#define LEDCURRENT_025MA    (0)
#define LEDCURRENT_050MA    (1)
#define LEDCURRENT_100MA    (2)
#define LEDCURRENT_200MA    (3)

#define PS_THH_ONLY         (0 << 4)
#define PS_THH_BOTH_HYS     (1 << 4)
#define PS_THH_BOTH_OUTSIDE (2 << 4)
#define POLA_ACTIVEL        (0 << 3)
#define POLA_INACTIVEL      (1 << 3)
#define OUTPUT_ANYTIME      (0 << 2)
#define OUTPUT_LATCH        (1 << 2)
#define MODE_NONUSE         (0)
#define MODE_PROXIMITY      (1)
#define MODE_ILLUMINANCE    (2)
#define MODE_BOTH           (3)

//grace modify in 2014.4.11 end


//grace modify in 2014.4.22 begin
#define INFRA_LOW 		0X0
#define INFRA_MID 		0X01
#define INFRA_HIGH 		0X03
//grace modify in 2014.4.22 end

/*=======================================================================

                  Macros

========================================================================*/
/* Negative ADC counts will be treated as zero */
#define ALSPRX_CONV_TO_UNSIGNED(var, bits) ((var & (1<<(bits-1))) ? (0) : (var))

#define SNS_DD_RPR0521_DEFAULT_ALS_CHANGE_PCNT SNS_DD_ALSPRX_ALS_CHANGE_0_78_PCNT
#define PCNT_OF_RAW(x, raw) (raw>>x)


//#define ALSPRX_DEBUG
#ifdef ALSPRX_DEBUG
#if defined QDSP6
#define DBG_MEDIUM_PRIO DBG_MED_PRIO
#define ALSPRX_MSG_0(level,msg)          MSG(MSG_SSID_SNS,DBG_##level##_PRIO, "ALSPRX - " msg)
#define ALSPRX_MSG_1(level,msg,p1)       MSG_1(MSG_SSID_SNS,DBG_##level##_PRIO, "ALSPRX - " msg,p1)
#define ALSPRX_MSG_2(level,msg,p1,p2)    MSG_2(MSG_SSID_SNS,DBG_##level##_PRIO, "ALSPRX - " msg,p1,p2)
#define ALSPRX_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_SNS,DBG_##level##_PRIO, "ALSPRX - " msg,p1,p2,p3)
#elif ! defined(ADSP_STANDALONE)
#define MED MEDIUM
#include "sns_debug_str.h"
#define ALSPRX_MSG_0(level,msg)          SNS_PRINTF_STRING_ID_##level##_0(SNS_DBG_MOD_DSPS_SMGR,DBG_DD_ALSPRX_STRING0)
#define ALSPRX_MSG_1(level,msg,p1)       SNS_PRINTF_STRING_ID_##level##_1(SNS_DBG_MOD_DSPS_SMGR,DBG_DD_ALSPRX_STRING1,p1)
#define ALSPRX_MSG_2(level,msg,p1,p2)    SNS_PRINTF_STRING_ID_##level##_2(SNS_DBG_MOD_DSPS_SMGR,DBG_DD_ALSPRX_STRING2,p1,p2)
#define ALSPRX_MSG_3(level,msg,p1,p2,p3) SNS_PRINTF_STRING_ID_##level##_3(SNS_DBG_MOD_DSPS_SMGR,DBG_DD_ALSPRX_STRING3,p1,p2,p3)
#else
#define ALSPRX_MSG_0(level,msg)          printf(msg)
#define ALSPRX_MSG_1(level,msg,p1)       printf(msg,p1)
#define ALSPRX_MSG_2(level,msg,p1,p2)    printf(msg,p1,p2)
#define ALSPRX_MSG_3(level,msg,p1,p2,p3) printf(msg,p1,p2,p3)
#endif
#else
#define ALSPRX_MSG_0(level,msg)
#define ALSPRX_MSG_1(level,msg,p1)
#define ALSPRX_MSG_2(level,msg,p1,p2)
#define ALSPRX_MSG_3(level,msg,p1,p2,p3)
#endif

//[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,PR-1021636, 2015/12/04, add for TCT log
#ifndef ALSPRX_DEBUG

#define TCT_ALSPRX_DEBUG
#ifdef TCT_ALSPRX_DEBUG
#define DBG_MEDIUM_PRIO DBG_MED_PRIO
#define TCT_ALSPRX_MSG_0(level,msg)          MSG(MSG_SSID_SNS,DBG_##level##_PRIO, "TCT - " msg)
#define TCT_ALSPRX_MSG_1(level,msg,p1)       MSG_1(MSG_SSID_SNS,DBG_##level##_PRIO, "TCT - " msg,p1)
#define TCT_ALSPRX_MSG_2(level,msg,p1,p2)    MSG_2(MSG_SSID_SNS,DBG_##level##_PRIO, "TCT - " msg,p1,p2)
#define TCT_ALSPRX_MSG_3(level,msg,p1,p2,p3) MSG_3(MSG_SSID_SNS,DBG_##level##_PRIO, "TCT - " msg,p1,p2,p3)
#else
#define TCT_ALSPRX_MSG_0(level,msg)
#define TCT_ALSPRX_MSG_1(level,msg,p1)
#define TCT_ALSPRX_MSG_2(level,msg,p1,p2)
#define TCT_ALSPRX_MSG_3(level,msg,p1,p2,p3)
#endif

#else
#define TCT_ALSPRX_MSG_0(level,msg)
#define TCT_ALSPRX_MSG_1(level,msg,p1)
#define TCT_ALSPRX_MSG_2(level,msg,p1,p2)
#define TCT_ALSPRX_MSG_3(level,msg,p1,p2,p3)
#endif
//[BUFFIX]-Mod-End- by TCTNB.ZXZ

/*=======================================================================

                  TYPE DEFINITIONS

========================================================================*/

typedef enum 
{
  SNS_DD_ALSPRX_ALS_CHANGE_MIN_PCNT = 0,
  SNS_DD_ALSPRX_ALS_CHANGE_50_PCNT  = 1,  /* x>>1 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_25_PCNT  = 2,  /* x>>2 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_12_5_PCNT  = 3,  /* x>>3 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_6_25_PCNT  = 4,  /* x>>4 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_3_125_PCNT  = 5,  /* x>>5 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_1_56_PCNT  = 6,  /* x>>6 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_0_78_PCNT  = 7,  /* x>>7 */ 
  SNS_DD_ALSPRX_ALS_CHANGE_MAX_PCNT
} sns_dd_alsprx_als_change_pcnt_e;

typedef enum 
{
  SNS_DD_ALSPRX_RES_14BIT  = 0,  /* 27ms integration time */ 
  SNS_DD_ALSPRX_RES_15BIT = 1,   /* 50ms integration time */
  SNS_DD_ALSPRX_RES_16BIT = 2   /* 100ms integration time */
} sns_dd_alsprx_res_e;

typedef enum 
{
  SNS_DD_ALS_GAIN_1X    = 0,    /* 1x AGAIN */ 
  SNS_DD_ALS_GAIN_2X    = 1,    /* 2x AGAIN */
  SNS_DD_ALS_GAIN_64X   = 2,   /* 64x AGAIN */
  SNS_DD_ALS_GAIN_128X  = 3   /* 128x AGAIN */
} sns_dd_als_gain_e;

typedef enum
{
  SNS_DD_PS_GAIN_1X		=0,
  SNS_DD_PS_GAIN_2X		=1,
  SNS_DD_PS_GAIN_4X		=2,
  SNS_DD_PS_GAIN_INVALID	=3,
} sns_dd_ps_gain_e;

typedef enum 
{
  SNS_DD_PEND_STATE_IDLE,                     /* idle state */
  SNS_DD_PEND_STATE_PEND,                     /* waiting a response */
  SNS_DD_PEND_STATE_RSVD                      /* reserved */
} sns_dd_pend_state_type;

typedef enum 
{
  SNS_PRX_FAR_AWAY,
  SNS_PRX_NEAR_BY,
  SNS_PRX_NEAR_BY_UNKNOWN
} sns_dd_prx_nearby_type;

/* timer object type that is a parameter of sns_dd_start_general_timer/sns_dd_stop_general_timer  */
typedef struct
{   
  bool             active;
} sns_dd_timer_type;

/* data structure for proximity common handler */

/* The state of a device */
typedef  enum 
{
  SNS_DD_DEV_STOPPED,                         /* 0 */
  SNS_DD_DEV_CONFIGURED,                      /* 1 */
  SNS_DD_DEV_GET_DATA,                        /* 2 */
  SNS_DD_DEV_MAX_STATES                       /* 3 */
}sns_dd_device_state_type;

typedef enum
{
  SNS_DD_RPR0521_NV_SOURCE_DEFAULT,
  SNS_DD_RPR0521_NV_SOURCE_REG
}sns_dd_nv_source;

/* Error codes indicating reasons for test failures */
typedef enum
{
    SNS_DD_RPR0521_SUCCESS        = 0,
    SNS_DD_RPR0521_PRX_CAL_FAILED = 1,
    SNS_DD_RPR0521_ALS_CAL_FAILED = 2
} sns_dd_test_err_e;

/* data structure for light(ALS) driver */
typedef struct
{
  bool      enable;
  //uint8_t   als_res_index;    /* index for the als res */
  uint16_t  als_gain_index;   /* needed for Lux calculation */
  uint16_t  als_poll_delay;   /* needed for light sensor polling : micro-second (us) */
  uint16_t  als_atime;	      /* storage for als integratiion time */
  uint16_t  als_threshold_l;  /* low threshold */
  uint16_t  als_threshold_h;  /* high threshold */
  uint32_t  last_get_time;
  uint32_t  last_mlux;
  uint16_t  cdata;
  uint16_t  irdata;
  bool      als_reduce;       /* to be removed */
  sns_dd_timer_type         sns_dd_timer;
  sns_ddf_timer_s           timer;
  uint32_t                saturated_lux;
} sns_dd_als_db_type;

/* data structure for proximitydistance driver */
typedef struct
{
  bool                      enable;
  uint16_t                  thresh_near;
  uint16_t                  thresh_far;
  uint16_t                  pdata;			/* to store PS data */
  uint16_t  		ps_gain_index;   /* PS Gain */
  int32_t                   prxdist_data_cache;
  sns_dd_prx_nearby_type    prx_detection_state;	
  sns_dd_prx_nearby_type    last_nearby;
  sns_dd_timer_type         sns_dd_timer;
  sns_ddf_timer_s           timer;
} sns_dd_prx_db_type;

/* data structure for NV items */
typedef struct
{
  uint8_t  visible_ratio;    /* visible light transmission ratio of the glass/pipe in % */
  uint8_t  ir_ratio;         /* IR transmission ratio of light glass/pipe in % */
  uint16_t dc_offset;        /* DC offset */
  uint16_t thresh_near;      /* near detection threshold in ADC count adjusted by DC offset */
  uint16_t thresh_far;       /* far detection threshold in ADC count adjusted by DC offset */
  uint16_t prx_factor;       /* Proximity crosstalk offset */
  uint16_t als_factor;       /* ALS calibration factor */
  uint32_t version_num;      /* Version of NV Database */
  uint32_t id;               /* Unique identifier for sensor/vendor combination */
  uint32_t als_change_pcnt;  /* ALS change threshold for DRI */

} sns_dd_nv_db_type;

/* common data structure belong to all sub modules */
typedef struct
{   
  /* timer for reading data from the sensor instead of ISR sig handler */
  sns_dd_nv_db_type         nv_db;                  /* NV item DB type */  
  sns_dd_nv_source          nv_source;
  uint16_t                  nv_size;
  sns_ddf_powerstate_e      pwr_mode;  
  sns_dd_device_state_type  state;
  bool                      als_get_data;
  bool                      prx_get_data;  
  uint32_t                  als_odr;
  uint32_t                  prx_odr;
  uint32_t                  als_req_odr;
  uint32_t                  prx_req_odr;
   #ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
  uint32_t                  prx_pre_odr;
  #endif
  uint32_t                  cycle_time;
  bool                      prx_calibration;
  bool                      als_calibration;
  uint16_t                  prx_calibration_loop;
  uint16_t                  prx_calibration_pdata;

} sns_dd_rpr0521_common_db_type;

/* State struct for RPR0521 */
typedef struct {
   sns_ddf_handle_t               smgr_handle;    /* SDDI handle used to notify_data */
   uint32_t                       dropped_logs;
   sns_ddf_handle_t               port_handle; /* handle used to access the I2C bus */
   sns_ddf_handle_t               ZBW_port_handle; /* handle used to access the I2C bus for ZBW(zero byte writes)*/
   sns_dd_als_db_type             sns_dd_als_db;
   sns_dd_prx_db_type             sns_dd_prx_db;
   sns_dd_rpr0521_common_db_type sns_dd_rpr0521_common_db;
   sns_ddf_sensor_sample_s        sensor_sample[2];
   sns_ddf_sensor_data_s          sensor_data;
   uint32_t                       interrupt_gpio;
   bool                           dri_enabled;
   uint32_t                       driver_state; /* TODO - remove */
   sns_ddf_time_t                 int_timestamp;
   uint16_t                       enable_reg_data;
   #ifdef FEATURE_TCTNB_RPR0521_CALIBRATION
   unsigned char init_state;
   #endif
} sns_dd_rpr0521_state_t;

/*[BUFFIX]-Mod-Begin- by TCTNB.ZXZ,task-2205307, 2016/05/25,add this for change gain*/
	#define SNS_DD_RPR0521_ALS_GAIN_CHANGE SNS_DD_ALS_GAIN_2X
	#define SNS_DD_RPR0521_ALS_MLUX_HIGH 600000
	#define SNS_DD_RPR0521_ALS_MLUX_LOW 500000
/*[BUFFIX]-Mod-End- by TCTNB.ZXZ,task-2205307, 2016/05/25*/

#endif /* End include guard  SNSDALSPRXPRIV_H */
