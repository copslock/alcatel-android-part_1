/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains various functions for accessing the FP sensor.
    It is a small subset of functions implemented in TZ driver, needed
    for the finger detect functioanlity.

Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/inc/sns_qfp_sd_misc.h#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
08/12/15     LD      Initial version
==============================================================================*/

#ifndef _SNS_QFP_SD_MISC_H
#define _SNS_QFP_SD_MISC_H

#include "sns_common.h"
#include "utimer.h"
#include "sns_qfp_sd_spi_base.h"

/**
 * @brief Read the chip ID from the FP sensor
 * @param[in] spi_handle handle for accessing SPI device
 * @param[out] chip_id will be filled with chip id
 * @return SNS_SUCCESS if successful
 */
sns_err_code_e sns_qfp_sd_read_chip_id(
  sns_qfp_sd_spi_handle_t spi_handle,
  uint32_t* chip_id
);

/**
 * @brief Power up all modules in FP sensor, this is needed
 *        before reading image
 * @param[in] spi_handle handle for accessing SPI device 
 * @param[in] delay units after powering-up Ontario 
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_enable_all_modules(
  sns_qfp_sd_spi_handle_t spi_handle
);

/**
 * @brief Wake up the FP sensor
 * @param[in] spi_handle handle for accessing SPI device
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_ontario_wakeup(
  sns_qfp_sd_spi_handle_t spi_handle
);

/**
 * @brief Transition FP sensor to idle mode
 * @param[in] spi_handle handle for accessing SPI device
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_ontario_idle(
  sns_qfp_sd_spi_handle_t spi_handle
);

/**
 * @brief Enable or disable the tone burst in the FP sensor
 * @param[in] spi_handle handle for accessing SPI device
 * @param[in] enable true for enable, false for disable
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_set_tbenable(
  sns_qfp_sd_spi_handle_t spi_handle,
  bool enable
);

/**
 * @brief Suspends the task for the specified time
 * @param[in] timeout to wait in usec
 * @return None
 */
SNS_QFPSD_UIMAGE_CODE void sns_qfp_sd_utimer_sleep(
  utimer_timetick_type timeout
);

#endif // _SNS_QFP_SD_MISC_H

