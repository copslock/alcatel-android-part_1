/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains basic functions to configure SPI and read/write
    data blocks. It is based on OntarioSpi.h in Ontario TZ SPI driver

Copyright (c) 2015 by QUALCOMM Technologies, Incorporated. 
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/inc/sns_qfp_sd_spi_base.h#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
05/10/15     LD      Initial version based on OntarioSpi.h
==============================================================================*/

#ifndef _SNS_QFP_SD_SPI_BASE_H
#define _SNS_QFP_SD_SPI_BASE_H

#include "sns_common.h"
#include "SpiDriver.h"

#ifdef SNS_USES_ISLAND
  #define SNS_QFPSD_UIMAGE_CODE __attribute__((section (".text.QFPSD")))
  #define SNS_QFPSD_UIMAGE_DATA __attribute__((section (".data.QFPSD")))
#else
#define SNS_QFPSD_UIMAGE_CODE
#define SNS_QFPSD_UIMAGE_DATA
#endif /* SNS_USES_ISLAND */

/** opaque handle for accessing SPI port */
typedef void* sns_qfp_sd_spi_handle_t;

/** 
 *  @brief Allocate the SPI handle. Call this function before
 *  any other function in this module that receives an SPI
 *  handle.
 *  
 *  @param[out] handle SPI handle will be allocated
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
sns_err_code_e sns_qfp_sd_spi_alloc_handle(sns_qfp_sd_spi_handle_t* handle);

/** 
 *  @brief Free the allocated SPI handle. After successful
 *         return handle is invalid and must not be used again
 *  
 *  @param[in] handle SPI handle to be freed
 *  
 */
void sns_qfp_sd_spi_free_handle(sns_qfp_sd_spi_handle_t handle);

/** 
 *  @brief Configure SPI parameters, open the SPI ports
 *  and configure/enable clocks.
 *  
 *  @param[in] handle SPI handle for accessing SPI device
 *  @param[in] device_id SPI port id
 *  @param[in] slave_index  slave index, to identify sensor
 *               when multiple devices connected to SPI bus.
 *  @param[in] freq Requested clock frequency to use for SPI
 *        transactions (in Hz)
 *  
 *  @return SNS_SUCCESS if completed successfully
 */
sns_err_code_e sns_qfp_sd_spi_init(
    sns_qfp_sd_spi_handle_t handle,
    spi_device_id_t device_id,
    uint8_t slave_index,
    uint32_t freq);

/** 
 *  @brief Close SPI port, disable clocks
 *  
 *  @param[in] handle for accessing SPI device.
 *  
 *  @note Must be called after successful call to init
 */
void sns_qfp_sd_spi_deinit(sns_qfp_sd_spi_handle_t handle);

/** 
 *  @brief Perform a full duplex SPI transaction
 *  Data in buffer is sent to SPI device and in the same time
 *  data is read from SPI device into the same buffer
 *  
 *  @param[in] handle for accessing SPI device.
 *  @param[in,out] address buffer for read/write
 *  @param[in] size number of bytes to read/write
 *  
 *  @note must be called after a successful call to init
 *  address must be 64-bit aligned because of limitations
 *  in current SPI driver in SLPI (last checked on 00164
 *  release)
 *  Transaction is always performed immediately, buffering
 *  enabled state is ignored. If there accumulated data in the
 *  buffer they will not be sent first. 
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e spi_qfp_sd_spi_full_duplex(
  sns_qfp_sd_spi_handle_t handle,
  uint8_t* address,
  uint32_t size);

/** 
 *  @brief Perform a full duplex SPI transfer, with transaction
 *  buffering if enabled.
 *  
 *  @param[in] handle for accessing SPI device.
 *  @param[in,out] address buffer for read/write
 *  @param[in] size number of bytes to read/write
 *  
 *  @note must be called after a successful call to init
 *  address must be 64-bit aligned because of limitations
 *  in current SPI driver in SLPI (last checked on 00164
 *  release)
 *  if buffering is enabled, may not do transaction immediately,
 *  see comments in the functions below. If direct is true, data
 *  will be send to SPI device immediately even if buffering is
 *  enabled. However if buffering was enabled it will flush the
 *  buffer which may cause additional data to be sent before
 *  this transaction.
 *  Use this function only for write-only transactions (where
 *  read data can be ignored). 
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e spi_qfp_sd_spi_transfer(
  sns_qfp_sd_spi_handle_t handle,
  uint8_t* address, uint32_t size);

/**
 * @brief Flash the SPI transaction buffer, sending all data in 
 *        buffer as a single SPI transaction
 *  
 * @param[in] handle for accessing SPI device 
 * @return SNS_SUCCESS if transaction is successful 
 * @note if buffering is disabled, does nothing and returns 
 *       success
 */
SNS_QFPSD_UIMAGE_CODE sns_err_code_e sns_qfp_sd_spi_flush(
  sns_qfp_sd_spi_handle_t handle);

/**
 * @brief enables buffering for SPI write-only transactions 
 *  
 * @param[in] handle for accessing SPI device 
 * @note when buffering is enabled, transactions are not sent 
 *       immediately to SPI device, but instead are accumulated
 *       in a buffer. When buffer fills up or flush is called,
 *       the accumulated data will be sent as a single SPI
 *       transaction. This can improve performance.
 */
void sns_qfp_sd_spi_enable_buffering(sns_qfp_sd_spi_handle_t handle);

/**
 * @brief disables buffering for SPI write-only transactions 
 *  
 * @param[in] handle for accessing SPI device 
 * @note if buffering was enabled, this call will flush the 
 *       buffer before disabling buffering, which will cause an
 *       SPI transaction.
 */
void sns_qfp_sd_spi_disable_buffering(sns_qfp_sd_spi_handle_t handle);

/**
 * @brief Set the clock frequency of the SPI device.
 *  
 * @param[in] handle for accessing SPI device
 * @param[in] freq frequency in Hz
 * 
 * @note The new frequency will take effect from next
 *       transaction
 */
sns_err_code_e sns_qfp_sd_spi_set_clk_freq(
  sns_qfp_sd_spi_handle_t handle, uint32_t freq);

/**
 * @brief Get the current clock frequency of the SPI device.
 *  
 * @param[in] handle for accessing SPI device
 * @param[out] freq will be filled with frequency in Hz
 * 
 */
sns_err_code_e sns_qfp_sd_spi_get_clk_freq(
  sns_qfp_sd_spi_handle_t handle, uint32_t* freq);

#endif // _SNS_QFP_SD_SPI_BASE_H

