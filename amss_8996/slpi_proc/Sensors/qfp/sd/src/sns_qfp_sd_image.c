/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains functions to read an image from the FP sensor over
    SPI bus.

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/src/sns_qfp_sd_image.c#2 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
03/22/16     GJU     Use consolidated SNS_OS_FREE rather than SNS_OS_U_FREE
11/03/15     ND      Modify sequencer start addresses for partial images
05/12/15     LD      Initial version based on OntarioImage.c
==============================================================================*/

/*-----------------------------------------------------------------------------
* Include files
* ---------------------------------------------------------------------------*/

// SLPI SPI driver does not change byte order when changing bits_per_word,
// so always use 8 bits here
#define ENCODING_8BIT_BPW 8 // 16
#define ENCODING_BLOCK_BPW 8 // 16

#include "sns_common.h"
#include "sns_memmgr.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_priv.h"
#include "sns_qfp_sd_image.h"
#include "sns_qfp_sd_spi_base.h"
#include "sns_qfp_sd_reg_rw.h"
#include "sns_qfp_sd_regmap.h"
#include "sns_qfp_sd_misc.h"

/*-----------------------------------------------------------------------------
* Types
* ---------------------------------------------------------------------------*/

/**
 * information for reading an image using a specific sequencer
 */
typedef struct {
  /* bytes per single block of image (FIFO size) */
  uint32_t bytes_per_block;
  /* sequencer start address */
  uint32_t start_address;
  /* Powerup delay after powering up Ontario modules.
     arbitrary units; will be translated linearly to some delay in the code.
     adjust by experiment (0 means no delay) */
  uint32_t powerup_delay;
  /* Startup delay after starting sequencer and before reading first block.
     arbitrary units; will be translated linearly to some delay in the code.
     adjust by experiment (0 means no delay) */
  uint32_t startup_delay;
  /* delay after reading a full block and before reading the next block.
     arbitrary units; will be translated linearly to some delay in the code.
     adjust by experiment (0 means no delay) */
  uint32_t block_delay;
} sns_qfp_sd_rid_sequencer_info_t;

/**
 * sequencer information for known sequencer types.
 * The sequencer is loaded by TrustZone and supports the image
 * types below
 */
// small image, 270 pixels
SNS_QFPSD_UIMAGE_DATA static sns_qfp_sd_rid_sequencer_info_t SEQUENCER_INFO_S1 = {
  8190, /* bytes per block */
  1014, /* start address */ // 12/22
  0, /* powerup delay */
  17, /* startup delay */
  0 /* block delay */
};

// small image, 1782 pixels
SNS_QFPSD_UIMAGE_DATA static sns_qfp_sd_rid_sequencer_info_t SEQUENCER_INFO_S2 = {
  8190, /* bytes per block */
  768, /* start address */ //12/22
  0, /* powerup delay */
  37, /* startup delay */
  0 /* block delay */
};

/** maximum number of parts (blocks) of image data */
#define MAX_IMAGE_PARTS 1

/** information about a single image part(block) */
typedef struct {
  /* pointer to image data for this part */
  uint8_t* buffer;
  /* part size in bytes */
  uint32_t size;
  /* FIFO read pointer after reading part, LSB (debugging only) */
  uint8_t* read_ptr_lsb;
  /* FIFO read pointer after reading part, MSB (debugging only) */
  uint8_t* read_ptr_msb;
  /* FIFO write pointer after reading part, LSB (debugging only) */
  uint8_t* write_ptr_lsb;
  /* FIFO write pointer after reading part, MSB (debugging only) */
  uint8_t* write_ptr_msb;
} image_part_info;

/**
 * size of scratch buffer for reading image data with a single
 * SPI transaction
 */
#define SPI_SCRATCH_BUFFER_SIZE 16000

/**
 * structure with context information needed for read image data
 */
typedef struct {
  /* scratch buffer for single block SPI transaction
     add 8 bytes for 64 bit alignment */
  uint8_t scratch[SPI_SCRATCH_BUFFER_SIZE+ALIGN64_SPARE];
  /* 64 bit aligned pointer to scratch buffer */
  uint8_t* buffer_start_ptr;
  /* total scratch buffer size */
  uint32_t buffer_total_size;
  /* buffer used amount in bytes */
  uint32_t buffer_used_size;
  /* information about image parts */
  image_part_info image_parts[MAX_IMAGE_PARTS];
  /* total parts in the read image */
  uint32_t total_parts;
} sns_qfp_sd_rid_context_t;

// Turn on this define to enable recording timers in the
// image.  By enabling this option, the first 16-bits of each
// image block will be replaced with a timer value that represents
// the number of micro-seconds it took to request and read the
// block.  Also, the values will be printed to the TZ log every so often.
//#define INSTRUMENTED_BLOCK_TIMERS 1

#ifdef INSTRUMENTED_BLOCK_TIMERS
static uint32 image_count = 1;
#endif


// Turn on this define to enable logging the Ontario FIFO
// read and write pointers while operating in Singleblock
// Image Read mode.  Turning this feature on for debugging is
// useful for debugging but can introduce artifacts into
// the image.
//#define LOG_READ_WRITE_POINTERS 1
// back-offset to the "data" field of 8 bit register read structure
#define READ_REG_8BIT_DATA_BACK_OFFSET  \
    (sizeof(sns_qfp_sd_8bit_read_t) - offsetof(sns_qfp_sd_8bit_read_t, data))

/*-----------------------------------------------------------------------------
* Private functions
* ---------------------------------------------------------------------------*/

/**
 * @brief append a 8 bit register read SPI transaction to the
 *        buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] address - register address
 *
 * @note *buff and *size are updated past the transaction
 *       written to the buffer.
 * @return sns_err_code_e
 */
/*
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_8bit_read(
  uint8_t** buff,
  uint32_t* size,
  uint32_t address)
{
  sns_qfp_sd_8bit_read_t* sptr;
  uint32_t amount;

  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  sptr = (sns_qfp_sd_8bit_read_t *)*buff;
  amount = sizeof(sns_qfp_sd_8bit_read_t);

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  sptr->opcode = 0x83;
  sptr->data_size = 0x02;
  sptr->dummy1 = 0x00;
  sptr->num_trailing_bytes = 0x01;
  sptr->address_byte0 = 0xFF & address;
  sptr->address_byte1 = 0xFF & (address >> 8);
  sptr->address_byte2 = 0xFF & (address >> 16);
  sptr->address_byte3 = 0xFF & (address >> 24);
  sptr->dummy2 = 0x00;
  sptr->dummy3 = 0x00;
  sptr->dummy4 = 0x00;
  sptr->dummy5 = 0x00;
  sptr->dummy6 = 0x00;
  sptr->data = 0x0;

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}
*/
/**
 * @brief append a 8 bit register write SPI transaction to the
 *        buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] address - register address
 * @param[in] data - data to write
 *
 * @note *buff and *size are updated past the transaction
 *       written to the buffer.
 * @return sns_err_code_e
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_8bit_write(
  uint8_t** buff,
  uint32_t* size,
  uint32_t address,
  uint8_t data)
{
  sns_qfp_sd_8bit_write_t* sptr;
  uint32_t amount;

  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  sptr = (sns_qfp_sd_8bit_write_t *)*buff;
  amount = sizeof(sns_qfp_sd_8bit_write_t);

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  sptr->opcode = 0x84;
  sptr->data_size = 0x2;
  sptr->dummy1 = 0x0;
  sptr->num_trailing_bytes = 0x1;
  sptr->address_byte0 = 0xFF & address;
  sptr->address_byte1 = 0xFF & (address>>8);
  sptr->address_byte2 = 0xFF & (address>>16);
  sptr->address_byte3 = 0xFF & (address>>24);
  sptr->data = data;
  sptr->trailing_dummy1 = 0x0;

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}

/**
 * @brief append a 32 bit register read SPI transaction to the
 *        buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] address - register address
 * @param[in] dummysize - number of trailing bytes
 *
 * @note *buff and *size are updated past the transaction
 *       written to the buffer.
 * @return sns_err_code_e
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_32bit_read(
  uint8_t** buff,
  uint32_t* size,
  uint32_t address,
  uint8_t dummysize)
{
  sns_qfp_sd_32bit_read_t* sptr;
  uint32_t amount;

  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  sptr = (sns_qfp_sd_32bit_read_t *)*buff;
  amount = sizeof(sns_qfp_sd_32bit_read_t);

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  sptr->opcode = 0x83;
  sptr->data_size = 0x00;
  sptr->dummy1 = 0x00;
  sptr->num_trailing_bytes = dummysize;
  sptr->address_byte0=0xFF & address;
  sptr->address_byte1=0xFF & (address>>8);
  sptr->address_byte2=0xFF & (address>>16);
  sptr->address_byte3=0xFF & (address>>24);
  sptr->dummy2 = 0x00;
  sptr->dummy3 = 0x00;
  sptr->dummy4 = 0x00;
  sptr->dummy5 = 0x00;
  sptr->data_byte0 = 0x0;
  sptr->data_byte1 = 0x0;
  sptr->data_byte2 = 0x0;
  sptr->data_byte3 = 0x0;

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}

/**
 * @brief append a 32 bit register write SPI transaction to the
 *        buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] address - register address
 * @param[in] data - data to write
 *
 * @note *buff and *size are updated past the transaction
 *       written to the buffer.
 * @return sns_err_code_e
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_32bit_write(
  uint8_t** buff,
  uint32_t* size,
  uint32_t address,
  uint32_t data)
{
  sns_qfp_sd_32bit_write_t* sptr;
  uint32_t amount;

  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  sptr = (sns_qfp_sd_32bit_write_t *)*buff;
  amount = sizeof(sns_qfp_sd_32bit_write_t);

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  sptr->opcode = 0x84;
  sptr->data_size = 0x00;
  sptr->dummy1 = 0x00;
  sptr->num_trailing_bytes = 0x00;
  sptr->address_byte0 = 0xFF & address;
  sptr->address_byte1 = 0xFF & (address>>8);
  sptr->address_byte2 = 0xFF & (address>>16);
  sptr->address_byte3 = 0xFF & (address>>24);
  sptr->data_byte0 = 0xFF & data;
  sptr->data_byte1 = 0xFF & (data>>8);
  sptr->data_byte2 = 0xFF & (data>>16);
  sptr->data_byte3 = 0xFF & (data>>24);

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}

/**
 * @brief append a block read SPI transaction to the
 *        buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] address - block address
 *
 * @note *buff and *size are updated past the transaction
 *       written to the buffer, but only past the block header,
 *       so the caller can record the start of the block data.
 *
 * @return sns_err_code_e
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_read_block(
  uint8** buff,
  uint32_t* size,
  uint32 address)
{
  sns_qfp_sd_block_read_header_t* sptr;
  uint32_t amount;

  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  sptr = (sns_qfp_sd_block_read_header_t *)*buff;
  amount = sizeof(sns_qfp_sd_block_read_header_t);

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  sptr->opcode = 0x86;
  sptr->data_size = 0x01;
  sptr->dummy1 = 0x00;
  sptr->num_trailing_bytes = 0x04;
  sptr->address_byte3 = 0x0;
  sptr->address_byte2 = 0x0;
  sptr->address_byte1 = 0x4;
  sptr->address_byte0 = 0x0;
  sptr->dummy1 = 0x0;
  sptr->dummy2 = 0x0;
  sptr->dummy3 = 0x0;
  sptr->dummy4 = 0x0;

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}

/**
 * @brief advance the SPI transaction scratch buffer
 * @param[in,out] buff - pointer to buffer
 * @param[in,out] size - remaining size in the buffer
 * @param[in] amount - amount to advance in bytes
 * @note call this after read block or read register with
 *       trailing bytes, to adjust the buffer past the whole
 *       transaction
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e buffer_advance(
  uint8** buff,
  uint32_t* size,
  uint32 amount)
{
  if (buff == NULL || size == NULL)
  {
    return SNS_ERR_BAD_PTR;
  }

  if (*size < amount)
  {
    SNS_QFP_PRINTF0(ERROR, "scratch buffer overflow");
    return SNS_ERR_FAILED;
  }

  *buff += amount;
  *size -= amount;
  return SNS_SUCCESS;
}

SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_extract_temperature_data(
   char *buffer, 
   char *temperature_data, 
   uint32 *temperature_data_size,
   uint32_t image_pixels
)
{ 
    int32 readingIndex;
    sns_err_code_e result = SNS_SUCCESS;   
    //int32_t i = 0;
    if(NULL == buffer || NULL == temperature_data || NULL == temperature_data_size)
    {
      SNS_QFP_PRINTF0(ERROR, "Invalid input to func extract temp data");    
      return SNS_ERR_BAD_PARM;
    }

    SNS_QFP_PRINTF1(LOW, "extracting temperature pixels; %d", image_pixels);

    for(readingIndex = 0; readingIndex < TEMPERATURE_DATA_SIZE; readingIndex++) 
    {
      *(temperature_data + readingIndex) = *(buffer + readingIndex + image_pixels * sizeof(uint16_t));
    }

#if (ENCODING_BLOCK_BPW == 16)
    // Swap the bytes for proper byte order
    for(int i = 0; i < TEMPERATURE_DATA_SIZE; i += 2)
    {
      char s, *bd;

      bd = temperature_data + i;
      s = *bd;

      *bd = *(bd + 1);
      *(bd + 1) = s;
    }
#endif

    *temperature_data_size = TEMPERATURE_DATA_SIZE;
    return result;
}

SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_calculate_temperature(
   char *temp_data, 
   int temp_size, 
   double slope, 
   double offset, 
   double *out_temp
)
{
    sns_err_code_e status = SNS_SUCCESS;    
    double code_bg, code_hbg, temp_single_bias, code_vdiode;

    SNS_QFP_PRINTF0(LOW, "calculating temperature");

    if (temp_size < TEMPERATURE_DATA_SIZE) {
        SNS_QFP_PRINTF2(ERROR, "temperature buffer too small (expected %d, actual %d)",
               TEMPERATURE_DATA_SIZE, temp_size);
        status = SNS_ERR_BAD_PARM;
        return status;
    }

    /*code_vdiode = Raw ADC reading when diode voltage is conncted to the ADC*/
    code_vdiode = (double) (temp_data[34] * 256 + temp_data[35]);
    /*code_bg = Raw ADC reading when VBG is connected to the ADC */	
    code_bg = (double) (temp_data[22] * 256 + temp_data[23]);
    /*code hbg = Raw ADC reading when VBG/2 connected to the ADC*/
    code_hbg = (double) (temp_data[10] * 256 + temp_data[11]);

    /* Equation: */
    /* voltage_diode = VGE * (1-((code_bg-code_vdiode)/2*(code_bg-code_hbg))*/
    /* temp_single_bias = (slope*voltage_diode + offset); */
    temp_single_bias = (double)(slope * ((double)1.25 * ((2 * (code_bg-code_hbg)) - 
                                   (code_bg-code_vdiode)))) / (2 * (code_bg-code_hbg)) + offset; 

    SNS_QFP_PRINTF3(LOW, "temp data: cvd:[%d] bg:[%d] hbg:[%d]",
                    (int) (code_vdiode * 1000),
                    (int) (code_bg * 1000),
                    (int) (code_hbg * 1000));
    
    SNS_QFP_PRINTF1(LOW, "temperature: t:[%d]", (int) (temp_single_bias * 1000));

    *out_temp = temp_single_bias;
    return status;
}

SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_get_temperature(
   char *buffer, 
   uint32_t image_pixels, 
   double *temperature
)
{    
    sns_err_code_e result = SNS_SUCCESS;    
    char temperature_data[TEMPERATURE_DATA_SIZE];
    uint32 temperature_data_size;

    SNS_QFP_PRINTF0(LOW, "getting temperature");

    // extract temperature buffer
    if (SNS_SUCCESS != (result = sns_qfp_sd_extract_temperature_data(buffer, temperature_data, 
                                                                     &temperature_data_size, image_pixels))) {
        SNS_QFP_PRINTF0(ERROR, "extracting temperature data");
        return SNS_ERR_FAILED;
    }

    // calculate temperature
    if ((result = sns_qfp_sd_calculate_temperature(temperature_data, temperature_data_size, TEMPERATURE_SLOPE, 
                                                   TEMPERATURE_OFFSET, temperature)) != SNS_SUCCESS) {
        SNS_QFP_PRINTF0(ERROR, "calculating temperature");
        return SNS_ERR_FAILED;
    }

    return result;
}

/**
 * @brief setup the SPI transaction for reading image data
 * @param[in,out] ctx - context that will be updated with setup
 * @param[in] sinfo - sequencer information for reading the
 *       image data
 * @param[in] image_size - size of requested image data in bytes
 * @return SNS_SUCCESS if successful
 *
 * @note the function fills the SPI scratch buffer with a single
 *       transaction with all commands for reading the image
 *       data, and updates other context variables to record the
 *       image.
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e setup_single_block_buffer(
  sns_qfp_sd_rid_context_t* ctx,
  sns_qfp_sd_rid_sequencer_info_t* sinfo,
  uint32_t image_size
)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint8_t extra_dummy = 254;
  uint32_t dummy_reads = 0, i, j;
  image_part_info *curr_image;
  uint8_t* buff;
  uint32_t size;

  // Looks like since we're using the same buffer for source and destination
  // for the SPI buffer, we can't re-use the previous SPI buffer.

  // compute number of image parts
  ctx->total_parts = (image_size / sinfo->bytes_per_block) +
      ((image_size % sinfo->bytes_per_block) ? 1 : 0);

  // Make sure we have enough space to store information about all the
  // image parts.
  if (ctx->total_parts > MAX_IMAGE_PARTS)
  {
    ctx->total_parts = 0;
    ctx->buffer_used_size = 0;
    return SNS_ERR_FAILED;
  }

  // Clear the initial SPI buffer to help reduce EMI issues on the
  // SPI bus (according to Systems)
  SNS_OS_MEMSET(ctx->buffer_start_ptr, 0, ctx->buffer_total_size);

  // setup SPI transaction
  buff = ctx->buffer_start_ptr;
  size = ctx->buffer_total_size;

  // Start the sequencer
  STATUS_CHECK(buffer_8bit_write(&buff, &size, START_SEQ, 1));

  // Dummy reads to let the sequencer run
  SNS_QFP_PRINTF1(LOW, "Startup delay for %d reads", sinfo->startup_delay);
  dummy_reads = sinfo->startup_delay;
  for (i = 0; i < dummy_reads; i++)
  {
    STATUS_CHECK(buffer_32bit_read(&buff, &size, SPI_SLAVE_SANITY,
                                   extra_dummy));
    STATUS_CHECK(buffer_advance(&buff, &size, extra_dummy));
  }

  // Start reading image blocks
  curr_image = &ctx->image_parts[0];
  curr_image->size = (image_size % sinfo->bytes_per_block);

  // Read a partial block first if there is one
  if (curr_image->size != 0)
  {
    // specify block read size
    STATUS_CHECK(buffer_32bit_write(&buff, &size, SPI_SLAVE_TRNS_LEN,
                                    curr_image->size));

    // read the data
    STATUS_CHECK(buffer_read_block(&buff, &size, 0x400));
    curr_image->buffer = buff;
    STATUS_CHECK(buffer_advance(&buff, &size, curr_image->size));

    // 8-bit reads for write/read pointers
#ifdef LOG_READ_WRITE_POINTERS
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_RD_PTR_LSB));
    curr_image->read_ptr_lsb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_RD_PTR_MSB));
    curr_image->read_ptr_msb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_WR_PTR_LSB));
    curr_image->write_ptr_lsb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_WR_PTR_MSB));
    curr_image->write_ptr_msb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
#endif

    curr_image++;
  }

  // Read blocks of image data using the full FIFO buffer
  for (i=0; i < (image_size / sinfo->bytes_per_block); i++)
  {
    // Dummy reads to let the sequencer run
    dummy_reads = sinfo->block_delay;
    for (j = 0; j < dummy_reads; j++)
    {
      STATUS_CHECK(buffer_32bit_read(&buff, &size, SPI_SLAVE_SANITY,
                                     extra_dummy));
      STATUS_CHECK(buffer_advance(&buff, &size, extra_dummy));
    }

    // specify block read size
    curr_image->size = sinfo->bytes_per_block;
    STATUS_CHECK(buffer_32bit_write(&buff, &size,
                                    SPI_SLAVE_TRNS_LEN, curr_image->size));

    // read the data
    STATUS_CHECK(buffer_read_block(&buff, &size, 0x400));
    curr_image->buffer = buff;
    STATUS_CHECK(buffer_advance(&buff, &size, curr_image->size));

    // 8-bit reads for write/read FIFO pointers
#ifdef LOG_READ_WRITE_POINTERS
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_RD_PTR_LSB));
    curr_image->read_ptr_lsb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_RD_PTR_MSB));
    curr_image->read_ptr_msb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_WR_PTR_LSB));
    curr_image->write_ptr_lsb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
    STATUS_CHECK(buffer_8bit_read(&buff, &size, FIFO_WR_PTR_MSB));
    curr_image->write_ptr_msb = buff - READ_REG_8BIT_DATA_BACK_OFFSET;
#endif

    curr_image++;
  }

  ctx->buffer_used_size = (buff - ctx->buffer_start_ptr);

End:
  return status;
}

/**
 * @brief Transfer image from SPI scratch buffer to final
 *        destination. Swap bytes if needed according to bits
 *        per word setting
 * @param[out] dest destination buffer
 * @param[in] dest_size size of destination buffer in bytes
 * @param[in] src pointer to image data inside scratch buffer
 * @param[in] amount amount of bytes to transfer from scratch
 *       buffer
 */
SNS_QFPSD_UIMAGE_CODE static void image_xfer(
  uint8_t* dest,
  uint32_t dest_size,
  uint8_t* src,
  uint32_t amount)
{
  uint8_t *bd, *bs;
  uint32_t i;

  // make sure we're transfering something
  if (amount < 1)
  {
    return;
  }

  // make sure we have enough room at the destination
  if (amount > dest_size)
  {
    amount = dest_size;
  }

  // make sure we an even number of bytes
  if ((amount & 1) != 0)
  {
    amount -= 1;
  }

#if (ENCODING_BLOCK_BPW == 16)
  SNS_OS_MEMSCPY(dest, dest_size, src, amount);
#else
  // transfer the data, swapping every other byte
  for(i = 0; i < amount; i+= 2)
  {
    bd = dest + i;
    bs = src + i;

    *bd = *(bs + 1);
    *(bd + 1) = *bs;
  }
#endif
}

/**
 * @brief Setup the FP sensor for image data read.
 * @param[in] spi_handle handle for accessing SPI device
 * @param[in] sinfo sequencer information
 *
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_rid_setup(
  sns_qfp_sd_rid_context_t* ctx,
  sns_qfp_sd_spi_handle_t spi_handle,
  sns_qfp_sd_rid_sequencer_info_t* sinfo
)
{
  sns_err_code_e status = SNS_SUCCESS;

  // Set sequencer program start point
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, SEQ_CTL_2,
                                      (sinfo->start_address & 0xff)));
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, SEQ_CTL_3,
                                      ((sinfo->start_address >> 8) & 0x7)));

  // Clear FIFOs
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, FIFO_MGR_CTL, 3));
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, FIFO_MGR_CTL, 1));
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, FIFO_MGR_CTL, 0));
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, FIFO_MGR_CTL, 2));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

End:
  return status;
}

/**
 * @brief Tear down the FP sensor after image data read.
 * @param[in] spi_handle handle for accessing SPI device
 * @param[in] sinfo sequencer information
 *
 * @return SNS_SUCCESS if successful
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_rid_teardown(
  sns_qfp_sd_spi_handle_t spi_handle,
  sns_qfp_sd_rid_sequencer_info_t* sinfo
)
{
  sns_err_code_e status = SNS_SUCCESS;

  // Turn off temperature sensor module
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, TS_REG3, 0x0));

  // Clear interrupts
  STATUS_CHECK(sns_qfp_sd_write_8bits(spi_handle, INTR_CLR, 0x10));

  STATUS_CHECK(sns_qfp_sd_spi_flush(spi_handle));

End:
  return status;
}

/**
 *  @brief Perform a single block image data read
 *
 *  @param[in] ctx context for read image data operation
 *  @param[in] spi_handle handle for accessing SPI device
 *  @param[in] sinfo sequencer information
 *  @param[in] image_pixels requested image size in pixels
 *  @param[out] buffer where raw image data will be stored
 *  @param[in] buffer_size size of buffer in bytes
 *
 *  @return SNS_SUCCESS if completed successfully
 *  @note image_size must be one of the supported image sizes
 */
SNS_QFPSD_UIMAGE_CODE static sns_err_code_e sns_qfp_sd_single_block_image_read(
  sns_qfp_sd_rid_context_t* ctx,
  sns_qfp_sd_spi_handle_t spi_handle,
  sns_qfp_sd_rid_sequencer_info_t* sinfo,
  uint32_t image_pixels,
  uint8_t* buffer,
  uint32_t buffer_size
)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint32_t image_size, i, total_bytes;
  uint8_t* image;
  image_part_info* image_part;
  double temperature;

#ifdef INSTRUMENTED_BLOCK_TIMERS
  uint32_t last_timer_val;
  uint32_t start_frame, end_frame;
  uint32_t diff_frame;
  int32_t do_frame;
#endif


#ifdef INSTRUMENTED_BLOCK_TIMERS
  image_count++;
  do_frame = (image_count % 100 == 0);
  start_frame = sns_em_get_timestamp();
#endif

  status = sns_qfp_sd_rid_setup(ctx, spi_handle, sinfo);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "read_single_block: fail to setup sensor: %d",
                    (int)status);
    return status;
  }

  // sensor returns 16 bit image pixels
  image_size = image_pixels * sizeof(uint16_t) + TEMPERATURE_DATA_SIZE;

  // Set up the image read commands
  status = setup_single_block_buffer(ctx, sinfo, image_size);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "read_single_block: fail to setup buffer: %d",
                    (int)status);
    return status;
  }

  // Execute the Image Read
  status = spi_qfp_sd_spi_full_duplex(spi_handle, ctx->buffer_start_ptr,
                                   ctx->buffer_used_size);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "read_single_block: SPI transaction failed: %d",
                    (int)status);
    return status;
  }

#ifdef INSTRUMENTED_BLOCK_TIMERS
  end_frame = sns_em_get_timestamp();
  diff_frame = sns_em_convert_dspstick_to_usec(end_frame - start_frame);

  if (do_frame)
  {
    SNS_QFP_PRINTF1(ERROR, "Frame Image Timer Diff = %d", (int)diff_frame);
  }
#endif

  sns_qfp_sd_get_temperature((char *)ctx->image_parts[0].buffer, image_pixels, &temperature);

  // copy image data in parts
  image = buffer;
  total_bytes = 0;
  for (i=0, image_part = &ctx->image_parts[0]; i < ctx->total_parts;
        i++, image_part++)
  {
#ifdef LOG_READ_WRITE_POINTERS
    uint16_t read_ptr = 0, write_ptr = 0;
    uint8_t read_lsb, read_msb, write_lsb, write_msb;
#endif

    // Transfer image data
    image_xfer(image, buffer_size - total_bytes, image_part->buffer,
               image_part->size);
    image += image_part->size;
    total_bytes += image_part->size;

#ifdef LOG_READ_WRITE_POINTERS
    /* Log the read/write FIFO pointers */
    if ((image_part->read_ptr_lsb) &&
        (image_part->read_ptr_msb) &&
        (image_part->write_ptr_lsb) &&
        (image_part->write_ptr_msb))
    {
      read_lsb = *(image_part->read_ptr_lsb);
      read_msb = *(image_part->read_ptr_msb);
      write_lsb = *(image_part->write_ptr_lsb);
      write_msb = *(image_part->write_ptr_msb);

      read_ptr = ((uint16_t)(read_msb) << 8) | read_lsb;
      write_ptr = ((uint16_t)(write_msb) << 8) | write_lsb;

      SNS_QFP_PRINTF4(HIGH,
         "read_single_block: Copied image part %d, size %d, rPt = %d, wPt = %d",
         i, image_part->size, read_ptr, write_ptr);
    }
#endif
  }

  // Tear down the FP sensor
  status = sns_qfp_sd_rid_teardown(spi_handle, sinfo);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR,
                    "read_single_block: fail to teardown sensor: %d",
                    (int)status);
    return status;
  }

  return status;
}

/*-----------------------------------------------------------------------------
* Exported functions implementation
* ---------------------------------------------------------------------------*/
sns_err_code_e sns_qfp_sd_rid_alloc_handle(
  sns_qfp_sd_rid_handle_t* handle
)
{
  sns_qfp_sd_rid_context_t* ctx;

  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "rid_alloc_handle: NULL handle");
    return SNS_ERR_BAD_PTR;
  }

  *handle = SNS_OS_U_MALLOC(SNS_DBG_MOD_QFP,
                          sizeof(sns_qfp_sd_rid_context_t));
  if (*handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "rid_alloc_handle: failed to allocate");
    return SNS_ERR_NOMEM;
  }

  ctx = (sns_qfp_sd_rid_context_t*)(*handle);
  SNS_OS_MEMSET(ctx, 0, sizeof(sns_qfp_sd_rid_context_t));
  ctx->buffer_start_ptr = (uint8_t*)ALIGN64(&ctx->scratch[0]);
  ctx->buffer_total_size = SPI_SCRATCH_BUFFER_SIZE;
  return SNS_SUCCESS;
}

void sns_qfp_sd_rid_free_handle(
  sns_qfp_sd_rid_handle_t handle
)
{
  if (handle == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "rid_free_handle: NULL handle");
    return;
  }

  SNS_OS_FREE(handle);
}

sns_err_code_e sns_qfp_sd_read_image_data(
  sns_qfp_sd_rid_handle_t rid_handle,
  sns_qfp_sd_spi_handle_t spi_handle,
  uint32_t image_pixels,
  uint8_t* buffer,
  uint32_t buffer_size,
  uint32_t* image_size
)
{
  sns_err_code_e status = SNS_SUCCESS;
  sns_qfp_sd_rid_context_t* ctx;
  sns_qfp_sd_rid_sequencer_info_t* sinfo;

  if (rid_handle == NULL || spi_handle == NULL || buffer == NULL ||
      image_size == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "read_image_data: bad argument(s)");
    return SNS_ERR_BAD_PTR;
  }

  ctx = (sns_qfp_sd_rid_context_t*)rid_handle;
  switch (image_pixels)
  {
  case QFP_SD_S1_IMAGE_SIZE:
    sinfo = &SEQUENCER_INFO_S1;
    break;
  case QFP_SD_S2_IMAGE_SIZE:
    sinfo = &SEQUENCER_INFO_S2;
    break;
  default:
    SNS_QFP_PRINTF1(ERROR, "read_image_data: unsupported image size: %d",
                    (int)image_pixels);
    return SNS_ERR_BAD_PARM;
  }

  *image_size = image_pixels * sizeof(uint16_t) + TEMPERATURE_DATA_SIZE;
  if (buffer_size < *image_size)
  {
    SNS_QFP_PRINTF2(ERROR, "read_image_data: buffer too small, need %d got %d",
                    (int)(*image_size), (int)buffer_size);
    return SNS_ERR_BUFFER;
  }

  status = sns_qfp_sd_single_block_image_read(ctx, spi_handle, sinfo,
                                              image_pixels, buffer, buffer_size);

  return status;
}

