/*==============================================================================

    S E N S O R S   FP SENSOR FINGER DETECT  SPI D R I V E R

DESCRIPTION

    This file contains functions to perform register and block read/writes
    from/to the FP sensor

Copyright (c) 2015 by QUALCOMM Technologies, Incorporated. 
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/qfp.slpi/1.0/sd/src/sns_qfp_sd_reg_rw.c#1 $


when         who     what, where, why
----------   ---     -----------------------------------------------------------
05/10/15     LD      Initial version based on OntarioRegReadWrite.c
==============================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "sns_common.h"
#include "sns_em.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_sd_reg_rw.h"

/*-------------------------------------------------------------------------
 * Externalized Function Implementations
 * ----------------------------------------------------------------------*/
sns_err_code_e sns_qfp_sd_read_8bits(sns_qfp_sd_spi_handle_t handle, 
                                     uint32_t address, uint8_t* data)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint8_t buff[sizeof(sns_qfp_sd_8bit_read_t) + 16];
  uint8_t *aptr = buff;
  sns_qfp_sd_8bit_read_t *pBuff;

  aptr = (uint8_t*)(((intptr_t)aptr + 0x7) & ~0x7); // 64 bit align
  pBuff = (sns_qfp_sd_8bit_read_t*)aptr;

  if (NULL == data) 
  {
    return SNS_ERR_BAD_PTR;
  }

  // flash transaction buffer before any read operation
  status = sns_qfp_sd_spi_flush(handle);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "call to flush failed: %d", (int)status);
    return status;
  }
	
  pBuff->opcode = 0x83;
  pBuff->data_size = 0x02;
  pBuff->dummy1 = 0x00;
  pBuff->num_trailing_bytes = 0x01;
  pBuff->address_byte0 = 0xFF & address;
  pBuff->address_byte1 = 0xFF & (address >> 8);
  pBuff->address_byte2 = 0xFF & (address >> 16);
  pBuff->address_byte3 = 0xFF & (address >> 24);
  pBuff->dummy2 = 0x00;
  pBuff->dummy3 = 0x00;
  pBuff->dummy4 = 0x00;
  pBuff->dummy5 = 0x00;    
  pBuff->dummy6 = 0x00;        
  pBuff->data = 0x0;

  status = spi_qfp_sd_spi_full_duplex(handle, (uint8_t*)pBuff, 
                                   sizeof(sns_qfp_sd_8bit_read_t));

  *data = pBuff->data;    
    
  return status;   
}


sns_err_code_e sns_qfp_sd_write_8bits(sns_qfp_sd_spi_handle_t handle, 
                                      uint32_t address, uint8_t data)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint8_t buff[sizeof(sns_qfp_sd_8bit_write_t) + 8];
  uint8_t *aptr = buff;
  sns_qfp_sd_8bit_write_t *pBuff;

  aptr = (uint8_t*)(((intptr_t)aptr + 0x7) & ~0x7); // 64 bit align
  pBuff = (sns_qfp_sd_8bit_write_t*)aptr;

  pBuff->opcode = 0x84;
  pBuff->data_size = 0x2;
  pBuff->dummy1 = 0x0;
  pBuff->num_trailing_bytes = 0x1;
  pBuff->address_byte0 = 0xFF & address;
  pBuff->address_byte1 = 0xFF & (address>>8);
  pBuff->address_byte2 = 0xFF & (address>>16);
  pBuff->address_byte3 = 0xFF & (address>>24);
  pBuff->data = data;
  pBuff->trailing_dummy1 = 0x0;
  status = spi_qfp_sd_spi_transfer(handle, (uint8_t*)pBuff, 
                                   sizeof(sns_qfp_sd_8bit_write_t));
  return status;
}

sns_err_code_e sns_qfp_sd_read_32bits(sns_qfp_sd_spi_handle_t handle, 
                                     uint32_t address, uint32_t* data)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint8_t buff[sizeof(sns_qfp_sd_32bit_read_t) + 8];
  uint8_t *aptr = buff;
  sns_qfp_sd_32bit_read_t *pBuff;

  aptr = (uint8_t*)(((intptr_t)aptr + 0x7) & ~0x7); // 64 bit align
  pBuff = (sns_qfp_sd_32bit_read_t*)aptr;

  if(NULL == data) 
  {
    return SNS_ERR_BAD_PTR;
  }
   
  // flash transaction buffer before any read operation
  status = sns_qfp_sd_spi_flush(handle);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "call to flush failed: %d", (int)status);
    return status;
  }

  pBuff->opcode = 0x83;
  pBuff->data_size = 0x00;
  pBuff->dummy1 = 0x00;
  pBuff->num_trailing_bytes = 0x04;
  pBuff->address_byte0=0xFF & address;
  pBuff->address_byte1=0xFF & (address>>8);
  pBuff->address_byte2=0xFF & (address>>16);
  pBuff->address_byte3=0xFF & (address>>24);
  pBuff->dummy2 = 0x00;
  pBuff->dummy3 = 0x00;
  pBuff->dummy4 = 0x00;
  pBuff->dummy5 = 0x00;
  pBuff->data_byte0 = 0x0;
  pBuff->data_byte1 = 0x0;
  pBuff->data_byte2 = 0x0;
  pBuff->data_byte3 = 0x0;

  status = spi_qfp_sd_spi_full_duplex(handle, (uint8_t*)pBuff, 
                                   sizeof(sns_qfp_sd_32bit_read_t));
  // TODO fill output data
  return status;
}

sns_err_code_e sns_qfp_sd_write_32bits(sns_qfp_sd_spi_handle_t handle, 
                                      uint32_t address, uint32_t data)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint8_t buff[sizeof(sns_qfp_sd_32bit_write_t) + 8];
  uint8_t *aptr = buff;
  sns_qfp_sd_32bit_write_t *pBuff;

  aptr = (uint8_t*)(((intptr_t)aptr + 0x7) & ~0x7); // 64 bit align
  pBuff = (sns_qfp_sd_32bit_write_t*)aptr;

  pBuff->opcode = 0x84;
  pBuff->data_size = 0x00;
  pBuff->dummy1 = 0x00;
  pBuff->num_trailing_bytes = 0x00;
  pBuff->address_byte0 = 0xFF & address;
  pBuff->address_byte1 = 0xFF & (address>>8);
  pBuff->address_byte2 = 0xFF & (address>>16);
  pBuff->address_byte3 = 0xFF & (address>>24);
  pBuff->data_byte0 = 0xFF & data;
  pBuff->data_byte1 = 0xFF & (data>>8);
  pBuff->data_byte2 = 0xFF & (data>>16);
  pBuff->data_byte3 = 0xFF & (data>>24);

  status = spi_qfp_sd_spi_transfer(handle, (uint8_t*)pBuff, 
                                   sizeof(sns_qfp_sd_32bit_write_t));
   
  return status;
}


sns_err_code_e sns_qfp_sd_read_block(sns_qfp_sd_spi_handle_t handle,
                                     uint8_t* address, uint32_t size)
{
  uint32_t before, after; // DEBUGGING
  uint32_t delta;
  sns_err_code_e status = SNS_SUCCESS;

  sns_qfp_sd_block_read_header_t* header;

  if(NULL == address) 
  {
    return SNS_ERR_BAD_PTR;
  }

  // flash transaction buffer before any read operation
  status = sns_qfp_sd_spi_flush(handle);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "call to flush failed: %d", (int)status);
    return status;
  }

  header = (sns_qfp_sd_block_read_header_t *)address;
  header->opcode = 0x86;
  header->data_size = 0x01;
  header->dummy1 = 0x00;
  header->num_trailing_bytes = 0x04;
  header->address_byte3 = 0x0;
  header->address_byte2 = 0x0;
  header->address_byte1 = 0x4;
  header->address_byte0 = 0x0;
  header->dummy1 = 0x0;
  header->dummy2 = 0x0;
  header->dummy3 = 0x0;
  header->dummy4 = 0x0;   
   
  before = sns_em_get_timestamp();
  status = spi_qfp_sd_spi_full_duplex(handle, address, 
                      size + sizeof(sns_qfp_sd_block_read_header_t));
  after = sns_ddf_get_timestamp();
  delta = after - before;
  SNS_QFP_PRINTF2(HIGH, "read block size %d run time: %d", (int) size, 
          (int) sns_em_convert_dspstick_to_usec(delta));
   
  return status;   
}

