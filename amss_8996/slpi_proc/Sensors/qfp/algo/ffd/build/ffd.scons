#===============================================================================
#
# Fast Finger Detection algorithm Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/qfp.slpi/1.0/algo/ffd/build/ffd.scons#1 $
#  $DateTime: 2016/02/25 10:35:39 $
#  $Author: kshyamas $
#  $Change: 9958535 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 2015-08-09  LD     Initial version
#
#===============================================================================
Import('env')
from glob import glob
from os.path import join, basename
env = env.Clone()
env.Append(CCFLAGS = " -mllvm -diff-and-merge-extract-regions=0 ")

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/Sensors/qfp/algo/ffd/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within Sensors
#-------------------------------------------------------------------------------
SENSORS_API = [
   'SNS_MEMMGR',
   'SNS_COMMON',
]

env.RequireRestrictedApi(SENSORS_API)

#-------------------------------------------------------------------------------
# Internal depends within module
#-------------------------------------------------------------------------------
env.Append(CPPPATH = [
   "${BUILD_ROOT}/core/api/power/uSleep",
   "${BUILD_ROOT}/Sensors/api",
   "${BUILD_ROOT}/Sensors/common/inc",
   "${BUILD_ROOT}/Sensors/common/util/memmgr/inc",
   "${BUILD_ROOT}/Sensors/common/smr/inc",
   "${BUILD_ROOT}/Sensors/pm/inc",
   "${BUILD_ROOT}/Sensors/qfp/framework/inc",
   "${BUILD_ROOT}/Sensors/qfp/algo/common/inc",
   "${BUILD_ROOT}/Sensors/qfp/algo/ffd/inc",
   "${BUILD_ROOT}/core/buses/api/spi/",
   "${BUILD_ROOT}/Sensors/qfp/sd/inc"
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
FFD_SOURCES = [
   ]

FFD_U_SOURCES = [
    '${BUILDPATH}/ffd_api.c',
    '${BUILDPATH}/ffd_algo.c',
   ]

algo_ffd_lib = env.AddBinaryLibrary(
  ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
  '${BUILDPATH}/qfp_algo_ffd', FFD_SOURCES)
algo_ffd_u_lib = env.AddBinaryLibrary(
  ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
  '${BUILDPATH}/qfp_algo_u_ffd', FFD_U_SOURCES)


# Sources and headers that should not be shared
FFD_CLEAN_SOURCES = env.FindFiles(['*.c'], '${BUILD_ROOT}/Sensors/qfp/algo/ffd/src')
#FFD_CLEAN_SOURCES += env.FindFiles(['*.h'], '${BUILD_ROOT}/Sensors/qfp/algo/ffd/inc')
#FFD_CLEAN_SOURCES += env.FindFiles(['*.h'], '${BUILD_ROOT}/Sensors/qfp/algo/common')

# Clean sources
env.CleanPack(['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'], FFD_CLEAN_SOURCES)

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
if 'USES_ISLAND' in env:
   ffd_sections = ['.text.FFD', '.data.FFD']
   env.AddIslandLibrary(['CORE_QDSP6_SENSOR_SW'], algo_ffd_u_lib, ffd_sections)

