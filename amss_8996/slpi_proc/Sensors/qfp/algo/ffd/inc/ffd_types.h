#ifndef INC_QUALCOMM_FFD_TYPES_H
#define INC_QUALCOMM_FFD_TYPES_H
/*=============================================================================
  @file ffd_types

  Type definitions for fast finger detection algorithm

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/algo/ffd/inc/ffd_types.h#1 $ */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-16  RD   Memory optimizations
  2015-10-28  ND   Low-power wake-up
  2015-08-09  LD   Initial version

=============================================================================*/ 
#include "qfp_algo_common.h"

#define FFD_MAX_VECTOR_SIZE         1782
#define FFD_MAX_SMALL_IMAGE_WIDTH   54
#define FFD_ALGO_ON_OFF             0
#define FFD_ALGO_OFF_ONLY           1
#define BGE_FACTOR                  127
#define HOME_ENERGY_HISTORY         4

/*
 * Define max/min
 */
#define max(a, b) ((a)>(b) ? (a) : (b))
#define min(a, b) ((a)<(b) ? (a) : (b))

/*
 * Types of fgon/fgoff images to be captured
 */
typedef enum ffd_image_on_off_e
{
  FFD_IMAGE_ON_OFF,
  FFD_IMAGE_ON_ONLY,
  FFD_IMAGE_OFF_ONLY,
} ffd_image_on_off_e;

/*
 * Types of ffd on/off images processing
 */
typedef enum ffd_proc_on_off_e
{
  FFD_PROC_ON_OFF,
  FFD_PROC_ON_ONLY,
  FFD_PROC_OFF_ONLY,
} ffd_proc_on_off_e;

/*
 * Types of ffd processing
 */
typedef enum ffd_proc_type_e
{
  FFD_PROC_WAKE_UP,
  FFD_PROC_HOME,
} ffd_proc_type_e;

/*
 * Types of input and calculations
 */
typedef int8_t ffd_algo_basis_data_t;
typedef int16_t ffd_algo_in_data_t;
typedef int32_t ffd_algo_mid_data_t;

typedef struct
{
  float factor;
  float offset;
} norm_str;

/*
 * Basis data configuration structure
*/
typedef struct
{
  ffd_algo_basis_data_t *data;
  norm_str *norm_vector;
  uint32_t num_vectors;
  uint32_t vector_size;
} ffd_basis_config_t;

/*
 * FastFingerDetection module initialization configuration
 */ 
typedef struct 
{
  uint32_t s1_disabled;
  ffd_basis_config_t basis_s1;
  ffd_basis_config_t basis_s1_off;
  ffd_basis_config_t basis_s2;
  ffd_basis_config_t basis_s2_off;
  float32_t threshold_s1;
  float32_t threshold_s1_off;
  float32_t threshold_s2;
  float32_t threshold_s2_off;
  float32_t threshold_s1_rubbing;
  float32_t threshold_s2_home_touch;
  float32_t threshold_s2_home_lift;
  uint32_t num_image_runs_s1;
  uint32_t fgoff_dilution_s1;
  uint32_t fgoff_dilution_home;
  uint32_t enable_island_mode;
} ffd_init_config_t;

/* 
 * FastFingerDetection processing input structure.
 *
 * width/height       The width and height of the input image.
 *
 * on, off            The input images (fg_on, fg_off)
 *
 */
typedef struct
{
  uint32_t              width;
  uint32_t              height;
  uint16_t              *on;
  uint16_t              *off;
  ffd_proc_on_off_e     ffd_proc_on_off;
  ffd_proc_type_e       ffd_proc_type;
} ffd_proc_input_t;
 
/* 
 * FastFingerDetection processing output structure.
 *
 */
typedef struct
{
  int32_t               result;
  float32_t             energy;
  float32_t             energy_off;
} ffd_proc_output_t;


/*
 * Represent a handle to the Fingerprint Detector module
 */
typedef struct
{
  ffd_init_config_t config;
  ffd_algo_in_data_t f[FFD_MAX_VECTOR_SIZE];
  float32_t f_bnd[4];
  float32_t *test_sum;
  float32_t *offset;
  float32_t sum54[FFD_MAX_SMALL_IMAGE_WIDTH];
  float32_t sum5[FFD_MAX_SMALL_IMAGE_WIDTH];
  float32_t previous_s1_energy;
  float32_t previous_s1_energy_off;
  float32_t s2_home_energy_history[HOME_ENERGY_HISTORY];
  float32_t s2_home_energy_total;
  uint8_t s2_home_energy_history_index;
  uint8_t s2_home_energy_history_min_count;
} fast_finger_detect_t;

typedef fast_finger_detect_t* ffd_handle_t;

#endif /* INC_QUALCOMM_FFD_TYPES_H */
