#ifndef FFD_API_H
#define FFD_API_H
/*=============================================================================
  @file ffd_api

  API for the fast finger detection algorithm

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/algo/ffd/inc/ffd_api.h#1 $ */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-16  RD   Memory optimizations
  2016-02-04  lb   Added separate ffd_alloc function
  2015-08-09  ld   Initial version

=============================================================================*/
//#define SNS_QFP_TEST_Q6_SIMULATOR

#include "sns_common.h"
#include "qfp_algo_common.h"
#include "ffd_types.h"
#include "sns_qfp_priv.h"

#ifdef SNS_USES_ISLAND
  #define SNS_FFD_UIMAGE_CODE __attribute__((section (".text.FFD")))
  #define SNS_FFD_UIMAGE_DATA __attribute__((section (".data.FFD")))
#else
#define SNS_FFD_UIMAGE_CODE
#define SNS_FFD_UIMAGE_DATA
#endif /* SNS_USES_ISLAND */

/**
 * @brief Allocates memory required by the FFD algorithm 
 *        flow
 * @param[in,out] ctx QFP service context, will be updated 
 * @return SNS_SUCCESS if successful 
 * @note in case of failure, partial allocations will be freed.
 */
sns_err_code_e ffd_alg_alloc(sns_qfp_type_s* ctx);

/**
 * @brief Frees memory required by the FFD algorithm flow
 */
void ffd_alg_free(sns_qfp_type_s* ctx);

/**
 * @brief Initializes the FFD algorithm flow. Call this function
 *        when enabling the wakeup sensor
 * @param[in,out] ctx QFP service context, will be updated 
 * @return SNS_SUCCESS if successful 
 * @note in case of failure, partial allocations will be freed.
 */
sns_err_code_e ffd_alg_init(sns_qfp_type_s* ctx);

/**
 * @brief Deinitializes the FFD algorithm flow. 
 *        Call this function when disabling the wakeup sensor 
 */
void ffd_alg_deinit(sns_qfp_type_s* ctx);

/*!
 * @brief
 * Allocates memory needed for the fast finger detection algorithm.
 * This function must be called before calling ffd_open.
 *
 * @param[out] handle A handle to the FingerDetection instance
 *                    which will be allocated
 *
 * @retval  SNS_SUCCESS if successful, other value if error
 */
SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_alloc(
  ffd_handle_t* handle
);

/*!
 * @brief
 * Deallocates memory used by the fast finger detection algorithm.
 *
 * @param[in,out] handle A handle to the FingerDetection instance
 *                       which will be deallocated
 *
 * @retval  SNS_SUCCESS if successful, other value if error
 */
SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_dealloc(
  ffd_handle_t* handle
);

/*!
 * @brief
 * Initialize an instance of FastFingerDetection.  This function
 * initializes any processing configuration to default value.
 * This function must called only after calling ffd_alloc.
 * This function must be called before calling ffd_process.
 *
 * @param[in] handle A handle to the FingerDetection instance
 *
 * @param[in]  config  The initialization configuration for the module.
 *
 * @retval  SNS_SUCCESS if successful, other value if error
 */
SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_open(
  ffd_handle_t handle,
  ffd_init_config_t* config
);

/*!
 * @brief
 * Close down an instance of FastFingerDetection.
 *
 * @param[in] handle  A handle to the FingerDetection instance.  This handle
 *                     must have been initialized through ffd_open call.
 *
 * @retval SNS_SUCCESS if success, other value if failed
 */
SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_close(
  ffd_handle_t handle
);

/*!
 * @brief
 * Run the processing fo the FastFingerDetection module.
 * Detect a finger in the input image, fill in detection result
 *
 * @param[in] handle   A handle to the FastFingerDetection instance. This handle
 *                     must have been initialized through ffd_open call.
 *
 * @param[in] input Structure containing input data for processing
 * @param[out] output Structure with output (results) to be filled
 *
 * @retval SNS_SUCCESS if success, other value if failed
 */
SNS_FFD_UIMAGE_CODE sns_err_code_e ffd_process(
  ffd_handle_t handle, 
  ffd_proc_input_t* input,
  ffd_proc_output_t* output);

#endif /* FFD_API_H */

