/*=============================================================================
  @file sns_qfp_debug.c

  Implements debug and diagnostics messages for the QFP service
  For internal usage only, may be removed in production


  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/src/sns_qfp_debug.h#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2014-08-11  LD   Initial version

=============================================================================*/
#ifndef _SNS_QFP_DEBUG_H
#define _SNS_QFP_DEBUG_H

#include "sns_common.h"
#include "sns_qfp_priv.h"
#include "sns_qfp_v01.h"

/** 
 *  @brief Process a debug message for the QFP service
 *  
 *  @param[in] ctx QFP service context
 *  @param[in] req request structure
 *  @param[out] resp response structure
 *  
 *  @return SENSOR1_SUCCESS if completed successfully
 */
sensor1_error_e sns_qfp_process_debug_msg(
   sns_qfp_type_s* ctx,
   sns_qfp_debug_req_msg_v01* req,
   sns_qfp_debug_resp_msg_v01* resp
);

#endif // _SNS_QFP_DEBUG_H
