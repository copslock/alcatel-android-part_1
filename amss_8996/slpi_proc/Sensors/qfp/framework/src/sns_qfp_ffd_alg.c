/*=============================================================================
  @file sns_qfp_ffd_alg.c
 
  This module operates the fast finger detection algorithm flow. It reads one
  or more images from the FP sensor, runs the FFD algorithm and return finger
  detection result.

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/src/sns_qfp_ffd_alg.c#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-02-16  RD   Memory optimizations
  2016-02-04  LB   Added separate ffd_alloc function
  2016-02-03  LB   Fix continuous mode island mode voting
  2016-02-02  LB   Added separate algorithm init/deinit functions
  2015-10-28  ND   Low-power wake-up
  2014-08-16  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/
#include "sns_common.h"
#include "sns_em.h"
#include "sns_qfp_ffd_alg.h"
#include "sns_qfp_sm_priv.h"
#include "ffd_api.h"
#include "ffd_algo.h"
#include "sns_qfp_sd_image.h"
#include "sns_qfp_sd_misc.h"
#include "uSleep.h"

/*-----------------------------------------------------------------------------
* Definitions Files
* ---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
* Prototypes of private functions
* ---------------------------------------------------------------------------*/
SNS_QFP_UIMAGE_CODE sns_err_code_e sns_qfp_ffd_alg_scan_images(
  sns_qfp_type_s* ctx,
  uint32_t image_pixels,
  ffd_image_on_off_e image_on_off
);

SNS_QFP_UIMAGE_CODE static sns_err_code_e sns_qfp_ffd_alg_get_energy(
  sns_qfp_type_s* ctx,
  uint32_t width,
  uint32_t height,
  ffd_proc_on_off_e proc_on_off,
  ffd_proc_type_e proc_type,
  uint32_t* result
);

/*-----------------------------------------------------------------------------
* Implementation of exported functions
* ---------------------------------------------------------------------------*/

SNS_QFP_UIMAGE_CODE sns_err_code_e sns_qfp_ffd_alg_run(
  sns_qfp_type_s* ctx, 
  sns_qfp_ffd_alg_run_result_t* result)
{
  sns_err_code_e status = SNS_SUCCESS;
  uint32_t k, s1_result_off = 0, s1_result = 0, s2_result = 0;
  ffd_image_on_off_e image_on_off = FFD_IMAGE_ON_OFF;
  ffd_proc_on_off_e proc_on_off = FFD_PROC_ON_OFF;
  ffd_proc_type_e proc_type = FFD_PROC_WAKE_UP;
  uint32_t start_iteration_time, end_read_images_time, end_iteration_time;

  if (ctx == NULL || result == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "ctx or result NULL");
    return SNS_ERR_BAD_PTR;
  }

  if (ctx->ffd_handle == NULL || ctx->fg_on_buffer == NULL ||
      ctx->fg_off_buffer == NULL)
  {
    SNS_QFP_PRINTF0(ERROR, "ffd algorithm not properly enabled");
    return SNS_ERR_BAD_PTR;
  }

  result->result = 0;

  if (!ctx->ffd_config.s1_disabled)
  {
    SNS_QFP_PRINTF1(LOW,
                    "Wake-up 1st stage using 270 pixels sequencer, fgoff dilution counter %d",
                    ctx->fgoff_dilution_counter_s1);

    // fgoff dilution counter reached it limit --> process fgoff
    if (ctx->fgoff_dilution_counter_s1 <= 1)
    {
      SNS_QFP_PRINTF0(LOW, "Wake-up fgoff processing");

      // Timestamp: begin
      start_iteration_time = sns_em_get_timestamp();

      // scan fgoff image
      status = sns_qfp_ffd_alg_scan_images(ctx,
                                           FFD_PHASE1_IMAGE_SIZE,
                                           FFD_IMAGE_OFF_ONLY);
      if (status != SNS_SUCCESS)
      {
        SNS_QFP_PRINTF0(ERROR, "FAIL reading images");
        goto End;
      }

      // Timestamp: read images
      end_read_images_time = sns_em_get_timestamp();

      // Leave island mode if an exit request is pending
      if (uSleep_isExitRequested())
      {
        sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
      }

      // run the finger detection algorithm
      status = sns_qfp_ffd_alg_get_energy(ctx,
                                          FFD_PHASE1_IMAGE_WIDTH,
                                          FFD_PHASE1_IMAGE_HEIGHT,
                                          FFD_PROC_OFF_ONLY,
                                          FFD_PROC_WAKE_UP,
                                          &s1_result_off);
      if (status != SNS_SUCCESS)
      {
        SNS_QFP_PRINTF0(ERROR, "FAIL get s1_energy_off");
        goto End;
      }

      // Timestamp: end processing
      end_iteration_time = sns_em_get_timestamp();

      SNS_QFP_PRINTF3(LOW,
                      "s1 off finger detect result = %d, read image time = %dus, processing time = %dus",
                      (int) s1_result_off,
                      (int) sns_em_convert_dspstick_to_usec(end_read_images_time - start_iteration_time),
                      (int) sns_em_convert_dspstick_to_usec(end_iteration_time - end_read_images_time));

      if (s1_result_off != SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED)
      {
        // No finger detected --> stay in fgoff processing
        ctx->fgoff_dilution_counter_s1 = 1;
      }
      else
      {
        // Reset the fgoff dilution counter
        ctx->fgoff_dilution_counter_s1 = ctx->ffd_config.fgoff_dilution_s1;
      }
    }
    // fgoff dilution counter not reaching its limit --> continue with fgon
    else
    {
      s1_result_off = SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED;
    }

    // Continue processing fgon after passing fgoff processing
    if (s1_result_off == SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED)
    {
      image_on_off = FFD_IMAGE_ON_ONLY;
      proc_on_off = FFD_PROC_ON_ONLY;
      ctx->fgoff_dilution_counter_s1--;
  
      for (k = 0; k < ctx->ffd_config.num_image_runs_s1; k++)
      {
        // Leave island mode if an exit request is pending
        if (uSleep_isExitRequested())
        {
          sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
        }
  
        // Timestamp: begin
        start_iteration_time = sns_em_get_timestamp();
  
        // scan fgon, fgoff images
        status = sns_qfp_ffd_alg_scan_images(ctx,
                                             FFD_PHASE1_IMAGE_SIZE,
                                             image_on_off);
        if (status != SNS_SUCCESS)
        {
          SNS_QFP_PRINTF0(ERROR, "FAIL reading images");
          goto End;
        }
    
        // Timestamp: read images
        end_read_images_time = sns_em_get_timestamp();
  
        // Leave island mode if an exit request is pending
        if (uSleep_isExitRequested())
        {
          sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
        }
  
        // run the finger detection algorithm
        status = sns_qfp_ffd_alg_get_energy(ctx,
                                            FFD_PHASE1_IMAGE_WIDTH,
                                            FFD_PHASE1_IMAGE_HEIGHT,
                                            proc_on_off,
                                            FFD_PROC_WAKE_UP,
                                            &s1_result);
        if (status != SNS_SUCCESS)
        {
          SNS_QFP_PRINTF0(ERROR, "FAIL get s1_energy");
          goto End;
        }
    
        // Timestamp: end processing
        end_iteration_time = sns_em_get_timestamp();
    
        SNS_QFP_PRINTF4(HIGH,
                        "s1 finger detect result = %d, k = %d, read image time = %dus, processing time = %dus",
                        (int) s1_result,
                        (int) k,
                        (int) sns_em_convert_dspstick_to_usec(end_read_images_time - start_iteration_time),
                        (int) sns_em_convert_dspstick_to_usec(end_iteration_time - end_read_images_time));
    
        if (s1_result != SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED)
        {
          // no finger detected
          break;
        }
        else
        {
          // Finger detected --> scan fgon/fgoff and reset fgoff dilution counter
          image_on_off = FFD_IMAGE_ON_OFF;
          proc_on_off = FFD_PROC_ON_OFF;
          ctx->fgoff_dilution_counter_s1 = ctx->ffd_config.fgoff_dilution_s1;
        }
      }
    }
  }

  // Phase 2: using 1782 pixels image
  if (ctx->ffd_config.s1_disabled ||
      (s1_result == SNS_QFP_FFD_ALG_RUN_RESULT_FINGER_DETECTED))
  {
    // Leave island mode if an exit request is pending
    if (uSleep_isExitRequested())
    {
      sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
    }

    // Set to home-button processing type in case of continuous mode detection
    if (ctx->finger_detect_mode == SNS_QFP_FINGER_DETECT_MODE_CONTINUOUS_V01)
    {
      proc_type = FFD_PROC_HOME;
      proc_on_off = FFD_PROC_ON_ONLY;

      SNS_QFP_PRINTF1(LOW,
                      "Home-button using 1782 sequencer, fgoff dilution counter %d",
                      ctx->fgoff_dilution_counter_home);

      // Set image capture based-on fgoff dilution counter
      if (ctx->fgoff_dilution_counter_home == 1)
      {
        ctx->fgoff_dilution_counter_home = ctx->ffd_config.fgoff_dilution_home;
        image_on_off = FFD_IMAGE_ON_OFF;
      }
      else
      {
        ctx->fgoff_dilution_counter_home--;
        image_on_off = FFD_IMAGE_ON_ONLY;
      }
    }
    else
    {
      SNS_QFP_PRINTF0(LOW, "Wake-up 2nd stage using 1782 sequencer");

      image_on_off = FFD_IMAGE_ON_OFF;
      proc_type = FFD_PROC_WAKE_UP;
      proc_on_off = FFD_PROC_ON_OFF;
      // Set the wake-up fgoff dilution counter to 1 after a successful 270 pixel
      // finger detect to guarantee fgoff image capture on the next iteration
      ctx->fgoff_dilution_counter_s1 = 1;
    }

    // Timestamp: s2 start
    start_iteration_time = sns_em_get_timestamp();

    status = sns_qfp_ffd_alg_scan_images(ctx,
                                         FFD_PHASE2_IMAGE_SIZE,
                                         image_on_off);
    if (status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF0(ERROR, "FAIL reading images");
      goto End;
    }

    // Timestamp: read images
    end_read_images_time = sns_em_get_timestamp();

    // Leave island mode if an exit request is pending
    if (uSleep_isExitRequested())
    {
      sns_qfp_pm_vote(SNS_IMG_MODE_NOCLIENT);
    }

    status = sns_qfp_ffd_alg_get_energy(ctx,
                                        FFD_PHASE2_IMAGE_WIDTH,
                                        FFD_PHASE2_IMAGE_HEIGHT,
                                        proc_on_off,
                                        proc_type,
                                        &s2_result);

    if (status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF0(ERROR, "FAIL get s2_energy");
      goto End;
    }

    // Timestamp: end processing
    end_iteration_time = sns_em_get_timestamp();

    SNS_QFP_PRINTF3(HIGH,
                    "s2 finger detect result = %d, read image time = %dus, processing time = %dus",
                    (int) s2_result,
                    (int) sns_em_convert_dspstick_to_usec(end_read_images_time - start_iteration_time),
                    (int) sns_em_convert_dspstick_to_usec(end_iteration_time - end_read_images_time));
  }

  // Fill results
  result->result = s2_result;
End:
  return status;
}

/*-----------------------------------------------------------------------------
* Implementation of private functions
* ---------------------------------------------------------------------------*/
SNS_QFP_UIMAGE_CODE sns_err_code_e sns_qfp_ffd_alg_scan_images(
  sns_qfp_type_s* ctx,
  uint32_t image_pixels,
  ffd_image_on_off_e image_on_off
)
{
  sns_err_code_e status;
  uint32_t out_size;


  SNS_QFP_PRINTF1(LOW,
                  "enter sns_qfp_ffd_alg_scan_images, image_on_off = %d",
                  image_on_off);

  // Wakeup Ontario
  STATUS_CHECK(sns_qfp_sd_ontario_wakeup(ctx->spi_handle));

  if ((image_on_off == FFD_IMAGE_ON_OFF) ||
      (image_on_off == FFD_IMAGE_ON_ONLY))
  {
    STATUS_CHECK(sns_qfp_sd_set_tbenable(ctx->spi_handle, true));
  
    status = sns_qfp_sd_read_image_data(
                ctx->rid_handle, ctx->spi_handle, image_pixels,
                ctx->fg_on_buffer, FFD_MAX_IMAGE_SIZE_BYTES, &out_size);
  
    if (status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "fail to read fg_on image: %d", (int)status);
      goto End;
    }
  }
  else
  {
    SNS_QFP_PRINTF0(LOW, "skip image capture on");
  }

  if ((image_on_off == FFD_IMAGE_ON_OFF) ||
      (image_on_off == FFD_IMAGE_OFF_ONLY))
  {
    STATUS_CHECK(sns_qfp_sd_set_tbenable(ctx->spi_handle, false));

    status = sns_qfp_sd_read_image_data(
                ctx->rid_handle, ctx->spi_handle, image_pixels,
                ctx->fg_off_buffer, FFD_MAX_IMAGE_SIZE_BYTES, &out_size);

    if (status != SNS_SUCCESS)
    {
      SNS_QFP_PRINTF1(ERROR, "fail to read fg_off image: %d", (int)status);
      goto End;
    }
  }
  else
  {
    SNS_QFP_PRINTF0(LOW, "skip image capture off");
  }
End:
  // Put Ontario to idle mode
  sns_qfp_sd_ontario_idle(ctx->spi_handle);

  return status;
}

SNS_QFP_UIMAGE_CODE static sns_err_code_e sns_qfp_ffd_alg_get_energy(
  sns_qfp_type_s* ctx,
  uint32_t width,
  uint32_t height,
  ffd_proc_on_off_e proc_on_off,
  ffd_proc_type_e proc_type,
  uint32_t* result
)
{
  sns_err_code_e status;
  ffd_proc_input_t input;
  ffd_proc_output_t output;

  input.width = width;
  input.height = height;
  input.on = (uint16_t*)ctx->fg_on_buffer;
  input.off = (uint16_t*)ctx->fg_off_buffer;
  input.ffd_proc_on_off = proc_on_off;
  input.ffd_proc_type = proc_type;

  status = ffd_process(ctx->ffd_handle, &input, &output);
  if (status != SNS_SUCCESS)
  {
    SNS_QFP_PRINTF1(ERROR, "ffd_process call failed: %d", (int)status);
    goto End;
  }

  *result = (uint32_t)output.result;
End:
  return status;
}

