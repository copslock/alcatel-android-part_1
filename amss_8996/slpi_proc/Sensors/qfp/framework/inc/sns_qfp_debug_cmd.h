#ifndef SNS_QFP_DEBUG_CMD_H
#define SNS_QFP_DEBUG_CMD_H
/*=============================================================================
  @file sns_qfp_debug.h

  Definitions related to the debug command of the QFP service.
  Contains debug request IDs and request and response structures for
  debug requests. This header is shared between SLPI and HLOS

  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/inc/sns_qfp_debug_cmd.h#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2015-10-28  ND   Low-power wake-up
  2015-08-12  LD   Initial version

=============================================================================*/
#include "comdef.h"

/** debug request IDs */
typedef enum {
  /* read chip ID from the sensor */
  SNS_QFP_DEBUG_READ_CHIP_ID = 1,
  /* read an image from the sensor */
  SNS_QFP_DEBUG_READ_IMAGE = 2,
  /* perform an SPI speed test */
  SNS_QFP_DEBUG_SPI_TEST = 3,
  /* read images and run energy estimator */
  SNS_QFP_DEBUG_ENERGY_ESTIMATOR = 4,
} sns_qfp_debug_request_id_e;

/** response structure for SNS_QFP_DEBUG_READ_CHIP_ID */
typedef PACK(struct) {
  uint32_t chip_id;
} sns_qfp_debug_read_chip_id_resp_t;

/** request structure for SNS_QFP_DEBUG_READ_IMAGE */
typedef PACK(struct) {
  /* size of image in pixels. Must be one the support image sizes */
  uint32_t image_pixels;
  /* enable/disable tone burst (1 = enable 0 = disable) */
  uint32_t tb_enable;
} sns_qfp_debug_read_image_req_t;

/** response structure for SNS_QFP_DEBUG_READ_IMAGE */
typedef PACK(struct) {
  /* size of the image in bytes */
  uint32_t image_size;
  /* image bytes, variable size (fills the response structure) */
  uint8_t image_data[4];
} sns_qfp_debug_read_image_resp_t;

/** request structure for SNS_QFP_DEBUG_SPI_TEST */
typedef PACK(struct) {
  /* clock frequency to use in Hz */
  uint32_t freq;
  /* size of the test transfer in bytes */
  uint32_t size;
} sns_qfp_debug_spi_test_req_t;

/** response structure for SNS_QFP_DEBUG_SPI_TEST */
typedef PACK(struct) {
  /* amount of bytes transferred over SPI */
  uint32_t amount;
  /* transfer time in microseconds */
  uint32_t time;
} sns_qfp_debug_spi_test_resp_t;

/** request structure for SNS_QFP_DEBUG_ENERGY_ESTIMATOR */
typedef PACK(struct) {
  /* image size in pixels, Must be one of the supported image sizes */
  uint32_t image_pixels;
  /* debug stage */
  uint32_t debug_stage;
} sns_qfp_debug_energy_estimator_req_t;

/** response structure for SNS_QFP_DEBUG_ENERGY_ESTIMATOR */
typedef PACK(struct) {
  /* estimated energy */
  float energy;
  /* estimated OFF energy */
  float energy_off;
  /* result reported from energy estimator (1=finger detected) */
  uint32_t result;
  /* image size for verification */
  uint32_t image_size;
  /* image bytes, variable size (fills the response structure) */
  uint8_t fgon_fgoff_data[4];
} sns_qfp_debug_energy_estimator_resp_t;

#endif /* SNS_QFP_DEBUG_CMD_H */

