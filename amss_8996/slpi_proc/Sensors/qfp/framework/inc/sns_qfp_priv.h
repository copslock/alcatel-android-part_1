#ifndef SNS_QFP_PRIV_H
#define SNS_QFP_PRIV_H
/*=============================================================================
  @file sns_qfp_priv.h

  Header file for QFP service private data
 
  This header file contains data and functions provided by QFP service

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/inc/sns_qfp_priv.h#2 $  */
/* $DateTime: 2016/05/31 01:37:39 $ */
/* $Author: pwbldsvc $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order. 

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2016-04-15  ND   Add CBGE timer counter to check and trigger CBGE in APSS
  2016-02-03  LB   Fix continuous mode island mode voting
  2016-01-20  ND   Allocate maximum number of BGE basis vectors at initialization
  2015-07-05  LD   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/

#include "sns_common.h"
#include "sns_common_v01.h"
#include "sns_em.h"
#include "sns_qfp_v01.h"
#include "ffd_types.h"
#include "sns_qfp_sm_priv.h"
#include "sns_qfp_sd_spi_base.h"
#include "sns_qfp_sd_image.h"

/*-----------------------------------------------------------------------------
* Definitions for structures maintained by QFP
* ---------------------------------------------------------------------------*/
/** Maximum number of BGE basis to be allocated at initialization */
#define SNS_QFP_MAX_BG_NUM_VECTORS 15

/**
 * the states the service can be in
 */
typedef enum
{
  // INIT: service was just created
  SNS_QFP_STATE_INIT = 0,
  // BUSY: SPI device initialized, SPI device is in use externally.
  // SLPI cannot run the wakeup sensor in this state.
  SNS_QFP_STATE_BUSY,
  // IDLE: SPI device initialized, SPI device owned by SLPI,
  // wakeup sensor is disabled. Algorithm configuration can take place
  // in this state
  SNS_QFP_STATE_IDLE,
  // RUNNING: SPI device initialized and owned by SLPI, wakeup sensor
  // is running.
  SNS_QFP_STATE_RUNNING
} sns_qfp_state_e;

/**
   structure for tracking state of file upload
   (used mainly for basis data files
 */
typedef struct
{
  /* item id of basis file uploaded */
  uint32_t item_id;
  /* pointer to upload data (NULL if no upload in progress) */
  uint8_t* data;
  /* total size of data in bytes */
  uint32_t size;
  /* amount of bytes already uploaded */
  uint32_t uploaded;
} sns_qfp_upload_state_t;

/** 
* structure for keeping QFP internal data
*/
typedef struct
{
  /* current service state */
  sns_qfp_state_e state;
  /* handle for accessing SPI device */
  sns_qfp_sd_spi_handle_t spi_handle;
  /* handle for reading images from sensor */
  sns_qfp_sd_rid_handle_t rid_handle;
  /* current keep alive state */
  uint32_t keep_alive_enabled;
  /* current polling state while in wakeup mode. Can be 0/1 (low/high) */
  uint8_t current_poll_state;
  /* polling time between finger detect iterations, in milliseconds */
  uint32_t finger_detect_poll_time;
  /* polling time between finger detect iterations in milliseconds when GPIO is up */
  uint32_t finger_detect_low_poll_time;
  /* finger detect mode (one shot or continuous) */
  sns_qfp_finger_detect_mode_e_v01 finger_detect_mode;
  /* last finger detect result */
  bool last_finger_detect_result;
  /* in continuous mode, indicates whether finger detect result was sent to client */
  bool finger_detect_result_reported;
  /* connection handle for sending indications. Only valid
     when finger_detect_enabled is true */
  void* finger_detect_connection_handle;
  /* Finger detect timer object */
  sns_em_timer_obj_t  finger_detect_timer_obj;
  /* algorithm initial configuration */
  ffd_init_config_t ffd_config;
  /* handle to algorithm module */
  ffd_handle_t ffd_handle;
  /* buffer for fg_on image, needed by FFD algorithm */
  uint8_t* fg_on_buffer;
  /* buffer for fg_off image, needed by FFD algorithm */
  uint8_t* fg_off_buffer;
  /* tracks basis file uploads */
  sns_qfp_upload_state_t upload_state;
  /* Wake-up first stage FGOFF dilution counter */
  uint32_t fgoff_dilution_counter_s1;
  /* Home button FGOFF dilution counter */
  uint32_t fgoff_dilution_counter_home;
  /* Island mode status */
  sns_pm_img_mode_e island_mode_status;
  /* CBGE timer */
  uint32_t cbge_timer;
  /* CBGE timer counter */
  uint32_t cbge_timer_counter;
} sns_qfp_type_s;

/*-----------------------------------------------------------------------------
* sns_qfp_init
* ---------------------------------------------------------------------------*/
sns_err_code_e sns_qfp_init(sns_qfp_service* srv);

/*-----------------------------------------------------------------------------
* sns_qfp_proc_req
* ---------------------------------------------------------------------------*/
sns_err_code_e sns_qfp_proc_req(sns_qfp_service* srv,
                                sns_qfp_qmi_connection_s *connection_handle,
                                qmi_req_handle req_handle,
                                unsigned int msg_id,
                                void *req_c_struct,
                                unsigned int req_c_struct_len);

/*-----------------------------------------------------------------------------
* sns_qfp_proc_signals
*
* Check the signals received by QFP and process them accordingly
* ---------------------------------------------------------------------------*/
SNS_QFP_UIMAGE_CODE void sns_qfp_proc_signals(sns_qfp_service* srv, 
                                              OS_FLAGS sig_flags);
/*-----------------------------------------------------------------------------
* sns_qfp_pm_vote
* ---------------------------------------------------------------------------*/
SNS_QFP_UIMAGE_CODE void sns_qfp_pm_vote(sns_pm_img_mode_e img_mode);

#endif /* #ifndef SNS_QFP_PRIV_H */
