#ifndef SNS_QFP_SM_PRIV_H
#define SNS_QFP_SM_PRIV_H
/*=============================================================================
  @file sns_qfp_sm_priv.h

  QFP service manager private header

  This header file contains the data types, macros and constants used by
  QFP service manager
  This is a generic implementation similar to SSM, allowing multiple
  independent services to be hosted on the same thread, although initially
  we may only have one service.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

=============================================================================*/

/* $Header: //components/rel/qfp.slpi/1.0/framework/inc/sns_qfp_sm_priv.h#1 $  */
/* $DateTime: 2016/02/25 10:35:39 $ */
/* $Author: kshyamas $ */

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  when        who  what, where, why
  ----------  ---  ------------------------------------------------------------
  2015-07-05  ld   Initial version

=============================================================================*/


/*-----------------------------------------------------------------------------
* Include Files
* ---------------------------------------------------------------------------*/

#include "sns_common.h"
#include "sns_common_v01.h"
#include "sns_debug_api.h"
#include "sns_em.h"
#include "sns_debug_str.h"
#include "sns_pm.h"
#include "qmi_csi.h"

/*-----------------------------------------------------------------------------
* Definitions for QFP service thread
* ----------------------------------------------------------------------------*/

// QFP Max constants
#define SNS_QFP_MAX_CLIENT_CONNECTIONS 3
#define SNS_QFP_MAX_SERVICES    1

// Bitmasks for SSM signals
// QMI messages signal
#define SNS_QFP_QMI_WAIT_SIG            0x01
// Finger detect timer signal
#define SNS_QFP_FINGER_DETECT_SIG       0x02
// Polling rate changed signal
#define SNS_QFP_POLLING_RATE_SIG        0x04

/*-----------------------------------------------------------------------------
* Macros for QFP
* ---------------------------------------------------------------------------*/

// F3 messages
#if defined(SNS_QFP_TEST_Q6_SIMULATOR)
  #define SNS_QFP_PRINTF0(level,msg)          printf("\n"); printf(msg)
  #define SNS_QFP_PRINTF1(level,msg,p1)       printf("\n"); printf(msg,p1)
  #define SNS_QFP_PRINTF2(level,msg,p1,p2)    printf("\n"); printf(msg,p1,p2)
  #define SNS_QFP_PRINTF3(level,msg,p1,p2,p3) printf("\n"); printf(msg,p1,p2,p3)
  #define SNS_QFP_PRINTF4(level,msg,p1,p2,p3,p4) printf("\n"); printf(msg,p1,p2,p3,p4)
  #define SNS_QFP_PRINTF5(level,msg,p1,p2,p3,p4,p5) printf("\n"); printf(msg,p1,p2,p3,p4,p5)
#elif defined QDSP6
  #ifndef DBG_MEDIUM_PRIO
    #define DBG_MEDIUM_PRIO DBG_MED_PRIO
  #endif // DBG_MEDIUM_PRIO
  #define SNS_QFP_PRINTF0(level,msg)             SMGR_MSG_0(DBG_##level##_PRIO, msg)
  #define SNS_QFP_PRINTF1(level,msg,p1)          SMGR_MSG_1(DBG_##level##_PRIO, msg, p1)
  #define SNS_QFP_PRINTF2(level,msg,p1,p2)       SMGR_MSG_2(DBG_##level##_PRIO, msg, p1, p2)
  #define SNS_QFP_PRINTF3(level,msg,p1,p2,p3)    SMGR_MSG_3(DBG_##level##_PRIO, msg, p1, p2, p3)
  #define SNS_QFP_PRINTF4(level,msg,p1,p2,p3,p4) SMGR_MSG_4(DBG_##level##_PRIO, msg, p1, p2, p3, p4)
  #define SNS_QFP_PRINTF5(level,msg,p1,p2,p3,p4,p5) SMGR_MSG_5(DBG_##level##_PRIO, msg, p1, p2, p3, p4, p5)
#elif defined(ADSP_STANDALONE)
  #define SNS_QFP_PRINTF0(level,msg)          qurt_printf("\n"); qurt_printf(msg)
  #define SNS_QFP_PRINTF1(level,msg,p1)       qurt_printf("\n"); qurt_printf(msg,p1)
  #define SNS_QFP_PRINTF2(level,msg,p1,p2)    qurt_printf("\n"); qurt_printf(msg,p1,p2)
  #define SNS_QFP_PRINTF3(level,msg,p1,p2,p3) qurt_printf("\n"); qurt_printf(msg,p1,p2,p3)
  #define SNS_QFP_PRINTF4(level,msg,p1,p2,p3,p4) qurt_printf("\n"); qurt_printf(msg,p1,p2,p3,p4)
  #define SNS_QFP_PRINTF5(level,msg,p1,p2,p3,p4,p5) qurt_printf("\n"); qurt_printf(msg,p1,p2,p3,p4,p5)
#endif // QDSP6

#define QFP_BIT_TEST(f,b)   (((f) & (b)) == (b))

#ifdef SNS_USES_ISLAND
  #define SNS_QFP_UIMAGE_CODE __attribute__((section (".text.uQFP")))
  #define SNS_QFP_UIMAGE_DATA __attribute__((section (".data.uQFP")))
#else
#define SNS_QFP_UIMAGE_CODE
#define SNS_QFP_UIMAGE_DATA
#endif /* SNS_USES_ISLAND */

/** helper macro to run a function and check return code, jump in case of error
    assumes containing function has an "End" exit label and defines "status"
    local variable */
#define STATUS_CHECK(func) \
  status = (func); \
  if(status != SNS_SUCCESS) \
  { \
    goto End; \
  }

/** 64 bit alignment */
#define ALIGN64_SPARE   8
#define ALIGN64(x)  (((intptr_t)(x) + 7) & (~7))

/*-----------------------------------------------------------------------------
* Definitions for structures maintained by QFP
* ---------------------------------------------------------------------------*/

/**
*  Services contained in QFP
*/
typedef enum
{
  SNS_QFP_SRV_NONE = -1,
  SNS_QFP_SRV_MAIN = 0,
  /* Add more services here */
} sns_qfp_services_e;

/**
* QCSI Connection info used by services
*/
typedef struct
{
  qmi_client_handle    client_handle;
  bool                 connection_valid;
} sns_qfp_qmi_connection_s;

/**
* QCSI information used by QFP services
*/
typedef struct
{
  //Handles provided by QCSI framework
  sns_qfp_qmi_connection_s   connection_handle[SNS_QFP_MAX_CLIENT_CONNECTIONS];
  qmi_csi_os_params          os_params;
  sns_qfp_services_e         service;
  void                       *service_cookie_ptr;
  sns_err_code_e             (*service_disconnect_fcn)(void* service_cookie_ptr,
                                                       void* connection_handle);
  uint32_t                   service_status;
  qmi_csi_service_handle     service_handle;
  qmi_csi_options            service_options;
  bool                       service_valid;
  // private data, service-specific
  void*                      data;
} sns_qfp_service;

/**
* All the data required by the QFP service manager
*/
typedef struct
{
  // OS signals
  // Flags set outside QFP SM, to be consumed by QFP SM
  OS_FLAG_GRP *qfp_flag_grp;
  // Flags set by QFP SM to be consumed by QFP SM
  OS_FLAGS     qfp_flags;

  // Services
  sns_qfp_service qfp_service[SNS_QFP_MAX_SERVICES];
} sns_qfp_sm_type_s;

extern sns_qfp_sm_type_s sns_qfp;
extern sns_pm_handle_t sns_qfp_pm_handle;

#endif /*#ifndef SNS_QFP_SM_PRIV_H*/

