/*
#====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
#
#                     SCATTER LOADING DESCRIPTION FILE
#
#  Copyright (c) 2014 by QUALCOMM Incorporated. All Rights Reserved.
#
#  GENERAL DESCRIPTION
#
#  The scatter loading description file is used to define the RPM memory map.
#
#*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#define RPMSS_CODE_START      0x0
#define RPMSS_CODE_SIZE       0x28000           // 160KB
#define RPMSS_DATA_START      0x90000
#define RPMSS_DATA_SIZE       0x14000           // 80KB
#define DDR_CODE_RAM_SIZE     (0x6400) /*  25 kB */
#define RPMSS_CODE_END        (RPMSS_CODE_START + RPMSS_CODE_SIZE)

#define RPMSS_DATA_END        (RPMSS_DATA_START + RPMSS_DATA_SIZE)

#define CODE_RAM_SAVE_SIZE    0x200 //512 for static data. We can change this if there is more data to store
#define CODE_RAM_SAVE_START   (RPMSS_CODE_END - CODE_RAM_SAVE_SIZE)  //15KB for prioritized heap 

// ddr struct data section
#define DDR_STRUCT_DATA_END     RPMSS_DATA_END
#define DDR_STRUCT_DATA_SIZE    0x2000
#define DDR_STRUCT_DATA_START   (DDR_STRUCT_DATA_END - DDR_STRUCT_DATA_SIZE)

#define IMAGE_INFO_HEADER_SIZE  0x40
#define RAIL_RESIDENCIES_SIZE   0x100 //256 bytes
#define SLEEP_STATS_SIZE        0x60
#define CPR_STATS_SIZE          0x100 //This is the size on 8996.
#define STACK_SIZE              0xA00   //2.5K
#define STACK_OVERFLOW_SIZE     0x20
#define RPM_LOG_SIZE            0x2400
#define RPM_LOG_START           (DDR_STRUCT_DATA_START - RPM_LOG_SIZE)

/* 
#This needs to aligerd with image layout RPM_CPR_STATS_END
# Already we have compile assert added for this*/
#define RPMSS_DRAM_RW_DATA 0x90310

RPMSS_CODE RPMSS_CODE_START RPMSS_CODE_SIZE
{
  CODE_RAM RPMSS_CODE_START RPMSS_CODE_SIZE
  {
    startup.o(RPM_ENTRY, +FIRST)
    * (InRoot$$Sections)
    * (+RO-CODE)
    * (+RO-DATA)
#if defined DDR_ABORT_REG_DUMP
    *(ddr_abort_funcs)
#endif
  }

  // DDR code ram section, place at the end of code ram
  DDR_CODE_RAM +0x0 FIXED NOCOMPRESS DDR_CODE_RAM_SIZE 
  {
#if defined MSM8998_STUBS
    *(ddr_stub)
#else
    *rpm_proc/core/boot/ddr*.lib (+RO)
#endif
  }

  DDR_CODE_RAM_END (ImageBase(DDR_CODE_RAM) + DDR_CODE_RAM_SIZE) EMPTY 0x0
  {
  }

  // Reclaimed after all initialization done
  CODE_RAM_RECLAIM_POOL +0x0 FIXED NOCOMPRESS
  {
    *(pm_cram_reclaim_pool)
    *(clk_cram_reclaim_pool)
    *(icb_cram_reclaim_pool)
    *(qdss_cram_reclaim_pool)
    *(cpr_cram_reclaim_pool)
    *(rpmserver_cram_reclaim_pool)
    *(sleep_cram_reclaim_pool)
    *(mpm_cram_reclaim_pool)
    *(proxy_cram_reclaim_pool)
    *(glink_cram_reclaim_pool)
  }

  INITIAL_CODE_RAM_HEAP +0x0 EMPTY 0x0
  {
  }

  //static data for saving
  CODE_RAM_SAVE CODE_RAM_SAVE_START FIXED CODE_RAM_SAVE_SIZE
  {
    *(cram_save_pool)
  }

  CODE_RAM_SAVE_END ImageBase(CODE_RAM_SAVE)+CODE_RAM_SAVE_SIZE EMPTY 0x0
  {
  }
}



RPMSS_DATA RPMSS_DATA_START RPMSS_DATA_SIZE
{
  IMAGE_INFO_HEADER +0x0 EMPTY 0x0
  {
    // reserved space
  }

  RPM_IMAGE_ID ImageBase(IMAGE_INFO_HEADER)+IMAGE_INFO_HEADER_SIZE FIXED NOCOMPRESS ZEROPAD
  {
    oem_uuid.o (+RW)
    qc_version.o (+RW)
    oem_version.o (+RW)
  }

  RAIL_RESIDENCIES +0x0 ALIGN 4 EMPTY 0x0
  {
  }

  SLEEP_STATS ImageBase(RAIL_RESIDENCIES)+RAIL_RESIDENCIES_SIZE EMPTY 0x0
  {
  }

  CPR_STATS ImageBase(SLEEP_STATS)+SLEEP_STATS_SIZE EMPTY 0x0
  {
  }

  DATA_RAM ImageBase(CPR_STATS)+CPR_STATS_SIZE FIXED NOCOMPRESS ZEROPAD
  {
    * (+RW)
    * (+ZI)
  }

#ifdef MSM8996_IMAGE_LAYOUT
  8996_xml_DEVCONFIG_DATA +0x0 FIXED NOCOMPRESS ZEROPAD
  {
    8996_devcfg*.o (+RW,+ZI,+RO-DATA)
  }
#endif

#ifdef MSM8998_IMAGE_LAYOUT
  8998_xml_DEVCONFIG_DATA +0x0 FIXED NOCOMPRESS ZEROPAD
  {
#if defined MSM8998_STUBS
    *(icb_stub)
#else
    8998_devcfg*.o (+RW,+ZI,+RO-DATA)
#endif
  }
#endif

  DAL_CONFIG_SECTIONS +0x0 FIXED NOCOMPRESS ZEROPAD
  {
    //Store the DALConfigs here, we're going to copy the appropriate one out of there early in boot.
    DALConfig*.o (+RW,+ZI,+RO-DATA)
  }

  STACK_OVERFLOW +0x0 ALIGN 32 EMPTY 0x0
  {
  }

  STACK ImageBase(STACK_OVERFLOW)+STACK_OVERFLOW_SIZE EMPTY 0x0
  {
  }

  // Reclaimed during initialization (before clock_init)
  DATA_RAM_RECLAIM_POOL ImageBase(STACK)+STACK_SIZE FIXED NOCOMPRESS ZEROPAD
  {
    *(mpu_dram_reclaim_pool)
    *(pm_dram_reclaim_pool)
    *(railway_dram_reclaim_pool)
    *(swevent_dram_reclaim_pool)
    *(xpu_dram_reclaim_pool)
    *(mpm_dram_reclaim_pool)
    *(npa_dram_reclaim_pool)
    *(cpr_dram_reclaim_pool)
  }

  DATA_RAM_HEAP +0x0 ALIGN 4 EMPTY 0x0
  {
  }

  RPM_LOG RPM_LOG_START EMPTY 0x0
  {
  }

  // Reserve a 8KB empty block of memory for DDR STRUCT at the end of RPMSS_DATA section
  DDR_STRUCT_DATA  DDR_STRUCT_DATA_START  EMPTY  DDR_STRUCT_DATA_SIZE
  {
    // reserved space
  }
}

