#include "npa_dump_module.h"
#include "npa_dump.h"
#include "swevent.h"

#ifdef MISC_NPA_DUMP
extern char Image$$NPA_DUMP_DATA$$ZI$$Base[];
extern char Image$$NPA_DUMP_DATA$$ZI$$Limit[];
extern uint32 Image$$NPA_DUMP_DATA$$ZI$$Length;
#endif

void misc_npa_dump_module_apply_cb(const misc_callback_data *data)
{
#ifdef MISC_NPA_DUMP
    uint64 time = data->votes->NPA_dump_trigger_time;

    SWEVENT(MISC_NPA_DUMP_REQUEST, (uint32)time, (uint32)(time >> 32));

    npa_dump(Image$$NPA_DUMP_DATA$$ZI$$Base, (uint32)&Image$$NPA_DUMP_DATA$$ZI$$Length);

    SWEVENT(MISC_NPA_DUMP_COMPLETE);
#endif
}

void misc_npa_dump_module_init(void)
{
}

