#ifndef NPA_DUMP_MODULE_H
#define NPA_DUMP_MODULE_H

#include "misc_resource.h"

void misc_npa_dump_module_apply_cb(const misc_callback_data *data);

void misc_npa_dump_module_init(void);

#endif /* NPA_DUMP_MODULE_H */

