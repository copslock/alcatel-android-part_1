#include "misc_resource.h"
#include "NPA_DUMP/npa_dump_module.h"
#include "Chipinfo.h"

static const misc_module misc_modules[] =
{
    {
        .module_apply_client_vote = misc_npa_dump_module_apply_cb,
        .init = misc_npa_dump_module_init,
        .keys_mask = RPM_MISC_NPA_DUMP_MASK,
    },
};

static const misc_registration temp_config_data =
{
    .num_modules = sizeof(misc_modules)/sizeof(misc_module),
    .modules = misc_modules,
};

const misc_registration * MISC_CONFIG_DATA = &temp_config_data;


void misc_target_init(void)
{
}

