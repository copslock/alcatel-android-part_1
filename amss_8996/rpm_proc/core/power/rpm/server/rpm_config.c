/*===========================================================================

  Copyright (c) 2012-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "rpm_config.h"

static const unsigned SUPPORTED_CLASSES   = 35;

#if defined MSM8994INT_SUPPORTED_RESOURCES
static const unsigned SUPPORTED_RESOURCES = 140;
#else
static const unsigned SUPPORTED_RESOURCES = 167;
#endif /* MSM8994INT_SUPPORTED_RESOURCES */

//
// BEGIN config data; should migrate to the system enumeration data method
//
static SystemData temp_config_data =
{
    .num_ees = 5, // 4 EE's, [apps, modem, lpass, ssc, tz]

    .ees    = (EEData[] ) {
        [0] = {
            .remote_ss = "apss",
            .glink_fifo_sz = 1024,
            .ee_buflen = 256,
            .priority  = TASK_PRIORITY_APPS,
            .wakeupInt = (1 << 5) | (1 << 7),
            .spm       = {
                             .numCores = 1,
                             .bringupInts  = (unsigned[]) {  6 }, // ? Monaco
                             .bringupAcks  = (unsigned[]) { 17 }, // ? Monaco
                             .shutdownInts = (unsigned[]) { 14 }, // ? Monaco
                             .shutdownAcks = (unsigned[]) {  1 }, // ? Monaco
                         },
        },
        [1] = {
            .remote_ss = "mpss",
            .glink_fifo_sz = 1024,
            .ee_buflen = 1024,
            .priority  = TASK_PRIORITY_MODEM,
            .wakeupInt = (1 << 13) | (1 << 15),                   // not clear
            .spm       = {
                             .numCores = 1,
                             .bringupInts  = (unsigned[]) { 33 }, 
                             .bringupAcks  = (unsigned[]) { 15 }, 
                             .shutdownInts = (unsigned[]) { 43 }, 
                             .shutdownAcks = (unsigned[]) {  6 }, 
                         },
        },
        [2] = {
            .remote_ss = "lpass",
            .glink_fifo_sz = 1024,
            .ee_buflen = 256,
            .priority  = TASK_PRIORITY_QDSP,
            .wakeupInt = (1 << 9) | (1 << 11),
            .spm       = {
                             .numCores = 1,
                             .bringupInts  = (unsigned[]) { 19 },
                             .bringupAcks  = (unsigned[]) { 21 },
                             .shutdownInts = (unsigned[]) { 18 },
                             .shutdownAcks = (unsigned[]) {  5 },
                         },
        },
        [3] = {
            .remote_ss = "dsps",
            .glink_fifo_sz = 1024,
            .ee_buflen = 256,
            .priority  = TASK_PRIORITY_SSC,
            .wakeupInt = (1 << 25) | (1 << 27),
            .spm       = {
                             .numCores = 1,
                             .bringupInts  = (unsigned[]) { 56 },
                             .bringupAcks  = (unsigned[]) { 19 },
                             .shutdownInts = (unsigned[]) { 57 },
                             .shutdownAcks = (unsigned[]) {  3 },
                         },
        },
        [4] = {
            .remote_ss = "tz",
            .glink_fifo_sz = 1024,
            .ee_buflen = 256,
            .priority  = TASK_PRIORITY_TZ,
            .wakeupInt = 0,
            .spm       = {
                             .numCores = 0,
                             .bringupInts  = (unsigned[]) { 31 },
                             .bringupAcks  = (unsigned[]) { 23 },
                             .shutdownInts = (unsigned[]) { 30 },
                             .shutdownAcks = (unsigned[]) {  7 },
                         },
        },
    },

    .supported_classes   = SUPPORTED_CLASSES,
    .supported_resources = SUPPORTED_RESOURCES,
    .classes             = (ResourceClassData[SUPPORTED_CLASSES]) { 0 },
    .resources           = (ResourceData[SUPPORTED_RESOURCES])    { 0 },
    .resource_seeds      = (int16_t[SUPPORTED_RESOURCES])         { 0 },
};
//
// END config data
//

__attribute__((section("cram_save_pool")))
SystemData * const rpm = &temp_config_data;

