#ifndef __QDSS_HWIO_H__
#define __QDSS_HWIO_H__
/*
===========================================================================
*/
/**
  @file qdss_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    QDSS_QDSSCSR

  'Include' filters applied: QDSSCLKVOTE 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/power/rpm/inc/target/8996/qdss_hwio.h#1 $
  $DateTime: 2014/07/30 21:54:31 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"



/*----------------------------------------------------------------------------
 * MODULE: QDSS_QDSSCSR
 *--------------------------------------------------------------------------*/

#define QDSS_QDSSCSR_REG_BASE                                                            (QDSS_QDSS_APB_BASE      + 0x00001000)

#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_ADDR                                            (QDSS_QDSSCSR_REG_BASE      + 0x00000058)
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_RMSK                                            0xffffffff
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_IN          \
        in_dword_masked(HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_ADDR, HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_RMSK)
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_INM(m)      \
        in_dword_masked(HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_ADDR, m)
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_OUT(v)      \
        out_dword(HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_ADDR,v)
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_ADDR,m,v,HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_IN)
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_CLKVOTE_BMSK                                    0xffffffff
#define HWIO_QDSS_CS_QDSSCSR_QDSSCLKVOTE_CLKVOTE_SHFT                                           0x0


#endif /* __QDSS_HWIO_H__ */
