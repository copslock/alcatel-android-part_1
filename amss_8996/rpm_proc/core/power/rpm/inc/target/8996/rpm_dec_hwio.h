#ifndef __RPM_DEC_HWIO_H__
#define __RPM_DEC_HWIO_H__
/*
===========================================================================
*/
/**
  @file rpm_dec_hwio.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    RPM_DEC

  'Include' filters applied: PAGE_SELECT[RPM_DEC] 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/power/rpm/inc/target/8996/rpm_dec_hwio.h#1 $
  $DateTime: 2014/07/30 21:54:31 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"



/*----------------------------------------------------------------------------
 * MODULE: RPM_DEC
 *--------------------------------------------------------------------------*/

#define RPM_DEC_REG_BASE                                                    (RPM_BASE      + 0x00080000)

#define HWIO_RPM_PAGE_SELECT_ADDR                                           (RPM_DEC_REG_BASE      + 0x00000070)
#define HWIO_RPM_PAGE_SELECT_RMSK                                                 0x3f
#define HWIO_RPM_PAGE_SELECT_IN          \
        in_dword_masked(HWIO_RPM_PAGE_SELECT_ADDR, HWIO_RPM_PAGE_SELECT_RMSK)
#define HWIO_RPM_PAGE_SELECT_INM(m)      \
        in_dword_masked(HWIO_RPM_PAGE_SELECT_ADDR, m)
#define HWIO_RPM_PAGE_SELECT_OUT(v)      \
        out_dword(HWIO_RPM_PAGE_SELECT_ADDR,v)
#define HWIO_RPM_PAGE_SELECT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_PAGE_SELECT_ADDR,m,v,HWIO_RPM_PAGE_SELECT_IN)
#define HWIO_RPM_PAGE_SELECT_PAGE_SELECT_BMSK                                     0x3f
#define HWIO_RPM_PAGE_SELECT_PAGE_SELECT_SHFT                                      0x0


#endif /* __RPM_DEC_HWIO_H__ */
