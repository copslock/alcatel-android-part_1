#ifndef __MPM_PSHOLD_HWIO_H__
#define __MPM_PSHOLD_HWIO_H__
/*
===========================================================================
*/
/**
  @file mpm_pshold_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v3 [istari_v3.1.3_p3q2r26.0]
 
  This file contains HWIO register definitions for the following modules:
    MPM2_PSHOLD

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/power/mpm/hal/include/8996/mpm_pshold_hwio.h#1 $
  $DateTime: 2016/01/29 14:15:30 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: MPM2_PSHOLD
 *--------------------------------------------------------------------------*/

#define MPM2_PSHOLD_REG_BASE                                                    (MPM2_MPM_BASE      + 0x0000b000)

#define HWIO_MPM2_MPM_PS_HOLD_ADDR                                              (MPM2_PSHOLD_REG_BASE      + 0x00000000)
#define HWIO_MPM2_MPM_PS_HOLD_RMSK                                                     0x1
#define HWIO_MPM2_MPM_PS_HOLD_IN          \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR, HWIO_MPM2_MPM_PS_HOLD_RMSK)
#define HWIO_MPM2_MPM_PS_HOLD_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_PS_HOLD_ADDR, m)
#define HWIO_MPM2_MPM_PS_HOLD_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_PS_HOLD_ADDR,v)
#define HWIO_MPM2_MPM_PS_HOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_PS_HOLD_ADDR,m,v,HWIO_MPM2_MPM_PS_HOLD_IN)
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_BMSK                                              0x1
#define HWIO_MPM2_MPM_PS_HOLD_PSHOLD_SHFT                                              0x0

#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR                                (MPM2_PSHOLD_REG_BASE      + 0x00000004)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK                                       0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN          \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR, HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_RMSK)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR, m)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR,v)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_ADDR,m,v,HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_IN)
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_BMSK                 0x1
#define HWIO_MPM2_MPM_DDR_PHY_FREEZEIO_EBI1_DDR_PHY_FREEZEIO_EBI1_SHFT                 0x0

#define HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR                                        (MPM2_PSHOLD_REG_BASE      + 0x00000008)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR, HWIO_MPM2_MPM_SSCAON_CONFIG_RMSK)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR, m)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUT(v)      \
        out_dword(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR,v)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_MPM_SSCAON_CONFIG_ADDR,m,v,HWIO_MPM2_MPM_SSCAON_CONFIG_IN)
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_CONFIG_SSCAON_CONFIG_SHFT                                 0x0

#define HWIO_MPM2_MPM_SSCAON_STATUS_ADDR                                        (MPM2_PSHOLD_REG_BASE      + 0x0000000c)
#define HWIO_MPM2_MPM_SSCAON_STATUS_RMSK                                        0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR, HWIO_MPM2_MPM_SSCAON_STATUS_RMSK)
#define HWIO_MPM2_MPM_SSCAON_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_MPM_SSCAON_STATUS_ADDR, m)
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_BMSK                          0xffffffff
#define HWIO_MPM2_MPM_SSCAON_STATUS_SSCAON_STATUS_SHFT                                 0x0


#endif /* __MPM_PSHOLD_HWIO_H__ */
