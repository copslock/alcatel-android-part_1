/*
===========================================================================

FILE:         HALmpmintPlatform.c

DESCRIPTION:
  This is the platform hardware abstraction layer implementation for the
  MPM interrupt controller block.
  This platform is for the RPM on 8996.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/power/mpm/hal/source/8996/HALmpmintTable.c#4 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2014 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <HALmpmint.h>
#include "HALmpmintInternal.h"

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

/*
 * DEFINE_IRQ
 *
 * Macro to define an IRQ mpmint_irq_data_type entry in the table
 */
#define DEFINE_IRQ( trigger, gpio, padPos ) \
  {                                                 \
    HAL_MPMINT_TRIGGER_##trigger,                   \
    gpio,                                           \
    padPos,                                         \
  }

/*
 * Target-specific interrupt configurations
 */
HAL_mpmint_PlatformIntType aInterruptTable[HAL_MPMINT_NUM] =
{
  /*          Trigger            GPIO                      Pad Bit          */
  /*          -------  -----------------------   -------------------------- */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_QTIMER_ISR                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_PEN_ISR                     */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_TSENS_UPPER_LOWER_ISR       */
  DEFINE_IRQ( RISING,  1,                        0                          ), /* HAL_MPMINT_GPIO1_ISR                   */
  DEFINE_IRQ( RISING,  5,                        1                          ), /* HAL_MPMINT_GPIO5_ISR                   */
  DEFINE_IRQ( RISING,  9,                        2                          ), /* HAL_MPMINT_GPIO9_ISR                   */
  DEFINE_IRQ( RISING,  11,                       3                          ), /* HAL_MPMINT_GPIO11_ISR                  */
  DEFINE_IRQ( RISING,  66,                       26                         ), /* HAL_MPMINT_GPIO66_ISR                  */
  DEFINE_IRQ( RISING,  22,                       4                          ), /* HAL_MPMINT_GPIO22_ISR                  */
  DEFINE_IRQ( RISING,  24,                       5                          ), /* HAL_MPMINT_GPIO24_ISR                  */
  DEFINE_IRQ( RISING,  26,                       6                          ), /* HAL_MPMINT_GPIO26_ISR                  */
  DEFINE_IRQ( RISING,  34,                       7                          ), /* HAL_MPMINT_GPIO34_ISR                  */
  DEFINE_IRQ( RISING,  36,                       8                          ), /* HAL_MPMINT_GPIO36_ISR                  */
  DEFINE_IRQ( RISING,  37,                       9                          ), /* HAL_MPMINT_GPIO37_ISR                  */
  DEFINE_IRQ( RISING,  38,                       10                         ), /* HAL_MPMINT_GPIO38_ISR                  */
  DEFINE_IRQ( RISING,  40,                       11                         ), /* HAL_MPMINT_GPIO40_ISR                  */
  DEFINE_IRQ( RISING,  42,                       12                         ), /* HAL_MPMINT_GPIO42_ISR                  */
  DEFINE_IRQ( RISING,  46,                       13                         ), /* HAL_MPMINT_GPIO46_ISR                  */
  DEFINE_IRQ( RISING,  50,                       14                         ), /* HAL_MPMINT_GPIO50_ISR                  */
  DEFINE_IRQ( RISING,  53,                       15                         ), /* HAL_MPMINT_GPIO53_ISR                  */
  DEFINE_IRQ( RISING,  54,                       16                         ), /* HAL_MPMINT_GPIO54_ISR                  */
  DEFINE_IRQ( RISING,  56,                       17                         ), /* HAL_MPMINT_GPIO56_ISR                  */
  DEFINE_IRQ( RISING,  57,                       18                         ), /* HAL_MPMINT_GPIO57_ISR                  */
  DEFINE_IRQ( RISING,  58,                       19                         ), /* HAL_MPMINT_GPIO58_ISR                  */
  DEFINE_IRQ( RISING,  59,                       20                         ), /* HAL_MPMINT_GPIO59_ISR                  */
  DEFINE_IRQ( RISING,  60,                       21                         ), /* HAL_MPMINT_GPIO60_ISR                  */
  DEFINE_IRQ( RISING,  61,                       22                         ), /* HAL_MPMINT_GPIO61_ISR                  */
  DEFINE_IRQ( RISING,  62,                       23                         ), /* HAL_MPMINT_GPIO62_ISR                  */
  DEFINE_IRQ( RISING,  63,                       24                         ), /* HAL_MPMINT_GPIO63_ISR                  */
  DEFINE_IRQ( RISING,  64,                       25                         ), /* HAL_MPMINT_GPIO64_ISR                  */
  DEFINE_IRQ( RISING,  71,                       27                         ), /* HAL_MPMINT_GPIO71_ISR                  */
  DEFINE_IRQ( RISING,  73,                       28                         ), /* HAL_MPMINT_GPIO73_ISR                  */
  DEFINE_IRQ( RISING,  77,                       29                         ), /* HAL_MPMINT_GPIO77_ISR                  */
  DEFINE_IRQ( RISING,  78,                       30                         ), /* HAL_MPMINT_GPIO78_ISR                  */
  DEFINE_IRQ( RISING,  79,                       31                         ), /* HAL_MPMINT_GPIO79_ISR                  */
  DEFINE_IRQ( RISING,  80,                       32 + 0                     ), /* HAL_MPMINT_GPIO80_ISR                  */
  DEFINE_IRQ( RISING,  82,                       32 + 1                     ), /* HAL_MPMINT_GPIO82_ISR                  */
  DEFINE_IRQ( RISING,  86,                       32 + 2                     ), /* HAL_MPMINT_GPIO86_ISR                  */
  DEFINE_IRQ( RISING,  91,                       32 + 3                     ), /* HAL_MPMINT_GPIO91_ISR                  */
  DEFINE_IRQ( RISING,  92,                       32 + 4                     ), /* HAL_MPMINT_GPIO92_ISR                  */
  DEFINE_IRQ( RISING,  95,                       32 + 5                     ), /* HAL_MPMINT_GPIO95_ISR                  */
  DEFINE_IRQ( RISING,  97,                       32 + 6                     ), /* HAL_MPMINT_GPIO97_ISR                  */
  DEFINE_IRQ( RISING,  101,                      32 + 7                     ), /* HAL_MPMINT_GPIO101_ISR                 */
  DEFINE_IRQ( RISING,  104,                      32 + 8                     ), /* HAL_MPMINT_GPIO104_ISR                 */
  DEFINE_IRQ( RISING,  106,                      32 + 9                     ), /* HAL_MPMINT_GPIO106_ISR                 */
  DEFINE_IRQ( RISING,  108,                      32 + 10                    ), /* HAL_MPMINT_GPIO108_ISR                 */
  DEFINE_IRQ( RISING,  112,                      32 + 12                    ), /* HAL_MPMINT_GPIO112_ISR                 */
  DEFINE_IRQ( RISING,  113,                      32 + 13                    ), /* HAL_MPMINT_GPIO113_ISR                 */
  DEFINE_IRQ( RISING,  110,                      32 + 11                    ), /* HAL_MPMINT_GPIO110_ISR                 */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  127,                      32 + 26                    ), /* HAL_MPMINT_GPIO127_ISR                 */
  DEFINE_IRQ( RISING,  115,                      32 + 14                    ), /* HAL_MPMINT_GPIO115_ISR                 */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_PCIE_USB3_PHY_ISR           */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_HDMI_PHY_ISR                */
  DEFINE_IRQ( RISING,  116,                      32 + 15                    ), /* HAL_MPMINT_GPIO116_ISR                 */
  DEFINE_IRQ( RISING,  117,                      32 + 16                    ), /* HAL_MPMINT_GPIO117_ISR                 */
  DEFINE_IRQ( RISING,  118,                      32 + 17                    ), /* HAL_MPMINT_GPIO118_ISR                 */
  DEFINE_IRQ( RISING,  119,                      32 + 18                    ), /* HAL_MPMINT_GPIO119_ISR                 */
  DEFINE_IRQ( RISING,  120,                      32 + 19                    ), /* HAL_MPMINT_GPIO120_ISR                 */
  DEFINE_IRQ( RISING,  121,                      32 + 20                    ), /* HAL_MPMINT_GPIO121_ISR                 */
  DEFINE_IRQ( RISING,  122,                      32 + 21                    ), /* HAL_MPMINT_GPIO122_ISR                 */
  DEFINE_IRQ( RISING,  123,                      32 + 22                    ), /* HAL_MPMINT_GPIO123_ISR                 */
  DEFINE_IRQ( RISING,  124,                      32 + 23                    ), /* HAL_MPMINT_GPIO124_ISR                 */
  DEFINE_IRQ( RISING,  125,                      32 + 24                    ), /* HAL_MPMINT_GPIO125_ISR                 */
  DEFINE_IRQ( RISING,  126,                      32 + 25                    ), /* HAL_MPMINT_GPIO126_ISR                 */
  DEFINE_IRQ( RISING,  129,                      32 + 27                    ), /* HAL_MPMINT_GPIO129_ISR                 */
  DEFINE_IRQ( RISING,  131,                      32 + 28                    ), /* HAL_MPMINT_GPIO131_ISR                 */
  DEFINE_IRQ( RISING,  132,                      32 + 29                    ), /* HAL_MPMINT_GPIO132_ISR                 */
  DEFINE_IRQ( RISING,  133,                      32 + 30                    ), /* HAL_MPMINT_GPIO133_ISR                 */
  DEFINE_IRQ( RISING,  145,                      32 + 31                    ), /* HAL_MPMINT_GPIO145_ISR                 */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 0                     ), /* HAL_MPMINT_SDC1_DATA1                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 1                     ), /* HAL_MPMINT_SDC1_DATA3                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 2                     ), /* HAL_MPMINT_SDC2_DATA1                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 3                     ), /* HAL_MPMINT_SDC2_DATA3                  */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 4                     ), /* HAL_MPMINT_SDC2_CMD                    */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  64 + 7                     ), /* HAL_MPMINT_SRST_N                      */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_PHY_DMSE_HS1_ISR        */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_PHY_DMSE_HS2_ISR        */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_PHY_DPSE_HS1_ISR        */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_USB_PHY_DPSE_HS2_ISR        */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_MPM_WAKE_SPMI_ISR           */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_Q6_SPM_BRINGUP_REQ_ISR      */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_SSC_TMR_TIMEOUT_ISR         */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* OPEN                                   */
  DEFINE_IRQ( RISING,  HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_UIM_CONTROLLER_CARD_ISR     */
  DEFINE_IRQ( HIGH,    HAL_MPMINT_INVALID_GPIO,  HAL_MPMINT_INVALID_BIT_POS ), /* HAL_MPMINT_UIM_CONTROLLER_BATT_ISR     */

};

