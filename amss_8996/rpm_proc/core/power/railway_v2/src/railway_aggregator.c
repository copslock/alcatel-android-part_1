/*===========================================================================
                              railway_aggregator.c

SERVICES:

DESCRIPTION:

INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include "railway.h"
#include "railway_internal.h"
#include "CoreVerify.h"
#include <stdlib.h>
#include <stdbool.h>
#include "railway_config.h"

//---------------------------------------------------------------------------
// Constant / Define Declarations
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------
typedef struct railway_voter_s {
    railway_corner voltage_corner;
    railway_corner active_floor;
    bool suppressible;
    int id;
    int rail;
    bool sw_enable;
    uint32 ipeak_in_ma;   // peak current in milliamps
    railway_voter_t voter_link;       //Need a better plan for the linked list.
} railway_voter_s;

//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// External References
//---------------------------------------------------------------------------


//===========================================================================
//                             Macro Definitions
//===========================================================================

//===========================================================================
//                           Function Definitions
//===========================================================================

/*===========================================================================
FUNCTION: railway_create_voter

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
railway_voter_t railway_create_voter(int rail, bool suppressible, int id)
{
  //Rail ID goes from 0 to (num_rails-1)
  CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
  //Store all the voters in a linked list. To do, make this nicer.
  railway_voter_t voter = (railway_voter_t)malloc(sizeof(railway_voter_s));
  CORE_VERIFY_PTR(voter);
  voter->voltage_corner = RAILWAY_NO_REQUEST;
  voter->active_floor = RAILWAY_NO_REQUEST;
  voter->suppressible = suppressible;
  voter->id = id;
  voter->rail = rail;
  voter->sw_enable = true;
  voter->ipeak_in_ma = 0;
  voter->voter_link = railway.rail_state[rail].voter_list_head;   //Will set the first voter's link to NULL, after that it just pushes out the list.
  railway.rail_state[rail].voter_list_head = voter;
  return voter;
}

/*===========================================================================
FUNCTION: railway_sw_enable_vote

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_sw_enable_vote(railway_voter_t voter, bool sw_enable)
{
  CORE_VERIFY(voter);
  voter->sw_enable = sw_enable;
}

/*===========================================================================
FUNCTION: railway_corner_vote

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_corner_vote(railway_voter_t voter, railway_corner corner)
{
  CORE_VERIFY(voter);
  CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
  voter->voltage_corner = corner;
}

void railway_active_floor_vote(railway_voter_t voter, railway_corner corner)
{
  CORE_VERIFY(voter);
  CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);
  voter->active_floor = corner;
}

/*===========================================================================
FUNCTION: railway_aggregated_voltage_target_uv

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
railway_corner railway_aggregated_voltage_target(int rail, bool for_sleep)
{
  //Rail ID goes from 0 to (num_rails-1)
  CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
    
  if(RAILWAY_CONFIG_DATA->rails[rail].aggregation_workaround)
  {
    RAILWAY_CONFIG_DATA->rails[rail].aggregation_workaround(rail);
  }

  railway_corner target_setting = RAILWAY_NO_REQUEST;
    
  railway_voter_t voter = railway.rail_state[rail].voter_list_head;
  bool sw_enable = false;

  //First, find highest corner vote.
  while (voter)
  {
    if(!(for_sleep && voter->suppressible)) //Ignore suppressible votes if we're asking for sleep.
    {
      if(voter->voltage_corner > target_setting)
      {
        target_setting = voter->voltage_corner;
      }
            
      if(voter->voltage_corner!=RAILWAY_NO_REQUEST)
      {
        sw_enable = sw_enable || voter->sw_enable;
      }
    }
    if(!for_sleep)  //Factor in active floor requests.
    {
      if(voter->active_floor > target_setting)
      {
        target_setting = voter->active_floor;
      }
    }
    voter = (railway_voter_t)voter->voter_link;
  }
   
  // If no clients have both a non-zero voltage/corner request AND sw_enable set
  // then the target is 0/off.
  if(!sw_enable)
  {
    target_setting = RAILWAY_NO_REQUEST;
  }
  
  return target_setting;
}

static void railway_do_set_corner_voltage(int rail, railway_corner corner, uint32 microvolts)
{
    CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
    CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);

    if(corner>0 && railway.rail_state[rail].corner_uvs[corner-1]>microvolts)
    {
        railway_do_set_corner_voltage(rail, (railway_corner)(corner-1), microvolts); //Should recurse such that voltages for modes can't overlap
    }

    if(corner<(RAILWAY_CORNERS_COUNT-1) && railway.rail_state[rail].corner_uvs[corner+1]<microvolts)
    {
        railway_do_set_corner_voltage(rail, (railway_corner)(corner+1), microvolts); //Should recurse such that voltages for modes can't overlap
    }
    
	railway.rail_state[rail].corner_uvs[corner]=microvolts;
}

void railway_set_corner_voltage(int rail, railway_corner corner, uint32 microvolts)
{
    railway_do_set_corner_voltage(rail, corner, microvolts);
    
    //Now verify that the voltages are increasing with the corner
    for(int i=0; i<RAILWAY_CORNERS_COUNT-1; i++)
    {
        CORE_VERIFY(railway.rail_state[rail].corner_uvs[i] <= railway.rail_state[rail].corner_uvs[i+1]);
    }
}
uint32 railway_get_corner_voltage(int rail, railway_corner corner)
{
    CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
    CORE_VERIFY(corner<RAILWAY_CORNERS_COUNT);

	return railway.rail_state[rail].corner_uvs[corner];
}

bool railway_voter_preventing_vdd_min(int id)
{
    //RailID 1 is Cx. Should probably add a #define.
    railway_voter_t voter = railway.rail_state[1].voter_list_head;
    while (voter)
    {
        if(voter->id==id)
        {
            if((!voter->suppressible) &&
                (voter->voltage_corner>RAILWAY_RETENTION))
                return true;
            else
                return false;
        }
        voter = voter->voter_link;
    }
    return false;
}

extern bool railway_transition_in_progress;

/*===========================================================================
FUNCTION: railway_get_current_settings

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_get_current_settings(int rail, railway_settings* settings)
{
  CORE_VERIFY(rail < RAILWAY_CONFIG_DATA->num_rails);
  CORE_VERIFY_PTR(settings);
  CORE_VERIFY(!railway_transition_in_progress);
  
  memcpy(settings, &railway.rail_state[rail].current_active, sizeof(railway_settings));
}

/*===========================================================================
FUNCTION: rail_id

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
int rail_id(const char* rail)
{
  const railway_rail_config_t *rail_data = NULL;
    
  for(int i = 0; i < RAILWAY_CONFIG_DATA->num_rails; ++i)
  {
    rail_data = &RAILWAY_CONFIG_DATA->rails[i];
    if(!strcmp(rail, rail_data->vreg_name))
      return i;
  }
    
  return RAIL_NOT_SUPPORTED_BY_RAILWAY;
}
