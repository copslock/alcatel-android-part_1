
/*============================================================================
  FILE:         sleep_target_config.c

  OVERVIEW:     This file provides target-specific functionality for the RPM.

  DEPENDENCIES: None

                Copyright (c) 2011 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

  $Header: //components/rel/rpm.bf/1.6/core/power/sleep/src/8996/sleep_target_config.c#10 $
  $DateTime: 2016/01/19 13:46:50 $
  $Author: pwbldsvc $
============================================================================*/

#include "qfprom_pte_lpass_hwio.h"
#include "msmhwio.h"
#include "railway.h"
#include "Chipinfo.h"
#include "CoreVerify.h"

extern int cx_id, mx_id;

volatile uint32 cx_fuse_value;
volatile uint32 mx_fuse_value;


/*
  EARLY_INIT below
*/

typedef uint32 (*custom_read_fuse_setting_cb_t)(void);

typedef struct
{
    const uint32 *retention_values;
    const custom_read_fuse_setting_cb_t custom_read_fuse_setting_cb;
} pvs_data_t;

static uint32 read_cx_fuse_setting(void);
static uint32 read_mx_fuse_setting(void);

// retention programmed in uV ( 600000uV = 0.6V )
static const uint32 vddcx_pvs_retention_data[8] =
{
  /* 000 */ 600000, 
  /* 001 */ 550000,
  /* 010 */ 500000,
  /* 011 */ 450000,
  /* 100 */ 400000,
  /* 101 */ 400000, //limiting based on CR812560
  /* 110 */ 400000, //limiting based on CR812560
  /* 111 */ 600000
};

static const uint32 vddmx_pvs_retention_data[8] =
{
  /* 000 */ 700000, 
  /* 001 */ 650000,
  /* 010 */ 580000, 
  /* 011 */ 550000, 
  /* 100 */ 490000,  
  /* 101 */ 490000, 
  /* 110 */ 490000, 
  /* 111 */ 490000
};

static pvs_data_t vddcx_pvs_data =
{
    /* .retention_values            */ vddcx_pvs_retention_data,
    /* .custom_read_fuse_setting_cb */ read_cx_fuse_setting
};


static pvs_data_t vddmx_pvs_data =
{
    /* .retention_values            */ vddmx_pvs_retention_data,
    /* .custom_read_fuse_setting_cb */ read_mx_fuse_setting
};

static uint32 read_mx_fuse_setting(void)
{

    /* 0x0x00070134[31:29] */
    return (HWIO_INM(QFPROM_RAW_PTE_ROW3_LSB, (0x7 << 2)) >> 2);
}

static uint32 read_cx_fuse_setting(void)
{
    unsigned offset = 29;	
    /* 0x00070148[4:2] */
    return (HWIO_INM(QFPROM_RAW_PTE_ROW0_MSB, (0x7 << offset)) >> offset);
}

static uint32 set_sleep_voltage(int rail_id, pvs_data_t *pvs_data_ptr, uint32 floor_val)
{
  uint32 lookup_val;

  CORE_VERIFY_PTR(pvs_data_ptr->custom_read_fuse_setting_cb);
  lookup_val = pvs_data_ptr->custom_read_fuse_setting_cb();

  CORE_VERIFY(lookup_val < 8);

  railway_set_corner_voltage(rail_id, RAILWAY_RETENTION, MAX(pvs_data_ptr->retention_values[lookup_val], floor_val));

  return lookup_val;
}

__attribute__((section("sleep_cram_reclaim_pool")))
void sleep_voltages_init(void)
{
    //floor value for < V3 is 580000. CR812560
    uint32 mx_floor_val = 580000;
    uint32 ssc_mx_floor_val = 587500;
    int ebi_id = rail_id("vdda_ebi");
    int ssc_cx_id = rail_id("vdd_ssc_cx");
    int ssc_mx_id = rail_id("vdd_ssc_mx");

    CORE_VERIFY(RAIL_NOT_SUPPORTED_BY_RAILWAY!=ebi_id);
    CORE_VERIFY(RAIL_NOT_SUPPORTED_BY_RAILWAY!=ssc_cx_id);
    CORE_VERIFY(RAIL_NOT_SUPPORTED_BY_RAILWAY!=ssc_mx_id);

    //floor value for >= V3 is 490000. CR886146
    if((Chipinfo_GetVersion() >= CHIPINFO_VERSION(3,0)) || (Chipinfo_GetFamily() == CHIPINFO_FAMILY_MSM8996SG))
    {
        mx_floor_val = 490000;
        ssc_mx_floor_val = 487500;
    }    

    cx_fuse_value = set_sleep_voltage(cx_id, &vddcx_pvs_data, 0);
    mx_fuse_value = set_sleep_voltage(mx_id, &vddmx_pvs_data, mx_floor_val);

    /*
     * Set EBI retention voltage    == CX retention voltage
     * Set SSC CX retention voltage == CX retention voltage
     * Set SSC MX retention voltage == MX retention voltage
     */
    (void)set_sleep_voltage(ebi_id, &vddcx_pvs_data, 0);
    (void)set_sleep_voltage(ssc_cx_id, &vddcx_pvs_data, 0);
    (void)set_sleep_voltage(ssc_mx_id, &vddmx_pvs_data, ssc_mx_floor_val);
}

