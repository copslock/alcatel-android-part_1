#ifndef QDSS_TPDA_CONFIG_H
#define QDSS_TPDA_CONFIG_H

/*=============================================================================

FILE:         qdss_tpda_config.h

DESCRIPTION:  

================================================================================
            Copyright (c) 2014 Qualcomm Technologies, Inc.
                         All Rights Reserved.
          Qualcomm Technologies Proprietary and Confidential
==============================================================================*/



#define QDSS_TPDA_REG_BASE_PHYS      (0x63003000) 


#endif //QDSS_TPDA_CONFIG_H
