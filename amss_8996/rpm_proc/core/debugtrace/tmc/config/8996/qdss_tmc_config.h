#ifndef QDSS_TMC_CONFIG_H
#define QDSS_TMC_CONFIG_H

/*=============================================================================

FILE:         qdss_tmc_config.h

DESCRIPTION:  

================================================================================
              Copyright (c) 2014 Qualcomm Technologies Incorporated.
                         All Rights Reserved.
                QUALCOMM Proprietary and Confidential
==============================================================================*/

#include "qdss_chip_version.h"

#define QDSS_ETFETB_BASE_PHYS_V2  (0x63027000)
#define QDSS_ETR_BASE_PHYS_V2     (0x63028000)

#define QDSS_ETFETB_BASE_PHYS_V1  (0x63025000)
#define QDSS_ETR_BASE_PHYS_V1     (0x63026000)

static __inline  uint32 QDSS_ETFETB_BASE_PHYS(void) 
{
   if (qdss_is_8996_v1()) {
      return QDSS_ETFETB_BASE_PHYS_V1;
   }
   else {
      return QDSS_ETFETB_BASE_PHYS_V2;
   }
}

static __inline  uint32 QDSS_ETR_BASE_PHYS(void) 
{
   if (qdss_is_8996_v1()) {
      return QDSS_ETR_BASE_PHYS_V1;
   }
   else {
      return QDSS_ETR_BASE_PHYS_V2;
   }
}


#endif //QDSS_TMC_CONFIG_H
