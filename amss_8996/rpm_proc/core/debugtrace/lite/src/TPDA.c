/*=============================================================================

FILE:         TPDA.c

DESCRIPTION:  

================================================================================
            Copyright (c) 2014 Qualcomm Technologies, Inc.
                         All Rights Reserved.
          Qualcomm Technologies Proprietary and Confidential
==============================================================================*/

#include "TPDA.h"
#include "halqdss_tpda.c"  //lite_inline 
#include "qdss_tpda_config.h"

__inline void TPDASetTimestampFreq(uint32 ts_counter_freq)
{
   HAL_qdss_tpda_AccessUnLock();
   HAL_qdss_tpda_SetTimeStampFreq(ts_counter_freq);
   HAL_qdss_tpda_AccessLock();
}


__attribute__((section("qdss_cram_reclaim_pool")))
__inline void TPDAPreInit(uint32 ts_counter_freq)
{
   HAL_qdss_tpda_HalConfigInit(QDSS_TPDA_REG_BASE_PHYS);
   TPDASetTimestampFreq(ts_counter_freq);
}

