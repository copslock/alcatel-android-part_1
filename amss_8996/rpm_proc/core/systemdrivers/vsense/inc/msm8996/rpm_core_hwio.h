#ifndef __RPM_CORE_HWIO_H__
#define __RPM_CORE_HWIO_H__
/*
===========================================================================
*/
/**
  @file rpm_core_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) v2 [istari_v2.1_p3q2r16.7]
 
  This file contains HWIO register definitions for the following modules:
    RPM_DEC

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/systemdrivers/vsense/inc/msm8996/rpm_core_hwio.h#1 $
  $DateTime: 2015/05/04 13:52:17 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: RPM_DEC
 *--------------------------------------------------------------------------*/

#define RPM_DEC_REG_BASE                                                    (RPM_BASE      + 0x00080000)

#define HWIO_RPM_HW_VERSION_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000000)
#define HWIO_RPM_HW_VERSION_RMSK                                            0xffffffff
#define HWIO_RPM_HW_VERSION_IN          \
        in_dword_masked(HWIO_RPM_HW_VERSION_ADDR, HWIO_RPM_HW_VERSION_RMSK)
#define HWIO_RPM_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_RPM_HW_VERSION_ADDR, m)
#define HWIO_RPM_HW_VERSION_MAJOR_BMSK                                      0xf0000000
#define HWIO_RPM_HW_VERSION_MAJOR_SHFT                                            0x1c
#define HWIO_RPM_HW_VERSION_MINOR_BMSK                                       0xfff0000
#define HWIO_RPM_HW_VERSION_MINOR_SHFT                                            0x10
#define HWIO_RPM_HW_VERSION_STEP_BMSK                                           0xffff
#define HWIO_RPM_HW_VERSION_STEP_SHFT                                              0x0

#define HWIO_RPM_WFI_CONFIG_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000004)
#define HWIO_RPM_WFI_CONFIG_RMSK                                                   0x7
#define HWIO_RPM_WFI_CONFIG_IN          \
        in_dword_masked(HWIO_RPM_WFI_CONFIG_ADDR, HWIO_RPM_WFI_CONFIG_RMSK)
#define HWIO_RPM_WFI_CONFIG_INM(m)      \
        in_dword_masked(HWIO_RPM_WFI_CONFIG_ADDR, m)
#define HWIO_RPM_WFI_CONFIG_OUT(v)      \
        out_dword(HWIO_RPM_WFI_CONFIG_ADDR,v)
#define HWIO_RPM_WFI_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WFI_CONFIG_ADDR,m,v,HWIO_RPM_WFI_CONFIG_IN)
#define HWIO_RPM_WFI_CONFIG_CHIP_SLEEP_UPON_WFI_BMSK                               0x4
#define HWIO_RPM_WFI_CONFIG_CHIP_SLEEP_UPON_WFI_SHFT                               0x2
#define HWIO_RPM_WFI_CONFIG_BUS_CLK_HALT_BMSK                                      0x2
#define HWIO_RPM_WFI_CONFIG_BUS_CLK_HALT_SHFT                                      0x1
#define HWIO_RPM_WFI_CONFIG_PROC_CLK_HALT_BMSK                                     0x1
#define HWIO_RPM_WFI_CONFIG_PROC_CLK_HALT_SHFT                                     0x0

#define HWIO_RPM_TIMERS_CLK_OFF_CTL_ADDR                                    (RPM_DEC_REG_BASE      + 0x00000008)
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_RMSK                                           0x1
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_IN          \
        in_dword_masked(HWIO_RPM_TIMERS_CLK_OFF_CTL_ADDR, HWIO_RPM_TIMERS_CLK_OFF_CTL_RMSK)
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_INM(m)      \
        in_dword_masked(HWIO_RPM_TIMERS_CLK_OFF_CTL_ADDR, m)
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_OUT(v)      \
        out_dword(HWIO_RPM_TIMERS_CLK_OFF_CTL_ADDR,v)
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_TIMERS_CLK_OFF_CTL_ADDR,m,v,HWIO_RPM_TIMERS_CLK_OFF_CTL_IN)
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_WDOG_TIMER_CLK_OFF_BMSK                        0x1
#define HWIO_RPM_TIMERS_CLK_OFF_CTL_WDOG_TIMER_CLK_OFF_SHFT                        0x0

#define HWIO_RPM_IPC_ADDR                                                   (RPM_DEC_REG_BASE      + 0x0000000c)
#define HWIO_RPM_IPC_RMSK                                                   0xffffffff
#define HWIO_RPM_IPC_OUT(v)      \
        out_dword(HWIO_RPM_IPC_ADDR,v)
#define HWIO_RPM_IPC_VMM_IPC_BMSK                                           0xf0000000
#define HWIO_RPM_IPC_VMM_IPC_SHFT                                                 0x1c
#define HWIO_RPM_IPC_SSC_IPC_BMSK                                            0xf000000
#define HWIO_RPM_IPC_SSC_IPC_SHFT                                                 0x18
#define HWIO_RPM_IPC_TZ_IPC_BMSK                                              0xf00000
#define HWIO_RPM_IPC_TZ_IPC_SHFT                                                  0x14
#define HWIO_RPM_IPC_WCN_IPC_BMSK                                              0xf0000
#define HWIO_RPM_IPC_WCN_IPC_SHFT                                                 0x10
#define HWIO_RPM_IPC_MPSS_IPC_BMSK                                              0xf000
#define HWIO_RPM_IPC_MPSS_IPC_SHFT                                                 0xc
#define HWIO_RPM_IPC_ADSP_IPC_BMSK                                               0xf00
#define HWIO_RPM_IPC_ADSP_IPC_SHFT                                                 0x8
#define HWIO_RPM_IPC_APCS_HLOS_IPC_BMSK                                           0xf0
#define HWIO_RPM_IPC_APCS_HLOS_IPC_SHFT                                            0x4
#define HWIO_RPM_IPC_RPM_RSRV_BMSK                                                 0xf
#define HWIO_RPM_IPC_RPM_RSRV_SHFT                                                 0x0

#define HWIO_RPM_GPO_WDATA_ADDR                                             (RPM_DEC_REG_BASE      + 0x00000010)
#define HWIO_RPM_GPO_WDATA_RMSK                                             0xffffffff
#define HWIO_RPM_GPO_WDATA_IN          \
        in_dword_masked(HWIO_RPM_GPO_WDATA_ADDR, HWIO_RPM_GPO_WDATA_RMSK)
#define HWIO_RPM_GPO_WDATA_INM(m)      \
        in_dword_masked(HWIO_RPM_GPO_WDATA_ADDR, m)
#define HWIO_RPM_GPO_WDATA_OUT(v)      \
        out_dword(HWIO_RPM_GPO_WDATA_ADDR,v)
#define HWIO_RPM_GPO_WDATA_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_GPO_WDATA_ADDR,m,v,HWIO_RPM_GPO_WDATA_IN)
#define HWIO_RPM_GPO_WDATA_WDATA_BMSK                                       0xffffffff
#define HWIO_RPM_GPO_WDATA_WDATA_SHFT                                              0x0

#define HWIO_RPM_GPO_WDSET_ADDR                                             (RPM_DEC_REG_BASE      + 0x00000014)
#define HWIO_RPM_GPO_WDSET_RMSK                                             0xffffffff
#define HWIO_RPM_GPO_WDSET_OUT(v)      \
        out_dword(HWIO_RPM_GPO_WDSET_ADDR,v)
#define HWIO_RPM_GPO_WDSET_WDSET_BMSK                                       0xffffffff
#define HWIO_RPM_GPO_WDSET_WDSET_SHFT                                              0x0

#define HWIO_RPM_GPO_WDCLR_ADDR                                             (RPM_DEC_REG_BASE      + 0x00000018)
#define HWIO_RPM_GPO_WDCLR_RMSK                                             0xffffffff
#define HWIO_RPM_GPO_WDCLR_OUT(v)      \
        out_dword(HWIO_RPM_GPO_WDCLR_ADDR,v)
#define HWIO_RPM_GPO_WDCLR_WDCLR_BMSK                                       0xffffffff
#define HWIO_RPM_GPO_WDCLR_WDCLR_SHFT                                              0x0

#define HWIO_RPM_SLAVES_CLK_GATING_ADDR                                     (RPM_DEC_REG_BASE      + 0x0000001c)
#define HWIO_RPM_SLAVES_CLK_GATING_RMSK                                            0xf
#define HWIO_RPM_SLAVES_CLK_GATING_IN          \
        in_dword_masked(HWIO_RPM_SLAVES_CLK_GATING_ADDR, HWIO_RPM_SLAVES_CLK_GATING_RMSK)
#define HWIO_RPM_SLAVES_CLK_GATING_INM(m)      \
        in_dword_masked(HWIO_RPM_SLAVES_CLK_GATING_ADDR, m)
#define HWIO_RPM_SLAVES_CLK_GATING_OUT(v)      \
        out_dword(HWIO_RPM_SLAVES_CLK_GATING_ADDR,v)
#define HWIO_RPM_SLAVES_CLK_GATING_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_SLAVES_CLK_GATING_ADDR,m,v,HWIO_RPM_SLAVES_CLK_GATING_IN)
#define HWIO_RPM_SLAVES_CLK_GATING_INTR_CLK_GATING_BMSK                            0x8
#define HWIO_RPM_SLAVES_CLK_GATING_INTR_CLK_GATING_SHFT                            0x3
#define HWIO_RPM_SLAVES_CLK_GATING_RAM_CLK_GATING_BMSK                             0x4
#define HWIO_RPM_SLAVES_CLK_GATING_RAM_CLK_GATING_SHFT                             0x2
#define HWIO_RPM_SLAVES_CLK_GATING_PERIPH_CLK_GATING_BMSK                          0x2
#define HWIO_RPM_SLAVES_CLK_GATING_PERIPH_CLK_GATING_SHFT                          0x1
#define HWIO_RPM_SLAVES_CLK_GATING_SLP_WKUP_FSM_CLK_GATING_BMSK                    0x1
#define HWIO_RPM_SLAVES_CLK_GATING_SLP_WKUP_FSM_CLK_GATING_SHFT                    0x0

#define HWIO_RPM_INTR_POLARITY_0_ADDR                                       (RPM_DEC_REG_BASE      + 0x00000030)
#define HWIO_RPM_INTR_POLARITY_0_RMSK                                       0xffffffff
#define HWIO_RPM_INTR_POLARITY_0_IN          \
        in_dword_masked(HWIO_RPM_INTR_POLARITY_0_ADDR, HWIO_RPM_INTR_POLARITY_0_RMSK)
#define HWIO_RPM_INTR_POLARITY_0_INM(m)      \
        in_dword_masked(HWIO_RPM_INTR_POLARITY_0_ADDR, m)
#define HWIO_RPM_INTR_POLARITY_0_OUT(v)      \
        out_dword(HWIO_RPM_INTR_POLARITY_0_ADDR,v)
#define HWIO_RPM_INTR_POLARITY_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_INTR_POLARITY_0_ADDR,m,v,HWIO_RPM_INTR_POLARITY_0_IN)
#define HWIO_RPM_INTR_POLARITY_0_POLARITY_BMSK                              0xffffffff
#define HWIO_RPM_INTR_POLARITY_0_POLARITY_SHFT                                     0x0

#define HWIO_RPM_INTR_POLARITY_1_ADDR                                       (RPM_DEC_REG_BASE      + 0x00000034)
#define HWIO_RPM_INTR_POLARITY_1_RMSK                                       0xffffffff
#define HWIO_RPM_INTR_POLARITY_1_IN          \
        in_dword_masked(HWIO_RPM_INTR_POLARITY_1_ADDR, HWIO_RPM_INTR_POLARITY_1_RMSK)
#define HWIO_RPM_INTR_POLARITY_1_INM(m)      \
        in_dword_masked(HWIO_RPM_INTR_POLARITY_1_ADDR, m)
#define HWIO_RPM_INTR_POLARITY_1_OUT(v)      \
        out_dword(HWIO_RPM_INTR_POLARITY_1_ADDR,v)
#define HWIO_RPM_INTR_POLARITY_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_INTR_POLARITY_1_ADDR,m,v,HWIO_RPM_INTR_POLARITY_1_IN)
#define HWIO_RPM_INTR_POLARITY_1_POLARITY_BMSK                              0xffffffff
#define HWIO_RPM_INTR_POLARITY_1_POLARITY_SHFT                                     0x0

#define HWIO_RPM_INTR_EDG_LVL_0_ADDR                                        (RPM_DEC_REG_BASE      + 0x00000038)
#define HWIO_RPM_INTR_EDG_LVL_0_RMSK                                        0xffffffff
#define HWIO_RPM_INTR_EDG_LVL_0_IN          \
        in_dword_masked(HWIO_RPM_INTR_EDG_LVL_0_ADDR, HWIO_RPM_INTR_EDG_LVL_0_RMSK)
#define HWIO_RPM_INTR_EDG_LVL_0_INM(m)      \
        in_dword_masked(HWIO_RPM_INTR_EDG_LVL_0_ADDR, m)
#define HWIO_RPM_INTR_EDG_LVL_0_OUT(v)      \
        out_dword(HWIO_RPM_INTR_EDG_LVL_0_ADDR,v)
#define HWIO_RPM_INTR_EDG_LVL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_INTR_EDG_LVL_0_ADDR,m,v,HWIO_RPM_INTR_EDG_LVL_0_IN)
#define HWIO_RPM_INTR_EDG_LVL_0_EDG_LVL_BMSK                                0xffffffff
#define HWIO_RPM_INTR_EDG_LVL_0_EDG_LVL_SHFT                                       0x0

#define HWIO_RPM_INTR_EDG_LVL_1_ADDR                                        (RPM_DEC_REG_BASE      + 0x0000003c)
#define HWIO_RPM_INTR_EDG_LVL_1_RMSK                                        0xffffffff
#define HWIO_RPM_INTR_EDG_LVL_1_IN          \
        in_dword_masked(HWIO_RPM_INTR_EDG_LVL_1_ADDR, HWIO_RPM_INTR_EDG_LVL_1_RMSK)
#define HWIO_RPM_INTR_EDG_LVL_1_INM(m)      \
        in_dword_masked(HWIO_RPM_INTR_EDG_LVL_1_ADDR, m)
#define HWIO_RPM_INTR_EDG_LVL_1_OUT(v)      \
        out_dword(HWIO_RPM_INTR_EDG_LVL_1_ADDR,v)
#define HWIO_RPM_INTR_EDG_LVL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_INTR_EDG_LVL_1_ADDR,m,v,HWIO_RPM_INTR_EDG_LVL_1_IN)
#define HWIO_RPM_INTR_EDG_LVL_1_EDG_LVL_BMSK                                0xffffffff
#define HWIO_RPM_INTR_EDG_LVL_1_EDG_LVL_SHFT                                       0x0

#define HWIO_RPM_WDOG_RESET_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000040)
#define HWIO_RPM_WDOG_RESET_RMSK                                                   0x3
#define HWIO_RPM_WDOG_RESET_IN          \
        in_dword_masked(HWIO_RPM_WDOG_RESET_ADDR, HWIO_RPM_WDOG_RESET_RMSK)
#define HWIO_RPM_WDOG_RESET_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_RESET_ADDR, m)
#define HWIO_RPM_WDOG_RESET_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_RESET_ADDR,v)
#define HWIO_RPM_WDOG_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_RESET_ADDR,m,v,HWIO_RPM_WDOG_RESET_IN)
#define HWIO_RPM_WDOG_RESET_SYNC_STATUS_BMSK                                       0x2
#define HWIO_RPM_WDOG_RESET_SYNC_STATUS_SHFT                                       0x1
#define HWIO_RPM_WDOG_RESET_WDOG_RESET_BMSK                                        0x1
#define HWIO_RPM_WDOG_RESET_WDOG_RESET_SHFT                                        0x0

#define HWIO_RPM_WDOG_CTRL_ADDR                                             (RPM_DEC_REG_BASE      + 0x00000044)
#define HWIO_RPM_WDOG_CTRL_RMSK                                                    0x3
#define HWIO_RPM_WDOG_CTRL_IN          \
        in_dword_masked(HWIO_RPM_WDOG_CTRL_ADDR, HWIO_RPM_WDOG_CTRL_RMSK)
#define HWIO_RPM_WDOG_CTRL_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_CTRL_ADDR, m)
#define HWIO_RPM_WDOG_CTRL_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_CTRL_ADDR,v)
#define HWIO_RPM_WDOG_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_CTRL_ADDR,m,v,HWIO_RPM_WDOG_CTRL_IN)
#define HWIO_RPM_WDOG_CTRL_HW_WAKEUP_SLEEP_EN_BMSK                                 0x2
#define HWIO_RPM_WDOG_CTRL_HW_WAKEUP_SLEEP_EN_SHFT                                 0x1
#define HWIO_RPM_WDOG_CTRL_ENABLE_BMSK                                             0x1
#define HWIO_RPM_WDOG_CTRL_ENABLE_SHFT                                             0x0

#define HWIO_RPM_WDOG_STATUS_ADDR                                           (RPM_DEC_REG_BASE      + 0x00000048)
#define HWIO_RPM_WDOG_STATUS_RMSK                                             0x7fffff
#define HWIO_RPM_WDOG_STATUS_IN          \
        in_dword_masked(HWIO_RPM_WDOG_STATUS_ADDR, HWIO_RPM_WDOG_STATUS_RMSK)
#define HWIO_RPM_WDOG_STATUS_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_STATUS_ADDR, m)
#define HWIO_RPM_WDOG_STATUS_WDOG_COUNT_BMSK                                  0x7ffff8
#define HWIO_RPM_WDOG_STATUS_WDOG_COUNT_SHFT                                       0x3
#define HWIO_RPM_WDOG_STATUS_WDOG_CNT_RESET_STATUS_BMSK                            0x4
#define HWIO_RPM_WDOG_STATUS_WDOG_CNT_RESET_STATUS_SHFT                            0x2
#define HWIO_RPM_WDOG_STATUS_WDOG_FROZEN_BMSK                                      0x2
#define HWIO_RPM_WDOG_STATUS_WDOG_FROZEN_SHFT                                      0x1
#define HWIO_RPM_WDOG_STATUS_WDOG_EXPIRED_STATUS_BMSK                              0x1
#define HWIO_RPM_WDOG_STATUS_WDOG_EXPIRED_STATUS_SHFT                              0x0

#define HWIO_RPM_WDOG_BARK_TIME_ADDR                                        (RPM_DEC_REG_BASE      + 0x0000004c)
#define HWIO_RPM_WDOG_BARK_TIME_RMSK                                          0x1fffff
#define HWIO_RPM_WDOG_BARK_TIME_IN          \
        in_dword_masked(HWIO_RPM_WDOG_BARK_TIME_ADDR, HWIO_RPM_WDOG_BARK_TIME_RMSK)
#define HWIO_RPM_WDOG_BARK_TIME_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_BARK_TIME_ADDR, m)
#define HWIO_RPM_WDOG_BARK_TIME_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_BARK_TIME_ADDR,v)
#define HWIO_RPM_WDOG_BARK_TIME_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_BARK_TIME_ADDR,m,v,HWIO_RPM_WDOG_BARK_TIME_IN)
#define HWIO_RPM_WDOG_BARK_TIME_SYNC_STATUS_BMSK                              0x100000
#define HWIO_RPM_WDOG_BARK_TIME_SYNC_STATUS_SHFT                                  0x14
#define HWIO_RPM_WDOG_BARK_TIME_WDOG_BARK_VAL_BMSK                             0xfffff
#define HWIO_RPM_WDOG_BARK_TIME_WDOG_BARK_VAL_SHFT                                 0x0

#define HWIO_RPM_WDOG_BITE_TIME_ADDR                                        (RPM_DEC_REG_BASE      + 0x00000050)
#define HWIO_RPM_WDOG_BITE_TIME_RMSK                                          0x1fffff
#define HWIO_RPM_WDOG_BITE_TIME_IN          \
        in_dword_masked(HWIO_RPM_WDOG_BITE_TIME_ADDR, HWIO_RPM_WDOG_BITE_TIME_RMSK)
#define HWIO_RPM_WDOG_BITE_TIME_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_BITE_TIME_ADDR, m)
#define HWIO_RPM_WDOG_BITE_TIME_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_BITE_TIME_ADDR,v)
#define HWIO_RPM_WDOG_BITE_TIME_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_BITE_TIME_ADDR,m,v,HWIO_RPM_WDOG_BITE_TIME_IN)
#define HWIO_RPM_WDOG_BITE_TIME_SYNC_STATUS_BMSK                              0x100000
#define HWIO_RPM_WDOG_BITE_TIME_SYNC_STATUS_SHFT                                  0x14
#define HWIO_RPM_WDOG_BITE_TIME_WDOG_BITE_VAL_BMSK                             0xfffff
#define HWIO_RPM_WDOG_BITE_TIME_WDOG_BITE_VAL_SHFT                                 0x0

#define HWIO_RPM_WDOG_TEST_LOAD_ADDR                                        (RPM_DEC_REG_BASE      + 0x00000054)
#define HWIO_RPM_WDOG_TEST_LOAD_RMSK                                               0x3
#define HWIO_RPM_WDOG_TEST_LOAD_IN          \
        in_dword_masked(HWIO_RPM_WDOG_TEST_LOAD_ADDR, HWIO_RPM_WDOG_TEST_LOAD_RMSK)
#define HWIO_RPM_WDOG_TEST_LOAD_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_TEST_LOAD_ADDR, m)
#define HWIO_RPM_WDOG_TEST_LOAD_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_TEST_LOAD_ADDR,v)
#define HWIO_RPM_WDOG_TEST_LOAD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_TEST_LOAD_ADDR,m,v,HWIO_RPM_WDOG_TEST_LOAD_IN)
#define HWIO_RPM_WDOG_TEST_LOAD_SYNC_STATUS_BMSK                                   0x2
#define HWIO_RPM_WDOG_TEST_LOAD_SYNC_STATUS_SHFT                                   0x1
#define HWIO_RPM_WDOG_TEST_LOAD_LOAD_BMSK                                          0x1
#define HWIO_RPM_WDOG_TEST_LOAD_LOAD_SHFT                                          0x0

#define HWIO_RPM_WDOG_TEST_ADDR                                             (RPM_DEC_REG_BASE      + 0x00000058)
#define HWIO_RPM_WDOG_TEST_RMSK                                               0x1fffff
#define HWIO_RPM_WDOG_TEST_IN          \
        in_dword_masked(HWIO_RPM_WDOG_TEST_ADDR, HWIO_RPM_WDOG_TEST_RMSK)
#define HWIO_RPM_WDOG_TEST_INM(m)      \
        in_dword_masked(HWIO_RPM_WDOG_TEST_ADDR, m)
#define HWIO_RPM_WDOG_TEST_OUT(v)      \
        out_dword(HWIO_RPM_WDOG_TEST_ADDR,v)
#define HWIO_RPM_WDOG_TEST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_WDOG_TEST_ADDR,m,v,HWIO_RPM_WDOG_TEST_IN)
#define HWIO_RPM_WDOG_TEST_SYNC_STATUS_BMSK                                   0x100000
#define HWIO_RPM_WDOG_TEST_SYNC_STATUS_SHFT                                       0x14
#define HWIO_RPM_WDOG_TEST_LOAD_VALUE_BMSK                                     0xfffff
#define HWIO_RPM_WDOG_TEST_LOAD_VALUE_SHFT                                         0x0

#define HWIO_RPM_TEST_BUS_SEL_ADDR                                          (RPM_DEC_REG_BASE      + 0x0000005c)
#define HWIO_RPM_TEST_BUS_SEL_RMSK                                                 0xf
#define HWIO_RPM_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_RPM_TEST_BUS_SEL_ADDR, HWIO_RPM_TEST_BUS_SEL_RMSK)
#define HWIO_RPM_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_RPM_TEST_BUS_SEL_ADDR, m)
#define HWIO_RPM_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_RPM_TEST_BUS_SEL_ADDR,v)
#define HWIO_RPM_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_TEST_BUS_SEL_ADDR,m,v,HWIO_RPM_TEST_BUS_SEL_IN)
#define HWIO_RPM_TEST_BUS_SEL_VAL_BMSK                                             0xf
#define HWIO_RPM_TEST_BUS_SEL_VAL_SHFT                                             0x0

#define HWIO_RPM_SPARE_REG0_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000060)
#define HWIO_RPM_SPARE_REG0_RMSK                                            0xffffffff
#define HWIO_RPM_SPARE_REG0_IN          \
        in_dword_masked(HWIO_RPM_SPARE_REG0_ADDR, HWIO_RPM_SPARE_REG0_RMSK)
#define HWIO_RPM_SPARE_REG0_INM(m)      \
        in_dword_masked(HWIO_RPM_SPARE_REG0_ADDR, m)
#define HWIO_RPM_SPARE_REG0_OUT(v)      \
        out_dword(HWIO_RPM_SPARE_REG0_ADDR,v)
#define HWIO_RPM_SPARE_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_SPARE_REG0_ADDR,m,v,HWIO_RPM_SPARE_REG0_IN)
#define HWIO_RPM_SPARE_REG0_WDATA_BMSK                                      0xffffffff
#define HWIO_RPM_SPARE_REG0_WDATA_SHFT                                             0x0

#define HWIO_RPM_SPARE_REG1_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000064)
#define HWIO_RPM_SPARE_REG1_RMSK                                            0xffffffff
#define HWIO_RPM_SPARE_REG1_IN          \
        in_dword_masked(HWIO_RPM_SPARE_REG1_ADDR, HWIO_RPM_SPARE_REG1_RMSK)
#define HWIO_RPM_SPARE_REG1_INM(m)      \
        in_dword_masked(HWIO_RPM_SPARE_REG1_ADDR, m)
#define HWIO_RPM_SPARE_REG1_OUT(v)      \
        out_dword(HWIO_RPM_SPARE_REG1_ADDR,v)
#define HWIO_RPM_SPARE_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_SPARE_REG1_ADDR,m,v,HWIO_RPM_SPARE_REG1_IN)
#define HWIO_RPM_SPARE_REG1_WDATA_BMSK                                      0xffffffff
#define HWIO_RPM_SPARE_REG1_WDATA_SHFT                                             0x0

#define HWIO_RPM_SPARE_REG2_ADDR                                            (RPM_DEC_REG_BASE      + 0x00000068)
#define HWIO_RPM_SPARE_REG2_RMSK                                            0xffffffff
#define HWIO_RPM_SPARE_REG2_IN          \
        in_dword_masked(HWIO_RPM_SPARE_REG2_ADDR, HWIO_RPM_SPARE_REG2_RMSK)
#define HWIO_RPM_SPARE_REG2_INM(m)      \
        in_dword_masked(HWIO_RPM_SPARE_REG2_ADDR, m)
#define HWIO_RPM_SPARE_REG2_OUT(v)      \
        out_dword(HWIO_RPM_SPARE_REG2_ADDR,v)
#define HWIO_RPM_SPARE_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_SPARE_REG2_ADDR,m,v,HWIO_RPM_SPARE_REG2_IN)
#define HWIO_RPM_SPARE_REG2_WDATA_BMSK                                      0xffffffff
#define HWIO_RPM_SPARE_REG2_WDATA_SHFT                                             0x0

#define HWIO_RPM_SPARE_REG3_ADDR                                            (RPM_DEC_REG_BASE      + 0x0000006c)
#define HWIO_RPM_SPARE_REG3_RMSK                                            0xffffffff
#define HWIO_RPM_SPARE_REG3_IN          \
        in_dword_masked(HWIO_RPM_SPARE_REG3_ADDR, HWIO_RPM_SPARE_REG3_RMSK)
#define HWIO_RPM_SPARE_REG3_INM(m)      \
        in_dword_masked(HWIO_RPM_SPARE_REG3_ADDR, m)
#define HWIO_RPM_SPARE_REG3_OUT(v)      \
        out_dword(HWIO_RPM_SPARE_REG3_ADDR,v)
#define HWIO_RPM_SPARE_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_SPARE_REG3_ADDR,m,v,HWIO_RPM_SPARE_REG3_IN)
#define HWIO_RPM_SPARE_REG3_WDATA_BMSK                                      0xffffffff
#define HWIO_RPM_SPARE_REG3_WDATA_SHFT                                             0x0

#define HWIO_RPM_PAGE_SELECT_ADDR                                           (RPM_DEC_REG_BASE      + 0x00000070)
#define HWIO_RPM_PAGE_SELECT_RMSK                                                 0x3f
#define HWIO_RPM_PAGE_SELECT_IN          \
        in_dword_masked(HWIO_RPM_PAGE_SELECT_ADDR, HWIO_RPM_PAGE_SELECT_RMSK)
#define HWIO_RPM_PAGE_SELECT_INM(m)      \
        in_dword_masked(HWIO_RPM_PAGE_SELECT_ADDR, m)
#define HWIO_RPM_PAGE_SELECT_OUT(v)      \
        out_dword(HWIO_RPM_PAGE_SELECT_ADDR,v)
#define HWIO_RPM_PAGE_SELECT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_PAGE_SELECT_ADDR,m,v,HWIO_RPM_PAGE_SELECT_IN)
#define HWIO_RPM_PAGE_SELECT_PAGE_SELECT_BMSK                                     0x3f
#define HWIO_RPM_PAGE_SELECT_PAGE_SELECT_SHFT                                      0x0

#define HWIO_RPM_BRIDGES_CLK_GATING_ADDR                                    (RPM_DEC_REG_BASE      + 0x00000074)
#define HWIO_RPM_BRIDGES_CLK_GATING_RMSK                                           0x3
#define HWIO_RPM_BRIDGES_CLK_GATING_IN          \
        in_dword_masked(HWIO_RPM_BRIDGES_CLK_GATING_ADDR, HWIO_RPM_BRIDGES_CLK_GATING_RMSK)
#define HWIO_RPM_BRIDGES_CLK_GATING_INM(m)      \
        in_dword_masked(HWIO_RPM_BRIDGES_CLK_GATING_ADDR, m)
#define HWIO_RPM_BRIDGES_CLK_GATING_OUT(v)      \
        out_dword(HWIO_RPM_BRIDGES_CLK_GATING_ADDR,v)
#define HWIO_RPM_BRIDGES_CLK_GATING_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_BRIDGES_CLK_GATING_ADDR,m,v,HWIO_RPM_BRIDGES_CLK_GATING_IN)
#define HWIO_RPM_BRIDGES_CLK_GATING_RPM_BRIDGE_CLK_GATING_BMSK                     0x2
#define HWIO_RPM_BRIDGES_CLK_GATING_RPM_BRIDGE_CLK_GATING_SHFT                     0x1
#define HWIO_RPM_BRIDGES_CLK_GATING_NOC_BRIDGE_CLK_GATING_BMSK                     0x1
#define HWIO_RPM_BRIDGES_CLK_GATING_NOC_BRIDGE_CLK_GATING_SHFT                     0x0

#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_ADDR                               (RPM_DEC_REG_BASE      + 0x00000078)
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_RMSK                               0x7fffffff
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_IN          \
        in_dword_masked(HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_ADDR, HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_RMSK)
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_INM(m)      \
        in_dword_masked(HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_ADDR, m)
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_OUT(v)      \
        out_dword(HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_ADDR,v)
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_ADDR,m,v,HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_IN)
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_RPM_BRIDGE_HYST_CNTR_BMSK          0x7fff0000
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_RPM_BRIDGE_HYST_CNTR_SHFT                0x10
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_NOC_BRDG_SYNC_STATUS_BMSK              0x8000
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_NOC_BRDG_SYNC_STATUS_SHFT                 0xf
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_NOC_BRIDGE_HYST_CNTR_BMSK              0x7fff
#define HWIO_RPM_BRIDGES_HYSTERESIS_CNTR_NOC_BRIDGE_HYST_CNTR_SHFT                 0x0

#define HWIO_RPM_MISC_ADDR                                                  (RPM_DEC_REG_BASE      + 0x0000007c)
#define HWIO_RPM_MISC_RMSK                                                         0x3
#define HWIO_RPM_MISC_IN          \
        in_dword_masked(HWIO_RPM_MISC_ADDR, HWIO_RPM_MISC_RMSK)
#define HWIO_RPM_MISC_INM(m)      \
        in_dword_masked(HWIO_RPM_MISC_ADDR, m)
#define HWIO_RPM_MISC_OUT(v)      \
        out_dword(HWIO_RPM_MISC_ADDR,v)
#define HWIO_RPM_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_RPM_MISC_ADDR,m,v,HWIO_RPM_MISC_IN)
#define HWIO_RPM_MISC_RPM_BRIDGE_POST_EN_BMSK                                      0x2
#define HWIO_RPM_MISC_RPM_BRIDGE_POST_EN_SHFT                                      0x1
#define HWIO_RPM_MISC_NOC_BRIDGE_POST_EN_BMSK                                      0x1
#define HWIO_RPM_MISC_NOC_BRIDGE_POST_EN_SHFT                                      0x0


#endif /* __RPM_CORE_HWIO_H__ */
