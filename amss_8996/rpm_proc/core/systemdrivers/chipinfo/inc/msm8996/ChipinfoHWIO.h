#ifndef __CHIPINFOHWIO_H__
#define __CHIPINFOHWIO_H__
/*
===========================================================================
*/
/**
  @file ChipInfoHWIO.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8996 (Istari) [istari_v1.0_p2q1r8.1.4_MTO]
 
  This file contains HWIO register definitions for the following modules:
    TCSR_TCSR_REGS
    SECURITY_CONTROL_CORE

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/systemdrivers/chipinfo/inc/msm8996/ChipinfoHWIO.h#2 $
  $DateTime: 2015/02/03 20:52:58 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_REGS
 *--------------------------------------------------------------------------*/

#define TCSR_TCSR_REGS_REG_BASE                                                                                                                       (CORE_TOP_CSR_BASE      + 0x000a0000)

#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00000800)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK                                                                                                                  0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, m)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,v)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,m,v,HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_BMSK                                                                                             0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_SHFT                                                                                             0x0

#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00000804)
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_RMSK                                                                                                                   0x1
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTERNAL_EN_ADDR, HWIO_TCSR_TIMEOUT_INTERNAL_EN_RMSK)
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTERNAL_EN_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTERNAL_EN_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTERNAL_EN_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTERNAL_EN_IN)
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_TIMEOUT_INTERNAL_EN_BMSK                                                                                               0x1
#define HWIO_TCSR_TIMEOUT_INTERNAL_EN_TIMEOUT_INTERNAL_EN_SHFT                                                                                               0x0

#define HWIO_TCSR_XPU_NSEN_STATUS_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00000820)
#define HWIO_TCSR_XPU_NSEN_STATUS_RMSK                                                                                                                       0x3
#define HWIO_TCSR_XPU_NSEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, HWIO_TCSR_XPU_NSEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_NSEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_BMSK                                                                                                 0x2
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_SHFT                                                                                                 0x1
#define HWIO_TCSR_XPU_NSEN_STATUS_MUTEX_XPU2_NSEN_STATUS_BMSK                                                                                                0x1
#define HWIO_TCSR_XPU_NSEN_STATUS_MUTEX_XPU2_NSEN_STATUS_SHFT                                                                                                0x0

#define HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000824)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK                                                                                                                     0x3
#define HWIO_TCSR_XPU_VMIDEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_BMSK                                                                                             0x2
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_SHFT                                                                                             0x1
#define HWIO_TCSR_XPU_VMIDEN_STATUS_MUTEX_XPU2_VMIDEN_STATUS_BMSK                                                                                            0x1
#define HWIO_TCSR_XPU_VMIDEN_STATUS_MUTEX_XPU2_VMIDEN_STATUS_SHFT                                                                                            0x0

#define HWIO_TCSR_XPU_MSAEN_STATUS_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00000828)
#define HWIO_TCSR_XPU_MSAEN_STATUS_RMSK                                                                                                                      0x3
#define HWIO_TCSR_XPU_MSAEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, HWIO_TCSR_XPU_MSAEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_MSAEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_BMSK                                                                                               0x2
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_SHFT                                                                                               0x1
#define HWIO_TCSR_XPU_MSAEN_STATUS_MUTEX_XPU2_MSAEN_STATUS_BMSK                                                                                              0x1
#define HWIO_TCSR_XPU_MSAEN_STATUS_MUTEX_XPU2_MSAEN_STATUS_SHFT                                                                                              0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00002000)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_QDSS_MPU_APU_XPU2_NON_SEC_INTR_BMSK                                                                           0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_QDSS_MPU_APU_XPU2_NON_SEC_INTR_SHFT                                                                                 0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BOOT_ROM_XPU2_NON_SEC_INTR_BMSK                                                                               0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BOOT_ROM_XPU2_NON_SEC_INTR_SHFT                                                                                     0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_NOC_CFG_XPU2_NON_SEC_INTR_BMSK                                                                                0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_NOC_CFG_XPU2_NON_SEC_INTR_SHFT                                                                                      0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_1_BMSK                                                                                 0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_1_SHFT                                                                                       0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_XPU2_NON_SEC_INTR_BMSK                                                                                0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_XPU2_NON_SEC_INTR_SHFT                                                                                     0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPDM_XPU_XPU2_NON_SEC_INTR_BMSK                                                                                0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SPDM_XPU_XPU2_NON_SEC_INTR_SHFT                                                                                     0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_XPU2_NON_SEC_INTR_BMSK                                                                                0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_XPU2_NON_SEC_INTR_SHFT                                                                                     0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_UFS_ICE_XPU2_NON_SEC_INTR_BMSK                                                                                 0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_UFS_ICE_XPU2_NON_SEC_INTR_SHFT                                                                                      0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_VENUS_WRAPPER_XPU2_NON_SEC_INTERRUPT_BMSK                                                                       0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_VENUS_WRAPPER_XPU2_NON_SEC_INTERRUPT_SHFT                                                                           0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH1_XPU2_NON_SEC_INTERRUPT_BMSK                                                                            0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH1_XPU2_NON_SEC_INTERRUPT_SHFT                                                                                0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NON_SEC_INTERRUPT_BMSK                                                                            0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NON_SEC_INTERRUPT_SHFT                                                                                0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CFG_XPU2_NON_SEC_INTERRUPT_BMSK                                                                            0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CFG_XPU2_NON_SEC_INTERRUPT_SHFT                                                                                0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SEC_INTR_BMSK                                                                                        0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SEC_INTR_SHFT                                                                                           0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_UFS_XPU_NON_SEC_INTR_BMSK                                                                                        0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_UFS_XPU_NON_SEC_INTR_SHFT                                                                                           0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A2NOC_MPU_CFG_XPU2_NON_SEC_INTR_BMSK                                                                             0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A2NOC_MPU_CFG_XPU2_NON_SEC_INTR_SHFT                                                                                0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A1NOC_MPU_CFG_XPU2_NON_SEC_INTR_BMSK                                                                             0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A1NOC_MPU_CFG_XPU2_NON_SEC_INTR_SHFT                                                                                0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A0NOC_MPU_CFG_XPU2_NON_SEC_INTR_BMSK                                                                              0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_A0NOC_MPU_CFG_XPU2_NON_SEC_INTR_SHFT                                                                                 0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PIMEM_MPU_XPU2_NON_SEC_IRQ_BMSK                                                                                   0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PIMEM_MPU_XPU2_NON_SEC_IRQ_SHFT                                                                                      0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PIMEM_APU_XPU2_NON_SEC_IRQ_BMSK                                                                                   0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PIMEM_APU_XPU2_NON_SEC_IRQ_SHFT                                                                                      0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_1_BMSK                                                                                           0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_1_SHFT                                                                                              0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_9_BMSK                                                                                            0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_9_SHFT                                                                                              0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_4_BMSK                                                                                            0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SSC_XPU_IRQ_APPS_4_SHFT                                                                                              0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SEC_CTRL_XPU2_NON_SEC_INTR_BMSK                                                                                    0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SEC_CTRL_XPU2_NON_SEC_INTR_SHFT                                                                                      0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DCC_XPU2_NON_SEC_INTR_BMSK                                                                                         0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DCC_XPU2_NON_SEC_INTR_SHFT                                                                                           0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_RPU_XPU2_NON_SEC_INTR_BMSK                                                                                   0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_RPU_XPU2_NON_SEC_INTR_SHFT                                                                                    0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CRYPTO0_BAM_XPU2_NON_SEC_INTR_BMSK                                                                                  0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CRYPTO0_BAM_XPU2_NON_SEC_INTR_SHFT                                                                                   0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_MUTEX_XPU2_NON_SEC_INTR_BMSK                                                                                 0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_MUTEX_XPU2_NON_SEC_INTR_SHFT                                                                                  0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_COPSS_XPU2_NON_SEC_IRQ_BMSK                                                                                         0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_COPSS_XPU2_NON_SEC_IRQ_SHFT                                                                                          0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_REGS_XPU2_NON_SEC_INTR_BMSK                                                                                   0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_REGS_XPU2_NON_SEC_INTR_SHFT                                                                                   0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MMSS_NOC_XPU2_NON_SEC_INTR_BMSK                                                                                      0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MMSS_NOC_XPU2_NON_SEC_INTR_SHFT                                                                                      0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DSA_XPU2_NON_SEC_INTR_BMSK                                                                                           0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DSA_XPU2_NON_SEC_INTR_SHFT                                                                                           0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SDC1_XPU2_NON_SEC_INTR_BMSK                                                                                          0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SDC1_XPU2_NON_SEC_INTR_SHFT                                                                                          0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00002004)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RMSK                                                                                                               0x3ff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RPM_MPU_XPU2_NON_SEC_INTR_BMSK                                                                                     0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RPM_MPU_XPU2_NON_SEC_INTR_SHFT                                                                                       0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_LPASS_IRQ_OUT_SECURIT_9_BMSK                                                                                       0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_LPASS_IRQ_OUT_SECURIT_9_SHFT                                                                                         0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MDSS_XPU2_NON_SEC_INTR_BMSK                                                                                         0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MDSS_XPU2_NON_SEC_INTR_SHFT                                                                                          0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_QDSS_BAM_XPU2_NON_SEC_INTR_BMSK                                                                                     0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_QDSS_BAM_XPU2_NON_SEC_INTR_SHFT                                                                                      0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MPM_XPU2_NON_SEC_INTR_BMSK                                                                                          0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_MPM_XPU2_NON_SEC_INTR_SHFT                                                                                           0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDCC2_XPU2_NON_SEC_ERROR_INTR_BMSK                                                                                  0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_SDCC2_XPU2_NON_SEC_ERROR_INTR_SHFT                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RBCPR_APU_XPU2_NON_SEC_INTR_BMSK                                                                                     0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_RBCPR_APU_XPU2_NON_SEC_INTR_SHFT                                                                                     0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP2_XPU2_NON_SEC_INTR_BMSK                                                                                         0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP2_XPU2_NON_SEC_INTR_SHFT                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP1_XPU2_NON_SEC_INTR_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_BLSP1_XPU2_NON_SEC_INTR_SHFT                                                                                         0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCIMEM_MPU_XPU2_NON_SEC_INTR_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_OCIMEM_MPU_XPU2_NON_SEC_INTR_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00002040)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK                                                                                                   0xffffffff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_QDSS_MPU_APU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                             0x80000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_QDSS_MPU_APU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                   0x1f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BOOT_ROM_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                 0x40000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BOOT_ROM_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x1e
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_NOC_CFG_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                  0x20000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_NOC_CFG_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                        0x1d
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_1_INTR_ENABLE_BMSK                                                              0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_1_INTR_ENABLE_SHFT                                                                    0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                  0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPDM_XPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                  0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SPDM_XPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                  0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_UFS_ICE_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                   0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_UFS_ICE_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                        0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_VENUS_WRAPPER_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                              0x800000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_VENUS_WRAPPER_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                  0x17
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                   0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                   0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                   0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SEC_INTR_ENABLE_BMSK                                                                          0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SEC_INTR_ENABLE_SHFT                                                                             0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_UFS_XPU_NON_SEC_INTR_ENABLE_BMSK                                                                          0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_UFS_XPU_NON_SEC_INTR_ENABLE_SHFT                                                                             0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                               0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                  0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                               0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                  0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                   0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PIMEM_MPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                    0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PIMEM_MPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PIMEM_APU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                    0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PIMEM_APU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_1_INTR_ENABLE_BMSK                                                                        0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_1_INTR_ENABLE_SHFT                                                                           0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_9_INTR_ENABLE_BMSK                                                                         0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_9_INTR_ENABLE_SHFT                                                                           0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_4_INTR_ENABLE_BMSK                                                                         0x400
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_4_INTR_ENABLE_SHFT                                                                           0xa
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                      0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                        0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DCC_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                           0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DCC_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                             0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_RPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                     0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_RPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                      0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CRYPTO0_BAM_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                    0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CRYPTO0_BAM_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                     0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_MUTEX_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                   0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_MUTEX_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                    0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_COPSS_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                          0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_COPSS_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                           0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_REGS_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                     0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_REGS_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                     0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MMSS_NOC_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                        0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MMSS_NOC_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                        0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DSA_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                             0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DSA_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                             0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SDC1_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                            0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SDC1_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                            0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00002044)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RMSK                                                                                                        0x3ff
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RPM_MPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                       0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RPM_MPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                         0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_9_INTR_ENABLE_BMSK                                                                    0x100
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_9_INTR_ENABLE_SHFT                                                                      0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MDSS_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                           0x80
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MDSS_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                            0x7
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_QDSS_BAM_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                       0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_QDSS_BAM_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                        0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MPM_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                            0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_MPM_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                             0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDCC2_XPU2_NON_SEC_ERROR_INTR_ENABLE_BMSK                                                                    0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_SDCC2_XPU2_NON_SEC_ERROR_INTR_ENABLE_SHFT                                                                     0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RBCPR_APU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                       0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_RBCPR_APU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                       0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP2_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                           0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP2_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                           0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP1_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                           0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_BLSP1_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                           0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCIMEM_MPU_XPU2_NON_SEC_INTR_ENABLE_BMSK                                                                      0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR1_ENABLE_OCIMEM_MPU_XPU2_NON_SEC_INTR_ENABLE_SHFT                                                                      0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00004000)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_QDSS_MPU_APU_XPU2_SEC_INTR_BMSK                                                                                   0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_QDSS_MPU_APU_XPU2_SEC_INTR_SHFT                                                                                         0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BOOT_ROM_XPU2_SEC_INTR_BMSK                                                                                       0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BOOT_ROM_XPU2_SEC_INTR_SHFT                                                                                             0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_NOC_CFG_XPU2_SEC_INTR_BMSK                                                                                        0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_NOC_CFG_XPU2_SEC_INTR_SHFT                                                                                              0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_0_BMSK                                                                                     0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_LPASS_IRQ_OUT_SECURITY_0_SHFT                                                                                           0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_XPU2_SEC_INTR_BMSK                                                                                        0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_XPU2_SEC_INTR_SHFT                                                                                             0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPDM_XPU_XPU2_SEC_INTR_BMSK                                                                                        0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SPDM_XPU_XPU2_SEC_INTR_SHFT                                                                                             0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_XPU2_SEC_INTR_BMSK                                                                                        0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_XPU2_SEC_INTR_SHFT                                                                                             0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_UFS_ICE_XPU2_SEC_INTR_BMSK                                                                                         0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_UFS_ICE_XPU2_SEC_INTR_SHFT                                                                                              0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_VENUS_WRAPPER_XPU2_SEC_INTERRUPT_BMSK                                                                               0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_VENUS_WRAPPER_XPU2_SEC_INTERRUPT_SHFT                                                                                   0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH1_XPU2_SEC_INTERRUPT_BMSK                                                                                    0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH1_XPU2_SEC_INTERRUPT_SHFT                                                                                        0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_SEC_INTERRUPT_BMSK                                                                                    0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_SEC_INTERRUPT_SHFT                                                                                        0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CFG_XPU2_SEC_INTERRUPT_BMSK                                                                                    0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CFG_XPU2_SEC_INTERRUPT_SHFT                                                                                        0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SEC_INTR_BMSK                                                                                                0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SEC_INTR_SHFT                                                                                                   0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_UFS_XPU_SEC_INTR_BMSK                                                                                                0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_UFS_XPU_SEC_INTR_SHFT                                                                                                   0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A2NOC_MPU_CFG_XPU2_SEC_INTR_BMSK                                                                                     0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A2NOC_MPU_CFG_XPU2_SEC_INTR_SHFT                                                                                        0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A1NOC_MPU_CFG_XPU2_SEC_INTR_BMSK                                                                                     0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A1NOC_MPU_CFG_XPU2_SEC_INTR_SHFT                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A0NOC_MPU_CFG_XPU2_SEC_INTR_BMSK                                                                                      0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_A0NOC_MPU_CFG_XPU2_SEC_INTR_SHFT                                                                                         0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PIMEM_MPU_XPU2_SEC_IRQ_BMSK                                                                                           0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PIMEM_MPU_XPU2_SEC_IRQ_SHFT                                                                                              0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PIMEM_APU_XPU2_SEC_IRQ_BMSK                                                                                           0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PIMEM_APU_XPU2_SEC_IRQ_SHFT                                                                                              0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_2_BMSK                                                                                               0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_2_SHFT                                                                                                  0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_8_BMSK                                                                                                0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_8_SHFT                                                                                                  0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_5_BMSK                                                                                                0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SSC_XPU_IRQ_APPS_5_SHFT                                                                                                  0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SEC_CTRL_XPU2_SEC_INTR_BMSK                                                                                            0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SEC_CTRL_XPU2_SEC_INTR_SHFT                                                                                              0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DCC_XPU2_SEC_INTR_BMSK                                                                                                 0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DCC_XPU2_SEC_INTR_SHFT                                                                                                   0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_RPU_XPU2_SEC_INTR_BMSK                                                                                           0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_RPU_XPU2_SEC_INTR_SHFT                                                                                            0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CRYPTO0_BAM_XPU2_SEC_INTR_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CRYPTO0_BAM_XPU2_SEC_INTR_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_MUTEX_XPU2_SEC_INTR_BMSK                                                                                         0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_MUTEX_XPU2_SEC_INTR_SHFT                                                                                          0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_COPSS_XPU2_SEC_IRQ_BMSK                                                                                                 0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_COPSS_XPU2_SEC_IRQ_SHFT                                                                                                  0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_REGS_XPU2_SEC_INTR_BMSK                                                                                           0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_REGS_XPU2_SEC_INTR_SHFT                                                                                           0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MMSS_NOC_XPU2_SEC_INTR_BMSK                                                                                              0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MMSS_NOC_XPU2_SEC_INTR_SHFT                                                                                              0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DSA_XPU2_SEC_INTR_BMSK                                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DSA_XPU2_SEC_INTR_SHFT                                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SDC1_XPU2_SEC_INTR_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SDC1_XPU2_SEC_INTR_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00004004)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RMSK                                                                                                                   0x3ff
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RPM_MPU_XPU2_SEC_INTR_BMSK                                                                                             0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RPM_MPU_XPU2_SEC_INTR_SHFT                                                                                               0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_LPASS_IRQ_OUT_SECURIT_8_BMSK                                                                                           0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_LPASS_IRQ_OUT_SECURIT_8_SHFT                                                                                             0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MDSS_XPU2_SEC_INTR_BMSK                                                                                                 0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MDSS_XPU2_SEC_INTR_SHFT                                                                                                  0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_QDSS_BAM_XPU2_SEC_INTR_BMSK                                                                                             0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_QDSS_BAM_XPU2_SEC_INTR_SHFT                                                                                              0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MPM_XPU2_SEC_INTR_BMSK                                                                                                  0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_MPM_XPU2_SEC_INTR_SHFT                                                                                                   0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDCC2_XPU2_SEC_ERROR_INTR_BMSK                                                                                          0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_SDCC2_XPU2_SEC_ERROR_INTR_SHFT                                                                                           0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RBCPR_APU_XPU2_SEC_INTR_BMSK                                                                                             0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_RBCPR_APU_XPU2_SEC_INTR_SHFT                                                                                             0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP2_XPU2_SEC_INTR_BMSK                                                                                                 0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP2_XPU2_SEC_INTR_SHFT                                                                                                 0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP1_XPU2_SEC_INTR_BMSK                                                                                                 0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_BLSP1_XPU2_SEC_INTR_SHFT                                                                                                 0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCIMEM_MPU_XPU2_SEC_INTR_BMSK                                                                                            0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_OCIMEM_MPU_XPU2_SEC_INTR_SHFT                                                                                            0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00004040)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK                                                                                                       0xffffffff
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_QDSS_MPU_APU_XPU2_SEC_INTR_ENABLE_BMSK                                                                     0x80000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_QDSS_MPU_APU_XPU2_SEC_INTR_ENABLE_SHFT                                                                           0x1f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BOOT_ROM_XPU2_SEC_INTR_ENABLE_BMSK                                                                         0x40000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BOOT_ROM_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x1e
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_NOC_CFG_XPU2_SEC_INTR_ENABLE_BMSK                                                                          0x20000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_NOC_CFG_XPU2_SEC_INTR_ENABLE_SHFT                                                                                0x1d
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_0_INTR_ENABLE_BMSK                                                                  0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_0_INTR_ENABLE_SHFT                                                                        0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                          0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPDM_XPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                          0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SPDM_XPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_XPU2_SEC_INTR_ENABLE_BMSK                                                                          0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_UFS_ICE_XPU2_SEC_INTR_ENABLE_BMSK                                                                           0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_UFS_ICE_XPU2_SEC_INTR_ENABLE_SHFT                                                                                0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_VENUS_WRAPPER_XPU2_SEC_INTR_ENABLE_BMSK                                                                      0x800000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_VENUS_WRAPPER_XPU2_SEC_INTR_ENABLE_SHFT                                                                          0x17
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_SEC_INTR_ENABLE_BMSK                                                                           0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH1_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_SEC_INTR_ENABLE_BMSK                                                                           0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_SEC_INTR_ENABLE_BMSK                                                                           0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CFG_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SEC_INTR_ENABLE_BMSK                                                                                  0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SEC_INTR_ENABLE_SHFT                                                                                     0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_UFS_XPU_SEC_INTR_ENABLE_BMSK                                                                                  0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_UFS_XPU_SEC_INTR_ENABLE_SHFT                                                                                     0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_BMSK                                                                       0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_SHFT                                                                          0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_BMSK                                                                       0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_SHFT                                                                          0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_BMSK                                                                        0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_SEC_INTR_ENABLE_SHFT                                                                           0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PIMEM_MPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                            0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PIMEM_MPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PIMEM_APU_XPU2_SEC_INTR_ENABLE_BMSK                                                                            0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PIMEM_APU_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_2_INTR_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_2_INTR_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_8_INTR_ENABLE_BMSK                                                                             0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_8_INTR_ENABLE_SHFT                                                                               0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_5_INTR_ENABLE_BMSK                                                                             0x400
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SSC_XPU_IRQ_APPS_5_INTR_ENABLE_SHFT                                                                               0xa
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_SEC_INTR_ENABLE_BMSK                                                                              0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_SEC_INTR_ENABLE_SHFT                                                                                0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DCC_XPU2_SEC_INTR_ENABLE_BMSK                                                                                   0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DCC_XPU2_SEC_INTR_ENABLE_SHFT                                                                                     0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_RPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                             0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_RPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                              0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CRYPTO0_BAM_XPU2_SEC_INTR_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CRYPTO0_BAM_XPU2_SEC_INTR_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_MUTEX_XPU2_SEC_INTR_ENABLE_BMSK                                                                           0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_MUTEX_XPU2_SEC_INTR_ENABLE_SHFT                                                                            0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_COPSS_XPU2_SEC_INTR_ENABLE_BMSK                                                                                  0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_COPSS_XPU2_SEC_INTR_ENABLE_SHFT                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_REGS_XPU2_SEC_INTR_ENABLE_BMSK                                                                             0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_REGS_XPU2_SEC_INTR_ENABLE_SHFT                                                                             0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MMSS_NOC_XPU2_SEC_INTR_ENABLE_BMSK                                                                                0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MMSS_NOC_XPU2_SEC_INTR_ENABLE_SHFT                                                                                0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DSA_XPU2_SEC_INTR_ENABLE_BMSK                                                                                     0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DSA_XPU2_SEC_INTR_ENABLE_SHFT                                                                                     0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SDC1_XPU2_SEC_INTR_ENABLE_BMSK                                                                                    0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SDC1_XPU2_SEC_INTR_ENABLE_SHFT                                                                                    0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00004044)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RMSK                                                                                                            0x3ff
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RPM_MPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                               0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RPM_MPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                                 0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_8_INTR_ENABLE_BMSK                                                                        0x100
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_8_INTR_ENABLE_SHFT                                                                          0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MDSS_XPU2_SEC_INTR_ENABLE_BMSK                                                                                   0x80
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MDSS_XPU2_SEC_INTR_ENABLE_SHFT                                                                                    0x7
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_QDSS_BAM_XPU2_SEC_INTR_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_QDSS_BAM_XPU2_SEC_INTR_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MPM_XPU2_SEC_INTR_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_MPM_XPU2_SEC_INTR_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDCC2_XPU2_SEC_ERROR_INTR_ENABLE_BMSK                                                                            0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_SDCC2_XPU2_SEC_ERROR_INTR_ENABLE_SHFT                                                                             0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RBCPR_APU_XPU2_SEC_INTR_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_RBCPR_APU_XPU2_SEC_INTR_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP2_XPU2_SEC_INTR_ENABLE_BMSK                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP2_XPU2_SEC_INTR_ENABLE_SHFT                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP1_XPU2_SEC_INTR_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_BLSP1_XPU2_SEC_INTR_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCIMEM_MPU_XPU2_SEC_INTR_ENABLE_BMSK                                                                              0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR1_ENABLE_OCIMEM_MPU_XPU2_SEC_INTR_ENABLE_SHFT                                                                              0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00002010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK                                                                                                      0x3dff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_PIMEM_VMIDMT_NSGIRPT_BMSK                                                                                 0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_PIMEM_VMIDMT_NSGIRPT_SHFT                                                                                    0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SPDM_VMID_NSGIRPT_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SPDM_VMID_NSGIRPT_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGIRPT_BMSK                                                                               0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGIRPT_SHFT                                                                                 0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGIRPT_BMSK                                                                             0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGIRPT_SHFT                                                                               0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP2_VMIDMT_NSGIRPT_BMSK                                                                                  0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP2_VMIDMT_NSGIRPT_SHFT                                                                                    0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_BMSK                                                                                   0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_SHFT                                                                                    0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_BMSK                                                                                  0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_SHFT                                                                                   0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGIRPT_BMSK                                                                                0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGIRPT_SHFT                                                                                 0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_UFS_VMIDMT_NSGIRPT_BMSK                                                                                     0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_UFS_VMIDMT_NSGIRPT_SHFT                                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS1_VBIF_VMIDMT_NSGIRPT_BMSK                                                                              0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS1_VBIF_VMIDMT_NSGIRPT_SHFT                                                                              0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT_BMSK                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGIRPT_SHFT                                                                              0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_BMSK                                                                                  0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_SHFT                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_BMSK                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_SHFT                                                                                  0x0

#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00002014)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RMSK                                                                                                         0x1ffe
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_LPASS_5_IRQ_OUT_SECURITY_BMSK                                                                                0x1000
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_LPASS_5_IRQ_OUT_SECURITY_SHFT                                                                                   0xc
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_LPASS_13_IRQ_OUT_SECURITY_BMSK                                                                                0x800
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_LPASS_13_IRQ_OUT_SECURITY_SHFT                                                                                  0xb
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR2_NOC_SMMU_MMU_NSGIRPT_BMSK                                                                               0x400
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR2_NOC_SMMU_MMU_NSGIRPT_SHFT                                                                                 0xa
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR1_NOC_SMMU_MMU_NSGIRPT_BMSK                                                                               0x200
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR1_NOC_SMMU_MMU_NSGIRPT_SHFT                                                                                 0x9
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR0_NOC_SMMU_MMU_NSGIRPT_BMSK                                                                               0x100
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_AGGR0_NOC_SMMU_MMU_NSGIRPT_SHFT                                                                                 0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_DMA_MMU_NSGIRPT_BMSK                                                                                      0x80
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_DMA_MMU_NSGIRPT_SHFT                                                                                       0x7
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_OGPU_MMU_NSGIRPT_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_OGPU_MMU_NSGIRPT_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_MMU_NSGIRPT_BMSK                                                                                          0x20
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_MDSS_MMU_NSGIRPT_SHFT                                                                                           0x5
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGIRPT_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGIRPT_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_VFE_MMU_NSGIRPT_BMSK                                                                                      0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_VFE_MMU_NSGIRPT_SHFT                                                                                      0x3
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_CPP_MMU_NSGIRPT_BMSK                                                                                      0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_CAMSS_CPP_MMU_NSGIRPT_SHFT                                                                                      0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_VENUS0_MMU_NSGIRPT_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_VENUS0_MMU_NSGIRPT_SHFT                                                                                         0x1

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00002050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK                                                                                               0x3dff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_PIMEM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                   0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_PIMEM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                      0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGIRPT_ENABLE_BMSK                                                                      0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGIRPT_ENABLE_SHFT                                                                         0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                 0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                   0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGIRPT_ENABLE_BMSK                                                               0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                 0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                    0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                      0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                     0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                      0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SSC_VMIDMT_1_APPS_IRQ_ENABLE_BMSK                                                                    0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_SSC_VMIDMT_1_APPS_IRQ_ENABLE_SHFT                                                                     0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                       0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                        0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_NSGIRPT_ENABLE_BMSK                                                                 0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_NSGIRPT_ENABLE_SHFT                                                                 0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_ENABLE_BMSK                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_ENABLE_SHFT                                                                    0x0

#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00002054)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RMSK                                                                                                  0x1ffe
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_LPASS_5_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                             0x1000
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_LPASS_5_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                0xc
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_LPASS_13_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                             0x800
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_LPASS_13_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                               0xb
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_NSGIRPT_ENABLE_BMSK                                                                 0x400
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_NSGIRPT_ENABLE_SHFT                                                                   0xa
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_NSGIRPT_ENABLE_BMSK                                                                 0x200
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_NSGIRPT_ENABLE_SHFT                                                                   0x9
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_NSGIRPT_ENABLE_BMSK                                                                 0x100
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_NSGIRPT_ENABLE_SHFT                                                                   0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_DMA_MMU_NSGIRPT_ENABLE_BMSK                                                                        0x80
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_DMA_MMU_NSGIRPT_ENABLE_SHFT                                                                         0x7
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OGPU_MMU_NSGIRPT_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_OGPU_MMU_NSGIRPT_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGIRPT_ENABLE_BMSK                                                                            0x20
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGIRPT_ENABLE_SHFT                                                                             0x5
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGIRPT_ENABLE_BMSK                                                                      0x10
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGIRPT_ENABLE_SHFT                                                                       0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGIRPT_ENABLE_BMSK                                                                        0x8
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGIRPT_ENABLE_SHFT                                                                        0x3
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGIRPT_ENABLE_BMSK                                                                        0x4
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGIRPT_ENABLE_SHFT                                                                        0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGIRPT_ENABLE_BMSK                                                                           0x2
#define HWIO_TCSR_SS_MMU_CLIENT_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGIRPT_ENABLE_SHFT                                                                           0x1

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00003000)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK                                                                                                         0x3dff
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_PIMEM_VMIDMT_NSGCFGIRPT_BMSK                                                                                 0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_PIMEM_VMIDMT_NSGCFGIRPT_SHFT                                                                                    0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SPDM_VMID_NSGCFGIRPT_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SPDM_VMID_NSGCFGIRPT_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGCFGIRPT_BMSK                                                                               0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_DAP_VMIDMT_NSGCFGIRPT_SHFT                                                                                 0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGCFGIRPT_BMSK                                                                             0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_TRACE_VMIDMT_NSGCFGIRPT_SHFT                                                                               0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP2_VMIDMT_NSGCFGIRPT_BMSK                                                                                  0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP2_VMIDMT_NSGCFGIRPT_SHFT                                                                                    0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_BMSK                                                                                   0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_SHFT                                                                                    0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_BMSK                                                                                     0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_SHFT                                                                                      0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGCFGIRPT_BMSK                                                                                0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QDSS_BAM_VMIDMT_NSGCFGIRPT_SHFT                                                                                 0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_UFS_VMIDMT_NSGCFGIRPT_BMSK                                                                                     0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_UFS_VMIDMT_NSGCFGIRPT_SHFT                                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS1_VBIF_VMIDMT_NSGCFGIRPT_BMSK                                                                              0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS1_VBIF_VMIDMT_NSGCFGIRPT_SHFT                                                                              0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT_BMSK                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_VENUS0_VBIF_VMIDMT_NSGCFGIRPT_SHFT                                                                              0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_BMSK                                                                                     0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_SHFT                                                                                     0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_BMSK                                                                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_SHFT                                                                                  0x0

#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00003004)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RMSK                                                                                                            0x1ffe
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_LPASS_4_IRQ_OUT_SECURITY_BMSK                                                                                   0x1000
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_LPASS_4_IRQ_OUT_SECURITY_SHFT                                                                                      0xc
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_LPASS_12_IRQ_OUT_SECURITY_BMSK                                                                                   0x800
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_LPASS_12_IRQ_OUT_SECURITY_SHFT                                                                                     0xb
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR2_NOC_SMMU_MMU_NSGCFGIRPT_BMSK                                                                               0x400
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR2_NOC_SMMU_MMU_NSGCFGIRPT_SHFT                                                                                 0xa
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR1_NOC_SMMU_MMU_NSGCFGIRPT_BMSK                                                                               0x200
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR1_NOC_SMMU_MMU_NSGCFGIRPT_SHFT                                                                                 0x9
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR0_NOC_SMMU_MMU_NSGCFGIRPT_BMSK                                                                               0x100
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_AGGR0_NOC_SMMU_MMU_NSGCFGIRPT_SHFT                                                                                 0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_DMA_MMU_NSGCFGIRPT_BMSK                                                                                      0x80
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_DMA_MMU_NSGCFGIRPT_SHFT                                                                                       0x7
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_OGPU_MMU_NSGCFGIRPT_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_OGPU_MMU_NSGCFGIRPT_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_MMU_NSGCFGIRPT_BMSK                                                                                          0x20
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_MDSS_MMU_NSGCFGIRPT_SHFT                                                                                           0x5
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGCFGIRPT_BMSK                                                                                    0x10
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_JPEG_MMU_NSGCFGIRPT_SHFT                                                                                     0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_VFE_MMU_NSGCFGIRPT_BMSK                                                                                      0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_VFE_MMU_NSGCFGIRPT_SHFT                                                                                      0x3
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_CPP_MMU_NSGCFGIRPT_BMSK                                                                                      0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_CAMSS_CPP_MMU_NSGCFGIRPT_SHFT                                                                                      0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_VENUS0_MMU_NSGCFGIRPT_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_VENUS0_MMU_NSGCFGIRPT_SHFT                                                                                         0x1

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00003040)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK                                                                                                  0x3dff
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_PIMEM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                   0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_PIMEM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                      0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGCFGIRPT_ENABLE_BMSK                                                                      0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SPDM_VMID_NSGCFGIRPT_ENABLE_SHFT                                                                         0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                 0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                   0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                               0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                 0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                    0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP2_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                      0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                     0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                      0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_BMSK                                                                       0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_SHFT                                                                        0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                       0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_UFS_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                        0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_NSGCFGIRPT_ENABLE_BMSK                                                                 0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_NSGCFGIRPT_ENABLE_SHFT                                                                 0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                                                                    0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                                                                    0x0

#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00003044)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RMSK                                                                                                     0x1ffe
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_LPASS_4_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                0x1000
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_LPASS_4_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                   0xc
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_LPASS_12_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                0x800
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_LPASS_12_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                  0xb
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                 0x400
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                   0xa
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                 0x200
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                   0x9
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                 0x100
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                   0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_DMA_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                        0x80
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_DMA_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                         0x7
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OGPU_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_OGPU_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                            0x20
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_MDSS_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                             0x5
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                      0x10
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                       0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                        0x8
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_VFE_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                        0x3
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                        0x4
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_CAMSS_CPP_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                        0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGCFGIRPT_ENABLE_BMSK                                                                           0x2
#define HWIO_TCSR_SS_MMU_CFG_NON_SEC_INTR_ENABLE_VENUS0_MMU_NSGCFGIRPT_ENABLE_SHFT                                                                           0x1

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00004010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK                                                                                                          0x3dff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_PIMEM_VMIDMT_GIRPT_BMSK                                                                                       0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_PIMEM_VMIDMT_GIRPT_SHFT                                                                                          0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SPDM_VMID_GIRPT_BMSK                                                                                          0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SPDM_VMID_GIRPT_SHFT                                                                                             0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_DAP_VMIDMT_GIRPT_BMSK                                                                                     0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_DAP_VMIDMT_GIRPT_SHFT                                                                                       0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_TRACE_VMIDMT_GIRPT_BMSK                                                                                   0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_TRACE_VMIDMT_GIRPT_SHFT                                                                                     0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP2_VMIDMT_GIRPT_BMSK                                                                                        0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP2_VMIDMT_GIRPT_SHFT                                                                                          0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_BMSK                                                                                         0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_SHFT                                                                                          0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_BMSK                                                                                      0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_SHFT                                                                                       0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_BAM_VMIDMT_GIRPT_BMSK                                                                                      0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QDSS_BAM_VMIDMT_GIRPT_SHFT                                                                                       0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_UFS_VMIDMT_GIRPT_BMSK                                                                                           0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_UFS_VMIDMT_GIRPT_SHFT                                                                                            0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS1_VBIF_VMIDMT_GIRPT_BMSK                                                                                    0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS1_VBIF_VMIDMT_GIRPT_SHFT                                                                                    0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT_BMSK                                                                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_VENUS0_VBIF_VMIDMT_GIRPT_SHFT                                                                                    0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_BMSK                                                                                      0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_SHFT                                                                                      0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_BMSK                                                                                        0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_SHFT                                                                                        0x0

#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00004014)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RMSK                                                                                                             0x1ffe
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_LPASS_7_IRQ_OUT_SECURITY_BMSK                                                                                    0x1000
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_LPASS_7_IRQ_OUT_SECURITY_SHFT                                                                                       0xc
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_LPASS_15_IRQ_OUT_SECURITY_BMSK                                                                                    0x800
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_LPASS_15_IRQ_OUT_SECURITY_SHFT                                                                                      0xb
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR2_NOC_SMMU_MMU_GIRPT_BMSK                                                                                     0x400
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR2_NOC_SMMU_MMU_GIRPT_SHFT                                                                                       0xa
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR1_NOC_SMMU_MMU_GIRPT_BMSK                                                                                     0x200
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR1_NOC_SMMU_MMU_GIRPT_SHFT                                                                                       0x9
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR0_NOC_SMMU_MMU_GIRPT_BMSK                                                                                     0x100
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_AGGR0_NOC_SMMU_MMU_GIRPT_SHFT                                                                                       0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_DMA_MMU_GIRPT_BMSK                                                                                            0x80
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_DMA_MMU_GIRPT_SHFT                                                                                             0x7
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_OGPU_MMU_GIRPT_BMSK                                                                                                0x40
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_OGPU_MMU_GIRPT_SHFT                                                                                                 0x6
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_MMU_GIRPT_BMSK                                                                                                0x20
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_MDSS_MMU_GIRPT_SHFT                                                                                                 0x5
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_JPEG_MMU_GIRPT_BMSK                                                                                          0x10
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_JPEG_MMU_GIRPT_SHFT                                                                                           0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_VFE_MMU_GIRPT_BMSK                                                                                            0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_VFE_MMU_GIRPT_SHFT                                                                                            0x3
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_CPP_MMU_GIRPT_BMSK                                                                                            0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_CAMSS_CPP_MMU_GIRPT_SHFT                                                                                            0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_VENUS0_MMU_GIRPT_BMSK                                                                                               0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_VENUS0_MMU_GIRPT_SHFT                                                                                               0x1

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00004050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK                                                                                                   0x3dff
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_PIMEM_VMIDMT_GIRPT_ENABLE_BMSK                                                                         0x2000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_PIMEM_VMIDMT_GIRPT_ENABLE_SHFT                                                                            0xd
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SPDM_VMID_GIRPT_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SPDM_VMID_GIRPT_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GIRPT_ENABLE_BMSK                                                                       0x800
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GIRPT_ENABLE_SHFT                                                                         0xb
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GIRPT_ENABLE_BMSK                                                                     0x400
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GIRPT_ENABLE_SHFT                                                                       0xa
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP2_VMIDMT_GIRPT_ENABLE_BMSK                                                                          0x100
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP2_VMIDMT_GIRPT_ENABLE_SHFT                                                                            0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_BMSK                                                                           0x80
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_SHFT                                                                            0x7
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_BMSK                                                                        0x40
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_SHFT                                                                         0x6
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GIRPT_ENABLE_BMSK                                                                        0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GIRPT_ENABLE_SHFT                                                                         0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_UFS_VMIDMT_GIRPT_ENABLE_BMSK                                                                             0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_UFS_VMIDMT_GIRPT_ENABLE_SHFT                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_GIRPT_ENABLE_BMSK                                                                      0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_GIRPT_ENABLE_SHFT                                                                      0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT_ENABLE_BMSK                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GIRPT_ENABLE_SHFT                                                                      0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_GIRPT_ENABLE_BMSK                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_GIRPT_ENABLE_SHFT                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_ENABLE_BMSK                                                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_ENABLE_SHFT                                                                          0x0

#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00004054)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RMSK                                                                                                      0x1ffe
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_LPASS_7_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                 0x1000
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_LPASS_7_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                    0xc
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_LPASS_15_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                 0x800
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_LPASS_15_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                   0xb
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_GIRPT_ENABLE_BMSK                                                                       0x400
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_GIRPT_ENABLE_SHFT                                                                         0xa
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_GIRPT_ENABLE_BMSK                                                                       0x200
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_GIRPT_ENABLE_SHFT                                                                         0x9
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_GIRPT_ENABLE_BMSK                                                                       0x100
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_GIRPT_ENABLE_SHFT                                                                         0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_DMA_MMU_GIRPT_ENABLE_BMSK                                                                              0x80
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_DMA_MMU_GIRPT_ENABLE_SHFT                                                                               0x7
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OGPU_MMU_GIRPT_ENABLE_BMSK                                                                                  0x40
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_OGPU_MMU_GIRPT_ENABLE_SHFT                                                                                   0x6
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_MMU_GIRPT_ENABLE_BMSK                                                                                  0x20
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_MDSS_MMU_GIRPT_ENABLE_SHFT                                                                                   0x5
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GIRPT_ENABLE_BMSK                                                                            0x10
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GIRPT_ENABLE_SHFT                                                                             0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GIRPT_ENABLE_BMSK                                                                              0x8
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GIRPT_ENABLE_SHFT                                                                              0x3
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GIRPT_ENABLE_BMSK                                                                              0x4
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GIRPT_ENABLE_SHFT                                                                              0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_VENUS0_MMU_GIRPT_ENABLE_BMSK                                                                                 0x2
#define HWIO_TCSR_SS_MMU_CLIENT_SEC_INTR_ENABLE_VENUS0_MMU_GIRPT_ENABLE_SHFT                                                                                 0x1

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00005000)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK                                                                                                             0x3dff
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_PIMEM_VMIDMT_GCFGIRPT_BMSK                                                                                       0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_PIMEM_VMIDMT_GCFGIRPT_SHFT                                                                                          0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SPDM_VMID_GCFGIRPT_BMSK                                                                                          0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SPDM_VMID_GCFGIRPT_SHFT                                                                                             0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_DAP_VMIDMT_GCFGIRPT_BMSK                                                                                     0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_DAP_VMIDMT_GCFGIRPT_SHFT                                                                                       0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_TRACE_VMIDMT_GCFGIRPT_BMSK                                                                                   0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_TRACE_VMIDMT_GCFGIRPT_SHFT                                                                                     0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP2_VMIDMT_GCFGIRPT_BMSK                                                                                        0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP2_VMIDMT_GCFGIRPT_SHFT                                                                                          0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_BMSK                                                                                         0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_SHFT                                                                                          0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_BMSK                                                                                         0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_SSC_VMIDMT_1_APPS_IRQ_SHFT                                                                                          0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_BAM_VMIDMT_GCFGIRPT_BMSK                                                                                      0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QDSS_BAM_VMIDMT_GCFGIRPT_SHFT                                                                                       0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_UFS_VMIDMT_GCFGIRPT_BMSK                                                                                           0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_UFS_VMIDMT_GCFGIRPT_SHFT                                                                                            0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS1_VBIF_VMIDMT_GCFGIRPT_BMSK                                                                                    0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS1_VBIF_VMIDMT_GCFGIRPT_SHFT                                                                                    0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT_BMSK                                                                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_VENUS0_VBIF_VMIDMT_GCFGIRPT_SHFT                                                                                    0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_CLIENT_INTR_SHFT                                                                                         0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_BMSK                                                                                        0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_SHFT                                                                                        0x0

#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00005004)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RMSK                                                                                                                0x1ffe
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR, HWIO_TCSR_SS_MMU_CFG_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_LPASS_6_IRQ_OUT_SECURITY_BMSK                                                                                       0x1000
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_LPASS_6_IRQ_OUT_SECURITY_SHFT                                                                                          0xc
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_LPASS_14_IRQ_OUT_SECURITY_BMSK                                                                                       0x800
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_LPASS_14_IRQ_OUT_SECURITY_SHFT                                                                                         0xb
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR2_NOC_SMMU_MMU_GCFGIRPT_BMSK                                                                                     0x400
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR2_NOC_SMMU_MMU_GCFGIRPT_SHFT                                                                                       0xa
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR1_NOC_SMMU_MMU_GCFGIRPT_BMSK                                                                                     0x200
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR1_NOC_SMMU_MMU_GCFGIRPT_SHFT                                                                                       0x9
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR0_NOC_SMMU_MMU_GCFGIRPT_BMSK                                                                                     0x100
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_AGGR0_NOC_SMMU_MMU_GCFGIRPT_SHFT                                                                                       0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_DMA_MMU_GCFGIRPT_BMSK                                                                                            0x80
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_DMA_MMU_GCFGIRPT_SHFT                                                                                             0x7
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_OGPU_MMU_GCFGIRPT_BMSK                                                                                                0x40
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_OGPU_MMU_GCFGIRPT_SHFT                                                                                                 0x6
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_MMU_GCFGIRPT_BMSK                                                                                                0x20
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_MDSS_MMU_GCFGIRPT_SHFT                                                                                                 0x5
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_JPEG_MMU_GCFGIRPT_BMSK                                                                                          0x10
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_JPEG_MMU_GCFGIRPT_SHFT                                                                                           0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_VFE_MMU_GCFGIRPT_BMSK                                                                                            0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_VFE_MMU_GCFGIRPT_SHFT                                                                                            0x3
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_CPP_MMU_GCFGIRPT_BMSK                                                                                            0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_CAMSS_CPP_MMU_GCFGIRPT_SHFT                                                                                            0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_VENUS0_MMU_GCFGIRPT_BMSK                                                                                               0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_VENUS0_MMU_GCFGIRPT_SHFT                                                                                               0x1

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00005040)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK                                                                                                      0x3dff
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_PIMEM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                         0x2000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_PIMEM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                            0xd
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SPDM_VMID_GCFGIRPT_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SPDM_VMID_GCFGIRPT_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                       0x800
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_DAP_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                         0xb
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                     0x400
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_TRACE_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                       0xa
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP2_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                          0x100
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP2_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                            0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                           0x80
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                            0x7
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_BMSK                                                                           0x40
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_SSC_VMIDMT_0_APPS_IRQ_ENABLE_SHFT                                                                            0x6
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                        0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QDSS_BAM_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                         0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_UFS_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                             0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_UFS_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                              0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                      0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS1_VBIF_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                      0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                      0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_VENUS0_VBIF_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                      0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_GCFGIRPT_ENABLE_BMSK                                                                       0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_CLIENT_GCFGIRPT_ENABLE_SHFT                                                                       0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_ENABLE_BMSK                                                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_ENABLE_SHFT                                                                          0x0

#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00005044)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RMSK                                                                                                         0x1ffe
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_LPASS_6_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                    0x1000
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_LPASS_6_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                       0xc
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_LPASS_14_IRQ_OUT_SECURITY_INTR_ENABLE_BMSK                                                                    0x800
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_LPASS_14_IRQ_OUT_SECURITY_INTR_ENABLE_SHFT                                                                      0xb
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_GCFGIRPT_ENABLE_BMSK                                                                       0x400
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR2_NOC_SMMU_MMU_GCFGIRPT_ENABLE_SHFT                                                                         0xa
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_GCFGIRPT_ENABLE_BMSK                                                                       0x200
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR1_NOC_SMMU_MMU_GCFGIRPT_ENABLE_SHFT                                                                         0x9
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_GCFGIRPT_ENABLE_BMSK                                                                       0x100
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_AGGR0_NOC_SMMU_MMU_GCFGIRPT_ENABLE_SHFT                                                                         0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_DMA_MMU_GCFGIRPT_ENABLE_BMSK                                                                              0x80
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_DMA_MMU_GCFGIRPT_ENABLE_SHFT                                                                               0x7
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OGPU_MMU_GCFGIRPT_ENABLE_BMSK                                                                                  0x40
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_OGPU_MMU_GCFGIRPT_ENABLE_SHFT                                                                                   0x6
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_MMU_GCFGIRPT_ENABLE_BMSK                                                                                  0x20
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_MDSS_MMU_GCFGIRPT_ENABLE_SHFT                                                                                   0x5
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GCFGIRPT_ENABLE_BMSK                                                                            0x10
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_JPEG_MMU_GCFGIRPT_ENABLE_SHFT                                                                             0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GCFGIRPT_ENABLE_BMSK                                                                              0x8
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_VFE_MMU_GCFGIRPT_ENABLE_SHFT                                                                              0x3
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GCFGIRPT_ENABLE_BMSK                                                                              0x4
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_CAMSS_CPP_MMU_GCFGIRPT_ENABLE_SHFT                                                                              0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_VENUS0_MMU_GCFGIRPT_ENABLE_BMSK                                                                                 0x2
#define HWIO_TCSR_SS_MMU_CFG_SEC_INTR_ENABLE_VENUS0_MMU_GCFGIRPT_ENABLE_SHFT                                                                                 0x1

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00006000)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_QDSS_MPU_CFG_XPU2_MSA_INTR_BMSK                                                                                   0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_QDSS_MPU_CFG_XPU2_MSA_INTR_SHFT                                                                                         0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BOOT_ROM_MSA_INTR_BMSK                                                                                            0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BOOT_ROM_MSA_INTR_SHFT                                                                                                  0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_NOC_CFG_XPU2_MSA_INTR_BMSK                                                                                        0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_NOC_CFG_XPU2_MSA_INTR_SHFT                                                                                              0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_LPASS_IRQ_OUT_SECURITY_2_BMSK                                                                                     0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_LPASS_IRQ_OUT_SECURITY_2_SHFT                                                                                           0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_BMSK                                                                                             0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_SHFT                                                                                                  0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPDM_XPU_MSA_INTR_BMSK                                                                                             0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SPDM_XPU_MSA_INTR_SHFT                                                                                                  0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_XPU2_MSA_INTR_BMSK                                                                                        0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_XPU2_MSA_INTR_SHFT                                                                                             0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_UFS_ICE_XPU2_MSA_INTR_BMSK                                                                                         0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_UFS_ICE_XPU2_MSA_INTR_SHFT                                                                                              0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_VENUS_WRAPPER_XPU2_MSA_INTERRUPT_BMSK                                                                               0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_VENUS_WRAPPER_XPU2_MSA_INTERRUPT_SHFT                                                                                   0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH1_XPU2_MSA_INTERRUPT_BMSK                                                                                    0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH1_XPU2_MSA_INTERRUPT_SHFT                                                                                        0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_BMSK                                                                                    0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_SHFT                                                                                        0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CFG_XPU2_MSA_INTERRUPT_BMSK                                                                                    0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CFG_XPU2_MSA_INTERRUPT_SHFT                                                                                        0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_BMSK                                                                                                0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_SHFT                                                                                                   0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_UFS_XPU_MSA_INTR_BMSK                                                                                                0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_UFS_XPU_MSA_INTR_SHFT                                                                                                   0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A2NOC_MPU_CFG_XPU2_MSA_INTR_BMSK                                                                                     0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A2NOC_MPU_CFG_XPU2_MSA_INTR_SHFT                                                                                        0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A1NOC_MPU_CFG_XPU2_MSA_INTR_BMSK                                                                                     0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A1NOC_MPU_CFG_XPU2_MSA_INTR_SHFT                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A0NOC_MPU_CFG_XPU2_MSA_INTR_BMSK                                                                                      0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_A0NOC_MPU_CFG_XPU2_MSA_INTR_SHFT                                                                                         0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PIMEM_MPU_MSA_IRQ_BMSK                                                                                                0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PIMEM_MPU_MSA_IRQ_SHFT                                                                                                   0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PIMEM_APU_MSA_IRQ_BMSK                                                                                                0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PIMEM_APU_MSA_IRQ_SHFT                                                                                                   0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_0_BMSK                                                                                               0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_0_SHFT                                                                                                  0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_10_BMSK                                                                                               0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_10_SHFT                                                                                                 0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_3_BMSK                                                                                                0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SSC_XPU_IRQ_APPS_3_SHFT                                                                                                  0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SEC_CTRL_XPU2_MSA_INTR_BMSK                                                                                            0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SEC_CTRL_XPU2_MSA_INTR_SHFT                                                                                              0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DCC_XPU2_MSA_INTR_BMSK                                                                                                 0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DCC_XPU2_MSA_INTR_SHFT                                                                                                   0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_RPU_MSA_INTR_BMSK                                                                                                0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_RPU_MSA_INTR_SHFT                                                                                                 0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CRYPTO0_BAM_XPU2_MSA_INTR_BMSK                                                                                          0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CRYPTO0_BAM_XPU2_MSA_INTR_SHFT                                                                                           0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_MUTEX_MSA_INTR_BMSK                                                                                              0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_MUTEX_MSA_INTR_SHFT                                                                                               0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_COPSS_MSA_IRQ_BMSK                                                                                                      0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_COPSS_MSA_IRQ_SHFT                                                                                                       0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_REGS_MSA_INTR_BMSK                                                                                                0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_REGS_MSA_INTR_SHFT                                                                                                0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MMSS_NOC_XPU2_MSA_INTR_BMSK                                                                                              0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MMSS_NOC_XPU2_MSA_INTR_SHFT                                                                                              0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DSA_XPU2_MSA_INTR_BMSK                                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DSA_XPU2_MSA_INTR_SHFT                                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SDC1_XPU2_MSA_INTR_BMSK                                                                                                  0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SDC1_XPU2_MSA_INTR_SHFT                                                                                                  0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00006004)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RMSK                                                                                                                   0x3ff
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR1_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RPM_MPU_MSA_INTR_BMSK                                                                                                  0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RPM_MPU_MSA_INTR_SHFT                                                                                                    0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_LPASS_IRQ_OUT_SECURIT_10_BMSK                                                                                          0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_LPASS_IRQ_OUT_SECURIT_10_SHFT                                                                                            0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MDSS_XPU2_MSA_INTR_BMSK                                                                                                 0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MDSS_XPU2_MSA_INTR_SHFT                                                                                                  0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_QDSS_BAM_XPU2_MSA_INTR_BMSK                                                                                             0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_QDSS_BAM_XPU2_MSA_INTR_SHFT                                                                                              0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MPM_XPU2_MSA_INTR_BMSK                                                                                                  0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_MPM_XPU2_MSA_INTR_SHFT                                                                                                   0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDCC2_XPU2_MSA_INTR_BMSK                                                                                                0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_SDCC2_XPU2_MSA_INTR_SHFT                                                                                                 0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RBCPR_APU_XPU2_MSA_INTR_BMSK                                                                                             0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_RBCPR_APU_XPU2_MSA_INTR_SHFT                                                                                             0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP2_XPU2_MSA_INTR_BMSK                                                                                                 0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP2_XPU2_MSA_INTR_SHFT                                                                                                 0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP1_XPU2_MSA_INTR_BMSK                                                                                                 0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_BLSP1_XPU2_MSA_INTR_SHFT                                                                                                 0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCIMEM_RPU_MSA_INTR_BMSK                                                                                                 0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_OCIMEM_RPU_MSA_INTR_SHFT                                                                                                 0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00006040)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK                                                                                                       0xffffffff
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_QDSS_MPU_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                     0x80000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_QDSS_MPU_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                           0x1f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_BMSK                                                                              0x40000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_SHFT                                                                                    0x1e
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_NOC_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                          0x20000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_NOC_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x1d
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_2_INTR_ENABLE_BMSK                                                                  0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_LPASS_IRQ_OUT_SECURITY_2_INTR_ENABLE_SHFT                                                                        0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_BMSK                                                                               0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_SHFT                                                                                    0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPDM_XPU_MSA_INTR_ENABLE_BMSK                                                                               0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SPDM_XPU_MSA_INTR_ENABLE_SHFT                                                                                    0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_XPU2_MSA_INTR_ENABLE_BMSK                                                                          0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_UFS_ICE_XPU2_MSA_INTR_ENABLE_BMSK                                                                           0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_UFS_ICE_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_VENUS_WRAPPER_XPU2_MSA_INTR_ENABLE_BMSK                                                                      0x800000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_VENUS_WRAPPER_XPU2_MSA_INTR_ENABLE_SHFT                                                                          0x17
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH1_XPU2_MSA_INTR_ENABLE_BMSK                                                                           0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH1_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTR_ENABLE_BMSK                                                                           0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                           0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_BMSK                                                                                  0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_SHFT                                                                                     0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_UFS_XPU_MSA_INTR_ENABLE_BMSK                                                                                  0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_UFS_XPU_MSA_INTR_ENABLE_SHFT                                                                                     0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                       0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A2NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                          0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                       0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A1NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                          0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                                                        0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_A0NOC_MPU_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                                                           0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PIMEM_MPU_MSA_INTR_ENABLE_BMSK                                                                                 0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PIMEM_MPU_MSA_INTR_ENABLE_SHFT                                                                                    0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PIMEM_APU_MSA_INTR_ENABLE_BMSK                                                                                 0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PIMEM_APU_MSA_INTR_ENABLE_SHFT                                                                                    0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_0_INTR_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_0_INTR_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_10_INTR_ENABLE_BMSK                                                                            0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_10_INTR_ENABLE_SHFT                                                                              0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_3_INTR_ENABLE_BMSK                                                                             0x400
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SSC_XPU_IRQ_APPS_3_INTR_ENABLE_SHFT                                                                               0xa
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SEC_CTRL_XPU2_MSA_INTR_ENABLE_BMSK                                                                              0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SEC_CTRL_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DCC_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DCC_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_BMSK                                                                                  0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_SHFT                                                                                   0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                            0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                             0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_MUTEX_MSA_INTR_ENABLE_BMSK                                                                                0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_MUTEX_MSA_INTR_ENABLE_SHFT                                                                                 0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_COPSS_MSA_IRQ_ENABLE_BMSK                                                                                        0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_COPSS_MSA_IRQ_ENABLE_SHFT                                                                                         0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_BMSK                                                                                  0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_SHFT                                                                                  0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MMSS_NOC_XPU2_MSA_INTR_EN_BMSK                                                                                    0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MMSS_NOC_XPU2_MSA_INTR_EN_SHFT                                                                                    0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DSA_XPU2_MSA_INTR_EN_BMSK                                                                                         0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DSA_XPU2_MSA_INTR_EN_SHFT                                                                                         0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SDC1_XPU2_MSA_INTR_EN_BMSK                                                                                        0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SDC1_XPU2_MSA_INTR_EN_SHFT                                                                                        0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00006044)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RMSK                                                                                                            0x3ff
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RPM_MPU_MSA_INTR_ENABLE_BMSK                                                                                    0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RPM_MPU_MSA_INTR_ENABLE_SHFT                                                                                      0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_10_INTR_ENABLE_BMSK                                                                       0x100
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_LPASS_IRQ_OUT_SECURIT_10_INTR_ENABLE_SHFT                                                                         0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MDSS_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x80
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MDSS_XPU2_MSA_INTR_ENABLE_SHFT                                                                                    0x7
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_QDSS_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_QDSS_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDCC2_XPU2_MSA_INTR_ENABLE_BMSK                                                                                  0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_SDCC2_XPU2_MSA_INTR_ENABLE_SHFT                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RBCPR_APU_XPU2_MSA_INTR_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_RBCPR_APU_XPU2_MSA_INTR_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP2_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP2_XPU2_MSA_INTR_ENABLE_SHFT                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_BMSK                                                                                   0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR1_ENABLE_OCIMEM_RPU_MSA_INTR_ENABLE_SHFT                                                                                   0x0

#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007000)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK                                                                                                                  0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, m)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,v)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,m,v,HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_BMSK                                                                                             0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_SHFT                                                                                                0x0

#define HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00007004)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, m)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,v)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,m,v,HWIO_TCSR_SPDM_DLY_FIFO_EN_IN)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_SHFT                                                                                                     0x0

#define HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007008)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK                                                                                                                  0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG1_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_BMSK                                                                                                0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_SHFT                                                                                                   0x0

#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000700c)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_SHFT                                                                                               0x0

#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00007010)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_SHFT                                                                                               0x0

#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00007014)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_SHFT                                                                                               0x0

#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00007018)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK                                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_BMSK                                                                                        0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_SHFT                                                                                               0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000701c)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_SPDM_WRP_RT_INTF_CTL0_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL0_SPDM_WRP_RT_INTF_CTL0_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007020)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_SPDM_WRP_RT_INTF_CTL1_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL1_SPDM_WRP_RT_INTF_CTL1_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007024)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_SPDM_WRP_RT_INTF_CTL2_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL2_SPDM_WRP_RT_INTF_CTL2_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007028)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_SPDM_WRP_RT_INTF_CTL3_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL3_SPDM_WRP_RT_INTF_CTL3_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000702c)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_SPDM_WRP_RT_INTF_CTL4_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL4_SPDM_WRP_RT_INTF_CTL4_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007030)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_SPDM_WRP_RT_INTF_CTL5_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL5_SPDM_WRP_RT_INTF_CTL5_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007034)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_SPDM_WRP_RT_INTF_CTL6_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL6_SPDM_WRP_RT_INTF_CTL6_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007038)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_SPDM_WRP_RT_INTF_CTL7_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL7_SPDM_WRP_RT_INTF_CTL7_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000703c)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_SPDM_WRP_RT_INTF_CTL8_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL8_SPDM_WRP_RT_INTF_CTL8_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00007040)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_SPDM_WRP_RT_INTF_CTL9_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL9_SPDM_WRP_RT_INTF_CTL9_SHFT                                                                                           0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007044)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_SPDM_WRP_RT_INTF_CTL10_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL10_SPDM_WRP_RT_INTF_CTL10_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007048)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_SPDM_WRP_RT_INTF_CTL11_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL11_SPDM_WRP_RT_INTF_CTL11_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000704c)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_SPDM_WRP_RT_INTF_CTL12_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL12_SPDM_WRP_RT_INTF_CTL12_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007050)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_SPDM_WRP_RT_INTF_CTL13_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL13_SPDM_WRP_RT_INTF_CTL13_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007054)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_SPDM_WRP_RT_INTF_CTL14_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL14_SPDM_WRP_RT_INTF_CTL14_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007058)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_ADDR, HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_IN)
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_SPDM_WRP_RT_INTF_CTL15_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_INTF_CTL15_SPDM_WRP_RT_INTF_CTL15_SHFT                                                                                         0x0

#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000705c)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL0_ADDR, HWIO_TCSR_SPDM_WRP_CTI_CTL0_RMSK)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL0_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_CTI_CTL0_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_CTI_CTL0_ADDR,m,v,HWIO_TCSR_SPDM_WRP_CTI_CTL0_IN)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_SPDM_WRP_CTI_CTL0_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL0_SPDM_WRP_CTI_CTL0_SHFT                                                                                                   0x0

#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007060)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL1_ADDR, HWIO_TCSR_SPDM_WRP_CTI_CTL1_RMSK)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL1_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_CTI_CTL1_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_CTI_CTL1_ADDR,m,v,HWIO_TCSR_SPDM_WRP_CTI_CTL1_IN)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_SPDM_WRP_CTI_CTL1_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL1_SPDM_WRP_CTI_CTL1_SHFT                                                                                                   0x0

#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007064)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL2_ADDR, HWIO_TCSR_SPDM_WRP_CTI_CTL2_RMSK)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL2_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_CTI_CTL2_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_CTI_CTL2_ADDR,m,v,HWIO_TCSR_SPDM_WRP_CTI_CTL2_IN)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_SPDM_WRP_CTI_CTL2_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL2_SPDM_WRP_CTI_CTL2_SHFT                                                                                                   0x0

#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007068)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_RMSK                                                                                                              0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL3_ADDR, HWIO_TCSR_SPDM_WRP_CTI_CTL3_RMSK)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL3_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_CTI_CTL3_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_CTI_CTL3_ADDR,m,v,HWIO_TCSR_SPDM_WRP_CTI_CTL3_IN)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_SPDM_WRP_CTI_CTL3_BMSK                                                                                            0xffffffff
#define HWIO_TCSR_SPDM_WRP_CTI_CTL3_SPDM_WRP_CTI_CTL3_SHFT                                                                                                   0x0

#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000706c)
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_ADDR, HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_RMSK)
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_ADDR,m,v,HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_IN)
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_SPDM_WRP_CTL_COMMON_BMSK                                                                                     0xffffffff
#define HWIO_TCSR_SPDM_WRP_RT_CTL_COMMON_SPDM_WRP_CTL_COMMON_SHFT                                                                                            0x0

#define HWIO_TCSR_SPDM_WRP_CTI_CTL_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00007070)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_RMSK                                                                                                                      0x7
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL_ADDR, HWIO_TCSR_SPDM_WRP_CTI_CTL_RMSK)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_WRP_CTI_CTL_ADDR, m)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_WRP_CTI_CTL_ADDR,v)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_WRP_CTI_CTL_ADDR,m,v,HWIO_TCSR_SPDM_WRP_CTI_CTL_IN)
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_SPDM_WRP_CTI_CTL_BMSK                                                                                                     0x7
#define HWIO_TCSR_SPDM_WRP_CTI_CTL_SPDM_WRP_CTI_CTL_SHFT                                                                                                     0x0

#define HWIO_TCSR_SOC_HW_VERSION_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00008000)
#define HWIO_TCSR_SOC_HW_VERSION_RMSK                                                                                                                 0xffffffff
#define HWIO_TCSR_SOC_HW_VERSION_IN          \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, HWIO_TCSR_SOC_HW_VERSION_RMSK)
#define HWIO_TCSR_SOC_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, m)
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_BMSK                                                                                                   0xf0000000
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_SHFT                                                                                                         0x1c
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_BMSK                                                                                                    0xfff0000
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_SHFT                                                                                                         0x10
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_BMSK                                                                                                       0xff00
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_SHFT                                                                                                          0x8
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_BMSK                                                                                                         0xff
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_SHFT                                                                                                          0x0

#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00008020)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK                                                                                                            0x1f6ffbff
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S9_IRQ_BMSK                                                                                    0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S9_IRQ_SHFT                                                                                          0x1c
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S8_IRQ_BMSK                                                                                     0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S8_IRQ_SHFT                                                                                          0x1b
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S1_IRQ_BMSK                                                                                     0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S1_IRQ_SHFT                                                                                          0x1a
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS3_BUS_TIMEOUT_IRQ_BMSK                                                                                   0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS3_BUS_TIMEOUT_IRQ_SHFT                                                                                        0x19
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS4_BUS_TIMEOUT_IRQ_BMSK                                                                                   0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS4_BUS_TIMEOUT_IRQ_SHFT                                                                                        0x18
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT1_BMSK                                                                                   0x400000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT1_SHFT                                                                                       0x16
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT0_BMSK                                                                                   0x200000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_LPASS_IRQ_OUT_AHB_TIMEOUT0_SHFT                                                                                       0x15
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS2_BUS_TIMEOUT_IRQ_BMSK                                                                                     0x80000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS2_BUS_TIMEOUT_IRQ_SHFT                                                                                        0x13
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS1_BUS_TIMEOUT_IRQ_BMSK                                                                                     0x40000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS1_BUS_TIMEOUT_IRQ_SHFT                                                                                        0x12
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS_BUS_TIMEOUT_IRQ_BMSK                                                                                      0x20000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_QHS_MMSS_BUS_TIMEOUT_IRQ_SHFT                                                                                         0x11
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S7_IRQ_BMSK                                                                                       0x10000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S7_IRQ_SHFT                                                                                          0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_4_IRQ_BMSK                                                                                        0x8000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_4_IRQ_SHFT                                                                                           0xf
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_3_IRQ_BMSK                                                                                        0x4000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_3_IRQ_SHFT                                                                                           0xe
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_2_IRQ_BMSK                                                                                        0x2000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_2_IRQ_SHFT                                                                                           0xd
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_1_IRQ_BMSK                                                                                        0x1000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_1_IRQ_SHFT                                                                                           0xc
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_0_IRQ_BMSK                                                                                         0x800
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_COPSS_BUS_TIMEOUT_0_IRQ_SHFT                                                                                           0xb
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_0_BMSK                                                                                       0x200
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_0_SHFT                                                                                         0x9
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S2_IRQ_BMSK                                                                                         0x100
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_SNOC_S2_IRQ_SHFT                                                                                           0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_1_BMSK                                                                                        0x80
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SSC_IRQ_OUT_AHB_TIMEOUT_1_SHFT                                                                                         0x7
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S6_IRQ_BMSK                                                                                          0x40
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S6_IRQ_SHFT                                                                                           0x6
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S5_IRQ_BMSK                                                                                          0x20
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S5_IRQ_SHFT                                                                                           0x5
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S4_IRQ_BMSK                                                                                          0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S4_IRQ_SHFT                                                                                           0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S3_IRQ_BMSK                                                                                           0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S3_IRQ_SHFT                                                                                           0x3
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S2_IRQ_BMSK                                                                                           0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S2_IRQ_SHFT                                                                                           0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S1_IRQ_BMSK                                                                                           0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S1_IRQ_SHFT                                                                                           0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S0_IRQ_BMSK                                                                                           0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_BUS_TIMEOUT_CNOC_S0_IRQ_SHFT                                                                                           0x0

#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00008030)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK                                                                                                        0x1f6ffbff
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_BMSK                                                                         0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_SHFT                                                                               0x1c
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_BMSK                                                                          0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_SHFT                                                                               0x1b
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                          0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                               0x1a
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x19
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x18
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_BMSK                                                                        0x400000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_SHFT                                                                            0x16
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_BMSK                                                                        0x200000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_SHFT                                                                            0x15
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                          0x80000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x13
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                          0x40000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x12
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                           0x20000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                              0x11
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                            0x10000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                               0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                             0x8000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                0xf
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                             0x4000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                0xe
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                             0x2000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                0xd
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                             0x1000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                0xc
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                              0x800
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                0xb
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_BMSK                                                                     0x200
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_SHFT                                                                       0x9
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                              0x100
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_BMSK                                                                      0x80
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_SHFT                                                                       0x7
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                               0x20
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                0x5
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                               0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                0x3
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                0x0

#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_ADDR                                                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00008040)
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_RMSK                                                                                                       0x1f6ffbff
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_BMSK                                                                        0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_SHFT                                                                              0x1c
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_BMSK                                                                         0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_SHFT                                                                              0x1b
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                         0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                              0x1a
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                       0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                            0x19
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                       0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                            0x18
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_BMSK                                                                       0x400000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_SHFT                                                                           0x16
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_BMSK                                                                       0x200000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_SHFT                                                                           0x15
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                         0x80000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                            0x13
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                         0x40000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                            0x12
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                          0x20000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x11
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                           0x10000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                              0x10
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                            0x8000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                               0xf
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                            0x4000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                               0xe
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                            0x2000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                               0xd
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                            0x1000
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                               0xc
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                             0x800
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                               0xb
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_BMSK                                                                    0x200
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_SHFT                                                                      0x9
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                             0x100
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                               0x8
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_BMSK                                                                     0x80
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_SHFT                                                                      0x7
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                              0x40
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                               0x6
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                              0x20
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                               0x5
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                              0x10
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                               0x4
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                               0x4
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                               0x2
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                               0x2
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                               0x1
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                               0x1
#define HWIO_TCSR_TIMEOUT_INTR_HMSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                               0x0

#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00008050)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_RMSK                                                                                                      0x1f6ffbff
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_BMSK                                                                       0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_SHFT                                                                             0x1c
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_BMSK                                                                        0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_SHFT                                                                             0x1b
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                        0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                             0x1a
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                      0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                           0x19
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                      0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                           0x18
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_BMSK                                                                      0x400000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_SHFT                                                                          0x16
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_BMSK                                                                      0x200000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_SHFT                                                                          0x15
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x80000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                           0x13
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x40000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                           0x12
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                         0x20000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                            0x11
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                          0x10000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                             0x10
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                           0x8000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                              0xf
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                           0x4000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                              0xe
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                           0x2000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                              0xd
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                           0x1000
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                              0xc
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                            0x800
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                              0xb
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_BMSK                                                                   0x200
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_SHFT                                                                     0x9
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                            0x100
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                              0x8
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_BMSK                                                                    0x80
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_SHFT                                                                     0x7
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                             0x40
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                              0x6
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                             0x20
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                              0x5
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                             0x10
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                              0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                              0x8
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                              0x3
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                              0x4
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                              0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                              0x2
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                              0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                              0x1
#define HWIO_TCSR_TIMEOUT_INTR_LPASS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                              0x0

#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00008060)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK                                                                                                        0x1f6ffbff
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_BMSK                                                                         0x10000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S9_IRQ_ENABLE_SHFT                                                                               0x1c
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_BMSK                                                                          0x8000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S8_IRQ_ENABLE_SHFT                                                                               0x1b
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_BMSK                                                                          0x4000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S1_IRQ_ENABLE_SHFT                                                                               0x1a
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x2000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS3_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x19
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                        0x1000000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS4_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x18
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_BMSK                                                                        0x400000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT1_SHFT                                                                            0x16
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_BMSK                                                                        0x200000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_LPASS_IRQ_ENABLE_OUT_AHB_TIMEOUT0_SHFT                                                                            0x15
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                          0x80000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS2_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x13
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                          0x40000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS1_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                             0x12
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_BMSK                                                                           0x20000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_QHS_MMSS_BUS_TIMEOUT_IRQ_ENABLE_SHFT                                                                              0x11
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_BMSK                                                                            0x10000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S7_IRQ_ENABLE_SHFT                                                                               0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_BMSK                                                                             0x8000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_4_IRQ_ENABLE_SHFT                                                                                0xf
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_BMSK                                                                             0x4000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_3_IRQ_ENABLE_SHFT                                                                                0xe
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_BMSK                                                                             0x2000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_2_IRQ_ENABLE_SHFT                                                                                0xd
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_BMSK                                                                             0x1000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_1_IRQ_ENABLE_SHFT                                                                                0xc
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_BMSK                                                                              0x800
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_COPSS_BUS_TIMEOUT_0_IRQ_ENABLE_SHFT                                                                                0xb
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_BMSK                                                                     0x200
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_0_ENABLE_SHFT                                                                       0x9
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_BMSK                                                                              0x100
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_SNOC_S2_IRQ_ENABLE_SHFT                                                                                0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_BMSK                                                                      0x80
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SSC_IRQ_ENABLE_OUT_AHB_TIMEOUT_1_ENABLE_SHFT                                                                       0x7
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_BMSK                                                                               0x40
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S6_IRQ_ENABLE_SHFT                                                                                0x6
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_BMSK                                                                               0x20
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S5_IRQ_ENABLE_SHFT                                                                                0x5
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_BMSK                                                                               0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S4_IRQ_ENABLE_SHFT                                                                                0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_BMSK                                                                                0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S3_IRQ_ENABLE_SHFT                                                                                0x3
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_BMSK                                                                                0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S2_IRQ_ENABLE_SHFT                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_BMSK                                                                                0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S1_IRQ_ENABLE_SHFT                                                                                0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_BMSK                                                                                0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_BUS_TIMEOUT_CNOC_S0_IRQ_ENABLE_SHFT                                                                                0x0

#define HWIO_TCSR_TCSR_CLK_EN_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000907c)
#define HWIO_TCSR_TCSR_CLK_EN_RMSK                                                                                                                           0x1
#define HWIO_TCSR_TCSR_CLK_EN_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, HWIO_TCSR_TCSR_CLK_EN_RMSK)
#define HWIO_TCSR_TCSR_CLK_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, m)
#define HWIO_TCSR_TCSR_CLK_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_CLK_EN_ADDR,v)
#define HWIO_TCSR_TCSR_CLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_CLK_EN_ADDR,m,v,HWIO_TCSR_TCSR_CLK_EN_IN)
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_BMSK                                                                                                               0x1
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_SHFT                                                                                                               0x0

#define HWIO_TCSR_SYS_POWER_CTRL_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000a000)
#define HWIO_TCSR_SYS_POWER_CTRL_RMSK                                                                                                                     0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, HWIO_TCSR_SYS_POWER_CTRL_RMSK)
#define HWIO_TCSR_SYS_POWER_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, m)
#define HWIO_TCSR_SYS_POWER_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SYS_POWER_CTRL_ADDR,v)
#define HWIO_TCSR_SYS_POWER_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SYS_POWER_CTRL_ADDR,m,v,HWIO_TCSR_SYS_POWER_CTRL_IN)
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_BMSK                                                                                                      0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_SHFT                                                                                                         0x0

#define HWIO_TCSR_USB_CORE_ID_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000a004)
#define HWIO_TCSR_USB_CORE_ID_RMSK                                                                                                                           0x3
#define HWIO_TCSR_USB_CORE_ID_IN          \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, HWIO_TCSR_USB_CORE_ID_RMSK)
#define HWIO_TCSR_USB_CORE_ID_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, m)
#define HWIO_TCSR_USB_CORE_ID_OUT(v)      \
        out_dword(HWIO_TCSR_USB_CORE_ID_ADDR,v)
#define HWIO_TCSR_USB_CORE_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_CORE_ID_ADDR,m,v,HWIO_TCSR_USB_CORE_ID_IN)
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_BMSK                                                                                                               0x3
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_SHFT                                                                                                               0x0

#define HWIO_TCSR_SPARE_REG0_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a044)
#define HWIO_TCSR_SPARE_REG0_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_SPARE_REG0_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, HWIO_TCSR_SPARE_REG0_RMSK)
#define HWIO_TCSR_SPARE_REG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, m)
#define HWIO_TCSR_SPARE_REG0_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG0_ADDR,v)
#define HWIO_TCSR_SPARE_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG0_ADDR,m,v,HWIO_TCSR_SPARE_REG0_IN)
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_BMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_REG1_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a048)
#define HWIO_TCSR_SPARE_REG1_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_SPARE_REG1_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, HWIO_TCSR_SPARE_REG1_RMSK)
#define HWIO_TCSR_SPARE_REG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, m)
#define HWIO_TCSR_SPARE_REG1_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG1_ADDR,v)
#define HWIO_TCSR_SPARE_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG1_ADDR,m,v,HWIO_TCSR_SPARE_REG1_IN)
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_BMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_REG2_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a04c)
#define HWIO_TCSR_SPARE_REG2_RMSK                                                                                                                           0xff
#define HWIO_TCSR_SPARE_REG2_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, HWIO_TCSR_SPARE_REG2_RMSK)
#define HWIO_TCSR_SPARE_REG2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, m)
#define HWIO_TCSR_SPARE_REG2_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG2_ADDR,v)
#define HWIO_TCSR_SPARE_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG2_ADDR,m,v,HWIO_TCSR_SPARE_REG2_IN)
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_BMSK                                                                                                                0xff
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_REG3_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a050)
#define HWIO_TCSR_SPARE_REG3_RMSK                                                                                                                           0xff
#define HWIO_TCSR_SPARE_REG3_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, HWIO_TCSR_SPARE_REG3_RMSK)
#define HWIO_TCSR_SPARE_REG3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, m)
#define HWIO_TCSR_SPARE_REG3_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG3_ADDR,v)
#define HWIO_TCSR_SPARE_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG3_ADDR,m,v,HWIO_TCSR_SPARE_REG3_IN)
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_BMSK                                                                                                                0xff
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_REG4_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a054)
#define HWIO_TCSR_SPARE_REG4_RMSK                                                                                                                           0xff
#define HWIO_TCSR_SPARE_REG4_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG4_ADDR, HWIO_TCSR_SPARE_REG4_RMSK)
#define HWIO_TCSR_SPARE_REG4_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG4_ADDR, m)
#define HWIO_TCSR_SPARE_REG4_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG4_ADDR,v)
#define HWIO_TCSR_SPARE_REG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG4_ADDR,m,v,HWIO_TCSR_SPARE_REG4_IN)
#define HWIO_TCSR_SPARE_REG4_SPARE_REG4_BMSK                                                                                                                0xff
#define HWIO_TCSR_SPARE_REG4_SPARE_REG4_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_REG5_ADDR                                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a058)
#define HWIO_TCSR_SPARE_REG5_RMSK                                                                                                                           0xff
#define HWIO_TCSR_SPARE_REG5_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG5_ADDR, HWIO_TCSR_SPARE_REG5_RMSK)
#define HWIO_TCSR_SPARE_REG5_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG5_ADDR, m)
#define HWIO_TCSR_SPARE_REG5_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG5_ADDR,v)
#define HWIO_TCSR_SPARE_REG5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG5_ADDR,m,v,HWIO_TCSR_SPARE_REG5_IN)
#define HWIO_TCSR_SPARE_REG5_SPARE_REG5_BMSK                                                                                                                0xff
#define HWIO_TCSR_SPARE_REG5_SPARE_REG5_SHFT                                                                                                                 0x0

#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000a05c)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR, HWIO_TCSR_SPARE_QGIC_INTERRUPTS_RMSK)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR, m)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR,v)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_QGIC_INTERRUPTS_ADDR,m,v,HWIO_TCSR_SPARE_QGIC_INTERRUPTS_IN)
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_SPARE_QGIC_INTERRUPTS_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_SPARE_QGIC_INTERRUPTS_SPARE_QGIC_INTERRUPTS_SHFT                                                                                           0x0

#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000a070)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_RMSK                                                                                                                     0x1
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR, HWIO_TCSR_UFS_SCM_FAULT_IRQ_RMSK)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SCM_FAULT_IRQ_ADDR, m)
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_UFS_SCM_FAULT_IRQ_BMSK                                                                                                   0x1
#define HWIO_TCSR_UFS_SCM_FAULT_IRQ_UFS_SCM_FAULT_IRQ_SHFT                                                                                                   0x0

#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000a074)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_RMSK                                                                                                                   0x1
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_IN          \
        in_dword_masked(HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR, HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_RMSK)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_ADDR, m)
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_SDCC5_SCM_FAULT_IRQ_BMSK                                                                                               0x1
#define HWIO_TCSR_SDCC5_SCM_FAULT_IRQ_SDCC5_SCM_FAULT_IRQ_SHFT                                                                                               0x0

#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_ADDR                                                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000a078)
#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_RMSK                                                                                                                   0x1
#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_IN          \
        in_dword_masked(HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_ADDR, HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_RMSK)
#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_ADDR, m)
#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_SDCC1_SCM_FAULT_IRQ_BMSK                                                                                               0x1
#define HWIO_TCSR_SDCC1_SCM_FAULT_IRQ_SDCC1_SCM_FAULT_IRQ_SHFT                                                                                               0x0

#define HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000b008)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_RMSK                                                                                                                     0x3
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR, HWIO_TCSR_PHSS_TEST_BUS_SEL_RMSK)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR, m)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR,v)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_TEST_BUS_SEL_ADDR,m,v,HWIO_TCSR_PHSS_TEST_BUS_SEL_IN)
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_PHSS_TEST_BUS_SEL_BMSK                                                                                                   0x3
#define HWIO_TCSR_PHSS_TEST_BUS_SEL_PHSS_TEST_BUS_SEL_SHFT                                                                                                   0x0

#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n)                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000b040 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_RMSK                                                                                                           0xfff
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_MAXn                                                                                                               1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_6_IRQ_ENABLE_BMSK                                                                             0x800
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_6_IRQ_ENABLE_SHFT                                                                               0xb
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_5_IRQ_ENABLE_BMSK                                                                             0x400
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_5_IRQ_ENABLE_SHFT                                                                               0xa
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_4_IRQ_ENABLE_BMSK                                                                             0x200
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_4_IRQ_ENABLE_SHFT                                                                               0x9
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_3_IRQ_ENABLE_BMSK                                                                             0x100
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_3_IRQ_ENABLE_SHFT                                                                               0x8
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_2_IRQ_ENABLE_BMSK                                                                              0x80
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_2_IRQ_ENABLE_SHFT                                                                               0x7
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_1_IRQ_ENABLE_BMSK                                                                              0x40
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP2_UART_1_IRQ_ENABLE_SHFT                                                                               0x6
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_6_IRQ_ENABLE_BMSK                                                                              0x20
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_6_IRQ_ENABLE_SHFT                                                                               0x5
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_5_IRQ_ENABLE_BMSK                                                                              0x10
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_5_IRQ_ENABLE_SHFT                                                                               0x4
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_4_IRQ_ENABLE_BMSK                                                                               0x8
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_4_IRQ_ENABLE_SHFT                                                                               0x3
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_3_IRQ_ENABLE_BMSK                                                                               0x4
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_3_IRQ_ENABLE_SHFT                                                                               0x2
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_2_IRQ_ENABLE_BMSK                                                                               0x2
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_2_IRQ_ENABLE_SHFT                                                                               0x1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_1_IRQ_ENABLE_BMSK                                                                               0x1
#define HWIO_TCSR_PHSS_UART_LPASS_INT_SEL_n_LPASS_BLSP1_UART_1_IRQ_ENABLE_SHFT                                                                               0x0

#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_ADDR(n)                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b080 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_RMSK                                                                                                             0xfff
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_MAXn                                                                                                                 1
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_6_IRQ_ENABLE_BMSK                                                                                 0x800
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_6_IRQ_ENABLE_SHFT                                                                                   0xb
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_5_IRQ_ENABLE_BMSK                                                                                 0x400
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_5_IRQ_ENABLE_SHFT                                                                                   0xa
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_4_IRQ_ENABLE_BMSK                                                                                 0x200
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_4_IRQ_ENABLE_SHFT                                                                                   0x9
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_3_IRQ_ENABLE_BMSK                                                                                 0x100
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_3_IRQ_ENABLE_SHFT                                                                                   0x8
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_2_IRQ_ENABLE_BMSK                                                                                  0x80
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_2_IRQ_ENABLE_SHFT                                                                                   0x7
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_1_IRQ_ENABLE_BMSK                                                                                  0x40
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP2_UART_1_IRQ_ENABLE_SHFT                                                                                   0x6
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_6_IRQ_ENABLE_BMSK                                                                                  0x20
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_6_IRQ_ENABLE_SHFT                                                                                   0x5
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_5_IRQ_ENABLE_BMSK                                                                                  0x10
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_5_IRQ_ENABLE_SHFT                                                                                   0x4
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_4_IRQ_ENABLE_BMSK                                                                                   0x8
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_4_IRQ_ENABLE_SHFT                                                                                   0x3
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_3_IRQ_ENABLE_BMSK                                                                                   0x4
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_3_IRQ_ENABLE_SHFT                                                                                   0x2
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_2_IRQ_ENABLE_BMSK                                                                                   0x2
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_2_IRQ_ENABLE_SHFT                                                                                   0x1
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_1_IRQ_ENABLE_BMSK                                                                                   0x1
#define HWIO_TCSR_PHSS_UART_SSC_INT_SEL_n_SSC_BLSP1_UART_1_IRQ_ENABLE_SHFT                                                                                   0x0

#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_ADDR(n)                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b100 + 0x10 * (n))
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_RMSK                                                                                                              0xfff
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_MAXn                                                                                                                  1
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_6_IRQ_ENABLE_BMSK                                                                                   0x800
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_6_IRQ_ENABLE_SHFT                                                                                     0xb
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_5_IRQ_ENABLE_BMSK                                                                                   0x400
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_5_IRQ_ENABLE_SHFT                                                                                     0xa
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_4_IRQ_ENABLE_BMSK                                                                                   0x200
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_4_IRQ_ENABLE_SHFT                                                                                     0x9
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_3_IRQ_ENABLE_BMSK                                                                                   0x100
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_3_IRQ_ENABLE_SHFT                                                                                     0x8
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_2_IRQ_ENABLE_BMSK                                                                                    0x80
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_2_IRQ_ENABLE_SHFT                                                                                     0x7
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_1_IRQ_ENABLE_BMSK                                                                                    0x40
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP2_QUP_1_IRQ_ENABLE_SHFT                                                                                     0x6
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_6_IRQ_ENABLE_BMSK                                                                                    0x20
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_6_IRQ_ENABLE_SHFT                                                                                     0x5
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_5_IRQ_ENABLE_BMSK                                                                                    0x10
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_5_IRQ_ENABLE_SHFT                                                                                     0x4
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_4_IRQ_ENABLE_BMSK                                                                                     0x8
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_4_IRQ_ENABLE_SHFT                                                                                     0x3
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_3_IRQ_ENABLE_BMSK                                                                                     0x4
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_3_IRQ_ENABLE_SHFT                                                                                     0x2
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_2_IRQ_ENABLE_BMSK                                                                                     0x2
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_2_IRQ_ENABLE_SHFT                                                                                     0x1
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_1_IRQ_ENABLE_BMSK                                                                                     0x1
#define HWIO_TCSR_PHSS_QUP_SSC_INT_SEL_n_SSC_BLSP1_QUP_1_IRQ_ENABLE_SHFT                                                                                     0x0

#define HWIO_TCSR_SSC_QUP_INT_SEL_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000b800)
#define HWIO_TCSR_SSC_QUP_INT_SEL_RMSK                                                                                                                       0x7
#define HWIO_TCSR_SSC_QUP_INT_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SSC_QUP_INT_SEL_ADDR, HWIO_TCSR_SSC_QUP_INT_SEL_RMSK)
#define HWIO_TCSR_SSC_QUP_INT_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SSC_QUP_INT_SEL_ADDR, m)
#define HWIO_TCSR_SSC_QUP_INT_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SSC_QUP_INT_SEL_ADDR,v)
#define HWIO_TCSR_SSC_QUP_INT_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SSC_QUP_INT_SEL_ADDR,m,v,HWIO_TCSR_SSC_QUP_INT_SEL_IN)
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_3_INT_ENABLE_BMSK                                                                                                  0x4
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_3_INT_ENABLE_SHFT                                                                                                  0x2
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_2_INT_ENABLE_BMSK                                                                                                  0x2
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_2_INT_ENABLE_SHFT                                                                                                  0x1
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_1_INT_ENABLE_BMSK                                                                                                  0x1
#define HWIO_TCSR_SSC_QUP_INT_SEL_SSC_QUP_1_INT_ENABLE_SHFT                                                                                                  0x0

#define HWIO_TCSR_SSC_UART_INT_SEL_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000bc00)
#define HWIO_TCSR_SSC_UART_INT_SEL_RMSK                                                                                                                      0x7
#define HWIO_TCSR_SSC_UART_INT_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SSC_UART_INT_SEL_ADDR, HWIO_TCSR_SSC_UART_INT_SEL_RMSK)
#define HWIO_TCSR_SSC_UART_INT_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SSC_UART_INT_SEL_ADDR, m)
#define HWIO_TCSR_SSC_UART_INT_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SSC_UART_INT_SEL_ADDR,v)
#define HWIO_TCSR_SSC_UART_INT_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SSC_UART_INT_SEL_ADDR,m,v,HWIO_TCSR_SSC_UART_INT_SEL_IN)
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_3_INT_ENABLE_BMSK                                                                                                0x4
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_3_INT_ENABLE_SHFT                                                                                                0x2
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_2_INT_ENABLE_BMSK                                                                                                0x2
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_2_INT_ENABLE_SHFT                                                                                                0x1
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_1_INT_ENABLE_BMSK                                                                                                0x1
#define HWIO_TCSR_SSC_UART_INT_SEL_SSC_UART_1_INT_ENABLE_SHFT                                                                                                0x0

#define HWIO_TCSR_TCSR_LDO_MISC_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000b22c)
#define HWIO_TCSR_TCSR_LDO_MISC_RMSK                                                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_LDO_MISC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_LDO_MISC_ADDR, HWIO_TCSR_TCSR_LDO_MISC_RMSK)
#define HWIO_TCSR_TCSR_LDO_MISC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_LDO_MISC_ADDR, m)
#define HWIO_TCSR_TCSR_LDO_MISC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_LDO_MISC_ADDR,v)
#define HWIO_TCSR_TCSR_LDO_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_LDO_MISC_ADDR,m,v,HWIO_TCSR_TCSR_LDO_MISC_IN)
#define HWIO_TCSR_TCSR_LDO_MISC_TCSR_LDO_MISC_BMSK                                                                                                    0xffffffff
#define HWIO_TCSR_TCSR_LDO_MISC_TCSR_LDO_MISC_SHFT                                                                                                           0x0

#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR                                                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0000b244)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK                                                                                                         0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, m)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,v)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,m,v,HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_BMSK                                                                                  0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_SHFT                                                                                         0x0

#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR                                                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b204)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_RMSK                                                                                                     0xffffffff
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_IN          \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR, HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_RMSK)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR, m)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_OUT(v)      \
        out_dword(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR,v)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_ADDR,m,v,HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_IN)
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_COPSS_USB_CONTROL_WITH_JDR_BMSK                                                                          0xffffffff
#define HWIO_TCSR_COPSS_USB_CONTROL_WITH_JDR_COPSS_USB_CONTROL_WITH_JDR_SHFT                                                                                 0x0

#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b20c)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_RMSK                                                                                                             0x1
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_IN          \
        in_dword_masked(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR, HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_RMSK)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_INM(m)      \
        in_dword_masked(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR, m)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_OUT(v)      \
        out_dword(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR,v)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_ADDR,m,v,HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_IN)
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_UFS_SATA_CTRL_SEL_BMSK                                                                                           0x1
#define HWIO_TCSR_UFS_SATA_CONTROL_WITH_JDR_UFS_SATA_CTRL_SEL_SHFT                                                                                           0x0

#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n)                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000b160 + 0x4 * (n))
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_RMSK                                                                                                     0x8000007f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_MAXn                                                                                                             31
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n), HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_RMSK)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_INI(n))
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_EN_BMSK                                                                              0x80000000
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_EN_SHFT                                                                                    0x1f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_SEL_BMSK                                                                                   0x7f
#define HWIO_TCSR_PHSS_QDSS_HW_EVENTS_CTRL_n_PHSS_QDSS_HW_EVENTS_SEL_SHFT                                                                                    0x0

#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n)                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000b360 + 0x4 * (n))
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_RMSK                                                                                                 0x80000007
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_MAXn                                                                                                         31
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n), HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_RMSK)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n), mask)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n),val)
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_ADDR(n),mask,val,HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_INI(n))
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_EN_BMSK                                                                      0x80000000
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_EN_SHFT                                                                            0x1f
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_SEL_BMSK                                                                            0x7
#define HWIO_TCSR_TCSR_GEN_QDSS_HW_EVENTS_CTRL_n_TCSR_GEN_QDSS_HW_EVENTS_SEL_SHFT                                                                            0x0

#define HWIO_TCSR_QPDI_DISABLE_CFG_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001000)
#define HWIO_TCSR_QPDI_DISABLE_CFG_RMSK                                                                                                                    0x303
#define HWIO_TCSR_QPDI_DISABLE_CFG_IN          \
        in_dword_masked(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR, HWIO_TCSR_QPDI_DISABLE_CFG_RMSK)
#define HWIO_TCSR_QPDI_DISABLE_CFG_INM(m)      \
        in_dword_masked(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR, m)
#define HWIO_TCSR_QPDI_DISABLE_CFG_OUT(v)      \
        out_dword(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR,v)
#define HWIO_TCSR_QPDI_DISABLE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR,m,v,HWIO_TCSR_QPDI_DISABLE_CFG_IN)
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_ACK_BMSK                                                                                                  0x200
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_ACK_SHFT                                                                                                    0x9
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_REQ_BMSK                                                                                                  0x100
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_REQ_SHFT                                                                                                    0x8
#define HWIO_TCSR_QPDI_DISABLE_CFG_SPMI_HANDSHAKE_DISABLE_BMSK                                                                                               0x2
#define HWIO_TCSR_QPDI_DISABLE_CFG_SPMI_HANDSHAKE_DISABLE_SHFT                                                                                               0x1
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_DISABLE_CFG_BMSK                                                                                                     0x1
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_DISABLE_CFG_SHFT                                                                                                     0x0

#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_ADDR                                                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00016000)
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_RMSK                                                                                                               0x3
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_IN          \
        in_dword_masked(HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_ADDR, HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_RMSK)
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_INM(m)      \
        in_dword_masked(HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_ADDR, m)
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_OUT(v)      \
        out_dword(HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_ADDR,v)
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_ADDR,m,v,HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_IN)
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_TEST_EN_BMSK                                                                                                       0x2
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_TEST_EN_SHFT                                                                                                       0x1
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_TEST_DATA_BMSK                                                                                                     0x1
#define HWIO_TCSR_DIFFERENTIAL_TEST_CLOCK_TEST_DATA_SHFT                                                                                                     0x0

#define HWIO_TCSR_LDO_SLEEP_CTRL_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000c000)
#define HWIO_TCSR_LDO_SLEEP_CTRL_RMSK                                                                                                                        0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, HWIO_TCSR_LDO_SLEEP_CTRL_RMSK)
#define HWIO_TCSR_LDO_SLEEP_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,m,v,HWIO_TCSR_LDO_SLEEP_CTRL_IN)
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_BMSK                                                                                                              0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_SHFT                                                                                                              0x0

#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000c004)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK                                                                                                                 0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,m,v,HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_BMSK                                                                                                0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_SHFT                                                                                                0x0

#define HWIO_TCSR_LDO_OBIAS_CTRL_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000c008)
#define HWIO_TCSR_LDO_OBIAS_CTRL_RMSK                                                                                                                        0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, HWIO_TCSR_LDO_OBIAS_CTRL_RMSK)
#define HWIO_TCSR_LDO_OBIAS_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OBIAS_CTRL_IN)
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_BMSK                                                                                                           0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_SHFT                                                                                                           0x0

#define HWIO_TCSR_LDO_VREF_CONFIG_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000c00c)
#define HWIO_TCSR_LDO_VREF_CONFIG_RMSK                                                                                                                       0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, HWIO_TCSR_LDO_VREF_CONFIG_RMSK)
#define HWIO_TCSR_LDO_VREF_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_VREF_CONFIG_IN)
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_BMSK                                                                                                       0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_SHFT                                                                                                       0x0

#define HWIO_TCSR_LDO_IB_CONFIG_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000c010)
#define HWIO_TCSR_LDO_IB_CONFIG_RMSK                                                                                                                         0x7
#define HWIO_TCSR_LDO_IB_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, HWIO_TCSR_LDO_IB_CONFIG_RMSK)
#define HWIO_TCSR_LDO_IB_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_IB_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_IB_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_IB_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_IB_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_IB_CONFIG_IN)
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_BMSK                                                                                                           0x7
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_SHFT                                                                                                           0x0

#define HWIO_TCSR_LDO_BGC_CONFIG_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000c014)
#define HWIO_TCSR_LDO_BGC_CONFIG_RMSK                                                                                                                        0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, HWIO_TCSR_LDO_BGC_CONFIG_RMSK)
#define HWIO_TCSR_LDO_BGC_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_BGC_CONFIG_IN)
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_BMSK                                                                                                                0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_SHFT                                                                                                                0x0

#define HWIO_TCSR_LDO_VREF_CTRL_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000c018)
#define HWIO_TCSR_LDO_VREF_CTRL_RMSK                                                                                                                     0x10001
#define HWIO_TCSR_LDO_VREF_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, HWIO_TCSR_LDO_VREF_CTRL_RMSK)
#define HWIO_TCSR_LDO_VREF_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CTRL_ADDR,m,v,HWIO_TCSR_LDO_VREF_CTRL_IN)
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_BMSK                                                                                                    0x10000
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_SHFT                                                                                                       0x10
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_BMSK                                                                                                         0x1
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_SHFT                                                                                                         0x0

#define HWIO_TCSR_LDO_LD_EN_ADDR                                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000c01c)
#define HWIO_TCSR_LDO_LD_EN_RMSK                                                                                                                      0x80000000
#define HWIO_TCSR_LDO_LD_EN_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, HWIO_TCSR_LDO_LD_EN_RMSK)
#define HWIO_TCSR_LDO_LD_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, m)
#define HWIO_TCSR_LDO_LD_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_EN_ADDR,v)
#define HWIO_TCSR_LDO_LD_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_EN_ADDR,m,v,HWIO_TCSR_LDO_LD_EN_IN)
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_BMSK                                                                                                            0x80000000
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_SHFT                                                                                                                  0x1f

#define HWIO_TCSR_LDO_LD_CTRL_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000c020)
#define HWIO_TCSR_LDO_LD_CTRL_RMSK                                                                                                                      0xff00ff
#define HWIO_TCSR_LDO_LD_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, HWIO_TCSR_LDO_LD_CTRL_RMSK)
#define HWIO_TCSR_LDO_LD_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_LD_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_LD_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_CTRL_ADDR,m,v,HWIO_TCSR_LDO_LD_CTRL_IN)
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_BMSK                                                                                                           0xff0000
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_SHFT                                                                                                               0x10
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_BMSK                                                                                                               0xff
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_SHFT                                                                                                                0x0

#define HWIO_TCSR_LDO_OSC_RESETB_ADDR                                                                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000c024)
#define HWIO_TCSR_LDO_OSC_RESETB_RMSK                                                                                                                 0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, HWIO_TCSR_LDO_OSC_RESETB_RMSK)
#define HWIO_TCSR_LDO_OSC_RESETB_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, m)
#define HWIO_TCSR_LDO_OSC_RESETB_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_RESETB_ADDR,v)
#define HWIO_TCSR_LDO_OSC_RESETB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_RESETB_ADDR,m,v,HWIO_TCSR_LDO_OSC_RESETB_IN)
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_BMSK                                                                                                  0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_SHFT                                                                                                        0x1f

#define HWIO_TCSR_LDO_OSC_CTRL_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000c028)
#define HWIO_TCSR_LDO_OSC_CTRL_RMSK                                                                                                                          0x3
#define HWIO_TCSR_LDO_OSC_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, HWIO_TCSR_LDO_OSC_CTRL_RMSK)
#define HWIO_TCSR_LDO_OSC_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OSC_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OSC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OSC_CTRL_IN)
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_BMSK                                                                                                             0x3
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_SHFT                                                                                                             0x0

#define HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR                                                                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000c02c)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK                                                                                                                0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_EN_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_BMSK                                                                                                     0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_SHFT                                                                                                           0x1f

#define HWIO_TCSR_LDO_DFT_CTRL_ADDR                                                                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x0000c030)
#define HWIO_TCSR_LDO_DFT_CTRL_RMSK                                                                                                                          0x7
#define HWIO_TCSR_LDO_DFT_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, HWIO_TCSR_LDO_DFT_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_BMSK                                                                                                           0x7
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_SHFT                                                                                                           0x0

#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000d000)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDSSC_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDSSC_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDSSC_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_COMPILER_VDDSSC_ACC_0_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_0_COMPILER_VDDSSC_ACC_0_SHFT                                                                                           0x0

#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000d004)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDSSC_ACC_1_ADDR, HWIO_TCSR_COMPILER_VDDSSC_ACC_1_RMSK)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDSSC_ACC_1_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_COMPILER_VDDSSC_ACC_1_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_VDDSSC_ACC_1_COMPILER_VDDSSC_ACC_1_SHFT                                                                                           0x0

#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000d080)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK                                                                                                           0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_SHFT                                                                                             0x0

#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000d084)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_RMSK                                                                                                           0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_1_ADDR, HWIO_TCSR_COMPILER_VDDCX_ACC_1_RMSK)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_1_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_COMPILER_VDDCX_ACC_1_BMSK                                                                                      0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_1_COMPILER_VDDCX_ACC_1_SHFT                                                                                             0x0

#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000d100)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDGFX_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_COMPILER_VDDGFX_ACC_0_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_0_COMPILER_VDDGFX_ACC_0_SHFT                                                                                           0x0

#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000d104)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_1_ADDR, HWIO_TCSR_COMPILER_VDDGFX_ACC_1_RMSK)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDGFX_ACC_1_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_COMPILER_VDDGFX_ACC_1_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_VDDGFX_ACC_1_COMPILER_VDDGFX_ACC_1_SHFT                                                                                           0x0

#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000d110)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_RMSK                                                                           0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR, HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_BMSK                0xff
#define HWIO_TCSR_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_CUSTOM_ACC_QCRF6421_162RBYNGDB00_256X128_1_CUSTOM6P_VDDGFX_SHFT                 0x0

#define HWIO_TCSR_MEM_ARRY_STBY_ADDR                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0000d180)
#define HWIO_TCSR_MEM_ARRY_STBY_RMSK                                                                                                                         0x1
#define HWIO_TCSR_MEM_ARRY_STBY_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, HWIO_TCSR_MEM_ARRY_STBY_RMSK)
#define HWIO_TCSR_MEM_ARRY_STBY_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, m)
#define HWIO_TCSR_MEM_ARRY_STBY_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ARRY_STBY_ADDR,v)
#define HWIO_TCSR_MEM_ARRY_STBY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ARRY_STBY_ADDR,m,v,HWIO_TCSR_MEM_ARRY_STBY_IN)
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_BMSK                                                                                                         0x1
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_SHFT                                                                                                         0x0

#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR                                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x0000e004)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK                                                                                                                     0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_BMSK                                                                                                   0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_SHFT                                                                                                   0x0

#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000f004)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_RMSK                                                                                                                    0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDGFX_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDGFX_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDGFX_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_MEM_SVS_SEL_VDDGFX_BMSK                                                                                                 0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDGFX_MEM_SVS_SEL_VDDGFX_SHFT                                                                                                 0x0

#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00010000)
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_RMSK                                                                                                                    0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDSSC_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDSSC_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDSSC_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDSSC_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDSSC_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDSSC_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_MEM_SVS_SEL_VDDSSC_BMSK                                                                                                 0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDSSC_MEM_SVS_SEL_VDDSSC_SHFT                                                                                                 0x0

#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR                                                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0000b220)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_RMSK                                                                                                                 0x1f
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_IN          \
        in_dword_masked(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR, HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_RMSK)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR, m)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR,v)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_ADDR,m,v,HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_IN)
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_DDR_SS_DEBUG_BUS_SEL_BMSK                                                                                            0x1f
#define HWIO_TCSR_DDR_SS_DEBUG_BUS_SEL_DDR_SS_DEBUG_BUS_SEL_SHFT                                                                                             0x0

#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_ADDR                                                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00011000)
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_RMSK                                                                                                     0x1
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_IN          \
        in_dword_masked(HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_ADDR, HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_RMSK)
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_INM(m)      \
        in_dword_masked(HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_ADDR, m)
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_OUT(v)      \
        out_dword(HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_ADDR,v)
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_ADDR,m,v,HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_IN)
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_VSENSE_CONTROLLER_ENABLE_REGISTER_BMSK                                                                   0x1
#define HWIO_TCSR_VSENSE_CONTROLLER_ENABLE_REGISTER_VSENSE_CONTROLLER_ENABLE_REGISTER_SHFT                                                                   0x0

#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_ADDR                                                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00012000)
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_RMSK                                                                                                      0xffffffff
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_ADDR, HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_RMSK)
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_ADDR, m)
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_ADDR,v)
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_ADDR,m,v,HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_IN)
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_TCSR_RESET_DEBUG_SW_ENTRY_BMSK                                                                            0xffffffff
#define HWIO_TCSR_TCSR_RESET_DEBUG_SW_ENTRY_TCSR_RESET_DEBUG_SW_ENTRY_SHFT                                                                                   0x0

#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_ADDR                                                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00013000)
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_RMSK                                                                                                          0xffffffff
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_BOOT_MISC_DETECT_ADDR, HWIO_TCSR_TCSR_BOOT_MISC_DETECT_RMSK)
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_BOOT_MISC_DETECT_ADDR, m)
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_BOOT_MISC_DETECT_ADDR,v)
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_BOOT_MISC_DETECT_ADDR,m,v,HWIO_TCSR_TCSR_BOOT_MISC_DETECT_IN)
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_TCSR_BOOT_MISC_DETECT_BMSK                                                                                    0xffffffff
#define HWIO_TCSR_TCSR_BOOT_MISC_DETECT_TCSR_BOOT_MISC_DETECT_SHFT                                                                                           0x0

#define HWIO_TCSR_TZ_WONCE_n_ADDR(n)                                                                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00014000 + 0x4 * (n))
#define HWIO_TCSR_TZ_WONCE_n_RMSK                                                                                                                     0xffffffff
#define HWIO_TCSR_TZ_WONCE_n_MAXn                                                                                                                             15
#define HWIO_TCSR_TZ_WONCE_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_TZ_WONCE_n_ADDR(n), HWIO_TCSR_TZ_WONCE_n_RMSK)
#define HWIO_TCSR_TZ_WONCE_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_TZ_WONCE_n_ADDR(n), mask)
#define HWIO_TCSR_TZ_WONCE_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_TZ_WONCE_n_ADDR(n),val)
#define HWIO_TCSR_TZ_WONCE_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_TZ_WONCE_n_ADDR(n),mask,val,HWIO_TCSR_TZ_WONCE_n_INI(n))
#define HWIO_TCSR_TZ_WONCE_n_TZ_WONCE_ADDRESS_BMSK                                                                                                    0xffffffff
#define HWIO_TCSR_TZ_WONCE_n_TZ_WONCE_ADDRESS_SHFT                                                                                                           0x0

#define HWIO_TCSR_QREFS_RPT_CONFIG_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00015000)
#define HWIO_TCSR_QREFS_RPT_CONFIG_RMSK                                                                                                                   0xffff
#define HWIO_TCSR_QREFS_RPT_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_QREFS_RPT_CONFIG_ADDR, HWIO_TCSR_QREFS_RPT_CONFIG_RMSK)
#define HWIO_TCSR_QREFS_RPT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_QREFS_RPT_CONFIG_ADDR, m)
#define HWIO_TCSR_QREFS_RPT_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_QREFS_RPT_CONFIG_ADDR,v)
#define HWIO_TCSR_QREFS_RPT_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_QREFS_RPT_CONFIG_ADDR,m,v,HWIO_TCSR_QREFS_RPT_CONFIG_IN)
#define HWIO_TCSR_QREFS_RPT_CONFIG_QREFS_RPT_CONFIG_BUS_SELECT_BMSK                                                                                       0xffff
#define HWIO_TCSR_QREFS_RPT_CONFIG_QREFS_RPT_CONFIG_BUS_SELECT_SHFT                                                                                          0x0

#define HWIO_TCSR_QREFS_TXVBG_CONFIG_ADDR                                                                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00015004)
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_RMSK                                                                                                                 0xffff
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_QREFS_TXVBG_CONFIG_ADDR, HWIO_TCSR_QREFS_TXVBG_CONFIG_RMSK)
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_QREFS_TXVBG_CONFIG_ADDR, m)
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_QREFS_TXVBG_CONFIG_ADDR,v)
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_QREFS_TXVBG_CONFIG_ADDR,m,v,HWIO_TCSR_QREFS_TXVBG_CONFIG_IN)
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_QREFS_TXVBG_CONFIG_BMSK                                                                                              0xffff
#define HWIO_TCSR_QREFS_TXVBG_CONFIG_QREFS_TXVBG_CONFIG_SHFT                                                                                                 0x0

#define HWIO_TCSR_TIC_CNOC_NS_ADDR                                                                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000b3e0)
#define HWIO_TCSR_TIC_CNOC_NS_RMSK                                                                                                                           0x1
#define HWIO_TCSR_TIC_CNOC_NS_IN          \
        in_dword_masked(HWIO_TCSR_TIC_CNOC_NS_ADDR, HWIO_TCSR_TIC_CNOC_NS_RMSK)
#define HWIO_TCSR_TIC_CNOC_NS_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIC_CNOC_NS_ADDR, m)
#define HWIO_TCSR_TIC_CNOC_NS_OUT(v)      \
        out_dword(HWIO_TCSR_TIC_CNOC_NS_ADDR,v)
#define HWIO_TCSR_TIC_CNOC_NS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIC_CNOC_NS_ADDR,m,v,HWIO_TCSR_TIC_CNOC_NS_IN)
#define HWIO_TCSR_TIC_CNOC_NS_TIC_CNOC_NS_BMSK                                                                                                               0x1
#define HWIO_TCSR_TIC_CNOC_NS_TIC_CNOC_NS_SHFT                                                                                                               0x0

#define HWIO_TCSR_CONN_BOX_SPARE_0_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000b3e4)
#define HWIO_TCSR_CONN_BOX_SPARE_0_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_0_IN          \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_0_ADDR, HWIO_TCSR_CONN_BOX_SPARE_0_RMSK)
#define HWIO_TCSR_CONN_BOX_SPARE_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_0_ADDR, m)
#define HWIO_TCSR_CONN_BOX_SPARE_0_OUT(v)      \
        out_dword(HWIO_TCSR_CONN_BOX_SPARE_0_ADDR,v)
#define HWIO_TCSR_CONN_BOX_SPARE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CONN_BOX_SPARE_0_ADDR,m,v,HWIO_TCSR_CONN_BOX_SPARE_0_IN)
#define HWIO_TCSR_CONN_BOX_SPARE_0_CONN_BOX_SPARE_0_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_0_CONN_BOX_SPARE_0_SHFT                                                                                                     0x0

#define HWIO_TCSR_CONN_BOX_SPARE_1_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000b3e8)
#define HWIO_TCSR_CONN_BOX_SPARE_1_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_1_IN          \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_1_ADDR, HWIO_TCSR_CONN_BOX_SPARE_1_RMSK)
#define HWIO_TCSR_CONN_BOX_SPARE_1_INM(m)      \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_1_ADDR, m)
#define HWIO_TCSR_CONN_BOX_SPARE_1_OUT(v)      \
        out_dword(HWIO_TCSR_CONN_BOX_SPARE_1_ADDR,v)
#define HWIO_TCSR_CONN_BOX_SPARE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CONN_BOX_SPARE_1_ADDR,m,v,HWIO_TCSR_CONN_BOX_SPARE_1_IN)
#define HWIO_TCSR_CONN_BOX_SPARE_1_CONN_BOX_SPARE_1_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_1_CONN_BOX_SPARE_1_SHFT                                                                                                     0x0

#define HWIO_TCSR_CONN_BOX_SPARE_2_ADDR                                                                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x0000b3ec)
#define HWIO_TCSR_CONN_BOX_SPARE_2_RMSK                                                                                                               0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_2_IN          \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_2_ADDR, HWIO_TCSR_CONN_BOX_SPARE_2_RMSK)
#define HWIO_TCSR_CONN_BOX_SPARE_2_INM(m)      \
        in_dword_masked(HWIO_TCSR_CONN_BOX_SPARE_2_ADDR, m)
#define HWIO_TCSR_CONN_BOX_SPARE_2_OUT(v)      \
        out_dword(HWIO_TCSR_CONN_BOX_SPARE_2_ADDR,v)
#define HWIO_TCSR_CONN_BOX_SPARE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CONN_BOX_SPARE_2_ADDR,m,v,HWIO_TCSR_CONN_BOX_SPARE_2_IN)
#define HWIO_TCSR_CONN_BOX_SPARE_2_CONN_BOX_SPARE_2_BMSK                                                                                              0xffffffff
#define HWIO_TCSR_CONN_BOX_SPARE_2_CONN_BOX_SPARE_2_SHFT                                                                                                     0x0

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                        (SECURITY_CONTROL_BASE      + 0x00000000)

#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000000 + 0x4 * (n))
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_MAXn                                                          71
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_SHFT                                          0x0

#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_ADDR(n)                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000120 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_RMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_MAXn                                                    3
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_ADDR(n), HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_RMSK)
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_ACC_PRIVATE_FEAT_PROV_BMSK                     0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATE_FEAT_PROVn_ACC_PRIVATE_FEAT_PROV_SHFT                            0x0

#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000130)
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_PTE_DATA0_BMSK                                           0xe0000000
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_PTE_DATA0_SHFT                                                 0x1d
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_MACCHIATO_EN_BMSK                                        0x10000000
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_MACCHIATO_EN_SHFT                                              0x1c
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_FEATURE_ID_BMSK                                           0xff00000
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_FEATURE_ID_SHFT                                                0x14
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_JTAG_ID_BMSK                                                0xfffff
#define HWIO_QFPROM_RAW_PTE_ROW0_LSB_JTAG_ID_SHFT                                                    0x0

#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000134)
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_PTE_DATA1_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW0_MSB_PTE_DATA1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000138)
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_SERIAL_NUM_BMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW1_LSB_SERIAL_NUM_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000013c)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_PTE_DATA1_BMSK                                           0xffff0000
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_PTE_DATA1_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_CHIP_ID_BMSK                                                 0xffff
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_CHIP_ID_SHFT                                                    0x0

#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000140)
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_PTE_DATA0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW2_LSB_PTE_DATA0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000144)
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_PTE_DATA1_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW2_MSB_PTE_DATA1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000148)
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_PTE_DATA0_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW3_LSB_PTE_DATA0_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000014c)
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_PTE_DATA1_BMSK                                           0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW3_MSB_PTE_DATA1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_RD_PERM_LSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000150)
#define HWIO_QFPROM_RAW_RD_PERM_LSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_RD_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_PERM_LSB_ADDR, HWIO_QFPROM_RAW_RD_PERM_LSB_RMSK)
#define HWIO_QFPROM_RAW_RD_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_PERM_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_PERM_LSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_PERM_LSB_IN)
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG31_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG31_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG30_BMSK                                      0x40000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG30_SHFT                                            0x1e
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG29_BMSK                                      0x20000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SPARE_REG29_SHFT                                            0x1d
#define HWIO_QFPROM_RAW_RD_PERM_LSB_HDCP_KEY_BMSK                                             0x10000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_HDCP_KEY_SHFT                                                   0x1c
#define HWIO_QFPROM_RAW_RD_PERM_LSB_BOOT_ROM_PATCH_BMSK                                        0x8000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_BOOT_ROM_PATCH_SHFT                                             0x1b
#define HWIO_QFPROM_RAW_RD_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                0x4000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                     0x1a
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                0x2000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                     0x19
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SEC_BOOT_BMSK                                          0x1000000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_SEC_BOOT_SHFT                                               0x18
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_IMAGE_ENCR_KEY_BMSK                                     0x800000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_IMAGE_ENCR_KEY_SHFT                                         0x17
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_IMAGE_ENCR_KEY_BMSK                                      0x400000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_IMAGE_ENCR_KEY_SHFT                                          0x16
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG21_BMSK                                         0x200000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG21_SHFT                                             0x15
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG20_BMSK                                         0x100000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG20_SHFT                                             0x14
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG19_BMSK                                          0x80000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG19_SHFT                                             0x13
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG18_BMSK                                          0x40000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG18_SHFT                                             0x12
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG17_BMSK                                          0x20000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG17_SHFT                                             0x11
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG16_BMSK                                          0x10000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG16_SHFT                                             0x10
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG15_BMSK                                           0x8000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_QC_SPARE_REG15_SHFT                                              0xf
#define HWIO_QFPROM_RAW_RD_PERM_LSB_MEM_CONFIG_BMSK                                               0x4000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_MEM_CONFIG_SHFT                                                  0xe
#define HWIO_QFPROM_RAW_RD_PERM_LSB_CALIB_BMSK                                                    0x2000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_CALIB_SHFT                                                       0xd
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PK_HASH0_BMSK                                                 0x1000
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PK_HASH0_SHFT                                                    0xc
#define HWIO_QFPROM_RAW_RD_PERM_LSB_FEAT_CONFIG_BMSK                                               0x800
#define HWIO_QFPROM_RAW_RD_PERM_LSB_FEAT_CONFIG_SHFT                                                 0xb
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_CONFIG_BMSK                                                0x400
#define HWIO_QFPROM_RAW_RD_PERM_LSB_OEM_CONFIG_SHFT                                                  0xa
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_4_BMSK                                           0x200
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_4_SHFT                                             0x9
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                           0x100
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                             0x8
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                            0x80
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                             0x7
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                            0x40
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                             0x6
#define HWIO_QFPROM_RAW_RD_PERM_LSB_FEC_EN_BMSK                                                     0x20
#define HWIO_QFPROM_RAW_RD_PERM_LSB_FEC_EN_SHFT                                                      0x5
#define HWIO_QFPROM_RAW_RD_PERM_LSB_WR_PERM_BMSK                                                    0x10
#define HWIO_QFPROM_RAW_RD_PERM_LSB_WR_PERM_SHFT                                                     0x4
#define HWIO_QFPROM_RAW_RD_PERM_LSB_RD_PERM_BMSK                                                     0x8
#define HWIO_QFPROM_RAW_RD_PERM_LSB_RD_PERM_SHFT                                                     0x3
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PTE_BMSK                                                         0x4
#define HWIO_QFPROM_RAW_RD_PERM_LSB_PTE_SHFT                                                         0x2
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ACC_PRIVATE_FEAT_PROV_BMSK                                       0x2
#define HWIO_QFPROM_RAW_RD_PERM_LSB_ACC_PRIVATE_FEAT_PROV_SHFT                                       0x1
#define HWIO_QFPROM_RAW_RD_PERM_LSB_CRI_CM_PRIVATE_BMSK                                              0x1
#define HWIO_QFPROM_RAW_RD_PERM_LSB_CRI_CM_PRIVATE_SHFT                                              0x0

#define HWIO_QFPROM_RAW_RD_PERM_MSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000154)
#define HWIO_QFPROM_RAW_RD_PERM_MSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_RD_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_PERM_MSB_ADDR, HWIO_QFPROM_RAW_RD_PERM_MSB_RMSK)
#define HWIO_QFPROM_RAW_RD_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_PERM_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_PERM_MSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_PERM_MSB_IN)
#define HWIO_QFPROM_RAW_RD_PERM_MSB_RSVD0_BMSK                                                0xffff8000
#define HWIO_QFPROM_RAW_RD_PERM_MSB_RSVD0_SHFT                                                       0xf
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG46_BMSK                                          0x4000
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG46_SHFT                                             0xe
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG45_BMSK                                          0x2000
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG45_SHFT                                             0xd
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG44_BMSK                                          0x1000
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG44_SHFT                                             0xc
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG43_BMSK                                           0x800
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG43_SHFT                                             0xb
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG42_BMSK                                           0x400
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG42_SHFT                                             0xa
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG41_BMSK                                           0x200
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG41_SHFT                                             0x9
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG40_BMSK                                           0x100
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG40_SHFT                                             0x8
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG39_BMSK                                            0x80
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG39_SHFT                                             0x7
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG38_BMSK                                            0x40
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG38_SHFT                                             0x6
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG37_BMSK                                            0x20
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG37_SHFT                                             0x5
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG36_BMSK                                            0x10
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG36_SHFT                                             0x4
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG35_BMSK                                             0x8
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG35_SHFT                                             0x3
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG34_BMSK                                             0x4
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG34_SHFT                                             0x2
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG33_BMSK                                             0x2
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG33_SHFT                                             0x1
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG32_BMSK                                             0x1
#define HWIO_QFPROM_RAW_RD_PERM_MSB_OEM_SPARE_REG32_SHFT                                             0x0

#define HWIO_QFPROM_RAW_WR_PERM_LSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000158)
#define HWIO_QFPROM_RAW_WR_PERM_LSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_WR_PERM_LSB_ADDR, HWIO_QFPROM_RAW_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_RAW_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_WR_PERM_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_WR_PERM_LSB_ADDR,m,v,HWIO_QFPROM_RAW_WR_PERM_LSB_IN)
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG31_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG31_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG30_BMSK                                      0x40000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG30_SHFT                                            0x1e
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG29_BMSK                                      0x20000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SPARE_REG29_SHFT                                            0x1d
#define HWIO_QFPROM_RAW_WR_PERM_LSB_HDCP_KEY_BMSK                                             0x10000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_HDCP_KEY_SHFT                                                   0x1c
#define HWIO_QFPROM_RAW_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                        0x8000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                             0x1b
#define HWIO_QFPROM_RAW_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                0x4000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                     0x1a
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                0x2000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                     0x19
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                          0x1000000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                               0x18
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_IMAGE_ENCR_KEY_BMSK                                     0x800000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_IMAGE_ENCR_KEY_SHFT                                         0x17
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_IMAGE_ENCR_KEY_BMSK                                      0x400000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_IMAGE_ENCR_KEY_SHFT                                          0x16
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG21_BMSK                                         0x200000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG21_SHFT                                             0x15
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG20_BMSK                                         0x100000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG20_SHFT                                             0x14
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG19_BMSK                                          0x80000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG19_SHFT                                             0x13
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG18_BMSK                                          0x40000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG18_SHFT                                             0x12
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG17_BMSK                                          0x20000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG17_SHFT                                             0x11
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG16_BMSK                                          0x10000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG16_SHFT                                             0x10
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG15_BMSK                                           0x8000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_QC_SPARE_REG15_SHFT                                              0xf
#define HWIO_QFPROM_RAW_WR_PERM_LSB_MEM_CONFIG_BMSK                                               0x4000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_MEM_CONFIG_SHFT                                                  0xe
#define HWIO_QFPROM_RAW_WR_PERM_LSB_CALIB_BMSK                                                    0x2000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_CALIB_SHFT                                                       0xd
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PK_HASH0_BMSK                                                 0x1000
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PK_HASH0_SHFT                                                    0xc
#define HWIO_QFPROM_RAW_WR_PERM_LSB_FEAT_CONFIG_BMSK                                               0x800
#define HWIO_QFPROM_RAW_WR_PERM_LSB_FEAT_CONFIG_SHFT                                                 0xb
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_CONFIG_BMSK                                                0x400
#define HWIO_QFPROM_RAW_WR_PERM_LSB_OEM_CONFIG_SHFT                                                  0xa
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_4_BMSK                                           0x200
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_4_SHFT                                             0x9
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                           0x100
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                             0x8
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                            0x80
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                             0x7
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                            0x40
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                             0x6
#define HWIO_QFPROM_RAW_WR_PERM_LSB_FEC_EN_BMSK                                                     0x20
#define HWIO_QFPROM_RAW_WR_PERM_LSB_FEC_EN_SHFT                                                      0x5
#define HWIO_QFPROM_RAW_WR_PERM_LSB_WR_PERM_BMSK                                                    0x10
#define HWIO_QFPROM_RAW_WR_PERM_LSB_WR_PERM_SHFT                                                     0x4
#define HWIO_QFPROM_RAW_WR_PERM_LSB_RD_PERM_BMSK                                                     0x8
#define HWIO_QFPROM_RAW_WR_PERM_LSB_RD_PERM_SHFT                                                     0x3
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PTE_BMSK                                                         0x4
#define HWIO_QFPROM_RAW_WR_PERM_LSB_PTE_SHFT                                                         0x2
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ACC_PRIVATE_FEAT_PROV_BMSK                                       0x2
#define HWIO_QFPROM_RAW_WR_PERM_LSB_ACC_PRIVATE_FEAT_PROV_SHFT                                       0x1
#define HWIO_QFPROM_RAW_WR_PERM_LSB_CRI_CM_PRIVATE_BMSK                                              0x1
#define HWIO_QFPROM_RAW_WR_PERM_LSB_CRI_CM_PRIVATE_SHFT                                              0x0

#define HWIO_QFPROM_RAW_WR_PERM_MSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000015c)
#define HWIO_QFPROM_RAW_WR_PERM_MSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_WR_PERM_MSB_ADDR, HWIO_QFPROM_RAW_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_RAW_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_WR_PERM_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_WR_PERM_MSB_ADDR,m,v,HWIO_QFPROM_RAW_WR_PERM_MSB_IN)
#define HWIO_QFPROM_RAW_WR_PERM_MSB_RSVD0_BMSK                                                0xffff8000
#define HWIO_QFPROM_RAW_WR_PERM_MSB_RSVD0_SHFT                                                       0xf
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG46_BMSK                                          0x4000
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG46_SHFT                                             0xe
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG45_BMSK                                          0x2000
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG45_SHFT                                             0xd
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG44_BMSK                                          0x1000
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG44_SHFT                                             0xc
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG43_BMSK                                           0x800
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG43_SHFT                                             0xb
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG42_BMSK                                           0x400
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG42_SHFT                                             0xa
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG41_BMSK                                           0x200
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG41_SHFT                                             0x9
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG40_BMSK                                           0x100
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG40_SHFT                                             0x8
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG39_BMSK                                            0x80
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG39_SHFT                                             0x7
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG38_BMSK                                            0x40
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG38_SHFT                                             0x6
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG37_BMSK                                            0x20
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG37_SHFT                                             0x5
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG36_BMSK                                            0x10
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG36_SHFT                                             0x4
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG35_BMSK                                             0x8
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG35_SHFT                                             0x3
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG34_BMSK                                             0x4
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG34_SHFT                                             0x2
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG33_BMSK                                             0x2
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG33_SHFT                                             0x1
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG32_BMSK                                             0x1
#define HWIO_QFPROM_RAW_WR_PERM_MSB_OEM_SPARE_REG32_SHFT                                             0x0

#define HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000160)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_LSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                       0x40000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                             0x1e
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                       0x20000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                             0x1d
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                       0x10000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                             0x1c
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                        0x8000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                             0x1b
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                        0x4000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                             0x1a
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                        0x2000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                             0x19
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                        0x1000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                             0x18
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                         0x800000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                             0x17
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                         0x400000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                             0x16
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                         0x200000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                             0x15
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                         0x100000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                             0x14
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                          0x80000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                             0x13
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                          0x40000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                             0x12
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                          0x20000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                             0x11
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                          0x10000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                             0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                           0x8000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                              0xf
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                           0x4000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                              0xe
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                           0x2000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                              0xd
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                           0x1000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                              0xc
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                            0x800
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                              0xb
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                            0x400
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                              0xa
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                             0x200
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                               0x9
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                             0x100
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                               0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                              0x80
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                               0x7
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                              0x40
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                               0x6
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                              0x20
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                               0x5
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                              0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                               0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                               0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                               0x3
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                               0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                               0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                               0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                               0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                               0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                               0x0

#define HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000164)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_MSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RSVD0_BMSK                                                 0xffff8000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RSVD0_SHFT                                                        0xf
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION46_FEC_EN_BMSK                                           0x4000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION46_FEC_EN_SHFT                                              0xe
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION45_FEC_EN_BMSK                                           0x2000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION45_FEC_EN_SHFT                                              0xd
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION44_FEC_EN_BMSK                                           0x1000
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION44_FEC_EN_SHFT                                              0xc
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION43_FEC_EN_BMSK                                            0x800
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION43_FEC_EN_SHFT                                              0xb
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION42_FEC_EN_BMSK                                            0x400
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION42_FEC_EN_SHFT                                              0xa
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION41_FEC_EN_BMSK                                            0x200
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION41_FEC_EN_SHFT                                              0x9
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION40_FEC_EN_BMSK                                            0x100
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION40_FEC_EN_SHFT                                              0x8
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION39_FEC_EN_BMSK                                             0x80
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION39_FEC_EN_SHFT                                              0x7
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION38_FEC_EN_BMSK                                             0x40
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION38_FEC_EN_SHFT                                              0x6
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION37_FEC_EN_BMSK                                             0x20
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION37_FEC_EN_SHFT                                              0x5
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION36_FEC_EN_BMSK                                             0x10
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION36_FEC_EN_SHFT                                              0x4
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION35_FEC_EN_BMSK                                              0x8
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION35_FEC_EN_SHFT                                              0x3
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION34_FEC_EN_BMSK                                              0x4
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION34_FEC_EN_SHFT                                              0x2
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION33_FEC_EN_BMSK                                              0x2
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION33_FEC_EN_SHFT                                              0x1
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION32_FEC_EN_BMSK                                              0x1
#define HWIO_QFPROM_RAW_FEC_EN_MSB_REGION32_FEC_EN_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000168)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_XBL0_BMSK                                         0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_XBL0_SHFT                                                0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000016c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_XBL1_BMSK                                         0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_XBL1_SHFT                                                0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000170)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_PIL_SUBSYSTEM_31_0_BMSK                           0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_PIL_SUBSYSTEM_31_0_SHFT                                  0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000174)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RSVD0_BMSK                                        0xfe000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RSVD0_SHFT                                              0x19
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                           0x1fe0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                0x11
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_TZ_BMSK                                              0x1ffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_TZ_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000178)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RSVD1_BMSK                                        0xc0000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RSVD1_SHFT                                              0x1e
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_TQS_HASH_ACTIVE_BMSK                              0x3e000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_TQS_HASH_ACTIVE_SHFT                                    0x19
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RPMB_KEY_PROVISIONED_BMSK                          0x1000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RPMB_KEY_PROVISIONED_SHFT                               0x18
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_PIL_SUBSYSTEM_47_32_BMSK                            0xffff00
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_PIL_SUBSYSTEM_47_32_SHFT                                 0x8
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RSVD0_BMSK                                              0xff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RSVD0_SHFT                                               0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000017c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_BMSK               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SHFT                      0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000180)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_MSS_BMSK                                          0xffff0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_MSS_SHFT                                                0x10
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_MBA_BMSK                                              0xffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_MBA_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000184)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_BMSK               0xffffff00
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_SHFT                      0x8
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_RSVD0_BMSK                                              0xff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_MSB_RSVD0_SHFT                                               0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000188)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE1_DISABLE_BMSK                               0x80000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE1_DISABLE_SHFT                                     0x1f
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_DISABLE_BMSK                               0x40000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_DISABLE_SHFT                                     0x1e
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALL_DEBUG_DISABLE_BMSK                            0x20000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALL_DEBUG_DISABLE_SHFT                                  0x1d
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_BMSK                         0x10000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_SHFT                               0x1c
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RSVD0_BMSK                                         0xe000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RSVD0_SHFT                                              0x19
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_MSS_HASH_INTEGRITY_CHECK_DISABLE_BMSK              0x1000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_MSS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                   0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_APPS_HASH_INTEGRITY_CHECK_DISABLE_BMSK              0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_APPS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                  0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_USB_SS_DISABLE_BMSK                                 0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_USB_SS_DISABLE_SHFT                                     0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SW_ROT_USE_SERIAL_NUM_BMSK                          0x200000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SW_ROT_USE_SERIAL_NUM_SHFT                              0x15
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DISABLE_ROT_TRANSFER_BMSK                           0x100000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DISABLE_ROT_TRANSFER_SHFT                               0x14
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IMAGE_ENCRYPTION_ENABLE_BMSK                         0x80000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IMAGE_ENCRYPTION_ENABLE_SHFT                            0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC1_SCM_FORCE_EFUSE_KEY_BMSK                        0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC1_SCM_FORCE_EFUSE_KEY_SHFT                           0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_UFS_SCM_FORCE_EFUSE_KEY_BMSK                         0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_UFS_SCM_FORCE_EFUSE_KEY_SHFT                            0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC2_SCM_FORCE_EFUSE_KEY_BMSK                        0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC2_SCM_FORCE_EFUSE_KEY_SHFT                           0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                  0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                     0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                          0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                             0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                 0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                    0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SW_FUSE_PROG_DISABLE_BMSK                             0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SW_FUSE_PROG_DISABLE_SHFT                                0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                     0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                       0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                               0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                 0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                         0x3e0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                           0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                 0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_USB_BOOT_DISABLE_BMSK                              0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_USB_BOOT_DISABLE_SHFT                              0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                 0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                 0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                        0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                        0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                     0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000018c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_SPNIDEN_DISABLE_BMSK                        0x80000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_SPNIDEN_DISABLE_SHFT                              0x1f
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                            0x40000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                  0x1e
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_GSS_A5_NIDEN_DISABLE_BMSK                         0x20000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_GSS_A5_NIDEN_DISABLE_SHFT                               0x1d
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_NIDEN_DISABLE_BMSK                            0x10000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_NIDEN_DISABLE_SHFT                                  0x1c
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PIMEM_NIDEN_DISABLE_BMSK                           0x8000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PIMEM_NIDEN_DISABLE_SHFT                                0x1b
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_NIDEN_DISABLE_BMSK                             0x4000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_NIDEN_DISABLE_SHFT                                  0x1a
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_NIDEN_DISABLE_BMSK                            0x2000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_NIDEN_DISABLE_SHFT                                 0x19
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_BMSK                           0x1000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_SHFT                                0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                              0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                  0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS1_NIDEN_DISABLE_BMSK                            0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS1_NIDEN_DISABLE_SHFT                                0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_NIDEN_DISABLE_BMSK                            0x200000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_NIDEN_DISABLE_SHFT                                0x15
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                              0x100000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                  0x14
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_GSS_A5_DBGEN_DISABLE_BMSK                            0x80000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_GSS_A5_DBGEN_DISABLE_SHFT                               0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_BMSK                          0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_SHFT                             0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                           0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                              0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_Q6_DBGEN_DISABLE_BMSK                            0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_Q6_DBGEN_DISABLE_SHFT                               0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_DBGEN_DISABLE_BMSK                                0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_DBGEN_DISABLE_SHFT                                   0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PIMEM_DBGEN_DISABLE_BMSK                              0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PIMEM_DBGEN_DISABLE_SHFT                                 0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                   0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                               0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                  0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_BMSK                               0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_SHFT                                 0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                 0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                   0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS1_DBGEN_DISABLE_BMSK                               0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS1_DBGEN_DISABLE_SHFT                                 0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_DBGEN_DISABLE_BMSK                               0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS0_DBGEN_DISABLE_SHFT                                 0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                               0x80
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                                0x7
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                  0x40
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                   0x6
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_Q6_ETM_DISABLE_BMSK                                 0x20
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SSC_Q6_ETM_DISABLE_SHFT                                  0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE6_DISABLE_BMSK                                     0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE6_DISABLE_SHFT                                      0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE5_DISABLE_BMSK                                      0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE5_DISABLE_SHFT                                      0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE4_DISABLE_BMSK                                      0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE4_DISABLE_SHFT                                      0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE3_DISABLE_BMSK                                      0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE3_DISABLE_SHFT                                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE2_DISABLE_BMSK                                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE2_DISABLE_SHFT                                      0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000190)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG46_SECURE_BMSK                       0x80000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG46_SECURE_SHFT                             0x1f
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG45_SECURE_BMSK                       0x40000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG45_SECURE_SHFT                             0x1e
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG44_SECURE_BMSK                       0x20000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG44_SECURE_SHFT                             0x1d
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG43_SECURE_BMSK                       0x10000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG43_SECURE_SHFT                             0x1c
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG42_SECURE_BMSK                        0x8000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG42_SECURE_SHFT                             0x1b
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG41_SECURE_BMSK                        0x4000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG41_SECURE_SHFT                             0x1a
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG40_SECURE_BMSK                        0x2000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG40_SECURE_SHFT                             0x19
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG39_SECURE_BMSK                        0x1000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG39_SECURE_SHFT                             0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG38_SECURE_BMSK                         0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG38_SECURE_SHFT                             0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG37_SECURE_BMSK                         0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG37_SECURE_SHFT                             0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG36_SECURE_BMSK                         0x200000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG36_SECURE_SHFT                             0x15
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG35_SECURE_BMSK                         0x100000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG35_SECURE_SHFT                             0x14
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG34_SECURE_BMSK                          0x80000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG34_SECURE_SHFT                             0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG33_SECURE_BMSK                          0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG33_SECURE_SHFT                             0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG32_SECURE_BMSK                          0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG32_SECURE_SHFT                             0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG31_SECURE_BMSK                          0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG31_SECURE_SHFT                             0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG30_SECURE_BMSK                           0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG30_SECURE_SHFT                              0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG29_SECURE_BMSK                           0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG29_SECURE_SHFT                              0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RSVD0_BMSK                                            0x3800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RSVD0_SHFT                                               0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_GSS_A5_SPIDEN_DISABLE_BMSK                             0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_GSS_A5_SPIDEN_DISABLE_SHFT                               0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_SSC_SPIDEN_DISABLE_BMSK                                0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_SSC_SPIDEN_DISABLE_SHFT                                  0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PIMEM_SPIDEN_DISABLE_BMSK                              0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PIMEM_SPIDEN_DISABLE_SHFT                                0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_DAP_SPIDEN_DISABLE_BMSK                                 0x80
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_DAP_SPIDEN_DISABLE_SHFT                                  0x7
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS1_SPIDEN_DISABLE_BMSK                               0x40
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS1_SPIDEN_DISABLE_SHFT                                0x6
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS0_SPIDEN_DISABLE_BMSK                               0x20
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS0_SPIDEN_DISABLE_SHFT                                0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_GSS_A5_SPNIDEN_DISABLE_BMSK                             0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_GSS_A5_SPNIDEN_DISABLE_SHFT                              0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_SSC_SPNIDEN_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_SSC_SPNIDEN_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PIMEM_SPNIDEN_DISABLE_BMSK                               0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PIMEM_SPNIDEN_DISABLE_SHFT                               0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_DAP_SPNIDEN_DISABLE_BMSK                                 0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_DAP_SPNIDEN_DISABLE_SHFT                                 0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS1_SPNIDEN_DISABLE_BMSK                               0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_APPS1_SPNIDEN_DISABLE_SHFT                               0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000194)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OEM_PRODUCT_ID_BMSK                               0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OEM_PRODUCT_ID_SHFT                                     0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OEM_HW_ID_BMSK                                        0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OEM_HW_ID_SHFT                                           0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000198)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_PERIPH_VID_BMSK                                   0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_PERIPH_VID_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_PERIPH_PID_BMSK                                       0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_LSB_PERIPH_PID_SHFT                                          0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000019c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_RSVD0_BMSK                                        0xffffff00
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_RSVD0_SHFT                                               0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                           0xff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW2_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                            0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a0)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_UFS_SW_CONTROL_DISABLE_BMSK                      0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_UFS_SW_CONTROL_DISABLE_SHFT                            0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SATA_DISABLE_BMSK                                0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SATA_DISABLE_SHFT                                      0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_BMSK                      0x20000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_SHFT                            0x1d
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SCM_DISABLE_BMSK                                 0x10000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SCM_DISABLE_SHFT                                       0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY1_BMSK                            0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY1_SHFT                                 0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY0_BMSK                            0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY0_SHFT                                 0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_DISABLE_BMSK                                  0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ICE_DISABLE_SHFT                                       0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RICA_DISABLE_BMSK                                 0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RICA_DISABLE_SHFT                                      0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPI_SLAVE_DISABLE_BMSK                             0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_SPI_SLAVE_DISABLE_SHFT                                 0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_APPS_ACG_DISABLE_BMSK                              0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_APPS_ACG_DISABLE_SHFT                                  0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL1_BMSK                              0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL1_SHFT                                  0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL0_BMSK                              0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL0_SHFT                                  0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_DISABLE_BMSK                            0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_DISABLE_SHFT                               0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_ENCODE_DISABLE_BMSK                      0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_ENCODE_DISABLE_SHFT                         0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_DECODE_DISABLE_BMSK                      0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_DECODE_DISABLE_SHFT                         0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_4K_DISABLE_BMSK                               0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VENUS_4K_DISABLE_SHFT                                  0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_BMSK                             0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_SHFT                                0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_BMSK                             0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_SHFT                                0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_HDMI_DISABLE_BMSK                                    0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_HDMI_DISABLE_SHFT                                       0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_HDCP_DISABLE_BMSK                                    0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_HDCP_DISABLE_SHFT                                       0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MDP_APICAL_LTC_DISABLE_BMSK                           0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MDP_APICAL_LTC_DISABLE_SHFT                             0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_EDP_DISABLE_BMSK                                      0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_EDP_DISABLE_SHFT                                        0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_BMSK                                    0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_SHFT                                      0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_0_DISABLE_BMSK                                    0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_DSI_0_DISABLE_SHFT                                      0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_FD_DISABLE_BMSK                                        0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_FD_DISABLE_SHFT                                         0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_DPCM_14_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_DPCM_14_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ISP_1_DISABLE_BMSK                                     0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ISP_1_DISABLE_SHFT                                      0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_3_DISABLE_BMSK                                    0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_3_DISABLE_SHFT                                     0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_2_DISABLE_BMSK                                     0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_CSID_2_DISABLE_SHFT                                     0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_BOOT_ROM_PATCH_DISABLE_BMSK                             0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_BOOT_ROM_PATCH_DISABLE_SHFT                             0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a4)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RSVD0_BMSK                                       0xff800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RSVD0_SHFT                                             0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APS_RESET_DISABLE_BMSK                             0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APS_RESET_DISABLE_SHFT                                 0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_HVX_DISABLE_BMSK                                   0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_HVX_DISABLE_SHFT                                       0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE1_DIS_BMSK                       0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE1_DIS_SHFT                           0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE0_EN_BMSK                         0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE0_EN_SHFT                            0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_STACKED_MEMORY_ID_BMSK                              0x7c000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_STACKED_MEMORY_ID_SHFT                                  0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PRNG_TESTMODE_DISABLE_BMSK                           0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PRNG_TESTMODE_DISABLE_SHFT                              0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_BMSK                                     0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_SHFT                                        0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MOCHA_PART_BMSK                                       0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MOCHA_PART_SHFT                                         0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_NIDNT_DISABLE_BMSK                                    0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_NIDNT_DISABLE_SHFT                                      0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SMMU_DISABLE_BMSK                                     0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SMMU_DISABLE_SHFT                                       0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENDOR_LOCK_BMSK                                      0x1e0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_VENDOR_LOCK_SHFT                                        0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                       0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                        0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_NAV_DISABLE_BMSK                                        0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_NAV_DISABLE_SHFT                                        0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_2_DISABLE_BMSK                                     0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_2_DISABLE_SHFT                                     0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_1_DISABLE_BMSK                                     0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_1_DISABLE_SHFT                                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_0_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PCIE_0_DISABLE_SHFT                                     0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a8)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RSVD0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RSVD0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001ac)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RSVD0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RSVD0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b0)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_NIDEN_DISABLE_BMSK                        0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_NIDEN_DISABLE_SHFT                              0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS1_NIDEN_DISABLE_BMSK                      0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS1_NIDEN_DISABLE_SHFT                            0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS0_NIDEN_DISABLE_BMSK                      0x20000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS0_NIDEN_DISABLE_SHFT                            0x1d
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_MSS_DBGEN_DISABLE_BMSK                        0x10000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_MSS_DBGEN_DISABLE_SHFT                              0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GSS_A5_DBGEN_DISABLE_BMSK                      0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GSS_A5_DBGEN_DISABLE_SHFT                           0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                    0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                         0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                     0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                          0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_DBGEN_DISABLE_BMSK                      0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_DBGEN_DISABLE_SHFT                           0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_DBGEN_DISABLE_BMSK                          0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_DBGEN_DISABLE_SHFT                              0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_PIMEM_DBGEN_DISABLE_BMSK                        0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_PIMEM_DBGEN_DISABLE_SHFT                            0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_RPM_DBGEN_DISABLE_BMSK                          0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_RPM_DBGEN_DISABLE_SHFT                              0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                         0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                             0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_LPASS_DBGEN_DISABLE_BMSK                         0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_LPASS_DBGEN_DISABLE_SHFT                            0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_DBGEN_DISABLE_BMSK                           0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_DBGEN_DISABLE_SHFT                              0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS1_DBGEN_DISABLE_BMSK                         0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS1_DBGEN_DISABLE_SHFT                            0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS0_DBGEN_DISABLE_BMSK                         0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_APPS0_DBGEN_DISABLE_SHFT                            0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                         0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                            0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_RPM_DAPEN_DISABLE_BMSK                            0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_RPM_DAPEN_DISABLE_SHFT                               0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_ETM_DISABLE_BMSK                           0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_ETM_DISABLE_SHFT                              0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE6_DISABLE_BMSK                               0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE6_DISABLE_SHFT                                  0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE5_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE5_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE4_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE4_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE3_DISABLE_BMSK                                0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE3_DISABLE_SHFT                                  0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE2_DISABLE_BMSK                                0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE2_DISABLE_SHFT                                  0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE1_DISABLE_BMSK                                 0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE1_DISABLE_SHFT                                  0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE0_DISABLE_BMSK                                 0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_SPARE0_DISABLE_SHFT                                  0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_NIDEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_NIDEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DBGEN_DISABLE_BMSK                             0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DBGEN_DISABLE_SHFT                              0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DAPEN_DISABLE_BMSK                              0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DAPEN_DISABLE_SHFT                              0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QDI_SPMI_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_QDI_SPMI_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_SM_BIST_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_SM_BIST_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TIC_DISABLE_BMSK                                        0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TIC_DISABLE_SHFT                                        0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b4)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGCRYPTODISABLE_BMSK                 0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGCRYPTODISABLE_SHFT                       0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3V_BMSK                  0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3V_SHFT                        0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3TE_BMSK                 0x20000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3TE_SHFT                       0x1d
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3EE_BMSK                 0x10000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3EE_SHFT                       0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS1_CFGCPUPRESENT_N_BMSK                        0xc000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS1_CFGCPUPRESENT_N_SHFT                             0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS0_CFGCPUPRESENT_N_BMSK                        0x3000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS0_CFGCPUPRESENT_N_SHFT                             0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RSVD0_BMSK                                         0xf80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RSVD0_SHFT                                             0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPIDEN_DISABLE_BMSK                       0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPIDEN_DISABLE_SHFT                          0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPIDEN_DISABLE_BMSK                          0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPIDEN_DISABLE_SHFT                             0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPIDEN_DISABLE_BMSK                        0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPIDEN_DISABLE_SHFT                           0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPIDEN_DISABLE_BMSK                           0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPIDEN_DISABLE_SHFT                              0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPIDEN_DISABLE_BMSK                         0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPIDEN_DISABLE_SHFT                            0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPIDEN_DISABLE_BMSK                         0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPIDEN_DISABLE_SHFT                            0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPNIDEN_DISABLE_BMSK                       0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPNIDEN_DISABLE_SHFT                          0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPNIDEN_DISABLE_BMSK                           0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPNIDEN_DISABLE_SHFT                             0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPNIDEN_DISABLE_BMSK                         0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPNIDEN_DISABLE_SHFT                           0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPNIDEN_DISABLE_BMSK                           0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPNIDEN_DISABLE_SHFT                             0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPNIDEN_DISABLE_BMSK                         0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPNIDEN_DISABLE_SHFT                           0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPNIDEN_DISABLE_BMSK                          0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPNIDEN_DISABLE_SHFT                           0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_MSS_NIDEN_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_MSS_NIDEN_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_NIDEN_DISABLE_BMSK                           0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_NIDEN_DISABLE_SHFT                            0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_NIDEN_DISABLE_BMSK                              0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_SSC_NIDEN_DISABLE_SHFT                               0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_NIDEN_DISABLE_BMSK                             0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_NIDEN_DISABLE_SHFT                             0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_RPM_NIDEN_DISABLE_BMSK                               0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_RPM_NIDEN_DISABLE_SHFT                               0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_WCSS_NIDEN_DISABLE_BMSK                              0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_WCSS_NIDEN_DISABLE_SHFT                              0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_LPASS_NIDEN_DISABLE_BMSK                             0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_QC_LPASS_NIDEN_DISABLE_SHFT                             0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b8)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_TAP_GEN_SPARE_INSTR_DISABLE_13_0_BMSK            0xfffc0000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_TAP_GEN_SPARE_INSTR_DISABLE_13_0_SHFT                  0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_TAP_INSTR_DISABLE_BMSK                              0x3ffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_LSB_TAP_INSTR_DISABLE_SHFT                                  0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001bc)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                      0xfffc0000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                            0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_TAP_GEN_SPARE_INSTR_DISABLE_31_14_BMSK              0x3ffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW3_MSB_TAP_GEN_SPARE_INSTR_DISABLE_31_14_SHFT                  0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001c0)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MODEM_PBL_PATCH_VERSION_BMSK                     0xf8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MODEM_PBL_PATCH_VERSION_SHFT                           0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_PBL_PATCH_VERSION_BMSK                       0x7c00000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_PBL_PATCH_VERSION_SHFT                            0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RSVD0_BMSK                                         0x3c0000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RSVD0_SHFT                                             0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_PBL_BOOT_SPEED_BMSK                            0x30000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_PBL_BOOT_SPEED_SHFT                               0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RPM_PBL_BOOT_SPEED_BMSK                              0xc000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RPM_PBL_BOOT_SPEED_SHFT                                 0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_BOOT_FROM_ROM_BMSK                              0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_APPS_BOOT_FROM_ROM_SHFT                                 0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RPM_BOOT_FROM_ROM_BMSK                               0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_RPM_BOOT_FROM_ROM_SHFT                                  0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MODEM_BOOT_FROM_ROM_BMSK                              0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MODEM_BOOT_FROM_ROM_SHFT                                0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MSA_ENA_BMSK                                          0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_MSA_ENA_SHFT                                            0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_FORCE_MSA_AUTH_EN_BMSK                                0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_FORCE_MSA_AUTH_EN_SHFT                                  0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ARM_CE_DISABLE_USAGE_BMSK                             0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_ARM_CE_DISABLE_USAGE_SHFT                               0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_BOOT_ROM_CFG_BMSK                                      0xff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_LSB_BOOT_ROM_CFG_SHFT                                       0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001c4)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_RSVD0_BMSK                                       0xffffc000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_RSVD0_SHFT                                              0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_FOUNDRY_ID_BMSK                                      0x3c00
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_FOUNDRY_ID_SHFT                                         0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_PLL_CFG_BMSK                                          0x3f0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_PLL_CFG_SHFT                                            0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_APPS_PBL_PLL_CTRL_BMSK                                  0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW4_MSB_APPS_PBL_PLL_CTRL_SHFT                                  0x0

#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001c8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_MAXn                                                         3
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_HASH_DATA0_BMSK                                     0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_LSB_HASH_DATA0_SHFT                                            0x0

#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001cc + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_MAXn                                                         3
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_HASH_DATA1_BMSK                                     0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH0_ROWn_MSB_HASH_DATA1_SHFT                                            0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001e8)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_BMSK                      0xf8000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_SHFT                            0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_BMSK                        0x7c00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_SHFT                             0x16
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_GP_HW_CALIB_BMSK                                         0x3c0000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_GP_HW_CALIB_SHFT                                             0x12
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_BMSK                                  0x3fc00
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_SHFT                                      0xa
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS1_LDO_ENABLE_BMSK                                       0x200
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS1_LDO_ENABLE_SHFT                                         0x9
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                    0x1fe
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                      0x1
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS0_LDO_ENABLE_BMSK                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_Q6SS0_LDO_ENABLE_SHFT                                         0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001ec)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE0_BMSK                                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE0_SHFT                                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK            0x3e000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x19
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK             0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0xf8000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                     0xf
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                  0x7c00
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                     0xa
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_BMSK                           0x3e0
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_SHFT                             0x5
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_BMSK                             0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_SHFT                              0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001f0)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_SPARE0_BMSK                                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_SPARE0_SHFT                                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK            0x3e000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x19
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK             0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_BMSK                     0xf8000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_SHFT                         0xf
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_BMSK                      0x7c00
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_SHFT                         0xa
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0x3e0
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001f4)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE0_BMSK                                            0xf8000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_SPARE0_SHFT                                                  0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_BANDGAP_TRIM_BMSK                                       0x7f00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_BANDGAP_TRIM_SHFT                                            0x14
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0xf8000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                     0xf
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                  0x7c00
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                     0xa
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0x3e0
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001f8)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_BMSK                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_SHFT                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_BMSK                                0x3e000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_SHFT                                      0x19
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_BMSK                                 0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_SHFT                                      0x14
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_BMSK                                  0xf8000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                      0xf
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_BMSK                                    0x7c00
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_SHFT                                       0xa
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_BMSK                                     0x3e0
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_SHFT                                       0x5
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_BMSK                                      0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_SHFT                                       0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001fc)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_BMSK                            0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_SHFT                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_BMSK                                0xf800000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_SHFT                                     0x17
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_BMSK                                  0x7c0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_SHFT                                      0x12
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_BMSK                                   0x3e000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_SHFT                                       0xd
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_BMSK                                    0x1f00
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_SHFT                                       0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                     0xf8
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                      0x3
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_BMSK                                   0x7
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_SHFT                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000200)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR5_TARG_VOLT_NOM_0_BMSK                              0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR5_TARG_VOLT_NOM_0_SHFT                                    0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_SUTUR_BMSK                              0x7c000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_SUTUR_SHFT                                    0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_NOM_BMSK                                 0x3e00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_NOM_SHFT                                      0x15
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_TUR_BMSK                                  0x1f0000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR4_TARG_VOLT_TUR_SHFT                                      0x10
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS2_BMSK                                   0xf800
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS2_SHFT                                      0xb
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS_BMSK                                     0x7c0
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS_SHFT                                       0x6
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_NOM_BMSK                                      0x3e
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_NOM_SHFT                                       0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_TUR_4_BMSK                                     0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_CPR3_TARG_VOLT_TUR_4_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000204)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_TARG_VOLT_TUR_1_0_BMSK                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_TARG_VOLT_TUR_1_0_SHFT                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_SVS2_ROSEL_BMSK                                   0x3c000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_SVS2_ROSEL_SHFT                                         0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_SVS_ROSEL_BMSK                                     0x3c00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_SVS_ROSEL_SHFT                                          0x16
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_NOMINAL_ROSEL_BMSK                                  0x3c0000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_NOMINAL_ROSEL_SHFT                                      0x12
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_TURBO_ROSEL_BMSK                                     0x3c000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR6_TURBO_ROSEL_SHFT                                         0xe
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS2_BMSK                                   0x3e00
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS2_SHFT                                      0x9
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS_BMSK                                     0x1f0
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS_SHFT                                       0x4
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_NOM_4_1_BMSK                                   0xf
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR5_TARG_VOLT_NOM_4_1_SHFT                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000208)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TURBO_QUOT_VMIN_9_0_BMSK                          0xffc00000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TURBO_QUOT_VMIN_9_0_SHFT                                0x16
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS2_BMSK                                 0x3f0000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS2_SHFT                                     0x10
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS_BMSK                                    0xfc00
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS_SHFT                                       0xa
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_NOM_BMSK                                     0x3f0
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_NOM_SHFT                                       0x4
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_TUR_5_2_BMSK                                   0xf
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CPR6_TARG_VOLT_TUR_5_2_SHFT                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000020c)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_SVS2_QUOT_VMIN_5_0_BMSK                           0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_SVS2_QUOT_VMIN_5_0_SHFT                                 0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_SVS_QUOT_VMIN_BMSK                                 0x3ffc000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_SVS_QUOT_VMIN_SHFT                                       0xe
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_NOMINAL_QUOT_VMIN_BMSK                                0x3ffc
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_NOMINAL_QUOT_VMIN_SHFT                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_TURBO_QUOT_VMIN_11_10_BMSK                               0x3
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CPR6_TURBO_QUOT_VMIN_11_10_SHFT                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000210)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR7_TURBO_ROSEL_1_0_BMSK                              0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR7_TURBO_ROSEL_1_0_SHFT                                    0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_SVS_QUOT_OFFSET_BMSK                              0x3fc00000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_SVS_QUOT_OFFSET_SHFT                                    0x16
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_NOMINAL_QUOT_OFFSET_BMSK                            0x3fc000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_NOMINAL_QUOT_OFFSET_SHFT                                 0xe
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_TURBO_QUOT_OFFSET_BMSK                                0x3fc0
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_TURBO_QUOT_OFFSET_SHFT                                   0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_SVS2_QUOT_VMIN_11_6_BMSK                                0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CPR6_SVS2_QUOT_VMIN_11_6_SHFT                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000214)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_SVS_BMSK                                0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_SVS_SHFT                                      0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_NOM_BMSK                                 0x3f00000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_NOM_SHFT                                      0x14
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_TUR_BMSK                                   0xfc000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TARG_VOLT_TUR_SHFT                                       0xe
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_SVS2_ROSEL_BMSK                                       0x3c00
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_SVS2_ROSEL_SHFT                                          0xa
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_SVS_ROSEL_BMSK                                         0x3c0
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_SVS_ROSEL_SHFT                                           0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_NOMINAL_ROSEL_BMSK                                      0x3c
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_NOMINAL_ROSEL_SHFT                                       0x2
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TURBO_ROSEL_3_2_BMSK                                     0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CPR7_TURBO_ROSEL_3_2_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000218)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_SVS_QUOT_VMIN_1_0_BMSK                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_SVS_QUOT_VMIN_1_0_SHFT                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_NOMINAL_QUOT_VMIN_BMSK                            0x3ffc0000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_NOMINAL_QUOT_VMIN_SHFT                                  0x12
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_TURBO_QUOT_VMIN_BMSK                                 0x3ffc0
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_TURBO_QUOT_VMIN_SHFT                                     0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_TARG_VOLT_SVS2_BMSK                                     0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CPR7_TARG_VOLT_SVS2_SHFT                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000021c)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_NOMINAL_QUOT_OFFSET_1_0_BMSK                      0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_NOMINAL_QUOT_OFFSET_1_0_SHFT                            0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_TURBO_QUOT_OFFSET_BMSK                            0x3fc00000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_TURBO_QUOT_OFFSET_SHFT                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_SVS2_QUOT_VMIN_BMSK                                 0x3ffc00
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_SVS2_QUOT_VMIN_SHFT                                      0xa
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_SVS_QUOT_VMIN_11_2_BMSK                                0x3ff
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CPR7_SVS_QUOT_VMIN_11_2_SHFT                                  0x0

#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000220)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW7_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW7_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW7_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_SW_CAL_REDUN_258_256_BMSK                              0xe0000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_SW_CAL_REDUN_258_256_SHFT                                    0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS2_BMSK                               0x1f000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS2_SHFT                                     0x18
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS_BMSK                                  0xf80000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS_SHFT                                      0x13
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_NOM_BMSK                                   0x7c000
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR8_TARG_VOLT_NOM_SHFT                                       0xe
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR7_SVS_QUOT_OFFSET_BMSK                                  0x3fc0
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR7_SVS_QUOT_OFFSET_SHFT                                     0x6
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR7_NOMINAL_QUOT_OFFSET_7_2_BMSK                            0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW7_LSB_CPR7_NOMINAL_QUOT_OFFSET_7_2_SHFT                             0x0

#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000224)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW7_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW7_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_PH_B0M0_BMSK                                           0xe0000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_PH_B0M0_SHFT                                                 0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_G_B0_BMSK                                              0x1c000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_G_B0_SHFT                                                    0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SAR_B0_BMSK                                             0x3000000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SAR_B0_SHFT                                                  0x18
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_APPS_APC_ODCM_CAL_SEL_BMSK                               0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_APPS_APC_ODCM_CAL_SEL_SHFT                                   0x15
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RSVD0_BMSK                                               0x1f8000
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_RSVD0_SHFT                                                    0xf
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SW_CAL_REDUN_273_259_BMSK                                  0x7fff
#define HWIO_QFPROM_RAW_CALIB_ROW7_MSB_SW_CAL_REDUN_273_259_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000228)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW8_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW8_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW8_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_VREF_B1_BMSK                                           0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_VREF_B1_SHFT                                                 0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M3_BMSK                                           0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M3_SHFT                                                 0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M2_BMSK                                            0x7000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M2_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M1_BMSK                                             0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M1_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M0_BMSK                                             0x1c0000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B1M0_SHFT                                                 0x12
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_G_B1_BMSK                                                 0x38000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_G_B1_SHFT                                                     0xf
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_SAR_B1_BMSK                                                0x6000
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_SAR_B1_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CLK_B0_BMSK                                                0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_CLK_B0_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_VREF_B0_BMSK                                                0x600
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_VREF_B0_SHFT                                                  0x9
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M3_BMSK                                                0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M3_SHFT                                                  0x6
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M2_BMSK                                                 0x38
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M2_SHFT                                                  0x3
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M1_BMSK                                                  0x7
#define HWIO_QFPROM_RAW_CALIB_ROW8_LSB_PH_B0M1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000022c)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW8_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW8_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RSVD0_BMSK                                             0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_RSVD0_SHFT                                                   0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B3M0_BMSK                                           0x70000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B3M0_SHFT                                                 0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_G_B3_BMSK                                               0xe000000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_G_B3_SHFT                                                    0x19
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SAR_B3_BMSK                                             0x1800000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SAR_B3_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CLK_B2_BMSK                                              0x600000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CLK_B2_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_VREF_B2_BMSK                                             0x180000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_VREF_B2_SHFT                                                 0x13
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M3_BMSK                                              0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M3_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M2_BMSK                                               0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M2_SHFT                                                  0xd
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M1_BMSK                                               0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M1_SHFT                                                  0xa
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M0_BMSK                                                0x380
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_PH_B2M0_SHFT                                                  0x7
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_G_B2_BMSK                                                    0x70
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_G_B2_SHFT                                                     0x4
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SAR_B2_BMSK                                                   0xc
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_SAR_B2_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CLK_B1_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW8_MSB_CLK_B1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000230)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW9_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW9_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW9_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_VREF_B4_BMSK                                           0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_VREF_B4_SHFT                                                 0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M3_BMSK                                           0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M3_SHFT                                                 0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M2_BMSK                                            0x7000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M2_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M1_BMSK                                             0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M1_SHFT                                                 0x15
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M0_BMSK                                             0x1c0000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B4M0_SHFT                                                 0x12
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_G_B4_BMSK                                                 0x38000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_G_B4_SHFT                                                     0xf
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_SAR_B4_BMSK                                                0x6000
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_SAR_B4_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CLK_B3_BMSK                                                0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_CLK_B3_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_VREF_B3_BMSK                                                0x600
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_VREF_B3_SHFT                                                  0x9
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M3_BMSK                                                0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M3_SHFT                                                  0x6
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M2_BMSK                                                 0x38
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M2_SHFT                                                  0x3
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M1_BMSK                                                  0x7
#define HWIO_QFPROM_RAW_CALIB_ROW9_LSB_PH_B3M1_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000234)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW9_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW9_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RSVD0_BMSK                                             0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_RSVD0_SHFT                                                   0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B6M0_BMSK                                           0x70000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B6M0_SHFT                                                 0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_G_B6_BMSK                                               0xe000000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_G_B6_SHFT                                                    0x19
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SAR_B6_BMSK                                             0x1800000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SAR_B6_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CLK_B5_BMSK                                              0x600000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CLK_B5_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_VREF_B5_BMSK                                             0x180000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_VREF_B5_SHFT                                                 0x13
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M3_BMSK                                              0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M3_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M2_BMSK                                               0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M2_SHFT                                                  0xd
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M1_BMSK                                               0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M1_SHFT                                                  0xa
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M0_BMSK                                                0x380
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_PH_B5M0_SHFT                                                  0x7
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_G_B5_BMSK                                                    0x70
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_G_B5_SHFT                                                     0x4
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SAR_B5_BMSK                                                   0xc
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_SAR_B5_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CLK_B4_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW9_MSB_CLK_B4_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000238)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW10_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW10_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW10_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_BMSK                                 0xfc000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_SHFT                                       0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS0_BASE0_BMSK                                      0x3ff0000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS0_BASE0_SHFT                                           0x10
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS_CAL_SEL_BMSK                                        0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_TSENS_CAL_SEL_SHFT                                           0xd
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CLK_B6_BMSK                                               0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_CLK_B6_SHFT                                                  0xb
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_VREF_B6_BMSK                                               0x600
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_VREF_B6_SHFT                                                 0x9
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M3_BMSK                                               0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M3_SHFT                                                 0x6
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M2_BMSK                                                0x38
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M2_SHFT                                                 0x3
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M1_BMSK                                                 0x7
#define HWIO_QFPROM_RAW_CALIB_ROW10_LSB_PH_B6M1_SHFT                                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000023c)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW10_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW10_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW10_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_OFFSET_BMSK                                    0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_OFFSET_SHFT                                          0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS0_OFFSET_BMSK                                     0xf000000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS0_OFFSET_SHFT                                          0x18
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_BASE1_BMSK                                       0xffc000
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_BASE1_SHFT                                            0xe
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_BASE0_BMSK                                         0x3ff0
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS1_BASE0_SHFT                                            0x4
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_BMSK                                        0xf
#define HWIO_QFPROM_RAW_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_SHFT                                        0x0

#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000240)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW11_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW11_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW11_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS9_OFFSET_BMSK                                    0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS9_OFFSET_SHFT                                          0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS8_OFFSET_BMSK                                     0xf000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS8_OFFSET_SHFT                                          0x18
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS7_OFFSET_BMSK                                      0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS7_OFFSET_SHFT                                          0x14
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS6_OFFSET_BMSK                                       0xf0000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS6_OFFSET_SHFT                                          0x10
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS5_OFFSET_BMSK                                        0xf000
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS5_OFFSET_SHFT                                           0xc
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS4_OFFSET_BMSK                                         0xf00
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS4_OFFSET_SHFT                                           0x8
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS3_OFFSET_BMSK                                          0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS3_OFFSET_SHFT                                           0x4
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS2_OFFSET_BMSK                                           0xf
#define HWIO_QFPROM_RAW_CALIB_ROW11_LSB_TSENS2_OFFSET_SHFT                                           0x0

#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000244)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW11_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW11_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW11_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS17_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS17_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS16_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS16_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS15_OFFSET_BMSK                                     0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS15_OFFSET_SHFT                                         0x14
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS14_OFFSET_BMSK                                      0xf0000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS14_OFFSET_SHFT                                         0x10
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS13_OFFSET_BMSK                                       0xf000
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS13_OFFSET_SHFT                                          0xc
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS12_OFFSET_BMSK                                        0xf00
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS12_OFFSET_SHFT                                          0x8
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS11_OFFSET_BMSK                                         0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS11_OFFSET_SHFT                                          0x4
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS10_OFFSET_BMSK                                          0xf
#define HWIO_QFPROM_RAW_CALIB_ROW11_MSB_TSENS10_OFFSET_SHFT                                          0x0

#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000248)
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW12_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW12_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW12_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW12_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW12_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW12_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_BMSK                               0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_SHFT                                     0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_BMSK                                   0x40000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_SHFT                                         0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_BMSK                                0x20000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_SHFT                                      0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_BMSK                                0x10000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_SHFT                                      0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC1_CAL_BMSK                                        0xff00000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC1_CAL_SHFT                                             0x14
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_BMSK                                          0xff000
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TXDAC0_CAL_SHFT                                              0xc
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS20_OFFSET_BMSK                                        0xf00
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS20_OFFSET_SHFT                                          0x8
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS19_OFFSET_BMSK                                         0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS19_OFFSET_SHFT                                          0x4
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS18_OFFSET_BMSK                                          0xf
#define HWIO_QFPROM_RAW_CALIB_ROW12_LSB_TSENS18_OFFSET_SHFT                                          0x0

#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000024c)
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW12_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW12_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW12_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW12_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW12_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW12_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_BMSK                                 0xe0000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_SHFT                                       0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_PORT1_HSTX_TRIM_LSB_BMSK                              0x1e000000
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_PORT1_HSTX_TRIM_LSB_SHFT                                    0x19
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_BMSK                               0x1e00000
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_SHFT                                    0x15
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_BMSK                               0x1fff80
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_SHFT                                    0x7
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_VCM_BMSK                                           0x60
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_VCM_SHFT                                            0x5
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_LDO_BMSK                                           0x18
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_LDO_SHFT                                            0x3
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_VREF_BMSK                                           0x6
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_GNSS_ADC_VREF_SHFT                                           0x1
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_BMSK                                      0x1
#define HWIO_QFPROM_RAW_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_SHFT                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000250)
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW13_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW13_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW13_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW13_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW13_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW13_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_BMSK                                0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_SHFT                                       0x0

#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000254)
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW13_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW13_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW13_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW13_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW13_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW13_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_BMSK                               0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_SHFT                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000258)
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW14_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW14_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW14_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW14_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW14_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW14_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_BMSK                               0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_SHFT                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000025c)
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW14_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW14_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW14_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW14_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW14_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW14_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_BMSK                              0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000260)
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW15_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW15_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW15_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW15_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW15_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW15_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_SW_CAL_REDUN_159_128_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW15_LSB_SW_CAL_REDUN_159_128_SHFT                                    0x0

#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000264)
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW15_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW15_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW15_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW15_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW15_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW15_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_SW_CAL_REDUN_191_160_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW15_MSB_SW_CAL_REDUN_191_160_SHFT                                    0x0

#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000268)
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW16_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW16_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW16_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_SW_CAL_REDUN_223_192_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW16_LSB_SW_CAL_REDUN_223_192_SHFT                                    0x0

#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000026c)
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW16_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW16_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW16_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_SW_CAL_REDUN_255_224_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW16_MSB_SW_CAL_REDUN_255_224_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000270 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_MAXn                                                      19
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000274 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_MAXn                                                      19
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000310)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_ADDR, HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_IN)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_QC_SPARE_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_LSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000314)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_ADDR, HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_IN)
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_QC_SPARE_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG15_MSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000318)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_IN)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_QC_SPARE_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_LSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000031c)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_IN)
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_QC_SPARE_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REG16_MSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000298 + 0x8 * (n))
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_MAXn                                                        21
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_ADDR(n), HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_INI(n))
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_QC_SPARE_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_LSB_QC_SPARE_SHFT                                              0x0

#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000029c + 0x8 * (n))
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_MAXn                                                        21
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_ADDR(n), HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_INI(n))
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_RSVD0_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_RSVD0_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_FEC_VALUE_BMSK                                      0x7f000000
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_FEC_VALUE_SHFT                                            0x18
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_QC_SPARE_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_QC_SPARE_REGn_MSB_QC_SPARE_SHFT                                              0x0

#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n)                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000348 + 0x8 * (n))
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_RMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_MAXn                                                1
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_SHFT                                    0x0

#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n)                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000034c + 0x8 * (n))
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_RMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_MAXn                                                1
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_RSVD1_BMSK                                 0x80000000
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_RSVD1_SHFT                                       0x1f
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_FEC_VALUE_BMSK                             0x7f000000
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_FEC_VALUE_SHFT                                   0x18
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_BMSK                               0xffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_SHFT                                    0x0

#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000358)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_RMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_BMSK                                 0xffff0000
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_SHFT                                       0x10
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_BMSK                                 0xffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_SHFT                                    0x0

#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000035c)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD1_BMSK                                 0x80000000
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD1_SHFT                                       0x1f
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_FEC_VALUE_BMSK                             0x7f000000
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_FEC_VALUE_SHFT                                   0x18
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_BMSK                                   0xffffff
#define HWIO_QFPROM_RAW_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_SHFT                                        0x0

#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n)                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000360 + 0x8 * (n))
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_MAXn                                               1
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_BMSK                            0xffffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_SHFT                                   0x0

#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n)                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000364 + 0x8 * (n))
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_MAXn                                               1
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RSVD1_BMSK                                0x80000000
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RSVD1_SHFT                                      0x1f
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_FEC_VALUE_BMSK                            0x7f000000
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_FEC_VALUE_SHFT                                  0x18
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_BMSK                              0xffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_SHFT                                   0x0

#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000370)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_BMSK                                0xffff0000
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_SHFT                                      0x10
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_BMSK                                0xffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_SHFT                                   0x0

#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000374)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD1_BMSK                                0x80000000
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD1_SHFT                                      0x1f
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_FEC_VALUE_BMSK                            0x7f000000
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_FEC_VALUE_SHFT                                  0x18
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_BMSK                                  0xffffff
#define HWIO_QFPROM_RAW_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_SHFT                                       0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_ADDR(n)                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000378 + 0x8 * (n))
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_MAXn                                                     1
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT4_BMSK                                  0xff000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT4_SHFT                                        0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT3_BMSK                                    0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT3_SHFT                                        0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT2_BMSK                                      0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT2_SHFT                                         0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT1_BMSK                                        0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT1_SHFT                                         0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_ADDR(n)                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000037c + 0x8 * (n))
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_MAXn                                                     1
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_RSVD1_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_RSVD1_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_FEC_VALUE_BMSK                                  0x7f000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_FEC_VALUE_SHFT                                        0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT7_BMSK                                    0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT7_SHFT                                        0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT6_BMSK                                      0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT6_SHFT                                         0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT5_BMSK                                        0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT5_SHFT                                         0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000388 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000038c + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RSVD1_BMSK                            0x80000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RSVD1_SHFT                                  0x1f
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_FEC_VALUE_BMSK                        0x7f000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_FEC_VALUE_SHFT                              0x18
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                          0xffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003a8)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003ac)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD1_BMSK                            0x80000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD1_SHFT                                  0x1f
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_FEC_VALUE_BMSK                        0x7f000000
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_FEC_VALUE_SHFT                              0x18
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_BMSK                              0xffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_SHFT                                   0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RSVD1_BMSK                            0x80000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RSVD1_SHFT                                  0x1f
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_FEC_VALUE_BMSK                        0x7f000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_FEC_VALUE_SHFT                              0x18
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                          0xffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003d0)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003d4)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD1_BMSK                            0x80000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD1_SHFT                                  0x1f
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_FEC_VALUE_BMSK                        0x7f000000
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_FEC_VALUE_SHFT                              0x18
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_BMSK                              0xffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_SHFT                                   0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003d8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_MAXn                                                       55
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000003dc + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_MAXn                                                       55
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RSVD1_BMSK                                         0x80000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RSVD1_SHFT                                               0x1f
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_BMSK                                     0x7f000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_SHFT                                           0x18
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RSVD0_BMSK                                           0xfe0000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RSVD0_SHFT                                               0x11
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                       0x1fffe
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                           0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                             0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                             0x0

#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000598)
#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KSV_LSB_ADDR, HWIO_QFPROM_RAW_HDCP_KSV_LSB_RMSK)
#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KSV_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_KSV0_BMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KSV_LSB_KSV0_SHFT                                                       0x0

#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000059c)
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KSV_MSB_ADDR, HWIO_QFPROM_RAW_HDCP_KSV_MSB_RMSK)
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KSV_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_RSVD1_BMSK                                               0x80000000
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_RSVD1_SHFT                                                     0x1f
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_FEC_VALUE_BMSK                                           0x7f000000
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_FEC_VALUE_SHFT                                                 0x18
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_RSVD0_BMSK                                                 0xffff00
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_RSVD0_SHFT                                                      0x8
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_KSV1_BMSK                                                      0xff
#define HWIO_QFPROM_RAW_HDCP_KSV_MSB_KSV1_SHFT                                                       0x0

#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000598 + 0x8 * (n))
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_MAXn                                                        40
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_KEY_DATA_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_LSB_KEY_DATA_SHFT                                              0x0

#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000059c + 0x8 * (n))
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_MAXn                                                        40
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_RSVD1_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_RSVD1_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_FEC_VALUE_BMSK                                      0x7f000000
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_FEC_VALUE_SHFT                                            0x18
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_KEY_DATA_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_HDCP_KEY_ROWn_MSB_KEY_DATA_SHFT                                              0x0

#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(n)                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000510 + 0x10 * (n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_MAXn                                                  46
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_OEM_SPARE_BMSK                                0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_OEM_SPARE_SHFT                                       0x0

#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_ADDR(n)                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000514 + 0x10 * (n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_MAXn                                                  46
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_OEM_SPARE_BMSK                                0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_MSB_OEM_SPARE_SHFT                                       0x0

#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_ADDR(n)                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000518 + 0x10 * (n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_MAXn                                                  46
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_OEM_SPARE_BMSK                                0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_LSB_OEM_SPARE_SHFT                                       0x0

#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_ADDR(n)                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000051c + 0x10 * (n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_RMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_MAXn                                                  46
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_ADDR(n), HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_INI(n))
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_OEM_SPARE_BMSK                                0xffffffff
#define HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW1_MSB_OEM_SPARE_SHFT                                       0x0

#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_MAXn                                                             39
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                         0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                0x0

#define HWIO_ACC_IR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002000)
#define HWIO_ACC_IR_RMSK                                                                            0x1f
#define HWIO_ACC_IR_OUT(v)      \
        out_dword(HWIO_ACC_IR_ADDR,v)
#define HWIO_ACC_IR_INSTRUCTION_BMSK                                                                0x1f
#define HWIO_ACC_IR_INSTRUCTION_SHFT                                                                 0x0

#define HWIO_ACC_DR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002004)
#define HWIO_ACC_DR_RMSK                                                                      0xffffffff
#define HWIO_ACC_DR_IN          \
        in_dword_masked(HWIO_ACC_DR_ADDR, HWIO_ACC_DR_RMSK)
#define HWIO_ACC_DR_INM(m)      \
        in_dword_masked(HWIO_ACC_DR_ADDR, m)
#define HWIO_ACC_DR_OUT(v)      \
        out_dword(HWIO_ACC_DR_ADDR,v)
#define HWIO_ACC_DR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_ACC_DR_ADDR,m,v,HWIO_ACC_DR_IN)
#define HWIO_ACC_DR_DR_BMSK                                                                   0xffffffff
#define HWIO_ACC_DR_DR_SHFT                                                                          0x0

#define HWIO_ACC_VERID_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002008)
#define HWIO_ACC_VERID_RMSK                                                                       0xffff
#define HWIO_ACC_VERID_IN          \
        in_dword_masked(HWIO_ACC_VERID_ADDR, HWIO_ACC_VERID_RMSK)
#define HWIO_ACC_VERID_INM(m)      \
        in_dword_masked(HWIO_ACC_VERID_ADDR, m)
#define HWIO_ACC_VERID_FWVERID_BMSK                                                               0xff00
#define HWIO_ACC_VERID_FWVERID_SHFT                                                                  0x8
#define HWIO_ACC_VERID_HWVERID_BMSK                                                                 0xff
#define HWIO_ACC_VERID_HWVERID_SHFT                                                                  0x0

#define HWIO_ACC_FEATSETn_ADDR(n)                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002010 + 0x4 * (n))
#define HWIO_ACC_FEATSETn_RMSK                                                                0xffffffff
#define HWIO_ACC_FEATSETn_MAXn                                                                         7
#define HWIO_ACC_FEATSETn_INI(n)        \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), HWIO_ACC_FEATSETn_RMSK)
#define HWIO_ACC_FEATSETn_INMI(n,mask)    \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), mask)
#define HWIO_ACC_FEATSETn_FEAT_BMSK                                                           0xffffffff
#define HWIO_ACC_FEATSETn_FEAT_SHFT                                                                  0x0

#define HWIO_ACC_STATE_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002038)
#define HWIO_ACC_STATE_RMSK                                                                          0x7
#define HWIO_ACC_STATE_IN          \
        in_dword_masked(HWIO_ACC_STATE_ADDR, HWIO_ACC_STATE_RMSK)
#define HWIO_ACC_STATE_INM(m)      \
        in_dword_masked(HWIO_ACC_STATE_ADDR, m)
#define HWIO_ACC_STATE_ACC_READY_BMSK                                                                0x4
#define HWIO_ACC_STATE_ACC_READY_SHFT                                                                0x2
#define HWIO_ACC_STATE_ACC_LOCKED_BMSK                                                               0x2
#define HWIO_ACC_STATE_ACC_LOCKED_SHFT                                                               0x1
#define HWIO_ACC_STATE_ACC_STOP_BMSK                                                                 0x1
#define HWIO_ACC_STATE_ACC_STOP_SHFT                                                                 0x0

#define HWIO_QFPROM_BLOW_TIMER_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_RMSK                                                                0xfff
#define HWIO_QFPROM_BLOW_TIMER_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, HWIO_QFPROM_BLOW_TIMER_RMSK)
#define HWIO_QFPROM_BLOW_TIMER_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, m)
#define HWIO_QFPROM_BLOW_TIMER_OUT(v)      \
        out_dword(HWIO_QFPROM_BLOW_TIMER_ADDR,v)
#define HWIO_QFPROM_BLOW_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BLOW_TIMER_ADDR,m,v,HWIO_QFPROM_BLOW_TIMER_IN)
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_BMSK                                                     0xfff
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_SHFT                                                       0x0

#define HWIO_QFPROM_TEST_CTRL_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_RMSK                                                                   0xf
#define HWIO_QFPROM_TEST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, HWIO_QFPROM_TEST_CTRL_RMSK)
#define HWIO_QFPROM_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, m)
#define HWIO_QFPROM_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_TEST_CTRL_ADDR,v)
#define HWIO_QFPROM_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_TEST_CTRL_ADDR,m,v,HWIO_QFPROM_TEST_CTRL_IN)
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_BMSK                                                       0x8
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_SHFT                                                       0x3
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_BMSK                                                        0x4
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_SHFT                                                        0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_BMSK                                                        0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_SHFT                                                        0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_BMSK                                                  0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_SHFT                                                  0x0

#define HWIO_QFPROM_ACCEL_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002044)
#define HWIO_QFPROM_ACCEL_RMSK                                                                     0xfff
#define HWIO_QFPROM_ACCEL_IN          \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, HWIO_QFPROM_ACCEL_RMSK)
#define HWIO_QFPROM_ACCEL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, m)
#define HWIO_QFPROM_ACCEL_OUT(v)      \
        out_dword(HWIO_QFPROM_ACCEL_ADDR,v)
#define HWIO_QFPROM_ACCEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_ACCEL_ADDR,m,v,HWIO_QFPROM_ACCEL_IN)
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_BMSK                                                     0x800
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_SHFT                                                       0xb
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_BMSK                                                   0x700
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_SHFT                                                     0x8
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_BMSK                                                         0xff
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_SHFT                                                          0x0

#define HWIO_QFPROM_BLOW_STATUS_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_RMSK                                                                 0x3
#define HWIO_QFPROM_BLOW_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, HWIO_QFPROM_BLOW_STATUS_RMSK)
#define HWIO_QFPROM_BLOW_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, m)
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_BMSK                                                   0x2
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_SHFT                                                   0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_BMSK                                                     0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_SHFT                                                     0x0

#define HWIO_QFPROM_ROM_ERROR_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_RMSK                                                                   0x1
#define HWIO_QFPROM_ROM_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, HWIO_QFPROM_ROM_ERROR_RMSK)
#define HWIO_QFPROM_ROM_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, m)
#define HWIO_QFPROM_ROM_ERROR_ERROR_BMSK                                                             0x1
#define HWIO_QFPROM_ROM_ERROR_ERROR_SHFT                                                             0x0

#define HWIO_QFPROM0_MATCH_STATUS_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002050)
#define HWIO_QFPROM0_MATCH_STATUS_RMSK                                                        0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, HWIO_QFPROM0_MATCH_STATUS_RMSK)
#define HWIO_QFPROM0_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_BMSK                                                   0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_SHFT                                                          0x0

#define HWIO_QFPROM1_MATCH_STATUS_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002054)
#define HWIO_QFPROM1_MATCH_STATUS_RMSK                                                        0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, HWIO_QFPROM1_MATCH_STATUS_RMSK)
#define HWIO_QFPROM1_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_BMSK                                                   0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_SHFT                                                          0x0

#define HWIO_FEC_ESR_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002058)
#define HWIO_FEC_ESR_RMSK                                                                         0x3fff
#define HWIO_FEC_ESR_IN          \
        in_dword_masked(HWIO_FEC_ESR_ADDR, HWIO_FEC_ESR_RMSK)
#define HWIO_FEC_ESR_INM(m)      \
        in_dword_masked(HWIO_FEC_ESR_ADDR, m)
#define HWIO_FEC_ESR_OUT(v)      \
        out_dword(HWIO_FEC_ESR_ADDR,v)
#define HWIO_FEC_ESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_FEC_ESR_ADDR,m,v,HWIO_FEC_ESR_IN)
#define HWIO_FEC_ESR_CORR_SW_ACC_BMSK                                                             0x2000
#define HWIO_FEC_ESR_CORR_SW_ACC_SHFT                                                                0xd
#define HWIO_FEC_ESR_CORR_HDCP_BMSK                                                               0x1000
#define HWIO_FEC_ESR_CORR_HDCP_SHFT                                                                  0xc
#define HWIO_FEC_ESR_CORR_SECURE_CHANNEL_BMSK                                                      0x800
#define HWIO_FEC_ESR_CORR_SECURE_CHANNEL_SHFT                                                        0xb
#define HWIO_FEC_ESR_CORR_BOOT_ROM_BMSK                                                            0x400
#define HWIO_FEC_ESR_CORR_BOOT_ROM_SHFT                                                              0xa
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_BMSK                                                          0x200
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_SHFT                                                            0x9
#define HWIO_FEC_ESR_CORR_MULT_BMSK                                                                0x100
#define HWIO_FEC_ESR_CORR_MULT_SHFT                                                                  0x8
#define HWIO_FEC_ESR_CORR_SEEN_BMSK                                                                 0x80
#define HWIO_FEC_ESR_CORR_SEEN_SHFT                                                                  0x7
#define HWIO_FEC_ESR_ERR_SW_ACC_BMSK                                                                0x40
#define HWIO_FEC_ESR_ERR_SW_ACC_SHFT                                                                 0x6
#define HWIO_FEC_ESR_ERR_HDCP_BMSK                                                                  0x20
#define HWIO_FEC_ESR_ERR_HDCP_SHFT                                                                   0x5
#define HWIO_FEC_ESR_ERR_SECURE_CHANNEL_BMSK                                                        0x10
#define HWIO_FEC_ESR_ERR_SECURE_CHANNEL_SHFT                                                         0x4
#define HWIO_FEC_ESR_ERR_BOOT_ROM_BMSK                                                               0x8
#define HWIO_FEC_ESR_ERR_BOOT_ROM_SHFT                                                               0x3
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_BMSK                                                             0x4
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_SHFT                                                             0x2
#define HWIO_FEC_ESR_ERR_MULT_BMSK                                                                   0x2
#define HWIO_FEC_ESR_ERR_MULT_SHFT                                                                   0x1
#define HWIO_FEC_ESR_ERR_SEEN_BMSK                                                                   0x1
#define HWIO_FEC_ESR_ERR_SEEN_SHFT                                                                   0x0

#define HWIO_FEC_EAR_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000205c)
#define HWIO_FEC_EAR_RMSK                                                                     0xffffffff
#define HWIO_FEC_EAR_IN          \
        in_dword_masked(HWIO_FEC_EAR_ADDR, HWIO_FEC_EAR_RMSK)
#define HWIO_FEC_EAR_INM(m)      \
        in_dword_masked(HWIO_FEC_EAR_ADDR, m)
#define HWIO_FEC_EAR_CORR_ADDR_BMSK                                                           0xffff0000
#define HWIO_FEC_EAR_CORR_ADDR_SHFT                                                                 0x10
#define HWIO_FEC_EAR_ERR_ADDR_BMSK                                                                0xffff
#define HWIO_FEC_EAR_ERR_ADDR_SHFT                                                                   0x0

#define HWIO_QFPROM_BIST_CTRL_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002060)
#define HWIO_QFPROM_BIST_CTRL_RMSK                                                                  0xff
#define HWIO_QFPROM_BIST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, HWIO_QFPROM_BIST_CTRL_RMSK)
#define HWIO_QFPROM_BIST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, m)
#define HWIO_QFPROM_BIST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_BIST_CTRL_ADDR,v)
#define HWIO_QFPROM_BIST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BIST_CTRL_ADDR,m,v,HWIO_QFPROM_BIST_CTRL_IN)
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_BMSK                                                      0xfc
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_SHFT                                                       0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_BMSK                                                        0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_SHFT                                                        0x1
#define HWIO_QFPROM_BIST_CTRL_START_BMSK                                                             0x1
#define HWIO_QFPROM_BIST_CTRL_START_SHFT                                                             0x0

#define HWIO_QFPROM_BIST_ERROR0_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002064)
#define HWIO_QFPROM_BIST_ERROR0_RMSK                                                          0xffffffff
#define HWIO_QFPROM_BIST_ERROR0_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR0_ADDR, HWIO_QFPROM_BIST_ERROR0_RMSK)
#define HWIO_QFPROM_BIST_ERROR0_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR0_ADDR, m)
#define HWIO_QFPROM_BIST_ERROR0_ERROR_BMSK                                                    0xffffffff
#define HWIO_QFPROM_BIST_ERROR0_ERROR_SHFT                                                           0x0

#define HWIO_QFPROM_BIST_ERROR1_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002068)
#define HWIO_QFPROM_BIST_ERROR1_RMSK                                                          0xffffffff
#define HWIO_QFPROM_BIST_ERROR1_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR1_ADDR, HWIO_QFPROM_BIST_ERROR1_RMSK)
#define HWIO_QFPROM_BIST_ERROR1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR1_ADDR, m)
#define HWIO_QFPROM_BIST_ERROR1_ERROR_BMSK                                                    0xffffffff
#define HWIO_QFPROM_BIST_ERROR1_ERROR_SHFT                                                           0x0

#define HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000206c + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_RMSK                                                      0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_MAXn                                                               7
#define HWIO_QFPROM_HASH_SIGNATUREn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), HWIO_QFPROM_HASH_SIGNATUREn_RMSK)
#define HWIO_QFPROM_HASH_SIGNATUREn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), mask)
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_BMSK                                           0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_SHFT                                                  0x0

#define HWIO_HW_KEY_STATUS_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000208c)
#define HWIO_HW_KEY_STATUS_RMSK                                                                     0x7f
#define HWIO_HW_KEY_STATUS_IN          \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, HWIO_HW_KEY_STATUS_RMSK)
#define HWIO_HW_KEY_STATUS_INM(m)      \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, m)
#define HWIO_HW_KEY_STATUS_FUSE_SENSE_DONE_BMSK                                                     0x40
#define HWIO_HW_KEY_STATUS_FUSE_SENSE_DONE_SHFT                                                      0x6
#define HWIO_HW_KEY_STATUS_CRI_CM_BOOT_DONE_BMSK                                                    0x20
#define HWIO_HW_KEY_STATUS_CRI_CM_BOOT_DONE_SHFT                                                     0x5
#define HWIO_HW_KEY_STATUS_KDF_DONE_BMSK                                                            0x10
#define HWIO_HW_KEY_STATUS_KDF_DONE_SHFT                                                             0x4
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_BMSK                                                     0x8
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_SHFT                                                     0x3
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_BMSK                                                    0x4
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_SHFT                                                    0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_BMSK                                         0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_SHFT                                         0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_BMSK                                         0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_SHFT                                         0x0

#define HWIO_RESET_JDR_STATUS_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002090)
#define HWIO_RESET_JDR_STATUS_RMSK                                                                   0x3
#define HWIO_RESET_JDR_STATUS_IN          \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, HWIO_RESET_JDR_STATUS_RMSK)
#define HWIO_RESET_JDR_STATUS_INM(m)      \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, m)
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_BMSK                                                       0x2
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_SHFT                                                       0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_BMSK                                              0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_SHFT                                              0x0

#define HWIO_FEAT_PROV_OUTn_ADDR(n)                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002094 + 0x4 * (n))
#define HWIO_FEAT_PROV_OUTn_RMSK                                                              0xffffffff
#define HWIO_FEAT_PROV_OUTn_MAXn                                                                       3
#define HWIO_FEAT_PROV_OUTn_INI(n)        \
        in_dword_masked(HWIO_FEAT_PROV_OUTn_ADDR(n), HWIO_FEAT_PROV_OUTn_RMSK)
#define HWIO_FEAT_PROV_OUTn_INMI(n,mask)    \
        in_dword_masked(HWIO_FEAT_PROV_OUTn_ADDR(n), mask)
#define HWIO_FEAT_PROV_OUTn_FEAT_PROV_OUT_VALUE_BMSK                                          0xffffffff
#define HWIO_FEAT_PROV_OUTn_FEAT_PROV_OUT_VALUE_SHFT                                                 0x0

#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000020a4 + 0x4 * (n))
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_RMSK                                                0xffffffff
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_MAXn                                                         3
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_INI(n)        \
        in_dword_masked(HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n), HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_RMSK)
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_INMI(n,mask)    \
        in_dword_masked(HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_ADDR(n), mask)
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_SEC_CTRL_MISC_CONFIG_STATUS_VALUE_BMSK              0xffffffff
#define HWIO_SEC_CTRL_MISC_CONFIG_STATUSn_SEC_CTRL_MISC_CONFIG_STATUS_VALUE_SHFT                     0x0

#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004000 + 0x4 * (n))
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_MAXn                                                         71
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_CRI_CM_PRIVATEn_CRI_CM_PRIVATE_SHFT                                         0x0

#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_ADDR(n)                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004120 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_RMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_MAXn                                                   3
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_ADDR(n), HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_RMSK)
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_ACC_PRIVATE_FEAT_PROV_BMSK                    0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATE_FEAT_PROVn_ACC_PRIVATE_FEAT_PROV_SHFT                           0x0

#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004130)
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_PTE_DATA0_BMSK                                          0xe0000000
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_PTE_DATA0_SHFT                                                0x1d
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_MACCHIATO_EN_BMSK                                       0x10000000
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_MACCHIATO_EN_SHFT                                             0x1c
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_FEATURE_ID_BMSK                                          0xff00000
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_FEATURE_ID_SHFT                                               0x14
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_JTAG_ID_BMSK                                               0xfffff
#define HWIO_QFPROM_CORR_PTE_ROW0_LSB_JTAG_ID_SHFT                                                   0x0

#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004134)
#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_PTE_DATA1_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW0_MSB_PTE_DATA1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004138)
#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_SERIAL_NUM_BMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW1_LSB_SERIAL_NUM_SHFT                                                0x0

#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000413c)
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_PTE_DATA1_BMSK                                          0xffff0000
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_PTE_DATA1_SHFT                                                0x10
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_CHIP_ID_BMSK                                                0xffff
#define HWIO_QFPROM_CORR_PTE_ROW1_MSB_CHIP_ID_SHFT                                                   0x0

#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004140)
#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_PTE_DATA0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW2_LSB_PTE_DATA0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004144)
#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_PTE_DATA1_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW2_MSB_PTE_DATA1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004148)
#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_PTE_DATA0_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW3_LSB_PTE_DATA0_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000414c)
#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_PTE_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_PTE_DATA1_BMSK                                          0xffffffff
#define HWIO_QFPROM_CORR_PTE_ROW3_MSB_PTE_DATA1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_RD_PERM_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004150)
#define HWIO_QFPROM_CORR_RD_PERM_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_RD_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_PERM_LSB_ADDR, HWIO_QFPROM_CORR_RD_PERM_LSB_RMSK)
#define HWIO_QFPROM_CORR_RD_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG31_BMSK                                     0x80000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG31_SHFT                                           0x1f
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG30_BMSK                                     0x40000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG30_SHFT                                           0x1e
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG29_BMSK                                     0x20000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SPARE_REG29_SHFT                                           0x1d
#define HWIO_QFPROM_CORR_RD_PERM_LSB_HDCP_KEY_BMSK                                            0x10000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_HDCP_KEY_SHFT                                                  0x1c
#define HWIO_QFPROM_CORR_RD_PERM_LSB_BOOT_ROM_PATCH_BMSK                                       0x8000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_BOOT_ROM_PATCH_SHFT                                            0x1b
#define HWIO_QFPROM_CORR_RD_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                               0x4000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x1a
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                               0x2000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x19
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SEC_BOOT_BMSK                                         0x1000000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_SEC_BOOT_SHFT                                              0x18
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_IMAGE_ENCR_KEY_BMSK                                    0x800000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_IMAGE_ENCR_KEY_SHFT                                        0x17
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_IMAGE_ENCR_KEY_BMSK                                     0x400000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_IMAGE_ENCR_KEY_SHFT                                         0x16
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG21_BMSK                                        0x200000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG21_SHFT                                            0x15
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG20_BMSK                                        0x100000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG20_SHFT                                            0x14
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG19_BMSK                                         0x80000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG19_SHFT                                            0x13
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG18_BMSK                                         0x40000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG18_SHFT                                            0x12
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG17_BMSK                                         0x20000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG17_SHFT                                            0x11
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG16_BMSK                                         0x10000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG16_SHFT                                            0x10
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG15_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_QC_SPARE_REG15_SHFT                                             0xf
#define HWIO_QFPROM_CORR_RD_PERM_LSB_MEM_CONFIG_BMSK                                              0x4000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_MEM_CONFIG_SHFT                                                 0xe
#define HWIO_QFPROM_CORR_RD_PERM_LSB_CALIB_BMSK                                                   0x2000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_CALIB_SHFT                                                      0xd
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PK_HASH0_BMSK                                                0x1000
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PK_HASH0_SHFT                                                   0xc
#define HWIO_QFPROM_CORR_RD_PERM_LSB_FEAT_CONFIG_BMSK                                              0x800
#define HWIO_QFPROM_CORR_RD_PERM_LSB_FEAT_CONFIG_SHFT                                                0xb
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_CONFIG_BMSK                                               0x400
#define HWIO_QFPROM_CORR_RD_PERM_LSB_OEM_CONFIG_SHFT                                                 0xa
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_4_BMSK                                          0x200
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_4_SHFT                                            0x9
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                          0x100
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                            0x8
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                           0x80
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                            0x7
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                           0x40
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                            0x6
#define HWIO_QFPROM_CORR_RD_PERM_LSB_FEC_EN_BMSK                                                    0x20
#define HWIO_QFPROM_CORR_RD_PERM_LSB_FEC_EN_SHFT                                                     0x5
#define HWIO_QFPROM_CORR_RD_PERM_LSB_WR_PERM_BMSK                                                   0x10
#define HWIO_QFPROM_CORR_RD_PERM_LSB_WR_PERM_SHFT                                                    0x4
#define HWIO_QFPROM_CORR_RD_PERM_LSB_RD_PERM_BMSK                                                    0x8
#define HWIO_QFPROM_CORR_RD_PERM_LSB_RD_PERM_SHFT                                                    0x3
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PTE_BMSK                                                        0x4
#define HWIO_QFPROM_CORR_RD_PERM_LSB_PTE_SHFT                                                        0x2
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ACC_PRIVATE_FEAT_PROV_BMSK                                      0x2
#define HWIO_QFPROM_CORR_RD_PERM_LSB_ACC_PRIVATE_FEAT_PROV_SHFT                                      0x1
#define HWIO_QFPROM_CORR_RD_PERM_LSB_CRI_CM_PRIVATE_BMSK                                             0x1
#define HWIO_QFPROM_CORR_RD_PERM_LSB_CRI_CM_PRIVATE_SHFT                                             0x0

#define HWIO_QFPROM_CORR_RD_PERM_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004154)
#define HWIO_QFPROM_CORR_RD_PERM_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_RD_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_PERM_MSB_ADDR, HWIO_QFPROM_CORR_RD_PERM_MSB_RMSK)
#define HWIO_QFPROM_CORR_RD_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_PERM_MSB_RSVD0_BMSK                                               0xffff8000
#define HWIO_QFPROM_CORR_RD_PERM_MSB_RSVD0_SHFT                                                      0xf
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG46_BMSK                                         0x4000
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG46_SHFT                                            0xe
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG45_BMSK                                         0x2000
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG45_SHFT                                            0xd
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG44_BMSK                                         0x1000
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG44_SHFT                                            0xc
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG43_BMSK                                          0x800
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG43_SHFT                                            0xb
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG42_BMSK                                          0x400
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG42_SHFT                                            0xa
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG41_BMSK                                          0x200
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG41_SHFT                                            0x9
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG40_BMSK                                          0x100
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG40_SHFT                                            0x8
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG39_BMSK                                           0x80
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG39_SHFT                                            0x7
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG38_BMSK                                           0x40
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG38_SHFT                                            0x6
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG37_BMSK                                           0x20
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG37_SHFT                                            0x5
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG36_BMSK                                           0x10
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG36_SHFT                                            0x4
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG35_BMSK                                            0x8
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG35_SHFT                                            0x3
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG34_BMSK                                            0x4
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG34_SHFT                                            0x2
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG33_BMSK                                            0x2
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG33_SHFT                                            0x1
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG32_BMSK                                            0x1
#define HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG32_SHFT                                            0x0

#define HWIO_QFPROM_CORR_WR_PERM_LSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004158)
#define HWIO_QFPROM_CORR_WR_PERM_LSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_WR_PERM_LSB_ADDR, HWIO_QFPROM_CORR_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_CORR_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG31_BMSK                                     0x80000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG31_SHFT                                           0x1f
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG30_BMSK                                     0x40000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG30_SHFT                                           0x1e
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG29_BMSK                                     0x20000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SPARE_REG29_SHFT                                           0x1d
#define HWIO_QFPROM_CORR_WR_PERM_LSB_HDCP_KEY_BMSK                                            0x10000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_HDCP_KEY_SHFT                                                  0x1c
#define HWIO_QFPROM_CORR_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                       0x8000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                            0x1b
#define HWIO_QFPROM_CORR_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                               0x4000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x1a
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                               0x2000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x19
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                         0x1000000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                              0x18
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_IMAGE_ENCR_KEY_BMSK                                    0x800000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_IMAGE_ENCR_KEY_SHFT                                        0x17
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_IMAGE_ENCR_KEY_BMSK                                     0x400000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_IMAGE_ENCR_KEY_SHFT                                         0x16
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG21_BMSK                                        0x200000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG21_SHFT                                            0x15
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG20_BMSK                                        0x100000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG20_SHFT                                            0x14
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG19_BMSK                                         0x80000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG19_SHFT                                            0x13
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG18_BMSK                                         0x40000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG18_SHFT                                            0x12
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG17_BMSK                                         0x20000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG17_SHFT                                            0x11
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG16_BMSK                                         0x10000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG16_SHFT                                            0x10
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG15_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_QC_SPARE_REG15_SHFT                                             0xf
#define HWIO_QFPROM_CORR_WR_PERM_LSB_MEM_CONFIG_BMSK                                              0x4000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_MEM_CONFIG_SHFT                                                 0xe
#define HWIO_QFPROM_CORR_WR_PERM_LSB_CALIB_BMSK                                                   0x2000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_CALIB_SHFT                                                      0xd
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PK_HASH0_BMSK                                                0x1000
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PK_HASH0_SHFT                                                   0xc
#define HWIO_QFPROM_CORR_WR_PERM_LSB_FEAT_CONFIG_BMSK                                              0x800
#define HWIO_QFPROM_CORR_WR_PERM_LSB_FEAT_CONFIG_SHFT                                                0xb
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_CONFIG_BMSK                                               0x400
#define HWIO_QFPROM_CORR_WR_PERM_LSB_OEM_CONFIG_SHFT                                                 0xa
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_4_BMSK                                          0x200
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_4_SHFT                                            0x9
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                          0x100
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                            0x8
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                           0x80
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                            0x7
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                           0x40
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                            0x6
#define HWIO_QFPROM_CORR_WR_PERM_LSB_FEC_EN_BMSK                                                    0x20
#define HWIO_QFPROM_CORR_WR_PERM_LSB_FEC_EN_SHFT                                                     0x5
#define HWIO_QFPROM_CORR_WR_PERM_LSB_WR_PERM_BMSK                                                   0x10
#define HWIO_QFPROM_CORR_WR_PERM_LSB_WR_PERM_SHFT                                                    0x4
#define HWIO_QFPROM_CORR_WR_PERM_LSB_RD_PERM_BMSK                                                    0x8
#define HWIO_QFPROM_CORR_WR_PERM_LSB_RD_PERM_SHFT                                                    0x3
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PTE_BMSK                                                        0x4
#define HWIO_QFPROM_CORR_WR_PERM_LSB_PTE_SHFT                                                        0x2
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ACC_PRIVATE_FEAT_PROV_BMSK                                      0x2
#define HWIO_QFPROM_CORR_WR_PERM_LSB_ACC_PRIVATE_FEAT_PROV_SHFT                                      0x1
#define HWIO_QFPROM_CORR_WR_PERM_LSB_CRI_CM_PRIVATE_BMSK                                             0x1
#define HWIO_QFPROM_CORR_WR_PERM_LSB_CRI_CM_PRIVATE_SHFT                                             0x0

#define HWIO_QFPROM_CORR_WR_PERM_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000415c)
#define HWIO_QFPROM_CORR_WR_PERM_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_WR_PERM_MSB_ADDR, HWIO_QFPROM_CORR_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_CORR_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_WR_PERM_MSB_RSVD0_BMSK                                               0xffff8000
#define HWIO_QFPROM_CORR_WR_PERM_MSB_RSVD0_SHFT                                                      0xf
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG46_BMSK                                         0x4000
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG46_SHFT                                            0xe
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG45_BMSK                                         0x2000
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG45_SHFT                                            0xd
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG44_BMSK                                         0x1000
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG44_SHFT                                            0xc
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG43_BMSK                                          0x800
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG43_SHFT                                            0xb
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG42_BMSK                                          0x400
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG42_SHFT                                            0xa
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG41_BMSK                                          0x200
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG41_SHFT                                            0x9
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG40_BMSK                                          0x100
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG40_SHFT                                            0x8
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG39_BMSK                                           0x80
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG39_SHFT                                            0x7
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG38_BMSK                                           0x40
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG38_SHFT                                            0x6
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG37_BMSK                                           0x20
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG37_SHFT                                            0x5
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG36_BMSK                                           0x10
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG36_SHFT                                            0x4
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG35_BMSK                                            0x8
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG35_SHFT                                            0x3
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG34_BMSK                                            0x4
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG34_SHFT                                            0x2
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG33_BMSK                                            0x2
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG33_SHFT                                            0x1
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG32_BMSK                                            0x1
#define HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG32_SHFT                                            0x0

#define HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004160)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                      0x80000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                            0x1f
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                      0x40000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                            0x1e
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                      0x20000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                            0x1d
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                      0x10000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                            0x1c
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                       0x8000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                            0x1b
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                       0x4000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                            0x1a
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                       0x2000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                            0x19
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                       0x1000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                            0x18
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                        0x800000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                            0x17
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                        0x400000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                            0x16
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                        0x200000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                            0x15
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                        0x100000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                            0x14
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                         0x80000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                            0x13
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                         0x40000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                            0x12
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                         0x20000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                            0x11
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                         0x10000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                            0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                             0xf
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                          0x4000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                             0xe
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                          0x2000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                             0xd
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                          0x1000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                             0xc
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                           0x800
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                             0xb
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                           0x400
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                             0xa
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                            0x200
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                              0x9
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                            0x100
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                              0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                             0x80
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                              0x7
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                             0x40
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                              0x6
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                             0x20
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                              0x5
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                             0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                              0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                              0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                              0x3
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                              0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                              0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                              0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                              0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                              0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                              0x0

#define HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004164)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK                                                      0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RSVD0_BMSK                                                0xffff8000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RSVD0_SHFT                                                       0xf
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION46_FEC_EN_BMSK                                          0x4000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION46_FEC_EN_SHFT                                             0xe
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION45_FEC_EN_BMSK                                          0x2000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION45_FEC_EN_SHFT                                             0xd
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION44_FEC_EN_BMSK                                          0x1000
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION44_FEC_EN_SHFT                                             0xc
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION43_FEC_EN_BMSK                                           0x800
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION43_FEC_EN_SHFT                                             0xb
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION42_FEC_EN_BMSK                                           0x400
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION42_FEC_EN_SHFT                                             0xa
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION41_FEC_EN_BMSK                                           0x200
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION41_FEC_EN_SHFT                                             0x9
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION40_FEC_EN_BMSK                                           0x100
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION40_FEC_EN_SHFT                                             0x8
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION39_FEC_EN_BMSK                                            0x80
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION39_FEC_EN_SHFT                                             0x7
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION38_FEC_EN_BMSK                                            0x40
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION38_FEC_EN_SHFT                                             0x6
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION37_FEC_EN_BMSK                                            0x20
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION37_FEC_EN_SHFT                                             0x5
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION36_FEC_EN_BMSK                                            0x10
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION36_FEC_EN_SHFT                                             0x4
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION35_FEC_EN_BMSK                                             0x8
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION35_FEC_EN_SHFT                                             0x3
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION34_FEC_EN_BMSK                                             0x4
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION34_FEC_EN_SHFT                                             0x2
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION33_FEC_EN_BMSK                                             0x2
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION33_FEC_EN_SHFT                                             0x1
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION32_FEC_EN_BMSK                                             0x1
#define HWIO_QFPROM_CORR_FEC_EN_MSB_REGION32_FEC_EN_SHFT                                             0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004168)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_XBL0_BMSK                                        0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_XBL0_SHFT                                               0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000416c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_XBL1_BMSK                                        0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_XBL1_SHFT                                               0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004170)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_PIL_SUBSYSTEM_31_0_BMSK                          0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_PIL_SUBSYSTEM_31_0_SHFT                                 0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004174)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RSVD0_BMSK                                       0xfe000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RSVD0_SHFT                                             0x19
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                          0x1fe0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                               0x11
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_TZ_BMSK                                             0x1ffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_TZ_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004178)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RSVD1_BMSK                                       0xc0000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RSVD1_SHFT                                             0x1e
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_TQS_HASH_ACTIVE_BMSK                             0x3e000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_TQS_HASH_ACTIVE_SHFT                                   0x19
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RPMB_KEY_PROVISIONED_BMSK                         0x1000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RPMB_KEY_PROVISIONED_SHFT                              0x18
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_PIL_SUBSYSTEM_47_32_BMSK                           0xffff00
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_PIL_SUBSYSTEM_47_32_SHFT                                0x8
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RSVD0_BMSK                                             0xff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RSVD0_SHFT                                              0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000417c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_BMSK             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SHFT                    0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004180)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_MSS_BMSK                                         0xffff0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_MSS_SHFT                                               0x10
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_MBA_BMSK                                             0xffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_MBA_SHFT                                                0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004184)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_BMSK             0xffffff00
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_SHFT                    0x8
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_RSVD0_BMSK                                             0xff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_MSB_RSVD0_SHFT                                              0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004188)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE1_DISABLE_BMSK                              0x80000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE1_DISABLE_SHFT                                    0x1f
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_DISABLE_BMSK                              0x40000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_DISABLE_SHFT                                    0x1e
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALL_DEBUG_DISABLE_BMSK                           0x20000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALL_DEBUG_DISABLE_SHFT                                 0x1d
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_BMSK                        0x10000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_DISABLE_SHFT                              0x1c
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RSVD0_BMSK                                        0xe000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RSVD0_SHFT                                             0x19
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_MSS_HASH_INTEGRITY_CHECK_DISABLE_BMSK             0x1000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_MSS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                  0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_APPS_HASH_INTEGRITY_CHECK_DISABLE_BMSK             0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_APPS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                 0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_USB_SS_DISABLE_BMSK                                0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_USB_SS_DISABLE_SHFT                                    0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SW_ROT_USE_SERIAL_NUM_BMSK                         0x200000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SW_ROT_USE_SERIAL_NUM_SHFT                             0x15
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DISABLE_ROT_TRANSFER_BMSK                          0x100000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_DISABLE_ROT_TRANSFER_SHFT                              0x14
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IMAGE_ENCRYPTION_ENABLE_BMSK                        0x80000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IMAGE_ENCRYPTION_ENABLE_SHFT                           0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC1_SCM_FORCE_EFUSE_KEY_BMSK                       0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC1_SCM_FORCE_EFUSE_KEY_SHFT                          0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_UFS_SCM_FORCE_EFUSE_KEY_BMSK                        0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_UFS_SCM_FORCE_EFUSE_KEY_SHFT                           0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC2_SCM_FORCE_EFUSE_KEY_BMSK                       0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC2_SCM_FORCE_EFUSE_KEY_SHFT                          0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                 0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                    0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                         0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                            0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                   0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SW_FUSE_PROG_DISABLE_BMSK                            0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SW_FUSE_PROG_DISABLE_SHFT                               0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                    0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                      0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                              0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                        0x3e0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                          0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                               0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_USB_BOOT_DISABLE_BMSK                             0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_USB_BOOT_DISABLE_SHFT                             0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                       0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                       0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                    0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                    0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000418c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_SPNIDEN_DISABLE_BMSK                       0x80000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_SPNIDEN_DISABLE_SHFT                             0x1f
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                           0x40000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                 0x1e
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_GSS_A5_NIDEN_DISABLE_BMSK                        0x20000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_GSS_A5_NIDEN_DISABLE_SHFT                              0x1d
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_NIDEN_DISABLE_BMSK                           0x10000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_NIDEN_DISABLE_SHFT                                 0x1c
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PIMEM_NIDEN_DISABLE_BMSK                          0x8000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PIMEM_NIDEN_DISABLE_SHFT                               0x1b
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_NIDEN_DISABLE_BMSK                            0x4000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_NIDEN_DISABLE_SHFT                                 0x1a
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_NIDEN_DISABLE_BMSK                           0x2000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_NIDEN_DISABLE_SHFT                                0x19
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_BMSK                          0x1000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_NIDEN_DISABLE_SHFT                               0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                             0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                 0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS1_NIDEN_DISABLE_BMSK                           0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS1_NIDEN_DISABLE_SHFT                               0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_NIDEN_DISABLE_BMSK                           0x200000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_NIDEN_DISABLE_SHFT                               0x15
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                             0x100000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                 0x14
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_GSS_A5_DBGEN_DISABLE_BMSK                           0x80000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_GSS_A5_DBGEN_DISABLE_SHFT                              0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_BMSK                         0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_A5X_ISDB_DBGEN_DISABLE_SHFT                            0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                          0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                             0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_Q6_DBGEN_DISABLE_BMSK                           0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_Q6_DBGEN_DISABLE_SHFT                              0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_DBGEN_DISABLE_BMSK                               0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_DBGEN_DISABLE_SHFT                                  0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PIMEM_DBGEN_DISABLE_BMSK                             0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PIMEM_DBGEN_DISABLE_SHFT                                0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                               0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                  0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                              0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                 0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_BMSK                              0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_LPASS_DBGEN_DISABLE_SHFT                                0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS1_DBGEN_DISABLE_BMSK                              0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS1_DBGEN_DISABLE_SHFT                                0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_DBGEN_DISABLE_BMSK                              0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS0_DBGEN_DISABLE_SHFT                                0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                              0x80
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                               0x7
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                 0x40
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                  0x6
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_Q6_ETM_DISABLE_BMSK                                0x20
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SSC_Q6_ETM_DISABLE_SHFT                                 0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE6_DISABLE_BMSK                                    0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE6_DISABLE_SHFT                                     0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE5_DISABLE_BMSK                                     0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE5_DISABLE_SHFT                                     0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE4_DISABLE_BMSK                                     0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE4_DISABLE_SHFT                                     0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE3_DISABLE_BMSK                                     0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE3_DISABLE_SHFT                                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE2_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE2_DISABLE_SHFT                                     0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004190)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG46_SECURE_BMSK                      0x80000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG46_SECURE_SHFT                            0x1f
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG45_SECURE_BMSK                      0x40000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG45_SECURE_SHFT                            0x1e
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG44_SECURE_BMSK                      0x20000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG44_SECURE_SHFT                            0x1d
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG43_SECURE_BMSK                      0x10000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG43_SECURE_SHFT                            0x1c
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG42_SECURE_BMSK                       0x8000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG42_SECURE_SHFT                            0x1b
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG41_SECURE_BMSK                       0x4000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG41_SECURE_SHFT                            0x1a
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG40_SECURE_BMSK                       0x2000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG40_SECURE_SHFT                            0x19
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG39_SECURE_BMSK                       0x1000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG39_SECURE_SHFT                            0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG38_SECURE_BMSK                        0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG38_SECURE_SHFT                            0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG37_SECURE_BMSK                        0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG37_SECURE_SHFT                            0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG36_SECURE_BMSK                        0x200000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG36_SECURE_SHFT                            0x15
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG35_SECURE_BMSK                        0x100000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG35_SECURE_SHFT                            0x14
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG34_SECURE_BMSK                         0x80000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG34_SECURE_SHFT                            0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG33_SECURE_BMSK                         0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG33_SECURE_SHFT                            0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG32_SECURE_BMSK                         0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG32_SECURE_SHFT                            0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG31_SECURE_BMSK                         0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG31_SECURE_SHFT                            0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG30_SECURE_BMSK                          0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG30_SECURE_SHFT                             0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG29_SECURE_BMSK                          0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_SPARE_REG29_SECURE_SHFT                             0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RSVD0_BMSK                                           0x3800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RSVD0_SHFT                                              0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_GSS_A5_SPIDEN_DISABLE_BMSK                            0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_GSS_A5_SPIDEN_DISABLE_SHFT                              0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_SSC_SPIDEN_DISABLE_BMSK                               0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_SSC_SPIDEN_DISABLE_SHFT                                 0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PIMEM_SPIDEN_DISABLE_BMSK                             0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PIMEM_SPIDEN_DISABLE_SHFT                               0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_DAP_SPIDEN_DISABLE_BMSK                                0x80
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_DAP_SPIDEN_DISABLE_SHFT                                 0x7
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS1_SPIDEN_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS1_SPIDEN_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS0_SPIDEN_DISABLE_BMSK                              0x20
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS0_SPIDEN_DISABLE_SHFT                               0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_GSS_A5_SPNIDEN_DISABLE_BMSK                            0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_GSS_A5_SPNIDEN_DISABLE_SHFT                             0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_SSC_SPNIDEN_DISABLE_BMSK                                0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_SSC_SPNIDEN_DISABLE_SHFT                                0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PIMEM_SPNIDEN_DISABLE_BMSK                              0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PIMEM_SPNIDEN_DISABLE_SHFT                              0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_DAP_SPNIDEN_DISABLE_BMSK                                0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_DAP_SPNIDEN_DISABLE_SHFT                                0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS1_SPNIDEN_DISABLE_BMSK                              0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_APPS1_SPNIDEN_DISABLE_SHFT                              0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004194)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_OEM_PRODUCT_ID_BMSK                              0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_OEM_PRODUCT_ID_SHFT                                    0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_OEM_HW_ID_BMSK                                       0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_OEM_HW_ID_SHFT                                          0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004198)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_PERIPH_VID_BMSK                                  0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_PERIPH_VID_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_PERIPH_PID_BMSK                                      0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_LSB_PERIPH_PID_SHFT                                         0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000419c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_RSVD0_BMSK                                       0xffffff00
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_RSVD0_SHFT                                              0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                          0xff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW2_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                           0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a0)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_UFS_SW_CONTROL_DISABLE_BMSK                     0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_UFS_SW_CONTROL_DISABLE_SHFT                           0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SATA_DISABLE_BMSK                               0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SATA_DISABLE_SHFT                                     0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_BMSK                     0x20000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SECURE_CHANNEL_DISABLE_SHFT                           0x1d
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SCM_DISABLE_BMSK                                0x10000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SCM_DISABLE_SHFT                                      0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY1_BMSK                           0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY1_SHFT                                0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY0_BMSK                           0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_FORCE_HW_KEY0_SHFT                                0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_DISABLE_BMSK                                 0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ICE_DISABLE_SHFT                                      0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RICA_DISABLE_BMSK                                0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RICA_DISABLE_SHFT                                     0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPI_SLAVE_DISABLE_BMSK                            0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_SPI_SLAVE_DISABLE_SHFT                                0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_APPS_ACG_DISABLE_BMSK                             0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_APPS_ACG_DISABLE_SHFT                                 0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL1_BMSK                             0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL1_SHFT                                 0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL0_BMSK                             0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_SEL0_SHFT                                 0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_DISABLE_BMSK                           0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_GFX3D_TURBO_DISABLE_SHFT                              0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_ENCODE_DISABLE_BMSK                     0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_ENCODE_DISABLE_SHFT                        0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_DECODE_DISABLE_BMSK                     0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_HEVC_DECODE_DISABLE_SHFT                        0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_4K_DISABLE_BMSK                              0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VENUS_4K_DISABLE_SHFT                                 0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_BMSK                            0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_DECODER_DISABLE_SHFT                               0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_BMSK                            0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_VP8_ENCODER_DISABLE_SHFT                               0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_HDMI_DISABLE_BMSK                                   0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_HDMI_DISABLE_SHFT                                      0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_HDCP_DISABLE_BMSK                                   0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_HDCP_DISABLE_SHFT                                      0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MDP_APICAL_LTC_DISABLE_BMSK                          0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MDP_APICAL_LTC_DISABLE_SHFT                            0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_EDP_DISABLE_BMSK                                     0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_EDP_DISABLE_SHFT                                       0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_BMSK                                   0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_1_DISABLE_SHFT                                     0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_0_DISABLE_BMSK                                   0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_DSI_0_DISABLE_SHFT                                     0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_FD_DISABLE_BMSK                                       0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_FD_DISABLE_SHFT                                        0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_DPCM_14_DISABLE_BMSK                             0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_DPCM_14_DISABLE_SHFT                              0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ISP_1_DISABLE_BMSK                                    0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ISP_1_DISABLE_SHFT                                     0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_3_DISABLE_BMSK                                   0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_3_DISABLE_SHFT                                    0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_2_DISABLE_BMSK                                    0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_CSID_2_DISABLE_SHFT                                    0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_BOOT_ROM_PATCH_DISABLE_BMSK                            0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_BOOT_ROM_PATCH_DISABLE_SHFT                            0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a4)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RSVD0_BMSK                                      0xff800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RSVD0_SHFT                                            0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APS_RESET_DISABLE_BMSK                            0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APS_RESET_DISABLE_SHFT                                0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_HVX_DISABLE_BMSK                                  0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_HVX_DISABLE_SHFT                                      0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE1_DIS_BMSK                      0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE1_DIS_SHFT                          0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE0_EN_BMSK                        0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_APB2JTAG_LIFECYCLE0_EN_SHFT                           0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_STACKED_MEMORY_ID_BMSK                             0x7c000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_STACKED_MEMORY_ID_SHFT                                 0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PRNG_TESTMODE_DISABLE_BMSK                          0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PRNG_TESTMODE_DISABLE_SHFT                             0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_BMSK                                    0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_DCC_DISABLE_SHFT                                       0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MOCHA_PART_BMSK                                      0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MOCHA_PART_SHFT                                        0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_NIDNT_DISABLE_BMSK                                   0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_NIDNT_DISABLE_SHFT                                     0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SMMU_DISABLE_BMSK                                    0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SMMU_DISABLE_SHFT                                      0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENDOR_LOCK_BMSK                                     0x1e0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_VENDOR_LOCK_SHFT                                       0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                      0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                       0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_NAV_DISABLE_BMSK                                       0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_NAV_DISABLE_SHFT                                       0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_2_DISABLE_BMSK                                    0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_2_DISABLE_SHFT                                    0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_1_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_1_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_0_DISABLE_BMSK                                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PCIE_0_DISABLE_SHFT                                    0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a8)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RSVD0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RSVD0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041ac)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RSVD0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RSVD0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b0)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_NIDEN_DISABLE_BMSK                       0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_NIDEN_DISABLE_SHFT                             0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS1_NIDEN_DISABLE_BMSK                     0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS1_NIDEN_DISABLE_SHFT                           0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS0_NIDEN_DISABLE_BMSK                     0x20000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS0_NIDEN_DISABLE_SHFT                           0x1d
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_MSS_DBGEN_DISABLE_BMSK                       0x10000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_MSS_DBGEN_DISABLE_SHFT                             0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GSS_A5_DBGEN_DISABLE_BMSK                     0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GSS_A5_DBGEN_DISABLE_SHFT                          0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                   0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                        0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                    0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                         0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_DBGEN_DISABLE_BMSK                     0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_DBGEN_DISABLE_SHFT                          0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_DBGEN_DISABLE_BMSK                         0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_DBGEN_DISABLE_SHFT                             0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_PIMEM_DBGEN_DISABLE_BMSK                       0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_PIMEM_DBGEN_DISABLE_SHFT                           0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_RPM_DBGEN_DISABLE_BMSK                         0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_RPM_DBGEN_DISABLE_SHFT                             0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                        0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                            0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_LPASS_DBGEN_DISABLE_BMSK                        0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_LPASS_DBGEN_DISABLE_SHFT                           0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_DBGEN_DISABLE_BMSK                          0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_DBGEN_DISABLE_SHFT                             0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS1_DBGEN_DISABLE_BMSK                        0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS1_DBGEN_DISABLE_SHFT                           0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS0_DBGEN_DISABLE_BMSK                        0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_APPS0_DBGEN_DISABLE_SHFT                           0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                        0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                           0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_RPM_DAPEN_DISABLE_BMSK                           0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_RPM_DAPEN_DISABLE_SHFT                              0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_ETM_DISABLE_BMSK                          0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SSC_Q6_ETM_DISABLE_SHFT                             0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE6_DISABLE_BMSK                              0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE6_DISABLE_SHFT                                 0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE5_DISABLE_BMSK                               0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE5_DISABLE_SHFT                                 0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE4_DISABLE_BMSK                               0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE4_DISABLE_SHFT                                 0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE3_DISABLE_BMSK                               0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE3_DISABLE_SHFT                                 0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE2_DISABLE_BMSK                               0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE2_DISABLE_SHFT                                 0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE1_DISABLE_BMSK                                0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE1_DISABLE_SHFT                                 0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE0_DISABLE_BMSK                                0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_SPARE0_DISABLE_SHFT                                 0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_NIDEN_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_NIDEN_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DBGEN_DISABLE_BMSK                            0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DBGEN_DISABLE_SHFT                             0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DAPEN_DISABLE_BMSK                             0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QC_GPMU_DAPEN_DISABLE_SHFT                             0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QDI_SPMI_DISABLE_BMSK                                  0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_QDI_SPMI_DISABLE_SHFT                                  0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_SM_BIST_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_SM_BIST_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TIC_DISABLE_BMSK                                       0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TIC_DISABLE_SHFT                                       0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b4)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGCRYPTODISABLE_BMSK                0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGCRYPTODISABLE_SHFT                      0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3V_BMSK                 0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3V_SHFT                       0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3TE_BMSK                0x20000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3TE_SHFT                      0x1d
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3EE_BMSK                0x10000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SYS_APCSCPUCFGRSTSCTLREL3EE_SHFT                      0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS1_CFGCPUPRESENT_N_BMSK                       0xc000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS1_CFGCPUPRESENT_N_SHFT                            0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS0_CFGCPUPRESENT_N_BMSK                       0x3000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS0_CFGCPUPRESENT_N_SHFT                            0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RSVD0_BMSK                                        0xf80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RSVD0_SHFT                                            0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPIDEN_DISABLE_BMSK                      0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPIDEN_DISABLE_SHFT                         0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPIDEN_DISABLE_BMSK                         0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPIDEN_DISABLE_SHFT                            0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPIDEN_DISABLE_BMSK                       0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPIDEN_DISABLE_SHFT                          0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPIDEN_DISABLE_BMSK                          0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPIDEN_DISABLE_SHFT                             0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPIDEN_DISABLE_BMSK                        0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPIDEN_DISABLE_SHFT                           0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPIDEN_DISABLE_BMSK                        0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPIDEN_DISABLE_SHFT                           0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPNIDEN_DISABLE_BMSK                      0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_SPNIDEN_DISABLE_SHFT                         0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPNIDEN_DISABLE_BMSK                          0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_SPNIDEN_DISABLE_SHFT                            0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPNIDEN_DISABLE_BMSK                        0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_SPNIDEN_DISABLE_SHFT                          0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPNIDEN_DISABLE_BMSK                          0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_DAP_SPNIDEN_DISABLE_SHFT                            0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPNIDEN_DISABLE_BMSK                        0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS1_SPNIDEN_DISABLE_SHFT                          0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPNIDEN_DISABLE_BMSK                         0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_APPS0_SPNIDEN_DISABLE_SHFT                          0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_MSS_NIDEN_DISABLE_BMSK                             0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_MSS_NIDEN_DISABLE_SHFT                              0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_NIDEN_DISABLE_BMSK                          0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_GSS_A5_NIDEN_DISABLE_SHFT                           0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_NIDEN_DISABLE_BMSK                             0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_SSC_NIDEN_DISABLE_SHFT                              0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_NIDEN_DISABLE_BMSK                            0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_PIMEM_NIDEN_DISABLE_SHFT                            0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_RPM_NIDEN_DISABLE_BMSK                              0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_RPM_NIDEN_DISABLE_SHFT                              0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_WCSS_NIDEN_DISABLE_BMSK                             0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_WCSS_NIDEN_DISABLE_SHFT                             0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_LPASS_NIDEN_DISABLE_BMSK                            0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_QC_LPASS_NIDEN_DISABLE_SHFT                            0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b8)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_TAP_GEN_SPARE_INSTR_DISABLE_13_0_BMSK           0xfffc0000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_TAP_GEN_SPARE_INSTR_DISABLE_13_0_SHFT                 0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_TAP_INSTR_DISABLE_BMSK                             0x3ffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_LSB_TAP_INSTR_DISABLE_SHFT                                 0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041bc)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                     0xfffc0000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                           0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_TAP_GEN_SPARE_INSTR_DISABLE_31_14_BMSK             0x3ffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW3_MSB_TAP_GEN_SPARE_INSTR_DISABLE_31_14_SHFT                 0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041c0)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MODEM_PBL_PATCH_VERSION_BMSK                    0xf8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MODEM_PBL_PATCH_VERSION_SHFT                          0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_PBL_PATCH_VERSION_BMSK                      0x7c00000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_PBL_PATCH_VERSION_SHFT                           0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RSVD0_BMSK                                        0x3c0000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RSVD0_SHFT                                            0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_PBL_BOOT_SPEED_BMSK                           0x30000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_PBL_BOOT_SPEED_SHFT                              0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RPM_PBL_BOOT_SPEED_BMSK                             0xc000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RPM_PBL_BOOT_SPEED_SHFT                                0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_BOOT_FROM_ROM_BMSK                             0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_APPS_BOOT_FROM_ROM_SHFT                                0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RPM_BOOT_FROM_ROM_BMSK                              0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_RPM_BOOT_FROM_ROM_SHFT                                 0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MODEM_BOOT_FROM_ROM_BMSK                             0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MODEM_BOOT_FROM_ROM_SHFT                               0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MSA_ENA_BMSK                                         0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_MSA_ENA_SHFT                                           0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_FORCE_MSA_AUTH_EN_BMSK                               0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_FORCE_MSA_AUTH_EN_SHFT                                 0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_ARM_CE_DISABLE_USAGE_BMSK                            0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_ARM_CE_DISABLE_USAGE_SHFT                              0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_BOOT_ROM_CFG_BMSK                                     0xff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_LSB_BOOT_ROM_CFG_SHFT                                      0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041c4)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_RSVD0_BMSK                                      0xffffc000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_RSVD0_SHFT                                             0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_FOUNDRY_ID_BMSK                                     0x3c00
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_FOUNDRY_ID_SHFT                                        0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_PLL_CFG_BMSK                                         0x3f0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_PLL_CFG_SHFT                                           0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_APPS_PBL_PLL_CTRL_BMSK                                 0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW4_MSB_APPS_PBL_PLL_CTRL_SHFT                                 0x0

#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041c8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_MAXn                                                        3
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_HASH_DATA0_BMSK                                    0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_LSB_HASH_DATA0_SHFT                                           0x0

#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041cc + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_MAXn                                                        3
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_HASH_DATA1_BMSK                                    0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH0_ROWn_MSB_HASH_DATA1_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041e8)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_BMSK                     0xf8000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_SHFT                           0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_BMSK                       0x7c00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_SHFT                            0x16
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_GP_HW_CALIB_BMSK                                        0x3c0000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_GP_HW_CALIB_SHFT                                            0x12
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_BMSK                                 0x3fc00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_SHFT                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_ENABLE_BMSK                                      0x200
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_ENABLE_SHFT                                        0x9
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                   0x1fe
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                     0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_ENABLE_BMSK                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_ENABLE_SHFT                                        0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041ec)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE0_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE0_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK           0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                 0x19
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK            0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                    0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_BMSK                          0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_SHFT                            0x5
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_BMSK                            0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_SHFT                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f0)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_SPARE0_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_SPARE0_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK           0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                 0x19
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK            0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_BMSK                    0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_SHFT                        0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_BMSK                     0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_SHFT                        0xa
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK                0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f4)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE0_BMSK                                           0xf8000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE0_SHFT                                                 0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_BANDGAP_TRIM_BMSK                                      0x7f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_BANDGAP_TRIM_SHFT                                           0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                    0xa
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK                0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f8)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_BMSK                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_SHFT                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_BMSK                               0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_SHFT                                     0x19
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_BMSK                                0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_SHFT                                     0x14
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_BMSK                                 0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                     0xf
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_BMSK                                   0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_SHFT                                      0xa
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_BMSK                                    0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_SHFT                                      0x5
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_BMSK                                     0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_SHFT                                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041fc)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_BMSK                           0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_SHFT                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_BMSK                               0xf800000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_SHFT                                    0x17
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_BMSK                                 0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_SHFT                                     0x12
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_BMSK                                  0x3e000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_SHFT                                      0xd
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_BMSK                                   0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_SHFT                                      0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                    0xf8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                     0x3
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_BMSK                                  0x7
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_SHFT                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004200)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR5_TARG_VOLT_NOM_0_BMSK                             0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR5_TARG_VOLT_NOM_0_SHFT                                   0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_SUTUR_BMSK                             0x7c000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_SUTUR_SHFT                                   0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_NOM_BMSK                                0x3e00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_NOM_SHFT                                     0x15
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_TUR_BMSK                                 0x1f0000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR4_TARG_VOLT_TUR_SHFT                                     0x10
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS2_BMSK                                  0xf800
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS2_SHFT                                     0xb
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS_BMSK                                    0x7c0
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_SVS_SHFT                                      0x6
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_NOM_BMSK                                     0x3e
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_NOM_SHFT                                      0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_TUR_4_BMSK                                    0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_CPR3_TARG_VOLT_TUR_4_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004204)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_TARG_VOLT_TUR_1_0_BMSK                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_TARG_VOLT_TUR_1_0_SHFT                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_SVS2_ROSEL_BMSK                                  0x3c000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_SVS2_ROSEL_SHFT                                        0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_SVS_ROSEL_BMSK                                    0x3c00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_SVS_ROSEL_SHFT                                         0x16
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_NOMINAL_ROSEL_BMSK                                 0x3c0000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_NOMINAL_ROSEL_SHFT                                     0x12
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_TURBO_ROSEL_BMSK                                    0x3c000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR6_TURBO_ROSEL_SHFT                                        0xe
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS2_BMSK                                  0x3e00
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS2_SHFT                                     0x9
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS_BMSK                                    0x1f0
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_SVS_SHFT                                      0x4
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_NOM_4_1_BMSK                                  0xf
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR5_TARG_VOLT_NOM_4_1_SHFT                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004208)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TURBO_QUOT_VMIN_9_0_BMSK                         0xffc00000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TURBO_QUOT_VMIN_9_0_SHFT                               0x16
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS2_BMSK                                0x3f0000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS2_SHFT                                    0x10
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS_BMSK                                   0xfc00
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_SVS_SHFT                                      0xa
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_NOM_BMSK                                    0x3f0
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_NOM_SHFT                                      0x4
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_TUR_5_2_BMSK                                  0xf
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CPR6_TARG_VOLT_TUR_5_2_SHFT                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000420c)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_SVS2_QUOT_VMIN_5_0_BMSK                          0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_SVS2_QUOT_VMIN_5_0_SHFT                                0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_SVS_QUOT_VMIN_BMSK                                0x3ffc000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_SVS_QUOT_VMIN_SHFT                                      0xe
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_NOMINAL_QUOT_VMIN_BMSK                               0x3ffc
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_NOMINAL_QUOT_VMIN_SHFT                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_TURBO_QUOT_VMIN_11_10_BMSK                              0x3
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CPR6_TURBO_QUOT_VMIN_11_10_SHFT                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004210)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR7_TURBO_ROSEL_1_0_BMSK                             0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR7_TURBO_ROSEL_1_0_SHFT                                   0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_SVS_QUOT_OFFSET_BMSK                             0x3fc00000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_SVS_QUOT_OFFSET_SHFT                                   0x16
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_NOMINAL_QUOT_OFFSET_BMSK                           0x3fc000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_NOMINAL_QUOT_OFFSET_SHFT                                0xe
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_TURBO_QUOT_OFFSET_BMSK                               0x3fc0
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_TURBO_QUOT_OFFSET_SHFT                                  0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_SVS2_QUOT_VMIN_11_6_BMSK                               0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CPR6_SVS2_QUOT_VMIN_11_6_SHFT                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004214)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_SVS_BMSK                               0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_SVS_SHFT                                     0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_NOM_BMSK                                0x3f00000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_NOM_SHFT                                     0x14
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_TUR_BMSK                                  0xfc000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TARG_VOLT_TUR_SHFT                                      0xe
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_SVS2_ROSEL_BMSK                                      0x3c00
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_SVS2_ROSEL_SHFT                                         0xa
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_SVS_ROSEL_BMSK                                        0x3c0
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_SVS_ROSEL_SHFT                                          0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_NOMINAL_ROSEL_BMSK                                     0x3c
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_NOMINAL_ROSEL_SHFT                                      0x2
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TURBO_ROSEL_3_2_BMSK                                    0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CPR7_TURBO_ROSEL_3_2_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004218)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_SVS_QUOT_VMIN_1_0_BMSK                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_SVS_QUOT_VMIN_1_0_SHFT                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_NOMINAL_QUOT_VMIN_BMSK                           0x3ffc0000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_NOMINAL_QUOT_VMIN_SHFT                                 0x12
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_TURBO_QUOT_VMIN_BMSK                                0x3ffc0
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_TURBO_QUOT_VMIN_SHFT                                    0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_TARG_VOLT_SVS2_BMSK                                    0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CPR7_TARG_VOLT_SVS2_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000421c)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_NOMINAL_QUOT_OFFSET_1_0_BMSK                     0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_NOMINAL_QUOT_OFFSET_1_0_SHFT                           0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_TURBO_QUOT_OFFSET_BMSK                           0x3fc00000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_TURBO_QUOT_OFFSET_SHFT                                 0x16
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_SVS2_QUOT_VMIN_BMSK                                0x3ffc00
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_SVS2_QUOT_VMIN_SHFT                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_SVS_QUOT_VMIN_11_2_BMSK                               0x3ff
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CPR7_SVS_QUOT_VMIN_11_2_SHFT                                 0x0

#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004220)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW7_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_SW_CAL_REDUN_258_256_BMSK                             0xe0000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_SW_CAL_REDUN_258_256_SHFT                                   0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS2_BMSK                              0x1f000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS2_SHFT                                    0x18
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS_BMSK                                 0xf80000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_SVS_SHFT                                     0x13
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_NOM_BMSK                                  0x7c000
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR8_TARG_VOLT_NOM_SHFT                                      0xe
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR7_SVS_QUOT_OFFSET_BMSK                                 0x3fc0
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR7_SVS_QUOT_OFFSET_SHFT                                    0x6
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR7_NOMINAL_QUOT_OFFSET_7_2_BMSK                           0x3f
#define HWIO_QFPROM_CORR_CALIB_ROW7_LSB_CPR7_NOMINAL_QUOT_OFFSET_7_2_SHFT                            0x0

#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004224)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW7_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_PH_B0M0_BMSK                                          0xe0000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_PH_B0M0_SHFT                                                0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_G_B0_BMSK                                             0x1c000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_G_B0_SHFT                                                   0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SAR_B0_BMSK                                            0x3000000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SAR_B0_SHFT                                                 0x18
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_APPS_APC_ODCM_CAL_SEL_BMSK                              0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_APPS_APC_ODCM_CAL_SEL_SHFT                                  0x15
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RSVD0_BMSK                                              0x1f8000
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_RSVD0_SHFT                                                   0xf
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SW_CAL_REDUN_273_259_BMSK                                 0x7fff
#define HWIO_QFPROM_CORR_CALIB_ROW7_MSB_SW_CAL_REDUN_273_259_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004228)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW8_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_VREF_B1_BMSK                                          0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_VREF_B1_SHFT                                                0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M3_BMSK                                          0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M3_SHFT                                                0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M2_BMSK                                           0x7000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M2_SHFT                                                0x18
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M1_BMSK                                            0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M1_SHFT                                                0x15
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M0_BMSK                                            0x1c0000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B1M0_SHFT                                                0x12
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_G_B1_BMSK                                                0x38000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_G_B1_SHFT                                                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_SAR_B1_BMSK                                               0x6000
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_SAR_B1_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CLK_B0_BMSK                                               0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_CLK_B0_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_VREF_B0_BMSK                                               0x600
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_VREF_B0_SHFT                                                 0x9
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M3_BMSK                                               0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M3_SHFT                                                 0x6
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M2_BMSK                                                0x38
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M2_SHFT                                                 0x3
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M1_BMSK                                                 0x7
#define HWIO_QFPROM_CORR_CALIB_ROW8_LSB_PH_B0M1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000422c)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW8_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RSVD0_BMSK                                            0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_RSVD0_SHFT                                                  0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B3M0_BMSK                                          0x70000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B3M0_SHFT                                                0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_G_B3_BMSK                                              0xe000000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_G_B3_SHFT                                                   0x19
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_SAR_B3_BMSK                                            0x1800000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_SAR_B3_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CLK_B2_BMSK                                             0x600000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CLK_B2_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_VREF_B2_BMSK                                            0x180000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_VREF_B2_SHFT                                                0x13
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M3_BMSK                                             0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M3_SHFT                                                0x10
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M2_BMSK                                              0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M2_SHFT                                                 0xd
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M1_BMSK                                              0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M1_SHFT                                                 0xa
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M0_BMSK                                               0x380
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_PH_B2M0_SHFT                                                 0x7
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_G_B2_BMSK                                                   0x70
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_G_B2_SHFT                                                    0x4
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_SAR_B2_BMSK                                                  0xc
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_SAR_B2_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CLK_B1_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW8_MSB_CLK_B1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004230)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW9_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_VREF_B4_BMSK                                          0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_VREF_B4_SHFT                                                0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M3_BMSK                                          0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M3_SHFT                                                0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M2_BMSK                                           0x7000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M2_SHFT                                                0x18
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M1_BMSK                                            0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M1_SHFT                                                0x15
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M0_BMSK                                            0x1c0000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B4M0_SHFT                                                0x12
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_G_B4_BMSK                                                0x38000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_G_B4_SHFT                                                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_SAR_B4_BMSK                                               0x6000
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_SAR_B4_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CLK_B3_BMSK                                               0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_CLK_B3_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_VREF_B3_BMSK                                               0x600
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_VREF_B3_SHFT                                                 0x9
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M3_BMSK                                               0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M3_SHFT                                                 0x6
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M2_BMSK                                                0x38
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M2_SHFT                                                 0x3
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M1_BMSK                                                 0x7
#define HWIO_QFPROM_CORR_CALIB_ROW9_LSB_PH_B3M1_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004234)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW9_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RSVD0_BMSK                                            0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_RSVD0_SHFT                                                  0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B6M0_BMSK                                          0x70000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B6M0_SHFT                                                0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_G_B6_BMSK                                              0xe000000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_G_B6_SHFT                                                   0x19
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SAR_B6_BMSK                                            0x1800000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SAR_B6_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CLK_B5_BMSK                                             0x600000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CLK_B5_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_VREF_B5_BMSK                                            0x180000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_VREF_B5_SHFT                                                0x13
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M3_BMSK                                             0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M3_SHFT                                                0x10
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M2_BMSK                                              0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M2_SHFT                                                 0xd
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M1_BMSK                                              0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M1_SHFT                                                 0xa
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M0_BMSK                                               0x380
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_PH_B5M0_SHFT                                                 0x7
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_G_B5_BMSK                                                   0x70
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_G_B5_SHFT                                                    0x4
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SAR_B5_BMSK                                                  0xc
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_SAR_B5_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CLK_B4_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW9_MSB_CLK_B4_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004238)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_BMSK                                0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_SHFT                                      0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE0_BMSK                                     0x3ff0000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE0_SHFT                                          0x10
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS_CAL_SEL_BMSK                                       0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS_CAL_SEL_SHFT                                          0xd
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CLK_B6_BMSK                                              0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CLK_B6_SHFT                                                 0xb
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_VREF_B6_BMSK                                              0x600
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_VREF_B6_SHFT                                                0x9
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M3_BMSK                                              0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M3_SHFT                                                0x6
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M2_BMSK                                               0x38
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M2_SHFT                                                0x3
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M1_BMSK                                                0x7
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M1_SHFT                                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000423c)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE1_BMSK                                      0xffc000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE1_SHFT                                           0xe
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE0_BMSK                                        0x3ff0
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE0_SHFT                                           0x4
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_BMSK                                       0xf
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_SHFT                                       0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004240)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS9_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS9_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS8_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS8_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS7_OFFSET_BMSK                                     0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS7_OFFSET_SHFT                                         0x14
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS6_OFFSET_BMSK                                      0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS6_OFFSET_SHFT                                         0x10
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS5_OFFSET_BMSK                                       0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS5_OFFSET_SHFT                                          0xc
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS4_OFFSET_BMSK                                        0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS4_OFFSET_SHFT                                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS3_OFFSET_BMSK                                         0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS3_OFFSET_SHFT                                          0x4
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS2_OFFSET_BMSK                                          0xf
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS2_OFFSET_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004244)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS17_OFFSET_BMSK                                  0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS17_OFFSET_SHFT                                        0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS16_OFFSET_BMSK                                   0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS16_OFFSET_SHFT                                        0x18
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS15_OFFSET_BMSK                                    0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS15_OFFSET_SHFT                                        0x14
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS14_OFFSET_BMSK                                     0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS14_OFFSET_SHFT                                        0x10
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS13_OFFSET_BMSK                                      0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS13_OFFSET_SHFT                                         0xc
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS12_OFFSET_BMSK                                       0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS12_OFFSET_SHFT                                         0x8
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS11_OFFSET_BMSK                                        0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS11_OFFSET_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS10_OFFSET_BMSK                                         0xf
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS10_OFFSET_SHFT                                         0x0

#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004248)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW12_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_BMSK                              0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_SHFT                                    0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_BMSK                                  0x40000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_SHFT                                        0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_BMSK                               0x20000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_SHFT                                     0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_BMSK                               0x10000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_SHFT                                     0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_BMSK                                       0xff00000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_SHFT                                            0x14
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_BMSK                                         0xff000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_SHFT                                             0xc
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS20_OFFSET_BMSK                                       0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS20_OFFSET_SHFT                                         0x8
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS19_OFFSET_BMSK                                        0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS19_OFFSET_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS18_OFFSET_BMSK                                         0xf
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS18_OFFSET_SHFT                                         0x0

#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000424c)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_BMSK                                0xe0000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_SHFT                                      0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT1_HSTX_TRIM_LSB_BMSK                             0x1e000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT1_HSTX_TRIM_LSB_SHFT                                   0x19
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_BMSK                              0x1e00000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_SHFT                                   0x15
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_BMSK                              0x1fff80
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_SHFT                                   0x7
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VCM_BMSK                                          0x60
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VCM_SHFT                                           0x5
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_LDO_BMSK                                          0x18
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_LDO_SHFT                                           0x3
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VREF_BMSK                                          0x6
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VREF_SHFT                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_BMSK                                     0x1
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004250)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW13_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_SHFT                                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004254)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW13_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_BMSK                              0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004258)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW14_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_BMSK                              0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000425c)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW14_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_BMSK                             0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004260)
#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW15_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW15_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW15_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_SW_CAL_REDUN_159_128_BMSK                            0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW15_LSB_SW_CAL_REDUN_159_128_SHFT                                   0x0

#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004264)
#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW15_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW15_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW15_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_SW_CAL_REDUN_191_160_BMSK                            0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW15_MSB_SW_CAL_REDUN_191_160_SHFT                                   0x0

#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004268)
#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW16_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW16_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_SW_CAL_REDUN_223_192_BMSK                            0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW16_LSB_SW_CAL_REDUN_223_192_SHFT                                   0x0

#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000426c)
#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW16_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW16_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_SW_CAL_REDUN_255_224_BMSK                            0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW16_MSB_SW_CAL_REDUN_255_224_SHFT                                   0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n)                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004270 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_MAXn                                                     11
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                         0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n)                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004274 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_MAXn                                                     11
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                         0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004310)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_ADDR, HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_QC_SPARE_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_LSB_QC_SPARE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004314)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_ADDR, HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_QC_SPARE_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG15_MSB_QC_SPARE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004318)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_QC_SPARE_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_LSB_QC_SPARE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000431c)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_QC_SPARE_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REG16_MSB_QC_SPARE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004298 + 0x8 * (n))
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_MAXn                                                       21
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_ADDR(n), HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_QC_SPARE_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_LSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000429c + 0x8 * (n))
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_RMSK                                                 0xffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_MAXn                                                       21
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_ADDR(n), HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_QC_SPARE_BMSK                                        0xffffff
#define HWIO_QFPROM_CORR_QC_SPARE_REGn_MSB_QC_SPARE_SHFT                                             0x0

#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n)                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004348 + 0x8 * (n))
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_MAXn                                               1
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_BMSK                            0xffffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_SHFT                                   0x0

#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n)                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000434c + 0x8 * (n))
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_RMSK                                        0xffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_MAXn                                               1
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_BMSK                              0xffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_SHFT                                   0x0

#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004358)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_RMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_BMSK                                0xffff0000
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_SHFT                                      0x10
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_BMSK                                0xffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_SHFT                                   0x0

#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000435c)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_RMSK                                        0xffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_BMSK                                  0xffffff
#define HWIO_QFPROM_CORR_QC_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_SHFT                                       0x0

#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n)                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004360 + 0x8 * (n))
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_RMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_MAXn                                              1
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_BMSK                           0xffffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_LSB_KEY_DATA0_SHFT                                  0x0

#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n)                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004364 + 0x8 * (n))
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RMSK                                       0xffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_MAXn                                              1
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_BMSK                             0xffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROWn_MSB_KEY_DATA1_SHFT                                  0x0

#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004370)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_BMSK                               0xffff0000
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_RSVD0_SHFT                                     0x10
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_BMSK                               0xffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_LSB_KEY_DATA0_SHFT                                  0x0

#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004374)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RMSK                                       0xffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_BMSK                                 0xffffff
#define HWIO_QFPROM_CORR_OEM_IMAGE_ENCR_KEY_ROW2_MSB_RSVD0_SHFT                                      0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_ADDR(n)                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004378 + 0x8 * (n))
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_RMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_MAXn                                                    1
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT4_BMSK                                 0xff000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT4_SHFT                                       0x18
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT3_BMSK                                   0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT3_SHFT                                       0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT2_BMSK                                     0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT2_SHFT                                        0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT1_BMSK                                       0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_LSB_SEC_BOOT1_SHFT                                        0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_ADDR(n)                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000437c + 0x8 * (n))
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_RMSK                                             0xffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_MAXn                                                    1
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT7_BMSK                                   0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT7_SHFT                                       0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT6_BMSK                                     0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT6_SHFT                                        0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT5_BMSK                                       0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROWn_MSB_SEC_BOOT5_SHFT                                        0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004388 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                          3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000438c + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                          3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                              0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043a8)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043ac)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RMSK                                   0xffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_BMSK                             0xffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_SHFT                                  0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                          3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                          3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                              0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043d0)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_RMSK                                 0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_BMSK                       0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_LSB_KEY_DATA0_SHFT                              0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043d4)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RMSK                                   0xffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_BMSK                             0xffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROW4_MSB_RSVD0_SHFT                                  0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043d8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_MAXn                                                      55
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x000043dc + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK                                                0xffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_MAXn                                                      55
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RSVD0_BMSK                                          0xfe0000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RSVD0_SHFT                                              0x11
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                      0x1fffe
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                          0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                            0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                            0x0

#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004598)
#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KSV_LSB_ADDR, HWIO_QFPROM_CORR_HDCP_KSV_LSB_RMSK)
#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KSV_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_KSV0_BMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_HDCP_KSV_LSB_KSV0_SHFT                                                      0x0

#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000459c)
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_RMSK                                                      0xffffff
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KSV_MSB_ADDR, HWIO_QFPROM_CORR_HDCP_KSV_MSB_RMSK)
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KSV_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_RSVD0_BMSK                                                0xffff00
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_RSVD0_SHFT                                                     0x8
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_KSV1_BMSK                                                     0xff
#define HWIO_QFPROM_CORR_HDCP_KSV_MSB_KSV1_SHFT                                                      0x0

#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004598 + 0x8 * (n))
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_MAXn                                                       40
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_KEY_DATA_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_LSB_KEY_DATA_SHFT                                             0x0

#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000459c + 0x8 * (n))
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_RMSK                                                 0xffffff
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_MAXn                                                       40
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_KEY_DATA_BMSK                                        0xffffff
#define HWIO_QFPROM_CORR_HDCP_KEY_ROWn_MSB_KEY_DATA_SHFT                                             0x0

#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_ADDR(n)                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004510 + 0x10 * (n))
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_MAXn                                                 46
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_OEM_SPARE_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_OEM_SPARE_SHFT                                      0x0

#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_ADDR(n)                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004514 + 0x10 * (n))
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_MAXn                                                 46
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_OEM_SPARE_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_MSB_OEM_SPARE_SHFT                                      0x0

#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_ADDR(n)                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004518 + 0x10 * (n))
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_MAXn                                                 46
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_OEM_SPARE_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_LSB_OEM_SPARE_SHFT                                      0x0

#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_ADDR(n)                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000451c + 0x10 * (n))
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_RMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_MAXn                                                 46
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_ADDR(n), HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_OEM_SPARE_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW1_MSB_OEM_SPARE_SHFT                                      0x0

#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n)                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004800 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK                                                    0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_MAXn                                                            39
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                        0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                               0x0

#define HWIO_SEC_CTRL_HW_VERSION_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006000)
#define HWIO_SEC_CTRL_HW_VERSION_RMSK                                                         0xffffffff
#define HWIO_SEC_CTRL_HW_VERSION_IN          \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, HWIO_SEC_CTRL_HW_VERSION_RMSK)
#define HWIO_SEC_CTRL_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, m)
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_BMSK                                                   0xf0000000
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_SHFT                                                         0x1c
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_BMSK                                                    0xfff0000
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_SHFT                                                         0x10
#define HWIO_SEC_CTRL_HW_VERSION_STEP_BMSK                                                        0xffff
#define HWIO_SEC_CTRL_HW_VERSION_STEP_SHFT                                                           0x0

#define HWIO_FEATURE_CONFIG0_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006004)
#define HWIO_FEATURE_CONFIG0_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG0_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, HWIO_FEATURE_CONFIG0_RMSK)
#define HWIO_FEATURE_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, m)
#define HWIO_FEATURE_CONFIG0_UFS_SW_CONTROL_DISABLE_BMSK                                      0x80000000
#define HWIO_FEATURE_CONFIG0_UFS_SW_CONTROL_DISABLE_SHFT                                            0x1f
#define HWIO_FEATURE_CONFIG0_SATA_DISABLE_BMSK                                                0x40000000
#define HWIO_FEATURE_CONFIG0_SATA_DISABLE_SHFT                                                      0x1e
#define HWIO_FEATURE_CONFIG0_SECURE_CHANNEL_DISABLE_BMSK                                      0x20000000
#define HWIO_FEATURE_CONFIG0_SECURE_CHANNEL_DISABLE_SHFT                                            0x1d
#define HWIO_FEATURE_CONFIG0_SCM_DISABLE_BMSK                                                 0x10000000
#define HWIO_FEATURE_CONFIG0_SCM_DISABLE_SHFT                                                       0x1c
#define HWIO_FEATURE_CONFIG0_ICE_FORCE_HW_KEY1_BMSK                                            0x8000000
#define HWIO_FEATURE_CONFIG0_ICE_FORCE_HW_KEY1_SHFT                                                 0x1b
#define HWIO_FEATURE_CONFIG0_ICE_FORCE_HW_KEY0_BMSK                                            0x4000000
#define HWIO_FEATURE_CONFIG0_ICE_FORCE_HW_KEY0_SHFT                                                 0x1a
#define HWIO_FEATURE_CONFIG0_ICE_DISABLE_BMSK                                                  0x2000000
#define HWIO_FEATURE_CONFIG0_ICE_DISABLE_SHFT                                                       0x19
#define HWIO_FEATURE_CONFIG0_RICA_DISABLE_BMSK                                                 0x1000000
#define HWIO_FEATURE_CONFIG0_RICA_DISABLE_SHFT                                                      0x18
#define HWIO_FEATURE_CONFIG0_SPI_SLAVE_DISABLE_BMSK                                             0x800000
#define HWIO_FEATURE_CONFIG0_SPI_SLAVE_DISABLE_SHFT                                                 0x17
#define HWIO_FEATURE_CONFIG0_APPS_ACG_DISABLE_BMSK                                              0x400000
#define HWIO_FEATURE_CONFIG0_APPS_ACG_DISABLE_SHFT                                                  0x16
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_SEL1_BMSK                                              0x200000
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_SEL1_SHFT                                                  0x15
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_SEL0_BMSK                                              0x100000
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_SEL0_SHFT                                                  0x14
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_DISABLE_BMSK                                            0x80000
#define HWIO_FEATURE_CONFIG0_GFX3D_TURBO_DISABLE_SHFT                                               0x13
#define HWIO_FEATURE_CONFIG0_VENUS_HEVC_ENCODE_DISABLE_BMSK                                      0x40000
#define HWIO_FEATURE_CONFIG0_VENUS_HEVC_ENCODE_DISABLE_SHFT                                         0x12
#define HWIO_FEATURE_CONFIG0_VENUS_HEVC_DECODE_DISABLE_BMSK                                      0x20000
#define HWIO_FEATURE_CONFIG0_VENUS_HEVC_DECODE_DISABLE_SHFT                                         0x11
#define HWIO_FEATURE_CONFIG0_VENUS_4K_DISABLE_BMSK                                               0x10000
#define HWIO_FEATURE_CONFIG0_VENUS_4K_DISABLE_SHFT                                                  0x10
#define HWIO_FEATURE_CONFIG0_VP8_DECODER_DISABLE_BMSK                                             0x8000
#define HWIO_FEATURE_CONFIG0_VP8_DECODER_DISABLE_SHFT                                                0xf
#define HWIO_FEATURE_CONFIG0_VP8_ENCODER_DISABLE_BMSK                                             0x4000
#define HWIO_FEATURE_CONFIG0_VP8_ENCODER_DISABLE_SHFT                                                0xe
#define HWIO_FEATURE_CONFIG0_HDMI_DISABLE_BMSK                                                    0x2000
#define HWIO_FEATURE_CONFIG0_HDMI_DISABLE_SHFT                                                       0xd
#define HWIO_FEATURE_CONFIG0_HDCP_DISABLE_BMSK                                                    0x1000
#define HWIO_FEATURE_CONFIG0_HDCP_DISABLE_SHFT                                                       0xc
#define HWIO_FEATURE_CONFIG0_MDP_APICAL_LTC_DISABLE_BMSK                                           0x800
#define HWIO_FEATURE_CONFIG0_MDP_APICAL_LTC_DISABLE_SHFT                                             0xb
#define HWIO_FEATURE_CONFIG0_EDP_DISABLE_BMSK                                                      0x400
#define HWIO_FEATURE_CONFIG0_EDP_DISABLE_SHFT                                                        0xa
#define HWIO_FEATURE_CONFIG0_DSI_1_DISABLE_BMSK                                                    0x200
#define HWIO_FEATURE_CONFIG0_DSI_1_DISABLE_SHFT                                                      0x9
#define HWIO_FEATURE_CONFIG0_DSI_0_DISABLE_BMSK                                                    0x100
#define HWIO_FEATURE_CONFIG0_DSI_0_DISABLE_SHFT                                                      0x8
#define HWIO_FEATURE_CONFIG0_FD_DISABLE_BMSK                                                        0x80
#define HWIO_FEATURE_CONFIG0_FD_DISABLE_SHFT                                                         0x7
#define HWIO_FEATURE_CONFIG0_CSID_DPCM_14_DISABLE_BMSK                                              0x40
#define HWIO_FEATURE_CONFIG0_CSID_DPCM_14_DISABLE_SHFT                                               0x6
#define HWIO_FEATURE_CONFIG0_ISP_1_DISABLE_BMSK                                                     0x20
#define HWIO_FEATURE_CONFIG0_ISP_1_DISABLE_SHFT                                                      0x5
#define HWIO_FEATURE_CONFIG0_CSID_3_DISABLE_BMSK                                                    0x10
#define HWIO_FEATURE_CONFIG0_CSID_3_DISABLE_SHFT                                                     0x4
#define HWIO_FEATURE_CONFIG0_CSID_2_DISABLE_BMSK                                                     0x8
#define HWIO_FEATURE_CONFIG0_CSID_2_DISABLE_SHFT                                                     0x3
#define HWIO_FEATURE_CONFIG0_BOOT_ROM_PATCH_DISABLE_BMSK                                             0x7
#define HWIO_FEATURE_CONFIG0_BOOT_ROM_PATCH_DISABLE_SHFT                                             0x0

#define HWIO_FEATURE_CONFIG1_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006008)
#define HWIO_FEATURE_CONFIG1_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG1_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, HWIO_FEATURE_CONFIG1_RMSK)
#define HWIO_FEATURE_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, m)
#define HWIO_FEATURE_CONFIG1_RSVD0_BMSK                                                       0xff800000
#define HWIO_FEATURE_CONFIG1_RSVD0_SHFT                                                             0x17
#define HWIO_FEATURE_CONFIG1_APS_RESET_DISABLE_BMSK                                             0x400000
#define HWIO_FEATURE_CONFIG1_APS_RESET_DISABLE_SHFT                                                 0x16
#define HWIO_FEATURE_CONFIG1_HVX_DISABLE_BMSK                                                   0x200000
#define HWIO_FEATURE_CONFIG1_HVX_DISABLE_SHFT                                                       0x15
#define HWIO_FEATURE_CONFIG1_APB2JTAG_LIFECYCLE1_DIS_BMSK                                       0x100000
#define HWIO_FEATURE_CONFIG1_APB2JTAG_LIFECYCLE1_DIS_SHFT                                           0x14
#define HWIO_FEATURE_CONFIG1_APB2JTAG_LIFECYCLE0_EN_BMSK                                         0x80000
#define HWIO_FEATURE_CONFIG1_APB2JTAG_LIFECYCLE0_EN_SHFT                                            0x13
#define HWIO_FEATURE_CONFIG1_STACKED_MEMORY_ID_BMSK                                              0x7c000
#define HWIO_FEATURE_CONFIG1_STACKED_MEMORY_ID_SHFT                                                  0xe
#define HWIO_FEATURE_CONFIG1_PRNG_TESTMODE_DISABLE_BMSK                                           0x2000
#define HWIO_FEATURE_CONFIG1_PRNG_TESTMODE_DISABLE_SHFT                                              0xd
#define HWIO_FEATURE_CONFIG1_DCC_DISABLE_BMSK                                                     0x1000
#define HWIO_FEATURE_CONFIG1_DCC_DISABLE_SHFT                                                        0xc
#define HWIO_FEATURE_CONFIG1_MOCHA_PART_BMSK                                                       0x800
#define HWIO_FEATURE_CONFIG1_MOCHA_PART_SHFT                                                         0xb
#define HWIO_FEATURE_CONFIG1_NIDNT_DISABLE_BMSK                                                    0x400
#define HWIO_FEATURE_CONFIG1_NIDNT_DISABLE_SHFT                                                      0xa
#define HWIO_FEATURE_CONFIG1_SMMU_DISABLE_BMSK                                                     0x200
#define HWIO_FEATURE_CONFIG1_SMMU_DISABLE_SHFT                                                       0x9
#define HWIO_FEATURE_CONFIG1_VENDOR_LOCK_BMSK                                                      0x1e0
#define HWIO_FEATURE_CONFIG1_VENDOR_LOCK_SHFT                                                        0x5
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                                       0x10
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                                        0x4
#define HWIO_FEATURE_CONFIG1_NAV_DISABLE_BMSK                                                        0x8
#define HWIO_FEATURE_CONFIG1_NAV_DISABLE_SHFT                                                        0x3
#define HWIO_FEATURE_CONFIG1_PCIE_2_DISABLE_BMSK                                                     0x4
#define HWIO_FEATURE_CONFIG1_PCIE_2_DISABLE_SHFT                                                     0x2
#define HWIO_FEATURE_CONFIG1_PCIE_1_DISABLE_BMSK                                                     0x2
#define HWIO_FEATURE_CONFIG1_PCIE_1_DISABLE_SHFT                                                     0x1
#define HWIO_FEATURE_CONFIG1_PCIE_0_DISABLE_BMSK                                                     0x1
#define HWIO_FEATURE_CONFIG1_PCIE_0_DISABLE_SHFT                                                     0x0

#define HWIO_FEATURE_CONFIG2_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000600c)
#define HWIO_FEATURE_CONFIG2_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG2_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, HWIO_FEATURE_CONFIG2_RMSK)
#define HWIO_FEATURE_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, m)
#define HWIO_FEATURE_CONFIG2_RSVD0_BMSK                                                       0xffffffff
#define HWIO_FEATURE_CONFIG2_RSVD0_SHFT                                                              0x0

#define HWIO_FEATURE_CONFIG3_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006010)
#define HWIO_FEATURE_CONFIG3_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG3_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, HWIO_FEATURE_CONFIG3_RMSK)
#define HWIO_FEATURE_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, m)
#define HWIO_FEATURE_CONFIG3_RSVD0_BMSK                                                       0xffffffff
#define HWIO_FEATURE_CONFIG3_RSVD0_SHFT                                                              0x0

#define HWIO_FEATURE_CONFIG4_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006014)
#define HWIO_FEATURE_CONFIG4_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG4_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, HWIO_FEATURE_CONFIG4_RMSK)
#define HWIO_FEATURE_CONFIG4_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, m)
#define HWIO_FEATURE_CONFIG4_QC_DAP_NIDEN_DISABLE_BMSK                                        0x80000000
#define HWIO_FEATURE_CONFIG4_QC_DAP_NIDEN_DISABLE_SHFT                                              0x1f
#define HWIO_FEATURE_CONFIG4_QC_APPS1_NIDEN_DISABLE_BMSK                                      0x40000000
#define HWIO_FEATURE_CONFIG4_QC_APPS1_NIDEN_DISABLE_SHFT                                            0x1e
#define HWIO_FEATURE_CONFIG4_QC_APPS0_NIDEN_DISABLE_BMSK                                      0x20000000
#define HWIO_FEATURE_CONFIG4_QC_APPS0_NIDEN_DISABLE_SHFT                                            0x1d
#define HWIO_FEATURE_CONFIG4_QC_MSS_DBGEN_DISABLE_BMSK                                        0x10000000
#define HWIO_FEATURE_CONFIG4_QC_MSS_DBGEN_DISABLE_SHFT                                              0x1c
#define HWIO_FEATURE_CONFIG4_QC_GSS_A5_DBGEN_DISABLE_BMSK                                      0x8000000
#define HWIO_FEATURE_CONFIG4_QC_GSS_A5_DBGEN_DISABLE_SHFT                                           0x1b
#define HWIO_FEATURE_CONFIG4_QC_A5X_ISDB_DBGEN_DISABLE_BMSK                                    0x4000000
#define HWIO_FEATURE_CONFIG4_QC_A5X_ISDB_DBGEN_DISABLE_SHFT                                         0x1a
#define HWIO_FEATURE_CONFIG4_QC_VENUS_0_DBGEN_DISABLE_BMSK                                     0x2000000
#define HWIO_FEATURE_CONFIG4_QC_VENUS_0_DBGEN_DISABLE_SHFT                                          0x19
#define HWIO_FEATURE_CONFIG4_QC_SSC_Q6_DBGEN_DISABLE_BMSK                                      0x1000000
#define HWIO_FEATURE_CONFIG4_QC_SSC_Q6_DBGEN_DISABLE_SHFT                                           0x18
#define HWIO_FEATURE_CONFIG4_QC_SSC_DBGEN_DISABLE_BMSK                                          0x800000
#define HWIO_FEATURE_CONFIG4_QC_SSC_DBGEN_DISABLE_SHFT                                              0x17
#define HWIO_FEATURE_CONFIG4_QC_PIMEM_DBGEN_DISABLE_BMSK                                        0x400000
#define HWIO_FEATURE_CONFIG4_QC_PIMEM_DBGEN_DISABLE_SHFT                                            0x16
#define HWIO_FEATURE_CONFIG4_QC_RPM_DBGEN_DISABLE_BMSK                                          0x200000
#define HWIO_FEATURE_CONFIG4_QC_RPM_DBGEN_DISABLE_SHFT                                              0x15
#define HWIO_FEATURE_CONFIG4_QC_WCSS_DBGEN_DISABLE_BMSK                                         0x100000
#define HWIO_FEATURE_CONFIG4_QC_WCSS_DBGEN_DISABLE_SHFT                                             0x14
#define HWIO_FEATURE_CONFIG4_QC_LPASS_DBGEN_DISABLE_BMSK                                         0x80000
#define HWIO_FEATURE_CONFIG4_QC_LPASS_DBGEN_DISABLE_SHFT                                            0x13
#define HWIO_FEATURE_CONFIG4_QC_DAP_DBGEN_DISABLE_BMSK                                           0x40000
#define HWIO_FEATURE_CONFIG4_QC_DAP_DBGEN_DISABLE_SHFT                                              0x12
#define HWIO_FEATURE_CONFIG4_QC_APPS1_DBGEN_DISABLE_BMSK                                         0x20000
#define HWIO_FEATURE_CONFIG4_QC_APPS1_DBGEN_DISABLE_SHFT                                            0x11
#define HWIO_FEATURE_CONFIG4_QC_APPS0_DBGEN_DISABLE_BMSK                                         0x10000
#define HWIO_FEATURE_CONFIG4_QC_APPS0_DBGEN_DISABLE_SHFT                                            0x10
#define HWIO_FEATURE_CONFIG4_QC_DAP_DEVICEEN_DISABLE_BMSK                                         0x8000
#define HWIO_FEATURE_CONFIG4_QC_DAP_DEVICEEN_DISABLE_SHFT                                            0xf
#define HWIO_FEATURE_CONFIG4_QC_RPM_DAPEN_DISABLE_BMSK                                            0x4000
#define HWIO_FEATURE_CONFIG4_QC_RPM_DAPEN_DISABLE_SHFT                                               0xe
#define HWIO_FEATURE_CONFIG4_QC_SSC_Q6_ETM_DISABLE_BMSK                                           0x2000
#define HWIO_FEATURE_CONFIG4_QC_SSC_Q6_ETM_DISABLE_SHFT                                              0xd
#define HWIO_FEATURE_CONFIG4_QC_SPARE6_DISABLE_BMSK                                               0x1000
#define HWIO_FEATURE_CONFIG4_QC_SPARE6_DISABLE_SHFT                                                  0xc
#define HWIO_FEATURE_CONFIG4_QC_SPARE5_DISABLE_BMSK                                                0x800
#define HWIO_FEATURE_CONFIG4_QC_SPARE5_DISABLE_SHFT                                                  0xb
#define HWIO_FEATURE_CONFIG4_QC_SPARE4_DISABLE_BMSK                                                0x400
#define HWIO_FEATURE_CONFIG4_QC_SPARE4_DISABLE_SHFT                                                  0xa
#define HWIO_FEATURE_CONFIG4_QC_SPARE3_DISABLE_BMSK                                                0x200
#define HWIO_FEATURE_CONFIG4_QC_SPARE3_DISABLE_SHFT                                                  0x9
#define HWIO_FEATURE_CONFIG4_QC_SPARE2_DISABLE_BMSK                                                0x100
#define HWIO_FEATURE_CONFIG4_QC_SPARE2_DISABLE_SHFT                                                  0x8
#define HWIO_FEATURE_CONFIG4_QC_SPARE1_DISABLE_BMSK                                                 0x80
#define HWIO_FEATURE_CONFIG4_QC_SPARE1_DISABLE_SHFT                                                  0x7
#define HWIO_FEATURE_CONFIG4_QC_SPARE0_DISABLE_BMSK                                                 0x40
#define HWIO_FEATURE_CONFIG4_QC_SPARE0_DISABLE_SHFT                                                  0x6
#define HWIO_FEATURE_CONFIG4_QC_GPMU_NIDEN_DISABLE_BMSK                                             0x20
#define HWIO_FEATURE_CONFIG4_QC_GPMU_NIDEN_DISABLE_SHFT                                              0x5
#define HWIO_FEATURE_CONFIG4_QC_GPMU_DBGEN_DISABLE_BMSK                                             0x10
#define HWIO_FEATURE_CONFIG4_QC_GPMU_DBGEN_DISABLE_SHFT                                              0x4
#define HWIO_FEATURE_CONFIG4_QC_GPMU_DAPEN_DISABLE_BMSK                                              0x8
#define HWIO_FEATURE_CONFIG4_QC_GPMU_DAPEN_DISABLE_SHFT                                              0x3
#define HWIO_FEATURE_CONFIG4_QDI_SPMI_DISABLE_BMSK                                                   0x4
#define HWIO_FEATURE_CONFIG4_QDI_SPMI_DISABLE_SHFT                                                   0x2
#define HWIO_FEATURE_CONFIG4_SM_BIST_DISABLE_BMSK                                                    0x2
#define HWIO_FEATURE_CONFIG4_SM_BIST_DISABLE_SHFT                                                    0x1
#define HWIO_FEATURE_CONFIG4_TIC_DISABLE_BMSK                                                        0x1
#define HWIO_FEATURE_CONFIG4_TIC_DISABLE_SHFT                                                        0x0

#define HWIO_FEATURE_CONFIG5_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006018)
#define HWIO_FEATURE_CONFIG5_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG5_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, HWIO_FEATURE_CONFIG5_RMSK)
#define HWIO_FEATURE_CONFIG5_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, m)
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGCRYPTODISABLE_BMSK                                 0x80000000
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGCRYPTODISABLE_SHFT                                       0x1f
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3V_BMSK                                  0x40000000
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3V_SHFT                                        0x1e
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3TE_BMSK                                 0x20000000
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3TE_SHFT                                       0x1d
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3EE_BMSK                                 0x10000000
#define HWIO_FEATURE_CONFIG5_SYS_APCSCPUCFGRSTSCTLREL3EE_SHFT                                       0x1c
#define HWIO_FEATURE_CONFIG5_APPS1_CFGCPUPRESENT_N_BMSK                                        0xc000000
#define HWIO_FEATURE_CONFIG5_APPS1_CFGCPUPRESENT_N_SHFT                                             0x1a
#define HWIO_FEATURE_CONFIG5_APPS0_CFGCPUPRESENT_N_BMSK                                        0x3000000
#define HWIO_FEATURE_CONFIG5_APPS0_CFGCPUPRESENT_N_SHFT                                             0x18
#define HWIO_FEATURE_CONFIG5_RSVD0_BMSK                                                         0xf80000
#define HWIO_FEATURE_CONFIG5_RSVD0_SHFT                                                             0x13
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_SPIDEN_DISABLE_BMSK                                       0x40000
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_SPIDEN_DISABLE_SHFT                                          0x12
#define HWIO_FEATURE_CONFIG5_QC_SSC_SPIDEN_DISABLE_BMSK                                          0x20000
#define HWIO_FEATURE_CONFIG5_QC_SSC_SPIDEN_DISABLE_SHFT                                             0x11
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_SPIDEN_DISABLE_BMSK                                        0x10000
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_SPIDEN_DISABLE_SHFT                                           0x10
#define HWIO_FEATURE_CONFIG5_QC_DAP_SPIDEN_DISABLE_BMSK                                           0x8000
#define HWIO_FEATURE_CONFIG5_QC_DAP_SPIDEN_DISABLE_SHFT                                              0xf
#define HWIO_FEATURE_CONFIG5_QC_APPS1_SPIDEN_DISABLE_BMSK                                         0x4000
#define HWIO_FEATURE_CONFIG5_QC_APPS1_SPIDEN_DISABLE_SHFT                                            0xe
#define HWIO_FEATURE_CONFIG5_QC_APPS0_SPIDEN_DISABLE_BMSK                                         0x2000
#define HWIO_FEATURE_CONFIG5_QC_APPS0_SPIDEN_DISABLE_SHFT                                            0xd
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_SPNIDEN_DISABLE_BMSK                                       0x1000
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_SPNIDEN_DISABLE_SHFT                                          0xc
#define HWIO_FEATURE_CONFIG5_QC_SSC_SPNIDEN_DISABLE_BMSK                                           0x800
#define HWIO_FEATURE_CONFIG5_QC_SSC_SPNIDEN_DISABLE_SHFT                                             0xb
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_SPNIDEN_DISABLE_BMSK                                         0x400
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_SPNIDEN_DISABLE_SHFT                                           0xa
#define HWIO_FEATURE_CONFIG5_QC_DAP_SPNIDEN_DISABLE_BMSK                                           0x200
#define HWIO_FEATURE_CONFIG5_QC_DAP_SPNIDEN_DISABLE_SHFT                                             0x9
#define HWIO_FEATURE_CONFIG5_QC_APPS1_SPNIDEN_DISABLE_BMSK                                         0x100
#define HWIO_FEATURE_CONFIG5_QC_APPS1_SPNIDEN_DISABLE_SHFT                                           0x8
#define HWIO_FEATURE_CONFIG5_QC_APPS0_SPNIDEN_DISABLE_BMSK                                          0x80
#define HWIO_FEATURE_CONFIG5_QC_APPS0_SPNIDEN_DISABLE_SHFT                                           0x7
#define HWIO_FEATURE_CONFIG5_QC_MSS_NIDEN_DISABLE_BMSK                                              0x40
#define HWIO_FEATURE_CONFIG5_QC_MSS_NIDEN_DISABLE_SHFT                                               0x6
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_NIDEN_DISABLE_BMSK                                           0x20
#define HWIO_FEATURE_CONFIG5_QC_GSS_A5_NIDEN_DISABLE_SHFT                                            0x5
#define HWIO_FEATURE_CONFIG5_QC_SSC_NIDEN_DISABLE_BMSK                                              0x10
#define HWIO_FEATURE_CONFIG5_QC_SSC_NIDEN_DISABLE_SHFT                                               0x4
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_NIDEN_DISABLE_BMSK                                             0x8
#define HWIO_FEATURE_CONFIG5_QC_PIMEM_NIDEN_DISABLE_SHFT                                             0x3
#define HWIO_FEATURE_CONFIG5_QC_RPM_NIDEN_DISABLE_BMSK                                               0x4
#define HWIO_FEATURE_CONFIG5_QC_RPM_NIDEN_DISABLE_SHFT                                               0x2
#define HWIO_FEATURE_CONFIG5_QC_WCSS_NIDEN_DISABLE_BMSK                                              0x2
#define HWIO_FEATURE_CONFIG5_QC_WCSS_NIDEN_DISABLE_SHFT                                              0x1
#define HWIO_FEATURE_CONFIG5_QC_LPASS_NIDEN_DISABLE_BMSK                                             0x1
#define HWIO_FEATURE_CONFIG5_QC_LPASS_NIDEN_DISABLE_SHFT                                             0x0

#define HWIO_FEATURE_CONFIG6_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000601c)
#define HWIO_FEATURE_CONFIG6_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG6_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG6_ADDR, HWIO_FEATURE_CONFIG6_RMSK)
#define HWIO_FEATURE_CONFIG6_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG6_ADDR, m)
#define HWIO_FEATURE_CONFIG6_TAP_GEN_SPARE_INSTR_DISABLE_13_0_BMSK                            0xfffc0000
#define HWIO_FEATURE_CONFIG6_TAP_GEN_SPARE_INSTR_DISABLE_13_0_SHFT                                  0x12
#define HWIO_FEATURE_CONFIG6_TAP_INSTR_DISABLE_BMSK                                              0x3ffff
#define HWIO_FEATURE_CONFIG6_TAP_INSTR_DISABLE_SHFT                                                  0x0

#define HWIO_FEATURE_CONFIG7_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006020)
#define HWIO_FEATURE_CONFIG7_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG7_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG7_ADDR, HWIO_FEATURE_CONFIG7_RMSK)
#define HWIO_FEATURE_CONFIG7_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG7_ADDR, m)
#define HWIO_FEATURE_CONFIG7_SEC_TAP_ACCESS_DISABLE_BMSK                                      0xfffc0000
#define HWIO_FEATURE_CONFIG7_SEC_TAP_ACCESS_DISABLE_SHFT                                            0x12
#define HWIO_FEATURE_CONFIG7_TAP_GEN_SPARE_INSTR_DISABLE_31_14_BMSK                              0x3ffff
#define HWIO_FEATURE_CONFIG7_TAP_GEN_SPARE_INSTR_DISABLE_31_14_SHFT                                  0x0

#define HWIO_FEATURE_CONFIG8_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006024)
#define HWIO_FEATURE_CONFIG8_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG8_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG8_ADDR, HWIO_FEATURE_CONFIG8_RMSK)
#define HWIO_FEATURE_CONFIG8_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG8_ADDR, m)
#define HWIO_FEATURE_CONFIG8_MODEM_PBL_PATCH_VERSION_BMSK                                     0xf8000000
#define HWIO_FEATURE_CONFIG8_MODEM_PBL_PATCH_VERSION_SHFT                                           0x1b
#define HWIO_FEATURE_CONFIG8_APPS_PBL_PATCH_VERSION_BMSK                                       0x7c00000
#define HWIO_FEATURE_CONFIG8_APPS_PBL_PATCH_VERSION_SHFT                                            0x16
#define HWIO_FEATURE_CONFIG8_RSVD0_BMSK                                                         0x3c0000
#define HWIO_FEATURE_CONFIG8_RSVD0_SHFT                                                             0x12
#define HWIO_FEATURE_CONFIG8_APPS_PBL_BOOT_SPEED_BMSK                                            0x30000
#define HWIO_FEATURE_CONFIG8_APPS_PBL_BOOT_SPEED_SHFT                                               0x10
#define HWIO_FEATURE_CONFIG8_RPM_PBL_BOOT_SPEED_BMSK                                              0xc000
#define HWIO_FEATURE_CONFIG8_RPM_PBL_BOOT_SPEED_SHFT                                                 0xe
#define HWIO_FEATURE_CONFIG8_APPS_BOOT_FROM_ROM_BMSK                                              0x2000
#define HWIO_FEATURE_CONFIG8_APPS_BOOT_FROM_ROM_SHFT                                                 0xd
#define HWIO_FEATURE_CONFIG8_RPM_BOOT_FROM_ROM_BMSK                                               0x1000
#define HWIO_FEATURE_CONFIG8_RPM_BOOT_FROM_ROM_SHFT                                                  0xc
#define HWIO_FEATURE_CONFIG8_MODEM_BOOT_FROM_ROM_BMSK                                              0x800
#define HWIO_FEATURE_CONFIG8_MODEM_BOOT_FROM_ROM_SHFT                                                0xb
#define HWIO_FEATURE_CONFIG8_MSA_ENA_BMSK                                                          0x400
#define HWIO_FEATURE_CONFIG8_MSA_ENA_SHFT                                                            0xa
#define HWIO_FEATURE_CONFIG8_FORCE_MSA_AUTH_EN_BMSK                                                0x200
#define HWIO_FEATURE_CONFIG8_FORCE_MSA_AUTH_EN_SHFT                                                  0x9
#define HWIO_FEATURE_CONFIG8_ARM_CE_DISABLE_USAGE_BMSK                                             0x100
#define HWIO_FEATURE_CONFIG8_ARM_CE_DISABLE_USAGE_SHFT                                               0x8
#define HWIO_FEATURE_CONFIG8_BOOT_ROM_CFG_BMSK                                                      0xff
#define HWIO_FEATURE_CONFIG8_BOOT_ROM_CFG_SHFT                                                       0x0

#define HWIO_FEATURE_CONFIG9_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006028)
#define HWIO_FEATURE_CONFIG9_RMSK                                                             0xffffffff
#define HWIO_FEATURE_CONFIG9_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG9_ADDR, HWIO_FEATURE_CONFIG9_RMSK)
#define HWIO_FEATURE_CONFIG9_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG9_ADDR, m)
#define HWIO_FEATURE_CONFIG9_RSVD0_BMSK                                                       0xffffc000
#define HWIO_FEATURE_CONFIG9_RSVD0_SHFT                                                              0xe
#define HWIO_FEATURE_CONFIG9_FOUNDRY_ID_BMSK                                                      0x3c00
#define HWIO_FEATURE_CONFIG9_FOUNDRY_ID_SHFT                                                         0xa
#define HWIO_FEATURE_CONFIG9_PLL_CFG_BMSK                                                          0x3f0
#define HWIO_FEATURE_CONFIG9_PLL_CFG_SHFT                                                            0x4
#define HWIO_FEATURE_CONFIG9_APPS_PBL_PLL_CTRL_BMSK                                                  0xf
#define HWIO_FEATURE_CONFIG9_APPS_PBL_PLL_CTRL_SHFT                                                  0x0

#define HWIO_OEM_CONFIG0_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000602c)
#define HWIO_OEM_CONFIG0_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG0_IN          \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, HWIO_OEM_CONFIG0_RMSK)
#define HWIO_OEM_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, m)
#define HWIO_OEM_CONFIG0_SPARE1_DISABLE_BMSK                                                  0x80000000
#define HWIO_OEM_CONFIG0_SPARE1_DISABLE_SHFT                                                        0x1f
#define HWIO_OEM_CONFIG0_SPARE0_DISABLE_BMSK                                                  0x40000000
#define HWIO_OEM_CONFIG0_SPARE0_DISABLE_SHFT                                                        0x1e
#define HWIO_OEM_CONFIG0_ALL_DEBUG_DISABLE_BMSK                                               0x20000000
#define HWIO_OEM_CONFIG0_ALL_DEBUG_DISABLE_SHFT                                                     0x1d
#define HWIO_OEM_CONFIG0_DEBUG_POLICY_DISABLE_BMSK                                            0x10000000
#define HWIO_OEM_CONFIG0_DEBUG_POLICY_DISABLE_SHFT                                                  0x1c
#define HWIO_OEM_CONFIG0_RSVD0_BMSK                                                            0xe000000
#define HWIO_OEM_CONFIG0_RSVD0_SHFT                                                                 0x19
#define HWIO_OEM_CONFIG0_MSS_HASH_INTEGRITY_CHECK_DISABLE_BMSK                                 0x1000000
#define HWIO_OEM_CONFIG0_MSS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                                      0x18
#define HWIO_OEM_CONFIG0_APPS_HASH_INTEGRITY_CHECK_DISABLE_BMSK                                 0x800000
#define HWIO_OEM_CONFIG0_APPS_HASH_INTEGRITY_CHECK_DISABLE_SHFT                                     0x17
#define HWIO_OEM_CONFIG0_USB_SS_DISABLE_BMSK                                                    0x400000
#define HWIO_OEM_CONFIG0_USB_SS_DISABLE_SHFT                                                        0x16
#define HWIO_OEM_CONFIG0_SW_ROT_USE_SERIAL_NUM_BMSK                                             0x200000
#define HWIO_OEM_CONFIG0_SW_ROT_USE_SERIAL_NUM_SHFT                                                 0x15
#define HWIO_OEM_CONFIG0_DISABLE_ROT_TRANSFER_BMSK                                              0x100000
#define HWIO_OEM_CONFIG0_DISABLE_ROT_TRANSFER_SHFT                                                  0x14
#define HWIO_OEM_CONFIG0_IMAGE_ENCRYPTION_ENABLE_BMSK                                            0x80000
#define HWIO_OEM_CONFIG0_IMAGE_ENCRYPTION_ENABLE_SHFT                                               0x13
#define HWIO_OEM_CONFIG0_SDC1_SCM_FORCE_EFUSE_KEY_BMSK                                           0x40000
#define HWIO_OEM_CONFIG0_SDC1_SCM_FORCE_EFUSE_KEY_SHFT                                              0x12
#define HWIO_OEM_CONFIG0_UFS_SCM_FORCE_EFUSE_KEY_BMSK                                            0x20000
#define HWIO_OEM_CONFIG0_UFS_SCM_FORCE_EFUSE_KEY_SHFT                                               0x11
#define HWIO_OEM_CONFIG0_SDC2_SCM_FORCE_EFUSE_KEY_BMSK                                           0x10000
#define HWIO_OEM_CONFIG0_SDC2_SCM_FORCE_EFUSE_KEY_SHFT                                              0x10
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_BMSK                                                     0x8000
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_SHFT                                                        0xf
#define HWIO_OEM_CONFIG0_WDOG_EN_BMSK                                                             0x4000
#define HWIO_OEM_CONFIG0_WDOG_EN_SHFT                                                                0xe
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_BMSK                                                    0x2000
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_SHFT                                                       0xd
#define HWIO_OEM_CONFIG0_SW_FUSE_PROG_DISABLE_BMSK                                                0x1000
#define HWIO_OEM_CONFIG0_SW_FUSE_PROG_DISABLE_SHFT                                                   0xc
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                                        0x800
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                                          0xb
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_BMSK                                                  0x400
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_SHFT                                                    0xa
#define HWIO_OEM_CONFIG0_FAST_BOOT_BMSK                                                            0x3e0
#define HWIO_OEM_CONFIG0_FAST_BOOT_SHFT                                                              0x5
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_BMSK                                                   0x10
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_SHFT                                                    0x4
#define HWIO_OEM_CONFIG0_FORCE_USB_BOOT_DISABLE_BMSK                                                 0x8
#define HWIO_OEM_CONFIG0_FORCE_USB_BOOT_DISABLE_SHFT                                                 0x3
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_BMSK                                                    0x4
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_SHFT                                                    0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_BMSK                                                           0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_SHFT                                                           0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_BMSK                                                        0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_SHFT                                                        0x0

#define HWIO_OEM_CONFIG1_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006030)
#define HWIO_OEM_CONFIG1_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG1_IN          \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, HWIO_OEM_CONFIG1_RMSK)
#define HWIO_OEM_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, m)
#define HWIO_OEM_CONFIG1_APPS0_SPNIDEN_DISABLE_BMSK                                           0x80000000
#define HWIO_OEM_CONFIG1_APPS0_SPNIDEN_DISABLE_SHFT                                                 0x1f
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_BMSK                                               0x40000000
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_SHFT                                                     0x1e
#define HWIO_OEM_CONFIG1_GSS_A5_NIDEN_DISABLE_BMSK                                            0x20000000
#define HWIO_OEM_CONFIG1_GSS_A5_NIDEN_DISABLE_SHFT                                                  0x1d
#define HWIO_OEM_CONFIG1_SSC_NIDEN_DISABLE_BMSK                                               0x10000000
#define HWIO_OEM_CONFIG1_SSC_NIDEN_DISABLE_SHFT                                                     0x1c
#define HWIO_OEM_CONFIG1_PIMEM_NIDEN_DISABLE_BMSK                                              0x8000000
#define HWIO_OEM_CONFIG1_PIMEM_NIDEN_DISABLE_SHFT                                                   0x1b
#define HWIO_OEM_CONFIG1_RPM_NIDEN_DISABLE_BMSK                                                0x4000000
#define HWIO_OEM_CONFIG1_RPM_NIDEN_DISABLE_SHFT                                                     0x1a
#define HWIO_OEM_CONFIG1_WCSS_NIDEN_DISABLE_BMSK                                               0x2000000
#define HWIO_OEM_CONFIG1_WCSS_NIDEN_DISABLE_SHFT                                                    0x19
#define HWIO_OEM_CONFIG1_LPASS_NIDEN_DISABLE_BMSK                                              0x1000000
#define HWIO_OEM_CONFIG1_LPASS_NIDEN_DISABLE_SHFT                                                   0x18
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_BMSK                                                 0x800000
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_SHFT                                                     0x17
#define HWIO_OEM_CONFIG1_APPS1_NIDEN_DISABLE_BMSK                                               0x400000
#define HWIO_OEM_CONFIG1_APPS1_NIDEN_DISABLE_SHFT                                                   0x16
#define HWIO_OEM_CONFIG1_APPS0_NIDEN_DISABLE_BMSK                                               0x200000
#define HWIO_OEM_CONFIG1_APPS0_NIDEN_DISABLE_SHFT                                                   0x15
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_BMSK                                                 0x100000
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_SHFT                                                     0x14
#define HWIO_OEM_CONFIG1_GSS_A5_DBGEN_DISABLE_BMSK                                               0x80000
#define HWIO_OEM_CONFIG1_GSS_A5_DBGEN_DISABLE_SHFT                                                  0x13
#define HWIO_OEM_CONFIG1_A5X_ISDB_DBGEN_DISABLE_BMSK                                             0x40000
#define HWIO_OEM_CONFIG1_A5X_ISDB_DBGEN_DISABLE_SHFT                                                0x12
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_BMSK                                              0x20000
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_SHFT                                                 0x11
#define HWIO_OEM_CONFIG1_SSC_Q6_DBGEN_DISABLE_BMSK                                               0x10000
#define HWIO_OEM_CONFIG1_SSC_Q6_DBGEN_DISABLE_SHFT                                                  0x10
#define HWIO_OEM_CONFIG1_SSC_DBGEN_DISABLE_BMSK                                                   0x8000
#define HWIO_OEM_CONFIG1_SSC_DBGEN_DISABLE_SHFT                                                      0xf
#define HWIO_OEM_CONFIG1_PIMEM_DBGEN_DISABLE_BMSK                                                 0x4000
#define HWIO_OEM_CONFIG1_PIMEM_DBGEN_DISABLE_SHFT                                                    0xe
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_BMSK                                                   0x2000
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_SHFT                                                      0xd
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_BMSK                                                  0x1000
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_SHFT                                                     0xc
#define HWIO_OEM_CONFIG1_LPASS_DBGEN_DISABLE_BMSK                                                  0x800
#define HWIO_OEM_CONFIG1_LPASS_DBGEN_DISABLE_SHFT                                                    0xb
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_BMSK                                                    0x400
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_SHFT                                                      0xa
#define HWIO_OEM_CONFIG1_APPS1_DBGEN_DISABLE_BMSK                                                  0x200
#define HWIO_OEM_CONFIG1_APPS1_DBGEN_DISABLE_SHFT                                                    0x9
#define HWIO_OEM_CONFIG1_APPS0_DBGEN_DISABLE_BMSK                                                  0x100
#define HWIO_OEM_CONFIG1_APPS0_DBGEN_DISABLE_SHFT                                                    0x8
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_BMSK                                                  0x80
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_SHFT                                                   0x7
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_BMSK                                                     0x40
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_SHFT                                                      0x6
#define HWIO_OEM_CONFIG1_SSC_Q6_ETM_DISABLE_BMSK                                                    0x20
#define HWIO_OEM_CONFIG1_SSC_Q6_ETM_DISABLE_SHFT                                                     0x5
#define HWIO_OEM_CONFIG1_SPARE6_DISABLE_BMSK                                                        0x10
#define HWIO_OEM_CONFIG1_SPARE6_DISABLE_SHFT                                                         0x4
#define HWIO_OEM_CONFIG1_SPARE5_DISABLE_BMSK                                                         0x8
#define HWIO_OEM_CONFIG1_SPARE5_DISABLE_SHFT                                                         0x3
#define HWIO_OEM_CONFIG1_SPARE4_DISABLE_BMSK                                                         0x4
#define HWIO_OEM_CONFIG1_SPARE4_DISABLE_SHFT                                                         0x2
#define HWIO_OEM_CONFIG1_SPARE3_DISABLE_BMSK                                                         0x2
#define HWIO_OEM_CONFIG1_SPARE3_DISABLE_SHFT                                                         0x1
#define HWIO_OEM_CONFIG1_SPARE2_DISABLE_BMSK                                                         0x1
#define HWIO_OEM_CONFIG1_SPARE2_DISABLE_SHFT                                                         0x0

#define HWIO_OEM_CONFIG2_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006034)
#define HWIO_OEM_CONFIG2_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG2_IN          \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, HWIO_OEM_CONFIG2_RMSK)
#define HWIO_OEM_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, m)
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG46_SECURE_BMSK                                          0x80000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG46_SECURE_SHFT                                                0x1f
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG45_SECURE_BMSK                                          0x40000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG45_SECURE_SHFT                                                0x1e
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG44_SECURE_BMSK                                          0x20000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG44_SECURE_SHFT                                                0x1d
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG43_SECURE_BMSK                                          0x10000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG43_SECURE_SHFT                                                0x1c
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG42_SECURE_BMSK                                           0x8000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG42_SECURE_SHFT                                                0x1b
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG41_SECURE_BMSK                                           0x4000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG41_SECURE_SHFT                                                0x1a
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG40_SECURE_BMSK                                           0x2000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG40_SECURE_SHFT                                                0x19
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG39_SECURE_BMSK                                           0x1000000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG39_SECURE_SHFT                                                0x18
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG38_SECURE_BMSK                                            0x800000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG38_SECURE_SHFT                                                0x17
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG37_SECURE_BMSK                                            0x400000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG37_SECURE_SHFT                                                0x16
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG36_SECURE_BMSK                                            0x200000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG36_SECURE_SHFT                                                0x15
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG35_SECURE_BMSK                                            0x100000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG35_SECURE_SHFT                                                0x14
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG34_SECURE_BMSK                                             0x80000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG34_SECURE_SHFT                                                0x13
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG33_SECURE_BMSK                                             0x40000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG33_SECURE_SHFT                                                0x12
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG32_SECURE_BMSK                                             0x20000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG32_SECURE_SHFT                                                0x11
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG31_SECURE_BMSK                                             0x10000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG31_SECURE_SHFT                                                0x10
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG30_SECURE_BMSK                                              0x8000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG30_SECURE_SHFT                                                 0xf
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG29_SECURE_BMSK                                              0x4000
#define HWIO_OEM_CONFIG2_OEM_SPARE_REG29_SECURE_SHFT                                                 0xe
#define HWIO_OEM_CONFIG2_RSVD0_BMSK                                                               0x3800
#define HWIO_OEM_CONFIG2_RSVD0_SHFT                                                                  0xb
#define HWIO_OEM_CONFIG2_GSS_A5_SPIDEN_DISABLE_BMSK                                                0x400
#define HWIO_OEM_CONFIG2_GSS_A5_SPIDEN_DISABLE_SHFT                                                  0xa
#define HWIO_OEM_CONFIG2_SSC_SPIDEN_DISABLE_BMSK                                                   0x200
#define HWIO_OEM_CONFIG2_SSC_SPIDEN_DISABLE_SHFT                                                     0x9
#define HWIO_OEM_CONFIG2_PIMEM_SPIDEN_DISABLE_BMSK                                                 0x100
#define HWIO_OEM_CONFIG2_PIMEM_SPIDEN_DISABLE_SHFT                                                   0x8
#define HWIO_OEM_CONFIG2_DAP_SPIDEN_DISABLE_BMSK                                                    0x80
#define HWIO_OEM_CONFIG2_DAP_SPIDEN_DISABLE_SHFT                                                     0x7
#define HWIO_OEM_CONFIG2_APPS1_SPIDEN_DISABLE_BMSK                                                  0x40
#define HWIO_OEM_CONFIG2_APPS1_SPIDEN_DISABLE_SHFT                                                   0x6
#define HWIO_OEM_CONFIG2_APPS0_SPIDEN_DISABLE_BMSK                                                  0x20
#define HWIO_OEM_CONFIG2_APPS0_SPIDEN_DISABLE_SHFT                                                   0x5
#define HWIO_OEM_CONFIG2_GSS_A5_SPNIDEN_DISABLE_BMSK                                                0x10
#define HWIO_OEM_CONFIG2_GSS_A5_SPNIDEN_DISABLE_SHFT                                                 0x4
#define HWIO_OEM_CONFIG2_SSC_SPNIDEN_DISABLE_BMSK                                                    0x8
#define HWIO_OEM_CONFIG2_SSC_SPNIDEN_DISABLE_SHFT                                                    0x3
#define HWIO_OEM_CONFIG2_PIMEM_SPNIDEN_DISABLE_BMSK                                                  0x4
#define HWIO_OEM_CONFIG2_PIMEM_SPNIDEN_DISABLE_SHFT                                                  0x2
#define HWIO_OEM_CONFIG2_DAP_SPNIDEN_DISABLE_BMSK                                                    0x2
#define HWIO_OEM_CONFIG2_DAP_SPNIDEN_DISABLE_SHFT                                                    0x1
#define HWIO_OEM_CONFIG2_APPS1_SPNIDEN_DISABLE_BMSK                                                  0x1
#define HWIO_OEM_CONFIG2_APPS1_SPNIDEN_DISABLE_SHFT                                                  0x0

#define HWIO_OEM_CONFIG3_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006038)
#define HWIO_OEM_CONFIG3_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG3_IN          \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, HWIO_OEM_CONFIG3_RMSK)
#define HWIO_OEM_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, m)
#define HWIO_OEM_CONFIG3_OEM_PRODUCT_ID_BMSK                                                  0xffff0000
#define HWIO_OEM_CONFIG3_OEM_PRODUCT_ID_SHFT                                                        0x10
#define HWIO_OEM_CONFIG3_OEM_HW_ID_BMSK                                                           0xffff
#define HWIO_OEM_CONFIG3_OEM_HW_ID_SHFT                                                              0x0

#define HWIO_OEM_CONFIG4_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000603c)
#define HWIO_OEM_CONFIG4_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG4_IN          \
        in_dword_masked(HWIO_OEM_CONFIG4_ADDR, HWIO_OEM_CONFIG4_RMSK)
#define HWIO_OEM_CONFIG4_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG4_ADDR, m)
#define HWIO_OEM_CONFIG4_PERIPH_VID_BMSK                                                      0xffff0000
#define HWIO_OEM_CONFIG4_PERIPH_VID_SHFT                                                            0x10
#define HWIO_OEM_CONFIG4_PERIPH_PID_BMSK                                                          0xffff
#define HWIO_OEM_CONFIG4_PERIPH_PID_SHFT                                                             0x0

#define HWIO_OEM_CONFIG5_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006040)
#define HWIO_OEM_CONFIG5_RMSK                                                                 0xffffffff
#define HWIO_OEM_CONFIG5_IN          \
        in_dword_masked(HWIO_OEM_CONFIG5_ADDR, HWIO_OEM_CONFIG5_RMSK)
#define HWIO_OEM_CONFIG5_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG5_ADDR, m)
#define HWIO_OEM_CONFIG5_RSVD0_BMSK                                                           0xffffff00
#define HWIO_OEM_CONFIG5_RSVD0_SHFT                                                                  0x8
#define HWIO_OEM_CONFIG5_ANTI_ROLLBACK_FEATURE_EN_BMSK                                              0xff
#define HWIO_OEM_CONFIG5_ANTI_ROLLBACK_FEATURE_EN_SHFT                                               0x0

#define HWIO_BOOT_CONFIG_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006044)
#define HWIO_BOOT_CONFIG_RMSK                                                                      0x7ff
#define HWIO_BOOT_CONFIG_IN          \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, HWIO_BOOT_CONFIG_RMSK)
#define HWIO_BOOT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, m)
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_BMSK                                                    0x400
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_SHFT                                                      0xa
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_BMSK                                                  0x300
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_SHFT                                                    0x8
#define HWIO_BOOT_CONFIG_RPM_BOOT_FROM_ROM_BMSK                                                     0x80
#define HWIO_BOOT_CONFIG_RPM_BOOT_FROM_ROM_SHFT                                                      0x7
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_BMSK                                                    0x40
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_SHFT                                                     0x6
#define HWIO_BOOT_CONFIG_FAST_BOOT_BMSK                                                             0x3e
#define HWIO_BOOT_CONFIG_FAST_BOOT_SHFT                                                              0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_BMSK                                                                0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_SHFT                                                                0x0

#define HWIO_SECURE_BOOTn_ADDR(n)                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006048 + 0x4 * (n))
#define HWIO_SECURE_BOOTn_RMSK                                                                     0x1ff
#define HWIO_SECURE_BOOTn_MAXn                                                                        14
#define HWIO_SECURE_BOOTn_INI(n)        \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), HWIO_SECURE_BOOTn_RMSK)
#define HWIO_SECURE_BOOTn_INMI(n,mask)    \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), mask)
#define HWIO_SECURE_BOOTn_FUSE_SRC_BMSK                                                            0x100
#define HWIO_SECURE_BOOTn_FUSE_SRC_SHFT                                                              0x8
#define HWIO_SECURE_BOOTn_RSVD_7_BMSK                                                               0x80
#define HWIO_SECURE_BOOTn_RSVD_7_SHFT                                                                0x7
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_BMSK                                                       0x40
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_SHFT                                                        0x6
#define HWIO_SECURE_BOOTn_AUTH_EN_BMSK                                                              0x20
#define HWIO_SECURE_BOOTn_AUTH_EN_SHFT                                                               0x5
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_BMSK                                                      0x10
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_SHFT                                                       0x4
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_BMSK                                                     0xf
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_SHFT                                                     0x0

#define HWIO_OVERRIDE_0_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c0)
#define HWIO_OVERRIDE_0_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_0_IN          \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, HWIO_OVERRIDE_0_RMSK)
#define HWIO_OVERRIDE_0_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, m)
#define HWIO_OVERRIDE_0_OUT(v)      \
        out_dword(HWIO_OVERRIDE_0_ADDR,v)
#define HWIO_OVERRIDE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_0_ADDR,m,v,HWIO_OVERRIDE_0_IN)
#define HWIO_OVERRIDE_0_RSVD_31_3_BMSK                                                        0xfffffff8
#define HWIO_OVERRIDE_0_RSVD_31_3_SHFT                                                               0x3
#define HWIO_OVERRIDE_0_TX_DISABLE_BMSK                                                              0x4
#define HWIO_OVERRIDE_0_TX_DISABLE_SHFT                                                              0x2
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_BMSK                                                     0x2
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_SHFT                                                     0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_BMSK                                               0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_SHFT                                               0x0

#define HWIO_OVERRIDE_1_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c4)
#define HWIO_OVERRIDE_1_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_1_IN          \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, HWIO_OVERRIDE_1_RMSK)
#define HWIO_OVERRIDE_1_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, m)
#define HWIO_OVERRIDE_1_OUT(v)      \
        out_dword(HWIO_OVERRIDE_1_ADDR,v)
#define HWIO_OVERRIDE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_1_ADDR,m,v,HWIO_OVERRIDE_1_IN)
#define HWIO_OVERRIDE_1_RSVD_31_10_BMSK                                                       0xfffffc00
#define HWIO_OVERRIDE_1_RSVD_31_10_SHFT                                                              0xa
#define HWIO_OVERRIDE_1_OVRID_DAP_DEVICEEN_DISABLE_BMSK                                            0x200
#define HWIO_OVERRIDE_1_OVRID_DAP_DEVICEEN_DISABLE_SHFT                                              0x9
#define HWIO_OVERRIDE_1_OVRID_RPM_DAPEN_DISABLE_BMSK                                               0x100
#define HWIO_OVERRIDE_1_OVRID_RPM_DAPEN_DISABLE_SHFT                                                 0x8
#define HWIO_OVERRIDE_1_OVRID_SSC_Q6_ETM_DISABLE_BMSK                                               0x80
#define HWIO_OVERRIDE_1_OVRID_SSC_Q6_ETM_DISABLE_SHFT                                                0x7
#define HWIO_OVERRIDE_1_OVRID_SPARE6_DISABLE_BMSK                                                   0x40
#define HWIO_OVERRIDE_1_OVRID_SPARE6_DISABLE_SHFT                                                    0x6
#define HWIO_OVERRIDE_1_OVRID_SPARE5_DISABLE_BMSK                                                   0x20
#define HWIO_OVERRIDE_1_OVRID_SPARE5_DISABLE_SHFT                                                    0x5
#define HWIO_OVERRIDE_1_OVRID_SPARE4_DISABLE_BMSK                                                   0x10
#define HWIO_OVERRIDE_1_OVRID_SPARE4_DISABLE_SHFT                                                    0x4
#define HWIO_OVERRIDE_1_OVRID_SPARE3_DISABLE_BMSK                                                    0x8
#define HWIO_OVERRIDE_1_OVRID_SPARE3_DISABLE_SHFT                                                    0x3
#define HWIO_OVERRIDE_1_OVRID_SPARE2_DISABLE_BMSK                                                    0x4
#define HWIO_OVERRIDE_1_OVRID_SPARE2_DISABLE_SHFT                                                    0x2
#define HWIO_OVERRIDE_1_OVRID_SPARE1_DISABLE_BMSK                                                    0x2
#define HWIO_OVERRIDE_1_OVRID_SPARE1_DISABLE_SHFT                                                    0x1
#define HWIO_OVERRIDE_1_OVRID_SPARE0_DISABLE_BMSK                                                    0x1
#define HWIO_OVERRIDE_1_OVRID_SPARE0_DISABLE_SHFT                                                    0x0

#define HWIO_OVERRIDE_2_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060c8)
#define HWIO_OVERRIDE_2_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_2_IN          \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, HWIO_OVERRIDE_2_RMSK)
#define HWIO_OVERRIDE_2_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, m)
#define HWIO_OVERRIDE_2_OUT(v)      \
        out_dword(HWIO_OVERRIDE_2_ADDR,v)
#define HWIO_OVERRIDE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_2_ADDR,m,v,HWIO_OVERRIDE_2_IN)
#define HWIO_OVERRIDE_2_RSVD_31_19_BMSK                                                       0xfff80000
#define HWIO_OVERRIDE_2_RSVD_31_19_SHFT                                                             0x13
#define HWIO_OVERRIDE_2_OVRID_SSC_NIDEN_DISABLE_BMSK                                             0x40000
#define HWIO_OVERRIDE_2_OVRID_SSC_NIDEN_DISABLE_SHFT                                                0x12
#define HWIO_OVERRIDE_2_OVRID_PIMEM_NIDEN_DISABLE_BMSK                                           0x20000
#define HWIO_OVERRIDE_2_OVRID_PIMEM_NIDEN_DISABLE_SHFT                                              0x11
#define HWIO_OVERRIDE_2_OVRID_RPM_NIDEN_DISABLE_BMSK                                             0x10000
#define HWIO_OVERRIDE_2_OVRID_RPM_NIDEN_DISABLE_SHFT                                                0x10
#define HWIO_OVERRIDE_2_OVRID_WCSS_NIDEN_DISABLE_BMSK                                             0x8000
#define HWIO_OVERRIDE_2_OVRID_WCSS_NIDEN_DISABLE_SHFT                                                0xf
#define HWIO_OVERRIDE_2_OVRID_LPASS_NIDEN_DISABLE_BMSK                                            0x4000
#define HWIO_OVERRIDE_2_OVRID_LPASS_NIDEN_DISABLE_SHFT                                               0xe
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_BMSK                                              0x2000
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_SHFT                                                 0xd
#define HWIO_OVERRIDE_2_OVRID_APPS1_NIDEN_DISABLE_BMSK                                            0x1000
#define HWIO_OVERRIDE_2_OVRID_APPS1_NIDEN_DISABLE_SHFT                                               0xc
#define HWIO_OVERRIDE_2_OVRID_APPS0_NIDEN_DISABLE_BMSK                                             0x800
#define HWIO_OVERRIDE_2_OVRID_APPS0_NIDEN_DISABLE_SHFT                                               0xb
#define HWIO_OVERRIDE_2_OVRID_A5X_ISDB_DBGEN_DISABLE_BMSK                                          0x400
#define HWIO_OVERRIDE_2_OVRID_A5X_ISDB_DBGEN_DISABLE_SHFT                                            0xa
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_BMSK                                           0x200
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_SHFT                                             0x9
#define HWIO_OVERRIDE_2_OVRID_SSC_Q6_DBGEN_DISABLE_BMSK                                            0x100
#define HWIO_OVERRIDE_2_OVRID_SSC_Q6_DBGEN_DISABLE_SHFT                                              0x8
#define HWIO_OVERRIDE_2_OVRID_SSC_DBGEN_DISABLE_BMSK                                                0x80
#define HWIO_OVERRIDE_2_OVRID_SSC_DBGEN_DISABLE_SHFT                                                 0x7
#define HWIO_OVERRIDE_2_OVRID_PIMEM_DBGEN_DISABLE_BMSK                                              0x40
#define HWIO_OVERRIDE_2_OVRID_PIMEM_DBGEN_DISABLE_SHFT                                               0x6
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_BMSK                                                0x20
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_SHFT                                                 0x5
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_BMSK                                               0x10
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_SHFT                                                0x4
#define HWIO_OVERRIDE_2_OVRID_LPASS_DBGEN_DISABLE_BMSK                                               0x8
#define HWIO_OVERRIDE_2_OVRID_LPASS_DBGEN_DISABLE_SHFT                                               0x3
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_BMSK                                                 0x4
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_SHFT                                                 0x2
#define HWIO_OVERRIDE_2_OVRID_APPS1_DBGEN_DISABLE_BMSK                                               0x2
#define HWIO_OVERRIDE_2_OVRID_APPS1_DBGEN_DISABLE_SHFT                                               0x1
#define HWIO_OVERRIDE_2_OVRID_APPS0_DBGEN_DISABLE_BMSK                                               0x1
#define HWIO_OVERRIDE_2_OVRID_APPS0_DBGEN_DISABLE_SHFT                                               0x0

#define HWIO_OVERRIDE_3_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060cc)
#define HWIO_OVERRIDE_3_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_3_IN          \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, HWIO_OVERRIDE_3_RMSK)
#define HWIO_OVERRIDE_3_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, m)
#define HWIO_OVERRIDE_3_OUT(v)      \
        out_dword(HWIO_OVERRIDE_3_ADDR,v)
#define HWIO_OVERRIDE_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_3_ADDR,m,v,HWIO_OVERRIDE_3_IN)
#define HWIO_OVERRIDE_3_RSVD_31_11_BMSK                                                       0xfffff800
#define HWIO_OVERRIDE_3_RSVD_31_11_SHFT                                                              0xb
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_BMSK                                                0x400
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_SHFT                                                  0xa
#define HWIO_OVERRIDE_3_OVRID_SSC_SPIDEN_DISABLE_BMSK                                              0x200
#define HWIO_OVERRIDE_3_OVRID_SSC_SPIDEN_DISABLE_SHFT                                                0x9
#define HWIO_OVERRIDE_3_OVRID_PIMEM_SPIDEN_DISABLE_BMSK                                            0x100
#define HWIO_OVERRIDE_3_OVRID_PIMEM_SPIDEN_DISABLE_SHFT                                              0x8
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_BMSK                                               0x80
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_SHFT                                                0x7
#define HWIO_OVERRIDE_3_OVRID_APPS1_SPIDEN_DISABLE_BMSK                                             0x40
#define HWIO_OVERRIDE_3_OVRID_APPS1_SPIDEN_DISABLE_SHFT                                              0x6
#define HWIO_OVERRIDE_3_OVRID_APPS0_SPIDEN_DISABLE_BMSK                                             0x20
#define HWIO_OVERRIDE_3_OVRID_APPS0_SPIDEN_DISABLE_SHFT                                              0x5
#define HWIO_OVERRIDE_3_OVRID_SSC_SPNIDEN_DISABLE_BMSK                                              0x10
#define HWIO_OVERRIDE_3_OVRID_SSC_SPNIDEN_DISABLE_SHFT                                               0x4
#define HWIO_OVERRIDE_3_OVRID_PIMEM_SPNIDEN_DISABLE_BMSK                                             0x8
#define HWIO_OVERRIDE_3_OVRID_PIMEM_SPNIDEN_DISABLE_SHFT                                             0x3
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_BMSK                                               0x4
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_SHFT                                               0x2
#define HWIO_OVERRIDE_3_OVRID_APPS1_SPNIDEN_DISABLE_BMSK                                             0x2
#define HWIO_OVERRIDE_3_OVRID_APPS1_SPNIDEN_DISABLE_SHFT                                             0x1
#define HWIO_OVERRIDE_3_OVRID_APPS0_SPNIDEN_DISABLE_BMSK                                             0x1
#define HWIO_OVERRIDE_3_OVRID_APPS0_SPNIDEN_DISABLE_SHFT                                             0x0

#define HWIO_OVERRIDE_4_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d0)
#define HWIO_OVERRIDE_4_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_4_IN          \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, HWIO_OVERRIDE_4_RMSK)
#define HWIO_OVERRIDE_4_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, m)
#define HWIO_OVERRIDE_4_OUT(v)      \
        out_dword(HWIO_OVERRIDE_4_ADDR,v)
#define HWIO_OVERRIDE_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_4_ADDR,m,v,HWIO_OVERRIDE_4_IN)
#define HWIO_OVERRIDE_4_RSVD_31_6_BMSK                                                        0xffffffc0
#define HWIO_OVERRIDE_4_RSVD_31_6_SHFT                                                               0x6
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_NIDEN_DISABLE_BMSK                                             0x20
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_NIDEN_DISABLE_SHFT                                              0x5
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_DBGEN_DISABLE_BMSK                                             0x10
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_DBGEN_DISABLE_SHFT                                              0x4
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_SPIDEN_DISABLE_BMSK                                             0x8
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_SPIDEN_DISABLE_SHFT                                             0x3
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_SPNIDEN_DISABLE_BMSK                                            0x4
#define HWIO_OVERRIDE_4_OVRID_GSS_A5_SPNIDEN_DISABLE_SHFT                                            0x2
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_BMSK                                                 0x2
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_SHFT                                                 0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_BMSK                                                 0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_SHFT                                                 0x0

#define HWIO_OVERRIDE_5_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d4)
#define HWIO_OVERRIDE_5_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_5_IN          \
        in_dword_masked(HWIO_OVERRIDE_5_ADDR, HWIO_OVERRIDE_5_RMSK)
#define HWIO_OVERRIDE_5_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_5_ADDR, m)
#define HWIO_OVERRIDE_5_OUT(v)      \
        out_dword(HWIO_OVERRIDE_5_ADDR,v)
#define HWIO_OVERRIDE_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_5_ADDR,m,v,HWIO_OVERRIDE_5_IN)
#define HWIO_OVERRIDE_5_RSVD_31_0_BMSK                                                        0xffffffff
#define HWIO_OVERRIDE_5_RSVD_31_0_SHFT                                                               0x0

#define HWIO_OVERRIDE_6_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060d8)
#define HWIO_OVERRIDE_6_RMSK                                                                  0xffffffff
#define HWIO_OVERRIDE_6_IN          \
        in_dword_masked(HWIO_OVERRIDE_6_ADDR, HWIO_OVERRIDE_6_RMSK)
#define HWIO_OVERRIDE_6_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_6_ADDR, m)
#define HWIO_OVERRIDE_6_OUT(v)      \
        out_dword(HWIO_OVERRIDE_6_ADDR,v)
#define HWIO_OVERRIDE_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_6_ADDR,m,v,HWIO_OVERRIDE_6_IN)
#define HWIO_OVERRIDE_6_RSVD_31_0_BMSK                                                        0xffffffff
#define HWIO_OVERRIDE_6_RSVD_31_0_SHFT                                                               0x0

#define HWIO_CAPT_SEC_GPIO_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060dc)
#define HWIO_CAPT_SEC_GPIO_RMSK                                                                  0x3ffff
#define HWIO_CAPT_SEC_GPIO_IN          \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, HWIO_CAPT_SEC_GPIO_RMSK)
#define HWIO_CAPT_SEC_GPIO_INM(m)      \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, m)
#define HWIO_CAPT_SEC_GPIO_OUT(v)      \
        out_dword(HWIO_CAPT_SEC_GPIO_ADDR,v)
#define HWIO_CAPT_SEC_GPIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CAPT_SEC_GPIO_ADDR,m,v,HWIO_CAPT_SEC_GPIO_IN)
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_BMSK                                              0x20000
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_SHFT                                                 0x11
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_BMSK                                              0x10000
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_SHFT                                                 0x10
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FORCE_MSA_AUTH_EN_BMSK                                0x8000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FORCE_MSA_AUTH_EN_SHFT                                   0xf
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_AUTH_EN_BMSK                                       0x4000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_AUTH_EN_SHFT                                          0xe
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_PK_HASH_IN_FUSE_BMSK                               0x2000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_AP_PK_HASH_IN_FUSE_SHFT                                  0xd
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_AUTH_EN_BMSK                                      0x1000
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_AUTH_EN_SHFT                                         0xc
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_PK_HASH_IN_FUSE_BMSK                               0x800
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_MSA_PK_HASH_IN_FUSE_SHFT                                 0xb
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_ALL_USE_SERIAL_NUM_BMSK                                0x400
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_ALL_USE_SERIAL_NUM_SHFT                                  0xa
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_PK_HASH_INDEX_SRC_BMSK                                 0x200
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_PK_HASH_INDEX_SRC_SHFT                                   0x9
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_PBL_BOOT_SPEED_BMSK                               0x180
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_PBL_BOOT_SPEED_SHFT                                 0x7
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_RPM_BOOT_FROM_ROM_BMSK                                  0x40
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_RPM_BOOT_FROM_ROM_SHFT                                   0x6
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_BOOT_FROM_ROM_BMSK                                 0x20
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_APPS_BOOT_FROM_ROM_SHFT                                  0x5
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FAST_BOOT_BMSK                                          0x1e
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_FAST_BOOT_SHFT                                           0x1
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_WDOG_DISABLE_BMSK                                        0x1
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_WDOG_DISABLE_SHFT                                        0x0

#define HWIO_APP_PROC_CFG_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060e0)
#define HWIO_APP_PROC_CFG_RMSK                                                                    0x1fff
#define HWIO_APP_PROC_CFG_IN          \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, HWIO_APP_PROC_CFG_RMSK)
#define HWIO_APP_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, m)
#define HWIO_APP_PROC_CFG_OUT(v)      \
        out_dword(HWIO_APP_PROC_CFG_ADDR,v)
#define HWIO_APP_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APP_PROC_CFG_ADDR,m,v,HWIO_APP_PROC_CFG_IN)
#define HWIO_APP_PROC_CFG_SSC_DBG_SPNIDEN_BMSK                                                    0x1000
#define HWIO_APP_PROC_CFG_SSC_DBG_SPNIDEN_SHFT                                                       0xc
#define HWIO_APP_PROC_CFG_PIMEM_DBG_SPNIDEN_BMSK                                                   0x800
#define HWIO_APP_PROC_CFG_PIMEM_DBG_SPNIDEN_SHFT                                                     0xb
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_BMSK                                                     0x400
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_SHFT                                                       0xa
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_BMSK                                                    0x300
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_SHFT                                                      0x8
#define HWIO_APP_PROC_CFG_SSC_DBG_NIDEN_BMSK                                                        0x80
#define HWIO_APP_PROC_CFG_SSC_DBG_NIDEN_SHFT                                                         0x7
#define HWIO_APP_PROC_CFG_PIMEM_DBG_NIDEN_BMSK                                                      0x40
#define HWIO_APP_PROC_CFG_PIMEM_DBG_NIDEN_SHFT                                                       0x6
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_BMSK                                                        0x20
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_SHFT                                                         0x5
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_BMSK                                                       0x10
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_SHFT                                                        0x4
#define HWIO_APP_PROC_CFG_LPASS_DBG_NIDEN_BMSK                                                       0x8
#define HWIO_APP_PROC_CFG_LPASS_DBG_NIDEN_SHFT                                                       0x3
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_BMSK                                                         0x4
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_SHFT                                                         0x2
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_BMSK                                                        0x3
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_SHFT                                                        0x0

#define HWIO_MSS_PROC_CFG_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060e4)
#define HWIO_MSS_PROC_CFG_RMSK                                                                       0x7
#define HWIO_MSS_PROC_CFG_IN          \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, HWIO_MSS_PROC_CFG_RMSK)
#define HWIO_MSS_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, m)
#define HWIO_MSS_PROC_CFG_OUT(v)      \
        out_dword(HWIO_MSS_PROC_CFG_ADDR,v)
#define HWIO_MSS_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_PROC_CFG_ADDR,m,v,HWIO_MSS_PROC_CFG_IN)
#define HWIO_MSS_PROC_CFG_GSS_A5_DBG_SPNIDEN_BMSK                                                    0x4
#define HWIO_MSS_PROC_CFG_GSS_A5_DBG_SPNIDEN_SHFT                                                    0x2
#define HWIO_MSS_PROC_CFG_GSS_A5_DBG_NIDEN_BMSK                                                      0x2
#define HWIO_MSS_PROC_CFG_GSS_A5_DBG_NIDEN_SHFT                                                      0x1
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_BMSK                                                         0x1
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_SHFT                                                         0x0

#define HWIO_QFPROM_CLK_CTL_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060e8)
#define HWIO_QFPROM_CLK_CTL_RMSK                                                                     0x1
#define HWIO_QFPROM_CLK_CTL_IN          \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, HWIO_QFPROM_CLK_CTL_RMSK)
#define HWIO_QFPROM_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, m)
#define HWIO_QFPROM_CLK_CTL_OUT(v)      \
        out_dword(HWIO_QFPROM_CLK_CTL_ADDR,v)
#define HWIO_QFPROM_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CLK_CTL_ADDR,m,v,HWIO_QFPROM_CLK_CTL_IN)
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_BMSK                                                            0x1
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_SHFT                                                            0x0

#define HWIO_HDCP_KSV_LSB_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060ec)
#define HWIO_HDCP_KSV_LSB_RMSK                                                                0xffffffff
#define HWIO_HDCP_KSV_LSB_IN          \
        in_dword_masked(HWIO_HDCP_KSV_LSB_ADDR, HWIO_HDCP_KSV_LSB_RMSK)
#define HWIO_HDCP_KSV_LSB_INM(m)      \
        in_dword_masked(HWIO_HDCP_KSV_LSB_ADDR, m)
#define HWIO_HDCP_KSV_LSB_KSV_LSB_BMSK                                                        0xffffffff
#define HWIO_HDCP_KSV_LSB_KSV_LSB_SHFT                                                               0x0

#define HWIO_HDCP_KSV_MSB_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060f0)
#define HWIO_HDCP_KSV_MSB_RMSK                                                                      0xff
#define HWIO_HDCP_KSV_MSB_IN          \
        in_dword_masked(HWIO_HDCP_KSV_MSB_ADDR, HWIO_HDCP_KSV_MSB_RMSK)
#define HWIO_HDCP_KSV_MSB_INM(m)      \
        in_dword_masked(HWIO_HDCP_KSV_MSB_ADDR, m)
#define HWIO_HDCP_KSV_MSB_KSV_MSB_BMSK                                                              0xff
#define HWIO_HDCP_KSV_MSB_KSV_MSB_SHFT                                                               0x0

#define HWIO_JTAG_ID_ADDR                                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060f4)
#define HWIO_JTAG_ID_RMSK                                                                     0xffffffff
#define HWIO_JTAG_ID_IN          \
        in_dword_masked(HWIO_JTAG_ID_ADDR, HWIO_JTAG_ID_RMSK)
#define HWIO_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_JTAG_ID_ADDR, m)
#define HWIO_JTAG_ID_JTAG_ID_BMSK                                                             0xffffffff
#define HWIO_JTAG_ID_JTAG_ID_SHFT                                                                    0x0

#define HWIO_OEM_ID_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060f8)
#define HWIO_OEM_ID_RMSK                                                                      0xffffffff
#define HWIO_OEM_ID_IN          \
        in_dword_masked(HWIO_OEM_ID_ADDR, HWIO_OEM_ID_RMSK)
#define HWIO_OEM_ID_INM(m)      \
        in_dword_masked(HWIO_OEM_ID_ADDR, m)
#define HWIO_OEM_ID_OEM_ID_BMSK                                                               0xffff0000
#define HWIO_OEM_ID_OEM_ID_SHFT                                                                     0x10
#define HWIO_OEM_ID_OEM_PRODUCT_ID_BMSK                                                           0xffff
#define HWIO_OEM_ID_OEM_PRODUCT_ID_SHFT                                                              0x0

#define HWIO_TEST_BUS_SEL_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x000060fc)
#define HWIO_TEST_BUS_SEL_RMSK                                                                       0x7
#define HWIO_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, HWIO_TEST_BUS_SEL_RMSK)
#define HWIO_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, m)
#define HWIO_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TEST_BUS_SEL_ADDR,v)
#define HWIO_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TEST_BUS_SEL_ADDR,m,v,HWIO_TEST_BUS_SEL_IN)
#define HWIO_TEST_BUS_SEL_TEST_EN_BMSK                                                               0x4
#define HWIO_TEST_BUS_SEL_TEST_EN_SHFT                                                               0x2
#define HWIO_TEST_BUS_SEL_TEST_SELECT_BMSK                                                           0x3
#define HWIO_TEST_BUS_SEL_TEST_SELECT_SHFT                                                           0x0

#define HWIO_SPDM_DYN_SECURE_MODE_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006100)
#define HWIO_SPDM_DYN_SECURE_MODE_RMSK                                                               0x1
#define HWIO_SPDM_DYN_SECURE_MODE_IN          \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, HWIO_SPDM_DYN_SECURE_MODE_RMSK)
#define HWIO_SPDM_DYN_SECURE_MODE_INM(m)      \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, m)
#define HWIO_SPDM_DYN_SECURE_MODE_OUT(v)      \
        out_dword(HWIO_SPDM_DYN_SECURE_MODE_ADDR,v)
#define HWIO_SPDM_DYN_SECURE_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_SPDM_DYN_SECURE_MODE_ADDR,m,v,HWIO_SPDM_DYN_SECURE_MODE_IN)
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_BMSK                                                   0x1
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_SHFT                                                   0x0

#define HWIO_QC_IMAGE_ENCR_KEYn_ADDR(n)                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006104 + 0x4 * (n))
#define HWIO_QC_IMAGE_ENCR_KEYn_RMSK                                                          0xffffffff
#define HWIO_QC_IMAGE_ENCR_KEYn_MAXn                                                                   3
#define HWIO_QC_IMAGE_ENCR_KEYn_INI(n)        \
        in_dword_masked(HWIO_QC_IMAGE_ENCR_KEYn_ADDR(n), HWIO_QC_IMAGE_ENCR_KEYn_RMSK)
#define HWIO_QC_IMAGE_ENCR_KEYn_INMI(n,mask)    \
        in_dword_masked(HWIO_QC_IMAGE_ENCR_KEYn_ADDR(n), mask)
#define HWIO_QC_IMAGE_ENCR_KEYn_KEY_DATA0_BMSK                                                0xffffffff
#define HWIO_QC_IMAGE_ENCR_KEYn_KEY_DATA0_SHFT                                                       0x0

#define HWIO_OEM_IMAGE_ENCR_KEYn_ADDR(n)                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006114 + 0x4 * (n))
#define HWIO_OEM_IMAGE_ENCR_KEYn_RMSK                                                         0xffffffff
#define HWIO_OEM_IMAGE_ENCR_KEYn_MAXn                                                                  3
#define HWIO_OEM_IMAGE_ENCR_KEYn_INI(n)        \
        in_dword_masked(HWIO_OEM_IMAGE_ENCR_KEYn_ADDR(n), HWIO_OEM_IMAGE_ENCR_KEYn_RMSK)
#define HWIO_OEM_IMAGE_ENCR_KEYn_INMI(n,mask)    \
        in_dword_masked(HWIO_OEM_IMAGE_ENCR_KEYn_ADDR(n), mask)
#define HWIO_OEM_IMAGE_ENCR_KEYn_KEY_DATA0_BMSK                                               0xffffffff
#define HWIO_OEM_IMAGE_ENCR_KEYn_KEY_DATA0_SHFT                                                      0x0

#define HWIO_IMAGE_ENCR_KEY1_0_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006124)
#define HWIO_IMAGE_ENCR_KEY1_0_RMSK                                                           0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_0_IN          \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_0_ADDR, HWIO_IMAGE_ENCR_KEY1_0_RMSK)
#define HWIO_IMAGE_ENCR_KEY1_0_INM(m)      \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_0_ADDR, m)
#define HWIO_IMAGE_ENCR_KEY1_0_KEY_DATA0_BMSK                                                 0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_0_KEY_DATA0_SHFT                                                        0x0

#define HWIO_IMAGE_ENCR_KEY1_1_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006128)
#define HWIO_IMAGE_ENCR_KEY1_1_RMSK                                                           0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_1_IN          \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_1_ADDR, HWIO_IMAGE_ENCR_KEY1_1_RMSK)
#define HWIO_IMAGE_ENCR_KEY1_1_INM(m)      \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_1_ADDR, m)
#define HWIO_IMAGE_ENCR_KEY1_1_KEY_DATA0_BMSK                                                 0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_1_KEY_DATA0_SHFT                                                        0x0

#define HWIO_IMAGE_ENCR_KEY1_2_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000612c)
#define HWIO_IMAGE_ENCR_KEY1_2_RMSK                                                           0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_2_IN          \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_2_ADDR, HWIO_IMAGE_ENCR_KEY1_2_RMSK)
#define HWIO_IMAGE_ENCR_KEY1_2_INM(m)      \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_2_ADDR, m)
#define HWIO_IMAGE_ENCR_KEY1_2_KEY_DATA0_BMSK                                                 0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_2_KEY_DATA0_SHFT                                                        0x0

#define HWIO_IMAGE_ENCR_KEY1_3_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006130)
#define HWIO_IMAGE_ENCR_KEY1_3_RMSK                                                           0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_3_IN          \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_3_ADDR, HWIO_IMAGE_ENCR_KEY1_3_RMSK)
#define HWIO_IMAGE_ENCR_KEY1_3_INM(m)      \
        in_dword_masked(HWIO_IMAGE_ENCR_KEY1_3_ADDR, m)
#define HWIO_IMAGE_ENCR_KEY1_3_KEY_DATA0_BMSK                                                 0xffffffff
#define HWIO_IMAGE_ENCR_KEY1_3_KEY_DATA0_SHFT                                                        0x0

#define HWIO_PK_HASH1_0_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006134)
#define HWIO_PK_HASH1_0_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_0_IN          \
        in_dword_masked(HWIO_PK_HASH1_0_ADDR, HWIO_PK_HASH1_0_RMSK)
#define HWIO_PK_HASH1_0_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_0_ADDR, m)
#define HWIO_PK_HASH1_0_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_0_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_1_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006138)
#define HWIO_PK_HASH1_1_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_1_IN          \
        in_dword_masked(HWIO_PK_HASH1_1_ADDR, HWIO_PK_HASH1_1_RMSK)
#define HWIO_PK_HASH1_1_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_1_ADDR, m)
#define HWIO_PK_HASH1_1_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_1_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_2_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000613c)
#define HWIO_PK_HASH1_2_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_2_IN          \
        in_dword_masked(HWIO_PK_HASH1_2_ADDR, HWIO_PK_HASH1_2_RMSK)
#define HWIO_PK_HASH1_2_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_2_ADDR, m)
#define HWIO_PK_HASH1_2_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_2_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_3_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006140)
#define HWIO_PK_HASH1_3_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_3_IN          \
        in_dword_masked(HWIO_PK_HASH1_3_ADDR, HWIO_PK_HASH1_3_RMSK)
#define HWIO_PK_HASH1_3_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_3_ADDR, m)
#define HWIO_PK_HASH1_3_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_3_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_4_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006144)
#define HWIO_PK_HASH1_4_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_4_IN          \
        in_dword_masked(HWIO_PK_HASH1_4_ADDR, HWIO_PK_HASH1_4_RMSK)
#define HWIO_PK_HASH1_4_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_4_ADDR, m)
#define HWIO_PK_HASH1_4_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_4_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_5_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006148)
#define HWIO_PK_HASH1_5_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_5_IN          \
        in_dword_masked(HWIO_PK_HASH1_5_ADDR, HWIO_PK_HASH1_5_RMSK)
#define HWIO_PK_HASH1_5_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_5_ADDR, m)
#define HWIO_PK_HASH1_5_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_5_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_6_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000614c)
#define HWIO_PK_HASH1_6_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_6_IN          \
        in_dword_masked(HWIO_PK_HASH1_6_ADDR, HWIO_PK_HASH1_6_RMSK)
#define HWIO_PK_HASH1_6_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_6_ADDR, m)
#define HWIO_PK_HASH1_6_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_6_HASH_DATA0_SHFT                                                              0x0

#define HWIO_PK_HASH1_7_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006150)
#define HWIO_PK_HASH1_7_RMSK                                                                  0xffffffff
#define HWIO_PK_HASH1_7_IN          \
        in_dword_masked(HWIO_PK_HASH1_7_ADDR, HWIO_PK_HASH1_7_RMSK)
#define HWIO_PK_HASH1_7_INM(m)      \
        in_dword_masked(HWIO_PK_HASH1_7_ADDR, m)
#define HWIO_PK_HASH1_7_HASH_DATA0_BMSK                                                       0xffffffff
#define HWIO_PK_HASH1_7_HASH_DATA0_SHFT                                                              0x0

#define HWIO_SW_ROT_STICKY_BIT_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006154)
#define HWIO_SW_ROT_STICKY_BIT_RMSK                                                                  0x1
#define HWIO_SW_ROT_STICKY_BIT_IN          \
        in_dword_masked(HWIO_SW_ROT_STICKY_BIT_ADDR, HWIO_SW_ROT_STICKY_BIT_RMSK)
#define HWIO_SW_ROT_STICKY_BIT_INM(m)      \
        in_dword_masked(HWIO_SW_ROT_STICKY_BIT_ADDR, m)
#define HWIO_SW_ROT_STICKY_BIT_OUT(v)      \
        out_dword(HWIO_SW_ROT_STICKY_BIT_ADDR,v)
#define HWIO_SW_ROT_STICKY_BIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_SW_ROT_STICKY_BIT_ADDR,m,v,HWIO_SW_ROT_STICKY_BIT_IN)
#define HWIO_SW_ROT_STICKY_BIT_SW_ROT_STICKY_BIT_0_BMSK                                              0x1
#define HWIO_SW_ROT_STICKY_BIT_SW_ROT_STICKY_BIT_0_SHFT                                              0x0

#define HWIO_SW_ROT_CONFIG_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006158)
#define HWIO_SW_ROT_CONFIG_RMSK                                                                      0x3
#define HWIO_SW_ROT_CONFIG_IN          \
        in_dword_masked(HWIO_SW_ROT_CONFIG_ADDR, HWIO_SW_ROT_CONFIG_RMSK)
#define HWIO_SW_ROT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_SW_ROT_CONFIG_ADDR, m)
#define HWIO_SW_ROT_CONFIG_CURRENT_SW_ROT_MODEM_BMSK                                                 0x2
#define HWIO_SW_ROT_CONFIG_CURRENT_SW_ROT_MODEM_SHFT                                                 0x1
#define HWIO_SW_ROT_CONFIG_CURRENT_SW_ROT_APPS_BMSK                                                  0x1
#define HWIO_SW_ROT_CONFIG_CURRENT_SW_ROT_APPS_SHFT                                                  0x0


#endif /* __CHIPINFOHWIO_H__ */
