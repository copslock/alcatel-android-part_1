/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PM RPM PROC

GENERAL DESCRIPTION
This file contains PMIC initialization functions

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/framework/src/pm_init.c#5 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/23/13   hs      Fixed the naming convention in \config.
04/12/13   hs      Code refactoring.
02/27/13   hs      Code refactoring.
05/10/11   jtn     Fix RPM init bug for 8960
07/01/10   umr     Created.
===========================================================================*/
#include "pm_comm.h"
#include "pm_target_information.h"
#include "pm_rpm_npa.h"
#include "pmapp_npa.h"
#include "pm_resource_manager.h"
#include "pm_rpm_target.h"
#include "pm_mpm_internal.h"
#include "device_info.h"

npa_client_handle handle_rpm_init = NULL;

#ifdef PM_IMAGE_RPM_PROC
__attribute__((section("pm_dram_reclaim_pool")))
void pm_init( void )
#else
void pm_rpm_init( void )
#endif
{
    pm_comm_channel_init_internal();
    
    pm_version_detect();

    pm_target_information_init();
    
    pm_rpm_platform_pre_init();

    pm_comm_info_init();

    pm_resource_manager_init();

    pm_rpm_proc_npa_init ();

    pm_rpm_railway_init();

    pm_rpm_platform_init();

    //This function may be no-op on targets that do not support
    //sleep yet
    pm_rpm_sleep_init();

    /*Init for SPMI MPM command sequence. */
    pm_mpm_cmd_init();
    
    return ;
}
