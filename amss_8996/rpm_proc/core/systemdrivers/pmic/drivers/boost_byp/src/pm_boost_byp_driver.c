/*! \file pm_boost_byp_driver.c 
*  \n
*  \brief This file contains BOOST BYPASS peripheral driver initialization during which the driver
*         driver data is stored.
*  \n  
*  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/drivers/boost_byp/src/pm_boost_byp_driver.c#3 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/06/14   kt      Created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_boost_byp_driver.h"
#include "pm_target_information.h"
#include "device_info.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the BOOST BYPASS driver data */
static pm_boost_byp_data_type *pm_boost_byp_data_arr[PM_MAX_NUM_PMICS];

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS 

===========================================================================*/

__attribute__((section("pm_dram_reclaim_pool")))
void pm_boost_byp_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_boost_byp_data_type *boost_byp_ptr = NULL;
    uint16 boost_byp_index = 0;
    pm_register_address_type base_address = 0;
    pm_register_address_type periph_offset = 0;
    
    boost_byp_ptr = pm_boost_byp_data_arr[pmic_index];
    
    if (boost_byp_ptr == NULL)
    {
        pm_malloc( sizeof(pm_boost_byp_data_type), (void**)&boost_byp_ptr);

        /* Assign Comm ptr */
        boost_byp_ptr->comm_ptr = comm_ptr;

        /* BOOST BYPASS Register Info - Obtaining Data through dal config */
        boost_byp_ptr->pm_pwr_data.pwr_reg_table = (pm_pwr_register_info_type*)pm_target_information_get_common_info(PM_PROP_BOOST_BYP_REG);

        CORE_VERIFY_PTR(boost_byp_ptr->pm_pwr_data.pwr_reg_table);

        /* BOOST BYPASS Num of peripherals - Obtaining Data through dal config */
        boost_byp_ptr->pm_pwr_data.num_of_peripherals = pm_target_information_get_periph_count_info(PM_PROP_BOOST_BYP_NUM, pmic_index);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(boost_byp_ptr->pm_pwr_data.num_of_peripherals != 0);

        /* BOOST BYPASS pwr rail specific info pointer malloc to save all the peripheral's base address, Type, Range and Vset */
        pm_malloc(sizeof(pm_pwr_specific_info_type)*(boost_byp_ptr->pm_pwr_data.num_of_peripherals), (void**)&(boost_byp_ptr->pm_pwr_data.pwr_specific_info));

        /* Save first BOOST BYPASS peripheral's base address */
        boost_byp_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address = peripheral_info->base_address;

        pm_boost_byp_data_arr[pmic_index] = boost_byp_ptr;
    }
    
    if (boost_byp_ptr != NULL)
    {
        base_address = boost_byp_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address;
        periph_offset = boost_byp_ptr->pm_pwr_data.pwr_reg_table->peripheral_offset;

        /* Peripheral Baseaddress should be >= first peripheral's base addr */
        CORE_VERIFY(peripheral_info->base_address >= base_address);

        /* Calculate BOOST BYPASS peripheral index */
        boost_byp_index = ((peripheral_info->base_address - base_address)/periph_offset);

        /* Peripheral Index should be less than number of peripherals */
        CORE_VERIFY(boost_byp_index < (boost_byp_ptr->pm_pwr_data.num_of_peripherals));

        /* Save BOOST's Peripheral Type value */
        boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].periph_type = peripheral_info->peripheral_type;

        /* Save each BOOST BYPASS peripheral's base address */
        boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].periph_base_address = peripheral_info->base_address;

        /* Boost bypass has only range 0 */
        boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_range = 0;

        /* Boost bypass Vset Info - Obtaining Data through dal config */
        boost_byp_ptr->pm_pwr_data.pwr_specific_info[boost_byp_index].pwr_vset = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_BOOST_BYP_VOLT);
    }
}

pm_boost_byp_data_type* pm_boost_byp_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_PMICS)
    {
        return pm_boost_byp_data_arr[pmic_index];
    }

    return NULL;
}

uint8 pm_boost_byp_get_num_peripherals(uint8 pmic_index)
{
    if((pmic_index < PM_MAX_NUM_PMICS) &&
       (pm_boost_byp_data_arr[pmic_index] != NULL))
    {
        return pm_boost_byp_data_arr[pmic_index]->pm_pwr_data.num_of_peripherals;
    }

    return NULL;
}
