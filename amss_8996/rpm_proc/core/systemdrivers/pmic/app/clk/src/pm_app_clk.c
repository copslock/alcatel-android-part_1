/*! \file pm_app_clk.c
 *  \n
 *  \brief Implementation file for CLOCK application level APIs. 
 *  \n  
 *  \n &copy; Copyright 2015 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/app/clk/src/pm_app_clk.c#1 $
 
when        who     what, where, why
--------    ---     ----------------------------------------------------------
07/23/15    kt       Created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_app_clk.h"
#include "pm_clk_xo.h"
#include "pm_clk_driver.h"
#include "pm_comm.h"

/*===========================================================================

                     API IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_app_clk_get_xo_warmup_time(uint32 *warmup_time_usec)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint32 xo_num_sleep_clk_cycles = 0;
    uint32 bbclk_num_sleep_clk_cycles = 0;
    uint32 num_sleep_clk_cycles = 0;
    uint8 pmk8001_pmic_index = 3;

    if (warmup_time_usec == NULL)
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }

    /* Get the number of sleep (32 kHz) clock cycles to wait */
    err_flag = pm_clk_xo_get_warmup_time(0, PM_CLK_XO, &xo_num_sleep_clk_cycles);

    if (pm_get_pmic_model(pmk8001_pmic_index) == PMIC_IS_PMK8001)
    {
       /* if there is a PMK8001, check for the warm-up time programmed on LN_BB_CLK */
       err_flag |= pm_clk_xo_get_warmup_time(pmk8001_pmic_index, PM_CLK_LN_BB, &bbclk_num_sleep_clk_cycles);
    }

    if (err_flag == PM_ERR_FLAG__SUCCESS)
    {
       /* Warmup time in number of sleep (32KHz) clock cycles = MAX(XO_TIMER, LN_BB_CLK_TIMER).
          LN_BB_CLK timer is valid only if there is a PMK8001 pmic
       */
       num_sleep_clk_cycles = MAX(xo_num_sleep_clk_cycles, bbclk_num_sleep_clk_cycles);

       /* Calculate the warm-up time in micro seconds: N is the number of sleep (32 KHz) clock cycles,
          we need to add at most 3 clock cycles of synchronization time.
        
          warmup_time (micro secs) = ((N + 3) * 1000)/32
       */
       *warmup_time_usec = ((num_sleep_clk_cycles + 3) * 1000)/32;
    }
    
    return err_flag;
}
