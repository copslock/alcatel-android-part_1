#ifndef PM_RPM_BOOST_BYP_TRANS_APPLY__H
#define PM_RPM_BOOST_BYP_TRANS_APPLY__H

/*! \file pm_rpm_boost_byp_trans_apply.h
 *  \n
 *  \brief This file contains prototype definitions for PMIC Boost Bypass
 *         register resources and register resource dependencies functions.
 *  \n  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/npa/inc/pm_rpm_boost_byp_trans_apply.h#2 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
11/22/13    kt      Created.
========================================================================== */
/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "pm_rpm_utilities.h"
#include "rpmserver.h"
#include "pm_boost_byp_driver.h"
#include "pm_boost_byp.h"

#define GET_PMIC_INDEX_BOOST_BYP(resourceType) ((resourceType & 0xFF000000 )>> 24) - 0x61;

/* Boost bypass voltage step size in micro volts (CR761922) */
#define PM_BBYP_VOLT_STEP_SIZE_UV          50000
/*===========================================================================

                     STRUCTURE TYPE AND ENUM

===========================================================================*/

typedef struct 
{
    unsigned        AccessAllowed:1;  // 0 � NO NPA Access; 1 � NPA Access Granted
    unsigned        AlwaysOn:1;       // 0 - allow to be turned off; 1 - always on
    unsigned        MinPwrMode:3;     // 0 - Force Bypass, 1- Auto Boost
    unsigned        reserved:27; 
    //32 bit boundary
    unsigned        MinVoltage:16;    // in mV
    unsigned        MaxVoltage:16;    // in mV
    //32 bit boundary
    unsigned        MinPinVoltage:16; // in mV
    unsigned        MaxPinVoltage:16; // in mV
    //32 bit boundary
    
}pm_rpm_boost_byp_rail_info_type;

/* BOOST BYPASS Callback data used during translate and apply */
typedef struct 
{
    rpm_resource_type                         resourceType;
    pm_boost_byp_data_type                    *boostBypDriverData;
    pm_pwr_resource_dependency_info           **depInfo;
    pm_rpm_boost_byp_rail_info_type           **railInfo;
}pm_npa_boost_byp_data_type;

/*===========================================================================

                        FUNCTION PROTOTYPE

===========================================================================*/

void pm_rpm_boost_byp_register_resources(rpm_resource_type resource, uint32 num_npa_resources, uint8 pmic_index);
void pm_rpm_boost_byp_register_resource_dependencies(rpm_resource_type resource, uint32 num_npa_resources, uint8 pmic_index);

#endif // PM_RPM_BOOST_BYP_TRANS_APPLY__H
