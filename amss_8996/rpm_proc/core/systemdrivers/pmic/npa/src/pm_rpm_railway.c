/*! \file pm_rpm_railway.c
 *
 *  \brief Implementation file for PMIC Railway public APIs.
 *
 *  &copy; Copyright 2012 - 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/npa/src/pm_rpm_railway.c#4 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/14   vtw     Workaround for gfx multi-phase support.
03/20/14   kt      Added EBI rail support.
06/10/13   aks     Disable BYPASS for LDO3 for 8x26.
06/07/13   hs      Added the alg for settling time.
06/05/13   aks     Added api for railway to get vset and range info
05/21/13   hs      Added support for FTS mode transition.
05/16/13   hs      Added the alg Vstepper sequencing change
                   needed for Auto configured FTS2.
04/25/13   aks     Code Refactoring: Removing PMIC_SubRsc from pwr algs
04/23/13   hs      Fixed the naming convention in \config.
04/12/13   hs      Code refactoring.
03/27/13   hs      Add support for SW mode.
02/27/13   hs      Code refactoring.
11/11/12   hs      Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pmapp_npa.h"
#include "pm_target_information.h"
#include "pm_smps_driver.h"
#include "pm_ldo_driver.h"
#include "pm_version.h"
#include "pm_comm.h"
#include "pm_version.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_pwr_alg.h"
#include "pm_railway.h"


#define PM_RAILWAY_LDO_SAFETY_HEADROOM            50000//uV

#define RPM_LDO_REQ         0x6F646C    // 'ldo'  in little endian
#define RPM_SMPS_REQ        0x706D73 // 'smp' in little endian 

// Function Pointers for Virtual Methods
typedef unsigned (*pm_func_railway_set_voltage)(pm_railway_type_info_type rail, unsigned voltage_uv);
typedef unsigned (*pm_func_railway_set_mode)(pm_railway_type_info_type rail, pm_sw_mode_type  mode);

typedef struct
{
    unsigned volt_uv:28;
    unsigned sw_en:1;
    unsigned sw_mode:3;
    unsigned byp_allowed:1;
    unsigned input_uvol :28;
}pm_railway_state_info_type;

typedef struct
{
    // Function Pointers for Virtual Methods
    pm_func_railway_set_voltage           set_voltage;
    pm_func_railway_set_mode              set_mode;
    
   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   pm_pwr_resource_info_type*             resource;
   void*                                  rpm_cb_data;
   pm_railway_state_info_type             pre_state;
}pm_railway_type;

static pm_railway_type pmRailway[PM_RAILWAY_INVALID];

static pm_npa_smps_int_rep  smps_cur_data;
static pm_npa_ldo_int_rep   ldo_cur_data;

static pm_npa_smps_int_rep  smps_shadow_data;
static pm_npa_ldo_int_rep   ldo_shadow_data;
static rpm_application_info rpmInfo;

// Store SBL setting for soft start
static uint8 pm_gfx_smps_sbl_ss_cfg = 0;
// Store SBL setting for vstep
static uint8 pm_gfx_smps_sbl_vstep_cfg = 0;

static void pm_rpm_railway_init_i(pm_railway_type_info_type rail);
static unsigned pm_railway_ldo_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv);
static unsigned pm_railway_ldo_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode);
static unsigned pm_railway_smps_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv);
static unsigned pm_railway_smps_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode);

__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_railway_init(void)
{
    pmRailway[PM_RAILWAY_MX].resource = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_MX);
    pm_rpm_railway_init_i(PM_RAILWAY_MX);

    pmRailway[PM_RAILWAY_CX].resource = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_CX);
    pm_rpm_railway_init_i(PM_RAILWAY_CX);

    pmRailway[PM_RAILWAY_GFX].resource = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_GFX);
    if(pmRailway[PM_RAILWAY_GFX].resource)
    {
       if(PMIC_IS_PM8004 == pm_get_pmic_model(2))
       {
          pmRailway[PM_RAILWAY_GFX].resource->resource_type = RPM_SMPS_C_REQ;
       }
    }
    pm_rpm_railway_init_i(PM_RAILWAY_GFX);


    smps_cur_data.global_byp_en = 1;
    
}

__attribute__((section("pm_dram_reclaim_pool")))
void pm_rpm_railway_init_i(pm_railway_type_info_type rail)
{
    pm_volt_level_type vol = 0;
    pm_on_off_type en_status;
    pm_sw_mode_type mode_status;
    uint8 resource_index = 0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 pmic_index = 0xFF;
    if (pmRailway[rail].resource == NULL)
    {
        return;
    }

    resource_index = pmRailway[rail].resource->resource_index - 1;
    
    switch(pmRailway[rail].resource->resource_type & 0x00FFFFFF)
    {
    case RPM_LDO_REQ:
    
        {
            pm_ldo_data_type* ldo_ptr = NULL;
            pmic_index = GET_PMIC_INDEX_LDO(pmRailway[rail].resource->resource_type);
            pmRailway[rail].set_voltage = pm_railway_ldo_set_voltage_i;
            pmRailway[rail].set_mode = pm_railway_ldo_set_mode_i;
            pmRailway[rail].rpm_cb_data = pm_rpm_ldo_get_resource_data(pmic_index);
            ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;

            
            pm_pwr_sw_enable_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &en_status);
            pmRailway[rail].pre_state.sw_en = en_status;

            pm_pwr_sw_mode_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &mode_status);
            pmRailway[rail].pre_state.sw_mode = mode_status;

            if(en_status) // only update the voltage when the rail is on, otherwise, put the voltage to 0.
            {
                pm_pwr_volt_level_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &vol);
                pmRailway[rail].pre_state.volt_uv = vol;
                pmRailway[rail].pre_state.input_uvol  = vol + PM_RAILWAY_LDO_SAFETY_HEADROOM;
            }
            else
            {
                pmRailway[rail].pre_state.volt_uv = 0;
                pmRailway[rail].pre_state.input_uvol  = 0;
            }
        }
        break;
    case RPM_SMPS_REQ:
        {
            pm_smps_data_type* smps_ptr = NULL;
            pmic_index = GET_PMIC_INDEX_SMPS(pmRailway[rail].resource->resource_type);;

            pmRailway[rail].set_voltage = pm_railway_smps_set_voltage_i;
            pmRailway[rail].set_mode = pm_railway_smps_set_mode_i;
            pmRailway[rail].rpm_cb_data = pm_rpm_smps_get_resource_data(pmic_index);
            smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;

            pm_pwr_sw_enable_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &en_status);
            pmRailway[rail].pre_state.sw_en = en_status;

            pm_pwr_sw_mode_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &mode_status);
            pmRailway[rail].pre_state.sw_mode = mode_status;

            if(en_status) // only update the voltage when the rail is on, otherwise, put the voltage to 0.
            {
                pm_pwr_volt_level_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &vol);
                pmRailway[rail].pre_state.volt_uv = vol;
            }
            else
            {
                pmRailway[rail].pre_state.volt_uv = 0;
            }

            // For gfx, save soft start and voltage step settings by boot.
            if(rail == PM_RAILWAY_GFX)
            {
                err_flag = pm_smps_get_softstart(pmic_index, resource_index,
                                                   &pm_gfx_smps_sbl_ss_cfg);
                err_flag |= pm_smps_get_vstep(pmic_index, resource_index,
                                                   &pm_gfx_smps_sbl_vstep_cfg);
            }
        }
        break;
    default:
        break;
    }
}

unsigned pm_railway_set_mode(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
    if(rail<PM_RAILWAY_INVALID)
    {
        return pmRailway[rail].set_mode(rail, mode);
    }
    else
    {
        return 1; // error flag
    }
}

unsigned pm_railway_set_voltage(pm_railway_type_info_type rail, unsigned voltage_uv)
{
    if(rail<PM_RAILWAY_INVALID)
    {
        return pmRailway[rail].set_voltage(rail, voltage_uv);
    }
    else
    {
        return 1; // error flag
    }
}

unsigned pm_railway_set_phase(pm_railway_type_info_type rail, pm_phase_cnt_type phase)
{
    return 1; //error flag - not supported any more
}

pm_err_flag_type pm_railway_calculate_vset(pm_railway_type_info_type rail , unsigned voltage_uv ,uint32*  vset)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 pmic_index = 0xFF;
    pm_volt_level_type  dummy_ceiling = 0;
    
    if(rail<PM_RAILWAY_INVALID)
    {
        uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
        switch(pmRailway[rail].resource->resource_type & 0x00FFFFFF)
        {
            case RPM_LDO_REQ:
            {
                pm_ldo_data_type* ldo_ptr = NULL;
                pmic_index = GET_PMIC_INDEX_LDO(pmRailway[rail].resource->resource_type); 
                pmRailway[rail].rpm_cb_data = pm_rpm_ldo_get_resource_data(pmic_index);
                ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
                err_flag = pm_pwr_volt_calculate_vset_celing_uv(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, voltage_uv,vset, &dummy_ceiling);
            }
            break;
            case RPM_SMPS_REQ:
            {
                pm_smps_data_type* smps_ptr = NULL;
                 pmic_index = GET_PMIC_INDEX_SMPS(pmRailway[rail].resource->resource_type); 
                pmRailway[rail].rpm_cb_data = pm_rpm_smps_get_resource_data(pmic_index);
                smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;
                err_flag = pm_pwr_volt_calculate_vset_celing_uv(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, voltage_uv,vset, &dummy_ceiling);
            }
            break;
        }
    }
    else
    {
        err_flag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

    return err_flag;
}

unsigned pm_railway_smps_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
    uint8 pmic_index = GET_PMIC_INDEX_SMPS(pmRailway[rail].resource->resource_type);;
    pm_smps_data_type* smps_ptr = NULL;
    pm_volt_level_type ceiling_voltage = 0; //voltage rounded to next high 
    uint32             dummy_vset = 0;
    // construct the current_aggregation.
    if(voltage_uv > 0) //turning on the rail
    {
        smps_cur_data.sw_en = 1;
        smps_cur_data.uvol = voltage_uv;
        //check if rounding off vset is needed
		pmRailway[rail].rpm_cb_data = pm_rpm_smps_get_resource_data(pmic_index);
        smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;
				    
        pm_pwr_volt_calculate_vset_celing_uv(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index ,
		                          (pm_volt_level_type)smps_cur_data.uvol, &dummy_vset, &ceiling_voltage);
				    
		if(ceiling_voltage != 0)
        {
           smps_cur_data.uvol  =  ceiling_voltage; 
        }
    }
    else //turning off the rail
    {
        smps_cur_data.sw_en = 0;
        smps_cur_data.uvol = 0;
    }

    // construct the shadow
    smps_shadow_data.sw_en = pmRailway[rail].pre_state.sw_en;
    smps_shadow_data.uvol = pmRailway[rail].pre_state.volt_uv;

    rpmInfo.current_aggregation = &smps_cur_data;

    //construct the cb_data
    rpmInfo.cb_data = pmRailway[rail].rpm_cb_data;

    rpmInfo.id = pmRailway[rail].resource->resource_index;

    // consider request from PMIC railway as an internal client request
    rpmInfo.client_type = PM_RPM_NPA_CLIENT_SMPS_REQ;

    
    
    // pass over to the driver.
    pm_rpm_smps_dependency_execute(&rpmInfo, &smps_shadow_data );

    
    // Apply soft start setting for mutli-phase workaround.
    if (rail == PM_RAILWAY_GFX)
    {
        // rail OFF --> ON, set soft-start setting equal to vstep setting
        if((voltage_uv > 0) && (pmRailway[rail].pre_state.sw_en == 0))
        {
            err_flag |= pm_smps_set_softstart(pmic_index, resource_index,
                                                pm_gfx_smps_sbl_vstep_cfg);
        }
         // rail ON --> OFF, set soft-start setting equal to soft start setting
        else if ((voltage_uv == 0) && (pmRailway[rail].pre_state.sw_en == 1))
        {
            err_flag |= pm_smps_set_softstart(pmic_index, resource_index,
                                               pm_gfx_smps_sbl_ss_cfg);
        }
    }

    // update the previous state
    pmRailway[rail].pre_state.sw_en = smps_cur_data.sw_en;
    pmRailway[rail].pre_state.volt_uv = smps_cur_data.uvol;

    return (unsigned)err_flag;
}

unsigned pm_railway_smps_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_sw_mode_type  sw_mode = mode;
  uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
  uint8 pmic_index = GET_PMIC_INDEX_SMPS(pmRailway[rail].resource->resource_type);

  if(sw_mode == PM_SW_MODE_AUTO) // Enable automode
  {
      err_flag |= pm_smps_sw_mode(pmic_index, resource_index, PM_SW_MODE_AUTO);
      //update the mode
      pmRailway[rail].pre_state.sw_mode = PM_SW_MODE_AUTO;
    
  }
  else if (sw_mode == PM_SW_MODE_NPM)//disable automode
  {
    err_flag |= pm_smps_sw_mode(pmic_index, resource_index, PM_SW_MODE_NPM);
    //update the mode
    pmRailway[rail].pre_state.sw_mode = PM_SW_MODE_NPM;
   
  }
  else // un-supported modes
  {
      return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }

  return (unsigned)err_flag;
}


unsigned pm_railway_ldo_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv)
{
    pm_ldo_data_type* ldo_ptr = NULL;
    pm_volt_level_type ceiling_voltage = 0; //voltage rounded to next high
    uint32             dummy_vset = 0;   
    // construct the current_aggregation.
    if(voltage_uv > 0) //turning on the rail
    {
        ldo_cur_data.sw_en = 1;
        ldo_cur_data.output_uvol   = voltage_uv;
        if (rail == PM_RAILWAY_EBI) 
        {
            //hack for DDR EBI - remove this when the railway framework is ready
            ldo_cur_data.input_uvol  = voltage_uv + 75000; 
        }
        else
        {
            ldo_cur_data.input_uvol  = voltage_uv + PM_RAILWAY_LDO_SAFETY_HEADROOM;
        }

        //check if rounding off vset is needed
				    pmRailway[rail].rpm_cb_data = pm_rpm_ldo_get_resource_data(ldo_cur_data.device_index);
				    ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
				    
				    pm_pwr_volt_calculate_vset_celing_uv(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr,ldo_cur_data.resource_id -1 ,
				                             (pm_volt_level_type)ldo_cur_data.output_uvol, &dummy_vset, &ceiling_voltage);
				    
				    if(ceiling_voltage != 0)
				    {
				        ldo_cur_data.input_uvol +=   ceiling_voltage - ldo_cur_data.output_uvol; 
				        ldo_cur_data.output_uvol =  ceiling_voltage; 
				    }

    }
    else //turning off the rail
    {
        ldo_cur_data.sw_en = 0;
        ldo_cur_data.output_uvol   = 0;
        ldo_cur_data.input_uvol  = 0;
    }
    ldo_cur_data.resource_id = pmRailway[rail].resource->resource_index;
    ldo_cur_data.device_index = GET_PMIC_INDEX_LDO(pmRailway[rail].resource->resource_type);

    //disable BYPASS for CORE LDO rails
    ldo_cur_data.byp_allowed = PM_NPA_BYPASS_DISALLOWED;

    // construct the shadow
    ldo_shadow_data.sw_en = pmRailway[rail].pre_state.sw_en;
    ldo_shadow_data.output_uvol   = pmRailway[rail].pre_state.volt_uv;
    ldo_shadow_data.input_uvol  = pmRailway[rail].pre_state.input_uvol ;
    ldo_shadow_data.byp_allowed = pmRailway[rail].pre_state.byp_allowed;

    rpmInfo.current_aggregation = &ldo_cur_data;
    rpmInfo.new_state = NULL;
    //construct the cb_data
    rpmInfo.cb_data = pmRailway[rail].rpm_cb_data;

    rpmInfo.id = pmRailway[rail].resource->resource_index;

    // consider request from PMIC railway as an internal client request
    rpmInfo.client_type = PM_RPM_NPA_CLIENT_LDO_REQ;

    
    // need to update shadow and pass over to the driver.
    pm_rpm_ldo_dependency_execute(&rpmInfo, &ldo_shadow_data);

    // update the previous state
    pmRailway[rail].pre_state.sw_en = ldo_cur_data.sw_en;
    pmRailway[rail].pre_state.volt_uv = ldo_cur_data.output_uvol  ;
    pmRailway[rail].pre_state.input_uvol  = ldo_cur_data.input_uvol ;
    pmRailway[rail].pre_state.byp_allowed = ldo_cur_data.byp_allowed;

    return 0;

}

unsigned pm_railway_ldo_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_sw_mode_type  sw_mode = mode;
  uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
  uint8 pmic_index = GET_PMIC_INDEX_LDO(pmRailway[rail].resource->resource_type);

  if(sw_mode == PM_SW_MODE_NPM) // Enable NPM
  {
    if(pmRailway[rail].pre_state.sw_mode != PM_SW_MODE_NPM)
      err_flag |= pm_ldo_sw_mode(pmic_index, resource_index, PM_SW_MODE_NPM);
  }
  else // un-supported modes
  {
      return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }

  //update the mode
  pmRailway[rail].pre_state.sw_mode = sw_mode;

  return (unsigned)err_flag;
}

pm_err_flag_type pm_railway_get_voltage(pm_railway_type_info_type rail , pm_volt_level_type* voltage_uv)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  uint8 resource_index;

  if(voltage_uv == NULL)
  {
    return PM_ERR_FLAG__INVALID_POINTER;
  }
  if(rail > PM_RAILWAY_INVALID)
  {
    return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
  }

  /* Get rail resource index. */
  resource_index = pmRailway[rail].resource->resource_index - 1;

  /* Get voltage */
  switch(pmRailway[rail].resource->resource_type & 0x00FFFFFF)
  {
    case RPM_LDO_REQ:
    {
      pm_ldo_data_type* ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
      err_flag = pm_pwr_volt_level_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, voltage_uv);
    }
    break;
    case RPM_SMPS_REQ:
    {
      pm_smps_data_type* smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;
      err_flag = pm_pwr_volt_level_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, voltage_uv);
    }
    break;
  }

  return err_flag;
}

