/*! \file pm_rpm_utilities.c
 *
 *  \brief This file contains RPM PMIC utility functions for trans apply aggregation layer.
 *
 *  &copy; Copyright 2012 - 2015 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/npa/src/pm_rpm_utilities.c#8 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/12/13   hs      Code refactoring.
02/27/13   hs      Code refactoring.
01/29/13   aks     Adding support for Boost as separate peripheral
05/18/12   wra     Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "rpmserver.h"
#include "pm_rpm_utilities.h"
#include "CoreVerify.h"
#include "pm_rpm_target.h"

#define PM_NUM_OF_STATUS_REGS 5

/* Settling errors log info */
typedef struct
{
    uint32 actual_time_us;
    uint32 estimated_time_us;
    uint8 periph_type;
    uint8 periph_index;
    uint8 pmic_index;
    uint8 status_reg_dump[PM_NUM_OF_STATUS_REGS];
    /* Battery status */
    pm_battery_status_type batt_status;
}pm_settle_info_type;

pm_settle_info_type pm_settle_vreg_ok_err_info;

pm_settle_info_type pm_settle_stepper_done_err_info;

/*===========================================================================

                     FUNCTION IMPLEMENTATION 

===========================================================================*/

unsigned pm_rpm_retrieve_operand_value( pm_pwr_resource_operand * operand) 
{
    unsigned leftValue;
    //Get the internal rep information for left and right operands
    if(operand->operand_type == PM_PWR_OPERAND__RESOURCE)
    {
        leftValue = pm_rpm_get_component_resource_value(operand->resource->resource_type, 
            operand->resource->internal_resource_index, operand->resource_key); 
    }
    else if(operand->operand_type == PM_PWR_OPERAND__STATIC)
    {
        // It is very unlikely this will get called
        leftValue = operand->static_value;
    }
    else
    {
        // TODO: fill in the LUT functionality
        // Has to be a LUT
        leftValue = 0;
    }	
    return leftValue;
}

boolean pm_rpm_review_condition(pm_pwr_resource_operation *condition)
{
    boolean condtion_is_true = 0;
    pm_pwr_resource_operand *leftOp;
    unsigned                 leftValue;
    pm_pwr_resource_operand *rightOp;
    unsigned                 rightValue;

    leftOp = condition->left_operand;
    rightOp = condition->right_operand;

    leftValue = pm_rpm_retrieve_operand_value(leftOp);
    rightValue = pm_rpm_retrieve_operand_value(rightOp);

    switch((pm_pwr_resource_operation_type)condition->operation_type)
    {
    case PM_RESOURCE_OPERATION__GT:
        {
            condtion_is_true = leftValue > rightValue;
            break;
        }
    case PM_RESOURCE_OPERATION__GT_EQ:
        {
            condtion_is_true = leftValue >= rightValue;
            break;
        }
    case PM_RESOURCE_OPERATION__LT:
        {
            condtion_is_true = leftValue < rightValue;
            break;
        }
    case PM_RESOURCE_OPERATION__LT_EQ:
        {
            condtion_is_true = leftValue <= rightValue;
            break;
        }
    case PM_RESOURCE_OPERATION__EQ_EQ:
        {
            condtion_is_true = leftValue == rightValue;
            break;
        }
    default:
        {
            condtion_is_true = 0;
            break;
        }
    }

    return condtion_is_true;
}

unsigned pm_rpm_get_component_resource_value(rpm_resource_type resource_type, unsigned resource_index,
                          unsigned npa_key)
{
    unsigned value = 0;
    void* state = NULL;
    rpm_get_aggregated_request_buffer(resource_type, resource_index, (const void**)&state);

    switch(resource_type)
    {
    case RPM_CLK_BUFFER_A_REQ:
    case RPM_CLK_BUFFER_B_REQ:
        {
            switch(npa_key)
            {
            case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_clk_buffer_int_rep *)state)->sw_enable;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_CLK_BUFFER_ENABLE_KEY:
                {
                    value = ((pm_npa_clk_buffer_int_rep *)state)->pc_enable;
                    break;
                }
            default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
    case RPM_VS_A_REQ:
    case RPM_VS_B_REQ:
        {
            switch(npa_key)
            {
            case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_vs_int_rep *)state)->sw_en;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_vs_int_rep *)state)->pc_en;
                    break;
                }
            case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_vs_int_rep *)state)->ip;
                    break;
                }
            default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
    case RPM_LDO_A_REQ:
    case RPM_LDO_B_REQ:
        {
            switch(npa_key)
            {
            case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->sw_en;
                    break;
                }
            case PM_NPA_KEY_LDO_SOFTWARE_MODE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->ldo_sw_mode;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->pc_en;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->pc_mode;
                    break;
                }
            case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->ip;
                    break;
                }
            case PM_NPA_KEY_MICRO_VOLT: 
                {
                    value = ((pm_npa_ldo_int_rep *)state)->input_uvol ;
                    break;
                }
            case PM_NPA_KEY_HEAD_ROOM:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->noise_hr;
                    break;
                }
            case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->byp_allowed;
                    break;
                }
            default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
    case RPM_SMPS_A_REQ:
    case RPM_SMPS_B_REQ:
        {
            switch(npa_key)
            {
            case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->sw_en;
                    break;
                }
            case PM_NPA_KEY_SMPS_SOFTWARE_MODE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->smps_sw_mode;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->pc_en;
                    break;
                }
            case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->pc_mode;
                    break;
                }
            case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_smps_int_rep *)state)->ip;
                    break;
                }
            case PM_NPA_KEY_MICRO_VOLT: 
                {
                    value = ((pm_npa_smps_int_rep *)state)->uvol;
                    break;
                }
            case PM_NPA_KEY_FREQUENCY:
                {
                    value = ((pm_npa_smps_int_rep *)state)->freq;
                    break;
                }
            case PM_NPA_KEY_FREQUENCY_REASON:
                {
                    value = ((pm_npa_smps_int_rep *)state)->freq_reason;
                    break;
                }
            case PM_NPA_KEY_FOLLOW_QUIET_MODE: 
                {
                    value = ((pm_npa_smps_int_rep *)state)->quiet_mode;
                    break;
                }
            case PM_NPA_KEY_HEAD_ROOM:
                {
                    value = ((pm_npa_smps_int_rep *)state)->hr;
                    break;
                }
            case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->byp_allowed;
                    break;
                }
            default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
    }
    return value;
}

int pm_rpm_int_copy(void *source, void *destination, size_t num)
{
    int change_detected = 0;

    change_detected = memcmp(destination, source, num);

    if(change_detected != 0)
    {
        DALSYS_memcpy(destination, source, num);
    }

    return change_detected;
}

boolean pm_rpm_check_vreg_settle_status(uint64 settle_start_time, uint32 estimated_settling_time_us, 
                                        pm_pwr_data_type *pwr_res, pm_comm_info_type *comm_ptr, 
                                        uint8 resource_index, boolean settling_err_en)
{
    boolean vreg_status = FALSE;
    uint64 current_time = 0;
    uint64 settle_end_time = 0;
    uint32 max_settling_time_us = 0;
    uint32 actual_time_us = 0;
    uint8 pmic_index = comm_ptr->pmic_index;
    uint8 periph_type = pwr_res->pwr_specific_info[resource_index].periph_type;

    // calculate the time at which software should end the polling/settling for the rail
    // which is after maximum allowed settling time for the rail has elapsed (10 times the
    // estimated settling time)
    max_settling_time_us = PM_RPM_MAX_SETTLING_TIME_MULTIPLIER * estimated_settling_time_us;
	//If the max settling time is less than 300us , then use minimum 300 us before
	//SW declares a failure /abort
	if(max_settling_time_us <  PM_RPM_MIN_BAIL_OUT_TIME)
	{
	   max_settling_time_us =  PM_RPM_MIN_BAIL_OUT_TIME;
	}
    settle_end_time = settle_start_time + pm_convert_time_to_timetick(max_settling_time_us);

    /* Get the current time and check if the maximum allowed settle time on the rail has elapsed */
    current_time = time_service_now();

    pm_pwr_is_vreg_ready_alg(pwr_res, comm_ptr, resource_index, &vreg_status);
    
    while(vreg_status == FALSE)
    {
        /* Log and abort if the rail has not finished settling after maximum allowed settle time on the rail has elapsed */
        if(current_time > settle_end_time)
        {
            pm_rpm_check_battery_status(&pm_settle_vreg_ok_err_info.batt_status);

            actual_time_us = pm_convert_timetick_to_time(current_time - settle_start_time);

            /* saving the settling info for the error case */
            pm_settle_vreg_ok_err_info.actual_time_us = actual_time_us;
            pm_settle_vreg_ok_err_info.estimated_time_us = estimated_settling_time_us;  
            pm_settle_vreg_ok_err_info.periph_type = periph_type;
            pm_settle_vreg_ok_err_info.periph_index = resource_index;  
            pm_settle_vreg_ok_err_info.pmic_index = pmic_index;

            pm_pwr_status_reg_dump_alg(pwr_res, comm_ptr, resource_index, pm_settle_vreg_ok_err_info.status_reg_dump, PM_NUM_OF_STATUS_REGS);

            PM_LOG_MSG_ERROR(PMIC_RPM_VREG_SETTLING_ERROR, pmic_index, periph_type, resource_index);

            PM_LOG_MSG_ERROR(PMIC_RPM_SETTLING_TIME, PM_LOG_FORMAT_PERIPH_INFO(pmic_index, periph_type, resource_index), 
                             estimated_settling_time_us, actual_time_us);
          
            if(settling_err_en == TRUE)
            {
              CORE_VERIFY(0); // abort 
            }
            break;
        }

        DALSYS_BusyWait(PM_RPM_SETTLING_TIME_POLL_INTERVAL); // us

        /* Get the current time and check if the maximum allowed settle time on the rail has elapsed */
        current_time = time_service_now();

        pm_pwr_is_vreg_ready_alg(pwr_res, comm_ptr, resource_index, &vreg_status);
    }

    actual_time_us = pm_convert_timetick_to_time(current_time - settle_start_time);

    PM_LOG_MSG_DEBUG(PMIC_RPM_SETTLING_TIME, PM_LOG_FORMAT_PERIPH_INFO(pmic_index, periph_type, resource_index), 
                     estimated_settling_time_us, actual_time_us);

    return vreg_status;
}


