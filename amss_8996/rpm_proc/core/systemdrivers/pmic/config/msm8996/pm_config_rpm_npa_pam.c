/*! \file  pm_config_rpm_npa_pam.c
 *  
 *  \brief  File Contains the PMIC NPA CMI Code
 *  \details  This file contains the needed definition and enum for PMIC NPA layer.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: MSM8996V3_pm8004_3p6_04_20_2016 - LOCKED

 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2016 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/config/msm8996/pm_config_rpm_npa_pam.c#14 $ 
$DateTime: 2016/04/21 01:06:47 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                        INCLUDE HEADER FILES

===========================================================================*/

#include "pm_npa_device_clk_buff.h"
#include "pm_npa_device_ldo.h"
#include "pm_npa_device_smps.h"
#include "pm_npa.h"
#include "pm_rpm_npa.h"
#include "pm_rpm_npa_device.h"
/*===========================================================================

                        VARIABLES DEFINITION

===========================================================================*/

/* DDR Client */
static pm_npa_ldo_int_rep
pm_rpm_pam_ddr_ldo2 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
    },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
    },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
};


static pm_npa_ldo_int_rep
pm_rpm_pam_ddr_ldo28 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_ddr_smps1 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_ddr_smps4 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

static pm_npa_smps_int_rep
pm_rpm_pam_ddr_smps12 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

static pm_npa_smps_int_rep
pm_rpm_pam_ddr_smps8 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

pm_npa_pam_client_cfg_type
pm_rpm_pam_ddr_rails_info [] =
{
    {
      (void*)pm_rpm_pam_ddr_ldo2,
      PM_NPA_VREG_LDO
   },
   {
      (void*)pm_rpm_pam_ddr_smps8,
      PM_NPA_VREG_SMPS
   },
   {
      (void*)pm_rpm_pam_ddr_ldo28,
      PM_NPA_VREG_LDO
   },
   {
      (void*)pm_rpm_pam_ddr_smps1,
      PM_NPA_VREG_SMPS
   },
   {
      (void*)pm_rpm_pam_ddr_smps4,
      PM_NPA_VREG_SMPS
   },
    {
      (void*)pm_rpm_pam_ddr_smps12,
      PM_NPA_VREG_SMPS
   },
};


static pm_npa_ldo_int_rep
pm_rpm_pam_ddr2_ldo2 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
    },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
    },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      2, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      1250000, /**< [X uV] -> max aggregation. */
   },
};
static pm_npa_ldo_int_rep
pm_rpm_pam_ddr2_ldo3 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      50, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__NPM, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      3, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      75, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
   },
};
static pm_npa_ldo_int_rep
pm_rpm_pam_ddr2_ldo28 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_LDO__IPEAK, /**< [BYPASS, IPEAK (default), NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      28, /**< Perpherial Index */
      0, /**< Primary (0) or Secondary (1) PMIC */
      0, /**< If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed]*/
      0, /**< reserve 1 - for 32 bit boundary */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      0, /**< [X uV] -> voltage headroom needed. */
      925000, /**< [X uV] -> max aggregation. */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_ddr2_smps1 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_ddr2_smps4 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1800000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

static pm_npa_smps_int_rep
pm_rpm_pam_ddr2_smps12 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_0 */
   /**< Comments: No Activity */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_1 */
   /**< Comments: <=150 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_2 */
   /**< Comments: 151 - 200 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_3 */
   /**< Comments: 201 - 333 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_4 */
   /**< Comments: 334 - 557 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_5 */
   /**< Comments: 558 - 868 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_6 */
   /**< Comments: 869 - 1116 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_7 */
   /**< Comments: 1117 - 1302 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_8 */
   /**< Comments: 1303 - 1600 */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_CFG_9 */
   /**< Comments: 1601 - 1866 (TURBO) */
   {
      PM_NPA_GENERIC_ENABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      1125000, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

pm_npa_pam_client_cfg_type
pm_rpm_pam_ddr2_rails_info [] =
{
   
    {
      (void*)pm_rpm_pam_ddr2_ldo2,
      PM_NPA_VREG_LDO
   },
   {
      (void*)pm_rpm_pam_ddr2_ldo3,
      PM_NPA_VREG_LDO
   },
   {
      (void*)pm_rpm_pam_ddr2_ldo28,
      PM_NPA_VREG_LDO
   },
   {
      (void*)pm_rpm_pam_ddr2_smps1,
      PM_NPA_VREG_SMPS
   },
   {
      (void*)pm_rpm_pam_ddr2_smps4,
      PM_NPA_VREG_SMPS
   },
    {
      (void*)pm_rpm_pam_ddr2_smps12,
      PM_NPA_VREG_SMPS
   },
};

