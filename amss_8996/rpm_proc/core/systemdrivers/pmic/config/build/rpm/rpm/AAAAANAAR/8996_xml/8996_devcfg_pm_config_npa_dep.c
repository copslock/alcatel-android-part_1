#include "devcfg_pm_config_target.h"
#include "devcfg_pm_config_common.h"
/*! \file
 *  
 *  \brief  pm_config_npa_dep.c ----This file contains initialization functions for NPA Device layer
 *  \details This file contains initialization functions for NPA Device layer and
 *          the node and resource definition implementations
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation NPA Client Version: MSM8994_baseline_0p8 NEW - Approved
 *    This file contains code for Target specific settings and modes.
 * 
 *  &copy; Copyright 2014 - 2015 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/pmic/config/msm8996/pm_config_npa_dep.c#6 $ 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_npa.h"

// Component Resource Structure Creation
extern pm_pwr_resource smps_a[];
pm_pwr_resource
smps_a[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_SMPS_A_REQ, 1},
   {RPM_SMPS_A_REQ, 2},
   {RPM_SMPS_A_REQ, 3},
   {RPM_SMPS_A_REQ, 4},
   {RPM_SMPS_A_REQ, 5},
   {RPM_SMPS_A_REQ, 6},
   {RPM_SMPS_A_REQ, 7},
   {RPM_SMPS_A_REQ, 8},
   {RPM_SMPS_A_REQ, 9},
   {RPM_SMPS_A_REQ, 10},
   {RPM_SMPS_A_REQ, 11},
   {RPM_SMPS_A_REQ, 12},
};
static pm_pwr_resource
smps_b[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_SMPS_B_REQ, 1},
   {RPM_SMPS_B_REQ, 2},
   {RPM_SMPS_B_REQ, 3},
};

static pm_pwr_resource
smps_c[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_SMPS_C_REQ, 1},
   {RPM_SMPS_C_REQ, 2},
   {RPM_SMPS_C_REQ, 3},
   {RPM_SMPS_C_REQ, 4},
   {RPM_SMPS_C_REQ, 5},
};


// Component Resource Structure Creation
static pm_pwr_resource
boost_byp_b[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_BOOST_BYP_B_REQ, 1},
};
static pm_pwr_resource
ldo_a[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_LDO_A_REQ, 1},
   {RPM_LDO_A_REQ, 2},
   {RPM_LDO_A_REQ, 3},
   {RPM_LDO_A_REQ, 4},
   {RPM_LDO_A_REQ, 5},
   {RPM_LDO_A_REQ, 6},
   {RPM_LDO_A_REQ, 7},
   {RPM_LDO_A_REQ, 8},
   {RPM_LDO_A_REQ, 9},
   {RPM_LDO_A_REQ, 10},
   {RPM_LDO_A_REQ, 11},
   {RPM_LDO_A_REQ, 12},
   {RPM_LDO_A_REQ, 13},
   {RPM_LDO_A_REQ, 14},
   {RPM_LDO_A_REQ, 15},
   {RPM_LDO_A_REQ, 16},
   {RPM_LDO_A_REQ, 17},
   {RPM_LDO_A_REQ, 18},
   {RPM_LDO_A_REQ, 19},
   {RPM_LDO_A_REQ, 20},
   {RPM_LDO_A_REQ, 21},
   {RPM_LDO_A_REQ, 22},
   {RPM_LDO_A_REQ, 23},
   {RPM_LDO_A_REQ, 24},
   {RPM_LDO_A_REQ, 25},
   {RPM_LDO_A_REQ, 26},
   {RPM_LDO_A_REQ, 27},
   {RPM_LDO_A_REQ, 28},
   {RPM_LDO_A_REQ, 29},
   {RPM_LDO_A_REQ, 30},
   {RPM_LDO_A_REQ, 31},
   {RPM_LDO_A_REQ, 32},
};

static pm_pwr_resource
ldo_c[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_LDO_C_REQ, 1},
};

static pm_pwr_resource
vs_a[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_VS_A_REQ, 1},
   {RPM_VS_A_REQ, 2},
};
static pm_pwr_resource
clk_buff_a[] =
{
   {(rpm_resource_type)0, 0}, // this is invalid place holder
   {RPM_CLK_BUFFER_A_REQ, 1},
   {RPM_CLK_BUFFER_A_REQ, 2},
   {RPM_CLK_BUFFER_A_REQ, 3},
   {RPM_CLK_BUFFER_A_REQ, 4},
   {RPM_CLK_BUFFER_A_REQ, 5},
   {RPM_CLK_BUFFER_A_REQ, 6},
   {RPM_CLK_BUFFER_A_REQ, 7},
   {RPM_CLK_BUFFER_A_REQ, 8},
   {RPM_CLK_BUFFER_A_REQ, 9},
   {RPM_CLK_BUFFER_A_REQ, 10},
   {RPM_CLK_BUFFER_A_REQ, 11},
   {RPM_CLK_BUFFER_A_REQ, 12},
   {RPM_CLK_BUFFER_A_REQ, 13},
};

// Component Resource Child Depend Structure Creation
static pm_pwr_resource *
smps3_a_child_dep[] =
{
   &ldo_a[2],
   &ldo_a[3],
   &ldo_a[4],
   &ldo_a[11],
   &ldo_a[25],
   &ldo_a[26],
   &ldo_a[27],
   &ldo_a[28],
   &ldo_a[31],
};

static pm_pwr_resource *
smps1_c_child_dep[] =
{
   &ldo_c[1],
};

static pm_pwr_resource *
smps4_a_child_dep[] =
{
   &vs_a[1],
   &vs_a[2],
};

static pm_pwr_resource *
smps5_a_child_dep[] =
{
   &ldo_a[5],
   &ldo_a[6],
   &ldo_a[7],
   &ldo_a[12],
   &ldo_a[14],
   &ldo_a[15],
   &ldo_a[32],
};

static pm_pwr_resource *
ldo5_a_child_dep[] =
{
   &clk_buff_a[4],
   &clk_buff_a[5],
};

static pm_pwr_resource *
ldo12_a_child_dep[] =
{
   &clk_buff_a[1],
   &clk_buff_a[2],
   &clk_buff_a[8],
   &clk_buff_a[10],
   &clk_buff_a[11],
   &clk_buff_a[12],
   &clk_buff_a[13],
};

static pm_pwr_resource *
smps1_b_child_dep[] =
{
   &ldo_a[1],
};

static pm_pwr_resource *
boost_byp1_b_child_dep[] =
{
   &ldo_a[9],
   &ldo_a[10],
   &ldo_a[13],
   &ldo_a[17],
   &ldo_a[18],
   &ldo_a[19],
   &ldo_a[20],
   &ldo_a[21],
   &ldo_a[22],
   &ldo_a[23],
   &ldo_a[24],
   &ldo_a[29],
};


// Component Resource Dependency Information
pm_pwr_resource_dependency_info
smps_dep_a[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // S1
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S2
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S3
   {
      NULL, // parent resource
      smps3_a_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // S4
   {
      NULL, // parent resource
      smps4_a_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // S5
   {
      NULL, // parent resource
      smps5_a_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // S6
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S7
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S8
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S9
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S10
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S11
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S12
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};
pm_pwr_resource_dependency_info
smps_dep_b[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // S1
   {
      NULL, // parent resource
      smps1_b_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // S2
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S3
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};

pm_pwr_resource_dependency_info
smps_dep_c[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // S1
   {
      NULL, // parent resource
      smps1_c_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // S2
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S3
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S4
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // S5
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};

pm_pwr_resource_dependency_info
boost_dep_b[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // BOOST
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};
pm_pwr_resource_dependency_info
boost_byp_dep_b[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // B1
   {
      NULL, // parent resource
      boost_byp1_b_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
};
pm_pwr_resource_dependency_info
ldo_dep_a[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // L1
   {
      &smps_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L2
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L3
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L4
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L5
   {
      &smps_a[5],
      ldo5_a_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // L6
   {
      &smps_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L7
   {
      &smps_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L8
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L9
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L10
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L11
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L12
   {
      &smps_a[5],
      ldo12_a_child_dep, // child resource dependencies
      0,  // parent internal client handle
   },
   // L13
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L14
   {
      &smps_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L15
   {
      &smps_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L16
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L17
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L18
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L19
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L20
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L21
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L22
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L23
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L24
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L25
   {
      &smps_a[3],// parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L26
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L27
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L28
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L29
   {
      &boost_byp_b[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L30
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L31
   {
      &smps_a[3],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // L32
   {
      &smps_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};
pm_pwr_resource_dependency_info
ldo_dep_b[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // L1
   {
      NULL,
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};

pm_pwr_resource_dependency_info
ldo_dep_c[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // L1
   {
      &smps_c[1],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};


pm_pwr_resource_dependency_info
vs_dep_a[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // V1
   {
      &smps_a[4],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // V2
   {
      &smps_a[4],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};
pm_pwr_resource_dependency_info
clk_dep_a[] =
{
   { NULL, NULL, 0, }, // Invalid zeroth array - NOT USED
   // C1
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C2
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C3
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C4
   {
      &ldo_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C5
   {
      &ldo_a[5],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C6
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C7
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C8
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C9
   {
      NULL, // parent resource
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C10
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C11
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C12
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
   // C13
   {
      &ldo_a[12],
      NULL, // child resource dependencies
      0,  // parent internal client handle
   },
};


pm_pwr_resource_dependency_info* 
smps_dep[] = 
{
    smps_dep_a, 
    smps_dep_b,
    smps_dep_c,
    NULL
};

pm_pwr_resource_dependency_info* 
ldo_dep[] = 
{
    ldo_dep_a,
    ldo_dep_b,
    ldo_dep_c,
    NULL
};
pm_pwr_resource_dependency_info* 
vs_dep[] = 
{
    vs_dep_a,
    NULL,
    NULL,
    NULL
};

pm_pwr_resource_dependency_info* 
boost_dep[] = 
{
    NULL, 
    boost_dep_b,
    NULL,
    NULL
};

pm_pwr_resource_dependency_info* 
boost_byp_dep[] = 
{
    NULL, 
    boost_byp_dep_b,
    NULL,
    NULL
};

pm_pwr_resource_dependency_info* 
clk_dep[] = 
{
    clk_dep_a,
    NULL,
    NULL,
    NULL 
};




