/*
==============================================================================

FILE:         HALclkAGGRE2NOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   AGGRE2NOC clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mGCCAGGRE2NOCPowerDomain



==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkAGGRE2NOC.c#2 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/08/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_Aggre2NOCPowerDomainEnable( HAL_clk_PowerDomainDescType *pmPowerDomainDesc,  boolean bAsync );
void HAL_clk_Aggre2NOCPowerDomainDisable( HAL_clk_PowerDomainDescType *pmPowerDomainDesc );


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;
extern void HAL_clk_GenericPowerDomainEnable( HAL_clk_PowerDomainDescType *pmPowerDomainDesc, boolean bAsync );
extern void HAL_clk_GenericPowerDomainDisable( HAL_clk_PowerDomainDescType *pmPowerDomainDesc );

/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mAggre2NOCPowerDomainControl
 *
 * Functions for controlling Aggre2 NOC power domain functions.
 */
HAL_clk_PowerDomainControlType HAL_clk_mAggre2NOCPowerDomainControl =
{
   /* .Enable     = */ HAL_clk_Aggre2NOCPowerDomainEnable,
   /* .Disable    = */ HAL_clk_Aggre2NOCPowerDomainDisable,
   /* .IsEnabled  = */ HAL_clk_GenericPowerDomainIsEnabled
};


/*
 * HAL_clk_mGCCAGGRE2NOCPowerDomain
 *
 * AGGRE2_NOC power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCAGGRE2NOCPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_AGGRE2_NOC",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_AGGRE2_NOC_GDSCR),
  /* .pmControl               = */ &HAL_clk_mAggre2NOCPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};


/* ===========================================================================
**  HAL_clk_Aggre2NOCPowerDomainEnable
** ======================================================================== */
void HAL_clk_Aggre2NOCPowerDomainEnable
(
  HAL_clk_PowerDomainDescType *pmPowerDomainDesc, 
  boolean                     bAsync
)
{

  HAL_clk_GenericPowerDomainEnable( pmPowerDomainDesc, bAsync );
  HWIO_OUTF(GCC_AGGRE2_NOC_BCR, BLK_ARES, 0 );
} /* END HAL_clk_Aggre2NOCPowerDomainEnable */


/* ===========================================================================
**  HAL_clk_Aggre2NOCPowerDomainDisable
** ======================================================================== */

void HAL_clk_Aggre2NOCPowerDomainDisable
(
  HAL_clk_PowerDomainDescType *pmPowerDomainDesc
)
{
  HWIO_OUTF(GCC_AGGRE2_NOC_BCR, BLK_ARES, 1 );
  HAL_clk_GenericPowerDomainDisable (pmPowerDomainDesc);
} /* END HAL_clk_Aggre2NOCPowerDomainDisable */
