/*
==============================================================================

FILE:         HALclkMSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MSS clocks.

   List of clock domains:
   -HAL_clk_mGCCMSSQ6BIMCAXIClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/GCC/HALclkMSS.c#2 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
10/21/2013            Auto-generated.

==============================================================================
            Copyright (c) 2013 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCNoRootClockDomainControl;

/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mMSSQ6BIMCAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMSSQ6BIMCAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_mss_q6_bimc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MSS_Q6_BIMC_AXI_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MSS_Q6_BIMC_AXI_CLK
  },
};


/*
 * HAL_clk_mGCCMSSQ6BIMCAXIClkDomain
 *
 * MSS Q6 BIMC AXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCMSSQ6BIMCAXIClkDomain =
{
  /* .nCGRAddr             = */ 0,
  /* .pmClocks             = */ HAL_clk_mMSSQ6BIMCAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMSSQ6BIMCAXIClkDomainClks)/sizeof(HAL_clk_mMSSQ6BIMCAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCNoRootClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


