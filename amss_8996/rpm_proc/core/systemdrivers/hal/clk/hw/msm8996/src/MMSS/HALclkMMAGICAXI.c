/*
==============================================================================

FILE:         HALclkMMAGICAXI.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICAXI clocks.

   List of clock domains:
     - HAL_clk_mMMSSAXIClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/MMSS/HALclkMMAGICAXI.c#1 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_MMAXIEnable
(
  HAL_clk_ClockDescType *pmClockDesc
);

void HAL_clk_MMAXIDisable
(
  HAL_clk_ClockDescType *pmClockDesc
);


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/
/*
 * HAL_clk_mMMAXIClockControl
 *
 * Functions for controlling MM AXI clocks functions.
 */
HAL_clk_ClockControlType HAL_clk_mMMAXIClockControl =
{
  /* .Enable           = */ HAL_clk_MMAXIEnable,
  /* .Disable          = */ HAL_clk_MMAXIDisable,
  /* .IsEnabled        = */ HAL_clk_GenericIsEnabled,
  /* .IsOn             = */ HAL_clk_GenericIsOn,
  /* .Reset            = */ HAL_clk_GenericReset,
  /* .Config           = */ NULL,
  /* .DetectConfig     = */ NULL,
  /* .ConfigDivider    = */ HAL_clk_GenericConfigDivider,
  /* .DetectDivider    = */ HAL_clk_GenericDetectDivider,
  /* .ConfigFootswitch = */ HAL_clk_GenericConfigFootswitch,
};


/*                           
 *  HAL_clk_mAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mmss_mmagic_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MMAGIC_AXI_CBCR), HWIO_OFFS(MMSS_MMAGICAXI_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mMMAXIClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MMAGIC_AXI_CLK
  },
};


/*
 * HAL_clk_mMMSSAXIClkDomain
 *
 * AXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_AXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mAXIClkDomainClks)/sizeof(HAL_clk_mAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/* ===========================================================================
**  HAL_clk_MMAXIEnable
**
** ======================================================================== */
void HAL_clk_MMAXIEnable
(
  HAL_clk_ClockDescType *pmClockDesc
)
{
  HWIO_OUTF( MMSS_MMAGIC_BIMC_AXI_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF( MMSS_MMSS_MMAGIC_AXI_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF( MMSS_MMSS_S0_AXI_CBCR, CLK_ENABLE, 1);
  HAL_clk_BusyWait( 10 );

} /* HAL_clk_MMAXIEnable */


/* ===========================================================================
**  HAL_clk_MMAXIDisable
**
** ======================================================================== */
void HAL_clk_MMAXIDisable
(
  HAL_clk_ClockDescType *pmClockDesc
)
{
  /* Disable MM AXI clocks
   */
  HWIO_OUTF( MMSS_MMAGIC_BIMC_AXI_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF( MMSS_MMSS_MMAGIC_AXI_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF( MMSS_MMSS_S0_AXI_CBCR, CLK_ENABLE, 0);
  HAL_clk_BusyWait( 10 );
} /* HAL_clk_MMAXIDisable */
