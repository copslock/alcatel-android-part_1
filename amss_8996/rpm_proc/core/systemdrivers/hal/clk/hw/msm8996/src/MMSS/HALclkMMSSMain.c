/*
==============================================================================

FILE:         HALclkMMSSMain.c

DESCRIPTION:
   The main auto-generated file for MMSS.


==============================================================================

$Header: //components/rel/rpm.bf/1.6/core/systemdrivers/hal/clk/hw/msm8996/src/MMSS/HALclkMMSSMain.c#1 $

==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/*
 * Clock domains
 */
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSAXIClkDomain;

/*
 * Clock power domains
 */
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICBIMCPowerDomain;

/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aMMSSSourceMap
 *
 * MMSS HW source mapping
 * 
 */
static HAL_clk_SourceMapType aMMSSSourceMap[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_MMPLL0,             1 },
  { HAL_CLK_SOURCE_MMPLL1,             2 },
  { HAL_CLK_SOURCE_MMPLL4,             3 },
  { HAL_CLK_SOURCE_SLEEPCLK,           4 },
  { HAL_CLK_SOURCE_GPLL0,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap
};


/*
 * HAL_clk_aMMSSClockDomainDesc
 *
 * List of MMSS clock domains
*/
static HAL_clk_ClockDomainDescType * HAL_clk_aMMSSClockDomainDesc [] =
{
  &HAL_clk_mMMSSAXIClkDomain,
  NULL
};


/*
 * HAL_clk_aMMSSPowerDomainDesc
 *
 * List of MMSS power domains
 */
static HAL_clk_PowerDomainDescType * HAL_clk_aMMSSPowerDomainDesc [] =
{
  &HAL_clk_mMMSSMMAGICBIMCPowerDomain,
  NULL
};



/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitMMSSMain
**
** ======================================================================== */

void HAL_clk_PlatformInitMMSSMain (void)
{

  /*
   * Install all clock domains
   */
  HAL_clk_InstallClockDomains(HAL_clk_aMMSSClockDomainDesc, MMSS_BASE);

  /*
   * Install all power domains
   */
  HAL_clk_InstallPowerDomains(HAL_clk_aMMSSPowerDomainDesc, MMSS_BASE);

} /* END HAL_clk_PlatformInitMMSSMain */

