#ifndef PM_RAILWAY_H
#define PM_RAILWAY_H

/*! \file pm_railway.h
 *  \n
 *  \brief Contains PMIC Railway public interfaces.
 *  \n
 *  \n &copy; Copyright (c) 2013-2014 QUALCOMM Technologies Incorporated, All Rights Reserved.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/1.6/core/api/systemdrivers/pmic/pm_railway.h#2 $
  $DateTime: 2015/07/07 19:51:15 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/29/14   vtw     Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pmapp_npa.h"

/*===========================================================================

                        DEFINITIONS

===========================================================================*/

typedef enum
{
  PM_GRID_REV_0,
  PM_GRID_REV_1,
  PM_GRID_REV_2,
  PM_GRID_REV_MAX,
}pm_grid_revision_type;

/*===========================================================================

FUNCTION pm_get_grid_revision

DESCRIPTION
    This function returns the power grid rev on the target

INPUT PARAMETERS
  NONE

RETURN VALUE
  power_grid_rev - power grid revision num  

DEPENDENCIES 
  NONE 

===========================================================================*/
void pm_get_grid_revision(uint32 *power_grid_rev);

/**
*
* @brief Sets SW mode transition for a core rail.
* @param rail The core rail ID:
* @enum PM_RAILWAY_MX
* @enum PM_RAILWAY_CX
* @enum PM_RAILWAY_GFX
* @enum PM_RAILWAY_EBI
*
* @param mode The SW mode being configured.
* @enum PM_SW_MODE_LPM -- Low Power Mode.
* @enum PM_SW_MODE_BYPASS -- Bypass.
* @enum PM_SW_MODE_AUTO -- Auto mode.
* @enum PM_SW_MODE_NPM -- Normal Power Mode
*
* @note This API is supposed to be used by the Railway only.
*
* @return pmic error flag
*/
unsigned pm_railway_set_mode(pm_railway_type_info_type rail, pm_sw_mode_type  mode);

/**
*
* @brief Sets voltage for a core rail.
* @param rail The core rail ID:
* @enum PM_RAILWAY_MX
* @enum PM_RAILWAY_CX
* @enum PM_RAILWAY_GFX
* @enum PM_RAILWAY_EBI
*
* @param voltage_uv The voltage being configured in micro-volt.
*
* @note This API is supposed to be used by the Railway only.
*
* @return pmic error flag
*/
unsigned pm_railway_set_voltage(pm_railway_type_info_type rail, unsigned voltage_uv);

/**
*
* @brief Calculate vset value from voltage rail value.
* @param rail The core rail ID:
* @enum PM_RAILWAY_MX
* @enum PM_RAILWAY_CX
* @enum PM_RAILWAY_GFX
* @enum PM_RAILWAY_EBI
*
* @param voltage_uv The voltage value in micro-volt.
*
* @param vset Pointer to the new vset value.
*
* @note This API is supposed to be used by the Railway only.
*
* @return pmic error flag
*/
pm_err_flag_type pm_railway_calculate_vset(pm_railway_type_info_type rail , unsigned voltage_uv ,uint32*  vset);

/**
*
* @brief Returns the current voltage of core rail.
* @param rail The core rail ID:
* @enum PM_RAILWAY_MX
* @enum PM_RAILWAY_CX
* @enum PM_RAILWAY_GFX
* @enum PM_RAILWAY_EBI
*
* @param vset Pointer to current voltage value.
*
* @note
*
* @return pmic error flag
*/
pm_err_flag_type pm_railway_get_voltage(pm_railway_type_info_type rail, pm_volt_level_type * voltage_uv);

/**
*
* @brief Sets the number of phases that the autonomous phase controller (APC) can use on the core rails.
* @param rail The core rail ID:
* @enum PM_RAILWAY_MX
* @enum PM_RAILWAY_CX
* @enum PM_RAILWAY_GFX
* @enum PM_RAILWAY_EBI
*
* @param phase The max number of phases:
* @enum PM_PHASE_CNT__1 -- 1 phase
* @enum PM_PHASE_CNT__2 -- 2 phases
* @enum PM_PHASE_CNT__3 -- 3 phases
* @enum PM_PHASE_CNT__4 -- 4 phases
*
* @note
*
* @return pmic error flag
*/
unsigned pm_railway_set_phase(pm_railway_type_info_type rail, pm_phase_cnt_type phase);

/**
*
* @brief Votes for certain SMPSs in/out of auto mode.
* @param ddr_cfg an entry that is maintained in a ddr freq- mode table 
* @note The implementation of this API is target specific.
*
* @return pmic error flag
*/
pm_err_flag_type pm_npa_rpm_smps_auto_mode_config (uint32  ddr_cfg);
/**
*
* @brief Verfies if certain SMPS regulators are in the right SW mode.
* @param ddr_cfg an entry that is maintained in a ddr freq- mode table 
* @note The implementation of this API is target specific.
*
* @return pmic error flag
*/
pm_err_flag_type pm_npa_rpm_verify_smps_mode(uint32  ddr_cfg);

#endif // PM_RAILWAY_H

