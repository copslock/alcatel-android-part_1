#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/1.6/core/api/systemdrivers/hwio/msm8996/msmhwiobase.h#7 $
  $DateTime: 2015/07/20 17:54:27 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0x00000000
#define RPM_BASE_SIZE                                               0x00100000
#define RPM_BASE_PHYS                                               0x00000000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_START_ADDRESS_BASE                             0x00200000
#define RPM_CODE_RAM_START_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_CODE_RAM_START_ADDRESS_BASE_PHYS                        0x00200000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_END_ADDRESS_BASE                               0x00227fff
#define RPM_CODE_RAM_END_ADDRESS_BASE_SIZE                          0x00000000
#define RPM_CODE_RAM_END_ADDRESS_BASE_PHYS                          0x00227fff

/*----------------------------------------------------------------------------
 * BASE: SPDM_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                       0x60048000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                  0x00008000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                  0x60048000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0x60068000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00000000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x60068000

/*----------------------------------------------------------------------------
 * BASE: RPM_MSG_RAM
 *--------------------------------------------------------------------------*/

#define RPM_MSG_RAM_BASE                                            0x60068000
#define RPM_MSG_RAM_BASE_SIZE                                       0x00006000
#define RPM_MSG_RAM_BASE_PHYS                                       0x60068000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE                             0x6006dfff
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                        0x6006dfff

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0x60070000
#define SECURITY_CONTROL_BASE_SIZE                                  0x00010000
#define SECURITY_CONTROL_BASE_PHYS                                  0x60070000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0x60300000
#define CLK_CTL_BASE_SIZE                                           0x000a0000
#define CLK_CTL_BASE_PHYS                                           0x60300000

/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/

#define BIMC_BASE                                                   0x60400000
#define BIMC_BASE_SIZE                                              0x00080000
#define BIMC_BASE_PHYS                                              0x60400000

/*----------------------------------------------------------------------------
 * BASE: DDR_SS
 *--------------------------------------------------------------------------*/

#define DDR_SS_BASE                                                 0x60480000
#define DDR_SS_BASE_SIZE                                            0x00020000
#define DDR_SS_BASE_PHYS                                            0x60480000

/*----------------------------------------------------------------------------
 * BASE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                               0x604a0000
#define MPM2_MPM_BASE_SIZE                                          0x00010000
#define MPM2_MPM_BASE_PHYS                                          0x604a0000

/*----------------------------------------------------------------------------
 * BASE: CONFIG_NOC
 *--------------------------------------------------------------------------*/

#define CONFIG_NOC_BASE                                             0x60500000
#define CONFIG_NOC_BASE_SIZE                                        0x00000080
#define CONFIG_NOC_BASE_PHYS                                        0x60500000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0x60520000
#define SYSTEM_NOC_BASE_SIZE                                        0x0000a133
#define SYSTEM_NOC_BASE_PHYS                                        0x60520000

/*----------------------------------------------------------------------------
 * BASE: A0_NOC_AGGRE0_NOC
 *--------------------------------------------------------------------------*/

#define A0_NOC_AGGRE0_NOC_BASE                                      0x60540000
#define A0_NOC_AGGRE0_NOC_BASE_SIZE                                 0x00005133
#define A0_NOC_AGGRE0_NOC_BASE_PHYS                                 0x60540000

/*----------------------------------------------------------------------------
 * BASE: A1_NOC_AGGRE1_NOC
 *--------------------------------------------------------------------------*/

#define A1_NOC_AGGRE1_NOC_BASE                                      0x60560000
#define A1_NOC_AGGRE1_NOC_BASE_SIZE                                 0x00003133
#define A1_NOC_AGGRE1_NOC_BASE_PHYS                                 0x60560000

/*----------------------------------------------------------------------------
 * BASE: A2_NOC_AGGRE2_NOC
 *--------------------------------------------------------------------------*/

#define A2_NOC_AGGRE2_NOC_BASE                                      0x60580000
#define A2_NOC_AGGRE2_NOC_BASE_SIZE                                 0x00008133
#define A2_NOC_AGGRE2_NOC_BASE_PHYS                                 0x60580000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_NOC
 *--------------------------------------------------------------------------*/

#define PERIPH_NOC_BASE                                             0x605c0000
#define PERIPH_NOC_BASE_SIZE                                        0x00002480
#define PERIPH_NOC_BASE_PHYS                                        0x605c0000

/*----------------------------------------------------------------------------
 * BASE: CRYPTO0_CRYPTO_TOP
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                     0x60640000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                0x00040000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                0x60640000

/*----------------------------------------------------------------------------
 * BASE: IPA_0_IPA_WRAPPER
 *--------------------------------------------------------------------------*/

#define IPA_0_IPA_WRAPPER_BASE                                      0x60680000
#define IPA_0_IPA_WRAPPER_BASE_SIZE                                 0x00080000
#define IPA_0_IPA_WRAPPER_BASE_PHYS                                 0x60680000

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0x60700000
#define CORE_TOP_CSR_BASE_SIZE                                      0x00100000
#define CORE_TOP_CSR_BASE_PHYS                                      0x60700000

/*----------------------------------------------------------------------------
 * BASE: MMSS
 *--------------------------------------------------------------------------*/

#define MMSS_BASE                                                   0x60800000
#define MMSS_BASE_SIZE                                              0x00800000
#define MMSS_BASE_PHYS                                              0x60800000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0x61000000
#define TLMM_BASE_SIZE                                              0x00319999
#define TLMM_BASE_PHYS                                              0x61000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_QDSS_APB
 *--------------------------------------------------------------------------*/

#define QDSS_QDSS_APB_BASE                                          0x63000000
#define QDSS_QDSS_APB_BASE_SIZE                                     0x00800000
#define QDSS_QDSS_APB_BASE_PHYS                                     0x63000000

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0x64000000
#define PMIC_ARB_BASE_SIZE                                          0x01c00000
#define PMIC_ARB_BASE_PHYS                                          0x64000000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_BASE                                              0x67400000
#define PERIPH_SS_BASE_SIZE                                         0x00910000
#define PERIPH_SS_BASE_PHYS                                         0x67400000

/*----------------------------------------------------------------------------
 * BASE: LPASS
 *--------------------------------------------------------------------------*/

#define LPASS_BASE                                                  0x69000000
#define LPASS_BASE_SIZE                                             0x00800000
#define LPASS_BASE_PHYS                                             0x69000000


#endif /* __MSMHWIOBASE_H__ */
