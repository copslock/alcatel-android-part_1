/**
 * @file ddr_automode.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/msm8996/ddr_automode.c#7 $
$DateTime: 2015/10/26 12:51:59 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
06/10/14   tw      Initial version.
================================================================================
                   Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_params.h"
#include "ddr_log.h"
#include "npa.h"
#include "railway.h"
#include "pm_railway.h"
#include "ddr_automode.h"

/*==============================================================================
                                  MACROS
==============================================================================*/

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */
/* global flag to turn on and off auto mode*/
boolean ddr_automode_en = TRUE;

/* structure to contain automode enums and thresholds */
#define DDR_AUTOMODE_MAX 0x7FFFFFFF

ddr_automode ddr_automode_cfg[] =
{
  {000000,  PMIC_NPA_MODE_ID_DDR_CFG_0},
  {150000,  PMIC_NPA_MODE_ID_DDR_CFG_1},	
  {200000,  PMIC_NPA_MODE_ID_DDR_CFG_2},
  {333000,  PMIC_NPA_MODE_ID_DDR_CFG_3},
  {557000,  PMIC_NPA_MODE_ID_DDR_CFG_4},
  {700000,  PMIC_NPA_MODE_ID_DDR_CFG_5}, // was 868000
  {1116000, PMIC_NPA_MODE_ID_DDR_CFG_6},
  {1302000, PMIC_NPA_MODE_ID_DDR_CFG_7},
  {1600000, PMIC_NPA_MODE_ID_DDR_CFG_8},
  {1866000, PMIC_NPA_MODE_ID_DDR_CFG_9},
  {DDR_AUTOMODE_MAX, DDR_AUTOMODE_MAX},
};
/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

/* =============================================================================
**  Function : ddr_automode_init
** =============================================================================
*/
/**
*   @brief
*   Function called to initialize automode settings
*
*   @param[in]  None
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_automode_init(void)
{
//  if(!ddr_automode_en)
//  {
    /* force pmic enum to max to ensure we don't operate in automode for
       high frequency cases when dynamic auto mode is disabled
    */
    /* Apply pmic automode setting */
    pm_npa_rpm_smps_auto_mode_config(PMIC_NPA_MODE_ID_DDR_CFG_9);

    /* perform sanity check to ensure pmic setting is what we expected */
    pm_npa_rpm_verify_smps_mode(PMIC_NPA_MODE_ID_DDR_CFG_9);    
//  }
}

/* =============================================================================
**  Function : ddr_automode_toggle
** =============================================================================
*/
/**
*   @brief
*   Function called to update automode settings
*
*   @param[in]  clk_speed_in_khz  Current clock speed (in KHz)
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_automode_toggle(uint32 clk_speed_in_khz)
{
  uint8 i = 0;
  
  if(ddr_automode_en)
  {
    while(ddr_automode_cfg[i].freq != DDR_AUTOMODE_MAX)
    {
      if(clk_speed_in_khz <= ddr_automode_cfg[i].freq)
      {
        /* Apply pmic automode setting */
        pm_npa_rpm_smps_auto_mode_config(ddr_automode_cfg[i].pmic_enum);

        /* perform sanity check to ensure pmic setting is what we expected */
        pm_npa_rpm_verify_smps_mode(ddr_automode_cfg[i].pmic_enum);
        
        break;
      }
      i++;
    }  
  }
} /* ddr_automode_toggle */

