/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/hw_sequence/ddrss/src/ddrss_common.c#20 $
$DateTime: 2016/04/20 01:18:00 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/11/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"

extern uint8 dq_dbi_bit;
extern uint8 dq_spare_bit;
extern uint8 connected_bit_mapping_CA [PINS_PER_PHY_CONNECTED_CA];

/*Freq ranges:               250000,    350000,    600000,    800000,    1066000,   1488000,   1603000,   1894000. */
uint32 PRFS_index_table[] = {F_RANGE_0, F_RANGE_1, F_RANGE_2, F_RANGE_3, F_RANGE_4, F_RANGE_5, F_RANGE_6, F_RANGE_7};

//================================================================================================//
// Get an index based on Frequency provided.
//================================================================================================//
uint8 DDRSS_Get_Freq_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 prfs_index;

   for (prfs_index = 0; (prfs_index < sizeof(PRFS_index_table)/sizeof(uint32)); prfs_index++)
   {
      if (clk_freq_khz < PRFS_index_table[prfs_index])
         break;
   }

   return prfs_index;
}


//================================================================================================//
// Get the flat 32-bit system address to which training patterns will be written to and read from.
// Adjust address for execution from RPM if needed.
// ===============================================================================================//
void DDRSS_Get_Training_Address(DDR_STRUCT *ddr)
{

    // Adjust if needed for RPM view of system DRAM address map.
    training_address[0][0] = (ddr->ddr_size_info.ddr0_cs0_remapped_addr) + TRAINING_BASE_ADDRESS_OFFSET;

    training_address[0][1] = (ddr->ddr_size_info.ddr0_cs1_remapped_addr) + TRAINING_BASE_ADDRESS_OFFSET;

    training_address[1][0] = (ddr->ddr_size_info.ddr1_cs0_remapped_addr + TRAINING_BASE_ADDRESS_OFFSET +
                             ((ddr->cdt_params[0].common.interleave_en & DDR_CS0_INTERLEAVE) ? TRAINING_INTERLEAVE_ADDRESS_OFFSET : 0));

    training_address[1][1] = (ddr->ddr_size_info.ddr1_cs1_remapped_addr + TRAINING_BASE_ADDRESS_OFFSET +
                             ((ddr->cdt_params[0].common.interleave_en & DDR_CS1_INTERLEAVE) ? TRAINING_INTERLEAVE_ADDRESS_OFFSET : 0));

}

//================================================================================================//
// Memory read from DRAM.
// This function will read data of a given size to a given address supplied by the caller.
//================================================================================================//
void ddr_mem_copy(uint32 * source, uint32 * destination, uint32 size, uint32 burst)
{
    uint32 i;

    if(burst)
    {
        /* perform a burst write */
        #ifndef CDVI_SVTB
        __blocksCopy(source, destination, size);
        #endif
    }
    else
    {
        for(i = 0; i < size; i++)
        {
            destination[i] = source[i];
        }
    }
}



//================================================================================================//
// Process coarse histogram to find horizon eye
//================================================================================================//
void DDRSS_Post_Histogram_Coarse_Horizon_Eye (best_eye_struct *best_eye_ptr,
                                              uint8 (*coarse_fail_count_ptr)[COARSE_VREF][COARSE_CDC],
                                              training_data *training_data_ptr,
                                              training_params_t *training_params_ptr,
                                              uint8 training_type,  /* 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                              uint8 phy_inx,
                                              uint32 clk_freq_khz
                                             )
{
   uint8       left_boundary_eye_cdc_value = 0xFF;
   uint8      right_boundary_eye_cdc_value = 0xFF;
   uint8  temp_left_boundary_eye_cdc_value = 0xFF;
   uint8 temp_right_boundary_eye_cdc_value = 0xFF;

   uint8                   first_pass_flag = 0;
   uint8                         cdc_value = 0;
   uint8                        vref_value = 0;
   uint8                         cdc_index = 0;
   uint8                        vref_index = 0;
   uint8            max_eye_width_all_vref = 0;
   uint8         max_eye_width_single_vref = 0;
   uint8                         eye_width = 0;
   uint8                   eye_width_count = 0;
   uint8                        start_vref = 0;
   uint8                         start_cdc = 0;
   uint8                          max_vref = 0;
   uint8                           max_cdc = 0;
   uint8                         vref_step = 0;
   uint8                          cdc_step = 0;
 

   if (training_type == TRAINING_TYPE_CA_VREF)
   {
      start_vref = training_params_ptr->ca_vref.coarse_vref_start_value;
      start_cdc  = training_params_ptr->ca_vref.coarse_cdc_start_value;
      max_vref   = training_params_ptr->ca_vref.coarse_vref_max_value;
      max_cdc    = training_params_ptr->ca_vref.coarse_cdc_max_value;
      vref_step  = training_params_ptr->ca_vref.coarse_vref_step;
      cdc_step   = training_params_ptr->ca_vref.coarse_cdc_step;
   }
   else if (training_type == TRAINING_TYPE_WR_DQDQS)
   {
      start_vref = training_params_ptr->wr_dqdqs.coarse_vref_start_value;
      start_cdc  = training_params_ptr->wr_dqdqs.coarse_cdc_start_value;
      max_vref   = training_params_ptr->wr_dqdqs.coarse_vref_max_value;
      max_cdc    = training_params_ptr->wr_dqdqs.coarse_cdc_max_value;
      vref_step  = training_params_ptr->wr_dqdqs.coarse_vref_step;
      cdc_step   = training_params_ptr->wr_dqdqs.coarse_cdc_step;
   }
   else if (training_type == TRAINING_TYPE_RD_DQDQS )
   {
      // Check for frequency range (HP or MP global vref)
      if ((clk_freq_khz > F_RANGE_3) && (training_params_ptr->rd_dqdqs.coarse_vref_max_value > 32))
      {
         max_vref = 32;
      }
      else
      {
         max_vref = training_params_ptr->rd_dqdqs.coarse_vref_max_value;
      }
      start_vref = 0; //read pass band info always starts from 0  
      start_cdc  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value;
      max_cdc    = training_params_ptr->rd_dqdqs.coarse_cdc_max_value;
      vref_step  = training_params_ptr->rd_dqdqs.coarse_vref_step;
      cdc_step   = training_params_ptr->rd_dqdqs.coarse_cdc_step;
   }

   best_eye_ptr[phy_inx].bottom_max_eye_vref_value = 0xFF;
   best_eye_ptr[phy_inx].top_max_eye_vref_value    = 0xFF;
   best_eye_ptr[phy_inx].vref_all_fail_flag        = 0;

   // Scan the eye histogram in Y-axis from bottom to top (vref increases)
   for (vref_value = start_vref; vref_value <= max_vref; vref_value += vref_step)
   {
      vref_index = (vref_value - start_vref) / vref_step;   // Vref_step is never to be 0, truncation is expected

      first_pass_flag           = 0;
      eye_width_count           = 0;
      max_eye_width_single_vref = 0;

      // Scan the eye histogram in X-axis from left to right (cdc increases)
      for (cdc_value = start_cdc; cdc_value <= max_cdc; cdc_value += cdc_step)
      {
         cdc_index = (cdc_value - start_cdc) / cdc_step;    // CDC_step is never to be 0, truncation is expected

         // Record each of the eye band width info (width, left/right boundary) for this line
         // When the current point is PASS, eye_width_count ++
         if (coarse_fail_count_ptr[phy_inx][vref_index][cdc_index] == 0)   // When the current point is PASS
         {
            eye_width_count ++;

            if (first_pass_flag == 0) {      // Record the first PASS point as left boundary
               temp_left_boundary_eye_cdc_value = cdc_value;
            }

            // If the max_cdc point is PASS, also record it as right boundary
            if (cdc_index == (max_cdc - start_cdc) / cdc_step)
            {
               temp_right_boundary_eye_cdc_value = cdc_value;

               // In case several eye bands are in same width, choose the rightmost one
               if (eye_width_count >= max_eye_width_single_vref)
               {
                  max_eye_width_single_vref = eye_width_count;
                  left_boundary_eye_cdc_value  = temp_left_boundary_eye_cdc_value;
                  right_boundary_eye_cdc_value = temp_right_boundary_eye_cdc_value;
               }
            }

            first_pass_flag = 1;
         }

         // Current point is FAIL and previous point is PASS
         else if (cdc_index >= 1 && coarse_fail_count_ptr[phy_inx][vref_index][cdc_index-1] == 0)
         {
            temp_right_boundary_eye_cdc_value = cdc_value - cdc_step;

            // In case several eye bands are in same width, choose the rightmost one
            if (eye_width_count >= max_eye_width_single_vref)
            {
               max_eye_width_single_vref = eye_width_count;
               left_boundary_eye_cdc_value  = temp_left_boundary_eye_cdc_value;
               right_boundary_eye_cdc_value = temp_right_boundary_eye_cdc_value;
            }

            eye_width_count = 0;
            first_pass_flag = 0;
         }

         // max_eye_width_single_vref can only be 0 when all points in this line are FAIL
         // set both left and right boundary to 0xFF to be invalid
         if (max_eye_width_single_vref == 0)
         {
            left_boundary_eye_cdc_value  = 0xFF ;
            right_boundary_eye_cdc_value = 0xFF ;
         }
      } // END of CDC loop

      eye_width = max_eye_width_single_vref;

      // In case that several vref values have the same eye_width, using bottom_max_eye_vref_value and top_max_eye_vref_value to record
      if (eye_width > max_eye_width_all_vref)
      {
         best_eye_ptr[phy_inx].bottom_max_eye_vref_value = vref_value;
         max_eye_width_all_vref = eye_width;
         best_eye_ptr[phy_inx].max_eye_right_boundary_cdc_value = right_boundary_eye_cdc_value;
         best_eye_ptr[phy_inx].max_eye_left_boundary_cdc_value = left_boundary_eye_cdc_value;
      }

      else if (eye_width == max_eye_width_all_vref && eye_width != 0) {
         best_eye_ptr[phy_inx].top_max_eye_vref_value = vref_value;
      }
   }   //END of Vref Loop

   // max_eye_width_all_vref can only be 0 when all points of entire histogram are FAIL
   // set both max eye left and right boundary, best_vref_value and best_cdc_value to 0xFF
   // and set vref_all_fail_flag to 1 as well
   if (max_eye_width_all_vref == 0)
   {
      best_eye_ptr[phy_inx].max_eye_right_boundary_cdc_value = 0xFF;    // 0xFF means an invalid value
      best_eye_ptr[phy_inx].max_eye_left_boundary_cdc_value = 0xFF;     // 0xFF means an invalid value
      best_eye_ptr[phy_inx].best_vref_value = 0xFF;                     // 0xFF means an invalid value
      best_eye_ptr[phy_inx].best_cdc_value  = 0xFF;                     // 0xFF means an invalid value
      best_eye_ptr[phy_inx].vref_all_fail_flag = 1;
   }
   else  // We have at least one valid horizontal open eye in the histogram
   {
      best_eye_ptr[phy_inx].vref_all_fail_flag = 0;
      best_eye_ptr[phy_inx].best_cdc_value     = ( best_eye_ptr[phy_inx].max_eye_right_boundary_cdc_value
                                                 + best_eye_ptr[phy_inx].max_eye_left_boundary_cdc_value ) / 2; 

      // When top_max_eye_vref_value(not 0xFF) is greater than the bottom, their average is the best_vref_value
      if (( best_eye_ptr[phy_inx].top_max_eye_vref_value > best_eye_ptr[phy_inx].bottom_max_eye_vref_value)
         && best_eye_ptr[phy_inx].top_max_eye_vref_value != 0xFF)   {
         best_eye_ptr[phy_inx].best_vref_value = ( best_eye_ptr[phy_inx].bottom_max_eye_vref_value
                                                 + best_eye_ptr[phy_inx].top_max_eye_vref_value ) / 2;
      }
      // The case that top_max_eye_vref_value is smaller than bottom or top_max_eye_vref_value is still 0xFF but bottom is not
      // means that we have only one vref having max_eye_width, so bottom_max_eye_vref_value is the best vref
      if ((  best_eye_ptr[phy_inx].top_max_eye_vref_value < best_eye_ptr[phy_inx].bottom_max_eye_vref_value)
         || (best_eye_ptr[phy_inx].top_max_eye_vref_value == 0xFF && best_eye_ptr[phy_inx].bottom_max_eye_vref_value != 0xFF))
      {
         best_eye_ptr[phy_inx].best_vref_value = best_eye_ptr[phy_inx].bottom_max_eye_vref_value;
      }
   }
}


//================================================================================================//
// Process fine histogram of each boundary
//================================================================================================//
void DDRSS_Post_Histogram_Fine_Each_Boundary (uint8 (*fine_fail_count_ptr)[PINS_PER_PHY][FINE_VREF][FINE_CDC],
                                              uint8 (*boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                              uint8 start_vref,
                                              uint8 max_vref,
                                              training_data *training_data_ptr,
                                              training_params_t *training_params_ptr,
                                              uint8 left_right,  // 0:Left side fine training; 1:Right side fine training
                                              uint8 training_type, /* 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                              uint8 phy_inx,
                                              uint8 bit
                                             )
{
   uint8   cdc_value = 0;
   uint8  vref_value = 0;
   uint8   cdc_index = 0;
   uint8  vref_index = 0;
   uint8     max_cdc = 0;
   uint8   vref_step = 0;
   uint8    cdc_step = 0;
   
   if (training_type == TRAINING_TYPE_CA_VREF)
   {
      max_cdc     = training_params_ptr->ca_vref.fine_cdc_max_value;
      vref_step   = training_params_ptr->ca_vref.fine_vref_step;
      cdc_step    = training_params_ptr->ca_vref.fine_cdc_step;
   }
   else if (training_type == TRAINING_TYPE_WR_DQDQS)
   {
      max_cdc     = training_params_ptr->wr_dqdqs.fine_cdc_max_value;
      vref_step   = training_params_ptr->wr_dqdqs.fine_vref_step;
      cdc_step    = training_params_ptr->wr_dqdqs.fine_cdc_step;
   }

   // Scan the eye histogram in Y-axis from bottom to top (vref increases)
   for (vref_value = start_vref; vref_value <= max_vref; vref_value+=vref_step)
   {
      vref_index = (vref_value - start_vref) / vref_step;

      // Fine training for left boundary
      if (left_right == 0)
      {         
         // Scan the eye histogram in X-axis from right to left (cdc decreases) and find the pass-to-fail point.
         for (cdc_value = (max_cdc +1); cdc_value >= 1; cdc_value -= cdc_step)
         {
            cdc_index = ((cdc_value - 1) /cdc_step);

            // If fail at cdc max_value, then that point is the failed boundary value itself.
            if ((cdc_value == (max_cdc +1)) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            { 
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - 1;  
               break;
            }
            // If fail at some cdc value, then the last passing point was one cdc higher due to our direction.
            else if ((cdc_value != (max_cdc +1)) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - 1 + cdc_step;
               break;
            }
            // In case that all CDC values pass, left_boundary is 0
            boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = 0;

         }// CDC Loop
      }

      // Fine training for right boundary
      else
      {
         // Scan the eye histogram in X-axis from left to right (cdc increases) and find the pass-to-fail point.         
         for (cdc_value = 0; cdc_value <= max_cdc; cdc_value += cdc_step)
         {
            cdc_index = (cdc_value/cdc_step);

            // If fail at cdc value 0, then that point is the failed boundary value itself.
            if ((cdc_value == 0) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value;
               break;
            }
            // if fail at some cdc value, then the last passing point was one cdc before due to our direction.
            else if ((cdc_value != 0) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - cdc_step;
               break;
            }
            // In case that all CDC values pass, right_boundary is set to max_cdc
            boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = max_cdc;

         }  // CDC Loop
      }
   }  // Vref Loop

}
//================================================================================================//
// Process fine histogram of each boundary
//================================================================================================//
void DDRSS_Post_Histogram_RD_Fine_Each_Boundary (uint8 (*fine_fail_count_ptr)[PINS_PER_PHY][FINE_VREF][FINE_RD_CDC],
                                                 uint8 (*boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                                 uint8 start_vref,
                                                 uint8 max_vref,
                                                training_data *training_data_ptr,
                                                 training_params_t *training_params_ptr,
                                                 uint8 left_right,  // 0:Left side fine training; 1:Right side fine training
                                                 uint8 phy_inx,
                                                 uint8 bit
                                                )
{
   uint8   cdc_value = 0;
   uint8  vref_value = 0;
   uint8   cdc_index = 0;
   uint8  vref_index = 0;
   uint8     max_cdc = 0;
   uint8   vref_step = 0;
   uint8    cdc_step = 0;
   
   max_cdc     = training_params_ptr->rd_dqdqs.fine_cdc_max_value;
   vref_step   = training_params_ptr->rd_dqdqs.fine_vref_step;
   cdc_step    = training_params_ptr->rd_dqdqs.fine_cdc_step;


   // Scan the eye histogram in Y-axis from bottom to top (vref increases)
   for (vref_value = start_vref; vref_value <= max_vref; vref_value+=vref_step)
   {
      vref_index = (vref_value - start_vref) / vref_step;

      // Fine training for left boundary
      if (left_right == 0)
      {         
         // Scan the eye histogram in X-axis from right to left (cdc decreases) and find the pass-to-fail point.
         for (cdc_value = (max_cdc +1); cdc_value >= 1; cdc_value -= cdc_step)
         {
            cdc_index = ((cdc_value - 1) /cdc_step);

            // If fail at cdc max_value, then that point is the failed boundary value itself.
            if ((cdc_value == (max_cdc +1)) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            { 
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - 1;  
               break;
            }
            // If fail at some cdc value, then the last passing point was one cdc higher due to our direction.
            else if ((cdc_value != (max_cdc +1)) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - 1 + cdc_step;
               break;
            }
            // In case that all CDC values pass, left_boundary is 0
            boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = 0;

         }// CDC Loop
      }

      // Fine training for right boundary
      else
      {
         // Scan the eye histogram in X-axis from left to right (cdc increases) and find the pass-to-fail point.         
         for (cdc_value = 0; cdc_value <= max_cdc; cdc_value += cdc_step)
         {
            cdc_index = (cdc_value/cdc_step);

            // If fail at cdc value 0, then that point is the failed boundary value itself.
            if ((cdc_value == 0) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value;
               break;
            }
            // if fail at some cdc value, then the last passing point was one cdc before due to our direction.
            else if ((cdc_value != 0) && (fine_fail_count_ptr[phy_inx][bit][vref_index][cdc_index] != 0)) 
            {
               boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = cdc_value - cdc_step;
               break;
            }
            // In case that all CDC values pass, right_boundary is set to max_cdc
            boundary_fine_cdc_ptr[phy_inx][bit][vref_index] = max_cdc;

         }  // CDC Loop
      }
   }  // Vref Loop

}

//================================================================================================//
// Process fine histogram to find best eye
//================================================================================================//
 void DDRSS_Post_Histogram_Fine_Best_Eye_Cal (best_eye_struct (*best_eye_ptr)[PINS_PER_PHY],
                                              uint8 (*left_boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                              uint8 (*right_boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                              uint8 start_vref,
                                              uint8 max_vref,
                                              training_data *training_data_ptr,
                                              training_params_t *training_params_ptr,
                                              uint8 training_type, /* 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                                              uint8 phy_inx,
                                              uint8 bit
                                             )
 {
    uint8    vref_value = 0;
    uint8    vref_index = 0;
    uint8     eye_width = 0;
    uint8 max_eye_width = 0;
    uint8       max_cdc = 0;
    uint8     vref_step = 0;

    best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value = 0xFF;
    best_eye_ptr[phy_inx][bit].top_max_eye_vref_value    = 0xFF;
    
    if (training_type == TRAINING_TYPE_CA_VREF)
    {
       max_cdc    = training_params_ptr->ca_vref.fine_cdc_max_value;
       vref_step  = training_params_ptr->ca_vref.fine_vref_step;
    }
    else if (training_type == TRAINING_TYPE_WR_DQDQS)
    {
       max_cdc    = training_params_ptr->wr_dqdqs.fine_cdc_max_value;
       vref_step  = training_params_ptr->wr_dqdqs.fine_vref_step;
    }
    else if (training_type == TRAINING_TYPE_RD_DQDQS )
    {
       max_cdc    = training_params_ptr->rd_dqdqs.fine_cdc_max_value;
       vref_step  = training_params_ptr->rd_dqdqs.fine_vref_step;
    }

    for (vref_value = start_vref; vref_value <= max_vref; vref_value+=vref_step)
    {
       vref_index = (vref_value - start_vref) / vref_step;   // Vref_step is never to be 0, truncation is expected

       // When this vref value is valid, calculate correct eye_width, otherwise, eye_width = 0xFF
       if (right_boundary_fine_cdc_ptr[phy_inx][bit][vref_index] != 0xFF && left_boundary_fine_cdc_ptr[phy_inx][bit][vref_index] != 0xFF)   {
          eye_width = (right_boundary_fine_cdc_ptr[phy_inx][bit][vref_index] + max_cdc - left_boundary_fine_cdc_ptr[phy_inx][bit][vref_index]);
       }
       else  {  //It is not possible that eye_width for all the vref_value are 0xFF, at least one value is valid
          eye_width = 0xFF;
       }
       // In case that several vref values have the same eye_width, using bottom_max_eye_vref_value and top_max_eye_vref_value to record
       if (eye_width > max_eye_width && eye_width != 0xFF)
       {
          best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value = vref_value;
          max_eye_width = eye_width;
          best_eye_ptr[phy_inx][bit].max_eye_right_boundary_cdc_value = right_boundary_fine_cdc_ptr[phy_inx][bit][vref_index];
          best_eye_ptr[phy_inx][bit].max_eye_left_boundary_cdc_value  = left_boundary_fine_cdc_ptr[phy_inx][bit][vref_index];
       }

       else if (eye_width == max_eye_width) {
          best_eye_ptr[phy_inx][bit].top_max_eye_vref_value = vref_value;
       }
    }   //END of Vref Loop

    // The case that we have several vref values with the same max_eye_width,
    // the best vref is the medium value of top_max_eye_vref_value and bottom_max_eye_vref_value
    if (( best_eye_ptr[phy_inx][bit].top_max_eye_vref_value > best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value)
       && best_eye_ptr[phy_inx][bit].top_max_eye_vref_value != 0xFF)   {
       best_eye_ptr[phy_inx][bit].best_vref_value = ( best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value
                                               + best_eye_ptr[phy_inx][bit].top_max_eye_vref_value ) / 2;
    }

    // The case that top_max_eye_vref_value is smaller than bottom or top_max_eye_vref_value is still 0xFF
    // means the we have only one vref having max_eye_width, so bottom_max_eye_vref_value is the best vref
    if ((best_eye_ptr[phy_inx][bit].top_max_eye_vref_value < best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value && max_eye_width != 0)
       || (best_eye_ptr[phy_inx][bit].top_max_eye_vref_value == 0xFF && best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value != 0xFF))  {
       best_eye_ptr[phy_inx][bit].best_vref_value = best_eye_ptr[phy_inx][bit].bottom_max_eye_vref_value;
    }

    // max_eye_width == 0 can only occur when eye_width is always 0 except for cases eye_width is 0xFF
    if (max_eye_width == 0)
    {
       best_eye_ptr[phy_inx][bit].best_vref_value = (best_eye_ptr[phy_inx][bit].top_max_eye_vref_value + start_vref) / 2;
       best_eye_ptr[phy_inx][bit].max_eye_right_boundary_cdc_value = 0;
       best_eye_ptr[phy_inx][bit].max_eye_left_boundary_cdc_value  = max_cdc;
    }
 }


//================================================================================================//
// CDC retimer
//================================================================================================//
void DDRSS_CDC_Retimer (DDR_STRUCT *ddr,
                        uint8 cs,
                        uint8 coarse_dqs_delay,
                        uint8 fine_dqs_delay,
                        uint8 coarse_wrlvl_delay,
                        uint8 fine_wrlvl_delay,
                        uint32 cadq_ddr_phy_base,
                        uint32 clk_freq_khz
                       )
{
   uint8             dq_retmr = 0;
   uint32     coarse_delay_ps = 0;
   uint32       fine_delay_ps = 0;
   uint32              period = 0;
   uint32                  T4 = 0;
   uint32                 T34 = 0;

   // Determine the Clock period normalized to ps
   period = CONVERT_CYC_TO_PS/clk_freq_khz;

   // Determine the period boundaries (T/4 and 3T/4)
   T4  = period / 4;
   T34 = (3 * T4);

   coarse_delay_ps = (coarse_dqs_delay + coarse_wrlvl_delay) * COARSE_STEP_IN_PS;
   fine_delay_ps   = (fine_dqs_delay   +   fine_wrlvl_delay) * FINE_STEP_IN_PS;

   // Increment the retimer when the delay exceeds the T/4 period boundaries
   if ((coarse_delay_ps + fine_delay_ps) <=  T4) {
      dq_retmr  = 0;
   }

   else if (((coarse_delay_ps + fine_delay_ps) >  T4) &&
       ((coarse_delay_ps + fine_delay_ps) <= T34)) {
      dq_retmr  = 1;
    }

   else if ((coarse_delay_ps + fine_delay_ps) >  T34) {
      dq_retmr  = 2;
   }

   // Update the re-timer registers at T/4 boundaries
   //Write wrlvl delay values back to each PHY (non-broadcast)
   DDR_PHY_hal_cfg_ca_vref_dq_retmr (cadq_ddr_phy_base,
                                        cs,
                                        dq_retmr);

}

//================================================================================================//
// Frequency Scaling
//================================================================================================//

// Divide and remainder calculation functions for WRLVL CDC conversion
uint32 calc (uint32 step, uint32 i_delay, uint8 set_rem_out)
{
   uint32 div_rem = 0;

   // Divide or Mod depending on set_rem_out decode
   if ((step <= i_delay) && (step != 0)) {
      // Mod delay by step
      if (set_rem_out == 1) {
         div_rem   = (i_delay % step);
      }
      else {
         // Divide delay by step
         div_rem   = (i_delay / step);
         if (set_rem_out == 2) {
             div_rem = div_rem  * step;
         }
      }
   }
   // Bypass the result if illegal
   else {
       if (set_rem_out == 1) {
           div_rem = i_delay;
       }
   }

   // Return the result
   return div_rem;
}

// Optimize Coarse and Fine CDC, Half, and Retimer values based on trained results
void DDRSS_cdc_convert (DDR_STRUCT *ddr, wrlvl_params_struct *wrlvl_params_ptr, uint32 wrlvl_delay, 
                        uint32 period, uint8  training_type, /*1:wr_dqdqs training*/
                        uint8 half_cycle, uint8 full_cycle)
{

    //  Calculation Constants
    uint8  opt_unit_cdcc   = COARSE_STEP_IN_PS; // CDC coarse unit delay (ps)
    uint8  opt_unit_cdcf   = FINE_STEP_IN_PS;   // CDC fine unit delay (ps)
    uint8  opt_step_cdcc   = 25;                // Number of CDC coarse delay steps

    // Calculation variables
    uint16 dqs_wrlvl_delay;
    uint16 dqs_wrlvl_full_setting;
    uint16 dqs_wrlvl_half_rem_delay;
    uint16 dqs_wrlvl_half_setting;
    uint16 dqs_wrlvl_cdcc_setting;
    uint16 dqs_wrlvl_cdcc_rem_delay;
    uint16 dqs_wrlvl_cdcf_setting;
    uint16 dqs_wrlvl_cdc_delay;
    uint16 dqs_retimer_sel;
    uint16 t2;
    uint16 t34;
    uint16 t4;

    //--------------------------------------------------------------------
    //DQS WRLVL CDC Calculations
    //--------------------------------------------------------------------

     // Calculate t/4 and 3t/4
    t2  = period / 2;
    t4  = period / 4;
    t34 = 3 * t4;

    // Trained WRLVL skew relative to CK
    dqs_wrlvl_delay = wrlvl_delay;

    // Do not calculate full setting due to CDC range requirement
    dqs_wrlvl_full_setting = 0;

    // Use the full_cycle value found from WR DQDQS training.
    if (training_type == TRAINING_TYPE_WR_DQDQS)
    {
        dqs_wrlvl_full_setting = full_cycle;
    }
    
    
    // Use the half delay only if the total delay exceeds the coarse CDC limit and period is less than 2.5ns
    if ((dqs_wrlvl_delay > (opt_step_cdcc * opt_unit_cdcc)) && (period < 2500)) {
      dqs_wrlvl_half_setting   = calc(t2,dqs_wrlvl_delay,0x0);
      dqs_wrlvl_half_rem_delay = calc(t2,dqs_wrlvl_delay,0x1);
    }
      else {
        dqs_wrlvl_half_setting   = 0;
        if (dqs_wrlvl_delay > COARSE_STEP_IN_PS * opt_step_cdcc)
        {
         dqs_wrlvl_delay = COARSE_STEP_IN_PS * opt_step_cdcc;
          
        }
        dqs_wrlvl_half_rem_delay = dqs_wrlvl_delay;
    }

    // Correct half setting if greater than one
    if (dqs_wrlvl_half_setting > 1) {
       dqs_wrlvl_half_rem_delay += (dqs_wrlvl_half_setting - 1) * t2;
       dqs_wrlvl_half_setting = 1;
    }

    // Use the half_cycle value found from WR DQDQS training.
    if (training_type == TRAINING_TYPE_WR_DQDQS)
    {
        dqs_wrlvl_half_setting = half_cycle;
    }
    
    // Calculate WRLVL CDC Coarse Settings
    dqs_wrlvl_cdcc_setting   = calc(opt_unit_cdcc,dqs_wrlvl_half_rem_delay,0x0);
    dqs_wrlvl_cdcc_rem_delay = calc(opt_unit_cdcc,dqs_wrlvl_half_rem_delay,0x1);

    // Check WRLVL CDC coarse setting against the maximum
    if (dqs_wrlvl_cdcc_setting > opt_step_cdcc) {
       dqs_wrlvl_cdcc_setting = opt_step_cdcc;
       dqs_wrlvl_cdcc_rem_delay = dqs_wrlvl_half_rem_delay - (opt_step_cdcc * opt_unit_cdcc);
    }

    // Calculate WRLVL Fine setting
    dqs_wrlvl_cdcf_setting   = calc(opt_unit_cdcf,dqs_wrlvl_cdcc_rem_delay,0x0);

    // Round up fine delay if the a half delay is used and the coarse CDC remainder
    //is greater than the fine CDC unit
    if ((dqs_wrlvl_half_setting == 1) && (dqs_wrlvl_cdcc_rem_delay > 0)) {
       dqs_wrlvl_cdcf_setting++;
    }

    // Adjust the fine value to have at least 4 for DTTS periodic training
    if ((dqs_wrlvl_cdcc_setting > 0) && (dqs_wrlvl_cdcf_setting < ((NUM_DIT_COUNT/2)-1))) 
    {
      dqs_wrlvl_cdcc_setting--;
      dqs_wrlvl_cdcf_setting += FINE_STEPS_PER_COARSE;
    }

    // Calculate the WRLVL CDC delay
    dqs_wrlvl_cdc_delay = (dqs_wrlvl_cdcc_setting * opt_unit_cdcc) +
                          (dqs_wrlvl_cdcf_setting * opt_unit_cdcf);

    // Determine Optimal Retimer Settings
    if (dqs_wrlvl_cdc_delay <= t4) {
       dqs_retimer_sel = 0;
    }
    else if ((dqs_wrlvl_cdc_delay >  t4) && (dqs_wrlvl_cdc_delay <= t34)) {
       dqs_retimer_sel = 1;
    }
    else /* if (dqs_wrlvl_cdc_delay >  t34) */ {	
       dqs_retimer_sel = 2;
    }

    // Push the converted values into the wrlvl_delay struct
    wrlvl_params_ptr->dqs_retmr        = dqs_retimer_sel;
    wrlvl_params_ptr->coarse_dqs_delay = dqs_wrlvl_cdcc_setting;
    wrlvl_params_ptr->fine_dqs_delay   = dqs_wrlvl_cdcf_setting;
    wrlvl_params_ptr->dqs_full_cycle   = dqs_wrlvl_full_setting;
    wrlvl_params_ptr->dqs_half_cycle   = dqs_wrlvl_half_setting;
    
} // DDR_PHY_cdc_convert


void DDRSS_ddr_phy_sw_freq_switch (DDR_STRUCT *ddr, uint32 clk_freq_khz, uint8 ch)
{

    uint32   reg_offset_ddr_phy = 0;
    uint32   reg_offset_dpe     = 0;
    uint8 sw_handshake_complete = 1;

    // Base pointer to BIMC
    reg_offset_dpe   = REG_OFFSET_DPE(ch);

    // Configure the DDR PHY address offset
    reg_offset_ddr_phy = REG_OFFSET_DDR_PHY_CH(ch);

    // Turn on continuous GCC clock per channel 
    HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
    HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
	
    // Enable broadcast mode for 4 DQ PHYs and 2 CA PHYs
    HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x3F << (ch * 7));

    /// set FPM_INIT_START to 1 to start frequency switch
    DDR_PHY_hal_cfg_sw_handshake_start (ddr, BROADCAST_BASE, clk_freq_khz);

    /// Poll for DDRPHY_FPM_TOP_STA[FPM_SW_INIT_COMP]
    while (sw_handshake_complete == 0x1) {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// set FPM_INIT_START to 0 to stop frequency switch
    DDR_PHY_hal_cfg_sw_handshake_stop (ddr, BROADCAST_BASE, clk_freq_khz);

    // Poll for handshake complete
    while (sw_handshake_complete == 0x0) {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// disable software trigger handshake, and enable hardware FPM
    DDR_PHY_hal_cfg_sw_handshake_complete (BROADCAST_BASE);

    // Disable broadcast mode
    HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);

    // Turn off continuous GCC clock per channel 
    HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);
    HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
}

void DDRSS_ddr_phy_sw_freq_switch_PC (DDR_STRUCT *ddr, uint32 clk_freq_khz )
{
    uint32   reg_offset_ddr_phy0 = 0;
    uint32   reg_offset_ddr_phy1 = 0;
    uint32   reg_offset_dpe0     = 0;
    uint32   reg_offset_dpe1     = 0;
    uint8 sw_handshake_complete  = 1;

    // Base pointer to BIMC
    reg_offset_dpe0   = REG_OFFSET_DPE(0);
    reg_offset_dpe1   = REG_OFFSET_DPE(1);

    // Configure the DDR PHY address offset
    reg_offset_ddr_phy0 = REG_OFFSET_DDR_PHY_CH(0);
    reg_offset_ddr_phy1 = REG_OFFSET_DDR_PHY_CH(1);

    // Turn on continuous GCC clock per channel 
    HWIO_OUTXF2(reg_offset_dpe0, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
    HWIO_OUTXF (reg_offset_dpe0, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);
    HWIO_OUTXF2(reg_offset_dpe1, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
    HWIO_OUTXF (reg_offset_dpe1, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

    // Enable broadcast mode for 4 DQ PHYs and 2 CA PHYs
    HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
               AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 
               PC_PHY_CA_DQ_BROADCAST
             );

    /// set FPM_INIT_START to 1 to start frequency switch
    DDR_PHY_hal_cfg_sw_handshake_start (ddr, BROADCAST_BASE, clk_freq_khz);

    /// Poll for DDRPHY_FPM_TOP_STA[FPM_SW_INIT_COMP]
    while (sw_handshake_complete == 0x1) 
    {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// set FPM_INIT_START to 0 to stop frequency switch
    DDR_PHY_hal_cfg_sw_handshake_stop (ddr, BROADCAST_BASE, clk_freq_khz);

    // Poll for handshake complete
    while (sw_handshake_complete == 0x0) 
    {
      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy0 + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &
                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy1 + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));
    }

    /// disable software trigger handshake, and enable hardware FPM
    DDR_PHY_hal_cfg_sw_handshake_complete (BROADCAST_BASE);
    
}



