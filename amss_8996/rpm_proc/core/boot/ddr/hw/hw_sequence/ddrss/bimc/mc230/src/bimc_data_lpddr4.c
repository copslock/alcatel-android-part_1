/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/hw_sequence/ddrss/bimc/mc230/src/bimc_data_lpddr4.c#12 $
$DateTime: 2015/10/15 14:57:05 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"

//================================================================================================//
// Geometry Tables
// Number of rows and columns for each device density
//================================================================================================//
uint8   lpddr_geometry_table[][2] = {
  /* nrows, ncols */
    {14,    10},  //  4 Gb per die ( 2 Gb per channel)
    {15,    10},  //  6 Gb per die ( 3 Gb per channel)
    {15,    10},  //  8 Gb per die ( 4 Gb per channel)
    {16,    10},  // 12 Gb per die ( 6 Gb per channel)
    {16,    10},  // 16 Gb per die ( 8 Gb per channel)
    {16,    10},  // 24 Gb per die (12 Gb per channel)TODO:value TBD in spec
    {0,     0 },  // Reserved
    {16,    10}   // 32 Gb per die (16 Gb per channel)TODO:value TBD in spec
};
uint16   lpddr_size_table[][2] = {
  /* size_mb, addr_offset_bit*/
    {0x200,       29},  //  4 Gb per die ( 2 Gb per channel)
    {0x300,       30},  //  6 Gb per die ( 3 Gb per channel)
    {0x400,       30},  //  8 Gb per die ( 4 Gb per channel)
    {0x600,       31},  // 12 Gb per die ( 6 Gb per channel)
    {0x800,       31},  // 16 Gb per die ( 8 Gb per channel)
    {0xc00,       31},  // 24 Gb per die (12 Gb per channel)
    {0x1000,      31}   // 32 Gb per die (16 Gb per channel)
};
uint16   lpddr_timing_table[][2] = {
  /* trfc, txsr in 100ps*/
    {1300, 1375},  //  4 Gb per die ( 2 Gb per channel)
    {1800, 1875},  //  6 Gb per die ( 3 Gb per channel)
    {1800, 1875},  //  8 Gb per die ( 4 Gb per channel)
    {2800, 2875},  // 12 Gb per die ( 6 Gb per channel)
    {2800, 2875},  // 16 Gb per die ( 8 Gb per channel)
    {2800, 2875},  // 24 Gb per die (12 Gb per channel), trfc TBD
    {2800, 2875}   // 32 Gb per die (16 Gb per channel), trfc TBD
};

//================================================================================================//
// Read and Write Latency Tables
// RL, WL and corresponding MR2 values
//================================================================================================//
struct ecdt_dram_latency_runtime_struct RL_WL_lpddr_struct[] =
{
    /*RL,   RL
   DBI_OFF DBI_ON  WL, MR2,  freq */
   { 6 ,     6 ,   4 , 0x00, 266000},  /*   10 < F <= 266MHz  */
   { 10,     12,   6 , 0x09, 533000},  /*  266 < F <= 533MHz  */
   { 14,     16,   8 , 0x12, 800000},  /*  533 < F <= 800MHz  */
   { 20,     22,   10, 0x1B, 1066000}, /*  800 < F <= 1066MHz */
   { 24,     28,   12, 0x24, 1333000}, /* 1066 < F <= 1333MHz */
   { 28,     32,   14, 0x2D, 1600000}, /* 1333 < F <= 1600MHz */
   { 32,     36,   16, 0x36, 1866000}, /* 1600 < F <= 1866MHz */
   { 36,     40,   18, 0x3F, 2133000}, /* 1866 < F <= 2133MHz */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}, /* reserved */
   {  0,      0,    0,    0,       0}  /* reserved */
};

uint8 RL_WL_freq_range_table_size = sizeof(RL_WL_lpddr_struct)/sizeof(struct ecdt_dram_latency_runtime_struct);

uint32 interface_width[]                        = {16,16};

//================================================================================================//
// Frequency tables for other LPDDR4 specific timing parameters
// ODTLon:  ODTLon values.
// MR13_wrdata: MR13 write data used during frequency switch across 2 FSP sets.
//================================================================================================//
uint8 ODTLon[][2]= {
   /*WPRE_1, WPRE_2*/
   { 0x4,    0x4 },     /*   10 < F <= 266MHz  */
   { 0x4,    0x4 },     /*  266 < F <= 533MHz  */
   { 0x4,    0x4 },     /*  533 < F <= 800MHz  */
   { 0x4,    0x4 },     /*  800 < F <= 1066MHz */
   { 0x6,    0x4 },     /* 1066 < F <= 1333MHz */
   { 0x6,    0x6 },     /* 1333 < F <= 1600MHz */
   { 0x8,    0x6 },     /* 1600 < F <= 1866MHz */
   { 0x8,    0x8 }      /* 1866 < F <= 2133MHz */
};

struct ecdt_bimc_freq_switch_params_runtime_struct bimc_freq_switch_params_struct[] =
{
   /* RD-DBI, ODT, FSP, MR1,  MR3,  MR11, freq_switch_params_freq_range */
   { 0,   0,   0,   0x06,   0xB0,   0x00,  400000}, /*  10  < F <= 400MHz, WR-pre =1, DBI_WR=1          */
   { 0,   0,   0,   0x06,   0xB0,   0x00,  750000}, /* 400  < F <= 750MHz, PU-CAL=Vddq/2.5              */
   { 0,   0,   0,   0x06,   0xB0,   0x00, 1000000}, /* 750  < F <= 1000MHz,                             */
   { 0,   1,   1,   0x06,   0xB0,   0x33, 1295000}, /* 1000 < F <= 1295MHz CA-ODT=Rzq/2, DQ-ODT=Rzq/3   */
   { 0,   1,   1,   0x0E,   0xB0,   0x33, 1570000}, /* 1295 < F <= 1570MHz RD-PRE=1,  DQ ODT = 80ohm    */
   { 0,   1,   1,   0x0E,   0xB2,   0x34, 1890000}, /* 1570 < F <= 1890MHz WR-PST =1, DQ ODT = 60ohm    */
   { 0,   0,   0,      0,      0,      0,       0}, /* reserved                                         */
   { 0,   0,   0,      0,      0,      0,       0}  /* reserved                                         */
};

uint8 bimc_freq_switch_params_table_size = sizeof(bimc_freq_switch_params_struct)/sizeof(struct ecdt_bimc_freq_switch_params_runtime_struct);


uint8 MR13_wrdata[][2] = {

   /*MR13_1, MR13_2*/
   { 0x48,   0x00},    /*  Next clock FSP Set 0  */
   { 0x88,   0xC0}     /*  Next clock FSP Set 1  */
   /*   FSP Set 0
        MR13_1 = 0x48; //FSP-OP=0, FSP-WR=1, VRCG=1
        MR13_2 = 0x00; //FSP-OP=0, FSP-WR=0, VRCG=0

        FSP Set 1
        MR13_1 = 0x88; //FSP-OP=1, FSP-WR=0, VRCG=1
        MR13_2 = 0xC0; //FSP-OP=1, FSP-WR=1, VRCG=0
   */
};




