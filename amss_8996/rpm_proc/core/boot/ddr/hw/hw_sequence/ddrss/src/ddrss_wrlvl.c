/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/hw_sequence/ddrss/src/ddrss_wrlvl.c#14 $
$DateTime: 2016/01/27 10:14:30 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
07/09/14   jeasley      Added IO control.  Removed CA fine training.  
05/27/14   jeasley      Convert coarse and fine step variables to COARSE_STEP_IN_PS
                        and FINE_STEP_IN_PS
05/22/14   jeasley      Optimized calc routine
05/21/14   jeasley      Updated the convert routine to use half-cycle only
05/08/14   jeasley      Changed channel and cs to zero based.  Removed DRAM register access.
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

/***************************************************************************************************
   Description: The DDRSS Write Leveling Routine performs the LPDDR4 write leveling function.
   Write Leveling is used to determine the skew at the DRAM device between the CK and DQS strobe.
   The hardware does not support training for cs (rank) 1.  The rank 0 results are populated to 
   rank 1 after rank 0 is trained.   
 
***************************************************************************************************/
#include "ddrss.h"

void DDRSS_wrlvl( DDR_STRUCT *ddr, 
                  uint8 ch, 
                  uint8 cs, 
                  uint32 clk_freq_khz, 
                  uint32 wrlvl_clk_freq_idx,
                  training_params_t *training_params_ptr)
{  
  //============================================================
  // WRLVL Main Code Thread
  //============================================================

  //-------------------------------------------------------------------------------------
  // Check for CLK->DQS negative skew and update CA PHY WRLVL CDCs
  //-------------------------------------------------------------------------------------

  // CA does not have rank switch capability
  //if (cs == 0) 
  //  DDRSS_wrlvl_ca(ddr,ch,cs,clk_freq_khz,training_params_ptr, wrlvl_clk_freq_idx);

  //-------------------------------------------------------------------------------------
  // Loop and repeat the Write Leveling procedure until all DQ PHYs are done or error
  //-------------------------------------------------------------------------------------

  DDRSS_wrlvl_dqs(ddr,ch,cs,clk_freq_khz,training_params_ptr,wrlvl_clk_freq_idx);

  //-------------------------------------------------------------------------------------
  // Read from the PHY CDC registers and write to the Flash Parameters Data Structure
  //-------------------------------------------------------------------------------------

  DDRSS_wrlvl_write_flash_params(ddr,ch,cs,wrlvl_clk_freq_idx);

} // DDR_PHY_wrlvl


void DDRSS_wrlvl_ca(DDR_STRUCT *ddr,uint8 ch, uint8 cs, uint32 clk_freq_khz, training_params_t *training_params_ptr, uint32 wrlvl_clk_freq_idx )
{
  //-------------------------------------------------------------------------------------
  // Check for DQS->CLK skew. If the CLK is early, add delay to the CA PHY WRLVL CDC
  // There are two DQ PHYs for each CA PHY.  Both DQ PHYs for each CA PHY must start with
  // a zero (fail) Write Level value.  
  //-------------------------------------------------------------------------------------

  uint8  wrlvl_ca_done                 = 0;  // CA wrlvl routine status
  uint8  bisc_status                   = 0;  // Device WRLVL result
  uint8  feedback_percent              = 0;  // Local feedback percent
  uint8  ca                            = 0;  // CA Loop counter
  uint8  loopcnt                       = 0;  // CA Loop counter
  uint8  dq                            = 0;  // DQ Loop counter
  uint8  done                          = 0;  // Loop done counter
  uint16 period                        = 0;  // Period calculated from frequency
  uint16 T4                            = 0;  // 1/4 Period
  uint16 T34                           = 0;  // 3/4 Period
  uint8  wrlvl_max_coarse_cdc          = 0;  // Coarse CDC Saturation Value
  uint8  wrlvl_max_loopcnt             = 0;  // Coarse CDC Saturation Value
//  uint8  wrlvl_feedback_percent        = 0;  // Coarse CDC Saturation Value

  uint8     byte_one_cnt[NUM_DQ_PCH]   = {0};
  uint8    byte_zero_cnt[NUM_DQ_PCH]   = {0};
  uint8     byte_ca_done[NUM_DQ_PCH]   = {0};   // CA status of the dq bytes
  uint8         ca_delay[NUM_CA_PCH]   = {0};   // CA Write level coarse delay
  uint8         ca_retmr[NUM_CA_PCH]   = {0};   // CA retmr value
  uint8   dqs_half_cycle[NUM_CA_PCH]   = {0};   // CA dqs half cycle value
  uint8   dqs_full_cycle[NUM_CA_PCH]   = {0};   // CA dqs full cycle value  
  
  uint32 dq0_ddr_phy_base              = 0;     // DDR PHY DQ0 Base Address
  uint32 ca0_ddr_phy_base              = 0;     // DDR PHY CA0 Base Address
  uint16 ca_delay_ps                   = 0;     // Coarse delay in picoseconds
  uint8  wrlvl_ctl[NUM_CA_PCH]         = {0};
  uint8  wrlvl_retimer[NUM_CA_PCH]     = {0};
  
  // Set DQ0 PHY BASE for the channel and cs
  dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
  ca0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
  
  // Set CDC Saturation Limits from the training params data structure
  wrlvl_max_coarse_cdc   = training_params_ptr->wrlvl.max_coarse_cdc;
  wrlvl_max_loopcnt      = training_params_ptr->wrlvl.max_loopcnt;
//  wrlvl_feedback_percent = training_params_ptr->wrlvl.feedback_percent;

  // Calculate the period based on the start frequency
  period = CONVERT_CYC_TO_PS/clk_freq_khz;

  // Calculate T4, T3/4
  T4  = period / 4;
  T34 = (3 * period) / 4;
 
 // Enable software override to DQS/DQ pad OEs.
  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Turn on continuous DDRCC clock (per byte lane to drive the DQS pulses)
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY, 0);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY_BYPASS, 1);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRAFFIC_CGC_EN, 1);
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG, BIST_TOP_CGC_EN, 1);     
    } 
    // Enable PAD SW override 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQS, 1);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQ, 0x3FF);

    // Disable PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQ,  0);

    //select BIST in datapath
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, WR_DP_SRC_SEL ,  0x1);

    // Enable DQS PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 1);

    // Enable DQ PAD IE
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_ENABLE_IE_DQ, 0x3FF);

    // Set SW Enable for DQS pulses
    HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x5);
    
    if (cs ==1)
    {
      HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, DLY_SEL, 0x1);
    }
  }

  // Initialize CA Rank 0 WRLVL CDC to zero delay
  for (ca=0;ca<NUM_CA_PCH;ca++) {


    DDR_PHY_hal_cfg_cdc_slave_wrlvl(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                               0, 
                               1, // coarse
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_cdc_slave_wrlvl(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                               0, 
                               0, // fine
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_wrlvl_retmr(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                                0, // cs
                                0);// retmr
  }

  // Loop until all DQs are done or error
  while (wrlvl_ca_done < NUM_DQ_PCH) 
  {
    // Read the bisc_status (Write Level result) from each DQ PHY pair
    for (dq=0;dq<NUM_DQ_PCH;dq++) {

      // Map the CA -> DQ.
      // Map the DQ to the CA CDC delays for 8996
      if ((dq == 0) || (dq == 1)) {
        ca = 0;
      }
      else {
        ca = 1;
      }


      if (ca_delay[ca] >= wrlvl_max_coarse_cdc) {
        byte_ca_done[dq] = 1;
      }
      else {
        if (byte_ca_done[dq] == 0) {

          byte_one_cnt[dq]  = 0;
          byte_zero_cnt[dq] = 0;

          for (loopcnt = wrlvl_max_loopcnt;loopcnt > 0;loopcnt-- ) {

            // Send two DQS pulses to all DQ PHYs
            HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);
            HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x1);     

            bisc_status =  DDR_PHY_hal_sta_wrlvl_training(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET));
  
            // Collapse the status to a single bit
            bisc_status = (bisc_status & 0x00000001);

            //  Check the bisc status and increment the one or zero count
            if (bisc_status == 1) {
               byte_one_cnt[dq]++;
            } else {
               byte_zero_cnt[dq]++;
            }
          }

          // Calculate the threshold in percent for Write Level pass (histogram)
          if ((byte_one_cnt[dq] + byte_zero_cnt[dq]) != 0) {
            feedback_percent =  ((byte_one_cnt[dq] * 100) / (byte_one_cnt[dq] + byte_zero_cnt[dq]));
          }
          else {
            feedback_percent = 0;
          }

          // Check bisc status.  If equal to one (pass), increment the CA WRLVL delay (CA CLK and Data)
          if (feedback_percent != 0) {
            ca_delay[ca]++;
  
            DDR_PHY_hal_cfg_cdc_slave_wrlvl(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                                           ca_delay[ca], 
                                           1, // coarse
                                           1, // hp_mode
                                           0 );

            // Check and update the retmr in the CA 
            ca_delay_ps = ca_delay[ca] * COARSE_STEP_IN_PS;
          
            // Increment the retimer when the delay exceeds the T/4 period boundaries
            if (ca_delay_ps <= T4) {
              ca_retmr[ca] = 0;
            }
            else if ((ca_delay_ps > T4) && (ca_delay_ps <= T34)) {
              ca_retmr[ca]  = 1;
            }
            else if (ca_delay_ps  > T34) {
              ca_retmr[ca] = 2;
            }
        
            DDR_PHY_hal_cfg_wrlvl_retmr(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                                        0,
                                        ca_retmr[ca]);
          }
          else {
           byte_ca_done[dq] = 1;
          }

        } // if byte_ca_done
      } // else byte_ca_done
    } // for dq

    // Aggregate all of the byte status to determine the ca done state
    wrlvl_ca_done = 0;
    for (done = 0; done < NUM_DQ_PCH; done++) {
      wrlvl_ca_done = wrlvl_ca_done + byte_ca_done[done];
    }
      
  } // while wrlvl_ca_done

  // De activate WRLVL test modes 
  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Disable PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQ, 0);

    //De-select BIST in datapath
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, WR_DP_SRC_SEL, 0x0);

    // Disable DQS PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 0);

    // Disable DQ PAD IE
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_ENABLE_IE_DQ, 0);

    // Disble WRLVL pulse control
    HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x0);

    // Disable PAD SW override 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQ, 0);

    // Turn off continuous DDRCC clock (per byte lane)
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY, 0);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY_BYPASS, 0);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRAFFIC_CGC_EN, 0);
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG, BIST_TOP_CGC_EN, 0);     
    }

    if (cs ==1)
    {
        HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, DLY_SEL, 0x0);
    } 
  }

  for (ca=0;ca<NUM_CA_PCH;ca++) 
  {
    if (cs == 1) 
    { 
      //when rank1 training is done, rank1 still stores rank0's cdc and retimer values
      wrlvl_ctl[ca] = DDR_PHY_hal_sta_wrlvl_coarse(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),1);
      wrlvl_retimer[ca] = DDR_PHY_hal_sta_wrlvl_retmr(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),1);      
      if (ca_delay[ca]< wrlvl_ctl[ca])
      {
        ca_delay[ca] = wrlvl_ctl[ca];
        ca_retmr[ca] = wrlvl_retimer[ca];
      }  
    }
    
    DDR_PHY_hal_cfg_cdc_slave_wrlvl(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                               ca_delay[ca], 
                               1, // coarse
                               1, // hp_mode
                               0);//cs

    DDR_PHY_hal_cfg_wrlvl_retmr(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                                0, // cs
                                ca_retmr[ca]);// retmr

    //copy rank0's value to rank1 as temporary storage, so that later on we can get rank0's value from rank1
    DDR_PHY_hal_cfg_cdc_slave_wrlvl(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                               ca_delay[ca], 
                               1, // coarse
                               1, // hp_mode
                               1);//cs

    DDR_PHY_hal_cfg_wrlvl_retmr(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),
                                1, // cs
                                ca_retmr[ca]);// retmr

    dqs_half_cycle[ca] = DDR_PHY_hal_sta_wrlvl_half (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), cs); 
    dqs_full_cycle[ca] = DDR_PHY_hal_sta_wrlvl_full (ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), cs);
    // Write trained CA WRLVL values to the ext registers
    DDR_PHY_hal_cfg_wrlvlext_ctl_update(ca0_ddr_phy_base  + (ca * DDR_PHY_OFFSET), 
                                        wrlvl_clk_freq_idx, 
                                        0,  //cs 
                                        ca_retmr[ca], 
                                        dqs_half_cycle[ca],  // half cycle
                                        dqs_full_cycle[ca]); // full cycle

    DDR_PHY_hal_cfg_cdcext_wrlvl_update(ca0_ddr_phy_base  + (ca * DDR_PHY_OFFSET),
                                        wrlvl_clk_freq_idx, 
                                        0, //cs
                                        0, //fine
                                        ca_delay[ca]); //coarse
  }

}// DDRSS_wrlvl_ca


void DDRSS_wrlvl_dqs(DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint32 clk_freq_khz, training_params_t *training_params_ptr, uint32 wrlvl_clk_freq_idx)
{
  //-----------------------------------------------------------------------------
  //  For the selected frequency, measure dqs->clk delay (wrlvl_max_loopcnt) 
  //  times for each wrlvl delay value. if the one count
  //  is greater than the zero count, the wrlvl training is finished.
  //  If not, the coarse CDC register is incremented. When the coarse CDC passes, 
  //  it is decremented and the loop repeats and increments the fine CDC delay.
  //  This loop is kept continued until either the one count is bigger than the
  //  zero count or when the wrlvl delay value reaches its maximum limit (MAX_CDC).
  //--------------------------------------------------------------------------------------

  // Local Variables
  uint8  dq                           = 0;     // dq loop counter
  uint8  done                         = 0;     // done loop counter
  uint8  loopcnt                      = 0;     // number of dq measurements
  uint8  feedback_percent             = 50;    // Percent of feedback ones in sample
  uint16 coarse_dqs_delay_ps          = 0;     // Write level coarse delay in ps
  uint16 fine_dqs_delay_ps            = 0;     // Write Level fine delay in ps
  uint8  wrlvl_dqs_done               = 0;     // Status of the wrlvl routine
  uint8  bisc_status                  = 0;     // Status of the wrlvl result

  uint16     byte_one_cnt[NUM_DQ_PCH] = {0};   // Count of 1 in dq0 measurement
  uint16    byte_zero_cnt[NUM_DQ_PCH] = {0};   // Count of 0 in dq0 measurement
  uint8  fine_dqs_started[NUM_DQ_PCH] = {0};   // Status of the dq bytes
  uint8     byte_dqs_done[NUM_DQ_PCH] = {0};   // Status of the dq bytes
  uint8    fine_dqs_delay[NUM_DQ_PCH] = {0};   // Write Level fine delay
  uint8  coarse_dqs_delay[NUM_DQ_PCH] = {0};   // Write level coarse delay
  uint8   coarse_dqs_done[NUM_DQ_PCH] = {0};   // Write level coarse delay status
  uint8         dqs_retmr[NUM_DQ_PCH] = {0};   // Local dqs re-timer variable
  uint8     dqs_half_cycle[NUM_DQ_PCH] = {0};   // Local dq half_cycle variable
  uint8     dqs_full_cycle[NUM_DQ_PCH] = {0};   // Local dq full_cycle variable
  
  uint8  wrlvl_max_loopcnt            = 0;     // Maximum measurement loop count
  uint8  wrlvl_max_coarse_cdc         = 0;     // Coarse CDC saturation limit
  uint8  wrlvl_max_fine_cdc           = 0;     // Fine CDC saturation limit
  uint8  wrlvl_feedback_percent       = 0;     // Feedback histogram pass percent
  uint16 period                       = 0;     // Period derived from the frequency
  uint16 T4                           = 0;     // 1/4 Period
  uint16 T34                          = 0;     // 3/4 Period
  uint32 dq0_ddr_phy_base             = 0;     // DDR PHY DQ0 base address
  uint8  wrlvl_coarse_cdc_step        = 0;     // Coarse CDC step
  uint8  wrlvl_fine_cdc_step          = 0;     // Fine CDC step

 // Set DQ0 PHY BASE for the channel and cs
  dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

  // Calculate the period based on the start frequency
  period = CONVERT_CYC_TO_PS/clk_freq_khz;

  // Calculate T/4 and 3T/4
  T4  =      period  / 4;
  T34 = (3 * period) / 4;

  wrlvl_max_loopcnt      = training_params_ptr->wrlvl.max_loopcnt;
  wrlvl_max_coarse_cdc   = training_params_ptr->wrlvl.max_coarse_cdc;
  wrlvl_max_fine_cdc     = training_params_ptr->wrlvl.max_fine_cdc;
  wrlvl_feedback_percent = training_params_ptr->wrlvl.feedback_percent;
  wrlvl_coarse_cdc_step  = training_params_ptr->wrlvl.coarse_cdc_step;
  wrlvl_fine_cdc_step    = training_params_ptr->wrlvl.fine_cdc_step;
  

 // Enable software override to DQS/DQ pad OEs.
  for (dq = 0; dq < NUM_DQ_PCH; dq++) 
  {
    // Turn on continuous DDRCC clock (per byte lane)
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY, 0);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY_BYPASS, 1);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRAFFIC_CGC_EN, 1);
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG, BIST_TOP_CGC_EN, 1);     
    }      
      
    // Enable PAD SW override 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQS, 1);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQ, 0x3FF);

    // Disable PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQ, 0);

    //select BIST in datapath
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, WR_DP_SRC_SEL, 0x1);

    // Enable DQS PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 1);

    // Enable DQ PAD IE
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_ENABLE_IE_DQ, 0x3FF);

    // Set SW Enable for DQS pulses
    HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x5);
    
    if (cs ==1)
    {
        HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, DLY_SEL, 0x1);
    } 
  }

  // Loop through each DQ until Done or Error
  while (wrlvl_dqs_done < NUM_DQ_PCH) 
  {
         
    // Setup all of the DQ PHYs
    for (dq = 0; dq < NUM_DQ_PCH; dq++) {

      // Update WRLVL CDC
      if (byte_dqs_done[dq] == 0) 
      {
        if (fine_dqs_started[dq]) {
          DDR_PHY_hal_cfg_cdc_slave_wrlvl(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                          fine_dqs_delay[dq], 
                                          0, // fine 
                                          1, // hp_mode
                                          cs );
        }
        else {
          DDR_PHY_hal_cfg_cdc_slave_wrlvl(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                        coarse_dqs_delay[dq], 
                                        1, // coarse 
                                        1, // hp_mode
                                        cs );
        }

        coarse_dqs_delay_ps = coarse_dqs_delay[dq] * COARSE_STEP_IN_PS;
        fine_dqs_delay_ps   = fine_dqs_delay[dq]   * FINE_STEP_IN_PS;

        // Increment the retimer when the delay exceeds the T/4 period boundaries
        if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T4) {
          dqs_retmr[dq] = 0;
        }
        else if (((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T4) && ((coarse_dqs_delay_ps + fine_dqs_delay_ps) <= T34)) {
          dqs_retmr[dq]  = 1;
        }
        else if ((coarse_dqs_delay_ps + fine_dqs_delay_ps) > T34) {
          dqs_retmr[dq] = 2;
        }

        DDR_PHY_hal_cfg_wrlvl_retmr(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                    cs,
                                    dqs_retmr[dq]);
      }

      //  Repeat the WRLVL test for max_loopcount to make a 1 dimensional histogram
      for (loopcnt = wrlvl_max_loopcnt;loopcnt > 0;loopcnt-- ) 
      {
        // Update the status of bytes that are not done
        if (byte_dqs_done[dq] == 0) {

          // Send a Write Level DQS pulse to all PHYs
          HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x0);
          HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_TRIGGER_CFG, 0x1);          

          bisc_status = DDR_PHY_hal_sta_wrlvl_training(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET));
        
          // Collapse the status to a single bit
          bisc_status = (bisc_status & 0x00000001);

          //  Check the bisc status and increment the one or zero count
          if (bisc_status == 1) {
             byte_one_cnt[dq]++;
          } else {
             byte_zero_cnt[dq]++;
          }

        } // if (!byte_dqs_done
      } // for (loopcnt
    } //  End of loop for a given wrlvl delay value

    
    //=========================================================================
    // Process the Write Level Loop Result
    //=========================================================================
    for (dq = 0; dq < NUM_DQ_PCH; dq++) 
    {
      //-------------------------------------------------------------------------
      // Write Level Result Process Flow Control
      //-------------------------------------------------------------------------

      // Process the DQ if it is not done
      if (byte_dqs_done[dq] == 0) 
      {
      // Calculate the threshold in percent for Write Level pass (histogram)
        if ((byte_one_cnt[dq] + byte_zero_cnt[dq]) != 0) {
          feedback_percent =  ((byte_one_cnt[dq] * 100) / (byte_one_cnt[dq] + byte_zero_cnt[dq]));
        }
        else {
          feedback_percent = 0;
        }
  
        // Generate an Error and terminate if CDC overflows
        if ((coarse_dqs_delay[dq] >= (wrlvl_max_coarse_cdc - 1)) && (fine_dqs_delay[dq] >= wrlvl_max_fine_cdc)) {
          byte_dqs_done[dq]   = 1;
        }
        // Coarse completed and the fine one count is greater than the zero count: wrlvl is complete
        else if ((coarse_dqs_done[dq] == 1) && (fine_dqs_started[dq] == 1) && 
                 (feedback_percent >= wrlvl_feedback_percent)) {
          byte_dqs_done[dq] = 1;
        }
        // Course is complete and fine has not started : subtract one from coarse and start fine
        else if ((fine_dqs_started[dq] == 0) && 
                 ((feedback_percent >= wrlvl_feedback_percent) || (coarse_dqs_delay[dq] == wrlvl_max_coarse_cdc))) {
          coarse_dqs_done[dq] = 1;
          fine_dqs_started[dq] = 1;
          if (coarse_dqs_delay[dq] != 0) {
            coarse_dqs_delay[dq] -= wrlvl_coarse_cdc_step;

            // Update decremented coarse CDC
            DDR_PHY_hal_cfg_cdc_slave_wrlvl(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                          coarse_dqs_delay[dq], 
                                          1, // coarse 
                                          1, // hp_mode
                                          cs );
          }
        }
        // Coarse is complete and the one count is NOT greater than the zero count : increment fine
        else if ((coarse_dqs_done[dq] == 1) && 
                 (feedback_percent < wrlvl_feedback_percent)) {
          fine_dqs_delay[dq] += wrlvl_fine_cdc_step;
        }
        else {
        // Increment coarse delay 
          coarse_dqs_delay[dq] += wrlvl_coarse_cdc_step;
        } 


      }// if !byte_dqs_done

      // Reset byte counters for the next iteration
      byte_one_cnt[dq]  = 0;
      byte_zero_cnt[dq] = 0;

    } // for (dq

    // Aggregate all of the byte status to determine the done state
    wrlvl_dqs_done = 0;
    for (done = 0; done < NUM_DQ_PCH; done++) {
      wrlvl_dqs_done = wrlvl_dqs_done + byte_dqs_done[done];
    }

  } // while (wrlvl_dqs_done<)

  for (dq = 0; dq < NUM_DQ_PCH; dq++) {
   
    dqs_half_cycle[dq] = DDR_PHY_hal_sta_wrlvl_half (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), cs); 
    dqs_full_cycle[dq] = DDR_PHY_hal_sta_wrlvl_full (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), cs);
    // Write trained WRLVL values to the ext registers
    DDR_PHY_hal_cfg_wrlvlext_ctl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET), 
                                        wrlvl_clk_freq_idx, 
                                        cs, 
                                        dqs_retmr[dq], 
                                        dqs_half_cycle[dq],  // half cycle
                                        dqs_full_cycle[dq]); // full cycle

    DDR_PHY_hal_cfg_cdcext_wrlvl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET),
                                        wrlvl_clk_freq_idx, 
                                        cs,
                                        fine_dqs_delay[dq],
                                        coarse_dqs_delay[dq]);

    // Copy rank 0 results to rank 1 
//    if (cs == 0) {

      // Write the trained WRLVL values to the rank 1 fields
      DDR_PHY_hal_cfg_cdc_slave_wrlvl(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                      fine_dqs_delay[dq], 
                                      0, // fine 
                                      1, // hp_mode
                                      cs );

      DDR_PHY_hal_cfg_cdc_slave_wrlvl(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),
                                      coarse_dqs_delay[dq], 
                                      1, // coarse 
                                      1, // hp_mode
                                      cs );

      // // Write  the trained WRLVL values to the rank 1 ext registers
      // DDR_PHY_hal_cfg_wrlvlext_ctl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET), 
                                          // wrlvl_clk_freq_idx, 
                                          // 1, 
                                          // dqs_retmr[dq], 
                                          // 0,  // half cycle
                                          // 0); // full cycle
  
      // DDR_PHY_hal_cfg_cdcext_wrlvl_update(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET),
                                          // wrlvl_clk_freq_idx, 
                                          // 1, // CS
                                          // fine_dqs_delay[dq],
                                          // coarse_dqs_delay[dq]);
//    } // if (cs)

    // Disable PAD OE 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG, SW_PAD_ENABLE_OE_DQ, 0);

    //De-select BIST in datapath
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, WR_DP_SRC_SEL, 0x0);

    // Disable DQ PAD IE
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG, SW_PAD_ENABLE_IE_DQ, 0);

    // Disble WRLVL pulse control
    HWIO_OUTX (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_WRLVL_CTRL_0_CFG, 0x0);

    // Disable PAD SW override 
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQS, 0);
    HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG, SW_PAD_MODE_DQ, 0);

    // Turn off continuous DDRCC clock (per byte lane)
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY, 0); 
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, DISABLE_PHY_BYPASS, 0);
    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRAFFIC_CGC_EN, 0);
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_BIST_TOP_CGC_CFG, BIST_TOP_CGC_EN, 0);     
    } 
    if (cs ==1)
    {
        HWIO_OUTXF (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG, DLY_SEL, 0x0);
    } 
  } // for (dq)
} // DDRSS_wrlvl_dqs
    

void DDRSS_wrlvl_write_flash_params(DDR_STRUCT *ddr,uint8 ch, uint8 cs, uint32 wrlvl_clk_freq_idx)
{
//---------------------------------------------------------------------
// Write the FLASH_PARAMETERS data structure  by reading the registers
//---------------------------------------------------------------------

  uint8   dq                      = 0;// loop counter
  uint8   ca                      = 0;// loop counter
  uint8   final_dq_retmr          = 0;// Final DQS retmr value
  uint8   final_dq_coarse_delay   = 0;// Final DQS coarse value value
  uint8   final_dq_fine_delay     = 0;// Final DQS fine delay value
  uint8   final_dq_full_cycle     = 0;// Final DQS full cycle value
  uint8   final_dq_half_cycle     = 0;// Final DQS half cycle value
  uint8   final_ca_retmr          = 0;// Final CA retmr value
  uint8   final_ca_coarse_delay   = 0;// Final CA coarse value value
  uint8   final_ca_full_cycle     = 0;// Final CA full cycle value
  uint8   final_ca_half_cycle     = 0;// Final CA half cycle value
  uint32  dq0_ddr_phy_base        = 0;// DDR PHY DQ0 base address
  uint32  ca0_ddr_phy_base        = 0;// DDR PHY CA0 base address
 
  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
 
  // Set DDR PHY DQ0 Base
  dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
  ca0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;

    for (ca=0;ca<NUM_CA_PCH;ca++) {

      // Read the CA PHY WRLVL registers
      final_ca_retmr        = DDR_PHY_hal_sta_wrlvl_retmr(ca0_ddr_phy_base  + (ca * DDR_PHY_OFFSET),0);
      final_ca_coarse_delay = DDR_PHY_hal_sta_wrlvl_coarse(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET),0);
      final_ca_full_cycle   = DDR_PHY_hal_sta_wrlvl_full(ca0_ddr_phy_base   + (ca * DDR_PHY_OFFSET),0);
      final_ca_half_cycle   = DDR_PHY_hal_sta_wrlvl_half(ca0_ddr_phy_base   + (ca * DDR_PHY_OFFSET),0);

      //  Store register values into the training data structure
      training_data_ptr->results.wrlvl.ca_dqs_retmr[wrlvl_clk_freq_idx][ch][0][ca]        = final_ca_retmr;
      training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][ca] = final_ca_coarse_delay;
      training_data_ptr->results.wrlvl.ca_dqs_half_cycle[wrlvl_clk_freq_idx][ch][0][ca]   = final_ca_half_cycle;
      training_data_ptr->results.wrlvl.ca_dqs_full_cycle[wrlvl_clk_freq_idx][ch][0][ca]   = final_ca_full_cycle;
     //copy rank0's alue to rank1
      training_data_ptr->results.wrlvl.ca_dqs_half_cycle[wrlvl_clk_freq_idx][ch][1][ca]   = final_ca_half_cycle;
      training_data_ptr->results.wrlvl.ca_dqs_full_cycle[wrlvl_clk_freq_idx][ch][1][ca]   = final_ca_full_cycle;     
    }

    for (dq=0;dq<NUM_DQ_PCH;dq++) {

      // Read the DQ PHY WRLVL registers
      final_dq_retmr        = DDR_PHY_hal_sta_wrlvl_retmr(dq0_ddr_phy_base  + (dq * DDR_PHY_OFFSET),cs);
      final_dq_coarse_delay = DDR_PHY_hal_sta_wrlvl_coarse(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET),cs);
      final_dq_fine_delay   = DDR_PHY_hal_sta_wrlvl_fine(dq0_ddr_phy_base   + (dq * DDR_PHY_OFFSET),cs);
      final_dq_full_cycle   = DDR_PHY_hal_sta_wrlvl_full(dq0_ddr_phy_base   + (dq * DDR_PHY_OFFSET),cs);
      final_dq_half_cycle   = DDR_PHY_hal_sta_wrlvl_half(dq0_ddr_phy_base   + (dq * DDR_PHY_OFFSET),cs);

      //  Store register values into the training data structure
      training_data_ptr->results.wrlvl.dq_dqs_retmr[wrlvl_clk_freq_idx][ch][cs][dq]        = final_dq_retmr;
      training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][cs][dq] = final_dq_coarse_delay;
      training_data_ptr->results.wrlvl.dq_fine_dqs_delay[wrlvl_clk_freq_idx][ch][cs][dq]   = final_dq_fine_delay;
      training_data_ptr->results.wrlvl.dq_dqs_half_cycle[wrlvl_clk_freq_idx][ch][cs][dq]   = final_dq_half_cycle;
      training_data_ptr->results.wrlvl.dq_dqs_full_cycle[wrlvl_clk_freq_idx][ch][cs][dq]   = final_dq_full_cycle;

      #if DSF_WRLVL_LOG_EN
        ddr_printf(DDR_NORMAL,"\n"); 
        ddr_printf(DDR_NORMAL,"    Channel %u Rank %u DQ %u\n",ch,cs,dq); 
        ddr_printf(DDR_NORMAL,"    DQS WRLVL Coarse %u ps Fine %u ps   Total %u ps\n",
                   final_dq_coarse_delay * COARSE_STEP_IN_PS,
                   final_dq_fine_delay   * FINE_STEP_IN_PS,
                  (final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS));
  
          //  DQ2 and DQ3 attached to CA0
        if ((dq == 0) || (dq == 1)) {
          ddr_printf(DDR_NORMAL,"    CA0 WRLVL coarse %u ps\n\n",
                     training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][0] * COARSE_STEP_IN_PS);

          if ((training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][0] * COARSE_STEP_IN_PS) >= 
              ((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS))) {
            ddr_printf(DDR_NORMAL,"    Normalized WRLVL Delay -%u ps\n\n",
                ((training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][0] * COARSE_STEP_IN_PS) - 
                        ((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS))));
          }
          else {
              ddr_printf(DDR_NORMAL,"    Normalized Delay +%u ps\n\n",
                        (((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS)) - 
                          (training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][0] * COARSE_STEP_IN_PS))); 
          }
        }
        //  DQ2 and DQ3 attached to CA1
        else {
            ddr_printf(DDR_NORMAL,"    CA1 WRLVL coarse %u ps\n\n",
                       training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][1] * COARSE_STEP_IN_PS);

            if ((training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][1] * COARSE_STEP_IN_PS) >= 
                ((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS))) {
              ddr_printf(DDR_NORMAL,"    Normalized WRLVL Delay -%u ps\n\n",
                        ((training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][1] * COARSE_STEP_IN_PS) - 
                        ((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS))));
            }
            else {
              ddr_printf(DDR_NORMAL,"    Normalized Delay +%u ps\n\n",
                        (((final_dq_coarse_delay * COARSE_STEP_IN_PS) + (final_dq_fine_delay * FINE_STEP_IN_PS)) - 
                          (training_data_ptr->results.wrlvl.ca_coarse_dqs_delay[wrlvl_clk_freq_idx][ch][0][1] * COARSE_STEP_IN_PS))); 
            }
        } // else
      #endif
    } // for (dq)
} // DDR_PHY_write_flash_params

// EOF
