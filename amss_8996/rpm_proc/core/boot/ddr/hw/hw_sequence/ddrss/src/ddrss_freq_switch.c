/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014,2016 Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/hw/hw_sequence/ddrss/src/ddrss_freq_switch.c#8 $
$DateTime: 2016/05/03 05:33:38 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/07/14   ejose        Initial version
================================================================================*/

#include "ddrss.h"


//================================================================================================//
// DDR Pre-Clock Frequency Switch
//================================================================================================//
boolean HAL_DDR_Pre_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                  uint32 new_clk_khz)
{
    uint8 new_clk_idx = 0;
    uint8 ch = 0;
    uint8 dq = 0;
    uint8 ca = 0;
    uint32 dq0_ddr_phy_base  = 0;
    uint32 ca0_ddr_phy_base  = 0;
    uint8 ddrcc_target_pll = 0;
    uint32 pll_update_rate = 0x22D; // full rate
   
    // Get the clk index in clock plan
    for (new_clk_idx = 0; new_clk_idx < ddr->misc.ddr_num_clock_levels; new_clk_idx++)
    {
        if (new_clk_khz <= ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz)
            break;
    }
    
    BIMC_Pre_Clock_Switch (ddr, channel, curr_clk_khz, new_clk_khz, new_clk_idx);
    
    // Turn on cfg clock used for accessing DDR_PHY/DDR_CC CSRs
    ddr_external_set_ddrss_cfg_clk (TRUE);
    
    // Loop through channels
    for (ch = 0; ch < 2; ch++)
    {
        if ((channel >> ch) & 0x1)
        {
            dq0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            ca0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
            
            // Enable DLL PHY/MC phase clock
            if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0301)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
            {
                for (dq = 0; dq < NUM_DQ_PCH; dq++)
                {
                    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, PH_CGC_MODE, 0 );
                }
                
                for (ca = 0; ca < NUM_CA_PCH; ca++)
                {
                    HWIO_OUTXF(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, PH_CGC_MODE, 0 );
                }
                
                // Increase PLL update rate (low to high frequency switch only)
                if ((curr_clk_khz < F_RANGE_3) && (new_clk_khz >= F_RANGE_3))
                {
                    for (dq = 0; dq < NUM_DQ_PCH; dq++)
                    {
                        HWIO_OUTX(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG, pll_update_rate );
                    }
                    
                    for (ca = 0; ca < NUM_CA_PCH; ca++)
                    {
                        HWIO_OUTX(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG, pll_update_rate );
                    }
                }
            }
            
            // Check if the clock mode is DDRCC to configure DDRCC PLLs
            if (ddr->misc.clock_plan[new_clk_idx].clk_mode == 1)
            {
                ddrcc_target_pll =  ! (HWIO_INXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET,
                                    DDR_CC_DDRCC_PLLCTRL_STATUS, ACTIVE_PLL));

                // Execute target-specific sequence to program the PLLs in the DDRCC.
                // Target-specific code, as DDRCC PLL programming characteristics and CSRs
                // are different across targets. Targets 8992 and 8994 are the same, 8996 is different.
                // Sequence is in target_config.c
                DDR_CC_Pre_Clock_Switch(ddr, ch, new_clk_idx, ddrcc_target_pll);
            }
        }
    }
    
    return TRUE;
}

//================================================================================================//
// DDR Post-Clock Frequency Switch
//================================================================================================//
boolean HAL_DDR_Post_Clock_Switch (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                   uint32 new_clk_khz)
{
    uint8 new_clk_idx  = 0;
    uint8 curr_clk_idx = 0;
    uint8 ch = 0;
    uint8 dq = 0;
    uint8 ca = 0;
    uint32 dq0_ddr_phy_base  = 0;
    uint32 ca0_ddr_phy_base  = 0;
    uint32 pll_update_rate = 0x62D;  // half rate
    
    // Get the clk index in clock plan
    for (new_clk_idx = 0; new_clk_idx < ddr->misc.ddr_num_clock_levels; new_clk_idx++)
    {
        if (new_clk_khz <= ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz)
            break;
    }
    
    for (curr_clk_idx = 0; curr_clk_idx < ddr->misc.ddr_num_clock_levels; curr_clk_idx++)
    {
        if (curr_clk_khz <= ddr->misc.clock_plan[curr_clk_idx].clk_freq_in_khz)
            break;
    }
    
    BIMC_Post_Clock_Switch (ddr, channel, curr_clk_khz, new_clk_khz);
    
    // Loop through channels
    for (ch = 0; ch < 2; ch++)
    {
        if ((channel >> ch) & 0x1)
        {
            dq0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            ca0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
            
            // Disable DLL PHY/MC phase clock
            if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0301)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
            {
                for (dq = 0; dq < NUM_DQ_PCH; dq++)
                {
                    HWIO_OUTXF(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, PH_CGC_MODE, 1 );
                }
                
                for (ca = 0; ca < NUM_CA_PCH; ca++)
                {
                    HWIO_OUTXF(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, PH_CGC_MODE, 1 );
                }
                
                // Decrease PLL update rate (high to low frequency switch only)
                if ((curr_clk_khz >= F_RANGE_3) && (new_clk_khz < F_RANGE_3))
                {
                    for (dq = 0; dq < NUM_DQ_PCH; dq++)
                    {
                        HWIO_OUTX(dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG, pll_update_rate );
                    }
                    
                    for (ca = 0; ca < NUM_CA_PCH; ca++)
                    {
                        HWIO_OUTX(ca0_ddr_phy_base + (ca * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG, pll_update_rate );
                    }
                }
            }
        }
    }
    
    // Turn off cfg clock used for accessing DDR_PHY/DDR_CC CSRs
    ddr_external_set_ddrss_cfg_clk (FALSE);
    
   return TRUE;
}






