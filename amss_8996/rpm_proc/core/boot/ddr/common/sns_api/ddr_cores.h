#ifndef __DDR_CORES_H__
#define __DDR_CORES_H__

/*=============================================================================

                                DDR CORES
                                Header File
GENERAL DESCRIPTION
 This file defines the DDR controller and DDR PHY core data structures


Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/1.6/core/boot/ddr/common/sns_api/ddr_cores.h#2 $
$DateTime: 2015/02/10 15:38:44 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
12/01/13   dp      Initial version.
==============================================================================*/
/*==========================================================================
                               INCLUDE FILES
===========================================================================*/
#include "HALcomdef.h"

/*==============================================================================
                                  TYPES
==============================================================================*/

/**********************/
/*** DDR Controller ***/
/**********************/

struct ddr_ctlr
{
  struct
  {
    uint32 arch;
    uint32 major;
    uint32 minor;
  } version;
};

/**********************/
/*** DDR PHY  ***/
/**********************/

struct ddr_phy
{
    struct
    {
      uint32 major;
      uint32 minor;
    } version;
};

#endif /* __DDR_CORES_H__ */
