:: /** @file pack_build.bat
:: Batch file for packed builds
::
:: Copyright (c) 2015, Qualcomm Technoligies, Inc. All rights reserved.
::
::===========================================================================
::                              EDIT HISTORY
::
::
:: when       who     what, where, why
:: --------   ---     -------------------------------------------------------
:: 06/24/15   bh      Add PackIt cmd line argument
:: 05/11/15   vk      Move to HY11_1
:: 05/10/15   vk      End one level above boot_images to find Build products
:: 05/10/15   vk      Update start directory
:: 05/10/15   vk      Initial revision
::=========================================================================

echo [pack_build.bat] ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo [pack_build.bat] ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo [pack_build.bat] +++++++++++++++++++++++++++++++ pack_build.bat +++++++++++++++++++++++++++++++
echo [pack_build.bat] ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo [pack_build.bat] ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

echo [pack_build.bat] +++++++++++++++++++++++++++++++ pack_build.bat +++++++++++++++++++++++++++++++ > pack_build.log
 
echo [pack_build.bat] CWD: %cd%
set START_DIR=%cd%
echo %START_DIR%

set PACK_BUID_TARGET=%1
set PACKIT_OPTIONS=%2

cd QcomPkg\%PACK_BUID_TARGET%Pkg
if ERRORLEVEL 1 goto Failure
set TARGET=RELEASE

echo [pack_build.bat] Begining %PACK_BUID_TARGET% build ....
call b64.bat RELEASE
if ERRORLEVEL 1 goto Failure
echo [pack_build.bat] CWD: %cd%


echo [pack_build.bat] Return to base [%START_DIR%] ....
cd %START_DIR%
if ERRORLEVEL 1 goto Failure
echo [pack_build.bat] CWD: %cd%

cd ..
echo [pack_build.bat] CWD: %cd%


echo [pack_build.bat] Calling packit.py script ....
if [%2]==[] (
  python boot_images\packit.py -t %PACK_BUID_TARGET% -r 
) else (
  python boot_images\packit.py -t %PACK_BUID_TARGET% -r -k %PACKIT_OPTIONS%
)
if ERRORLEVEL 1 goto Failure
echo [pack_build.bat] CWD: %cd%

echo [pack_build.bat] Return to base [%START_DIR%] ....
cd %START_DIR%
if ERRORLEVEL 1 goto Failure
echo [pack_build.bat] CWD: %cd%

cd ..\HY11_1\boot_images\QcomPkg\%PACK_BUID_TARGET%Pkg
call b64.bat RELEASE
if ERRORLEVEL 1 goto Failure
echo [pack_build.bat] CWD: %cd%

echo [pack_build.bat] Return to base [%START_DIR%] ....
cd %START_DIR%
if ERRORLEVEL 1 goto Failure

echo [pack_build.bat] Done, Exiting ... 
echo [pack_build.bat] Done, Exiting ... >> pack_build.log
cd ..
echo [pack_build.bat] CWD: %cd%
echo [pack_build.bat] CWD: %cd% >> pack_build.log

goto Exit

:Failure
echo [pack_build.bat] ERROR: Failed to build package: %PACK_BUID_TARGET%
echo [pack_build.bat] CWD: %cd%
echo [pack_build.bat] CWD: %cd% >> pack_build.log
echo [pack_build.bat] ERROR: Failed to build package: %PACK_BUID_TARGET% >> pack_build.log
EXIT /B %ERRORLEVEL%

:Exit
EXIT /B 0

ExitError
