/*======================================================================
FILE: GicV3.h
DESCRIPTION:
 This file defines protoypes for assembly implemented functions to read
 and write to GicV3 system registers
 Copyright (c) 2014, Qualcomm Technologies, Inc. All rights reserved.
======================================================================*/

#ifndef __GICV3_H__
#define __GICV3_H__

UINT32
EFIAPI
Read_ICC_CTLR_EL3 (
  UINTN GicInterruptInterfaceBase
  );

VOID
EFIAPI
Write_ICC_CTLR_EL3 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );


UINT32
EFIAPI
Read_ICC_CTLR_EL1 (
  UINTN GicInterruptInterfaceBase
  );

VOID
EFIAPI
Write_ICC_CTLR_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

UINT32
EFIAPI
Read_ICC_PMR_EL1 (
  UINTN GicInterruptInterfaceBase
  );

VOID
EFIAPI
Write_ICC_PMR_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

VOID
EFIAPI
Write_ICC_BPR0_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

VOID
EFIAPI
Write_ICC_BPR1_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

VOID
EFIAPI
Write_ICC_EIOR0_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

VOID
EFIAPI
Write_ICC_EIOR1_EL1 (
  UINTN GicInterruptInterfaceBase,
  UINT32 val
  );

UINT32
EFIAPI
Read_ICC_IAR0_EL1 (
  UINTN GicInterruptInterfaceBase
  );

UINT32
Read_ICC_IAR1_EL1 (
  UINTN GicInterruptInterfaceBase
);

VOID
Write_ICC_IGRPEN1_EL1 (
  UINT32 val
  );

VOID
Write_ICC_SGI1R_EL1 (
  UINT32 val
  );

#endif
