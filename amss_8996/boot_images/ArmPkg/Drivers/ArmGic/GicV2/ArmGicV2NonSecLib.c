/** @file
*
*  Copyright (c) 2014, Qualcomm Technologies, Inc. All rights reserved.
*  Portions Copyright (c) 2011-2014, ARM Limited. All rights reserved.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution.  The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
*
**/

#include <Uefi.h>
#include <Library/IoLib.h>
#include <Library/ArmGicLib.h>

#include "GicV3.h"

VOID
EFIAPI
ArmGicV2EnableInterruptInterface (
  IN  INTN          GicInterruptInterfaceBase
  )
{
  /*
  * Enable the CPU interface in Non-Secure world
  * Note: The ICCICR register is banked when Security extensions are implemented
  */
  if(GicInterruptInterfaceBase != 0) {
  MmioWrite32 (GicInterruptInterfaceBase + ARM_GIC_ICCICR, 0x1);
  }
  else {
    Write_ICC_IGRPEN1_EL1(0x1);
  }
}

VOID
EFIAPI
ArmGicV2DisableInterruptInterface (
  IN  INTN          GicInterruptInterfaceBase
  )
{
  // Disable Gic Interface
  if(GicInterruptInterfaceBase != 0) {
  MmioWrite32 (GicInterruptInterfaceBase + ARM_GIC_ICCICR, 0x0);
  }
  else {
    Write_ICC_IGRPEN1_EL1(0x0);
  }
  Write_ICC_PMR_EL1(GicInterruptInterfaceBase, 0x0);
}
