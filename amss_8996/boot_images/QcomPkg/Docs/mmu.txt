/*=============================================================================
   MMU Register spec for help during debug/analysis

  Copyright (c) 2014, Qualcomm Inc. All rights reserved.

                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/18/14   yg      Initial version
=============================================================================*/

ARM v7
First :   TTBR0

L1 Table:
  [1-0]
   0 1 : Page table
   1 0 : Section or Super section (Bit 18 determines)
   1 1 : Rsrv
   0 0 : Fault


    0 1 : Page table
     [31 - 10] : Base Addr :Pointer to the base address of a L2 page table
     9         : 0
     [ 8 -  5] : Domain
     4         : SBZ
     3         : NS
     2         : PXN (Physical Addr Extension)



    1 0 : Section or Super section (Bit 18 determines)
     [31 - 20] : Base Addr : PA[31:20]
     19        : NS (Non Secure)
     18        : 0 => Section, 1 => Super Section
     17        : nG (Not Global)
     16        : S Shareable
     15        : AP[2]       0 => RW  1 => RO
     [14 - 12] : TEX[2:0]   Region attributes
     [11 - 10] : AP[1:0]     00 => No Access, others look at AP[2]
     9         : Imp
     [8 - 5]   : Domain
     4         : XN
     3         : C  : Cacheable
     2         : B  : Buffered


L2 Table:
   [1 0]
    0 0 : Fault
    0 1 : Large Page
    1 x : Small Page


    0 1 : Large Page:
    [31 16] : = PA[31:16]
    15      : XN
    [14:12] : TEX[2:0]
    11      : nG
    10      : S Shareable
    9       : AP[2]
    [8:6]   : SBZ
    [5:4]   : AP[1:0]
    3       : C
    2       : B
    [1:0]   : Descriptor type Large page

    1 x : Small Page
    [31 12] : = PA[31:12]
    11      : nG
    10      : S Shareable
    9       : AP[2]
    [8:6]   : TEX[2:0]
    [5:4]   : AP[1:0]
    3       : C
    2       : B
    1       : Descriptor type Small page
    0       : XN

----------------------------------------------------------------------

Memory Region Attributes: (Cache policy)
TEX[2:0]  C  B

  0       0  0   Strongly ordered
             1   Shareable device
          1  0   Write through (no write allocate)
             1   Write back (no write allocate)

  1       0  0  Non cacheable
          1  1  Write back (write allocate)

  2       0  0  Non shareable device
-------------------------------------------------------------------

Access Permissions:
AP[2] AP[1:0]
PL0 => User or App level
PL1 => OS Kernel or supervisor level
PL2 => Hypervisor level

AP[2 1 0]   PL1,PL2   PL0
   0 0 0     NO        NO
   0 0 1     RW        NO
   0 1 0     RW        RO
   0 1 1     RW        RW
   1 0 0     RESERVED
   1 0 1     RO        NO
   1 1 0     RO        RO
   1 1 1     RO        RO

-----------------------------------------------------------------
