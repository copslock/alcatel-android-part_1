#ifndef BOOT_EXTERN_TLMM_INTERFACE_H
#define BOOT_EXTERN_TLMM_INTERFACE_H
/*===========================================================================

                    BOOT EXTERN TLMM DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external tlmm drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2011-2012, 2014-2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/24/15   ck      Added boot_gpio_configure and boot_gpio_assertion
09/23/14   ck      Removed feature flag as API is always enabled in XBL
07/18/12   kpa     Change boot_gpio_init return type to True.
10/19/11   dh      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"
#include "HALtlmm.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/*===========================================================================

  FUNCTION GPIO_INIT

  DESCRIPTION
        This function initializes the GPIOs for the TLMM configuration
        specified at boot time.

  PARAMETERS
    None.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    It uses the stack

===========================================================================*/
boolean boot_gpio_init(void);


/*===========================================================================

  FUNCTION boot_gpio_config

  DESCRIPTION
    This function configures a GPIO's drive strength and if it is
    a pull up or pull down.

  PARAMETERS
    uint32 gpio_number
    uint32 gpio_function_select,
    HAL_tlmm_PullType gpio_pull_type,
    HAL_tlmm_DriveType gpio_drive_strength,
    HAL_tlmm_DirType gpio_output_enable

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void boot_gpio_configure(uint32 gpio_number,
                         uint32 gpio_function_select,
                         HAL_tlmm_PullType gpio_pull_type,
                         HAL_tlmm_DriveType gpio_drive_strength,
                         HAL_tlmm_DirType gpio_output_enable);


/*===========================================================================

  FUNCTION boot_gpio_assertion

  DESCRIPTION
    This function either drives a GPIO high or low.

  PARAMETERS
    uint32 gpio_number
    boolean gpio_assertion

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void boot_gpio_assertion(uint32 gpio_number,
                         boolean gpio_assertion);


#endif /* BOOT_EXTERN_TLMM_INTERFACE_H */
