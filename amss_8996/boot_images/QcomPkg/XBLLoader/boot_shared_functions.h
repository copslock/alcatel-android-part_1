#ifndef BOOT_SHARED_FUNCTIONS_H
#define BOOT_SHARED_FUNCTIONS_H

/*===========================================================================

                                Boot Shared Functions
                                Header File

GENERAL DESCRIPTION
  This header file contains declarations and definitions for Boot's shared 
  functions. 

Copyright 2014-2015 by QUALCOMM Technologies Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ----     ----------------------------------------------------------
09/16/15   kpa      Added dcache_inval_region
08/24/15   ck       Added boot_dload_transition_pbl_forced_dload
08/01/15   kpa      Added dcache_flush_region
07/28/15   rp       Added Clock_SetBIMCSpeed
07/11/15   rp	    Changed boot_enable_led function declaration
06/25/15   as       Added sbl1_get_shared_data and boot_sbl_qsee_interface_get_image_entry
04/23/15   kpa      Added pmic and hotplug apis
10/01/14   ck       Removed Hotplug functions
09/30/14   ck       Added boot_err_fatal
09/30/14   ck       Removing efs functions as EFS driver properly split
09/23/14   ck       Added boot_extern_crypto_interface functions
09/23/14   ck       Added boot_extern_efs_interface functions
09/23/14   ck       Added boot_extern_seccfg_interface functions
08/05/14   ck       Updated boot_clobber_check_global_whitelist_range prototype
07/14/14   ck       Initial creation
============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include BOOT_PBL_H
#include "boot_comdef.h"
#include "boot_cache_mmu.h"
#include "boot_clobber_prot.h"
#include "boot_ddr_info.h"
#include "boot_dload.h"
#include "boot_error_handler.h"
#include "boot_extern_crypto_interface.h"
#include "boot_extern_hotplug_interface.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_seccfg_interface.h"
#include "boot_flash_dev_if.h"
#include "boot_logger.h"
#include "boot_sdcc.h"
#include "boot_visual_indication.h"
#include "boothw_target.h"
#include "sbl1_hw.h"
#include "sbl1_mc.h"
#include "pm_smbchg_usb_chgpth.h"
#include "qusb_pbl_dload_check.h"
#include "ClockBoot.h"
#include "tct.h"
/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
#define SHARED_FUNCTIONS_VERSION           5
#define SHARED_FUNCTIONS_MAGIC_COOKIE_1    0x8420C107
#define SHARED_FUNCTIONS_MAGIC_COOKIE_2    0x3B9A68DF


/* Single structure that houses all of the functions to be shared between
   XBLLoader and any other image running at the same time as XBLLoader.
   I.E. XBLDebug */

typedef struct
{
  uint64 version;
  uint32 magic_cookie_1;
  uint32 magic_cookie_2;


  CeMLErrorType(* boot_CeMLDeInit)(void);

  CeMLErrorType(* boot_CeMLHashDeInit)(CeMLCntxHandle **);

  CeMLErrorType(* boot_CeMLHashFinal)(CeMLCntxHandle *,
                                      CEMLIovecListType *); 

  CeMLErrorType(* boot_CeMLHashInit)(CeMLCntxHandle **,
                                     CeMLHashAlgoType);

  CeMLErrorType(* boot_CeMLHashSetParam)(CeMLCntxHandle *,
                                         CeMLHashParamType, 
                                         const void *, 
                                         uint32,
                                         CeMLHashAlgoType);

  CeMLErrorType(* boot_CeMLHashUpdate)(CeMLCntxHandle *,
                                       CEMLIovecListType);

  CeMLErrorType(* boot_CeMLInit)(void);

  void(* boot_clobber_clear_whitelist_table)(void);

  boolean(* boot_clobber_check_global_whitelist_range)(const void *,
                                                       uintnt);

  void(* boot_dload_transition_pbl_forced_dload)(void);

  void(* boot_dload_set_cookie)(void);

  boot_boolean(* boot_enable_led)(uint32, boot_boolean);

  void(* boot_err_fatal)(void);

  void(* boot_error_handler)(const char *,
                             uint32,
                             uint32);

  boot_flash_dev_if_type *(* boot_flash_dev_if_get_singleton)(void);

  sbl_if_shared_ddr_device_info_type *(* boot_get_ddr_info)(void);

  void(* boot_hw_reset)(boot_hw_reset_type);

  void(* boot_install_error_callback)(bl_error_callback_func_type,
                                      void *,
                                      bl_error_callback_node_type *);

  void (* boot_log_message)(char *);

/*[FEATURE]-Add-by TCTNB.(LWS), 2016/01/20, FR-1195511  [system]Dump method improvement . */
#ifdef FEATURE_TCTNB_DUMPSDCARD
  void (* boot_read_flash_partition)(uint8 * , void* ,uint32 , uint32 );

  void (* boot_log_start_timer)(void );

  uint32 (* boot_log_stop_timer)(char * message);

  struct boot_log_meta_info* (* boot_log_get_meta_info )(void ) ;
#endif
/*[FEATURE]-Add-End-by TCTNB.(LWS), 2016/01/20, FR-1195511 . */

  boot_boolean(* boot_pbl_is_auth_enabled)(void);

  pm_err_flag_type(* boot_pm_dev_get_power_on_reason)(unsigned,
                                                      uint64 *);

  boot_boolean(* boot_toggle_led)(void);

  void(* boot_toggle_led_init)(void);

  boot_boolean(* boot_qsee_is_memory_dump_allowed)(secboot_verified_info_type *);

  boot_boolean(* boot_qsee_is_retail_unlocked)(secboot_verified_info_type *);

  void(* boot_qsee_zero_peripheral_memory)(void);

  int32(* boot_sbl_qsee_interface_get_image_entry)(boot_sbl_qsee_interface * sbl_qsee_interface,
                                                    secboot_sw_type image_id);

  void(* mmu_flush_cache)(void);

  bl_shared_data_type * (* sbl1_get_shared_data)(void);

  void(* sbl_error_handler)(void);

  uint32(* sbl1_hw_get_reset_status)(void);

  void(* sbl1_load_ddr_training_data_from_partition)(uint8 *,
                                                     uint32);

  void(* sbl1_save_ddr_training_data_to_partition)(uint8 *, uint32, uint32);
  uint64 (*hotplug_get_partition_size_by_image_id)( image_type image_id );                                                     
  
  boolean(* dev_sdcc_write_bytes)(void *, uint64, uint32, image_type);

  /*API's used by USB driver */

  pm_err_flag_type(* pm_smbchg_usb_chgpth_irq_status)(uint32, 
                                                      pm_smbchg_usb_chgpth_irq_type, 
                                                      pm_irq_status_type, 
                                                      boolean *);
  boolean(* qusb_pbl_dload_check)(void);

  boolean(*Clock_SetBIMCSpeed)(uint32 );
  
  void (* dcache_flush_region)(void *addr, unsigned int length);  
  
  void (* dcache_inval_region)(void *addr, unsigned int length);  
  
} boot_shared_functions_type;


#endif /* BOOT_SHARED_FUNCTIONS */


