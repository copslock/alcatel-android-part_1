/*===========================================================================

                                boot profiler 
                                Source File

GENERAL DESCRIPTION
  This source file contains C definitions for 
  Boot Profiler functionality.
  Time unit in boot profiler is microsecond(10^-6 second).

Copyright  2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who          what, where, why
--------   --------     ------------------------------------------------------
06/22/15   elt          Fixed to use outer level BOOT_PROFILER_FEATURE define 
                        to control compilation 
02/27/15   plc          Initial revision
============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_profiler.h"
#include "boot_logger.h"
#include <stdio.h>
#include <stdarg.h>
/*===========================================================================

                        LOCAL GLOBAL DEFINITIONS

===========================================================================*/


/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

#ifdef BOOT_PROFILER_FEATURE
/*===========================================================================

**  Function : boot_format_string

** ==========================================================================
*/
/*!
* 
* @brief  
*   Handles string with %x, %d, etc arguments, and generates formatted string
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
*  
*/
#define boot_format_string(out_string, out_string_size, in_string, ...) \
  va_list args; \
  va_start(args, in_string); \
  vsnprintf(out_string, out_string_size, in_string, args); \
  va_end(args);  

/*===========================================================================

**  Function : boot_prof_message

** ==========================================================================
*/
/*!
* 
* @brief  
*   Format's string and arguments passed in, and passes to boot_log_message()
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
*  
*/
void boot_prof_message(char *xxstring, ...)
{
  char message[BOOT_LOG_TEMP_BUFFER_SIZE];
  
  boot_format_string(message, sizeof(message), xxstring, (##__VA_ARGS__));
  boot_log_message(message);
}

/*===========================================================================

**  Function : boot_prof_start_timer

** ==========================================================================
*/
/*!
* 
* @brief  
*   Format's string and arguments passed in, and passes to boot_log_message()
*   and starts the timer via boot_log_start_timer()
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
*  
*/
void boot_prof_start_timer(char *xxstring, ...)
{
  char message[BOOT_LOG_TEMP_BUFFER_SIZE];

  boot_format_string(message, sizeof(message), xxstring, ##__VA_ARGS__);
  boot_log_message(message);
  boot_log_start_timer();
}

/*===========================================================================

**  Function : boot_prof_stop_timer

** ==========================================================================
*/
/*!
* 
* @brief  
*   Format's string and arguments passed in, and passes to 
*   boot_log_stop_timer()
*
* @par Dependencies
*   None
*
* @retval
*   None
* 
* @par Side Effects
*   None
*  
*/
void boot_prof_stop_timer(char *xxstring, ...)
{
  char message[BOOT_LOG_TEMP_BUFFER_SIZE];

  boot_format_string(message, sizeof(message), xxstring, ##__VA_ARGS__);
  boot_log_stop_timer(message);
}
#endif /* BOOT_PROFILER_FEATURE */
