/*===========================================================================

                    BOOT EXTERN TLMM DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external tlmm drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2011, 2014-2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/24/15   ck      Added boot_gpio_configure and boot_gpio_assertion
09/26/14   ck      Call to new TlmmLib
10/19/11   dh      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "TlmmLib.h"
#include "HALhwio.h"
#include "HALbootHWIO.h"
#include "boot_extern_tlmm_interface.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/*===========================================================================

  FUNCTION GPIO_INIT

  DESCRIPTION
        This function initializes the GPIOs for the TLMM configuration
        specified at boot time.

  PARAMETERS
    None.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    It uses the stack

===========================================================================*/
boolean boot_gpio_init()
{
  return tlmm_gpio_init();
}


/*===========================================================================

  FUNCTION boot_gpio_config

  DESCRIPTION
    This function configures a GPIO's drive strength and if it is
    a pull up or pull down.

  PARAMETERS
    uint32 gpio_number
    uint32 gpio_function_select,
    HAL_tlmm_PullType gpio_pull_type,
    HAL_tlmm_DriveType gpio_drive_strength,
    HAL_tlmm_DirType gpio_output_enable

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void boot_gpio_configure(uint32 gpio_number,
                         uint32 gpio_function_select,
                         HAL_tlmm_PullType gpio_pull_type,
                         HAL_tlmm_DriveType gpio_drive_strength,
                         HAL_tlmm_DirType gpio_output_enable)
{
  /* Set output enable */
  HWIO_TLMM_GPIO_CFGn_OUTMI(gpio_number,
                            HWIO_TLMM_GPIO_CFGn_GPIO_OE_BMSK,
                            gpio_output_enable << HWIO_TLMM_GPIO_CFGn_GPIO_OE_SHFT);

  /* Set the drive strength */
  HWIO_TLMM_GPIO_CFGn_OUTMI(gpio_number,
                            HWIO_TLMM_GPIO_CFGn_DRV_STRENGTH_BMSK,
                            gpio_drive_strength << HWIO_TLMM_GPIO_CFGn_DRV_STRENGTH_SHFT);

  /* Set the function select value */
  HWIO_TLMM_GPIO_CFGn_OUTMI(gpio_number,
                            HWIO_TLMM_GPIO_CFGn_FUNC_SEL_BMSK,
                            gpio_function_select << HWIO_TLMM_GPIO_CFGn_FUNC_SEL_SHFT);

  /* Set the pull type */
  HWIO_TLMM_GPIO_CFGn_OUTMI(gpio_number,
                            HWIO_TLMM_GPIO_CFGn_GPIO_PULL_BMSK,
                            gpio_pull_type << HWIO_TLMM_GPIO_CFGn_GPIO_PULL_SHFT);
}


/*===========================================================================

  FUNCTION boot_gpio_assertion

  DESCRIPTION
    This function either drives a GPIO high or low.

  PARAMETERS
    uint32 gpio_number
    boolean gpio_assertion

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void boot_gpio_assertion(uint32 gpio_number,
                         boolean gpio_assertion)
{
  HWIO_TLMM_GPIO_IN_OUTn_OUTMI(gpio_number,
                               HWIO_TLMM_GPIO_IN_OUTn_GPIO_OUT_BMSK,
                               gpio_assertion << HWIO_TLMM_GPIO_IN_OUTn_GPIO_OUT_SHFT);
}
