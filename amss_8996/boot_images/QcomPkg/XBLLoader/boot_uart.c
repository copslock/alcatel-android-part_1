/*===========================================================================

                    BOOT UART DRIVER 

DESCRIPTION
  Contains wrapper definition for external uart drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2013, 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/17/14   kedara  Update Uart Driver api's.
08/20/13   lm      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "boot_uart.h"

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 


/*===========================================================================

**  Function :  boot_uart_init

** ==========================================================================
*/
/*!
* 
* @brief
* This wrapper funcion will call the uart driver function which will
* initiate the uart driver for Tx/Rx operation.
*
*@param[in] 
*  None
*
* @par Dependencies
*  None
*   
* @retval
*  boolean:  TRUE if success.
* 
* @par Side Effects
*  None
* 
*/
boolean boot_uart_init(void)
{
  uart_initialize();

  /* uart init does not return any status currently */
  return TRUE;
}


/* ========================================================================
**  Function : boot_uart_receive
** ======================================================================*/
/*!
* 
* @brief
* This wrapper funcion will call the uart driver receive function which will
* receive the data comming from the uart port.
*
*@param[in] buf - receive buffer
*
* @param[in] bytes_to_rx - receive data size

* @par Dependencies
*   None
*   
* @retval
*   Received Bytes
* 
* @par Side Effects
*   None
* 
*/
uint32 boot_uart_rx(char* buf, uint32 bytes_to_rx)
{
  return uart_read( (UINT8 *)buf, bytes_to_rx);
}

/* ============================================================================
**  Function : boot_uart_transmit
** ============================================================================
*/
/*!
* 
* @brief
* This wrapper funcion will call the uart driver transmit function which will
* transmit the data from the uart port.
*
*@param[in] buf - transmit buffer
*
* @param[in] bytes_to_tx - transmit data size

* @par Dependencies
*  None
*   
* @retval
*  Transmitted Bytes
* 
* @par Side Effects
*  None
* 
*/
uint32 boot_uart_tx(char* buf, uint32 bytes_to_tx)
{
  UINT8* msg_buff;
  uintnt bytes_remaining, bytes_sent;

  bytes_remaining = bytes_to_tx;
  msg_buff = (UINT8 *) buf;

  while (bytes_remaining)
  {
    bytes_sent = uart_write (msg_buff, bytes_remaining);
    msg_buff += bytes_sent;
    bytes_remaining -= bytes_sent;
  }

  return bytes_to_tx;  
}
