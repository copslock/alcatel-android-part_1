/** @file Sec.c
  C Entry point for the SEC. First C code after the reset vector.
  
  Copyright (c) 2010-2015, Qualcomm Technologies, Inc. All rights reserved.
  Portions Copyright (c) 2008 - 2009, Apple Inc. All rights reserved.

  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 07/24/15   vk      Make error message as INFO
 07/15/15   bh      Get config value after initializing shared libraries
 06/24/15   bh      Add placeholder for HaltBootOnFuseBlown logic in cfg file
 05/19/15   vk      Support STI 32 bit
 05/18/15   jb      Check SPI as a valid boot device.
 05/15/15   jb      Move adding FV mem regions to after late init
 04/17/15   jb      Change output to occur even in release builds
 04/17/15   vk      Move to start initially from RAM partition entry with FD
 03/02/15   jb      Add option to skip DBI setup
 02/12.15   dg      Build Mem Alloc Hob for regions marked EfiRuntimeServicesData
 02/12/15   vk      Enable crashdump handling
 02/11/15   vk      Disable crashdump handling
 01/22.15   jb      Add UefiDebugModeEntry loop
 11/13/14   jb      Add ability to skip early cache init
 10/30/14   vk      Add RAM parition table lib
 10/18/14   jb      Update values to 64bits, Single resource memory allocation
 10/30/14   vk      New cfg file, support for 4GB DDR
 09/04/14   na      Add AU and Version info to DisplayEarlyInfo
 07/09/14   vk      Do not handle crashdump in PRE_SIL
 06/06/14   yg      Limit memory to 2GB
 06/03/14   vk      Support for 4GB DDR
 05/22/14   vk      Remove limiting DDR size to be 2GB
 05/15/14   shireesh removed support for SDI dump version 0 and Added support for SDI dump ver 1
 05/08/14   vk      Move to updated InfoBlock
 04/22/14   aus     Fixed to use ImemCookieBase instead of DloadCookieBase
 04/10/14   niting  Fixed warnings
 04/09/14   vk      Update InitFBPT signature
 04/01/14   cpaulo  Removed call to TimerInit since it's not required
 03/18/14   vk      Fix gcc warnings
 02/28/14   vk      Print warning for RAM partition table v0
 02/07/14   vk      Add stack canary init
 01/21/14   vk      Update adding memory region hob for absolute address
 11/26/13   vk      Remove stack zero out
 11/15/13   vk      Remove carve out support
 10/22/13   vk      Update to use MAX_ADDRESS
 08/29/13   niting  Limit DDR size to be 2GB
 07/26/13   vk      Support for RAM partition v1
 07/24/13   yg      Reduce logging
 06/25/13   niting  Always setup HOB for legacy UEFI dump
 05/17/13   vk      Initialize cache early
 04/03/13   niting  Added offline crash dump support
 03/19/13   vk      Add hob for InfoBlock
 03/14/13   yg      Print UEFI Start Time
 03/12/13   vk      Add support for SmemNullLib
 02/29/13   vk      Add carve out mode to InfoBlk
 02/15/13   vk      Print AU info
 02/11/13   niting  Use UefiCfgLib to initialize mass storage cookie address
 02/06/13   vk      Print cfg file, handle incase ASSERT is disabled
 01/29/13   vk      Branch from 8974 for target independent version

=============================================================================*/

/*---------------------------------------------------------------------------*/
/*  NOTE: SEC is common for all targets, changes must be target independent  */
/*---------------------------------------------------------------------------*/

#include <PiPei.h>

#include <Pi/PiHob.h>
#include <Library/DebugLib.h>
#include <Library/PrePiLib.h>
#include <Library/PcdLib.h>
#include <Library/IoLib.h>
#include <Library/HobLib.h>
#include <Library/ArmLib.h>
#include <Library/PeCoffGetEntryPointLib.h>
#include <Library/DebugAgentLib.h>
#include <Ppi/GuidedSectionExtraction.h>
#include <Guid/LzmaDecompress.h>
#include <Library/SerialPortLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PrintLib.h>

#include <Library/RamPartitionTableLib.h>
#include <Include/UefiInfoBlk.h>
#include <Library/QcomTargetLib.h>
#include <Library/QcomTimerLib.h>
#include <Library/QcomLib.h>
#include <Library/TimerLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/FBPTLib.h>
#include <Library/ProcAsmLib.h>
#include <Library/PerformanceLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/DBIDump.h>
#include <Library/TargetInitLib.h>
#include <Library/OfflineCrashDump.h>
#include <Library/BootConfig.h>
#include <ReleaseInfo.h>

#include "LzmaDecompress.h"
#include "UefiPlatCfg.h"
#include "ShLibInstall.h"
#include "SerialPortShLibInstall.h"
#include "UefiCfgLibInstall.h"

//
// Define the maximum debug and assert message length that this library supports
//
#define MAX_DEBUG_MESSAGE_LENGTH  0x100

/* Global Variables */
STATIC UINT64 SharedIMEMBaseAddr = 0;

EFI_STATUS EFIAPI ExtractGuidedSectionLibConstructor (VOID);
EFI_STATUS EFIAPI LzmaDecompressLibConstructor (VOID);
EFI_STATUS EarlyCacheInit (UINTN MemBase, UINTN MemSize);
UefiInfoBlkType* InitInfoBlock (UINTN Address);
UefiInfoBlkType* GetInfoBlock (VOID);
VOID* AddInfoBlkHob (VOID);
VOID ArchInitialize (VOID);

STATIC VOID
UefiDebugModeEntry(VOID)
{
  UINT32 UefiDebugCookie;
  UINTN UefiDebugCookieAddr;
  volatile UINT32  *UefiDebugCookiePtr;
  UefiDebugCookie = PcdGet32(PcdUefiDebugCookie);

  UefiDebugCookieAddr = (UINTN)(PcdGet32(PcdIMemCookiesBase) + PcdGet64(PcdUefiDebugCookieOffset));
  UefiDebugCookiePtr = (UINT32 *)UefiDebugCookieAddr;

  /* Loop here to wait for jtag attach if cookie value matches*/
  while( *UefiDebugCookiePtr == UefiDebugCookie );
}

VOID
BuildMemoryTypeInformationHob (
  VOID
  );

STATIC VOID 
EFIAPI
SerialPrint (
  IN  CONST CHAR8  *Format,
  ...
  )
{
  CHAR8    AsciiBuffer[MAX_DEBUG_MESSAGE_LENGTH];
  UINTN    CharCount;
  VA_LIST  Marker;

  //
  // If Format is NULL, then ASSERT().
  //
  ASSERT (Format != NULL);

  //
  // Convert the DEBUG() message to a Unicode String
  //
  VA_START (Marker, Format);
  CharCount = AsciiVSPrint (AsciiBuffer, sizeof (AsciiBuffer), Format, Marker);
  VA_END (Marker);

  SerialPortWrite ((UINT8 *) AsciiBuffer, CharCount);
}

STATIC VOID
UartInit (VOID)
{
  UINT32 AbsTimems;

  AbsTimems = GetTimerCountms ();

  SerialPortInitialize ();

  if (PRODMODE_ENABLED)
    SerialPrint ("\n\nUEFI Start : %d mS\nPROD Mode  : On\n", AbsTimems);
  else
    SerialPrint ("\n\nUEFI Start : %d mS\nPROD Mode  : Off\n", AbsTimems);

  if (DEBUGMODE_ENABLED)
    SerialPrint ("DEBUG Mode : On\n");
  else
    SerialPrint ("DEBUG Mode : Off\n");
}

STATIC VOID
DisplayEarlyInfo(VOID)
{
  EFI_STATUS Status;
  CHAR8 ConfigFileName[32];
  UINTN FileNameBuffLen = sizeof (ConfigFileName);

  Status = GetConfigString("PlatConfigFileName", ConfigFileName, &FileNameBuffLen);
  if (EFI_ERROR(Status) || (FileNameBuffLen == 0))
    DEBUG ((EFI_D_ERROR, "Error reading config file name\n"));
  else
  {
    if ((ConfigFileName != NULL) && (FileNameBuffLen != 0))
      DEBUG ((EFI_D_WARN, "CONF File  : %a\n", ConfigFileName));
  }

  if ((REL_LABEL != NULL) && (BUILD_VER4 != NULL))
    SerialPrint ("FW Version : %a.%a\n", REL_LABEL, BUILD_VER4);

  if(sizeof (UINTN) == 0x8)
    SerialPrint ("Build Info : 64b %a %a\n", __DATE__, __TIME__);
  else
    SerialPrint ("Build Info : 32b %a %a\n", __DATE__, __TIME__);

  if (boot_from_ufs()) 
    DEBUG ((EFI_D_ERROR,   "Boot Device: UFS\n"));
  else if (boot_from_emmc())
    DEBUG ((EFI_D_ERROR,   "Boot Device: eMMC\n"));
  else if (boot_from_spi_nor())
    DEBUG ((EFI_D_ERROR,   "Boot Device: SPI\n"));
  else
  {
     DEBUG ((EFI_D_ERROR, "Unable to determine boot device ...\n"));
     ASSERT (FALSE);
     CpuDeadLoop();
  }
}


/**
  Build HOB for the memory region

  @param  MemRegion       pointer to the memory region

  @retval EFI_SUCCESS     Successfully retrieves memory base and size
  @retval EFI_INVALID_PARAMETER  The RAM partition table is invalid
**/
STATIC EFI_STATUS
BuildMemoryHob (IN MemRegionInfo    *pMemRegion)
{
  /* Make sure the region's end address doesn't exceed the MAX_ADDRESS) */
  ASSERT (pMemRegion->MemBase < MAX_ADDRESS);
  ASSERT ((pMemRegion->MemBase + pMemRegion->MemSize) < MAX_ADDRESS);

  /* Build ResourceHob */
  if (pMemRegion->BuildHobOption != AllocOnly) {
    BuildResourceDescriptorHob (pMemRegion->ResourceType,
                                pMemRegion->ResourceAttribute,
                                pMemRegion->MemBase,
                                pMemRegion->MemSize);
  }

  if (pMemRegion->ResourceType == EFI_RESOURCE_SYSTEM_MEMORY || pMemRegion->MemoryType == EfiRuntimeServicesData)
  {
    /* Build MemoryAllocationHob */
    BuildMemoryAllocationHob (pMemRegion->MemBase,
                              pMemRegion->MemSize,
                              pMemRegion->MemoryType);
  }

  return EFI_SUCCESS;
}

/* Build HOB for the FV type specified */
/* Currently only EFI_HOB_TYPE_FV2 is handled */
STATIC VOID
BuildMemHobForFv (IN UINT16 Type)
{
  EFI_PEI_HOB_POINTERS      HobPtr;
  EFI_HOB_FIRMWARE_VOLUME2  *Hob = NULL;

  HobPtr.Raw = GetHobList ();
  while ((HobPtr.Raw = GetNextHob (Type, HobPtr.Raw)) != NULL)
  {
    if (Type == EFI_HOB_TYPE_FV2)
    {
      Hob = HobPtr.FirmwareVolume2;
      /* Build memory allocation HOB to mark it as BootServicesData */
      BuildMemoryAllocationHob(Hob->BaseAddress, EFI_SIZE_TO_PAGES(Hob->Length) * EFI_PAGE_SIZE, EfiBootServicesData);
    }
    HobPtr.Raw = GET_NEXT_HOB (HobPtr);
  }
}

/* Initialize the cycle counter to track performance */
STATIC VOID
StartCyclCounter (VOID)
{
  UINTN RegVal;
  UINT32 Val;
  UINT64 Scale;
  UINT32 KraitClkMhz;

  /* User mode enable to read in non secure mode */
  WriteUserEnReg (1);

  /* Reset counters */
  RegVal = (0x41 << 24) |  /* IMP */
           (4 << 11)    |  /* N */
           (1 << 3)     |  /* 1/64 */
           (1 << 2);       /* Reset CCNT */
  WritePMonCtlReg (RegVal);

  Val = ReadCycleCntReg();

  /* Scale bootcounter running at 32KHz to 918 MHz, counting every 64 cycles.
     At 918 MHz, each tick is ~1 nanosec, time in nano seconds which is
     almost number of ticks at 918 MHz, here Scale ~= 1, and divide
     by 64 (Right shift 6) to set Cycle Counter Start Value */
  KraitClkMhz = PcdGet32(PcdKraitFrequencyMhz);
  Scale = (1000000000/( KraitClkMhz *1000000));
  Val =  Scale * BootGetTimeNanoSec();
  Val = (Val >> 6);
  WriteCycleCntReg(Val);

  /* Check if write went through */
  ReadCycleCntReg();

  /* Enable Cycle counter */
  WriteCntEnSetReg (((UINT32)1 << 31));

  /* Check if we start counting */
  ReadCycleCntReg();

  /* Enable CCNT */
  RegVal = (0x41 << 24) |  /* IMP */
           (4 << 11)    |  /* N */
           (1 << 3)     |  /* 1/64 */
           (1);            /* Enable all counters */
  WritePMonCtlReg (RegVal);

  /* Disable User mode access */
  WriteUserEnReg (0);

  /* Write to TPIDRURW */
  WriteTPIDRURWReg (0x56430000);

  /* Write to TPIDRURO */
  WriteTPIDRUROReg (0);

  /* Example to Read the counter value, Should read small */
  ReadCycleCntReg();
}

#define MAX_SERIAL_PORT_BUFFER   (64 * 1024)
STATIC EFI_STATUS
InitSharedLibs (VOID)
{
  EFI_STATUS Status;
  MemRegionInfo* MemRegions = NULL;
  UINTN MemRegionsCnt = 0;
  ConfigPair* ConfigTablePtr;
  UINTN ConfigCnt;
  CHAR8* SerialLogBuffer;
  UefiInfoBlkType* UefiInfoBlkPtr;
  UINT32 BufferSz;

  /* Get info block */
  UefiInfoBlkPtr = GetInfoBlock();

  if (UefiInfoBlkPtr == NULL)
    return EFI_OUT_OF_RESOURCES;

  ShLibMgrInit ();

  /* Get Memory map from parsed cfg file */
  GetMemRegionInfo(&MemRegions, &MemRegionsCnt);
  UefiInfoBlkPtr->MemTablePtr = (UINTN*) MemRegions;
  UefiInfoBlkPtr->MemTableCount = MemRegionsCnt;

  /* Get config table from parsed config file */
  GetConfigTable(&ConfigTablePtr, &ConfigCnt);
  UefiInfoBlkPtr->ConfigTablePtr = (UINTN*) ConfigTablePtr;
  UefiInfoBlkPtr->ConfigTableCount = ConfigCnt;

  /* Initialize cfg shared lib */
  Status = UefiCfgShLibInit (MemRegions, MemRegionsCnt, ConfigTablePtr, ConfigCnt);
  if (Status != EFI_SUCCESS)
    return EFI_OUT_OF_RESOURCES;

  /* Read config file for serial port buffer size */
  Status = GetConfigValue ("SerialPortBufferSize", &BufferSz);
  if ((Status != EFI_SUCCESS) || (BufferSz == 0))
    BufferSz = MAX_SERIAL_PORT_BUFFER;

  SerialLogBuffer = AllocatePages (BufferSz / EFI_PAGE_SIZE);
  if (SerialLogBuffer == NULL)
    return EFI_OUT_OF_RESOURCES;

  UefiInfoBlkPtr->UartLogBufferPtr = (UINTN*)SerialLogBuffer;
  UefiInfoBlkPtr->UartLogBufferLen = BufferSz;

  Status = SerialPortShLibInit (SerialLogBuffer, BufferSz);
  if (Status != EFI_SUCCESS)
    return EFI_OUT_OF_RESOURCES;

  /* Add hob for sharing InfoBlock with Dxe */
  if(AddInfoBlkHob() == NULL)
    DEBUG((EFI_D_ERROR, "WARNING: Unable to add InfoBlk HOB\r\n"));

  return Status;
}

STATIC VOID
SetupDBIDump (UINTN MemoryBase)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN WriteAddr;
  UINT32 WriteValue;
  UINT32 i;
  UINTN NumCores = 0;
  UINT32 SkipDBISetup = 0;

  DUMP_TABLE_TYPE *DbiDumpTable = (DUMP_TABLE_TYPE*)(MemoryBase + DBI_DUMP_TABLE_OFFSET);
  DUMP_TABLE_TYPE *AppDumpTable = (DUMP_TABLE_TYPE*)(MemoryBase + DBI_DUMP_TABLE_OFFSET + sizeof(DUMP_TABLE_TYPE));
  DUMP_DATA_TYPE_TABLE *DumpDataTypeTable = (DUMP_DATA_TYPE_TABLE*)(MemoryBase + DBI_DUMP_TABLE_OFFSET + 2*sizeof(DUMP_TABLE_TYPE));
  
  if (DbiDumpTable == NULL)
  {
    DEBUG ((EFI_D_ERROR, "ERROR: DBI Dump table not setup\r\n"));
    CpuDeadLoop();
    return;
  }

  Status = GetConfigValue("SkipDBISetup", (UINT32*)&SkipDBISetup);
  if ((Status == EFI_SUCCESS) && (SkipDBISetup == 1)) {
    return;
  }

  Status = GetConfigValue("NumCpus", (UINT32*)&NumCores);
  if (Status != EFI_SUCCESS)
  {
      DEBUG(( EFI_D_ERROR, "Could not NumCpus uefiplat.cfg Defaulting to 8 cores , Status = (0x%x)\r\n", Status));
      NumCores = 0x08;
  }
  /* Fill out the top level DbiDumpTable version and entries*/
  DbiDumpTable->Version    =    0x1;
  DbiDumpTable->NumEntries =    0x1;

  /* Fill out the second level APP dump table */
  AppDumpTable->Version     =    0x1;
  AppDumpTable->NumEntries  =   NumCores + 1;

  DbiDumpTable->Entries[0].id         = MSM_DUMP_CLIENT_APPS;
  DbiDumpTable->Entries[0].type       = MSM_DUMP_TYPE_TABLE;
  DbiDumpTable->Entries[0].start_addr = (UINT64)AppDumpTable; 
  
  DEBUG ((EFI_D_INFO, "NumCores : %d \n", NumCores));
  
  for (i=0; i < NumCores ; i++)
  {
    DumpDataTypeTable->DumpDataType[i].header.version        = 0x0;
    DumpDataTypeTable->DumpDataType[i].header.magic          = 0x0;
    DumpDataTypeTable->DumpDataType[i].start_addr     = MemoryBase + CPU_REG_DUMP_START_OFFSET + i * SDI_DUMP_CORE_AP_REG_SIZE;
    DumpDataTypeTable->DumpDataType[i].len            = SDI_DUMP_CORE_AP_REG_SIZE;
    AppDumpTable->Entries[i].id         = MSM_CPU_REGS_DUMP + i;
    AppDumpTable->Entries[i].type       = MSM_DUMP_TYPE_DATA;
    AppDumpTable->Entries[i].start_addr = (UINT64)(&DumpDataTypeTable->DumpDataType[i]); 
  }
   DumpDataTypeTable->DumpDataType[NumCores].header.version         = 0x0;
   DumpDataTypeTable->DumpDataType[NumCores].header.magic           = 0x0;
   DumpDataTypeTable->DumpDataType[NumCores].start_addr      = MemoryBase + CPU_REG_DUMP_END_OFFSET;
   DumpDataTypeTable->DumpDataType[NumCores].len             = SDI_DUMP_ETB_DUMP_SIZE;
   AppDumpTable->Entries[NumCores].id          = MSM_ETB_DUMP;
   AppDumpTable->Entries[NumCores].type        = MSM_DUMP_TYPE_DATA;
   AppDumpTable->Entries[NumCores].start_addr  = (UINT64)(&DumpDataTypeTable->DumpDataType[NumCores]);
   
   DumpDataTypeTable->DumpDataType[NumCores + 1].header.version         = 0x0;
   DumpDataTypeTable->DumpDataType[NumCores + 1].header.magic           = 0x0;
   DumpDataTypeTable->DumpDataType[NumCores + 1].start_addr      = MemoryBase + CPU_REG_DUMP_END_CHECK_OFFSET;
   DumpDataTypeTable->DumpDataType[NumCores + 1].len             = SDI_DUMP_CORE_AP_REG_SIZE;
   AppDumpTable->Entries[NumCores + 1].id          = CPU_REG_DUMP_END_CHECK;
   AppDumpTable->Entries[NumCores + 1].type        = 0x0;
   AppDumpTable->Entries[NumCores + 1].start_addr  = (UINT64)(&DumpDataTypeTable->DumpDataType[NumCores + 1]);

  /* Set the IMEM Cookie with the address to the DbiDumpTable */
  Status = GetConfigValue ("SharedIMEMBaseAddr", (UINT32 *)&SharedIMEMBaseAddr); 
  if ((Status != EFI_SUCCESS) || (SharedIMEMBaseAddr == 0))
  {
    SharedIMEMBaseAddr = PcdGet32(PcdIMemCookiesBase); 
  }
  /* TODO: Fix this when Shared IMEM table is fixed to support 64-bit addressing */
  WriteAddr = (UINTN) SharedIMEMBaseAddr + DBI_SHARED_IMEM_COOKIE_OFFSET;
  //ASSERT((UINTN) DbiDumpTable < 0xFFFFFFFF);
  WriteValue = ((UINTN)DbiDumpTable & 0xFFFFFFFF);
  MmioWrite32(WriteAddr, WriteValue);
}

STATIC VOID
AddMemRegionHobs (VOID)
{
  UINTN           Index;
  MemRegionInfo*  MemRegions = NULL;
  UINTN           MemRegionsCnt = 0;

  GetMemRegionInfo(&MemRegions, &MemRegionsCnt);
  if ((MemRegions == NULL) || (MemRegionsCnt == 0))
  {
    DEBUG((EFI_D_ERROR, "UEFI Memory Map configuration not found\r\n"));
    ASSERT (MemRegions != NULL);
    ASSERT (MemRegionsCnt != 0);
    CpuDeadLoop();
    return; /* For KW */
  }

  for (Index = 0; Index < MemRegionsCnt; Index++)
  {
    switch (MemRegions[Index].BuildHobOption)
    {
      case AllocOnly:
        BuildMemoryHob(&MemRegions[Index]);
        break;

      case AddMem:
        BuildMemoryHob(&MemRegions[Index]);
        break;

      case AddPeripheral:
        BuildMemoryHob(&MemRegions[Index]);
        break;

      case HobOnlyNoCacheSetting:
        BuildMemoryHob(&MemRegions[Index]);
        break;

      case NoBuildHob:
        /* Don't do anything, only cache is initialized */
        break;

      case CacheSettingCarveOutOnly:
      case AddMemForNonDebugMode:
      case AddMemForNonDebugOrNonCrashMode:
      case ReserveMemForNonCrashMode:
        DEBUG ((EFI_D_ERROR, "Adding hob using deprecated option\n"));
        ASSERT(FALSE);
        CpuDeadLoop();
        break;

      case ErrorBuildHob:
      default:
        DEBUG ((EFI_D_ERROR, "Invalid BuildHob Option\n"));
        ASSERT(FALSE);
        CpuDeadLoop();
        break;
    } /* end of switch */
  } /* end of for */
}

STATIC VOID
InitPerf (VOID)
{
  UINT64 Tick = 1;
  if (PerformanceMeasurementEnabled ())
    Tick = GetPerformanceCounter ();

  PERF_START (NULL, "SEC", NULL, Tick);
}

STATIC VOID
InitMemory(UINT64* MemoryBase, UINT64* MemorySize, UINTN* StackBase, UINTN StackSize)
{
  EFI_STATUS Status;
  UefiInfoBlkType* UefiInfoBlkPtr;
  UINT64 UefiFdBase = FixedPcdGet64(PcdEmbeddedFdBaseAddress);
  UINTN UefiInfoBlockOffset = FixedPcdGet32(PcdUefiInfoBlockOffset);
  MemRegionInfo FdRegion;

  InitRamPartitionTableLib ();
  
  /* Initialize Info Block */
  UefiInfoBlkPtr = InitInfoBlock (UefiFdBase + UefiInfoBlockOffset);
  UefiInfoBlkPtr->StackBase = StackBase;
  UefiInfoBlkPtr->StackSize = StackSize;

  if ((MemoryBase == NULL) || (MemorySize == NULL))
  {
    DEBUG ((EFI_D_ERROR, "Invalid memory base and size \n"));
    ASSERT (MemoryBase != NULL);
    ASSERT (MemorySize != NULL);
    CpuDeadLoop();
    return; /* For KW */
  }

  /* Get Available memory from partition table */
  Status = GetPartitionEntryByAddr (UefiFdBase, &FdRegion);
  if (EFI_ERROR(Status))
  {
    DEBUG ((EFI_D_ERROR, "Invalid memory configuration, check memory partition table\n"));
    ASSERT (Status == EFI_SUCCESS);
    CpuDeadLoop();
    return; /* For KW */
  }
  
  *MemoryBase = (UINTN)FdRegion.MemBase;
  *MemorySize = (UINTN)FdRegion.MemSize;

  /* Update InfoBlock Memory Base and Size from above */
  UefiInfoBlkPtr->MemBase = (UINTN)FdRegion.MemBase; 
  UefiInfoBlkPtr->MemSize = *MemorySize;

  InitializeFBPT();
}

VOID
HandleCrashDump ( VOID )
{
  EFI_STATUS Status = EFI_SUCCESS;
  /* HLOS Cookie Addr is offset 0xC from start of cookie base */
  UINTN* HLOSCrashCookieAddr = (UINTN*)((UINTN)SharedIMEMBaseAddr + 0xC);
  UINT32 MemoryCaptureModeValue = 0;
  UINT32 AbnormalResetOccurred = 0;

  DEBUG((EFI_D_INFO, "SEC Offline Crash Dump Handling Start\r\n"));
  PrintOfflineCrashDumpValues();

  /* Build HOB to pass up cookie address for DXE if debug mode is enabled */
  BuildGuidDataHob (&gQcomMemoryCaptureGuid, &HLOSCrashCookieAddr, sizeof(HLOSCrashCookieAddr));

  /* Always enable SBL dumps */
  SetDLOADCookie();

  /* Build HOB to pass up current Memory Capture Mode value */
  Status = GetMemoryCaptureMode(&MemoryCaptureModeValue);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Failed to retrieve Memory Capture Mode value\r\n"));
    return;
  }
  if (!IsMemoryCaptureModeValid(MemoryCaptureModeValue))
  {
    /* Initialize HOB to 0 */
    MemoryCaptureModeValue = OFFLINE_CRASH_DUMP_DISABLE;
  }
  BuildGuidDataHob (&gQcomMemoryCaptureValueGuid, &MemoryCaptureModeValue, sizeof(MemoryCaptureModeValue));

  /* Enable SBL offline crash dump */
  SetMemoryCaptureMode(OFFLINE_CRASH_DUMP_ENABLE);

  /* Build HOB to pass up current Abnormal Reset Occurred value */
  Status = GetAbnormalResetOccurred(&AbnormalResetOccurred);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Failed to retrieve AbnormalResetOccurred value\r\n"));
    return;
  }
  if (!IsAbnormalResetOccurredValid(AbnormalResetOccurred))
  {
    /* Initialize HOB to 0 */
    AbnormalResetOccurred = ABNORMAL_RESET_DISABLE;
  }
  BuildGuidDataHob (&gQcomAbnormalResetOccurredValueGuid, &AbnormalResetOccurred, sizeof(AbnormalResetOccurred));

  /* Setup abnormal reset cookie */
  SetAbnormalResetOccurred(ABNORMAL_RESET_ENABLE);

  DEBUG((EFI_D_INFO, "SEC Offline Crash Dump Handling Complete\r\n"));
  PrintOfflineCrashDumpValues();
}

/**
  HOB list initialization

  @param  StackBase       pointer to the stack base
  @param  StackSize       stack size

**/
STATIC
VOID
InitHobList (IN  UINTN StackSize, UINTN MemoryBase, UINTN MemorySize)
{
  UINTN UefiMemorySize = 0;
  UINTN StackBase;

  UefiMemorySize = FixedPcdGet32(PcdUefiMemPoolSize);

  StackBase = MemoryBase + UefiMemorySize - StackSize;

 /* HobBase starts at UefiMemoryBase */
  SetHobList (HobConstructor ((VOID *)MemoryBase, UefiMemorySize, (VOID*)MemoryBase, (VOID *)StackBase));

  if (FeaturePcdGet (PcdSingleDDRSystemResource)) {
    BuildResourceDescriptorHob (EFI_RESOURCE_SYSTEM_MEMORY, SYSTEM_MEMORY_RESOURCE_ATTR_CAPABILITIES, MemoryBase, MemorySize);
  } else {
    BuildResourceDescriptorHob (EFI_RESOURCE_SYSTEM_MEMORY, SYSTEM_MEMORY_RESOURCE_ATTR_CAPABILITIES, MemoryBase, UefiMemorySize);
  }

  BuildStackHob ((EFI_PHYSICAL_ADDRESS)StackBase, StackSize);
  BuildCpuHob (PcdGet8 (PcdPrePiCpuMemorySize), PcdGet8 (PcdPrePiCpuIoSize));

  if (FeaturePcdGet (PcdPrePiProduceMemoryTypeInformationHob)) {
    // Optional feature that helps prevent EFI memory map fragmentation.
    BuildMemoryTypeInformationHob ();
  }
}

/**
  Entry point

  @param  StackBase       pointer to the stack base
  @param  StackSize       stack size

**/
VOID
Main (IN  VOID  *StackBase, IN  UINTN StackSize)
{
  EFI_STATUS Status = EFI_NOT_READY;
  UINT64 MemoryBase = 0, MemorySize = 0;
  UINTN UefiMemoryBase = 0;
  VOID* HobBasePtr = NULL;
  UefiInfoBlkType* UefiInfoBlkPtr = NULL;
  UINT32 HaltBootOnFuseBlown = 0;

  InitStackCanary();

  StartCyclCounter ();

  /* Start UART debug output */
  UartInit();

  /* Enable program flow prediction, if supported */
  ArmEnableBranchPrediction ();

  /* Initialize memory, returns only on success */
  InitMemory(&MemoryBase, &MemorySize, StackBase, StackSize);

  UefiMemoryBase = MemoryBase + FixedPcdGet32(PcdPreAllocatedMemorySize);
  HobBasePtr = (VOID*)UefiMemoryBase;

  UefiInfoBlkPtr = GetInfoBlock();
  if (UefiInfoBlkPtr != NULL)
    UefiInfoBlkPtr->HobBase = HobBasePtr;

  InitHobList(StackSize, UefiMemoryBase, MemorySize - FixedPcdGet32(PcdPreAllocatedMemorySize));

  /* Add the FVs to the hob list */
  BuildFvHob (PcdGet64(PcdFlashFvMainBase), PcdGet64(PcdFlashFvMainSize));

	/* Should be done after we have setup HOB for memory allocation  */
  if (FixedPcdGet32(PcdSkipEarlyCacheMaint) == 0) {
    Status = EarlyCacheInit (MemoryBase, MemorySize);
    if (EFI_ERROR (Status))
    {
      DEBUG((EFI_D_ERROR, "EarlyCacheInit Failed\r\n"));
      ASSERT(Status == EFI_SUCCESS);
      CpuDeadLoop();
    }
  }

  /* Load and Parse platform cfg file, cache re-initialized per cfg file */
  Status = LoadAndParsePlatformCfg();
  if (EFI_ERROR (Status))
  {
    DEBUG((EFI_D_ERROR, "Error parsing configuration file\r\n"));
    ASSERT(Status == EFI_SUCCESS);
    CpuDeadLoop();
    return; /* For KW */
  }

  /* All shared lib related initialization */
  Status = InitSharedLibs();
  if (Status != EFI_SUCCESS)
  {
     DEBUG((EFI_D_ERROR, "InitSharedLibs failed\r\n"));
     ASSERT(Status == EFI_SUCCESS);
     CpuDeadLoop();
  }
  
  DisplayEarlyInfo();

  Status = GetConfigValue("HaltBootOnFuseBlown", &HaltBootOnFuseBlown);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_INFO, "HaltBootOnFuseBlown flag not found !\r\n"));
    HaltBootOnFuseBlown = 0;
  }

  if (HaltBootOnFuseBlown)// || FusesBlown())
  {
    DEBUG((EFI_D_ERROR, "Not permitted for retail. Shutting down\r\n"));
    CpuDeadLoop();
    return;
  }
  
  /* Setup DBI dump */
  SetupDBIDump(MemoryBase);

  AddMemRegionHobs ();

  HandleCrashDump();

  /* Save the memory base and size to be used later */
  SetMemoryInfo(DDR_SYSTEM_MEMORY, &MemoryBase, &MemorySize);

  InitializeDebugAgent (DEBUG_AGENT_INIT_PREMEM_SEC, NULL, NULL);
  SaveAndSetDebugTimerInterrupt (TRUE);

  /* Start perf here, after timer init, start at current tick value */
  InitPerf();

  /* SEC phase needs to run library constructors by hand */
  ExtractGuidedSectionLibConstructor ();
  LzmaDecompressLibConstructor ();

  /* Build HOBs to pass up our Version of stuff the DXE Core needs to save space */
  BuildPeCoffLoaderHob ();
  BuildExtractSectionHob (
    &gLzmaCustomDecompressGuid,
    LzmaGuidedSectionGetInfo,
    LzmaGuidedSectionExtraction
    );

  /* Assume the FV that contains the SEC (our code) also contains a compressed FV */
  DecompressFirstFv ();

  /* Any non-critical initialization */
  TargetLateInit();

  /* Build memory allocation HOB for FV2 type */
  BuildMemHobForFv(EFI_HOB_TYPE_FV2);

  DEBUG ((EFI_D_WARN, "SEC End    : %d mS\n", GetTimerCountms () ));

  /* Load the DXE Core and transfer control to it */
  LoadDxeCoreFromFv (NULL, 0);

  /* DXE Core should always load and never return */
  ASSERT (FALSE);
  CpuDeadLoop();
}

/**
  Entry point

  @param  StackBase       pointer to the stack base
  @param  StackSize       stack size
**/
VOID
CEntryPoint (
  IN  VOID  *StackBase,
  IN  UINTN StackSize
  )
{
  /* TODO: MOve this to assembly, code generated on AARCH64 uses stack */
#if 0
  /* NOTE: Stack will be cleared, do not use stack here */
  /* Zero out stack and jump to Main() */
  UINT32* Ptr, *End;

  Ptr = StackBase;
  End = (UINT32*)((UINT32)StackBase + StackSize);

  while (Ptr < End)
  {
    *Ptr = 0;
     ++Ptr;
  }
#endif

  UefiDebugModeEntry();

  TargetEarlyInit();

  Main (StackBase, StackSize);
}

