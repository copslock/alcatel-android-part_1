/**
  @file  tf_list.h
  @brief internal header file for DALTF driver test list use
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#ifndef __TF_LIST_H__
#define __TF_LIST_H__

#include<DDITF.h>
#include<Uefi.h>
#include<Library/UefiLib.h>
//#include<CoreString.h>
//#include "string.h"
#include<DALSys.h>

typedef struct test_entry_t{
    const TF_TestFunction_Descr * test_descr;  /*<-- DATA: user provided test description structure */
    struct test_entry_t * next;			   /*<-- PTR: next node in the list */
    uint32 index;
}TF_TestEntry;

typedef struct test_list_t{
	TF_TestEntry * dummy_head;		/*<-- HEAD: dummy head node, having dummy data */
	uint32 entry_num;				/*<-- COUNTER: how many nodes in the list */
	uint32 max_test_num;
	DALSYSSyncObj lock_obj;         /**< Sync object storage */
	DALSYSSyncHandle lock_h;        /**< Handle to our mutex */
}TF_TestList;

#define LOCK_ENTER(ctxt) (ctxt->lock_h) ? DALSYS_SyncEnter(ctxt->lock_h) : (void)ctxt->lock_h

#define LOCK_LEAVE(ctxt) (ctxt->lock_h) ? DALSYS_SyncLeave(ctxt->lock_h) : (void)ctxt->lock_h


TF_Result tf_list_init(TF_TestList* list_ptr, uint32 max_entries);

TF_Result tf_list_deinit(TF_TestList* list_ptr);

TF_Result tf_list_get_test(TF_TestList* list_ptr, const char* test2get, TF_TestEntry** test_des_ptr);

TF_Result tf_list_add_test(TF_TestList* list_ptr, const TF_TestFunction_Descr* test2add);

TF_Result tf_list_remove_test(TF_TestList* list_ptr, const TF_TestFunction_Descr* test2del);

TF_Result tf_list_iter_list(TF_TestList* list_ptr);

TF_Result tf_list_get_test_by_id(TF_TestList* list_ptr, uint32 id, TF_TestEntry** test_entry_ptr);

#endif /*#ifndef __TF_LIST_H__*/
