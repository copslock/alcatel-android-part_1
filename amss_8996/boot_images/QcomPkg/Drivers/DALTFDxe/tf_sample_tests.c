/**
  @file  tf_sample_tests.c
  @brief sample test cases defination
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#include<tf_sample_tests.h>

/* ============================================================================
**  Function : DummyTestFunction
** ========================================================================== */
/*!
   @brief
   A sample DalTF test case.  Sends "Hello World!!" to the log, 
   and returns the success code.

   @param dwArg      - [IN] # parameters: disregarded
   @param pszArg     - [IN] parameters: disregarded

   @par Dependencies
   None

   @par Side Effects
   None

   @return	
   TF_RESULT_CODE_SUCCESS

   @sa	DummyTestFunctionTwo
   @sa	DummyTestFunctionThree
   @sa	DummyTestFunctionFour
   @sa	DummyTestSpinner
   @sa	DummyBabTest
*/



#define PRINTSIG "[SAMPLE]"

static uint32 DummyTestFunction(uint32 dwArg, char* pszArg[])
{
    // Print to Log
    AsciiPrint(PRINTSIG"Hello World!!\n", NULL);

    return TF_RESULT_CODE_SUCCESS;  
}

/* ============================================================================
**  Function : DummyTestFunctionTwo
** ========================================================================== */
/*!
   @brief
   A sample DalTF test case.  Sends "Hello World!!" to the log and the number of
   parameters input. Also prints the first two parameters if they both exist.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return	
   TF_RESULT_CODE_SUCCESS

   @sa	DummyTestFunction
   @sa	DummyTestFunctionThree
   @sa	DummyTestFunctionFour
   @sa	DummyTestSpinner
   @sa	DummyBabTest
*/
static uint32 DummyTestFunctionTwo(uint32 dwArg, char* pszArg[])
{   
    // Print to Log
    AsciiPrint(PRINTSIG"Hello World Part 2!!\n", NULL);
    AsciiPrint(PRINTSIG"dwArg - %d\n", dwArg);

    // If we have 2 parameters, print them. Note: they could both be "".
    if(dwArg > 1)
    {
        AsciiPrint(PRINTSIG"arg1 %a\n", pszArg[0]);
        AsciiPrint(PRINTSIG"arg2 %a\n", pszArg[1]);
    }

    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : DummyTestFunctionThree
** ========================================================================== */
/*!
   @brief
   A sample DalTF test case.  Prints "Hello World 3" and returns
   a variable success code.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return	
   TF_RESULT_CODE_SUCCESS if the first parameter is "pass", 
   TF_RESULT_CODE_FAILURE if the first parameter is "fail" and
   TF_RESULT_CODE_BAD_PARAM otherwise.

   @sa	DummyTestFunction
   @sa	DummyTestFunctionTwo
   @sa	DummyTestFunctionFour
   @sa	DummyTestSpinner
   @sa	DummyBabTest
*/
static uint32 DummyTestFunctionThree(uint32 dwArg, char* pszArg[])
{
   // Send to log
   AsciiPrint(PRINTSIG"Hello world 3\n", NULL);

   // Depending on what the string says, return the associated result.
   if(dwArg > 0)
   {
      if(0 == AsciiStrnCmp("fail", pszArg[0], 4))
      {
         return TF_RESULT_CODE_FAILURE;
      }
      else if(0 == AsciiStrnCmp("pass", pszArg[0], 4))
      {
         AsciiPrint(PRINTSIG"%a\n",pszArg[1]); // print random string
         return TF_RESULT_CODE_SUCCESS;
      }
      else
      {
         return TF_RESULT_CODE_BAD_PARAM;
      }
   }

   return TF_RESULT_CODE_BAD_PARAM;
}

/* ============================================================================
**  Function : DummyTestFunctionFour
** ========================================================================== */
/*!
   @brief
   Prints second parameter passed to it if first parameter is "y"

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return	
   TF_RESULT_CODE_SUCCESS

   @sa	DummyTestFunction
   @sa	DummyTestFunctionTwo
   @sa	DummyTestFunctionThree
   @sa	DummyTestSpinner
   @sa	DummyBabTest
*/
static uint32 DummyTestFunctionFour(uint32 dwArg, char* pszArg[])
{
   // If the first arg starts with a y, print the second arg if it exists.
   if(dwArg > 0){
      if(0 == AsciiStrnCmp("y", pszArg[0], 1)){
         if(dwArg > 1)	AsciiPrint(PRINTSIG"%a\n",pszArg[1]);
      }
   }

   return TF_RESULT_CODE_SUCCESS;
}


// Sample Test Case 2's Help Data
static const TF_HelpDescr DTF2Help = 
{
    "This is dummy test function 2, the best test function",
    0,
    NULL
};

// Sample Test Case 3's Parameters
static const TF_ParamDescr DTF3Param[] = 
{
    {TF_PARAM_UINT32, "pass/fail", "A value of pass will make the return value "
      "Success, fail, Failure, and nothing, Bad Parameters"},
    {TF_PARAM_INT32, "random string", "does absolutely nothing"},
    {TF_PARAM_UINT16, "cmd buffer size", "does absolutely nothing"},
    {TF_PARAM_INT16, "random string version 2", "does absolutely nothing"},
    {TF_PARAM_UINT8, "thread number", "does absolutely nothing"},
    {TF_PARAM_INT8, "useless", NULL},
    {TF_PARAM_STRING, "useless 2", NULL},
    {TF_PARAM_STRING, "useless 3", NULL}
};

// Sample Test Case 3's Help Data
static const TF_HelpDescr DTF3Help = 
{
    "This is dummy test function 3, the second best test function",
    sizeof(DTF3Param) / sizeof(TF_ParamDescr),
    DTF3Param
};

// Sample Test Case 4's Parameters
static const TF_ParamDescr DTF4Param[] = 
{
    {TF_PARAM_STRING, "print", "This parameter controls whether"
      " we print to the screen or not, (answers are y or n)"},
    {TF_PARAM_STRING, "Line", "This is what gets printed"}
};

// Sample Test Case 4's Help Data
static const TF_HelpDescr DTF4Help = 
{
    "This is dummy test function 4, the best",
    sizeof(DTF4Param) / sizeof(TF_ParamDescr),
    DTF4Param
};

static const TF_ContextDescr context = { 0, 0 };


static const TF_TestFunction_Descr testCase1 = 
{
   PRINTSIG"DummyTestFunctionOne",
   DummyTestFunction,
   NULL, 
   &context
};

static const TF_TestFunction_Descr testCase2 = 
{
   PRINTSIG"DummyTestFunctionTwo",
   DummyTestFunctionTwo,
   &DTF2Help, 
   &context
};

static const TF_TestFunction_Descr testCase3 = 
{
   PRINTSIG"DummyTestFunctionThree",
   DummyTestFunctionThree,
   &DTF3Help, 
   &context
};

static const TF_TestFunction_Descr testCase4 = 
{
   PRINTSIG"DummyTestFunctionFour",
   DummyTestFunctionFour,
   &DTF4Help, 
   &context
};

const TF_TestFunction_Descr* tf_apSampleTestDescriptions[] = 
   { &testCase1, &testCase2, &testCase3, &testCase4};

const uint16 tf_wNumberSampleTests = sizeof(tf_apSampleTestDescriptions)/sizeof(TF_TestFunction_Descr*);

