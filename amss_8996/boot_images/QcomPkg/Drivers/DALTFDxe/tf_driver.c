/**
  @file  tf_driver.c
  @brief DALTF driver interface implemention.
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/
/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#include<tf_driver.h>
#include<Library/UefiLib.h>
#include<DALDeviceId.h>
#include<tf_sample_tests.h>
#include<Library/CoreTestsLib.h>

#define TF_LOG_INFO(str) DALSYS_LogEvent((DALDEVICEID)DALDEVICEID_URAD_DALTF, \
   DALSYS_LOGEVENT_INFO, str)

/*******************************************************************
 *  				VARIABLES
 *******************************************************************/

/*******************************************************************
 *  				FUNCTION DEFINITIONS
 *******************************************************************/

DALResult tf_init(TFPrivateCtxt *ctxt_ptr)
{
	TF_Result ret;
	/*-----------------------------------------------------------------------*/
	/* Create test list.                             						 */
	/*-----------------------------------------------------------------------*/
	ret = tf_list_init(&ctxt_ptr->test_list, MAX_TEST_NUM);

	if (TF_SUCCESS != ret)	return ret;
	/*-----------------------------------------------------------------------*/
	/* init TFHandleCtxt.                             						 */
	/*-----------------------------------------------------------------------*/
	ret = tf_handle_init(&ctxt_ptr->handle_ctxt, &ctxt_ptr->test_list);

	if(TF_SUCCESS != ret) return ret;

	/*-----------------------------------------------------------------------*/
	/* Add dummy tests in the list.                             			 */
	/*-----------------------------------------------------------------------*/
	//tf_add_tests(ctxt_ptr, tf_apSampleTestDescriptions, tf_wNumberSampleTests);

	/*-----------------------------------------------------------------------*/
	/* Add apt init/deinit tests in the list.                             	 */
	/*-----------------------------------------------------------------------*/
	tf_add_tests(ctxt_ptr, daltf_setup_tests, daltf_setup_number);
	
	return DAL_SUCCESS;
}

DALResult tf_deinit(TFPrivateCtxt *ctxt_ptr)
{
	tf_list_deinit(&ctxt_ptr->test_list);
	tf_handle_deinit(&ctxt_ptr->handle_ctxt);
	return DAL_SUCCESS;
}

DALResult tf_add_tests
(
	TFPrivateCtxt *ctxt_ptr,
	const TF_TestFunction_Descr *  tests2add[],
	uint32  num_tests2add
)
{
	uint32 i = 0;
	uint32 num_tests = 0;
	TF_Result ret;

	for(i = 0; i < num_tests2add; i++){
		ret = tf_list_add_test(&ctxt_ptr->test_list, tests2add[i]);

		if(ret == DAL_SUCCESS)
			num_tests++;
	}

	return (num_tests == num_tests2add) ? DAL_SUCCESS : DAL_ERROR;
}

DALResult tf_remove_tests
(
	TFPrivateCtxt *ctxt_ptr,
	const TF_TestFunction_Descr *  tests2remove[],
	uint32  num_tests2rm
)
{
	uint32 num_tests = 0;
	uint32 i = 0;
	TF_Result ret;
	for(i = 0; i < num_tests2rm; i++){
		ret = tf_list_remove_test(&ctxt_ptr->test_list, tests2remove[i]);
		if(ret == DAL_SUCCESS)
			num_tests++;
	}
	return (num_tests == num_tests2rm) ? DAL_SUCCESS : DAL_ERROR;
}


DALResult tf_handle_cmd(TFPrivateCtxt *ctxt_ptr, const char* req)
{
	TF_Result res;
	TFHandleCtxt * h = &ctxt_ptr->handle_ctxt;
	char * request = ctxt_ptr->handle_ctxt.cur_test.raw_data_buf;

	DALSYS_memset(request, 0, BUF_SIZE);
	DALSYS_memcpy(request, req, strlen(req)+1);

	while(request != NULL && (*request < 'A' || *request > 'Z')){
		if(*request == '\n')
			return DAL_ERROR;
		request++;
	}

	// parse command received and go to correspondent handler
	if (AsciiStrnCmp(request,LIST_STR,LIST_LEN) == 0){
		res = tf_handle_list(h);
	}
	else if (AsciiStrnCmp(request,RUN_STR,RUN_LEN) == 0){
		res = tf_handle_run(h, request + RUN_LEN);
	}
	else if (AsciiStrnCmp(request,MESSAGE_STR,MESSAGE_LEN) == 0){
		res = tf_handle_message(h);
	}
	else if (AsciiStrnCmp(request,HELP_STR,HELP_LEN) == 0){
		res = tf_handle_help(h, request + HELP_LEN);
	}
	else if (AsciiStrnCmp(request,STATUS_STR,STATUS_LEN) == 0){
		res = tf_handle_status(h, request + STATUS_LEN);
	}
	else{
		res = TF_ERROR_CMD_UNKNOWN;
	}
	return res;
}


