/**
  @file  tf_driver.c
  @brief DALTF driver interface implemention.
*/

/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/23/15   jz      Created.
=============================================================================*/

#include<tf_handle.h>

/*

  DALTF serial protocol supports the following commands/requests

  o LIST      			 							- Get all the tests in the daltf instance

  o RUN(test_name)  	 							- Run test 'test_name' once

  o RUN(test_name,iters) 							- Run test 'test_name' for 'iters' iterations,zero parameters

  o RUN(test_name,iters,#params,param0,param1,.. )  - Run test with parameters

  o STATUS(test_id) 	 							- get the status of test 'test_id'

  o HELP(test_id) 		 							- get the help for test 'test_id'

  o MESSAGE  			 							- get the messages from message buffer
*/

/*******************************************************************
 *  				VARIABLES
 *******************************************************************/
Timetick_timer_Type Timer = TIMETICK_QTIMER;

/*******************************************************************
 *  				FUNCTION PROTOTYPES
 *******************************************************************/
TF_Result tf_get_help
(
	const TF_TestFunction_Descr* test_des_ptr
);

TF_Result tf_get_status
(
	TF_TestData *  test_data_ptr
);

void tf_parse_test
(
	char* parameters,
	TF_TestData * test_data
);


TF_Result tf_run_test
(
	TF_TCFunctionPtr test_func,
	TF_TestData * data
);

char* tf_cmdline_parser
(
	char* cp,
	uint32* param_len,
	uint32* endofstr
);

/*******************************************************************
 *  				FUNCTION DEFINITIONS
 *******************************************************************/
TF_Result tf_handle_init(TFHandleCtxt* h, TF_TestList * list_ptr)
{
	DALSYS_memset(h, 0, sizeof(TFHandleCtxt));

	h->test_list_ptr = list_ptr;

	if(DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
										&h->cur_test.lock_h,
										&h->cur_test.lock_obj))
		return TF_ERROR_ALLOC;

	Timetick_Init();
	Timetick_Enable(Timer, TRUE);

	return TF_SUCCESS;
}

TF_Result tf_handle_deinit(TFHandleCtxt* h)
{
	DALSYS_DestroyObject(h->cur_test.lock_h);
	return TF_SUCCESS;
}

TF_Result tf_handle_list(TFHandleCtxt* h)
{
	return tf_list_iter_list(h->test_list_ptr);
}

TF_Result tf_handle_status(TFHandleCtxt* h, char* params)
{
	TF_Result res;
	uint32 len;
	uint32 endofstr;
	uint32 idx;

	TF_TestEntry* test2stat_ptr = NULL;

	while(params != NULL && *params == '(')
	   	params++;

	tf_cmdline_parser(params, &len, &endofstr);

	idx = AsciiStrDecimalToUintn(params);

	res = tf_list_get_test_by_id(h->test_list_ptr, idx, &test2stat_ptr);

	if(res == TF_SUCCESS){
		if(0 == AsciiStrnCmp(test2stat_ptr->test_descr->pszTestName,
						h->cur_test.test_name,
						MAX_TEST_NAME_LENGTH))
			// go and get the test status
			res = tf_get_status(&h->cur_test);
		else
			AsciiPrint(TFSIG"No status info for test (id = %d)\n", idx);
	}

	return res;
}

TF_Result tf_handle_run(TFHandleCtxt* h, char* params)
{
	TF_Result res;
	TF_TestEntry * test2run_ptr;
    TF_TestData * cur_test_ptr = &h->cur_test;

    LOCK_ENTER(cur_test_ptr);

    // check if daltf is currently running
    if(cur_test_ptr->busy == 1){
    	AsciiPrint(TFSIG"currently busy running, please try later\n");
    	return TF_FAILURE;
    }
    // reset the test data and result struct
    DALSYS_memset(&cur_test_ptr->test_res, 0, sizeof(TF_TestResult));
    DALSYS_memset(&cur_test_ptr->test_input, 0, sizeof(TF_TestInput));

    // parse the run command and populate the test data structure
    tf_parse_test(params, cur_test_ptr);

    // get the test to run
    res = tf_list_get_test(h->test_list_ptr,
    					   cur_test_ptr->test_name,
						   &test2run_ptr);

    if(res != TF_SUCCESS){
    	AsciiPrint(TFSIG"test %a not in the list\n", cur_test_ptr->test_name);
    	return TF_ERROR_TEST_NONEXIST;
    }

    res = tf_run_test(test2run_ptr->test_descr->pTestFunc, cur_test_ptr);

    DALSYS_Free(cur_test_ptr->test_input.argv);

    LOCK_LEAVE(cur_test_ptr);
    return res;
}

TF_Result tf_handle_help(TFHandleCtxt* h, char* params)
{
	TF_Result res;
	uint32 len;
	uint32 endofstr;
	uint32 idx;

	TF_TestEntry* test2help_ptr = NULL;

	while(params != NULL && *params == '(')
	   	params++;

	tf_cmdline_parser(params, &len, &endofstr);

	idx = AsciiStrDecimalToUintn(params);

	res = tf_list_get_test_by_id(h->test_list_ptr, idx, &test2help_ptr);

	if(res == TF_SUCCESS)
		res = tf_get_help(test2help_ptr->test_descr);
	else
		AsciiPrint(TFSIG"Failed to find test (id = %d), error %X\n", idx, res);
	return res;
}


TF_Result tf_handle_message(TFHandleCtxt* h)
{
	return TF_SUCCESS;
}

TF_Result tf_get_help(const TF_TestFunction_Descr* test_des_ptr)
{
	const TF_HelpDescr * help_ptr = test_des_ptr->pHelp;
	uint32 i = 0;

	if(help_ptr == NULL){
		AsciiPrint(TFSIG"No help provided in test %a\n", test_des_ptr->pszTestName);
		return TF_SUCCESS;
	}
	AsciiPrint("TEST DESCRIPTION: %a\n", help_ptr->pszFunctionDescr);
	for(i=0; i<help_ptr->dwParametersLength; i++){
		AsciiPrint("PARAM #%d:\n", i);
		AsciiPrint("\t\tType: 		 %a\n", help_ptr->pParameters[i].pszType);
		AsciiPrint("\t\tName: 		 %a\n", help_ptr->pParameters[i].pszName);
		AsciiPrint("\t\tDescription: %a\n", help_ptr->pParameters[i].pszHelpString);
	}

	return TF_SUCCESS;
}

TF_Result tf_get_status(TF_TestData * cur_test_ptr)
{
	char* stat_res = NULL;
	TF_TestResult * curtest = &cur_test_ptr->test_res;

	if ( TF_RESULT_CODE_BAD_PARAM == curtest->last_result )
		AsciiPrint("STATUS - The test case was passed bad parameters, all further runs have been cancelled.\n");

	else
	{
		if(cur_test_ptr->busy == 1)
			stat_res = "RUNNING";
		else if(curtest->total_cnt == 0)
			stat_res = "NOT STARTED";
		else
			stat_res = (curtest->success_cnt == curtest->total_cnt) ? "PASSED" : "FAILED";

		AsciiPrint("STATUS - success count=%d\n", curtest->success_cnt);
		AsciiPrint("STATUS - run count=%d\n", curtest->total_cnt);
		AsciiPrint("STATUS - min time=%d\n", curtest->min_time);
		AsciiPrint("STATUS - max time=%d\n", curtest->max_time);
		AsciiPrint("STATUS - last_result=%d\n", curtest->last_result);
		AsciiPrint("\nSTATUS *** TEST %a ***\n", stat_res);
	}

	return TF_SUCCESS;
}

TF_Result tf_calculate_time(uint64 TotalTicks, uint64 *usec)
{
	Timetick_time_32Type TimetickFreq = 0;
	/* Convert ticks to micro seconds */
	Timetick_GetFreq(Timer, &TimetickFreq);
	if(TimetickFreq == 0){
		*usec = 0;
		return TF_FAILURE;
	}
	*usec = MultU64x32(TotalTicks, 1000000);
	*usec = DivU64x32(TotalTicks, TimetickFreq);

	return TF_SUCCESS;
}

TF_Result tf_run_test(TF_TCFunctionPtr test_func, TF_TestData * test_data)
{
	TF_TestResult * test_out;
	TF_TestInput * test_in;
	uint32 apt_ret;
	Timetick_time_64Type elapsed_t, start_t, end_t;
	uint32 i = 0;

	if(test_func == NULL){
		return TF_ERROR_TEST_NONEXIST;
	}

	// the test can be conducted
	test_out = &test_data->test_res;
	test_in = &test_data->test_input;

	test_data->busy = 1;

	for(i = 0; i < test_in->max_run; i++){

		Timetick_GetCount(Timer, &start_t);

		apt_ret = test_func(test_in->num_args, test_in->argv);

		Timetick_GetCount(Timer, &end_t);

		// update result
		test_out->total_cnt++;
		test_out->last_result = apt_ret;
		tf_calculate_time((end_t - start_t), &elapsed_t);

		if(apt_ret == TF_RESULT_CODE_SUCCESS){
			test_out->success_cnt++;

			if(elapsed_t > test_out->max_time)
				test_out->max_time = elapsed_t;

			else if(elapsed_t < test_out->min_time)
				test_out->min_time = elapsed_t;
		}
		else
			test_out->failure_cnt++;

		if(apt_ret == TF_RESULT_CODE_BAD_PARAM){
			AsciiPrint(TFSIG"bad parameter pass in, further running will be canceled!\n");
			break;
		}
		else
			DALSYS_BusyWait(test_in->period_time);
	}

	test_data->busy = 0;
	return test_out->failure_cnt > 0 ? TF_FAILURE : TF_SUCCESS;
}

char* tf_cmdline_parser(char* cp, uint32* param_len, uint32* endofstr)
{
	if(param_len == NULL || endofstr == NULL) return NULL;

	*param_len = 0;
	*endofstr = 0;

	while(cp != NULL ){
		(*param_len)++;
		if( *cp == ',' ){
			*cp = '\0';
			return ++cp;
		}
		else if(*cp == ')'){
			*cp = '\0';
			*endofstr = 1;
			return cp;
		}
		else
			cp++;
	}
	return cp;
}

void tf_parse_test(char* parameters, TF_TestData* test_data)
{
    char* cp;
	char* num_arg_str;
	uint32 idx = 0;
    uint32 len = 0;
	uint32 cmdend = 0;
    uint32 num_params = 0;
    TF_TestInput * test_in;

    while(parameters != NULL && *parameters == '(')
    	parameters++;

	cp = tf_cmdline_parser(parameters, &len, &cmdend);
	// AsciiPrint("parameters = %a, cp = %a, len=%d\n", parameters, cp, len);

	DALSYS_memcpy(test_data->test_name,
				  parameters,
				  len > MAX_TEST_NAME_LENGTH? MAX_TEST_NAME_LENGTH : len);
	// truncate
	test_data->test_name[MAX_TEST_NAME_LENGTH] = '\0';
	test_in = &test_data->test_input;

    if (cmdend == 1)
        num_params = 0; // parse done
    else
    {
        //extract the num params
		num_arg_str = cp;
		cp = tf_cmdline_parser(cp, &len, &cmdend);
        num_params = AsciiStrDecimalToUintn(num_arg_str);

        if(num_params > 0){
			if (DAL_SUCCESS != DALSYS_Malloc(num_params * sizeof(char*),
											 (void**)&test_in->argv))
			{
				AsciiPrint(TFSIG"failed to allocate memory for argvs\n");
				return;
			}
			for(idx=0; idx<num_params; idx++){
				test_in->argv[idx] = cp;
				if(cmdend == 1 && idx < num_params -1){
					AsciiPrint(TFSIG"number of args is less than present args\n");
					break;
				}
				cp = tf_cmdline_parser(cp, &len, &cmdend);
			}
			if(cmdend == 0)
				AsciiPrint(TFSIG"number of args is greater than present args\n");
        }
		else
			AsciiPrint(TFSIG"number of params = 0\n");
    }
    test_in->num_args = num_params;
    test_in->max_run = 1;
    test_in->period_time = MIN_TEST_PERIOD;
}

/*
uint32 atoint (char* st) {
   uint32 ret = 0;
   uint16 i = 0;
   uint8 n;
   while (st[i] >= '0' && st[i] <= '9') {
      n = st[i] - '0';
      ret = ret*10 + n;
      i++;
   }
   return ret;
}
*/
