/** @file Charger.c

  Implements the Charger protocol

  Copyright (c) 2012 - 2015,  Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


when         who     what, where, why
--------     ---     -----------------------------------------------------------
05/12/15     mr      Enabled Charger functionality for 8952 (CR-846387)
02/24/15     sv      Added support for onetime sticky charging.
02/20/15     al      Adding stubs for key press
01/14/15     al      Changing API name for USB current setting & adding battery error check
01/13/15     va      Logging for ChargerApp states
12/26/14     al      Changing variable name to more appropriate
12/11/14     al      Installing Charger protocol during exit
08/21/14     va      Enable File Logging
09/05/13     sm      Added error check for output parameter validity
05/06/13     sm      Added EFI_BATTERY_CHARGING_PROTOCOL_REVISION to
                     EFI_BATTERY_CHARGING_PROTOCOL
03/06/13     dy      Add Charger Extn Driver
01/30/13     al      Cleaning warnings
10/04/12     dy      New File

=============================================================================*/
#include <Uefi.h>

/**
  EFI interfaces
 */
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

/**
  Charger PROTOCOL interface
 */
#include "Protocol/EFICharger.h"

#include "Protocol/EFIChargerExtn.h"

#include "ChargerLib.h"
#include "ChargerApp.h"

/**
  EFI interfaces
 */
 #include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/QcomLib.h>
#include <api/systemdrivers/pmic/pm_uefi.h>

/**
  Protocol Dependencies
*/
#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/EFIPmicFg.h>
#include <Protocol/EFIPmicSmbchg.h>
#include <Protocol/EFIPmicVersion.h>
#include <Library/QcomTargetLib.h>

#include "ChargerApp.h"
/**
  ADC Dependencies
 */
#include <Protocol/EFIAdc.h>

/**
  Ext HW Dependencies
*/




/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/

EFI_STATUS EFI_ChargerGetBatteryStatus( IN  EFI_BATTERY_CHARGING_PROTOCOL  *This,
                                        OUT UINT32                     *StateOfCharger,
                                        OUT UINT32                     *RatedCapacity,
                                        OUT INT32                      *ChargeCurrent);

EFI_STATUS EFI_ChargerChargeBattery( IN EFI_BATTERY_CHARGING_PROTOCOL            *This,
                                     IN UINT32                                MaximumCurrent,
                                     IN UINT32                                TargetStateOfCharge,
                                     IN EFI_BATTERY_CHARGING_COMPLETION_TOKEN *CompletionToken);

EFI_STATUS ChargerSignalEvent(EFI_BATTERY_CHARGING_STATUS ChgStatus);

/**
  Charger UEFI Protocol implementation
 */
EFI_BATTERY_CHARGING_PROTOCOL ChargerProtocolImplementation =
{
  EFI_ChargerGetBatteryStatus,
  EFI_ChargerChargeBattery,
  EFI_BATTERY_CHARGING_PROTOCOL_REVISION
};

/* Save maximum charge current, 0 to 100% */
static UINT32 ChargerTargetStateOfCharge = 0;

/* Keeps track of Maximum Charge Current (mA) */
static UINT32 ChargerMaximumCurrent = 0;

/* Save Completion Passed by the Charger App. */
static EFI_BATTERY_CHARGING_COMPLETION_TOKEN *ChargerCompletionToken = NULL;

extern EFI_BATTERY_CHARGING_EXTN_PROTOCOL ChargerExtnProtocolImplementation;

#define EXIT_CHARGERDXE                 L"ExitChargerDxe"


/**
  ChargerInitialize ()

  @brief
  Initialize Charger Protocol
 */
EFI_STATUS ChargerInitialize(IN EFI_HANDLE         ImageHandle,
                             IN EFI_SYSTEM_TABLE   *SystemTable)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32     ContinueBoot = 0;
  BOOLEAN    EnablOnetimeStickychg = FALSE;
  UINTN      DataSize;
  //BOOLEAN bKeyPressed = FALSE;

  ChgAppConfig ChargerConfig = {0};
  ChargerLibHandleType ChargerLibHandle;

  Status = ChargerLibInit(&ChargerConfig);

  if (EFI_SUCCESS != Status)
  {
    DEBUG((EFI_D_ERROR, "ChargerDXE - Failed to initialize Charger Protocol! Error Code: 0x%08X\r\n", Status));
    return Status;
  }

  if (TRUE == ChargerConfig.SupportQcomChargingApp)
  {
    DataSize = sizeof(ContinueBoot);
    Status = gRT->GetVariable(EXIT_CHARGERDXE,
                              &gQcomTokenSpaceGuid,
                              NULL,
                              &DataSize,
                              &ContinueBoot);
    /*assign only if variable exist in fv*/
    ContinueBoot = (Status == EFI_SUCCESS) ? ContinueBoot : 0;

    /*Call function to check Onetime sticky charging is enable status*/
    ChargerGetOnetimeStickyChargingStatus(&EnablOnetimeStickychg);

    /* If Sticky charging enabled then perform 100% charging*/
    if (EnablOnetimeStickychg)
    {
        ChargerConfig.OneTimeStickyChargingEnabled =TRUE;
        DEBUG((EFI_D_INFO, "ChargerDxe: One time sticky charging enabled.. \r\n"));
    }
    else
    {
        ChargerConfig.OneTimeStickyChargingEnabled =FALSE;
    }

    /*If ExitChargerDxe is set, boot to HLOS, do not run QCChargerApp*/
    if ( !ContinueBoot )
    {
      /* Two times debug print to let other folks know that charging have started*/
      DEBUG((EFI_D_WARN, "ChargerDxe: Start Charging now..\n\r"));
      CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerDxe: Start Charging now..\n\r"));

      /*Check for chargerlib battery error handling status*/
      Status |= ChargerLibBatteryErrorHandler(&ChargerLibHandle);
      DEBUG((EFI_D_ERROR, "ChargerDxe: ChargerInitialize: ChargerLibHandle: (%d) \r\n", ChargerLibHandle));

      switch (ChargerLibHandle)
      {
        case CHARGERLIB_HANDLE_CHARGE :
          /*Device will stay in this call until charging criteria are satisfied*/
          Status = RunQcomChargerApp(ChargerConfig);
          break;
        case CHARGERLIB_HANDLE_SHUTDOWN:
          ChargerLibForceSysShutdown(CHGAPP_RESET_AFP);
          break;
        case CHARGERLIB_HANDLE_CONTINUE_BOOT:
          /*do nothing and continute to boot*/
          break;
        default:
          /*if error happened then do cold reset*/
          ChargerLibForceSysShutdown(CHGAPP_RESET_COLD);
          break;
      }
    }
    else
    {
      Status = ChargerEnable(TRUE);

      /*Clear the fv variable*/
      ChgAppFlushVariable(EXIT_CHARGERDXE, FALSE);
    }
  }

  /* Install the Charger Protocol for backward compatibility */
  Status = gBS->InstallProtocolInterface(
    &ImageHandle,
    &gBatteryChargerProtocolGuid,
    EFI_NATIVE_INTERFACE,
    &ChargerProtocolImplementation
    );

  if (EFI_SUCCESS != Status)
  {
    DEBUG((EFI_D_ERROR, "ChargerDxe: Failed to install Charger Protocol! Error Code: 0x%08X \r\n", Status));
    return Status;
  }

  return Status;
}

/*
EFI_chargerGetOnetimeStickyChargingStatus(BOOLEAN* Enabled)
{
    EFI_STATUS  Status = EFI_SUCCESS;
    UINT8       EnablStikychg = 0;
    UINTN       DataSize;
    UINT8       TriggerType;
    EFI_QCOM_PMIC_PWRON_PROTOCOL  *pPmicPwron = NULL;
    BOOLEAN     ColdBoot = FALSE;

    Status = gBS->LocateProtocol( &gQcomPmicPwrOnProtocolGuid,
                                NULL,
                                (VOID**) &pPmicPwron );
    if (Status != EFI_SUCCESS)
    {
    return Status;
    }

    Status = pPmicPwron->GetPowerOnTrigger(0, &TriggerType);
    if (EFI_SUCCESS == Status)
    {
        PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: GetPowerOnTrigger %d:  \n\r", TriggerType));
        if (TriggerType == pm_pon_pon_reason_type.hard_reset)
        {
              ColdBoot = TRUE;
        }
    }
    DEBUG((EFI_D_WARN, "Cold boot status %d", ColdBoot));
    //Get onetime sticky charging variable
    DataSize = sizeof(EnablStikychg);
    Status = gRT->GetVariable (STICKYCHARGING_RUNTIME_VARIABLE_NAME,
                               &gStickyGuid,
                               NULL,
                               &DataSize,
                               &EnablStikychg);
    //assign only if variable exist in fv
    EnablStikychg = (Status == EFI_SUCCESS) ? EnablStikychg : 0;
    DEBUG((EFI_D_WARN, "One tome stick charging status %d", EnablStikychg));
    //test
    EnablStikychg = TRUE;

    if( !ColdBoot && EnablStikychg)
    {
        Enabled = TRUE;
        //Clear FV variable
        EnablStikychg = FALSE;
        DataSize = sizeof(EnablStikychg);
        Status = gRT->SetVariable (STICKYCHARGING_RUNTIME_VARIABLE_NAME,
                               &gStickyGuid,
                                EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                                DataSize,
                                &EnablStikychg);
    }

    return Status;
}

*/
/**
  GetBatteryStatus ()

  @brief
  GetBatteryStatus implementation of EFI_QCOM_CHARGER_PROTOCOL
 */
EFI_STATUS
EFI_ChargerGetBatteryStatus (
  IN EFI_BATTERY_CHARGING_PROTOCOL *This,
  OUT UINT32 *StateOfCharge,
  OUT UINT32 *RatedCapacity,
  OUT INT32  *ChargeCurrent )
{
  EFI_STATUS                  Status        = EFI_SUCCESS;
  EFI_BATTERY_CHARGING_STATUS ChargerErrors = EfiBatteryChargingStatusNone;

  if((!StateOfCharge) || (!RatedCapacity) || (!ChargeCurrent))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - NULL OUT parameters.\r\n"));
    Status = EFI_INVALID_PARAMETER;
    goto ErrorHandling;
  }

  Status = ChargerCheckBatteryStatus(StateOfCharge, RatedCapacity, ChargeCurrent, &ChargerErrors);

  //Log Charger Params
  ChargerLogParams(StateOfCharge, RatedCapacity, ChargeCurrent);

  /*
   * Handle Charger Errors
   */
  if (ChargerMaximumCurrent != 0)
  {
    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get Charger Errors(%d).\r\n", Status));
      goto ErrorHandling;
    }

    if ( (ChargerErrors != EfiBatteryChargingStatusNone) &&
         (ChargerErrors != EfiBatteryChargingStatusSuccess) )
    {
      ChargerMaximumCurrent = 0;

      Status = ChargerEnable(FALSE);

      Status = ChargerSignalEvent(ChargerErrors);

      if (EFI_ERROR(Status))
      {
        CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get signal event(%d).\r\n", Status));
        goto ErrorHandling;
      }
    }
    else if ( (*StateOfCharge >= ChargerTargetStateOfCharge) ||
              (ChargerErrors == EfiBatteryChargingStatusSuccess) )
    {
      if (ChargerTargetStateOfCharge != 100)
      {
        ChargerMaximumCurrent = 0;

        Status = ChargerEnable(FALSE);

        if (EFI_ERROR(Status))
        {
          CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to disable charger(%d).\r\n", Status));
          goto End;
        }

        Status = ChargerSignalEvent(EfiBatteryChargingStatusSuccess);

        if (EFI_ERROR(Status))
        {
          CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to signal event(%d).\r\n", Status));
          goto End;
        }
      }
      else
      {
        /* Do Nothing */
      }
    }
  }

End:
  return Status;

/*
 * Handle API Errors
 */
ErrorHandling:
  if (ChargerMaximumCurrent != 0)
  {
    ChargerMaximumCurrent = 0;

    Status = ChargerEnable(FALSE);

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (Error Handling) - Failed to disable charger(%d).\r\n", Status));
    }

    Status = ChargerSignalEvent(EfiBatteryChargingStatusDeviceError);

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (Error Handling) - Failed to signal event(%d).\r\n", Status));
    }
  }

  return Status;
}

/**
  ChargeBattery ()

  @brief
  ChargeBattery implementation of EFI_QCOM_CHARGER_PROTOCOL
 */
EFI_STATUS
EFI_ChargerChargeBattery (
  IN EFI_BATTERY_CHARGING_PROTOCOL *This,
  IN UINT32 MaximumCurrent,
  IN UINT32 TargetStateOfCharge,
  IN EFI_BATTERY_CHARGING_COMPLETION_TOKEN *CompletionToken )
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(!CompletionToken)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (ChargeBattery) - NULL completion token.\r\n"));
    Status = EFI_INVALID_PARAMETER;
    goto ErrorHandling;
  }
  /* Ensure that Target SOC is within 0 to 100%.*/
  if (TargetStateOfCharge > 100)
    TargetStateOfCharge = 100;

  if (MaximumCurrent == 0)
  {
    /* If the maximum charge current is 0 mA, then stop charging. */
    Status =  ChargerEnable(FALSE);

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (ChargeBattery) - Failed to get disable charger(%d).\r\n", Status));
      goto ErrorHandling;
    }

    /* Charging aborted by user - Signal Event */
    Status = ChargerSignalEvent( EfiBatteryChargingStatusAborted );

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (ChargeBattery) - Failed to signal event(%d).\r\n", Status));
      goto ErrorHandling;
    }
  }
  else
  {
    /* Save New Maximum Charge Level. */
    ChargerTargetStateOfCharge = TargetStateOfCharge;

    /* Save pointer to Completion pointer. */
    ChargerCompletionToken = CompletionToken;

    ChargerMaximumCurrent = MaximumCurrent;

    /* Set charger maximum charge current */
    Status = ChargerSetMaxUsbCurrent( MaximumCurrent );

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (ChargeBattery) - Failed to set max current(%d).\r\n", Status));
      goto ErrorHandling;
    }

    /* Enable Charger */
    Status = ChargerEnable(TRUE);

    if (EFI_ERROR(Status))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (ChargeBattery) - Failed to enable charger(%d).\r\n", Status));
      goto ErrorHandling;
    }
  }

ErrorHandling:
  return Status;
}

/**
  ChargerSignalEvent ()

  @brief
  Signal event when charge status has changed
 */
EFI_STATUS ChargerSignalEvent(EFI_BATTERY_CHARGING_STATUS ChgStatus)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (ChargerCompletionToken != NULL)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe - Signal Event(%d).\r\n", ChgStatus));

    /* Get Charging Battery Status */
    ChargerCompletionToken->Status = ChgStatus;
    Status = gBS->SignalEvent(ChargerCompletionToken->Event);

    if (Status != EFI_SUCCESS)
    {
       CHARGER_DEBUG((EFI_D_ERROR, " ChargerDxe - Failed to signal event to charger app, return St=0x%08X\r\n", \
                                                     Status));
    }
    else
    {
       CHARGER_DEBUG((EFI_D_ERROR, " ChargerDxe - Successfully signaled the Chg App.\r\n"));
    }
  }
  else /* Invalid Token Pointer. */
  {
    /* Report Invalid Token. */
    Status = EFI_INVALID_PARAMETER;
    CHARGER_DEBUG((EFI_D_ERROR, " ChargerDriverDxe - Invalid Token pointer, signal(%d).\r\n", ChgStatus));
  }

  return Status;
}


