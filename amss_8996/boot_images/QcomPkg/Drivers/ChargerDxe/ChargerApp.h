/** 
  @file  ChargerApp.h
  @brief Charger App API definitions.
*/
/*=============================================================================
  Copyright (c) 2011-2015 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 02/20/15   al      Adding stubs for key press
 01/14/15   al      Moving shutdown API to ChargerLib
 12/16/14   al      Adding as per new changes
 12/02/14   sm      New File

=============================================================================*/
#ifndef __CHARGERAPP_H__
#define __CHARGERAPP_H__

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
typedef enum {
  CHGAPP_DISP_CHARGING_SYMBOL,
  CHGAPP_DISP_BATTERY_MISSING,
  CHGAPP_DISP_CHARGER_MISSING,
  CHGAPP_DISP_MIN_SOC_THRESHOLD
}CHGAPP_DISP_IMAGE_TYPE; 


typedef struct 
{
  UINT32  SocThreshold;
  BOOLEAN SupportQcomChargingApp;
  BOOLEAN FullBattChargingEnabled;
  BOOLEAN OneTimeStickyChargingEnabled;
}ChgAppConfig;

typedef enum
{
  CHGAPP_THRESHOLD_CHARGING,
  CHGAPP_FULLBATTERY_CHARGING,
  CHGAPP_INVALID_CHARGING
}ChgAppChargingType;

/*===========================================================================
                     MACRO DEFINATIONS
===========================================================================*/
EFI_STATUS RunQcomChargerApp(ChgAppConfig ChargerConfig);

EFI_STATUS ChgAppFlushVariable(UINT16* variable, BOOLEAN Value);

/*For each ChargerError type take defined action*/
EFI_STATUS ChgAppErrorHandler(EFI_BATTERY_CHARGING_STATUS  ChargerErrors);

//EFI_STATUS ChargerAppWaitForAnyKey(BOOLEAN *bKeyPressed);

#endif  /* __CHARGERAPP_H__ */
