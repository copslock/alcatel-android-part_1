/** @file ChargerLib.c

  Provide the link to Charger Libraries

  Copyright (c) 2012-2015, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


 when         who     what, where, why
 --------     ---     -----------------------------------------------------------
 06/10/15     sm      Reading OsNonStandardBoot UEFI variable value in ChargerLibInit
 06/11/15      al      Adding support for SBC platform to boot to tiles
 06/04/15     al      Add ability to set current for uknown port type
 06/04/15     al      RTC alarm support 
 06/04/15     sv      Flushing UEFI NV variables after clearing Onetime sticky charging variable.
 05/12/15     mr      Enabled Charger functionality for 8952 (CR-846387)
 06/02/15     sm      Added API to pet charger watchdog
 06/02/15     al      Fix PMIC index   
 05/12/15     al      For CDP allow to boot irrespective of other errors
 04/05/15     sv      Using gRT->SetVariable function to clear EnableStickyChg instead of ChgAppFlushVariable.
 04/30/15     va      IChgMax and VddMax configuration support for all Jeita windows
                      JEITA Low temp, high temp handle
 04/30/15     al      Adding changes to disable USB as PON and shutdown device if battery missing
 04/11/15     sv      Added 50mV margin in calculated vbatt since the value read
                      from ADC is higher than the actual battery voltage
 03/23/15     sv      Added File logging support for 8909 target.
 04/17/15     al      Adding liquid platform
 04/09/15     va      JEITA algo Fix - Fcc and Fv Max to be set when temperature reading is available (8994)
 04/07/15     sv      Added GUID for Sticky Charging variable.
 04/01/15     al      Check battery temperature only if battery presence detected
 03/23/15     al      Adding SBC platform type
 02/24/15     sv      Added Onetime sticky charging support.
 02/20/15     al      Added to report parameters by default
 02/09/15     al      Setting charge current for conservative parameter
 02/04/15     al      Fixed file name
 01/14/15     al      Adding battery error handling
 01/13/15     sv      Set ChgFvMax configuration for QRD device to ChgQrdVddMaxInmV.
 01/13/15     va      Logging for ChargerApp states
 12/16/14     al      Adding AFP mode support
 12/10/14     al      Adding CHI platform support
 12/03/14     sm      Changed API definition for ChargerInit
                      Added ChargerCheckBatteryStatus  and ChargerGetMaxCurrent APIs
 11/25/14     sv      Updated SW Jeita feature for 8916
 11/24/14     sm      Added robustness for reporting current out-of-range error
 11/13/14     va      Fix for 64 bit compilation error (making signed comparision)
 11/13/14     sv      Made 8916 battery emulator configurable
 10/30/14     va      Added SW Jeita feature
 10/21/14     sm      Removed assigning ChargerErrros to None before exiting in BatteryGetChargerErrors API
 09/30/14     al      Added support 8916 battery emulator
 09/15/14     va      Enable File Logging
 09/02/14     vs      Updated ChargerLibInit for PM8909.(CR-679803)
 08/20/14     vs      Added SMB1360 support.
 08/04/14     sm      Added SMBCHG and FG protocol and API calls
 07/20/14     va      Compiling for  64 bit.
 06/27/14     vs      Removed SMB350Lib & BQ30Z55Lib specific code.
 06/24/14     vs      Protocol BmsInit changed to BmsVmInit.(CR-672289)
 01/31/14     ps      Added linear battery charger (LBC) and BMS voltage mode driver support
 07/15/13     al      Confirm ADC errors by reading twice
 07/02/13     sm      Added battery presence check in ChargerEnable API
 05/01/13     al      Added support for QRD platform
 03/12/13     sm      Changed the order in which Battery gauge and charger drivers
                      are initialized
 03/06/13     dy      Add Charger Extension APIs
                      Clean up Charger Error API
 03/05/13     sm      Added BQ Driver API call for WA platform
 02/19/13     sm      Added GetBatteryConfigData API call to get battery CFG data
 02/13/13     dy      Added Charger Enable/Disable Event
 02/12/13     sm      Added battery CFG file usage
 02/07/13     sm      Added SmbbInit API call
 01/15/13     sm      Added Bms_Init API call
 01/30/13     al      Cleaning warnings
 10/04/12     dy      New File

=============================================================================*/

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/QcomLib.h>
#include <api/systemdrivers/pmic/pm_uefi.h>

/**
  Protocol Dependencies
*/
#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/EFIPmicFg.h>
#include <Protocol/EFIPmicSmbchg.h>
#include <Protocol/EFIPmicVersion.h>
#include <Protocol/EFIPmicPwrOn.h>
#include <Protocol/EFIVariableServices.h>
#include <Protocol/EFIPmicRTC.h>

/**
  ADC Dependencies
 */
#include <Protocol/EFIAdc.h>

/**
  Ext HW Dependencies
*/
#include "ChargerLib.h"
#include "ChargerApp.h"

#define BATTERY_EMULATOR_UPPER_THRSHOLD_MV 4450
#define BATTERY_EMULATOR_LOWER_THRSHOLD_MV 3450
#define BATTERY_EMULATOR_BATT_ID 1500
#define BATTERY_EMULATOR_TEMP_C 23
#define BATTERY_EMULATOR_SOC 99

#define VBATT_TOLERANCE  50 //Battery upper voltage tolerance limit.

#define UNKNOWN_BATT_SHUTDOWN 0
#define UNKNOWN_BATT_BOOT_TO_HLOS 1
#define UNKNOWN_BATT_CONSERVATIVE_CHARGING 2
#define UNKNOWN_BATT_REGULAR_CHARGING 3
#define CHARGER_COOL_OFF_PERIOD_DEFAULT 300000 //5minutes=1000millisec*60*5=300000 milli sec
#define BOOT_THRESHOLD_VOLT 3600

#define TWO_SECONDS         2000000 /*2seconds = 2000000 uSec*/

static UINT32  BatteryEmulatorBatIdMin;
STATIC BOOLEAN BatteryEmulatorPresent = FALSE;


STATIC BOOLEAN QcomCharginAppSupported = TRUE;
STATIC UINT32  RtcDeviceIndex = PM_DEVICE_0;
/*===========================================================================*/
/*                  FUNCTIONS PROTOTYPES                                     */
/*===========================================================================*/

static EFI_STATUS ChargerGetAdcData(IN CONST CHAR8 *pszAdcInputName, OUT INT32 *pAdcData);
EFI_STATUS ChargerSetMaxBatCurrent(UINT32 FccMaxCurrent);
EFI_STATUS ChargerSetFvMaxVoltage(UINT32 VddMaxVoltage);

/* Jeita Funtions */
static EFI_STATUS
ChargerGetJeitaTHystState(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                          EFI_BATTARY_JEITA_TEMP_STATE *BatteryJeitaTstate,
                          EFI_BATTERY_CHARGING_STATUS  *ChargerErrors);

static EFI_STATUS
ChargerGetJeitaTState(EFI_PM_JEITA_CFG_DATA_TYPE  *BattConfigDataFg,
                      EFI_BATTARY_JEITA_TEMP_STATE *BatteryJeitaTstate,
                      EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

static EFI_STATUS
BatteryHandleJeitaT1LowLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
BatteryHandleJeitaT1T2Limit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
BatteryHandleJeitaT2T3Limit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

static EFI_STATUS
BatteryHandleJeitaT3T4Limit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
BatteryHandleJeitaT4T5Limit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
BatteryHandleJeitaCoolOffLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

static EFI_STATUS
BatteryHandleBeyondHighJeitaLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                                  EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

static EFI_STATUS
CheckBatteryVoltageHighLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
CheckBatteryChargeCurrent(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                          EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
static EFI_STATUS
CheckBatteryTempErrors(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                       EFI_BATTERY_CHARGING_STATUS *ChargerErrors);

VOID EFIAPI ChargerLibEnableCallback(IN EFI_EVENT Event, IN VOID *Context);
VOID EFIAPI ChargerLibDisableCallback(IN EFI_EVENT Event, IN VOID *Context);
EFI_STATUS ChargerGetFgErrors(EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
EFI_STATUS ChargerGetBmsVmErrors(EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
EFI_STATUS ChargerGetSmb1360Errors(EFI_BATTERY_CHARGING_STATUS *ChargerErrors);
EFI_STATUS ChargerLib_InitFileLog(void *);
EFI_STATUS ChargerLibInitJeitaCfgData(void);
EFI_STATUS ChargerLibInitJeitaCfg(void);
EFI_STATUS ChargerLibAlarmEnableStatus(BOOLEAN *EnableStatus);


/*===========================================================================*/
/*                  LOCAL VARIABLE DECLARATIONS                              */
/*===========================================================================*/

static EFI_QCOM_PMIC_SMBCHG_PROTOCOL        *PmicSmbchgProtocol  = NULL;
static EFI_QCOM_PMIC_FG_PROTOCOL            *PmicFgProtocol      = NULL;
static EFI_QCOM_PMIC_VERSION_PROTOCOL       *PmicVersionProtocol = NULL;
static EFI_QCOM_PMIC_PWRON_PROTOCOL         *PmicPwronProtocol = NULL;
static EFI_QCOM_PMIC_RTC_PROTOCOL           *PmicRtcProtocol = NULL;

static EFI_BATTERY_CHARGER_INFO_TYPE         ChargerInfo;
static EFI_BATTERY_GAUGE_INFO_TYPE           BatteryGaugeInfo;
static EFI_PM_FG_CFGDATA_TYPE                BattConfigDataFg;
static EFI_BATTERY_JEITA_T_INFO              BatteryJeitaTInfo;
STATIC ULogHandle                            gULogHandle = NULL;
static BOOLEAN                               RtcAlarmEnabled = FALSE;

EFI_EVENT EfiChargerEnableEvent = (EFI_EVENT)NULL;
EFI_EVENT EfiChargerDisableEvent = (EFI_EVENT)NULL;

extern EFI_GUID gEfiEventChargerEnableGuid;
extern EFI_GUID gEfiEventChargerDisableGuid;
extern EFI_GUID gQcomPmicPwrOnProtocolGuid;
extern EFI_GUID gQcomPmicRtcProtocolGuid;

static EFI_PLATFORMINFO_PLATFORM_TYPE PlatformType;
static EFI_PM_DEVICE_INFO_TYPE deviceInfo;

static EFI_PM_JEITA_CFG_DATA_TYPE JeitaConfigData;

EFI_GUID gStickyGuid = {0xaf02f6ad, 0xb602, 0x4873, {0xb4, 0x28, 0xc, 0xa0, 0xe, 0xcd, 0x91, 0xed}};

#define	STICKYCHARGING_RUNTIME_VARIABLE_NAME	L"StickyCharging"
extern EFI_GUID NonStandardBootGuid;



/*===========================================================================*/
/*                 EXTERNAL FUNCTION DECLARATIONS                            */
/*===========================================================================*/

/**
  Initialize ChargerDXE Library functions

  @param none

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_INVALID_PARAMETER: A Parameter was incorrect.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_NOT_READY:         The physical device is busy or not ready to
                         process this request.
*/

EFI_STATUS ChargerLibInit
(
  ChgAppConfig *ChargerConfig
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_PM_DEVICE_INFO_TYPE deviceInfoPMK;
  UINT32     NonStandardBoot = 0;
  UINTN      DataSize;

  Status = GetPlatformType(&PlatformType);

  /* Locate Version Protocol */
  if (!PmicVersionProtocol)
  {
    Status = gBS->LocateProtocol(&gQcomPmicVersionProtocolGuid, NULL, (VOID **)&PmicVersionProtocol);
  }
  /* Get PMIC Model */
  Status = PmicVersionProtocol->GetPmicInfo(PM_DEVICE_0, &deviceInfo);

  /*enable USB and DCIN PON triggers to be certain and avoid bad use case*/
  if (!PmicPwronProtocol)
  {
    Status = gBS->LocateProtocol(&gQcomPmicPwrOnProtocolGuid, NULL, (VOID **)&PmicPwronProtocol);
  }
  Status = PmicPwronProtocol->SetPonTrigger(PM_DEVICE_0, EFI_PM_PON_TRIGGER_DC_CHG, TRUE);
  Status = PmicPwronProtocol->SetPonTrigger(PM_DEVICE_0, EFI_PM_PON_TRIGGER_USB_CHG, TRUE);

  /* Configure Platform HW - Move to CFG File */
  switch (PlatformType)
  {
  case EFI_PLATFORMINFO_TYPE_MTP_MDM:
  case EFI_PLATFORMINFO_TYPE_MTP_MSM:
  case EFI_PLATFORMINFO_TYPE_FLUID:
  case EFI_PLATFORMINFO_TYPE_QRD:
  case EFI_PLATFORMINFO_TYPE_CDP:
  //case EFI_PLATFORMINFO_TYPE_CHI:
  case EFI_PLATFORMINFO_TYPE_SBC:
  case EFI_PLATFORMINFO_TYPE_LIQUID:
    {
        ChargerInfo.ChargerHW           = EfiBatteryChargerQcomPmicSmbChg;
        ChargerInfo.Version             = 0x00010000;
        BatteryGaugeInfo.BatteryGaugeHW = EfiBatteryGaugeQcomPmicFg;
        BatteryGaugeInfo.Version        = 0x00010000;
    }
    break;

  default:
    ChargerInfo.ChargerHW           = EfiBatteryChargerNone;
    ChargerInfo.Version             = 0x00010000;

    BatteryGaugeInfo.BatteryGaugeHW = EfiBatteryGaugeNone;
    BatteryGaugeInfo.Version        = 0x00010000;
    DEBUG((EFI_D_ERROR, "Charging not supported on this platform  \r\n"));
  }
  
  //Read UEFI Variable OsNonStandardBoot 
  DataSize = sizeof(NonStandardBoot);
  Status = gRT->GetVariable( L"OsNonStandardBoot",
                            &NonStandardBootGuid,
                             NULL,
                            &DataSize,
                            &NonStandardBoot);

  if(Status != EFI_SUCCESS)
  { //if variable does not exists or reading variable returns an error
    DEBUG((EFI_D_ERROR, "ChargerLibInit: OsNonStandardBoot GetVariable status error: 0x%08X \r\n", Status));
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerLibInit: OsNonStandardBoot GetVariable status error: 0x%08X \r\n", Status));
    NonStandardBoot = 0;
  }

  switch (BatteryGaugeInfo.BatteryGaugeHW)
  {
  case EfiBatteryGaugeQcomPmicFg:
    /* Locate FG Protocol */
    if (!PmicFgProtocol)
    {
      Status = gBS->LocateProtocol(
        &gQcomPmicFgProtocolGuid,
        NULL,
        (VOID **)&PmicFgProtocol
        );
    }

    /* Initialize FG Protocol */
    PmicFgProtocol->FgInit(PM_DEVICE_1);
    Status = PmicFgProtocol->GetBatteryConfigData(&BattConfigDataFg);
    if ((EFI_SUCCESS == Status) && (TRUE == BattConfigDataFg.PrintChargerAppDbgMsgToFile))
    {
      Status = ChargerLib_InitFileLog(NULL);
    }
    else
    {
      CHARGER_DEBUG((EFI_D_WARN, "Failed to Enable File Logging!\n"));
    }
    
    if(NonStandardBoot)
    {
      ChargerConfig->SocThreshold            = BattConfigDataFg.OsNonStandardBootSocThreshold;
    }
    else
    {
      ChargerConfig->SocThreshold            = BattConfigDataFg.OsStandardBootSocThreshold;
    }
    
    ChargerConfig->SupportQcomChargingApp = BattConfigDataFg.SupportQcomChargingApp;
    ChargerConfig->FullBattChargingEnabled = BattConfigDataFg.FullBattChargingEnabled;

    break;
  default:
    break;

  }

  /* Initialize JEITA CFG Data */
  Status = ChargerLibInitJeitaCfgData();
  if (Status != EFI_SUCCESS)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Unable Initialise SW JEITA CFG (%d)\n\r", Status));
  }

  /* Initialize Charger and Gauge HW */
  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    /* Locate SMBCHG Protocol */
    if (!PmicSmbchgProtocol)
    {
      Status = gBS->LocateProtocol(
        &gQcomPmicSmbchgProtocolGuid,
        NULL,
        (VOID **)&PmicSmbchgProtocol
        );
    }

    /* Initialize SMBCHG Protocol */
    Status = PmicSmbchgProtocol->SmbchgInit(PM_DEVICE_1);
    break;

  default:
    break;

  }

  if (!(ChargerConfig->SupportQcomChargingApp))
  {
    Status = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL,
                                TPL_CALLBACK,
                                ChargerLibEnableCallback,
                                NULL,
                                &gEfiEventChargerEnableGuid,
                                &EfiChargerEnableEvent);

    Status = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL,
                                TPL_CALLBACK,
                                ChargerLibDisableCallback,
                                NULL,
                                &gEfiEventChargerDisableGuid,
                                &EfiChargerDisableEvent);
  }

  //Jeita state Init
  BatteryJeitaTInfo.JeitaTState             = JEITA_STATE_INIT;
  BatteryJeitaTInfo.JeitaPrevTState         = JEITA_STATE_INIT;
  BatteryJeitaTInfo.JeitaNextTState         = JEITA_STATE_INIT;
  //JeitaInfo init
  Status = ChargerLibInitJeitaCfg();

  QcomCharginAppSupported = ChargerConfig->SupportQcomChargingApp;
  
  if (TRUE == ChargerConfig->FullBattChargingEnabled)
  {
    Status = PmicVersionProtocol->GetPmicInfo(PM_DEVICE_3, &deviceInfoPMK);
    if ((EFI_SUCCESS==Status) && (EFI_PMIC_IS_PMK8001 == deviceInfoPMK.PmicModel))
    {
      /*PMK8001 will be fourth PMIC*/
      RtcDeviceIndex = PM_DEVICE_3;
    }
    else
    {
      /*Primary PMIC*/
      RtcDeviceIndex = PM_DEVICE_0;
    }
    Status = ChargerLibAlarmEnableStatus(&RtcAlarmEnabled);
  }
  
  return Status;
}

/**
ChargerLibInitJeitaCfg()

@brief initializes the JEITA Config flags
Returns Status
*/
EFI_STATUS ChargerLibInitJeitaCfg(void)
{

  BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2  = FALSE;
  BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3  = FALSE;
  BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4  = FALSE;
  BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5  = FALSE;
  BatteryJeitaTInfo.IsFvMaxAdjustedT5       = FALSE;

  return EFI_SUCCESS;
}


/**
ChargerLibInitJeitaCfgData()

@brief initialises the JEITA CFG Data.
Returns Status
*/
EFI_STATUS ChargerLibInitJeitaCfgData(void)
{
  EFI_STATUS Status = EFI_SUCCESS;
  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    JeitaConfigData.BattTempJeitaT1Limit = BattConfigDataFg.JeitaCfgData.BattTempJeitaT1Limit;
    JeitaConfigData.BattTempJeitaT2Limit = BattConfigDataFg.JeitaCfgData.BattTempJeitaT2Limit;
    JeitaConfigData.BattTempJeitaT3Limit = BattConfigDataFg.JeitaCfgData.BattTempJeitaT3Limit;
    JeitaConfigData.BattTempJeitaT4Limit = BattConfigDataFg.JeitaCfgData.BattTempJeitaT4Limit;
    JeitaConfigData.BattTempJeitaT5Limit = BattConfigDataFg.JeitaCfgData.BattTempJeitaT5Limit;
    JeitaConfigData.BattTempHysteresis = BattConfigDataFg.JeitaCfgData.BattTempHysteresis;
    JeitaConfigData.BattTempLimLow = BattConfigDataFg.BattTempLimLow;
    JeitaConfigData.BattTempLimHigh = BattConfigDataFg.BattTempLimHigh;
    JeitaConfigData.ChgFccMax = BattConfigDataFg.ChgFccMax;
    JeitaConfigData.ChgFvMax = BattConfigDataFg.ChgFvMax;
    JeitaConfigData.SWJeitaEnable = BattConfigDataFg.JeitaCfgData.SWJeitaEnable;
    JeitaConfigData.WPBattVoltLimHighInmV = BattConfigDataFg.WPBattVoltLimHighInmV;
    JeitaConfigData.WPBattCurrLimHighInmA = BattConfigDataFg.WPBattCurrLimHighInmA;

    JeitaConfigData.BattTempLimLowCharging = BattConfigDataFg.JeitaCfgData.BattTempLimLowCharging;

    JeitaConfigData.ChgFvMaxInJeitaT2Limit = BattConfigDataFg.JeitaCfgData.ChgFvMaxInJeitaT2Limit;
    JeitaConfigData.ChgFvMaxInJeitaT3Limit = BattConfigDataFg.JeitaCfgData.ChgFvMaxInJeitaT3Limit;
    JeitaConfigData.ChgFvMaxInJeitaT4Limit = BattConfigDataFg.JeitaCfgData.ChgFvMaxInJeitaT4Limit;
    JeitaConfigData.ChgFvMaxInJeitaT5Limit = BattConfigDataFg.JeitaCfgData.ChgFvMaxInJeitaT5Limit;

    JeitaConfigData.ChgFccMaxInJeitaT2Limit = BattConfigDataFg.JeitaCfgData.ChgFccMaxInJeitaT2Limit;
    JeitaConfigData.ChgFccMaxInJeitaT3Limit = BattConfigDataFg.JeitaCfgData.ChgFccMaxInJeitaT3Limit;
    JeitaConfigData.ChgFccMaxInJeitaT4Limit = BattConfigDataFg.JeitaCfgData.ChgFccMaxInJeitaT4Limit;
    JeitaConfigData.ChgFccMaxInJeitaT5Limit = BattConfigDataFg.JeitaCfgData.ChgFccMaxInJeitaT5Limit;

    JeitaConfigData.ChargerCoolOffPeriodMs = BattConfigDataFg.JeitaCfgData.ChargerCoolOffPeriodMs;
    break;

  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Get Battery State of Charge

  @param[out] StateOfCharge  Battery SOC (%)

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetSOC(UINT32 *StateOfCharge)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (BatteryGaugeInfo.BatteryGaugeHW)
  {
  case EfiBatteryGaugeQcomPmicFg:
    Status = PmicFgProtocol->GetSoc(PM_DEVICE_1, StateOfCharge);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Get full charge capacity of the battery

  @param[out] RatedCapacity  returns rated capacity of battery (mAh)

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetRatedCapacity(UINT32 *RatedCapacity)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (BatteryGaugeInfo.BatteryGaugeHW)
  {
  case EfiBatteryGaugeQcomPmicFg:
    Status = PmicFgProtocol->GetRatedCapacity(PM_DEVICE_1, RatedCapacity);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Get charge current into the battery

  @param[out] ChargeCurrent  returns charge current (mA)
                             + : Charging
                             - : Discharging

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetChargeCurrent(INT32 *ChargeCurrent)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (BatteryGaugeInfo.BatteryGaugeHW)
  {
  case EfiBatteryGaugeQcomPmicFg:
    Status = PmicFgProtocol->GetChargeCurrent(PM_DEVICE_1, ChargeCurrent);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Get Battery Voltage into the battery

  @param[out] BattVoltage  returns current battery Voltage (mV)

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetBattVoltage(INT32 *BattVoltage, EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Battery Voltage */
  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicFgProtocol->GetBatteryVoltage(PM_DEVICE_1, (UINT32 *)BattVoltage);
    if (Status != EFI_SUCCESS)
    {
      *ChargerErrors = EfiBatteryChargingFgError;
    }
    break;
  default:
    *ChargerErrors = EfiBatteryChargingStatusDeviceError;
    break;
  }

  return Status;
}



static EFI_STATUS ChargerLibGetBattTemp(INT32 *BattTemp, EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;

  if (NULL == BattTemp || ChargerErrors == NULL)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Get battery temperature */
  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    /* Get battery temperature */
    Status = PmicFgProtocol->GetBatteryTemperature(BattTemp);
    if (Status != EFI_SUCCESS)
    {
      *ChargerErrors = EfiBatteryChargingFgError;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Unable to read Temperature (%d)\n\r", Status));
      Status = EFI_INVALID_PARAMETER;
    }
    break;
  default:
    break;
  }

  /*if status is not success then return here otherwise continue to assign error*/
  if (Status != EFI_SUCCESS)
  {
    return Status;
  }

  if (*BattTemp < JeitaConfigData.BattTempLimLow)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Charging Range Temperature : (%d) than config limit = (%d) \n\r", *BattTemp, JeitaConfigData.BattTempLimLow));
    *ChargerErrors =  EfiBatteryChargingStatusExtremeCold;
  }
  else if (*BattTemp < JeitaConfigData.BattTempLimLowCharging)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Temperature limit low : (%d) than config limit = (%d) \n\r", *BattTemp, JeitaConfigData.BattTempLimLow));
    *ChargerErrors =  EfiBatteryChargingTempBelowChargeLimit;
  }
  else if (*BattTemp > JeitaConfigData.BattTempLimHigh)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Range Temperature = (%d) than config limit = (%d) \n\r", *BattTemp, JeitaConfigData.BattTempLimHigh));
    *ChargerErrors = EfiBatteryChargingStatusOverheat;
  }
  else if (*BattTemp > JeitaConfigData.BattTempJeitaT5Limit)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charging Temperature Cool off Range  = (%d) config limit = (%d) \n\r", *BattTemp, JeitaConfigData.BattTempLimHigh));
    *ChargerErrors = EfiBatteryChargingTempCoolOff;
  }
  else
  {
    /*Don't do anything*/
  }

  return Status;
}


/**
  Gets current Jeita Temperature state and control Hysteresis apply

  @param[in]  JeitaConfigData      Battery temperature
  @param[out] BatteryJeitaTstate  Jeita Temperature state
  @param[out] ChargerErrors       Charging state Error Code

  @return
  EFI_SUCCESS:           Function returned successfully
  EFI_DEVICE_ERROR:      The physical device reported an error
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
ChargerGetJeitaTHystState(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                          EFI_BATTARY_JEITA_TEMP_STATE *BatteryJeitaTstate,
                          EFI_BATTERY_CHARGING_STATUS  *ChargerErrors)
{

  EFI_STATUS Status   = EFI_SUCCESS;
  INT32      BattTemp = 0;
  EFI_BATTARY_JEITA_TEMP_STATE JeitaCurrentTstate = JEITA_STATE_INVALID;

  if (!JeitaConfigData || !ChargerErrors || !BatteryJeitaTstate)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  /* Check for Low Temperature Limit */
  if (BattTemp < JeitaConfigData->BattTempLimLowCharging)
  {
    /*Read battery temperature again to confirm error was not spurious this is only required for low temperature */
    /* Get battery temperature */
    Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  }

  //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Jeita State Prev = (%d) Next (%d) \n\r", BatteryJeitaTInfo.JeitaPrevTState, BatteryJeitaTInfo.JeitaNextTState));

  //Get current Jeita TState
  Status = ChargerGetJeitaTState(JeitaConfigData, &JeitaCurrentTstate, ChargerErrors);
  if (Status != EFI_SUCCESS)
  {
    switch (ChargerInfo.ChargerHW)
    {
    case EfiBatteryChargerQcomPmicSmbChg:
      *ChargerErrors = EfiBatteryChargingFgError;
      break;
    default:
      break;
    }

    return EFI_DEVICE_ERROR;
  }

  /* Assign current state to Previous state */
  //BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
  switch (JeitaCurrentTstate)
  {
  case JEITA_STATE_INIT:
    //In Valid State after getting JEITA State
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Init Jeita State (%d)\n\r", JeitaCurrentTstate));
    break;

  case JEITA_STATE_T1_LOW_TEMP:

    //Just Update states no Hysteresis is needed for low, high temp limits
    *BatteryJeitaTstate = JeitaCurrentTstate;
    BatteryJeitaTInfo.JeitaPrevTState = JEITA_STATE_INVALID;
    BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T1_T2;
    break;

  case JEITA_STATE_T1_T2:

    //If prev state is equal to current state
    if (JEITA_STATE_T2_T3 == BatteryJeitaTInfo.JeitaPrevTState)
    {
      //Apply Hysteresis
      if (BattTemp <= (JeitaConfigData->BattTempJeitaT2Limit - JeitaConfigData->BattTempHysteresis))
      {
        //temp is less than 8, stay is in current state i.e. T1_t2
        //change PS (JEITA_STATE_T1_LOW_TEMP) and NS (JEITA_STATE_T2_T3)
        BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
        *BatteryJeitaTstate = JeitaCurrentTstate;
        BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T2_T3;
      }
      else
      {
        //Remain in previous state as Temperature might be toggling i.e. hysteresis
        *BatteryJeitaTstate = BatteryJeitaTInfo.JeitaTState;
      }

    }
    else if (JEITA_STATE_INVALID == BatteryJeitaTInfo.JeitaPrevTState)
    {
      //Apply Hysteresis
      if (BattTemp >= (JeitaConfigData->BattTempJeitaT1Limit + JeitaConfigData->BattTempHysteresis))
      {
        //temp is less than 8, stay is in current state i.e. T1_t2
        //change PS (JEITA_STATE_T1_LOW_TEMP) and NS (JEITA_STATE_T2_T3)
        BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
        *BatteryJeitaTstate = JeitaCurrentTstate;
        BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T2_T3;
      }
      else
      {
        //Remain in previous state as Temperature might be toggling i.e. hysteresis
        *BatteryJeitaTstate = BatteryJeitaTInfo.JeitaTState;
      }
    }
    else
    {
      // This means there is sudden raise/fall in battery temperature then to act immediatly change state
      BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
      *BatteryJeitaTstate = JeitaCurrentTstate;
      BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T2_T3;
    }
    break;

  case JEITA_STATE_T2_T3:

    //If prev state is equal to current state
    if (JEITA_STATE_T3_T4 == BatteryJeitaTInfo.JeitaPrevTState)
    {
      //Apply Hysteresis
      if (BattTemp <= (JeitaConfigData->BattTempJeitaT3Limit - JeitaConfigData->BattTempHysteresis))
      {
        //temp is less than 43, stay is in current state i.e. T2_T3
        //change PS (JEITA_STATE_T1_T2) and NS (JEITA_STATE_T3_T4)
        BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
        *BatteryJeitaTstate = JeitaCurrentTstate;
        BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T3_T4;
      }
      else
      {
        //Remain in previous state as Temperature might be toggling i.e. hysteresis
        *BatteryJeitaTstate = BatteryJeitaTInfo.JeitaTState;
      }
    }
    else
    {
      // This means there is sudden raise/fall in battery temperature then to act immediatly change state
      BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
      *BatteryJeitaTstate = JeitaCurrentTstate;
      BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T3_T4;
    }
    break;

  case JEITA_STATE_T3_T4:

    //If prev state is equal to current state
    if (JEITA_STATE_T4_T5 == BatteryJeitaTInfo.JeitaPrevTState)
    {
      //Apply Hysteresis
      if (BattTemp <= (JeitaConfigData->BattTempJeitaT4Limit - JeitaConfigData->BattTempHysteresis))
      {
        //temp is less than 48, stay is in current state i.e. T3_T4
        //change PS (JEITA_STATE_T2_T3) and NS (JEITA_STATE_T4_T5)
        BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
        *BatteryJeitaTstate = JeitaCurrentTstate;
        BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T4_T5;
      }
      else
      {
        //Remain in previous state as Temperature might be toggling i.e. hysteresis
        *BatteryJeitaTstate = BatteryJeitaTInfo.JeitaTState;
      }
    }
    else
    {
      // This means there is sudden raise/fall in battery temperature then to act immediatly change state
      BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
      *BatteryJeitaTstate = JeitaCurrentTstate;
      BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_T4_T5;
    }
    break;

  case JEITA_STATE_T4_T5:
    //If prev state is equal to current state
    if (JEITA_STATE_COOL_OFF == BatteryJeitaTInfo.JeitaPrevTState)
    {
      //Apply Hysteresis
      if (BattTemp <= (JeitaConfigData->BattTempJeitaT5Limit - JeitaConfigData->BattTempHysteresis))
      {
        //temp is less than 58, stay is in current state i.e. T4_T5
        //change PS (JEITA_STATE_T3_T4) and NS (JEITA_STATE_T5_MAX)
        BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
        *BatteryJeitaTstate = JeitaCurrentTstate;
        BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_COOL_OFF;
      }
      else
      {
        //Remain in previous state as Temperature might be toggling i.e. hysteresis
        *BatteryJeitaTstate = BatteryJeitaTInfo.JeitaTState;
      }
    }
    else
    {
      // This means there is sudden raise/fall in battery temperature then to act immediatly change state
      BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
      *BatteryJeitaTstate = JeitaCurrentTstate;
      BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_COOL_OFF;
    }
    break;

  case JEITA_STATE_COOL_OFF:
    // if Cool off then just be in Cool Off irrespective of previous state and no hysteresis required
    BatteryJeitaTInfo.JeitaPrevTState = BatteryJeitaTInfo.JeitaTState;
    *BatteryJeitaTstate = JeitaCurrentTstate;
    BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_MAX_JEITA_T;
    break;

  case JEITA_STATE_MAX_JEITA_T:
    //Update states
    *BatteryJeitaTstate = JeitaCurrentTstate;
    BatteryJeitaTInfo.JeitaPrevTState = JEITA_STATE_COOL_OFF;
    BatteryJeitaTInfo.JeitaNextTState = JEITA_STATE_INVALID;
    break;

  default:
  case JEITA_STATE_INVALID:
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: ChargerGetJeitaTHystState JEITA current state (%d Prev state (%d) Next state (%d))\n\r",
                    JeitaCurrentTstate, BatteryJeitaTInfo.JeitaPrevTState, BatteryJeitaTInfo.JeitaNextTState));
    break;
  }

  /*CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Jeita State Current = (%d) Prev = (%d) Next = (%d) \n\r",
                 JeitaCurrentTstate, BatteryJeitaTInfo.JeitaPrevTState, BatteryJeitaTInfo.JeitaNextTState));*/

  return Status;
}


/**
  Gets current Jeita Temperature state

  @param[in]  JeitaConfigData      Battery temperature
  @param[out] BatteryJeitaTstate  Jeita Temperature state
  @param[out] ChargerErrors       Charging state Error Code

  @return
  EFI_SUCCESS:           Function returned successfully
  EFI_DEVICE_ERROR:      The physical device reported an error
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
ChargerGetJeitaTState(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                      EFI_BATTARY_JEITA_TEMP_STATE *BatteryJeitaTstate,
                      EFI_BATTERY_CHARGING_STATUS  *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;
  INT32      BattTemp = 0;

  if (!JeitaConfigData || !ChargerErrors || !BatteryJeitaTstate)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  /* Check for Low Temperature Limit */
  if ((BattTemp < JeitaConfigData->BattTempLimLowCharging) || (BattTemp > JeitaConfigData->BattTempLimHigh))
  {
    /*Read battery temperature again to confirm error was not spurious this is only required for low temperature */
    /* Get battery temperature */
    Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);
  }

  //Jeita Temperature Low Limit - Less than 0
  if (BattTemp < JeitaConfigData->BattTempLimLowCharging)
  {
    *BatteryJeitaTstate = JEITA_STATE_T1_LOW_TEMP;
  }

  //Jeita Temperature Oveheat limit - Greater than 70 /BattTempLimHigh
  else if (BattTemp > JeitaConfigData->BattTempLimHigh)
  {
    *BatteryJeitaTstate = JEITA_STATE_MAX_JEITA_T;
  }
  //Jeita Temperature Low Limit - Greater than 0 and Less than or equal to 10
  else if ((BattTemp <= JeitaConfigData->BattTempJeitaT2Limit) &&
           (BattTemp >= JeitaConfigData->BattTempLimLowCharging))
  {
    *BatteryJeitaTstate = JEITA_STATE_T1_T2;
  }
  //Jeita Temperature normal Limit - Greater than 10 and Less than equal to 45
  else if ((BattTemp > JeitaConfigData->BattTempJeitaT2Limit) &&
           (BattTemp <= JeitaConfigData->BattTempJeitaT3Limit))
  {
    *BatteryJeitaTstate = JEITA_STATE_T2_T3;
  }
  //Jeita Temperature high Limit - Greater than 45 and Less than equal to 50
  else if ((BattTemp > JeitaConfigData->BattTempJeitaT3Limit) &&
           (BattTemp <= JeitaConfigData->BattTempJeitaT4Limit))
  {
    *BatteryJeitaTstate = JEITA_STATE_T3_T4;
  }
  //Jeita Temperature normal Limit - Greater than 50 and Less than equal to 55
  else if ((BattTemp > JeitaConfigData->BattTempJeitaT4Limit) &&
           (BattTemp <= JeitaConfigData->BattTempJeitaT5Limit))
  {
    *BatteryJeitaTstate = JEITA_STATE_T4_T5;
  }
  //Jeita Temperature normal Limit - Greater than 55 and Less than equal to 60
  else if ((BattTemp > JeitaConfigData->BattTempJeitaT5Limit) &&
           (BattTemp <= JeitaConfigData->BattTempLimHigh))
  {
    *BatteryJeitaTstate = JEITA_STATE_COOL_OFF;
  }
  else
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: InCorrect JeitaTState = (%d) \n\r", *BatteryJeitaTstate));
    *ChargerErrors = EfiBatteryChargingStatusDeviceError;
    Status = EFI_DEVICE_ERROR;
  }

  CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Battery Temperature = (%d) Current Jeita State (%d)\n\r", BattTemp, *BatteryJeitaTstate));

  return Status;

}


/**
  Handles when Battery temperature is Very High

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Charging state Error Code

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaT4T5Limit(EFI_PM_JEITA_CFG_DATA_TYPE  *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  if ((FALSE == BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5) && (JEITA_STATE_T4_T5 == BatteryJeitaTInfo.JeitaTState))
  {
    //Set VDD MAX To T5 Limit (Apply/Reset only once)
    Status = ChargerSetFvMaxVoltage(JeitaConfigData->ChgFvMaxInJeitaT5Limit);
    /*Set Fcc To its default value (Apply/Reset only once) */
    Status |= ChargerSetMaxBatCurrent(JeitaConfigData->ChgFccMaxInJeitaT5Limit);
    if (EFI_SUCCESS == Status)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FvMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFvMaxInJeitaT5Limit, BatteryJeitaTInfo.JeitaTState));
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FccMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFccMaxInJeitaT5Limit, BatteryJeitaTInfo.JeitaTState));
      //Update Adjust Flag
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = TRUE;
    }
    else
    {
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = FALSE;
      *ChargerErrors = EfiBatteryChargingStatusDeviceError;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Error in Setting FvMax = %d, ChargerErrors = %d \n\r", Status,*ChargerErrors));
    }
    //Making sure to reset other state flags
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = FALSE;
    BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = FALSE;

  }
  else
  {
    //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: IsFvMaxAdjustedT4T5 Not set = (%d) and JEITA state (%d) \n\r", BatteryJeitaTInfo.IsFvMaxAdjustedT4T5, BatteryJeitaTInfo.JeitaTState));
  }

  return Status;

}


/**
  Handle when Battery temperature is High

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Charging state Error Code

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaT3T4Limit(EFI_PM_JEITA_CFG_DATA_TYPE  *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  if ((FALSE == BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4) && (JEITA_STATE_T3_T4 == BatteryJeitaTInfo.JeitaTState))
  {

    //Set VDD MAX To T4 Limit (Apply/Reset only once)
    Status = ChargerSetFvMaxVoltage(JeitaConfigData->ChgFvMaxInJeitaT4Limit);
    /*Set Fcc To its default value (Apply/Reset only once) */
    Status |= ChargerSetMaxBatCurrent(JeitaConfigData->ChgFccMaxInJeitaT4Limit);
    if (EFI_SUCCESS == Status)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FccMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFccMaxInJeitaT4Limit, BatteryJeitaTInfo.JeitaTState));
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FvMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFvMaxInJeitaT4Limit, BatteryJeitaTInfo.JeitaTState));
      //Update Adjust Flag
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = TRUE;
    }
    else
    {
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = FALSE;
      *ChargerErrors = EfiBatteryChargingStatusDeviceError;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Error in Setting FvMAX = (%d), ChargerErrors = (%d) \n\r", Status,*ChargerErrors));
    }
    //Making sure to reset other state flags
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 = FALSE;
    BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = FALSE;
  }
  else
  {
    //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: IsFvMaxAdjustedT3T4 Not set = (%d) and JEITA state (%d) \n\r", BatteryJeitaTInfo.IsFvMaxAdjustedT3T4 , BatteryJeitaTInfo.JeitaTState));
  }

  return Status;

}


/**
  Handle when Battery temperature is Normal

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Charging state Error Code

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaT2T3Limit(EFI_PM_JEITA_CFG_DATA_TYPE  *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  if ((FALSE == BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 ) && (JEITA_STATE_T2_T3 == BatteryJeitaTInfo.JeitaTState ))
  {
    /*Set Fcc To its default value (Apply/Reset only once) */
    Status = ChargerSetMaxBatCurrent(JeitaConfigData->ChgFccMaxInJeitaT3Limit);
    /* Set Fv Max */
    Status |= ChargerSetFvMaxVoltage(JeitaConfigData->ChgFvMaxInJeitaT3Limit);
    if (EFI_SUCCESS == Status)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FccMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFccMaxInJeitaT3Limit, BatteryJeitaTInfo.JeitaTState));
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FvMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFvMaxInJeitaT3Limit, BatteryJeitaTInfo.JeitaTState));
      //Update Adjust Flag
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3  = TRUE;
    }
    else
    {
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3  = FALSE;
      *ChargerErrors = EfiBatteryChargingStatusDeviceError;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Error in ReSetting = %d, ChargerErrors = %d \n\r", Status,*ChargerErrors));
      goto ErrorExit;
    }

    //Making sure to reset other state flags
    BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2  = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4  = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5  = FALSE;
  }
  else
  {
    //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: IsFccMaxAdjusted Not set = (%d) and BatteryJeitaTInfo.IsFvMaxAdjustedT2T3 not Set (%d) \n\r", BatteryJeitaTInfo.IsFccMaxAdjusted, BatteryJeitaTInfo.IsFvMaxAdjustedT2T3));
  }

ErrorExit:
  return Status;

}


/**
  Handle when Battery temperature Low Limit

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Returns error code for charging state if any

  @return
  EFI_SUCCESS:       Function returned successfully.
  EFI_DEVICE_ERROR:  The physical device reported an error.
  EFI_UNSUPPORTED:   No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaT1T2Limit(EFI_PM_JEITA_CFG_DATA_TYPE  *JeitaConfigData,
                            EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  if ((FALSE == BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2) && (JEITA_STATE_T1_T2 == BatteryJeitaTInfo.JeitaTState))
  {
    // Set IBAT MAX to half of Current IUSB MAX value (Apply only once )
    Status = ChargerSetMaxBatCurrent(JeitaConfigData->ChgFccMaxInJeitaT2Limit);
    /* Update FvMax Once for initilization */
    Status |= ChargerSetFvMaxVoltage(JeitaConfigData->ChgFvMaxInJeitaT2Limit);
    if (EFI_SUCCESS == Status)
    {
      BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = TRUE;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FccMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFccMaxInJeitaT2Limit, BatteryJeitaTInfo.JeitaTState));
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FvMax Adjusted to = (%d) and JEITA state (%d) \n\r", JeitaConfigData->ChgFvMaxInJeitaT2Limit, BatteryJeitaTInfo.JeitaTState));
    }
    else
    {
      *ChargerErrors = EfiBatteryChargingStatusDeviceError;
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Error in Adjusting FccMax = %d, ChargerErrors = %d \n\r", Status,*ChargerErrors));
    }
    //Update Flags
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3  = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4  = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5  = FALSE;
  }
  else
  {
    //CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Charger FccMax Already Adjusted = (%d) and JEITA state (%d) \n\r", BatteryJeitaTInfo.IsFccMaxAdjusted, BatteryJeitaTInfo.JeitaTState));
  }

  return Status;

}


/**
  Handle when Battery temperature Low Limit

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Returns error code for charging state if any

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaT1LowLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;
  INT32      BattTemp = 0;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  /* Check for Low Temperature Limit */
  if (BattTemp < JeitaConfigData->BattTempLimLowCharging)
  {
    /*Read battery temperature again to confirm error was not spurious this is only required for low temperature */
    /* Get battery temperature */
    Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

    if (BattTemp < JeitaConfigData->BattTempLimLow)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Charging Range Temperature : (%d) than config limit = (%d) \n\r", BattTemp, JeitaConfigData->BattTempLimLow));
      *ChargerErrors =  EfiBatteryChargingStatusExtremeCold;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = FALSE;
      BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = FALSE;
    }
    else if (BattTemp < JeitaConfigData->BattTempLimLowCharging)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Range Temperature : (%d) than config limit = (%d) \n\r", BattTemp, JeitaConfigData->BattTempLimLow));
      *ChargerErrors =  EfiBatteryChargingTempBelowChargeLimit;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = FALSE;
      BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = FALSE;
    }
    else
    {
      /*else nothing*/
    }
  }

  return Status;
}


/**
  Handle when Battery temperature extreme High Limit

  @param[in]  JeitaConfigData  Battery config data
  @param[out] ChargerErrors   Returns error code for charging state if any

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
BatteryHandleJeitaCoolOffLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;
  INT32      BattTemp = 0;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Get battery temperature */
  Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  /* Check for High Temperature Limit */

  if ((BattTemp >= JeitaConfigData->BattTempJeitaT5Limit) && (BattTemp <= JeitaConfigData->BattTempLimHigh))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Above High JEITA temp limit = (%d) than config limit = (%d). Going into wait \n\r", BattTemp, JeitaConfigData->BattTempLimHigh));

    *ChargerErrors =  EfiBatteryChargingTempCoolOff;
    /*Reset All Flags*/
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3 = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4 = FALSE;
    BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5 = FALSE;
    BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2 = FALSE;
    BatteryJeitaTInfo.IsFvMaxAdjustedT5      = FALSE;
  }

  return Status;
}



static EFI_STATUS
BatteryHandleBeyondHighJeitaLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                                  EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS Status   = EFI_SUCCESS;
  INT32      BattTemp = 0;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Get battery temperature */
  Status = ChargerLibGetBattTemp(&BattTemp, ChargerErrors);

  /* Check for High Temperature Limit */
  if (BattTemp >= JeitaConfigData->BattTempLimHigh)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Range Temperature = (%d) than config limit = (%d) \n\r", BattTemp, JeitaConfigData->BattTempLimHigh));
    *ChargerErrors = EfiBatteryChargingStatusOverheat;
  }

  return Status;

}


/**
  Check Battery Voltage  High Limit

  @param[out] ChargerErrors  returns error code for charging state if any

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
CheckBatteryVoltageHighLimit(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                             EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS  Status      = EFI_SUCCESS;
  INT32       BattVoltage = 0;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  Status = BatteryGetBattVoltage(&BattVoltage, ChargerErrors);
  if (EFI_SUCCESS == Status)
  {
    /* Check for High Voltage Limit */
    if ((UINT32)BattVoltage > (INT32)JeitaConfigData->WPBattVoltLimHighInmV)
    {
      Status = BatteryGetBattVoltage(&BattVoltage, ChargerErrors);
      /* Check for High Voltage Limit */
      if ((UINT32)(BattVoltage - VBATT_TOLERANCE) > (INT32)JeitaConfigData->WPBattVoltLimHighInmV)
      {
        CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out-of-range BattVoltage = (%d) than config limit = (%d)\n\r", BattVoltage, JeitaConfigData->WPBattVoltLimHighInmV));
        *ChargerErrors = EfiBatteryChargingStatusVoltageOutOfRange;
      }
    }
  }

  return Status;

}

/**
  Check Battery Charge Current

  @param[out] ChargerErrors  returns error code for charging state if any

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS
CheckBatteryChargeCurrent(EFI_PM_JEITA_CFG_DATA_TYPE  *JeitaConfigData,
                          EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS  Status   = EFI_SUCCESS;
  INT32       BattCurrent = 0;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Get Battery Charge Current */
  Status = BatteryGetChargeCurrent(&BattCurrent);
  if (EFI_SUCCESS != Status)
  {
    *ChargerErrors = EfiBatteryChargingBmsError;
    goto ErrorExit;
  }
  if (BattCurrent > (INT32)JeitaConfigData->WPBattCurrLimHighInmA)
  {
    /*Read charge current again to confirm error was not spurious*/
    Status = BatteryGetChargeCurrent(&BattCurrent);
    /* Check for High Current Limit */
    if (BattCurrent > (INT32)JeitaConfigData->WPBattCurrLimHighInmA)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: BattCurrent out-of-range (%d) than config limit = (%d)\n\r", BattCurrent, JeitaConfigData->WPBattCurrLimHighInmA));
      *ChargerErrors = EfiBatteryChargingStatusCurrentOutOfRange;
      goto ErrorExit;
    }
  }

ErrorExit:
  return Status;

}



/**
  Get charge errors for high/low temperature conditions

  @param[in] JeitaConfigData   Battery temperature
  @param[out] ChargerErrors  returns error code for charging state

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
static EFI_STATUS CheckBatteryTempErrors(EFI_PM_JEITA_CFG_DATA_TYPE *JeitaConfigData,
                                         EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS  Status = EFI_SUCCESS;
  EFI_BATTARY_JEITA_TEMP_STATE BatteryJeitaTstate = JEITA_STATE_FORCE_32_BITS;

  if (!JeitaConfigData || !ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  //Check Battery Temp High limit
  Status = ChargerGetJeitaTHystState(JeitaConfigData, &BatteryJeitaTstate, ChargerErrors);
  if (/*(EfiBatteryChargingStatusNone != *ChargerErrors) || */(EFI_SUCCESS != Status))
  {
    goto ErrorExit;
  }
  //Keep track of crrent state
  BatteryJeitaTInfo.JeitaTState = BatteryJeitaTstate;

  /* Get Battery temperature again after cool off to know if charging needs to be enabled */
  if((JEITA_STATE_COOL_OFF == BatteryJeitaTInfo.JeitaPrevTState )&&
     (JEITA_STATE_COOL_OFF != BatteryJeitaTInfo.JeitaTState)) /* If JEITA current state is also cool off then do not enable charging */
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Previous State was Cool Off = (%d) Enabling Charging at Current State (%d)\n\r",
                   BatteryJeitaTInfo.JeitaPrevTState, BatteryJeitaTInfo.JeitaTState));
    /* Charging needs to be enabled */
    ChargerEnable(TRUE);
  }
  else if((JEITA_STATE_COOL_OFF == BatteryJeitaTInfo.JeitaTState )&&
          (JEITA_STATE_COOL_OFF != BatteryJeitaTInfo.JeitaPrevTState))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: JEITA Cool Off State = (%d) Disable Charging \n\r",
                   BatteryJeitaTInfo.JeitaTState));
    /* Charging needs to be enabled */
    ChargerEnable(FALSE);
  }

  switch (BatteryJeitaTstate)
  {
  case JEITA_STATE_T1_LOW_TEMP:
    //Check Battery Temp low limit
    Status = BatteryHandleJeitaT1LowLimit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  case JEITA_STATE_T1_T2:
    //Check for JEITA standard limitation  IF BattTemp is greater than 0 and less than BattTempJeitaT2Limit (default 10)
    Status = BatteryHandleJeitaT1T2Limit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  case JEITA_STATE_T2_T3:
    //Check BattTemp for Normal Temperature (temp greater than 10 and less than High Limit / BattTempJeitaT3Limit)
    Status = BatteryHandleJeitaT2T3Limit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  case JEITA_STATE_T3_T4:
    //Check for JEITA standard limitation  If BattTemp is greater than BattTempJeitaT3Limit and less than BattTempJeitaT4Limit
    Status = BatteryHandleJeitaT3T4Limit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  case JEITA_STATE_T4_T5:
    //Check BattTemp for High Temperature ( temp greater than BattTempJeitaT4Limit and less than BattTempJeitaT5Limit )
    Status = BatteryHandleJeitaT4T5Limit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  case JEITA_STATE_COOL_OFF:
    //Check Battery for Extreme Temp High limit (temp greater than BattTempJeitaT5Limit/ BattTempLimHigh)
    Status = BatteryHandleJeitaCoolOffLimit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

   case JEITA_STATE_MAX_JEITA_T:
    //Check Battery for Extreme Temp High limit (temp greater than BattTempLimHigh)
    Status = BatteryHandleBeyondHighJeitaLimit(JeitaConfigData, ChargerErrors);
    if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
    {
      goto ErrorExit;
    }
    break;

  default:
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: InCorrect JeitaTState = (%d) \n\r", BatteryJeitaTInfo.JeitaTState));
    *ChargerErrors = EfiBatteryChargingStatusDeviceError;
    Status = EFI_DEVICE_ERROR;
    break;

  }

ErrorExit:
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: JeitaTState = (%d) ChargerErrors = (%d) \n\r", BatteryJeitaTInfo.JeitaTState,*ChargerErrors));
  return Status;

}


/**
  Get charge errors

  @param[out] ChargerErrors  returns error code for charging state

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetChargerErrors(EFI_BATTERY_CHARGING_STATUS *ChargerErrors)
{
  EFI_STATUS                      Status = EFI_SUCCESS;
  EFI_CHARGER_ATTACHED_CHGR_TYPE  AttachedCharger = EFI_CHARGER_ATTACHED_CHGR__NONE;

  BOOLEAN BatteryPresent = FALSE;

  if (!ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  *ChargerErrors = EfiBatteryChargingStatusNone;

  /*  Common HW Error Detection  */

  /* Battery Presence Detection */
  Status = BatteryGetBatteryPresence(&BatteryPresent);
  if (!BatteryPresent)
  {
    *ChargerErrors = EfiBatteryChargingStatusBatteryNotDetected;
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: BatteryGetChargerErrors: ERROR: Battery not detected \r\n"));
    return Status;
  }

  /* Charge Source Detection */
  Status = BatteryGetPowerPath(&AttachedCharger);
  if ((AttachedCharger == EFI_CHARGER_ATTACHED_CHGR__BATT) || (EFI_CHARGER_ATTACHED_CHGR__NONE == AttachedCharger))
  {
    *ChargerErrors = EfiBatteryChargingSourceNotDetected;
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: BatteryGetChargerErrors: ERROR: Charging Source not detected \r\n"));
    return Status;
  }

  switch (BatteryGaugeInfo.BatteryGaugeHW)
  {
    case EfiBatteryGaugeQcomPmicFg:
      Status = ChargerGetFgErrors(ChargerErrors);
      if (Status != EFI_SUCCESS)
      {
        *ChargerErrors = EfiBatteryChargingFgError;
        CHARGER_DEBUG((EFI_D_ERROR, "BatteryGetChargerErrors ERROR: FG Error detected \r\n"));
      }
      break;
    default:
      *ChargerErrors = EfiBatteryChargingStatusNone;
      break;
  }

  return Status;
}

/**
  Enable charger

  @param[in] Enable  TRUE  - Enable Charging
                     FALSE - Disable Charging

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerEnable(BOOLEAN Enable)
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_EVENT  EnableEvent;
  BOOLEAN    BatteryPresent = FALSE;

  Status = BatteryGetBatteryPresence(&BatteryPresent);
  if (BatteryPresent)
  {
    switch (ChargerInfo.ChargerHW)
    {
    case EfiBatteryChargerQcomPmicSmbChg:
      Status = PmicSmbchgProtocol->EnableCharger(PM_DEVICE_1, Enable);
    default:
      Status = EFI_UNSUPPORTED;
      break;
    }

    if (!QcomCharginAppSupported)
    {
      /* Signal Charger Enable/Disable Event */
      EnableEvent = (Enable) ? EfiChargerEnableEvent : EfiChargerDisableEvent;
      Status = gBS->SignalEvent(EnableEvent);
    }
  }

  return Status;
}

/**
  Set charger max current

  @param[in] MaxCurrent  Max current for charger in mA

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerSetMaxUsbCurrent(UINT32 MaxCurrent)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicSmbchgProtocol->SetUsbMaxCurrent(PM_DEVICE_1, MaxCurrent);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Set Battery FCC Max current

  @param[in] IBatMaxCurrent  Max current for charger in mA

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerSetMaxBatCurrent(UINT32 IBatMaxCurrent)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    //IUSB MAX value is rounded to lower value
    Status = PmicSmbchgProtocol->SetFccMaxCurrent(PM_DEVICE_1, IBatMaxCurrent);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}


/**
  Set Battery Floating Max Voltage

  @param[in] VddMaxVoltage  Max Voltage for Charger in mV

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerSetFvMaxVoltage(UINT32 VddMaxVoltage)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    //IUSB MAX value is rounded to lower value
    Status = PmicSmbchgProtocol->SetFvMaxVoltage(PM_DEVICE_1, VddMaxVoltage);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}


/**
  Get Charger/Power Path

  @param[out] Powerpath

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_INVALID_PARAMETER: Parameter is invalid.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetPowerPath(EFI_CHARGER_ATTACHED_CHGR_TYPE *PowerPath)
{
  EFI_STATUS Status = EFI_SUCCESS;
  //BOOLEAN SMBCharger = FALSE;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicSmbchgProtocol->GetChargePath(PM_DEVICE_1, (PM_SMBCHG_PWR_PTH_TYPE *)PowerPath);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/*support battery emulator for SMB1360 charger*/
EFI_STATUS BatteryGetBatteryEmulatorPresence(BOOLEAN *BatteryPresence)
{
  EFI_STATUS Status = EFI_SUCCESS;
  INT32   BattVoltage = 0;
  INT32   BattId = 0;

  /*read battery ID and battery voltage to check for battery emulator.
    SMB1360 doesn't give correct value for battery emulator and so use ADC*/
  Status = ChargerGetAdcData(EFI_ADC_INPUT_BATT_ID, &BattId);
  Status = ChargerGetAdcData(EFI_ADC_INPUT_VBATT, &BattVoltage);

  if ((Status == EFI_SUCCESS)
      && (BattId > (INT32)BatteryEmulatorBatIdMin)
      && (BattVoltage < BATTERY_EMULATOR_UPPER_THRSHOLD_MV)
      && (BattVoltage > BATTERY_EMULATOR_LOWER_THRSHOLD_MV)
      )
  {
    BatteryEmulatorPresent =  TRUE;
  }
  else
  {
    BatteryEmulatorPresent =  FALSE;
  }

  *BatteryPresence = BatteryEmulatorPresent;

  return Status;
}

/**
  Get Battery Presence

  @param[out] BatteryPresence

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_INVALID_PARAMETER: Parameter is invalid.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS BatteryGetBatteryPresence(BOOLEAN *BatteryPresence)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicSmbchgProtocol->IsBatteryPresent(PM_DEVICE_1, BatteryPresence);
    if (FALSE == *BatteryPresence)
    {
      CHARGER_DEBUG((EFI_D_ERROR, "Confirm Battery Presence...\n"));
      /*wait for 2 sec and read again*/
      gBS->Stall(TWO_SECONDS);
      Status |= PmicSmbchgProtocol->IsBatteryPresent(PM_DEVICE_1, BatteryPresence);
    }
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}

/**
  Wrapper for ADC Functions

  @param[in]  pszAdcInputName  ADC Channel name
  @param[out] pAdcData         Output ADC Data

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS
ChargerGetAdcData
(
  IN CONST CHAR8 *AdcInputName,
  OUT INT32 *AdcData
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  EFI_ADC_PROTOCOL        *AdcProtocol = NULL;
  EfiAdcDeviceChannelType Channel;
  EfiAdcResultType        AdcResult;

  Status = gBS->LocateProtocol(&gEfiAdcProtocolGuid,
                               NULL, (VOID **)&AdcProtocol);
  if (EFI_SUCCESS != Status)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "Failed to locate ADC Protocol!\n"));
    goto cleanup;
  }

  Status = AdcProtocol->GetChannel(AdcInputName, sizeof(AdcInputName), &Channel);

  if (EFI_SUCCESS != Status)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "Failed to get ADC Channel\n"));
    goto cleanup;
  }

  Status = AdcProtocol->AdcRead(&Channel, &AdcResult);

  if (EFI_SUCCESS != Status)
  {
    CHARGER_DEBUG((EFI_D_ERROR, "Failed to read ADC Channel\n"));
    goto cleanup;
  }

  *AdcData = AdcResult.nPhysical;

cleanup:
  return Status;
}

EFI_STATUS
ChargerGetFgErrors
(
  EFI_BATTERY_CHARGING_STATUS *ChargerErrors
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  BOOLEAN  bBattTemperatureValid = FALSE;
  if (!ChargerErrors)
  {
    return EFI_INVALID_PARAMETER;
  }

  /* Check Battery High Voltage Limit */
  Status = CheckBatteryVoltageHighLimit(&JeitaConfigData, ChargerErrors);
  if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
  {
    //Exit with Received ChagerErrors
    goto ErrorExit;
  }

  /* Check Battery Charge current Limit */
  Status = CheckBatteryChargeCurrent(&JeitaConfigData, ChargerErrors);
  if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
  {
    //Exit with received ChagerErrors
    goto ErrorExit;
  }

  if(NULL != PmicFgProtocol)
  {
    Status = PmicFgProtocol->IsTemperatureValid(&bBattTemperatureValid);
    if (EFI_SUCCESS == Status && TRUE == bBattTemperatureValid)
    {
      /* Check Battery Errors for Low Temperature conditions */
      Status = CheckBatteryTempErrors(&JeitaConfigData, ChargerErrors);
      if ((EfiBatteryChargingStatusNone != *ChargerErrors) || (EFI_SUCCESS != Status))
      {
        //Exit with received ChagerErrors
        goto ErrorExit;
      }
    }
    else
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Temperature Read InValid \n\r"));
    }
  }
  /* No errors detected */
  *ChargerErrors = EfiBatteryChargingStatusNone;

ErrorExit:
  return Status;
}


VOID
EFIAPI
ChargerLibEnableCallback(
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  (void)Event;
  (void)Context;
}

VOID
EFIAPI
ChargerLibDisableCallback(
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  (void)Event;
  (void)Context;
}

/**
ChargerLib_PrintDebugMsg()

@brief
Returns ChargerLib_PrintDebugMsg Flag Status
*/
inline BOOLEAN
EFIAPI
ChargerLib_PrintDebugMsg(void)
{
  return ((BOOLEAN)BattConfigDataFg.PrintChargerAppDbgMsg);
}

/**
ChargerLib_PrintDebugMsgToFile()

@brief
Returns ChargerLib_PrintDebugMsgToFile Flag Status
*/
inline BOOLEAN
EFIAPI
ChargerLib_PrintDebugMsgToFile(void)
{
  if (NULL != gULogHandle) return (BOOLEAN)BattConfigDataFg.PrintChargerAppDbgMsgToFile;
  return FALSE;
}

/**
ULogPrint ()

@brief Helper API to print multiple arguments
Returns
*/
inline void EFIAPI
ULogPrint(
  IN  UINTN        ErrorLevel,
  IN  CONST CHAR8  *Format,
  ...
  )
{
  va_list    vlist;
  UINT32 dataCount;
  UINTN i;
  BOOLEAN  boEsc = FALSE;
  for (i = 0, dataCount = 0; Format[i] != '\0'; i++)
  {
    if (Format[i] == '%' && !boEsc) dataCount++;
    else if (Format[i] == '\\' && !boEsc) boEsc = TRUE;
    else if (boEsc) boEsc = FALSE;
  }
  va_start(vlist, Format);
  ULogFront_RealTimeVprintf(gULogHandle,  dataCount, Format, vlist);
  va_end(vlist);
}


/**
ChargerLib_InitFileLog()

@brief  Init File Logging in ULog
Returns Status
*/
EFI_STATUS ChargerLib_InitFileLog(void *BattConfigData)
{
  //get BDS menu ULOG setting from BDS Menu
  ULogResult result = 0;

  result = ULogFile_Open(&gULogHandle, LOGFILE_IN_EFS, LOGFILE_SIZE);
  if (0 == result)
  {
    ULOG_RT_PRINTF_1(gULogHandle, "ChargerDxe: InitFileLog SUCCESS gULogHandle = (%d)", gULogHandle);
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: InitFileLog SUCCESS \n\r"));
  }
  else
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: InitFileLog FAILED \n\r"));
  }

  CHARGER_DEBUG((EFI_D_ERROR, "RebootCount,TimeStamp,StateOfCharge,RatedCapacity,Voltage,ChargeCurrent,Temp \r\n"));

  return EFI_SUCCESS;
}

/**
ChargerLogParams()

@brief  Logs Charger Parameters to File
Returns Status
*/
EFI_STATUS ChargerLogParams(UINT32 *StateOfCharge, UINT32 *RatedCapacity, INT32 *ChargeCurrent)
{
  EFI_STATUS Status = EFI_SUCCESS;
  INT32   BattVoltage = 0;
  INT32   BattTemp = 0;

  //Get Voltage and Temperature for logging
  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    if (NULL != PmicFgProtocol)
    {
      Status = PmicFgProtocol->GetBatteryVoltage(PM_DEVICE_1, (UINT32 *)&BattVoltage);
      Status = PmicFgProtocol->GetBatteryTemperature(&BattTemp);
    }
    else
    {
      CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerDxe: Error is Fg Protocol Handle Status = (%d)\r\n", Status));
    }
    break;
  default:
    CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerDxe: Error unable to find chargertype \r\n"));
    break;
  }
  CHARGER_FILE_DEBUG((EFI_D_ERROR, "%d,%d,%d,%d,%d \r\n",*StateOfCharge,*RatedCapacity, BattVoltage,*ChargeCurrent, BattTemp));
  DEBUG((EFI_D_ERROR, "%d,%d,%d,%d,%d \r\n",*StateOfCharge,*RatedCapacity, BattVoltage,*ChargeCurrent, BattTemp));

  return Status;
}

EFI_STATUS
ChargerCheckBatteryStatus
(
  OUT UINT32                       *StateOfCharge,
  OUT UINT32                       *RatedCapacity,
  OUT INT32                        *ChargeCurrent,
  OUT EFI_BATTERY_CHARGING_STATUS  *ChargerErrors
  )
{
  EFI_STATUS  Status        = EFI_SUCCESS;

  if ((!StateOfCharge) || (!RatedCapacity) || (!ChargeCurrent))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - NULL OUT parameters.\r\n"));
    Status = EFI_INVALID_PARAMETER;
    return  Status;
  }

  /* Get Battery State of Charge (%) */
  Status = BatteryGetSOC(StateOfCharge);
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: StateOfCharge (%d).\r\n",*StateOfCharge));
  if (EFI_ERROR(Status))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get SOC (%d).\r\n", Status));
    return  Status;
  }

  /* Get Battery Rated Capacity (mAh) */
  Status = BatteryGetRatedCapacity(RatedCapacity);
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: RatedCapacity (%d).\r\n",*RatedCapacity));
  if (EFI_ERROR(Status))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get Rated Capacity (d).\r\n", Status));
    return  Status;
  }

  /* Get Battery Current (mA) */
  Status = BatteryGetChargeCurrent(ChargeCurrent);
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerCheckBatteryStatus - ChargeCurrent (%d).\r\n",*ChargeCurrent));
  if (EFI_ERROR(Status))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get Charge Current (%d).\r\n", Status));
    return  Status;
  }

  Status = BatteryGetChargerErrors(ChargerErrors);
  if (EFI_ERROR(Status))
  {
    CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe (GetBatteryStatus) - Failed to get Charger Errors(%d).\r\n", Status));
    return  Status;
  }

  //Log Charger Params
  ChargerLogParams(StateOfCharge, RatedCapacity, ChargeCurrent);

  return  Status;
}


/**
  Gets Max charger current to be set based on charger port type

  @param[out] pMaxCurrent  Max current for charger in mA

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerGetMaxCurrent(UINT32 *pMaxCurrent)
{
  EFI_STATUS                    Status = EFI_SUCCESS;
  EFI_PM_SMBCHG_CHGR_PORT_TYPE  PortType;
  OUT EFI_PM_FG_CFGDATA_TYPE    BatteryCfgData;

  switch (ChargerInfo.ChargerHW)
  {
    case EfiBatteryChargerQcomPmicSmbChg:
    Status  = PmicSmbchgProtocol->ChargerPort(PM_DEVICE_1, &PortType);
    Status |= PmicFgProtocol->GetBatteryConfigData(&BatteryCfgData);
    switch (PortType)
    {
    case EFI_PM_SMBCHG_CHG_PORT_CDP:
      *pMaxCurrent = BatteryCfgData.CDPMaxChargeCurrent;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_DCP:
      *pMaxCurrent = BatteryCfgData.DCPMaxChargeCurrent;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_SDP:
      *pMaxCurrent = BatteryCfgData.SDPMaxChargeCurrent;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_OTHER:
    default:
      *pMaxCurrent = BatteryCfgData.OtherChargerChargeCurrent;
      CHARGER_DEBUG((EFI_D_WARN, "Other or Unknown charge port type detected \r\n"));
      break;
    }
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}


EFI_STATUS ChargerLibWaitToCoolOff(void)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /*Disable Charging */
  Status |= ChargerEnable(FALSE);

  DEBUG((EFI_D_ERROR, "ChargerDxe: Temperature Cool Off. Charging may restart after (%d)milli seconds.\r\n", JeitaConfigData.ChargerCoolOffPeriodMs));
  CHARGER_FILE_DEBUG((EFI_D_ERROR, "ChargerDxe: Temperature Cool Off. Charging may restart after (%d)milli seconds.\r\n", JeitaConfigData.ChargerCoolOffPeriodMs));

  gBS->Stall(JeitaConfigData.ChargerCoolOffPeriodMs * 1000); /*convert milli in micro seconds*/

  return Status;
}



EFI_STATUS ChargerLibEnableAfpMode(void)
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicSmbchgProtocol->EnableAfpMode(PM_DEVICE_1);
    break;
  default:
    Status = EFI_UNSUPPORTED;
    break;
  }

  return Status;
}


EFI_STATUS ChargerLibGetBatteryType(ChgBattType *BatteryType)
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_PM_FG_BATT_TYPE FgBatteryType = EFI_PM_FG_BATT_TYPE_INVALID;

  if (NULL == BatteryType)
  {
    return EFI_DEVICE_ERROR;
  }

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicFgProtocol->GetBatteryType(&FgBatteryType);
    *BatteryType = (ChgBattType)FgBatteryType;
    break;
  default:
    *BatteryType = CHG_BATT_TYPE_UNKNOWN;
    break;
  }

  return Status;
}


EFI_STATUS ChargerLibUsbSuspend(BOOLEAN Enable)
{
  EFI_STATUS Status = EFI_SUCCESS; 
  CHARGER_DEBUG((EFI_D_ERROR, "ChargerLib UsbSuspend : (%d)\n", Enable));

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    Status = PmicSmbchgProtocol->UsbSuspend(PM_DEVICE_1, Enable);
    break;
  default:
    /*Not supported on SMB1360*/
    break;
  }

  return Status;
}


EFI_STATUS ChargerLibHandleDebugBoard(INT32 BatteryVolt, ChargerLibHandleType *ChargerLibHandle)
{
  /*temperature verification already done*/
  /* Battery Voltage */
  INT32 BootThreshold = BOOT_THRESHOLD_VOLT;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    BootThreshold = BattConfigDataFg.BootToHLOSThresholdMv;
    break;
  default:
    BootThreshold = BOOT_THRESHOLD_VOLT;
    break;
  }
  /*ignore return type to allow continued booting*/
  ChargerLibUsbSuspend(TRUE);

  *ChargerLibHandle = (BatteryVolt < BootThreshold) ? CHARGERLIB_HANDLE_SHUTDOWN : CHARGERLIB_HANDLE_CONTINUE_BOOT;

  return EFI_SUCCESS;
}



EFI_STATUS ChargerLibConservativeCharging(ChargerLibHandleType *ChargerLibHandle)
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_BATTERY_CHARGING_STATUS ChargerError = EfiBatteryChargingStatusNone;

  switch (ChargerInfo.ChargerHW)
  {
  case EfiBatteryChargerQcomPmicSmbChg:
    {
      /*set conservative parameter*/
      JeitaConfigData.ChgFvMaxInJeitaT4Limit  = BattConfigDataFg.JeitaCfgData.ConservChgFvMaxInJeitaT4Limit;
      JeitaConfigData.ChgFvMaxInJeitaT5Limit  = BattConfigDataFg.JeitaCfgData.ConservChgFvMaxInJeitaT5Limit;
      JeitaConfigData.ChgFccMaxInJeitaT2Limit = BattConfigDataFg.JeitaCfgData.ConservChgFccMaxInJeitaT2Limit;
      JeitaConfigData.ChgFccMax               = BattConfigDataFg.ConservChgFvMax;
      BatteryJeitaTInfo.IsFccFvMaxAdjustedT1T2  = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT2T3  = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT3T4  = FALSE;
      BatteryJeitaTInfo.IsFvFccMaxAdjustedT4T5  = FALSE;
      BatteryJeitaTInfo.IsFvMaxAdjustedT5       = FALSE;
      ChargerSetMaxBatCurrent(BattConfigDataFg.OtherChargerChargeCurrent);
    }
    break;
  default:
    break;
  }

  Status = CheckBatteryTempErrors(&JeitaConfigData, &ChargerError);

  if ((EFI_SUCCESS == Status) && (ChargerError == EfiBatteryChargingStatusExtremeCold || ChargerError == EfiBatteryChargingStatusOverheat))
  {
    *ChargerLibHandle = CHARGERLIB_HANDLE_SHUTDOWN;
  }
  else
  {
    *ChargerLibHandle = CHARGERLIB_HANDLE_CHARGE;
  }

  return Status;
}


EFI_STATUS ChargerLibHandleUnknownBattery(INT32 BatteryVolt, ChargerLibHandleType *ChargerLibHandle)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 UnknownBattBehaviour = UNKNOWN_BATT_BOOT_TO_HLOS;

  /*CHI platform requirement to charge conservatively upon detecting unknown battery*/
 /*CHI platform not present currently and needs to be added*/
  //if (PlatformType == EFI_PLATFORMINFO_TYPE_CHI)
  //{
  //  UnknownBattBehaviour = UNKNOWN_BATT_CONSERVATIVE_CHARGING;
  //}
  //else
  {
    switch (ChargerInfo.ChargerHW)
    {
    case EfiBatteryChargerQcomPmicSmbChg:
      UnknownBattBehaviour = BattConfigDataFg.UnknownBattBehavior;
      break;
    default:
      UnknownBattBehaviour = UNKNOWN_BATT_BOOT_TO_HLOS;
      break;
    }

  }

  switch (UnknownBattBehaviour)
  {
  case UNKNOWN_BATT_SHUTDOWN:
    *ChargerLibHandle = CHARGERLIB_HANDLE_SHUTDOWN;
    break;
  case UNKNOWN_BATT_BOOT_TO_HLOS:
    /*same set of steps need to be followed as for debug board*/
    ChargerLibHandleDebugBoard(BatteryVolt, ChargerLibHandle);
    break;
  case UNKNOWN_BATT_CONSERVATIVE_CHARGING:
    ChargerLibConservativeCharging(ChargerLibHandle);
    break;
  case UNKNOWN_BATT_REGULAR_CHARGING:
    *ChargerLibHandle = CHARGERLIB_HANDLE_CHARGE;
    break;
  default:
    ChargerLibHandleDebugBoard(BatteryVolt, ChargerLibHandle);
    break;
  }

  return Status;
}


/*This API initiates different types of reset */
EFI_STATUS ChargerLibForceSysShutdown(ChgAppSysShutdownType ShutdownType)
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_RESET_TYPE   ResetType = EfiResetPlatformSpecific;

  switch (ShutdownType)
  {
  case CHGAPP_RESET_SHUTDOWN:
    DEBUG((EFI_D_ERROR, "ChargerApp - CHGAPP_RESET_SHUTDOWN.\r\n"));
    ResetType = EfiResetShutdown;
    break;
  case CHGAPP_RESET_AFP:
    DEBUG((EFI_D_ERROR, "ChargerApp - CHGAPP_RESET_AFP.\r\n"));
    if (EFI_SUCCESS != ChargerLibEnableAfpMode())
    {
      /*if AFP not supported then disable USB as PON trigger and issue shut down */
      if (NULL != PmicPwronProtocol)
      {
        Status |= PmicPwronProtocol->SetPonTrigger(PM_DEVICE_0, EFI_PM_PON_TRIGGER_DC_CHG, FALSE);
        Status |= PmicPwronProtocol->SetPonTrigger(PM_DEVICE_0, EFI_PM_PON_TRIGGER_USB_CHG, FALSE);
      }
      ResetType = EfiResetShutdown;
    }
    break;
  case CHGAPP_RESET_COLD:
    DEBUG((EFI_D_ERROR, "ChargerApp - CHGAPP_RESET_COLD.\r\n"));
    ResetType = EfiResetCold;
    break;

  case CHGAPP_RESET_NONE:
  case CHGAPP_RESET_INVALID:
  default:
    /*do nothing*/
    return Status;
    //break;
  }

  /*wait for 5seconds to display the message*/
  gBS->Stall(5000000);

  /* Clean up */
  if(EfiBatteryGaugeQcomPmicFg == BatteryGaugeInfo.BatteryGaugeHW){
    if(NULL != PmicFgProtocol){
      PmicFgProtocol->FgExit(PM_DEVICE_1); /* Please do Other Target Check and call Exit if Needed */
    }
  }

  /* Reset  Device*/
  gRT->ResetSystem(ResetType, EFI_SUCCESS, 0, NULL);

  return Status;
}



/*battery error handling*/
EFI_STATUS ChargerLibBatteryErrorHandler(ChargerLibHandleType *ChargerLibHandle)
{
  BOOLEAN BatteryPresent = TRUE;
  EFI_STATUS Status = EFI_SUCCESS;
  INT32 BatteryVolt = 0;
  EFI_BATTERY_CHARGING_STATUS ChargerError = EfiBatteryChargingStatusNone;
  ChgBattType BattType = CHG_BATT_TYPE_UNKNOWN;
  INT32      BattTemp = 0;

  /*clear USB suspend if in case it happened*/
  ChargerLibUsbSuspend(FALSE);

  /*if CDP platform then continue to boot irrespective of anything else */
  if (EFI_PLATFORMINFO_TYPE_CDP == PlatformType)
  {
    *ChargerLibHandle = CHARGERLIB_HANDLE_CONTINUE_BOOT;
    return Status;
  }

  //get battery present, if not present then read voltage and is debug board
  Status = BatteryGetBattVoltage(&BatteryVolt, &ChargerError);

  Status |= BatteryGetBatteryPresence(&BatteryPresent);

  if ((EFI_SUCCESS == Status) && (TRUE == BatteryPresent))
  {
    /*check battery temperature if battery present*/
    Status |= ChargerLibGetBattTemp(&BattTemp, &ChargerError);
    /* Check for Low Temperature Limit */
    if ((EFI_SUCCESS == Status) && (BattTemp < JeitaConfigData.BattTempLimLow))
    {
      /*Read battery temperature again to confirm error was not spurious this is only required for low temperature */
      /* Get battery temperature */
      Status |= ChargerLibGetBattTemp(&BattTemp, &ChargerError);
      if ((EFI_SUCCESS == Status) && (BattTemp < JeitaConfigData.BattTempLimLow))
      {
        ChargerError = EfiBatteryChargingStatusExtremeCold;
        CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Range Temperature = (%d) than config limit = (%d) \n\r", BattTemp, JeitaConfigData.BattTempLimLow));
      }
    }
    /* Check for High Temperature */
    if ((EFI_SUCCESS == Status) && (BattTemp >= JeitaConfigData.BattTempLimHigh))
    {
      CHARGER_DEBUG((EFI_D_ERROR, "ChargerDxe: Out of Range Temperature = (%d) than config limit = (%d) \n\r", BattTemp, JeitaConfigData.BattTempLimHigh));
      ChargerError = EfiBatteryChargingStatusOverheat;
    }
    /* Check temperature Errors */
    if (ChargerError == EfiBatteryChargingStatusExtremeCold || ChargerError == EfiBatteryChargingStatusOverheat)
    {
      *ChargerLibHandle = CHARGERLIB_HANDLE_SHUTDOWN;
      return Status;
    }

    Status = ChargerLibGetBatteryType(&BattType);
    DEBUG((EFI_D_ERROR, "ChargerDxe: ChargerLib_BatteryErrorHandler: BatteryType: (%d).\r\n", BattType));
    switch (BattType)
    {
      case CHG_BATT_TYPE_NORMAL:
      case CHG_BATT_TYPE_SMART:
        *ChargerLibHandle = CHARGERLIB_HANDLE_CHARGE;
        break;
      case CHG_BATT_TYPE_UNKNOWN:
      case CHG_BATT_TYPE_BATT_EMULATOR:
        ChargerLibHandleUnknownBattery(BatteryVolt, ChargerLibHandle);
        break;
      case CHG_BATT_TYPE_DEBUG_BOARD:
        ChargerLibHandleDebugBoard(BatteryVolt, ChargerLibHandle);
        break;
      default:
        /*treat default as debug board*/
        ChargerLibHandleDebugBoard(BatteryVolt, ChargerLibHandle);
        break;
    }
  }
  else
  {
    /*device powered up then debug board connected*/
    ChargerLibHandleDebugBoard(BatteryVolt, ChargerLibHandle);
  }

  return Status;
}


EFI_STATUS ChargerGetOnetimeStickyChargingStatus(BOOLEAN* Enabled)
{
    EFI_STATUS  Status = EFI_SUCCESS;
	EFI_VARIABLESERVICES_PROTOCOL *VariableServicesProtocol = NULL;
    BOOLEAN     EnableStickyChg = 0;
    UINTN       DataSize;
    UINT8       TriggerType;
    EFI_QCOM_PMIC_PWRON_PROTOCOL  *pPmicPwron = NULL;
    BOOLEAN     ColdBoot = TRUE;

    if (!PmicVersionProtocol)
    {
        /* Locate Version Protocol */
        Status |= gBS->LocateProtocol(&gQcomPmicVersionProtocolGuid, NULL, (VOID **)&PmicVersionProtocol);
        /* Get PMIC Model */
        Status |= PmicVersionProtocol->GetPmicInfo(PM_DEVICE_0, &deviceInfo);
    }
    if((Status != EFI_SUCCESS)||(EFI_PMIC_IS_PM8909 != deviceInfo.PmicModel))
    {
        *Enabled = FALSE;
        return Status;
    }

    /* Locate PON Protocol */
    Status = gBS->LocateProtocol( &gQcomPmicPwrOnProtocolGuid,
                                NULL,
                                (VOID**) &pPmicPwron );
    if (Status != EFI_SUCCESS)
    {
        return Status;
    }

    Status = pPmicPwron->GetPowerOnTrigger(0, &TriggerType);
    if (EFI_SUCCESS == Status)
    {
        DEBUG(( EFI_D_ERROR, "PmicDxe: GetPowerOnTrigger %d:  \n\r", TriggerType));
        if (TriggerType&0x01) //If it is Hard Reset, treat it as cold boot
        {
              ColdBoot = FALSE;
        }
    }
    DEBUG((EFI_D_WARN, "Cold boot status %d", ColdBoot));
    //Get onetime sticky charging variable
    DataSize = sizeof(EnableStickyChg);
    Status = gRT->GetVariable (STICKYCHARGING_RUNTIME_VARIABLE_NAME,
                               &gStickyGuid,
                               NULL,
                               &DataSize,
                               &EnableStickyChg);
    /*assign only if variable exist in fv*/
    EnableStickyChg = (Status == EFI_SUCCESS) ? EnableStickyChg : 0;
    DEBUG((EFI_D_WARN, "One time sticky charging status %d \r\n", EnableStickyChg));

    if( !ColdBoot && EnableStickyChg)
    {
        *Enabled = TRUE;
        //Clear FV variable
        EnableStickyChg = FALSE;
        Status = gRT->SetVariable (STICKYCHARGING_RUNTIME_VARIABLE_NAME,
                               &gStickyGuid,
                               EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                               DataSize,
                               &EnableStickyChg);
        if(Status != EFI_SUCCESS)
        {
            DEBUG((EFI_D_WARN, "Clearing one time Sticky variable failed \r\n"));
        }
		gBS->LocateProtocol(&gEfiVariableServicesProtocolGuid, NULL, (VOID **)&VariableServicesProtocol);

		if (VariableServicesProtocol != NULL)
		{
			VariableServicesProtocol->FlushVariableNV(VariableServicesProtocol);
		}
    }
    else
    {
        *Enabled = FALSE;
    }
    return Status;
}

/**
  Pets Charger watchdog

  @return
  EFI_SUCCESS:           Function returned successfully.
  EFI_DEVICE_ERROR:      The physical device reported an error.
  EFI_UNSUPPORTED:       No Library function linked
*/
EFI_STATUS ChargerLibPetChgWdog( void )
{
  EFI_STATUS Status = EFI_SUCCESS;

  switch (ChargerInfo.ChargerHW)
  {
    case EfiBatteryChargerQcomPmicSmbChg:
      Status = PmicSmbchgProtocol->PetChgWdog(PM_DEVICE_1);
      break;

    default:
      Status = EFI_UNSUPPORTED;
      break;
  }

  return Status;
}

EFI_STATUS ChargerLibAlarmExpireStatus(BOOLEAN *ExpireStatus)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(NULL==ExpireStatus)
  {
    return EFI_DEVICE_ERROR;
  }

  /*if not enabled then return FALSE status*/
  if (RtcAlarmEnabled==FALSE)
  {
    *ExpireStatus = FALSE;
     return Status;
  }

  if (NULL == PmicRtcProtocol)
  {
    Status = gBS->LocateProtocol(&gQcomPmicRtcProtocolGuid, 
                                 NULL,
                                 (VOID **)&PmicRtcProtocol);
  }

  if (EFI_SUCCESS == Status && NULL!= PmicRtcProtocol)
  {

    Status = PmicRtcProtocol->AlarmExpireStatus(RtcDeviceIndex, ExpireStatus);
  }
  
  return Status;
}


EFI_STATUS ChargerLibAlarmEnableStatus(BOOLEAN *EnableStatus)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(NULL==EnableStatus)
  {
    return EFI_DEVICE_ERROR;
  }

  if (NULL == PmicRtcProtocol)
  {
    Status = gBS->LocateProtocol(&gQcomPmicRtcProtocolGuid,
                                 NULL,
                                 (VOID **)&PmicRtcProtocol);
  }

  if (EFI_SUCCESS == Status && NULL!= PmicRtcProtocol)
  {

    Status |= PmicRtcProtocol->AlarmEnableStatus(RtcDeviceIndex, EnableStatus);
  }

  return Status;
}

