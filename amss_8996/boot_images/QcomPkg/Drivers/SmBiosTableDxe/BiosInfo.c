/** @file
  Implements the BIOS Info Table for SMBios. 
   
  Copyright (c) 2010-2013, 2015, Qualcomm Technologies Inc. All rights reserved.
  
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 01/15/15   bh      Change UINT32 to UINTN
 01/30/12   vk      Fixed KW warning
 07/23/12   jz      Fixed version display when build version number is supplied
 07/09/12   yg      Fixed warning
 04/10/12   jz      Fixed release date format and klockwork warnings
 03/23/12   kpa     Add support for config file based smbios table init
 01/27/12   yg      Added Another char for year in version
 12/20/11   niting  Added build version string to SmBios
 09/13/11   jz      Added support for gST->FirmwareRevision (UINT32)
 09/01/11   jz      Fixed the handling of date
 08/31/11   jz      Update version with date
 07/28/11   yg      Added null termination
 07/15/11   yg      Added date information
 05/25/11   niting  Initial revision

=============================================================================*/

#include <Uefi.h>

#include <Protocol/Smbios.h>

#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/PcdLib.h>
#include <Library/BaseLib.h>
#include <Library/QcomLib.h>

#include "BuildVersion.h"
#include <SmbiosCfgInfo.h>

#define MAX_VERSION_STR_LENGTH 128

#define MAX_RELEASE_DATE_STR_LENGTH 11

#define CHAR_TO_INT(c1, c2, c3, c4) \
  (c1 | c2 << 8 | c3 << 16 | c4 << 24)

#define JAN CHAR_TO_INT('J', 'a', 'n', ' ')
#define FEB CHAR_TO_INT('F', 'e', 'b', ' ')
#define MAR CHAR_TO_INT('M', 'a', 'r', ' ')
#define APR CHAR_TO_INT('A', 'p', 'r', ' ')
#define MAY CHAR_TO_INT('M', 'a', 'y', ' ')
#define JUN CHAR_TO_INT('J', 'u', 'n', ' ')
#define JUL CHAR_TO_INT('J', 'u', 'l', ' ')
#define AUG CHAR_TO_INT('A', 'u', 'g', ' ')
#define SEP CHAR_TO_INT('S', 'e', 'p', ' ')
#define OCT CHAR_TO_INT('O', 'c', 't', ' ')
#define NOV CHAR_TO_INT('N', 'o', 'v', ' ')
#define DEC CHAR_TO_INT('D', 'e', 'c', ' ')

#pragma pack(1)
typedef struct _SMBIOS_BIOS_INFO
{
  EFI_SMBIOS_TABLE_HEADER  Header;
  UINT8                    VendorStringNum;
  UINT8                    BiosStringNum;
  UINT16                   BiosAddress;
  UINT8                    BiosReleaseDateNum;
  UINT8                    BiosRomSize;
  UINT64                   BiosCharacteristics;
  UINT8                    BiosCharacteristicsExtension1;
  UINT8                    BiosCharacteristicsExtension2;
  UINT8                    SystemBiosMajorRevision;
  UINT8                    SystemBiosMinorRevision;
  UINT8                    EmbeddedControllerMajorRevision;
  UINT8                    EmbeddedControllerMinorRevision;

  UINT8                    String1[2];
  UINT8                    String2[2];
  UINT8                    String3[2];
  UINT8                    EndNull;
}SMBIOS_BIOS_INFO;

typedef struct _BIOS_STRING_STRUCTURE{
  UINT8                    String1[2];
  UINT8                    String2[2];
  UINT8                    String3[2];
  UINT8                    EndNull;
}BIOS_STRING_STRUCTURE;


//Bios information
SMBIOS_BIOS_INFO SmBiosBootInfo = 
{
  {
    EFI_SMBIOS_TYPE_BIOS_INFORMATION,
    sizeof(SMBIOS_BIOS_INFO) - sizeof(BIOS_STRING_STRUCTURE),
    0         //handle
  },
  1,          // VendorStringNum
  2,          // BiosStringNum
  0x00000,    // Bios Address Segment (doesn't apply to us)
  3,          // BiosReleaseDateNum
  0x00,       // BiosRomSize (doesn't apply to us)
  0,          // BiosCharacteristics
  1,       	  // Extension1 - bit 0 - supports ACPI
  0x06,       // Extension2 - bit 3 UEFI, bit 2, this is valid 
  0xFF,       // SystemBiosMajorRevision;
  0xFF,       // SystemBiosMinorRevision;
  0xFF,       // EmbeddedControllerMajorRevision;
  0xFF,       // EmbeddedControllerMinorRevision;

  "1",
  "2",
  "3",
  0
};


#pragma pack()

UINTN  BiosInfoVendorNameStringNum = 1;
UINTN  BiosInfoVersionStringNum = 2;
UINTN  BiosInfoReleaseDateStringNum = 3;

CHAR8 *BuildVersionNum = UEFI_BUILD_VERSION;


/**
  Add Bios Info /Type 0 table to Smbios table List

  @param  SmbiosTableNode   Pointer to the Smbios Table list 
                            node

  @retval EFI_SUCCESS    Initialization success

**/
EFI_STATUS BiosInfoTableInit( SMBIOS_TABLE_NODE     *SmbiosTableNode)
{
  EFI_STATUS       Status = EFI_INVALID_PARAMETER ;
  
  if( SmbiosTableNode == NULL)
    return Status;

  // Add type 0 /Bios info table.
  SmbiosTableNode->TablePtr= (VOID *) &SmBiosBootInfo ; 
  SmbiosTableNode->TableType = EFI_SMBIOS_TYPE_BIOS_INFORMATION; 
  Status = EFI_SUCCESS;

  return Status;
}

EFI_STATUS UpdateBiosInfoTable( EFI_SMBIOS_PROTOCOL     *Smbios)
{
  EFI_STATUS              Status;
  EFI_SMBIOS_HANDLE       BiosHandle = 0;  

  CHAR8 AsciiString[MAX_VERSION_STR_LENGTH];
  CHAR8 *DateString = __DATE__; /* Format: Jan 11 2011 */
  UINTN i = 0;
  UINTN index = 0;
  UINTN CharsToCopy = 0;
  CHAR16 *UnicodeStringPCD;
  UINTN Version;
  CHAR8  RelDateString[MAX_RELEASE_DATE_STR_LENGTH];  /* format: mm/dd/yyyy */
	  
  Status = Smbios->Add(Smbios, NULL, &BiosHandle, (EFI_SMBIOS_TABLE_HEADER*)&SmBiosBootInfo);
  if(Status != EFI_SUCCESS)
  {
    return Status;
  }

  // Store table handle and Update strings if any.
  UpdateTableData(Smbios, BiosHandle, EFI_SMBIOS_TYPE_BIOS_INFORMATION );

  //convert vendor name to ASCII
  UnicodeStringPCD = FixedPcdGetPtr (PcdFirmwareVendor);
  while((*UnicodeStringPCD != 0) && (i < (MAX_VERSION_STR_LENGTH - 1)))
  {
    AsciiString[i] = (CHAR8)*UnicodeStringPCD;
    UnicodeStringPCD++;
    i++;  
  }
  AsciiString[i] = 0;
  
  Status = Smbios->UpdateString(Smbios,  
                                &BiosHandle,  
                                &BiosInfoVendorNameStringNum, 
                                AsciiString);

  if(Status != EFI_SUCCESS)
  {
    return Status;
  }

  // convert version string to ASCII
  i = 0; //reset the counter for copy
  UnicodeStringPCD = FixedPcdGetPtr (PcdFirmwareVersionString);
  while((*UnicodeStringPCD != 0) && (i < (MAX_VERSION_STR_LENGTH - 1)))
  {
    AsciiString[i] = (CHAR8)*UnicodeStringPCD;
    UnicodeStringPCD++;
    i++;  
  }

  if ( (i + MAX_RELEASE_DATE_STR_LENGTH ) >  (MAX_VERSION_STR_LENGTH - 1)) /* For KW */
    return EFI_BUFFER_TOO_SMALL;

  // gST->FirmwareRevision: UINT32, e.g. 0x12345678
  // PcdFirmwareVersionString is in the format of x.xx, e.g., 1.20
  // Start with the PcdFirmwareVersionString (without the .), i.e., 120
  Version = (AsciiString[0] - '0') << (7 * 4);
  Version |= (AsciiString[2] - '0') << (6 * 4);
  
  AsciiString[i++] = '.';
  
  // append year to make the version string
  AsciiString[i++] = DateString[9];
  AsciiString[i++] = DateString[10];

  // append the last two char of year to Version
  Version |= (AsciiString[i-2] - '0') << (5 * 4);
  Version |= (AsciiString[i-1] - '0') << (4 * 4);

  // append month to make the version string
  switch (CHAR_TO_INT(DateString[0], DateString[1], DateString[2], DateString[3]))
  {
  case JAN:
    AsciiString[i++] = '0'; AsciiString[i++] = '1'; break;
  case FEB:
    AsciiString[i++] = '0'; AsciiString[i++] = '2'; break;
  case MAR:
    AsciiString[i++] = '0'; AsciiString[i++] = '3'; break;
  case APR:
    AsciiString[i++] = '0'; AsciiString[i++] = '4'; break;
  case MAY:
    AsciiString[i++] = '0'; AsciiString[i++] = '5'; break;
  case JUN:
    AsciiString[i++] = '0'; AsciiString[i++] = '6'; break;
  case JUL:
    AsciiString[i++] = '0'; AsciiString[i++] = '7'; break;
  case AUG:
    AsciiString[i++] = '0'; AsciiString[i++] = '8'; break;
  case SEP:
    AsciiString[i++] = '0'; AsciiString[i++] = '9'; break;
  case OCT:
    AsciiString[i++] = '1'; AsciiString[i++] = '0'; break;
  case NOV:
    AsciiString[i++] = '1'; AsciiString[i++] = '1'; break;
  case DEC:
    AsciiString[i++] = '1'; AsciiString[i++] = '2'; break;
  }

  // fill in the RelDateString with month
  RelDateString[index++] = AsciiString[i-2];
  RelDateString[index++] = AsciiString[i-1];
  RelDateString[index++] = '/';
  // fill in the RelDateString with date
  RelDateString[index++] = DateString[4];
  RelDateString[index++] = DateString[5];
  RelDateString[index++] = '/';
  // fill in the RelDateString with year
  RelDateString[index++] = DateString[7];
  RelDateString[index++] = DateString[8];
  RelDateString[index++] = DateString[9];
  RelDateString[index++] = DateString[10];
  RelDateString[index] = 0;

  // append month to Version
  Version |= (AsciiString[i-2] - '0') << (3 * 4);
  Version |= (AsciiString[i-1] - '0') << (2 * 4);
  
  // append date to make the version string
  if (DateString[4] == ' ')
  {
    AsciiString[i++] = '0';
  }
  else
  {
    AsciiString[i++] = DateString[4];
  }
  AsciiString[i++] = DateString[5];

  // Append date to Version
  Version |= (AsciiString[i-2] - '0') << (1 * 4);
  Version |= (AsciiString[i-1] - '0');
  
  AsciiString[i++] = '.';

  // Only append Build Version number if not set to 0
  if (AsciiStrCmp(BuildVersionNum, "0") != 0)
  {
    CharsToCopy = AsciiStrLen(BuildVersionNum);
    // Check for room to add build version plus NULL terminator
    if(i + CharsToCopy + 1 > MAX_VERSION_STR_LENGTH)
    {
      /* Set length of characters to copy to string */
      CharsToCopy = MAX_VERSION_STR_LENGTH - i - 1;
    }
    
    // Add Build Version number
    for(index = 0; index < CharsToCopy; index++)
    {
      AsciiString[i++] = BuildVersionNum[index];
    }
  }

  // Null terminate the AsciiString
  AsciiString[i] = 0;
  
  gRT->SetVariable(L"FwVerStr", &gQcomTokenSpaceGuid,
    EFI_VARIABLE_BOOTSERVICE_ACCESS, (i+1), AsciiString); 
  
  gRT->SetVariable(L"FwVerHex", &gQcomTokenSpaceGuid,
    EFI_VARIABLE_BOOTSERVICE_ACCESS, sizeof(Version), &Version); 

  Status = Smbios->UpdateString(Smbios,  
                                &BiosHandle,  
                                &BiosInfoVersionStringNum, 
                                AsciiString);
 
  if(Status != EFI_SUCCESS)
  {
    return Status;
  }
  
  // Format of date string should be "mm/dd/yyyy"
  return Smbios->UpdateString(Smbios,  
                                &BiosHandle,  
                                &BiosInfoReleaseDateStringNum, 
                                RelDateString);
}
