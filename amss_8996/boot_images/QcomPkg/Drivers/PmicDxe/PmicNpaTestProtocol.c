/*! @file PmicNpaTestProtocol.c 

*  PMIC- NPATEST MODULE RELATED DECLARATION
*  This file contains functions and variable declarations to support 
*  the PMIC NPATEST module.to enable APT tests to call in NPA API calls for executing NPA Tests
*
*  Copyright (c) 2012 - 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/28/14   va     New file.(Expose Npa Test protocol)
===========================================================================*/

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>

#include "pm_uefi.h"

#include <Protocol/EFIPmicNpaTest.h>
#include <Library/PmicLib/npa/test/pm_npa_test.h>


/*===========================================================================
EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/

/**
EFI_PmicNpaTestInit ()

@brief
Initializes NPATEST
*/
EFI_STATUS
  EFI_PmicNpaTestInit(void)
{
  EFI_STATUS Status = EFI_SUCCESS;
  return Status;
}



/**
EFI_PmicNpaTestInit ()

@brief
Initializes NPATEST
*/
EFI_STATUS
EFIAPI
  EFI_CreateTestClients()
{
  EFI_STATUS Status = EFI_SUCCESS;

    /* Call to create test clinets */
    pm_npa_test_init();
    return Status;
}


/**
EFI_GetResourceInfo ()

@brief
Get Npa Resource Info
*/
EFI_STATUS
EFIAPI
  EFI_GetResourceInfo(PMIC_NPATEST_RESOURCEINFO *PamResourceInfo)
{
  EFI_STATUS Status = EFI_SUCCESS;

  /* Call to get PAM resource Info */
  Status = pm_npa_get_resource_info(PamResourceInfo);

  return Status;
}

/**
PMIC NPATEST UEFI Protocol implementation
*/
EFI_QCOM_PMIC_NPATEST_PROTOCOL PmicNpaTestProtocolImplementation = 
{
  PMIC_NPATEST_REVISION,
  EFI_PmicNpaTestInit,
  EFI_CreateTestClients,
  EFI_GetResourceInfo
};

