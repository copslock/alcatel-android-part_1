/*! @file PmicUsbProtocol.c 

*  PMIC- SMBCHG MODULE RELATED DECLARATION
*  This file contains functions and variable declarations to support 
*  the PMIC SMBCHG (Switch Mode Battery Charger and Boost) module.
*
*  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Inc Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/14/15   al      added support for PMI8950 
02/11/15   sv      added support for 8909 and 8916
01/30/15   al      New file.
===========================================================================*/

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>

#include <Library/DebugLib.h>

#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/EFIPmicSmbchg.h>
#include <Protocol/EFIPmicUsb.h>
#include "pm_version.h"

static EFI_QCOM_PMIC_SMBCHG_PROTOCOL        *PmicSmbchgProtocol  = NULL;
static pm_model_type PmicIs = PMIC_IS_INVALID;

/*===========================================================================
EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/


EFI_STATUS EFIPmicUsbInit(IN  UINT32 PmicDeviceIndex)
{
    EFI_STATUS  Status = EFI_SUCCESS;

    PmicIs = pm_get_pmic_model(PmicDeviceIndex);
    
    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        if(NULL == PmicSmbchgProtocol)
        {
            Status = gBS->LocateProtocol(&gQcomPmicSmbchgProtocolGuid, NULL, (VOID **)&PmicSmbchgProtocol);
        }
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicUsbUsbinValid
(
  IN  UINT32 PmicDeviceIndex,
  OUT BOOLEAN *Valid
)
{   
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }

    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->UsbinValid(PmicDeviceIndex,Valid);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}



/**
EFI_PmicChargerPortType ()

@brief
Gets charger port type
*/
EFI_STATUS
EFIAPI
EFI_PmicUsbChargerPortType
(
  IN  UINT32 PmicDeviceIndex,
  OUT EFI_PM_USB_CHGR_PORT_TYPE *PortType
)
{
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }
       
    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->ChargerPort(PmicDeviceIndex,(EFI_PM_SMBCHG_CHGR_PORT_TYPE*)PortType);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}


/**
@brief
Gets HVDCP status
*/
EFI_STATUS
EFIAPI
EFI_PmicUsbGetHvdcp
(
  IN  UINT32                     PmicDeviceIndex,
  IN  EFI_PM_USB_HVDCP_STS_TYPE  HvdcpSts,
  OUT BOOLEAN                    *Enable
)
{   
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }

    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->GetHvdcp(PmicDeviceIndex,(EFI_PM_HVDCP_STS_TYPE)HvdcpSts,Enable);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}



EFI_STATUS
EFIAPI
EFI_PmicUsbSetOtgILimit
(
  IN  UINT32   PmicDeviceIndex,
  IN  UINT32   ImAmp
)
{   
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }

    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->SetOtgILimit(PmicDeviceIndex,ImAmp);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}


EFI_STATUS
EFIAPI 
EFI_PmicUsbEnableOtg
(
  IN  UINT32   PmicDeviceIndex,
  IN  BOOLEAN  Enable
)
{   
    EFI_STATUS  Status = EFI_SUCCESS;
	
    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }
	
    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->EnableOtg(PmicDeviceIndex,Enable);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}


EFI_STATUS
EFIAPI
EFI_PmicUsbOtgStatus
(
   IN  UINT32   PmicDeviceIndex,
   OUT BOOLEAN  *Ok
   )
{   
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }

    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->OtgSts(PmicDeviceIndex,Ok);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}




EFI_STATUS
EFIAPI 
EFI_PmicUsbGetRidStatus
(
  IN   UINT32                           PmicDeviceIndex,
  OUT  EFI_PM_USB_USB_RID_STS_TYPE      *RidStsType
)
{   
    EFI_STATUS  Status = EFI_SUCCESS;

    if(EFI_SUCCESS != EFIPmicUsbInit(PmicDeviceIndex))
    {
        return EFI_UNSUPPORTED;
    }

    switch(PmicIs)
    {
    case PMIC_IS_PMI8994:
    case PMIC_IS_PMI8950:
        Status = PmicSmbchgProtocol->GetRidStatus(PmicDeviceIndex,(EFI_PM_SMBCHG_USB_RID_STS_TYPE*)RidStsType);
        break;
    default:
        Status = EFI_UNSUPPORTED;
        break;
    }

    return Status;
}


/**
PMIC USB UEFI Protocol implementation
*/
EFI_QCOM_PMIC_USB_PROTOCOL PmicUsbProtocolImplementation = 
{
    PMIC_USB_REVISION,
    EFI_PmicUsbUsbinValid,  
    EFI_PmicUsbChargerPortType, 
    EFI_PmicUsbGetHvdcp,
    EFI_PmicUsbSetOtgILimit,
    EFI_PmicUsbEnableOtg,
    EFI_PmicUsbOtgStatus,
    EFI_PmicUsbGetRidStatus,
};

