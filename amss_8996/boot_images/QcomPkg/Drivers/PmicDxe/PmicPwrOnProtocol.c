/*! @file PmicPowerOnProtocol.c 

 *  PMIC-POWERON MODULE RELATED DECLARATION
 *  This file contains functions and variable declarations to support 
 *  the PMIC POWERON module.
 *
 * Copyright (c) 2012-2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/21/15   sv      Removed unwanted variable check from PonTrigger API.
01/14/15   al      Adding API to enable/disable PON trigger
06/09/14   al      Arch update
05/19/14   sm      Added API to get PBL_PON Status
                   Deprecated GetBatteryRemovedStatus API
04/29/14   al      Deprecating unsupported APIs� 
02/20/14   al      Adding watchdog APIs 
13/12/13   aa      PmicLib Dec Addition
01/29/13   al      Cleaning compiler warnings  
01/24/13   al      Adding API to get pmic on/off/reset reason 
11/01/12   al      Battery removal status 
10/25/12   al      File renamed 
02/27/12   al      Added device index and resource index
04/11/12   dy      Add GetWatchDogStatus API
04/04/12   al      Added API EFI_PmicPwrUpHardReset 
03/20/12   sm      New file.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/**
  EFI interfaces
 */
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>

/**
  PMIC Lib interfaces
 */
#include "pm_uefi.h"
#include "pm_pon.h"
#include "Protocol/EFIPmicPwrOn.h"

/*===========================================================================
                  EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/
/**
  EFI_PmicPwronHardResetEnable ()

  @brief
  HardResetEnable implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicPwronHardResetEnable
(
  IN UINT32  PmicDeviceIndex, 
  IN BOOLEAN Enable
)
{
  (void)PmicDeviceIndex;
  (void)Enable;
  return EFI_UNSUPPORTED;
}

/**
  EFI_PmicPwronHardResetPwrup()

  @brief
  PwrUpHardReset implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicPwronHardResetPwrup
(
  IN UINT32  PmicDeviceIndex, 
  IN BOOLEAN Enable
)
{
  (void)PmicDeviceIndex;
  (void)Enable;
  return EFI_UNSUPPORTED;
}

/**
  EFI_PmicPwronHardResetSetDebounceTimer ()

  @brief
  SetDebounceTimer implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicPwronHardResetSetDebounceTimer
(
  IN UINT32 PmicDeviceIndex, 
  IN INT32  DebounceTimermS
)
{
  (void)PmicDeviceIndex;
  (void)DebounceTimermS;
  return EFI_UNSUPPORTED;
}

/**
  EFI_PmicPwronHardResetSetDelayTimer ()

  @brief
  SetDelayTimer implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicPwronHardResetSetDelayTimer
(
  IN UINT32 PmicDeviceIndex, 
  IN INT32  DelayTimermS
)
{
  (void)PmicDeviceIndex;
  (void)DelayTimermS;
  return EFI_UNSUPPORTED;
}

/**
  EFI_PmicPwronHardResetGetPoweronTrigger ()

  @brief
  GetPoweronTrigger implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS 
EFIAPI 
EFI_PmicPwronHardResetGetPoweronTrigger
(
  IN UINT32 PmicDeviceIndex, 
  OUT UINT8 *TriggerType
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_pon_get_pon_reason(PmicDeviceIndex, (pm_pon_pon_reason_type *)TriggerType);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicPwronGetWatchdogStatus ()

  @brief
  GetWatchdogStatus implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS 
EFIAPI 
EFI_PmicPwronGetWatchdogStatus
(
  IN  UINT32  PmicDeviceIndex, 
  OUT BOOLEAN *DogStatus
)
{
  (void)PmicDeviceIndex;
  (void)DogStatus;
  return EFI_UNSUPPORTED;
}

EFI_STATUS 
EFIAPI 
EFI_PmicPwronGetBatteryRemovedStatus
(
  IN  UINT32  PmicDeviceIndex, 
  OUT BOOLEAN *BatteryRemoved
)
{
 (void)PmicDeviceIndex;
 (void)BatteryRemoved;

  return EFI_UNSUPPORTED;
}


EFI_STATUS 
EFIAPI 
EFI_PmicPwronWdogCfg
(
  IN UINT32                      PmicDeviceIndex, 
  IN UINT32                      S1Timer,
  IN UINT32                      S2Timer,
  IN EFI_PM_PWRON_RESET_CFG_TYPE ResetCfgType 
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;
  
  errFlag = pm_pon_wdog_cfg(PmicDeviceIndex, S1Timer, S2Timer,(pm_pon_reset_cfg_type)ResetCfgType);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

EFI_STATUS 
EFIAPI 
EFI_PmicPwronWdogEnable
(
  IN UINT32                      PmicDeviceIndex, 
  IN BOOLEAN                     Enable
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;
  pm_on_off_type OnOff = (Enable)? PM_ON : PM_OFF ;

  errFlag = pm_pon_wdog_enable(PmicDeviceIndex, OnOff);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}


EFI_STATUS 
EFIAPI 
EFI_PmicPwronWdogPet
(
  IN UINT32 PmicDeviceIndex
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  errFlag = pm_pon_wdog_pet(PmicDeviceIndex);

  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}


/**
  EFI_PmicPwronGetPonPblStatus ()

  @brief
  GetPonPblStatus implementation of EFI_QCOM_PMIC_POWERON_PROTOCOL
 */
EFI_STATUS 
EFIAPI 
EFI_PmicPwronGetPonPblStatus
(
  IN  UINT32 PmicDeviceIndex, 
  IN  EFI_PM_PWRON_PON_PBL_STATUS_TYPE PblStatusType,
  OUT BOOLEAN *Status
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  if(!Status)
  {
    return EFI_INVALID_PARAMETER;
  }

  errFlag = pm_pon_pbl_get_status(PmicDeviceIndex, (pm_pon_pbl_status_type)PblStatusType, Status);
  if(PM_ERR_FLAG__SUCCESS != errFlag)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicPwronSetPonTrigger ()

  @brief
  EFI_PmicPwronSetPonTrigger implementation of EFI_PM_PWRON_SET_PON_TRIGGER 
 */
EFI_STATUS 
EFIAPI 
EFI_PmicPwronSetPonTrigger
(
  IN UINT32 PmicDeviceIndex, 
  IN EFI_PM_PON_TRIGGER_TYPE PonTrigger,
  OUT BOOLEAN Enable
)
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;
  EFI_STATUS Status = EFI_SUCCESS;

  errFlag = pm_pon_trigger_enable(PmicDeviceIndex, (pm_pon_trigger_type)PonTrigger, Enable);

  Status = (PM_ERR_FLAG__SUCCESS == errFlag)? EFI_SUCCESS : EFI_DEVICE_ERROR;

  return Status;
}



/**
  PMIC PWRON UEFI Protocol implementation
 */
EFI_QCOM_PMIC_PWRON_PROTOCOL PmicPwronProtocolImplementation = 
{
  PMIC_PWRON_REVISION,
  EFI_PmicPwronHardResetEnable,
  EFI_PmicPwronHardResetPwrup,
  EFI_PmicPwronHardResetSetDebounceTimer,
  EFI_PmicPwronHardResetSetDelayTimer,
  EFI_PmicPwronHardResetGetPoweronTrigger,
  EFI_PmicPwronGetWatchdogStatus,
  EFI_PmicPwronGetBatteryRemovedStatus,
  EFI_PmicPwronWdogCfg,
  EFI_PmicPwronWdogEnable,
  EFI_PmicPwronWdogPet,
  EFI_PmicPwronGetPonPblStatus,
  EFI_PmicPwronSetPonTrigger,
};
