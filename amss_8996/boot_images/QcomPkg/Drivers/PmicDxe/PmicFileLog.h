#ifndef __PMICFILELOG_H__
#define __PMICFILELOG_H__

/*! @file PmicFileLog.h
 *
 *  PMIC FILE Logging
 *
 *  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
12/09/14   al      Sync with latest
08/21/14   va      Enable File Logging 

===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
  /**
  File Logging Dependencies 
  */
#include "ULogFront.h"
#include "ULogUEFI.h"

/** 
Protocol Dependencies 
*/ 
#include <Protocol/EFIPmicFg.h>

/*===========================================================================
                     MACRO DEFINATIONS
===========================================================================*/
//Charger Log File Name
#define LOGFILE_IN_EFS "PmicLog"
#define LOGFILE_SIZE   8388608 // 8MB


#define _ULOG(Expression)   ULogPrint Expression

#if !defined(CHARGER_DEBUG)      
  #define PMIC_DEBUG(Expression)        \
    do {                           \
      if (PmicFileLog_PrintDebugMsg ()) {  \
        _DEBUG (Expression);       \
      }                            \
      if(PmicFileLog_PrintDebugMsgToFile()) {  \
        _ULOG (Expression);         \
      }                            \
    } while (FALSE)
  #define PMIC_FILE_DEBUG(Expression)        \
    do {                           \
      if(PmicFileLog_PrintDebugMsgToFile()) {  \
        _ULOG (Expression);         \
      }                            \
    } while (FALSE)
  #define PMIC_UART_DEBUG(Expression)        \
    do {                           \
      if (PmicFileLog_PrintDebugMsg ()) {  \
        _DEBUG (Expression);       \
      }                            \
    } while (FALSE)
#else
  //#define CHARGER_DEBUG(Expression)
#endif


/*===========================================================================
                     TYPE DECLARATIONS
===========================================================================*/


/*===========================================================================
                       FUNCTION PROTOTYPES
===========================================================================*/

BOOLEAN EFIAPI PmicFileLog_PrintDebugMsg(void);

BOOLEAN EFIAPI PmicFileLog_PrintDebugMsgToFile(void);

EFI_STATUS PmicFileLog_InitFileLog(EFI_PM_FG_CFGDATA_TYPE * BatteryDefaults);

VOID  ULogPrint (IN  UINTN ErrorLevel, IN  CONST CHAR8  *Format,  ...);


#endif // __PMICFILELOG_H__


