/*! @file PmicFileLog.c 
 *
 *  PMIC FILE Logging
 *
 *  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   al      Sync with latest 
08/21/14   va      Enable File Logging 

===========================================================================*/

/*===========================================================================
                     CONSTANTS FOR MODULE
===========================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <Uefi.h>

#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/QcomLib.h>
#include <Library/QcomTargetLib.h>

#include "pm_uefi.h"
#include "PmicFileLog.h"
/** 
Protocol Dependencies 
*/ 
#include <Protocol/EFIPlatformInfo.h>


/*===========================================================================
                               LOCAL FUNCTION DECLARATIONS 
===========================================================================*/


/*===========================================================================
                               TYPE DEFINITIONS
===========================================================================*/
STATIC ULogHandle  gULogHandle = NULL;
STATIC BOOLEAN     PrintChargerAppDbgMsg = FALSE;
STATIC BOOLEAN     PrintChargerAppDbgMsgToFile = FALSE;

/*===========================================================================
                  FUNCTION DEFINATIONS
===========================================================================*/
/**
PmicFileLog_PrintDebugMsg()

@brief
Returns PmicFileLog_PrintDebugMsg Flag Status
*/
BOOLEAN
EFIAPI
  PmicFileLog_PrintDebugMsg(void)
{
  return PrintChargerAppDbgMsg;
}

/**
PmicFileLog_PrintDebugMsgToFile()

@brief
Returns PmicFileLog_PrintDebugMsgToFile Flag Status
*/
BOOLEAN
EFIAPI
  PmicFileLog_PrintDebugMsgToFile(void)
{
  //TBD ULOG to file dump
  if (NULL != gULogHandle)
    return PrintChargerAppDbgMsgToFile;
  return FALSE;
}


void EFIAPI
ULogPrint (
  IN  UINTN        ErrorLevel,
  IN  CONST CHAR8  *Format,
  ...
  )
  {
    va_list    vlist;
    UINT32 dataCount;
    UINTN i;
    BOOLEAN  boEsc=FALSE;
    for (i=0, dataCount=0; Format[i] !='\0'; i++)
    {
        if(Format[i]=='%' && !boEsc) dataCount++;
                    else if (Format[i]=='\\' && !boEsc) boEsc = TRUE;
                       else if (boEsc) boEsc = FALSE;
    }
    va_start(vlist, Format);
    ULogFront_RealTimeVprintf(gULogHandle,  dataCount, Format, vlist);
    va_end(vlist);
}

/* Init File Logging in ULog */
EFI_STATUS PmicFileLog_InitFileLog(EFI_PM_FG_CFGDATA_TYPE * BatteryDefaults)
{
  //get BDS menu ULOG setting from BDS Menu
  ULogResult result = 0;
  BOOLEAN    FileLoggingIsEnabled = TRUE;
  EFI_STATUS Status = EFI_UNSUPPORTED;
  UINTN      VarSize = 0;
  ULOG_CONFIG_TYPE configType = {0};

  if (NULL == BatteryDefaults)
    return EFI_INVALID_PARAMETER;

  PrintChargerAppDbgMsg = BatteryDefaults->PrintChargerAppDbgMsg;

  VarSize = sizeof(FileLoggingIsEnabled);

  if (TRUE == BatteryDefaults->PrintChargerAppDbgMsgToFile){
    //Enable default file logging if production mode is disabled
    if(PRODMODE_DISABLED)
    {
      Status = gRT->SetVariable (L"EnableFileLogging", &gQcomTokenSpaceGuid,
                   EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                 VarSize, &FileLoggingIsEnabled);
      if(Status != EFI_SUCCESS)
      {
        DEBUG(( EFI_D_WARN, "PmicDxe: File Logging BDS menu var read error \n\r"));
        return EFI_SUCCESS;
      }
      result = ULogFile_Open(&gULogHandle, LOGFILE_IN_EFS, LOGFILE_SIZE);
      if (0 == result){
        ULOG_RT_PRINTF_1(gULogHandle, "PmicDxe: InitFileLog SUCCESS gULogHandle = (%d)", gULogHandle);
        /* Set ULog configuration */
        ULogFile_GetConfig(&gULogHandle, &configType);
        configType.separator = ',';
        configType.PrintTimestamp = TRUE;
        configType.TrimNewline = TRUE;
        ULogFile_SetConfig(&gULogHandle, &configType);

        //Set Flag now 
        PrintChargerAppDbgMsgToFile = BatteryDefaults->PrintChargerAppDbgMsgToFile;
        DEBUG(( EFI_D_INFO, "PmicDxe: InitFileLog SUCCESS \n\r"));
      }else{
        DEBUG(( EFI_D_WARN, "PmicDxe: InitFileLog FAILED \n\r"));
      }
    }
    else
    {
      //Disbale file logging flag since production mode is enabled
      PrintChargerAppDbgMsgToFile = FALSE;
    }
  }
  else{
    DEBUG(( EFI_D_WARN, "PmicDxe: File Logging Disabled \n\r"));
  }
  return EFI_SUCCESS;
}



