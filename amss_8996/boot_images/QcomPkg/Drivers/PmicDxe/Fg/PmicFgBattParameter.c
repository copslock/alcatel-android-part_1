/*! @file PmicBatteryParameter.c
 *
 *  PMIC-FG, CHARGER BATTERY PARAMETERS MODULE RELATED DECLARATION
 *  This file contains functions and variable declarations to support
 *  the PMIC Charger module.
 *
 *  Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/10/15   sm      Added OsNonStandardBootSocThreshold and OsStandardBootSocThreshold 
                   config parameters
05/29/15   va      Read Battery Temperature in Fg Init
05/12/15   mr      Enabled Charger functionality for 8952 (CR-846387)
04/30/15   va      IChgMax and VddMax configuration support for all Jeita windows
                   JEITA Low temp, high temp handle
04/16/15   al      Added for reading FG restart time
04/30/15   al      Make FCC calibration execution configurable
04/16/15   va      Moving FG SRAM Release to protocol file
04/09/15   va      JEITA algo Fix - Fcc and Fv Max to be set when temperature reading is available
04/08/15   va      Rslow WA Changes and sram dump only on DEBUG builds
02/25/15   sm      Added function to read SRAM shadow current
02/23/15   al      removing redundant debug message
02/20/15   al      Adding for range for analog battery for battery provision not present
01/19/15   al      Adding battery error handling related
01/12/15   al      Adding volt reading in cache
01/05/15   va      Added Multiple Battery Profile and Profile Parser Status API
12/19/14   al      Removing redundant
12/17/14   va      Reading QcomCharger support from BDS
12/02/14   va      Conditional restart workaround Changes, updated DEBUG flags
11/10/14   sm      Removed CalibrateIbat and CalibrateVbat APIs
09/29/14   va      Jeita Feature
10/27/14   va      Update for New Battery Profile Format
10/06/14   va      Release FG Sram access during UEFI exit
09/25/14   al      Typecast fix
09/23/14   al      Implementing feature to over-write default config data using mass storage
08/21/14   va      Enable File Logging
06/20/14   va      New file.
===========================================================================*/

/*===========================================================================
                     CONSTANTS FOR MODULE
===========================================================================*/

#define TIMER_MICRO_SEC(x) ((x) * 10)
#define TIMER_MILLI_SEC(x) (TIMER_MICRO_SEC(x) * 1000)
//Battery Configuration File
#define PMIC_BATTERY_CFG_FILE_IN_FV     L"PmicChargerApp.cfg"

#define PMIC_BATTERY_OVER_WRT_CFG_FILE_PATH     L"\\PmicChargerApp.cfg"

#define TEMP_DEGREE_SCALED_CONVERSION         625
#define TEMP_DEGREE_CONVERSION_SCALING_FACTOR 10000

#define VOLT_DEGREE_SCALED_CONVERSION         152588 /*0.000152587890625*/
#define VOLT_DEGREE_CONVERSION_SCALING_FACTOR 1000000

#define VOLT_DEGREE_SCALED_CONVERSION_IRQ          9765625 /*0.009765625*/
#define VOLT_DEGREE_IRQ_CONVERSION_SCALING_FACTOR  1000000

#define TEMP_IN_KELVIN 273

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <Uefi.h>

#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/QcomLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/ParserLib.h>
#include <Library/QcomTargetLib.h>
#include <Library/BaseMemoryLib.h>

#include "pm_uefi.h"

#include "PmicFgBattParameter.h"
/**
File Log Dependencies
*/
#include "PmicFileLog.h"

/**
  Fg Dependencies
 */
#include "PmicFgSram.h"
#include "pm_fg_soc.h"
#include "pm_fg_adc_usr.h"
#include "pm_fg_memif.h"

/**
Protocol Dependencies
*/
#include <Protocol/EFIPlatformInfo.h>



/*===========================================================================
                               LOCAL FUNCTION DECLARATIONS
===========================================================================*/


/*===========================================================================
                               TYPE DEFINITIONS
===========================================================================*/

EFI_EVENT SramCacheParamEvent;
EFI_EVENT SramDumpEvent;

VOID PmicFgBattParam_CacheEventHandler(
  IN EFI_EVENT Event,
  IN VOID *Context
);

VOID PmicFgBattParam_DumpSramEventHandler(
  IN EFI_EVENT Event,
  IN VOID *Context
);

STATIC EFI_PM_FG_CFGDATA_TYPE PmicBatteryConfigDefaults;
STATIC FgBattParamCacheState  BattParamCacheState;
STATIC FgBattParamCache       BattParamCache;

static VOID  PmicFgBattProfileValCb (UINT8* Section, UINT8* Key, UINT8* Value);
extern INT32 PmicFgBattProfileAsciiToInt( char *Str );
static UINT32 PmicFgBattParamConvertTemp(UINT32 *fg_memif_data);

static EFI_STATUS PmicFgBattProfileOverWrtDfltConfig(void);

extern EFI_GUID gEfiEmmcUserPartitionGuid;
extern EFI_GUID gEfiBDPPartitionGuid;

/*===========================================================================
                  EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/
/**
PmicFgBattParam_Init()

@brief
Initializes Battery Parameters
*/
EFI_STATUS PmicFgBattParam_Init( void )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;

  //Initialize temperature value = 25 i.e. 297K, battery voltage = 3800 mV
  BattParamCache.FgBattParam_CachedTemperature = 298;
  /* set temperature invalid */
  BattParamCache.FgBattParam_TemperatureValid = FALSE;
  BattParamCache.FgBattParam_CachedBattVoltage = 3800;
  BattParamCache.FgBattParam_CachedShadowCurrent = 0;
  /*FALSE to first execute Rslow Charge clear sequence */
  BattParamCache.FgBattParam_RslowChargeFix      = FALSE;

  BattParamCacheState = FG_BATTPARAM_CACHESTATE_INIT;

  /* Reading Battery Profile from EFIESP, DPP, default */
  (void) PmicFgBattParam_ReadDefaultCfg();

  SetMem(&BatteryDefaults, sizeof(EFI_PM_FG_CFGDATA_TYPE), 0x00);

  Status |= PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);
  if ( EFI_SUCCESS == Status )
  {
    DEBUG(( EFI_D_INFO, "PmicDxe: Success in reading battery default config parameters  = (%d)\n\r", Status));
  }else{
    DEBUG(( EFI_D_ERROR, "PmicDxe: Error in getting battery default config parameters  = (%d)\n\r", Status));
  }

 /* Init File log and UART log */
  PmicFileLog_InitFileLog(&BatteryDefaults);

  PmicFgBattParam_InitJeita(&BatteryDefaults);

  PmicFgBattProfileInit();

  return Status;
}

/**
PmicFgBattParam_InitJeita()

@brief
Initializes Battery Jeita Feature
*/
EFI_STATUS PmicFgBattParam_InitJeita(EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (TRUE == BatteryDefaults->JeitaCfgData.SWJeitaEnable){
    PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: Software JEITA Enabled (%d)\n\r", Status));
  }else{
    PMIC_DEBUG(( EFI_D_WARN, "PmicDxe: HW JEITA Enabled (%d)\n\r", Status));
  }
  return Status;
}

/**
PmicFgBattParam_StartCacheUpdate()

@brief
Starts Fuel Gauge SRAM Cache and Dump Timer
*/
EFI_STATUS PmicFgBattParam_StartCacheUpdate( void )
{

  EFI_STATUS Status = EFI_SUCCESS;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;

  Status |= PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);

  if(TRUE == BatteryDefaults.TempCacheTimer){
    if (FG_TEMPERATURE_CACHETIMER_LOWER_LIMIT < BatteryDefaults.TempCacheTimerDuration){
      Status |= PmicFgBattParam_StartFgParamCacheTimer(TIMER_MILLI_SEC(BatteryDefaults.TempCacheTimerDuration));
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: _StartCacheUpdate Status (%d) FG Parameter Cache timer Started duration = (%d)\n\r", Status, TIMER_MILLI_SEC(BatteryDefaults.TempCacheTimerDuration)));
    }else{
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: _StartCacheUpdate FG Parameter Cache timer Periodic value is invalid = (%d)\n\r", BatteryDefaults.TempCacheTimerDuration));
      Status = EFI_INVALID_PARAMETER;
    }
  }else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: _StartCacheUpdate FG Parameter Cache timer NOT Started \n\r"));
  }

  if(TRUE == BatteryDefaults.DumpSram){
    if(PRODMODE_DISABLED)
    {
      PmicFgBattParam_StartSramDumpTimer(TIMER_MILLI_SEC(BatteryDefaults.DumpSramDuration));
      PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: _StartCacheUpdate SRAM dump initiated BatteryDefaults.DumpSramDuration = (%d)\n\r", TIMER_MILLI_SEC(BatteryDefaults.DumpSramDuration)));
    }
  }else{
    PMIC_DEBUG(( EFI_D_ERROR, "PmicDxe: _StartCacheUpdate SRAM dump was not initiated BatteryDefaults.DumpSram = (%d) \n\r", BatteryDefaults.DumpSramDuration));
  }
  return Status;
}


/**
PmicFgGetBattCurrent()

  @brief
  Get Battery Current
 */
EFI_STATUS
PmicFgGetBattCurrent
(
  IN UINT32 PmicDeviceIndex, OUT INT32 *pIBattInternal
)
{
  EFI_STATUS Status = EFI_SUCCESS;
  //TBD
  return Status;
}


/**
PmicFgBattParam_StartFgParamCacheTimer()

@brief
Starts Fuel Gauge Cache Timer to retrieve Temperature value
*/
EFI_STATUS PmicFgBattParam_StartFgParamCacheTimer(UINT32 FgParamCacheTimerDuration)
{
  EFI_STATUS Status  = EFI_SUCCESS;
  /* Create Charge Termination Event Token */
  Status  = gBS->CreateEvent(  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                     TPL_CALLBACK,
                     PmicFgBattParam_CacheEventHandler,
                     NULL,
                     &SramCacheParamEvent );
  if( PM_ERR_FLAG__SUCCESS != Status)
  {
    return EFI_DEVICE_ERROR;
  }

  Status |= gBS->SetTimer(SramCacheParamEvent, TimerPeriodic, FgParamCacheTimerDuration);

  if( PM_ERR_FLAG__SUCCESS == Status)
  {
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _StartFgParamCacheTimer, Timer started - TimerDuration (%d)\n", FgParamCacheTimerDuration));
  }
  else
  {
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _StartFgParamCacheTimer, Timer start fails - Status (%d)\n", Status));
  }

  return Status;
}


/**
PmicFgBattParam_StartFgParamCacheTimer()

@brief
Starts Fuel Gauge SRAM Dumpe Timer for log purpose
*/
EFI_STATUS PmicFgBattParam_StartSramDumpTimer(UINT32 DumpSramDuration)
{
  EFI_STATUS Status  = EFI_SUCCESS;
  /* Create Charge Termination Event Token */
  Status  = gBS->CreateEvent(  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                             TPL_CALLBACK,
                             PmicFgBattParam_DumpSramEventHandler,
                             NULL,
                             &SramDumpEvent );
  if( PM_ERR_FLAG__SUCCESS != Status)
  {
    return EFI_DEVICE_ERROR;
  }

  Status |= gBS->SetTimer(SramDumpEvent, TimerPeriodic, DumpSramDuration);
  if( PM_ERR_FLAG__SUCCESS == Status){
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _StartSramDumpTimer Timer Started DumpSramDuration (%d), Status (%d)\n", DumpSramDuration, Status));
  }else{
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _StartSramDumpTimer Timer Started Status (%d)\n", Status));
  }
  return Status;
}


/**
PmicFgBattParam_DumpSramEventHandler()

@brief
Fuel Gauge Sram Dump Timer Timer Event Handler
*/
VOID EFIAPI PmicFgBattParam_DumpSramEventHandler(IN EFI_EVENT Event, IN VOID *Context)
{
  EFI_STATUS Status  = EFI_SUCCESS;
  UINT32  PmicDeviceIndex = 1;
  FgSramState SramSt = FG_SRAM_STATUS_INVALID;
  EFI_PM_FG_CFGDATA_TYPE BatteryDefaults;

  Status  = PmicFgBattParam_GetDefaultCfg(&BatteryDefaults);

  Status |= PmicFgSram_GetState(&SramSt);

  if((FG_SRAM_STATUS_AVAILABLE == SramSt) && (EFI_SUCCESS == Status))
  {
    // PMIC_DEBUG((EFI_D_ERROR, "PmicDxe : _DumpSramEventHandler SRAM Stat (%d), Status = (%d)\n", SramSt, Status));
    Status = PmicFgSram_Dump(PmicDeviceIndex, BatteryDefaults.DumpSramStartAddr, BatteryDefaults.DumpSramEndAddr);
  }
  // else
  // {
    // PMIC_DEBUG((EFI_D_ERROR, "## PmicFgBattParam_DumpSramEventHandler SRAM In USE Stat = (%d) Status = (%d)\n", SramSt, Status));
  // }

  return;
}

/**
PmicFgBattParam_CancelTimers()

@brief
Cancel All timer associated with Battery Parameters
*/
EFI_STATUS PmicFgBattParam_CancelTimers()
{
  //Make sure to release SRAM memroy access since this is UEFI exit process
  PmicFgSram_ReleaseFgSramAccess(PM_DEVICE_1);
  //Cancelling both timers
  gBS->CloseEvent(SramDumpEvent);
  gBS->CloseEvent(SramCacheParamEvent);

  return EFI_SUCCESS;
}

/**
PmicFgBattParamConvertTemp()

@brief
Temprature conversion to K from 2nd -3 rd byte value from 4 byte read
*/
static UINT32 PmicFgBattParamConvertTemp(UINT32 *fg_memif_data)
{
  UINT32 temperature = 0;
  UINT8  byte1 = 0x00, byte2 = 0x00;

  //get 2-3 byte as per the MDOS offset 1
  temperature = (0x00FFFF00 & *fg_memif_data);

  byte1 = (temperature >> 8);
  byte2 = (temperature >> 16);
  temperature = (UINT32 ) byte1 | (byte2 << 8) ;
  temperature = (temperature * TEMP_DEGREE_SCALED_CONVERSION )/ TEMP_DEGREE_CONVERSION_SCALING_FACTOR; //temperature is in K

  return temperature;
}


/**
PmicFgBattParamConvertVolt()

@brief
Temprature conversion to K from 2nd -3 rd byte value from 4 byte read
*/
EFI_STATUS PmicFgBattParamConvertVolt(UINT32 *Data)
{
  UINT32 batt_volt = 0;
  UINT8  byte1 = 0x00, byte2 = 0x00;

  //get 2-3 byte as per the MDOS offset 1
  batt_volt = (0x00FFFF00 & *Data);

  byte1 = (batt_volt >> 8);
  byte2 = (batt_volt >> 16);
  batt_volt = (UINT32 ) byte1 | (byte2 << 8) ;
  *Data = (batt_volt * VOLT_DEGREE_SCALED_CONVERSION )/ VOLT_DEGREE_CONVERSION_SCALING_FACTOR; //voltage in milliVolt

  return EFI_SUCCESS;
}

/**
PmicFgBattParamConvertVEmptyIrq()

@brief
   Empty Voltage Conversion
*/
EFI_STATUS PmicFgBattParamConvertVEmptyIrq(UINT32 *Data)
{
  UINT32 VBat = 0;
  UINT32 VoltOFfset = 2500; /*2.5 V = 2500mv*/

  //get 4th byte as per the MDOS offset 1
  VBat = (*Data >> 24);

  *Data = VoltOFfset + (VBat * VOLT_DEGREE_SCALED_CONVERSION_IRQ )/ VOLT_DEGREE_IRQ_CONVERSION_SCALING_FACTOR; //Data in milliVolt

  return EFI_SUCCESS;
}

/**
PmicFgBattParamConvertCurrent()

@brief
Temprature conversion to K from 2nd -3 rd byte value from 4 byte read
*/
static INT32 PmicFgBattParamConvertCurrent(FgBattParamCache *ParamCache)
{
  INT16 shadow_current = 0;
  UINT8 byte1 = 0x00, byte2 = 0x00;

  /* Get last byte from 0x5CC */
  byte1 = (UINT8) ((0xFF000000 & ParamCache->FgBattParam_CachedBattVoltage)>>24) ;
  /* Get first byte from 0x5D0 */
  byte2 = (UINT8) (0x000000FF & ParamCache->FgBattParam_CachedTemperature);

  shadow_current = (UINT32 ) ((byte2 << 8) | byte1 ) ;
  if(byte2 & 0x80)
  {
    //Perform 2'c complement
    shadow_current = (~shadow_current) + 1;
  }

  shadow_current = (shadow_current * VOLT_DEGREE_SCALED_CONVERSION )/ VOLT_DEGREE_CONVERSION_SCALING_FACTOR; //voltage in milliVolt

  if(!(byte2 & 0x80))
  {
    //Invert sign for user if we are discharging
    shadow_current = -shadow_current;
  }

  return (INT32)shadow_current;
}


/**
PmicFgBattParam_CacheEventHandler()

@brief
Fuel Gauge Sram Cache Timer Timer Event Handler
*/
VOID EFIAPI PmicFgBattParam_CacheEventHandler(IN EFI_EVENT Event, IN VOID *Context)
{
  EFI_STATUS Status  = EFI_SUCCESS;
  UINT32  PmicDeviceIndex = PM_DEVICE_1;
  FgSramState SramSt = FG_SRAM_STATUS_INVALID;
  FgBattParamCache ParamCache = {0};

  // PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: PmicFgBattParam_CacheEventHandler.. BattParamCacheState : (%d) \n", BattParamCacheState));
  switch(BattParamCacheState)
  {
    case FG_BATTPARAM_CACHESTATE_INIT:
    case FG_BATTPARAM_CACHESTATE_READ:
    {
      Status |= PmicFgSram_GetState(&SramSt);
      if(FG_SRAM_STATUS_AVAILABLE == SramSt )
      {
        Status |= PmicFgSram_ReadSingleAccess(PmicDeviceIndex, &ParamCache, &SramSt);
      }
      else
      {
        //PMIC_UART_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler SRAM In USE Stats = (%d) Status = (%d)\n", SramSt, Status));
        /* Continue and wait for SRAM Available */
        break;
      }

      if (FG_SRAM_STATUS_POLLING == SramSt)
      {
        BattParamCacheState = FG_BATTPARAM_CACHESTATE_POLL;
      }
      else if (FG_SRAM_STATUS_AVAILABLE == SramSt)    /* case where we get instant sram memory access */
      {
        PmicFgBattParam_UpdateCache(&ParamCache);
        /* Temperature read valid */
        BattParamCache.FgBattParam_TemperatureValid = TRUE;
        //PMIC_UART_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler Status = (%d) Battery Temperature = (0x%x)\n", Status, fg_memif_data));
      }
      // else
      // {
        // PMIC_UART_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler Status = (%d) SramSt = (0x%x)\n", Status, SramSt));
      // }
    }
    break;

    case FG_BATTPARAM_CACHESTATE_POLL:
    {
      Status = PmicFgSram_ReadSingleAccess(PmicDeviceIndex, &ParamCache, &SramSt);
      if (FG_SRAM_STATUS_AVAILABLE == SramSt)
      {
        BattParamCacheState = FG_BATTPARAM_CACHESTATE_READ;
        // PMIC_UART_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler Status = (%d) Battery Temperature = (0x%x)\n", Status, fg_memif_data));
        /* Update Cached value now, temperature is 16 bit value so masking 2 byte MSB */
        PmicFgBattParam_UpdateCache(&ParamCache);
        /* Temperature read valid */
        BattParamCache.FgBattParam_TemperatureValid = TRUE;

        // PMIC_UART_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler Queried Temperature = (%d) K \n", BattParamCache.FgBattParam_CachedTemperature));
      }
      else
      {
        BattParamCacheState = FG_BATTPARAM_CACHESTATE_POLL;
      }
    }
    break;

    case FG_BATTPARAM_CACHESTATE_INVALID:
    {
      PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler FG_BATTPARAM_CACHESTATE_INVALID Status = (%d) \r\n", Status));
      BattParamCacheState = FG_BATTPARAM_CACHESTATE_READ;
    }
    break;

    default:
    {
      PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _CacheEventHandler FG_BATTPARAM_CACHESTATE_DEFAULT Status = (%d) \r\n", Status));
      BattParamCacheState = FG_BATTPARAM_CACHESTATE_READ;
    }
    break;
  }

  return;
}

/**
  PmicFgBattParam_GetCachedShadowCurrent ()

  @brief
  Get shadow current from SRAM, Returns cached value
 */
EFI_STATUS
PmicFgBattParam_GetCachedShadowCurrent
(
  OUT INT32 *pShadowCurrent
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  *pShadowCurrent = BattParamCache.FgBattParam_CachedShadowCurrent;

  return Status;
}

/**
  PmicAdcGetBatteryTemperature ()

  @brief
  Get Battery Temperature, Returns cached value
 */
EFI_STATUS
PmicFgBattParam_GetCachedBattTemp
(
  OUT INT32 *pIBatttherm
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  //check for mipi bif temperature variation as well
  if(TRUE == BattParamCache.FgBattParam_TemperatureValid)
  {
    *pIBatttherm = BattParamCache.FgBattParam_CachedTemperature - TEMP_IN_KELVIN;
    //PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Battery Temperature Invalid = (%d)\r\n", *pIBatttherm));
  }
  else
  {
    Status = EFI_DEVICE_ERROR;
    PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: Battery Temperature Invalid = (%d)\r\n", BattParamCache.FgBattParam_TemperatureValid));
  }

  return Status;
}

/**
  PmicFgBattParam_IsBattTempValid ()

  @brief
  Get Battery Temperature valid status
 */
EFI_STATUS
PmicFgBattParam_IsBattTempValid
(
  OUT BOOLEAN *bValid
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(!bValid)
    return EFI_INVALID_PARAMETER;

  /* Get temp valid status from cache */
  *bValid = BattParamCache.FgBattParam_TemperatureValid;
  //PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: FgBattParam_TemperatureValid = (%d)\r\n", *bValid));

  return Status;
}


/**
  PmicFgBattParam_UpdateCache ()

  @brief
  Updates Cache
 */
EFI_STATUS
PmicFgBattParam_UpdateCache(FgBattParamCache *ParamCache)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if(!ParamCache)
    return EFI_INVALID_PARAMETER;

  BattParamCache.FgBattParam_CachedTemperature = PmicFgBattParamConvertTemp(&(ParamCache->FgBattParam_CachedTemperature));
  BattParamCache.FgBattParam_CachedBattVoltage = PmicFgBattParamConvertVolt(&(ParamCache->FgBattParam_CachedBattVoltage));
  BattParamCache.FgBattParam_CachedShadowCurrent = PmicFgBattParamConvertCurrent(ParamCache);
  BattParamCache.FgBattParam_TemperatureValid = ParamCache->FgBattParam_TemperatureValid;

  /*PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: PmicFgBattParam_UpdateCache  Temperature = (%d), BattVoltage = (%d) ShadowCurrent = (%d) ValidBit = (%d) \r\n",
              BattParamCache.FgBattParam_CachedTemperature - TEMP_IN_KELVIN,BattParamCache.FgBattParam_CachedBattVoltage,
              BattParamCache.FgBattParam_CachedShadowCurrent,BattParamCache.FgBattParam_TemperatureValid ));*/
  return Status;
}


/**
  PmicFgBattParam_GetCachedBattVolt ()

  @brief
  Get Battery voltage, Returns cached value
 */
EFI_STATUS
PmicFgBattParam_GetCachedBattVolt
(
  OUT INT32 *pBattVolt
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  *pBattVolt = BattParamCache.FgBattParam_CachedBattVoltage;
  //PMIC_DEBUG((EFI_D_ERROR, "PmicDxe: _Battery voltage = (%d)\r\n", *pBattVolt));

  return Status;
}


/**
PmicFgBattParam_ReadDefaultCfg()

@brief
Read Battery Parameter Default Configurations from configuration file
*/
EFI_STATUS
PmicFgBattParam_ReadDefaultCfg
(
  void
)
{
  EFI_STATUS  Status;
  UINT8*      FileBuffer = NULL;
  UINTN       FileSize   = 0;
  INTN        Pd;

  SetMem(&PmicBatteryConfigDefaults, sizeof(EFI_PM_FG_CFGDATA_TYPE), 0x00);
  Status = ReadFromFV(PMIC_BATTERY_CFG_FILE_IN_FV, (void **) &FileBuffer, &FileSize);
  if (Status == EFI_SUCCESS)
  {
    Pd = OpenParser (FileBuffer, (UINT32)FileSize, NULL);

    if (Pd < 0)
    {
      DEBUG(( EFI_D_WARN, "PmicDxe: Charger Config- Parser open failed\n"));
      Status = EFI_LOAD_ERROR;
    }
    else
    {
      /* Kick off Pmic CFG file interpreter */
      EnumKeyValues (Pd, (UINT8 *)"CHARGER Config", PmicFgBattProfileValCb);
    }

    /* Clean up resources */
    CloseParser(Pd);
    FreePool(FileBuffer);
  }

  /*read from EFISP partition to over write value*/
  if(PRODMODE_DISABLED)
  {
    PmicFgBattProfileOverWrtDfltConfig();
  }

  return Status;
}

/**
PmicFgBattProfileValCb()

@brief
Battery Parameter Default Configurations file read Call Back
*/
VOID
PmicFgBattProfileValCb
(
  UINT8* Section,
  UINT8* Key,
  UINT8* Value
)
{

  //PMIC_DEBUG(( EFI_D_INFO, "Section = \"%s\", Key = \"%s\", Value = \"%s\"\n", Section, Key, Value));

  if (AsciiStriCmp ((CHAR8*)Key, "CfgVersion") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "CfgVersion = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.CfgVersion = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  // SW jeita enable/disable
  if (AsciiStriCmp ((CHAR8*)Key, "SWJeitaEnable") == 0)
  {
    PmicBatteryConfigDefaults.JeitaCfgData.SWJeitaEnable = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempLimLow") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "BattTempLimLow = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.BattTempLimLow = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempLimHigh") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "BattTempLimHigh = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.BattTempLimHigh = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "WPBattCurrLimHighInmA") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "BattCurrLimHighInmA = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.WPBattCurrLimHighInmA = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp((CHAR8 *)Key, "ChgVddSafeInmV") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "ChgVddSafeInmV = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.ChgVddSafeInmV = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp((CHAR8 *)Key, "ChgFvMax") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "ChgFvMax = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.ChgFvMax = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp((CHAR8 *)Key, "WPBattVoltLimHighInmV") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "BattVoltLimHighInmV = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.WPBattVoltLimHighInmV = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "ChgIbatSafeInmA") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "ChgIbatSafeInmA = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.ChgIbatSafeInmA = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "ChgFccMax") == 0)
  {
    //PMIC_DEBUG(( EFI_D_INFO, "ChgFccMax = %d\n", PmicFgBattProfileAsciiToInt(Value) ));
    PmicBatteryConfigDefaults.ChgFccMax = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "BadBattShutdown") == 0)
  {
    PmicBatteryConfigDefaults.BadBattShutdown = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "ReadBattProfileFromPartition") == 0)
  {
    PmicBatteryConfigDefaults.ReadBattProfileFromPartition = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChkBattProfileFirstFromEfiEspOrPlat") == 0)
  {
    PmicBatteryConfigDefaults.ChkBattProfileFirstFromEfiEspOrPlat = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "PrintChargerAppDbgMsg") == 0)
  {
    PmicBatteryConfigDefaults.PrintChargerAppDbgMsg = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "PrintChargerAppDbgMsgToFile") == 0)
  {
    PmicBatteryConfigDefaults.PrintChargerAppDbgMsgToFile= ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DumpSram") == 0)
  {
    PmicBatteryConfigDefaults.DumpSram = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DumpSramStartAddr") == 0)
  {
    PmicBatteryConfigDefaults.DumpSramStartAddr = AsciiStrToHex((char *)Value, StrLen ((  UINT16 *)Value) + 2 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DumpSramEndAddr") == 0)
  {
    PmicBatteryConfigDefaults.DumpSramEndAddr = AsciiStrToHex((char *)Value, StrLen ((UINT16 *)Value) + 2 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DumpSramDuration") == 0)
  {
    PmicBatteryConfigDefaults.DumpSramDuration = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "TempCacheTimer") == 0)
  {
    PmicBatteryConfigDefaults.TempCacheTimer = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "TempCacheTimerDuration") == 0)
  {
    PmicBatteryConfigDefaults.TempCacheTimerDuration = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp((CHAR8 *)Key, "ChrgingTermCurrent") == 0)
  {
    PmicBatteryConfigDefaults.ChrgingTermCurrent = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "BatteryIdTolerance") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.BatteryIdTolerance = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "EnableDummyBattId") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.EnableDummyBattId = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DummyBattIdMax1") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DummyBattIdMax1 = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DummyBattIdMin1") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DummyBattIdMin1 = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

    if (AsciiStriCmp ((CHAR8*)Key, "DummyBattIdMax2") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DummyBattIdMax2 = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DummyBattIdMin2") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DummyBattIdMin2 = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DebugBoardBattIdMin") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DebugBoardBattIdMin = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DebugBoardBattIdMax") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.DebugBoardBattIdMax = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "AnalogBattIdMin") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.AnalogBattIdMin = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "AnalogBattIdMax") == 0)
  {
    PmicBatteryConfigDefaults.BattIdCfgData.AnalogBattIdMax = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "FgCondRestart") == 0)
  {
    PmicBatteryConfigDefaults.FgCondRestart = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "VBattEstDiffThreshold") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "VBattEstDiffThreshold = (%d) \n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.VBattEstDiffThreshold = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempLimLowCharging") == 0)
  {
    DEBUG(( EFI_D_ERROR, "BattTempLimLowCharging = (%d) \n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempLimLowCharging = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempJeitaT1Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT1Limit = (%d) \n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempJeitaT1Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempJeitaT2Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT2Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempJeitaT2Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempJeitaT3Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT3Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempJeitaT3Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempJeitaT4Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT4Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempJeitaT4Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "BattTempJeitaT5Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempJeitaT5Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "BattTempHysteresis") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempHysteresis = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.BattTempHysteresis = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFccMaxInJeitaT2Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFccMaxInJeitaT2Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFccMaxInJeitaT3Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ChgFccMaxInJeitaT3Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFccMaxInJeitaT3Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFccMaxInJeitaT4Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ChgFccMaxInJeitaT4Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFccMaxInJeitaT4Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFccMaxInJeitaT5Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ChgFccMaxInJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFccMaxInJeitaT5Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFvMaxInJeitaT2Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ChgFvMaxInJeitaT2Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFvMaxInJeitaT2Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFvMaxInJeitaT3Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ChgFvMaxInJeitaT3Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFvMaxInJeitaT3Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFvMaxInJeitaT4Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFvMaxInJeitaT4Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ChgFvMaxInJeitaT5Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChgFvMaxInJeitaT5Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
    if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFccMaxInJeitaT2Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "ConservChgFccMaxInJeitaT2Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFccMaxInJeitaT2Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFccMaxInJeitaT3Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ConservChgFccMaxInJeitaT3Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFccMaxInJeitaT3Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFccMaxInJeitaT4Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ConservChgFccMaxInJeitaT4Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFccMaxInJeitaT4Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFccMaxInJeitaT5Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ConservChgFccMaxInJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFccMaxInJeitaT5Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFvMaxInJeitaT2Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ConservChgFvMaxInJeitaT2Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFvMaxInJeitaT2Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFvMaxInJeitaT3Limit") == 0)
  {
    DEBUG(( EFI_D_ERROR, "ConservChgFvMaxInJeitaT3Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFvMaxInJeitaT3Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFvMaxInJeitaT4Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "ConservChgFvMaxInJeitaT4Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFvMaxInJeitaT4Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFvMaxInJeitaT5Limit") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "ConservChgFvMaxInJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ConservChgFvMaxInJeitaT5Limit = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "OsStandardBootSocThreshold") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "OsStandardBootSocThreshold = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.OsStandardBootSocThreshold = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "OsNonStandardBootSocThreshold") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "OsNonStandardBootSocThreshold = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.OsNonStandardBootSocThreshold = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "SupportQcomChargingApp") == 0)
  {
    PmicBatteryConfigDefaults.SupportQcomChargingApp = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "FullBattChargingEnabled") == 0)
  {
    PmicBatteryConfigDefaults.FullBattChargingEnabled = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "SDPMaxChargeCurrent") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.SDPMaxChargeCurrent = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "CDPMaxChargeCurrent") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.CDPMaxChargeCurrent = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "DCPMaxChargeCurrent") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BattTempJeitaT5Limit = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.DCPMaxChargeCurrent = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "OtherChargerChargeCurrent") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "OtherChargerChargeCurrent = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.OtherChargerChargeCurrent = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }
  if (AsciiStriCmp ((CHAR8*)Key, "FgForceOtpProfile") == 0)
  {
    PmicBatteryConfigDefaults.FgForceOtpProfile = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

  if (AsciiStriCmp ((CHAR8*)Key, "FgOtpProfileNum") == 0)
  {
    PmicBatteryConfigDefaults.FgOtpProfileNum = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

    if (AsciiStriCmp ((CHAR8*)Key, "ConservChgFvMax") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "ConservChgFvMax = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.ConservChgFvMax = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

   if (AsciiStriCmp ((CHAR8*)Key, "UnknownBattBehavior") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "UnknownBattBehavior = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.UnknownBattBehavior = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

   if (AsciiStriCmp ((CHAR8*)Key, "BootToHLOSThresholdMv") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "BootToHLOSThresholdMv = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.BootToHLOSThresholdMv = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

   if (AsciiStriCmp ((CHAR8*)Key, "ChargerCoolOffPeriodMs") == 0)
  {
    //DEBUG(( EFI_D_ERROR, "ChargerCoolOffPeriodMs = (%d)\n", PmicFgBattProfileAsciiToInt((char*)Value) ));
    PmicBatteryConfigDefaults.JeitaCfgData.ChargerCoolOffPeriodMs = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

   if (AsciiStriCmp ((CHAR8*)Key, "RunFccCalibration") == 0)
  {
    PmicBatteryConfigDefaults.RunFccCalibration = ( AsciiStriCmp ((CHAR8*)Value, "TRUE") == 0 );
    return;
  }

   if (AsciiStriCmp ((CHAR8*)Key, "RestartFgInactiveHrs") == 0)
  {
    PmicBatteryConfigDefaults.RestartFgInactiveHrs = PmicFgBattProfileAsciiToInt((char *)Value);
    return;
  }

  return;
}


/**
PmicFgBattParam_GetDefaultCfg()

@brief
Returns Battery Default Configurations
*/
EFI_STATUS
PmicFgBattParam_GetDefaultCfg
(
  EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults
  )
{
  UINT8  Flag = 0;
  UINTN  VarSize = 0;
  static boolean DebugMsgRead = FALSE;

  if (!BatteryDefaults)
    return EFI_INVALID_PARAMETER;

  if (FALSE == DebugMsgRead)
  {
    DebugMsgRead = TRUE;
    /*We are supposed to read only once. if the variable doesn't exist, make assumption on the value to treat
      variable not being there as expected condition and cache the expectation instead. We are memsetting the
      structure variable and so by default it is disabled
    */
    VarSize = sizeof(Flag);
    if (EFI_SUCCESS == gRT->GetVariable(L"PrintChargerAppDbgMsg", &gQcomTokenSpaceGuid, NULL, &VarSize, &Flag))
    {
      PmicBatteryConfigDefaults.PrintChargerAppDbgMsg = (Flag) ? TRUE : FALSE;
    }

    if (EFI_SUCCESS == gRT->GetVariable(L"RunQcomChgrApp", &gQcomTokenSpaceGuid, NULL, &VarSize, &Flag))
    {
      PmicBatteryConfigDefaults.SupportQcomChargingApp = (Flag) ? TRUE : FALSE;
    }

  }

  *BatteryDefaults = PmicBatteryConfigDefaults;

  return EFI_SUCCESS;
}

/**
  PmicFgBattProfileOverWrtDfltConfig
  @brief
  over writes the default config data from EFISP partition
**/
static EFI_STATUS PmicFgBattProfileOverWrtDfltConfig(void)
{
  EFI_STATUS Status          = EFI_SUCCESS;
  char       *tpData         = NULL;
  UINT32      DataSize       = 0;
  EFI_GUID   *RootDeviceType = &gEfiEmmcUserPartitionGuid;
  EFI_GUID   *PartitionType  = &gEfiBDPPartitionGuid;
  INTN        Pd;

  //Attempt to load Battery Provision file from EFI system parition
  Status = PmicFgBattProfileGetFileSize(PMIC_BATTERY_OVER_WRT_CFG_FILE_PATH,
                                        RootDeviceType,
                                        PartitionType,
                                        TRUE,
                                        NULL,
                                        (UINTN *)&DataSize);

  if (EFI_SUCCESS == Status && 0 != DataSize)
  {
    Status = gBS->AllocatePool(EfiBootServicesData, DataSize, (VOID **)&tpData);

    Status = PmicFgBattProfileReadFile(PMIC_BATTERY_OVER_WRT_CFG_FILE_PATH,
                                       RootDeviceType,
                                       PartitionType,
                                       TRUE,
                                       NULL,
                                       (UINTN *)&DataSize,
                                       0,
                                       (UINT8 *)tpData,
                                       DataSize);
  }

  Pd = OpenParser((UINT8 *)tpData, DataSize, NULL);

  if (Pd < 0)
  {
    Status = EFI_LOAD_ERROR;
  }
  else
  {
    /* Kick off Pmic CFG file interpreter */
    EnumKeyValues(Pd, (UINT8 *)"CHARGER Config", PmicFgBattProfileValCb);
  }

  /* Clean up resources */
  CloseParser(Pd);

  if(tpData != NULL)
  {
    Status = gBS->FreePool(tpData);
  }

  return Status;
}

