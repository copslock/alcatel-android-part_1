#ifndef __PMICFGBATTPARAMETER_H__
#define __PMICFGBATTPARAMETER_H__

/*! @file PmicBatteryParameter.h
 *
 *  PMIC-BATTERY PARAMETER CONVERSIONS RELATED DECLARATION
 *  This file contains functions and variable declarations to support 
 *  the PMIC BATTERY PARAMETER module.
 *
 *  Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/29/15   va      Read Battery Temperature in Fg Init
04/09/15   va      JEITA algo Fix - Fcc and Fv Max to be set when temperature reading is available
04/08/15   va      Rslow WA Changes 
02/25/15   sm      Added changes to read SRAM shadow current
01/19/15   al      Adding API to return battery type
12/26/14   al      Adding voltage caching from SRAM 
11/10/14   sm      Removed CalibrateIbat and CalibrateVbat APIs
10/26/14   vk      Remove inline
11/24/14   al      Removing inline
09/29/14   va      Jeita Feature
09/25/14   va      Update for New Battery Profile Format
08/21/14   va      Enable File Logging 
06/20/14   va      New file.
===========================================================================*/
/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
  
#include <Protocol/EFIPmicFg.h>

/*===========================================================================
                     MACRO DEFINATIONS
===========================================================================*/
#define FG_TEMPERATURE_CACHETIMER_LOWER_LIMIT 200 // ms

/*===========================================================================
                     TYPE DECLARATIONS
===========================================================================*/

//Cached FG Battery Parameter from SRAM 
typedef struct _FgBattParamCache
{
  UINT32 FgBattParam_CachedTemperature;
  UINT32 FgBattParam_CachedBattVoltage;
  INT32  FgBattParam_CachedShadowCurrent;
  BOOLEAN FgBattParam_RslowChargeFix; /* Rlsow work around Charge Fix or Clear seq flag */
  BOOLEAN  FgBattParam_TemperatureValid;
  //might need to add required parameters here
}FgBattParamCache;


//Cached FG Battery Parameter from SRAM 
typedef enum {

  FG_BATTPARAM_CACHESTATE_INIT,
  FG_BATTPARAM_CACHESTATE_READ,
  FG_BATTPARAM_CACHESTATE_POLL,
  FG_BATTPARAM_CACHESTATE_INVALID
  //might need to add required parameters here
}FgBattParamCacheState;


/*===========================================================================
                       FUNCTION PROTOTYPES
===========================================================================*/

EFI_STATUS PmicFgBattParam_Init( void );

EFI_STATUS PmicFgGetBattCurrent(IN UINT32 PmicDeviceIndex, OUT INT32 *BattCurrent);

EFI_STATUS PmicFgBattParam_GetCachedBattTemp( OUT INT32 *pBatttherm);

EFI_STATUS PmicFgBattParam_GetCachedShadowCurrent( OUT INT32 *pShadowCurrent );

EFI_STATUS PmicFgBattParam_GetCachedBattVolt( OUT INT32 *pBattVolt);

/*Read Config file*/
EFI_STATUS PmicFgBattParam_ReadDefaultCfg( void );

/*Get stored default values read from CFG file*/
EFI_STATUS PmicFgBattParam_GetDefaultCfg( EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults );

EFI_STATUS PmicFgBattParam_StartFgParamCacheTimer(UINT32 FgParamCacheTimerDuration);

EFI_STATUS PmicFgBattParam_StartSramDumpTimer(UINT32 DumpSramDuration);

EFI_STATUS PmicFgBattParam_StartCacheUpdate( void );

EFI_STATUS PmicFgBattParam_CancelTimers(void);

BOOLEAN EFIAPI PmicFgBattParam_PrintDebugMsg(void);

BOOLEAN EFIAPI PmicFgBattParam_PrintDebugMsgToFile(void);
EFI_STATUS PmicFgBattParam_InitJeita(EFI_PM_FG_CFGDATA_TYPE *BatteryDefaults);

EFI_STATUS PmicFgGetBatteryType(EFI_PM_FG_BATT_TYPE* BattType);

EFI_STATUS PmicFgBattParam_IsBattTempValid( OUT BOOLEAN *bValid);

EFI_STATUS PmicFgBattParamConvertVEmptyIrq(UINT32 *Data);

EFI_STATUS PmicFgBattParamConvertVolt(UINT32 *Data);

EFI_STATUS PmicFgBattParam_UpdateCache(FgBattParamCache *ParamCache);

#endif // __PMICBATTERYPARAMETER_H__

