/*! @file PmicVregProtocol.c 

 *  PMIC-VREG MODULE RELATED DECLARATION
 *  This file contains functions and variable declarations to support 
 *  the PMIC GPIO module.
 *
 *  Copyright (c) 2012 - 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 *  Qualcomm Technologies Proprietary and Confidential.
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/29/14   al      KW fixes
08/11/14   al      Adding API to read VREG_OK
06/09/14   al      Arch update 
04/24/14   al      Adding get_status 
13/12/13   aa      PmicLib Dec Addition
10/23/12   al      Adding phase control 
01/29/13   al      Cleaning compiler warnings  
10/25/12   al      File renamed 
10/23/12   al      Updating copyright info 
9/14/12    al      Added PmicVregSetPwrMode 
7/27/12    al      Moved to QcomPkg/Drivers/PmicDxe
09/15/11   sm      Added STELEVEL and GETLEVEL functions.
05/11/11   dy      New file.
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include "pm_ldo.h"
#include "pm_boost.h"
#include "pm_smps.h"
#include "pm_vs.h"
#include "pm_version.h"
#include "pm_uefi.h"

#include <Protocol/EFIPmicVreg.h>

/*===========================================================================
                  EXTERNAL FUNCTION DECLARATIONS
===========================================================================*/
/**
  EFI_PmicVregControl ()

  @brief
  VregControl implementation of EFI_QCOM_PMIC_VREG_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicVregControl
(
  IN UINT32                PmicDeviceIndex,  
  IN EFI_PM_VREG_ID_TYPE   VregId,
  IN BOOLEAN               Enable
)
{
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
  
  if(VregId < EFI_PM_SMPS_1) /*LDO*/
  {
    errFlag = pm_ldo_sw_enable(PmicDeviceIndex, (uint8)VregId,(pm_on_off_type) Enable);
  }
  else if (VregId > EFI_PM_LDO_33 && VregId < EFI_PM_VS_LVS_1) /*SMPS*/
  {
    if ((EFI_PM_SMPS_4 == VregId) && (PMIC_IS_PM8941 == pm_get_pmic_model(PmicDeviceIndex)))
    {
      errFlag = pm_boost_sw_enable(PmicDeviceIndex, 0,(pm_on_off_type)Enable);
    }
    else
    {
      errFlag = pm_smps_sw_enable(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1),(pm_on_off_type)Enable);
    }
  }
  else if (VregId > EFI_PM_SMPS_10 && VregId < EFI_PM_VREG_INVALID)   /*VS*/
  {
    errFlag = pm_vs_sw_enable(PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1),(pm_on_off_type)Enable);
  }
  else
  {
    return EFI_DEVICE_ERROR;
  }
  
  if (errFlag != PM_ERR_FLAG__SUCCESS)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicVregSetLevel ()

  @brief
  VregSetLevel implementation of EFI_QCOM_PMIC_VREG_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicVregSetLevel
(
  IN UINT32                 PmicDeviceIndex, 
  IN EFI_PM_VREG_ID_TYPE    VregId,
  IN UINT32                 Voltage
)
{
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  Voltage *= 1000;

  if(VregId < EFI_PM_SMPS_1) /*LDO*/
  {
    errFlag = pm_ldo_volt_level(PmicDeviceIndex, (uint8)VregId, Voltage);
  }
  else if (VregId > EFI_PM_LDO_33 && VregId < EFI_PM_VS_LVS_1) /*SMPS*/
  {
    errFlag = pm_smps_volt_level(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), Voltage);
  }
  else 
  {
    return EFI_DEVICE_ERROR;
  }

  if (errFlag != PM_ERR_FLAG__SUCCESS)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicVregGetLevel ()

  @brief
  VregGetLevel implementation of EFI_QCOM_PMIC_VREG_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicVregGetLevel
(
  IN UINT32               PmicDeviceIndex, 
  IN EFI_PM_VREG_ID_TYPE  VregId,
  OUT UINT32              *Voltage
)
{
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  if(VregId < EFI_PM_SMPS_1) /*LDO*/
  {
    errFlag = pm_ldo_volt_level_status(PmicDeviceIndex, (uint8)VregId,(uint32*) Voltage);
  }
  else if (VregId > EFI_PM_LDO_33 && VregId < EFI_PM_VS_LVS_1) /*SMPS*/
  {
    if ((EFI_PM_SMPS_4 == VregId) && (PMIC_IS_PM8941 == pm_get_pmic_model(PmicDeviceIndex)))
    {
      errFlag = pm_boost_volt_level_status(PmicDeviceIndex, 0, (uint32*)Voltage);
    }
    else
    {
      errFlag = pm_smps_volt_level_status(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), (uint32*)Voltage);
    }
  }
  else 
  {
    return EFI_DEVICE_ERROR;
  }

  if (errFlag != PM_ERR_FLAG__SUCCESS)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicVregSetPwrMode ()

  @brief
  VregSetPwrMode implementation of EFI_QCOM_PMIC_VREG_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicVregSetPwrMode
(
  IN UINT32                  PmicDeviceIndex, 
  IN EFI_PM_VREG_ID_TYPE     VregId,
  IN EFI_PM_PWR_SW_MODE_TYPE SwMode
)
{
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  if(VregId < EFI_PM_SMPS_1) /*LDO*/
  {
    errFlag = pm_ldo_sw_mode(PmicDeviceIndex, (uint8)VregId, (pm_sw_mode_type)SwMode);
  }
  else if (VregId > EFI_PM_LDO_33 && VregId < EFI_PM_VS_LVS_1) /*SMPS*/
  {
    errFlag = pm_smps_sw_mode(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1),(pm_sw_mode_type)SwMode);
  }
  else if (VregId > EFI_PM_SMPS_10 && VregId < EFI_PM_VREG_INVALID)   /*VS*/
  {
    errFlag = pm_vs_sw_mode(PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1), (pm_sw_mode_type)SwMode);
  }
  else
  {
    return EFI_DEVICE_ERROR;
  }

  if (errFlag != PM_ERR_FLAG__SUCCESS)
  {
    return EFI_DEVICE_ERROR;
  }

  return EFI_SUCCESS;
}

/**
  EFI_PmicVregMultiphaseCtrl ()

  @brief
  VregMultiphaseCtrl implementation of EFI_QCOM_PMIC_VREG_PROTOCOL
 */
EFI_STATUS
EFIAPI
EFI_PmicVregMultiphaseCtrl
(
  IN UINT32                  PmicDeviceIndex, 
  IN EFI_PM_VREG_ID_TYPE     VregId,
  IN UINT32                  NumberOfPhase
)
{
  (void)PmicDeviceIndex;
  (void)VregId;
  (void)NumberOfPhase;

  return EFI_UNSUPPORTED;

}


/**
  EFI_PmicVregGetStatus ()
  @brief
  EFI_PmicVregGetStatus implementation of EFI_QCOM_PMIC_VREG_PROTOCOL 
 */
EFI_STATUS
EFIAPI
EFI_PmicVregGetStatus
( 
  IN  UINT32                   PmicDeviceIndex, 
  IN  EFI_PM_VREG_ID_TYPE      VregId,
  OUT EFI_PM_VREG_STATUS_TYPE  *VregStatus
)
{
  EFI_STATUS Status = EFI_SUCCESS;
  pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;

  pm_on_off_type  PullDown = PM_INVALID;
  pm_on_off_type  PinCtrled  = PM_INVALID;
  pm_on_off_type  SwEnable  = PM_INVALID;
  pm_sw_mode_type SwMode = PM_SW_MODE_INVALID;
  boolean VregOk = FALSE;

  if (NULL == VregStatus)
  {
    Status = EFI_INVALID_PARAMETER;
  }
  else if(VregId < EFI_PM_SMPS_1) /*LDO*/
  {
    errFlag = pm_ldo_pull_down_status (PmicDeviceIndex, (uint8)VregId, &PullDown); 
    errFlag |= pm_ldo_sw_mode_status   (PmicDeviceIndex, (uint8)VregId, &SwMode); 
    errFlag |= pm_ldo_pin_ctrled_status(PmicDeviceIndex, (uint8)VregId, &PinCtrled); 
    errFlag |= pm_ldo_sw_enable_status (PmicDeviceIndex, (uint8)VregId, &SwEnable);
    errFlag |= pm_ldo_vreg_ok_status(PmicDeviceIndex, (uint8)VregId, &VregOk);
    }
  else if (VregId > EFI_PM_LDO_33 && VregId < EFI_PM_VS_LVS_1) /*SMPS*/
  {
    errFlag = pm_smps_pull_down_status (PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), &PullDown);
    errFlag |= pm_smps_sw_mode_status   (PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), &SwMode);
    errFlag |= pm_smps_pin_ctrled_status(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), &PinCtrled);
    errFlag |= pm_smps_sw_enable_status (PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), &SwEnable);
    errFlag |= pm_smps_vreg_ok_status(PmicDeviceIndex, (uint8)(VregId - EFI_PM_SMPS_1), &VregOk);
  }
  else if (VregId > EFI_PM_SMPS_10 && VregId < EFI_PM_VREG_INVALID)   /*VS*/
  {
    errFlag = pm_vs_pull_down_status (PmicDeviceIndex, (uint8) (VregId - EFI_PM_VS_LVS_1), &PullDown);
    errFlag |= pm_vs_sw_mode_status   (PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1), &SwMode);
    errFlag |= pm_vs_pin_ctrled_status(PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1), &PinCtrled); 
    errFlag |= pm_vs_sw_enable_status (PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1), &SwEnable); 
    errFlag |= pm_vs_vreg_ok_status(PmicDeviceIndex, (uint8)(VregId - EFI_PM_VS_LVS_1), &VregOk);
  }

  if (PM_ERR_FLAG__SUCCESS == errFlag && Status == EFI_SUCCESS)
  {
    VregStatus->PullDown  = (EFI_PM_ON_OFF_TYPE)PullDown;
    VregStatus->SwMode    = (EFI_PM_PWR_SW_MODE_TYPE) SwMode;
    VregStatus->PinCtrled = (EFI_PM_ON_OFF_TYPE)PinCtrled;
    VregStatus->SwEnable  = (EFI_PM_ON_OFF_TYPE)SwEnable;
    VregStatus->VregOk    = (BOOLEAN)VregOk;
  }
  else if (PM_ERR_FLAG__SUCCESS != errFlag)
  {
    Status = EFI_DEVICE_ERROR;
  }

  return Status;
}

/**
  PMIC VREG UEFI Protocol implementation
 */
EFI_QCOM_PMIC_VREG_PROTOCOL PmicVregProtocolImplementation = 
{
    PMIC_VREG_REVISION,
    EFI_PmicVregControl,
    EFI_PmicVregSetLevel,
    EFI_PmicVregGetLevel,
    EFI_PmicVregSetPwrMode,
    EFI_PmicVregMultiphaseCtrl,
    EFI_PmicVregGetStatus
};
