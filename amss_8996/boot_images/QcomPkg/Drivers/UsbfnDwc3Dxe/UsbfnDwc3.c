/**
 * @file UsbfnDwc3.c
 *
 * @brief USB Function Driver which provides the UsbfnIo Protocol
 *
 * Copyright (c) 2011,2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Portions Copyright (c) 2006 - 2012, Intel Corporation. All rights reserved.<BR>
 * This program and the accompanying materials
 * are licensed and made available under the terms and conditions of the BSD License
 * which accompanies this distribution.  The full text of the license may be found at
 * http://opensource.org/licenses/bsd-license.php
 *
 * THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
 * WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
 */
/*==============================================================================
 EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/25/15   vsb     USB LPM, HS/SS PHY config changes, USB mode toggle, clamp UsbFn to HS if SS PHY failed
 01/20/15   ck      Remove deprecated functions
 11/20/14   ml      Migrate to official SuperSpeed support
 12/18/14   ck      Address software & hardware transfer cancel notification issue
 11/19/14   ck      Add toggle USB mode support
 10/07/14   wufan   Add support of HS Test Mode for compliance testing 
 08/13/14   amitg   Clean Up Code
 06/06/14   ck      Add note for Boot Variable retesting
 05/20/14   ar      Add PCD value to override max bus speed 
 04/30/14   ar      Cleanup for NDEBUG build  
 04/25/14   amitg   Charger and Cable Detection Updates for MSM 8994 (Elessar)
 03/14/14   amitg   Updates for 8994
 08/23/13   mliss   Cleanup and stabilization
 01/20/11   cching  Initial rework for Synopsys Usbfn driver
 =============================================================================*/

//
// Includes
//
#include <Protocol/BlockIo.h>
#include <Protocol/EFICardInfo.h>

#include "UsbfnDwc3.h"
#include "UsbfnDwc3Ch9.h"
#include "UsbfnDwc3Impl.h"
#include "UsbfnDwc3Util.h"
#include "DeviceInfo.h"


extern EFI_GUID gQcomTokenSpaceGuid;
extern EFI_GUID gEfiEventEnterLPMGuid;

/*******************************************************************************
 * Static Variables
 ******************************************************************************/

STATIC
USBFN_DEV
*Usbfn =  NULL;

STATIC
CONST
UINT8
str_manuf[] = {
  'Q',0,
  'u',0,
  'a',0,
  'l',0,
  'c',0,
  'o',0,
  'm',0,
  'm',0,
  ' ',0,
  'T',0,
  'e',0,
  'c',0,
  'h',0,
  'n',0,
  'o',0,
  'l',0,
  'o',0,
  'g',0,
  'i',0,
  'e',0,
  's',0,
  ',',0,
  ' ',0,
  'I',0,
  'n',0,
  'c',0,
  '.',0,
};

STATIC
CONST
UINT8
str_product[] = {
  'Q',0,
  'U',0,
  'A',0,
  'L',0,
  'C',0,
  'O',0,
  'M',0,
  'M',0,
  ' ',0,
  'M',0,
  'S',0,
  'M',0,
  ' ',0,
  'D',0,
  'E',0,
  'V',0,
  'I',0,
  'C',0,
  'E',0,
};

#ifndef MDEPKG_NDEBUG
STATIC
CONST
CHAR8
*MsgToStr[] =
{
  "EfiUsbMsgNone",
  "EfiUsbMsgSetupPacket",
  "EfiUsbMsgEndpointStatusChangedRx",
  "EfiUsbMsgEndpointStatusChangedTx",
  "EfiUsbMsgBusEventDetach",
  "EfiUsbMsgBusEventAttach",
  "EfiUsbMsgBusEventReset",
  "EfiUsbMsgBusEventSuspend",
  "EfiUsbMsgBusEventResume",
  "EfiUsbMsgBusEventSpeed",
};
#endif // MDEPKG_NDEBUG


/**
 * Array to define the maximum packet size of an endpoint on the basis of
 * bus speed and endpoint type. Used in place of hard coded logic throughout.
 * The specific values are defined in the USB 2.0 and USB 3.0 specifications.
 * This array is indexed by the EFI_USB_ENDPOINT_TYPE and EFI_USB_BUS_SPEED
 * enums and thus must have its values defined in the order of these enums.
 *
 * Example: UsbEpMaxPacketSize[UsbEndpointBulk][UsbBusSpeedHigh] == 512
 */
CONST
UINTN
UsbEpMaxPacketSize[USBFN_NUM_EP_TYPES][USBFN_NUM_BUS_SPEEDS] = {
// unknown, low,    full,   high,   super
  {0,       8,      64,     64,     512},   // control
  {0,       0,      1023,   1024,   1024},  // isoch
  {0,       0,      64,     512,    1024},  // bulk
  {0,       8,      64,     1024,   1024}   // interrupt
};


EFI_DRIVER_BINDING_PROTOCOL
gUsbFnDriverBinding = {
  UsbFnDriverBindingSupported,
  UsbFnDriverBindingStart,
  UsbFnDriverBindingStop,
  0x30,
  NULL,
  NULL
};


/*******************************************************************************
 * Initialization / Shutdown
 ******************************************************************************/


/**
 * @brief Driver exit function that is called on gBS->ExitBootServices()
 *
 * Disconnects from host if a connection is still active and frees memory.
 *
 * @param  Event                   Pointer to this event
 * @param  Context                 Event hanlder private data
 */
VOID
EFIAPI
UsbfnExitBootService (
  EFI_EVENT  Event,
  VOID       *Context
  )
{
  USBFN_DEV   *Usbfn = NULL;
  UINT8       Ep, EpDir;

  FNC_ENTER_MSG ();

  if (NULL == Context) {
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = (USBFN_DEV *) Context;

  // Deinit controller and cancel pending URBs
  UsbfnDwcCoreShutdown(Usbfn);

  // Free the UrbPool and setup packet
  UsbfnDwcFreeXferRsc(Usbfn);

  // Free common layer buffers
  for (Ep = 0; Ep < USBFN_ENDPOINT_NUMBER_MAX; Ep++) {
    for (EpDir = 0; EpDir < USBFN_ENDPOINT_DIR_MAX; EpDir++) {
      if (NULL != Usbfn->TrbBuf[Ep][EpDir]) {
        UncachedFreeAlignedPool(Usbfn->TrbBuf[Ep][EpDir]);
      }
    }
  }
  if (NULL != Usbfn->EvtBuffer) {
    UncachedFreeAlignedPool(Usbfn->EvtBuffer);
  }

  // Free descriptors
  if (Usbfn->HijackData.RequestHijackingEnabled) {
    if (Usbfn->SSDeviceInfo) {
      FreeDeviceInfo(Usbfn->SSDeviceInfo);
    }
  }

  // Free hijacking buffers
  if (Usbfn->HijackData.HijackDataBuffer) {
    FreePool(Usbfn->HijackData.HijackDataBuffer);
  }
  if (Usbfn->HijackData.HijackStatusBuffer) {
    FreePool(Usbfn->HijackData.HijackStatusBuffer);
  }

  // Close the ExitBootServices event and free the device
  if (Usbfn->ExitBootServiceEvent) {
    gBS->CloseEvent(Usbfn->ExitBootServiceEvent);
    Usbfn->ExitBootServiceEvent = NULL;
  }    

  FreePool(Usbfn);

ON_EXIT:
  FNC_LEAVE_MSG ();
}


/**
 * @brief Callback for the EFI_LOADED_IMAGE_PROTOCOL
 *
 * Calls ExitBootServices handler to clean up.
 *
 * @param [in]  ImageHandle     Handle to driver image
 *
 * @retval EFI_SUCCESS          Successfully shut down
 * @retval Others               Failed to shut down properly
 */
EFI_STATUS
UsbfnUnload (
  IN EFI_HANDLE ImageHandle
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  FNC_ENTER_MSG ();

  UsbfnExitBootService(0, Usbfn);

  FNC_LEAVE_MSG ();
  return Status;
}


/**
 * @brief Create and initialize a USBFN_DEV instance
 *
 * @retval New instance on success
 * @retval NULL on failure
 */
STATIC
USBFN_DEV *
CreateUsbfnInstance (
  VOID
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  USBFN_DEV               *Usbfn = NULL;
  UINTN                   i = 0;
  EFI_USBFN_MESSAGE_NODE  *NewMsg;
  UINT8                   UefiVar;
  UINTN                   UefiVarSize;
  EFI_PLATFORMINFO_PROTOCOL *PlatformInfoProtocol;
  EFI_PLATFORMINFO_PLATFORM_INFO_TYPE PlatformInfo;

  FNC_ENTER_MSG ();

  // Allocate the software device structure
  Usbfn = (USBFN_DEV *) AllocateZeroPool(sizeof(USBFN_DEV));
  if (!Usbfn) {
    DBG (EFI_D_ERROR, "memory allocation failed for USB FN");
    goto ON_EXIT;
  }

  Usbfn->Signature = USBFN_DEV_SIGNATURE;

  // USBFN_IO protocol.
  Usbfn->UsbfnIo.Revision                   = EFI_USBFN_IO_PROTOCOL_REVISION;
  Usbfn->UsbfnIo.DetectPort                 = UsbfnDetectPort;
  Usbfn->UsbfnIo.ConfigureEnableEndpoints   = UsbfnConfigureEnableEndpoints;
  Usbfn->UsbfnIo.GetEndpointMaxPacketSize   = UsbfnGetEndpointMaxPacketSize;
  Usbfn->UsbfnIo.GetDeviceInfo              = UsbfnGetDeviceInfo;
  Usbfn->UsbfnIo.GetVendorIdProductId       = UsbfnGetVendorIdProductId;
  Usbfn->UsbfnIo.AbortTransfer              = UsbfnAbortTransfer;
  Usbfn->UsbfnIo.GetEndpointStallState      = UsbfnGetEndpointStallState;
  Usbfn->UsbfnIo.SetEndpointStallState      = UsbfnSetEndpointStallState;
  Usbfn->UsbfnIo.EventHandler               = UsbfnEventHandler;
  Usbfn->UsbfnIo.Transfer                   = UsbfnTransfer;
  Usbfn->UsbfnIo.GetMaxTransferSize         = UsbfnGetMaxTransferSize;
  Usbfn->UsbfnIo.AllocateTransferBuffer     = UsbfnAllocateTransferBuffer;
  Usbfn->UsbfnIo.FreeTransferBuffer         = UsbfnFreeTransferBuffer;
  Usbfn->UsbfnIo.StartController            = UsbfnStartController;
  Usbfn->UsbfnIo.StopController             = UsbfnStopController;
  Usbfn->UsbfnIo.SetEndpointPolicy          = UsbfnSetEndpointPolicy;
  Usbfn->UsbfnIo.GetEndpointPolicy          = UsbfnGetEndpointPolicy;
  Usbfn->UsbfnIo.ConfigureEnableEndpointsEx = UsbfnConfigureEnableEndpointsEx;

  Usbfn->PmicSmbchgProtocol  = NULL;
  Usbfn->UsbConfigProtocol = NULL;

  // Set default pipe policy
  Usbfn->PipePolicy.ZeroLengthTerminationSupport = TRUE;
  for(i = 0; i < USBFN_ENDPOINT_NUMBER_MAX; ++i){
    Usbfn->PipePolicy.ZLT[i][EfiUsbEndpointDirectionDeviceTx] = FALSE;
    Usbfn->PipePolicy.ZLT[i][EfiUsbEndpointDirectionDeviceRx] = FALSE;
    Usbfn->PipePolicy.MaxTransferSize[i][EfiUsbEndpointDirectionDeviceTx] = USBFN_MAX_TRANSFER_SIZE;
    Usbfn->PipePolicy.MaxTransferSize[i][EfiUsbEndpointDirectionDeviceRx] = USBFN_MAX_TRANSFER_SIZE;
  }

  // Initialize device state
  Usbfn->CoreId          = 0;
  Usbfn->Enabled         = FALSE;
  Usbfn->IsAttached      = FALSE;
  Usbfn->IsSuspended     = FALSE;
  Usbfn->Speed           = UsbBusSpeedUnknown;
  Usbfn->Address         = 0;
  Usbfn->ConfigValue     = 0;
  Usbfn->MaxCurrent      = 0;
  Usbfn->ChgPortType     = EFI_PM_SMBCHG_CHG_PORT_INVALID;
  Usbfn->Disconnecting   = FALSE;
  Usbfn->HsTestMode      = USB_TEST_DISABLE;
  Usbfn->StoreSELData    = FALSE;
  Usbfn->MaxSpeed = DWC_DEV_SPEED_DEFAULT;

  // This will be set to FALSE when a ConnectDone event arrives. It
  // must be TRUE until then because a Disconnect event will not be
  // posted unless the connect happens and we would wait forever.
  Usbfn->DisconnectEvtPosted = TRUE;


  // Initialize the client messages
  InitializeListHead(&Usbfn->UsbfnMessages);
  InitializeListHead(&Usbfn->UsbfnMessagePool);

  for (i = 0; i < USBFN_MESSAGE_NODE_COUNT; ++i) {
    NewMsg = (EFI_USBFN_MESSAGE_NODE *)AllocateZeroPool(sizeof(EFI_USBFN_MESSAGE_NODE));
    InsertTailList(&Usbfn->UsbfnMessagePool, &NewMsg->Link);
  }

  // Fetch the platform info
  Status = gBS->LocateProtocol(&gEfiPlatformInfoProtocolGuid, NULL,
      (VOID **) &PlatformInfoProtocol);
  ERR_CHK ("Failed to get gEfiPlatformInfoProtocolGuid");

  Status = PlatformInfoProtocol->GetPlatformInfo(PlatformInfoProtocol,
      &PlatformInfo);
  ERR_CHK ("Failed to get PlatformInfo");

  Usbfn->PlatType = PlatformInfo;

  Usbfn->SetupPkt     = NULL;
      
  // Read UEFI variable to check for request hijacking
  UefiVarSize = sizeof(UefiVar);
  Status = gRT->GetVariable(L"UsbfnHijackRequests", &gQcomTokenSpaceGuid, NULL, &UefiVarSize, &UefiVar);
  if (EFI_ERROR(Status)) {
    UefiVar = 0;
  }

  // Need to retest boot variable setting.
  if (UefiVar) {
    DBG(EFI_D_WARN, "Request hijacking enabled");
    Usbfn->HijackData.RequestHijackingEnabled = TRUE;
  } else {
    Usbfn->HijackData.RequestHijackingEnabled = FALSE;
  }
  Usbfn->HijackData.HijackThisRequest    = FALSE;
  Usbfn->HijackData.HijackDescType       = USB_DESC_TYPE_UNDEFINED;
  Usbfn->HijackData.HijackedBuffer       = NULL;
  Usbfn->HijackData.HijackedBufferLength = 0;
  Usbfn->HijackData.HijackNextRxEvent    = FALSE;
  Usbfn->HijackData.HijackNextTxEvent    = FALSE;
  Usbfn->HijackData.HijackDataBuffer     = AllocateZeroPool(512); // max pkt size for ep 0
  Usbfn->HijackData.HijackStatusBuffer   = AllocateZeroPool(512);

  Status = EFI_SUCCESS;

ON_EXIT:
  if (EFI_ERROR (Status)) {
    Usbfn = NULL;
  }

  FNC_LEAVE_MSG ();
  return Usbfn;
}


/**
  Entry point for EFI drivers.

  @param  ImageHandle       EFI_HANDLE.
  @param  SystemTable       EFI_SYSTEM_TABLE.

  @return EFI_SUCCESS       Success.
          EFI_DEVICE_ERROR  Fail.

**/
EFI_STATUS
EFIAPI
UsbfnDwc3DriverEntryPoint (
  IN EFI_HANDLE           ImageHandle,
  IN EFI_SYSTEM_TABLE     *SystemTable
  )
{
  return EfiLibInstallDriverBindingComponentName2 (
           ImageHandle,
           SystemTable,
           &gUsbFnDriverBinding,
           ImageHandle,
           NULL,
           NULL
           );
}



/**
  Test to see if this driver supports ControllerHandle. Any
  ControllerHandle that has UsbConfigProtocol installed will
  be supported.

  @param  This                 Protocol instance pointer.
  @param  Controller           Handle of device to test.
  @param  RemainingDevicePath  Not used.

  @return EFI_SUCCESS          This driver supports this device.
  @return EFI_UNSUPPORTED      This driver does not support this device.

**/
EFI_STATUS
EFIAPI
UsbFnDriverBindingSupported (
  IN EFI_DRIVER_BINDING_PROTOCOL *This,
  IN EFI_HANDLE                  Controller,
  IN EFI_DEVICE_PATH_PROTOCOL    *RemainingDevicePath
  )
{

  QCOM_USB_CONFIG_PROTOCOL *pUsbConfigProtocol;
  EFI_STATUS              Status;

  // Check if UsbConfig protocol is installed
  Status = gBS->OpenProtocol (
                  Controller,
                  &gQcomUsbConfigProtocolGuid,
                  (VOID **) &pUsbConfigProtocol,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );
  if (EFI_ERROR (Status))
  {
    return Status;
  }

  //check if controller is for a valid core that supports device USB2.0 mode
  if ( (pUsbConfigProtocol->coreNum < USB_CORE_MAX_NUM) && (pUsbConfigProtocol->modeType == USB_DEVICE_MODE_SS) )
  {
    DEBUG(( EFI_D_INFO, "UsbFnDriverBindingSupported: This is core#%d, CoreType = (0x%x)\r\n",
            pUsbConfigProtocol->coreNum, pUsbConfigProtocol->modeType ));
    Status = EFI_SUCCESS;
  }
  else
  {
    Status = EFI_UNSUPPORTED;
  }

  // Close the USB_CONFIG used to perform the supported test
  gBS->CloseProtocol (
         Controller,
         &gQcomUsbConfigProtocolGuid,
         This->DriverBindingHandle,
         Controller
         );

  return Status;
}


EFI_STATUS
EFIAPI
UsbFnDriverBindingStart (
  IN EFI_DRIVER_BINDING_PROTOCOL *This,
  IN EFI_HANDLE                  Controller,
  IN EFI_DEVICE_PATH_PROTOCOL    *RemainingDevicePath
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_LOADED_IMAGE_PROTOCOL *LoadedImageProtocol = NULL;

  FNC_ENTER_MSG ();

  Usbfn = CreateUsbfnInstance();
  if (!Usbfn) {
    Status = EFI_OUT_OF_RESOURCES;
    DBG (EFI_D_ERROR, "unable to create USB FN");
    goto ON_EXIT;
  }

  //Open the Plat Config protocol.
  Status = gBS->OpenProtocol (
                  Controller,
                  &gQcomUsbConfigProtocolGuid,
                  (VOID **) &Usbfn->UsbConfigProtocol,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_BY_DRIVER
                  );
  if (EFI_ERROR (Status)) {
     DEBUG(( EFI_D_ERROR,
            "UsbFnDriverBindingStart: Unable to open USB Config Protocol, Status =  (0x%x)\r\n", Status));
    return Status;
  }

  DEBUG(( EFI_D_INFO, "UsbFnDriverBindingStart: This is core#%d, CoreType = (0x%x)\r\n",
          Usbfn->UsbConfigProtocol->coreNum, Usbfn->UsbConfigProtocol->modeType ));

  Status = gBS->OpenProtocol (
                  Controller,
                  &gEfiLoadedImageProtocolGuid,
                  (VOID **)&LoadedImageProtocol,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );
  WRN_CHK ("failed to open loaded image protocol");

  if (LoadedImageProtocol) {
    LoadedImageProtocol->Unload = UsbfnUnload;
  }

  // Open PMIC SMBB protocol for cable detection
  Status = gBS->LocateProtocol(&gQcomPmicSmbchgProtocolGuid, NULL,
      (VOID**)&Usbfn->PmicSmbchgProtocol);
  WRN_CHK("Unable to open PMIC SMBB Protocol: %r", Status);

  //save core type
  Status = Usbfn->UsbConfigProtocol->GetSSUsbFnConfig(Usbfn->UsbConfigProtocol,
      &Usbfn->CoreType);
  if (EFI_ERROR(Status)) {
    DBG(EFI_D_ERROR,"Usbfn->Unable to get SSUSB FN config\r\n");
    goto ON_EXIT;
  }

  // Get base address
  Status = Usbfn->UsbConfigProtocol->GetCoreBaseAddr(Usbfn->UsbConfigProtocol,
      Usbfn->CoreType, &Usbfn->Usb30WrapperBase);
  ERR_CHK("failed to get USB core base address");
  DBG(EFI_D_INFO,"Usbfn->Usb30WrapperBase = 0x%x\r\n",Usbfn->Usb30WrapperBase);

  //initialize USB core
  Status = Usbfn->UsbConfigProtocol->ConfigUsb(Usbfn->UsbConfigProtocol, USB_DEVICE_MODE_SS, Usbfn->UsbConfigProtocol->coreNum);
  ERR_CHK ("failed to perform platform configuration");

  // Install UsbfnIo protocol.
  Status = gBS->InstallMultipleProtocolInterfaces(&Controller,
      &gEfiUsbfnIoProtocolGuid, &Usbfn->UsbfnIo, NULL);
  ERR_CHK ("unable to install USBFN protocols");

  // Register for boot services exit event.
  Status = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, TPL_NOTIFY,
      UsbfnExitBootService, Usbfn, &gEfiEventExitBootServicesGuid,
      &Usbfn->ExitBootServiceEvent);

  if (EFI_ERROR (Status)) {
    DBG(EFI_D_ERROR,"UsbFnDriverBindingStart: ExitBootServiceEvent creation failed %r", Status);
    Usbfn->ExitBootServiceEvent = NULL;
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



EFI_STATUS
EFIAPI
UsbFnDriverBindingStop (
  IN EFI_DRIVER_BINDING_PROTOCOL *This,
  IN EFI_HANDLE                  Controller,
  IN UINTN                       NumberOfChildren,
  IN EFI_HANDLE                  *ChildHandleBuffer
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_USBFN_IO_PROTOCOL *UsbfnIo;

  FNC_ENTER_MSG ();

  //
  // Test whether the Controller handler passed in is a valid
  // Usb controller handle that should be supported, if not,
  // return the error status directly
  //
  Status = gBS->OpenProtocol (
                  Controller,
                  &gEfiUsbfnIoProtocolGuid,
                  (VOID **) &UsbfnIo,
                  This->DriverBindingHandle,
                  Controller,
                  EFI_OPEN_PROTOCOL_GET_PROTOCOL
                  );

  if (EFI_ERROR (Status)) {
    return Status;
  }

  Status = gBS->UninstallProtocolInterface (
                  Controller,
                  &gEfiUsbfnIoProtocolGuid,
                  UsbfnIo
                  );

  if (EFI_ERROR (Status)) {
    DEBUG(( EFI_D_ERROR,
            "UsbFnDriverBindingStop: failed to uninstall gEfiUsbfnIoProtocolGuid. Status =  (0x%x)\r\n", Status));
    return Status;
  }

  //stop all xfers and stop controller
  UsbfnExitBootService(0, Usbfn);

  //close UsbConfig handle on controller DriverBinding handle
  Status = gBS->CloseProtocol (
         Controller,
         &gQcomUsbConfigProtocolGuid,
         This->DriverBindingHandle,
         Controller
         );

  if (EFI_ERROR (Status)) {
      DEBUG(( EFI_D_ERROR,
          "UsbFnDriverBindingStop: failed to close gQcomUsbConfigProtocolGuid. Status =  (0x%x)\r\n", Status));
  }

  Status = gBS->CloseProtocol (
                  Controller,
                  &gEfiLoadedImageProtocolGuid,
                  This->DriverBindingHandle,
                  Controller
                  );
  WRN_CHK ("failed to close loaded image protocol. Status = %r", Status);
  

  // Due to a known bug, closing DriverBindingHandle will fail with 
  // EFI_NOT_FOUNC. Ignore the error so the usbfn can be stopped. 
  Status = EFI_SUCCESS;
  Usbfn  = NULL;

  FNC_LEAVE_MSG ();
  return Status;
}


/*******************************************************************************
 * Generate UsbFnIO protocol messages for the client
 ******************************************************************************/


/*
 * See header for documentation.
 */
EFI_STATUS
UsbfnSetupDevCb (
  IN USBFN_DEV                  *Usbfn,
  IN EFI_USB_DEVICE_REQUEST     *Req
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  EFI_USBFN_MESSAGE_NODE  *NewMsg = NULL;
  LIST_ENTRY              *ListEntry = NULL;
  UINT8                   DescType;
  VOID                    *Buffer;
  UINTN                   BufferSize;

  FNC_ENTER_MSG ();

  USB_ASSERT_RETURN((Usbfn && Req), EFI_INVALID_PARAMETER);

  if (Usbfn->Disconnecting) {
    DBG(EFI_D_INFO, "Disconnecting, not sending client notification");
    goto ON_EXIT;
  }

  // handle request hijacking
  if ( Usbfn->HsTestMode != USB_TEST_DISABLE ||
       ( Usbfn->HijackData.RequestHijackingEnabled &&
         Usbfn->Speed == UsbBusSpeedSuper) ) {

    // hijack descriptors
    if (Req->Request == USB_REQ_GET_DESCRIPTOR) {
      DescType = Req->Value >> 8 & 0xFF;

      // hijack device and config descriptors on return trip from client
      if (DescType == USB_DESC_TYPE_DEVICE ||
          DescType == USB_DESC_TYPE_CONFIG) {
        Usbfn->HijackData.HijackThisRequest = TRUE;
        Usbfn->HijackData.HijackDescType = DescType;
      }

      // don't let client know about BOS descriptor request
      if (DescType == USB_DESC_TYPE_BOS) {
        DBG(EFI_D_WARN, "Hijacking BOS Descriptor");

        // queue BOS data stage
        Buffer     = Usbfn->SSDeviceInfo->BosDescriptor;
        BufferSize = Usbfn->SSDeviceInfo->BosDescriptor->TotalLength;

        Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_TX, BufferSize, Buffer, NULL);
        ERR_CHK("failed to queue BOS descriptor data stage");

        // queue BOS status stage
        Buffer     = Usbfn->HijackData.HijackStatusBuffer;
        BufferSize = 0;

        Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_RX, BufferSize, Buffer, NULL);
        ERR_CHK("failed to queue BOS descriptor status stage");

        // hijack the completion events
        Usbfn->HijackData.HijackNextRxEvent = TRUE;
        Usbfn->HijackData.HijackNextTxEvent = TRUE;
        goto ON_EXIT;
      }
    }

    // hijack SET_ISOCH_DELAY
    if (Req->Request == USB_REQ_SET_ISOCH_DELAY) {
      DBG(EFI_D_WARN, "Hijacking SET_ISOCH_DELAY request");

      // queue status stage
      Buffer     = Usbfn->HijackData.HijackStatusBuffer;
      BufferSize = 0;

      Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_TX, BufferSize, Buffer, NULL);
      ERR_CHK("failed to queue SET_ISOCH_DELAY status stage");

      // hijack the completion events
      Usbfn->HijackData.HijackNextTxEvent = TRUE;
      goto ON_EXIT;
    }

    // hijack SET_SEL
    if (Req->Request == USB_REQ_SET_SEL) {
      DBG(EFI_D_WARN, "Hijacking SET_SEL request");

      // queue data to recieve SEL values
      Buffer     = Usbfn->HijackData.HijackDataBuffer;
      BufferSize = 6;

      Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_RX, BufferSize, Buffer, NULL);
      ERR_CHK("failed to queue SET_SEL data stage");

      // queue status stage
      Buffer     = Usbfn->HijackData.HijackStatusBuffer;
      BufferSize = 0;

      Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_TX, BufferSize, Buffer, NULL);
      ERR_CHK("failed to queue SET_SEL status stage");

      // hijack the completion events
      Usbfn->HijackData.HijackNextRxEvent = TRUE;
      Usbfn->HijackData.HijackNextTxEvent = TRUE;
      goto ON_EXIT;
    }

    // hijack SET_FEATURE (SS only features)
    if (Req->Request == USB_REQ_SET_FEATURE) {
      switch (Req->Value) {
        case USB_FEATURE_U1_ENABLE:
        case USB_FEATURE_U2_ENABLE:
        case USB_FEATURE_LTM_ENABLE:
        case USB_FEATURE_HS_TEST_MODE:

          DBG(EFI_D_WARN, "Hijacking SET_FEATURE 0x%x request", Req->Value);

          // queue status stage
          Buffer     = Usbfn->HijackData.HijackStatusBuffer;
          BufferSize = 0;

          Status = UsbfnDwcTransfer(Usbfn, 0, DWC_EP_DIR_TX, BufferSize, Buffer, NULL);
          ERR_CHK("failed to queue SET_FEATURE status stage");

          // hijack the completion event
          Usbfn->HijackData.HijackNextTxEvent = TRUE;
          goto ON_EXIT;

        default:
          DBG(EFI_D_INFO, "NOT Hijacking SET_FEATURE 0x%x request", Req->Value);
          break;
      }
    }
  }

  //
  // Get a free Message from the memory pool
  //
  ListEntry = GetFirstNode(&Usbfn->UsbfnMessagePool);
  USB_ASSERT_RETURN(ListEntry, EFI_DEVICE_ERROR);
  RemoveEntryList(ListEntry);
  NewMsg = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);

  NewMsg->Message = EfiUsbMsgSetupPacket;
  CopyMem (&(NewMsg->Data.SetupCbRequest), Req, sizeof (NewMsg->Data.SetupCbRequest));
  InsertTailList(&Usbfn->UsbfnMessages, &NewMsg->Link);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
UsbfnSetupIfcCb (
  IN USBFN_DEV                  *Usbfn,
  IN EFI_USB_DEVICE_REQUEST     *Req
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  EFI_USBFN_MESSAGE_NODE* NewMsg    = NULL;
  LIST_ENTRY*             ListEntry = NULL;

  FNC_ENTER_MSG ();

  USB_ASSERT_RETURN((Usbfn && Req), EFI_INVALID_PARAMETER);

  if (Usbfn->Disconnecting) {
    DBG(EFI_D_INFO, "Disconnecting, not sending client notification");
    goto ON_EXIT;
  }

  //
  // Get a free Message from the memory pool
  //
  ListEntry = GetFirstNode(&Usbfn->UsbfnMessagePool);
  USB_ASSERT_RETURN(ListEntry, EFI_DEVICE_ERROR);
  RemoveEntryList(ListEntry);
  NewMsg = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);

  NewMsg->Message = EfiUsbMsgSetupPacket;
  CopyMem (&(NewMsg->Data.SetupCbRequest), Req, sizeof (NewMsg->Data.SetupCbRequest));
  InsertTailList(&Usbfn->UsbfnMessages, &NewMsg->Link);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
VOID
UsbfnXtachCb (
  IN USBFN_DEV                  *Usbfn,
  IN BOOLEAN                    IsAttached
  )
{
  EFI_USBFN_MESSAGE_NODE* NewMsg    = NULL;
  LIST_ENTRY*             ListEntry = NULL;

  FNC_ENTER_MSG ();

  USB_ASSERT(Usbfn);

  //
  // Get a free Message from the memory pool
  //
  ListEntry = GetFirstNode(&Usbfn->UsbfnMessagePool);
  USB_ASSERT(ListEntry);
  RemoveEntryList(ListEntry);
  NewMsg = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);

  NewMsg->Message = IsAttached
                  ? EfiUsbMsgBusEventAttach
                  : EfiUsbMsgBusEventDetach;
  InsertTailList(&Usbfn->UsbfnMessages, &NewMsg->Link);

  FNC_LEAVE_MSG ();
}


/*
 * See header for documentation.
 */
VOID
UsbfnBusEventCb (
  IN USBFN_DEV                  *Usbfn,
  IN EFI_USBFN_MESSAGE          Message
  )
{
  EFI_USBFN_MESSAGE_NODE* NewMsg    = NULL;
  LIST_ENTRY*             ListEntry = NULL;

  FNC_ENTER_MSG ();

  USB_ASSERT (Usbfn);

  if (Usbfn->Disconnecting) {
    DBG(EFI_D_INFO, "Disconnecting, not sending client notification");
    goto ON_EXIT;
  }

  //
  // Get a free Message from the memory pool
  //
  ListEntry = GetFirstNode(&Usbfn->UsbfnMessagePool);
  USB_ASSERT(ListEntry);
  RemoveEntryList(ListEntry);
  NewMsg = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);

  NewMsg->Message = Message;
  InsertTailList(&Usbfn->UsbfnMessages, &NewMsg->Link);

ON_EXIT:
  FNC_LEAVE_MSG ();
}


/*
 * See header for documentation.
 */
VOID
UsbfnSpeedCb (
  IN USBFN_DEV                  *Usbfn,
  IN EFI_USB_BUS_SPEED          Speed
  )
{

  EFI_USBFN_MESSAGE_NODE* NewMsg    = NULL;
  LIST_ENTRY*             ListEntry = NULL;

  FNC_ENTER_MSG ();

  USB_ASSERT (Usbfn);

  if (Usbfn->Disconnecting) {
    DBG(EFI_D_INFO, "Disconnecting, not sending client notification");
    goto ON_EXIT;
  }

  //
  // Get a free Message from the memory pool
  //
  ListEntry = GetFirstNode(&Usbfn->UsbfnMessagePool);
  USB_ASSERT(ListEntry);
  RemoveEntryList(ListEntry);
  NewMsg = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);


  NewMsg->Message = EfiUsbMsgBusEventSpeed;
  NewMsg->Data.SpeedCbSpeed = Speed;
  InsertTailList(&Usbfn->UsbfnMessages, &NewMsg->Link);

ON_EXIT:
  FNC_LEAVE_MSG ();
}


/*******************************************************************************
 * Protocol Implementation
 ******************************************************************************/


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnDetectPort (
  IN  EFI_USBFN_IO_PROTOCOL  *This,
  OUT EFI_USBFN_PORT_TYPE    *PortType
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == PortType) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  if (!Usbfn->IsAttached) {
    Status = EFI_NOT_READY;
    *PortType = EfiUsbUnknownPort;
    goto ON_EXIT;
  }

  // Convert to USBFN_IO port type
  switch (Usbfn->ChgPortType) {
    case EFI_PM_SMBCHG_CHG_PORT_SDP:
      *PortType = EfiUsbStandardDownstreamPort;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_CDP:
      *PortType = EfiUsbChargingDownstreamPort;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_DCP:
      *PortType = EfiUsbDedicatedChargingPort;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_OTHER:
      *PortType = EfiUsbInvalidDedicatedChargingPort;
      break;
    case EFI_PM_SMBCHG_CHG_PORT_INVALID:
    default:
      Status = EFI_DEVICE_ERROR;
      *PortType = EfiUsbUnknownPort;
      break;
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnConfigureEnableEndpoints (
  IN EFI_USBFN_IO_PROTOCOL      *This,
  IN EFI_USB_DEVICE_INFO        *DeviceInfo
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == DeviceInfo) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);

  // Disconnect first if we are already attached
  UsbfnDwcCoreShutdown(Usbfn);

  // Save client's descriptor set
  Usbfn->DeviceInfo = DeviceInfo;

  // Update descriptor set only when hijacking enabled
  if (Usbfn->HijackData.RequestHijackingEnabled) {
    if (Usbfn->SSDeviceInfo) {
      FreeDeviceInfo(Usbfn->SSDeviceInfo);
      Usbfn->SSDeviceInfo = NULL;
    }

    // Upgrade the descriptor set to USB 3.0
    Status = UpdateDeviceInfo(Usbfn, DeviceInfo, &Usbfn->SSDeviceInfo);
    ERR_CHK ("failed to update device info structures");
  } else {
    Usbfn->SSDeviceInfo = NULL;
  }

  // validate descriptors
  Status = ValidateDeviceInfo(Usbfn->DeviceInfo, Usbfn->SSDeviceInfo);
  if (EFI_ERROR(Status)) {
    DBG(EFI_D_ERROR, "invalid DeviceInfo");
    goto ON_EXIT;
  }

  // Iniialize the core
  UsbfnDwcCoreInit(Usbfn);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnConfigureEnableEndpointsEx (
  IN EFI_USBFN_IO_PROTOCOL           *This,
  IN EFI_USB_DEVICE_INFO             *DeviceInfo,
  IN EFI_USB_SUPERSPEED_DEVICE_INFO  *SSDeviceInfo
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == DeviceInfo || NULL == SSDeviceInfo) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS(This);
  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);

  // Disconnect first if we are already attached
  UsbfnDwcCoreShutdown(Usbfn);

  // Save client's descriptor sets
  Usbfn->DeviceInfo = DeviceInfo;
  Usbfn->SSDeviceInfo = SSDeviceInfo;

  // validate descriptors
  Status = ValidateDeviceInfo(Usbfn->DeviceInfo, Usbfn->SSDeviceInfo);
  if (EFI_ERROR(Status)) {
    DBG(EFI_D_ERROR, "invalid DeviceInfo");
    goto ON_EXIT;
  }

  // Iniialize the core
  UsbfnDwcCoreInit(Usbfn);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetEndpointMaxPacketSize (
  IN  EFI_USBFN_IO_PROTOCOL     *This,
  IN  EFI_USB_ENDPOINT_TYPE     EndpointType,
  IN  EFI_USB_BUS_SPEED         BusSpeed,
  OUT UINT16                    *MaxPacketSize
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == MaxPacketSize ||
      (EndpointType > UsbEndpointInterrupt) || (BusSpeed > UsbBusSpeedSuper)) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  *MaxPacketSize = UsbEpMaxPacketSize[EndpointType][BusSpeed];

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  A UUID is a 16-octet (128-bit) number.
  In its canonical form, a UUID is represented by 32 hexadecimal digits,
  displayed in five groups separated by hyphens, in the form 8-4-4-4-12 for a
  total of 36 characters (32 alphanumeric characters and four hyphens).
  For example:
  550e8400-e29b-41d4-a716-446655440000

  @param [out] Dest             Destination buffer for UUID in canonical form
  @param [in]  Src              Source UUID in binary form
**/
STATIC
VOID
GetUUIDCanonicalForm(
  OUT CHAR16                    *Dest,
  IN  UINT8                     *Src
  )
{
  CHAR16 hexArray[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  CHAR16 UUIDHexString[32] = {0};
  CHAR16 *pUUIDHexString = &UUIDHexString[0];
  UINT8  numDigPerGroup[5] = {8,4,4,4,12};
  UINT8  i;
  UINT8  v;
  UINT8  numGroup = 5;

  for (i = 0; i < EMMC_UUID_OCTET_SIZE; i++) {
    v = Src[i] & 0xFF;
    UUIDHexString[i * 2]     = hexArray[v >> 4];
    UUIDHexString[i * 2 + 1] = hexArray[v & 0x0F];
  }

  for (i = 0; i < numGroup; i++) {
    CopyMem(Dest, pUUIDHexString, numDigPerGroup[i] * sizeof(CHAR16));
    Dest += numDigPerGroup[i];
    if (i < numGroup - 1) {
      *Dest = '-';
      Dest++;
    }
    pUUIDHexString += numDigPerGroup[i];
  }
}


/**
  Get the USB serial number.

  The serial number is based on the unique serial number from the non-removable emmc.

  @param  BufferSize            On input, the size of the user buffer.
                                On output, the size of the serial number in bytes
  @param  Buffer                The buffer provided by the user

  @retval EFI_SUCCESS           USB serial number is successfully obtained.
  @retval other                 Operation failed
**/
STATIC
EFI_STATUS
GetUsbSerialNumber (
  IN OUT UINTN                  *BufferSize,
  OUT    VOID                   *Buffer
  )
{
  EFI_STATUS                  Status = EFI_SUCCESS;
  EFI_BLOCK_IO_PROTOCOL       *BlkIo;
  EFI_HANDLE                  *SdccHandles;
  UINTN                       NumberOfSdccHandles;
  UINTN                       i, j;
  EFI_SDCC_CARDINFO_PROTOCOL  *CardInfo;
  SDCC_CARD_INFO              CardInfoData;
  UINT8                       UUID[EMMC_UUID_OCTET_SIZE];

  if (*BufferSize < EMMC_UUID_CANONICAL_FORM_SIZE_IN_BYTES) {
    Status = EFI_BUFFER_TOO_SMALL;
    *BufferSize = EMMC_UUID_CANONICAL_FORM_SIZE_IN_BYTES;
    DEBUG((EFI_D_ERROR,"Buffer size is too small"));
    goto ON_EXIT;
  }

  Status = gBS->LocateHandleBuffer (ByProtocol,
                                    &gEfiBlockIoProtocolGuid,
                                    NULL,
                                    &NumberOfSdccHandles,
                                    &SdccHandles);
  if (EFI_ERROR(Status)) {
    DEBUG((EFI_D_ERROR,"Failed to get BlkIo handles"));
    return Status;
  }

  /* Loop through to search for the ones we are interested in,
   * Non removable media. */
  for (i = 0; i < NumberOfSdccHandles; i++) {

    Status = gBS->HandleProtocol (SdccHandles[i],
                                  &gEfiBlockIoProtocolGuid,
                                  (VOID **) &BlkIo);
    if (EFI_ERROR(Status)) {
      continue;
    }
    if (BlkIo->Media->RemovableMedia) {
      continue;
    }
    Status = gBS->HandleProtocol (SdccHandles[i],
                                  &gEfiSdccCardInfoProtocolGuid,
                                  (VOID**)&CardInfo);
    if (EFI_ERROR(Status)) {
      continue;
    }
    if (CardInfo->GetCardInfo (CardInfo, &CardInfoData) == EFI_SUCCESS) {
      // copy the data to UUID
      CopyMem(UUID, &CardInfoData, EMMC_UUID_OCTET_SIZE);
      *BufferSize = EMMC_UUID_CANONICAL_FORM_SIZE_IN_BYTES;
      GetUUIDCanonicalForm(Buffer, UUID);
      for (j = 0; j < EMMC_UUID_CANONICAL_FORM_SIZE_IN_BYTES / 2; j++) {
        DBG(EFI_D_INFO,"Buffer[%d] = 0x%x",j, ((CHAR16*)Buffer)[j]);
      }
      break;
    }
  }

ON_EXIT:
  return Status;
}


/**
  Get the USB Manufacturer Name.

  @param  BufferSize            On input, the size of the user buffer.
                                On output, the size of the manufacturer name in bytes
  @param  Buffer                The buffer provided by the user

  @retval EFI_SUCCESS           USB Manufacturer Name is successfully obtained.
  @retval other                 Operation failed
**/
STATIC
EFI_STATUS
GetUsbManufacturerName (
  IN OUT UINTN                  *BufferSize,
  OUT    VOID                   *Buffer)
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  if (*BufferSize < sizeof(str_manuf)) {
    Status = EFI_BUFFER_TOO_SMALL;
    DEBUG((EFI_D_ERROR,"Buffer size is too small"));
    goto ON_EXIT;
  }

  // Copy the string.
  CopyMem(Buffer, str_manuf, sizeof(str_manuf));

ON_EXIT:
  *BufferSize = sizeof(str_manuf);
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  Get the USB Product String

  @param  BufferSize            On input, the size of the user buffer.
                                On output, the size of the product string in bytes
  @param  Buffer                The buffer provided by the user

  @retval EFI_SUCCESS           USB Product String is successfully obtained.
  @retval other                 Operation failed
**/
STATIC
EFI_STATUS
GetUsbProductString (
  IN OUT UINTN                  *BufferSize,
  OUT    VOID                   *Buffer)
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  if(*BufferSize < sizeof(str_product)){
    Status = EFI_BUFFER_TOO_SMALL;
    DEBUG((EFI_D_ERROR,"Buffer size is too small"));
    goto ON_EXIT;
  }

  // Copy the string.
  CopyMem(Buffer, str_product, sizeof(str_product));

ON_EXIT:
  *BufferSize = sizeof(str_product);
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetDeviceInfo (
  IN     EFI_USBFN_IO_PROTOCOL      *This,
  IN     EFI_USBFN_DEVICE_INFO_ID   Id,
  IN OUT UINTN                      *BufferSize,
  OUT    VOID                       *Buffer
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == Buffer ||
      NULL == BufferSize || *BufferSize == 0) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  // Decode request into source string and source string size.
  switch (Id) {
  case EfiUsbDeviceInfoSerialNumber:
    Status = GetUsbSerialNumber(BufferSize, Buffer);
    break;
  case EfiUsbDeviceInfoManufacturerName:
    Status = GetUsbManufacturerName(BufferSize, Buffer);
    break;
  case EfiUsbDeviceInfoProductName:
    Status = GetUsbProductString(BufferSize, Buffer);
    break;
  case EfiUsbDeviceInfoUnknown:
  default:
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    break;
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetVendorIdProductId (
  IN  EFI_USBFN_IO_PROTOCOL  *This,
  OUT UINT16                 *Vid,
  OUT UINT16                 *Pid
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if (NULL == This || NULL == Vid || NULL == Pid) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);
  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);

  if (!Usbfn->DeviceInfo->DeviceDescriptor) {
    Status = EFI_NOT_FOUND;
    goto ON_EXIT;
  }

  *Vid = Usbfn->DeviceInfo->DeviceDescriptor->IdVendor;
  *Pid = Usbfn->DeviceInfo->DeviceDescriptor->IdProduct;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnAbortTransfer (
  IN EFI_USBFN_IO_PROTOCOL         *This,
  IN UINT8                         EndpointIndex,
  IN EFI_USBFN_ENDPOINT_DIRECTION  Direction
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if ((NULL == This) || (EndpointIndex >= USBFN_ENDPOINT_NUMBER_MAX) ||
      ((UINTN) Direction >= USBFN_ENDPOINT_DIR_MAX)) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  DBG(EFI_D_INFO,"EndpointIndex = %d, EFI_USBFN_ENDPOINT_DIRECTION %d",EndpointIndex,Direction);

  // Abort transfer.
  Status = UsbfnDwcCancelTransfer(Usbfn, EndpointIndex, Direction);

ON_EXIT:
    FNC_LEAVE_MSG ();
    return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetEndpointStallState (
  IN  EFI_USBFN_IO_PROTOCOL         *This,
  IN  UINT8                         EndpointIndex,
  IN  EFI_USBFN_ENDPOINT_DIRECTION  Direction,
  OUT BOOLEAN                       *State
  )
{
  EFI_STATUS    Status = EFI_SUCCESS;
  DWC_STATUS    DwcStatus = DWC_SUCCESS;
  USBFN_DEV     *Usbfn = NULL;
  DWC_EP_STATE  EpState;

  FNC_ENTER_MSG ();
  if (NULL == This || NULL == State) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);
  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);

  // get state from common layer
  *State = FALSE;
  DwcStatus = DwcGetEPState(&Usbfn->DwcDevice, EndpointIndex, Direction, &EpState);
  DWC_ERR_CHK("Failed to get endpoint stall state: 0x%08x", DwcStatus);
  if (DWC_EP_STATE_STALLED == EpState || DWC_EP_STATE_STALLED_XFER_PENDING == EpState) {
    *State = TRUE;
  }

ON_EXIT:
  if (DWC_ERROR(DwcStatus)) {
    Status = EFI_DEVICE_ERROR;
  }
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnSetEndpointStallState (
  IN EFI_USBFN_IO_PROTOCOL         *This,
  IN UINT8                         EndpointIndex,
  IN EFI_USBFN_ENDPOINT_DIRECTION  Direction,
  IN BOOLEAN                       State
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV *Usbfn = NULL;

  FNC_ENTER_MSG ();
  if (NULL == This) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  Status = UsbfnDwcSetEndpointStallState(Usbfn,EndpointIndex,(DWC_ENDPOINT_DIR)Direction,State);
  ERR_CHK("Failed to %a ep %d, dir %d", State ? "stall" : "unstall",
      EndpointIndex, Direction);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnEventHandler (
  IN      EFI_USBFN_IO_PROTOCOL      *This,
  OUT     EFI_USBFN_MESSAGE          *Message,
  IN OUT  UINTN                      *PayloadSize,
  OUT     EFI_USBFN_MESSAGE_PAYLOAD  *Payload
  )
{
  EFI_STATUS              Status = EFI_SUCCESS;
  USBFN_DEV               *Usbfn = NULL;
  VOID                    *Src = NULL;
  UINTN                   SrcSize = 0;
  EFI_USBFN_MESSAGE       NoneMsg = EfiUsbMsgNone;
  EFI_USBFN_MESSAGE       *Msg = NULL;
  LIST_ENTRY              *ListEntry =  NULL;
  EFI_USBFN_MESSAGE_NODE  *CurrentMsgNode = NULL;

  if (NULL == This || NULL == Message || NULL == PayloadSize ||
      NULL == Payload || *PayloadSize == 0) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  if (IsListEmpty(&Usbfn->UsbfnMessages)) {
    Msg      =  &NoneMsg;
  } else {
    ListEntry = GetFirstNode(&Usbfn->UsbfnMessages);
    USB_ASSERT_RETURN(ListEntry, EFI_DEVICE_ERROR);
    CurrentMsgNode = BASE_CR(ListEntry, EFI_USBFN_MESSAGE_NODE, Link);
    Msg = &CurrentMsgNode->Message;

    // Check for a message payload, if any.
    switch (CurrentMsgNode->Message) {

    case EfiUsbMsgEndpointStatusChangedRx:
    case EfiUsbMsgEndpointStatusChangedTx:
      Src     = &(CurrentMsgNode->Data.XferCbResult);
      SrcSize = sizeof (CurrentMsgNode->Data.XferCbResult);
      break;

    case EfiUsbMsgSetupPacket:
      Src     = &(CurrentMsgNode->Data.SetupCbRequest);
      SrcSize = sizeof (CurrentMsgNode->Data.SetupCbRequest);
      break;

    case EfiUsbMsgBusEventSpeed:
      Src     = &(CurrentMsgNode->Data.SpeedCbSpeed);
      SrcSize = sizeof (CurrentMsgNode->Data.SpeedCbSpeed);
      break;
    default:
      ;
    }
  }
  USB_ASSERT_RETURN((NULL != Msg && *Msg <= EfiUsbMsgBusEventSpeed), EFI_DEVICE_ERROR);
  if (*Msg > EfiUsbMsgNone) {
    DBG (EFI_D_INFO, "%a", MsgToStr[*Msg]);
  }

  USB_ASSERT_RETURN(((Src && SrcSize) || (!Src && !SrcSize)), EFI_DEVICE_ERROR);

  if (*Msg == EfiUsbMsgNone) {
    // No messages in queue. It's safe to poll again.
    UsbfnDwcCorePoll(Usbfn);
  } else {
    // At least one message in queue. Try to copy it.
    if (*PayloadSize < SrcSize) {
      Status = EFI_BUFFER_TOO_SMALL;
    } else {
      CopyMem(Payload, Src, SrcSize);
    }
  }

  // Always update client.
  *Message = *Msg;
  if (SrcSize) {
    *PayloadSize = sizeof(*Payload);
  } else {
    *PayloadSize = 0;
  }

ON_EXIT:
  if(ListEntry) {
    //
    // Remove the message from the active
    // message list and put it back to the
    // memory pool
    //
    RemoveEntryList(ListEntry);
    InsertTailList(&Usbfn->UsbfnMessagePool, ListEntry);
  }
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnTransfer (
  IN      EFI_USBFN_IO_PROTOCOL         *This,
  IN      UINT8                         EndpointIndex,
  IN      EFI_USBFN_ENDPOINT_DIRECTION  Direction,
  IN OUT  UINTN                         *BufferSize,
  IN OUT  VOID                          *Buffer
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  USBFN_DEV   *Usbfn = NULL;
  UINTN       XferBufferSize;
  VOID        *XferBuffer;

  FNC_ENTER_MSG ();

  if ((NULL == This) || (NULL == BufferSize) || (NULL == Buffer) ||
      (EndpointIndex >= USBFN_ENDPOINT_NUMBER_MAX) || ((UINTN) Direction >= USBFN_ENDPOINT_DIR_MAX)) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  // set client's buffer
  XferBuffer     = Buffer;
  XferBufferSize = *BufferSize;

  // override client's buffer if hijacking enabled
  if (Usbfn->HijackData.RequestHijackingEnabled && Usbfn->HijackData.HijackThisRequest) {
    switch (Usbfn->HijackData.HijackDescType) {

      case USB_DESC_TYPE_DEVICE:
        DBG(EFI_D_WARN, "Hijacking Device Descriptor");
        XferBuffer     = Usbfn->SSDeviceInfo->DeviceDescriptor;
        XferBufferSize = Usbfn->SSDeviceInfo->DeviceDescriptor->Length;
        break;

      case USB_DESC_TYPE_CONFIG:
        DBG(EFI_D_WARN, "Hijacking Config Descriptor");
        XferBuffer     = Usbfn->SSDeviceInfo->ConfigInfoTable[USBFN_DEFAULT_CONFIG_INDEX]->ConfigDescriptor;
        XferBufferSize = Usbfn->SSDeviceInfo->ConfigInfoTable[USBFN_DEFAULT_CONFIG_INDEX]->ConfigDescriptor->TotalLength;
        break;
    }
    Usbfn->HijackData.HijackDescType       = USB_DESC_TYPE_UNDEFINED;
    Usbfn->HijackData.HijackedBuffer       = Buffer;
    Usbfn->HijackData.HijackedBufferLength = *BufferSize;
  }

  Status = UsbfnDwcTransfer(Usbfn, EndpointIndex, Direction, XferBufferSize, XferBuffer, Buffer);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetMaxTransferSize (
  IN  EFI_USBFN_IO_PROTOCOL  *This,
  OUT UINTN                  *MaxTransferSize
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  if (NULL == This || NULL == MaxTransferSize) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  *MaxTransferSize = USBFN_MAX_TRANSFER_SIZE;

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnAllocateTransferBuffer (
  IN   EFI_USBFN_IO_PROTOCOL  *This,
  IN   UINTN                  Size,
  OUT  VOID                   **Buffer
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  if (NULL == Buffer) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  *Buffer = UncachedAllocateAlignedPool(Size, USBFN_XFER_BUFFER_ALIGNMENT);
  if (NULL == *Buffer) {
    Status = EFI_OUT_OF_RESOURCES;
    DBG(EFI_D_ERROR, "out of memory");
    goto ON_EXIT;
  }
  DBG(EFI_D_POOL, "Transfer Buffer addr 0x%x. Transfer Buffer Size %d", *Buffer, Size);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnFreeTransferBuffer (
  IN EFI_USBFN_IO_PROTOCOL  *This,
  IN VOID                   *Buffer
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG ();
  if (NULL == Buffer) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  UncachedFreeAlignedPool(Buffer);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnStartController (
  IN EFI_USBFN_IO_PROTOCOL  *This
  )
{
  FNC_ENTER_MSG ();

  // Nothing to be done here

  FNC_LEAVE_MSG ();
  return EFI_SUCCESS;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnStopController (
  IN EFI_USBFN_IO_PROTOCOL  *This
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  USBFN_DEV  *Usbfn = NULL;

  FNC_ENTER_MSG ();

  if (NULL == This) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  Usbfn = USBFN_FROM_THIS (This);

  // Will force a disconnect
  UsbfnDwcCoreShutdown(Usbfn);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnSetEndpointPolicy(
  IN      EFI_USBFN_IO_PROTOCOL         *This,
  IN      UINT8                         EndpointIndex,
  IN      EFI_USBFN_ENDPOINT_DIRECTION  Direction,
  IN      EFI_USBFN_POLICY_TYPE         PolicyType,
  IN      UINTN                         BufferSize,
  IN      VOID                          *Buffer
  )
{
  EFI_STATUS      Status = EFI_SUCCESS;
  USBFN_DEV      *Usbfn  = NULL;

  FNC_ENTER_MSG();

  if(!This || !Buffer || EndpointIndex > USB_ENDPOINT_NUMBER_MAX ||
     Direction > EfiUsbEndpointDirectionHostIn){
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  DBG(EFI_D_INFO, "EP %d, Dir %d, PolicyType %d, BufferSize %d, Buffer %p",
      EndpointIndex,
      Direction,
      PolicyType,
      BufferSize,
      Buffer
      );

  Usbfn = USBFN_FROM_THIS(This);
  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);

  switch (PolicyType) {
  case EfiUsbPolicyZeroLengthTermination:
    if (Direction == EfiUsbEndpointDirectionDeviceRx) {
      Status = EFI_UNSUPPORTED;
      ERR_CHK("Enabling ZLT on an RX endpoint is not supported");
    }
    Usbfn->PipePolicy.ZLT[EndpointIndex][Direction] = *((BOOLEAN *)Buffer);
    break;
  default:
    Status = EFI_UNSUPPORTED;
  }

ON_EXIT:
  FNC_LEAVE_MSG();
  return Status;
}


/*
 * See header for documentation.
 */
EFI_STATUS
EFIAPI
UsbfnGetEndpointPolicy(
  IN      EFI_USBFN_IO_PROTOCOL         *This,
  IN      UINT8                         EndpointIndex,
  IN      EFI_USBFN_ENDPOINT_DIRECTION  Direction,
  IN      EFI_USBFN_POLICY_TYPE         PolicyType,
  IN OUT  UINTN                         *BufferSize,
  OUT     VOID                          *Buffer
  )
{
  EFI_STATUS          Status = EFI_SUCCESS;
  USBFN_DEV          *Usbfn  = NULL;

  FNC_ENTER_MSG ();

  if (!This || !BufferSize || !Buffer ||
      EndpointIndex > USB_ENDPOINT_NUMBER_MAX ||
      Direction > EfiUsbEndpointDirectionHostIn) {
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  DBG(EFI_D_INFO, "EP %d, Dir %d, PolicyType %d, BufferSize %d, Buffer %p",
    EndpointIndex,
    Direction,
    PolicyType,
    *BufferSize,
    Buffer);

  Usbfn = USBFN_FROM_THIS(This);
  USB_ASSERT_RETURN(Usbfn, EFI_INVALID_PARAMETER);  

  switch (PolicyType){  
  case EfiUsbPolicyMaxTransactionSize:
    if (*BufferSize < sizeof(UINT32)){
      Status = EFI_BUFFER_TOO_SMALL;
      *BufferSize = sizeof(UINT32);
      ERR_CHK("buffer too small");
    }

    *BufferSize = sizeof(UINTN);
    * ((UINTN *)Buffer) = Usbfn->PipePolicy.MaxTransferSize[EndpointIndex][Direction];
    break;
  case EfiUsbPolicyZeroLengthTerminationSupport:
    // check for buffer size
    if(*BufferSize < sizeof(BOOLEAN)){
      Status = EFI_BUFFER_TOO_SMALL;
      *BufferSize = sizeof(BOOLEAN);
      ERR_CHK("buffer too small");
    }
    // SNPS core supports ZLT on TX endpoints
    if (Direction == EfiUsbEndpointDirectionDeviceTx) {
      *((BOOLEAN *)Buffer) = TRUE;
    } else {
      *((BOOLEAN *)Buffer) = FALSE;
    }
    *BufferSize = sizeof(BOOLEAN);
    break;
  case EfiUsbPolicyZeroLengthTermination:
    if (*BufferSize < sizeof(BOOLEAN)){
      Status = EFI_BUFFER_TOO_SMALL;
      *BufferSize = sizeof(BOOLEAN);
      ERR_CHK("buffer too small");
    }

    *BufferSize = sizeof(BOOLEAN);
    if (Direction == EfiUsbEndpointDirectionDeviceRx) {
      *((BOOLEAN *) Buffer) = FALSE;
    }
    else {
      *((BOOLEAN *)Buffer) = Usbfn->PipePolicy.ZLT[EndpointIndex][Direction];
    }
    break;
  default:
    Status = EFI_UNSUPPORTED;
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}
