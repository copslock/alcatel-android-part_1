/** @file EnvDxe.c

  UEFI Environment Dxe code for registering FBPT event handlers
  For all Qualcomm specific initializations

  Copyright (c) 2012-2014 Qualcomm Technologies, Inc.
  All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.



 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 10/27/14    jb      Change FBPT base to uint64
 05/08/14    vk       Add 64 bit support
 04/04/2014  vk       Warning cleanup
 15/10/2013  vk       Disable FBPT
 10/04/2013  yg       Add Dxe Info into InfoBlk to load symbols
 03/04/2013  yg       Make runtime and improve robustness
 29/03/2013  vk       Add support for run time symbol load
 15/03/2013  vk       Move to EnvDxe, add gST to InfoBlock
 07/06/2012  vishalo  Initial revision
=============================================================================*/

#include <Uefi.h>
#include <Include/UefiInfoBlk.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/BaseMemoryLib.h>
#include <Guid/GlobalVariable.h>
#include <Guid/EventGroup.h>
#include <Library/ProcAsmLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/FBPTLib.h>
#include <Library/FBPT.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Guid/DebugImageInfoTable.h>
#include <Library/PeCoffGetEntryPointLib.h>

/* Event to signal */
EFI_EVENT  FBPTEventOsLoaderLoadImage;
EFI_EVENT  FBPTEventOsLoaderStartImage;
EFI_EVENT  FBPTEventExitBootStart;
EFI_EVENT  FBPTEventExitBootEnd;

/* Event Guids */
extern EFI_GUID gEfiEventFBPTOsLoaderLoadImageStart;
extern EFI_GUID gEfiEventFBPTOsLoaderStartImageStart;
extern EFI_GUID gEfiEventFBPTExitBootServicesEntry;
extern EFI_GUID gEfiEventFBPTExitBootServicesExit;
extern EFI_GUID gEfiEventVirtualAddressChangeGuid;

/* FBPT PayloadMemBase */
STATIC UINTN FBPTPayLoadMemBase;

/* NOTE: This structure size, member POS has dependency
 * in T32 Debug script */
typedef struct DbgTable
{
  EFI_PHYSICAL_ADDRESS PhyLoadAddr;
  EFI_VIRTUAL_ADDRESS VirLoadAddr;

  /* Only the past Build folder portion is stored,
   * 240 will make sure our structure will be of size
   * 256 aligned */
  CHAR8 DriverName[240];
} DriverList;

extern EFI_GUID gEfiDebugImageInfoTableGuid;

STATIC UefiInfoBlkType *InfoBlkPtr;

// Initialized to a value close to the number of runtime drivers we expect to load,
// Buffers will resize if this number is exceeded
#define INIT_DRIVER_COUNT   10
STATIC UINTN CurrRtDbgDriverCnt = INIT_DRIVER_COUNT;
STATIC  DriverList *RtDbgDrivers = NULL;

VOID InitDbgDriverList (VOID);

EFI_STATUS GetImageNames (EFI_DEBUG_IMAGE_INFO  *DebugTable, UINTN NumEntries);

/**
  Initializes global FBPTMemoryBase for handlers
  Must be called once before handlers update entries

  @retval EFI_SUCCESS  FBPTMemoryBase initialized
**/

EFI_STATUS
InitFBPTPayLoadMemBase(VOID)
{
  FBPTPayLoadMemBase =  (UINTN)PcdGet64(PcdFBPTPayloadBaseOffset);
  return EFI_SUCCESS;
}

/**
  This function updates FBPT entry for OsLoaderLoadImageStart
  FBPTPayLoadMemBase must be initialized before calling

  @retval EFI_SUCCESS on Success
**/
EFI_STATUS
UpdateOsLoaderLoadImageStart(VOID)
{
  UefiPerfRecord_t *UefiPerfRecord;
  UefiPerfRecord =  (UefiPerfRecord_t*) FBPTPayLoadMemBase;
  UefiPerfRecord->UefiFBBPDataRecord.OSLoaderLoadImageStart =  CycleToNanoSec(ReadCycleCntReg());
  return EFI_SUCCESS;
}

/**
  This function updates FBPT entry for OsLoaderStartImageStart
  FBPTPayLoadMemBase must be initialized before calling

  @retval EFI_SUCCESS on Success
**/
EFI_STATUS
UpdateOsLoaderStartImageStart(VOID)
{
  UefiPerfRecord_t *UefiPerfRecord;
  UefiPerfRecord =  (UefiPerfRecord_t*) FBPTPayLoadMemBase;
  UefiPerfRecord->UefiFBBPDataRecord.OSLoaderStartImageStart = CycleToNanoSec(ReadCycleCntReg());
  return EFI_SUCCESS;
}

/**
  This function updates FBPT entry for UpdateExitBootServicesEntry
  FBPTPayLoadMemBase must be initialized before calling

  @retval EFI_SUCCESS on Success
**/
EFI_STATUS
UpdateExitBootServicesEntry(VOID)
{

  UefiPerfRecord_t *UefiPerfRecord;
  UefiPerfRecord =  (UefiPerfRecord_t*) FBPTPayLoadMemBase;
  UefiPerfRecord->UefiFBBPDataRecord.ExitBootServicesEntry =   CycleToNanoSec(ReadCycleCntReg());

  InitDbgDriverList ();

  if (InfoBlkPtr)
    GetImageNames ((EFI_DEBUG_IMAGE_INFO*)InfoBlkPtr->DebugTablePtr, InfoBlkPtr->DebugTableEntryCnt);

  return EFI_SUCCESS;
}

/**
  This function updates FBPT entry for UpdateExitBootServicesExit
  FBPTPayLoadMemBase must be initialized before calling

  @retval EFI_SUCCESS on Success
**/
EFI_STATUS
UpdateExitBootServicesExit(VOID)
{
  UefiPerfRecord_t *UefiPerfRecord;
  UefiPerfRecord =  (UefiPerfRecord_t*) FBPTPayLoadMemBase;
  UefiPerfRecord->UefiFBBPDataRecord.ExitBootServicesExit =   CycleToNanoSec(ReadCycleCntReg());
  return EFI_SUCCESS;
}

/**
  FBPTEventOsLoaderLoadImageStart event handler

  @param Event    Not used
  @param Context  Not used
  @retval EFI_SUCCESS on Success
**/
VOID EFIAPI FBPTEventOsLoaderLoadImageStart(
  IN EFI_EVENT Event,
  IN VOID *Context
)
{
  UpdateOsLoaderLoadImageStart();
  return;
}

/**
  FBPTEventOsLoaderStartImageStart event handler

  @param Event    Not used
  @param Context  Not used
  @retval EFI_SUCCESS on Success
**/
VOID EFIAPI FBPTEventOsLoaderStartImageStart(
  IN EFI_EVENT Event,
  IN VOID *Context
)
{
  UpdateOsLoaderStartImageStart();
  return;
}

/**
  FBPTEventExitBootServiceEntry event handler

  @param Event    Not used
  @param Context  Not used
  @retval EFI_SUCCESS on Success
**/
VOID EFIAPI FBPTEventExitBootServiceEntry(
  IN EFI_EVENT Event,
  IN VOID *Context
)
{
  UpdateExitBootServicesEntry();
  return;
}

/**
  FBPTEventExitBootServiceExit event handler

  @param Event    Not used
  @param Context  Not used
  @retval EFI_SUCCESS on Success
**/
VOID EFIAPI FBPTEventExitBootServiceExit(
  IN EFI_EVENT Event,
  IN VOID *Context
)
{
  UpdateExitBootServicesExit();
  return;
}

EFI_STATUS InitFBPT(VOID)
{
  EFI_STATUS Status;

  Status = InitFBPTPayLoadMemBase();
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_INFO, "InitFBPTPayLoadMemBase Failed !\r\n"));
      goto ErrorExit;
  }

  //
  //Register for notification events
  //

  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               FBPTEventExitBootServiceEntry,
                               NULL,
                               &gEfiEventFBPTExitBootServicesEntryGuid,
                               &FBPTEventExitBootStart);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_INFO, "Create FBPTEventExitBootStart failed, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
  }

  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               FBPTEventExitBootServiceExit,
                               NULL,
                               &gEfiEventFBPTExitBootServicesExitGuid,
                               &FBPTEventExitBootEnd);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_INFO, "Create FBPTEventExitBootEnd failed, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
  }

  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               FBPTEventOsLoaderLoadImageStart,
                               NULL,
                               &gEfiEventFBPTOsLoaderLoadImageStartGuid,
                               &FBPTEventOsLoaderLoadImage);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_INFO, "Create FBPTEventOsLoaderLoadImage failed, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
  }

  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               FBPTEventOsLoaderStartImageStart,
                               NULL,
                               &gEfiEventFBPTOsLoaderStartImageStartGuid,
                               &FBPTEventOsLoaderStartImage);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_INFO, "Create FBPTEventOsLoaderStartImage failed, Status = (0x%x)\r\n", Status));
      goto ErrorExit;
  }

ErrorExit:
  return Status;
}

EFI_STATUS UpdateInfoBlkSystemTableAddr (VOID)
{
  if (InfoBlkPtr)
    InfoBlkPtr->SystemTablePtr = (UINTN*) gST;

  return EFI_SUCCESS;
}


VOID
InitDbgDriverList (VOID)
{
  EFI_CONFIGURATION_TABLE *EfiCfgTblPtr = NULL;
  EFI_DEBUG_IMAGE_INFO_TABLE_HEADER *EfiDbgImgInfTbl = NULL;
  UINTN NumDbgImages = 0;
  UINTN NumConfigTblEntries = 0;

  UINTN i;

  NumConfigTblEntries = gST->NumberOfTableEntries;
  EfiCfgTblPtr = gST->ConfigurationTable;

  /* Look at all configuration table entries for debug table */
  for (i = 0; i < NumConfigTblEntries; i++)
  {
    if(CompareGuid (&EfiCfgTblPtr->VendorGuid, &gEfiDebugImageInfoTableGuid) == TRUE)
    {
      EfiDbgImgInfTbl = (EFI_DEBUG_IMAGE_INFO_TABLE_HEADER*) EfiCfgTblPtr->VendorTable;
      NumDbgImages = EfiDbgImgInfTbl->TableSize;

      if (InfoBlkPtr)
      {
        InfoBlkPtr->DebugTablePtr = (UINTN*) EfiDbgImgInfTbl->EfiDebugImageInfoTable;
        InfoBlkPtr->DebugTableEntryCnt = NumDbgImages;
      }
      break;
    }
    else
      EfiCfgTblPtr++;
  }
}


EFI_STATUS
GetImageNames (EFI_DEBUG_IMAGE_INFO  *DebugTable, UINTN NumEntries)
{
  volatile UINTN Count = 0;
  volatile UINTN RtDbgDrvCnt = 0;
  UINTN Entry;

  if ((DebugTable == NULL) || (RtDbgDrivers == NULL))
    return EFI_INVALID_PARAMETER;

  for (Entry = 0; Entry < NumEntries; Entry++, DebugTable++)
  {
    if (DebugTable->NormalImage != NULL)
    {
      if ((DebugTable->NormalImage->ImageInfoType == EFI_DEBUG_IMAGE_INFO_TYPE_NORMAL) &&
          (DebugTable->NormalImage->LoadedImageProtocolInstance != NULL))
        {
          STATIC CHAR8* pName;
          pName = PeCoffLoaderGetPdbPointer (DebugTable->NormalImage->LoadedImageProtocolInstance->ImageBase);

          if (DebugTable->NormalImage->LoadedImageProtocolInstance->ImageCodeType == EfiRuntimeServicesCode)
          {
            if (RtDbgDrvCnt >= CurrRtDbgDriverCnt) {
              AllocateCopyPool(((CurrRtDbgDriverCnt*2) * sizeof(DriverList)), (VOID**)&RtDbgDrivers);
              CurrRtDbgDriverCnt*=2;
            }
            AsciiStrnCpy (RtDbgDrivers[RtDbgDrvCnt].DriverName, pName, sizeof(RtDbgDrivers[RtDbgDrvCnt].DriverName));
            RtDbgDrivers[RtDbgDrvCnt].PhyLoadAddr = (EFI_PHYSICAL_ADDRESS) DebugTable->NormalImage->LoadedImageProtocolInstance->ImageBase;
            RtDbgDrivers[RtDbgDrvCnt].VirLoadAddr = (EFI_PHYSICAL_ADDRESS) DebugTable->NormalImage->LoadedImageProtocolInstance->ImageBase;
            RtDbgDrvCnt++;
            continue;
          }
          Count++;
       }
    }
  }

  if (InfoBlkPtr)
    InfoBlkPtr->RtDbgTableEntryCnt = RtDbgDrvCnt;

  return EFI_SUCCESS;
}

EFI_EVENT VirtualAddressChangeEvent = NULL;

VOID
EFIAPI
VirtualAddressChangeCallBack (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  UINTN i;
  EFI_STATUS Status;
  volatile UINTN Errors = 0;

  for (i = 0; i < CurrRtDbgDriverCnt; i++)
  {
    Status = gRT->ConvertPointer(0, (VOID**)&RtDbgDrivers[i].VirLoadAddr);
    if (Status != EFI_SUCCESS)
      Errors++;
  }
}

EFI_STATUS
InitRuntimeDbg (VOID)
{
  EFI_STATUS Status;

  Status = gBS->AllocatePages (AllocateAnyPages,
                                EfiRuntimeServicesData,
                                EFI_SIZE_TO_PAGES((CurrRtDbgDriverCnt) * sizeof(DriverList)),
                                (VOID*)&RtDbgDrivers);
  CurrRtDbgDriverCnt = EFI_PAGE_SIZE / sizeof(DriverList);

  if (Status != EFI_SUCCESS)
  {
    DEBUG ((EFI_D_WARN, "EnvDxe: ERROR, unable to allocate memory for runtime debug table\n"));
    return Status;
  }

  SetMem (RtDbgDrivers, ((CurrRtDbgDriverCnt) * sizeof(DriverList)), 0);

  if (InfoBlkPtr)
    InfoBlkPtr->RuntimeDbgTablePtr = (UINTN*)RtDbgDrivers;

  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  VirtualAddressChangeCallBack,
                  NULL,
                  &gEfiEventVirtualAddressChangeGuid,
                  &VirtualAddressChangeEvent
                  );
  ASSERT_EFI_ERROR (Status);

  return Status;
}

VOID
AddDxeSymbolInfo (VOID)
{
  EFI_HOB_MEMORY_ALLOCATION *MemAllocHob;

  MemAllocHob = GetFirstHob (EFI_HOB_TYPE_MEMORY_ALLOCATION);

  while (MemAllocHob)
  {
    if (MemAllocHob->AllocDescriptor.MemoryType == EfiBootServicesCode)
    {
      InfoBlkPtr->DxeSymbolAddr = (UINT64) MemAllocHob->AllocDescriptor.MemoryBaseAddress;
      break;
    }
    MemAllocHob = (EFI_HOB_MEMORY_ALLOCATION *)((UINTN)MemAllocHob + MemAllocHob->Header.HobLength);
    MemAllocHob = GetNextHob (EFI_HOB_TYPE_MEMORY_ALLOCATION, MemAllocHob);
  }
}

/**
  EnvDxe Entry point. Registers to handle FBPT update events, and gST to InfoBlk

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.
  @param[in] SystemTable    A pointer to the EFI System Table.

  @retval EFI_SUCCESS       All handlers installed

**/

EFI_STATUS
EFIAPI
EnvDxeInitialize (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS  Status;

  /* Lot of modules here depend on InfoBlock */
  InfoBlkPtr = (UefiInfoBlkType*)GetInfoBlkPtr();
  ASSERT (InfoBlkPtr != NULL);

  AddDxeSymbolInfo ();


  Status = InitFBPT();
  if (EFI_ERROR (Status))
    DEBUG(( EFI_D_WARN, "ERROR: FBPT Init failed \n"));


  Status = UpdateInfoBlkSystemTableAddr();
  if (EFI_ERROR (Status))
    DEBUG(( EFI_D_WARN, "Error: Unable to update InfoBlk with gST \n"));

  Status = InitRuntimeDbg();
  if (EFI_ERROR (Status))
    DEBUG(( EFI_D_WARN, "Error: Unable to init runtime debug table \n"));

  return Status;
}

