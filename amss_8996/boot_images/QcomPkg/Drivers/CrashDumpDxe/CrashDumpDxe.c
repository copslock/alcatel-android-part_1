/** @file CrashDumpDxe.c
   
  Code for reset reason feature for retail devices

  Copyright (c) 2012-2014 Qualcomm Technologies, Inc. 
  All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.



  when        who       what, where, why
  --------    ---       ----------------------------------------------------------
  12/11/2014   ao       update with new SPMI API
  08/15/2014  sm        Switched to new SCM API
  07/09/2014  shireesh  Added pmic PON_PBL_STATUS register to reset data buffer
  06/06/2014  vk        Fix crash in 64 bit
  04/29/2014  shireesh  Added support for h/w button trigerd reset detection
  03/26/2014  shireesh  Initial version
  
=============================================================================*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
#include <Library/UefiLib.h>
#include <Library/PrintLib.h>
#include <Library/BaseLib.h>
#include <Include/UefiInfoBlk.h>
#include <Library/QcomLib.h>
#include <Library/OfflineCrashDump.h>
#include <Library/QcomBaseLib.h>
#include <Library/UefiCfgLib.h>
#include <Protocol/EFIScm.h>
#include <Include/scm_sip_interface.h>
#include <Protocol/EFIChipInfo.h>
#include <Protocol/EFIPmicPwrOn.h>
#include <Protocol/EFISPMI.h>

STATIC EFI_OFFLINE_CRASHDUMP_CONFIG_TABLE * OfflineCrashDumpConfigTable = NULL;

/**
  Function make syscall to TZ to provide buffer address to copy diag data

**/
STATIC
BOOLEAN
EFIAPI
TZResetDiagDataBufferAddressSysCall(VOID* address, UINTN size)
{ 
  EFI_STATUS        Status;
  BOOLEAN           SysCallStatus     = FALSE;
  QCOM_SCM_PROTOCOL *pQcomScmProtocol = NULL;
  UINT64            Parameters[SCM_MAX_NUM_PARAMETERS] = {0};
  UINT64            Results[SCM_MAX_NUM_RESULTS] = {0};
  tz_set_address_to_dump_tz_diag_for_uefi_req_t *SysCallReq = (tz_set_address_to_dump_tz_diag_for_uefi_req_t*)Parameters;
  tz_syscall_rsp_t *SysCallRsp = (tz_syscall_rsp_t*)Results;
  
  
  /* Initializing cmd structure for SCM sys call */
  SysCallReq->size        = size;
  SysCallReq->addr        = (UINTN)address;

  /* Locate QCOM_SCM_PROTOCOL */
  Status = gBS->LocateProtocol ( &gQcomScmProtocolGuid, NULL, (VOID **)&pQcomScmProtocol);
  if( EFI_ERROR(Status)) 
  {
    DEBUG(( EFI_D_ERROR, " Locate SCM Protocol failed, Status =  (0x%x)\r\n", Status));
    goto ErrorExit;
  }

  /* Make a SCM Sys call */
  Status = pQcomScmProtocol->ScmSipSysCall (pQcomScmProtocol,
                                            TZ_DUMP_SET_ADDRESS_TO_DUMP_TZ_DIAG_FOR_UEFI,
                                            TZ_DUMP_SET_ADDRESS_TO_DUMP_TZ_DIAG_FOR_UEFI_PARAM_ID,
                                            Parameters,
                                            Results
                                            );
  if (EFI_ERROR (Status)) 
  {
    DEBUG(( EFI_D_ERROR, "ScmSipSysCall() failed, Status = (0x%x)\r\n", Status));
    goto ErrorExit;
  }
  if (SysCallRsp->status != 1)
  {    
    DEBUG(( EFI_D_ERROR, "TZ_DUMP_SET_ADDRESS_TO_DUMP_TZ_DIAG_FOR_UEFI failed, Status = (0x%x)\r\n", SysCallRsp->status));
    goto ErrorExit;
  }

  SysCallStatus = TRUE;

ErrorExit:
  return SysCallStatus;
}

/**
  Function to get offlinedump Config table

**/
EFIAPI
EFI_STATUS
GetCrashDumpConfigTable( VOID )
{
  EFI_STATUS Status = EFI_SUCCESS;

  Status = EfiGetSystemConfigurationTable(&gEfiOfflineCrashDumpConfigTableGuid, (VOID**)&OfflineCrashDumpConfigTable);
  if (Status != EFI_SUCCESS) 
  {
    DEBUG(( EFI_D_ERROR, "Offline Crash Dump Config Table does not exist in System Configuration Table\r\n"));
    return EFI_NOT_FOUND;
  }

  return EFI_SUCCESS;
}

/**
 This function intializes Offline Crash Dump Config table fields for reset reason

**/
STATIC
EFI_STATUS
EFIAPI
InitializeResetDataBuffers ( EFI_OFFLINE_CRASHDUMP_CONFIG_TABLE *mOfflineCrashDumpConfigTable )
{
  EFI_STATUS    Status;
  MemRegionInfo ResetDataRegionInfo;
  volatile UINT32 RegDump;
  
  CHAR8* UefiResetBufferAddress = NULL;
  VOID* tzBufferAddress = 0;
  RESET_DATA_HEADER *mResetReasonData = NULL;
  EFI_QCOM_SPMI_PROTOCOL *pSPMI = NULL;
  Spmi_Result result = SPMI_SUCCESS;
  EFI_CHIPINFO_PROTOCOL *pChipInf;
  UINT64 *GCCResetValueAddress = NULL;

  const CHAR8 GccStr[]               = "GCC_RESET_STATUS=";
  const CHAR8 PONReason1Str[]        = "PON_REASON1=";	
  const CHAR8 PONWarmReset1Str[]     = "PON_WARM_RESET_REASON1=";  
  const CHAR8 PONWarmReset2Str[]     = "PON_WARM_RESET_REASON2=";	
  const CHAR8 POFF1Str[]             = "POFF_REASON1=";	
  const CHAR8 POFF2Str[]             = "POFF_REASON2=";
  const CHAR8 PONPBL2Str[]           = "PON_PBL_STATUS=";

  /* UEFI total reset data size in bytes */
  UINT64 UefiResetDataSize = AsciiStrLen(GccStr)+AsciiStrLen(PONReason1Str)+AsciiStrLen(PONWarmReset1Str)+AsciiStrLen(PONWarmReset2Str)+AsciiStrLen(GccStr)+AsciiStrLen(POFF1Str)+AsciiStrLen(POFF2Str)+ 6* sizeof(UINT64);

  /* Get the Reset Data memory region. */
  Status = GetMemRegionInfoByName("Reset Data", &ResetDataRegionInfo);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, " Could not find the Reset Data region .\r\n"));
    return Status;
  }
  
  mResetReasonData = (RESET_DATA_HEADER*) ((UINTN)ResetDataRegionInfo.MemBase);

  /* Initialize reset reason data structure*/
  mResetReasonData->Version = RESET_DATA_HEADER_VERSION;
  
  Status = gBS->LocateProtocol (&gEfiChipInfoProtocolGuid, NULL, (VOID **) &pChipInf);
  if (EFI_SUCCESS != Status)
  { 
     return Status;
  }

  if (EFI_SUCCESS != pChipInf->GetChipIdString(pChipInf, mResetReasonData->PlatformID, PLATFORMID_MAX_LENGTH))
  {
    DEBUG ((EFI_D_WARN, "gEfiChipInfoProtocolGuid->GetChipIdString() Failed. \n"));
    Status = EFI_UNSUPPORTED;
    return Status;
  }
  
  mResetReasonData->Guid                        = gQcomResetReasonDataCaptureGuid;
  /* Size of datbuffer+header */
  mResetReasonData->ResetDataBufferSize         = sizeof(RESET_DATA_BUFFER);       
  mResetReasonData->ResetDataBuffer.Version     = RESET_DATA_BUFFER_VERSION;
  
  /* Offset from reset data header to tz data buffer */
  mResetReasonData->ResetDataBuffer.DataOffset1 = sizeof(RESET_DATA_HEADER) - QCDATA_1_SIZE - QCDATA_2_SIZE;
  mResetReasonData->ResetDataBuffer.DataSize1   = QCDATA_1_SIZE;

  /* Offset from reset data header to UEFI data buffer */
  mResetReasonData->ResetDataBuffer.DataOffset2 = mResetReasonData->ResetDataBuffer.DataOffset1 + QCDATA_1_SIZE;
  mResetReasonData->ResetDataBuffer.DataSize2   = QCDATA_2_SIZE;


  /* Setup buffer address for TZ to dump to within Reset Data region */
  tzBufferAddress = (UINT8*)mResetReasonData + (UINTN)mResetReasonData->ResetDataBuffer.DataOffset1;
  TZResetDiagDataBufferAddressSysCall(tzBufferAddress, QCDATA_1_SIZE);

  mOfflineCrashDumpConfigTable->ResetDataAddress = (UINT64)mResetReasonData;
  mOfflineCrashDumpConfigTable->ResetDataSize    = sizeof(RESET_DATA_HEADER);
  
  /* Return if there is not enough room for all data */
  if ( UefiResetDataSize > QCDATA_2_SIZE )
  { 
     return EFI_OUT_OF_RESOURCES;
  }
  
  /* Sets up UefiResetBufferAddress to point to just after TZ buffer */
  UefiResetBufferAddress = (CHAR8*)((UINTN)tzBufferAddress + QCDATA_1_SIZE);
    
  /* Fill in version as first field in UEFI buffer */
  *((UINT64*)UefiResetBufferAddress) = RESET_DATA2_VERSION;
    
  /* Copy GCC reset string to UEFI data buffer */
  UefiResetBufferAddress = UefiResetBufferAddress + sizeof(UINT64);
  AsciiStrCpy(UefiResetBufferAddress, GccStr);
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(GccStr));
    
  /* Copy GCC reset from SMEM to UEFI data buffer */
  /* Get location of where GCC reset reason is backed up */
  Status = GetConfigValue("GCCResetValueAddress", (UINT32*)&GCCResetValueAddress);
  if (Status != EFI_SUCCESS)
  {
      DEBUG(( EFI_D_ERROR, "Could not get address from uefiplat.cfg to read GCC Reset Value, Status = (0x%x)\r\n", Status));
      return Status;
  }

  RegDump = *((UINT32*)GCCResetValueAddress) & 0x0FE;

  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress = UefiResetBufferAddress + sizeof(UINT64);

  /* Write PMIC registers to reset data buffer */

  /* Get SPMI Protocol */
  Status = gBS->LocateProtocol(&gQcomSPMIProtocolGuid, NULL, (VOID **) &pSPMI);
  if ( EFI_ERROR (Status) )
  {
      DEBUG(( EFI_D_ERROR, "MemMapDump: failed to locate SPMIProtocol, Status = (0x%x)\r\n", Status));
      return Status;
  }

  /* Read and write PON_REASON1 register to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_REASON1, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
      DEBUG(( EFI_D_ERROR, "Read to PON_REASON1 register failed, Status = (0x%x)\r\n", result));
  }
    
  AsciiStrCpy(UefiResetBufferAddress, PONReason1Str);    
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(PONReason1Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress=UefiResetBufferAddress+sizeof(UINT64);
  
  /* Read and write PON_WARM_RESET_REASON1 register to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_WARM_RESET_REASON1, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
      DEBUG(( EFI_D_ERROR, "Read to PON_WARM_RESET_REASON1 register failed, Status = (0x%x)\r\n", result));
  }
  
  AsciiStrCpy(UefiResetBufferAddress, PONWarmReset1Str);    
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(PONWarmReset1Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress=UefiResetBufferAddress+sizeof(UINT64);
    
  /* Read and write PON_WARM_RESET_REASON2 register to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_WARM_RESET_REASON2, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
    DEBUG(( EFI_D_ERROR, "Read to PON_WARM_RESET_REASON2 register failed, Status = (0x%x)\r\n", result));
    
  }

  AsciiStrCpy(UefiResetBufferAddress, PONWarmReset2Str);    
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(PONWarmReset2Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress = UefiResetBufferAddress+sizeof(UINT64);
    
  /* Read and write POFF_REASON1 egister to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, POFF_REASON1, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
      DEBUG(( EFI_D_ERROR, "Read to POFF_REASON1 register failed, Status = (0x%x)\r\n", result));

  }

  AsciiStrCpy(UefiResetBufferAddress, POFF1Str);   
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(POFF1Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress = UefiResetBufferAddress+sizeof(UINT64);
            
  /* Read and write POFF_REASON2 egister to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, POFF_REASON2, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
    DEBUG(( EFI_D_ERROR, "Read to POFF_REASON2 register failed, Status = (0x%x)\r\n", result));

  }

  AsciiStrCpy(UefiResetBufferAddress, POFF2Str);  
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(POFF2Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress = UefiResetBufferAddress+sizeof(UINT64);
  
  /* Read and write PON_PBL_STATUS egister to buffer */
  result = pSPMI->ReadLong(pSPMI, 0, SPMI_BUS_ACCESS_PRIORITY_LOW, PON_PBL_STATUS, (UINT8*)&RegDump, (UINT32)1,0);
  if ( result != SPMI_SUCCESS )
  {
    DEBUG(( EFI_D_ERROR, "Read to PON_PBL_STATUS register failed, Status = (0x%x)\r\n", result));

  }

  AsciiStrCpy(UefiResetBufferAddress, PONPBL2Str);  
  UefiResetBufferAddress = (CHAR8*)( (UINTN)UefiResetBufferAddress + AsciiStrLen(PONPBL2Str));
  AsciiValueToString ((UefiResetBufferAddress), 0, (UINT64)RegDump, 8);
  UefiResetBufferAddress = UefiResetBufferAddress+sizeof(UINT64);

  return EFI_SUCCESS; 
}


EFI_STATUS
EFIAPI
CrashDumpDxeInitialize(
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
   EFI_STATUS Status = EFI_SUCCESS;
   
   Status = GetCrashDumpConfigTable();
   if (Status != EFI_SUCCESS)
     return Status;
     
   Status = InitializeResetDataBuffers(OfflineCrashDumpConfigTable);
   return Status;
}

