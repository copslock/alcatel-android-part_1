/** @file SataUtil.c
   
  XBL SATA driver code
  
  Copyright (c) 2014 Qualcomm Technologies, Inc. 
  All Rights Reserved. 
  Qualcomm Technologies Proprietary and Confidential

  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php
   
  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED. 

**/

/*=============================================================================
                              EDIT HISTORY

when         who     what, where, why
----------   -----   ----------------------------------------------------------- 
2014/11/17   rm      Initial Version

=============================================================================*/

#include "SataPriv.h"
#include "SataUtil.h"

/**                                                                 
  Get AHCI controller base address
          
  @param  ControllerNum             AHCI controller number.
  @param  Address                   The base address of the memory operations.                                  
                                    
  @retval EFI_SUCCESS               The valid address is returned.  
  @retval EFI_INVALID_PARAMETER     Controller number is out of range 
                                    or Address is NULL
                                   
**/
EFI_STATUS
GetAhciBaseAddress (
  IN  UINT32         ControllerNum,
  OUT UINT32         *Address
  )
{
  if(Address == NULL || ControllerNum >= SATA_CONTROLLER_NUMBER)
  {
    return EFI_INVALID_PARAMETER;
  }
  
  *Address = HAL_Sata_GetAhciBaseAddress(ControllerNum);

  return EFI_SUCCESS;
}

/**                                                                 
  Vendor specific firmware initialization
          
  @param  ControllerNum             AHCI controller number.
                              
**/
VOID
FirmwareSpecificInitialization(UINT32 ControllerNum)
{
  /* CAP.SSS (support for staggered spin-up) - No */
  HAL_Sata_Cap_Sss(ControllerNum, FALSE);
  
  /* CAP.SMPS (support for mechanical presence switches) */
  HAL_Sata_Cap_Smps(ControllerNum, FALSE);
  
  /* PI (ports implemented) - Port 0*/
  HAL_Sata_SetPiRegister(ControllerNum);
  
  /* PxCMD.HPCP (whether port is hot plug capable) - No*/
  HAL_Sata_PxCmd_Hpcp(ControllerNum, FALSE);
  
  /* PxCMD.MPSP (whether mechanical presence switch is attached 
     to the port) - No*/
  HAL_Sata_PxCmd_Mpsp(ControllerNum, FALSE);
  
  /* PxCMD.CPD (whether cold presence detect logic is attached 
     to the port) - No*/  
  HAL_Sata_PxCmd_Cpd(ControllerNum, FALSE); 
}

/**                                                                 
  Initialize SATA PHY
          
  @retval EFI_SUCCESS               The valid address is returned.  
  @retval EFI_DEVICE_ERROR          Failed to Initialize PHY 
                                   
**/
EFI_STATUS
SataPhyInitialization (VOID)
{
#ifndef PRE_SIL
  UINT32 Count = 0;
  UINT32 Sp = 0;
  
  /*TODO: We only initialize sp0 here for now,
  later one we can initialize the lane we use */
  HAL_Sata_InitializePhy();
  // poll qserver_com_c_ready_status bit[0]
  Count = SATA_PHY_POLL_MAX;
  while(Count -= 100)
  {
    if (HAL_Sata_GetCReadyStatus(Sp)) break;
    mdelay(100);
  }
  
  if(Count == 0)
    return EFI_DEVICE_ERROR;
#endif  
  return EFI_SUCCESS;
}