/** @file
  Qualcomm ACPI Platform Driver

  Copyright (c) 2011-2015, Qualcomm Technologies Inc. All rights
  reserved. 
  Portions Copyright (c) 2008 - 2010, Intel Corporation. All rights reserved.
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/ 

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 04/23/15   vk      Add UFS support
 03/20/15   wayne   Added Acpi chip info callback
 06/17/14   swi     Updated AML register test
 04/17/14   swi     Fixed KW warning
 03/26/14   swi     Changed output to EFI_D_WARN as appropriate
                    Fixed 64-bit warnings
 02/19/14   swi     Implemented callback mechanism to update AML variables in DSDT
                    Added test functionality and test callbacks 
                    Changed ReadSize type to UINTN (64-bit compatible)
 01/16/14   vk      Remove InCarveOut
 08/12/13   sahn    Implemented callback mechanism to update ACPI tables
 07/22/13   zzx     Added register interface to enable functionality of ACPI table 
                    query/update with ACPITableLoadEvent.
 04/10/13   xc      remove PcdTZStorageTablesBaseOffset, read mem address from cfg file
 02/04/13   shl     Re-arrange the order get appliation ID and set IMEM bit.
 01/09/13   vk      Fix KlockWork warnings
 12/10/12   shl     Moved ACPI_TPM2_TABLE out to EFITree.h.
 09/14/12   bmuthuku Added logic to check if TPM is enabled or Disabled
 07/07/12   niting  Changed logging levels
 03/30/12   shl     Added functions for store acpi before and after fixup. This 
                    is requred by Microsoft
 03/06/12   jz      Updated to use PcdTZStorageTablesBaseOffset
 02/29/11   eamonn  Allocated TPM data at top of SCM region
 11/21/11   vishalo Load ACPI tables from specified partitions only
 11/21/11   shl     Added fTPM support 
 08/30/11   niting  Load ACPI tables in BDS.
 05/19/11   niting  Creation.

=============================================================================*/

#include <AcpiPlatform.h>
#include <AcpiPlatformChipInfo.h>
#include <Library/BootConfig.h>

EFI_QCOM_ACPIPLATFORM_PROTOCOL AcpiPlatformProtocolImpl =
{
  ACPIPLATFORM_REVISION,
  GetAcpiTable,
  GetAcpiTableSize,
  AcpiTableRegister,
  AmlVariableRegister
};

EFI_QCOM_ACPIPLATFORMTEST_PROTOCOL AcpiPlatformTestProtocolImpl =
{
  ACPIPLATFORMTEST_REVISION,
  AcpiTableRegisterTest
};

// Acpi table measurement support
AcpiTableEntry           PreFixArray[MAX_ACPI_TABLES] = {{NULL,0,0}};
AcpiTableEntry           PostFixArray[MAX_ACPI_TABLES] = {{NULL,0,0}};
UINTN                    NumOfAcpiTables = 0;

// AML Variable arrays
AmlVariableEntry           PreFixAmlVariableArray[MAX_AML_VARIABLES] = {
    {
       .AmlVariable =NULL,
       .AmlVariableSize = 0,
       .AmlVariableName={0,0,0,0},
       .TableOffset = 0
    },
};
AmlVariableEntry    PostFixAmlVariableArray[MAX_AML_VARIABLES] = 
{    
   {
       .AmlVariable =NULL,
       .AmlVariableSize = 0,
       .AmlVariableName={0,0,0,0},
       .TableOffset = 0
    },
};
UINTN                      NumOfAmlVariables = 0;

#define MAX_ACPI_REGISTERS  256
static RegisterTableType    RegisteredCallbacks[MAX_ACPI_REGISTERS];
static UINT32               NumRegisteredCallbacks = 0;

#define MAX_AML_REGISTERS   256
static RegisterAmlVariableTable    RegisteredAmlVariableCallbacks[MAX_AML_REGISTERS];
static UINT32                      NumRegisteredAmlVariableCallbacks = 0;

CHAR8 *AMLVARIABLENAME_ALL = "ALL";

STATIC CHAR8                     FileName[MAX_PATHNAME];
STATIC CHAR8                     FilePath[MAX_PATHNAME];
STATIC HandleInfo                HandleInfoList[2];
STATIC PartiSelectFilter         HandleFilter;

EFI_EVENT ACPITableLoadEvent  = NULL;
static BOOLEAN RegistrationClosed = FALSE;
extern EFI_GUID gEfiACPITableLoadGuid;

//Root Partition
extern EFI_GUID gEfiEmmcGppPartition1Guid;
extern EFI_GUID gEfiEmmcUserPartitionGuid;
extern EFI_GUID gEfiUfsLU4Guid;

//Partition Types
extern EFI_GUID gEfiPartTypeSystemPartGuid;
extern EFI_GUID gEfiPlatPartitionTypeGuid;
/**
  Print AML variables in either Prefix or PostFix

  @param  GetMode                GETMODE value to be printed

  @return EFI_SUCCESS            Successful
  @return EFI_INVALID_PARAMETERS GetMode is invalid
**/
EFI_STATUS
PrintAmlVariables( GETMODE GetMode )
{ 
  UINTN i;
  UINTN j;
  UINT8* BytePtr;
  UINT8 ByteValue;
  AmlVariableEntry *CurrentAmlVariableArray;
  if (GetMode != GETMODE_PREFIX && GetMode != GETMODE_POSTFIX)
    return EFI_INVALID_PARAMETER;
  if (NumOfAmlVariables == 0)
  {
    return EFI_SUCCESS;
  }
  if (GetMode == GETMODE_PREFIX)
  {
    CurrentAmlVariableArray = PreFixAmlVariableArray;
    DEBUG ((ACPI_PLATFORM_DEBUG_PRINT, "Prefix variable values:\r\n"));
  }
  else
  {
    CurrentAmlVariableArray = PostFixAmlVariableArray;
    DEBUG ((ACPI_PLATFORM_DEBUG_PRINT, "Postfix variable values:\r\n"));
  }
    for(i=0; i<NumOfAmlVariables; i++)
    {
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT,"=================================\r\n"));
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "  Name: "));
        for(j=0; j<AML_NAMESTRING_LENGTH; j++)
          DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "%c", CurrentAmlVariableArray[i].AmlVariableName[j]));
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT," Length: %d Type:0x%02x\r\n  Value: ", CurrentAmlVariableArray[i].AmlVariableSize,
          ((AmlVariableEncoding*)CurrentAmlVariableArray[i].AmlVariable)->AmlVariableDataType));
        // Length of payload = AmlVariableSize-AML_NAMESPACE_HEADER_SIZE
        BytePtr = (UINT8*)((AmlVariableEncoding*)CurrentAmlVariableArray[i].AmlVariable);
        for(j=AML_NAMESPACE_HEADER_SIZE;j<CurrentAmlVariableArray[i].AmlVariableSize; j++)
        {
          ByteValue = *(BytePtr+j);
          DEBUG((ACPI_PLATFORM_DEBUG_PRINT,"%x", ByteValue));
        }
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT,"\r\n"));
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT,"=================================\r\n"));
    }
  return EFI_SUCCESS;
}
/**
  Store an ACPI table.
  Never release the allocated memory. When boot time is finished, the memory
  will be collected.

  @param  *Table                Pointer to the table to be copied
  @param   TableSize            Size of the table to be copied
  @param   Signature            Signature of the table to be copied
  @param   Revision             Revision of the table to be copied
  @param  *ArrayEntry           Pointer to the address the table to be copied

  @return EFI_SUCCESS           Successful
  @return EFI_OUT_OF_RESOURCES  Could not allocate memory to make a copy of the table.
  @return EFI_INVALID_PARAMETERS Table is NULL, TableSize is 0, Signature is 0, or 
                                ArrayEntry is NULL.

**/
EFI_STATUS
StoreAcpi( VOID *Table, UINTN TableSize, UINT32 Signature, AcpiTableEntry *ArrayEntry )
{
  if( Table == NULL || TableSize == 0 || Signature == 0 || ArrayEntry == NULL)
  {
    DEBUG(( DEBUG_ERROR, "StoreAcpi called with invalid parameters.\r\n",TableSize));
    return EFI_INVALID_PARAMETER;
  }

  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "TableSize = %d\r\n",TableSize));
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "Table Signature = %x\r\n",Signature));

  // Allocate.
  ArrayEntry->Table = (VOID *)AllocatePool(TableSize);
  if ( ArrayEntry->Table == NULL)
  {
    DEBUG(( DEBUG_ERROR, "StoreAcpi failed to allocate memory.\r\n",TableSize));
    return EFI_OUT_OF_RESOURCES;
  }    

  // copy table data.
  CopyMem( ArrayEntry->Table, Table, TableSize );
  ArrayEntry->TableSize = TableSize;
  ArrayEntry->Signature = Signature;

  return EFI_SUCCESS;
}

/**
  Store an Aml Variable in a table.
  Never release the allocated memory. When boot time is finished, the memory
  will be collected.

  @param  *AmlVariable                Pointer to the variable to be copied
  @param   AmlVariableSize            Size of the variable buffer to be copied
  @param  *AmlVariableName            Name of variable to be copied
  @param  *ArrayEntry                 Pointer to the address of the array to be copied into

  @return EFI_SUCCESS                 Successful
  @return EFI_OUT_OF_RESOURCES        Could not allocate memory to make a copy of the table.
  @return EFI_INVALID_PARAMETERS      AmlVariable is NULL, AmlVariableSize is 0, 
                                      AmlVariableName is NULL, or ArrayEntry is NULL.
**/
EFI_STATUS
StoreAmlVariable( VOID *AmlVariable, UINTN AmlVariableSize, 
  CHAR8 AmlVariableName[AML_NAMESTRING_LENGTH], UINTN TableOffset, 
  AmlVariableEntry *ArrayEntry )
{
  UINTN i;
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "=========Store Aml Var Start==========\r\n"));
  if( AmlVariable == NULL || AmlVariableSize == 0 || AmlVariableName == NULL || ArrayEntry == NULL)
  {
    DEBUG(( DEBUG_ERROR, "StoreAmlVariable called with invalid parameters.\r\n", AmlVariableSize));
    return EFI_INVALID_PARAMETER;
  }
  
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "AmlVariable Name = %c%c%c%c\r\n",AmlVariableName[0],AmlVariableName[1],AmlVariableName[2],AmlVariableName[3]));
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "AmlVariable Size = %d\r\n",AmlVariableSize));
  // Allocate.
  ArrayEntry->AmlVariable = (VOID *)AllocatePool(AmlVariableSize);
  if ( ArrayEntry->AmlVariable == NULL)
  {
    DEBUG(( DEBUG_ERROR, "StoreAcpi failed to allocate memory.\r\n",AmlVariableSize));
    return EFI_OUT_OF_RESOURCES;
  }    

  // copy AmlVariable data.
  CopyMem( ArrayEntry->AmlVariable, AmlVariable, AmlVariableSize );
  ArrayEntry->AmlVariableSize = AmlVariableSize;
  ArrayEntry->TableOffset = TableOffset;
  CopyMem( ArrayEntry->AmlVariableName, AmlVariableName, AML_NAMESTRING_LENGTH );
  DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "Storing variable:"));  
  for(i=0; i<AML_NAMESTRING_LENGTH; i++)    
   DEBUG((ACPI_PLATFORM_DEBUG_PRINT,"%c", AmlVariableName[i] ));
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "=========Store Aml Var End==========\r\n"));

  return EFI_SUCCESS;
}

/**
  Locate the first instance of a protocol.  If the protocol requested is an
  FV protocol, then it will return the first FV that contains the ACPI table
  storage file.

  @param  Instance      Return pointer to the first instance of the protocol

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_NOT_FOUND         The protocol could not be located.
  @return EFI_OUT_OF_RESOURCES  There are not enough resources to find the protocol.

**/
EFI_STATUS
LocateFvInstanceWithTables (
  OUT EFI_FIRMWARE_VOLUME2_PROTOCOL **Instance
  )
{
  EFI_STATUS                    Status;
  EFI_HANDLE                    *HandleBuffer;
  UINTN                         NumberOfHandles;
  EFI_FV_FILETYPE               FileType;
  UINT32                        FvStatus;
  EFI_FV_FILE_ATTRIBUTES        Attributes;
  UINTN                         Size;
  UINTN                         Index;
  EFI_FIRMWARE_VOLUME2_PROTOCOL *FvInstance;

  FvStatus = 0;

  //
  // Locate protocol.
  //
  Status = gBS->LocateHandleBuffer (
                   ByProtocol,
                   &gEfiFirmwareVolume2ProtocolGuid,
                   NULL,
                   &NumberOfHandles,
                   &HandleBuffer
                   );
  if (EFI_ERROR (Status)) {
    //
    // Defined errors at this time are not found and out of resources.
    //
    return Status;
  }



  //
  // Looking for FV with ACPI storage file
  //

  for (Index = 0; Index < NumberOfHandles; Index++) {
    //
    // Get the protocol on this handle
    // This should not fail because of LocateHandleBuffer
    //
    Status = gBS->HandleProtocol (
                     HandleBuffer[Index],
                     &gEfiFirmwareVolume2ProtocolGuid,
                     (VOID**) &FvInstance
                     );
    ASSERT_EFI_ERROR (Status);

    //
    // See if it has the ACPI storage file
    //
    Status = FvInstance->ReadFile (
                           FvInstance,
                           (EFI_GUID*)PcdGetPtr (PcdAcpiTableStorageFile),
                           NULL,
                           &Size,
                           &FileType,
                           &Attributes,
                           &FvStatus
                           );

    //
    // If we found it, then we are done
    //
    if (Status == EFI_SUCCESS) {
      *Instance = FvInstance;
      break;
    }
  }

  //
  // Our exit status is determined by the success of the previous operations
  // If the protocol was found, Instance already points to it.
  //

  //
  // Free any allocated buffers
  //
  gBS->FreePool (HandleBuffer);

  return Status;
}


/**
  This function calculates and updates an UINT8 checksum.

  @param  Buffer          Pointer to buffer to checksum
  @param  Size            Number of bytes to checksum

**/
VOID
AcpiPlatformChecksum (
  IN UINT8      *Buffer,
  IN UINTN      Size
  )
{
  UINTN ChecksumOffset;

  ChecksumOffset = OFFSET_OF (EFI_ACPI_DESCRIPTION_HEADER, Checksum);

  //
  // Set checksum to 0 first
  //
  Buffer[ChecksumOffset] = 0;

  //
  // Update checksum value
  //
  Buffer[ChecksumOffset] = CalculateCheckSum8(Buffer, Size);
}

/**
  This function loads ACPI files from the specified FolderPath.

  @param  FolderPath      String for path of ACPI folder

**/
EFI_STATUS
LoadFromFilesystem(
  CHAR8 *FolderPath
  )
{
  EFI_STATUS                    Status         = EFI_LOAD_ERROR;
  EFI_OPEN_FILE                 *Dir           = NULL;
  EFI_OPEN_FILE                 *File          = NULL;
  EFI_FILE_INFO                 *DirInfo       = NULL;
  UINTN                         ReadSize       = 0;
  VOID                          *FileBuffer    = NULL;
  UINTN                         FileBufferSize = 0;
  UINTN                         TableSize;
  UINT32                        Signature;
  UINTN                         TableCount     = 0;

  // initialize the table count
  NumOfAcpiTables = 0;

  /* Attempt to load from any available filesystem */
  Dir = EfiOpen (FolderPath, EFI_FILE_MODE_READ, 0);
  if (Dir == NULL) {
    goto Done;
  }

  if ((Dir->Type == EfiOpenFileSystem) || (Dir->Type == EfiOpenBlockIo)) {
    if (Dir->FsFileInfo ==  NULL) {
      goto Done;
    }

    if (!(Dir->FsFileInfo->Attribute & EFI_FILE_DIRECTORY)) {
      goto Done;
    }

    Dir->FsFileHandle->SetPosition (Dir->FsFileHandle, 0);
    while (1) {
      // First read gets the size
      ReadSize = 0;
      Status = Dir->FsFileHandle->Read (Dir->FsFileHandle, &ReadSize, DirInfo);
      if (Status == EFI_BUFFER_TOO_SMALL) {
        // Allocate the buffer for the real read
        DirInfo = AllocatePool (ReadSize);
        if (DirInfo == NULL) {
          goto Done;
        }
        
        // Read the data
        Status = Dir->FsFileHandle->Read (Dir->FsFileHandle, &ReadSize, DirInfo);
        if ((EFI_ERROR (Status)) || (ReadSize == 0)) {
          goto Done;
        }
      } else {
        goto Done;
      }
      
      if (DirInfo->Attribute & EFI_FILE_DIRECTORY) {
        // Silently skip folders
        FreePool (DirInfo);
        DirInfo = NULL;
        continue;
      }

      AsciiStrnCpy (FilePath, FolderPath, MAX_PATHNAME);
      UnicodeStrToAsciiStr (DirInfo->FileName, FileName);
      AsciiStrnCat (FilePath, FileName, MAX_PATHNAME);

      DEBUG ((DEBUG_LOAD, "ACPI: Loading %a from FS.\r\n", FilePath));

      File = EfiOpen (FilePath, EFI_FILE_MODE_READ, 0);
      if (File == NULL) {
        goto Done;
      }

      Status = EfiReadAllocatePool (File, &FileBuffer, &FileBufferSize);

      if ((EFI_ERROR (Status)) || (FileBufferSize == 0)) {
        goto Done;
      }

      //
      // Add the table
      //
      TableSize = ((EFI_ACPI_DESCRIPTION_HEADER *) FileBuffer)->Length;
      ASSERT (FileBufferSize >= TableSize);
      Signature = ((EFI_ACPI_DESCRIPTION_HEADER *) FileBuffer)->Signature;
     
      // Checksum ACPI table
      AcpiPlatformChecksum ((UINT8*)FileBuffer, TableSize);
      // store for measurement
      Status = StoreAcpi( FileBuffer, TableSize, Signature, &PreFixArray[NumOfAcpiTables] );
      if (EFI_ERROR (Status)) 
      {
        DEBUG((EFI_D_WARN, "StoreAcpi failed. Signature: %x, Status: %d.\r\n",
          PreFixArray[NumOfAcpiTables].Signature, Status));
        goto Done;
      }

      // increase the table count
      NumOfAcpiTables++;

      TableCount++;

      EfiClose (File);
      File = NULL;
      FreePool (DirInfo);
      DirInfo = NULL;
      FileBuffer = NULL;
    }
  }

Done:
  if (0 == TableCount) {
    Status = EFI_NOT_FOUND;
  }
  else {
    DEBUG ((DEBUG_LOAD, "ACPI: Loaded %d tables\r\n", TableCount));
  }

  if (Dir != NULL) {
    EfiClose (Dir);
  }
  if (File != NULL) {
    EfiClose (File);
  }
  if (DirInfo != NULL) {
    FreePool (DirInfo);
  }

  return Status;
}

/**
  Loads ACPI from a file path specified by PCD

  @return EFI_ABORTED if any error
  @return EFI_SUCCESS if successful

**/
EFI_STATUS
LoadFromImage (
  VOID
  )
{
  EFI_STATUS                     Status;
  EFI_FIRMWARE_VOLUME2_PROTOCOL  *FwVol;
  INTN                           Instance;
  EFI_ACPI_COMMON_HEADER         *CurrentTable;
  UINT32                         FvStatus;
  UINTN                          TableSize;
  UINT32                         Signature;
  UINTN                          Size;

  Instance     = 0;
  CurrentTable = NULL;

  // initialize the table count
  NumOfAcpiTables = 0;

  //
  // Locate the firmware volume protocol
  //
  Status = LocateFvInstanceWithTables (&FwVol);
  if (EFI_ERROR (Status)) {
    return EFI_ABORTED;
  }
  //
  // Read tables from the storage file.
  //
  while (Status == EFI_SUCCESS) {

    Status = FwVol->ReadSection (
                      FwVol,
                      (EFI_GUID*)PcdGetPtr (PcdAcpiTableStorageFile),
                      EFI_SECTION_RAW,
                      Instance,
                      (VOID**) &CurrentTable,
                      &Size,
                      &FvStatus
                      );
    if (!EFI_ERROR(Status)) {
      //
      // Add the table
      //

      TableSize = ((EFI_ACPI_DESCRIPTION_HEADER *) CurrentTable)->Length;
      ASSERT (Size >= TableSize);
      Signature = ((EFI_ACPI_DESCRIPTION_HEADER *) CurrentTable)->Signature;

      // Checksum ACPI table
      AcpiPlatformChecksum ((UINT8*)CurrentTable, TableSize);
      // store for measurement
      Status = StoreAcpi( CurrentTable, TableSize, Signature, &PreFixArray[NumOfAcpiTables] );
      if (EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, " StoreAcpi failed. Signature: %x, Status: %d.\r\n",
          PreFixArray[NumOfAcpiTables].Signature, Status));
        return Status;
      }

      // increase the table count
      NumOfAcpiTables++;

      //
      // Increment the instance
      //
      Instance++;
      CurrentTable = NULL;
    }
  }

  DEBUG ((DEBUG_LOAD, "ACPI: Loaded %d tables\r\n", Instance));

  return EFI_SUCCESS;
}

/**
  Loads ACPI from a partition specified by Root Device type GUID 
  Partition Type GUID and Removable or Non-removable media
  
  @param  Path            Directory containging ACPI table files
  @param  RootDeviceType  Root Device Type GUID 
  @param  Partition Type  GUID eg: gEfiPartTypeSystemPartGuid
  @param  SelectNonRemovable  
            TRUE for Non-removable 
            FALSE for Removable
  @return EFI_SUCCESS if successful
**/
 
EFI_STATUS
LoadAcpiFromVolume(
  CHAR16        *Path,
  EFI_GUID      *RootDeviceType,
  EFI_GUID      *PartitionType,
  BOOLEAN       SelectNonRemovable
)
{
  EFI_STATUS Status;
  EFI_SIMPLE_FILE_SYSTEM_PROTOCOL   *Volume;
  EFI_FILE_HANDLE                   RootFileHandle;
  EFI_FILE_HANDLE                   DirFileHandle;
  EFI_FILE_HANDLE                   AcpiFileHandle;
  EFI_FILE_INFO                     *DirInfo = NULL, *Info = NULL;
  EFI_OPEN_FILE                     *AcpiFile = NULL;
  VOID                              *AcpiFileBuffer = NULL;
  UINTN                             AcpiFileBufferSize = 0, BufferSize = 0;
  UINTN                             ReadSize;
  UINTN                             TableSize;
  UINT32                            Signature;
  UINTN                             TableCount = 0;
  UINT32                            MaxHandles;
  UINT32                            Attrib = 0;
  MaxHandles = sizeof(HandleInfoList)/sizeof(*HandleInfoList);

  if( ( (RootDeviceType == NULL) && (PartitionType == NULL) ) || (Path == NULL) ) 
    return EFI_INVALID_PARAMETER;

  // initialize the table count
  NumOfAcpiTables = 0;

  Attrib |= BLK_IO_SEL_PARTITIONED_GPT;

  if (SelectNonRemovable == TRUE)
    Attrib |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;

  if (RootDeviceType != NULL)
    Attrib |= BLK_IO_SEL_MATCH_ROOT_DEVICE;

  if (PartitionType != NULL)
    Attrib |= BLK_IO_SEL_MATCH_PARTITION_TYPE_GUID;

  HandleFilter.PartitionType = PartitionType;
  HandleFilter.RootDeviceType = RootDeviceType;
  HandleFilter.VolumeName = 0;

  Status = GetBlkIOHandles(Attrib, &HandleFilter, HandleInfoList, &MaxHandles);

  if(Status == EFI_SUCCESS) {
    if(MaxHandles == 0) {
      return EFI_NO_MEDIA;
    }
    if(MaxHandles != 1) {
      //Unable to deterministically load from single partition 
      DEBUG(( EFI_D_WARN, "LoadAcpiFromVolume(Path: %s): multiple partitions found \r\n", Path));
      return EFI_LOAD_ERROR;
    }
  }

  // File the file system interface to the device
  Status = gBS->HandleProtocol (
                  HandleInfoList[0].Handle,
                  &gEfiSimpleFileSystemProtocolGuid,
                  (VOID *) &Volume
                  );
  if(Status != EFI_SUCCESS) {
    DEBUG ((EFI_D_WARN, "ACPI: LoadAcpiFromVolume() unable to find file system interface.\r\n" ));
    return Status;
  }

  // Open the root directory of the volume
  if (!EFI_ERROR (Status)) {
    Status = Volume->OpenVolume (
                      Volume,
                      &RootFileHandle
                      );
  }
  if(Status != EFI_SUCCESS) {
    DEBUG ((EFI_D_WARN, "ACPI: LoadAcpiFromVolume() failed to open volume \r\n" ));
    return Status;
  }
  
  //Open Directory 
  Status = RootFileHandle->Open(RootFileHandle,
                   &DirFileHandle,
                   Path,
                   EFI_FILE_MODE_READ,
                   0);
  if ((Status != EFI_SUCCESS) || (DirFileHandle == NULL))
  {
    DEBUG ((EFI_D_WARN, "ACPI: LoadAcpiFromVolume() failed to open directory\r\n" ));
    return Status;
  }
  
  Status = DirFileHandle->SetPosition(DirFileHandle, 0);
  if(Status != EFI_SUCCESS)
     return Status;

  while (1) {
      // Look at each directory listing
      // First read gets the size
      ReadSize = 0;
      Status = DirFileHandle->Read (DirFileHandle, &ReadSize, DirInfo);
      if (Status == EFI_BUFFER_TOO_SMALL) {
        // Allocate the buffer for the real read
        DirInfo = AllocatePool (ReadSize);
        if (DirInfo == NULL) {
          goto Done;
        }
        
        // Read the data
        Status = DirFileHandle->Read (DirFileHandle, &ReadSize, DirInfo);
        if ((EFI_ERROR (Status)) || (ReadSize == 0)) {
            goto Done;
        }
      } else {
        goto Done;
      }
      
      if (DirInfo->Attribute & EFI_FILE_DIRECTORY) {
        // Silently skip folders
        FreePool (DirInfo);
        DirInfo = NULL;
        continue;
      }
    UnicodeStrToAsciiStr (DirInfo->FileName, FileName);
    DEBUG ((DEBUG_LOAD, "ACPI: Loading %a\r\n", FileName));
    Status = DirFileHandle->Open(DirFileHandle,
                   &AcpiFileHandle,
                   DirInfo->FileName,
                   EFI_FILE_MODE_READ,
                   0);
    if(Status != EFI_SUCCESS)
       return Status;

    //Get file size info
    BufferSize  = SIZE_OF_EFI_FILE_INFO + 200;
    do{
      Info   = NULL;
      Info = AllocatePool (BufferSize);
      if (Info == NULL)
        goto Done;
      Status = AcpiFileHandle->GetInfo (
                           AcpiFileHandle,
                           &gEfiFileInfoGuid,
                           &BufferSize,
                           Info
                        );
      if(Status == EFI_SUCCESS)
        break;
      if (Status != EFI_BUFFER_TOO_SMALL) {
        FreePool (Info);
        goto Done;
      }
      FreePool (Info);
    } while (TRUE);

    AcpiFileBufferSize = Info->FileSize;
    FreePool (Info);
    AcpiFileBuffer = AllocatePool(AcpiFileBufferSize);
    // AllocatePool failed for some reason
    if (AcpiFileBuffer == NULL)
    {
      DEBUG(( EFI_D_WARN, " ACPI: AcpiFileBuffer is null\r\n"));
      goto Done;
    }
    // Read file content
    Status = AcpiFileHandle->Read (AcpiFileHandle, &AcpiFileBufferSize, AcpiFileBuffer);
    if ((EFI_ERROR (Status)) || (AcpiFileBufferSize == 0)) {
      UnicodeStrToAsciiStr (DirInfo->FileName, FileName);
      DEBUG ((EFI_D_WARN, "ACPI: LoadAcpiFromVolume() failed reading %a\r\n", FileName));
      goto Done;
    }
   
    // Add the table
    TableSize = ((EFI_ACPI_DESCRIPTION_HEADER *) AcpiFileBuffer)->Length;
    ASSERT (AcpiFileBufferSize >= TableSize);
    Signature = ((EFI_ACPI_DESCRIPTION_HEADER *) AcpiFileBuffer)->Signature;

    // Checksum ACPI table
    AcpiPlatformChecksum ((UINT8*)AcpiFileBuffer, TableSize);
    DEBUG ((ACPI_PLATFORM_DEBUG_PRINT, "Signature: %x Length: %x Checksum %x\r\n", Signature, TableSize, 
      ((EFI_ACPI_DESCRIPTION_HEADER *) AcpiFileBuffer)-> Checksum ));
    // store for measurement
    Status = StoreAcpi( AcpiFileBuffer, TableSize, Signature, &PreFixArray[NumOfAcpiTables] );
    if (EFI_ERROR (Status)) 
    {
      DEBUG(( EFI_D_WARN, "StoreAcpi failed. Signature: %x, Status: %d.\r\n",
        PreFixArray[NumOfAcpiTables].Signature, Status));
      goto Done;
    }

    // increase the table count
    NumOfAcpiTables++;

    TableCount++;

    FreePool (DirInfo);
    DirInfo = NULL;
    FreePool(AcpiFileBuffer);
    AcpiFileBuffer = NULL;
  }

  Done:
  if (0 == TableCount) {
    Status = EFI_NOT_FOUND;
  }
  else {
    DEBUG ((DEBUG_LOAD, "ACPI: Loaded %d tables\r\n", TableCount));
  }

  if (AcpiFile != NULL)
    EfiClose (AcpiFile);

  if (DirInfo != NULL)
    FreePool (DirInfo);
  
  if( DirFileHandle != NULL)
    DirFileHandle->Close(DirFileHandle); 

  return Status;
}

/**
  Allocates space for a copy of the ACPI tables, copies the ACPI table array 
  that buffer, and returns the copy.
  
  @param  Destination           Contains the copy of the array when the function returns.
  @param  TableSize             Contains the size of the destination when the 
                                function returns.
  @param  AcpiTablesArray       The ACPI tables array to be copied.
  @param  NumOfAcpiTAbles       The number of tables in the array.
  @return EFI_SUCCESS           Successful
  @return EFI_OUT_OF_RESOURCES  There are not enough resources to create the buffer.
*/
STATIC 
EFI_STATUS CopyAcpiTables (
  OUT VOID          **Destination, 
  OUT UINTN          *TableSize,
  IN AcpiTableEntry  *AcpiTablesArray,
  IN UINTN            NumOfTables
  )
{
  UINTN i = 0;
  UINTN Offset = 0;

  *TableSize = 0;

  // Calculate the amount of space that needs to be allocated.
  for (i=0; i<NumOfTables; i++) 
    *TableSize += AcpiTablesArray[i].TableSize;

  // Allocate a buffer for that table.
  *Destination = AllocatePool(*TableSize);
  if (*Destination == NULL) {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to allocate buffer.\r\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  // Copy the tables into the buffer.
  for (i=0; i<NumOfTables; i++) {
    CopyMem((UINT8 *)*Destination + Offset, (UINT8 *)AcpiTablesArray[i].Table,
      AcpiTablesArray[i].TableSize);
    Offset += AcpiTablesArray[i].TableSize;
  }

  return EFI_SUCCESS;
}
/**
  Allocates space for a copy of all the AML Variable buffers, copies the variables to
  that buffer, and returns the copy.
  
  @param  Destination           Contains the full buffer of variables when the function returns.
  @param  AmlVariableListSize   Contains the size of the destination when the function returns.
  @param  AmlVariableEntry      The aml Variables to be copied.
  @param  NumOfAmlVariables     The number of variables in the array.
  @return EFI_SUCCESS           Successful
  @return EFI_OUT_OF_RESOURCES  There are not enough resources to create the buffer.
*/
STATIC 
EFI_STATUS CopyAmlVariableTables (
  OUT VOID            **Destination, 
  OUT UINTN            *AmlVariableListSize,
  IN AmlVariableEntry  *AmlVariableEntry,
  IN UINTN              NumOfAmlVariables
  )
{
  UINTN i = 0;
  UINTN Offset = 0;

  *AmlVariableListSize = 0;

  // Calculate the amount of space that needs to be allocated. It is AML Variable buffer
  // plus size of AmlVariableSize, as we insert the size of the buffer between buffers
  for (i=0; i<NumOfAmlVariables; i++) 
    *AmlVariableListSize += (AmlVariableEntry[i].AmlVariableSize + sizeof(UINT8));

  // Allocate a buffer for that table.
  *Destination = AllocatePool(*AmlVariableListSize);
  if (*Destination == NULL) {
    DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to allocate buffer.\r\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  // Copy the tables into the buffer.
  for (i=0; i<NumOfAmlVariables; i++) {
    CopyMem((UINT8 *)*Destination + Offset, &(AmlVariableEntry[i].AmlVariableSize),sizeof(UINT8));
      Offset += sizeof(UINT8);
    CopyMem((UINT8 *)*Destination + Offset, (UINT8 *)AmlVariableEntry[i].AmlVariable,
      AmlVariableEntry[i].AmlVariableSize);
    Offset += AmlVariableEntry[i].AmlVariableSize;
  }

  return EFI_SUCCESS;
}


/**
 Call each of the registered GetTable callback functions.

 @param  IsPrefix              TRUE to call the callback functions with the 
                               pre fixed tables, FALSE to call the callback 
                               functions with the post fixed tables.
 @return EFI_SUCCESS           Successful
 @return EFI_OUT_OF_RESOURCES  Out of memory.
 @return EFI_INVALID_PARAMETER GetMode is not GETMODE_PREFIX or GETMODE_POSTFIX.
*/
STATIC 
EFI_STATUS CallGetTableCallbacks ( 
  IN BOOLEAN GetMode
 )
{
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINTN                           i;
  UINTN                           j;
  VOID                           *TablePtr;
  UINTN                           TableSize;
  AcpiTableEntry                 *CurrentAcpiTables = NULL;
 
  if (GetMode != GETMODE_PREFIX && GetMode != GETMODE_POSTFIX)
    return EFI_INVALID_PARAMETER;

  if (GetMode == GETMODE_PREFIX)
    CurrentAcpiTables = PreFixArray;
  else
    CurrentAcpiTables = PostFixArray;

  // Process each registration and call GetTable callback functions as appropriate
  // Loop through each of the registered callbacks
  for (i=0; i<NumRegisteredCallbacks; i++)
  {
    // If GetTableCallback is not registered or it's not registered for the current
    // GETMODE, skip this callback.
    if (RegisteredCallbacks[i].GetTableCallback == NULL 
        || (RegisteredCallbacks[i].GetMode & GetMode) != GetMode) 
      continue;

    // If the registered signature is ACPI_TABLE_SIGNATURE_ALL, then copy all of the tables and 
    // call the callback.
    if (RegisteredCallbacks[i].TableSignature == ACPI_TABLE_SIGNATURE_ALL)
    {
      // Copy the tables
      Status = CopyAcpiTables(&TablePtr, &TableSize, 
        (GetMode == GETMODE_PREFIX ? PreFixArray : PostFixArray), NumOfAcpiTables);
      if (EFI_ERROR(Status)) {
        DEBUG(( EFI_D_WARN, "Failed to copy the ACPI table array.\r\n"));
        continue;
      }

      // Call the callback function.
      Status = RegisteredCallbacks[i].GetTableCallback(TablePtr, 
        TableSize, GetMode == GETMODE_PREFIX);
      if ( EFI_ERROR (Status)) {
        DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call GetTable callback function.\r\n"));
        continue;
      }
    }
    // If the registered callback has a signature, find the table with that 
    // signature and call the callback with it.
    else 
    {
      // Loop through each of the ACPI tables
      for (j=0; j<NumOfAcpiTables; j++) {
        // If the signature doesn't match, skip this ACPI table.
        if(RegisteredCallbacks[i].TableSignature != CurrentAcpiTables[j].Signature) 
            continue;

        // Allocate a buffer for the table to be passed to the callback function,
        // and copy the ACPI table to the buffer.
        TablePtr = (VOID *)AllocatePool(CurrentAcpiTables[j].TableSize);
        if ( TablePtr == NULL) {
          DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to allocate buffer.\r\n"));
          return EFI_OUT_OF_RESOURCES;
        }
        CopyMem((UINT8 *)TablePtr, (UINT8 *)CurrentAcpiTables[j].Table, 
          CurrentAcpiTables[j].TableSize);

        // Call the callback function.
        DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "Calling GetTableCallback for %x at %x\r\n", 
          CurrentAcpiTables[j].Signature, RegisteredCallbacks[i].GetTableCallback));

        Status = RegisteredCallbacks[i].GetTableCallback(TablePtr, 
          CurrentAcpiTables[j].TableSize, GetMode == GETMODE_PREFIX);

        if ( EFI_ERROR (Status)) {
          DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call GetTableCallback "
            "for signature %x and function address %x. Status: %d\r\n",
            CurrentAcpiTables[j].Signature, 
            RegisteredCallbacks[i].GetTableCallback, Status));
        }
        break;
      }

      // If we got through all the acpi tables without finding the right one, call the
      // callback function passing in NULL for the table.
      if (j == NumOfAcpiTables) {
        DEBUG(( EFI_D_WARN, "Callback registered with invalid signature: %d.\r\n",
            RegisteredCallbacks[i].TableSignature));
        Status = RegisteredCallbacks[i].GetTableCallback(NULL, 
          0, GetMode == GETMODE_PREFIX);
        if ( EFI_ERROR (Status)) {
          DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call GetTableCallback "
            "function.\r\n"));
        }
      }
    }
  }
  return Status;
}

/**
 Call the SetTable callback functions, passing them the pre fixed tables, to get the 
 the post fixed tables. The post fixed tables are copied to PostFixArray.

 @return EFI_SUCCESS           Successful
 @return EFI_OUT_OF_RESOURCES  Out of memory.
*/
STATIC 
EFI_STATUS CallSetTableCallbacks ( 
 )
{
  UINTN                           CallbackIndex;
  UINTN                           TableIndex;
  BOOLEAN                         CallbackCalled[MAX_ACPI_REGISTERS];
  UINTN                           Counter;
  
  //set CallbacksCalled to false for each registration
  for (Counter = 0; Counter < MAX_ACPI_REGISTERS; Counter++)
  {
    CallbackCalled[Counter] = 0;
  }
  // Loop through each of the ACPI tables
  for (TableIndex = 0; TableIndex < NumOfAcpiTables; TableIndex++)
  {
    // TODO: remove this and replace with a callback.
    if(EFI_ACPI_5_0_BOOT_GRAPHICS_RESOURCE_TABLE_SIGNATURE == PreFixArray[TableIndex].Signature)
    {
      EFI_STATUS Status = EFI_SUCCESS;
      VOID *TablePtr = NULL;
      UINTN TableSize = 0;

      TableSize = PreFixArray[TableIndex].TableSize;
      TablePtr = AllocatePool(TableSize);
      if (TablePtr == NULL)
      {
        DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to allocate buffer.\r\n"));
        return EFI_OUT_OF_RESOURCES;
      }

      CopyMem(TablePtr, PreFixArray[TableIndex].Table, TableSize);

      // Enable BGRT logo processing.
      ProcessBGRT(TablePtr);
      // Checksum ACPI table
      AcpiPlatformChecksum ((UINT8*)TablePtr, TableSize);
      // Copy the table to the PostFixArray.
      Status = StoreAcpi(TablePtr, TableSize, PreFixArray[TableIndex].Signature,
        &PostFixArray[TableIndex]);
      FreePool(TablePtr);
      if (EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, "StoreAcpi failed. Signature: %x, Status: %d.\r\n",
          PreFixArray[TableIndex].Signature, Status));
        return Status;
      }
      continue;
    }

    // Loop through each of the registered callback functions until we 
    // find one with a matching signature.
    for (CallbackIndex = 0; CallbackIndex < NumRegisteredCallbacks; CallbackIndex++)
    {
      if ((PreFixArray[TableIndex].Signature 
          == RegisteredCallbacks[CallbackIndex].TableSignature)
        && RegisteredCallbacks[CallbackIndex].SetTableCallback != NULL)
      {
        EFI_STATUS Status = EFI_SUCCESS;
        VOID *TablePtr = NULL;
        UINTN TableSize = 0;

        TableSize = PreFixArray[TableIndex].TableSize;
        TablePtr = AllocatePool(TableSize);
        if (TablePtr == NULL)
        {
          DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to allocate buffer.\r\n"));
          return EFI_OUT_OF_RESOURCES;
        }

        CopyMem(TablePtr, PreFixArray[TableIndex].Table, TableSize);

        // Call the callback function.
        DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "Calling SetTableCallback for %x at %x\r\n", PreFixArray[TableIndex].Signature, 
          RegisteredCallbacks[CallbackIndex].SetTableCallback));

        Status = RegisteredCallbacks[CallbackIndex].SetTableCallback(
          &TablePtr, &TableSize);
        if ( EFI_ERROR (Status)) 
        {
          DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call SetTableCallback "
            "for signature %x and function address %x. Status: %d\r\n",
            PreFixArray[TableIndex].Signature, 
            RegisteredCallbacks[CallbackIndex].SetTableCallback, Status));
          FreePool(TablePtr);
          return Status;
        }
        // Mark this callback as called.
        CallbackCalled[CallbackIndex] = TRUE;

        // Calculate the checksum.
        AcpiPlatformChecksum ((UINT8*)TablePtr, TableSize);
        /* Our plan is to have client driver calculate checksum, and verify in AcpiPlatformDxe, however
         * this requires coordination of teams as it will break existing functionality. Code below is to
         * be uncommented when ready and remove above checksum calculation.
 
        //Calculate the checksum of table and confirm with checksum value in the table
        Checksum = CalculateCheckSum8(TablePtr, TableSize);
        if( ((UINT8 *)TablePtr)[OFFSET_OF (EFI_ACPI_DESCRIPTION_HEADER, Checksum)] != Checksum)
        {
          DEBUG(( EFI_D_WARN, " Checksum %x for table %x failed to match checksum %x in the table\r\n Client drivers must update checksum", 
            Checksum, PreFixArray[TableIndex].Signature, ((UINT8 *)TablePtr)[OFFSET_OF (EFI_ACPI_DESCRIPTION_HEADER, Checksum)]  ));
          return EFI_INVALID_PARAMETER;
        }
        */

        // Copy the table to the PostFixArray.
        Status = StoreAcpi(TablePtr, TableSize, PreFixArray[TableIndex].Signature,
          &PostFixArray[TableIndex]);
        FreePool(TablePtr);
        if (EFI_ERROR (Status)) 
        {
          DEBUG(( EFI_D_WARN, "StoreAcpi failed. Signature: %x, Status: %d.\r\n",
            PreFixArray[TableIndex].Signature, Status));
          return Status;
        }
        break;
      }
    }
    // If we got through all of the registered callbacks without finding 
    // one with a matching signature, simply copy the table from the prefix
    // to the postfix table.
    if (CallbackIndex == NumRegisteredCallbacks)
    {
      EFI_STATUS Status = StoreAcpi(PreFixArray[TableIndex].Table, PreFixArray[TableIndex].TableSize,
        PreFixArray[TableIndex].Signature, &PostFixArray[TableIndex]);
      if (EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, "StoreAcpi failed. Signature: %x, Status: %d.\r\n",
          PreFixArray[TableIndex].Signature, Status));
        return Status;
      }
    }
  }

  // Loop through all the callbacks that haven't been called, and call them
  // passing in NULL for the table to the indicate an invalid signature.
  for (CallbackIndex = 0; CallbackIndex < NumRegisteredCallbacks; CallbackIndex++)
  {
    if (!CallbackCalled[CallbackIndex] 
      && RegisteredCallbacks[CallbackIndex].SetTableCallback != NULL)
    {
      EFI_STATUS Status = EFI_SUCCESS;
      RegisteredCallbacks[CallbackIndex].SetTableCallback(NULL, NULL);
      if ( EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call SetTableCallback "
          "function.\r\n"));
        return Status;
      }
    }
  }
  return EFI_SUCCESS;
}

/**
 Call each of the registered AMLVariableGetTable callback functions.

 @param  IsPrefix              TRUE to call the callback functions with the 
                               pre fixed tables, FALSE to call the callback 
                               functions with the post fixed tables.
 @return EFI_SUCCESS           Successful
 @return EFI_OUT_OF_RESOURCES  Out of memory.
 @return EFI_INVALID_PARAMETER GetMode is not GETMODE_PREFIX or GETMODE_POSTFIX.
*/
STATIC 
EFI_STATUS CallGetAmlVariableCallbacks ( 
  IN BOOLEAN GetMode
 )
{
  EFI_STATUS                      Status = EFI_SUCCESS;
  UINTN                           i;
  UINTN                           j;
  VOID                           *AmlVariableBufferPtr;
  UINTN                           AmlVariableBufferSize;
  AmlVariableEntry               *CurrentAmlVariables = NULL;
  
  if (GetMode != GETMODE_PREFIX && GetMode != GETMODE_POSTFIX)
    return EFI_INVALID_PARAMETER;

  if (GetMode == GETMODE_PREFIX)
    CurrentAmlVariables = PreFixAmlVariableArray;
  else
    CurrentAmlVariables = PostFixAmlVariableArray;

  // Process each registration and call the callback functions as appropriate
  // Loop through each of the registered callbacks
  for (i=0; i<NumRegisteredAmlVariableCallbacks; i++)
  {
    // If GetAmlVariableCallback is not registered or it's not registered for the current
    // GETMODE, skip this callback.
    if (RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback == NULL 
        || (RegisteredAmlVariableCallbacks[i].GetMode & GetMode) != GetMode) 
      continue;

    // If the registered variable name is 'ALL', then copy all of the variables and 
    // call the callback.
    if (AsciiStrnCmp(RegisteredAmlVariableCallbacks[i].VariableName, AMLVARIABLENAME_ALL, AML_NAMESTRING_LENGTH) == 0)
    {
      // Copy the tables
      Status = CopyAmlVariableTables(&AmlVariableBufferPtr, &AmlVariableBufferSize, 
        (GetMode == GETMODE_PREFIX ? PreFixAmlVariableArray : PostFixAmlVariableArray), NumOfAmlVariables);
      if (EFI_ERROR(Status)) {
        DEBUG(( EFI_D_WARN, " Failed to copy the Aml variable table array.\r\n"));
        continue;
      }

      // Call the callback function.
      Status = RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback(AmlVariableBufferPtr, 
        AmlVariableBufferSize, GetMode == GETMODE_PREFIX);
      if ( EFI_ERROR (Status)) {
        DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call GetAmlVariable callback "
          "function.\r\n"));
        continue;
      }
    }
    // If the registered callback has a specific variable association, find the specific 
    // callback for it.
    else 
    {
      // Loop through each of the Aml Variable entries 
      for (j=0; j<NumOfAmlVariables; j++) {
        // If the name doesn't match then move on.
        if(AsciiStrnCmp(RegisteredAmlVariableCallbacks[i].VariableName,
             CurrentAmlVariables[j].AmlVariableName, AML_NAMESTRING_LENGTH)!=0) 
            continue;

        // Allocate a buffer for the variable entry to be passed to the callback function,
        // and copy the entry to the buffer.
        AmlVariableBufferPtr = (VOID *)AllocatePool(CurrentAmlVariables[j].AmlVariableSize);
        if ( AmlVariableBufferPtr == NULL) {
          DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to allocate buffer.\r\n"));
          return EFI_OUT_OF_RESOURCES;
        }
        CopyMem((UINT8 *)AmlVariableBufferPtr, (UINT8 *)CurrentAmlVariables[j].AmlVariable, 
          CurrentAmlVariables[j].AmlVariableSize);

        // Call the callback function.
        DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " Calling GetAmlVariableCallback for variable %x and "
          "function address %x.\r\n", CurrentAmlVariables[j].AmlVariableName, 
          RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback));

        Status = RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback(AmlVariableBufferPtr, 
          CurrentAmlVariables[j].AmlVariableSize, GetMode == GETMODE_PREFIX);

        if ( EFI_ERROR (Status)) {
          DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call GetAmlVariableCallback "
            "for signature %x and function address %x. Status: %d\r\n",
            CurrentAmlVariables[j].AmlVariableName, 
            RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback, Status));
        }
        else
        {
          DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " Successfully called GetAmlVariableCallback for signature "
            "%x and function address %x.\r\n", CurrentAmlVariables[j].AmlVariableName, 
            RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback));
        }
        break;
      }

      // If we got through all the variables without finding the right one, call the
      // callback function passing in NULL for the variable.
      if (j == NumOfAmlVariables) {
        DEBUG(( EFI_D_WARN, " Callback registered with invalid signature: %d.\r\n",
            RegisteredAmlVariableCallbacks[i].VariableName));
        Status = RegisteredAmlVariableCallbacks[i].GetAmlVariableCallback(NULL, 
          0, GetMode == GETMODE_PREFIX);
        if ( EFI_ERROR (Status)) {
          DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call GetAmlVariableCallback "
            "function.\r\n"));
        }
      }
    }
  }
  return Status;
}
STATIC 
EFI_STATUS AmlVariableUpdateVerification ( 
  IN AmlVariableEntry  AmlVariableEntry,
  IN VOID              *AmlVariableBuffer
 )
{
  UINT8   *OriginalPtr = AmlVariableEntry.AmlVariable;
  UINT8   *UpdatePtr = AmlVariableBuffer;
  UINTN    i;
  BOOLEAN  IsStringTerminated = FALSE;
  if( AmlVariableEntry.AmlVariable == NULL || AmlVariableBuffer == NULL)
  {
    DEBUG((EFI_D_WARN, "  Verification failed for SetAmlVariableCallback: Buffer is NULL\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  // Header should not change, so verify each byte is the same in both buffers
  for(i=0; i<AML_NAMESPACE_HEADER_SIZE; i++)
  {
    if( *(OriginalPtr+i) != *(UpdatePtr+i) )
    {
      DEBUG((EFI_D_WARN, "  Verification failed for SetAmlVariableCallback: Header was changed\r\n"));
      return EFI_INVALID_PARAMETER;
    }
  }
  /* All that's left to check is the Data Value. At this point we know the DataType hasn't changed and the buffer
   * size won't change. So we just have to make sure that a string type is correctly terminated with null char. If
   * the client driver wants to reduce string length, they must pad with null op, so we must make sure string 
   * terminates, and then pads null op to match the original string length.
   */
  
  DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "  AmlVariableUpdateVerification: Header ok \r\n"));
  if(*(UpdatePtr+i-1) == AML_STRING_PREFIX)
  {
    DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "  AmlVariableUpdateVerification: Check String Data \r\n"));
    // only check for original length
    for(;i<AmlVariableEntry.AmlVariableSize;i++)
    {
      DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "  AmlVariableUpdateVerification: Byte %x IsStringTerminated: %d \r\n", *(UpdatePtr+i), IsStringTerminated));
      if(IsStringTerminated)
      {
        if(*(UpdatePtr+i) != AML_ZERO_OP)
        {
          DEBUG((EFI_D_WARN, "  Verification failed for SetAmlVariableCallback: String data after Null Char is not Zero Op\r\n"));
          return EFI_INVALID_PARAMETER;
        }
      }
      else
      {
        if( *(UpdatePtr+i) == AML_NULL_CHAR)
          IsStringTerminated = TRUE;
        // Confirm the byte is a valid char for Strings
        else if( *(UpdatePtr+i) < '0' || *(UpdatePtr+i) > AML_NAME_CHAR__ )
        {
          DEBUG((EFI_D_WARN, "  Verification failed for SetAmlVariableCallback: String Data was invalid\r\n"));
          return EFI_INVALID_PARAMETER;
        }
      }
    }
    // confirm StringData was terminated
    if(IsStringTerminated == FALSE)
    {
      DEBUG((EFI_D_WARN, "  Verification failed for SetAmlVariableCallback: String data was never terminated\r\n"));
      return EFI_INVALID_PARAMETER;
    }
  }
  return EFI_SUCCESS;
}
/**
 Call the SetAmlVariable callback functions, passing them the pre fixed binary data, to get the 
 the post fixed data. The post fixed buffers are copied to PostFixAmlVariableArray and also 
 loaded into the PostFixArray of ACPI Tables. Thus the variable info is installed along with tables.

 @return EFI_SUCCESS           Successful
 @return EFI_INVALID_PARAMETER Out of memory.
*/
STATIC 
EFI_STATUS CallSetAmlVariableCallbacks ( 
 )
{
  UINTN                           CallbackIndex;
  UINTN                           VariableArrayIndex;
  BOOLEAN                         CallbackCalled[MAX_ACPI_REGISTERS];
  UINTN                           TableIndex = 0;
  UINTN                           i;
  
  //set CallbacksCalled to false for each registration
  for (i = 0; i < MAX_ACPI_REGISTERS; i++)
  {
    CallbackCalled[i] = 0;
  }
  //Find DSDT in table array 
  for(i = 0; i < NumOfAcpiTables; i++)
  {  
    if(PostFixArray[i].Signature ==
        EFI_ACPI_5_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE)
    {    
     TableIndex = i;
     break;
    } 
  }
  if( i == NumOfAcpiTables) 
    return EFI_INVALID_PARAMETER;
  // Loop through the variable array
  for (VariableArrayIndex = 0; VariableArrayIndex < NumOfAmlVariables; VariableArrayIndex++)
  {
    // Loop through each of the registered callback functions until we 
    // find one with a matching variable name.
    for (CallbackIndex = 0; CallbackIndex < NumRegisteredAmlVariableCallbacks; CallbackIndex++)
    {
      if (RegisteredAmlVariableCallbacks[CallbackIndex].SetAmlVariableCallback != NULL &&
           (AsciiStrnCmp(PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName, 
            RegisteredAmlVariableCallbacks[CallbackIndex].VariableName, AML_NAMESTRING_LENGTH)== 0))
      {
        EFI_STATUS Status = EFI_SUCCESS;
        VOID *AmlVariablePtr = NULL;
        UINTN AmlVariableSize = 0;

        AmlVariableSize = PreFixAmlVariableArray[VariableArrayIndex].AmlVariableSize;
        AmlVariablePtr = AllocatePool(AmlVariableSize);
        if (AmlVariablePtr == NULL)
        {
          DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to allocate buffer.\r\n"));
          return EFI_OUT_OF_RESOURCES;
        }

        CopyMem(AmlVariablePtr, PreFixAmlVariableArray[VariableArrayIndex].AmlVariable, AmlVariableSize);

        // Call the callback function.


        Status = RegisteredAmlVariableCallbacks[CallbackIndex].SetAmlVariableCallback(
          &AmlVariablePtr, AmlVariableSize);
        if ( EFI_ERROR (Status)) 
        {
          DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call SetAmlVariableCallback "
            "for AmlVariableName %x and function address %x. Status: %d\r\n",
            PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName, 
            RegisteredAmlVariableCallbacks[CallbackIndex].SetAmlVariableCallback, Status));
          FreePool(AmlVariablePtr);
          return Status;
        }
        else
        {
            DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " Calling SetAmlVariableCallback for AmlVariableName %c%c%c%c \r\n", 
              PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName[0], 
              PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName[1],
              PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName[2], 
              PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName[3]
             ));
        }

        /* Ensure the AML update adheres to guidelines:
         * Can only change payload (AmlVariableDataValue), should not touch the header (object type, name, data type)
         * Overall size should remain same, and Strings should finish with AML_NAME_CHAR_NULL. If verification fails
         * then ignore the callback.
         */
        Status = AmlVariableUpdateVerification(PreFixAmlVariableArray[VariableArrayIndex], AmlVariablePtr);  
        if (!EFI_ERROR (Status)){
          DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "  Verification of SetAmlVariableCallback passed\r\n"));
          // Copy the variable to the PostFixAmlVariableArray.
          Status = StoreAmlVariable(AmlVariablePtr, AmlVariableSize, PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName,
                   PreFixAmlVariableArray[VariableArrayIndex].TableOffset, &PostFixAmlVariableArray[VariableArrayIndex]);
          if (EFI_ERROR (Status)) 
            { 
              DEBUG(( EFI_D_WARN, " StoreAmlVariable failed. AmlVariableName: %x, Status: %d.\r\n",
              PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName, Status));
              return Status;
            }
          // Copy variable buffer into DSDT
          for(i=0;i< PostFixAmlVariableArray[VariableArrayIndex].AmlVariableSize;i++)
            CopyMem( (((UINT8*)PostFixArray[TableIndex].Table)+PreFixAmlVariableArray[VariableArrayIndex].TableOffset), AmlVariablePtr, AmlVariableSize);
          // Mark this callback as called.
          CallbackCalled[CallbackIndex] = TRUE;
        }
        FreePool(AmlVariablePtr);
        // we don't need to look at any more callbacks
        break;
      }
    }
    // If we got through all of the registered callbacks without finding 
    // one with a matching AmlVariableName, simply copy the variable from the prefix
    // to the postfix table.
    if (CallbackIndex == NumRegisteredAmlVariableCallbacks)
    {
      EFI_STATUS Status = StoreAmlVariable(PreFixAmlVariableArray[VariableArrayIndex].AmlVariable, 
        PreFixAmlVariableArray[VariableArrayIndex].AmlVariableSize,
        PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName, 
        PreFixAmlVariableArray[VariableArrayIndex].TableOffset,
        &PostFixAmlVariableArray[VariableArrayIndex]);
      if (EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, " StoreAmlVariable failed. AmlVariableName: %x, Status: %d.\r\n",
          PreFixAmlVariableArray[VariableArrayIndex].AmlVariableName, Status));
        return Status;
      }
    }
  }
  
  // Loop through all the callbacks that haven't been called, and call them
  // passing in NULL for the table to the indicate an invalid signature.
  for (CallbackIndex = 0; CallbackIndex < NumRegisteredAmlVariableCallbacks; CallbackIndex++)
  {
    if (!CallbackCalled[CallbackIndex] 
      && RegisteredAmlVariableCallbacks[CallbackIndex].SetAmlVariableCallback != NULL)
    {
      EFI_STATUS Status = EFI_SUCCESS;
      RegisteredAmlVariableCallbacks[CallbackIndex].SetAmlVariableCallback(NULL, 0);
      if ( EFI_ERROR (Status)) 
      {
        DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call SetAmlVariableCallback "
          "function.\r\n"));
        return Status;
      }
    }
  }

  return EFI_SUCCESS;
}
EFI_STATUS
LoadAcpi (  )
{
  UINTN                           i;
  EFI_ACPI_TABLE_PROTOCOL        *AcpiTable;
  UINTN                           TableHandle;
  EFI_STATUS                      Status;
  
  //Turn off registration 
  RegistrationClosed = TRUE;

  // Find the AcpiTable protocol
  Status = gBS->LocateProtocol (&gEfiAcpiTableProtocolGuid, NULL, (VOID**)&AcpiTable);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to locate EfiAcpiTableProtocol.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE successfully located EfiAcpiTableProtocol.\r\n"));
  }
  
  // Call GetTable callback functions if prefix tables are desired
  Status = CallGetTableCallbacks (GETMODE_PREFIX);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call GetTable callbacks to get prefix tables.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE successfully called GetTable callbacks to get prefix tables.\r\n"));
  }
  
  // Call SetTable callback functions if any
  Status = CallSetTableCallbacks ();
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call SetTable callbacks.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE successfully called SetTable callbacks.\r\n"));
  }
  
  // Call GetTable callback functions if postfix tables are desired
  Status = CallGetTableCallbacks (GETMODE_POSTFIX);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to call GetTable callbacks to get postfix tables.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE successfully called GetTable callbacks to get postfix tables.\r\n"));
  }
  
  /* Run AML variable callbacks */
  PrintAmlVariables(GETMODE_PREFIX);
  // Call GetAmlVariable callback functions if prefix tables are desired
  Status = CallGetAmlVariableCallbacks (GETMODE_PREFIX);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call GetAmlVariable callbacks to get prefix tables.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " ACPIPlatformDXE successfully called GetAmlVariable callbacks to get prefix tables.\r\n"));
  }
  // Call SetAmlVariable callback functions if any
  Status = CallSetAmlVariableCallbacks ();
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call SetAmlVariable callbacks.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " ACPIPlatformDXE successfully called SetAmlVariable callbacks.\r\n"));
  }
  // Call GetAmlVariable callback functions if postfix tables are desired
  Status = CallGetAmlVariableCallbacks (GETMODE_POSTFIX);
  if (EFI_ERROR (Status))
  {
    DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to call GetAmlVariable callbacks to get postfix tables.\r\n"));
    return EFI_ABORTED;
  } else {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, " ACPIPlatformDXE successfully called GetAmlVariable callbacks to get postfix tables.\r\n"));
  }
  PrintAmlVariables(GETMODE_PREFIX);
  PrintAmlVariables(GETMODE_POSTFIX);
  
  
  // All of the tables have been fixed up. Loop through all of the tables and
  // call InstallAcpiTable for each of them.
  for (i=0; i<NumOfAcpiTables; i++)
  {
    TableHandle = 0;
    // Install ACPI table
    Status = AcpiTable->InstallAcpiTable (
                          AcpiTable,
                          PostFixArray[i].Table,
                          PostFixArray[i].TableSize,
                          &TableHandle
                          );
    if (EFI_ERROR(Status)) {
      DEBUG(( EFI_D_WARN, "ACPIPlatformDXE failed to install ACPI table with "
        "signature %x.\r\n", PostFixArray[i].Signature));
      return Status;
    } else {
      DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE successfully installed ACPI table with "
        "signature %x.\r\n", PostFixArray[i].Signature));
    }
  }

  // Call GetTable callback functions if postfix tables are desired
  return Status; 
}
/**
  Given a pointer to the name op in binary data, the function parses the buffer and adds an 
  AmlVariableEntry to the given array
 @param  AmlNameObjectPtr      Pointer to byte in buffer with value AML_NAME_OP
 @param  TableOffset           Offset of this byte in the table. Stored in the AmlVariableEntry
 
 @return EFI_SUCCESS           Successful
 @return EFI_INVALID_PARAMETER Incorrect Table Signature given
**/
EFI_STATUS
LoadAmlNameObjectFromTable(
  IN VOID   *AmlNameObjectPtr,
  IN UINTN   TableOffset
)
{
  EFI_STATUS Status;
  UINTN  i=0;
  UINTN  Offset=0;
  UINTN  AmlNameObjectSize=0;
  UINT8  CurrentByte;
  CHAR8  AmlVariableName[AML_NAMESTRING_LENGTH];
 
  // if the first byte is not AML_NAME_OP then this buffer is invalid
  if((AmlNameObjectPtr == NULL) || (*((UINT8*)AmlNameObjectPtr) != AML_NAME_OP))
  {
    return EFI_INVALID_PARAMETER;
  }
  
  // after name op, the next AML_NAMESTRING_LENGTH bytes are the name of the variable
  for(i=0; i<AML_NAMESTRING_LENGTH; i++)
  {
    Offset++;
    /* Confirm the byte is a valid char. Compiler catches this error, but we're trying
     * to help weed out false positives for variables as we go byte by byte.
     */
    CurrentByte = *(((UINT8*)AmlNameObjectPtr)+Offset);
    if(CurrentByte < '0' || CurrentByte > AML_NAME_CHAR__ )
      return EFI_INVALID_PARAMETER;
    AmlVariableName[i]= (CHAR8) CurrentByte;
  }
  
  // The next byte is the data type
  Offset++;
  CurrentByte=*(((UINT8*)AmlNameObjectPtr)+Offset);
  
  /* If data type is AML_ZERO_OP, then name object was initialized with 0x00. This leads to
   * encoding optimization we can't handle. Print a warning message in that case. If data type 
   * is not 0x00 and not String, we know the length of data. If it is a string,
   * we must get length by searching through the bytes till we reach the null char
   */  
  switch (CurrentByte)
  {
    case AML_BYTE_PREFIX:
      Offset++;
      break;
    case AML_WORD_PREFIX:
      Offset+=2;
      break;
    case AML_DWORD_PREFIX:
      Offset+=4;
      break;
    case AML_STRING_PREFIX:
      do
      {
        Offset++;
        CurrentByte=*(((UINT8*)AmlNameObjectPtr)+Offset);
      } while(CurrentByte!=AML_NULL_CHAR);
      break;
    case AML_QWORD_PREFIX:
      Offset+=8;    
      break;
    case AML_ZERO_OP:
      DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "AML: Name Object "));
      for(i=0; i<AML_NAMESTRING_LENGTH; i++)
        DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "%c", AmlVariableName[i]));
      DEBUG((ACPI_PLATFORM_DEBUG_PRINT, " set to zero- must be nonzero to update\r\n"));
    default:
      return EFI_INVALID_PARAMETER;
  }
  // Offset+1 is now the total number of bytes for our aml variable size. We can store the variable in our array.
  AmlNameObjectSize = (Offset+1) * sizeof(UINT8);
  Status = StoreAmlVariable (AmlNameObjectPtr, AmlNameObjectSize, AmlVariableName, TableOffset,
             &PreFixAmlVariableArray[NumOfAmlVariables]);
  NumOfAmlVariables++;
  
  return Status;    
}

/**
 Parsing function which takes a table signature as a parameter, finds the root name objects
 and puts them into the prefixed aml variable array

 @param  TableSignature        Signature of the table to parse for root variables

 @return EFI_SUCCESS           Successful
 @return EFI_INVALID_PARAMETER Incorrect Table Signature given
*/
EFI_STATUS
LoadAmlVariables (
  IN UINT32 TableSignature 
)
{
  UINTN i;
  UINTN j;
  UINT8 *TableBytePtr;

  DEBUG((ACPI_PLATFORM_DEBUG_PRINT, "AML: Loading objects from %x\r\n", TableSignature));
  for (i=0; i<NumOfAcpiTables; i++) {
    // If the signature doesn't match, skip this ACPI table.
    if(PreFixArray[i].Signature != TableSignature) 
      continue;
    // Signature matches so call load function for variables on this table
    else
    {
      TableBytePtr = (UINT8*)PreFixArray[i].Table;
      j=sizeof(EFI_ACPI_DESCRIPTION_HEADER);
      while(j<PreFixArray[i].TableSize)
      {
        // we can stop looking once we hit a device
        if( *(TableBytePtr+j) == AML_EXT_OP)
          break;
        if(*(TableBytePtr+j) == AML_NAME_OP)
        {
          if (EFI_ERROR(LoadAmlNameObjectFromTable(TableBytePtr+j,j)))        
          // if LoadAmlNameObjectFromTable returns error than byte was not start of name op
            j++;

          else
          {
           // LoadAmlNameObjectFromTable was successful, so skip the aml variable to save time

            j+=PreFixAmlVariableArray[NumOfAmlVariables-1].AmlVariableSize;
          }
        }
        else
          j++;
      }
    }
  }
  return EFI_SUCCESS;
}



EFI_STATUS  AmlUpdateRegisterForChipInfo()
{
    EFI_QCOM_ACPIPLATFORM_PROTOCOL *pEfiAcpiPlatformProtocol = NULL;
    EFI_STATUS Status;
    
    CHAR8 AmlVariableNameSOID[AML_NAMESTRING_LENGTH]={'S','O','I','D'};
    CHAR8 AmlVariableNameSIDS[AML_NAMESTRING_LENGTH]={'S','I','D','S'};
    CHAR8 AmlVariableNameSIDV[AML_NAMESTRING_LENGTH]={'S','I','D','V'};
    CHAR8 AmlVariableNameSVMJ[AML_NAMESTRING_LENGTH]={'S','V','M','J'};
    CHAR8 AmlVariableNameSVMI[AML_NAMESTRING_LENGTH]={'S','V','M','I'};
    CHAR8 AmlVariableNameSDFE[AML_NAMESTRING_LENGTH]={'S','D','F','E'};

    CHAR8 AmlVariableNameSIDM[AML_NAMESTRING_LENGTH]={'S','I','D','M'};
    
    Status = gBS->LocateProtocol(&gQcomAcpiPlatformProtocolGuid,
                                  NULL, (VOID**) &pEfiAcpiPlatformProtocol);
    if (EFI_ERROR(Status))
    {
        DEBUG((EFI_D_WARN, "AML: Locate ACPI Protocol failed, Status = (0x%x)\r\n", Status));
        return Status;
    }    

    // Update info  
    /*start updating chip info, if one variable failed, we will not stop the procedure*/
    // Holds the Chip Id
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSOID, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateChipId);
    
    // Holds the Chip ID translated to a string
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSIDS, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateChipChipIdString);

    // Holds the Chip Version as (major<<16)|(minor&0xffff)
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSIDV, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateChipVersion);
    
    // Holds the major Chip Version
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSVMJ, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateMajorChipVersion);
    // Holds the minor Chip Version
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSVMI, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateMinorChipVersion);
    // Holds the Chip Family enum
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSDFE, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateChipFamily);
    // Holds the Modem Support bit field
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol,
                                                            AmlVariableNameSIDM, 
                                                            GETMODE_PREFIX, 
                                                            NULL,
                                                            AmlUpdateModemInfo);

    
    return Status;


}





/**
 Sample callback function to show how the AML update mechanism works. The callback
 changes the value of the TEST variable from 0xFF to 0xAA 
*/ 

static EFI_STATUS EFIAPI AmlUpdateTestCallback(
  IN OUT VOID         **AmlVariableBuffer,
  IN OUT UINTN          AmlVariableBufferSize
)
{
  UINTN i;
  DEBUG((DEBUG_LOAD,"AML: Test Register\r\n"));
  if(AmlVariableBuffer == NULL)
  {
    DEBUG((EFI_D_WARN,"AML: Buffer is empty\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  else
  {
    DEBUG((DEBUG_LOAD,"  buffer:\r\n"));
  }
  for(i=0; i<AmlVariableBufferSize; i++)
    DEBUG((DEBUG_LOAD," %02x", *( (UINT8*)(*AmlVariableBuffer)+i) ));
  DEBUG((DEBUG_LOAD,"\r\n"));

  *( (UINT8*)(*AmlVariableBuffer)+AML_NAMESPACE_HEADER_SIZE) = 0xAA;
  
  DEBUG((DEBUG_LOAD,"  New chip info buffer:\r\n"));
  for(i=0; i<AmlVariableBufferSize; i++)
    DEBUG((DEBUG_LOAD," %02x", *( (UINT8*)(*AmlVariableBuffer)+i) ));
  DEBUG((DEBUG_LOAD,"\r\n"));
  return EFI_SUCCESS;
}

/**
 Sample register function to show how the AML update mechanism works. The function
 registers a callback to update the TEST variable in the DSDT
 changes the value of the TEST variable from 0xFF to 0xAA.

 To enable this function, search for the function call from earlier in this file 
 and uncomment the line. 
*/ 
  
EFI_STATUS
AmlUpdateRegisterTest ()
{
    EFI_QCOM_ACPIPLATFORM_PROTOCOL *pEfiAcpiPlatformProtocol = NULL;
    EFI_STATUS Status;

    CHAR8 AmlVariableName[AML_NAMESTRING_LENGTH] =  {'T','E','S','T'};
    Status = gBS->LocateProtocol(&gQcomAcpiPlatformProtocolGuid,
                                  NULL, (VOID**) &pEfiAcpiPlatformProtocol);
    if (EFI_ERROR(Status))
      {
        DEBUG((EFI_D_WARN, "AML: Locate ACPI Protocol failed, Status = (0x%x)\r\n", Status));
        return Status;
      }
    

    // Update info 
    
    Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol, AmlVariableName, 
      GETMODE_POSTFIX, NULL, AmlUpdateTestCallback);
    return Status;
}

/**
  Callback function for ACPI table load.

  @param Event           The event that is signaled.
  @param Context         Not used here.

**/
VOID
EFIAPI
ACPITableLoadNotification( 
  IN EFI_EVENT   Event,
  IN VOID        *Context
  )
{
  EFI_STATUS Status = EFI_LOAD_ERROR;
    
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE has %d registers. \r\n", NumRegisteredCallbacks));

  if (boot_from_emmc())
  {
    //Attempt to load from Plat parition in eMMC GPP
    Status = LoadAcpiFromVolume(L"\\ACPI\\",
                                &gEfiEmmcGppPartition1Guid,
                                &gEfiPlatPartitionTypeGuid,
                                TRUE
                               );    
    if (Status != EFI_SUCCESS) 
    {
      //Attempt to load from User Partition
      Status = LoadAcpiFromVolume(L"\\ACPI\\",
                                  &gEfiEmmcUserPartitionGuid,
                                  &gEfiPlatPartitionTypeGuid,
                                  TRUE
                                 );
        
    }
  }

  else if (boot_from_ufs())
  {
    //Attempt to load from UFS LUN4 PLAT partition
    Status = LoadAcpiFromVolume(L"\\ACPI\\",
                                &gEfiUfsLU4Guid,
                                &gEfiPlatPartitionTypeGuid,
                                TRUE
                               );    
  }

  if (Status != EFI_SUCCESS) {
    DEBUG ((EFI_D_ERROR, "ACPI: Failed to load from specified partitions !\r\n"));
    return;
  } 

  //Parse the DSDT aml binary to set up the prefix array with variable information.
  Status = LoadAmlVariables(EFI_ACPI_5_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE);
  if (Status == EFI_NOT_FOUND) {
      DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed to find DSDT\r\n"));
    DEBUG ((DEBUG_LOAD, "ACPI: Failed to find DSDT\r\n")); 
    return;
  }
  else if(Status != EFI_SUCCESS){
      DEBUG(( EFI_D_WARN, " ACPIPlatformDXE failed while parsing DSDT Error:%x\r\n", Status));
    DEBUG ((DEBUG_LOAD, "ACPI: Failed parsing DSDT: %x\r\n", Status)); 
  }

  // run the register for update chip information
  AmlUpdateRegisterForChipInfo();

  // Load ACPI tables into memory
  Status = LoadAcpi();
  if(Status == EFI_SUCCESS) {
    DEBUG(( DEBUG_LOAD, "ACPI: tables loaded to system memory\r\n"));
      DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE finished callbacks and loaded tables\r\n"));
  } else {
    DEBUG ((EFI_D_WARN, "ACPI: Failed to load tables to system memory for HLOS!\r\n")); 
    DEBUG ((DEBUG_LOAD, "ACPI: Failed to load tables!\r\n")); 
  }
  return;
}

/**
  Get all ACPI tables.

  @param  This              Protocol instance pointer.
  @param  AcpiTablePtr      The pointer to the address acpi data to be copied to.
  @param  AcpiTableSize     The pointer to the size of acpi data.
  @param  IsPreFix          Table attribute, TRUE is pre fixup table, FLASE is post fixup table.

  @retval EFI_SUCCESS            The data was get successfully.
  @retval EFI_INVALID_PARAMETER  Bad parameter

**/

EFI_STATUS
GetAcpiTable (
  IN EFI_QCOM_ACPIPLATFORM_PROTOCOL  *This,
  OUT VOID                           *AcpiTablePtr,
  OUT UINTN                          *AcpiTableSize,
  IN BOOLEAN                          IsPreFix
  )
{
  UINT8  i;    

  if(AcpiTablePtr == NULL || AcpiTableSize == NULL )
  {
    return  EFI_INVALID_PARAMETER;
  }

  *AcpiTableSize = 0;
  for( i= 0; i < NumOfAcpiTables; i++)
  {
    if( IsPreFix == TRUE )
    {
      CopyMem( (UINT8 *)AcpiTablePtr + (*AcpiTableSize), (UINT8 *)(PreFixArray[i].Table),  PreFixArray[i].TableSize); 
      *AcpiTableSize += PreFixArray[i].TableSize;
    }
    else
    {
      CopyMem( (UINT8 *)AcpiTablePtr + (*AcpiTableSize), (UINT8 *)(PostFixArray[i].Table), PostFixArray[i].TableSize); 
      *AcpiTableSize += PostFixArray[i].TableSize;
    }
  }
  return EFI_SUCCESS;
}


/**
  Get all ACPI table size.

  @param  This                   Protocol instance pointer.
  @param  PreFixUpAcpiTableSize  The pointer to the size of pre fixup acpi table size.
  @param  PostFixUpAcpiTableSize The pointer to the size of post fixup acpi table size.
  @param  TotalNumOfAcpiTables   The pointer to the number of acpi tables.

  @retval EFI_SUCCESS            The size was get successfully.
  @retval EFI_INVALID_PARAMETER  Bad parameter

**/

EFI_STATUS
GetAcpiTableSize (
  IN EFI_QCOM_ACPIPLATFORM_PROTOCOL  *This,
  IN OUT UINTN                       *PreFixUpAcpiTableSize,
  IN OUT UINTN                       *PostFixUpAcpiTableSize, 
  IN OUT UINTN                       *TotalNumOfAcpiTables 
  )
{
  UINTN  i;    

  if(PreFixUpAcpiTableSize == NULL || PostFixUpAcpiTableSize == NULL || TotalNumOfAcpiTables == NULL )
  {
    return  EFI_INVALID_PARAMETER;
  }

  *PreFixUpAcpiTableSize = 0;
  *PostFixUpAcpiTableSize = 0;
  for( i= 0; i < NumOfAcpiTables; i++)
  {
    *PreFixUpAcpiTableSize += PreFixArray[i].TableSize;
    *PostFixUpAcpiTableSize += PostFixArray[i].TableSize;
  }

  *TotalNumOfAcpiTables = NumOfAcpiTables;

  return EFI_SUCCESS;
}

/** 
  Register callback function to support ACPI table query/update services.

  @param[in] This                   A pointer to the EFI_QCOM_ACPIPLATFORM_REGISTER_PROTOCOL instance.
  @param[in] TableSignature         Signature of ACPI Table to be associated with. Such signature needs to 
                                    be a valid signature of an ACPI table found on the system. Or if the 
                                    signature is ACPI_TABLE_SIGNATURE_ALL, ACPIPLatformDXE assumes that the client driver operates
                                    on all ACPI tables.
  @param[in] GetMode                Mode of the GetTableCallback function. There are 3 modes, either prefix
                                    or postfix or both.
  @param[in] GetTableCallback       This is the pointer to get ACPI table callback function. Could be NULL.
                                    It could not be NULL if SetTableCallback is not NULL.
  @param[in] SetTableCallback       This is the pointer to set postfix ACPI table callback function. Could be NULL.

  @return
    EFI_SUCCESS            Registration succeed. 
    EFI_OUT_OF_RESOURCES   The request could not be completed due to a lack of resources,
                           Maximum number of registrations is defined as MAX_ACPI_REGISTERS.
    EFI_INVALID_PARAMETER  Registration is closed when ACPIPlatformDxe start working on ACPITableLoadEvent
    EFI_INVALID_PARAMETER  This is NULL
    EFI_INVALID_PARAMETER  GetMode is invalid
    EFI_INVALID_PARAMETER  Same table was already registered to be updated 
    EFI_INVALID_PARAMETER  Signature is ACPI_TABLE_SIGNATURE_ALL while SetTableCallback is not NULL
*/
EFI_STATUS
AcpiTableRegister (
  IN  EFI_QCOM_ACPIPLATFORM_PROTOCOL    *This,
  IN  UINT32                             TableSignature,
  IN  GETMODE                            GetMode,                                       
  IN  ACPITableGetCallbackPtr            GetTableCallback,
  IN  ACPITableSetCallbackPtr            SetTableCallback
  )
{
  UINTN i;

  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "AcpiTableRegister called, Signature: %x; GetMode: %d;\r\n", 
    TableSignature, GetMode));
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "GetTableCallback ptr: %x; SetTableCallback ptr: %x.\r\n", 
    GetTableCallback, SetTableCallback));

  if(This == NULL)
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed, This is NULL\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  if(NumRegisteredCallbacks ==  MAX_ACPI_REGISTERS)
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed because of too many previous registrations.\r\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  if(RegistrationClosed ==  TRUE)
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed because "
      "registration is closed as ACPIPlatformDxe started working on "
      "ACPITableLoadEvent.\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  if(GetMode != GETMODE_PREFIX && GetMode != GETMODE_POSTFIX && GetMode != GETMODE_BOTH)
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed, GetMode "
      "is invalid, it is %d.\r\n", GetMode));
    return EFI_INVALID_PARAMETER;
  }
  if(TableSignature == ACPI_TABLE_SIGNATURE_ALL && SetTableCallback != NULL)
  {
    DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed, Set "
      "callback pointer is not NULL, and the client driver attempts to operate "
      "on all tables with Signature as ACPI_TABLE_SIGNATURE_ALL.\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  for (i = 0; i < NumRegisteredCallbacks; i++)
  {
    if (RegisteredCallbacks[i].SetTableCallback != NULL 
      && RegisteredCallbacks[i].TableSignature == TableSignature)
    {
      DEBUG(( EFI_D_WARN, "ACPIPlatformDXE Callback registration failed, Set "
        "callback pointer was already registered for signature %x.\r\n", 
        TableSignature));
      return EFI_INVALID_PARAMETER;
    }
  }

  RegisteredCallbacks[NumRegisteredCallbacks].TableSignature = TableSignature;
  RegisteredCallbacks[NumRegisteredCallbacks].GetMode = GetMode;
  RegisteredCallbacks[NumRegisteredCallbacks].GetTableCallback = GetTableCallback;
  RegisteredCallbacks[NumRegisteredCallbacks].SetTableCallback = SetTableCallback;
  NumRegisteredCallbacks++;

  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "ACPIPlatformDXE callback registration for index %d "
    "successful:\r\n", NumRegisteredCallbacks));

  return EFI_SUCCESS;
}

/** 
  Register callback function to support ACPI table query/update services.

  @param[in] This                       A pointer to the EFI_QCOM_ACPIPLATFORM_REGISTER_PROTOCOL instance.
  @param[in] VariableName               Name of the AML Variable being registered for callback. It needs to 
                                        be a valid name of a variable found in the AML. Or if the name is
                                        "ALL", ACPIPLatformDXE will return a list of all variables and their values
  @param[in] GetMode                    Mode of the GetAmlVariableCallback function. There are 3 modes: prefix,
                                        postfix, or both.
  @param[in] GetAmlVariableCallback    This is the pointer to get AML variable callback function. Could be NULL.
                                        It could not be NULL if SetAmlVariableCallback is not NULL.
  @param[in] SetAmlVariableCallback    This is the pointer to set postfix AML variable callback function. Could be NULL.

  @return
    EFI_SUCCESS            Registration succeed. 
    EFI_OUT_OF_RESOURCES   The request could not be completed due to a lack of resources,
                           Maximum number of registrations is defined as MAX_AML_VARIABLES_REGISTERS.
    EFI_INVALID_PARAMETER  Registration is closed when ACPIPlatformDxe start working on ACPITableLoadEvent
    EFI_INVALID_PARAMETER  This is NULL
    EFI_INVALID_PARAMETER  GetMode is invalid
    EFI_INVALID_PARAMETER  Same variable was already registered to be updated 
    EFI_INVALID_PARAMETER  Variable name is 'ALL' while SetTableCallback is not NULL
*/
EFI_STATUS
AmlVariableRegister
(
  IN  EFI_QCOM_ACPIPLATFORM_PROTOCOL    *This,
  IN  CHAR8                              VariableName[AML_NAMESTRING_LENGTH],
  IN  GETMODE                            GetMode,                                       
  IN  AMLVariableGetCallbackPtr          GetAmlVariableCallback,
  IN  AMLVariableSetCallbackPtr          SetAmlVariableCallback
)
{
  UINTN i;
  UINTN j;

  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "AML: Registering "));
  for (i = 0; i < AML_NAMESTRING_LENGTH; i++)
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "%c", VariableName[i]));
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "\r\n"));

  if(This == NULL)
  {
    DEBUG(( EFI_D_WARN, "AML: Registration failed- This is NULL\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  if(NumRegisteredAmlVariableCallbacks ==  MAX_AML_VARIABLES)
  {
    DEBUG(( EFI_D_WARN, "AML: Registration failed- too many previous registrations.\r\n"));
    return EFI_OUT_OF_RESOURCES;
  }
  if(RegistrationClosed ==  TRUE)
  {
    DEBUG(( EFI_D_WARN, "AML: Registration failed- ACPITableLoadEvent occurred.\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  if(GetMode != GETMODE_PREFIX && GetMode != GETMODE_POSTFIX && GetMode != GETMODE_BOTH)
  {
    DEBUG(( EFI_D_WARN, "AML: Registration failed- GetMode %d is invalid.\r\n", GetMode));
    return EFI_INVALID_PARAMETER;
  }
  if(SetAmlVariableCallback != NULL && (AsciiStrnCmp(VariableName, AMLVARIABLENAME_ALL, AML_NAMESTRING_LENGTH) == 0) )
  {
    DEBUG(( EFI_D_WARN, "AML: Registration failed- Set "
      "callback pointer is not NULL, and the client driver attempts to update "
      "on all variables.\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  for (i = 0; i < NumRegisteredAmlVariableCallbacks; i++)
  {
    if (RegisteredAmlVariableCallbacks[i].SetAmlVariableCallback != NULL 
      && AsciiStrnCmp(RegisteredAmlVariableCallbacks[i].VariableName, VariableName, AML_NAMESTRING_LENGTH) == 0)
    {
      DEBUG(( EFI_D_WARN, "AML: Registration failed- Set callback pointer already registered for "));
      for(j=0; j < AML_NAMESTRING_LENGTH; j++)
        DEBUG(( EFI_D_WARN, "%c", VariableName[j]));
      DEBUG(( EFI_D_WARN,"\r\n"));
      return EFI_INVALID_PARAMETER;
    }
  }

  for(j = 0; j < AML_NAMESTRING_LENGTH; j++)
    RegisteredAmlVariableCallbacks[NumRegisteredAmlVariableCallbacks].VariableName[j] = VariableName[j];
  RegisteredAmlVariableCallbacks[NumRegisteredAmlVariableCallbacks].GetMode = GetMode;
  RegisteredAmlVariableCallbacks[NumRegisteredAmlVariableCallbacks].GetAmlVariableCallback = GetAmlVariableCallback;
  RegisteredAmlVariableCallbacks[NumRegisteredAmlVariableCallbacks].SetAmlVariableCallback = SetAmlVariableCallback;
  RegisteredAmlVariableCallbacks[NumRegisteredAmlVariableCallbacks].TableSignature = 
    EFI_ACPI_5_0_DIFFERENTIATED_SYSTEM_DESCRIPTION_TABLE_SIGNATURE;
  DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "Aml object callback registration for index %d "
    "successful:\r\n", NumRegisteredAmlVariableCallbacks));
  NumRegisteredAmlVariableCallbacks++;
  return EFI_SUCCESS;
}


/**
  Entrypoint of Acpi Platform driver.

  @param  ImageHandle
  @param  SystemTable

  @return EFI_SUCCESS
  @return EFI_LOAD_ERROR
  @return EFI_OUT_OF_RESOURCES

**/
EFI_STATUS
EFIAPI
AcpiPlatformEntryPoint (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  /*
   * Set up to be notified when the ACPITableLoadEvent is signalled.
   */
  EFI_STATUS Status = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL, 
                                         TPL_CALLBACK, 
                                         ACPITableLoadNotification, 
                                         NULL,
                                         &gEfiACPITableLoadGuid,
                                         &ACPITableLoadEvent);

  ASSERT_EFI_ERROR (Status);

    // Install RNG protocols and Test protocol
  Status = gBS->InstallMultipleProtocolInterfaces (
                      &ImageHandle,
                      &gQcomAcpiPlatformProtocolGuid,
                      (void **)&AcpiPlatformProtocolImpl,
                      &gQcomAcpiPlatformTestProtocolGuid,
                      (void **)&AcpiPlatformTestProtocolImpl,
                       NULL
                    );
  if (EFI_ERROR (Status))
  {
    DEBUG(( ACPI_PLATFORM_DEBUG_PRINT, "AcpiPlatformDxeInitialize: Install Protocol failed, Status = (0x%p)\r\n", Status));
  }

  return Status;
}
