/** 
  @file  EFIPmicVersion.h
  @brief PMIC VERSION Protocol for UEFI.
*/
/*=============================================================================
  Copyright (c) 2013 - 2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 01/06/15   al      Updated PMIC model types
 09/02/14   vs      Added PM_8909 Model (CR-679803)
 03/14/14   ps      Added new PMIC models
 02/03/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
 04/11/13   al      Created
=============================================================================*/
#ifndef __EFIPMICVERSION_H__
#define __EFIPMICVERSION_H__

/*===========================================================================
  INCLUDE FILES
===========================================================================*/

/*===========================================================================
  MACRO DECLARATIONS
===========================================================================*/
/** @ingroup efi_pmicVersion_constants
  Protocol version.
*/
#define PMIC_VERSION_REVISION 0x0000000000010001
/** @} */ /* end_addtogroup efi_pmicVersion_constants */

/* Protocol GUID definition */
/** @ingroup efi_pmicVersion_protocol */
#define EFI_PMIC_VERSION_PROTOCOL_GUID \
{ 0x4684800a, 0x2755, 0x4edc, { 0xb4, 0x43, 0x7f, 0x8c, 0xeb, 0x32, 0x39, 0xd3 } }

/** @cond */
/**
  External reference to the PMIC Version Protocol GUID.
*/
extern EFI_GUID gQcomPmicVersionProtocolGuid;


/*===========================================================================
  TYPE DEFINITIONS
===========================================================================*/
/**
  PMIC VERSION UEFI typedefs
*/
typedef struct _EFI_QCOM_PMIC_VERSION_PROTOCOL   EFI_QCOM_PMIC_VERSION_PROTOCOL;
/** @endcond */

/** @addtogroup efi_pmicVersion_data_types 
@{ */
/* Specifies which PMIC we are using */
/**
  PMIC model; version resource ID.
*/
typedef enum
{
   EFI_PMIC_IS_UNKNOWN   = 0, /**< Unknown PMIC.*/
   EFI_PMIC_IS_PM8941    = 1, /**< PM8941.  */
   EFI_PMIC_IS_PM8841    = 2, /**< PM8841.  */
   EFI_PMIC_IS_PM8019    = 3, /**< PM8019.  */
   EFI_PMIC_IS_PM8026    = 4, /**< PM8026.  */
   EFI_PMIC_IS_PM8110    = 5, /**< PM8110.  */
   EFI_PMIC_IS_PMA8084   = 6, /**< PMA8084. */
   EFI_PMIC_IS_SMB8962   = 7, /**< SMB8962. */
   EFI_PMIC_IS_PMD9635   = 8, /**< PMD963   */
   EFI_PMIC_IS_PM8994    = 9, /**< PM8994   */
   EFI_PMIC_IS_PMI8994   = 0xA, /**< PMI8994 */
   EFI_PMIC_IS_PM8916    = 0xB, /**< PM8916  */
   EFI_PMIC_IS_PM8004    = 0xC, /**< PM8004  */
   EFI_PMIC_IS_PM8909    = 0xD, /**< PM8909  */
   EFI_PMIC_IS_PM2433    = 0xE, /**< PM2433  */
   EFI_PMIC_IS_PMF2432   = EFI_PMIC_IS_PMA8084, // TODO: Determine if the model number is changing
   EFI_PMIC_IS_PM8950    = 16,
   EFI_PMIC_IS_PMI8950   = 17,
   EFI_PMIC_IS_PMK8001   = 18,
   EFI_PMIC_IS_INVALID   = 0x7FFFFFFF, /**< Invalid PMIC.*/
} EFI_PM_MODEL_TYPE;


/**
  PMIC device information.
*/
typedef struct
{
  EFI_PM_MODEL_TYPE  PmicModel;
  /**< Model type; see #EFI_PM_MODEL_TYPE for details. */
  UINT32             PmicAllLayerRevision;
  /**< All layer revision number. */
  UINT32             PmicMetalRevision;
  /**< Metal revision number. */
} EFI_PM_DEVICE_INFO_TYPE;
/** @} */ /* end_addtogroup efi_pmicVersion_data_types */



/*===========================================================================
  FUNCTION DEFINITIONS
===========================================================================*/

/* EFI_PM_GET_PMIC_INFO */ 
/** @ingroup efi_pmicVersion_get_pmic_info
  @par Summary
  Retrieves information about the PMIC device for a specific device index. 

  @param[in]  PmicDeviceIndex  Primary: 0. Secondary: 1.               
  @param[out] PmicDeviceInfo   Variable with PMIC device information to return 
                               to the caller; see #EFI_PM_DEVICE_INFO_TYPE for 
                               details.

  @return
  EFI_SUCCESS        -- Function completed successfully. \n
  EFI_PROTOCOL_ERROR -- Error occurred during the operation.
*/
typedef
EFI_STATUS (EFIAPI *EFI_PM_GET_PMIC_INFO)(
  IN   UINT32                     PmicDeviceIndex, 
  OUT  EFI_PM_DEVICE_INFO_TYPE    *PmicDeviceInfo
);





/*===========================================================================
  PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicVersion_protocol
  @par Summary
  Qualcomm PMIC Version Protocol interface.

  @par Parameters
  @inputprotoparams{pmic_version_proto_params.tex} 
*/
struct _EFI_QCOM_PMIC_VERSION_PROTOCOL {
  UINT64                               Revision;
  EFI_PM_GET_PMIC_INFO                 GetPmicInfo;
};


#endif  /* __EFIPMICVERSION_H__ */
