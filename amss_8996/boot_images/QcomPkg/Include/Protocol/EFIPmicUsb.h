/** 
@file  EFIPmicUsb.h
@brief PMIC usb for UEFI.
*/
/*=============================================================================
Copyright (c) 2015 Qualcomm Technologies, Inc.
All rights reserved.
Qualcomm Technologies Inc Confidential and Proprietary.
    
=============================================================================*/

/*=============================================================================
EDIT HISTORY


when       who     what, where, why
--------   ---     ----------------------------------------------------------- 

01/30/15   al      New file.

=============================================================================*/

#ifndef __EFI_PMIC_USB_H__
#define __EFI_PMIC_USB_H__

/*===========================================================================
INCLUDE FILES
===========================================================================*/

/*===========================================================================
MACRO DECLARATIONS
===========================================================================*/
/** @addtogroup efi_pmicUsb_constants 
@{ */
/**
Protocol version.
*/

#define PMIC_USB_REVISION 0x0000000000010005
/** @} */ /* end_addtogroup efi_pmicUsb_constants */

/*  Protocol GUID definition */
/** @ingroup efi_pmicUsb_protocol */
#define EFI_PMIC_USB_PROTOCOL_GUID \
    { 0x6adb9972, 0x83d, 0x4be4,  { 0xbb, 0x74, 0xc7, 0x25, 0xa7, 0x30, 0x61, 0x56 } } 

/** @cond */
/*===========================================================================
EXTERNAL VARIABLES
===========================================================================*/
/**
External reference to the PMIC USB Protocol GUID.
*/
extern EFI_GUID gQcomPmicUsbProtocolGuid;

/*===========================================================================
TYPE DEFINITIONS
===========================================================================*/
/**
Protocol declaration.
*/
typedef struct _EFI_QCOM_PMIC_USB_PROTOCOL   EFI_QCOM_PMIC_USB_PROTOCOL;
/** @endcond */

/** @addtogroup efi_pmicUsb_data_types 
@{ */


/**  Charging port types. **/
typedef enum EFI_PM_USB_CHGR_PORT_TYPE
{
  EFI_PM_USB_CHG_PORT_CDP,           /**< Charging Downstream Port */
  EFI_PM_USB_CHG_PORT_DCP,           /**< Dedicated Charging Port  */
  EFI_PM_USB_CHG_PORT_OTHER,         /**< Other Charging Port      */
  EFI_PM_USB_CHG_PORT_SDP,           /**< Standard Downstream Port */
  EFI_PM_USB_CHG_PORT_FMB_UART_ON,   /**< FMB UART ON              */
  EFI_PM_USB_CHG_PORT_FMB_UART_OFF,  /**< FMB UART OFF             */
  EFI_PM_USB_CHG_PORT_FMB_USB_ON,    /**< FMB USB ON               */
  EFI_PM_USB_CHG_PORT_FMB_USB_OFF,   /**< FMB USB OFF              */
  EFI_PM_USB_CHG_PORT_INVALID        /**< INVALID PORT             */
}EFI_PM_USB_CHGR_PORT_TYPE;


/**  Enum for high voltage dcp status. **/
typedef enum
{
   EFI_PM_USB_HVDCP_SEL_5V,     /**< 5V High Voltage DCP Enabled >*/
   EFI_PM_USB_HVDCP_SEL_9V,     /**< 9V High Voltage DCP Enabled >*/
   EFI_PM_USB_HVDCP_SEL_12V,    /**< 12V High Voltage DCP Enabled>*/
   EFI_PM_USB_HVDCP_SEL_20V,    /**< 20V High Voltage DCP Enabled>*/
   EFI_PM_USB_IDEV_HVDCP_SEL_A, /**< High Voltage DCP detected   >*/
   EFI_PM_USB_HVDCP_STS_INVALID /**< INVALID >*/
}EFI_PM_USB_HVDCP_STS_TYPE;


typedef enum EFI_PM_USB_OTG_CTRL_TYPE
{
  EFI_PM_USB_OTG_CTR__CMD_REG_RID_DISABLED,  /**< Command register with RID Disabled*/  
  EFI_PM_USB_OTG_CTR__PIN_CTRL_RID_DISABLED, /**< Pin control with RID Disabled     */
  EFI_PM_USB_OTG_CTR__CMD_REG_RID_ENABLED,   /**< Command register with RID Enabled */
  EFI_PM_USB_OTG_CTR__AUTO_OTG_RID_ENABLED,  /**< Auto OTG (RID Enabled)            */
  EFI_PM_USB_OTG_CTR__INVALID
}EFI_PM_USB_OTG_CTRL_TYPE;


/**  RID status **/
typedef enum 
{
   EFI_PM_USB_USB_CHGPTH_RID_GND,
   EFI_PM_USB_USB_CHGPTH_RID_FLOAT,
   EFI_PM_USB_USB_CHGPTH_RID_A,
   EFI_PM_USB_USB_CHGPTH_RID_B,
   EFI_PM_USB_USB_CHGPTH_RID_C,
   EFI_PM_USB_USB_CHGPTH_RID_INVALID
}EFI_PM_USB_USB_RID_STS_TYPE;


/*===========================================================================
FUNCTION DEFINITIONS
===========================================================================*/

/* EFI_PMIC_USB_USBIN_VALID */
/** @ingroup efi_pmicUsb_usbin_valid
  @par Summary
  Indicates whether the VBUS on PMIC is high or low.
 
  @param[in]  PmicDeviceIndex    0:Primary, 1:Secondary
  @param[out] Valid              TRUE if VBUS high else FALSE.

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_USBIN_VALID)(
  IN  UINT32  PmicDeviceIndex,
  OUT BOOLEAN *Valid
);


/* EFI_PMIC_USB_CHARGER_PORT_TYPE */
/** @ingroup efi_pmic_usb_charger_port_type
  @par Summary
  reads charger port type.
 
  @param[in]  PmicDeviceIndex    Primary: 0, Secondary:1
  @param[out] PortType           Charging port type.
                                 For more info refer enum EFI_PM_USB_CHGR_PORT_TYPE

  @return
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_CHARGER_PORT_TYPE)
(
  IN  UINT32                       PmicDeviceIndex,
  OUT EFI_PM_USB_CHGR_PORT_TYPE    *PortType
);


/** @ingroup efi_pmic_usb_get_hvdcp
  @par Summary
  This will get hvdcp related status
 
  @param[in]  PmicDeviceIndex   0:Primary, 1:Secondary
  @param[out] HvdcpSts          Reads HVDP status. Refer enum 
                                EFI_PM_HVDCP_STS for more info

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_GET_HVDCP)
(
  IN  UINT32                     PmicDeviceIndex,
  IN  EFI_PM_USB_HVDCP_STS_TYPE  HvdcpSts,
  OUT BOOLEAN                    *Enable
);


/**@ingroup efi_pmic_usb_config_otg
  @par Summary
  This will set the OTG current limit. Valid values are 250 to 1000mAmp 
 
  @param[in]  PmicDeviceIndex   0:Primary, 1:Secondary
  @param[in]  ImAmp             Current limit in milliAmp. 
                                Valid values are 250 to 1000mAm

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_SET_OTG_I_LIMIT)
(
  IN  UINT32                      PmicDeviceIndex,
  IN  UINT32                      ImAmp
);

/**@ingroup efi_pmic_usb_enable_otg
  @par Summary
  This API enables/disables OTG via command register  
 
  @param[in]  PmicDeviceIndex   0:Primary, 1:Secondary
  @param[in]  Enable            TRUE: Enables; FALSE: Disables itv

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_ENABLE_OTG)
(
  IN  UINT32   PmicDeviceIndex,
  IN  BOOLEAN  Enable
);


/**@ingroup efi_pmic_usb_otg_status
  @par Summary
  This API returns the OTG status
 
  @param[in]   PmicDeviceIndex   0:Primary, 1:Secondary
  @param[out]  Ok     OTG is in good standing, FLASE: OTG needs to be re-enabled

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error. \n
*/
typedef
EFI_STATUS (EFIAPI *EFI_PMIC_USB_OTG_STATUS)
(
  IN  UINT32   PmicDeviceIndex,
  OUT BOOLEAN  *Ok
);


/**@ingroup efi_pmic_usb_get_rid_status
  @par Summary
  This API returns RID status   
 
  @param[in]   PmicDeviceIndex   0:Primary, 1:Secondary
  @param[out]  RidStsType        RID status as detected by PMIC. Refer #EFI_PM_USB_RID_STS_TYPE for more info

  @return
  EFI_SUCCESS            -- Function completed successfully. \n
  EFI_DEVICE_ERROR       -- Physical device reported an error. \n
*/
typedef 
EFI_STATUS (EFIAPI *EFI_PMIC_USB_GET_RID_STATUS)
(
  IN   UINT32                           PmicDeviceIndex,
  OUT  EFI_PM_USB_USB_RID_STS_TYPE   *RidStsType
);
/*===========================================================================
PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicUsb_protocol
@par Summary
Qualcomm PMIC USB Protocol interface.

@par Parameters
@inputprotoparams{pmic_usb_proto_params.tex} 
*/

struct _EFI_QCOM_PMIC_USB_PROTOCOL {
  UINT64                                        Revision;
  EFI_PMIC_USB_USBIN_VALID                      UsbinValid;
  EFI_PMIC_USB_CHARGER_PORT_TYPE                ChargerPort;
  EFI_PMIC_USB_GET_HVDCP                        GetHvdcp;
  EFI_PMIC_USB_SET_OTG_I_LIMIT                  SetOtgILimit;
  EFI_PMIC_USB_ENABLE_OTG                       EnableOtg;
  EFI_PMIC_USB_OTG_STATUS                       OtgStatus;
  EFI_PMIC_USB_GET_RID_STATUS                   GetRidStatus;
};


#endif  /* __EFI_PMIC_USB_H__ */
