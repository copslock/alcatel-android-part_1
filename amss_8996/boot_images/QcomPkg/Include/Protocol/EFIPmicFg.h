/** 
  @file  EFIPmicFg.h
  @brief PMIC Fuel Gauge (FG) UEFI Protocol definitions.
           Estimates Battery State of Charge (SOC) and holds other required Battery parameters Information
           For E.g. Battery Charge Current, Voltage
*/
/*=============================================================================
  Copyright (c) 2014 - 2015 Qualcomm Technologies, Incorporated.
  All rights reserved.
  Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
05/27/15    sm      Added OsNonStandardBootSocThreshold and OsStandardBootSocThreshold 
                    config parameters
04/30/15    va      IChgMax and VddMax configuration support for all Jeita windows
                    JEITA Low temp, high temp handle 
04/16/15    al      Added FG restart time
04/30/15    al      Make FCC calibration execution configurable
04/09/15    va      API to read Battery temperature validity
02/25/15    sm      Added API to read SRAM shadow register
02/20/15    al      Create separate struct for battery id related
01/19/15    al      Creating separate struct for JEITA
01/05/15    va      Added Multiple Battery Profile and Profile Parser Status API
09/29/14    va      Jeita Feature
09/25/14    va      Update for New Battery Profile Format
09/23/14    al      Adding charging termination current variable 
08/21/14    va      Enable File Logging 
08/13/14    sm      Added APIs to return Rated capacity and battery temperature
06/20/14    va      Initial Draft

=============================================================================*/

#ifndef __EFIPMICFG_H__
#define __EFIPMICFG_H__

/*===========================================================================
INCLUDE FILES
===========================================================================*/

/*===========================================================================
MACRO DECLARATIONS
===========================================================================*/
/**
  Protocol version.
*/
#define PMIC_FG_REVISION 0x0000000000010008
/** @} */ /* end_addtogroup efi_pmicfg_constants */

/*  Protocol GUID definition */
/** @ingroup efi_pmicSmbchg_protocol */
#define EFI_PMIC_FG_PROTOCOL_GUID \
  { 0x9aed0312, 0x724a, 0x4209, { 0x86, 0x20, 0xf9, 0x3d, 0xe0, 0xbb, 0xd8, 0xfb } }

/*===========================================================================
EXTERNAL VARIABLES
===========================================================================*/
/**
External reference to the PMIC Fg UEFI Protocol GUID.
*/

extern EFI_GUID gQcomPmicFgProtocolGuid;


/*===========================================================================
TYPE DEFINITIONS
===========================================================================*/
/**
Protocol declaration.
*/
typedef struct _EFI_QCOM_PMIC_FG_PROTOCOL  EFI_QCOM_PMIC_FG_PROTOCOL;

typedef struct {
  BOOLEAN  SWJeitaEnable;
  /**< Low battery Temperature upto which battery can be charged */
  INT32  BattTempLimLowCharging;
  /**< SW jeita enable/disable */
  INT32  BattTempJeitaT1Limit;
  /**< JEITA Standard Lower limit of the battery temperature Limit value.for controlling Maximum current (FCC_MAX) */
  INT32  BattTempJeitaT2Limit;
  /**< JEITA Standard Lower limit of the battery temperature Limit value.for controlling Maximum current (FCC_MAX) */
  INT32  BattTempJeitaT3Limit;
  /**< JEITA Standard normal limit for Maximum current (FCC_MAX) */
  INT32  BattTempJeitaT4Limit;
  /**< JEITA Standard Higher limit of the battery temperature Limit value.for controlling Maximum voltage (FV_MAX) */
  INT32  BattTempJeitaT5Limit;
  /**< JEITA Standard High limit of the battery temperature Limit value.for controlling Maximum voltage (FV_MAX) */
  INT32  BattTempHysteresis;

  /**< Battery temperature Hysteresis Limit value.for Jeita zones */
  UINT32  ChgFccMaxInJeitaT2Limit;
  /**< Fast Charging current Max Limit for JEITA Till T2 Limit */
  UINT32  ChgFccMaxInJeitaT3Limit;
  /**< Fast Charging current Max Limit for JEITA Till T3 Limit */
  UINT32  ChgFccMaxInJeitaT4Limit;
  /**< Fast Charging current Max Limit for JEITA Till T4 Limit */
  UINT32  ChgFccMaxInJeitaT5Limit;
  /**< Fast Charging current Max Limit for JEITA Till T5 Limit */

  UINT32  ChgFvMaxInJeitaT2Limit;
  /**< Floating Voltage Max Limit for JEITA Till T2 Limit */
  UINT32  ChgFvMaxInJeitaT3Limit;
  /**< Floating Voltage Max Limit for JEITA Till T3 Limit */
  UINT32  ChgFvMaxInJeitaT4Limit;
  /**< Floating Voltage Max Limit for JEITA Till T4 Limit */
  UINT32  ChgFvMaxInJeitaT5Limit;
  /**< Floating Voltage Max Limit for JEITA Till T5 Limit */

  UINT32 ConservChgFccMaxInJeitaT2Limit;
  /**< Fast Charging current Max Limit for JEITA Till T2 Limit */
  UINT32 ConservChgFccMaxInJeitaT3Limit;
  /**< Fast Charging current Max Limit for JEITA Till T3 Limit */
  UINT32 ConservChgFccMaxInJeitaT4Limit;
  /**< Fast Charging current Max Limit for JEITA Till T4 Limit */
  UINT32 ConservChgFccMaxInJeitaT5Limit;
  /**< Fast Charging current Max Limit for JEITA Till T5 Limit */

  UINT32 ConservChgFvMaxInJeitaT2Limit;
  /**< Floating Voltage Max Limit for JEITA Till T2 Limit */
  UINT32 ConservChgFvMaxInJeitaT3Limit;
  /**< Floating Voltage Max Limit for JEITA Till T3 Limit */
  UINT32 ConservChgFvMaxInJeitaT4Limit;
  /**< Floating Voltage Max Limit for JEITA Till T4 Limit */
  UINT32 ConservChgFvMaxInJeitaT5Limit;
  /**< Floating Voltage Max Limit for JEITA Till T5 Limit */

  UINT64 ChargerCoolOffPeriodMs;
  /**< Cool off period in milli sec when temperature is above max JEITA temp limit but lower than allowable max temp.*/
} EFI_PM_FG_JEITA_CFGDATA_TYPE; 



typedef struct
{
  UINT32   BatteryIdTolerance;
  /**<  FG Parameter Battery ID Tolerance Percentage */

  BOOLEAN  EnableDummyBattId;
  /**<  FG Parameter Enable Dummy Battery ID Detection  */

  UINT32  DummyBattIdMax1;
  /**<  FG Parameter Battery ID Range Max */

  UINT32  DummyBattIdMin1;
  /**<  FG Parameter Dummy Battery ID Range Min */

  UINT32  DummyBattIdMax2;
  /**<  FG Parameter Dummy Battery ID Range Max */

  UINT32  DummyBattIdMin2;
  /**<  FG Parameter Battery ID Range Min */

  UINT32  DebugBoardBattIdMin;
  /**<  FG Parameter Battery ID Range Max */

  UINT32  DebugBoardBattIdMax;
  /**<  FG Parameter Battery ID Range Min */

  UINT32 AnalogBattIdMin;
  /**<  FG Parameter Analog Battery ID Range Max */

  UINT32 AnalogBattIdMax;
  /**<  FG Parameter Analog Battery ID Range Max */

}EFI_PM_FG_BATTID_CFGDATA_TYPE;

/**
  Battery Config Data.
*/
typedef struct 
{
  UINT32   CfgVersion;
  /**< Version number for CFG file*/

  INT32    BattTempLimLow;
  /**< Lower limit of Batery temperature value */
  INT32    BattTempLimHigh;
  /**< Upper limit of Batery temperature value */

  UINT32   WPBattVoltLimHighInmV;
  /**< Max Value for battery voltage in mV for WP Platform*/  
  INT32   WPBattCurrLimHighInmA;
  /**< Max value for battery current in mA for WP Platform*/

  UINT32   ChgVddSafeInmV;
  /**< Value of VDD_SAFE in mV */
  UINT32   ChgIbatSafeInmA;
  /**< Value of IBAT_SAFE in mA */

  UINT32   ChgFvMax;
  /**< Value of FV_MAX in mV */
  UINT32   ChgFccMax;
  /**< Value of FCC_MAX in mA */

  BOOLEAN  BadBattShutdown;
  /**< Shutdown the device if bad battery condition is TRUE */

  BOOLEAN  ReadBattProfileFromPartition;
  /**< Whether to Read Battery Profile form Partion */

  BOOLEAN  ChkBattProfileFirstFromEfiEspOrPlat;
  /**< First Check Battery Profile from EFIESP or PLAT Partition. */

  BOOLEAN  PrintChargerAppDbgMsg;
  /**< Flag to Enable/Disable Charger debug messages.  */

  BOOLEAN  PrintChargerAppDbgMsgToFile;
  /**< Flag to Enable/Disable Charger debug messages to File.  */

  BOOLEAN  DumpSram;
  /**<  Dump SRAM contents  */

  UINT32   DumpSramStartAddr;
  /**<  Dump SRAM contents Start Address */

  UINT32   DumpSramEndAddr;
  /**<  Dump SRAM contents End Address */

  UINT32   DumpSramDuration;
  /**<  Dump SRAM contents timer Duration in ms  */

  BOOLEAN  TempCacheTimer;
  /**<  Dump SRAM contents  */

  UINT32   TempCacheTimerDuration;
  /**<  FG Parameter Cache timer Duration in ms  */

  UINT32   ChrgingTermCurrent;
  /**<  Charging termination current*/

  EFI_PM_FG_BATTID_CFGDATA_TYPE BattIdCfgData;
 /**< Battery ID configuration ID*/

  BOOLEAN FgCondRestart;
  /**<  FG Conditional Restart on reset */

  UINT32  VBattEstDiffThreshold;
  /**<  FG Fuel Gauge Voltage Battery Threshold difference to decide FG Restart condition at FG Algo start */

  EFI_PM_FG_JEITA_CFGDATA_TYPE JeitaCfgData;

  BOOLEAN  SupportQcomChargingApp;
  /**<  Support QCOM charging application  */

  BOOLEAN  FullBattChargingEnabled;
  /**<  Enable full battery charging  */

  UINT32 SDPMaxChargeCurrent;
  /**< Maximum charge current allowed for SDP port*/

  UINT32 CDPMaxChargeCurrent; 
  /**< Maximum charge current allowed for CDP port*/

  UINT32 DCPMaxChargeCurrent;
  /**< Maximum charge current allowed for DCP port*/

  BOOLEAN FgForceOtpProfile;
  /**<  Force battery OTP Prpfile to use */

  UINT32  FgOtpProfileNum;
  /**< OTP profile to be forced for unknown battery */
  
  UINT32 OtherChargerChargeCurrent;
  /**< Maximum charge current allowed for other charger type*/

  UINT32 ConservChgFvMax;
  /**< Conservative Floating Voltage Max Limit */

  UINT32 UnknownBattBehavior;
  /**<Shutdown device upon unknown battery detection */
  /**< 0: Shuts down device, 1: Boot to HLOS if battery more than threshold else shutdown
       2: Conservative Charging 3: Regular charging */

  UINT32 BootToHLOSThresholdMv;
  /**<Battery voltage at which device will boot to HLOS if debug board or unknown battery detected */

  BOOLEAN  RunFccCalibration;
  /**< Shutdown the device if bad battery condition is TRUE */
  UINT32 RestartFgInactiveHrs;
  /**<Force FG to restart after device being inactive for RestartFgInactiveHrs hours */

  UINT32 OsStandardBootSocThreshold;
  /**< Minimum SOC Threshold before allowing to boot to HLOS in case of Standard OS boot*/

  UINT32 OsNonStandardBootSocThreshold;
  /**< Minimum SOC Threshold before allowing to boot to HLOS in case of Non Standard OS boot*/

} EFI_PM_FG_CFGDATA_TYPE;
/** @} */ /* end_addtogroup efi_pmicFg_data_types */


/*!
 * \enum EFI_PM_FG_PARSER_STATUS_TYPE
 *  \brief Enumeration for Profile Parser Status
 */
 typedef enum _EFI_PM_FG_PROFILE_PARSER_STATUS
{
  PM_PARSER_STATUS_GOOD_MATCH,
  PM_PARSER_STATUS_NO_ID_MATCH,
  PM_PARSER_STATUS_NO_DPP,
  PM_PARSER_STATUS_MEM_ALLOC_ERROR,
  PM_PARSER_STATUS_PARSE_ERROR,
  PM_PARSER_STATUS_OTHER_ERROR,
  PM_PARSER_STATUS_CONFIG_ERROR,
  PM_PARSER_STATUS_BAD_PARAM,
  PM_PARSER_STATUS_DUMMY_BATTERY
} EFI_PM_FG_PROFILE_PARSER_STATUS;

/*!
 * \enum EFI_PM_FG_PROFILE_STATUS
 *  \brief Enumeration for Profile Status
 */
 typedef struct _EFI_PM_FG_PROFILE_STATUS
{
  /* Profile Parser Status */
  EFI_PM_FG_PROFILE_PARSER_STATUS ParserStatus;

} EFI_PM_FG_PROFILE_STATUS;


typedef enum
{
  EFI_PM_FG_BATT_TYPE_NORMAL,
  EFI_PM_FG_BATT_TYPE_SMART,
  EFI_PM_FG_BATT_BATT_EMULATOR,
  EFI_PM_FG_BATT_TYPE_DEBUG_BOARD,
  EFI_PM_FG_BATT_TYPE_UNKNOWN,
  EFI_PM_FG_BATT_TYPE_INVALID
}EFI_PM_FG_BATT_TYPE; 

/*===========================================================================
  FUNCTION DEFINITIONS
===========================================================================*/
/* EFI_PMIC_FG_INIT */
/** @ingroup efi_pmicFgInit
  @par Summary
  Initializes FG Module
 
  @param[in] PmicDeviceIndex: Pmic Device Index (0 for primary)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_INIT)( UINT32 PmicDeviceIndex);
/* EFI_PMIC_FG_EXIT */
/** @ingroup efi_pmicFgExit
  @par Summary
  Exit FG Module
 
  @param[in] PmicDeviceIndex: Pmic Device Index (0 for primary)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_EXIT)( UINT32 PmicDeviceIndex);

/* EFI_PMIC_FG_GET_CHARGE_CURRENT */
/** @ingroup efi_pmicFgGetChargeCurrent
  @par Summary
  Get Charge Current
 
  @param[in]  PmicDeviceIndex    : Pmic Device Index (0 for primary)
  @param[out] ChargeCurrent      : Charge Current in mA

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_CHARGE_CURRENT)(
  IN UINT32 PmicDeviceIndex,
  OUT INT32 *ChargeCurrent
);

/* EFI_PMIC_FG_GET_SOC */
/** @ingroup efi_pmicFgGetSoc
  @par Summary
  Get State of Charge of the battery
 
  @param[in]  PmicDeviceIndex    : Pmic Device Index (0 for primary)
  @param[out] StateOfCharge      : State of Charge of battery (%)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_SOC)(
  IN UINT32 PmicDeviceIndex,
  OUT UINT32 *StateOfCharge
);

/* EFI_PMIC_FG_GET_RATED_CAPACITY */
/** @ingroup efi_pmicFgGetRatedCapacity
  @par Summary
  Get full charge rated capacity of the battery
 
  @param[in]  PmicDeviceIndex    : Pmic Device Index (0 for primary)
  @param[out] RatedCapacity      : Full charge capacity of the battery (mAh)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_RATED_CAPACITY)(
  IN UINT32 PmicDeviceIndex,
  OUT UINT32 *RatedCapacity
);

/* EFI_PMIC_FG_GET_BATTERY_VOLTAGE */
/** @ingroup efi_pmicFgGetBatteryVoltage
  @par Summary
  Gets battery voltage
 
  @param[in]  PmicDeviceIndex    : Pmic Device Index (0 for primary)
  @param[out] BatteryVoltage     : Battery voltage (mV)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_BATTERY_VOLTAGE)(
  IN  UINT32 PmicDeviceIndex,
  OUT UINT32 *BatteryVoltage
);

/* EFI_PMIC_FG_GET_RATED_CAPACITY */
/** @ingroup efi_pmicFgGetRatedCapacity
  @par Summary
  Get full charge rated capacity of the battery
 
  @param[out] RatedCapacity      : Full charge capacity of the battery (mAh)

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_BATTERY_CONFIG_DATA)(
  OUT EFI_PM_FG_CFGDATA_TYPE *BatteryCfgData
);

/* EFI_PMIC_FG_GET_BATT_TEMP */
/** @ingroup efi_pmicFgGetBatteryTemperature
  @par Summary
  Get battery temperature
 
  @param[out] BattTemp           : Battery temperature

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_BATTERY_TEMPERATURE)(
  OUT INT32 *pBatteryTemperature
);

/* EFI_PMIC_FG_GET_PROFILESTATUS*/
/** @ingroup efi_pmicFgGetProfileParserStatus
  @par Summary
  Get Profile Parser Status Type
 
  @param[out] ProfileParserStatus : Profile Parser Status

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_PROFILESTATUS)(
  OUT EFI_PM_FG_PROFILE_STATUS *ProfileStatus
);

/* EFI_PMIC_FG_GET_BATT_ID */
/** @ingroup efi_pmicFgGetBatteryId
  @par Summary
  Get battery id
 
  @param[out] BattId           : Battery id

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_BATTERY_TYPE)(
  OUT EFI_PM_FG_BATT_TYPE *BatteryType
);

/* EFI_PMIC_FG_GET_SHADOW_CURRENT */
/** @ingroup efi_pmicFgGetShadowCurrent
  @par Summary
  Gets SRAM Shadow current
 
  @param[in]  PmicDeviceIndex   : Pmic Device Index (0 for primary)
  @param[out] pShadowCurrent    : Shadow current value in mA

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_FG_GET_SHADOW_CURRENT)(
   IN UINT32  PmicDeviceIndex,
  OUT INT32  *pShadowCurrent
);

/* EFI_PMIC_IS_TEMPERATURE_VALID */
/** @ingroup efi_pmicFgGetShadowCurrent
  @par Summary
    battery temperature is valid or Invaid for read
 
  @param[out] bValid    : TRUE is battery temprature is valid to read

 @return 
  EFI_SUCCESS           -- Function completed successfully. \n
  EFI_INVALID_PARAMETER -- Parameter is invalid. \n
  EFI_DEVICE_ERROR      -- Physical device reported an error. \n
  EFI_NOT_READY         -- Physical device is busy or not ready to
                           process this request.
*/
typedef
EFI_STATUS(EFIAPI *EFI_PMIC_IS_TEMPERATURE_VALID)(
  OUT BOOLEAN  *bValid
);


/*===========================================================================
  PROTOCOL INTERFACE
===========================================================================*/
/** @ingroup efi_pmicFg_protocol
  @par Summary
  Qualcomm PMIC Battery Monitoring System (FG) Protocol interface.

  @par Parameters
  @inputprotoparams{pmic_fg_proto_params.tex} 
*/
struct _EFI_QCOM_PMIC_FG_PROTOCOL
{
  UINT64                                 Revision;
  EFI_PMIC_FG_INIT                       FgInit;
  EFI_PMIC_FG_GET_SOC                    GetSoc;
  EFI_PMIC_FG_GET_CHARGE_CURRENT         GetChargeCurrent;
  EFI_PMIC_FG_GET_RATED_CAPACITY         GetRatedCapacity;
  EFI_PMIC_FG_GET_BATTERY_VOLTAGE        GetBatteryVoltage;
  EFI_PMIC_FG_GET_BATTERY_CONFIG_DATA    GetBatteryConfigData;
  EFI_PMIC_FG_GET_BATTERY_TEMPERATURE    GetBatteryTemperature;
  EFI_PMIC_FG_GET_PROFILESTATUS          GetProfileStatus;
  EFI_PMIC_FG_GET_BATTERY_TYPE           GetBatteryType; 
  EFI_PMIC_FG_EXIT                       FgExit;
  EFI_PMIC_FG_GET_SHADOW_CURRENT         GetShadowCurrent;
  EFI_PMIC_IS_TEMPERATURE_VALID          IsTemperatureValid;

};

#endif  /* __EFIPMICFG_H__*/
