#ifndef __RPMBLIB_H__
#define __RPMBLIB_H__

/** @file RpmbLib.h
   
  This file provides SDCC's RPMB Library external definitions. 
  
  Copyright (c) 2011-2013, 2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
 
**/

/*=============================================================================
                              EDIT HISTORY


when         who     what, where, why
----------   ---     ---------------------------------------------------------
2015-04-17   jt/rm   Add support for ufs rpmb
2013-02-19   jt      Added support for configuring GPT/RPMB partitions 
2012-04-03   bn      Added ProvisionRpmbKey function definition
2011-11-18   bn      Initial Version 

=============================================================================*/

/**
  Function: RpmbWritePackets
 
  Description:
  This function calls into the RPMB driver to write the data to the RPMB partition.
   
  @param RpmbWriteBuffer [IN]       Pointer to RPMB packets that have been formatted
                                    to RPMB data frames as specified by the eMMC specification,
                                    each packet contains 256 bytes (half sector) of data
                                    and other RPMB header fields.
 
  @param PktCount [IN]              Indicates the number of packets contained
                                    in the passed in RpmbData buffer (each packet contains
                                    half sector data plus other RPMB header fields).
 
  @param ResponsePkt [OUT]          Pointer to a Response packet of the last RPMB Write
                                    transfer that the eMMC returns. This Response packet
                                    can be used to authenticate and validate the status
                                    of the entire Write transfer.
 
  @return EFI_SUCCESS               RPMB write successful.
  @return EFI_INVALID_PARAMETER     Invalid parameter.
  @return EFI_DEVICE_ERROR          There was unknown error
**/
EFI_STATUS
EFIAPI
RpmbWritePackets (
   IN  UINT8  *RpmbWriteBuffer, 
   IN UINT32  PktCount, 
   OUT UINT8* ResponsePkt
);

/**
  Function: GetRpmbCounterPkt
 
  Description:
  This function calls into the RPMB driver to retrieve the eMMC's Read Counter
  Response packet
 
  @param ResponsePkt [OUT]          Pointer to a 512-byte Response packet from the
                                    eMMC in response to the Read Counter Request.
                                    This Response packet can be used to retrieve
                                    the eMMC's Write Counter value.
 
  @return EFI_SUCCESS               Successfully read the eMMC's Write Counter packet.
  @return EFI_INVALID_PARAMETER     Invalid parameter.
  @return EFI_DEVICE_ERROR          There was unknown error
**/
EFI_STATUS
EFIAPI
GetRpmbCounterPkt (
   OUT  UINT32  *RpmbResponsePkt
);

/**
  Function: GetRpmbPartitionSize
 
  Description:
  This function retrieves the RPMB's partition size in unit of bytes
 
  @param RpmbSizePtr [OUT]          Pointer to the RPMB patition size in unit
                                    of bytes.
 
  @return EFI_SUCCESS               Successfully retrieve the RPMB partition size.
  @return EFI_INVALID_PARAMETER     Invalid parameter.
**/
EFI_STATUS
EFIAPI
GetRpmbPartitionSize (
   OUT  UINT32  *RpmbSizePtrInBytes
);

/**
  Function: ProvisionRpmbKey
 
  Description:
  This function gets the KeyProvisionPkt from TZ and provisions the RPMB with the
  key embedded in the KeyProvisionPkt.
                      
  @return EFI_SUCCESS               Initialization successful.
  @return EFI_INVALID_PARAMETER     Invalid parameter.
  @return EFI_DEVICE_ERROR          
**/
EFI_STATUS
EFIAPI
ProvisionRpmbKey (VOID);

/**
  Function: RpmbInit
 
  Description:
  This function initializes the RPMB. It performs a sequence of calls
  into the Content Generator (CG) in TZ and the RPMB driver to initialize
  the RPMB. Upon boot up, if the eMMC Key has not been provisioned,
  it will provision the key. It then locks the GetKeyProvisionPkt from
  any future access. It then sends the Reliable Write Count info to CG.
  Then it reads the first half (64KB) of the RPMB partition. 
   
  @param RpmbReadBuffer             Pointer to data read from the RPMB
                                    partition
                      
  @return EFI_SUCCESS               Initialization successful.
  @return EFI_INVALID_PARAMETER     Invalid parameter.
  @return EFI_DEVICE_ERROR          
**/
EFI_STATUS
EFIAPI
RpmbInit (
  IN      UINT32  *RpmbReadBuffer
);

/**
  Function: ProvisionRpmbTestKey
 
  Description:
  This function sends a QSEE command to TZ in order to provision
  the RPMB with the test key.
                      
  @return EFI_SUCCESS               Initialization successful.
  @return EFI_INVALID_PARAMETER     Invalid parameter. 
**/
EFI_STATUS
EFIAPI
ProvisionRpmbTestKey (UINT32 *Result);

/**
  Function: ConfigStorPartitions
 
  Description:
  This function parses the config file and updates the BufferPtr with the 
  partition addition information. 
                      
  @return EFI_SUCCESS               Initialization successful.    
  @return EFI_INVALID_PARAMETER     Invalid parameter.
  @return EFI_LOAD_ERROR            Failed to open the parser. 
  @return EFI_UNSUPPORTED           Failed to enumerate the key values 
**/
EFI_STATUS
EFIAPI
ConfigStorPartitions (UINT32 DevId, UINT8 *PartitionGuid, 
                      UINT32 Version, UINT32 *NumPartitions, UINT8 *BufferPtr,
                      VOID *BlkIoMedia);

/**
  Function: InitPartitionConfig
 
  Description:
  This function loads the partition config file from the FV.
                      
  @return EFI_SUCCESS               Initialization successful.        
**/
EFI_STATUS
EFIAPI
InitPartitionConfig(VOID);

#endif /* __RPMBLIB_H__ */

