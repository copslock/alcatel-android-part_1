#ifndef __MDPPLATFORMLIB_H__
#define __MDPPLATFORMLIB_H__
/*=============================================================================
 
  File: MDPPlatformLib.h
 
  Header file for MDP platform functions
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include <Uefi.h>
#include <Protocol/EFIPlatformInfo.h>
#include <Protocol/EFIChipInfo.h>
#include "MDPTypes.h"


/*===========================================================================
                        Defines and Structs
===========================================================================*/

/* Platform identification information
 */
typedef struct 
{
   bool32                               bPlatformDetected;      /* Indicates if the palatform ID was successfully detected */
   
   EFI_PLATFORMINFO_PLATFORM_INFO_TYPE  sEFIPlatformType;       /* EFI Platform information */
   EFIChipInfoIdType                    sEFIChipSetId;          /* EFI Chipset information */
   EFIChipInfoFamilyType                sEFIChipSetFamily;      /* EFI Chipset family information */ 
   
   uint32                               uPrimaryPanelId;        /* Primary panel ID */
   bool32                               bSWRender;              /* Indicates if the displayDxe is SW renderer only */
} MDPPlatformInfo;

/* Bit fields for uFlags in PlatformDSIDetectParams structure. 
    Currently, it is used to define the clock lane config but this can be grown in future.
 */
#define MDPPLATFORM_PANEL_DETECT_FLAG_CLOCK_FORCEHS    0x00000001  /*Force clock lane in HS*/

/* Panel Reset related information
*/

/* Bit Fields for uFlags in the below structure */
#define MDDPLATFORM_PANEL_RESET_FLAG_USE_CUSTOM_DELAY    0x00000001  /* Wait for the specified delay instead of the default after reset */
#define MDDPLATFORM_PANEL_RESET_FLAG_FORCE_RESET         0x00000002  /* Force the panel reset */

typedef struct
{
    uint32 uFlags;              /* reset options */
    uint32 uCustomDelayMs;      /* This field is valid only if USE_CUSTOM_DELAY is set in uFlags. A value of 0 -> no delay */
}MDPPlatformPanelResetInfo;

/* Panel XML configuration 
 */
typedef struct
{
    int8   *pPanelXMLConfig;
    uint32  uConfigSize;
    uint8   uDefaultVendor;
    uint8   uDefaultRevision;
} MDPPlatformPanelInfo;


/* HDMI configuration*/
typedef struct
{
  uint32 uCecDataGpio;
  uint32 uDDCDataGpio;
  uint32 uDDDClkGpio;
  uint32 uHPDGpio;
} HDMI_PinInfo;


/* List of support platform specific configurations 
 */
typedef enum
{
    MDPPLATFORM_CONFIG_GETPANELCONFIG = 0x0,
    MDPPLATFORM_CONFIG_POWERUP,
    MDPPLATFORM_CONFIG_POWERDOWN,    
    MDPPLATFORM_CONFIG_GETPANELID, 
    MDPPLATFORM_CONFIG_SETBACKLIGHT,
    MDPPLATFORM_CONFIG_GETPLATFORMINFO,
    MDPPLATFORM_CONFIG_SW_RENDERER,
    MDPPLATFORM_CONFIG_RESETPANEL,
    MDPPLATFORM_CONFIG_MAX
} MDPPlatformConfigType;


/* Backlight configuration */
typedef struct
{
    bool32                          bEnable;             /* Enable/disable the backlight/brightness power grid */
    uint32                          uLevel;              /* New brightness level in percentage, 0-100% */
    MDP_Panel_BacklightType         eBacklightType;      /* Configuration of the backlight interface module */
    union {
        MDP_PmicBacklightControlType  eBacklightCtrl;      /* PMIC module for backlight configuration  */
    } uBacklightCntrl;

} BacklightConfigType;


/* Power configuration */
typedef struct
{
    MDP_PmicModuleControlType       ePMICSecondaryPower;      /* Configuration for PMIC based secondary power source */
} PowerConfigType;


/* Union of panel properties passed to MDPPlatformConfigure() function
*/
typedef union
{
    MDPPlatformPanelInfo       sPlatformPanel;      /* MDPPLATFORM_CONFIG_GETPANELCONFIG(Primary) */
    HDMI_PinInfo               sHDMIPinConfig;      /* MDPPLATFORM_CONFIG_GETPANELCONFIG(HDMI) */
    BacklightConfigType        sBacklightConfig;    /* MDPPLATFORM_CONFIG_SETBACKLIGHT */
    MDPPlatformInfo            sPlatformInfo;       /* MDPPLATFORM_CONFIG_GETPLATFORMINFO */
    MDPPlatformPanelResetInfo  sPlatformPanelReset; /* MDPPLATFORM_CONFIG_RESETPANEL */
    PowerConfigType            sPowerConfig;        /* MDPPLATFORM_CONFIG_POWERUP/MDPPLATFORM_CONFIG_POWERDOWN */
} MDPPlatformParams;
/*===========================================================================

                        Public Functions

===========================================================================*/

/* MDP platform specific configuration function 
 */
MDP_Status MDPPlatformConfigure(MDP_Display_IDType eDisplayId, MDPPlatformConfigType eConfig, MDPPlatformParams *pPlatformParams);


#endif // __MDPLIB_H__


