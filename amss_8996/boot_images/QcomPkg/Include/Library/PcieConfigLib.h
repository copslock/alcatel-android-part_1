/** @file PcieConfigLib.h

  PCIe driver header files with protocols defined

  Copyright (c) 2012-2015, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY


 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 05/13/15   tselvam  Unified code for multi platforms
 01/26/15   tselvam  First checkin for 8996
 04/18/14   hozefak  First checkin for 8994
=============================================================================*/
#ifndef PCIECONFIGLIB_H
#define PCIECONFIGLIB_H 

typedef enum e_PCIe_RP_IDX
{
	PCIe_RP0,
	PCIe_RP1,
	PCIe_RP2,
	PCIe_RP3
}PCIe_RP_IDX;

/**
  The Entry Point for PCI Bus module. The user code starts with this function.

  Calls into PCIe_Enable

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.
  @param[in] SystemTable    A pointer to the EFI System Table.

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurred when executing this entry point.

**/

EFI_STATUS
EFIAPI
PCIe_Enable(VOID);

/**
 * Deinitialize by turning off the clocks
 * 
 * @param[in] void
 * 
 * @return EFI_STATUS
 */
EFI_STATUS PCIe_Deinitialize(VOID);

/**
  Read the config table and fill up the linked list.

 Read the number of root ports

  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurred when executing this entry point.

**/
EFI_STATUS
EFIAPI
PCIe_Read_Config(VOID);

/**
  Read the config space of PCIe rootnode.

  @param[in] rpIndex    	root port index starting from 0 to n...
  @param[in] offset    		offset in the config space to read from.

  @retval 0xFFFFFFFF       Invalid rootnode index rpIndex passed .
  @retval other            32-bit value present in the configuration space register.

**/

UINT32
EFIAPI
PCIeLib_Config_Read_32 (
		IN PCIe_RP_IDX rpIndex,
		IN UINT32 offset);

#endif/* #ifndef PCIECONFIGLIB_H*/
