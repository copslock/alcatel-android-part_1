#ifndef PM_SMBCHG_OTG_H
#define PM_SMBCHG_OTG_H

/*! \file
*  \n
*  \brief  pm_SMBB.h PMIC-SMBC MODULE RELATED DECLARATION 
*  \details  This header file contains functions and variable declarations 
*  to support Qualcomm PMIC SMBCHG OTG (Switch-Mode Battery Charger) module. The 
*  Switched-Mode Battery Charger (SMBCHG OTG) module includes a buck regulated 
*  battery charger with integrated switches. The SMBCHG OTG module, along with the 
*  Over Voltage Proection (OVP) module will majorly be used by charger 
*  appliation for charging Li-Ion batteries with high current (up to 2A).
*  \n &copy;
*  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/* =======================================================================
                                Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/20/14   al      Architecture update
04/18/14   al      Updated copyright 
09/11/12   al      Initial version. 
========================================================================== */
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/*This API configures the OTG settings */
typedef enum pm_smbchg_otg_uvlo_sensor_src_type
{
 PM_SMBCHG_OTG_UVLO_SENSOR__ANA,     /**<Analog Comparator       */
 PM_SMBCHG_OTG_UVLO_SENSOR__FG,      /**<Fuel Gauge ADC          */
 PM_SMBCHG_OTG_UVLO_SENSOR__BOTH,    /**<Or of both Analog and FG*/
 PM_SMBCHG_OTG_UVLO_SENSOR__INVALID  /**<Invalid type sensor     */
}pm_smbchg_otg_uvlo_sensor_src_type;

typedef enum pm_smbchg_otg_ctrl_type
{
  PM_SMBCHG_OTG_CTR__CMD_REG_RID_DISABLED,  /**< Command register with RID Disabled*/  
  PM_SMBCHG_OTG_CTR__PIN_CTRL_RID_DISABLED, /**< Pin control with RID Disabled     */
  PM_SMBCHG_OTG_CTR__CMD_REG_RID_ENABLED,   /**< Command register with RID Enabled */
  PM_SMBCHG_OTG_CTR__AUTO_OTG_RID_ENABLED,  /**< Auto OTG (RID Enabled)            */
  PM_SMBCHG_OTG_CTR__INVALID
}pm_smbchg_otg_ctrl_type;

typedef struct pm_smbchg_otg_cfg_type
{
  boolean                            hv_otg_protection_en;    /**< high voltage OTG enable/disable        */
  boolean                            otg_pin_pol_active_high; /**< OTG pin polling on active high or low  */
  pm_smbchg_otg_ctrl_type            otg_ctrl_type;           /**< OTG control type                       */
  pm_smbchg_otg_uvlo_sensor_src_type uvlo_sensor_src;         /**< under voltage lockout sensor source    */
}pm_smbchg_otg_cfg_type;


/*===========================================================================

                 SMBCHG OTG DRIVER FUNCTION PROTOTYPES

===========================================================================*/

/**
 * @brief This function configures OTG.
 * 
 * @details
 *  This function configures OTG.
 * 
 * @param[in] pmic_device_index. Secondary: 1, Primary: 0
 * @param[in] otg_cfg:    Refer #pm_smbchg_otg_cfg_type for more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_config_otg(uint32 pmic_device, pm_smbchg_otg_cfg_type* otg_cfg);

/**
 * @brief This function returns OTG configuration.
 * 
 * @details
 *  This function returns OTG configuration.
 * 
 * @param[in] pmic_device_index. Secondary: 1, Primary: 0
 * @param[out] otg_cfg:    Refer #pm_smbchg_otg_cfg_type for 
 *       more info
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_get_otg_config(uint32 pmic_device, pm_smbchg_otg_cfg_type* otg_cfg);



 /**
 * @brief This function sets the battery under voltage lockout limit
 * 
 * @details
 *  This function sets the battery under voltage lockout limit
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] milli_volt          valid range is 2700 to 3300 milli volt
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_set_otg_batt_uvlo(uint32 pmic_device, uint32 milli_volt);


 /**
 * @brief This function returns the battery under voltage lockout limit
 * 
 * @details
 *  This function returns the battery under voltage lockout limit
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[out] milli_volt          valid range is 2700 to 3300 milli volt
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_get_otg_batt_uvlo(uint32 pmic_device, uint32 *milli_volt);

 /**
 * @brief This function  sets current limit for OTG
 * 
 * @details
 *  This function  sets current limit for OTG
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[in] otg_limit_ma        Current limit in milliamp. Values is from 250 to 1000 mAmp
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_set_otg_i_limit(uint32 pmic_device, uint32 otg_limit_ma);


 /**
 * @brief This function  sets current limit for OTG
 * 
 * @details
 *  This function  sets current limit for OTG
 * 
 * @param[in] pmic_device_index.  Primary: 0 Secondary: 1
 * @param[out] otg_limit_ma        Current limit in milliamp. Values is from 250 to 1000 mAmp
 *                                
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
 *          version of the PMIC.
 *          PM_ERR_FLAG__SUCCESS               = SUCCESS.
 *
 */
pm_err_flag_type pm_smbchg_otg_get_otg_i_limit(uint32 pmic_device, uint32 *otg_limit_ma);



#endif /* PM_SMBCHG_OTG_H*/

