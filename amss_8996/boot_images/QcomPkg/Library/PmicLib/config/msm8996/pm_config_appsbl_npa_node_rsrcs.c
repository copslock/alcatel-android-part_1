/*! \file  pm_config_appsbl_npa_node_rsrcs.c
 *  
 *  \brief  File Contains the PMIC NPA CMI Code
 *  \details  This file contains the needed definition and enum for PMIC NPA layer.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: MSM8996-pm8996-pmi8994_0p11 - LOCKED

 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2015 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                        INCLUDE HEADER FILES

===========================================================================*/

#include "pm_npa_device_clk_buff.h"
#include "pm_npa_device_ldo.h"
#include "pm_npa_device_smps.h"
#include "pm_npa.h"
#include "pm_appsbl_proc_npa.h"
#include "pmapp_npa.h"
/*===========================================================================

                        MACRO DEFINITIONS

===========================================================================*/

#define PMIC_NPA_CLIENT_NODE_DISP_EXT_HDMI "/node/pmic/client/disp_ext_hdmi"
#define PMIC_NPA_CLIENT_NODE_DISP_PRIM "/node/pmic/client/disp_prim"
#define PMIC_NPA_CLIENT_NODE_PCIE "/node/pmic/client/pcie"
#define PMIC_NPA_CLIENT_NODE_RAIL_CX "/node/pmic/client/rail_cx"
#define PMIC_NPA_CLIENT_NODE_RAIL_MX "/node/pmic/client/rail_mx"
#define PMIC_NPA_CLIENT_NODE_USB_HS1 "/node/pmic/client/usb_hs1"
#define PMIC_NPA_CLIENT_NODE_USB_HS2 "/node/pmic/client/usb_hs2"
#define PMIC_NPA_CLIENT_NODE_USB_HSIC "/node/pmic/client/usb_hsic"
#define PMIC_NPA_CLIENT_NODE_USB_SS1 "/node/pmic/client/usb_ss1"
/*===========================================================================

                        OTHER VARIABLES DEFINITION

===========================================================================*/

extern pm_npa_pam_client_cfg_type
pm_pam_disp_ext_hdmi_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_disp_prim_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_pcie_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_rail_cx_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_rail_mx_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_usb_hs1_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_usb_hs2_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_usb_hsic_rails_info [];

extern pm_npa_pam_client_cfg_type
pm_pam_usb_ss1_rails_info [];

/*===========================================================================

                        VARIABLES DEFINITION

===========================================================================*/


/* NPA NODE DEPENDENCY */

/* DISP_EXT_HDMI Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_disp_ext_hdmi [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 12 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 28 ),
   PMIC_NPA_NODE_DEP_LIST( A, smps, 4 ),
};

/* DISP_PRIM Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_disp_prim [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 2 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 14 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 22 ),
   PMIC_NPA_NODE_DEP_LIST( A, smps, 4 ),
};

/* PCIE Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_pcie [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 30 ),
};

/* RAIL_CX Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_rail_cx [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, smps, 1 ),
};

/* RAIL_MX Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_rail_mx [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, smps, 2 ),
};

/* USB_HS1 Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_usb_hs1 [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, clk, 8 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 6 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 24 ),
};

/* USB_HS2 Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_usb_hs2 [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, clk, 8 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 6 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 24 ),
};

/* USB_HSIC Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_usb_hsic [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, clk, 8 ),
};

/* USB_SS1 Client */
npa_node_dependency
pm_appsbl_proc_client_dev_deps_usb_ss1 [] =
{
   PMIC_NPA_NODE_DEP_LIST( A, clk, 8 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 6 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 24 ),
   PMIC_NPA_NODE_DEP_LIST( A, ldo, 28 ),
};

/* NPA NODE RESOURCE */
pm_npa_node_resource_info
pm_npa_appsbl_pam_node_rsrcs [] =
{
   {
      PMIC_NPA_GROUP_ID_DISP_EXT_HDMI, // Resource Name
      PMIC_NPA_MODE_ID_GENERIC_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_disp_ext_hdmi_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_DISP_EXT_HDMI, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_disp_ext_hdmi, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_disp_ext_hdmi), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_DISP_PRIM, // Resource Name
      PMIC_NPA_MODE_ID_GENERIC_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_disp_prim_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_DISP_PRIM, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_disp_prim, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_disp_prim), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_PCIE, // Resource Name
      PMIC_NPA_MODE_ID_PCIE_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_pcie_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_PCIE, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_pcie, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_pcie), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_RAIL_CX, // Resource Name
      PMIC_NPA_MODE_ID_CORE_RAIL_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_rail_cx_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_RAIL_CX, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_rail_cx, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_rail_cx), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_RAIL_MX, // Resource Name
      PMIC_NPA_MODE_ID_CORE_RAIL_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_rail_mx_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_RAIL_MX, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_rail_mx, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_rail_mx), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_USB_HS1, // Resource Name
      PMIC_NPA_MODE_ID_USB_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_usb_hs1_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_USB_HS1, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_usb_hs1, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_usb_hs1), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_USB_HS2, // Resource Name
      PMIC_NPA_MODE_ID_USB_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_usb_hs2_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_USB_HS2, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_usb_hs2, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_usb_hs2), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_USB_HSIC, // Resource Name
      PMIC_NPA_MODE_ID_USB_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_usb_hsic_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_USB_HSIC, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_usb_hsic, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_usb_hsic), // Deps Count & Deps Node Dependency Count
   },
   {
      PMIC_NPA_GROUP_ID_USB_SS1, // Resource Name
      PMIC_NPA_MODE_ID_USB_MAX - 1, // Maximum Value
      NPA_RESOURCE_SINGLE_CLIENT, // Resource Attribute
      (void*) pm_pam_usb_ss1_rails_info, // Resource User Data
      PMIC_NPA_CLIENT_NODE_USB_SS1, // Node Name
      NPA_NODE_DEFAULT, // Node Attributes
      NULL,
      pm_appsbl_proc_client_dev_deps_usb_ss1, // Node Dependency
      NPA_ARRAY_SIZE(pm_appsbl_proc_client_dev_deps_usb_ss1), // Deps Count & Deps Node Dependency Count
   },
};

uint32 num_of_pm_appsbl_nodes [] = { 9 };


pm_npa_remote_name_type
pmic_npa_clk_remote_resources [] =
{
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, clk, 8), "clka\x08\x00\x00\x00"    },
};

pm_npa_remote_resource_type
pmic_npa_remote_clk [1] = 
{
   {
      pmic_npa_clk_remote_resources,
      1
   }
};

pm_npa_remote_name_type
pmic_npa_ldo_remote_resources [] =
{
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 2), "ldoa\x02\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 6), "ldoa\x06\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 12), "ldoa\x0C\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 14), "ldoa\x0E\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 22), "ldoa\x16\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 24), "ldoa\x18\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 28), "ldoa\x1C\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, ldo, 30), "ldoa\x1E\x00\x00\x00"    },
};

pm_npa_remote_resource_type
pmic_npa_remote_ldo [1] = 
{
   {
      pmic_npa_ldo_remote_resources,
      8
   }
};

pm_npa_remote_name_type
pmic_npa_smps_remote_resources [] =
{
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, smps, 1), "smpa\x01\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, smps, 2), "smpa\x02\x00\x00\x00"    },
   { PMIC_DEV_RSRC_NAME_VEC_IN( A, smps, 4), "smpa\x04\x00\x00\x00"    },
};

pm_npa_remote_resource_type
pmic_npa_remote_smps [1] = 
{
   {
      pmic_npa_smps_remote_resources,
      3
   }
};
