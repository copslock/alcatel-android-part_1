#ifndef __PMIC_SBL_BOOT_UTIL_H__
#define __PMIC_SBL_BOOT_UTIL_H__

/*! \file pm_sbl_boot_util.h
*  \n
*  \brief This file contains PMIC device initialization function and globals declaration.
*  \n
*  \n &copy; Copyright 2013-2015 Qualcomm Technologies Inc, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/15   aab     Added prototype pm_check_pbs_ram_warm_reset_seq_presence()  
08/18/15   pb      Added "pm_config_sbl_test.h" for settings validation code
05/21/15   aab     Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "com_dtypes.h"
#include "pm_boot.h"
#include "pm_sbl_boot.h"
#include "pm_fg_sram.h"
#include "pm_config_sbl.h"
#include "pm_config_sbl_test.h"
#include "pm_pbs_info.h"
#include "device_info.h"
#include "pm_target_information.h"
#include "pm_app_smbchg.h"
#include "pm_utils.h"
#include "pm_comm.h"
#include "boot_api.h"
#include "boot_logger.h"
#include "boothw_target.h"
#include "CoreVerify.h"
#include "SpmiCfg.h"
#include "DALDeviceId.h"
#include "DDIPlatformInfo.h"
#include "DDIChipInfo.h"
#include "pm_pon.h"



/*===========================================================================

                        Function Prototypes

===========================================================================*/
pm_err_flag_type 
pm_check_pbs_ram_warm_reset_seq_presence(uint32 device_index);


#endif //__PMIC_SBL_BOOT_UTIL_H__
