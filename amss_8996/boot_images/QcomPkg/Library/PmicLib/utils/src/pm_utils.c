/*! \file
*  
*  \brief  pm_malloc.c ----This file contains the implementation of pm_malloc()
*  \details This file contains the implementation of pm_malloc()
*  
*  &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/09/15   aab     Updated pm_utils_get_upper_idx() 
05/01/14   aab     Added pm_clk_busy_wait()
03/20/14   aab     Added pm_boot_adc_get_mv() 
06/11/13   hs      Adding settling time for regulators.
06/20/12   hs      Created

===========================================================================*/

/*===========================================================================

INCLUDE FILES 

===========================================================================*/
#include "pm_utils.h"
#include "DALSys.h"
#include "CoreVerify.h"
#include "busywait.h"

static uint32 pmic_malloc_total_size = 0;

void pm_malloc(uint32 dwSize, void **ppMem)
{
    DALResult dalResult = DAL_SUCCESS;

    dalResult = DALSYS_Malloc(dwSize, ppMem);
    CORE_VERIFY(dalResult == DAL_SUCCESS );
    CORE_VERIFY_PTR(*ppMem);

    DALSYS_memset(*ppMem, 0, dwSize);

    pmic_malloc_total_size += dwSize;
}

uint64 pm_convert_time_to_timetick(uint64 time_us)
{
    return (time_us*19200000)/1000000;
}

uint64 pm_convert_timetick_to_time(uint64 time_tick)
{

    return (time_tick*1000000)/19200000;
}



pm_err_flag_type
pm_boot_adc_get_mv(const char *pszInputName, uint32 *battery_voltage)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  return err_flag;
}


pm_err_flag_type
pm_clk_busy_wait ( uint32 uS )
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if ( uS > 0 )
  {  
     (void) DALSYS_BusyWait(uS);
  }
  else
  {
     err_flag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
  }

  return err_flag;
}



uint8
pm_utils_get_upper_idx(uint32 data_value, uint32 *data_list, uint8 list_count)
{
  uint8 idx = list_count - 1, i;

  if (data_value == 0)
  {
    idx = 0;
  }
  else if (data_value == data_list[list_count - 1])
  {
    idx = list_count - 1;
  }
  else
  {
    /* Room for improvement if the list gets too long */
    for (i = 0; i < list_count - 1; i++)
    {
      if (data_value > data_list[i] && data_value <= data_list[i+1])
      {
        idx = i+1;
        break;
      }
    }
  }
  return idx;
}
