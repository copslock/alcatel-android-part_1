/*! \file pm_init.c
*   \n
*   \brief This file contains PMIC initialization function which initializes the PMIC Comm
*   \n layer, PMIC drivers and PMIC applications.
*   \n
*   \n &copy; Copyright 2010-2015 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
09/25/15   aab     Updated pm_device_setup(void) and Renamed pm_target_information_spmi_chnl_cfg() to pm_bus_init() 
08/08/15   aab     Added pm_device_setup()
06/23/15   aab     Updated pm_driver_init() to ensure if pm device is initialized.
                    pm_driver_init may be called directly in dload mode.
05/31/15   aab     Removed pm_oem_init()
07/16/14   akm     Comm change Updates
03/31/14   akm     Cleanup
01/15/13   aab     Fixed KW error
05/10/11   jtn     Fix RPM init bug for 8960
07/01/10   umr     Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_resource_manager.h"
#include "pm_sbl_boot.h"
#include "pm_comm.h"
#include "device_info.h"
#include "pm_target_information.h"
#include "SpmiCfg.h"
#include "pm_boot.h"


static boolean pm_device_setup_done = FALSE;


/*===========================================================================

                        FUNCTION DEFINITIONS 

===========================================================================*/
pm_err_flag_type
pm_device_setup(void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if(FALSE == pm_device_setup_done)
    {
       //SPMI init: Must be called before calling any SPMI R/W.
       err_flag |= pm_bus_init();

        err_flag |= pm_comm_channel_init_internal();
        err_flag |= pm_version_detect();

        pm_device_setup_done = TRUE;
    }
    return err_flag;
}




pm_err_flag_type pm_driver_init( void )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    err_flag |= pm_device_setup();
    pm_target_information_init();
    pm_comm_info_init();

    err_flag |= pm_driver_pre_init ();

    pm_resource_manager_init();

    err_flag |= pm_driver_post_init ();

    return err_flag;
}



/* =========================================================================
**  Function :  pm_target_information_spmi_chnl_cfg
** =========================================================================*/
/**
  Description: The function retrieves PMIC property handle.
               On success it queries the SPMI configurations.
               It configures SPMI channel and interrupt ownership for peripherals.
  @param [in] pmic_prop: property name
  @return
  PM_ERR_FLAG__SUCCESS on success otherwise PM_ERR_FLAG__SPMI_OPT_ERR.
  @dependencies
  SPMI APIs.
  @sa None
*/

pm_err_flag_type
pm_bus_init()
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  /* Initialize SPMI and enable SPMI global interrupt. */
  if (SpmiCfg_Init(TRUE) == SPMI_SUCCESS)
  {
    /* Auto configure SPMI channel and peripheral interrupt ownership. */
    if (SpmiCfg_SetDynamicChannelMode(TRUE) != SPMI_SUCCESS)
    {
      err_flag = PM_ERR_FLAG__SPMI_OPT_ERR;
    }
  }
  else 
  {
     err_flag  = PM_ERR_FLAG__SPMI_OPT_ERR;
  }

  return err_flag;
}