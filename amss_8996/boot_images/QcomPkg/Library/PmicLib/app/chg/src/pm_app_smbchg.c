/*! \file
*  
*  \brief  pm_app_smbchg_alg.c
*  \details Implementation file for pmic sbl charging algorithm
*    
*  Copyright (c) 2014-2016 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================
                                Edit History
This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

 
when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/11/16   aab     Added fix for CR988503: For targets with PMI8996, 
                    If IMA exception detected, clear sequence; If ADC NOT Ready, Reset FG once and try again 
04/01/16   aab     Updated pm_sbl_config_chg_parameters()					
11/16/15   aab     Updated pm_smbchg_apsd_check()
09/28/15   aab     Added pm_smbchg_apsd_check(). Implemented updated APSD Reset Algorithm
08/11/15   aab     Added workaround (reset APSD) to resolve DCP detection issue
07/15/15   aab     Updated SBL charging Algorithm: Remove 100mA usb max current limit
02/12/15   aab     Added Multiple try to read ADC READ is Ready
02/04/15   aab     Updated pm_sbl_chg_check_weak_battery_status() to support wipower
11/18/14   aab     Updated SBL charger driver to use Vbatt ADC
11/16/14   aab     Disabled log messages
10/15/14   aab     Added pm_sbl_config_chg_parameters() 
10/14/14   aab     Added pm_sbl_config_fg_sram(). Updated pm_sbl_chg_config_vbat_low_threshold()
08/20/14   aab     Updated to get DBC bootup threshold voltage from Dal Config.  Enabled DBC.
06/24/14   aab     Updated pm_sbl_chg_check_weak_battery_status() to include RED LED blinking
04/28/14   aab     Creation
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "com_dtypes.h"
#include "pm_app_smbchg.h"
#include "boothw_target.h"
#include "pm_smbchg_chgr.h"
#include "pm_smbchg_bat_if.h"
#include "pm_app_smbchg.h"
#include "boot_api.h"
#include "pm_utils.h"
#include "pm_rgb.h"
#include "pm_target_information.h"
#include "pm_fg_sram.h"
#include "pm_fg_adc_usr.h"
#include "pm_fg_memif.h"
#include "pm_comm.h"
#include "CoreVerify.h"
#include "boot_logger.h"
#include "boot_logger_timer.h"
#include "pm_smbchg_dc_chgpth.h"
#include <stdio.h>
#include <string.h>
#include "qusb_pbl_dload_check.h"

/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#include "tct.h"

#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
#include "pm_pon.h"
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ */

/*===========================================================================

                     PROTOTYPES 

===========================================================================*/


/*=========================================================================== 
 
                     GLOBAL TYPE DEFINITIONS
 
===========================================================================*/
#define  PM_REG_CONFIG_SETTLE_DELAY       175  * 1000 //175ms  ; Delay required for battery voltage de-glitch time
#define  PM_WEAK_BATTERY_CHARGING_DELAY   500 * 1000  //500ms
#define  PM_WIPOWER_START_CHARGING_DELAY  3500 * 1000 //3.5sec
#define  PM_MIN_ADC_READY_DELAY             1 * 1000  //1ms
#define  PM_MAX_ADC_READY_DELAY     2000              //2s
#define SBL_PACKED_SRAM_CONFIG_SIZE 3

#define PM_SBL_CHG_STATUS_MSG_LEN 40
static boolean verbose_chg_log = TRUE;
static char sbl_chg_status_message[PM_SBL_CHG_STATUS_MSG_LEN];
static pm_smbchg_vlowbatt_threshold_data_type pm_dbc_bootup_volt_threshold;
static pm_smbchg_vlowbatt_threshold_data_type pm_apsd_reset_vlowbatt_threshold;
static boolean apsd_reset_flag = FALSE;

pm_err_flag_type pm_smbchg_get_charger_path(uint32 device_index, pm_smbchg_usb_chgpth_pwr_pth_type* charger_path);
pm_err_flag_type pm_smbchg_apsd_check(uint32 device_index, pm_smbchg_specific_data_type *chg_param_ptr, uint32  entry_vbatt_level, uint32 vbat_adc);
pm_err_flag_type pm_smbchg_reset_apsd(uint32 device_index);

pm_err_flag_type pm_sbl_chg_no_battery_chgr_detection(uint32 device_index, boolean dbc_usb_500_mode);  

/*===========================================================================

                     FUNCTION IMPLEMENTATION 

===========================================================================*/

/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
#define ENTER_LK_VBAT_THRESHOLD 3200
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ */
/* [PLATFORM]-ADD-BEGIN by TCTNB.YQJ, defect-1857884 , 2016/4/14 */
static void pm_sbl_fg_reset_ima(void)
{
uint8 data = 0;
boot_log_message("Trigger FG IMA Reset.");
/*set clear sts*/
pm_comm_write_byte_mask(2, 0x4452 , 0x04, 0x04, 0);

pm_comm_write_byte(2, 0x4462, 0x04, 0);
pm_comm_write_byte(2, 0x4466, 0x00, 0);
pm_comm_read_byte(2, 0x4466, &data, 0);

/*re-set clear sts*/
pm_comm_write_byte_mask(2, 0x4452 , 0x04, 0x00, 0);

}

static void pm_sbl_reset_fg(void)
{
//uint32 data = 0;
boot_log_message("Trigger FG reset.");
pm_comm_write_byte(2, 0x40D0, 0xA5, 0);
pm_comm_write_byte_mask(2, 0x40F3 , 0xA0, 0xA0, 0);
/* wait 1 ms*/
pm_clk_busy_wait(1000);
pm_comm_write_byte(2, 0x40D0, 0xA5, 0);
pm_comm_write_byte_mask(2, 0x40F3 , 0xA0, 0x00, 0);
/* wait 1 ms*/
pm_clk_busy_wait(1000);
/*set clear sts*/
/* This is optional after Fg reset */
pm_sbl_fg_reset_ima();
}
/* [PLATFORM]-ADD-END */
pm_err_flag_type pm_sbl_chg_check_weak_battery_status(uint32 device_index)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   static pm_smbchg_specific_data_type *chg_param_ptr = NULL;
   pm_smbchg_chgr_chgr_status_type vbatt_chging_status;
   boolean hot_bat_hard_lim_rt_sts  = FALSE;
   boolean cold_bat_hard_lim_rt_sts = FALSE;
   boolean jetia_hard_limit_status  = FALSE;
   boolean vbatt_weak_status = TRUE;
   boolean toggle_led        = FALSE;
   boolean adc_reading_ready = FALSE;
   boolean bat_present       = TRUE;
   uint32 vbat_adc = 0;
   uint16  delay_count = 0;
   boolean vbatt_status = FALSE;
   pm_smbchg_misc_src_detect_type chgr_src_detected;
   boolean configure_icl_flag = FALSE;
   boolean chg_prog_message_flag = FALSE;
   pm_smbchg_usb_chgpth_pwr_pth_type charger_path = PM_SMBCHG_USB_CHGPTH_PWR_PATH__INVALID;;
   uint32  bootup_threshold;
   uint32  entry_vbatt_level = 0;
   boolean entry_vbatt_level_flag = FALSE;

   pm_model_type pmic_model = PMIC_IS_UNKNOWN;
   uint8 fg_ima_exception_sts = 0x00;
   boolean fg_reset_flag = FALSE;


   boolean fg_reset_tried = FALSE; //[PLATFORM]-ADD-BEGIN by TCTNB.YQJ, defect-1857884 , 2016/4/14
/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
   boolean press_pwrkey = FALSE;
   pm_pon_pon_reason_type pwron_reason;
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ*/
   //if(verbose_chg_log) { boot_log_message("BEGIN:  SBL Weak Battery Status Check"); }
   chg_param_ptr = (pm_smbchg_specific_data_type*)pm_target_information_get_specific_info(PM_PROP_SMBCHG_SPECIFIC_DATA);
   CORE_VERIFY_PTR(chg_param_ptr);
   bootup_threshold = chg_param_ptr->bootup_battery_theshold_mv; 

   if(chg_param_ptr->dbc_bootup_volt_threshold.enable_config == PM_ENABLE_CONFIG)
   {
      //Configure Vlowbatt threshold: Used by PMI on next bootup
      pm_dbc_bootup_volt_threshold = chg_param_ptr->dbc_bootup_volt_threshold;
      err_flag  |= pm_sbl_chg_config_vbat_low_threshold(device_index, pm_dbc_bootup_volt_threshold); 
   }

   //Check Battery presence
   err_flag |= pm_smbchg_bat_if_get_bat_pres_status(device_index, &bat_present);
   if( bat_present == FALSE )
   {
      //WA for Booting with NO Battery/SDP
      err_flag  |= pm_sbl_chg_no_battery_chgr_detection(device_index, chg_param_ptr->dbc_usb_500_mode); 

      if(verbose_chg_log) { boot_log_message("BOOTUP: NO Battery"); }
      return err_flag;
   }

   //Detect the type of charger used
   err_flag |= pm_smbchg_get_charger_path(device_index, &charger_path);
   if (charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__DC_CHARGER) 
   {
      bootup_threshold = chg_param_ptr->wipwr_bootup_battery_theshold_mv;
   }
   else if (charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__USB_CHARGER)
   {
      bootup_threshold = chg_param_ptr->bootup_battery_theshold_mv;
   }

   //Enable BMS FG Algorithm BCL
   err_flag |= pm_fg_adc_usr_enable_bcl_monitoring(device_index, TRUE);
   if ( err_flag != PM_ERR_FLAG__SUCCESS )  
   { 
       return err_flag;
   }

   err_flag |= pm_smbchg_usb_chgpth_set_usbin_adptr_allowance(device_index, PM_SMBCHG_USBIN_ADPTR_ALLOWANCE_5V_TO_9V);
   if ( err_flag != PM_ERR_FLAG__SUCCESS )  
   { 
       return err_flag;
   }

/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
   pm_pon_get_pon_reason(0,&pwron_reason);
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ*/
   pmic_model = pm_get_pmic_model(device_index);
   if( pmic_model == PMIC_IS_PMI8996)
   {
      /* Reset any way to make sure RIF_MEM_ACCESS_REQ is cleared */
      err_flag |= pm_fg_memif_set_mem_intf_cfg(device_index, PM_FG_MEMIF_MEM_INTF_CFG_RIF_MEM_ACCESS_REQ, FALSE);

      /* if Fuel gauge Ima exception Run IMA Clear Sequnce  Here */
      err_flag |= pm_fg_memif_get_ima_exception_sts(device_index, &fg_ima_exception_sts);
      if ((fg_ima_exception_sts != 0x00) && (err_flag == PM_ERR_FLAG__SUCCESS) )  
      {
         err_flag |= PmicFgSram_ResetIma(device_index);
      }
   }


   while( vbatt_weak_status == TRUE )  //While battery is in weak state
   {
      //Check Vbatt ADC level  
      err_flag |= pm_fg_adc_usr_get_bcl_values(device_index, &adc_reading_ready); //Check if Vbatt ADC is ready

      //Check if Vbatt ADC is Ready
      for (delay_count = 0; delay_count < PM_MAX_ADC_READY_DELAY; delay_count++)
      {
        if(adc_reading_ready == FALSE)
        {
           err_flag |= pm_clk_busy_wait(PM_MIN_ADC_READY_DELAY);
           err_flag |= pm_fg_adc_usr_get_bcl_values(device_index, &adc_reading_ready);
        }
        else
        {
           break;
        }
      }
      if ( err_flag != PM_ERR_FLAG__SUCCESS )  { break;} 


      if(  (pmic_model == PMIC_IS_PMI8996) &&
           (fg_reset_flag == FALSE)        && 
           (adc_reading_ready == FALSE) )
      {  /* IF ADC not Ready after the given delay then Reset FG and continue in ADC waiting loop*/
            fg_reset_flag = TRUE;
            err_flag |= PmicFgSram_ResetFg(device_index);//Reset FG
            continue;
      }


      if ( adc_reading_ready) 
      {
         err_flag |= pm_fg_adc_usr_get_calibrated_vbat(device_index, &vbat_adc); //Read calibrated vbatt ADC
         if ( err_flag != PM_ERR_FLAG__SUCCESS )  { break;} 
   
         //Check if ADC reading is within limit
         if ( vbat_adc >=  bootup_threshold)  //Compare it with SW bootup threshold
         {
            if(verbose_chg_log) { boot_log_message("BOOTUP: Battery is GOOD"); }
            vbatt_weak_status = FALSE;
            break; //bootup
         }
/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
         else if(pwron_reason.kpdpwr && (vbat_adc >= ENTER_LK_VBAT_THRESHOLD))
         {
            if(verbose_chg_log) { boot_log_message("powered by press key, bootup"); }
            vbatt_weak_status = FALSE;
            break;  //bootup
         }
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ*/

         snprintf(sbl_chg_status_message, PM_SBL_CHG_STATUS_MSG_LEN, "Vbatt: %d", vbat_adc);
         if(verbose_chg_log) { boot_log_message(sbl_chg_status_message);}

         //Store first Vbatt ADC reading
         if (entry_vbatt_level_flag == FALSE)
         {
            entry_vbatt_level = vbat_adc;
            entry_vbatt_level_flag = TRUE;
         }
       }
       else
       {
         boot_log_message("ERROR:  ADC NOT Ready");
/* [PLATFORM]-ADD-BEGIN by TCTNB.YQJ, defect-1857884 , 2016/4/14 */
		if(fg_reset_tried == FALSE)
		{
			fg_reset_tried = TRUE;
			pm_sbl_reset_fg();
			continue;
		} 
/* [PLATFORM]-ADD-END*/
         err_flag |= PM_ERR_FLAG__ADC_NOT_READY;
         break; 
       }
   
      //Check if USB charger is SDP
      err_flag |= pm_smbchg_misc_chgr_port_detected(device_index, &chgr_src_detected);
      if (chgr_src_detected == PM_SMBCHG_MISC_SRC_DETECT_SDP) 
      {
         if(verbose_chg_log) { boot_log_message("Charger source: SDP"); }
         if (configure_icl_flag == FALSE)
         {
            //Check Vlow_batt status
            err_flag |= pm_smbchg_chgr_vbat_sts(device_index, &vbatt_status);
            if (vbatt_status)
            {
               //set ICL to 500mA
               err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__USB51_MODE, TRUE);
               err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__USBIN_MODE_CHG, FALSE);
               configure_icl_flag = TRUE;
            }
         }

         //Rerun APSD if needed
         err_flag |= pm_smbchg_apsd_check(device_index, chg_param_ptr, entry_vbatt_level, vbat_adc);
      }
      else if (chgr_src_detected == PM_SMBCHG_MISC_SRC_DETECT_DCP) 
      {
         err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__ICL_OVERRIDE, FALSE);
         if(verbose_chg_log) { boot_log_message("Charger source: DCP"); }
      }
      else
      {
         snprintf(sbl_chg_status_message, PM_SBL_CHG_STATUS_MSG_LEN, "Charger source: OTHER; Type: %d", chgr_src_detected);
         if(verbose_chg_log) { boot_log_message(sbl_chg_status_message);}
      }


      if (chg_prog_message_flag == FALSE)
      {
          //Ensure that Charging is enabled
          err_flag |= pm_smbchg_chgr_enable_src(device_index, FALSE);
          err_flag |= pm_smbchg_chgr_set_chg_polarity_low(device_index, TRUE);
          err_flag |= pm_smbchg_bat_if_config_chg_cmd(device_index, PM_SMBCHG_BAT_IF_CMD__EN_BAT_CHG, FALSE);		  
          err_flag |= pm_clk_busy_wait(PM_WEAK_BATTERY_CHARGING_DELAY);
      }

      //Check if JEITA check is enabled
      if (chg_param_ptr->enable_jeita_hard_limit_check == TRUE)
      {
         //Read JEITA condition
         err_flag |= pm_smbchg_bat_if_irq_status(device_index, PM_SMBCHG_BAT_IF_HOT_BAT_HARD_LIM,  PM_IRQ_STATUS_RT, &hot_bat_hard_lim_rt_sts );
         err_flag |= pm_smbchg_bat_if_irq_status(device_index, PM_SMBCHG_BAT_IF_COLD_BAT_HARD_LIM, PM_IRQ_STATUS_RT, &cold_bat_hard_lim_rt_sts);
         if ( err_flag != PM_ERR_FLAG__SUCCESS )  { break;}  

         if ( ( hot_bat_hard_lim_rt_sts  == TRUE ) || (cold_bat_hard_lim_rt_sts == TRUE) )  
         {
            jetia_hard_limit_status = TRUE; 
            continue;  // Stay in this loop as long as JEITA Hard Hot/Cold limit is exceeded
         }
      }

      //Toggle Red LED and delay 500ms
      toggle_led = (toggle_led == FALSE)?TRUE:FALSE;
      err_flag |= pm_rgb_led_config(device_index, PM_RGB_1, PM_RGB_SEGMENT_R,  PM_RGB_VOLTAGE_SOURCE_VPH, PM_RGB_DIM_LEVEL_MID, toggle_led); 
      err_flag |= pm_clk_busy_wait(PM_WEAK_BATTERY_CHARGING_DELAY); //500ms 

      //Check if Charging in progress
      err_flag |= pm_smbchg_chgr_get_chgr_sts(device_index, &vbatt_chging_status);
      if ( err_flag != PM_ERR_FLAG__SUCCESS )  { break;}

      if ( vbatt_chging_status.charging_type == PM_SMBCHG_CHGR_NO_CHARGING )
      {
         if (charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__DC_CHARGER) 
         {
            //Delay for 3.5sec for charging to begin, and check charging status again prior to shutting down.
            err_flag |= pm_clk_busy_wait(PM_WIPOWER_START_CHARGING_DELAY); //3500ms 
            err_flag |= pm_smbchg_chgr_get_chgr_sts(device_index, &vbatt_chging_status);
            if ( err_flag != PM_ERR_FLAG__SUCCESS )  { break;}

            if ( vbatt_chging_status.charging_type == PM_SMBCHG_CHGR_NO_CHARGING )
            {
               boot_log_message("ERROR: Charging is NOT in progress: Shutting Down");
               boot_hw_powerdown();
            }
         }
         else
         {
            boot_log_message("ERROR: Charging is NOT in progress: Shutting Down");
            boot_hw_powerdown();
         }
      }
      else
      {
          boot_log_message("SBL Charging in progress....");
          chg_prog_message_flag = TRUE;

/* [PLATFORM]-ADD-BEGIN by TCTNB.WJ, FR1292217 , 2016/1/19 */
#if defined(FEATURE_TCT_LP_DISPLAY_FEATURE)
          pm_pon_irq_status(0, PM_PON_IRQ_KPDPWR_ON, PM_IRQ_STATUS_RT, &press_pwrkey);
          if(press_pwrkey)
          {
            boot_log_message("pwrkey triggerred, bootup ");
            vbatt_weak_status = FALSE;
            break; //bootup
          }
#endif
/* [PLATFORM]-ADD-END by TCTNB.WJ */
      }
   }//while


   toggle_led = FALSE;
   err_flag |= pm_rgb_led_config(device_index, PM_RGB_1, PM_RGB_SEGMENT_R,  PM_RGB_VOLTAGE_SOURCE_VPH, PM_RGB_DIM_LEVEL_MID, toggle_led);
   if (err_flag != PM_ERR_FLAG__SUCCESS)
   {
      boot_log_message("ERROR: In SBL Charging ...");
   }

   if (charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__DC_CHARGER) 
   {
      //If battery is good, Toggle SHDN_N_CLEAR_CMD Reg:  Set 0x1340[6] to  1 and then  0
      err_flag = pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__SHDN_N_CLEAR_CMD, TRUE);
      err_flag = pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__SHDN_N_CLEAR_CMD, FALSE);
   }

   if (chg_prog_message_flag == TRUE)
   {
        boot_log_message("SBL Charging completed.");
   }

   return err_flag; 
}





//APSD
pm_err_flag_type pm_smbchg_apsd_check(uint32 device_index, pm_smbchg_specific_data_type *chg_param_ptr, uint32  entry_vbatt_level, uint32 current_vbat_adc)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_pon_poff_reason_type poff_reason;
   uint32  apsd_reset_minimum_vbatt_level = 0x00;
   uint32  apsd_reset_vbatt_level = 0x00;
   uint32 primary_device_index = 0;

   //Rerun APSD to ensure that DCP is detected accurately
   //1. Reduce Vlowbatt to 2.55 V in SBL
   //2. Check last reset reason:
   //     If last reset reason is UVLO, wait for battery voltage to reach 3.2 V and then rerun APSD
   //     If last reset reason is NOT UVLO, wait for battery voltage to reach about 'x' voltage and then rerun APSD. 
   //       -   This �x� voltage should be high enough for the system to not UVLO with weak battery when PMI disables charger buck.  Used 2.8V as a default level
   if ( apsd_reset_flag == FALSE) 
   {
      CORE_VERIFY_PTR(chg_param_ptr);
      apsd_reset_minimum_vbatt_level = chg_param_ptr->apsd_reset_threshold_mv;
      if ( entry_vbatt_level < apsd_reset_minimum_vbatt_level ) //if entry vbatt level is < 2.8V
      {
         //Configure Vlowbatt threshold: Used by PMI on next bootup
         pm_apsd_reset_vlowbatt_threshold = chg_param_ptr->apsd_reset_vlowbatt_threshold;
         err_flag |= pm_sbl_chg_config_vbat_low_threshold(device_index, pm_apsd_reset_vlowbatt_threshold);  

         //Check if last reset reason is UVLO from primary index
         err_flag |= pm_pon_get_poff_reason(primary_device_index, &poff_reason); 

         snprintf(sbl_chg_status_message, PM_SBL_CHG_STATUS_MSG_LEN, "APSD: poff_reason.uvlo: %d", poff_reason.uvlo);
         if(verbose_chg_log) { boot_log_message(sbl_chg_status_message);}

         if (poff_reason.uvlo != 1) //If last reset reason is NOT UVLO
         {
            apsd_reset_vbatt_level = chg_param_ptr->apsd_reset_theshold_no_uvlo_mv; 
         }
         else
         { //If last reset reason is UVLO
            apsd_reset_vbatt_level = chg_param_ptr->apsd_reset_theshold_uvlo_mv;
         }
         //Reset APSD once, if the entry Vbatt level is < apsd_rerun_vbatt_level and current vbatt level >= apsd_rerun_vbatt_level
         if( current_vbat_adc >=  apsd_reset_vbatt_level )
         {
             if(verbose_chg_log) { boot_log_message("APSD Reset Start"); }
             err_flag |= pm_smbchg_reset_apsd(device_index);  
             apsd_reset_flag = TRUE;                                                          //Reset APSD
             if(verbose_chg_log) { boot_log_message("APSD Reset Done"); }
         }
      }
   }
   return err_flag;
}




pm_err_flag_type pm_smbchg_reset_apsd(uint32 device_index)
{
      pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_smbchg_misc_src_detect_type chgr_src_detected;
	  
   err_flag |= pm_smbchg_usb_chgpth_config_aicl(device_index, PM_SMBCHG_USB_CHGPTH_AICL_CFG__AICL_EN, FALSE); //Disable AICL

      err_flag |= pm_smbchg_usb_chgpth_set_usbin_adptr_allowance(device_index, PM_SMBCHG_USBIN_ADPTR_ALLOWANCE_9V);
      err_flag |= pm_smbchg_usb_chgpth_set_usbin_adptr_allowance(device_index, PM_SMBCHG_USBIN_ADPTR_ALLOWANCE_5V_TO_9V);

   err_flag |= pm_smbchg_usb_chgpth_config_aicl(device_index, PM_SMBCHG_USB_CHGPTH_AICL_CFG__AICL_EN, TRUE);  //Enable AICLb
   
   for (uint32 count = 0; count < 10; count++)
   {
      err_flag |= pm_smbchg_misc_chgr_port_detected(device_index, &chgr_src_detected); //Detect charger type
      if (chgr_src_detected != PM_SMBCHG_MISC_SRC_DETECT_INVALID)
      {
         break;
      }
      err_flag |= pm_clk_busy_wait(100 * 1000); //100ms 
   }
      return err_flag;
}



pm_err_flag_type pm_smbchg_get_charger_path(uint32 device_index, pm_smbchg_usb_chgpth_pwr_pth_type* charger_path)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   boolean usbin_uv_status = TRUE;
   boolean usbin_ov_status = TRUE;
   boolean dcbin_uv_status = TRUE;
   boolean dcbin_ov_status = TRUE;

   //DC charger present, if DCIN_UV_RT_STS and DCIN_UV_RT_STS status is 0 (INT_RT_STS : 0x1410[1] and [0] == 0)
   err_flag |= pm_smbchg_dc_chgpth_irq_status(device_index, PM_SMBCHG_DC_CHGPTH_DCBIN_UV, PM_IRQ_STATUS_RT, &dcbin_uv_status);
   err_flag |= pm_smbchg_dc_chgpth_irq_status(device_index, PM_SMBCHG_DC_CHGPTH_DCBIN_OV, PM_IRQ_STATUS_RT, &dcbin_ov_status);
   
   //USB charger present, if USBIN_UV_RT_STS and USBIN_OV_RT_STS status is 0 ( INT_RT_STS : 0x1310[1] and [0] == 0)
   err_flag |= pm_smbchg_usb_chgpth_irq_status(device_index, PM_SMBCHG_USB_CHGPTH_USBIN_UV, PM_IRQ_STATUS_RT, &usbin_uv_status);
   err_flag |= pm_smbchg_usb_chgpth_irq_status(device_index, PM_SMBCHG_USB_CHGPTH_USBIN_OV, PM_IRQ_STATUS_RT, &usbin_ov_status);  
   
   if((dcbin_uv_status == FALSE) && (dcbin_ov_status == FALSE))
   {
      *charger_path = PM_SMBCHG_USB_CHGPTH_PWR_PATH__DC_CHARGER;
   }
   else if((usbin_uv_status == FALSE) && (usbin_ov_status == FALSE))
   {
      *charger_path = PM_SMBCHG_USB_CHGPTH_PWR_PATH__USB_CHARGER;
   }
   else
   {
      *charger_path = PM_SMBCHG_USB_CHGPTH_PWR_PATH__INVALID;
   }

   return err_flag;
}



pm_err_flag_type pm_sbl_chg_config_vbat_low_threshold(uint32 device_index, pm_smbchg_vlowbatt_threshold_data_type vlowbatt_threshold_data)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   
   if (vlowbatt_threshold_data.enable_config == PM_ENABLE_CONFIG)
   {
      if (vlowbatt_threshold_data.vlowbatt_threshold  >= PM_SMBCHG_BAT_IF_LOW_BATTERY_THRESH_INVALID)
      {
         err_flag = PM_ERR_FLAG__INVALID_VBATT_INDEXED;
         return err_flag;
      }

      err_flag = pm_smbchg_bat_if_set_low_batt_volt_threshold(device_index, vlowbatt_threshold_data.vlowbatt_threshold);
      //boot_log_message("Configure Vlowbatt threshold");
   }

   return err_flag; 
}


pm_err_flag_type pm_sbl_config_fg_sram(uint32 device_index)
   {
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  FgSramAddrDataEx_type *sram_data_ptr = NULL;
  FgSramAddrDataEx_type pm_sbl_sram_data[SBL_PACKED_SRAM_CONFIG_SIZE];
  pm_model_type pmic_model = PMIC_IS_INVALID;
  boolean sram_enable_config_flag = FALSE;

  //Check if any SRAM configuration is needed
  sram_data_ptr = (FgSramAddrDataEx_type*)pm_target_information_get_specific_info(PM_PROP_FG_SPECIFIC_DATA);
  CORE_VERIFY_PTR(sram_data_ptr);
  for (int i=0; i< SBL_SRAM_CONFIG_SIZE; i++) 
  {
     sram_enable_config_flag |= sram_data_ptr[i].EnableConfig;
  }

   
  if (sram_enable_config_flag == TRUE )
  {
     pmic_model = pm_get_pmic_model(device_index);   //Check if PMIC exists
     if ( (pmic_model != PMIC_IS_INVALID) || (pmic_model != PMIC_IS_UNKNOWN) )
     {
        //boot_log_message("BEGIN: Configure FG SRAM");

        //Pre-process JEITA data
        pm_sbl_sram_data[0].SramAddr = sram_data_ptr[0].SramAddr;
        pm_sbl_sram_data[0].SramData = (sram_data_ptr[3].SramData  << 24)| 
                                       (sram_data_ptr[2].SramData  << 16)|   
                                       (sram_data_ptr[1].SramData  <<  8)|   
                                        sram_data_ptr[0].SramData;
        pm_sbl_sram_data[0].DataOffset = sram_data_ptr[0].DataOffset;  
        pm_sbl_sram_data[0].DataSize = 4;
        //Set JEITA threshould configuration flag
        pm_sbl_sram_data[0].EnableConfig = sram_data_ptr[0].EnableConfig | sram_data_ptr[1].EnableConfig | 
                                           sram_data_ptr[2].EnableConfig | sram_data_ptr[3].EnableConfig;   

        //Pre-process Thermistor Beta Data
        //thremistor_c1_coeff
        pm_sbl_sram_data[1]  = sram_data_ptr[4];

        //thremistor_c2_coeff and thremistor_c3_coeff
        pm_sbl_sram_data[2].SramAddr   = sram_data_ptr[5].SramAddr;
        pm_sbl_sram_data[2].SramData   = (sram_data_ptr[6].SramData << 16) | sram_data_ptr[5].SramData;
        pm_sbl_sram_data[2].DataOffset = sram_data_ptr[5].DataOffset;  
        pm_sbl_sram_data[2].DataSize = 4;
        pm_sbl_sram_data[2].EnableConfig   = sram_data_ptr[5].EnableConfig;

        //Configure SRAM Data
        err_flag |= PmicFgSram_ProgBurstAccessEx(device_index, pm_sbl_sram_data, SBL_PACKED_SRAM_CONFIG_SIZE);

        //Test: Read Back
        //err_flag |= PmicFgSram_Dump(device_index, 0x0454, 0x0454);
        //err_flag |= PmicFgSram_Dump(device_index, 0x0444, 0x0448);
        //err_flag |= PmicFgSram_Dump(device_index, 0x0448, 0x0452);
        
        //boot_log_message("END: Configure FG SRAM");
     }
  }

   return err_flag; 
}




pm_err_flag_type pm_sbl_config_chg_parameters(uint32 device_index)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   static pm_smbchg_specific_data_type *chg_param_ptr;
   
   if(chg_param_ptr == NULL)
   {
      chg_param_ptr = (pm_smbchg_specific_data_type*)pm_target_information_get_specific_info(PM_PROP_SMBCHG_SPECIFIC_DATA);
      CORE_VERIFY_PTR(chg_param_ptr);
   }

   //Vlowbatt Threshold  
   //  - Done on:  pm_sbl_chg_config_vbat_low_threshold()
   
   //Charger Path Input Priority 
   if (chg_param_ptr->chgpth_input_priority.enable_config == PM_ENABLE_CONFIG)
   {
      pm_smbchg_chgpth_input_priority_type chgpth_priority = chg_param_ptr->chgpth_input_priority.chgpth_input_priority;
      if (chgpth_priority < PM_SMBCHG_USBCHGPTH_INPUT_PRIORITY_INVALID) 
      {
          err_flag |= pm_smbchg_chgpth_set_input_priority(device_index, chgpth_priority);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }


   //Battery Missing Detection Source 
   if (chg_param_ptr->bat_miss_detect_src.enable_config == PM_ENABLE_CONFIG)
   {
      pm_smbchg_bat_miss_detect_src_type batt_missing_det_src = chg_param_ptr->bat_miss_detect_src.bat_missing_detection_src;
      if (batt_missing_det_src < PM_SMBCHG_BAT_IF_BAT_MISS_DETECT_SRC_INVALID) 
      {
          err_flag |= pm_smbchg_bat_if_set_bat_missing_detection_src(device_index, batt_missing_det_src);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }

   //WDOG Timeout      
   if (chg_param_ptr->wdog_timeout.enable_config == PM_ENABLE_CONFIG)
   {
      pm_smbchg_wdog_timeout_type wdog_timeout = chg_param_ptr->wdog_timeout.wdog_timeout;
      if (wdog_timeout < PM_SMBCHG_MISC_WD_TMOUT_INVALID) 
      {
          err_flag |= pm_smbchg_misc_set_wdog_timeout(device_index, wdog_timeout);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }


   //Enable WDOG                      
   if (chg_param_ptr->enable_wdog.enable_config == PM_ENABLE_CONFIG)
   {
      pm_smbchg_wdog_timeout_type enable_smbchg_wdog = chg_param_ptr->enable_wdog.enable_wdog;
      err_flag |= pm_smbchg_misc_enable_wdog(device_index, enable_smbchg_wdog);
   }


   //FAST Charging Current            
   if (chg_param_ptr->fast_chg_i.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 fast_chg_i_ma = chg_param_ptr->fast_chg_i.fast_chg_i_ma;
      if ((fast_chg_i_ma >= 300) && (fast_chg_i_ma <= 3000) )
      {
          err_flag |= pm_smbchg_chgr_set_fast_chg_i(device_index, fast_chg_i_ma);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }

   //Pre Charge Current               
   if (chg_param_ptr->pre_chg_i.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 pre_chg_i_ma = chg_param_ptr->pre_chg_i.pre_chg_i_ma;
      if ((pre_chg_i_ma >= 100) && (pre_chg_i_ma <= 550) )
      {
          err_flag |= pm_smbchg_chgr_set_pre_chg_i(device_index, pre_chg_i_ma);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }

   //Pre to Fast Charge Current       
   if (chg_param_ptr->pre_to_fast_chg_theshold_mv.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 p2f_chg_mv = chg_param_ptr->pre_to_fast_chg_theshold_mv.pre_to_fast_chg_theshold_mv;
      if ((p2f_chg_mv >= 2400) && (p2f_chg_mv <= 3000)  )
      {
          err_flag |= pm_smbchg_chgr_set_p2f_threshold(device_index, p2f_chg_mv);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }

   //Float Voltage : 3600mV to 4500 mv                   
   err_flag |= pm_smbchg_chgr_inhibit(device_index, FALSE);       
   if (chg_param_ptr->float_volt_theshold_mv.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 float_volt_mv = chg_param_ptr->float_volt_theshold_mv.float_volt_theshold_mv;
      if ((float_volt_mv >= 3600) && (float_volt_mv <= 4500))
      {
          err_flag |= pm_smbchg_chgr_set_float_volt(device_index, float_volt_mv);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }
   else
   {
      err_flag |= pm_smbchg_chgr_set_float_volt(device_index, 4200);//Set CSIR Default
   }
   err_flag |= pm_smbchg_chgr_inhibit(device_index, TRUE);


   //USBIN Input Current Limit  :Valid value is 300 to 3000mAmp      
   if (chg_param_ptr->usbin_input_current_limit.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 usbin_i_limit_ma = chg_param_ptr->usbin_input_current_limit.usbin_input_current_limit;
      if ((usbin_i_limit_ma >= 300) && (usbin_i_limit_ma <= 3000))
      {
          err_flag |= pm_smbchg_usb_chgpth_set_usbin_current_limit(device_index, usbin_i_limit_ma);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }


   //DCIN Input Current Limit : valid range is 300 to 2000 mAmp         
   if (chg_param_ptr->dcin_input_current_limit.enable_config == PM_ENABLE_CONFIG)
   {
      uint32 dcin_i_limit_ma = chg_param_ptr->dcin_input_current_limit.dcin_input_current_limit;
      if ((dcin_i_limit_ma >= 300) && (dcin_i_limit_ma <= 3200))
      {
          err_flag |= pm_smbchg_dc_chgpth_set_dcin_current_limit(device_index, dcin_i_limit_ma);
      }
      else
      {
         err_flag |= PM_ERR_FLAG__INVALID_PARAMETER;
      }
   }


   return err_flag; 
}
 


pm_err_flag_type pm_sbl_chg_no_battery_chgr_detection(uint32 device_index, boolean dbc_usb_500_mode ) 
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   pm_smbchg_misc_src_detect_type chgr_src_detected;
   static boolean no_battery_flag = TRUE;
   uint32 usbin_current_limit     = 1500; //Can not be set to lower than 700mA
   boolean toggle_led             = FALSE;

   if (dbc_usb_500_mode == TRUE)  //500mA limit is desired by OEM
   {
      err_flag |= pm_smbchg_usb_chgpth_en_hvdcp(device_index, FALSE); //Disable HVDCP
      err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__USBIN_MODE_CHG , FALSE); //Clear HC mode bit 
      err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__USB51_MODE, TRUE);    //set USB500 mode
      err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__ICL_OVERRIDE, TRUE);  //Set ICL_OVERRIDE 
   }
   else 
   {
      err_flag |= pm_smbchg_usb_chgpth_en_hvdcp(device_index, FALSE);                                              //Disable HVDCP
      err_flag |= pm_smbchg_usb_chgpth_config_aicl(device_index, PM_SMBCHG_USB_CHGPTH_AICL_CFG__AICL_EN, FALSE);   //Disable AICL 
      err_flag |= pm_smbchg_usb_chgpth_set_usbin_current_limit(device_index, usbin_current_limit);
      err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__USBIN_MODE_CHG, TRUE); //set HC mode
      err_flag |= pm_smbchg_usb_chgpth_set_cmd_il(device_index, PM_SMBCHG_USBCHGPTH_CMD_IL__ICL_OVERRIDE, TRUE);   //Set ICL_OVERRIDE 
   }

   //Detect Charger Type
   err_flag |= pm_smbchg_misc_chgr_port_detected(device_index, &chgr_src_detected);

   while(no_battery_flag)
   {
      boot_log_message(" NO Battery Detected ");

      switch(chgr_src_detected) //Log charger type
      {
         case PM_SMBCHG_MISC_SRC_DETECT_CDP:
               boot_log_message("Charger source: CDP");
               break;
         case PM_SMBCHG_MISC_SRC_DETECT_DCP:
               boot_log_message("Charger source: DCP");
               break;
         case PM_SMBCHG_MISC_SRC_DETECT_SDP:
               boot_log_message("Charger source: SDP");
               break;
         case PM_SMBCHG_MISC_SRC_DETECT_OTHER_CHARGING_PORT:
               boot_log_message("Charger source: OTHER_CHARGING_PORT");
               break;
        default:
               boot_log_message("Charger source: Unknown");
               break;
      }

      //Toggle Red LED
      toggle_led = (toggle_led == FALSE)?TRUE:FALSE;
      err_flag |= pm_rgb_led_config(device_index, PM_RGB_1, PM_RGB_SEGMENT_R,  PM_RGB_VOLTAGE_SOURCE_VPH, PM_RGB_DIM_LEVEL_MID, toggle_led); 
      err_flag |= pm_clk_busy_wait(PM_WEAK_BATTERY_CHARGING_DELAY * 4); //wait 2sec
   };

   return err_flag; 
 }


