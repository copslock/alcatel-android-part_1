#ifndef PM_NPA_TEST_H
#define PM_NPA_TEST_H
/*===========================================================================


                  P M    NPA   TEST H E A D E R    F I L E

DESCRIPTION
  This file contains prototype definitions to support interaction
  with the QUALCOMM Power Management ICs.

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/28/14   va     Created.(Expose protocol to call npa test init for tests run)
===========================================================================*/
/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

#include <Library/QcomLib.h>
#include "CoreVerify.h"
#include <../Include/Protocol/EFIPmicNpaTest.h>
#include <Library/MemoryAllocationLib.h>

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
#define strlcpy AsciiStrnCpy

/*===========================================================================

                        DEFINITIONS

===========================================================================*/
/*===========================================================================

                        GENERIC FUNCTION PROTOTYPES

===========================================================================*/
void pm_npa_test_init (void);
EFI_STATUS pm_npa_get_resource_info(PMIC_NPATEST_RESOURCEINFO *PamResourceInfo);


#endif /* PM_NPA_TEST_H */
