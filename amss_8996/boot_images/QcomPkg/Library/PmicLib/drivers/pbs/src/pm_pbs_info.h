/*! \file  pm_pbs_info.h
 *  
 *  \brief  This file contains the pmic PBS info driver definitions.
 *  \details  This file contains the pm_pbs_info_init & pm_pbs_info_store_glb_ctxt
 *  API definitions.
 *  
 *  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.


when         who     what, where, why
----------   ---     ---------------------------------------------------------- 
06/23/2015   aab    Replaced pm_pbs_info_init() with pm_pbs_info_ram_init() and pm_pbs_info_rom_init()
04/29/2015   aab     Added support for PMK8001 
04/05/2013   kt      Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_target_information.h"

/*===========================================================================

                    MACRO AND GLOBAL VARIABLES

===========================================================================*/
/* PBS ROM/RAM Size can be 128 or 256 words. */
/* PBS OTP/ROM Start Address */
#define PM_PBS_ROM_BASE_ADDR            0x000
/* PBS RAM Start Address */
#define PM_PBS_RAM_BASE_ADDR            0x400

/* PBS Memory Version stored at the last line */
#define PM_PBS_MEMORY_VER_LINE_FROM_END   1
/* MFG Info stored at the 7th from last line (if present in PBS ROM) */
#define PM_PBS_ROM_INFO_LINE_FROM_END     7

typedef enum
{
    PM_PBS_INFO_IN_OTP,
    PM_PBS_INFO_IN_MISC,
    PM_PBS_INFO_INVALID
} pm_pbs_info_place_holder_type;

typedef struct
{
    uint16 pbs_otp_mem_size;
    uint16 pbs_ram_mem_size;
    pm_pbs_info_place_holder_type pbs_info_place_holder;
} pm_pbs_info_data_type;


/*===========================================================================

      Function Definitions

============================================================================*/
/** 
 * @name pm_pbs_info_rom_init 
 *  
 * @brief This function is called to initialize the PBS Info driver.
 *        This function internally validates the PBS Core peripheral
 *        info to determine the valid PMIC Chips and calls an internal
 *        helper function to read PBS Manufacturing IDs and foundry
 *        information such as PBS Lot ID, ROM Version,
 *        Fab Id, Wafer Id, X coord and Y coord and stores it in static
 *        global variable. This function is called during pm_device_init
 *        after the PBS RAM data is loaded.
 *  
 * @param None. 
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.           
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = PBS peripheral is not
 *          supported.
 *          else SPMI errors 
 */
pm_err_flag_type pm_pbs_info_rom_init (void);


/** 
 * @name pm_pbs_info_ram_init 
 *  
 * @brief This function is called to initialize the PBS Info driver.
 *        This function internally validates the PBS Core peripheral
 *        info to determine the valid PMIC Chips and calls an internal
 *        helper function to read PBS Manufacturing IDs and foundry
 *        information such as  RAM Version and stores it in static
 *        global variable. This function is called during pm_device_init
 *        after the PBS RAM data is loaded.
 *  
 * @param None. 
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.           
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = PBS peripheral is not
 *          supported.
 *          else SPMI errors 
 */
pm_err_flag_type pm_pbs_info_ram_init (void);


/** 
 * @name pm_pbs_info_store_glb_ctxt 
 *  
 * @brief This function is called to copy the PBS info to Global
 *        Context (SMEM) from static global variables where the
 *        PBS info is stored during PBS Info initilization.
 *  
 * @param None. 
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS. 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Error in
 *          copying to shared memory. 
 */
pm_err_flag_type pm_pbs_info_store_glb_ctxt (void);
