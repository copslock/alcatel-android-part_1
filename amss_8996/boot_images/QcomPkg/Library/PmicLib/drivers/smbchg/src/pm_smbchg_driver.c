/*! \file
*  
*  \brief  pm_smbchg_driver.c driver implementation.
*  \details charger driver implementation.
*  &copy;
*  Copyright (c) 2014 - 2016 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE


when       	who     	 what, where, why
--------   	---    		 ---------------------------------------------------------- 
11/19/14         al              preventing it from being initalized for coin cell
08/20/14         al              Updating comm lib 
08/29/14         al              KW fixes
05/20/14         al              Arch update
05/09/14         va              Using common debug and assert Marco
04/18/14         al              Updated copyright 
04/18/14         al              Initial revision
========================================================================== */

/*===========================================================================

					INCLUDE FILES

===========================================================================*/
#include "pm_smbchg_driver.h"
#include "CoreVerify.h"
#include "pm_utils.h"
#include "hw_module_type.h"

void pm_smbchg_update_dcin_range(uint8 pmic_index);

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the SMBCHG driver data */
static pm_smbchg_data_type *pm_smbchg_data_arr[PM_MAX_NUM_PMICS];
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_smbchg_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_smbchg_data_type *smbchg_ptr = NULL;
    pm_device_info_type pmic_info;

    /*return if coincell since it is not supported at the moment*/
    if(PM_HW_MODULE_CHARGER_COINCELL == peripheral_info->peripheral_subtype)
    {
      return;
    }

    smbchg_ptr = pm_smbchg_data_arr[pmic_index];

    if (NULL == smbchg_ptr)
    {
        pm_malloc( sizeof(pm_smbchg_data_type), (void**)&smbchg_ptr);   
                                                    
        /* Assign Comm ptr */
        smbchg_ptr->comm_ptr = comm_ptr;

        /* smbchg Register Info - Obtaining Data through dal config */
        smbchg_ptr->smbchg_register = (smbchg_register_ds*)pm_target_information_get_common_info(PM_PROP_SMBCHG_REG);
        CORE_VERIFY_PTR(smbchg_ptr->smbchg_register);

	    smbchg_ptr->num_of_peripherals = pm_target_information_get_periph_count_info(PM_PROP_SMBCHG_NUM, pmic_index);
        CORE_VERIFY(smbchg_ptr->num_of_peripherals != 0);

        smbchg_ptr->chg_range_data = (chg_range_data_type*)pm_target_information_get_specific_info(PM_PROP_SMBCHG_DATA);
        CORE_VERIFY_PTR(smbchg_ptr->chg_range_data);

        pm_smbchg_data_arr[pmic_index] = smbchg_ptr;

        CORE_VERIFY(PM_ERR_FLAG__SUCCESS == pm_get_pmic_info(pmic_index, &pmic_info));
        if (PMIC_IS_PMI8996 == pmic_info.ePmicModel)
        {
            pm_smbchg_update_dcin_range(pmic_index);
        }

    }
}

pm_smbchg_data_type* pm_smbchg_get_data(uint8 pmic_index)
{
  if(pmic_index < PM_MAX_NUM_PMICS) 
  {
      return pm_smbchg_data_arr[pmic_index];
  }

  return NULL;
}

uint8 pm_smbchg_get_num_peripherals(uint8 pmic_index)
{
  if((pm_smbchg_data_arr[pmic_index] !=NULL)&& 
  	  (pmic_index < PM_MAX_NUM_PMICS))
  {
      return pm_smbchg_data_arr[pmic_index]->num_of_peripherals;
  }

  return 0;
}

/*if PMI8996 then support this*/
void pm_smbchg_update_dcin_range(uint8 pmic_index)
{
  uint32 updated_dcin_array[DCIN_SIZE] = {300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1450, 1500, 1550, 
                                    1600, 1700, 1800, 1900, 1950, 2000, 2050, 2100, 2200, 2300, 2400};

  uint32 updated_usbin_array[USBIN_SIZE] = {300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1450, 1500, 1550, 1600, 1700, 1800, 
                                            1900, 1950, 2000, 2050, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000};

  DALSYS_memscpy(pm_smbchg_data_arr[pmic_index]->chg_range_data->dcin_current_limits, sizeof(updated_dcin_array), updated_dcin_array, sizeof(updated_dcin_array));

  DALSYS_memscpy(pm_smbchg_data_arr[pmic_index]->chg_range_data->usbin_current_limits, sizeof(updated_usbin_array), updated_usbin_array, sizeof(updated_usbin_array));

  return;

}



