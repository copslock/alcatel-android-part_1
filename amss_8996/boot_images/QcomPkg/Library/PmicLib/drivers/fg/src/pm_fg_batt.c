
/*! \file pm_fg_batt.c 
*  \n
*  \brief  PMIC-BMS MODULE RELATED DECLARATION
*  \details  This file contains functions and variable declarations to support 
*   the PMIC Fule Gauge memory Battery interface module.
*
*  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
08/20/14   al      Updating comm lib 
08/27/14   va      Driver cleanup update
06/25/14   va      Driver update, added protected call
04/14/14   va      Initial Release 
===========================================================================*/

#include "pm_fg_batt.h"
#include "pm_fg_driver.h"
#include "pm_resource_manager.h"


/*===========================================================================
                        TYPE DEFINITIONS 
===========================================================================*/


/*===========================================================================
                         FUNCTION DEFINITIONS
===========================================================================*/
/**
* @brief This function grant access to FG batt access *
* @details
* This function grant access to FG batt access 
* 
* @param[in] pm_fg_data_type Self
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_grant_sec_access(pm_fg_data_type *fg_batt_ptr)
{

  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if (NULL == fg_batt_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type sec_access = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_sec_access;
    err_flag = pm_comm_write_byte(fg_batt_ptr->comm_ptr->slave_id, sec_access, 0xA5, 0);
  }

  return err_flag;
}


/**
* @brief This function returns BMS Fule Gauge Algorithm profile id currently in use* 
* @details
*  This function returns BMS Fule Gauge Algorithm profile id currently in use
* 
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_batt_profile_id_num profile number in use
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_batt_profile_id_in_use(uint32 pmic_device, pm_fg_batt_profile_id_in_use * profile_id_in_use)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  pm_register_address_type fg_batt_profile_id;
  
  pm_fg_data_type*  fg_batt_ptr  = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == profile_id_in_use)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_profile_id = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_battery;
    err_flag = pm_comm_read_byte(fg_batt_ptr->comm_ptr->slave_id, fg_batt_profile_id, &data, 0);
    profile_id_in_use->pm_fg_batt_prof_id = (uint8) (data & 0xC0);
  }

  return err_flag;

}

/**
* @brief This function returns BMS Fule Gauge Batt system status * 
* @details
*  This function returns BMS Fule Gauge Batt system status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_sys_sts system profile status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_sys_batt_sts(uint32 pmic_device, pm_fg_batt_sys_sts *batt_sys_sts)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_sys_sts_addr = 0x00;
  pm_register_data_type data = 0x00;
  pm_fg_batt_sys_sts sys_sts;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == batt_sys_sts)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_sys_sts_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_sys_batt;
    err_flag = pm_comm_read_byte(fg_batt_ptr->comm_ptr->slave_id, fg_batt_sys_sts_addr, &data, 0);
    sys_sts.pm_fg_batt_fg_sys_sts      = (data & 0xE0) >> 5;
    sys_sts.pm_fg_batt_rem_latched_sts = (data & 0x10)? TRUE : FALSE;
    sys_sts.pm_fg_batt_low_rt_sts      = (data & 0x08)? TRUE : FALSE;
    sys_sts.pm_fg_batt_profile_sts     = (data & 0x04)? TRUE : FALSE;

    *batt_sys_sts = (pm_fg_batt_sys_sts) sys_sts;
  }

  return err_flag;

}


/**
* @brief This function returns BMS Fule Gauge Batt detection status * 
* @details
*  This function returns BMS Fule Gauge Batt detection status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_det_sts battery detection status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_detect_batt(uint32 pmic_device, pm_fg_batt_det_sts batt_det_sts, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_det_sts = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 1 << batt_det_sts ;


  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  fg_batt_det_sts = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_det;
  if (NULL == enable || batt_det_sts >= PM_FG_BATT_DET_STS_INVALID )
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_det_sts, mask, &data, 0);
    *enable = (data & (0x1 << batt_det_sts)) ? TRUE : FALSE;

  }

  return err_flag;
}



/**
* @brief This function returns BMS Fule Gauge Batt detection status * 
* @details
*  This function returns BMS Fule Gauge Batt detection status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_det_sts battery detection status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_jeita_rt_info_sts(uint32 pmic_device, pm_fg_batt_jeita_rt_info batt_jeita_info, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_jeita_rt_info_addr = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 1 << batt_jeita_info ;


  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable || batt_jeita_info >= PM_FG_BATT_JEITA_RT_INFO_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_jeita_rt_info_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_info_sts;
    err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_jeita_rt_info_addr, mask, &data, 0);
    *enable = (data & (0x1 << batt_jeita_info)) ? TRUE : FALSE;
  }

  return err_flag;
}


/**
* @brief This function returns BMS Fule Gauge Batt recovery status * 
* @details
*  This function returns BMS Fule Gauge Batt recovery status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_recovery_sts Battery Recovery status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_batt_recovery_sts(uint32 pmic_device, pm_fg_batt_recovery_sts batt_recovery_sts, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_recovery_sts_addr = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 1 << batt_recovery_sts ;


  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable || batt_recovery_sts >= PM_FG_BATT_RECOVERY_STS_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_recovery_sts_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_recovery_sts;
    err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_recovery_sts_addr, mask, &data, 0);
    *enable = (data & (0x1 << batt_recovery_sts)) ? TRUE : FALSE;

  }

  return err_flag;

}

pm_err_flag_type pm_fg_batt_irq_enable(uint32 pmic_device, pm_fg_batt_irq_type irq, boolean enable)
{
    pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type irq_reg;
    pm_register_data_type data = 1 << irq;

    pm_fg_data_type *fg_batt_ptr  = pm_fg_get_data(pmic_device);

    if (NULL == fg_batt_ptr)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (irq >= PM_FG_BATT_IRQ_INVALID)
    {
        err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    else
    {
        if (enable)
        {
            irq_reg = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_en_set;
        }
        else
        {
            irq_reg = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_en_clr;
        }

        err_flag = pm_comm_write_byte(fg_batt_ptr->comm_ptr->slave_id, irq_reg, data, 0);
    }
    return err_flag;
}

pm_err_flag_type pm_fg_batt_irq_clear(uint32  pmic_device, pm_fg_batt_irq_type irq)
{
    pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type data = 1 << irq;
    pm_register_address_type int_latched_clr;
    pm_fg_data_type *fg_batt_ptr  = pm_fg_get_data(pmic_device);

    if (NULL == fg_batt_ptr)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (irq >= PM_FG_BATT_IRQ_INVALID)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        int_latched_clr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_latched_clr;
        err_flag = pm_comm_write_byte(fg_batt_ptr->comm_ptr->slave_id, int_latched_clr, data, 0);
    }

    return err_flag;
}


pm_err_flag_type pm_fg_batt_irq_set_trigger(uint32 pmic_device, pm_fg_batt_irq_type irq, pm_irq_trigger_type trigger)
{
    pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
    uint8 mask = 1 << irq;
    pm_register_data_type set_type, polarity_high, polarity_low;
    pm_register_address_type int_set_type, int_polarity_high, int_polarity_low;

    pm_fg_data_type *fg_batt_ptr  = pm_fg_get_data(pmic_device);

    if (NULL == fg_batt_ptr)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (irq >= PM_FG_BATT_IRQ_INVALID)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        int_set_type = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_set_type;
        int_polarity_high = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_polarity_high;
        int_polarity_low = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_polarity_low;

        switch (trigger)
        {
        case PM_IRQ_TRIGGER_ACTIVE_LOW:
            set_type = 0x00;
            polarity_high = 0x00;
            polarity_low = 0xFF;
            break;
        case PM_IRQ_TRIGGER_ACTIVE_HIGH:
            set_type = 0x00;
            polarity_high = 0xFF;
            polarity_low = 0x00;
            break;
        case PM_IRQ_TRIGGER_RISING_EDGE:
            set_type = 0xFF;
            polarity_high = 0xFF;
            polarity_low = 0x00;
            break;
        case PM_IRQ_TRIGGER_FALLING_EDGE:
            set_type = 0xFF;
            polarity_high = 0x00;
            polarity_low = 0xFF;
            break;
        case PM_IRQ_TRIGGER_DUAL_EDGE:
            set_type = 0xFF;
            polarity_high = 0xFF;
            polarity_low = 0xFF;
            break;
        default:
            return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
        }
        err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, int_set_type, mask, set_type, 0);
        err_flag |= pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, int_polarity_high, mask, polarity_high, 0);
        err_flag |= pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, int_polarity_low, mask, polarity_low, 0);
    }

    return err_flag;
}


pm_err_flag_type pm_fg_batt_irq_status(uint32 pmic_device, pm_fg_batt_irq_type irq, pm_irq_status_type type, boolean *status)
{
    pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type data;
    uint8 mask = 1 << irq;
    pm_register_address_type int_sts;
    pm_fg_data_type *fg_batt_ptr  = pm_fg_get_data(pmic_device);

    if (NULL == fg_batt_ptr)
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (irq >= PM_FG_BATT_IRQ_INVALID)
    {
        err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    else
    {
        switch (type)
        {
        case PM_IRQ_STATUS_RT:
            int_sts = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_rt_sts;
            break;
        case PM_IRQ_STATUS_LATCHED:
            int_sts = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_latched_sts;
            break;
        case PM_IRQ_STATUS_PENDING:
            int_sts = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->int_pending_sts;
            break;
        default:
            return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
        }

        err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, int_sts, mask, &data, 0);
        *status = data ? TRUE : FALSE;
    }

    return err_flag;
}


/* End of common interrupt prototype */

/**
* @brief  This function returns status of 'Force the Battery ID during a First SoC redetection' *
* @details
*  This function returns status of 'Force the Battery ID during a First SoC redetection'
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_sw_batt_id_force_sts software batter id force status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_read_sw_batt_id(uint32 pmic_device, pm_fg_batt_sw_batt_id_force_sts *sw_batt_id_sts)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_sw_batt_id_addr = 0x00;
  pm_register_data_type data = 0x00;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == sw_batt_id_sts)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_sw_batt_id_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_sw_batt_id;
    err_flag = pm_comm_read_byte(fg_batt_ptr->comm_ptr->slave_id, fg_batt_sw_batt_id_addr, &data, 0);
    *sw_batt_id_sts = (pm_fg_batt_sw_batt_id_force_sts) (data & 0x80);
  }

  return err_flag;
}


/**
* @brief  This function forces status of 'Force the Battery ID during a First SoC redetection' *
* @details
*  This function forces status of 'Force the Battery ID during a First SoC redetection'
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_sw_batt_id_force_sts software batter id force status
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_force_sw_batt_id(uint32 pmic_device, pm_fg_batt_sw_batt_id_force_sts sw_batt_id_sts)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_sw_batt_id_addr = 0x00;
  uint8 mask = 0x80;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    fg_batt_sw_batt_id_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_sw_batt_id;
    err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_sw_batt_id_addr, mask, (pm_register_data_type)(sw_batt_id_sts << 0x07), 0);
  }

  return err_flag;
}

/**
* @brief This function returns BMS Fule Gauge Algorithm current profile id currently in use * 
* @details
*  This function returns BMS Fule Gauge Algorithm current profile id currently in use 
*   Battery Profile that is Forced when SW_BATT_ID_FORCE is asserted
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_profile_id Profile number currently set
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_batt_profile_id(uint32 pmic_device, pm_fg_batt_profile_id *profile_id)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_sw_batt_id_addr = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 0xC0;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == profile_id)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_sw_batt_id_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_profile_id;
    err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_sw_batt_id_addr, mask, &data, 0);
    *profile_id = (pm_fg_batt_profile_id)data;
  }

  return err_flag;
}

/**
* @brief This function sets BMS Fule Gauge Algorithm current profile id currently in use * 
* @details
*  This function sets BMS Fule Gauge Algorithm current profile id currently in use 
*   Battery Profile that is Forced when SW_BATT_ID_FORCE is asserted
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_profile_id Profile number currently set
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_set_batt_profile_id(uint32 pmic_device, pm_fg_batt_profile_id profile_id)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_sw_batt_id_addr = 0x00;
  uint8 mask = 0xC0;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (profile_id >= PM_FG_BATT_SW_BATT_PROFILE_ID_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_sw_batt_id_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_profile_id;
    err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_sw_batt_id_addr, mask, (pm_register_data_type)(profile_id << 0x06), 0);
  }

  return err_flag;
}

/**
* @brief This function clear BMS Fule Gauge battery latched status * 
* @details
*  This function clear BMS Fule Gauge battery latched status
*  Writing a 1, will clear FG_BATT_SYS_BATT.BATT_REMOVED_LATCH; If battery remains missing, the latch will get set again 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_profile_id Profile number currently set
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_clr_batt_latched(uint32 pmic_device, pm_fg_batt_latch batt_latch)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_latch_addr = 0x00;
  uint8 mask = 0x80;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (batt_latch >= PM_FG_BATT_REMOVED_LATCH_CLEAR_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_latch_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_removed_latched;
    err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_latch_addr, mask, (pm_register_data_type)batt_latch, 0);
  }

  return err_flag;

}

/**
* @brief This function sets recovery data ready after BMS Fule Gauge gives recovery data information * 
* @details
*  This function sets recovery data ready after BMS Fule Gauge gives recovery data information 
*  Set by software after it provides recovery information
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_recovery_rdy recovery data value
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_set_recovery_rdy(uint32 pmic_device, pm_fg_batt_recovery_rdy recovery_rdy)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_recovery_rdy_addr = 0x00;
  uint8 mask = 0x80;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (recovery_rdy >= PM_FG_BATT_RECOVERY_DATA_RDY_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_recovery_rdy_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_batt_recovery;
    err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_recovery_rdy_addr, mask, (pm_register_data_type)recovery_rdy, 0);
  }

  return err_flag;

}

/**
* @brief This function returns BMS Fule Gauge algo status* 
* @details
*  This function returns BMS Fule Gauge Batt algo status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_peek_mux Battery Algo informaiton
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_batt_fg_algo_info(uint32 pmic_device, pm_fg_batt_peek_mux *batt_fg_algo_sts)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_peek_mux_addr = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 0x7F;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == batt_fg_algo_sts)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_peek_mux_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_peek_mux1;
    err_flag = pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_peek_mux_addr, mask, &data, 0);
    batt_fg_algo_sts->pm_fg_batt_peek_mux_dtest_sel = (uint8)data;
  }

  return err_flag;
}


/**
* @brief This function sets BMS Fule Gauge algo status* 
* @details
*  This function sets BMS Fule Gauge Batt algo status 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[out]pm_fg_batt_peek_mux Battery Algo information 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_set_batt_fg_algo_info(uint32 pmic_device, pm_fg_batt_peek_mux batt_fg_algo_sts)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_peek_mux_addr = 0x00;
  uint8 mask = 0x7F;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    fg_batt_peek_mux_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_peek_mux1;
    err_flag = pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_peek_mux_addr, mask, (pm_register_data_type)batt_fg_algo_sts.pm_fg_batt_peek_mux_dtest_sel, 0);
  }

  return err_flag;

}


/**
* @brief This function returns BMS Fule Gauge misc cfg* 
* @details
*  This function returns BMS Fule Gauge Batt misc cfg 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[in]pm_fg_batt_mis_cfg Battery Algo informaiton
* @param[out]enable cfg status/value
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_get_batt_misc_cfg(uint32 pmic_device, pm_fg_batt_mis_cfg batt_fg_misc_cfg, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_misc_cfg_addr = 0x00;
  pm_register_data_type data = 0x00;
  uint8 mask = 1 << batt_fg_misc_cfg;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable || batt_fg_misc_cfg >= PM_FG_BATT_MISC_CFG_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_misc_cfg_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_misc_cfg;
    //secure access control 
    err_flag  = pm_fg_batt_grant_sec_access(fg_batt_ptr);
    err_flag |= pm_comm_read_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_misc_cfg_addr, mask, &data, 0);
    *enable = (data & (0x1 << batt_fg_misc_cfg)) ? TRUE : FALSE;
  }

  return err_flag;

}


/**
* @brief This function sets BMS Fule Gauge misc cfg* 
* @details
*  This function sets BMS Fule Gauge Batt misc cfg 
*
* @param[in] pmic_pmic_device. Primary: 0 Secondary: 1
* @param[in]pm_fg_batt_mis_cfg Battery misc configuration 
* @param[in]enable TRUE/FALSE value to set 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_batt_set_batt_misc_cfg(uint32 pmic_device, pm_fg_batt_mis_cfg batt_fg_misc_cfg, boolean enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type  fg_batt_misc_cfg_addr = 0x00;
  uint8 mask = 1 << batt_fg_misc_cfg;

  pm_fg_data_type*  fg_batt_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_batt_ptr )
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (batt_fg_misc_cfg >= PM_FG_BATT_MISC_CFG_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    fg_batt_misc_cfg_addr = fg_batt_ptr->fg_register->batt_register->base_address + fg_batt_ptr->fg_register->batt_register->fg_batt_misc_cfg;
    //secure access control 
    err_flag  = pm_fg_batt_grant_sec_access(fg_batt_ptr);
    err_flag |= pm_comm_write_byte_mask(fg_batt_ptr->comm_ptr->slave_id, fg_batt_misc_cfg_addr, mask, (pm_register_data_type)(enable << batt_fg_misc_cfg), 0);
  }

  return err_flag;
}

