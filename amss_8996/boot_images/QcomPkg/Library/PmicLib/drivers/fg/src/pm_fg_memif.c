/*! \file
*  
*  \pm_fg_memif.c ---Fule Gauge Memory Interface Driver implementation.
*  \details FG Driver implementation.
*  Copyright (c) 2014-2016 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/*===========================================================================

EDIT HISTORY FOR MODULE


when        who     what, where, why
--------    ---     ---------------------------------------------------------- 
04/11/16    va      Added FG IMA related APIs
09/22/14    aab     Porting FG driver to SBL 
08/20/14    al      Updating comm lib 
07/17/14    va      Driver Minor Update
04/18/14    va      New file
========================================================================== */

/*===========================================================================

INCLUDE FILES 

===========================================================================*/
#include "pm_fg_memif.h"
#include "pm_fg_driver.h"
#include "pm_resource_manager.h"

/*************************************
NOTE:  METHOD IMPLEMENTATION
**************************************/

/**
* @brief This function grant access to FG MEMIF access *
* @details
* This function grant access to FG MEMIF access 
* 
* @param[in] pm_fg_data_type Self
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_grant_sec_access(pm_fg_data_type *fg_memif_ptr)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    pm_register_address_type sec_access = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_sec_access;
    err_flag = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, sec_access, 0xA5, 0);
  }

  return err_flag;
}

pm_err_flag_type pm_fg_memif_irq_enable(uint32 pmic_device, pm_fg_memif_irq_type irq, boolean enable)
{
  pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type irq_reg;
  pm_register_data_type data = 1 << irq;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (irq >= PM_FG_IRQ_MEMIF_TYPE_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    if (enable)
    {
      irq_reg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_en_set;
    }
    else
    {
      irq_reg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_en_clr;
    }

    err_flag = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, irq_reg, data, 0);
  }
  return err_flag;
}

pm_err_flag_type pm_fg_memif_irq_clear(uint32  pmic_device, pm_fg_memif_irq_type irq)
{
  pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data = 1 << irq;
  pm_register_address_type int_latched_clr;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (irq >= PM_FG_IRQ_MEMIF_TYPE_INVALID)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    int_latched_clr = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_latched_clr;
    err_flag = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, int_latched_clr, data, 0);
  }

  return err_flag;
}


pm_err_flag_type pm_fg_memif_irq_set_trigger(uint32 pmic_device, pm_fg_memif_irq_type irq, pm_irq_trigger_type trigger)
{
  pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
  uint8 mask = 1 << irq;
  pm_register_data_type set_type, polarity_high, polarity_low;
  pm_register_address_type int_set_type, int_polarity_high, int_polarity_low;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (irq >= PM_FG_IRQ_MEMIF_TYPE_INVALID)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    int_set_type = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_set_type;
    int_polarity_high = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_polarity_high;
    int_polarity_low = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_polarity_low;

    switch (trigger)
    {
    case PM_IRQ_TRIGGER_ACTIVE_LOW:
      set_type = 0x00;
      polarity_high = 0x00;
      polarity_low = 0xFF;
      break;
    case PM_IRQ_TRIGGER_ACTIVE_HIGH:
      set_type = 0x00;
      polarity_high = 0xFF;
      polarity_low = 0x00;
      break;
    case PM_IRQ_TRIGGER_RISING_EDGE:
      set_type = 0xFF;
      polarity_high = 0xFF;
      polarity_low = 0x00;
      break;
    case PM_IRQ_TRIGGER_FALLING_EDGE:
      set_type = 0xFF;
      polarity_high = 0x00;
      polarity_low = 0xFF;
      break;
    case PM_IRQ_TRIGGER_DUAL_EDGE:
      set_type = 0xFF;
      polarity_high = 0xFF;
      polarity_low = 0xFF;
      break;
    default:
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    err_flag = pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, int_set_type, mask, set_type, 0);
    err_flag |= pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, int_polarity_high, mask, polarity_high, 0);
    err_flag |= pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, int_polarity_low, mask, polarity_low, 0);
  }

  return err_flag;
}


pm_err_flag_type pm_fg_memif_irq_status(uint32 pmic_device, pm_fg_memif_irq_type irq, pm_irq_status_type type, boolean *status)
{
  pm_err_flag_type    err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_data_type data;
  uint8 mask = 1 << irq;
  pm_register_address_type int_sts;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  else if (NULL == fg_memif_ptr)
  {
    err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (irq >= PM_FG_IRQ_MEMIF_TYPE_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    switch (type)
    {
    case PM_IRQ_STATUS_RT:
      int_sts = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_rt_sts;
      break;
    case PM_IRQ_STATUS_LATCHED:
      int_sts = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_latched_sts;
      break;
    case PM_IRQ_STATUS_PENDING:
      int_sts = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->int_pending_sts;
      break;
    default:
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    err_flag = pm_comm_read_byte_mask(fg_memif_ptr->comm_ptr->slave_id, int_sts, mask, &data, 0);
    *status = data ? TRUE : FALSE;
  }

  return err_flag;
}


/**
* @brief This function returns Fule Gauge Memory Interface Configuration * 
* @details
*  This function returns Fule Gauge Memory Interface Configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_mem_intf_cfg memory Interface configuration
* @param[out]enable TRUE/FALSE value of passed memory interface cfg
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_get_mem_intf_cfg(uint32 pmic_device, pm_fg_memif_mem_intf_cfg fg_mem_intf_cfg, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_cfg;
  pm_register_data_type data;
  uint8 mask = 1 << fg_mem_intf_cfg;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable || fg_mem_intf_cfg >= PM_FG_MEMIF_MEM_INTF_CFG_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    intf_cfg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_cfg;
    err_flag = pm_comm_read_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_cfg, mask, &data, 0);
    *enable = (boolean)data;
  }

  return err_flag;
}


/**
* @brief This function sets Fule Gauge Memory Interface Configuration * 
* @details
*  This function sets Fule Gauge Memory Interface Configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_mem_intf_cfg memory Interface configuration
* @param[in]enable TRUE/FALSE value of passed memory interface cfg
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_set_mem_intf_cfg(uint32 pmic_device, pm_fg_memif_mem_intf_cfg fg_mem_intf_cfg, boolean enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_cfg;
  uint8 mask = 1 << fg_mem_intf_cfg;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (fg_mem_intf_cfg >= PM_FG_MEMIF_MEM_INTF_CFG_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    intf_cfg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_cfg;
    err_flag = pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_cfg, mask, (pm_register_data_type)(enable << fg_mem_intf_cfg), 0);
  }

  return err_flag;
}


/**
* @brief This function returns Fule Gauge Memory Interface Configuration Control * 
* @details
*  This function returns Fule Gauge Memory Interface Configuration Control 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]fg_memif_addr mem intf address 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_get_mem_intf_ctl(uint32 pmic_device, pm_fg_memif_mem_intf_ctl mem_intf_ctl, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_ctl;
  pm_register_data_type data;
  uint8 mask = 1 << mem_intf_ctl;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable || mem_intf_ctl >= PM_FG_MEMIF_MEM_INTF_CTL_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    intf_ctl = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_ctl;
    err_flag = pm_comm_read_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_ctl, mask, &data, 0);
    *enable = (boolean)data;
  }

  return err_flag;
}

/**
* @brief This function sets Fule Gauge Memory Interface Configuration Control * 
* @details
*  This function sets Fule Gauge Memory Interface Configuration Control
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_mem_intf_ctl memory Interface Configuration Control
* @param[in]enable TRUE/FALSE value of passed memory interface ctl
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_set_mem_intf_ctl(uint32 pmic_device, pm_fg_memif_mem_intf_ctl mem_intf_ctl, boolean enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_ctl;
  uint8 mask = 1 << mem_intf_ctl;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (mem_intf_ctl >= PM_FG_MEMIF_MEM_INTF_CTL_INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    intf_ctl = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_ctl;
    err_flag = pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_ctl, mask, (pm_register_data_type)(enable << mem_intf_ctl), 0);
  }
  return err_flag;
}

/**
* @brief  This function Writes Fule Gauge Memory Interface Address Regsiter * 
* @details
*  This function Writes Fule Gauge Memory Interface Address Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_mem_intf_ctl memory Interface configuration Control
* @param[out]enable TRUE/FALSE value of passed memory interface ctl
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_write_addr(uint32 pmic_device, uint16 fg_memif_addr)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type addr_msb;
  pm_register_address_type addr_lsb;
  pm_register_data_type    addr_0 = fg_memif_addr & 0x00FF;
  pm_register_data_type    addr_1 = (fg_memif_addr & 0xFF00) >> 8;


  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    addr_lsb = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_addr_lsb;
    addr_msb = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_addr_msb;

    err_flag  = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_lsb, addr_0, 0);
    err_flag |= pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb, addr_1, 0);
  }

  return err_flag;
}

/**
* @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
* @details
*  This function Reads Fule Gauge Memory Interface Address Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]fg_memif_addr mem intf address 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_read_addr_reg(uint32 pmic_device, uint16 *fg_memif_addr)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type addr_msb;
  pm_register_address_type addr_lsb;
  pm_register_data_type    addr_0 = 0x00;
  pm_register_data_type    addr_1 = 0x00;
  uint16 memif_addr = 0x0000;


  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_addr)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    addr_lsb = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_addr_lsb;
    addr_msb = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_addr_msb;

    err_flag  = pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_lsb, &addr_0, 0);
    err_flag |= pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb, &addr_1, 0);

    memif_addr = ((addr_1 | memif_addr) << 8) | (addr_0 | memif_addr);
    *fg_memif_addr = memif_addr;
  }

  return err_flag;
}


/**
* @brief  This function Writes Fule Gauge Memory Interface Data Regsiter * 
* @details
*  This function Writes Fule Gauge Memory Interface Data Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]fg_memif_data mem intf data 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_write_data(uint32 pmic_device, uint32 fg_memif_data)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type addr_msb_0, addr_msb_1;
  pm_register_address_type addr_lsb_0, addr_lsb_1;
  pm_register_data_type    data_0 = fg_memif_data & 0x00FF;
  pm_register_data_type    data_1 = (fg_memif_data & 0xFF00) >> 8;
  pm_register_data_type    data_2 = (fg_memif_data & 0xFF0000) >> 16;
  pm_register_data_type    data_3 = (fg_memif_data & 0xFF000000) >> 24;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    addr_lsb_0 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_wr_data0;
    addr_lsb_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_wr_data1;
    addr_msb_0 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_wr_data2;
    addr_msb_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_wr_data3;

    err_flag  = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_lsb_0, data_0, 0);
    err_flag |= pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_lsb_1, data_1, 0);
    err_flag |= pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb_0, data_2, 0);
    err_flag |= pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb_1, data_3, 0);
  }

  return err_flag;
}

/**
* @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
* @details
*  This function Reads Fule Gauge Memory Interface Address Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]fg_memif_data mem intf data 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_read_data_reg(uint32 pmic_device, uint32 *fg_memif_data)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type addr_msb_0, addr_msb_1;
  pm_register_address_type addr_lsb_0, addr_lsb_1;
  pm_register_data_type data_0 = 0x00, data_1 = 0x00;
  pm_register_data_type data_2 = 0x00, data_3 = 0x00;
  uint32 memif_data = 0x00000000;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_data)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {

    addr_lsb_0 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_rd_data0;
    addr_lsb_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_rd_data1;
    addr_msb_0 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_rd_data2;
    addr_msb_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_rd_data3;

    err_flag = pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id,  addr_lsb_0, &data_0, 0);
    err_flag |= pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_lsb_1, &data_1, 0);
    err_flag |= pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb_0, &data_2, 0);
    err_flag |= pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb_1, &data_3, 0);

    memif_data = ((data_3 | memif_data) << 24) | ((data_2 | memif_data) << 16) | ((data_1 | memif_data) << 8) | (data_0 | memif_data);

    *fg_memif_data = memif_data;
  }

  return err_flag;
}

/**
* @brief  This function Writes Fule Gauge Memory Interface Data Regsiter * 
* @details
*  This function Writes Fule Gauge Memory Interface Data Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_otp_cfg mem intf data 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_set_otp_cfg(uint32 pmic_device, pm_fg_memif_otp_cfg fg_memif_cfg)
{
  /* Not Needed as of now*/
  return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
}

/**
* @brief This function Reads Fule Gauge Memory Interface Address Regsiter* 
* @details
*  This function Reads Fule Gauge Memory Interface Address Regsiter
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out]pm_fg_memif_otp_cfg  mem intf data 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_get_otp_cfg(uint32 pmic_device, pm_fg_memif_otp_cfg *fg_memif_otp_cfg)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_cfg_1, intf_cfg_2;
  pm_register_data_type data_cfg_1 = 0x00, data_cfg_2 = 0x00;
  pm_fg_memif_otp_cfg otp_cfg;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == fg_memif_otp_cfg)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    intf_cfg_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_otp_cfg1;
    intf_cfg_2 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_otp_cfg2;

    //secure access control 
    err_flag =  pm_fg_memif_grant_sec_access(fg_memif_ptr);

    err_flag  = pm_comm_read_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_cfg_1, 0xFF, &data_cfg_1, 0);
    err_flag |= pm_comm_read_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_cfg_2, 0xFF, &data_cfg_2, 0);

    otp_cfg.pm_fg_memif_otp_cfg_ptm          = data_cfg_1 & 0x03;
    otp_cfg.pm_fg_memif_otp_cfg_pprog        = data_cfg_1 & 0x04;
    otp_cfg.pm_fg_memif_otp_cfg_vpp_sel      = data_cfg_1 & 0x08;
    otp_cfg.pm_fg_memif_otp_cfg_margin_rd_ws = data_cfg_2 & 0x0F;

    *fg_memif_otp_cfg = otp_cfg;

  }

  return err_flag;

}


/**
* @brief  This function Enable/Disbale Fule Gauge Memory Interface OTP Programming * 
* @details
*  This function Enable/Disbale Fule Gauge Memory Interface OTP Programming 
*  Read status of this setting by calling 'pm_fg_memif_get_ot_cfg function'
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]enable Enable/Disable 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_ctl_otp_pprog(uint32 pmic_device, boolean enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type intf_cfg_1;
  uint8 mask = 1 << 2;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    intf_cfg_1 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_otp_cfg1;
    err_flag = pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, intf_cfg_1, mask, (pm_register_data_type)enable, 0);
  }

  return err_flag;

}



/**
* @brief This function returns Fule Gauge Memory Interface IMA Configuration * 
* @details
*  This function returns Fule Gauge Memory Interface IMA Configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_ima_cfg  memory Interface configuration
* @param[out]enable TRUE/FALSE value of passed memory interface cfg
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_get_ima_cfg(uint32 pmic_device, pm_fg_memif_ima_cfg fg_ima_cfg, boolean *enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type ima_cfg_reg = 0;
  pm_register_data_type data = 0x00;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (NULL == enable)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    ima_cfg_reg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_ima_cfg;
    err_flag = pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, ima_cfg_reg, &data, 0);
    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
       *enable = (data & (0x1 << fg_ima_cfg)) ? TRUE : FALSE;
    }

  }

  return err_flag;
}


/**
* @brief This function sets Fule Gauge Memory Interface IMA Configuration * 
* @details
*  This function sets Fule Gauge Memory Interface IMA Configuration 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[in]pm_fg_memif_ima_cfg memory Interface configuration
* @param[in]enable TRUE/FALSE value of passed memory interface cfg
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_set_ima_cfg(uint32 pmic_device, pm_fg_memif_ima_cfg fg_ima_cfg, boolean enable)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type ima_cfg_reg = 0;
  uint8 mask = 1 << fg_ima_cfg;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (fg_ima_cfg >= PM_FG_MEMIF_IMA_CFG__INVALID)
  {
    err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
  }
  else
  {
    ima_cfg_reg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_ima_cfg;
    err_flag = pm_comm_write_byte_mask(fg_memif_ptr->comm_ptr->slave_id, ima_cfg_reg, mask, (pm_register_data_type)(enable << fg_ima_cfg), 0);
  }

  return err_flag;
}

/**
* @brief This function returns Fule Gauge Memory Interface IMA exception status * 
* @details
*  This function returns Fule Gauge Memory Interface IMA exception status
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
* @param[out] fg_ima_exception_sts : Uint8 
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_get_ima_exception_sts(uint32 pmic_device, uint8 *fg_ima_exception_sts)
{
  pm_err_flag_type         err_flag    = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type ima_exception_sts_reg = 0;
  pm_register_data_type    data = 0x00;

  pm_fg_data_type *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else if (fg_ima_exception_sts == NULL )
  {
    err_flag = PM_ERR_FLAG__PAR_OUT_OF_RANGE;
  }
  else
  {
    ima_exception_sts_reg = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_ima_exception_sts;
    err_flag = pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, ima_exception_sts_reg, &data, 0);

    *fg_ima_exception_sts = data;
  }

  return err_flag;
}



/**
* @brief  This function Runs IMA clear sequnce  * 
* @details
*  This function Runs IMA clear sequnce 
* 
* @param[in] pmic_device_index. Primary: 0 Secondary: 1
*
* @return  pm_err_flag_type 
*          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available on this
*          version of the PMIC.
*          PM_ERR_FLAG__SUCCESS               = SUCCESS.
*
*/
pm_err_flag_type pm_fg_memif_run_ima_clr_sequence(uint32 pmic_device)
{
  pm_err_flag_type         err_flag = PM_ERR_FLAG__SUCCESS;
  pm_register_address_type addr_msb = 0, addr_wr_data3 = 0, addr_rd_data3 = 0;
  pm_register_data_type    data     = 0;
  pm_fg_data_type         *fg_memif_ptr = pm_fg_get_data(pmic_device);

  if (NULL == fg_memif_ptr)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }
  else
  {
    addr_msb      = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_addr_msb;
    addr_wr_data3 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_wr_data3;
    addr_rd_data3 = fg_memif_ptr->fg_register->memif_register->base_address + fg_memif_ptr->fg_register->memif_register->fg_memif_mem_intf_rd_data3;

    err_flag  = pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_msb, 0x04, 0);
    err_flag |= pm_comm_write_byte(fg_memif_ptr->comm_ptr->slave_id, addr_wr_data3, 0x00, 0);
    err_flag |= pm_comm_read_byte(fg_memif_ptr->comm_ptr->slave_id, addr_rd_data3, &data, 0);
  }

  return err_flag;
}

