/*! \file
*  \n
*  \brief  pm_fg_driver.c 
*  \details  
*  Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  Qualcomm Technologies Proprietary and Confidential.
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when        who     what, where, why
--------    ---     ---------------------------------------------------------------- 
12/12/15    al      Updating MEMIF to support to both PMI8994 and PMI8996
09/22/14    aab     Porting FG driver to SBL        
08/20/14    al      Updating comm lib 
08/29/14    al      KW fixes
06/09/14    al      Arch update 
04/01/14    va      New file
================================================================================== */

#include "pm_fg_driver.h"
#include "CoreVerify.h"
#include "pm_utils.h"
#include "device_info.h"

void pm_fg_update_fg_memif_offset(uint8 pmic_index);

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the FG driver data */
static pm_fg_data_type *pm_fg_data_arr[PM_MAX_NUM_PMICS];
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_fg_driver_init(pm_comm_info_type *comm_ptr, peripheral_info_type *peripheral_info, uint8 pmic_index)
{
    pm_fg_data_type *fg_ptr = NULL;
    pm_device_info_type pmic_info;

    fg_ptr = pm_fg_data_arr[pmic_index];

    if (NULL == fg_ptr)
    {
        pm_malloc( sizeof(pm_fg_data_type), (void**)&fg_ptr);
                                                    
        /* Assign Comm ptr */
        fg_ptr->comm_ptr = comm_ptr;

        /* fg Register Info - Obtaining Data through dal config */
        fg_ptr->fg_register = (fg_register_ds*)pm_target_information_get_common_info(PM_PROP_FG_REG);
        CORE_VERIFY_PTR(fg_ptr->fg_register);

	    fg_ptr->num_of_peripherals = pm_target_information_get_periph_count_info(PM_PROP_FG_NUM, pmic_index);
        CORE_VERIFY(fg_ptr->num_of_peripherals != 0);
 
        pm_fg_data_arr[pmic_index] = fg_ptr;

        CORE_VERIFY(PM_ERR_FLAG__SUCCESS == pm_get_pmic_info(pmic_index, &pmic_info));
        if (PMIC_IS_PMI8996 == pmic_info.ePmicModel)
        {
            pm_fg_update_fg_memif_offset(pmic_index);
        }
    }
}


pm_fg_data_type* pm_fg_get_data(uint8 pmic_index)
{
  if(pmic_index <PM_MAX_NUM_PMICS) 
  {
      return pm_fg_data_arr[pmic_index];
  }

  return NULL;
}

uint8 pm_fg_get_num_peripherals(uint8 pmic_index)
{
  if((pm_fg_data_arr[pmic_index] !=NULL)&& 
  	  (pmic_index < PM_MAX_NUM_PMICS))
  {
      return pm_fg_data_arr[pmic_index]->num_of_peripherals;
  }

  return 0;
}


/* This function is to update some of FG_MEMIF registers' offset according to PMI8996 */
void pm_fg_update_fg_memif_offset(uint8 pmic_index)
{
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_cfg       = 0x050;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_ctl       = 0x051;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_cfg            = 0x052;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_operation_sts  = 0x054; 
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_exception_sts  = 0x055; 
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_hardware_sts   = 0x056; 
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_fg_beat_count      = 0x057;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_err_sts        = 0x05F;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_ima_byte_en        = 0x060;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_addr_lsb  = 0x061;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_addr_msb  = 0x062;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_wr_data0  = 0x063;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_wr_data1  = 0x064;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_wr_data2  = 0x065;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_wr_data3  = 0x066;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_rd_data0  = 0x067;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_rd_data1  = 0x068;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_rd_data2  = 0x069;
  pm_fg_data_arr[pmic_index]->fg_register->memif_register->fg_memif_mem_intf_rd_data3  = 0x06A;

  return;
}
