/**
 * @file smem_uefi.c
 *
 * UEFI-specific support code for SMEM
 */

/*==============================================================================
     Copyright (c) 2011-2014 Qualcomm Technologies Incorporated. 
     All rights reserved.
     Qualcomm Confidential and Proprietary
==============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE


when       who     what, where, why
--------   ---     ----------------------------------------------------------
# 12/30/14   bm      Smem PCD values are now 64 bit.
12/18/14   bm      Correct honeybadger RPM MSG RAM addr
12/01/14   bm      Support for Hawker
09/05/14   sm      Compilation warning fixes.
03/04/14   bm      64-bit compilation warning fixes
01/24/14   bm      Assign RPM MSG RAM base address at runtime based on
                   chip information
12/05/13   bm      Support re-locatable SMEM using TZ_WONCE register for 
                   SMEM information structure address.
10/15/13   sm      Support re-locatable SMEM.
02/16/13   bm      Support RPM Message RAM Virtual address conversion.
01/30/13   vk      Fix warning
04/08/11   tl      Created UEFI support code for SMEM
===========================================================================*/

/*===========================================================================
                        INCLUDE FILES
===========================================================================*/

#include "smem_os.h"
#include "smem_target.h"
#include "smem.h"
#include "smem_v.h"
#include "spinlock.h"
#include "smem_targ_info.h"

#include <Library/PcdLib.h>

/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

void*        SMEM_RPM_MSG_RAM_BASE_PHYS;
void*        SMEM_RPM_MSG_RAM_BASE;
unsigned int SMEM_RPM_MSG_RAM_SIZE;
void*        SMEM_TZ_WONCE_REG;

/** Structures for supporting architectures which do not have TZ_WONCE
 *  registers */
uint64 smem_tz_wonce_reg_sim = 0;

/*===========================================================================
FUNCTION      smem_ext_get_smem_size
                                                                             
DESCRIPTION   Returns the shared memory size

ARGUMENTS     None
                  
DEPENDENCIES  None

RETURN VALUE  shared memory size
  
SIDE EFFECTS  None
===========================================================================*/
extern uint32 smem_ext_get_smem_size( void )
{
  smem_targ_info_type *smem_info_ptr;
 
  /* Get SMEM info based on chipset family */
  if((PcdGet32(PcdSmemInformation) & 0xff) == 1/*badger*/)
  {
    /* For badger family, SMEM infomation is in IMEM */
    smem_info_ptr = (smem_targ_info_type *)SMEM_IMEM_BASE;

    /* See if SMEM info is located in IMEM */
    if (smem_info_ptr->identifier == SMEM_TARG_INFO_IDENTIFIER) 
    {
      return (smem_info_ptr->smem_size);
    }

  } else if((PcdGet32(PcdSmemInformation) & 0xff) >= 2/*bear onward*/)
  {
    /* Try to read TZ reg to get where SMEM info is located */
    smem_info_ptr = (smem_targ_info_type *)((UINTN)(*(uint32*)((UINTN)SMEM_TZ_WONCE_REG)));
    if (smem_info_ptr != NULL && 
        smem_info_ptr->identifier == SMEM_TARG_INFO_IDENTIFIER) 
    {
      return (smem_info_ptr->smem_size);
    }
  }
  return (uint32)PcdGet64(PcdSmemSize);
}
/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/

/*===========================================================================
FUNCTION      smem_ext_get_smem_base
                                                                             
DESCRIPTION   Returns the shared memory base address

ARGUMENTS     None
                  
DEPENDENCIES  None

RETURN VALUE  shared memory base address
  
SIDE EFFECTS  None
===========================================================================*/
extern void* smem_ext_get_smem_base( void )
{
  smem_targ_info_type *smem_info_ptr;
 
  /* Get SMEM info based on chipset family */
  if((PcdGet32(PcdSmemInformation) & 0xff) == 1/*badger*/)
  {
    /* For badger family, SMEM infomation is in IMEM */
    smem_info_ptr = (smem_targ_info_type *)SMEM_IMEM_BASE;

    /* See if SMEM info is located in IMEM */
    if (smem_info_ptr->identifier == SMEM_TARG_INFO_IDENTIFIER) 
    {
      return (void*)(uintptr_t)(smem_info_ptr->smem_base_phys_addr);
    }

  } else if((PcdGet32(PcdSmemInformation) & 0xff) >= 2/*bear onward*/)
  {
    if((PcdGet32(PcdSmemInformation) & 0xff) == 2) /* bear */
    {
      SMEM_TZ_WONCE_REG = (void *)0x193d000;
    } else if((PcdGet32(PcdSmemInformation) & 0xff) == 3) /* honeybadger */
    {
      SMEM_TZ_WONCE_REG = (void *)0x007B4000;
    } else if((PcdGet32(PcdSmemInformation) & 0xff) == 4) /* Dragonfly */
    {
      SMEM_TZ_WONCE_REG = (void *)&smem_tz_wonce_reg_sim;
    }
      
    /* Try to read TZ register to get where SMEM info is located */
    smem_info_ptr = (smem_targ_info_type *)((UINTN)(*(uint32*)((UINTN)SMEM_TZ_WONCE_REG)));
    if (smem_info_ptr != NULL && 
        smem_info_ptr->identifier == SMEM_TARG_INFO_IDENTIFIER) 
    {
      return (void*)(uintptr_t)(smem_info_ptr->smem_base_phys_addr);
    }
  }
  return (void*)(uintptr_t)(PcdGet64(PcdSmemBaseAddress));
}

/*===========================================================================
  FUNCTION  smem_map_base_va
===========================================================================*/
/*!
  @brief
  Intitializes a region of shared memory to be uncached to use for SMD.

  @param  None

  @return The shared memory base virtual address.
*/
/*==========================================================================*/
void* smem_map_base_va( void )
{
  return (void*)((UINTN)smem_ext_get_smem_base());
}

/*===========================================================================
  FUNCTION  smem_map_spinlock_region_va
===========================================================================*/
/*!
  @brief
  Intitializes a region of shared memory to be uncached to use for SMD.

  @param  None

  @return The shared memory base virtual address.
*/
/*==========================================================================*/
void* smem_map_spinlock_region_va( void )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
  return NULL;
}

/*===========================================================================
  FUNCTION  smem_map_memory_va
===========================================================================*/
/*!
  @brief
  Intitializes a region of shared memory to be uncached to use for SMD.

  @param  None

  @return The shared memory base virtual address.
*/
/*==========================================================================*/
void *smem_map_memory_va( void* memory, uint32 size )
{
  /* Get SMEM info based on chipset family */
  if((PcdGet32(PcdSmemInformation) & 0xff) == 1/*badger*/)
  {
    /* Set SMEM_RPM_MSG_RAM_BASE/PHYS based on chipset */
    SMEM_RPM_MSG_RAM_BASE_PHYS = (void *)0xfc428000;
    SMEM_RPM_MSG_RAM_BASE = (void *)0xfc428000;
    SMEM_RPM_MSG_RAM_SIZE = 0x4000;
  } else if((PcdGet32(PcdSmemInformation) & 0xff) == 2/*bear family*/)  
  {
    /* Set SMEM_RPM_MSG_RAM_BASE/PHYS based on chipset */
    SMEM_RPM_MSG_RAM_BASE_PHYS = (void *)0x00060000;
    SMEM_RPM_MSG_RAM_BASE = (void *)0x00060000;    
    SMEM_RPM_MSG_RAM_SIZE = 0x5000;
  } else if((PcdGet32(PcdSmemInformation) & 0xff) == 3/*honeybadger*/)  
  {
    /* Set SMEM_RPM_MSG_RAM_BASE/PHYS based on chipset */
    SMEM_RPM_MSG_RAM_BASE_PHYS = (void *)0x00068000;
    SMEM_RPM_MSG_RAM_BASE = (void *)0x00068000;    
    SMEM_RPM_MSG_RAM_SIZE = 0x6000;
  }
  if(memory == (void*)((UINTN)SMEM_RPM_MSG_RAM_BASE_PHYS))
  {
    return (void*)((UINTN)SMEM_RPM_MSG_RAM_BASE);
  } 
  else 
  {
    return smem_map_base_va();
  }
}
