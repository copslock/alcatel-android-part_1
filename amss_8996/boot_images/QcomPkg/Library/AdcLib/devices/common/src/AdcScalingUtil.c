/*============================================================================
  FILE:         AdcScalingUtil.c

  OVERVIEW:     This file provides utility functions for the Analog-to-Digital
                Converter driver Board Support Package.

  DEPENDENCIES: None

                Copyright (c) 2008-2013, 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/AdcLib/devices/common/src/AdcScalingUtil.c#5 $$DateTime: 2015/05/01 17:00:41 $$Author: pwbldsvc $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-04-23  jjo  Add thermistor scaling.
  2015-01-14  jjo  Added divide with rounding.
  2013-02-19  jjo  Added inverse scaling functions.
  2012-06-11  jjo  Added BSP scaling functions.
  2008-03-16  jdt  Initial revision.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DalVAdc.h"
#include "AdcScalingUtil.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
/*===========================================================================

  FUNCTION        AdcLinearInterpolate

  DESCRIPTION     Performs a linear interpolation.

  DEPENDENCIES    None

  PARAMETERS      nNum [in] Numerator
                  nDen [in] Denominator

  RETURN VALUE    Result

  SIDE EFFECTS    None

===========================================================================*/
static int32
AdcLinearInterpolate(
   int32 x,
   int32 x0,
   int32 x1,
   int32 y0,
   int32 y1
   )
{
   int32 y;
   int32 num;
   int32 den;

   num = (y1 - y0) * (x - x0);
   den = x1 - x0;

   y = y0 + AdcDivideWithRounding(num, den);

   return y;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*===========================================================================

  FUNCTION        AdcMapLinearInt32toInt32

  DESCRIPTION     This function uses linear interpolation to calculate an
                  output value given an input value and a map table that maps
                  input values to output values.

                  *pnOutput = (y2 - y1) * (nInput - x1) / (x2 - x1) + y1

  DEPENDENCIES    Requires the ADC DAL

  PARAMETERS      paPts      [in] Mapping between sensor output voltage and the
                                  actual temp (must be ascending or descending)
                  uTableSize [in] Number of entries in paPts
                  nInput     [in] Input value (x)
                  pnOutput   [out] Result (y) interpolated using input value and table

  RETURN VALUE    ADC_DEVICE_RESULT_INVALID if there is an error with
                  the parameters or in performing the calculation.

                  ADC_DEVICE_RESULT_VALID if successful.

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
AdcMapLinearInt32toInt32(
   const AdcMapPtInt32toInt32Type *paPts,
   uint32 uTableSize,
   int32 nInput,
   int32 *pnOutput
   )
{
   DALBOOL bDescending = TRUE;
   uint32 uSearchIdx = 0;
   int32 nX1, nX2, nY1, nY2;

   if ((paPts == NULL) || (pnOutput == NULL))
   {
      return ADC_DEVICE_RESULT_INVALID;
   }

   /* Check if table is descending or ascending */
   if (uTableSize > 1)
   {
      if (paPts[0].x < paPts[1].x)
      {
         bDescending = FALSE;
      }
   }

   while (uSearchIdx < uTableSize)
   {
      if ( (bDescending == TRUE) && (paPts[uSearchIdx].x < nInput) )
      {
        /* table entry is less than measured value and table is descending, stop */
        break;
      }
      else if ( (bDescending == FALSE) && (paPts[uSearchIdx].x > nInput) )
      {
        /* table entry is greater than measured value and table is ascending, stop */
        break;
      }
      else
      {
        uSearchIdx++;
      }
   }

   if (uSearchIdx == 0)
   {
      *pnOutput = paPts[0].y;
   }
   else if (uSearchIdx == uTableSize)
   {
      *pnOutput = paPts[uTableSize-1].y;
   }
   else
   {
      /* result is between search_index and search_index-1 */
      /* interpolate linearly */
      nX1 = paPts[uSearchIdx - 1].x;
      nX2 = paPts[uSearchIdx].x;
      nY1 = paPts[uSearchIdx-1].y;
      nY2 = paPts[uSearchIdx].y;

      *pnOutput = AdcLinearInterpolate(nInput,
                                       nX1,
                                       nX2,
                                       nY1,
                                       nY2);
   }
   return ADC_DEVICE_RESULT_VALID;
}

/*===========================================================================

  FUNCTION        AdcMapLinearInt32toInt32Inverse

  DESCRIPTION     Inverse of AdcMapLinearInt32toInt32:

                  *pnOutput = (x2 - x1) * (nInput - y1) / (y2 - y1) + x1

  DEPENDENCIES    Requires the ADC DAL

  PARAMETERS      paPts      [in] Mapping between sensor output voltage and the
                                  actual temp (must be ascending or descending)
                  uTableSize [in] Number of entries in paPts
                  nInput     [in] Input value (y)
                  pnOutput   [out] Result (x) interpolated using input value and table

  RETURN VALUE    ADC_DEVICE_RESULT_INVALID if there is an error with
                  the parameters or in performing the calculation.

                  ADC_DEVICE_RESULT_VALID if successful.

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
AdcMapLinearInt32toInt32Inverse(
   const AdcMapPtInt32toInt32Type *paPts,
   uint32 uTableSize,
   int32 nInput,
   int32 *pnOutput
   )
{
   DALBOOL bDescending = TRUE;
   uint32 uSearchIdx = 0;
   int32 nX1, nX2, nY1, nY2;

   if (paPts == NULL || pnOutput == NULL)
   {
      return ADC_DEVICE_RESULT_INVALID;
   }

   /* Check if table is descending or ascending */
   if (uTableSize > 1)
   {
      if (paPts[0].y < paPts[1].y)
      {
         bDescending = FALSE;
      }
   }

   while (uSearchIdx < uTableSize)
   {
      if ( (bDescending == TRUE) && (paPts[uSearchIdx].y < nInput) )
      {
        /* table entry is less than measured value and table is descending, stop */
        break;
      }
      else if ( (bDescending == FALSE) && (paPts[uSearchIdx].y > nInput) )
      {
        /* table entry is greater than measured value and table is ascending, stop */
        break;
      }
      else
      {
        uSearchIdx++;
      }
   }

   if (uSearchIdx == 0)
   {
      *pnOutput = paPts[0].x;
   }
   else if (uSearchIdx == uTableSize)
   {
      *pnOutput = paPts[uTableSize-1].x;
   }
   else
   {
      /* result is between search_index and search_index - 1 */
      /* interpolate linearly */
      nX1 = paPts[uSearchIdx - 1].x;
      nX2 = paPts[uSearchIdx].x;
      nY1 = paPts[uSearchIdx-1].y;
      nY2 = paPts[uSearchIdx].y;

      *pnOutput = AdcLinearInterpolate(nInput,
                                       nY1,
                                       nY2,
                                       nX1,
                                       nX2);
   }
   return ADC_DEVICE_RESULT_VALID;
}

/*===========================================================================

  FUNCTION        VAdcScaleTdkNTCGTherm

  DESCRIPTION     Scales the raw ADC result using the calibration data
                  and the Rt/R100-ratio-to-temperature table.

                  This calculation assumes that the ADC calibration readings
                  have a range of at least 2^16 between the positive to
                  negative reference measurements, otherwise it will return
                  ADC_DEVICE_RESULT_INVALID.

  DEPENDENCIES    None

  PARAMETERS      uCode        [in] Raw ADC code
                  uVrefN       [in] ADC code at GND
                  uVrefP       [in] ADC code at VDD
                  paMap        [in] Pointer to linear map table to use
                  uTableSize   [in] The number of points in the map table
                  pnPhysical  [out] Physical value

  RETURN VALUE    AdcDeviceResultStatusType

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScaleTdkNTCGTherm(
   uint32 uCode,
   uint32 uVrefN,
   uint32 uVrefP,
   const AdcMapPtInt32toInt32Type *paMap,
   uint32 uTableSize,
   int32 *pnPhysical
   )
{
   int32 nCode = (int32)uCode;
   int32 nVrefN = (int32)uVrefN;
   int32 nVrefP = (int32)uVrefP;
   int32 nRt_R25;
   int32 nNum;
   int32 nDenom;
   AdcDeviceResultStatusType resultStatus;

   /*
    * For the case where the pull up, Rp, is equal to R25 the
    * ratio of the thermistor divided by its ratio at 25 deg C
    * is Rt/R25 = Rt/Rp.
    *
    *                    C2 ___  (Vdd)
    *                          |
    *                          |
    *                          >
    *                      Rp  < (pull up)
    *                          >
    *                          |
    *                          |
    *                          |- - - Cin
    *                          |
    *                          |
    *                          >
    *                      Rt  < (thermistor)
    *                          >
    *                          |
    *                          |
    *                    C1 ---  (gnd)
    *
    * Voltage divider equation:
    *     Cin - C1 = (C2 - C1) * Rt / (Rp + Rt)
    *
    * Solving for T / P:
    *     Rt/Rp = (Cin - C1) / (C2 - Cin)
    *
    * The shift factor scales nRt_R25 into units of 2^-14 to match
    * the units in the BSP table
    *
    */
   if (nCode < nVrefN)
   {
      nCode = nVrefN;
   }
   else if (nCode > nVrefP)
   {
      nCode = nVrefP;
   }

   nNum = (nCode - nVrefN) << 14;
   nDenom = nVrefP - nCode;

   if (nDenom == 0)
   {
      nRt_R25 = 0x7fffffff;
   }
   else
   {
      nRt_R25 = AdcDivideWithRounding(nNum, nDenom);
   }

   resultStatus = AdcMapLinearInt32toInt32(paMap,
                                           uTableSize,
                                           nRt_R25,
                                           pnPhysical);

   return resultStatus;
}

/*===========================================================================

  FUNCTION        VAdcScaleTdkNTCGThermInverse

  DESCRIPTION     Inverse of VAdcScaleTdkNTCGTherm.

  DEPENDENCIES    None

  PARAMETERS      nPhysical    [in] Physical value
                  uVrefN       [in] ADC code at GND
                  uVrefP       [in] ADC code at VDD
                  paMap        [in] Pointer to linear map table to use
                  uTableSize   [in] The number of points in the map table
                  puCode      [out] Raw ADC code

  RETURN VALUE    AdcDeviceResultStatusType

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScaleTdkNTCGThermInverse(
   int32 nPhysical,
   uint32 uVrefN,
   uint32 uVrefP,
   const AdcMapPtInt32toInt32Type *paMap,
   uint32 uTableSize,
   uint32 *puCode
   )
{
   int32 nRt_R25;
   int64 nCode;
   int64 n64Rt_R25;
   int64 nVrefN = uVrefN;
   int64 nVrefP = uVrefP;
   int64 nNum;
   int64 nDen;
   AdcDeviceResultStatusType resultStatus;

   resultStatus = AdcMapLinearInt32toInt32Inverse(paMap,
                                                  uTableSize,
                                                  nPhysical,
                                                  &nRt_R25);

   n64Rt_R25 = nRt_R25;

   nNum = (n64Rt_R25 * nVrefP) + (nVrefN << 14);
   nDen = n64Rt_R25 + (1 << 14);
   nCode = nNum / nDen;

   if (nCode < nVrefN)
   {
      nCode = nVrefN;
   }
   else if (nCode > nVrefP)
   {
      nCode = nVrefP;
   }

   *puCode = (uint32)nCode;

   return resultStatus;
}

/*===========================================================================

  FUNCTION        VAdcScalePmicTherm

  DESCRIPTION     Scales the ADC result from millivolts to 0.001 degrees
                  Celsius using the PMIC thermistor conversion equation.

  DEPENDENCIES    None

  PARAMETERS      uMicrovolts [in]
                  pnPhyiscal [out]

  RETURN VALUE    AdcDeviceResultStatusType

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScalePmicTherm(
   uint32 uMicrovolts,
   int32 *pnPhysical
   )
{
   /*
    * Divide by two to convert from microvolt reading to micro-Kelvin.
    *
    * Subtract 273160 to convert the temperature from Kelvin to
    * 0.001 degrees Celsius.
    */
   *pnPhysical = AdcDivideWithRounding((int32)uMicrovolts, 2) - 273160;

   return ADC_DEVICE_RESULT_VALID;
}

/*===========================================================================

  FUNCTION        VAdcScalePmicThermInverse

  DESCRIPTION     Scales the ADC result from physical units of 0.001 deg C to
                  microvolts.

  DEPENDENCIES    None

  PARAMETERS      nPhysical     [in]
                  puMicrovolts [out]

  RETURN VALUE    AdcDeviceResultStatusType

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScalePmicThermInverse(
   int32 nPhysical,
   uint32 *puMicrovolts
   )
{
   /*
    * Add 273160 to convert temp from Kelvin to mDegC.
    *
    * Multiply by 2 to convert from  micro-Kelvin to microvolt.
    */
   *puMicrovolts = (uint32)((nPhysical + 273160) * 2);

   return ADC_DEVICE_RESULT_VALID;
}

/*===========================================================================

  FUNCTION        AdcDeviceResultStatusType

  DESCRIPTION     Scaled an ADC code to physical units for a thermistor.

  DEPENDENCIES    None

  PARAMETERS      uCode [in] ADC reading
                  uVrefN [in] ADC code of GND
                  uVrefP [in] ADC code of VREF
                  uPullUp [in] thermistor pull-up
                  paMap [in] interpolation table
                  uTableSize [in] interpolation table size
                  pnPhysical [out] calibrated physical value

  RETURN VALUE    ADC_DEVICE_RESULT_VALID or an error

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScaleThermistor(
   uint32 uCode,
   uint32 uVrefN,
   uint32 uVrefP,
   uint32 uPullUp,
   const AdcMapPtInt32toInt32Type *paMap,
   uint32 uTableSize,
   int32 *pnPhysical
   )
{
   int64 llCode = uCode;
   int64 llVrefN = uVrefN;
   int64 llVrefP = uVrefP;
   int64 llRp = uPullUp;
   int64 llNum;
   int64 llDenom;
   int32 nRt;
   AdcDeviceResultStatusType resultStatus;

   /*
    *
    *                    C2 ___  (Vdd)
    *                          |
    *                          |
    *                          >
    *                      Rp  < (pull up)
    *                          >
    *                          |
    *                          |
    *                          |- - - Cin
    *                          |
    *                          |
    *                          >
    *                      Rt  < (thermistor)
    *                          >
    *                          |
    *                          |
    *                    C1 ---  (gnd)
    *
    * Voltage divider equation:
    *     Cin - C1 = (C2 - C1) * Rt / (Rp + Rt)
    *
    * Solving for Rt:
    *     Rt = (Cin - C1) * Rp / (C2 - Cin)
    *
    */
   if (llCode < llVrefN)
   {
      llCode = llVrefN;
   }
   else if (llCode > llVrefP)
   {
      llCode = llVrefP;
   }

   llNum = (llCode - llVrefN) * llRp;
   llDenom = llVrefP - llCode;

   if (llDenom == 0)
   {
      nRt = 0x7fffffff;
   }
   else
   {
      nRt = (int32)(llNum / llDenom);
   }

   resultStatus = AdcMapLinearInt32toInt32(paMap,
                                           uTableSize,
                                           nRt,
                                           pnPhysical);

   return resultStatus;
}

/*===========================================================================

  FUNCTION        VAdcScaleThermistorInverse

  DESCRIPTION     Performs the reverse scaling of a thermistor channel.

  DEPENDENCIES    None

  PARAMETERS      nPhysical [in] thermistor temperature
                  uVrefN [in] ADC code of GND
                  uVrefP [in] ADC code of VREF
                  uPullUp [in] thermistor pull-up
                  paMap [in] interpolation table
                  uTableSize [in] interpolation table size
                  puCode [out] ADC code

  RETURN VALUE    ADC_DEVICE_RESULT_VALID or an error

  SIDE EFFECTS    None

===========================================================================*/
AdcDeviceResultStatusType
VAdcScaleThermistorInverse(
   int32 nPhysical,
   uint32 uVrefN,
   uint32 uVrefP,
   uint32 uPullUp,
   const AdcMapPtInt32toInt32Type *paMap,
   uint32 uTableSize,
   uint32 *puCode
   )
{
   int32 nRt;
   int64 llCode;
   int64 llRt;
   int64 llRp = uPullUp;
   int64 llVrefN = uVrefN;
   int64 llVrefP = uVrefP;
   AdcDeviceResultStatusType resultStatus;

   resultStatus = AdcMapLinearInt32toInt32Inverse(paMap,
                                                  uTableSize,
                                                  nPhysical,
                                                  &nRt);

   llRt = nRt;

   llCode = ((llVrefP - llVrefN) * llRt) / (llRp + llRt) + llVrefN;

   if (llCode < llVrefN)
   {
      llCode = llVrefN;
   }
   else if (llCode > llVrefP)
   {
      llCode = llVrefP;
   }

   *puCode = (uint32)llCode;

   return resultStatus;
}

/*===========================================================================

  FUNCTION        AdcDivideWithRounding

  DESCRIPTION     Performs a division with rounding.

  DEPENDENCIES    None

  PARAMETERS      nNum [in] Numerator
                  nDen [in] Denominator

  RETURN VALUE    Result

  SIDE EFFECTS    None

===========================================================================*/
int32
AdcDivideWithRounding(
   int32 nNum,
   int32 nDen
   )
{
   int32 nOffset = nDen / 2;

   if (nNum < 0)
   {
      nOffset *= -1;
   }

   return (nNum + nOffset) / nDen;
}

