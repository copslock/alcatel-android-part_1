/*=============================================================================
 
  File: HALHdmiHPD.c
 
  HDMI HAL layer for hot plug detection (HPD)
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
 =============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include <Library/PcdLib.h>

#include "MDPTypes.h"
#include "HALHdmiHPD.h"
#include "HALHdmiCommon.h"
#include "MDPSystem.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_HDMI_HPD_Engine_Ctrl()
*/
/*!
* \brief
*   The \b HAL_HDMI_HPD_Engine_Ctrl enable/disable HPD engine 
*   event
*
* \param [in] - bEnable TRUE/FALSE
*
* \retval void
*
****************************************************************************/
void HAL_HDMI_HPD_Engine_Ctrl(bool32 bEnable)
{
  uint32 uRegVal = 0;
  uint32 uConnectionTimer = 0x9C4;  // Default conncetion timer (2500us)
  uint32 uRxTimer = 0xFA;           // Default Rx timer (250us)

  uRegVal  = (uRxTimer << HWIO_MMSS_HDMI_HPD_CTRL_RX_INT_TIMER_SHFT) |
             (uConnectionTimer << HWIO_MMSS_HDMI_HPD_CTRL_CONNECTION_TIMER_SHFT);
  
  if(bEnable)
  {
    uRegVal |= HWIO_MMSS_HDMI_HPD_CTRL_ENABLE_BMSK;
  }

  HWIO_MMSS_HDMI_HPD_CTRL_OUT(uRegVal);
}


/****************************************************************************
*
** FUNCTION: HAL_HDMI_HPD_Engine_Enable()
*/
/*!
* \brief
*   The \b HAL_HDMI_HPD_Engine_Enable enables counters and control for HPD  
*   event
*
* \param [in] - bEnable TRUE/FALSE
*
* \retval void
*
****************************************************************************/
void HAL_HDMI_HPD_Engine_Enable()
{
    // Setup reference timer to 1 tick per us (from 27Mhz source)
    uint32 uRefTime  = (HWIO_MMSS_HDMI_USEC_REFTIMER_REFTIMER_ENABLE_BMSK | 0x1B); 
  
    // Setup the reference timer
    HWIO_MMSS_HDMI_USEC_REFTIMER_OUT(uRefTime); // divide by 27 for 27.0MHz clock
   
    // Reset the hot plug engine control   
    HAL_HDMI_HPD_Engine_Ctrl(FALSE);
    HAL_HDMI_HPD_Engine_Ctrl(TRUE);
}


/****************************************************************************
*
** FUNCTION: HAL_HDMI_HPD_CheckConnection()
*/
/*!
* \brief
*   The \b HAL_HDMI_HPD_CheckConnection checks whether it is a connect or disconnect 
*   event
*
* \param [in] - None
*
* \retval True/False for connection
*
****************************************************************************/
bool32 HAL_HDMI_HPD_CheckConnection()
{
   bool32 detected = FALSE;
   uint32 i;
   uint32 uHPDLoopCount = (0==PcdGet32(PcdHPDPollCount))?1:PcdGet32(PcdHPDPollCount); // Minimum polling time (ms) minimum of 1 polling loop
   uint32 uRegVal;

    // Wait before polling
   MDP_OSAL_DELAYMS(50);

   // Poll (uHPDLoopCount ms) to check for HDMI connection
   for (i=0; i<uHPDLoopCount; i++)
   {
      MDP_OSAL_DELAYMS(1);

      uRegVal = HWIO_MMSS_HDMI_HPD_INT_STATUS_IN;

      if (uRegVal & HWIO_MMSS_HDMI_HPD_INT_STATUS_SENSE_BMSK)
      {
           detected = TRUE;
           break;
      }
   }
   
   return detected;
}

#ifdef __cplusplus
}
#endif

