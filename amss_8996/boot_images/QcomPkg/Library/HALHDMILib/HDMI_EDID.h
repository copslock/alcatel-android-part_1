#ifndef HDMIEDID_H
#define HDMIEDID_H
/*
===========================================================================

FILE:         HDMI_EDID.h

DESCRIPTION: 
  This is the interface to be used to parse EDID structure
  The intended audience for this source file is HDMI Host layer
  which incorporates this hardware block with other similiar hardware blocks.  
  This interface is not intended or recommended for direct usage by any application.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HDMIHost.h"
#include "HALHdmiCommon.h"


/* -----------------------------------------------------------------------
** Static Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_Open()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Open initializes and caches
*   the EDID structure extracted from the sink device.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_Open(void);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_Close()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_Close initializes and caches
*   the EDID structure extracted from the sink device.
**
* \retval void
*
****************************************************************************/
void HDMI_EDID_Parser_Close(void);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetColorBitDepth()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetColorBitDepth parses the EDID structure and find out
*   the color bit depth.
*
* \param [in]      pVideoCap   - Structure that contains video capabilities settings
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetColorBitDepth(HDMI_PixelFormatType* pPixelFormat);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetOrientation()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetOrientation parses the EDID structure and find out
*   whether it is portrait or landscape mode.
*
* \param [in]      pVideoCap   - Structure that contains video capabilities settings
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetOrientation(HDMI_RotateFlipType* pRotation);

/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetAudioModeInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetAudioModeInfo parses the EDID structure and find out
*   the audio capabilities from each audio short descriptors.
*
* \param [in] pAudioModeInfo - Pointer to a structure containing the properties of a short audio descriptor.
*
* \retval void
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetAudioModeInfo(HDMI_AudioModeInfoType* pAudioModeInfo);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetVendorInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetVendorInfo parses the EDID structure and find out
*   the vendor specific information.
*
* \param [in] pVendorInfo  - pointer to the vendor information
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetVendorInfo(HDMI_VendorInfoType* pVendorInfo);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetDisplayModeInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetDisplayModeInfo parses display mode list and provide
*   display timing generations to the caller. 

* \param [in] pDispModeInfo - Contains all the attributes of a particular display mode
*
* \retval QDI_STATUS
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetDisplayModeInfo(HDMI_DispModeInfoType* pDispModeInfo);
/****************************************************************************
*
** FUNCTION: HDMI_EDID_Parser_GetRawEdidInfo()
*/
/*!
* \brief
*   The \b HDMI_EDID_Parser_GetRawEdidInfo extracts the entire RAW edid information
*
* \param [in] pEdidInfo - Contains the length and data of the display EDID block.
*
* \retval QDI_STATUS
*
****************************************************************************/
MDP_Status HDMI_EDID_Parser_GetRawEdidInfo(HDMI_EdidInfoType* pEdidInfo);
#endif /* HDMIEDID_H */


