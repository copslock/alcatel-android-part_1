/*=============================================================================
 
  File: HALHDMIDriver.c
 
  HAL layer for HDMI driver
  
 
 Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
 =============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/EFITlmm.h>
#include <Protocol/EFIPmicVreg.h>
#include <Protocol/EFIPmicMpp.h>
#include <Protocol/EFIPmicGpio.h>

#include <Library/PcdLib.h>

#include "HDMIHost.h"
#include "HDMI_EDID.h"
#include "HALHdmiVideo.h"
#include "HALHDMIDriver.h"
#include "MDPPlatformLib.h"
#include "HALHdmiPhy.h"


/* -----------------------------------------------------------------------
** Defines
** ----------------------------------------------------------------------- */
#define HDMI_PLL_SRC                     3  // DSIPLL0 SRC

/* -----------------------------------------------------------------------
** Enum Types 
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Structure Types 
** ----------------------------------------------------------------------- */

/* HDMI Handle */
static void       *hHDMIDeviceHandle = NULL;

/* -----------------------------------------------------------------------
** Global Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Forward Declaration of Private Functions.
** ----------------------------------------------------------------------- */
static MDP_Status initHDMICore(void);

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HAL_HDMI_Init()
** 
** DESCRIPTION:
**   Initialize LCD panel to use display.
**
*//* -------------------------------------------------------------------- */
MDP_Status HAL_HDMI_Init()
{
  MDP_Status   eRetStatus  = MDP_STATUS_OK;

  // Start HDMI Clocks 
  if (MDP_STATUS_OK != (eRetStatus = MDPSetupClocks(MDP_CLOCKTYPE_HDMI, NULL)))
  {
    DEBUG ((EFI_D_WARN, "DisplayDxe: Unable to setup HDMI clocks!\n"));
  } 
  // Initialize HDMI Core 
  else if (MDP_STATUS_OK != (eRetStatus = initHDMICore()))
  {
    DEBUG ((EFI_D_WARN, "DisplayDxe: Failed to initialize HDMI!\n"));
  }

  return eRetStatus;
} /* HAL_HDMI_Init() */


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HAL_HDMI_Init()
** 
** DESCRIPTION:
**   Initialize LCD panel to use display.
**
*//* -------------------------------------------------------------------- */
bool32 HAL_HDMI_DetectHPD(void)
{
#ifdef badger_bringup
    MDP_Status  eRetStatus    = MDP_STATUS_OK;
    bool32      bHPDConnected = FALSE;

    HDMI_Device_PropertyParamsType  sPropertyParam;
  
    // Query HPD status
    eRetStatus = HDMI_Device_GetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_CONNECTION_STATUS, &sPropertyParam);
  
    if ((EFI_SUCCESS == eRetStatus) && (TRUE == sPropertyParam.bConnected))
    {
       bHPDConnected = TRUE;
    }
#else
 bool32      bHPDConnected = TRUE;
#endif
    return bHPDConnected;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HAL_HDMI_Term()
** 
** DESCRIPTION:
**   Unload panel.
**
*//* -------------------------------------------------------------------- */
MDP_Status HAL_HDMI_Term()
{
  MDP_Status   eRetStatus = MDP_STATUS_OK;

  if (NULL != hHDMIDeviceHandle)
  {
     HDMI_Device_PropertyParamsType sPropertyParam;
     HdmiPhyConfigType              HdmiPhyConfigInfo;

     sPropertyParam.ePowerMode = HDMI_POWER_OFF;
     
     if (MDP_STATUS_OK == HDMI_Device_SetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_POWER_MODE, &sPropertyParam))
     {
         eRetStatus = HDMI_Device_Commit(hHDMIDeviceHandle, 0);
     }

     //Turn off HDMI PHY PLL
     HdmiPhyConfigInfo.bEnableOutVoltageSwingCtrl = 0;
     HdmiPhyConfigInfo.hdmi_pll_freq              = 0;
     HdmiPhyConfigInfo.uOutVoltageSwingCtrl       = 0;
     
     HAL_HDMI_PHY_Config(&HdmiPhyConfigInfo);

     // Close the HDMI devicve   
     HDMI_Device_Close(hHDMIDeviceHandle, 0x00);

     // Terminate core
     HDMI_DeInit();

     //Disable HDMI related clocks.
     MDPDisableClocks(MDP_CLOCKTYPE_HDMI);


     // Reset the HDMI handle
     hHDMIDeviceHandle = NULL;
  }

  return eRetStatus;
}


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HAL_HDMI_SetMode()
** 
** DESCRIPTION:
**   Set Display Mode.
**
*//* -------------------------------------------------------------------- */
MDP_Status HAL_HDMI_SetMode(uint32  uModeIndex)
{
  MDP_Status eRetStatus = MDP_STATUS_FAILED;
  // HDMI External Clock List 
  MDPExternalClockEntry HDMIExternalClocks[] =
  {
      {"mdss_extpclk_clk",   HDMI_PLL_SRC, 0, 0, 0, 0 ,0}, // 0
      {"\0", 0, 0, 0, 0, 0, 0}    
  };

  if (NULL != hHDMIDeviceHandle)
  {
    HDMI_Device_PropertyParamsType  sPropertyParam;
    HdmiPhyConfigType               HdmiPhyConfigInfo;
    
    // Clear the display mode structure
    MDP_OSAL_MEMZERO(&sPropertyParam, sizeof(HDMI_Device_PropertyParamsType));
    sPropertyParam.sDispModeInfo.uModeIndex = uModeIndex;
    
    //Query the mode information
    if(MDP_STATUS_OK == HDMI_Device_GetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_DISP_MODE, &sPropertyParam))
    {
       MDP_OSAL_MEMZERO(&HdmiPhyConfigInfo, sizeof(HdmiPhyConfigType));
       HdmiPhyConfigInfo.hdmi_pll_freq = sPropertyParam.sDispModeInfo.sDispModeTiming.uPixelFreq*1000;
       
       //Setup HDMI Phy
       if (TRUE != HAL_HDMI_PHY_Config(&HdmiPhyConfigInfo))
       {
          DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI PHY Initialization failed !\n"));
       }
       //Program the external clock source
       else if(MDP_STATUS_OK != MDPSetupClocks(MDP_CLOCKTYPE_HDMI, (MDPExternalClockEntry *)&HDMIExternalClocks))
       {
          DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI property Extenral clock configuration failed!\n"));
       }
       else
       {
          sPropertyParam.eMode = HDMI_VIDEO_MODE_DVI;
          if (MDP_STATUS_OK != HDMI_Device_SetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_VIDEO_MODE, &sPropertyParam))
          {
              DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI property HDMI_DEVICE_PROPERTY_VIDEO_MODE failed!\n"));
          }
                    
          sPropertyParam.ePowerMode = HDMI_POWER_ON;
          if (MDP_STATUS_OK !=HDMI_Device_SetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_POWER_MODE, &sPropertyParam))
          {
              DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI property HDMI_DEVICE_PROPERTY_POWER_MODE failed!\n"));
          }
          
          sPropertyParam.sDispModeInfo.uModeIndex = uModeIndex;
          if (MDP_STATUS_OK != HDMI_Device_SetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_DISP_MODE, &sPropertyParam))
          {
              DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI property HDMI_DEVICE_PROPERTY_DISP_MODE failed!\n"));
          }
          
          eRetStatus = HDMI_Device_Commit(hHDMIDeviceHandle, 0);

       }
    }
  }
  else
  {
    eRetStatus = MDP_STATUS_BAD_HANDLE;
  }
  
  return eRetStatus;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HAL_HDMI_GetInfo()
** 
** DESCRIPTION:
**   Get display info.
**   HAL_HDMI_Init MUST be called before calling this func
**
*//* -------------------------------------------------------------------- */
MDP_Status HAL_HDMI_GetInfo(uint32 uModeIndex, MDP_Panel_AttrType *psDisplayAttr)
{  
   MDP_Status                     eStatus = MDP_STATUS_OK;  
   HDMI_Device_PropertyParamsType sHDMIProp;

   // Clear the display mode structure
   MDP_OSAL_MEMZERO(&sHDMIProp, sizeof(HDMI_Device_PropertyParamsType));
   sHDMIProp.sDispModeInfo.uModeIndex = uModeIndex;
   
   //Query the mode
   if(MDP_STATUS_OK == HDMI_Device_GetProperty(hHDMIDeviceHandle, HDMI_DEVICE_PROPERTY_DISP_MODE, &sHDMIProp))
   {
        psDisplayAttr->eColorFormat                           = sHDMIProp.sDispModeInfo.ePixelFormat;    
        psDisplayAttr->uDisplayWidth                          = sHDMIProp.sDispModeInfo.sDispModeTiming.uActiveH;
        psDisplayAttr->uDisplayHeight                         = sHDMIProp.sDispModeInfo.sDispModeTiming.uActiveV;
        psDisplayAttr->sActiveTiming.uDataEnInvertSignal      = FALSE;
        psDisplayAttr->sActiveTiming.uHsyncPulseWidthDclk     = sHDMIProp.sDispModeInfo.sDispModeTiming.uPulseWidthH;
        psDisplayAttr->sActiveTiming.uHsyncFrontPorchDclk     = sHDMIProp.sDispModeInfo.sDispModeTiming.uFrontPorchH;
        psDisplayAttr->sActiveTiming.uHsyncBackPorchDclk      = sHDMIProp.sDispModeInfo.sDispModeTiming.uBackPorchH;
        psDisplayAttr->sActiveTiming.uHsyncSkewDclk           = 0;
        psDisplayAttr->sActiveTiming.uHsyncInvertSignal       = (TRUE==sHDMIProp.sDispModeInfo.sDispModeTiming.bActiveLowH)?1:0;
        psDisplayAttr->sActiveTiming.uVsyncPulseWidthLns      = sHDMIProp.sDispModeInfo.sDispModeTiming.uPulseWidthV;
        psDisplayAttr->sActiveTiming.uVsyncFrontPorchLns      = sHDMIProp.sDispModeInfo.sDispModeTiming.uFrontPorchV;
        psDisplayAttr->sActiveTiming.uVsyncBackPorchLns       = sHDMIProp.sDispModeInfo.sDispModeTiming.uBackPorchV;
        psDisplayAttr->sActiveTiming.uVsyncInvertSignal       = (TRUE==sHDMIProp.sDispModeInfo.sDispModeTiming.bActiveLowV)?1:0;
        psDisplayAttr->uAttrs.sHdmi.bInterlaced               = sHDMIProp.sDispModeInfo.sDispModeTiming.bInterlaced;
        psDisplayAttr->uAttrs.sHdmi.uRefreshRate              = (sHDMIProp.sDispModeInfo.sDispModeTiming.uRefreshRate/1000)<<16;
   }
   else
   {
      eStatus =  MDP_STATUS_BAD_HANDLE;
   }


  return eStatus;
} /* HAL_HDMI_GetInfo() */



/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
**
** FUNCTION: initHDMICore()
**
** DESCRIPTION:
**   Init HDMI Core
**
** ----------------------------------------------------------------------- */
static MDP_Status initHDMICore(void)
{
  MDP_Status              eRetStatus  = MDP_STATUS_OK;
  HDMI_DeviceOpenParam    sOpenParam;

  // Initialize structures
  MDP_OSAL_MEMZERO(&sOpenParam, sizeof(HDMI_DeviceOpenParam));
  sOpenParam.eDeviceID = HDMI_DEVICE_ID0;
  sOpenParam.eMode     = HDMI_VIDEO_MODE_HDMI;

  if (MDP_STATUS_OK != (eRetStatus = HDMI_Init()))
  {
     DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI_Init() failed!\n"));
  }
  else if (MDP_STATUS_OK != (eRetStatus = HDMI_Device_Open((void**)&hHDMIDeviceHandle, &sOpenParam, 0)))
  {
     DEBUG ((EFI_D_WARN, "DisplayDxe: HDMI_Device_Open() failed!\n"));
  }
  
  return eRetStatus;
}

#ifdef __cplusplus
}
#endif

