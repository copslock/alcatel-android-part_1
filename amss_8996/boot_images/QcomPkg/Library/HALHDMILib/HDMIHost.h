#ifndef HDMIHOST_H
#define HDMIHOST_H

/*=============================================================================
 
  File: HDMIHost.h
 
  This is the interface to be used to access Qualcomm's HDMI video interface.
  The intended audience for this source file is 
  panel control layer which incorporates this hardware block with other
  similiar hardware blocks.  This interface is not intended or recommended
  for direct usage by any application.

 
 Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
 =============================================================================*/


/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */

#include "MDPSystem.h"

/* -----------------------------------------------------------------------
** Macros / Definitions
** ----------------------------------------------------------------------- */
#define MAX_NUM_HDMI_DEVICE_USER_CONTEXT             0x1         /* Maximum # of user context */
#define DEVICE_USER_CONTEXT_HANDLE_TYPE              0x10000000  /* Device context signature */

/* HDMI Device Dirty Bits */
#define HDMI_DEVICE_DIRTYBIT_POWER_MODE              0x1
#define HDMI_DEVICE_DIRTYBIT_OPERATION_MODE          0x2
#define HDMI_DEVICE_DIRTYBIT_DISP_MODE_INDEX         0x4
#define HDMI_DEVICE_DIRTYBIT_AUDIO_PARAM             0x8
#define HDMI_DEVICE_DIRTYBIT_AVI_PACKET_TYPE         0x10
#define HDMI_DEVICE_DIRTYBIT_CEC_MSG_TYPE            0x40
#define HDMI_DEVICE_DIRTYBIT_CONTENT_PROTECTION      0x80


#define HDMI_Display_ModeInfoType                     QDI_Display_ModeInfoType

#define HDMI_GETCTX()       &gHdmiSharedData  /* Macro to access HDMI Shared Data*/


/*Defines for EDID features*/
#define HDMI_EDID_BLOCK_SEGMENT_ADDR                      (0x60)   //EDID is separated by multiple segments for multiple EDID blocks.
#define HDMI_EDID_BLOCK_ZERO_DEVICE_ADDR                  (0xA0)   //Block zero device address when reading using DDC protocol
#define HDMI_EDID_BLOCK_SIZE                              (0x80)   //Each page size in the EDID ROM
#define HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS         (0x4)    //Tentative to 4 blocks but theoretical maximum could be 128
#define HDMI_EDID_MAX_NUM_OF_VIDEO_DATA_BLOCKS            (0x4)    //Tentative to 4 blocks but theoretical maximum could be 128
#define HDMI_EDID_MAX_NUM_OF_BLOCKS                       (0x100)  //Maximum number of pages for EDID structure
#define HDMI_EDID_EXTENSION_BLOCK_MAP_TAG_CODE            (0xF0)   //Indicates that this extension block lists all the extension tags of subsequent extension blocks.
#define HDMI_EDID_VIDEO_DATA_BLOCK_TAG_CODE               (0x2)    //Video data block tag code
#define HDMI_EDID_AUDIO_DATA_BLOCK_TAG_CODE               (0x1)    //Audio data block tag code
#define HDMI_EDID_SPEAKER_ALLOC_DATA_BLOCK_TAG_CODE       (0x4)    //Speaker allocation data block tag code
#define HDMI_EDID_VENDOR_SPECIFIC_DATA_BLOCK_TAG_CODE     (0x3)    //Vendor specific data block tag code
#define HDMI_EDID_EXTENDED_DATA_BLOCK_TAG_CODE            (0x7)    //Extended data block tag code
#define HDMI_EDID_EXTENDED_VIDEO_CAP_TAG_CODE             (0x0)    //Extended tag code for video capability data block
#define HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_VIDEO_TAG_CODE (0x1)    //Extended tag code for vendor specific video data block
#define HDMI_EDID_EXTENDED_COLORIMETRY_TAG_CODE           (0x5)    //Extended tag code for colorimetry data block
#define HDMI_EDID_EXTENDED_VENDOR_SPECIFIC_AUDIO_TAG_CODE (0x11)   //Extended tag code for vendor specific audio data block
#define HDMI_EDID_VENDOR_SPECIFIC_BLOCK_IEEE_ID_LENGTH    (0x3)    //The IEEE Registration ID is 24 bit (3 bytes)
#define HDMI_EDID_VENDOR_SPECIFIC_PHYSICAL_ADDR_LENGTH    (0x2)    //The physical address of the source is 16 bit (2 bytes)

#define HDMI_EDID_AUDIO_SHORT_DESC_SIZE                   (0x3)    //Audio short descriptor size
#define HDMI_EDID_AUDIO_BIT_RATE_8KBPS                    (0x1F40) //8000 indicating 8Kbps 
#define HDMI_EDID_MAX_NUM_OF_TAG_CODES                    (0x7)    //Max number of data block tag codes 
//Maximum number of bytes for EDID structure
#define HDMI_EDID_MAX_BUF_SIZE                            (HDMI_EDID_BLOCK_SIZE * HDMI_EDID_MAX_NUM_OF_BLOCKS)
#define HDMI_EDID_READ_TIMEOUT                            (0xFFFFF) //Timeout value for EDID read operation
#define HDMI_EDID_CHECKSUM_OFFSET                         (0x7F)  //Offset to the EDID structure - Checksum byte
#define HDMI_EDID_CEA_EXTENSION_FLAG_OFFSET               (0x7E)  //Offset to the EDID structure - CEC extension byte
#define HDMI_EDID_FIRST_TIMING_DESC_OFFSET                (0x36)  //Offset to the EDID structure - 1st detailed timing descriptor
#define HDMI_EDID_DETAIL_TIMING_RELATIVE_OFFSET           (0x02)  //Offset to the EDID structure - Indicates the starting offset inside a CEA block where the detail timing descriptor exsits.
#define HDMI_EDID_DATA_BLOCK_RELATIVE_OFFSET              (0x04)  //Offset to the EDID structure - The start of any data block is always offset 4 from any CEA extension blocks.
#define HDMI_EDID_VERSION_OFFSET                          (0x12)  //Offset to the EDID structure - EDID Version
#define HDMI_EDID_REVISION_OFFSET                         (0x13)  //Offset to the EDID structure - EDID Revision
#define HDMI_EDID_CEA_EXTENSION_VERSION_OFFSET            (0x81)  //Offset to CEA extension version number - v1,v2,v3 (v1 is seldom, v2 is obsolete, v3 most common)
#define HDMI_EDID_CEA_EXTENSION_FIRST_DESC_OFFSET         (0x82)  //Offset to CEA extension first timing desc - indicate the offset of the first detailed timing descriptor
#define HDMI_EDID_CEA_EXTENSION_CABS_RELATIVE_OFFSET      (0x03)  //Relative offset to CEA extension basic capabilities - indicates underscan, basic audio etc...
#define HDMI_EDID_COMPONENT_PHYSICAL_ADDR_OFFSET          (0x98)  //Offset to Source Physical Address (CEC purposes) 
#define HDMI_EDID_COMPONENT_PHYSICAL_ADDR_SIZE            (0x02)  //Source Physical Address is 2 bytes 
#define HDMI_EDID_VIDEO_INPUT_DEFINITION_OFFSET           (0x14)  //Offset to Video input definition  - indicate the offset of the video input definition
#define HDMI_EDID_VIDEO_SIGNAL_INTERFACE_MASK             (0x80)  //Mask to see whether the sink is Digital signal or analog signal
#define HDMI_EDID_COLOR_BIT_DEPTH_MASK                    (0x70)  //Mask to for the color bit depth definition
#define HDMI_EDID_COLOR_BIT_DEPTH_SHIFT                   (0x4)   //Color Bit depth start from bit(4) to bit(6) - zero based
#define HDMI_EDID_DATA_BLOCK_LENGTH_MASK                  (0x1F)  //Lowest 5 bit mask to extract the amount of bytes inside various data blocks in the EDID structure
#define HDMI_EDID_DATA_BLOCK_TAG_MASK                     (0xE0)  //7-5 bit indicates the various data block tag code 
#define HDMI_EDID_DATA_BLOCK_TAG_SHIFT                    (0x5)   //Shift 5 bits to extract the various data block tag code 
#define HDMI_EDID_SHORT_VIDEO_DESC_VID_MASK               (0x7F)  //Lowest 7 bit mask to extract the Video Indentification code in the EDID structure
#define HDMI_EDID_DETAIL_TIMING_DESC_BLOCK_SIZE           (0x12)  //Each detailed timing descriptor has block size of 18
#define HDMI_EDID_AUDIO_DESC_AUDIO_FORMAT_MASK            (0x78)  //Bit 6-3 indicates the audio format inside a short audio descriptor
#define HDMI_EDID_AUDIO_DESC_AUDIO_FORMAT_SHIFT           (0x3)   //Shift 3 bits to extract the audio format inside a short audio descriptor
#define HDMI_EDID_AUDIO_DESC_AUDIO_MAX_CHANNEL_MASK       (0x7)   //Bit 3-0 indicates the maximum audio channels inside a short audio descriptor
#define HDMI_EDID_BASIC_AUDIO_SUPPORT_MASK                (0x40)  //Bit 6 indicates the whether the sink device supports basic audio or not
#define HDMI_EDID_BASIC_AUDIO_SUPPORT_SHIFT               (0x6)   //Shift 6 to extract the basic audio support bit

#define HDMI_EDID_MANUFACTURER_ID_OFFSET                  (0x08)   //Offset to Manufacturer ID
#define HDMI_EDID_MONITOR_NAME_TAG                        (0xFC)   //Monitor name tag - Indicate this descriptor block contain Monitor Name
#define HDMI_EDID_MONITOR_NAME_MAX_LENGTH                 (0x0D)   //Monitor Name's max length.
#define HDMI_EDID_MONITOR_NAME_TERMINATION_CHAR           (0x0A)   //Monitor Name's termination char.

#define HDMI_EDID_HORIZONTAL_SCREEN_SIZE_OFFSET           (0x15)  //Offset to Horizontal screen size  - indicate the offset of the horizontal screen size
#define HDMI_EDID_VERTICAL_SCREEN_SIZE_OFFSET             (0x16)  //Offset to Vertical screen size  - indicate the offset of the vertical screen size

// Display descriptor offset
#define HDMI_EDID_DISPLAY_DESC_TAG_OFFSET                 (0x03)  //Offset to Display Descriptor Tag Numbers
#define HDMI_EDID_DISPLAY_DESC_ESTABLISH_TIMINGIII_TAG    (0xF7)  //Tag value for establish timing III indentificaton
#define HDMI_EDID_DISPLAY_DESC_ESTABLISH_TIMINGIII_START_OFFSET   (0x06)  //Starting offset for establish timing III

#define HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_TAG               (0xFA)  //Tag value for standing timing identification
#define HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_START_OFFSET      (0x05)  //Starting offset for standing timing identification
#define HDMI_EDID_DISPLAY_DESC_STANDARD_TIMING_DEFINITION_COUNT  (0x06)  //Count of additional standard timing definition

// Established timing offset
#define HDMI_EDID_ESTABLISHED_TIMINGI_START_OFFSET        (0x23) // Established Timings I
#define HDMI_EDID_ESTABLISHED_TIMINGII_START_OFFSET       (0x24) // Established Timings II
#define HDMI_EDID_ESTABLISHED_TIMINGIII_START_OFFSET      (0x25) // Established Timings III



//Standard timing offset
#define HDMI_EDID_STANDARD_TIMING_START_OFFSET		      (0x26) //Standard timing start
#define HDMI_EDID_STANDARD_TIMING_END_OFFSET			  (0x35) //Standard timing end
#define HDMI_EDID_STANDARD_TIMING_ASPECTRATIO_MASK		  (0xC0) //Bits 6 & 7 : Aspect ratio
#define HDMI_EDID_STANDARD_TIMING_ASPECTRATIO_SHIFT       (0x6)  //Shift 6 bits to extract aspect ratio
#define HDMI_EDID_STANDARD_TIMING_VFREQUENCY_BITS_MASK    (0x3F) //Bits 0-5 for Vertical frequency.
#define HDMI_EDID_STANDARD_TIMING_MIN_VFREQUENCY          (0x3C) //Min Vitical frequency.
#define HDMI_EDID_STANDARD_TIMING_HRESOLUTION_SHIFT       (0x3)  //shift 3 bits left in order to get value.
#define HDMI_EDID_STANDARD_TIMING_HRESOLUTION_BASE        (0xF8) //H resolution will be based on this number.

//Offset to the Detailed Timing Descriptors
#define HDMI_EDID_TIMING_PIXEL_CLOCK_LOWER_BYTE_OFFSET        (0x0)  //Relative Offset to the EDID detailed timing descriptors - Pixel Clock
#define HDMI_EDID_TIMING_PIXEL_CLOCK_UPPER_BYTE_OFFSET        (0x1)  //Relative Offset to the EDID detailed timing descriptors - Pixel Clock
#define HDMI_EDID_TIMING_DESC_H_ACTIVE_OFFSET                 (0x2)  //Relative Offset to the EDID detailed timing descriptors - H active
#define HDMI_EDID_TIMING_DESC_H_BLANK_OFFSET                  (0x3)  //Relative Offset to the EDID detailed timing descriptors - H blank
#define HDMI_EDID_TIMING_DESC_UPPER_H_NIBBLE_OFFSET           (0x4)  //Relative Offset to the EDID detailed timing descriptors - Upper 4 bit for each H active/blank field
#define HDMI_EDID_TIMING_DESC_V_ACTIVE_OFFSET                 (0x5)  //Relative Offset to the EDID detailed timing descriptors - V active
#define HDMI_EDID_TIMING_DESC_V_BLANK_OFFSET                  (0x6)  //Relative Offset to the EDID detailed timing descriptors - V blank
#define HDMI_EDID_TIMING_DESC_UPPER_V_NIBBLE_OFFSET           (0x7)  //Relative Offset to the EDID detailed timing descriptors - Upper 4 bit for each V active/blank field
#define HDMI_EDID_TIMING_DESC_H_IMAGE_SIZE_OFFSET             (0xC)  //Relative Offset to the EDID detailed timing descriptors - H image size
#define HDMI_EDID_TIMING_DESC_V_IMAGE_SIZE_OFFSET             (0xD)  //Relative Offset to the EDID detailed timing descriptors - V image size
#define HDMI_EDID_TIMING_DESC_IMAGE_SIZE_UPPER_NIBBLE_OFFSET  (0xE)  //Relative Offset to the EDID detailed timing descriptors - Image Size upper nibble V and H
#define HDMI_EDID_TIMING_DESC_INTERLACE_OFFSET                (0x11) //Relative Offset to the EDID detailed timing descriptors - Interlace flag
#define HDMI_EDID_TIMING_H_ACTIVE_UPPER_NIBBLE_SHIFT          (0x4)  //H Active is upper 4 bit of HDMI_EDID_TIMING_DESC_UPPER_H_NIBBLE_OFFSET
#define HDMI_EDID_TIMING_H_ACTIVE_UPPER_NIBBLE_MASK           (0xF)  //H Active has 4 bit mask
#define HDMI_EDID_TIMING_H_BLANK_UPPER_NIBBLE_MASK            (0xF)  //H Blank is 4 bit mask
#define HDMI_EDID_TIMING_V_ACTIVE_UPPER_NIBBLE_SHIFT          (0x4)  //H Active is upper 4 bit of HDMI_EDID_TIMING_DESC_UPPER_H_NIBBLE_OFFSET
#define HDMI_EDID_TIMING_V_ACTIVE_UPPER_NIBBLE_MASK           (0xF)  //H Active has 4 bit mask
#define HDMI_EDID_TIMING_V_BLANK_UPPER_NIBBLE_MASK            (0xF)  //H Blank is 4 bit mask
#define HDMI_EDID_TIMING_H_IMAGE_SIZE_UPPER_NIBBLE_MASK       (0xF)  //H image size has 4 bit mask
#define HDMI_EDID_TIMING_H_IMAGE_SIZE_UPPER_NIBBLE_SHIFT      (0x4)  //H image size is upper 4 bit
#define HDMI_EDID_TIMING_V_IMAGE_SIZE_UPPER_NIBBLE_MASK       (0xF)  //V Active has 4 bit mask
#define HDMI_EDID_TIMING_INTERLACE_SHIFT                      (0x7)  //Interlace is in bit(7) zero based.
#define HDMI_EDID_TIMING_INTERLACE_MASK                       (0x80) //Interlace flag is 1 bit only.
#define HDMI_EDID_TIMING_BLOCK_0_MAX_DESC                     (0x04) //Maximum number of detail timing descriptors in block 0
#define HDMI_EDID_TIMING_PIXEL_CLOCK_RANGE                    (0x0A) //The ceiling and floor value of the pixel clock when EDID value is round up or round down.
#define HDMI_EDID_TIMING_ASPECT_RATIO_CHECK_RANGE             (0x32) //An arbitrary value used for aspect ratio calculation

/* Vendor specific data block */

#define HDMI_EDID_VENDOR_BLOCK_3D_STRUCTURE_ALL               (0x1)  //3D multi-present field value
#define HDMI_EDID_VENDOR_BLOCK_3D_STRUCTURE_MASK              (0x2)  //3D multi-present field value
#define HDMI_EDID_VENDOR_BLOCK_3D_MULTI_FIELD_LENGTH          (0x2)  //3D multi-present field contains two bytes
#define HDMI_EDID_VENDOR_BLOCK_3D_SIDE_BY_SIDE_HALF_VALUE     (0x8)  //See Table H-2 for 3D structure. Side by side is special as it contains extra sub sample info

#define HDMI_EDID_VENDOR_BLOCK_DEEP_COLOR_SHIFT               (0x3)  //Shift 3 bits to obtain the deep color modes supported
#define HDMI_EDID_VENDOR_BLOCK_DEEP_COLOR_MASK                (0xf)  //Mask for deep colors is 4 bit 
#define HDMI_EDID_VENDOR_BLOCK_AI_SUPPORT_SHIFT               (0x7)  //Shift 7 bits to obtain the AI support bit
#define HDMI_EDID_VENDOR_BLOCK_AI_SUPPORT_MASK                (0x1)  //Mask for AI support is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_DVI_DUAL_SUPPORT_SHIFT         (0x0)  //Shift 0 bits to obtain the DVI dual bit
#define HDMI_EDID_VENDOR_BLOCK_DVI_DUAL_SUPPORT_MASK          (0x1)  //Mask for DVI dual support is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_LATENCY_PRESENT_SHIFT          (0x7)  //Shift 7 bits to obtain the progressive present bit
#define HDMI_EDID_VENDOR_BLOCK_LATENCY_PRESENT_MASK           (0x1)  //Mask for progressive present is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_I_LATENCY_PRESENT_SHIFT        (0x6)  //Shift 6 bits to obtain the Interlace present bit
#define HDMI_EDID_VENDOR_BLOCK_I_LATENCY_PRESENT_MASK         (0x1)  //Mask for Interlace present is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_VIDEO_PRESENT_SHIFT            (0x5)  //Shift 0 bits to obtain the HDMI video present bit
#define HDMI_EDID_VENDOR_BLOCK_VIDEO_PRESENT_MASK             (0x1)  //Mask for HDMI video present is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_CONTENT_TYPE_SHIFT             (0x0)  //Shift 0 bits to obtain the HDMI video content type info
#define HDMI_EDID_VENDOR_BLOCK_CONTENT_TYPE_MASK              (0xf)  //Mask for HDMI video content type is 4 bits.
#define HDMI_EDID_VENDOR_BLOCK_IMAGE_INFO_SHIFT               (0x3)  //Shift 3 bits to obtain the HDMI Image Info field
#define HDMI_EDID_VENDOR_BLOCK_IMAGE_INFO_MASK                (0x3)  //Mask for HDMI Image info field is 2 bits.
#define HDMI_EDID_VENDOR_BLOCK_3D_PRESENT_SHIFT               (0x7)  //Shift 7 bits to obtain the 3D present bit
#define HDMI_EDID_VENDOR_BLOCK_3D_PRESENT_MASK                (0x1)  //Mask for HDMI video present is 1 bit.
#define HDMI_EDID_VENDOR_BLOCK_3D_MULTI_PRESENT_SHIFT         (0x5)  //Shift 6 bits to obtain the 3D multi present bit
#define HDMI_EDID_VENDOR_BLOCK_3D_MULTI_PRESENT_MASK          (0x3)  //Mask for HDMI video present is 2 bit.
#define HDMI_EDID_VENDOR_BLOCK_HDMI_VIC_LEN_SHIFT             (0x5)  //Shift 5 bits to obtain the HDMI_VIC_LEN field
#define HDMI_EDID_VENDOR_BLOCK_HDMI_VIC_LEN_MASK              (0x7)  //Mask for HDMI VIC_LEN field is 3 bits.
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_LEN_SHIFT              (0x0)  //Shift 0 bits to obtain the HDMI_3D_LEN field
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_LEN_MASK               (0x1f)  //Mask for HDMI_3D_LEN field is 5 bits.
#define HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_SHIFT            (0x4)  //Shift 4 bits to obtain the HDMI_2D_VIC_ORDER field
#define HDMI_EDID_VENDOR_BLOCK_HDMI_2D_VIC_X_MASK             (0xf)  //Mask for HDMI_2D_VIC_ORDER field is 4 bits.
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_SHIFT      (0x0)  //Shift 0 bits to obtain the HDMI_3D_STRUCTURE_X field
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_STRUCTURE_X_MASK       (0xf)  //Mask for HDMI_3D_STRUCTURE_X field is 4 bits.
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_DETAIL_X_SHIFT         (0x4)  //Shift 4 bits to obtain the HDMI_3D_DETAIL_X field
#define HDMI_EDID_VENDOR_BLOCK_HDMI_3D_DETAIL_X_MASK          (0xf)  //Mask for HDMI_3D_DETAIL_X field is 4 bits.


#define HDMI_EDID_ESTABLISHED_TIMINGI_720x400p70              (1<<7)
#define HDMI_EDID_ESTABLISHED_TIMINGI_720x400p88              (1<<6)
#define HDMI_EDID_ESTABLISHED_TIMINGI_640x480p60              (1<<5)
#define HDMI_EDID_ESTABLISHED_TIMINGI_640x480p67              (1<<4)
#define HDMI_EDID_ESTABLISHED_TIMINGI_640x480p72              (1<<3)
#define HDMI_EDID_ESTABLISHED_TIMINGI_640x480p75              (1<<2)
#define HDMI_EDID_ESTABLISHED_TIMINGI_800x600p56              (1<<1)
#define HDMI_EDID_ESTABLISHED_TIMINGI_800x600p60              (1<<0)

#define HDMI_EDID_ESTABLISHED_TIMINGII_800x600p72             (1<<7)
#define HDMI_EDID_ESTABLISHED_TIMINGII_800x600p75             (1<<6)
#define HDMI_EDID_ESTABLISHED_TIMINGII_832x624p75             (1<<5)
#define HDMI_EDID_ESTABLISHED_TIMINGII_1024x768p87            (1<<4)
#define HDMI_EDID_ESTABLISHED_TIMINGII_1024x768p60            (1<<3)
#define HDMI_EDID_ESTABLISHED_TIMINGII_1024x768p70            (1<<2)
#define HDMI_EDID_ESTABLISHED_TIMINGII_1024x768p75            (1<<1)
#define HDMI_EDID_ESTABLISHED_TIMINGII_1280x1024p75           (1<<0)

#define HDMI_EDID_ESTABLISHED_TIMINGIII_1280x768p60           (1<<6)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1280x960p60           (1<<3)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1280x1024p60          (1<<1)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1360x768p60           (1<<7)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1440x900p60           (1<<5)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1400x1050p60          (1<<1)
#define HDMI_EDID_ESTABLISHED_TIMINGIII_1680x1050p60          (1<<5)

#define HDMI_HDCP_MAX_BSTATUS_DEV_COUNT                       (0x7F)  // maximum downstream devices from the repeater


/* -----------------------------------------------------------------------
** Enum Types 
** ----------------------------------------------------------------------- */

/*!
 * \b HDMI_DeviceIDType
 *
 * The Logic ID for HDMI TX Core. Currently only support 1 HDMI TX Core.
 */
typedef enum 
{
  HDMI_DEVICE_ID0 = 0,
  HDMI_DEVICE_MAX,
  HDMI_DEVICE_FORCE_32BIT = 0x7FFFFFFF
}HDMI_DeviceIDType;

/*!
 * \b HDMI_VideoFormatType
 */

typedef enum 
{
  HDMI_VIDEO_FORMAT_640x480p60_4_3   = 0,
  HDMI_VIDEO_FORMAT_720x480p60_4_3,
  HDMI_VIDEO_FORMAT_720x480p60_16_9,
  HDMI_VIDEO_FORMAT_1280x720p60_16_9,
  HDMI_VIDEO_FORMAT_1920x1080i60_16_9,
  HDMI_VIDEO_FORMAT_720x480i60_4_3,
  HDMI_VIDEO_FORMAT_1440x480i60_4_3   = HDMI_VIDEO_FORMAT_720x480i60_4_3,
  HDMI_VIDEO_FORMAT_720x480i60_16_9,
  HDMI_VIDEO_FORMAT_1440x480i60_16_9  = HDMI_VIDEO_FORMAT_720x480i60_16_9,
  HDMI_VIDEO_FORMAT_720x240p60_4_3,
  HDMI_VIDEO_FORMAT_1440x240p60_4_3   = HDMI_VIDEO_FORMAT_720x240p60_4_3,
  HDMI_VIDEO_FORMAT_720x240p60_16_9,
  HDMI_VIDEO_FORMAT_1440x240p60_16_9  = HDMI_VIDEO_FORMAT_720x240p60_16_9,
  HDMI_VIDEO_FORMAT_2880x480i60_4_3,
  HDMI_VIDEO_FORMAT_2880x480i60_16_9,
  HDMI_VIDEO_FORMAT_2880x240p60_4_3,
  HDMI_VIDEO_FORMAT_2880x240p60_16_9,
  HDMI_VIDEO_FORMAT_1440x480p60_4_3,
  HDMI_VIDEO_FORMAT_1440x480p60_16_9,
  HDMI_VIDEO_FORMAT_1920x1080p60_16_9,
  HDMI_VIDEO_FORMAT_720x576p50_4_3,
  HDMI_VIDEO_FORMAT_720x576p50_16_9,
  HDMI_VIDEO_FORMAT_1280x720p50_16_9,
  HDMI_VIDEO_FORMAT_1920x1080i50_16_9,
  HDMI_VIDEO_FORMAT_720x576i50_4_3,
  HDMI_VIDEO_FORMAT_1440x576i50_4_3  =   HDMI_VIDEO_FORMAT_720x576i50_4_3,
  HDMI_VIDEO_FORMAT_720x576i50_16_9,
  HDMI_VIDEO_FORMAT_1440x576i50_16_9 =   HDMI_VIDEO_FORMAT_720x576i50_16_9,
  HDMI_VIDEO_FORMAT_720x288p50_4_3,
  HDMI_VIDEO_FORMAT_1440x288p50_4_3  =   HDMI_VIDEO_FORMAT_720x288p50_4_3,
  HDMI_VIDEO_FORMAT_720x288p50_16_9,
  HDMI_VIDEO_FORMAT_1440x288p50_16_9 =   HDMI_VIDEO_FORMAT_720x288p50_16_9,
  HDMI_VIDEO_FORMAT_2880x576i50_4_3,
  HDMI_VIDEO_FORMAT_2880x576i50_16_9,
  HDMI_VIDEO_FORMAT_2880x288p50_4_3,
  HDMI_VIDEO_FORMAT_2880x288p50_16_9,
  HDMI_VIDEO_FORMAT_1440x576p50_4_3,
  HDMI_VIDEO_FORMAT_1440x576p50_16_9,
  HDMI_VIDEO_FORMAT_1920x1080p50_16_9,
  HDMI_VIDEO_FORMAT_1920x1080p24_16_9,
  HDMI_VIDEO_FORMAT_1920x1080p25_16_9,
  HDMI_VIDEO_FORMAT_1920x1080p30_16_9,
  HDMI_VIDEO_FORMAT_2880x480p60_4_3,
  HDMI_VIDEO_FORMAT_2880x480p60_16_9,
  HDMI_VIDEO_FORMAT_2880x576p50_4_3,
  HDMI_VIDEO_FORMAT_2880x576p50_16_9,
  HDMI_VIDEO_FORMAT_1920x1080i50_16_9_special, //	Format 39 is 1080i50 mode with total 1250 vertical lines.
  HDMI_VIDEO_FORMAT_1920x1080i100_16_9,
  HDMI_VIDEO_FORMAT_1280x720p100_16_9,
  HDMI_VIDEO_FORMAT_720x576p100_4_3,
  HDMI_VIDEO_FORMAT_720x576p100_16_9,
  HDMI_VIDEO_FORMAT_720x576i100_4_3,
  HDMI_VIDEO_FORMAT_1440x576i100_4_3  = HDMI_VIDEO_FORMAT_720x576i100_4_3,
  HDMI_VIDEO_FORMAT_720x576i100_16_9,
  HDMI_VIDEO_FORMAT_1440x576i100_16_9 = HDMI_VIDEO_FORMAT_720x576i100_16_9,
  HDMI_VIDEO_FORMAT_1920x1080i120_16_9,
  HDMI_VIDEO_FORMAT_1280x720p120_16_9,
  HDMI_VIDEO_FORMAT_720x480p120_4_3,
  HDMI_VIDEO_FORMAT_720x480p120_16_9,
  HDMI_VIDEO_FORMAT_720x480i120_4_3,
  HDMI_VIDEO_FORMAT_1440x480i120_4_3  = HDMI_VIDEO_FORMAT_720x480i120_4_3,
  HDMI_VIDEO_FORMAT_720x480i120_16_9,
  HDMI_VIDEO_FORMAT_1440x480i120_16_9 = HDMI_VIDEO_FORMAT_720x480i120_16_9,
  HDMI_VIDEO_FORMAT_720x576p200_4_3,
  HDMI_VIDEO_FORMAT_720x576p200_16_9,
  HDMI_VIDEO_FORMAT_720x576i200_4_3,
  HDMI_VIDEO_FORMAT_1440x576i200_4_3  = HDMI_VIDEO_FORMAT_720x576i200_4_3,
  HDMI_VIDEO_FORMAT_720x576i200_16_9,
  HDMI_VIDEO_FORMAT_1440x576i200_16_9 = HDMI_VIDEO_FORMAT_720x576i200_16_9,
  HDMI_VIDEO_FORMAT_720x480p240_4_3,
  HDMI_VIDEO_FORMAT_720x480p240_16_9,
  HDMI_VIDEO_FORMAT_720x480i240_4_3,
  HDMI_VIDEO_FORMAT_1440x480i240_4_3  = HDMI_VIDEO_FORMAT_720x480i240_4_3,
  HDMI_VIDEO_FORMAT_720x480i240_16_9,
  HDMI_VIDEO_FORMAT_1440x480i240_16_9 = HDMI_VIDEO_FORMAT_720x480i240_16_9,
  HDMI_VIDEO_FORMAT_1024x768p60_4_3,
  HDMI_VIDEO_FORMAT_1024x768p70_4_3,  
  HDMI_VIDEO_FORMAT_1024x768p75_4_3,  
  HDMI_VIDEO_FORMAT_1280x768p60_5_3,  
  HDMI_VIDEO_FORMAT_1280x800p60_16_10,
  HDMI_VIDEO_FORMAT_1280x960p60_4_3,
  HDMI_VIDEO_FORMAT_1280x1024p60_5_4, 
  HDMI_VIDEO_FORMAT_1360x768p60_16_9,
  HDMI_VIDEO_FORMAT_1366x768p60_16_9,
  HDMI_VIDEO_FORMAT_1400x1050p60_4_3,  
  HDMI_VIDEO_FORMAT_1440x900p60_16_10,
  HDMI_VIDEO_FORMAT_1600x900p60_16_9,  
  HDMI_VIDEO_FORMAT_1680x1050p60_16_10,
  HDMI_VIDEO_FORMAT_2560x1600p60_16_10,
  HDMI_VIDEO_FORMAT_MAX,
  HDMI_VIDEO_FORMAT_32BIT = 0x7FFFFFFF
} HDMI_VideoFormatType;

/*!
 * \b HDMI_VideoFormat3D_type
 *
 * This is used for transferring 3D video formats
 */
typedef enum 
{
  HDMI_VIDEO_3D_FORMAT_NONE                               = 0,
  HDMI_VIDEO_3D_FORMAT_FRAME_PACKING,                           /* Full resolution frame packing */
  HDMI_VIDEO_3D_FORMAT_FIELD_ALTERNATIVE,                       /* Alternative Field for left and right frame */
  HDMI_VIDEO_3D_FORMAT_LINE_ALTERNATIVE,                        /* Line alternative between left and right frame */
  HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_FULL,                       /* Left and right are side by side full resolution */
  HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH,                            /* Left and depth frame format */
  HDMI_VIDEO_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH,         /* Left and depth and graphics and graphics - depth format */
  HDMI_VIDEO_3D_FORMAT_TOP_BOTTOM,                              /* Top and bottom half resolution */
  HDMI_VIDEO_3D_FORMAT_RESERVED1,                               /* Reserve for future use */
  HDMI_VIDEO_3D_FORMAT_SIDE_BY_SIDE_HALF,                       /* Left and right side by side half resolution */
  HDMI_VIDEO_3D_FORMAT_MAX,
  HDMI_VIDEO_3D_FORMAT_32BIT = 0x7FFFFFFF
}HDMI_VideoFormat3D_type;

/*!
 * \b HDMI_PowerModeType
 *
 * This is used for enabling or disabling the TX Core .
 */
typedef enum 
{
    HDMI_POWER_OFF            = 0,
    HDMI_POWER_ON,
    HDMI_POWER_MAX,
    HDMI_POWER_32BIT = 0x7FFFFFFF
} HDMI_PowerModeType;

/*!
 * \b HDMI_OperationModeType
 *
 * This is used to indicate whether this is DVI or full HDMI mode.
 */
typedef enum 
{
    HDMI_VIDEO_MODE_HDMI            = 0,
    HDMI_VIDEO_MODE_DVI,
    HDMI_VIDEO_MODE_MAX,
    HDMI_VIDEO_MODE_32BIT = 0x7FFFFFFF
} HDMI_OperationModeType;

/*!
 * \b HDMI_Video_AspectRatio
 *
* Defines the aspect ratio of the input video format 
 */
typedef enum
{
  HDMI_VIDEO_ASPECT_RATIO_NONE          = 0,
  HDMI_VIDEO_ASPECT_RATIO_4_3,
  HDMI_VIDEO_ASPECT_RATIO_16_9,
  HDMI_VIDEO_ASPECT_RATIO_5_4,  
  HDMI_VIDEO_ASPECT_RATIO_16_10,  
  HDMI_VIDEO_ASPECT_RATIO_5_3,
  HDMI_VIDEO_ASPECT_RATIO_RESERVED,
  HDMI_VIDEO_ASPECT_RATIO_MAX,
  HDMI_VIDEO_ASPECT_RATIO_FORCE_32BIT,
}HDMI_Video_AspectRatio;


/*!
 * \b HDMI_EDID_VendorBlock3DStructureType
 *
 *  Contains 3D structures indicated by the vendor data block
 */
typedef enum
{
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_NONE                                  = 0x0,
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FRAME_PACKING                         = 0x1,
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_FIELD_ALTERNATIVE                     = 0x2,
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_LINE_ALTERNATIVE                      = 0x4, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_FULL                     = 0x8, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH                          = 0x10, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_L_PLUS_DEPTH_PLUS_GRAPHIC_DEPTH       = 0x20, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_TOP_BOTTOM                            = 0x40, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_HORI_SUB_SAMPLE     = 0x100, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_SIDE_BY_SIDE_HALF_QUINCUNX_SUB_SAMPLE = 0x8000, 
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_MAX,
  HDMI_EDID_VENDOR_BLOCK_3D_FORMAT_32BIT = 0x7FFFFFFF
}HDMI_EDID_VendorBlock3DStructureType;
/*!
 * \b HDMI_EDID_VendorBlockPresentBitType
 *
 *  Contains all the present bits within the vendor specific data block
 */
typedef enum
{
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_NONE         = 0,
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_LATENCY      = 1,     /* Progressive video/audio latency */
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_I_LATENCY    = 2,     /* Interlace video/audio latency */
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_HDMI_VIDEO   = 4,     /* Indicate extra HDMI video specific information */
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_3D           = 8,     /* Indicate all mandatory 3D formats are supported */
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_MAX,
  HDMI_EDID_VENDOR_BLOCK_PRESENT_BIT_32BIT = 0x7FFFFFFF
}HDMI_EDID_VendorBlockPresentBitType;

/*!
 * \b HDMI_EDID_VendorBlockImageSizeInfoType
 *
 *  Indicates whether the image size field in the EDID structure is correct or not
 */
typedef enum
{
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_NO_ADDITIONAL_INFO       = 0,
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_RATIO_TRUE_SIZE_FALSE,        /* Image size has correct aspect ratio but size not guarantee correct */
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_ROUND_NEAREST_CENTIMETER,     /* Image size is correct and round to nearest centimeter */
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_DIVIDE_BY_FIVE,               /* Image size is correct and divide by 5, only applicable when size is greater than 255cm */
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_MAX,
  HDMI_EDID_VENDOR_BLOCK_IMAGE_SIZE_32BIT = 0x7FFFFFFF
}HDMI_EDID_VendorBlockImageSizeInfoType;

/*!
 * \b HDMI_EDID_VendorBlockContentFilterType
 *
 *  Indicates if the sink can support various filtering pertaining to various content types.
 */
typedef enum
{
  HDMI_EDID_VENDOR_BLOCK_CONTENT_NONE     = 0x0,
  HDMI_EDID_VENDOR_BLOCK_CONTENT_GRAPHICS = 0x1,        /* Sink device applies no filtering to the pixel data */
  HDMI_EDID_VENDOR_BLOCK_CONTENT_PHOTO    = 0x2,        /* Sink device supports filtering for still pictures */
  HDMI_EDID_VENDOR_BLOCK_CONTENT_CINEMA   = 0x4,        /* Sink device supports filtering for cinema contents */
  HDMI_EDID_VENDOR_BLOCK_CONTENT_GAME     = 0x8,        /* Sink device supports processing with low audio and video latency */
  HDMI_EDID_VENDOR_BLOCK_CONTENT_MAX,
  HDMI_EDID_VENDOR_BLOCK_CONTENT_32BIT = 0x7FFFFFFF
}HDMI_EDID_VendorBlockContentFilterType;


/*!
 * \b HAL_HDMI_VideoFormatLUTType
 *
 * Table entry for the LUT to check whether the videoFormat is supported by HDMI TX Core v1.0 and the corresponding property
 */ 
typedef struct
{
  HDMI_VideoFormatType      eVideoFormat;
  uint32                    uActiveH;
  uint32                    uFrontPorchH;
  uint32                    uPulseWidthH;
  uint32                    uBackPorchH;
  bool32                    bActiveLowH;
  uint32                    uActiveV;
  uint32                    uFrontPorchV;
  uint32                    uPulseWidthV;
  uint32                    uBackPorchV;
  bool32                    bActiveLowV;
  uint32                    uPixelFreq;      //Must divide by 1000 to get the actual frequency in MHZ
  uint32                    uRefreshRate;    //Must divide by 1000 to get the actual frequency in HZ
  bool32                    bInterlaced;
  bool32                    bSupported;
  uint32                    uPixelRepeatFactor; // Pixel repetition factor denoted as 2^n where is n is the repetition factor
  HDMI_Video_AspectRatio    eAspectRatio;
}HDMI_DispModeTimingType;

/*!
 * \b HDMI_PixelFormatType
 *
 * This is used to get and set the pixel format
 */
#define HDMI_PixelFormatType     MDP_PixelFormatType
/*!
 * \b HDMI_RotateFlipType
 *
 * This is used to get and set rotation orientation
 */
#define HDMI_RotateFlipType      MDP_RotateFlipType

/*!
 * \b HDMI_DispModeInfoType
 *
 * This is used to get and set the display mode information.
 */
typedef struct
{
  uint32                   uModeIndex;
  HDMI_PixelFormatType     ePixelFormat;
  HDMI_RotateFlipType      eRotation;
  HDMI_DispModeTimingType  sDispModeTiming;
  HDMI_VideoFormat3D_type  e3DFrameFormat;
}HDMI_DispModeInfoType;
/*!
 * \b HDMI_Device_PropertyType
 *
 * This is used to configure the various attributes of the TX Core.
 */
typedef enum
{
  HDMI_DEVICE_PROPERTY_POWER_MODE             = 0,   /* Enable/Disable TX Core */
  HDMI_DEVICE_PROPERTY_VIDEO_MODE,                   /* Select DVI or FULL HDMI mode */
  HDMI_DEVICE_PROPERTY_AUDIO_PARAM,                  /* Configure audio parameters (eg: samplerate, channel#) */
  HDMI_DEVICE_PROPERTY_DISP_MODE,                    /* Set and Get HDMI display mode */
  HDMI_DEVICE_PROPERTY_AVI_PACKET_TYPE,              /* Configure Auxiliary Video info frame */
  HDMI_DEVICE_PROPERTY_SEND_CEC_MSG,                 /* Send Consumer Electronic Control (CEC) message to HDMI sink device */
  HDMI_DEVICE_PROPERTY_AUDIO_MODE_INFO,              /* Extracts the audio capabilities(eg: samplerate, channel#) */
  HDMI_DEVICE_PROPERTY_VENDOR_SPECIFIC_INFO,         /* Extracts the vendor specific capabilities(eg: latency) */
  HDMI_DEVICE_PROPERTY_CONTENT_PROTECTION_ENABLE,    /* Enable/Disable content protection */
  HDMI_DEVICE_PROPERTY_EDID_INFO,                    /* Extract Edid Information from HDMI display */
  HDMI_DEVICE_PROPERTY_CONNECTION_STATUS,            /* Get Panel conection status */
  HDMI_DEVICE_PROPERTY_HDCP_INFO,                    /* Get HDCP info */  
  HDMI_DEVICE_PROPERTY_HDCP_STATUS,                  /* Get HDCP status */
  HDMI_DEVICE_PROPERTY_INVALID_EDID_CACHE,           /* Invalid EDID cache */
  HDMI_DEVICE_PROPERTY_DOWNSTREAM_KSV_LIST,          /* Get KSV List of the downstream devices connected to repeater */
  HDMI_DEVICE_PROPERTY_MAX,
  HDMI_DEVICE_PROPERTY_32BIT = 0x7FFFFFFF
}HDMI_Device_PropertyType;


/*!
 * \b HDMI_AVI_ScanType
 *
 *  Defines HDMI display scan type
 */
typedef enum
{
  HDMI_AVI_SCAN_NONE = 0,  /* Scan type will be up to the default of the HDMI display*/
  HDMI_AVI_SCAN_OVERSCAN,  /* Select overscan option for the HDMI display */
  HDMI_AVI_SCAN_UNDERSCAN, /* Select underscan option for the HDMI display */
  HDMI_AVI_SCAN_RESERVED,  /* Reserved for future */
  HDMI_AVI_SCAN_MAX,
  HDMI_AVI_SCAN_FORCE_32BIT = 0x7FFFFFFF
} HDMI_AVI_ScanType;


typedef enum
{ 
  HDMI_CONTENT_TYPE_NONE	 = 0x0,
  HDMI_CONTENT_TYPE_GRAPHICS = 0x1,		 /* Sink device applies no filtering to the pixel data */
  HDMI_CONTENT_TYPE_PHOTO    = 0x2,		 /* Sink device supports filtering for still pictures */
  HDMI_CONTENT_TYPE_CINEMA   = 0x4,		 /* Sink device supports filtering for cinema contents */
  HDMI_CONTENT_TYPE_GAME	 = 0x8,	     /* Sink device supports processing with low audio and video latency */
  HDMI_CONTENT_TYPE_MAX,
  HDMI_CONTENT_TYPE_32BIT    = 0x7FFFFFFF
}HDMI_AVI_ContentFilterType;

/*!
 * \b HDMI_AVI_LetterBoxInfoType
 *
 *  Defines letter box width and height
 */
typedef struct
{
  uint32 uEndOfTopBarLine;        /*Height of the top letter box bar */
  uint32 uStartOfBottomBarLine;   /*Height of the bottom letter box bar */
  uint32 uEndOfLeftBarLine;       /*Width of the left letter box bar */
  uint32 uStartOfRightBarLine;    /*Width of the right letter box bar */
} HDMI_AVI_LetterBoxInfoType;

/*!
 * \b HDMI_AVI_ActiveFormatType
 *
 *  Defines the display aspect ratio of the active pixels
 */
typedef enum
{
  HDMI_AVI_ACTIVE_FORMAT_NONE = 0,
  HDMI_AVI_ACTIVE_FORMAT_RESERVED,                 /* Reserved for future use */
  HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_TOP,             /* Active format is 16:9 and shift top */
  HDMI_AVI_ACTIVE_FORMAT_BOX_14_9_TOP,             /* Active format is 14:9 and shift top */
  HDMI_AVI_ACTIVE_FORMAT_BOX_16_9_CENTER,          /* Active format is 16:9 and shift center */
  HDMI_AVI_ACTIVE_FORMAT_RESERVED1,                /* Reserved for future use */
  HDMI_AVI_ACTIVE_FORMAT_RESERVED2,                /* Reserved for future use */
  HDMI_AVI_ACTIVE_FORMAT_RESERVED3,                /* Reserved for future use */
  HDMI_AVI_ACTIVE_FORMAT_CODED_FRAME,              /* Active format is same as coded frame */
  HDMI_AVI_ACTIVE_FORMAT_4_3_CENTER,               /* Active format is 4:3 shifted center */
  HDMI_AVI_ACTIVE_FORMAT_16_9_CENTER,              /* Active format is 16:9 shifted center */
  HDMI_AVI_ACTIVE_FORMAT_14_9_CENTER,              /* Active format is 14:9 shifted center */
  HDMI_AVI_ACTIVE_FORMAT_RESERVED4,                /* Reserved for future use */
  HDMI_AVI_ACTIVE_FORMAT_4_3_PROTECT_14_9_CENTER,  /* Active format is 4:3 with shoot and protect and 14:9 center*/
  HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_14_9_CENTER, /* Active format is 16:9 with shoot and protect and 14:9 center*/
  HDMI_AVI_ACTIVE_FORMAT_16_9_PROTECT_4_3_CENTER,  /* Active format is 16:9 with shoot and protect and 4:3 center*/
  HDMI_AVI_ACTIVE_FORMAT_MAX,
  HDMI_AVI_ACTIVE_FORMAT_FORCE_32BIT = 0x7FFFFFFF,
} HDMI_AVI_ActiveFormatType;


/*!
 * \b HDMI_AudioChannelNumType
 *
 * The various audio channel number for HDMI Display
 */
typedef enum
{
  HDMI_AUDIO_CHANNEL_NONE = 0x0,
  HDMI_AUDIO_CHANNEL_2    = 0x1,
  HDMI_AUDIO_CHANNEL_4    = 0x2,
  HDMI_AUDIO_CHANNEL_6    = 0x4,
  HDMI_AUDIO_CHANNEL_8    = 0x8,
  HDMI_AUDIO_CHANNEL_MAX,
  HDMI_AUDIO_CHANNEL_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioChannelType;

/*!
 * \b HDMI_AudioSampleRateType
 *
 * The various audio sample rate for HDMI Display
 */
typedef enum
{
  HDMI_AUDIO_SAMPLE_RATE_NONE              = 0x0,
  HDMI_AUDIO_SAMPLE_RATE_32KHZ             = 0x1,
  HDMI_AUDIO_SAMPLE_RATE_44_1KHZ           = 0x2,
  HDMI_AUDIO_SAMPLE_RATE_48KHZ             = 0x4,
  HDMI_AUDIO_SAMPLE_RATE_88_2KHZ           = 0x8,
  HDMI_AUDIO_SAMPLE_RATE_96KHZ             = 0x10,
  HDMI_AUDIO_SAMPLE_RATE_176_4KHZ          = 0x20,
  HDMI_AUDIO_SAMPLE_RATE_192KHZ            = 0x40,
  HDMI_AUDIO_SAMPLE_RATE_MAX,
  HDMI_AUDIO_SAMPLE_RATE_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioSampleRateType;

/*!
 * \b HDMI_AudioSampleBitDepthType
 *
 * The various audio sample bit depth for HDMI Display
 */
typedef enum
{
  HDMI_AUDIO_BIT_DEPTH_NONE   = 0x0,              /* No Audio Bit Depth */
  HDMI_AUDIO_BIT_DEPTH_16_BIT = 0x1,              /* Audio Bit Depth 16bit per sample*/
  HDMI_AUDIO_BIT_DEPTH_20_BIT = 0x2,              /* Audio Bit Depth 20bit per sample*/
  HDMI_AUDIO_BIT_DEPTH_24_BIT = 0x4,              /* Audio Bit Depth 24bit per sample*/
  HDMI_AUDIO_BIT_DEPTH_MAX,
  HDMI_AUDIO_BIT_DEPTH_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioSampleBitDepthType;

/*!
 * \b HDMI_AudioFormatType
 *
 *  Defines the audio formats supported by the HDMI display
 */
typedef enum
{
  HDMI_AUDIO_FORMAT_RESERVED = 0,                  /* Reserved for future */
  HDMI_AUDIO_FORMAT_LPCM,                          /* Audio codec type Linear PCM */
  HDMI_AUDIO_FORMAT_AC3,                           /* Audio codec type AC3 */
  HDMI_AUDIO_FORMAT_MPEG1_LAYER1_AND_2,            /* Audio codec type MPEG1 Layer1 and 2 */
  HDMI_AUDIO_FORMAT_MP3,                           /* Audio codec type MP3*/
  HDMI_AUDIO_FORMAT_MPEG2,                         /* Audio codec type MPEG2 */
  HDMI_AUDIO_FORMAT_AAC,                           /* Audio codec type AAC */
  HDMI_AUDIO_FORMAT_DTS,                           /* Audio codec type DTS */
  HDMI_AUDIO_FORMAT_ATRAC,                         /* Audio codec type ATRAC */
  HDMI_AUDIO_FORMAT_ONE_BIT_AUDIO,                 /* Audio codec type 1 bit audio */
  HDMI_AUDIO_FORMAT_DOLBY_DIGITAL_PLUS,            /* Audio codec type Dolby Digital+ */
  HDMI_AUDIO_FORMAT_DTS_HD,                        /* Audio codec type DTS HD*/
  HDMI_AUDIO_FORMAT_MAT,                           /* Audio codec type MAT */
  HDMI_AUDIO_FORMAT_DST,                           /* Audio codec type DST */
  HDMI_AUDIO_FORMAT_WMA_PRO,                       /* Audio codec type WMA PRO */
  HDMI_AUDIO_FORMAT_RESERVED1,                     /* Reserved for future */
  HDMI_AUDIO_FORMAT_MAX,
  HDMI_AUDIO_FORMAT_FORCE_32BIT = 0x7FFFFFFF
} HDMI_AudioFormatType;

/*!
 * \b HDMI_AudioChannelAllocType
 *
 *  Defines the mapping between the audio channel and sink device speakers
 *
 *  FL  - Front Left
 *  FC  - Front Center
 *  FR  - Front Right
 *  FLC - Front Left Center
 *  FRC - Front Right Center
 *  RL  - Rear Left
 *  RC  - Rear Center
 *  RR  - Rear Right
 *  RLC - Rear Left Center
 *  RRC - Rear Right Center
 *  LFE - Low Frequencey Effect
 */
typedef enum
{
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_0  = 0x0, /* Allocate to FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_1,        /* Allocate to LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_2,        /* Allocate to FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_3,        /* Allocate to FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_4,        /* Allocate to RC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_5,        /* Allocate to RC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_6,        /* Allocate to RC, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_7,        /* Allocate to RC, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_8,        /* Allocate to RR, RL, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_9,        /* Allocate to RR, RL, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_10,       /* Allocate to RR, RL, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_11,       /* Allocate to RR, RL, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_12,       /* Allocate to RC, RR, RL, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_13,       /* Allocate to RC, RR, RL, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_14,       /* Allocate to RC, RR, RL, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_15,       /* Allocate to RC, RR, RL, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_16,       /* Allocate to RRC, RLC, RR, RL, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_17,       /* Allocate to RRC, RLC, RR, RL, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_18,       /* Allocate to RRC, RLC, RR, RL, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_19,       /* Allocate to RRC, RLC, RR, RL, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_20,       /* Allocate to FRC, FLC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_21,       /* Allocate to FRC, FLC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_22,       /* Allocate to FRC, FLC, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_23,       /* Allocate to FRC, FLC, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_24,       /* Allocate to FRC, FLC, RC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_25,       /* Allocate to FRC, FLC, RC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_26,       /* Allocate to FRC, FLC, RC, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_27,       /* Allocate to FRC, FLC, RC, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_28,       /* Allocate to FRC, FLC, RR, RL, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_29,       /* Allocate to FRC, FLC, RR, RL, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_30,       /* Allocate to FRC, FLC, RR, RL, FC, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_31,       /* Allocate to FRC, FLC, RR, RL, FC, LFE, FR, FL speakers */
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_MAX,      
  HDMI_AUDIO_CHANNEL_ALLOC_CODE_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioChannelAllocType;

/*!
 * \b HDMI_AudioLevelShiftType
 *
 *  Defines the audio attenuation level to be shifted by the sink device 
 */
typedef enum
{
  HDMI_AUDIO_LEVEL_SHIFT_0DB   = 0x0,  /* 0 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_1DB,          /* 1 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_2DB,          /* 2 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_3DB,          /* 3 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_4DB,          /* 4 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_5DB,          /* 5 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_6DB,          /* 6 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_7DB,          /* 7 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_8DB,          /* 8 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_9DB,          /* 9 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_10DB,         /* 10 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_11DB,         /* 11 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_12DB,         /* 12 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_13DB,         /* 13 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_14DB,         /* 14 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_15DB,         /* 15 Decibel */
  HDMI_AUDIO_LEVEL_SHIFT_MAX,
  HDMI_AUDIO_LEVEL_SHIFT_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioLevelShiftType;

/*!
 * \b HDMI_AudioSpeakerLocationType
 *
 *  Defines various audio speaker locations
 */
typedef enum
{
  HDMI_AUDIO_SPEAKER_LOCATION_NONE   = 0x0,  /* No Speakers */
  HDMI_AUDIO_SPEAKER_LOCATION_FL     = 0x1,  /* FL  - Front Left */
  HDMI_AUDIO_SPEAKER_LOCATION_FC     = 0x2,  /* FC  - Front Center */
  HDMI_AUDIO_SPEAKER_LOCATION_FR     = 0x4,  /* FR  - Front Right */
  HDMI_AUDIO_SPEAKER_LOCATION_FLC    = 0x8,  /* FLC - Front Left Center */
  HDMI_AUDIO_SPEAKER_LOCATION_FRC    = 0x10, /* FRC - Front Right Center */
  HDMI_AUDIO_SPEAKER_LOCATION_RL     = 0x20, /* RL  - Rear Left */
  HDMI_AUDIO_SPEAKER_LOCATION_RC     = 0x40, /* RC  - Rear Center */
  HDMI_AUDIO_SPEAKER_LOCATION_RR     = 0x80, /* RR  - Rear Right */
  HDMI_AUDIO_SPEAKER_LOCATION_RLC    = 0x100,/* RLC - Rear Left Center */
  HDMI_AUDIO_SPEAKER_LOCATION_RRC    = 0x200,/* RRC - Rear Right Center */
  HDMI_AUDIO_SPEAKER_LOCATION_LFE    = 0x400,/* LFE - Low Frequency Effect */
  HDMI_AUDIO_SPEAKER_LOCATION_MAX,
  HDMI_AUDIO_SPEAKER_LOCATION_FORCE_32BIT = 0x7FFFFFFF
}HDMI_AudioSpeakerLocationType;

/*!
 * \b HDMI_AVI_COLOR_FORMAT
 */
typedef enum
{
  HDMI_AVI_COLOR_FORMAT_RGB,
  HDMI_AVI_COLOR_FORMAT_YUV422,
  HDMI_AVI_COLOR_FORMAT_YUV444,
  HDMI_AVI_COLOR_FORMAT_RESERVED,
  HDMI_AVI_COLOR_FORMAT_MAX,
  HDMI_AVI_COLOR_FORMAT_32BIT = 0x7FFFFFFF
}HDMI_AVI_ColorFormatType;

/*!
 * \b HDMI_AVI_Quantization
 *
* Defines the color encoding standard for the input video data
 */
typedef enum
{
  HDMI_AVI_QUANTIZATION_NONE          = 0,
  HDMI_AVI_QUANTIZATION_LIMITED_RANGE,
  HDMI_AVI_QUANTIZATION_FULL_RANGE,
  HDMI_AVI_QUANTIZATION_MAX,
  HDMI_AVI_QUANTIZATION_FORCE_32BIT = 0x7FFFFFFF
} HDMI_AVI_Quantization;

/*!
 * \b HDMI_AVI_ScaleInfo
 *
* Defines whether source data has been scaled in x and y direction
 */
typedef enum
{
  HDMI_AVI_SCALE_NONE          = 0,
  HDMI_AVI_SCALE_HORIZONTAL,
  HDMI_AVI_SCALE_VERTICAL,
  HDMI_AVI_SCALE_BOTH,
  HDMI_AVI_SCALE_MAX,
  HDMI_AVI_SCALE_FORCE_32BIT,
}HDMI_AVI_ScaleInfo;

/*!
 * \b HDMI_AVI_Colorimetry
 *
* Defines the color encoding standard for the input video data
 */
typedef enum
{
  HDMI_AVI_COLORIMETRY_NONE       = 0,
  HDMI_AVI_COLORIMETRY_ITU601,
  HDMI_AVI_COLORIMETRY_ITU709,
  HDMI_AVI_COLORIMETRY_XVYCC601,
  HDMI_AVI_COLORIMETRY_XVYCC709,
  HDMI_AVI_COLORIMETRY_MAX,
  HDMI_AVI_COLORIMETRY_FORCE_32BIT = 0x7FFFFFFF
} HDMI_AVI_Colorimetry;

/*!
 * \b HDMI_3D_EXT_DataType
 *
 * The indicates the extended data information for 3D formats*/
typedef enum
{
  HDMI_3D_EXT_DATA_NONE = 0,
  HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_ODD_R,                          /*Horizontal subsampling with odd left and odd right */
  HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_ODD_L_EVEN_R,                         /*Horizontal subsampling with odd left and even right */
  HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_EVEN_L_ODD_R,                         /*Horizontal subsampling with even left and odd right */
  HDMI_3D_EXT_DATA_HORI_SUBSAMPLE_EVEN_L_EVEN_R,                        /*Horizontal subsampling with even left and even right */
  HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_ODD_L_ODD_R,                         /*Quicuix subsampling with odd left and odd right */
  HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_ODD_L_EVEN_R,                        /*Quicuix subsampling with odd left and even right */
  HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_EVEN_L_ODD_R,                        /*Quicuix subsampling with even left and odd right */
  HDMI_3D_EXT_DATA_QUINCUIX_MATRIX_EVEN_L_EVEN_R,                       /*Quicuix subsampling with even left and odd right */
  HDMI_3D_EXT_DATA_MAX, 
  HDMI_3D_EXT_DATA_FORCE_32BIT = 0x7FFFFFFF, 
}HDMI_3D_EXT_DataType;

/* -----------------------------------------------------------------------
** Structure Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HDMI_DispModeAttrType
  *
 * Structure that encapsulates the attributes of each display mode supported by the sink device
 */
typedef struct
{
  HDMI_VideoFormatType      eVideoFormat;        /* Video resolution that the sink device supports */
  HDMI_VideoFormat3D_type   e3DFrameFormat;      /* 3D format this video resolution supports */ 
  HDMI_3D_EXT_DataType      e3DExtendedFormat;   /* Extended detail information for certain 3D formats (eg: Side by side half) */
  bool32                    bVideoOnly;          /* Only Video and no audio is supported by the VideoFormatType */
}HDMI_DispModeAttrType;


/*!
 * \b HDMI_DispModeListType
 *
 * Structure that encapsulates all the supported display modes by the HDMI sink device and the corresponding attributes
 */
typedef struct
{
  uint32                    uNumOfElements;                            /* Number of elements in the list */
  HDMI_DispModeAttrType     eDispModeList[HDMI_VIDEO_FORMAT_MAX];      /* List of the video resolutions and atributes that sink device supports */
}HDMI_DispModeListType;

/*!
* \b  HDMI_LipSyncInfoType
*
*  Defines the audio/video latency HDMI display
*/
typedef struct
{
  uint8            uVideoLatency;              /* Video latency in milliseconds for progressive video format */
  uint8            uInterlaceVideoLatency;     /* Video latency in milliseconds for interlaced video format */
  uint8            uAudioLatency;              /* Audio latency in milliseconds for progressive video format */
  uint8            uInterlaceAudioLatency;     /* Audio latency in milliseconds for interlaced video format */
} HDMI_LipSyncInfoType;


/*!
 * \b HDMI_EDID_DataBlockType
 *
 *  Contains all the data block offsets from the EDID ROM
 */
typedef struct
{
  /* Block 0 has fixed detail timing block offset, hence no need to include here for dynamic calculation */
  struct
  {
    uint32 uDetailTimingBlockOffset;      /* Offset of the 1st detail timing data block */
    uint32 uDataBlockLength;              /* Length of this data block */
  }sDetailTimingBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  /* There could be more than one video data block within the same CEA extension block */
  struct 
  {
    uint32 uVideoDataBlockOffset[HDMI_EDID_MAX_NUM_OF_VIDEO_DATA_BLOCKS];         /* Offset of the video data block */
    uint32 uDataBlockLength[HDMI_EDID_MAX_NUM_OF_VIDEO_DATA_BLOCKS];              /* Length of this data block */
  }sVideoDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct
  {
    uint32 uAudioDataBlockOffset;         /* Offset of the audio data block */
    uint32 uDataBlockLength;              /* Length of this data block */
  }sAudioDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct 
  {
    uint32 uVideoDataBlockOffset;         /* Offset of the Extended video data block */
    uint32 uDataBlockLength;              /* Length of this data block */
  }sExtVideoDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct
  {
    uint32 uAudioDataBlockOffset;         /* Offset of the audio data block */
    uint32 uDataBlockLength;              /* Length of this data block */
  }sExtAudioDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct
  {
    uint32                                  uVendorSpecificBlockOffset;     /* Offset of the vendor specific data block */
    uint32                                  uVendorSpecificBlockExtOffset;  /* Offset to the extension fields */
    uint32                                  uDataBlockLength;               /* Length of this data block */
    uint32                                  ePresetBitFlags;                /* Indicate all the present bits (Use HDMI_EDID_VendorBlockPresentBitType) */
    uint32                                  uIEEERegistrationID;            /* 24 bit IEEE registration ID */
    bool32                                  bAISupport;                     /* Supports ACP, ISRC1 and ISRC2 packets or not */
    bool32                                  bDVIDualSupport;                /* Supports DVI dual mode or not */
    uint16                                  uPhysicalAddress;               /* 16 bit source physical address for CEC purposes*/
    uint8                                   uSupportedDeepColorModeMask;    /* Deep color modes that are supported by the sinkk device */
    uint8                                   uMaxTMDSClock;                  /* Maximum TMDS clock supported by the sink device*/
    uint8                                   uVideoLatency;                  /* Video latency for progressive video */
    uint8                                   uAudioLatency;                  /* Audio latency for progressive video */
    uint8                                   uInterlacedVideoLatency;        /* Video latency for interlaced video */
    uint8                                   uInterlacedAudioLatency;        /* Audio latency for interlaced video */
    HDMI_EDID_VendorBlockImageSizeInfoType  eImageSizeInfo;                 /* Image size information */
    uint8                                   uSupportedContentFilterMask;    /* Supported filters for various content types (Use HDMI_EDID_VendorBlockContentFilterType)*/
    uint8                                   uVideo2DDescLen;                /* Stores the value of the HDMI VIC LEN */
    uint8                                   uVideo3DDescLen;                /* Stores the value of the HDMI 3D LEN */
    uint8                                   u3DMultiPresentFieldVal;        /* Stores the type of 3D descriptors inside the data block */
  }sVendorDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct
  {
    uint32    uSpeakerAllocBlockOffset;      /* Offset of the speaker allocation data block */
    uint32    uDataBlockLength;              /* Length of this data block */
  }sSpeakerDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];
  
  struct
  {
    uint32    uVideoCapDataBlockOffset;     /* Offset to the video capability data block */
    uint32    uDataBlockLength;             /* Length of this data block */
  }sVideoCapDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

  struct
  {
    uint32    uColorimetryDataBlockOffset;  /* Offset to the colorimetry data block */
    uint32    uDataBlockLength;             /* Length of this data block */
  }sColorimetryDataBlockDesc[HDMI_EDID_MAX_NUM_OF_CEA_EXTENSION_BLOCKS];

}HDMI_EDID_DataBlockType;


/*!
 * \b HDMI Event IDs
 *
 * HDMI Event IDs for callback purposes
 */
#define HDMI_DEVICE_EVENT_PLUGGED                     QDI_DEVICE_EVENT_HDMI_PLUGGED
#define HDMI_DEVICE_EVENT_UNPLUGGED                   QDI_DEVICE_EVENT_HDMI_UNPLUGGED
#define HDMI_DEVICE_EVENT_HDCP_AUTHENTICATE_SUCCES    QDI_DEVICE_EVENT_HDMI_HDCP_AUTHENTICATE_SUCCEED
#define HDMI_DEVICE_EVENT_HDCP_AUTHENTICATE_FAIL      QDI_DEVICE_EVENT_HDMI_HDCP_AUTHENTICATE_FAILED
#define HDMI_DEVICE_EVENT_MAX                         QDI_DEVICE_EVENT_MAX
#define HDMI_CbEventType                              QDI_Device_CbEventType

/*!
 * \b HDMI_AVIPacketParamType
 */
typedef struct 
{
  bool32                        bITContent;
  HDMI_AVI_ScanType             eScanInfo;
  HDMI_AVI_LetterBoxInfoType    sLetterBoxInfo;
  HDMI_AVI_ActiveFormatType     eActiveFormat;
  HDMI_AVI_ColorFormatType      eColorFormat;
  HDMI_VideoFormatType          eVideoIDCode;
  HDMI_AVI_Quantization         eQuantization;
  HDMI_AVI_Colorimetry          eColorimetry;
  HDMI_AVI_ScaleInfo            eScaleInfo;
} HDMI_AVIPacketParamType;

/*!
 * \b HDMI_VendorInfoFramePacketParamType
 *
 * The Vendor Specific info frame is for HDMI spec 1.4 and beyond specifically for 3D inclusion 
 */
typedef struct 
{
  bool32                      bVideoFormat3D;
  union 
  {
    HDMI_VideoFormatType     eVideoFormat2D;
    HDMI_VideoFormat3D_type  eVideoFormat3D;
  }videoFormat;
  HDMI_3D_EXT_DataType       eExtData3D;
}HDMI_VendorInfoFramePacketParamType;

/*!
 * \b HDMI_DeviceOpenParam
 *
 * Structure capturing device open parameters.
 */
typedef struct 
{
  HDMI_DeviceIDType       eDeviceID;           /* Logical device identifier */
  HDMI_OperationModeType  eMode;               /* FULL HDMI or DVI mode */
}HDMI_DeviceOpenParam;

/*!
 * \b HDMI_AudioParamType
 *
 * Structure capturing all audio related parameters.
 */
typedef struct
{
  bool32                         bAudioInfoPacket;     /* Enable/Disable Audio InfoFrame Packet */
  bool32                         bInhibitDownMix;      /* Enable/Disable Audio output down mixing */
  HDMI_AudioChannelType          eAudioNumOfChannel;   /* HDMI_Device_AudioChannelType */
  HDMI_AudioSampleRateType       eAudioSampleRate;     /* HDMI_Device_AudioSampleRateType */
  HDMI_AudioFormatType           eAudioFormat;         /* Audio format */
  HDMI_AudioSampleBitDepthType   eAudioSampleBitDepth; /* Audio bit depth */
  HDMI_AudioChannelAllocType     eChannelAllocCode;    /* Audio channel to speaker mapping */
  HDMI_AudioLevelShiftType       eLevelShiftValue;     /* Audio attenuation level */
}HDMI_AudioParamType;

/*!
 * \b HDMI_AudioModeInfoType
 *
 * Structure capturing all audio capabilities reported by the sink device 
 */
typedef struct
{
  uint32                         uAudioModeIndex;            /* Index to query each of the audio short descriptors reported by the sink device */
  HDMI_AudioFormatType           eAudioFormat;               /* Audio format this mode supports */
  uint16                         uAudioSampleRateMask;       /* All sampling rates this mode can support (Use HDMI_AudioSampleRateType) */
  uint16                         uAudioChannelMask;          /* Maximum number of audio channels this mode can support */
  uint32                         uAudioSpeakerLocationMask;  /* All speaker locations that the sink device can support (Use HDMI_AudioSpeakerLocationType) */
  union
  {
    uint32                       uAudioSampleBitDepthMask;   /* For LPCM audio format, this field indicates all the sample bit depths this mode can support (Use HDMI_AudioSampleBitDepthType) */
    uint32                       uAudioMaxBitRate;           /* Compressed audio, this field indicates the maximum bitrate this mode can support */
  }audioStreamInfo;
  uint32                         uReserved[7];               /* Reserved for future use */
}HDMI_AudioModeInfoType;


/*!
 * \b HDMI_VendorInfoType
 *
 * Structure captures vendor specific parameters 
 */
typedef struct
{
  bool32                                  bVideoOnly;                                           /* HDMI display support DVI mode (Video only) or HDMI mode (Audio and Video) */
  HDMI_LipSyncInfoType                    sLipSyncInfo;                                         /* Video and audio latency info */
  uint8                                   uSupportedDeepColorModeMask;                             /* Pixel depth for each deep color mode HDMI display can support (Use QDI_HDMI_DeepColorModeType) */
  uint8                                   uSupportedContentFilterMask;                             /* Sink supports various content filters (Use QDI_HDMI_ContentFilterType) */
  uint16                                  uPhysicalAddr;                                        /* 16 bit physical address of the source device, upper 16 bit is ignored. */
  bool32                                  bAISupport;                                           /* ACP, ISRC1 and ISRC2 packets supported or not */
  bool32                                  bDVIDualSupport;                                      /* Sink supports DVI Dual mode */
  uint32                                  uMaxScreenWidthInMillimeter;                          /* Maximum screen width reported by the Sink in millimeters */
  uint32                                  uMaxScreenHeightInMillimeter;                         /* Maximum screen height reported by the Sink in millimeters */
  uint8                                   uManufacturerId[2];                                   /* Manufacturer ID : using EISA ID,  uManufacturerId[0] is low byte */
  uint16                                  uProductId;                                           /* Product code,  used to differentiate between differnrt models from the manufacturer */
  uint8                                   uNameDescriptionLength;                               /* Sink name description length */                                       
  uint8                                   uNameDescription[13];                                 /* Sink name description*/
  uint32                                  uReserved[2];                                         /* Reserved for future use */
}HDMI_VendorInfoType;

/*!
 * \b HDMI_EdidInfoType
 *
 * Structure captures RAW EDID information
 */
typedef struct
{
  uint32                                  uDataLength;                                     /* Length of the EDID data block */
  uint8*                                  pDataBuf;                                        /* Data buffer storing the raw EDID data */
}HDMI_EdidInfoType;

/*!
 * \b HDMI_UserCtxType
 *
 * User context attributes.
 */
typedef struct 
{
  HDMI_DeviceIDType                    eDeviceID;                            /* Logical device identifier */
  HDMI_PowerModeType                   ePowerMode;                           /* Power ON or OFF */
  HDMI_OperationModeType               eMode;                                /* Full HDMI or DVI mode */
  uint32                               uDispModeIndex;                       /* Index to the list of supported display mode */
  HDMI_AudioParamType                  sAudioParams;                         /* Configure audio parameters to sink device */
  HDMI_AVIPacketParamType              sAVIPacketParams;                     /* AVI packet information */
  HDMI_VendorInfoFramePacketParamType  sVSPacketParams;                      /* Vendor specific info packet */
  uint32                               uDirtyBits;                           /* Marker indicate which property is dirty */
  bool32                               bContentProtectionEnable;             /* Enable/Disable content protection */
}HDMI_UserCtxType;


/*!
 * \b HDMI_DeviceDataType
 *
 * Structure capturing the logical device attributes.
 */
typedef struct 
{
  HDMI_UserCtxType          sDeviceUserCtx[MAX_NUM_HDMI_DEVICE_USER_CONTEXT];         /* User context that is attached to this device */
  uint32                    uNumDeviceUserCtx;                                        /* Number of user context attached */
  bool32                    bInitialized;                                             /* Indicates if the device is initialized */
  bool32                    bPhyInitialized;                                          /* Indicates whether the PHY interface is initialized */
  bool32                    bLastConnectionStatus;                                    /* Indicates the last known connection status of the HDMI device */
  bool32                    bContentProtectionEnable;                                 /* Indicate whether user has configured to enable authentication process or not */
  bool32                    bLastAuthenticationStatus;                                /* Tracks the last authentication status */
  bool32                    bEDIDParserComplete;                                      /* Tracks whether EDID parsing has completed */
  bool32                    bAudioSupport;                                            /* Tracks whether basic audio is supported in EDID */
  uint32                    uEDIDDataLength;                                          /* Length of EDID data.*/
  uint32                    uActiveModeIndex;                                         /* The current active video format index in the mode list*/  
  HDMI_DispModeListType     sDispModeList;                                            /* Holds the list of supported HDMI modes and attributes*/  
  uint8                     auEDIDCache[HDMI_EDID_MAX_BUF_SIZE];                      /* Cached EDID Data*/
  HDMI_EDID_DataBlockType   sDataBlockDesc;                                           /* Data block offsets  from EDID ROM*/
}HDMI_DeviceDataType;

/* -----------------------------------------------------------------------
** Union Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HDMI_Device_PropertyParamsType
 *
 * Structure capturing all the property types.
 */
typedef union
{
  HDMI_PowerModeType                    ePowerMode;                 /* HDMI_PowerModeType */
  HDMI_OperationModeType                eMode;                      /* HDMI_OperationModeType */
  HDMI_VideoFormatType                  eVideoResolution;           /* HDMI_VideoFormatType */
  HDMI_AudioParamType                   sAudioParams;               /* HDMI_AudioParamType */
  HDMI_DispModeInfoType                 sDispModeInfo;              /* HDMI_DispModeInfoType */
  HDMI_AVIPacketParamType               sAVIPacketParams;           /* AVI packet information */
  HDMI_VendorInfoFramePacketParamType   sVSPacketParams;            /* Vendor specific info packet */
  HDMI_AudioModeInfoType                sAudioModeInfo;             /* Extract audio capabilities from sink device */
  HDMI_VendorInfoType                   sVendorInfo;                /* Extract vendor specific info */
  bool32                                bContentProtectionEnable;   /* HDMI HDCP enable or not */
  HDMI_EdidInfoType                     sEdidInfo;                  /* Extract HDMI RAW EDID Info */
  bool32                                bConnected;                 /* HDMI connection status */
} HDMI_Device_PropertyParamsType;





/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HDMI_Init()
*/
/*!
* \brief
*   The \b HDMI_Init function initializes a device before it can be used.  
*   It includes initialization of any state variables and OS specific services.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Init(void);
/****************************************************************************
*
** FUNCTION: HDMI_DeInit()
*/
/*!
* \brief
*   The \b HDMI_DeInit function de-initializes the HDMI TX core driver.  
*   It closes all the resources allocated by the driver and reset to back to default
*   value for the state variables.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_DeInit(void);
/****************************************************************************
*
** FUNCTION: HDMI_Device_Open()
*/
/*!
* \brief
*   The \b HDMI_Device_Open function initializes a device before it can be used.  
*   An error will be returned if a new handle can not be obtained.
*
* \param [out] phDevice            - Pointer to handle for initialized device.
* \param [in]  pDeviceOpenParams   - Parameters list for initialization.
* \param [in]  flags               - Option to be requested ( Set to 0 for default call ).
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Device_Open(void **phDevice, HDMI_DeviceOpenParam *pDeviceOpenParams, uint32 flags);
/****************************************************************************
*
** FUNCTION: HDMI_Device_Close()
*/
/*!
* \brief
*   The \b HDMI_Device_Close function is to be called upon deinitialization.
*   A flag can be passed as a parameter to specify an option to enable.
*
* \param [in]  hDevice         - Handle for device to terminated
* \param [in]  flags           - Option to be requested ( Set to 0 for default call )   
*
* \retval MDP_Status
*
****************************************************************************/
void HDMI_Device_Close(void *hDevice, uint32 flags);
/****************************************************************************
*
** FUNCTION: HDMI_Device_SetProperty()
*/
/*!
* \brief
*   The \b HDMI_Device_SetProperty function sets one property for the device specified by hDevice
*   The newly changed property will be effective only if HDMI_Device_Commit
*   is invoked.
*
* \param [in]    hDevice           Handle for device whose properties are being set.
* \param [in]    ePropertyType     Property that will be applied to the device.  
* \param [in]    pPropertyData     Contains the new parameters of the property that will be applied.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Device_SetProperty(void *phDevice, HDMI_Device_PropertyType eProperty, HDMI_Device_PropertyParamsType *pPropertyData);
/****************************************************************************
*
** FUNCTION: HDMI_Device_GetProperty()
*/
/*!
* \brief
*   The \b HDMI_Device_GetProperty function gets one property for the device 
*   specified by hDevice
*
* \param [in]    hDevice           Handle for device whose properties are being set.
* \param [in]    ePropertyType     Property that will be applied to the device.  
* \param [in]    pPropertyData     Contains the new parameters of the property that will be applied.
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Device_GetProperty(void *phDevice, HDMI_Device_PropertyType eProperty, HDMI_Device_PropertyParamsType *pPropertyData);
/****************************************************************************
*
** FUNCTION: HDMI_Device_Commit()
*/
/*!
* \brief
*   The \b HDMI_Device_Commit function enables the changes made by xxx_SetProperty functions
*
* \param [in]   hDevice       - Device entity where the "commit" will occur.
* \param [in]   flags         - Option to be requested ( Set to 0 for default call )   
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_Device_Commit(void *phDevice, uint32 flags);

#endif /* HDMIHOST_H */


