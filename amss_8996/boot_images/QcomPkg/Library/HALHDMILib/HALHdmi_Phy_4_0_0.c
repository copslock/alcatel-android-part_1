
/*
===========================================================================

FILE:         HALHdmiPhy_4_0_0.c


DESCRIPTION:  
  This is the interface to be used to access Qualcomm's HDMI TX Core 
  PHY interface. The intended audience for this source file HDMI 
  host driver layer which incorporates this hardware interface with 
  HDMI TX Core. This interface is not intended or recommended for direct 
  usage by any application.

===========================================================================

                             Edit History

  $$

when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
  Copyright (c) 2010-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiPhy.h"
#include "HALHdmiCommon.h"
#include "HALHdmiPhySetting_4_0_0.h"
#include "HALHdmiPhy_4_0_0.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* timeout counter values used in iterations of polling PLL & Phy ready status; 
 * each polling iteration contains a delay of 1ms;                             */
#define HAL_HDMI_PLL_READY_TIMEOUT      500   /* ~500ms */
#define HAL_HDMI_PHY_READY_TIMEOUT      500   /* ~500ms */

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */

/* In Aragorn(8974), PLL reference clock frequency is 19.2 MHz.            */


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_Phy_Pll_Config()
*/
/*!
* \DESCRIPTION
*
* \param [in]   pRegSetting - pointer to PLL & Phy register settings;
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void HAL_HDMI_Phy_Pll_Config(uint8 *pRegSetting)
{ 
   /*
    * IMPORTANT: The following programming sequence was provided and verfied by HW team.
    *            Please do NOT make any changes on this function.
    */
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_GLB_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_REFCLK_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_VCOLPF_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LPFR_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LPFC1_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LPFC2_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_SDM_CFG0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_SDM_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_SDM_CFG2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_SDM_CFG3_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_SDM_CFG4_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LKDET_CFG0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LKDET_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_LKDET_CFG2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_POSTDIV1_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_POSTDIV2_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_POSTDIV3_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG8_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG9_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG10_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG11_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTRL0_OUT(*pRegSetting++);
   HAL_Stall_Us(250);    /* delay ~250us */
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTRL0_OUT(*pRegSetting++);
   HAL_Stall_Us(500);   /* delay ~500us */
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(*pRegSetting++);
   HAL_Stall_Us(500);   /* delay ~500us */
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(*pRegSetting++);
   HAL_Stall_Us(500);    /* delay ~500us */
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTRL1_OUT(*pRegSetting++);
   HAL_Stall_Us(500);    /* delay ~500us */
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_ANA_CFG0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_ANA_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_ANA_CFG2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_ANA_CFG3_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_VREG_CFG_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_DCC_CFG0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_DCC_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG3_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_PATN0_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_PATN1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_PATN2_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_PATN3_OUT(*pRegSetting++);
   HAL_Stall_Us(200);   /* delay ~200us */
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_CFG1_OUT(*pRegSetting++);
   HWIO_MMSS_HDMI_PHY_HDMI_PHY_BIST_CFG0_OUT(*pRegSetting);

}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_PowerCtrl()
*/
/*!
* \DESCRIPTION
*     power up/down the PLL and the PHY.
*
* \param [in]   bPllPowerUp - TRUE: power up; FALSE: power down
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void  HAL_HDMI_PhyPll_PowerCtrl( bool32   bPllPowerUp )
{
   if (bPllPowerUp == TRUE)
   {
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_CAL_CFG2_OUT(0x01);

      /* power up phy */
      HWIO_MMSS_HDMI_PHY_HDMI_PHY_GLB_CFG_OUT(0x81);         
      /* power up power gen and drivers */
      HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTRL0_OUT(0x00);

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power up PLL */
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(0x01);

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power up LDO */
	   HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(0x03);      

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power up powergen */
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(0x0F);      

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     
   }
   else
   {
      uint32   uRegVal;

      uRegVal = HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_IN;

      /* power down powergen */
      uRegVal = HWIO_OUT_FLD(uRegVal, HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG, PLL_GLB_CFG, 0);
      uRegVal = HWIO_OUT_FLD(uRegVal, HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG, PLL_PWRGEN_PWRDN_B, 0);
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(uRegVal);

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power down LDO */
      uRegVal = HWIO_OUT_FLD(uRegVal, HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG, PLL_LDO_PWRDN_B, 0);
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(uRegVal);

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power down PLL */
      uRegVal = HWIO_OUT_FLD(uRegVal, HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG, PLL_PWRDN_B, 0);
      HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_GLB_CFG_OUT(uRegVal);

      /* delay ~50Us */
      HAL_Stall_Us(50);                                     

      /* power down power gen and drivers */
      HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTRL0_OUT(0x5F);
      /* power down phy */
      HWIO_MMSS_HDMI_PHY_HDMI_PHY_GLB_CFG_OUT(0x01);         

   }

   return;
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_Phy_PollPllReady()
*/
/*!
* \DESCRIPTION
*     poll if PLL is ready.
*
* \param [in]   None
* \param [out]  None
*
* \retval HAL_HDMI_StatusType
*
**********************************************************************************************/
static HAL_HDMI_StatusType HAL_HDMI_Phy_PollPllReady(void)
{
   HAL_HDMI_StatusType   eStatus  = HAL_HDMI_SUCCESS;
   uint32                uTimeout = HAL_HDMI_PLL_READY_TIMEOUT;
   uint32                uStatus;

   uStatus = HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_STATUS_IN & HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_STATUS_PLL_RDY_BMSK;
   while ((!uStatus) && (uTimeout))
   {
      HAL_Stall_Us(1000);                                    /* delay ~1ms */
      uStatus = HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_STATUS_IN & HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_STATUS_PLL_RDY_BMSK;
      uTimeout--;
   }

   if (HWIO_MMSS_HDMI_PHY_PLL_UNIPHY_PLL_STATUS_PLL_RDY_BMSK != uStatus)
   {
      eStatus = HAL_HDMI_FAILED_TIMEOUT;
   }

   return eStatus;
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_Phy_PollPhyReady()
*/
/*!
* \DESCRIPTION
*     poll if Phy is ready.
*
* \param [in]   None
* \param [out]  None
*
* \retval HAL_HDMI_StatusType
*
**********************************************************************************************/
static HAL_HDMI_StatusType HAL_HDMI_Phy_PollPhyReady(void)
{
   HAL_HDMI_StatusType   eStatus  = HAL_HDMI_SUCCESS;
   uint32                uTimeout = HAL_HDMI_PHY_READY_TIMEOUT;
   uint32                uStatus;

   uStatus = HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_IN & HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_RDY_BMSK;

   while ((!uStatus) && (uTimeout))
   {
      HAL_Stall_Us(1000);                                    /* delay ~1ms */
      uStatus = HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_IN & HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_RDY_BMSK;
      uTimeout--;
   }

   if (HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_RDY_BMSK != uStatus)
   {
      eStatus = HAL_HDMI_FAILED_TIMEOUT;
   }

   return eStatus;
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_Reset()
*/
/*!
* \DESCRIPTION
*     this function is from the version #1 of this file;
*     the original funciton name was setup_hdmi_phy_reset().
*
* \param [in]   None
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void HAL_HDMI_PhyPll_Reset(void) 
{

   uint32   phy_reset_polarity = 0x0;
   uint32   pll_reset_polarity = 0x0;
   uint32   uHdmiPhyCtrl = 0;

   uHdmiPhyCtrl = HWIO_MMSS_HDMI_PHY_CTRL_IN;
    
   phy_reset_polarity = (uHdmiPhyCtrl & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK) >> HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_SHFT;  // Check if PHY reset polarity
   pll_reset_polarity = (uHdmiPhyCtrl & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_BMSK) >> HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_SHFT;  // Check if PHY reset polarity

   if (phy_reset_polarity == 0)
   {
      uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK;
   }
   else
   {
      uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK);
   }
    
   if (pll_reset_polarity == 0)
   {
      uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_BMSK;
   }
   else
   {
      uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_BMSK);
   }    
    
   HWIO_MMSS_HDMI_PHY_CTRL_OUT(uHdmiPhyCtrl);
   HAL_Stall_Us(100);

   if (phy_reset_polarity == 0)
   {
      uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK);
   }
   else
   {
      uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK;
   }

   if (pll_reset_polarity == 0)
   {
      uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_BMSK);
   }
   else
   {
      uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_PLL_BMSK;
   }  

   HWIO_MMSS_HDMI_PHY_CTRL_OUT(uHdmiPhyCtrl);
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_Setup()
*/
/*!
* \DESCRIPTION
*     set up HDMI PLL and Phy.
*
* \param [in]   pRegSetting - pointer to PLL & Phy register settings;
*
* \retval HAL_HDMI_StatusType 
*
**********************************************************************************************/
static HAL_HDMI_StatusType HAL_HDMI_PhyPll_Setup(uint8 *pRegSetting)
{
   HAL_HDMI_StatusType   eRetStatus = HAL_HDMI_SUCCESS;
   HAL_HDMI_StatusType   eStatus;

   /* enable HDMI block */
   HWIO_MMSS_HDMI_CTRL_OUT(0x1);

   /* SW reset the PHY interface */
   HAL_HDMI_PhyPll_Reset();

   /* program Phy & PLL */
   HAL_HDMI_Phy_Pll_Config(pRegSetting);
  
   /* poll the ready status of PLL */
   if (HAL_HDMI_SUCCESS != (eStatus =  HAL_HDMI_Phy_PollPllReady()))
   {
      eRetStatus = eStatus;
   }

    /* poll the ready status of Phy */
   if (HAL_HDMI_SUCCESS != (eStatus =  HAL_HDMI_Phy_PollPhyReady()))
   {
      eRetStatus = eStatus;
   }
   
   return eRetStatus;
}


/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_HDMI_4_0_0_PHY_Config()
*/
/*!
* \DESCRIPTION
*   The HAL_HDMI_PHY_Config function to config Hdmi phy and pll freg
*
* \param [in]   pHdmiPllConfigInfo  - Hdmi Phy Pll config info
*
* \retval boolean
*
****************************************************************************/
bool32 HAL_HDMI_4_0_0_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo)
{
  bool32      bStatus    = TRUE;
  uint32      uFreqIndex = 0xFFFFFFFF;
  uint32      i;

  if (0 != pHdmiPllConfigInfo->hdmi_pll_freq)
  {
    for (i=0; i<NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED; i++)
    {
      if (uHdmiPllFreqTable[i] == pHdmiPllConfigInfo->hdmi_pll_freq)
      {
        uFreqIndex = i;
        break;
      }
    }
  }

  if (NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED > uFreqIndex)
  {
    /* program HDMI PLL&Phy */
    bStatus = (HAL_HDMI_SUCCESS == HAL_HDMI_PhyPll_Setup(&uHdmiPhyPllRegSetting[uFreqIndex][0]));
  }
  else
  {
    /* power down pll and phy when the requested fequency is 0 or not supported */
    HAL_HDMI_PhyPll_PowerCtrl(FALSE);
    bStatus = FALSE;
  }

  return bStatus;
}



#ifdef __cplusplus
}
#endif

