
/*
===========================================================================

FILE:         HALHdmiPhy_3_3_0.c


DESCRIPTION:  
  This is the interface to be used to access Qualcomm's HDMI TX Core 
  PHY interface. The intended audience for this source file HDMI 
  host driver layer which incorporates this hardware interface with 
  HDMI TX Core. This interface is not intended or recommended for direct 
  usage by any application.

===========================================================================

                             Edit History

  $$

when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiPhy.h"
#include "HALHdmiCommon.h"
#include "HALHdmiPhySetting_3_3_0.h"
#include "HALHdmiPhy_3_3_0.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* timeout counter values used in iterations of polling PLL & Phy ready status; 
 * each polling iteration contains a delay of 1ms;                             */
#define HAL_HDMI_PLL_READY_TIMEOUT      500   /* ~500ms */
#define HAL_HDMI_PHY_READY_TIMEOUT      500   /* ~500ms */

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Private Functions
** ----------------------------------------------------------------------- */

/* In Elessar(8994), PLL reference clock frequency is 19.2 MHz.            */


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_Phy_Pll_Config()
*/
/*!
* \DESCRIPTION
*    Configure HDMI PHY/PLL registers for specified PLL frequency
*
* \param [in]   pRegSetting - pointer to PLL & Phy register settings;
* \param [in]   uPllFreq - PLL frequency to config
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void HAL_HDMI_Phy_Pll_Config(uint8 *pRegSetting, uint32 uPllFreq)
{
  /*
  * IMPORTANT: The following programming sequence was provided and verfied by HW team.
  *            Please do NOT make any changes on this function.
  */
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTL_OUT(0x0);
  //HAL_Stall_Us(250000);  /* delay 250ms */

  HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTL_OUT(0x1F);
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x01);
  HAL_Stall_Us(5000);  /* delay 5ms */
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x07);
  HAL_Stall_Us(5000);  /* delay 5ms */
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x05);
  HAL_Stall_Us(5000);  /* delay 5ms */

  HWIO_MMSS_HDMI_PHY_QSERDES_COM_SYS_CLK_CTRL_OUT(0x42);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_VCOTAIL_EN_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_CMN_MODE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_IE_TRIM_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_IP_TRIM_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_CNTRL_OUT(0x07);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_PHSEL_CONTROL_OUT(0x04);

  if (uPllFreq > TO_MHZ(250))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_COM_IPTAT_TRIM_VCCA_TX_SEL_OUT(0x80);
  }
  else if (uPllFreq > TO_MHZ(74))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_COM_IPTAT_TRIM_VCCA_TX_SEL_OUT(0xA0);
  }
  else
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_COM_IPTAT_TRIM_VCCA_TX_SEL_OUT(0xC0);
  }

  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_PHSEL_DC_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_CORE_CLK_IN_SYNC_SEL_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_BKG_KVCO_CAL_EN_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_OUT(0x0F);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_ATB_SEL1_OUT(0x01);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_ATB_SEL2_OUT(0x01);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_SYSCLK_EN_SEL_TXBAND_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_SYSCLK_EN_SEL_TXBAND]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_VREF_CFG1_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_VREF_CFG2_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_BGTC_OUT(0xFF);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_TEST_UPDN_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_VCO_TUNE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_AMP_OS_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_SSC_EN_CENTER_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_UP_OUT(0x00);

  if (uPllFreq > TO_MHZ(74))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_DN_OUT(0x3F);
  }
  else
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_DN_OUT(0x00);
  }

  HWIO_MMSS_HDMI_PHY_QSERDES_COM_KVCO_CODE_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_KVCO_CODE]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_KVCO_COUNT1_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_KVCO_COUNT1]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DIV_REF1_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DIV_REF1]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DIV_REF2_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DIV_REF2]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_KVCO_CAL_CNTRL_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_KVCO_CAL_CNTRL]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_VREF_CFG3_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_VREF_CFG3]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_VREF_CFG4_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_VREF_CFG4]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_VREF_CFG5_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_VREF_CFG5]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESETSM_CNTRL_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_RESETSM_CNTRL]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_CAL_CSR_OUT(0x77);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_TRIM_EN_VCOCALDONE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_RXTXEPCLK_EN_OUT(0x0C);

  // PLL loop bandwidth
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_IP_SETI_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLL_IP_SETI]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_CP_SETI_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLL_CP_SETI]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_IP_SETP_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLL_IP_SETP]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_CP_SETP_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLL_CP_SETP]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_CRCTRL_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLL_CRCTRL]);

  // PLL Calibration
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DIV_FRAC_START1_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DIV_FRAC_START1]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DIV_FRAC_START2_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DIV_FRAC_START2]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DIV_FRAC_START3_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DIV_FRAC_START3]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DEC_START1_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DEC_START1]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_DEC_START2_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_DEC_START2]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLLLOCK_CMP1_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLLLOCK_CMP1]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLLLOCK_CMP2_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLLLOCK_CMP2]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLLLOCK_CMP3_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLLLOCK_CMP3]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLLLOCK_CMP_EN_OUT(pRegSetting[HDMI_PLL_CALC_QSERDES_COM_PLLLOCK_CMP_EN]);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_PLL_CNTRL_OUT(0x07);

  // Resistor calibration linear search
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_START_SEG1_OUT(0x60);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_CODE_START_SEG2_OUT(0x60);
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RES_TRIM_CONTROL_OUT(0x01);

  // Reset state machine control
  HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESETSM_CNTRL2_OUT(0x07);
  //HAL_Stall_Us(100000);  /* delay 100ms */

  // TX lanes (transceivers) power-up sequence
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_MODE_OUT(pRegSetting[HDMI_PLL_CALC_HDMI_PHY_MODE]);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_CLKBUF_ENABLE_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L1_CLKBUF_ENABLE_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_CLKBUF_ENABLE_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L3_CLKBUF_ENABLE_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_TRAN_DRVR_EMP_EN_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L1_TRAN_DRVR_EMP_EN_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_TRAN_DRVR_EMP_EN_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L3_TRAN_DRVR_EMP_EN_OUT(0x03);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OUT(0x6F);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L1_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OUT(0x6F);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OUT(0x6F);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L3_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OUT(0x6F);

  if (uPllFreq > TO_MHZ(250))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_TX_EMP_POST1_LVL_OUT(0x2F);
    HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG0_OUT(0xAF);
  }
  else if (uPllFreq > TO_MHZ(74))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_TX_EMP_POST1_LVL_OUT(0x2F);
    HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG0_OUT(0xAF);
  }
  else
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_TX_EMP_POST1_LVL_OUT(0x20);
    HWIO_MMSS_HDMI_PHY_HDMI_PHY_TXCAL_CFG0_OUT(0xA1);
  }

  if (uPllFreq > TO_MHZ(250))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL1_OUT(0x08);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL1_OUT(0x09);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL5_OUT(0x00);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL6_OUT(0x00);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL5_OUT(0x00);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL6_OUT(0x00);
  }
  else if (uPllFreq > TO_MHZ(74))
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL1_OUT(0x08);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL1_OUT(0x09);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL5_OUT(0xA0);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL6_OUT(0x01);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL5_OUT(0xA0);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL6_OUT(0x01);
  }
  else
  {
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL1_OUT(0x02);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL1_OUT(0x03);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL5_OUT(0xA0);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_VMODE_CTRL6_OUT(0x01);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL5_OUT(0x80);
    HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_VMODE_CTRL6_OUT(0x00);
  }

  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_PARRATE_REC_DETECT_IDLE_EN_OUT(0x40);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L0_TX_INTERFACE_MODE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L1_PARRATE_REC_DETECT_IDLE_EN_OUT(0x40);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L1_TX_INTERFACE_MODE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_PARRATE_REC_DETECT_IDLE_EN_OUT(0x40);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L2_TX_INTERFACE_MODE_OUT(0x00);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L3_PARRATE_REC_DETECT_IDLE_EN_OUT(0x40);
  HWIO_MMSS_HDMI_PHY_QSERDES_TX_L3_TX_INTERFACE_MODE_OUT(0x00);

  // PCS settings
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x00);
  //HAL_Stall_Us(100000);  /* delay 100ms */
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x01);
  //HAL_Stall_Us(100000);  /* delay 100ms */
  HWIO_MMSS_HDMI_PHY_HDMI_PHY_CFG_OUT(0x09);
  //HAL_Stall_Us(100000);  /* delay 100ms */
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_PowerCtrl()
*/
/*!
* \DESCRIPTION
*     Power up/down the PLL and the PHY.
*
* \param [in]   bPllPowerUp - TRUE: power up; FALSE: power down
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void HAL_HDMI_PhyPll_PowerCtrl(bool32 bPllPowerUp)
{
  if (bPllPowerUp == TRUE)
  {
    HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTL_OUT(0x1F);
  }
  else
  {
    HWIO_MMSS_HDMI_PHY_HDMI_PHY_PD_CTL_OUT(0x00);
  }

  return;
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_Phy_PollPhyReady()
*/
/*!
* \DESCRIPTION
*     poll if PHY and PLL are ready.
*
* \param [in]   None
* \param [out]  None
*
* \retval HAL_HDMI_StatusType
*
**********************************************************************************************/
static HAL_HDMI_StatusType HAL_HDMI_Phy_PollPhyPllReady(void)
{
  HAL_HDMI_StatusType   eStatus = HAL_HDMI_SUCCESS;
  uint32                uTimeout = HAL_HDMI_PHY_READY_TIMEOUT;
  uint32                uStatus = 0;

  /* Poll PLL lock*/
  uStatus = HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESET_SM_IN & 0x20;
  while ((!uStatus) && (uTimeout))
  {
    HAL_Stall_Us(1000);  /* delay ~1ms */
    uStatus = HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESET_SM_IN & 0x20;
    uTimeout--;
  }

  if (0x20 != uStatus)
  {
    eStatus = HAL_HDMI_FAILED_TIMEOUT;
  }
  else
  {
    /* Poll C_READY */
    uTimeout = HAL_HDMI_PHY_READY_TIMEOUT;
    uStatus = HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESET_SM_IN & 0x40;
    while ((!uStatus) && (uTimeout))
    {
      HAL_Stall_Us(1000);  /* delay ~1ms */
      uStatus = HWIO_MMSS_HDMI_PHY_QSERDES_COM_RESET_SM_IN & 0x40;
      uTimeout--;
    }

    if (0x40 != uStatus)
    {
      eStatus = HAL_HDMI_FAILED_TIMEOUT;
    }
    else
    {
      /* Poll PHY_READY */
      uTimeout = HAL_HDMI_PHY_READY_TIMEOUT;
      uStatus = HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_IN & HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_READY_BMSK;
      while ((!uStatus) && (uTimeout))
      {
        HAL_Stall_Us(1000);  /* delay ~1ms */
        uStatus = HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_IN & HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_READY_BMSK;
        uTimeout--;
      }

      if (HWIO_MMSS_HDMI_PHY_HDMI_PHY_STATUS_PHY_READY_BMSK != uStatus)
      {
        eStatus = HAL_HDMI_FAILED_TIMEOUT;
      }
    }
  }

  return eStatus;
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_Reset()
*/
/*!
* \DESCRIPTION
*     Reset PHY and PLL.
*
* \param [in]   None
* \param [out]  None
*
* \retval None
*
**********************************************************************************************/
static void HAL_HDMI_PhyPll_Reset(void)
{
  uint32   phy_reset_polarity = 0x0;
  uint32   uHdmiPhyCtrl = 0;

  uHdmiPhyCtrl = HWIO_MMSS_HDMI_PHY_CTRL_IN;

  phy_reset_polarity = (uHdmiPhyCtrl & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_POL_BMSK) >> HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_POL_SHFT;  // Check if PHY reset polarity

  if (phy_reset_polarity == 0)
  {
    /* Active high */
    uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK;
  }
  else
  {
    /* Active low */
    uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK);
  }

  HWIO_MMSS_HDMI_PHY_CTRL_OUT(uHdmiPhyCtrl);

  HAL_Stall_Us(100);

  if (phy_reset_polarity == 0)
  {
    /* Active high */
    uHdmiPhyCtrl &= ~((1 << HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_SHFT) & HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK);
  }
  else
  {
    /* Active low */
    uHdmiPhyCtrl |= HWIO_MMSS_HDMI_PHY_CTRL_SW_RESET_BMSK;
  }

  HWIO_MMSS_HDMI_PHY_CTRL_OUT(uHdmiPhyCtrl);
}


/*********************************************************************************************
*
** FUNCTION: HAL_HDMI_PhyPll_Setup()
*/
/*!
* \DESCRIPTION
*     set up HDMI PLL and Phy.
*
* \param [in]   pRegSetting - pointer to PLL & Phy register settings;
* \param [in]   uPllFreq - PLL frequency to set
*
* \retval HAL_HDMI_StatusType
*
**********************************************************************************************/
static HAL_HDMI_StatusType HAL_HDMI_PhyPll_Setup(uint8 *pRegSetting, uint32 uPllFreq)
{
  HAL_HDMI_StatusType   eRetStatus = HAL_HDMI_SUCCESS;

  /* enable HDMI block */
  //HWIO_MMSS_HDMI_CTRL_OUT(0x1);

  /* SW reset the PHY interface */
  HAL_HDMI_PhyPll_Reset();

  /* program Phy & PLL */
  HAL_HDMI_Phy_Pll_Config(pRegSetting, uPllFreq);

  /* poll the ready status of Phy/Pll */
  eRetStatus = HAL_HDMI_Phy_PollPhyPllReady();

  return eRetStatus;
}


/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_HDMI_3_3_0_PHY_Config()
*/
/*!
* \DESCRIPTION
*   The HAL_HDMI_PHY_Config function to config Hdmi phy and pll freg
*
* \param [in]   pHdmiPllConfigInfo  - Hdmi Phy Pll config info
*
* \retval boolean
*
****************************************************************************/
bool32 HAL_HDMI_3_3_0_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo)
{
  bool32      bStatus    = TRUE;
  uint32      uFreqIndex = 0xFFFFFFFF;
  uint32      i;

  if (0 != pHdmiPllConfigInfo->hdmi_pll_freq)
  {
    for (i=0; i<NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED; i++)
    {
      if (uHdmiPllFreqTable[i] == pHdmiPllConfigInfo->hdmi_pll_freq)
      {
        uFreqIndex = i;
        break;
      }
    }
  }

  if (NUMBER_OF_HDMI_PLL_FREQ_SUPPORTED > uFreqIndex)
  {
    /* program HDMI PLL&Phy */
    bStatus = (HAL_HDMI_SUCCESS == HAL_HDMI_PhyPll_Setup(&uHdmiPhyPllRegSetting[uFreqIndex][0], uHdmiPllFreqTable[uFreqIndex]));
  }
  else
  {
    /* power down pll and phy when the requested fequency is 0 or not supported */
    HAL_HDMI_PhyPll_PowerCtrl(FALSE);
    bStatus = FALSE;
  }

  return bStatus;
}



#ifdef __cplusplus
}
#endif

