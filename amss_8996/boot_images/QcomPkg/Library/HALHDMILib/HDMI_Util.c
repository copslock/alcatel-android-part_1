
/*
===========================================================================

FILE:         HDMI_Util.c

DESCRIPTION:  
  This is the interface that provides helper functions to handle non HDMI specific
  configurations.This interface is not intended or recommended for direct 
  usage by any application.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Protocol/EFITlmm.h>

#include "HDMI_Util.h"
#include "DDITlmm.h"
#include "MDPPlatformLib.h"

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HDMI_EnableTLMM_DDC_HPD()
*/
/*!
* \brief
*   The \b HDMI_EnableTLMM_DDC_HPD Initializes the GPIO configuration for the 
*   DDC lines and enables the HPD GPIO configurations.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_EnableTLMM_DDC_HPD(void)
{
  EFI_STATUS           eStatus          = EFI_SUCCESS;
  EFI_TLMM_PROTOCOL   *TLMMProtocol; 
  MDPPlatformParams    ePlatformParam;
  MDP_Status           eRetStatus       = MDP_STATUS_OK;

  SetMem(&ePlatformParam, sizeof(MDPPlatformParams), 0x0); 

  if (MDP_STATUS_OK != (eRetStatus = MDPPlatformConfigure(MDP_DISPLAY_EXTERNAL, MDPPLATFORM_CONFIG_GETPANELCONFIG, &ePlatformParam)))
  {
      // Do nothing
  }
  else
  {
     eStatus = gBS->LocateProtocol( &gEfiTLMMProtocolGuid, NULL, (void**)&TLMMProtocol);
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uHPDGpio, 1, GPIO_INPUT,  GPIO_PULL_DOWN, GPIO_16MA),  DAL_TLMM_GPIO_ENABLE);
     }
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uDDDClkGpio, 1, GPIO_OUTPUT, GPIO_PULL_UP, GPIO_16MA),  DAL_TLMM_GPIO_ENABLE);
     }
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uDDCDataGpio, 1, GPIO_OUTPUT, GPIO_PULL_UP, GPIO_16MA), DAL_TLMM_GPIO_ENABLE);
     }

     if (EFI_SUCCESS != eStatus)
     {
       eRetStatus = MDP_STATUS_FAILED;
     }

  }

  return eRetStatus;
}

/****************************************************************************
*
** FUNCTION: HDMI_DisableTLMM_DDC()
*/
/*!
* \brief
*   The \b HDMI_DisableTLMM_DDC disable the GPIO configuration for the 
*   DDC lines.
**
* \retval MDP_Status
*
****************************************************************************/
MDP_Status HDMI_DisableTLMM_DDC_HPD(void)
{
  EFI_STATUS                 eStatus       = EFI_SUCCESS;
  EFI_TLMM_PROTOCOL         *TLMMProtocol; 
  MDPPlatformParams          ePlatformParam;
  MDP_Status                 eRetStatus    = MDP_STATUS_OK;

  SetMem(&ePlatformParam, sizeof(MDPPlatformParams), 0x0);

  if (MDP_STATUS_OK != (eRetStatus = MDPPlatformConfigure(MDP_DISPLAY_EXTERNAL, MDPPLATFORM_CONFIG_GETPANELCONFIG, &ePlatformParam)))
  {
      // Do nothing
  }
  else
  {
       eStatus = gBS->LocateProtocol( &gEfiTLMMProtocolGuid, NULL, (void**)&TLMMProtocol);
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uHPDGpio, 1, GPIO_INPUT,  GPIO_PULL_DOWN, GPIO_16MA),  DAL_TLMM_GPIO_DISABLE);
     }
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uDDDClkGpio, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA),  DAL_TLMM_GPIO_DISABLE);
     }
     
     if (EFI_SUCCESS == eStatus)
     {
       eStatus = TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(ePlatformParam.sHDMIPinConfig.uDDCDataGpio, 1, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA), DAL_TLMM_GPIO_DISABLE);
     }
     
     if (EFI_SUCCESS != eStatus)
     {
       eRetStatus = MDP_STATUS_FAILED;
     }

  }
  return eRetStatus;
}

#ifdef __cplusplus
}
#endif

