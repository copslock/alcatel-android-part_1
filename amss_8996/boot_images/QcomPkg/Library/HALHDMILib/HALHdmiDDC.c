
/*
===========================================================================

FILE:         HalHdmiDDC.c

DESCRIPTION:  
  This is the interface to be used to access Qualcomm's HDMI TX core DDC interface
  The intended audience for this source file is HDMI Host layer
  which incorporates this hardware block with other similiar hardware blocks.  
  This interface is not intended or recommended for direct usage by any application.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "MDPTypes.h"
#include "HALHdmiCommon.h"
#include "HALHdmiDDC.h"
#include "HDMI_EDID.h"

/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Init()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Init function configures the DDC setup registers such as
*   speed and drive strength.
*
* \param [in]  

* \retval None
*
****************************************************************************/
void HAL_HDMI_DDC_Init()
{
    uint32 uRegVal  = 0;

    /* Refer to VI settings */
    //Configure the Pre-Scale multiplier
    uRegVal = (0xA << HWIO_MMSS_HDMI_DDC_SPEED_PRESCALE_SHFT) & HWIO_MMSS_HDMI_DDC_SPEED_PRESCALE_BMSK;
    //Configure the Threshold
    uRegVal |= (0x2 << HWIO_MMSS_HDMI_DDC_SPEED_THRESHOLD_SHFT) & HWIO_MMSS_HDMI_DDC_SPEED_THRESHOLD_BMSK;
    HWIO_MMSS_HDMI_DDC_SPEED_OUT(uRegVal); // divide by 19 for 19.2MHz clock

    //Default value should be good
    uRegVal = 0;
    HWIO_MMSS_HDMI_DDC_SETUP_OUT(uRegVal);

    //Enable reference timer
    uRegVal = (1 << HWIO_MMSS_HDMI_DDC_REF_REFTIMER_ENABLE_SHFT) & HWIO_MMSS_HDMI_DDC_REF_REFTIMER_ENABLE_BMSK;
    //7 micro-seconds
    uRegVal |= (0x1B << HWIO_MMSS_HDMI_DDC_REF_REFTIMER_SHFT) & HWIO_MMSS_HDMI_DDC_REF_REFTIMER_BMSK;
    HWIO_MMSS_HDMI_DDC_REF_OUT(uRegVal);
}
/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Reset()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Reset function resets the DDC controller 
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_DDC_Reset(void)
{
  uint32 uRegVal = 0;
  //Toggle the reset bit to reset the controller when done
  uRegVal = (1 << HWIO_MMSS_HDMI_DDC_CTRL_SOFT_RESET_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_SOFT_RESET_BMSK;
  HWIO_MMSS_HDMI_DDC_CTRL_OUT(uRegVal);
  uRegVal = (0 << HWIO_MMSS_HDMI_DDC_CTRL_SOFT_RESET_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_SOFT_RESET_BMSK;
  HWIO_MMSS_HDMI_DDC_CTRL_OUT(uRegVal);
}
/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Read()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Read function read the DDC data from HDMI Sink device
*
* \param [in]      uDevAddr    - I2C Device Address
* \param [in]      uOffset     - The offset into the device to read
* \param [in/out]  pDataBuf    - Buffer to populate the read data
* \param [in]      uDataLength - Read length
* 
* \retval None
*
****************************************************************************/
HAL_HDMI_StatusType HAL_HDMI_DDC_Read(uint32 uSegmentAddr, uint32 uSegmentNum, uint32 uDevAddr, uint32 uOffset, uint8* pDataBuf, uint32 uDataLength, uint32 uTimeOut)
{
  uint32               uRegVal         = 0;
  uint32               uIndex          = 0;
  uint32               uTimeOutCount   = uTimeOut;
  HAL_HDMI_StatusType  eStatus         = HAL_HDMI_SUCCESS;
#if 0
  uint32               uCheckSum       = 0;
#endif

  /* Refer to VI code */

  if(NULL != pDataBuf)
  {
    do 
    {
      uTimeOutCount--;
      /*Clear and Enable DDC interrupt */
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_SHFT) & HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_BMSK;
      //Write
      HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);
      //Read back
      uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
    }while((uRegVal & HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_DONE_BMSK) && (uTimeOutCount));

    if(uTimeOutCount)
    {
      /* Write Segmentation Address */

      if(uSegmentNum)
      {
        //This is a write operation
        uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
        //Fill in the write data
        uRegVal |= (uSegmentAddr << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
        //Enable manual override of write index offset
        uRegVal |= (1U << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
        //Configure the write index offset to start with 0
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

        //Write this data to DDC buffer
        HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

        /* Setup the segment page number */

        //This is a write operation
        uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
        //Fill in the write data
        uRegVal |= (uSegmentNum << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
        //Enable manual override of write index offset
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
        //Configure the write index offset to start with 0
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

        //Write this data to DDC buffer
        HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);
      }

      //Ensure Device Address has LSB set to 0 to indicate Slave address read
      uDevAddr &= 0xFE;

      /*Setup device address as first element of the data - transation 1*/

      //This is a write operation
      uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
      //Fill in the write data
      uRegVal |= (uDevAddr << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
      //Enable manual override of write index offset
      if(uSegmentNum)
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
      else
        uRegVal |= (1U << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
      //Configure the write index offset to start with 0
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

      //Write this data to DDC buffer
      HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

      /*Setup offset as second element of the data - transaction 2*/

      //This is a write operation
      uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
      //Fill in the write data
      uRegVal |= (uOffset << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
      //Disable manual override of write index offset to allow HW auto increment.
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
      //Write index is auto-incremented by HW
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

      //Write this data to DDC buffer
      HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

      /*Setup the data buffer for read - transaction 3*/

      //This is a write operation
      uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
      //Fill in the write data, the OR 1 refers to a read operation on the I2C transaction
      uRegVal |= ((uDevAddr | 1)<< HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
      //Disable manual override of write index offset to allow HW auto increment.
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
       //Write index is auto-incremented by HW
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

      //Write this data to DDC buffer
      HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

      /* Data setup is complete, now setup the transaction characteristics */

      //Bytes count for transaction 1 & 2 - Device address not included in the byte count, so it is 1 byte.
      uRegVal  = (1 << HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_BMSK;
      //Stop on NACK 
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_BMSK;
      //Insert start bit
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_START0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_START0_BMSK;
      //Do not insert stop bit
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_BMSK;

      //Configure transaction 1 characteristics
      HWIO_MMSS_HDMI_DDC_TRANS0_OUT(uRegVal);

      if(uSegmentNum)
      {
        uRegVal  = (1 << HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_BMSK;
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_BMSK;
        uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_START0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_START0_BMSK;
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_BMSK;
        HWIO_MMSS_HDMI_DDC_TRANS1_OUT(uRegVal);
      }
      //Bytes count for transaction 3
      uRegVal  = (uDataLength << HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_BMSK;
      //Insert start bit
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_START0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_START0_BMSK;
      //Insert stop bit 
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_BMSK;
      //Insert 1 byte for transaction 1
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_RW0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_RW0_BMSK;

      if(uSegmentNum)
      {
        //Configure transaction 2 characteristics
        HWIO_MMSS_HDMI_DDC_TRANS2_OUT(uRegVal);
      }
      else
      {
        //Configure transaction 1 characteristics
        HWIO_MMSS_HDMI_DDC_TRANS1_OUT(uRegVal);
      }

      //Trigger the I2C transfer
      if(uSegmentNum)
      {
        //Execute transaction 0 followed by transaction 1 followed by transaction 2
        uRegVal  = (2<< HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_BMSK;
      }
      else
      {
        //Execute transaction 0 followed by transaction 1
        uRegVal  = (1<< HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_BMSK;
      }
      //Trigger HW to start the I2C transactions
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_CTRL_GO_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_GO_BMSK;
      //Write to register
      HWIO_MMSS_HDMI_DDC_CTRL_OUT(uRegVal);

      uRegVal = 0;
      uTimeOutCount = uTimeOut;
      /*Wait for Complete */
      do 
      {
        uTimeOutCount--;
        uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
      }while(!(uRegVal & HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_DONE_BMSK) && (uTimeOutCount));

      if(uTimeOutCount)
      {
        //Read DDC status 
        uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
        uRegVal &= (HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK0_BMSK | HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK1_BMSK | 
                   HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK2_BMSK | HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK3_BMSK);
        
        //Check if any NACK occurred
        if(!uRegVal)
        {
          //This is a write operation
          uRegVal  = (1<< HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
          //Disable manual override of write index offset to allow HW auto increment.
          uRegVal |= (1U << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
          if(uSegmentNum)
          {
             //Write index is auto-incremented by HW
            uRegVal |= (5 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;
          }
          else
          {
             //Write index is auto-incremented by HW
            uRegVal |= (3 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;
          }

          //Write this data to DDC buffer
          HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

          uRegVal = HWIO_MMSS_HDMI_DDC_DATA_IN; //Discard first data buffer as it is garbage

          for(uIndex = 0; uIndex < uDataLength; uIndex++)
          {
            uRegVal = HWIO_MMSS_HDMI_DDC_DATA_IN; 
            pDataBuf[uIndex] = (uint8) ((uRegVal & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK) >> HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT);
          }

#if 0
          //Calculate checksum
          for(uIndex = 0; uIndex < uDataLength - 1; uIndex++)
          {
            uCheckSum += pDataBuf[uIndex];
          }
          uCheckSum %= HAL_HDMI_DDC_CHECKSUM_MODULUS;

          //Check Checksum
          if(uCheckSum != pDataBuf[HDMI_EDID_CHECKSUM_OFFSET])
          {
            eStatus = HAL_HDMI_FAILED_CHECKSUM;
          }
#endif
        }
        else
        {
          eStatus = HAL_HDMI_FAILED_NACK;
        }
      }
      else
      {
        eStatus = HAL_HDMI_FAILED_TIMEOUT;
      }
    }
    else
    {
      eStatus = HAL_HDMI_FAILED_TIMEOUT;
    }
  }
  else
  {
    eStatus = HAL_HDMI_FAILED_INVALID_INPUT_PARAMETER;
  }
   /* Clear the DDC interrupt status */
  uRegVal = (1 << HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_SHFT) & HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_BMSK;
  HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);
  /* Disable the DDC interrupt */
  uRegVal = 0;
  HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);

  //Reset DDC engine
  HAL_HDMI_DDC_Reset();
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HAL_HDMI_DDC_Write()
*/
/*!
* \brief
*   The \b HAL_HDMI_DDC_Write function read the DDC data from HDMI Sink device
*
* \param [in]      uDevAddr    - I2C Device Address
* \param [in]      uOffset     - The offset into the device to write
* \param [in/out]  pDataBuf    - Buffer to populate the write data
* \param [in]      uDataLength - Write length
* 
* \retval None
*
****************************************************************************/
HAL_HDMI_StatusType HAL_HDMI_DDC_Write(uint32 uSegmentAddr, uint32 uSegmentNum, uint32 uDevAddr, uint32 uOffset, uint8* pDataBuf, uint32 uDataLength, uint32 uTimeOut)
{
  uint32               uRegVal         = 0;
  uint32               uIndex          = 0;
  uint32               uTimeOutCount   = uTimeOut;
  HAL_HDMI_StatusType  eStatus         = HAL_HDMI_SUCCESS;

  /* Refer to VI code */
  (void) uSegmentAddr; 
  (void) uSegmentNum;

  if(NULL != pDataBuf)
  {
    do 
    {
      uTimeOutCount--;
      /*Clear and Enable DDC interrupt */
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_SHFT) & HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_BMSK;
      //Write
      HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);
      //Read back
      uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
    }while((uRegVal & HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_DONE_BMSK) && (uTimeOutCount));

    if(uTimeOutCount)
    {
      //Ensure Device Address has LSB set to 0 to indicate Slave address read
      uDevAddr &= 0xFE; //??

      /*Setup device address as first element of the data - transation 1*/

      //This is a write operation
      uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
      //Fill in the write data
      uRegVal |= (uDevAddr << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
      //Enable manual override of write index offset
      uRegVal |= (1U << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
      //Configure the write index offset to start with 0
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

      //Write this data to DDC buffer
      HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

      /*Setup offset as second element of the data - transaction 2*/

      //This is a write operation
      uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
      //Fill in the write data
      uRegVal |= (uOffset << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
      //Disable manual override of write index offset to allow HW auto increment.
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
      //Write index is auto-incremented by HW
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;

      //Write this data to DDC buffer
      HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);

      /*Setup the data buffer for write - transaction 3*/

      for(uIndex = 0; uIndex < uDataLength; uIndex++)
      {
        //This is a write operation
        uRegVal  = (0 << HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_RW_BMSK;
        //Disable manual override of write index offset to allow HW auto increment.
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_WRITE_BMSK;
         //Write index is auto-incremented by HW
        uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_DATA_INDEX_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_INDEX_BMSK;
        //Fill in the write data, the OR 1 refers to a read operation on the I2C transaction
        uRegVal |= (pDataBuf[uIndex] << HWIO_MMSS_HDMI_DDC_DATA_DATA_SHFT) & HWIO_MMSS_HDMI_DDC_DATA_DATA_BMSK;
        //Write this data to DDC buffer
        HWIO_MMSS_HDMI_DDC_DATA_OUT(uRegVal);
      }

      /* Data setup is complete, now setup the transaction characteristics */

      //Bytes count for transaction 1 & 2 - Device address not included in the byte count, so it is 1 byte.
      uRegVal  = (1 << HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_BMSK;
      //Stop on NACK 
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP_ON_NACK0_BMSK;
      //Insert start bit
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_START0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_START0_BMSK;
      //Do not insert stop bit
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_BMSK;

      //Configure transaction 1 characteristics
      HWIO_MMSS_HDMI_DDC_TRANS0_OUT(uRegVal);

      //Bytes count for transaction 3
      uRegVal  = ((uDataLength - 1)<< HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_CNT0_BMSK;
      //Do not insert start bit
      uRegVal |= (0 << HWIO_MMSS_HDMI_DDC_TRANS0_START0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_START0_BMSK;
      //Insert stop bit 
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_SHFT) & HWIO_MMSS_HDMI_DDC_TRANS0_STOP0_BMSK;

      //Configure transaction 1 characteristics
      HWIO_MMSS_HDMI_DDC_TRANS1_OUT(uRegVal);

      //Trigger the I2C transfer

      //Execute transaction 0 followed by transaction 1
      uRegVal  = (1<< HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_TRANSACTION_CNT_BMSK;
      //Trigger HW to start the I2C transactions
      uRegVal |= (1 << HWIO_MMSS_HDMI_DDC_CTRL_GO_SHFT) & HWIO_MMSS_HDMI_DDC_CTRL_GO_BMSK;

      //Write to register
      HWIO_MMSS_HDMI_DDC_CTRL_OUT(uRegVal);

      uRegVal = 0;
      uTimeOutCount = uTimeOut;
      /*Wait for Interrupt Complete */
      do 
      {
        uTimeOutCount--;
        uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
      }while(!(uRegVal & HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_DONE_BMSK) && (uTimeOutCount));

      if(uTimeOutCount)
      {
        //Read DDC status 
        uRegVal = HWIO_MMSS_HDMI_DDC_SW_STATUS_IN;
        uRegVal &= (HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK0_BMSK | HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK1_BMSK | 
                   HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK2_BMSK | HWIO_MMSS_HDMI_DDC_SW_STATUS_SW_NACK3_BMSK);
        
        //Check if any NACK occurred
        if(uRegVal)
        {
          eStatus = HAL_HDMI_FAILED_NACK;
        }
      }
      else
      {
        eStatus = HAL_HDMI_FAILED_TIMEOUT;
      }
    }
    else
    {
      eStatus = HAL_HDMI_FAILED_TIMEOUT;
    }
  }
  else
  {
    eStatus = HAL_HDMI_FAILED_INVALID_INPUT_PARAMETER;
  }
  /* Clear the DDC interrupt status */
  uRegVal = (1 << HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_SHFT) & HWIO_MMSS_HDMI_DDC_INT_CTRL_SW_DONE_ACK_BMSK;
  HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);
  /* Disable the DDC interrupt */
  uRegVal = 0;
  HWIO_MMSS_HDMI_DDC_INT_CTRL_OUT(uRegVal);

  //Reset DDC engine
  HAL_HDMI_DDC_Reset();
  return eStatus;
}

#ifdef __cplusplus
}
#endif

