#ifndef HDMIPHY_H
#define HDMIPHY_H
/*
===========================================================================

FILE:         HALHdmiPhy.h

DESCRIPTION:  


===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "HALHdmiCommon.h"


/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */
/* -----------------------------------------------------------------------
** Defines
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */

/*!
 * \b HdmiPllConfigType
 *
 * Defines the structure with hdmi pll freq. Only for 8960.Not applicable for 8660.
 */

typedef struct
{
  uint32 hdmi_pll_freq;                 // HDMI pll frequency
  bool32 bEnableOutVoltageSwingCtrl;    // Indicate if use uOutVoltageSwingCtrl. If set to 0, use default value. 
  uint32 uOutVoltageSwingCtrl;          // HDMI phy output voltage swing control value
}HdmiPhyConfigType;

/*!
 * \b HdmiPhyType
 *
 * Defines the HDMI PHY types
 */

typedef enum
{
   HDMI_PHY_NONE = 0,
   HDMI_PHY_28NM,
   HDMI_PHY_20NM,
   HDMI_PHY_MAX
} HdmiPhyType;

/*!
* \b HAL_HDMI_PhyPllFunctionTable
*
* Defines the PHY/PLL function table for functions to config PHY and PLL
*/
typedef struct
{
    /* Config HDMI PHY and PLL */
    bool32 (*HAL_HDMI_PHY_Config) (HdmiPhyConfigType  *pHdmiPhyConfigInfo);
} HAL_HDMI_PhyPllFunctionTable;


/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */
bool32 HAL_HDMI_PHY_Config(HdmiPhyConfigType  *pHdmiPhyConfigInfo);

/****************************************************************************
*
** FUNCTION: HAL_HDMI_PhyFxnsInit()
*/
/*!
* \DESCRIPTION
*   Initialize HDMI PHY/PLL function table
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_PhyFxnsInit(void);

bool32 HAL_HDMI_PHY_Config(HdmiPhyConfigType  *pHdmiPhyConfigInfo);


bool32 HAL_HDMI_3_0_1_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo);
bool32 HAL_HDMI_3_3_0_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo);
bool32 HAL_HDMI_4_0_0_PHY_Config(HdmiPhyConfigType  *pHdmiPllConfigInfo);

#endif /* HDMIPHY_H */


