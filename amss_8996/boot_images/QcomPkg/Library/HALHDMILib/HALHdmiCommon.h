#ifndef HDMICOMMON_H
#define HDMICOMMON_H
/*
===========================================================================

FILE:         HALHdmiCommon.h

DESCRIPTION:  
  This is the common language for all display related HALs to communicate.
  No function prototypes or HW specific types are defined here.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "MDPLib_i.h"
#include "mdsshwio.h"
#include "HALHdmiVideo.h"
#include "HALHdmiAudio.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */

/* HAL HDMI Dirty Bits */
#define HAL_HDMI_DIRTYBIT_POWER_MODE              0x1
#define HAL_HDMI_DIRTYBIT_OPERATION_MODE          0x2
#define HAL_HDMI_DIRTYBIT_VIDEO_RESOLUTION        0x4
#define HAL_HDMI_DIRTYBIT_AUDIO_PARAM             0x8
#define HAL_HDMI_DIRTYBIT_AVI_PACKET_TYPES        0x10
#define HAL_HDMI_DIRTYBIT_VS_PACKET_TYPES         0x20

/* Info frame checksum's modulus */
#define HAL_HDMI_INFO_FRAME_CHECKSUM_MODULUS            0x100

/* HDMI VENDOR SPECIFIC INFO FRAME */
#define HAL_HDMI_VS_INFO_FRAME_ID                       0x81
#define HAL_HDMI_VS_INFO_FRAME_VERSION                  0x01
#define HAL_HDMI_VS_INFO_FRAME_IEEE_REGISTRATION_ID     0xC03
#define HAL_HDMI_VS_INFO_FRAME_PAYLOAD_LENGTH           0x1B
#define HAL_HDMI_VS_INFO_FRAME_3D_PRESENT               0x02
#define HAL_HDMI_VS_INFO_FRAME_3D_PRESENT_FIELD_SHIFT   0x05
#define HAL_HDMI_VS_INFO_FRAME_3D_STRUCTURE_FIELD_SHIFT 0x04


/* HDMI AVI INFO FRAME */
#define HAL_HDMI_AVI_INFO_FRAME_ID                       0x82
#define HAL_HDMI_AVI_INFO_FRAME_VERSION                  0x02
#define HAL_HDMI_AVI_INFO_FRAME_LENGTH                   0x0D

#define HAL_Stall_Us(_DelayUs_)    MDP_OSAL_DELAYUS(_DelayUs_)

/* -----------------------------------------------------------------------
** Enum Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HAL_HDMI_StatusType
 *
 * Define error codes for debugging the HAL.
 */
typedef enum
{
  HAL_HDMI_SUCCESS = 0,
  HAL_HDMI_FAILURE,
  HAL_HDMI_FAILED_NO_HW_SUPPORT,
  HAL_HDMI_FAILED_INVALID_INPUT_PARAMETER,
  HAL_HDMI_FAILED_NO_SUPPORT_OR_NO_HW,
  HAL_HDMI_FAILED_INTERRUPTS_UNMAPPED,
  HAL_HDMI_FAILED_EXPECTED_NON_NULL_PTR,
  HAL_HDMI_FAILED_FXN_NOT_SUPPORTED,
  HAL_HDMI_FAILED_PARAMETER_OUT_OF_RANGE,
  HAL_HDMI_FAILED_NUMBER_OF_TABLE_ENTRIES,
  HAL_HDMI_FAILED_ZERO_AREA_WINDOW_SPECIFIED,
  HAL_HDMI_FAILED_BAD_CONNECTION_PARAMETER,
  HAL_HDMI_FAILED_DISPLAY_FILL_NOT_SUPPORTED,
  HAL_HDMI_FAILED_NO_SUPPORT_FOR_COLOR_FORMAT,
  HAL_HDMI_FAILED_YSTRIDE_TOO_LARGE_FOR_HW,
  HAL_HDMI_FAILED_IMAGE_SIZE_TOO_BIG_FOR_HW,
  HAL_HDMI_FAILED_HW_IS_STILL_ACTIVE,
  HAL_HDMI_FAILED_SW_IMPLEMENTATION_ERR,
  HAL_HDMI_FAILED_ADDRESS_NOT_MEMORY_ALIGNED,
  HAL_HDMI_FAILED_CANNOT_CLEAR_UNMAPPED_INTERRUPTS,
  HAL_HDMI_FAILED_SW_DRIVER_DOES_NOT_MATCH_HW,
  HAL_HDMI_FAILED_HW_OPTION_OBSOLETE,
  HAL_HDMI_FAILED_UNABLE_TO_INIT_HW,
  HAL_HDMI_FAILED_TIMEOUT,
  HAL_HDMI_FAILED_NACK,
  HAL_HDMI_FAILED_CHECKSUM
} HAL_HDMI_StatusType;

/*!
 * \b HAL_HDMI_DeviceIDType
 *
 * The logical device ID 
 */
typedef enum 
{
  HAL_HDMI_DEVICE_ID0 = 0,
  HAL_HDMI_DEVICE_MAX,
  HAL_HDMI_DEVICE_FORCE_32BIT = 0x7FFFFFFF
}HAL_HDMI_DeviceIDType;

/*!
 * \b HAL_HDMI_PowerModeType
 *
 * The power mode for the HDMI TX Core
 */
typedef enum 
{
  HAL_HDMI_POWER_OFF = 0,
  HAL_HDMI_POWER_ON,
  HAL_HDMI_POWER_MAX,
  HAL_HDMI_POWER_FORCE_32BIT = 0x7FFFFFFF
}HAL_HDMI_PowerModeType;

/*!
 * \b HAL_HDMI_PixelClkType
 *
 * The pixel clock for the HDMI TX Core
 */
typedef enum 
{
  HAL_HDMI_PCLK_25M175 = 0,
  HAL_HDMI_PCLK_25M200,
  HAL_HDMI_PCLK_27M000,
  HAL_HDMI_PCLK_27M027,
  HAL_HDMI_PCLK_54M000,
  HAL_HDMI_PCLK_54M054,
  HAL_HDMI_PCLK_74M176,
  HAL_HDMI_PCLK_74M250,
  HAL_HDMI_PCLK_148M352,
  HAL_HDMI_PCLK_148M500,
  HAL_HDMI_PCLK_MAX,
  HAL_HDMI_PCLK_FORCE_32BIT = 0x7FFFFFFF
}HAL_HDMI_PixelClkType;

/* -----------------------------------------------------------------------
** Structure Types 
** ----------------------------------------------------------------------- */
/*!
 * \b HAL_HDMI_VendorInfoFramePacketParamType
 *
 * The Vendor Specific info frame is for HDMI spec 1.4 and beyond specifically for 3D inclusion 
 */
#define HAL_HDMI_VendorInfoFramePacketParamType    HDMI_VendorInfoFramePacketParamType

/*!
 * \b HAL_HDMI_VideoAudioParamType
 *
 * The Video and Audio parameters used to configure the HDMI TX Core
 */
typedef struct 
{
  HAL_HDMI_DeviceIDType                    eDeviceID;             /* Logical device ID */
  HAL_HDMI_PowerModeType                   ePowerMode;            /* Power_On or Power_Off */
  HAL_HDMI_OperationModeType               eMode;                 /* FULL HDMI or DVI mode */
  HAL_HDMI_VideoFormatType                 eVideoResolution;      /* Video Resolution */
  HAL_HDMI_AudioParamType                  sAudioParams;          /* Audio parameters */
  HAL_HDMI_AVIPacketParamType              sAVIPacketParams;      /* AVI packet information */
  HAL_HDMI_VendorInfoFramePacketParamType  sVSPacketParams;       /* Vendor specific info packet */
  uint32                                   uFeatureOp;            /* Mask of what features need to be configured */
}HAL_HDMI_VideoAudioParamType;

/* -----------------------------------------------------------------------
** Function Prototypes
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_PowerModeSet()
*/
/*!
* \brief
*   The \b HAL_HDMI_PowerModeSet function set the HDMI TX core to 
*   POWERED ON or OFF
*
* \param [in]  None
*
* \retval HAL_HDMI_PowerModeType
*
****************************************************************************/
void HAL_HDMI_PowerModeSet(HAL_HDMI_PowerModeType ePowerMode);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_PowerModeGet()
*/
/*!
* \brief
*   The \b HAL_HDMI_PowerModeGet function retreives whether the HDMI TX core is 
*   POWERED ON or OFF
*
* \param [in]  None
*
* \retval HAL_HDMI_PowerModeType
*
****************************************************************************/
HAL_HDMI_PowerModeType HAL_HDMI_PowerModeGet(void);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_VideoAudioSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VideoAudioSetup function configures all the video/audio 
*   attributes that the HDMI TX Core requires to be functional.
*
* \param [in]  sParam   - contains the values of the attributes for the corresponding
                          property that needs configuration.
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VideoAudioSetup(HAL_HDMI_VideoAudioParamType *sParam);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_EngineCtrl()
*/
/*!
* \brief
*   The \b HAL_HDMI_EngineCtrl function configures the power mode and the video mode 
*   of the HDMI TX core.
*
* \param [in]  mode   - either DVI mode or FULL HDMI Mode
* \param [in]  ePower - either POWER_ON or POWER_OFF
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_EngineCtrl(HAL_HDMI_OperationModeType mode, HAL_HDMI_PowerModeType ePower);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_OperationModeGet()
*/
/*!
* \brief
*   The \b HAL_HDMI_OperationModeGet function retreives whether the HDMI TX core is 
*   FULL HDMI MODE or DVI MODE
*
* \param [in]  None
*
* \retval HAL_HDMI_OperationModeType
*
****************************************************************************/
HAL_HDMI_OperationModeType HAL_HDMI_OperationModeGet(void);
/****************************************************************************
*
** FUNCTION: HAL_HDMI_VendorInfoFramePacketSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VendorInfoFramePacketSetup function configures all the vendor specific 
*   info packet parameters. (Refer to HDMI spec 1.4 and above)
*   These parameters are derived based on the recommended values provided in the HDMI
*   specification. (Spec 1.4 specifically for 3D inclusion)
*
* \param [in]  psVendorInfoPacketParam   - it contains video resolution
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VendorInfoFramePacketSetup(HAL_HDMI_VendorInfoFramePacketParamType* psVendorInfoPacketParam);


#endif /* HDMICOMMON_H */


