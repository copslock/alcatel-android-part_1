#ifndef HDMIHPD_H
#define HDMIHPD_H
/*
===========================================================================

FILE:         HALHdmiHPD.h

DESCRIPTION: 
  This is the interface to be used to access the SWI of HDMI TX Core 
  specifically for Hot plug detection. The intended audience for this 
  source file is HDMI HOST driver. It abtracts the functionalities 
  that the HDMI Core can provide. This interface is not intended 
  or recommended for direct usage by any application.


===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------


===========================================================================
Copyright (c) 2008-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "MDPTypes.h"

/* -----------------------------------------------------------------------
** Macros
** ----------------------------------------------------------------------- */
#define HAL_HDMI_HPD_INTERRUPT                  0x1
#define HAL_HDMI_HPD_INTERRUPT_RX               0x2
/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
** Static Variables
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Prototypes
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_HDMI_HPD_CheckConnection()
*/
/*!
* \brief
*   The \b HAL_HDMI_HPD_CheckConnection checks whether it is a connect or disconnect 
*   event
*
* \param [in] - None
*
* \retval True/False for connection
*
****************************************************************************/
bool32 HAL_HDMI_HPD_CheckConnection(void);



/****************************************************************************
*
** FUNCTION: HAL_HDMI_HPD_Engine_Enable()
*/
/*!
* \brief
*   The \b HAL_HDMI_HPD_Engine_Enable enables counters and control for HPD  
*   event
*
* \param [in] - bEnable TRUE/FALSE
*
* \retval void
*
****************************************************************************/
void HAL_HDMI_HPD_Engine_Enable(void);


#endif /* HDMIHPD_H */


