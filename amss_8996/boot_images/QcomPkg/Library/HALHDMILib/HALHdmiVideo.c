
/*
===========================================================================

FILE:         HalHdmiVideo.c

DESCRIPTION:  
  This is the interface to be used to access Qualcomm's HDMI video interface.
  The intended audience for this source file is 
  display service layer which incorporates this hardware block with other
  similiar hardware blocks.  This interface is not intended or recommended
  for direct usage by any application.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
Copyright (c) 2008-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------
** Includes
** ----------------------------------------------------------------------- */
#include "MDPTypes.h"
#include "HALHdmiCommon.h"
/* -----------------------------------------------------------------------
** Static Variable
** ----------------------------------------------------------------------- */
/* Table indicating the video format supported by the HDMI TX Core v1.0 - See HDD*/
const HAL_HDMI_DispModeTimingType gSupportedVideoModeLUT[HAL_HDMI_VIDEO_FORMAT_MAX]=
{
  {HAL_HDMI_VIDEO_FORMAT_640x480p60_4_3,      640,  16,  96,  48,  TRUE,  480,  10,  2,  33,  TRUE,  25200, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x480p60_4_3,      720,  16,  62,  60,  TRUE,  480,  9,   6,  30,  TRUE,  27027, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x480p60_16_9,     720,  16,  62,  60,  TRUE,  480,  9,   6,  30,  TRUE,  27027, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p60_16_9,    1280, 110, 40,  220, FALSE, 720,  5,   5,  20,  FALSE, 74250, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080i60_16_9,   1920, 88,  44,  148, FALSE, 540,  2,   5,  15,  FALSE, 74250, 60000, TRUE,  FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i60_4_3,     720, 38,  124, 114, TRUE,  240,  4,   3,  15,  TRUE,  27027, 60000, TRUE,  FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i60_16_9,    720, 38,  124, 114, TRUE,  240,  4,   3,  15,  TRUE,  27027, 60000, TRUE,  FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x240p60_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x240p60_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x480i60_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, TRUE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_2880x480i60_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, TRUE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x240p60_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_2880x240p60_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x480p60_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x480p60_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p60_16_9,   1920, 88,  44,  148,  FALSE,1080, 4,  5, 36, FALSE,148500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_720x576p50_4_3,      720,  12,  64,  68,   TRUE, 576,  5,  5, 39, TRUE, 27000,  50000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x576p50_16_9,     720,  12,  64,  68,   TRUE, 576,  5,  5, 39, TRUE, 27000,  50000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p50_16_9,    1280, 440, 40,  220,  FALSE,720,  5,  5, 20, FALSE,74250,  50000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080i50_16_9,   1920, 528, 44,  148,  FALSE,540,  2,  5, 15, FALSE,74250,  50000, TRUE,  FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i50_4_3,     720, 24,  126, 138,  TRUE, 288,  2,  3, 19, TRUE, 27000,  50000, TRUE,  FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i50_16_9,    720, 24,  126, 138,  TRUE, 288,  2,  3, 19, TRUE, 27000,  50000, TRUE,  FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x288p50_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x288p50_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 1, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x576i50_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_2880x576i50_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x288p50_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_2880x288p50_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x576p50_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x576p50_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p50_16_9,   1920,  528,  44,  148,  FALSE,  1080,  4,  5,  36,  FALSE, 148500, 50000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p24_16_9,   1920,  638,  44,  148,  FALSE,  1080,  4,  5,  36,  FALSE, 74250,  24000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p25_16_9,   1920,  528,  44,  148,  FALSE,  1080,  4,  5,  36,  FALSE, 74250,  25000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p30_16_9,   1920,  88,   44,  148,  FALSE,  1080,  4,  5,  36,  FALSE, 74250,  30000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x480p60_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3,},
  {HAL_HDMI_VIDEO_FORMAT_2880x480p60_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_2880x576p50_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3,},
  {HAL_HDMI_VIDEO_FORMAT_2880x576p50_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080i50_16_9_special,   1920, 32,  168, 184,  FALSE, 540,  23, 5, 57, TRUE, 72000,  50000, TRUE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080i100_16_9,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p100_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_720x576p100_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3,},
  {HAL_HDMI_VIDEO_FORMAT_720x576p100_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i100_4_3,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3,},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i100_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1920x1080i120_16_9,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p120_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_720x480p120_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x480p120_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i120_4_3,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i120_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_720x576p200_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x576p200_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i200_4_3,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x576i200_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_720x480p240_4_3,     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_720x480p240_16_9,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i240_4_3,    0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x480i240_16_9,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},  
  {HAL_HDMI_VIDEO_FORMAT_1024x768p60_4_3,     1024, 24, 136, 160, TRUE, 768,  3,   6,  29,  TRUE,  65000, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},  
  {HAL_HDMI_VIDEO_FORMAT_1024x768p70_4_3,     1024, 24, 136, 144, TRUE, 768,  3,   6,  29,  TRUE,  75000, 70000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},  
  {HAL_HDMI_VIDEO_FORMAT_1024x768p75_4_3,     1024, 16, 96  ,176, FALSE,768,  1,   3,  28,  FALSE, 78750, 75000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},  
  {HAL_HDMI_VIDEO_FORMAT_1280x768p60_5_3,     1280, 64, 128, 192, TRUE ,768,  3,  7,  20, FALSE, 79500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_5_3},
  {HAL_HDMI_VIDEO_FORMAT_1280x800p60_16_10,   1280, 72, 128, 200, TRUE, 800,  3,  6,  22, FALSE, 83500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_10},
  {HAL_HDMI_VIDEO_FORMAT_1280x960p60_4_3,     1280, 96, 112, 312, FALSE, 960, 1,  3,  36, FALSE, 108000,60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1280x1024p60_5_4,    1280, 48, 112, 248, FALSE,1024, 1,  3,  38, FALSE, 108000,60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_5_4},
  {HAL_HDMI_VIDEO_FORMAT_1360x768p60_16_9,    1360, 64, 112, 256, FALSE,768,  3,  6,  18, FALSE, 85500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1366x768p60_16_9,    1366, 70, 143, 213, FALSE,768,  3,  3,  24, FALSE, 85500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1400x1050p60_4_3,    1400, 88, 144, 232, TRUE, 1050, 3,  4,  32, FALSE, 121750,60000, FALSE, TRUE, 0,HDMI_VIDEO_ASPECT_RATIO_4_3},
  {HAL_HDMI_VIDEO_FORMAT_1440x900p60_16_10,   1440, 80, 152, 232, TRUE, 900,  3,  6,  25, FALSE, 106500,60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_10},
  {HAL_HDMI_VIDEO_FORMAT_1600x900p60_16_9,    1600, 24, 80,  96,  FALSE,900,  1,  3,  96, FALSE, 108000,60000, FALSE, TRUE, 0,HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1680x1050p60_16_10,  1680, 104,176, 280, TRUE, 1050, 3,  6,  30, FALSE, 146250,60000, FALSE, TRUE, 0,HDMI_VIDEO_ASPECT_RATIO_16_10},
   //Enable this mode on after Dual pipe support is available
  {HAL_HDMI_VIDEO_FORMAT_2560x1600p60_16_10,  2560, 48, 32  ,80, FALSE,1600,  3,   6,  37,  TRUE, 268500, 60000, FALSE, FALSE, 0, HDMI_VIDEO_ASPECT_RATIO_16_10},  
  
};

const HAL_HDMI_DispModeTimingType gSupported3DFramePackingModeLUT[HAL_HDMI_VIDEO_FORMAT_3D_FRAME_PACKING_MAX]=
{
  /* 1080p frame packing has HW limitation due to heigh restricted by max value of 2^11 and that is less than 2205 */
  {HAL_HDMI_VIDEO_FORMAT_1920x1080p24_16_9,   1920, 638, 44,  148,  FALSE,  2205,  4,   5,  36,  FALSE, 148500, 24000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p60_16_9,    1280, 110, 40,  220,  FALSE,  1470,  5,   5,  20,  FALSE, 148500, 60000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},
  {HAL_HDMI_VIDEO_FORMAT_1280x720p50_16_9,    1280, 440, 40,  220,  FALSE,  1470,  5,   5,  20,  FALSE, 148500, 50000, FALSE, TRUE, 0, HDMI_VIDEO_ASPECT_RATIO_16_9},

};
/* -----------------------------------------------------------------------
** Public Functions
** ----------------------------------------------------------------------- */
/****************************************************************************
*
** FUNCTION: HAL_HDMI_VideoInterfaceSetup()
*/
/*!
* \brief
*   The \b HAL_HDMI_VideoInterfaceSetup function configures all the video timing
*   generation parameters. These parameters are derived using parameters provided 
*   by the HDMI CEA spec and programmed according to the HDMI SWI specification.
*
* \param [in]  eVideoFormat     - Video resolution
* \param [in]  e3DFrameFormat   - 3D frame format
*
* \retval None
*
****************************************************************************/
void HAL_HDMI_VideoInterfaceSetup(HAL_HDMI_VideoFormatType eVideoFormat, HAL_HDMI_VideoFormat3DType e3DFrameFormat)
{
    uint32 uRegVal   = 0;
    uint32 uTotalV   = 0;
    uint32 uTotalH   = 0;
    uint32 uActiveH  = 0;
    uint32 uActiveV  = 0;
    uint32 uStartH   = 0;
    uint32 uEndH     = 0;
    uint32 uStartV   = 0;
    uint32 uEndV     = 0;
    uint32 uRefTime  = (0x1 << 6 | 0x13);  //Need more comments on this
    bool32 bActiveLowH = FALSE;
    bool32 bActiveLowV = FALSE;
    bool32 bInterlaced = FALSE;
    uint32 uLUTIndex   = 0;

    const HAL_HDMI_DispModeTimingType *pLUT = NULL;

    /* Only 3D frame packing mode currently requires new timing info - other 3D formats are not supported for 8660 */
    if(HAL_HDMI_VIDEO_3D_FORMAT_FRAME_PACKING == e3DFrameFormat)
    {
      /* 3D frame packing timing parameters */
      pLUT = gSupported3DFramePackingModeLUT;
      /* Search this table to find the right timing parameters - This table should be fairly small for the search */
      while((uLUTIndex < HAL_HDMI_VIDEO_FORMAT_3D_FRAME_PACKING_MAX) &&
            (eVideoFormat != gSupported3DFramePackingModeLUT[uLUTIndex].eVideoFormat))
        uLUTIndex++;
    }
    else
    {
      /* Non 3D frame packing timing parameters */
      pLUT      = gSupportedVideoModeLUT;
      uLUTIndex = eVideoFormat;
    }

    if(!HWIO_MMSS_HDMI_USEC_REFTIMER_IN)
      HWIO_MMSS_HDMI_USEC_REFTIMER_OUT(uRefTime); // divide by 19 for 19.2MHz clock

    if(pLUT[uLUTIndex].bSupported)
    {
      /* Hsync Total and Vsync Total */
      uActiveH = pLUT[uLUTIndex].uActiveH << pLUT[uLUTIndex].uPixelRepeatFactor;

      uTotalH  = uActiveH + pLUT[uLUTIndex].uFrontPorchH + 
                            pLUT[uLUTIndex].uBackPorchH  +
                            pLUT[uLUTIndex].uPulseWidthH - 1; 
      
      uActiveV = pLUT[uLUTIndex].uActiveV;
      
      uTotalV  = uActiveV + pLUT[uLUTIndex].uFrontPorchV + 
                            pLUT[uLUTIndex].uBackPorchV  + 
                            pLUT[uLUTIndex].uPulseWidthV - 1;

      uRegVal = ((uTotalV << HWIO_MMSS_HDMI_TOTAL_V_TOTAL_SHFT)& HWIO_MMSS_HDMI_TOTAL_V_TOTAL_BMSK)|
                ((uTotalH << HWIO_MMSS_HDMI_TOTAL_H_TOTAL_SHFT)& HWIO_MMSS_HDMI_TOTAL_H_TOTAL_BMSK);
      
      HWIO_MMSS_HDMI_TOTAL_OUT(uRegVal);

      /* Hsync Start and Hsync End */
      uStartH = pLUT[uLUTIndex].uBackPorchH + pLUT[uLUTIndex].uPulseWidthH;
      uEndH   = (uTotalH + 1) - pLUT[uLUTIndex].uFrontPorchH;

      uRegVal = ((uStartH << HWIO_MMSS_HDMI_ACTIVE_H_START_SHFT)& HWIO_MMSS_HDMI_ACTIVE_H_START_BMSK)|
                ((uEndH   << HWIO_MMSS_HDMI_ACTIVE_H_END_SHFT)& HWIO_MMSS_HDMI_ACTIVE_H_END_BMSK);
      
      HWIO_MMSS_HDMI_ACTIVE_H_OUT(uRegVal);

      uStartV = pLUT[uLUTIndex].uBackPorchV + pLUT[uLUTIndex].uPulseWidthV - 1;
      uEndV   = uTotalV - pLUT[uLUTIndex].uFrontPorchV;

      uRegVal = ((uStartV << HWIO_MMSS_HDMI_ACTIVE_V_START_SHFT)& HWIO_MMSS_HDMI_ACTIVE_V_START_BMSK)|
                ((uEndV   << HWIO_MMSS_HDMI_ACTIVE_V_END_SHFT)& HWIO_MMSS_HDMI_ACTIVE_V_END_BMSK);
      
      HWIO_MMSS_HDMI_ACTIVE_V_OUT(uRegVal);

      bInterlaced = pLUT[uLUTIndex].bInterlaced;
      if(bInterlaced)
      {
        /* VTotal = VTotal + 1 as the second field has 1 extra blank line */
        uRegVal = ((uTotalV + 1) <<  HWIO_MMSS_HDMI_V_TOTAL_F2_V_TOTAL_F2_SHFT)& HWIO_MMSS_HDMI_V_TOTAL_F2_RMSK; 
        HWIO_MMSS_HDMI_V_TOTAL_F2_OUT(uRegVal);

        /* vStart = vStartField1 + 1 */
        uStartV = uStartV + 1;
        /* vEnd   = vStartField1 + 1 */
        uEndV   = uEndV + 1;
        uRegVal = ((uStartV << HWIO_MMSS_HDMI_ACTIVE_V_F2_START_F2_SHFT)& HWIO_MMSS_HDMI_ACTIVE_V_F2_START_F2_BMSK)|
                  ((uEndV   << HWIO_MMSS_HDMI_ACTIVE_V_F2_END_F2_SHFT)& HWIO_MMSS_HDMI_ACTIVE_V_F2_END_F2_BMSK);
        HWIO_MMSS_HDMI_ACTIVE_V_F2_OUT(uRegVal);
      }
      else
      {
        HWIO_MMSS_HDMI_V_TOTAL_F2_OUT(0);
        HWIO_MMSS_HDMI_ACTIVE_V_F2_OUT(0);
      }

      bActiveLowH = pLUT[uLUTIndex].bActiveLowH;
      bActiveLowV = pLUT[uLUTIndex].bActiveLowV;

      uRegVal = ((bInterlaced << HWIO_MMSS_HDMI_FRAME_CTRL_INTERLACED_EN_SHFT  & HWIO_MMSS_HDMI_FRAME_CTRL_INTERLACED_EN_BMSK)  |
                 (bActiveLowH << HWIO_MMSS_HDMI_FRAME_CTRL_HSYNC_HDMI_POL_SHFT & HWIO_MMSS_HDMI_FRAME_CTRL_HSYNC_HDMI_POL_BMSK) |
                 (bActiveLowV << HWIO_MMSS_HDMI_FRAME_CTRL_VSYNC_HDMI_POL_SHFT & HWIO_MMSS_HDMI_FRAME_CTRL_VSYNC_HDMI_POL_BMSK));

      
      HWIO_MMSS_HDMI_FRAME_CTRL_OUT(uRegVal);
    }
}

#ifdef __cplusplus
}
#endif

