/**
@file CeEL_Reg.h 
@brief Crypto Engine Reg source file 
*/

/*===========================================================================

                     

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009- 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-07-31   nk      Boot version
2012-03-09   yk      Modification for Crypto5
2010-08-23   bm      Initial Version
============================================================================*/


/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/**
 * @brief Initialize the Data mover 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElRegInit (void);

/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELHashRegXfer( uint8 *buff_ptr, 
                               uint32 buff_len,
                               uint8 *digest_ptr,
                               uint32 digest_len);



/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELCipherRegXfer( uint8 *datain_ptr, 
                                 uint32 nDataLen,
                                 uint8 *dataout_ptr,
                                 uint32 nDataOutLen,
                                 uint8* cntx);


