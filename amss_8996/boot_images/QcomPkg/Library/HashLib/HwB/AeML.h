#ifndef _AeML
#define _AeML

/*===========================================================================

                    ArmV8 Crypto Security Extension Module API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014-2015 Copyright Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE


 when       who     what, where, why
 --------   ---     ----------------------------------------------------------
06/04/14   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
#define AEML_SHA_BLOCK_SIZE         64
// Max data size that crypto engine can handle
#define AEML_MAX_BLOCK_SIZE         0xFFFFFFFC

#define AEML_HASH_SHA_IV_LEN        20
#define AEML_HASH_SHA256_IV_LEN     32
#define AEML_HMAC_MAX_KEY_SIZE      16
#define AEML_AES_MAX_KEY_SIZE       32
#define AEML_AES_MAX_IV_SIZE_BYTES  32

#define MAX_SHA1_INPUT_SIZE   4096 //Max value can be 4096
#define MAX_SHA256_INPUT_SIZE 4096 //Max value can be 4096

#define SHA1_BLOCK_SIZE 16
#define SHA256_BLOCK_SIZE 16
#define BLOCK_SIZE 64

#define AEML_SHA1_DIGEST_SIZE     20
#define AEML_SHA256_DIGEST_SIZE   32

typedef enum
{
  AEML_ERROR_SUCCESS                = 0x0,
  AEML_ERROR_FAILURE                = 0x1,
  AEML_ERROR_INVALID_PARAM          = 0x2,
  AEML_ERROR_NOT_SUPPORTED          = 0x3
} AeMLErrorType;

typedef PACKED struct 
{
  void                              *pvBase; 
  uint32                            dwLen;  
}AEMLIovecType;

typedef PACKED struct 
{
  AEMLIovecType                     *iov;                 
  uint32                            size;                 
}AEMLIovecListType;

typedef enum
{
  AEML_HASH_ALGO                    = 0x1,
  AEML_CIPHER_ALGO                  = 0x2
} AeMLAlgoType;

typedef enum
{
  AEML_HASH_ALGO_SHA1               = 0x1,
  AEML_HASH_ALGO_SHA256             = 0x2
} AeMLHashAlgoType;

typedef enum
{
  AEML_HASH_MODE_HASH               = 0x0, // Plain SHA
  AEML_HASH_MODE_HMAC               = 0x1  // HMAC SHA
} AeMLHashModeType;

typedef enum 
{ 
  AEML_HASH_PARAM_MODE              = 0x01,
  AEML_HASH_PARAM_HMAC_KEY          = 0x02,
} AeMLHashParamType;

#define AEML_HASH_DIGEST_SIZE_SHA1    20
#define AEML_HASH_DIGEST_SIZE_SHA256  32
#define AEML_HASH_DIGEST_BLOCK_SIZE   64

typedef enum 
{
  AEML_CIPHER_MODE_ECB              = 0x0,
  AEML_CIPHER_MODE_CBC              = 0x1,
  AEML_CIPHER_MODE_CTR              = 0x2,
  AEML_CIPHER_MODE_XTS              = 0x3,
  AEML_CIPHER_MODE_CCM              = 0x4,
  AEML_CIPHER_MODE_CMAC             = 0x5,
  AEML_CIPHER_MODE_CTS              = 0x6
} AeMLCipherModeType;

typedef enum 
{
  AEML_CIPHER_ENCRYPT               = 0x00, 
  AEML_CIPHER_DECRYPT               = 0x01 
} AeMLCipherDir;

typedef enum 
{ 
  AEML_CIPHER_PARAM_DIRECTION       = 0x01,
  AEML_CIPHER_PARAM_KEY             = 0x02,
  AEML_CIPHER_PARAM_IV              = 0x03,
  AEML_CIPHER_PARAM_MODE            = 0x04,
} AeMLCipherParamType;

#define AEML_AES128_IV_SIZE         16
#define AEML_AES128_KEY_SIZE        16
#define AEML_AES256_IV_SIZE         16
#define AEML_AES256_KEY_SIZE        32
#define AEML_HMAC_KEY_SIZE          64

typedef enum 
{
  AEML_CIPHER_ALG_AES128            = 0x0,
  AEML_CIPHER_ALG_AES256            = 0x1
} AeMLCipherAlgType;

typedef PACKED struct 
{
  void * pClientCtxt;
} AeMLCntxHandle;

typedef PACKED struct
{
  AeMLHashAlgoType                  algo;
  AeMLHashModeType                  mode;
  uint32                            auth_key[8];
  uint32                            auth_iv[8];
  uint64                            dataLn;
  uint8                             leftover[AEML_SHA_BLOCK_SIZE];
  uint32                            leftover_size;
  uint8                             hash_size;
  uint32                            counter[2];
}AeMLHashAlgoCntxType;

typedef PACKED struct
{
  AeMLCipherAlgType                 algo;
  AeMLCipherModeType                mode;
  AeMLCipherDir                     dir;
  uint8                             aes_key[AEML_AES_MAX_KEY_SIZE];
  uint8                             iv[AEML_AES_MAX_IV_SIZE_BYTES];
  boolean                           firstBlock;
  boolean                           lastBlock;
  uint32                            dataLn;
  uint32                            seg_start;
  uint32                            seg_size;
}AeMLCipherAlgoCntxType;

int get_aes_index( int block_size);

/**
 * @brief This function initializes the AE
 *
 * @param void
 *
 * @return AeMLErrorType
 *
 * @see 
 *
 */

AeMLErrorType 
AeMLInit            (void);

/**
 * @brief This function deinitializes the CE
 *
 * @param void
 *
 * @return AeMLErrorType
 *
 * @return AeMLErrorType
 *
 * @see 
 */

AeMLErrorType 
AeMLDeInit          (void);

/**
 * @brief Intialize a hash context for Hash update and final functions
 *
 * @param _h      [in] Pointer to a pointer to the hash context
 * @param pAlgo   [in] Algorithm type
 *
 * @return AeMLErrorType
 *
 * @see AeMLHashUpdate and AeMLHashfinal
 *
 */

AeMLErrorType 
AeMLHashInit        (AeMLCntxHandle       ** _h, 
                     AeMLHashAlgoType     pAlgo);

/**
 * @brief Deintialize a hash context  
 *
 * @param _h      [in] Pointer to a pointer to the hash context
 *
 * @return AeMLErrorType
 *
 * @see AeMLDeInit
 *
 */

AeMLErrorType 
AeMLHashDeInit      (AeMLCntxHandle       ** _h);

/**
 * @brief This function will hash data into the hash context
 *        structure, which must have been initialized by
 *        AeMLHashInit.
 *
 * @param _h          [in] Pointer to Hash context
 * @param ioVecIn     [in] Pointer to input message to be
 *                     hashed
 * @return AeMLErrorType 
 *
 * @see AeMLHashInit 
 *
 */

AeMLErrorType 
AeMLHashAtomic(AeMLCntxHandle * _h,
               uint8 *buff_ptr, 
               uint32 buff_size,
               uint8 *hash_output);

/**
 * @brief This functions sets the Hash paramaters - Mode and Key for HMAC
 *
 * @param _h        [in] Pointer to cipher context handle
 * @param nParamID  [in] Cipher parameter id to set
 * @param pParam    [in] Pointer to parameter data 
 * @param cParam    [in] Size of parameter data in bytes
 * @param palgo     [in]  Algorithm type
 *
 * @return AeMLErrorType
 *
 */

AeMLErrorType  
AeMLHashSetParam    (AeMLCntxHandle       * _h,
                     AeMLHashParamType    nParamID, 
                     const void           *pParam, 
                     uint32               cParam,
                     AeMLHashAlgoType     pAlgo );

/**
 * @brief Intialize a cipher context 
 *
 * @param _h       [in] Pointer to a pointer to the cipher 
 *                 context structure
 * @param pAlgo    [in] Cipher algorithm type 
 *
 * @return AeMLErrorType
 *
 * @see 
 *
 */

AeMLErrorType 
AeMLCipherInit      (AeMLCntxHandle       ** _h, 
                     AeMLCipherAlgType    pAlgo);

/**
 * @brief Deintialize a cipher context 
 *
 * @param _h       [in] Pointer to a pointer to the cipher 
 *                 context structure
 * @return AeMLErrorType
 *
 * @see 
 *
 */

AeMLErrorType 
AeMLCipherDeInit    (AeMLCntxHandle       ** _h);

/**
 * @brief This functions sets the Cipher paramaters used by 
 *        AeMLCipherData
 *
 * @param _h        [in] Pointer to cipher context handle
 * @param nParamID  [in] Cipher parameter id to set
 * @param pParam    [in] Pointer to parameter data 
 * @param cParam    [in] Size of parameter data in bytes
 *
 * @return AeMLErrorType
 *
 * @see AeMLCipherData 
 *
 */

AeMLErrorType  
AeMLCipherSetParam    (AeMLCntxHandle       * _h,
                       AeMLCipherParamType  nParamID, 
                       const void           *pParam, 
                       uint32               cParam );
 
/**
 * @brief This functions gets the Cipher paramaters used by 
 *        AeMLCipherData
 *
 * @param _h        [in] Pointer to cipher context handle
 * @param nParamID  [in]  Cipher parameter id to get
 * @param pParam    [out] Pointer to parameter data 
 * @param pcParam   [out] Pointer to size of data 
 *
 * @return AeMLErrorType
 *
 * @see AeMLCipherData
 *
 */

AeMLErrorType  
AeMLCipherGetParam    (AeMLCntxHandle       * _h,
                       AeMLCipherParamType  nParamID, 
                       const void           *pParam, 
                       uint32               *cParam );

/**
 * @brief This function encrypts/decrypts the passed message
 *        using the specified algorithm.
 *
 * @param _h        [in] Pointer to cipher context handle
 * @param ioVecIn   [in] Pointer to input data. Input data 
 *                  length must be a multiple of 16 bytes
 * @param ioVecOut  [in] Pointer to output data
 *
 * @return AeMLErrorType
 *
 * @see  
 *
 */

AeMLErrorType 
AeMLCipherData      (AeMLCntxHandle       * _h, 
                     AEMLIovecListType    ioVecIn,
                     AEMLIovecListType    * ioVecOut);


/**
 * @brief This function encrypts/decrypts the passed message
 *        using the specified algorithm.
 *
 * @param _h        [in] Pointer to hash context handle
 * @param ioVecIn   [in] Pointer to input data. Input data 
 *                  length must be a multiple of 16 bytes
 * @param ioVecOut  [in] Pointer to output data
 *
 * @return AeMLErrorType
 *
 * @see  
 *
 */
//AeMLErrorType 
//AeMLHashUpdate      (AeMLCntxHandle* aeMlHandle, 
//                     uint8 *buff_ptr,
//                     uint64 buffer_size);

AeMLErrorType AeMLHashUpdate(AeMLCntxHandle* _h, 
                             AEMLIovecListType   ioVecIn);

/**
 * @brief This function encrypts/decrypts the passed message
 *        using the specified algorithm.
 *
 * @param _h        [in] Pointer to hash context handle
 * @param ioVecIn   [in] Pointer to input data. Input data 
 *                  length must be a multiple of 16 bytes
 * @param ioVecOut  [in] Pointer to output data
 *
 * @return AeMLErrorType
 *
 * @see  
 *
 */
//AeMLErrorType
//AeMLHashFinal        (AeMLCntxHandle* aeMlHandle, 
//                      uint8 *out);
AeMLErrorType AeMLHashFinal(AeMLCntxHandle* _h, 
                            AEMLIovecListType* ioVecOut);

#endif //_AeML
