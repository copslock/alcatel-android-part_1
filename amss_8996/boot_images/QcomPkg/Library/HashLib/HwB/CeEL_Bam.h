/**
@file CeEL_Bam.h 
@brief Crypto Engine BAM source file 
*/

/*===========================================================================

                     

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Library/HashLib/HwB/CeEL_Bam.h#3 $   
  $DateTime: 2015/07/17 20:26:52 $ 
  $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-07-31    nk     Boot version
2012-04-25   ejt     Initial Version
============================================================================*/


/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"
#include "comdef.h"


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/**
 * @brief Initialize the Data mover 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElBamInit (void);

/**
 * @brief Initialize the Data mover 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElBamDeInit (void);

/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELBamHashXfer(uint8 *buff_ptr, 
                              uint32 buff_len,
                              uint8 *digest_ptr,
                              uint32 digest_len,
                              boolean lastBlock,
                              uint8 *cntx);


/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELBamCipherXfer(uint8 *datain_ptr, 
                                uint32 nDataLen,
                                uint8 *dataout_ptr,
                                uint8 *ctx);

/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELBamHashCipherXfer(uint8 *datain_ptr, 
                                uint32 nDataLen,
                                uint8 *dataout_ptr);

