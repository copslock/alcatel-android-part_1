/**
@file CeEL.c 
@brief Crypto Engine Environment Library source file 
*/

/*===========================================================================

                     Crypto Engine Environment Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2010 - 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-03-09   yk      Modification for Crypto5
2010-05-24   bm      Initial version
============================================================================*/

#include <stringl/stringl.h>
#include "CeEL.h"
#include "CeCL_Env.h"
#include "CeEL_Env.h"
#include "CeEL_Reg.h"
#include "CeEL_Bam.h"
#include "CeCL.h"
#include <Library/BaseMemoryLib.h>

static uint32 crypto_bam_enabled = 0;

CeELErrorType CeElBamXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData,
                                   boolean lastBlock,
                                   uint8* cntx,
                                   CEELDataType dataType);

CeELErrorType CeElRegXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData,
                                   boolean lastBlock, 
                                   uint8* cntx,
                                   CEELDataType dataType);


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemoryBarrier(void)
{
  //CEEL_MEMORY_BARRIER();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElInit(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  /*if(CECL_BAM_IS_SUPPORTED())
  {
    stat = CeElBamInit();
  }*/
  
  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElDeinit(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  if(CECL_BAM_IS_SUPPORTED())
  {
    stat = CeElBamDeInit();
  }

  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
/*CeELErrorType CeElEnableClock(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  stat = (CeELErrorType) CeElClkEnable();

  return stat;
}*/

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
/*CeELErrorType CeElDisableClock(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  stat = (CeELErrorType) CeElClkDisable();

  return stat;
}*/

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElmalloc(void** ptr, uint64 ptrLen)
{ 
  CeElmalloc_Env(ptr, ptrLen);
  if(ptr == NULL)
  {
    return CEEL_ERROR_NO_MEMORY;
  }

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElfree(void* ptr)
{
  // safe free
  if(ptr != NULL)
  {
  CeElfree_Env(ptr);
    ptr = NULL;
  }

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemcpy(void* dst, void* src, uint64 len)
{
  //uint64 expected_len = 0;
  //memcpy(dst, src, (size_t)len);
  //expected_len = (uint64) (memscpy(dst, (size_t)len, src, (size_t)len));
  CopyMem(dst, src, len);

  /*if (expected_len != len) {
	  return CEEL_ERROR_FAILURE;
  }
  else {
	  return CEEL_ERROR_SUCCESS;
  }*/
  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemscpy(
                          void * dst,
                          uint64 dstLen,  // Max size of dst buffer
                          void * src,
                          uint64 srcLen   // Desired nbr of bytes to copy
                         )
{
  //uint64 expected_len;

  //expected_len = (uint64) (memscpy(dst, (size_t)dstLen, src, (size_t)srcLen));
  CopyMem(dst, src, srcLen);

  /*if (expected_len != srcLen) {
	  return CEEL_ERROR_FAILURE;
  }
  else {
	  return CEEL_ERROR_SUCCESS;
  }*/
  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemset(void* src, uint64 val, uint64 len)
{
  memset(src, val, len);

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 * Provide dividend and divisor(power of 2, eg: if 64, input 
 * will be 6 since 2^6 = 64) of which ceiling will be calculated
 *
 * @return CEEL_ERROR_SUCCESS
 *
 * @see 
 *
 */

CeELErrorType CeElCeil(uint32 total, uint32 power, uint32* ceil)
{
  *ceil = total>>power;
  if((*ceil<<power)!=total)
    (*ceil)++;

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElGetXferModeList(CeElXferModeType* xferMode)
{
  //Set XferMode to register
  *xferMode = CEEL_XFER_MODE_REG;

  //Set XferMode to BAM if supported
  if(CECL_BAM_IS_SUPPORTED())
  {
    *xferMode = CEEL_XFER_MODE_BAM; 
  }

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElGetXferfunction(CeElXferModeType xferMode, CeElXferFunctionType* xferFunc)
{
  //Check mode and set xfer function pointer
  if(xferMode == CEEL_XFER_MODE_REG)
    {
      *xferFunc = CeElRegXferFunction;
      return CEEL_ERROR_SUCCESS;
    }

  if(xferMode == CEEL_XFER_MODE_BAM)
  {      
    *xferFunc = CeElBamXferFunction;
    return CEEL_ERROR_SUCCESS;
  }  
  
  return CEEL_ERROR_FAILURE;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMutexEnter (void)
{
  CEEL_MUTEX_ENTER();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMutexExit (void)
{
  CEEL_MUTEX_EXIT();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElBamXferFunction(CEELIovecListType* inData, 
                                  CEELIovecListType* outData, 
                                  boolean lastBlock, 
                                  uint8* cntx,
                                  CEELDataType dataType)
{
  CeELErrorType ret = CEEL_ERROR_FAILURE;
   
  if(CECL_BAM_IS_SUPPORTED())
  {
    if(dataType == CEEL_DATA_HASH) 
    {
      ret = CeELBamHashXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase, outData->iov->dwLen, lastBlock, cntx); 
      return ret;
    }

    if(dataType == CEEL_DATA_CIPHER) 
    {
      ret = CeELBamCipherXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase, cntx); 
      return ret;
    }

    if(dataType == CEEL_DATA_HASH_CIPHER) 
    {
      ret = CeELBamHashCipherXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase); 
      return ret;
    }
  }

  return ret;
}


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElRegXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData, 
                                   boolean lastBlock, 
                                   uint8* cntx,
                                   CEELDataType dataType)
{
  CeELErrorType ret = CEEL_ERROR_FAILURE;

  if(dataType == CEEL_DATA_HASH) 
  {
    ret = CeELHashRegXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase, outData->iov->dwLen); 
    return ret;
  }

  if(dataType == CEEL_DATA_CIPHER || dataType == CEEL_DATA_HASH_CIPHER) 
  {
    ret = CeELCipherRegXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase,outData->iov->dwLen, cntx); 
    return ret;
  }

  return ret;

}

void crypto_bam_enable(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  stat = CeElBamInit();
  if(stat != CEEL_ERROR_SUCCESS)
  {
    crypto_bam_enabled = 0;
  }
  else{
  crypto_bam_enabled = 1;
  }
}

uint32 crypto_bam_check(void)
{
  return crypto_bam_enabled;
}