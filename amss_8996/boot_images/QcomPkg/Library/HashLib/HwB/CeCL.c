/**
@file CeCL.c 
@brief Crypto Engine Core Library source file 
*/

/*===========================================================================

                     Crypto Engine Core Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2011 - 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ----------------------------------------------------- 
2012-07-31   nk      Boot version
2012-07-08   nk      Added memory barriers
2012-03-09   yk      Modification for Crypto5
2010-08-21   bm      added support for reg interface xfers
2010-05-24   bm      Initial version
============================================================================*/
#include "CeCL.h"
#include "CeCL_Target.h"
#include "CeEL.h"
#include "CeCL_Env.h"
#include "DALSysTypes.h"
#include "DALSys.h"


CeCLErrorType CeCLIOCtlSetHashCntx(CeCLHashAlgoCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlGetHashCntx(CeCLHashAlgoCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlHashRegXfer(CeCLHashXferType     *pBufOut);
CeCLErrorType CeCLIOCtlSetCipherCntx(CeCLCipherCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlGetCipherCntx(CeCLCipherCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlCipherRegXfer(CeCLCipherXferType *pBufOut);
CeCLErrorType CeCLIOCtlSetHashCipherCntx(CeCLHashAlgoCntxType *hash_ctx_ptr, CeCLCipherCntxType *cipher_ctx_ptr);
CeCLErrorType CeCLIOCtlGetHashCipherCntx(CeCLHashAlgoCntxType *hash_ctx_ptr, CeCLCipherCntxType *cipher_ctx_ptr);

#define CECL_HW_SEC_ACC_DIN_SIZE 16
#define CECL_HW_SEC_CE_STATUS_DIN_RDY (HWIO_IN(CECL_CE_STATUS) & (1<<CECL_CE_STATUS_DIN_RDY_SHFT))
#define CECL_HW_SEC_CE_STATUS_DOUT_RDY (HWIO_IN(CECL_CE_STATUS) & (1<<CECL_CE_STATUS_DOUT_RDY_SHFT))  

#define CECL_CE_CIPHER_KEY_SIZE_AES128  0x0
#define CECL_CE_CIPHER_KEY_SIZE_AES256  0x2
#define CECL_BLOCK_SIZE_NON_BAM         16
#define CECL_BLOCK_SIZE_BAM             64


/**
 * @brief  Register CCM mode operation
 *
 * @param pBufOut   [in]  Pointer to data of type CeCLCipherXferType
 *  
 * @return CeCLErrorType
 *
 * @see 
 *
 */
static CeCLErrorType CeCLIOCtlCipherCCMRegXfer(CeCLCipherXferType *pBufOut)
{
  CeCLErrorType ret_val = CECL_ERROR_SUCCESS;
  uint32 counter_1 = 0;
  uint32 counter_2 = 0;
  uint32 counter_3 = 0;
  uint32 segment_count=0;
  //int64 out_segment_count=0;
  uint8* data_in = pBufOut->pDataIn;
  uint8* data_out = pBufOut->pDataOut;  
  uint32 data_write = 0;
  uint32 is_data_aligned = 0, is_data_aligned_out = 0;  
  //uint8  last_segment[CECL_HW_SEC_ACC_DIN_SIZE] = {0};
  uint32 last_seg_size = 0;
  //uint32 last_out_seg_size_bytes = 0;
  //uint32 seg_indx = 0;
  uint8  ccm_mac[CECL_AES_CCM_MAC_LEN_16] = {0};
  uint8* mac_out = ccm_mac;
  CeCLCipherCntxType *ctx_ptr;
  uint32 scratch_buff;
  uint32 last_seg_size_cnt = 0;
  uint32 temp_length;
  uint32 last_seg_size_temp;

  //Set context pointer
  ctx_ptr = (CeCLCipherCntxType *)pBufOut->pCntx;

  // reducing length to the shorter length, will take 
  // care of mac read from or write into crypto in a separate loop
  // to prevent buffer overflows
  if (ctx_ptr->dir == CECL_CIPHER_DECRYPT)
  {
    temp_length = pBufOut->nDataLen - ctx_ptr->macLn;
  }
  else
  {
    temp_length = pBufOut->nDataLen;
  }
  
  // Verifying if the pointers are aligned to 4bytes,
  // since data is written or read as uint32's to/from crypto
  is_data_aligned = (((uint64)(data_in) & 0x03) == 0);
  is_data_aligned_out = (((uint64)(data_out) & 0x03) == 0);

  // segment_count is the number of 16byte operations that need to be done
  // since we write 4 registers at a time
  // last_seg_size accounts for sizes that are not multiple of 16bytes
  // last_seg_size_cnt accounts for how many 4 byte transfers are there
  // for the pending data that is less than 16bytes
  segment_count = temp_length  / CECL_HW_SEC_ACC_DIN_SIZE;
  last_seg_size = temp_length % CECL_HW_SEC_ACC_DIN_SIZE;
  last_seg_size_cnt = last_seg_size/(CECL_HW_SEC_ACC_DIN_SIZE/4);
  if (last_seg_size)
  {
    segment_count++;
    if(last_seg_size % (CECL_HW_SEC_ACC_DIN_SIZE/4))
    {
      last_seg_size_cnt++;
    }
  }

  for (counter_1=0; counter_1 < segment_count; counter_1++)
  {
    //Feeding Data to CE
    if((counter_1 == segment_count-1) && last_seg_size)
    {
      /* Feed data in 4 bytes at a time to CE */ 
      for (counter_2=0, last_seg_size_temp = last_seg_size;
                        counter_2 < last_seg_size_cnt; counter_2++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DIN_RDY); 

        if(counter_2 == last_seg_size_cnt-1)
        {
          //Copy the data to an aligned address and then write it to the hw register
          //CeElMemcpy(&data_write, data_in, last_seg_size_temp);  
          if (CeElMemscpy(&data_write, sizeof(data_write), data_in, last_seg_size_temp)) return CECL_ERROR_FAILURE;  
          HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
          CeElMemoryBarrier();
          data_in += last_seg_size_temp;
        }
        else
        {
          if (is_data_aligned)
          {
            //As the data is aligned at a 32 bit boundry we can just write it to the hw register
            HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_in));
          }
          else
          {
            //Copy the data to an aligned address and then write it to the hw register
            //CeElMemcpy(&data_write, data_in, 4);  
            if (CeElMemscpy(&data_write, sizeof(data_write), data_in, 4)) return CECL_ERROR_FAILURE;
            HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
          }	  
          CeElMemoryBarrier();

          data_in += 4;
        }
        last_seg_size_temp -= 4;
      }
    }
    else
    {
      /* Feed data in 4 bytes at a time to CE */ 
      for (counter_2=0; counter_2<4; counter_2++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DIN_RDY); 

        if (is_data_aligned)
        {
          //As the data is aligned at a 32 bit boundry we can just write it to the hw register
          HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_in));
        }
        else
        {
          //Copy the data to an aligned address and then write it to the hw register
          //CeElMemcpy(&data_write, data_in, 4);  
          if (CeElMemscpy(&data_write, sizeof(data_write), data_in, 4)) return CECL_ERROR_FAILURE;
          HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
        }	  
        CeElMemoryBarrier();

        data_in += 4;
      }
    }//end of feeding data to CE
    
    //Reading out data from CE
    if((counter_1 == segment_count-1) && last_seg_size)
    {
      /* Feed data in 4 bytes at a time to CE */ 
      for (counter_3=0 , last_seg_size_temp = last_seg_size; 
                           counter_3 < last_seg_size_cnt; counter_3++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

        if (counter_3 == last_seg_size_cnt-1)
        {
          //Read data from CE
          data_write = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
          CeElMemoryBarrier();
          //CeElMemcpy(data_out, &data_write, last_seg_size_temp);  
	  if (CeElMemscpy(data_out, last_seg_size_temp, &data_write, last_seg_size_temp)) return CECL_ERROR_FAILURE;
          data_out += last_seg_size_temp;
        }	  
        else
        {
          if (is_data_aligned_out)
          {
            //Read data from CE
            *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
            CeElMemoryBarrier();
          }
          else
          {
            //Read data from CE
            data_write = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
            CeElMemoryBarrier();
            //CeElMemcpy(data_out, &data_write, 4);  
	    if (CeElMemscpy(data_out, 4, &data_write, 4)) return CECL_ERROR_FAILURE;
          }	  
          data_out += 4;
        }
        last_seg_size_temp -= 4;
      }
    }
    else
    {
      /* Feed data in 4 bytes at a time to CE */ 
      for (counter_3=0; counter_3<4; counter_3++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY);

        if (is_data_aligned_out)
        {
          //Read data from CE
          *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
          CeElMemoryBarrier();
          data_out += 4;
        }
        else
        {
          //Copy the data to an aligned address and then write it to the hw register
          //Read data from CE
          data_write = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
          CeElMemoryBarrier();
          //CeElMemcpy(data_out, &data_write, 4); 
	  if (CeElMemscpy(data_out, 4, &data_write, 4)) return CECL_ERROR_FAILURE;
          data_out += 4;
        }	  
      }
    }//end of Reading out data from CE
  }
  // Based on direction, need to read out MAC
  // or write the MAC to crypto
  if (ctx_ptr->dir == CECL_CIPHER_DECRYPT) 
  {
    //write out MAC to crypto
    for (counter_1=0; counter_1<4; counter_1++)
    {
      if (counter_1 >= (((ctx_ptr->macLn - 1) / 4) +1)) 
        break;

      while (!CECL_HW_SEC_CE_STATUS_DIN_RDY); 

      if (is_data_aligned)
      {
        //As the data is aligned at a 32 bit boundry we can just write it to the hw register
        HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_in));
      }
      else
      {
        //Copy the data to an aligned address and then write it to the hw register
        //CeElMemcpy(&data_write, data_in, 4);  
        if (CeElMemscpy(&data_write, sizeof(data_write), data_in, 4)) return CECL_ERROR_FAILURE;
        HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
      }	  
      CeElMemoryBarrier();

      data_in += 4;
    }

    // extra pad data, aligned to burst size, which is 4 in register mode, 
    // needs to be read out in decrypt,
    // this is garbage data
    for (counter_3=0; counter_3 < 4; counter_3++)
    {
      if(HWIO_IN(CECL_CE_STATUS) & CECL_CE_STATUS_OPERATION_DONE_BMSK)
      {
        break;
      }
      while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

      //Read data from CE
      scratch_buff = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
      CeElMemoryBarrier();
    }
  }
  else
  {  
    //Read out MAC from crypto
    /* Store data out 4 bytes at a time of the MAC */
    for (counter_1=0; counter_1<4; counter_1++)
    {
      if (counter_1 >= (((ctx_ptr->macLn - 1) / 4) +1)) 
        break;

      while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

      //Read data from CE
      *((uint32*)mac_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_1);
      CeElMemoryBarrier();
      mac_out += 4;
    }

    //Copy CCM MAC back to called
    //CeElMemcpy(data_out, ccm_mac, ctx_ptr->macLn); 
    if (CeElMemscpy(data_out, ctx_ptr->macLn, ccm_mac, ctx_ptr->macLn)) return CECL_ERROR_FAILURE;
  }

  // Check error conditions 
  // Data will be memset at the shared layer to ensure entire output buffer
  // is cleared
  if ((HWIO_IN(CECL_CE_STATUS)) & HWIO_CRYPTO0_CRYPTO_STATUS_MAC_FAILED_BMSK) 
  {
    ret_val = CECL_ERROR_FAILURE;
  }
  CeElMemoryBarrier();
  return ret_val;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClGetCeVersion(CeCLCeVersionType* CeEngVersion)
{
  if (CeEngVersion == NULL)
  {
    return CECL_ERROR_FAILURE;
  }

  *CeEngVersion = (CeCLCeVersionType) HWIO_INF(CECL_CE_VERSION, MAJ_VER);

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClInit(CeCLXferModeType mode)
{
  if (mode == CECL_XFER_MODE_REG)
  {
    //Set the bits that we need in Register mode
    HWIO_OUT(CECL_CE_CONFIG,
         ((0 << HWIO_SHFT(CECL_CE_CONFIG, MASK_ERR_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_OP_DONE_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DIN_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DOUT_INTR)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, HIGH_SPD_DATA_EN_N)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, LITTLE_ENDIAN_MODE))));
  }
  else if (mode == CECL_XFER_MODE_BAM)
  {
    //Set the bits that we need in BAM mode
    HWIO_OUT(CECL_CE_CONFIG,
         ((0 << HWIO_SHFT(CECL_CE_CONFIG, MASK_ERR_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_OP_DONE_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DIN_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DOUT_INTR)) |
          (0 << HWIO_SHFT(CECL_CE_CONFIG, HIGH_SPD_DATA_EN_N)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, PIPE_SET_SELECT)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, LITTLE_ENDIAN_MODE))));

    //Set the REQ_SIZE field to 8 beats * 8 = 64 bytes in BAM mode
    HWIO_OUTM(CECL_CE_CONFIG, HWIO_CRYPTO0_CRYPTO_CONFIG_REQ_SIZE_BMSK, 0xE0000);
  }
  else
  {
    return CECL_ERROR_FAILURE;
  }

  CeElMemoryBarrier();

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClDeinit(void)
{
  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClReset(void)
{
  /* pulling out of reset with default configuration */
  /* Reset is no longer there in a crypto register */
  /* It is controlled at the chip level clock block */

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */

void CeCLIOCtlHashCompletion(void)
{
  uint32 ce_status;
  
  while(1)
  {
    ce_status = HWIO_IN(CECL_CE_STATUS);
    CeElMemoryBarrier();
    if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
    {
      break;
    }
  }
}


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClIOCtlHash (CeCLIoCtlHashType ioCtlVal, 
                         uint8* pBufIn, 
                         uint32 dwLenIn, 
                         uint8* pBufOut, 
                         uint32 dwLenOut, 
                         uint32* pdwActualOut)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;

  switch (ioCtlVal)
  {
    case CECL_IOCTL_HASH_VERSION_NUM:
      if ((pBufOut!= NULL) && (dwLenOut >= CECL_VERSION_NUM_SIZE))
      {
        *pBufOut = 1;
        *pdwActualOut = CECL_VERSION_NUM_SIZE;
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_SET_HASH_CNTXT:
      if ((pBufIn!= NULL) && (dwLenIn == sizeof(CeCLHashAlgoCntxType)))
      { 
        retVal = CeCLIOCtlSetHashCntx((CeCLHashAlgoCntxType*) pBufIn);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_GET_HASH_CNTXT:
      
      if ((pBufOut!= NULL) && (dwLenOut >= sizeof(CeCLHashAlgoCntxType)))
      { 
        retVal = CeCLIOCtlGetHashCntx ((CeCLHashAlgoCntxType*) pBufOut);
        *pdwActualOut = sizeof(CeCLHashAlgoCntxType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    case CECL_IOCTL_HASH_XFER:

      if ((pBufIn!= NULL) && (dwLenIn >= sizeof(CeCLHashXferType)))
      { 
        retVal = CeCLIOCtlHashRegXfer ((CeCLHashXferType*) pBufIn);
        *pdwActualOut = sizeof(CeCLHashXferType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    default:
      retVal = CECL_ERROR_FAILURE;
      break;
  }

  return retVal;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClIOCtlCipher (CeCLIoCtlCipherType ioCtlVal, 
                               uint8* pBufIn, 
                               uint32 dwLenIn, 
                               uint8* pBufOut, 
                               uint32 dwLenOut, 
                               uint32* pdwActualOut)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;

  switch (ioCtlVal)
  {
    case CECL_IOCTL_CIPHER_VERSION_NUM:
      if ((pBufOut!= NULL) && (dwLenOut >= CECL_VERSION_NUM_SIZE))
      {
        *pBufOut = 1;
        *pdwActualOut = CECL_VERSION_NUM_SIZE;
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_SET_CIPHER_CNTXT:
      if ((pBufIn!= NULL) && (dwLenIn == sizeof(CeCLCipherCntxType)))
      { 
        retVal = CeCLIOCtlSetCipherCntx((CeCLCipherCntxType*) pBufIn);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    case CECL_IOCTL_GET_CIPHER_CNTXT:
      if ((pBufOut!= NULL) && (dwLenOut >= sizeof(CeCLCipherCntxType)))
      { 
        retVal = CeCLIOCtlGetCipherCntx ((CeCLCipherCntxType*) pBufOut);
        *pdwActualOut = sizeof(CeCLCipherCntxType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }            
      break;

    case CECL_IOCTL_CIPHER_XFER:
      if ((pBufIn!= NULL) && (dwLenIn >= sizeof(CeCLCipherXferType)))
      { 
        retVal = CeCLIOCtlCipherRegXfer ((CeCLCipherXferType*) pBufIn);
        *pdwActualOut = sizeof(CeCLCipherXferType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }            
      break;

    default:
      retVal = CECL_ERROR_FAILURE;
      break;
  }

  return retVal;
}


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */

CeCLErrorType CeClIOCtlHashCipher (CeCLIoCtlHashCipherType ioCtlVal, 
                         uint8* pHashBufIn, 
                         uint8* pCipherBufIn,                          
                         uint8* pHashBufOut,
                         uint8* pCipherBufOut)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;

  switch (ioCtlVal)
  {
    case CECL_IOCTL_SET_HASH_CIPHER_CNTXT:
      if (pHashBufIn!= NULL && pCipherBufIn!= NULL)
      { 
        retVal = CeCLIOCtlSetHashCipherCntx((CeCLHashAlgoCntxType*) pHashBufIn, (CeCLCipherCntxType*) pCipherBufIn);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_GET_HASH_CIPHER_CNTXT:
      if (pHashBufOut!= NULL && pCipherBufOut!= NULL)
      { 
        retVal = CeCLIOCtlGetHashCipherCntx((CeCLHashAlgoCntxType*) pHashBufOut, (CeCLCipherCntxType*) pCipherBufOut);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;
    default:
      retVal = CECL_ERROR_FAILURE;
      break;
  }

  return retVal;
}

/**
 * @brief This function sets various SHAx registers 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlSetHashCntx(CeCLHashAlgoCntxType *ctx_ptr)
{
  uint32 i                    = 0;
  uint32 iv_length_in_words   = 0;
  uint32 seg_cfg_val          = 0;
  uint32 temp_len;
  uint32 ce_block_size        = CECL_BLOCK_SIZE_NON_BAM;
  CeCLErrorType retVal        = CECL_ERROR_SUCCESS;

  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  //Clear encryption and authentication seg config registers
  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, 0);
  CeElMemoryBarrier();

  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, 0);
  CeElMemoryBarrier();

  //If BAM supported set ce block size to multiple of 64
  if(CECL_BAM_IS_SUPPORTED())
  {
    ce_block_size = CECL_BLOCK_SIZE_BAM;
  }

  if (CECL_HASH_ALGO_SHA1 == ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA1 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

    iv_length_in_words = CECL_SHA1_INIT_VECTOR_SIZE;

  }
  else if (CECL_HASH_ALGO_SHA256 == ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA256 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

     iv_length_in_words = CECL_SHA256_INIT_VECTOR_SIZE;
  }
  else
  {
     return CECL_ERROR_INVALID_PARAM;
  }

  if (ctx_ptr->lastBlock) 
  {
     seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, LAST)); 
  }

  /* Write Initialization Vector */
  for (i=0; i < iv_length_in_words; i++) 
  {
     HWIO_OUTI(CECL_CE_AUTH_IVn, i,   *(ctx_ptr->auth_iv + i));
  }
  CeElMemoryBarrier();

  HWIO_OUT( CECL_CE_AUTH_SEG_CFG,  seg_cfg_val );
  CeElMemoryBarrier();

  /* Write the AUTH_BYTECNT[0,1] registers, only 2 are valid at this time */
  HWIO_OUT(CECL_CE_AUTH_BYTECNT0, (ctx_ptr->auth_bytecnt[0]));
  HWIO_OUT(CECL_CE_AUTH_BYTECNT1, (ctx_ptr->auth_bytecnt[1]));
  CeElMemoryBarrier();

  /* Write the CRYPTO_CE_AUTH_SEG_SIZE register. AUTH_SIZE should be set
   * to buff_size */
  HWIO_OUT(CECL_CE_AUTH_SEG_SIZE, ctx_ptr->dataLn);
  CeElMemoryBarrier();

  /* Set the SEG START to 0 */
  HWIO_OUT(CECL_CE_AUTH_SEG_START, 0);
  CeElMemoryBarrier();

  /* Write the CRYPTO_CE_SEG_SIZE register: Multiple of 16 or 64 bytes depending on BAM
     or no BAM. For 1-pass SHA1, this value should be same as the total length */
  if (ctx_ptr->dataLn % ce_block_size)
  {
    /* Not a multiple of 16/64 bytes */
    temp_len = ctx_ptr->dataLn + (ce_block_size - (ctx_ptr->dataLn % ce_block_size));  
    HWIO_OUT(CECL_CE_SEG_SIZE, temp_len);
  }
  else 
  {
    HWIO_OUT(CECL_CE_SEG_SIZE, ctx_ptr->dataLn);
  }
  CeElMemoryBarrier();

  /* kick-off the crypto operation, the GOPROC is to be set once all the 
   * config registers are set. It has nothing to do with data */
  HWIO_OUT(CECL_CE_GOPROC, CECL_CE_GOPROC_GO_BMSK);
  CeElMemoryBarrier();

  while(1)
  {
    uint32 ce_state = HWIO_INF(CECL_CE_STATUS, CRYPTO_STATE);
    CeElMemoryBarrier();
    //We can change this to detect whether the PROCESSING bit is turned on. Currently
    //we do what the original drivers are doing i.e look for any bit set to indicate it's
    //not in idle state
    if(ce_state)
    {
      break;
    }
  }

  return retVal;
}

/**
 * @brief Get the SHA digest data from the Crypto HW registers
 *
 * @param 
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetHashDigest(uint32 *digest_ptr, uint32 digest_len)
{
   uint32 i = 0;
   uint32 ce_status;
   CeCLErrorType retVal = CECL_ERROR_SUCCESS;

   //Sanity check inputs 
   if (!digest_ptr || !digest_len)
   {
     return CECL_ERROR_INVALID_PARAM;
   }

   //Wait for operation done bit before we read HASH digest
   while(1)
   {
     ce_status = HWIO_IN(CECL_CE_STATUS);
     CeElMemoryBarrier();
     if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
     {
       break;
     }
   }

   //Get HASH digest data from CE registers 
   for(i = 0; i < digest_len / 4; i++) 
   {
     digest_ptr[i] = HWIO_INI(CECL_CE_AUTH_IVn, i);
   }
   CeElMemoryBarrier();

   return retVal;
}

/**
 * @brief Get the SHA context from the Crypto HW registers
 *
 * @param 
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetHashCntx(CeCLHashAlgoCntxType *ctx_ptr)
{
   uint32 i = 0;
   CeCLErrorType retVal        = CECL_ERROR_SUCCESS;
   uint32 ce_status;

   /* Sanity check inputs */
   if ((!ctx_ptr) || (((CECL_HASH_ALGO_SHA1 != ctx_ptr->algo)) && 
      ((CECL_HASH_ALGO_SHA256 != ctx_ptr->algo))))
   {
     return CECL_ERROR_INVALID_PARAM;
   }

   //Wait for operation done bit before we read HASH context
   while(1)
   {
     ce_status = HWIO_IN(CECL_CE_STATUS);
     CeElMemoryBarrier();
     if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
     {
       break;
     }
   }

   ctx_ptr->auth_bytecnt[0] = HWIO_IN(CECL_CE_AUTH_BYTECNT0); 
   ctx_ptr->auth_bytecnt[1] = HWIO_IN(CECL_CE_AUTH_BYTECNT1);
   CeElMemoryBarrier();

   /* Get HASH digest data */
   for(i = 0; i < 8; i++) 
   {
      ctx_ptr->auth_iv[i] = HWIO_INI(CECL_CE_AUTH_IVn, i);
   }
   CeElMemoryBarrier();

   return retVal;
}

/**
 * @brief  This function prepares the command buffer and then 
 *
 * @param ctx_ptr        [in]  Pointer to current context
 * @param buff_ptr       [in]  Pointer to input buffer
 * @param bytes_to_write [in]  Size of the input buffet
 * @param digest_ptr     [out] Pointer to output buffer
 * @param auth_alg       [in]  Algorithm typr 
 *  
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlHashRegXfer(CeCLHashXferType     *pBufOut)
{
  uint32 counter_1 = 0;
  uint32 last_seg_size = 0;
  uint32 segment_count = 0;
  uint8  last_segment[CECL_HW_SEC_ACC_DIN_SIZE];
  uint32 data_write = 0;
  uint8* data_ptr = NULL;
  CeCLErrorType ret_val = CECL_ERROR_FAILURE;
  uint32 is_data_aligned = 0;

  /* Sanity check inputs */
  if ((!pBufOut) || ( !pBufOut->buff_ptr ))
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  data_ptr = pBufOut->buff_ptr;

  segment_count = (pBufOut->buff_len) / CECL_HW_SEC_ACC_DIN_SIZE;
  last_seg_size = (pBufOut->buff_len) % CECL_HW_SEC_ACC_DIN_SIZE;

  //copy the last bytes into last_segment
  if(0 != last_seg_size)
  {
    uint8 i = 0;
    for(i=0; i<last_seg_size; i++)
    {
      last_segment[i] = (uint8)*((pBufOut->buff_ptr+(segment_count * CECL_HW_SEC_ACC_DIN_SIZE)) + i);
    }
  }

  // Check if the data is at an aligned address
  is_data_aligned = (((uint64)(data_ptr) & 0x03) == 0);

  // each segment has 16 bytes which is multiple of 4 bytes that can be copied to CE_DATA_IN
  for (counter_1=0; counter_1<(segment_count*4); counter_1++)
  { 
    while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

    /* Feed data into DATA_IN. We set crypto engine to little endian mode so don't need to */
    /* reverse the bytes */
    if (is_data_aligned)
    {
      //As the data is aligned at a 32 bit boundry we can just write it to the hw register
      HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_ptr));          
    }
    else
    {
      //Copy the data to an aligned address and then write it to the hw register
      //CeElMemcpy(&data_write, data_ptr, 4);  
      if (CeElMemscpy(&data_write, sizeof(data_write), data_ptr, 4)) return CECL_ERROR_FAILURE;
      HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
    }
    CeElMemoryBarrier();

    data_ptr = data_ptr + 4;
  }

  //fill  last segment
  if(0 != last_seg_size)
  {  
    for (counter_1=0; counter_1 < 4; counter_1++)
    {
      while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

      /* Feed data into DATA_IN */
      //CeElMemcpy(&data_write, (uint8*)(last_segment+(counter_1*4)), 4);
      if (CeElMemscpy(&data_write, sizeof(data_write), (uint8*)(last_segment+(counter_1*4)), 4)) return CECL_ERROR_FAILURE;
      HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);
      CeElMemoryBarrier();
    }
  }

  //wait for the hashing to complete
  while(1)
  {
      uint32 ce_status = HWIO_IN(CECL_CE_STATUS);
      CeElMemoryBarrier();

      if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
      {
        break;
      }
  }  

  /* Check for CE HW error */
  if (!(HWIO_IN(CECL_CE_STATUS) & 1))
  {
    ret_val = CECL_ERROR_SUCCESS;
  }
  CeElMemoryBarrier();

  return ret_val;
}

/**
 * @brief This function sets various SHAx registers 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlSetCipherCntx(CeCLCipherCntxType *ctx_ptr)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;
  uint32 seg_cfg_val = 0;
  uint32 auth_seg_cfg_val = 0;
  uint32 encr_seg_size  = 0;
  uint32 encr_seg_start  = 0;
  uint32 auth_seg_size  = 0;
  uint32 i = 0;

  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }
    
  //Clear encryption and authentication seg config registers
  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, 0);
  CeElMemoryBarrier();

  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, 0);
  CeElMemoryBarrier();

  /* Set ENCR mode */
  seg_cfg_val = ctx_ptr->mode << CECL_CE_ENCR_SEG_CFG_ENCR_MODE_SHFT;

  /* Set ENCR algo */
  seg_cfg_val |= CECL_CE_CIPHER_AES_ALGO_VAL << CECL_CE_ENCR_SEG_CFG_ENCR_ALG_SHFT;
  
       
  /* This bit should be set for encryption and clear otherwise.
     For CTR mode we need to run the engine in encrypt mode */  
  if (ctx_ptr->dir == CECL_CIPHER_ENCRYPT || ctx_ptr->mode == CECL_CIPHER_MODE_CTR)
  {
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT);
  }
    
  /* Set key size */
  if(ctx_ptr->algo == CECL_CIPHER_ALG_AES128)
  {
    //AES128
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES128 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
  else if(ctx_ptr->algo == CECL_CIPHER_ALG_AES256)
  {
    //AES256
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES256 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
    
  //Write out ENCR KEY if not using HW key                                                             
  if(!ctx_ptr->bAESUseHWKey) 
  {
    for (i=0; i < CECL_AES_MAX_KEY_SIZE; i++) 
    {     
      HWIO_OUTI(CECL_CE_ENCR_KEYn, i, ctx_ptr->aes_key[i]);
    }      
    CeElMemoryBarrier();
  }
  else 
  {
    //Use HW key 
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_USE_HW_KEY_ENCR_SHFT);
  }
  CeElMemoryBarrier();

  /* Set ENCR segment size */
  HWIO_OUT(CECL_CE_ENCR_SEG_SIZE, ctx_ptr->dataLn);
  CeElMemoryBarrier();
  HWIO_OUT(CECL_CE_ENCR_SEG_START, 0);
  CeElMemoryBarrier();

  /* Set CE seg size */
  HWIO_OUT(CECL_CE_SEG_SIZE, ctx_ptr->dataLn);
  CeElMemoryBarrier();

  //Write out ENCR CNTR IV vector 
  HWIO_OUT (CECL_CE_ENCR_CNTR0_IV0, ctx_ptr->iv[0]);
  HWIO_OUT (CECL_CE_ENCR_CNTR1_IV1, ctx_ptr->iv[1]);
  HWIO_OUT (CECL_CE_ENCR_CNTR2_IV2, ctx_ptr->iv[2]);
  HWIO_OUT (CECL_CE_ENCR_CNTR3_IV3, ctx_ptr->iv[3]);
  CeElMemoryBarrier();   

  HWIO_OUT (CECL_CE_ENCR_CNTR_MASK3, 0xFFFFFFFF);    
  HWIO_OUT (CECL_CE_ENCR_CNTR_MASK0, 0xFFFFFFFF);
  HWIO_OUT (CECL_CE_ENCR_CNTR_MASK1, 0xFFFFFFFF);
  HWIO_OUT (CECL_CE_ENCR_CNTR_MASK2, 0xFFFFFFFF);

  CeElMemoryBarrier();   


  //If CCM mode
  if (ctx_ptr->mode == CECL_CIPHER_MODE_CCM)
  {
    // if data length is less than or equal to header, then no data
    // needs to be encrypted/decrypted
    if (ctx_ptr->hdr_pad >= ctx_ptr->dataLn)
    {
      encr_seg_start = 0;
      encr_seg_size  = 0;
    }
    else
    {
      encr_seg_start = ctx_ptr->hdr_pad;
      encr_seg_size  = ctx_ptr->dataLn - ctx_ptr->hdr_pad;
    }

    if (ctx_ptr->dir==CECL_CIPHER_ENCRYPT)
      {
        auth_seg_size  = ctx_ptr->dataLn;
    }
    else  // DECRYPT
    {
      auth_seg_size = ctx_ptr->outdataLn;
    }

      HWIO_OUT(CECL_CE_SEG_SIZE, ctx_ptr->dataLn);
      CeElMemoryBarrier();
    
    //Set Encr seg start & size
    HWIO_OUT(CECL_CE_ENCR_SEG_START, encr_seg_start);
    CeElMemoryBarrier();
    HWIO_OUT(CECL_CE_ENCR_SEG_SIZE, encr_seg_size);
    CeElMemoryBarrier();
    
    //Set Auth seg start & size
    HWIO_OUT(CECL_CE_AUTH_SEG_START, 0);
    CeElMemoryBarrier();
    HWIO_OUT(CECL_CE_AUTH_SEG_SIZE, auth_seg_size);
    CeElMemoryBarrier();

    //Clear AUTH BYTECNT regs
    HWIO_OUT(CECL_CE_AUTH_BYTECNT0, ctx_ptr->auth_bytecnt[0]);
    CeElMemoryBarrier();
    HWIO_OUT(CECL_CE_AUTH_BYTECNT1, ctx_ptr->auth_bytecnt[1]);
    CeElMemoryBarrier();

    //Check for first block
    if (ctx_ptr->firstBlock) 
    {
      //CeElMemcpy(ctx_ptr->ccm_cntr, ctx_ptr->iv, CECL_AES_MAX_IV_SIZE_BYTES);
      if (CeElMemscpy(ctx_ptr->ccm_cntr, CECL_AES_MAX_IV_SIZE_BYTES, ctx_ptr->iv, CECL_AES_MAX_IV_SIZE_BYTES)) return CECL_ERROR_FAILURE;

      //Set AUTH first block bit
      auth_seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, FIRST)); 

      //+1 for CNTR IV for CCM mode
      ctx_ptr->iv[3] += 0x01000000;
      if(!(ctx_ptr->iv[3] & 0xffffffff))
      {
        ctx_ptr->iv[2] += 0x01000000;

        if(!(ctx_ptr->iv[2] & 0xffffffff))
        {
          ctx_ptr->iv[1] += 0x01000000;
          if(!(ctx_ptr->iv[1] & 0xffffffff))
          {
            ctx_ptr->iv[0] += 0x01000000;
          }
        }
      }
    }

    //Write out Encr CCM Cntr regs
    for (i=0; i < CECL_AES_CCM_CNTR_VECTOR_SIZE; i++) 
    {
      HWIO_OUTI(CECL_CE_ENCR_CCM_INIT_CNTRn, i, ctx_ptr->ccm_cntr[i]);
    }      
    CeElMemoryBarrier();
  
    HWIO_OUT (CECL_CE_ENCR_CNTR0_IV0, ctx_ptr->iv[0]);
    HWIO_OUT (CECL_CE_ENCR_CNTR1_IV1, ctx_ptr->iv[1]);
    HWIO_OUT (CECL_CE_ENCR_CNTR2_IV2, ctx_ptr->iv[2]);
    HWIO_OUT (CECL_CE_ENCR_CNTR3_IV3, ctx_ptr->iv[3]);
      CeElMemoryBarrier();

      /* Clear the AES Auth Initialization Vector */
      for (i=0; i < CECL_AES_CCM_INIT_VECTOR_SIZE; i++) 
      {
      HWIO_OUTI(CECL_CE_AUTH_IVn, i, ctx_ptr->auth_iv[i]);
      }
      CeElMemoryBarrier();

    //Check for last block
    if (ctx_ptr->lastBlock) 
    {
      //Set ENCR last block bit
      seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_ENCR_SEG_CFG, LAST));
      
      //Set AUTH last block bit
      auth_seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, LAST));          
    }

    /* Write AES key to AUTH Key registers if not using HW key */
    if(!ctx_ptr->bAESUseHWKey) 
    {
      for (i=0; i < CECL_AES_MAX_KEY_SIZE; i++) 
      {
        HWIO_OUTI(CECL_CE_AUTH_KEYn, i, ctx_ptr->aes_key[i]);
      }      
      CeElMemoryBarrier(); 
    }
    else
    {
      /* Use HW key */
      auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_USE_HW_KEY << CECL_CE_AUTH_SEG_CFG_AUTH_USE_HW_KEY_SHFT;
    }

    /* Write out Auth Info Nonce data */
    for (i=0; i < CECL_AES_CCM_NONCE_VECTOR_SIZE; i++) 
    {
      HWIO_OUTI(CECL_CE_AUTH_INFO_NONCEn, i, ctx_ptr->nonce[i]);
    }      
    CeElMemoryBarrier();

    /* Set up Auth Key Size values to write to register */
    if (ctx_ptr->algo == CECL_CIPHER_ALG_AES128)
    {//set AES128 key size
      auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_128 << CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_SHFT;
    }
    else
    {//set AES256 key size
      auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_256 << CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_SHFT;
    }
 
    //Set Auth sequence and cal Mac Length
    if (ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
    {
      auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_POS_BEFORE << CECL_CE_AUTH_SEG_CFG_AUTH_POS_SHFT;
    }
    else
    {
      auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_POS_AFTER << CECL_CE_AUTH_SEG_CFG_AUTH_POS_SHFT;
  }
 
    /* Set Algorithm, Mode, Auth size and Nonce Size values */
    auth_seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_ALG_AES  << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT  |
                        CECL_CE_AUTH_SEG_CFG_AUTH_MODE_CCM << CECL_CE_AUTH_SEG_CFG_AUTH_MODE_SHFT |
                        (ctx_ptr->macLn - 1)               << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT |
                        (CECL_AES_NONCE_SIZE_BYTES / 4)    << CECL_CE_AUTH_SEG_CFG_AUTH_NONCE_SHFT; 

  } /* end CCM if */

  /* Write out Auth and Encr seg cfg values */
  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, auth_seg_cfg_val);
  CeElMemoryBarrier(); 

  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, seg_cfg_val);
  CeElMemoryBarrier();

  /* kick-off the crypto operation, the GOPROC is to be set once all the 
   * config registers are set. It has nothing to do with data */
  if (ctx_ptr->bAESUseHWKey)       
  {    
    CECL_SYS_SEC_CRYPTO_GOPROC_OEM_KEY();
  }
  else 
  {
    HWIO_OUT(CECL_CE_GOPROC, CECL_CE_GOPROC_GO_BMSK);
  }
  CeElMemoryBarrier();
   
  while(1)
  {
    uint32 ce_state = HWIO_INF(CECL_CE_STATUS, CRYPTO_STATE);
    CeElMemoryBarrier();
    //We can change this to detect whether the PROCESSING bit is turned on. Currently
    //we do what the original drivers are doing i.e look for any bit set to indicate it's
    //not in idle state
    if(ce_state)
    {
      break;
    }
  }
 
  return retVal;
}
    
/** 
 * @brief Get the AES context from the HW registers
 *
 * @param ctx_ptr    [in] Pointer to current context
 * @param auth_alg   [in] Algorithm type 
 *
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetCipherCntx(CeCLCipherCntxType *ctx_ptr)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;
  uint32 auth_iv_ctr = 0;
 
  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  ctx_ptr->iv[0] = HWIO_IN(CECL_CE_ENCR_CNTR0_IV0);
  ctx_ptr->iv[1] = HWIO_IN(CECL_CE_ENCR_CNTR1_IV1);
  ctx_ptr->iv[2] = HWIO_IN(CECL_CE_ENCR_CNTR2_IV2);
  ctx_ptr->iv[3] = HWIO_IN(CECL_CE_ENCR_CNTR3_IV3);
  CeElMemoryBarrier(); 

  if(ctx_ptr->mode == CECL_CIPHER_MODE_CCM)
  {
    for(auth_iv_ctr = 0; auth_iv_ctr < CECL_AES_MAX_IV_SIZE_BYTES/4; auth_iv_ctr++)
    {
      ctx_ptr->auth_iv[auth_iv_ctr] = HWIO_INI(CECL_CE_AUTH_IVn, auth_iv_ctr);
    }
    CeElMemoryBarrier(); 

    ctx_ptr->auth_bytecnt[0] = HWIO_IN(CECL_CE_AUTH_BYTECNT0);
    ctx_ptr->auth_bytecnt[1] = HWIO_IN(CECL_CE_AUTH_BYTECNT1);
    CeElMemoryBarrier(); 
  }

  return retVal;
}

/**
 * @brief  This function prepares the command buffer and then 
 *
 * @param ctx_ptr        [in]  Pointer to current context
 * @param buff_ptr       [in]  Pointer to input buffer
 * @param bytes_to_write [in]  Size of the input buffet
 * @param digest_ptr     [out] Pointer to output buffer
 * @param auth_alg       [in]  Algorithm typr 
 *  
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlCipherRegXfer(CeCLCipherXferType *pBufOut)
{
  uint32 counter_1 = 0;
  uint32 counter_2 = 0;
  uint32 counter_3 = 0;
  uint32 segment_count=0;  
  CeCLErrorType ret_val = CECL_ERROR_SUCCESS;
  uint8* data_in = pBufOut->pDataIn;
  uint8* data_out = pBufOut->pDataOut;  
  uint32 data_write = 0;
  uint32 is_data_aligned = 0;  
  uint8  last_segment[CECL_HW_SEC_ACC_DIN_SIZE] = {0};
  uint32 last_seg_size = 0;
  uint32 seg_indx = 0;
  //uint8  ccm_mac[CECL_AES_CCM_MAC_LEN_16] = {0};
  //uint8* mac_out = ccm_mac;
  CeCLCipherCntxType *ctx_ptr;
  uint32 dummy_data_write = 0;
  
  /* Sanity check inputs */
   if (!pBufOut->pDataIn || !pBufOut->nDataLen || !pBufOut->pDataOut || !pBufOut->nDataOutLen || !pBufOut->pCntx)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  //Set context pointer
  ctx_ptr = (CeCLCipherCntxType *)pBufOut->pCntx;

  if (ctx_ptr->mode == CECL_CIPHER_MODE_CCM)
  {
    ret_val = CeCLIOCtlCipherCCMRegXfer(pBufOut);
  }
  else
  {

	is_data_aligned = (((uint64)(data_in) & 0x03) == 0);
    segment_count = pBufOut->nDataLen  / CECL_HW_SEC_ACC_DIN_SIZE;
    last_seg_size = pBufOut->nDataLen % CECL_HW_SEC_ACC_DIN_SIZE;

    //Takes care of use cases where the size is not multiple of 16 bytes
    for(counter_1=0; counter_1<last_seg_size; counter_1++)
    {
      last_segment[counter_1] = (uint8)*((data_in+(segment_count * CECL_HW_SEC_ACC_DIN_SIZE)) + counter_1);
    }

    //Cal last segment index
    CeElCeil(last_seg_size,2,&seg_indx);

    for (counter_1=0; counter_1<segment_count;counter_1++)
    {
      /* Feed data in 4 bytes at a time to CE */ 
      for (counter_2=0; counter_2<4; counter_2++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DIN_RDY); 

        if (is_data_aligned)
        {
          //As the data is aligned at a 32 bit boundry we can just write it to the hw register
          HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_in));          
        }
        else
        {
          //Copy the data to an aligned address and then write it to the hw register
          //CeElMemcpy(&data_write, data_in, 4);  
          if (CeElMemscpy(&data_write, sizeof(data_write), data_in, 4)) return CECL_ERROR_FAILURE;
          HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
        }	  
        CeElMemoryBarrier();

        data_in += 4;
      }

      //For AES CBC mode handle non multiple of 16 data after processing all multiple of 16 data
      if ((ctx_ptr->mode == CECL_CIPHER_MODE_CBC) && (counter_1 == segment_count - 1))
      {
        //Takes care of use cases where the size is not multiple of 16 bytes
        for (counter_2=0; counter_2 < seg_indx; counter_2++)
        {
          while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

          /* Feed data into CE DATA_IN */
          //CeElMemcpy(&data_write, (uint8*)(last_segment+(counter_2*4)), 4);
	      if (CeElMemscpy(&data_write, sizeof(data_write), (uint8*)(last_segment+(counter_2*4)), 4)) return CECL_ERROR_FAILURE;
          HWIO_OUTI(CECL_CE_DATA_INn, counter_2, data_write);
          CeElMemoryBarrier();
        }
        if (seg_indx!=0) {
        
           for (counter_2=0; counter_2 < (4 - seg_indx ); counter_2++)
           {
              while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);
              /* Feed data into CE DATA_IN */
              HWIO_OUTI(CECL_CE_DATA_INn, counter_2, dummy_data_write);
         }
        }
      }

      /* Store data out 4 bytes at a time */
      for (counter_3=0; counter_3 < 4; counter_3++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 
  
        //Read data from CE
        *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
        CeElMemoryBarrier();
        data_out += 4;
      }

      //For AES CBC mode handle non multiple of 16 data after processing all multiple of 16 data
      if ((ctx_ptr->mode == CECL_CIPHER_MODE_CBC) && (counter_1 == segment_count - 1))
      {
        //Takes care of use cases where the size is not multiple of 16 bytes
        for (counter_3=0; counter_3 < seg_indx; counter_3++)
        {
          while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

          //Read data from CE
          *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
          CeElMemoryBarrier();
          data_out += 4;
        }
        if (seg_indx!=0) {
               for (counter_3=0; counter_3 < 4 - seg_indx; counter_3++)
           {
             while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 
             //Read data from CE
             dummy_data_write = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
           }
        }

      }
    }

    //For non AES CBC mode handle non multiple of 16 data after processing all multiple of 16 data
    if (ctx_ptr->mode != CECL_CIPHER_MODE_CBC)
    {  
      //Takes care of use cases where the size is not multiple of 16 bytes
      for (counter_1=0; counter_1 < seg_indx; counter_1++)
      {
        while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

        /* Feed data into CE DATA_IN */
        //CeElMemcpy(&data_write, (uint8*)(last_segment+(counter_1*4)), 4);
	if (CeElMemscpy(&data_write, sizeof(data_write), (uint8*)(last_segment+(counter_1*4)), 4)) return CECL_ERROR_FAILURE;
        HWIO_OUTI(CECL_CE_DATA_INn, counter_1, data_write);
        CeElMemoryBarrier();
      }

      //Takes care of use cases where the size is not multiple of 16 bytes
      for (counter_1=0; counter_1 < seg_indx; counter_1++)
      {

      while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

      //Read data from CE
      *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_1);
      CeElMemoryBarrier();
      data_out += 4;
    }
  }

    if ((HWIO_IN(CECL_CE_STATUS) & 1))
    {
      ret_val = CECL_ERROR_FAILURE;
    }
  CeElMemoryBarrier();

  }

  return ret_val;
}

/**
 * @brief This function sets various SHA/AES registers 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlSetHashCipherCntx(CeCLHashAlgoCntxType *hash_ctx_ptr, CeCLCipherCntxType *cipher_ctx_ptr)
{
  uint32 i                    = 0;
  uint32 iv_length_in_words   = 0;
  uint32 seg_cfg_val          = 0;
  uint32 temp_len;
  uint32 ce_block_size        = CECL_BLOCK_SIZE_NON_BAM;
  CeCLErrorType retVal        = CECL_ERROR_SUCCESS;

  /* Sanity check inputs */  
  if (!hash_ctx_ptr || !cipher_ctx_ptr)  
  {    
    return CECL_ERROR_INVALID_PARAM;  
  }  

  //Clear encryption and authentication seg config registers  

  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, 0);  
  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, 0);
  CeElMemoryBarrier();

  //If BAM supported set ce block size to multiple of 64
  if(CECL_BAM_IS_SUPPORTED())
  {
    ce_block_size = CECL_BLOCK_SIZE_BAM;
  }

  if (CECL_HASH_ALGO_SHA1 == hash_ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA1 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

    iv_length_in_words = CECL_SHA1_INIT_VECTOR_SIZE;

  }
  else if (CECL_HASH_ALGO_SHA256 == hash_ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA256 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

     iv_length_in_words = CECL_SHA256_INIT_VECTOR_SIZE;
  }
  else
  {
     return CECL_ERROR_INVALID_PARAM;
  }

  if (hash_ctx_ptr->mode == CECL_HASH_MODE_HMAC)
  {
    seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_MODE_HMAC << CECL_CE_AUTH_SEG_CFG_AUTH_MODE_SHFT;
  }
  else
  {
    seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_MODE_HASH << CECL_CE_AUTH_SEG_CFG_AUTH_MODE_SHFT;
  }


  if (hash_ctx_ptr->firstBlock) 
  {
     seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, FIRST)); 
  }

  if (hash_ctx_ptr->lastBlock) 
  {
     seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, LAST)); 
  }

  if (hash_ctx_ptr->seq == CECL_HASH_FIRST)
  { 
      seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_POS_BEFORE << CECL_CE_AUTH_SEG_CFG_AUTH_POS_SHFT;
  }
  else if(hash_ctx_ptr->seq == CECL_HASH_LAST)
  {
      seg_cfg_val |= CECL_CE_AUTH_SEG_CFG_AUTH_POS_AFTER << CECL_CE_AUTH_SEG_CFG_AUTH_POS_SHFT;
  }

  /* Write Initialization Vector */
  for (i=0; i < iv_length_in_words; i++) 
  {
     HWIO_OUTI(CECL_CE_AUTH_IVn, i, *(hash_ctx_ptr->auth_iv + i));
  }
  CeElMemoryBarrier();

  /* Write the Key for HMAC */
  if (hash_ctx_ptr->mode == CECL_HASH_MODE_HMAC)
  {
    for (i=0; i < CECL_HMAC_MAX_KEY_SIZE; i++) 
    {
      HWIO_OUTI(CECL_CE_AUTH_KEYn, i, hash_ctx_ptr->hmac_key[i]);
    }
  }
  CeElMemoryBarrier();

  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, seg_cfg_val);
  CeElMemoryBarrier();

  /* Write the AUTH_BYTECNT[0,1] registers, only 2 are valid at this time */ 
  HWIO_OUT(CECL_CE_AUTH_BYTECNT0, (hash_ctx_ptr->auth_bytecnt[0]));
  HWIO_OUT(CECL_CE_AUTH_BYTECNT1, (hash_ctx_ptr->auth_bytecnt[1]));
  CeElMemoryBarrier();

  /* Write the CRYPTO_CE_AUTH_SEG_SIZE register. AUTH_SIZE should be set
   * to buff_size */
  HWIO_OUT(CECL_CE_AUTH_SEG_SIZE, hash_ctx_ptr->dataLn);
  CeElMemoryBarrier();

  /* Set the SEG START to 0 */
  HWIO_OUT(CECL_CE_AUTH_SEG_START, 0);
  CeElMemoryBarrier();

  seg_cfg_val = cipher_ctx_ptr->mode << CECL_CE_ENCR_SEG_CFG_ENCR_MODE_SHFT;

  /* Set algo to AES */
  seg_cfg_val |= CECL_CE_CIPHER_AES_ALGO_VAL << CECL_CE_ENCR_SEG_CFG_ENCR_ALG_SHFT;
    
  /* this bit should be set for encryption and clear otherwise */
  if (cipher_ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
  {
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT);
  }


  if (cipher_ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
  {
    seg_cfg_val |= CECL_CE_ENCR_SEG_CFG_ENCODE_ENC << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT;
  }
  else if (cipher_ctx_ptr->dir == CECL_CIPHER_DECRYPT)
  {
    seg_cfg_val |= CECL_CE_ENCR_SEG_CFG_ENCODE_DEC << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT;
  }
       
  /* For counter mode operation we need to run the engine in encrypt mode
     the XOR �undoes" the encrypted data into decrypted data.*/
  if (cipher_ctx_ptr->mode == CECL_CIPHER_MODE_CTR) 
  {
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT);
  }
    
  /* Set AES key size */
  if(cipher_ctx_ptr->algo == CECL_CIPHER_ALG_AES128)
  {
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES128 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
  else if(cipher_ctx_ptr->algo == CECL_CIPHER_ALG_AES256)
  {
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES256 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
    
  /* Use HW key ? */
  if (cipher_ctx_ptr->bAESUseHWKey) 
  {     
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_USE_HW_KEY_ENCR_SHFT);
    CECL_SYS_SEC_CRYPTO_CFG();
  }
  else 
  {
    HWIO_OUT( CECL_CE_ENCR_SEG_CFG, seg_cfg_val );
  }
  CeElMemoryBarrier();

  /* encryption/decryption segment configuration */
  HWIO_OUT(CECL_CE_ENCR_SEG_SIZE, cipher_ctx_ptr->dataLn);
  CeElMemoryBarrier();
  HWIO_OUT(CECL_CE_ENCR_SEG_START, 0);
  CeElMemoryBarrier();

  //Set CE seg size
  /*if(cipher_ctx_ptr->mode == CECL_CIPHER_MODE_CBC && !CECL_BAM_IS_SUPPORTED())
  {
    //Set CE seg size if CBC Register based mode
    HWIO_OUT(CECL_CE_SEG_SIZE, cipher_ctx_ptr->dataLn);
  }*/
  //Write the CRYPTO_CE_SEG_SIZE register: Multiple of 16 or 64 bytes depending on BAM or no BAM 
  if ((cipher_ctx_ptr->dataLn) % ce_block_size)
  {
    /* Not a multiple of 16/64 bytes */
    temp_len = cipher_ctx_ptr->dataLn + (ce_block_size - (cipher_ctx_ptr->dataLn % ce_block_size));  
    HWIO_OUT(CECL_CE_SEG_SIZE, temp_len);
  }
  else 
  {
    /* Multiple of 16/64 bytes */
    HWIO_OUT(CECL_CE_SEG_SIZE, cipher_ctx_ptr->dataLn);
  }  
  CeElMemoryBarrier();

  HWIO_OUT (CECL_CE_ENCR_CNTR0_IV0, cipher_ctx_ptr->iv[0]);
  HWIO_OUT (CECL_CE_ENCR_CNTR1_IV1, cipher_ctx_ptr->iv[1]);
  HWIO_OUT (CECL_CE_ENCR_CNTR2_IV2, cipher_ctx_ptr->iv[2]);
  HWIO_OUT (CECL_CE_ENCR_CNTR3_IV3, cipher_ctx_ptr->iv[3]);
  HWIO_OUT (CECL_CE_ENCR_CNTR_MASK3, 0xFFFFFFFF);
  if (!CEEL_CE_HW_IS_V1())
  {
    HWIO_OUT (CECL_CE_ENCR_CNTR_MASK0, 0xFFFFFFFF);
    HWIO_OUT (CECL_CE_ENCR_CNTR_MASK1, 0xFFFFFFFF);
    HWIO_OUT (CECL_CE_ENCR_CNTR_MASK2, 0xFFFFFFFF);
  }
  CeElMemoryBarrier(); 
  
  // write to aes round key RAM                                                              
  if(!cipher_ctx_ptr->bAESUseHWKey) 
  {
    uint32 j = 0;
    uint32 i = 0;
    if (cipher_ctx_ptr->algo == CECL_CIPHER_ALG_AES128)
    {
      j = CECL_AES128_KEY_SIZE/4;
    }
    else if(cipher_ctx_ptr->algo == CECL_CIPHER_ALG_AES256)
    {
      j = CECL_AES256_KEY_SIZE/4;
    }
    for (i=0; i < j; i++) 
    {
      HWIO_OUTI(CECL_CE_ENCR_KEYn, i, cipher_ctx_ptr->aes_key[i]);
    }      
      CeElMemoryBarrier();
  }

  /* kick-off the crypto operation, the GOPROC is to be set once all the 
   * config registers are set. It has nothing to do with data */
  if (cipher_ctx_ptr->bAESUseHWKey)       
  {    
    CECL_SYS_SEC_CRYPTO_GOPROC_QC_KEY();
  }
  else 
  {
    HWIO_OUT(CECL_CE_GOPROC, CECL_CE_GOPROC_GO_BMSK);
  }
  CeElMemoryBarrier(); 

  while(1)
  {
    uint32 ce_state = HWIO_INF(CECL_CE_STATUS, CRYPTO_STATE);
    CeElMemoryBarrier();
    //We can change this to detect whether the PROCESSING bit is turned on. Currently
    //we do what the original drivers are doing i.e look for any bit set to indicate it's
    //not in idle state
    if(ce_state)
    {
      break;
    }
  }
 
  return retVal;
}

/**
 * @brief Get the SHA/AES context from the Crypto HW registers
 *
 * @param 
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetHashCipherCntx(CeCLHashAlgoCntxType *hash_ctx_ptr, CeCLCipherCntxType *cipher_ctx_ptr)
{
   uint32 i = 0;
   CeCLErrorType retVal        = CECL_ERROR_SUCCESS;

   /* Sanity check inputs */
   if ((!hash_ctx_ptr) || (((CECL_HASH_ALGO_SHA1 != hash_ctx_ptr->algo)) && 
      ((CECL_HASH_ALGO_SHA256 != hash_ctx_ptr->algo))))
   {
     return CECL_ERROR_INVALID_PARAM;
   }

   /* Sanity check inputs */  
   if (!cipher_ctx_ptr)
   {
     return CECL_ERROR_INVALID_PARAM;  
   }
  
  CeCLIOCtlHashCompletion();
  hash_ctx_ptr->auth_bytecnt[0] = HWIO_IN(CECL_CE_AUTH_BYTECNT0); 
  hash_ctx_ptr->auth_bytecnt[1] = HWIO_IN(CECL_CE_AUTH_BYTECNT1);
  CeElMemoryBarrier();
  
  for(i = 0; i < 8; i++) 
  {
     hash_ctx_ptr->auth_iv[i] = HWIO_INI(CECL_CE_AUTH_IVn, i);
  }
  CeElMemoryBarrier();

  cipher_ctx_ptr->iv[0] = HWIO_IN(CECL_CE_ENCR_CNTR0_IV0);
  cipher_ctx_ptr->iv[1] = HWIO_IN(CECL_CE_ENCR_CNTR1_IV1);
  cipher_ctx_ptr->iv[2] = HWIO_IN(CECL_CE_ENCR_CNTR2_IV2);
  cipher_ctx_ptr->iv[3] = HWIO_IN(CECL_CE_ENCR_CNTR3_IV3);
  CeElMemoryBarrier(); 

   return retVal;
}


/**
 * @brief This function reverses the bytes for each four byte 
 *        set in the input data
 *
 * @param out_bytes_ptr  [out] Pointer to output data
 * @param in_bytes_ptr   [in]  Pointer to input data
 * @param byte_count     [in]  Length of data in bytes. 
 *
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLReverseBytes(uint8 *out_bytes_ptr, 
                               uint8 *in_bytes_ptr, 
                               uint64 byte_count)
{
  uint64 n = 0;
  uint8  tmp_byte = 0;

  /* Sanity check inputs */
  if (!out_bytes_ptr || !in_bytes_ptr || !byte_count)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  for (n = 0; n < byte_count / 4; n++) 
  {
     //swap first byte and fourth byte
     tmp_byte = in_bytes_ptr[4*n + 0];
     out_bytes_ptr[4*n + 0] =  in_bytes_ptr[4*n + 3];
     out_bytes_ptr[4*n + 3] =  tmp_byte;

     //swap second byte and third byte
     tmp_byte = in_bytes_ptr[4*n + 1];
     out_bytes_ptr[4*n + 1] =  in_bytes_ptr[4*n + 2];
     out_bytes_ptr[4*n + 2] =  tmp_byte;
  }

  return CECL_ERROR_SUCCESS;
}
