#ifndef _AeCL_NativeCrypto
#define _AeCL_NativeCrypto
//-----------------------------------------------------------------------------
// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2012-2013 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//
//      SVN Information
//
//      Checked In          : $Date: 2015/01/31 $
//
//      Revision            : $Revision: #1 $
//
//      Release Information :
//
//-----------------------------------------------------------------------------
#include "AeEL_Env.h"

/*===========================================================================
*
* FUNCTION: armv8ni_aes128_key_expand()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_aes128_key_expand(const AUCHAR *key_in, AUCHAR *key_out);


/*===========================================================================
*
* FUNCTION: armv8ni_ecb_aes128_encrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_ecb_aes128_encrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AUINT size);

/*===========================================================================
*
* FUNCTION: armv8ni_cbc_aes128_encrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_cbc_aes128_encrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AULONG* argument_list);

/*===========================================================================
*
* FUNCTION: armv8ni_ecb_aes128_decrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_ecb_aes128_decrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AUINT size);

/*===========================================================================
*
* FUNCTION: armv8ni_cbc_aes128_decrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_cbc_aes128_decrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AULONG* argument_list);

/*===========================================================================
*
* FUNCTION: armv8ni_aes256_key_expand()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_aes128_key_expand(const AUCHAR *key_in, AUCHAR *key_out);

/*===========================================================================
*
* FUNCTION: armv8ni_ecb_aes256_encrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_ecb_aes256_encrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AUINT size);

/*===========================================================================
*
* FUNCTION: armv8ni_cbc_aes256_encrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
extern void armv8ni_cbc_aes256_encrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AULONG* argument_list);

/*===========================================================================
*
* FUNCTION: armv8ni_ecb_aes256_decrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_ecb_aes256_decrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AUINT size);

/*===========================================================================
*
* FUNCTION: armv8ni_cbc_aes256_decrypt()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
extern void armv8ni_cbc_aes256_decrypt(const AUCHAR *key, const AUCHAR *in_data, AUCHAR *out_data, AULONG* argument_list);

/*===========================================================================
*
* FUNCTION: armv8ni_aes256_key_expand()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
extern void armv8ni_aes256_key_expand(const AUCHAR *key_in, AUCHAR *key_out);

/*===========================================================================
*
* FUNCTION: armv8ni_sha1_transform()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
extern void armv8ni_sha1_transform(uint32* digest, AUCHAR* data, uint32 rounds);

/*===========================================================================
*
* FUNCTION: armv8ni_sha256_transform()
*
* DESCRIPTION:
*
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
extern void armv8ni_sha256_transform( uint32 *digest, const AUCHAR *data, uint32 rounds );

#endif // _AeCL_NativeCrypto
