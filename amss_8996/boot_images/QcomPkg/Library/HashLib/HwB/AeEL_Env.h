#ifndef _AeEL_Env
#define _AeEL_Env

/*===========================================================================

                   ArmV8 Crypto Engine Test Environment API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014-2015 Copyright Qualcomm Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
 06/04/14   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"
#include "string.h"
#include "boot_comdef.h"
//#include "boot_cache_mmu.h"  

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
#define ARMARCH64
#if defined ARMARCH32
  #define AUCHAR uint8
  #define AUINT uint32
  #define AULONG uint32
#elif defined ARMARCH64
  #define AUCHAR uint8
  #define AUINT uint64
  #define AULONG uint64
#endif

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
//Set up clocks
#define AeElClkEnable()   AeClClockEnable()
#define AeElClkDisable()  AeClClockDisable()

#define AEEL_MUTEX_TYPE 

#define AEEL_MUTEX_ENTER()

#define AEEL_MUTEX_EXIT() 

#define AEEL_MEMORY_BARRIER() memory_barrier()

typedef enum
{
  AEEL_ENV_ERROR_SUCCESS                = 0x0,
  AEEL_ENV_ERROR_FAILURE                = 0x1,
  AEEL_ENV_ERROR_INVALID_PARAM          = 0x2,
  AEEL_ENV_ERROR_NOT_SUPPORTED          = 0x3,
  AEEL_ENV_ERROR_NO_MEMORY              = 0x4,

} AeEL_EnvErrorType;

AeEL_EnvErrorType AeElmalloc_Env(void** ptr, uint32 ptrLen);

AeEL_EnvErrorType AeElfree_Env(void* ptr);

#endif //_AeEL_Env



