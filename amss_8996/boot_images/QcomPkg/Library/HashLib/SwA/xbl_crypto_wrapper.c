/**
@file xbl_crypto_wrapper.c
@brief Wraper for crypto driver in xbl
*/

/**********************************************************************
 * Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 **********************************************************************/
/*======================================================================

                        EDIT HISTORY FOR MODULE
 

when         who     what, where, why
--------     ---     --------------------------------------------------- 
2015-01-08   bh      Include com_dtypes.h and string.h
2014-12-22   yk      xbl crypto wrapper
=======================================================================*/


#include "CeML.h"
#include "com_dtypes.h"
#include <string.h>
#include <stringl/stringl.h>
#include <Library/BaseMemoryLib.h>
//#include "sha_shared.h"

#define SW_SHA1_DIGEST_SIZE             20
#define SW_SHA256_DIGEST_SIZE             32
#define SW_SHA_BLOCK_SIZE               64

typedef enum
{
  SW_CORE_AUTH_ALG_SHA1               = 0x1,
  SW_CORE_AUTH_ALG_SHA256             = 0x2
} HashAlgoType;

typedef enum
{
  SW_AUTH_ALG_SHA1               = 0x1,
  SW_AUTH_ALG_SHA256
} SW_Auth_Alg_Type;

typedef struct 
{
	uint32  counter[2];
	uint32  iv[16];
	uint32  auth_iv[8]; 
	uint8   leftover[SW_SHA_BLOCK_SIZE];
	uint32  leftover_size;
	SW_Auth_Alg_Type auth_alg;
} SW_SHA_Ctx;

typedef void CryptoCntxHandle;

typedef struct {
  void                              *pvBase; 
  uint32                            dwLen;  
} IovecType;

typedef  struct {
  IovecType                     *iov;                 
  uint32                            size;                 
} IovecListType;

struct __sechsh_ctx_s
{
   uint32  iv[16];  // is 64 byte for SHA2-512
   uint32  counter[2];
   HashAlgoType auth_alg;
   uint32  auth_iv[8]; 
   uint8   leftover[SW_SHA_BLOCK_SIZE];
   uint32  leftover_size;
};

typedef struct 
{
  uint64 CeMLIovecTypeBuff[2]; 
  uint64 used;
} CeMLIovecType;

typedef struct 
{
  uint64 CeMLTypeBuff[31]; 
  uint64 used;
} CeMLIovecType2;

CeMLIovecType  CeMLIovecTypeArray[8];
CeMLIovecType2 CeMLHashCipherAlgoCntxTypeBuff[2];

CeMLErrorType SW_Hash_Update(CryptoCntxHandle *handle,IovecListType ioVecIn);
CeMLErrorType SW_Hash_Final (CryptoCntxHandle *handle,IovecListType *ioVecOut);
CeMLErrorType SW_Hash_Init(CryptoCntxHandle **handle, SW_Auth_Alg_Type auth_alg);

extern void sechsharm_sha1_init(   struct __sechsh_ctx_s* );
extern void sechsharm_sha1_update( struct __sechsh_ctx_s*, uint8*, uint32*, uint8*, uint32 );
extern void sechsharm_sha1_final(  struct __sechsh_ctx_s*, uint8*, uint32*, uint8* );
extern void sechsharm_sha1_transform( uint32*, unsigned char* );
#define SHA256_BLOCK_LENGTH    64
extern void sechsharm_sha256_init(   struct __sechsh_ctx_s* );
extern void sechsharm_sha256_update( struct __sechsh_ctx_s*, uint8*, uint32*, uint8*, uint32 );
extern void sechsharm_sha256_final(  struct __sechsh_ctx_s*, uint8*, uint32*, uint8* );

/* ZI type malloc helper */
CeMLErrorType CeLoaderMalloc(void** ptr, uint64 ptrLen)
{  
  uint32 i = 0;
  
  if (ptrLen < 16) 
  {
    for(i=0; i < 8; i++)
    {
      if (CeMLIovecTypeArray[i].used  == 1) continue;
      else
      {
        *ptr = (void*)CeMLIovecTypeArray[i].CeMLIovecTypeBuff;
  		CeMLIovecTypeArray[i].used = 1;
        break;
      }
    }
  }
  else if (ptrLen >= 16 && ptrLen < 248) 
  {
    if (CeMLHashCipherAlgoCntxTypeBuff[0].used == 0)
    {
      *ptr = (void*)CeMLHashCipherAlgoCntxTypeBuff[0].CeMLTypeBuff;
      CeMLHashCipherAlgoCntxTypeBuff[0].used = 1;
    }
    else if (CeMLHashCipherAlgoCntxTypeBuff[1].used == 0)
    {
      *ptr = (void*)CeMLHashCipherAlgoCntxTypeBuff[1].CeMLTypeBuff;
      CeMLHashCipherAlgoCntxTypeBuff[1].used = 1;
    }
    else
    {
      return CEML_ERROR_FAILURE;
    }
  }
  else 
  {
    return CEML_ERROR_FAILURE;
  }
  	
  if (i > 7) return CEML_ERROR_FAILURE;
  
  return CEML_ERROR_SUCCESS;
}

/* ZI type free helper */
CeMLErrorType CeLoaderFree(void* ptr)
{
  uint32 j;

  for (j=0; j < 8; j++)
  {
    if(ptr == CeMLIovecTypeArray[j].CeMLIovecTypeBuff)
    {
      CeMLIovecTypeArray[j].used = 0;
      break;
    }
    else
    {
      continue;
    }
  }

  if (ptr == CeMLHashCipherAlgoCntxTypeBuff[0].CeMLTypeBuff)
  {
    CeMLHashCipherAlgoCntxTypeBuff[0].used = 0; 
  }

  if (ptr == CeMLHashCipherAlgoCntxTypeBuff[1].CeMLTypeBuff)
  {
    CeMLHashCipherAlgoCntxTypeBuff[1].used = 0; 
  }
    
  return CEML_ERROR_SUCCESS;
}


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeMLErrorType CeMLInit(void)
{
  return CEML_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeMLErrorType CeMLDeInit(void)
{
  return CEML_ERROR_SUCCESS;
}


/**
 * @brief 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */                     
CeMLErrorType CeMLHashInit(CeMLCntxHandle** CeMlHandle, CeMLHashAlgoType pAlgo)
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
  CryptoCntxHandle *cntx = NULL;
  SW_Auth_Alg_Type auth_al;

  /* Sanity check inputs */
  if (CEML_HASH_ALGO_SHA1 == pAlgo)
  {
	  auth_al = SW_AUTH_ALG_SHA1;
  }
  else if (CEML_HASH_ALGO_SHA256 == pAlgo)
  {
	  auth_al = SW_AUTH_ALG_SHA256;
  }
  else
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  *CeMlHandle = NULL;

  /* Allocate memory and check for errors */
  //malloc((void**) &(*CeMlHandle), sizeof(CeMLCntxHandle));
  CeLoaderMalloc((void**) &(*CeMlHandle), sizeof(CeMLCntxHandle));

  if(!*CeMlHandle)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  (*CeMlHandle)->pClientCtxt = NULL;

  ret_val = SW_Hash_Init(&cntx, auth_al);
 
  ((*CeMlHandle)->pClientCtxt) = cntx;

 
 return ret_val;

}

/**
 * @brief 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */                     
CeMLErrorType CeMLHashDeInit(CeMLCntxHandle** CeMlHandle)
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;

  if ((!CeMlHandle) || (!*CeMlHandle))
  {  
    return CEML_ERROR_INVALID_PARAM;
  }

  CeLoaderFree(*CeMlHandle);
  *CeMlHandle = NULL;

  return ret_val;
}

/**
 * @brief 
 *
 * @param 
 *
 * @return 
 *
 * @see  
 *
 */
CeMLErrorType CeMLHashUpdate(CeMLCntxHandle* CeMlHandle, 
                             CEMLIovecListType ioVecIn)
{
   CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
   CryptoCntxHandle *cntx = NULL;
   IovecListType *ioVecIn_sw;

   /* Sanity check inputs */
   if (!CeMlHandle)
   {
     return CEML_ERROR_INVALID_PARAM;
   }

   if ((ioVecIn.size!= 1) || (!ioVecIn.iov))
   {
     return CEML_ERROR_INVALID_PARAM;
   }

   ioVecIn_sw = (IovecListType *)&ioVecIn;


   cntx = (CryptoCntxHandle*) (CeMlHandle->pClientCtxt);
   
   ret_val = SW_Hash_Update(cntx, *ioVecIn_sw);

   return ret_val;
}

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLHashFinal(CeMLCntxHandle* CeMlHandle, 
                            CEMLIovecListType* ioVecOut)
{
  CeMLErrorType         ret_val = CEML_ERROR_SUCCESS;    
  CryptoCntxHandle *cntx = NULL;
  
  /* Sanity check inputs */
  if ((!CeMlHandle) || (!CeMlHandle->pClientCtxt))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if ((!ioVecOut) || (ioVecOut->size!= 1) || (!ioVecOut->iov))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  cntx = (CryptoCntxHandle*) (CeMlHandle->pClientCtxt);

  ret_val = SW_Hash_Final(cntx, (IovecListType *)ioVecOut);

  return ret_val;
}  

CeMLErrorType CeMLHashAtomic(CeMLCntxHandle*    ceMlHandle, 
                             CEMLIovecListType  ioVecIn,
                             CEMLIovecListType* ioVecOut)
{
  CeMLErrorType         ret_val = CEML_ERROR_SUCCESS;    
  CryptoCntxHandle *cntx = NULL;
  IovecListType *ioVecIn_sw;
  
  do
  {         
    /* Sanity check inputs */
    if ((!ceMlHandle) || (!ceMlHandle->pClientCtxt))
    {
      return CEML_ERROR_INVALID_PARAM;
    }

    if ((!ioVecOut) || (ioVecOut->size!= 1) || (!ioVecOut->iov))
    {
      return CEML_ERROR_INVALID_PARAM;
    }

    if ((ioVecIn.size!= 1) || (!ioVecIn.iov))
    {
      return CEML_ERROR_INVALID_PARAM;
    }

    cntx = (CryptoCntxHandle*) (ceMlHandle->pClientCtxt);

    ioVecIn_sw = (IovecListType *)&ioVecIn;

    if (CEML_ERROR_SUCCESS != SW_Hash_Update(cntx, *ioVecIn_sw))
    {
      ret_val = CEML_ERROR_FAILURE;
      break;
    }

    if (CEML_ERROR_SUCCESS != SW_Hash_Final(cntx, (IovecListType *)ioVecOut))
      ret_val = CEML_ERROR_FAILURE;
   
  } while (0);

  return ret_val;
}


/**
 * @brief This functions gets the Cipher paramaters used by 
 *        SW_Hash_Init
 *
 * @param handle     [in]  Pointer to pointer to hash context
 * @param auth_alg  [in]  Hash algorithm
 
 *
 * @return sw_crypto_errno_enum_type
 *
 * @see 
 *
 */
CeMLErrorType SW_Hash_Init(CryptoCntxHandle **handle, SW_Auth_Alg_Type auth_alg) 
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
  struct __sechsh_ctx_s ctx;
  SW_SHA_Ctx * hashCtxt;
    
  /* Sanity check inputs */
  if (SW_AUTH_ALG_SHA1 != auth_alg && SW_AUTH_ALG_SHA256 != auth_alg) 
  {
      return CEML_ERROR_INVALID_PARAM;
  }
  
  //uc_mutex_enter(); 

  do{
    /* Allocate memory and check for errors */
    //malloc((void**) &(*handle), sizeof(SW_SHA_Ctx));
    CeLoaderMalloc((void**) &(*handle), sizeof(SW_SHA_Ctx));

    if(!*handle)
    {
       ret_val = CEML_ERROR_FAILURE;
       break;
    }

    memset(*handle, 0, sizeof(SW_SHA_Ctx)); 

    hashCtxt = (SW_SHA_Ctx*)*handle;

    if (auth_alg == SW_AUTH_ALG_SHA1){
     sechsharm_sha1_init(&ctx);
	 hashCtxt->auth_alg = SW_AUTH_ALG_SHA1 ;}
    else if (auth_alg == SW_AUTH_ALG_SHA256){
     sechsharm_sha256_init(&ctx);
	 hashCtxt->auth_alg = SW_AUTH_ALG_SHA256;}

    //memcpy ((unsigned char*)hashCtxt->counter, (unsigned char*)ctx.counter, sizeof(ctx.counter));
    //memcpy((unsigned char*)hashCtxt->iv, (unsigned char*)ctx.iv,sizeof(ctx.iv));
	
	CopyMem((unsigned char*)hashCtxt->counter, (unsigned char*)ctx.counter, sizeof(ctx.counter));
    CopyMem((unsigned char*)hashCtxt->iv, (unsigned char*)ctx.iv,sizeof(ctx.iv));

  }while(0);

  if (ret_val != CEML_ERROR_SUCCESS)
  {
    if(*handle)
    {
      CeLoaderFree(*handle);
    }
    //uc_mutex_exit();
  }

   return ret_val;
}

/**
 * @brief This function will hash data into the hash context
 *        structure, which must have been initialized by
 *        SW_Hash_Update().
 *
 * @param handle      [in] Pointer to Hash context
 * @param ioVecIn     [in] Input vector

 *
 * @return sw_crypto_errno_enum_type 
 *
 * @see SW_CRYPTO_Hash_Init 
 *
 */
CeMLErrorType SW_Hash_Update(CryptoCntxHandle *handle,IovecListType ioVecIn)
{
   CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
   uint8   leftover[SW_SHA_BLOCK_SIZE];
   uint32  leftover_size=0;
   uint8* pData = NULL;
   uint32 nDataLen = 0;
   struct __sechsh_ctx_s ctx;
   SW_SHA_Ctx* hashCtxt = NULL;

   do{
   /* Sanity check inputs */
     if (!handle)
   {
       ret_val = CEML_ERROR_INVALID_PARAM;
       break;
   }

   hashCtxt = (SW_SHA_Ctx*)(handle);

   if ((ioVecIn.size!= 1) || (!ioVecIn.iov))
   {
       ret_val = CEML_ERROR_INVALID_PARAM;
       break;
     }

     if (!ioVecIn.iov[0].pvBase)
     {
       ret_val = CEML_ERROR_INVALID_PARAM;
       break;
   }
   
   pData = (uint8*) ioVecIn.iov[0].pvBase;
   nDataLen = ioVecIn.iov[0].dwLen;

   //memcpy ((unsigned char*)ctx.counter, (unsigned char*)hashCtxt->counter, sizeof(ctx.counter));
   //memcpy((unsigned char*)ctx.iv, (unsigned char*) hashCtxt->iv, sizeof(ctx.iv));
   //memcpy((unsigned char *)leftover, (unsigned char *)hashCtxt->leftover, sizeof(leftover));
   
   CopyMem((unsigned char*)ctx.counter, (unsigned char*)hashCtxt->counter, sizeof(ctx.counter));
   CopyMem((unsigned char*)ctx.iv, (unsigned char*) hashCtxt->iv, sizeof(ctx.iv));
   CopyMem((unsigned char *)leftover, (unsigned char *)hashCtxt->leftover, sizeof(leftover));
   leftover_size = hashCtxt->leftover_size; 

   if (hashCtxt->auth_alg == SW_AUTH_ALG_SHA1)
    sechsharm_sha1_update( &ctx, leftover, &leftover_size, pData, nDataLen );
   else if (hashCtxt->auth_alg == SW_AUTH_ALG_SHA256)
    sechsharm_sha256_update( &ctx, leftover, &leftover_size, pData, nDataLen );
     else 
     {
       ret_val = CEML_ERROR_INVALID_PARAM;
       break;
     }


   CopyMem ((unsigned char*)hashCtxt->counter, (unsigned char*)ctx.counter, sizeof(ctx.counter));
   CopyMem((unsigned char*)hashCtxt->iv, (unsigned char*)ctx.iv,sizeof(ctx.iv));
   CopyMem((unsigned char*)hashCtxt->leftover, (unsigned char*)leftover, sizeof(leftover));
   hashCtxt->leftover_size = leftover_size;

   }while(0);


   if (ret_val != CEML_ERROR_SUCCESS)
   {
     if(handle)
     {
       CeLoaderFree(handle);
     }
     //uc_mutex_exit();
   }

   return ret_val;
}


/**
 * @brief Compute the final digest hash value.
 *
 * @param handle     [in] Pointer to Hash context
 * @param ioVecOut   [out] Output vector
 
 * @return sw_crypto_errno_enum_type 
 *
 * @see
 *
 */
CeMLErrorType SW_Hash_Final (CryptoCntxHandle *handle,IovecListType *ioVecOut)
{
   uint8   digest[SW_SHA256_DIGEST_SIZE];
   uint8   leftover[SW_SHA_BLOCK_SIZE];
   uint32  leftover_size=0;
   CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
   struct __sechsh_ctx_s ctx;
   uint32 hashLen = 0;
   SW_SHA_Ctx* hashCtxt = NULL;

  do{
  /* Sanity check inputs */
    if (!handle)
  {
      ret_val = CEML_ERROR_INVALID_PARAM;
      break;
   }

  if ((ioVecOut->size!= 1) || (!ioVecOut->iov))
  {
      ret_val = CEML_ERROR_INVALID_PARAM;
      break;
    }

    if (!ioVecOut->iov[0].pvBase)
    {
      ret_val = CEML_ERROR_INVALID_PARAM;
      break;
  }

    hashCtxt = (SW_SHA_Ctx*) (handle);

  if(hashCtxt->auth_alg == SW_AUTH_ALG_SHA1)
  {
    hashLen = SW_SHA1_DIGEST_SIZE;
  }
  else if (hashCtxt->auth_alg == SW_AUTH_ALG_SHA256)
  {
    hashLen = SW_SHA256_DIGEST_SIZE;
  }
  else
  {
      ret_val = CEML_ERROR_INVALID_PARAM;
      break;
  }

   CopyMem ((unsigned char*)ctx.counter,hashCtxt->counter, sizeof(ctx.counter));
   CopyMem((unsigned char*)ctx.iv, hashCtxt->iv, sizeof(ctx.iv));
   CopyMem ((unsigned char*)leftover, hashCtxt->leftover, sizeof(leftover));
   leftover_size = hashCtxt->leftover_size;

   if (hashCtxt->auth_alg == SW_AUTH_ALG_SHA1)
     sechsharm_sha1_final( &ctx,  leftover, &leftover_size, digest);
   else if (hashCtxt->auth_alg == SW_AUTH_ALG_SHA256)
     sechsharm_sha256_final( &ctx,  leftover, &leftover_size, digest);
     else 
     {
       ret_val = CEML_ERROR_INVALID_PARAM;
       break;
     }

   //copy the hash result into ioVecOut

   CopyMem(ioVecOut->iov[0].pvBase, digest, hashLen);
   ioVecOut->size = 1;
   ioVecOut->iov[0].dwLen = hashLen;

  }while(0);

  if(handle)
  {
   CeLoaderFree(handle);
   handle = NULL;
  }
   //uc_mutex_exit();
   return ret_val;
}

CeMLErrorType  
CeMLHashSetParam    (CeMLCntxHandle       * _h,
                     CeMLHashParamType    nParamID, 
                     const void           *pParam, 
                     UINT64               cParam,
                     CeMLHashAlgoType     pAlgo )
{
  return CEML_ERROR_FAILURE;
}

CeMLErrorType 
CeMLCipherInit      (CeMLCntxHandle       ** _h, 
                     CeMLCipherAlgType    pAlgo)
{
  return CEML_ERROR_FAILURE;
}

CeMLErrorType 
CeMLCipherDeInit    (CeMLCntxHandle       ** _h)
{
  return CEML_ERROR_FAILURE;
}

CeMLErrorType  
CeMLCipherSetParam    (CeMLCntxHandle       * _h,
                       CeMLCipherParamType  nParamID, 
                       const void           *pParam, 
                       UINT64               cParam )
{
  return CEML_ERROR_FAILURE;
}

CeMLErrorType 
CeMLCipherData      (CeMLCntxHandle       * _h, 
                     CEMLIovecListType    ioVecIn,
                     CEMLIovecListType    * ioVecOut)
{
  return CEML_ERROR_FAILURE;
}

CeMLErrorType 
CeMLHashCipherData (CeMLCntxHandle     *_h1, 
                    CeMLCntxHandle     *_h2,
                    CEMLIovecListType  ioVecIn,
                    CEMLIovecListType  * ioVecOut,
                    UINT8*             hash_out,
                    UINT64             hash_out_len)
{
  return CEML_ERROR_FAILURE;
}
