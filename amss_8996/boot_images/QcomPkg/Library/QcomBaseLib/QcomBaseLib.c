/******************************************************************//**
 * @file QcomBaseLib.c
 *
 * @brief QcomBaseLib functions
 *
 * Copyright (c) 2011, 2013-2014 by Qualcomm Technologies, Inc. 
 * All Rights Reserved.
 *
 *********************************************************************/
/*=======================================================================
                        Edit History

when       who     what, where, why
--------   ----    --------------------------------------------------- 
06/11/14   aus     Added CopyMemS function
05/15/14   kedara  Update __stack_chk_fail
02/07/13   vk      Add stack canary check
11/26/13   vk      Update GetInfoBlkPtr to UINTN
03/20/13   vk      Add GetInfoBlkPtr API
03/14/13   yg      Add GetTimerCount APIs
03/12/13   yg      Improve readable LogMsg & use Timer
03/07/13   vk      LogMsg print Hex
03/07/13   vk      Fix print parameter in LogMsg
01/17/13   vk      Fix warning
11/21/11   jz      Initial checkin
========================================================================*/

#include <PiDxe.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/ProcAsmLib.h>
#include <Library/TimerLib.h>
#include <Library/BaseLib.h>

/* Information about memory base, size, type */
typedef struct {
  MemType    MemoryType;
  UINT64     MemoryBase;
  UINT64     MemorySize;
} MemoryInfoType;

UINT32 TimerFreq = 0;
UINT32 FactoruS = 0;
UINT32 FactormS = 0;

void* __stack_chk_guard = (void*) 0xdeadface;

/**
 * Callback if stack cananry is corrupted
 * */
void __stack_chk_fail (void)
{
  volatile UINT32 i = 1;
  /* Loop forever in case of stack overflow. Avoid
  calling into another api in case of stack corruption/overflow */ 
  while (i);
}

/**
 * Initialize stack check canary
 */
VOID InitStackCanary (VOID)
{
  __stack_chk_guard = (VOID*) 0xdebac1ed;
}

/**
 * Extern reference to GUIDs.
 */
extern EFI_GUID gQcomMemoryInfoGuid;
extern EFI_GUID gEfiInfoBlkHobGuid;

/**
  Store memory related info (base, size, type) in Hob
  
  @param MemoryType      Memory type
  @param MemoryBase      Pointer to the memory base
  @param MemorySize      Ponter to memory size in bytes
  
  @retval EFI_SUCCESS The memory info is stored
  
**/
EFI_STATUS
EFIAPI
SetMemoryInfo(
  IN MemType  MemoryType,
  IN UINT64   *MemoryBase,
  IN UINT64   *MemorySize
  )
{
  MemoryInfoType MemInfo;

  ZeroMem(&MemInfo, sizeof(MemInfo));

  if (MemoryType < MAX_MEMORY_TYPE)
    MemInfo.MemoryType = MemoryType;

  if (MemoryBase)
    MemInfo.MemoryBase = *MemoryBase;

  if (MemorySize)
    MemInfo.MemorySize = *MemorySize;
  
  
  // Build HOB to pass up memory info
  BuildGuidDataHob (&gQcomMemoryInfoGuid, &MemInfo, sizeof(MemoryInfoType));

  return EFI_SUCCESS;
}

/**
  Retrieve memory related info (base, size, type) in Hob
  
  @param MemoryType      Memory type
  @param MemoryBase      Pointer to the memory base
  @param MemorySize      Ponter to memory size in bytes
  
  @retval EFI_SUCCESS    The memory info is retrieved
  @retval EFI_NOT_FOUND  The memory info not found
  
**/
EFI_STATUS
EFIAPI
GetMemoryInfo(
  IN MemType  MemoryType,
  OUT UINTN  *MemoryBase OPTIONAL,
  OUT UINTN  *MemorySize OPTIONAL
  )
{
  EFI_HOB_GUID_TYPE *GuidHob;
  MemoryInfoType    *MemInfo;
  UINTN             MemBase = 0, MemSize = 0, MemType = 0;
  
  GuidHob = GetFirstGuidHob(&gQcomMemoryInfoGuid);
  if (GuidHob == NULL)
  {
    DEBUG((EFI_D_INFO, "Memory Info Not Found\n"));
    return EFI_NOT_FOUND;
  }

  MemInfo = GET_GUID_HOB_DATA(GuidHob);

  if (MemInfo != NULL)
  {
    MemBase = MemInfo->MemoryBase;
    MemSize = MemInfo->MemorySize;
    MemType = MemInfo->MemoryType;
  }

  if (MemType != MemoryType)
    return EFI_NOT_FOUND;
    
  if (MemoryBase)
    *MemoryBase = MemBase;

  if (MemorySize)
    *MemorySize = MemSize;
  
  return EFI_SUCCESS;
}

#define TIMER_REF_MICRO_SEC           1000000
#define TIMER_REF_MILLI_SEC           1000

STATIC INT32
InitFactors (VOID)
{
  UINT64 TempFreq, StartVal, EndVal;

  TempFreq = GetPerformanceCounterProperties (&StartVal, &EndVal);
  /* This code doesn't support less than Ref timer Speed */
  if ((TempFreq <= TIMER_REF_MICRO_SEC) || (TempFreq <= TIMER_REF_MILLI_SEC))
    return -1;

  /* Assume the timer runs slower than 4GHz */
  if (TempFreq >= 0x100000000ULL)
    return -2;
    
 /* Assume the counter always counts up */
  if (StartVal >= EndVal)
    return -3;

  TimerFreq = (UINT32)(TempFreq & 0xFFFFFFFFULL);
  FactoruS = TimerFreq / TIMER_REF_MICRO_SEC;
  FactormS = TimerFreq / TIMER_REF_MILLI_SEC;

  return 0;
}

/* This log message consumes around 15uS */
VOID
LogMsg (CHAR8* Msg)
{
  UINT32 Sec, mSec, uSec;

#ifdef USE_CYCLE_COUNTER
  uSec = ReadCycleCntReg() / 15;
  Sec = (uSec / TIMER_REF_MICRO_SEC);
  mSec = (uSec / 1000) - (Sec * 1000);
  uSec = uSec - (mSec * 1000) - (Sec * 1000000);

#else

  UINT64 TimerVal, AbsTimeuS;

  if (FactoruS == 0)
    if (InitFactors () != 0)
      return;

  ASSERT(FactoruS != 0);

  TimerVal = GetPerformanceCounter ();

  AbsTimeuS = TimerVal / FactoruS;
  
  uSec = (UINT32)(AbsTimeuS % 1000);
  mSec = TimerVal / FactormS;
  Sec = mSec / 1000;
  mSec -= (Sec * 1000);
#endif

  DEBUG ((EFI_D_WARN, "%d,%03d.%03d LMsg: %a \n", Sec, mSec, uSec, Msg));
}

/* This log message consumes around 18uS */
VOID
LogMsgDataU32 (UINT32 UInt32Data, CHAR8* Msg)
{
  UINT32 Sec, mSec, uSec;
  UINT64 TimerVal, AbsTimeuS;

  if (FactoruS == 0)
    if (InitFactors () != 0)
      return;

  ASSERT(FactoruS != 0);
  if (FactoruS == 0)
    return;

  TimerVal = GetPerformanceCounter ();

  AbsTimeuS = TimerVal / FactoruS;

  uSec = (UINT32)(AbsTimeuS % 1000);
  mSec = TimerVal / FactormS;
  Sec = mSec / 1000;
  mSec -= (Sec * 1000);

  DEBUG ((EFI_D_WARN, "%d,%03d.%03d 0x%x LMsgD: %a \n", Sec, mSec, uSec, UInt32Data, Msg));
}

/* Customize this API to 32 bits for now, later when 64 bit timer is 
 * supported, there can be another API for 64 bits */
UINT32
GetTimerCountus (VOID)
{
  UINT32 TimerVal, AbsTimeuS;

  if (FactoruS == 0)
    if (InitFactors () != 0)
      return 0;
	  
  TimerVal = (UINT32)GetPerformanceCounter ();

  AbsTimeuS = TimerVal / FactoruS;

  return AbsTimeuS;
}

UINT32
GetTimerCountms (VOID)
{
  UINT32 TimerVal, AbsTimemS;

  if (FactormS == 0)
    if (InitFactors () != 0)
      return 0;

  TimerVal = (UINT32)GetPerformanceCounter ();

  AbsTimemS = TimerVal / FactormS;

  return AbsTimemS;
}

VOID* 
GetInfoBlkPtr (VOID)
{
  EFI_HOB_GUID_TYPE *GuidHob;
  UINTN **DataPtr;

  GuidHob = GetFirstGuidHob(&gEfiInfoBlkHobGuid);

  if (GuidHob == NULL)
    return NULL;

  DataPtr = GET_GUID_HOB_DATA(GuidHob);
  return  (VOID*) *DataPtr;
}

UINTN CopyMemS
(  
  IN VOID   *Destination,
  IN UINTN  DestLength, 
  IN const VOID   *Source,
  IN UINTN  SrcLength
) 
{
   if(DestLength >= SrcLength) {
      CopyMem(Destination, Source, SrcLength);
      return SrcLength;
    }
  
    CopyMem(Destination, Source, DestLength);
    return DestLength;
}  

