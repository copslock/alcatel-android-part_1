/*
 * @file RamPartitionTableLib.c
 *
 * @brief RamPartitionTableLib functions
 *
 * Copyright (c) 2014-2015 by Qualcomm Technologies, Inc. 
 * All Rights Reserved.
 *
 */
/*=======================================================================
                        Edit History

when       who     what, where, why
--------   ----    --------------------------------------------------- 
05/21/15   vk      Move to v2 table
04/17/15   vk      Use PcdMaxMem to allocate from below 4 GB
12/14/14   vk      Handle non-contiguous memory region
11/06/14   vk      Fix pointer check
10/30/14   vk      Initital Version
========================================================================*/
#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseLib.h>
#include <Library/RamPartitionTableLib.h>
#include <com_dtypes.h>
#include <Library/smem.h>
#include <ram_partition.h>
#include <MemRegionInfo.h>

STATIC MemRegionInfo mRamPartitionTable [MAX_SUPPORTED_PARTITON_ENTRIES]; 
STATIC UINTN         mRamPartitionTableEntryCount = 0;

EFI_STATUS
GetRamPartitionVersion (IN VOID *pRamPartitionTable, IN OUT UINT32 *Version)
{
  /* v0 and v1 have same header info */
  usable_ram_part_table_t pRamPartTable;
  pRamPartTable = (usable_ram_part_table_t) pRamPartitionTable;

  /* First, make sure the RAM partition table is valid */
  if( pRamPartTable->magic1 == RAM_PART_MAGIC1 &&
      pRamPartTable->magic2 == RAM_PART_MAGIC2 )
  {
     *Version = pRamPartTable->version;
     return EFI_SUCCESS;
  }
  else
  {
    return EFI_NOT_FOUND;
  }
}

#define UNSUPPORTED_BELOW_VER 2

STATIC EFI_STATUS
GetRamPartitionTable (IN OUT VOID **pRamPartitionTable)
{ 
  EFI_STATUS Status;
  UINT32 PartTableVer = 0;
  UINT32 RamPartitionBuffSz = 0;

  /* Get the RAM partition table */
  *pRamPartitionTable = smem_get_addr(SMEM_USABLE_RAM_PARTITION_TABLE, (uint32*)&RamPartitionBuffSz);
  if (*pRamPartitionTable == NULL)
  {
    /*NOTE: We should be here only if SMEM is not initialized (virtio, SmemNullLib)*/
    DEBUG ((EFI_D_ERROR, "WARNING: Unable to read memory partition table from SMEM\n"));
    return EFI_NOT_READY;
  }

  Status  = GetRamPartitionVersion(*pRamPartitionTable, &PartTableVer);
  if (Status != EFI_SUCCESS)  /* Invalid RAM Partition, return early */
   return Status; 

  if (PartTableVer < UNSUPPORTED_BELOW_VER)
  {
    DEBUG ((EFI_D_WARN, "WARNING: Using deprecated RAM partition table !\n"));
    CpuDeadLoop();
    return EFI_UNSUPPORTED;
  }

  return EFI_SUCCESS;
}

EFI_STATUS
GetRamPartitions(UINTN *EntryCount, MemRegionInfo *EntryTable)
{
  UINTN i = 0;

  if (EntryCount == NULL)
    return EFI_INVALID_PARAMETER;

  if ((*EntryCount > 0) && (EntryTable == NULL))
    return EFI_INVALID_PARAMETER;

  if (mRamPartitionTableEntryCount > *EntryCount)
  {
    *EntryCount = mRamPartitionTableEntryCount;
    return EFI_BUFFER_TOO_SMALL;
  }

  for( i = 0; i < mRamPartitionTableEntryCount; i++ )
  {
    AsciiStrCpy (EntryTable[i].Name, mRamPartitionTable[i].Name);

    EntryTable[i].MemBase           = mRamPartitionTable[i].MemBase;
    EntryTable[i].MemSize           = mRamPartitionTable[i].MemSize;
    EntryTable[i].BuildHobOption    = mRamPartitionTable[i].BuildHobOption;
    EntryTable[i].ResourceType      = mRamPartitionTable[i].ResourceType;
    EntryTable[i].ResourceAttribute = mRamPartitionTable[i].ResourceAttribute;
    EntryTable[i].MemoryType        = mRamPartitionTable[i].MemoryType;
    EntryTable[i].CacheAttributes   = mRamPartitionTable[i].CacheAttributes;
  }

  *EntryCount = mRamPartitionTableEntryCount;

  return EFI_SUCCESS;
}

EFI_STATUS
GetPartitionEntryByAddr (IN UINT64 Address, IN OUT MemRegionInfo *FirstBank)
{
  UINTN i = 0;
  UINTN MemEndAddr = 0;

  if (FirstBank == NULL)
    return EFI_INVALID_PARAMETER;

  for (i = 0; i < mRamPartitionTableEntryCount; i++ )
  {
    MemEndAddr = mRamPartitionTable[i].MemBase + mRamPartitionTable[i].MemSize;
    if ((Address >= mRamPartitionTable[i].MemBase) && (Address <= MemEndAddr))
    {
      *FirstBank = mRamPartitionTable[i];
      return EFI_SUCCESS;
    }
  }

  return EFI_NOT_FOUND;
}

EFI_STATUS
GetHighestBankBit (IN OUT UINT8 *HighBankBit)
{
  EFI_STATUS Status;
  VOID       *pRamPartitionTable = NULL;
  usable_ram_part_table_t pRamPartTable;
  UINT32     i;

  if (HighBankBit == NULL)
    return EFI_INVALID_PARAMETER;

  Status = GetRamPartitionTable (&pRamPartitionTable);
  if (Status != EFI_SUCCESS)
    return Status;

  pRamPartTable = (usable_ram_part_table_t) pRamPartitionTable;
  for (i = 0; i < pRamPartTable->num_partitions; i++)
  {
    if ((pRamPartTable->ram_part_entry[i].partition_type == RAM_PARTITION_SYS_MEMORY) &&
        (pRamPartTable->ram_part_entry[i].partition_category == RAM_PARTITION_SDRAM))
    {
      *HighBankBit = pRamPartTable->ram_part_entry[i].highest_bank_bit;
      return EFI_SUCCESS;
    }
  }

  return EFI_NOT_FOUND;
}

EFI_STATUS
InitRamPartitionTableLib (VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 i;
  VOID   *pRamPartitionTable = NULL;
  usable_ram_part_table_t pRamPartTable;
  smem_init();

  Status = GetRamPartitionTable (&pRamPartitionTable);
  if (Status != EFI_SUCCESS)
    return Status;

  pRamPartTable = (usable_ram_part_table_t) pRamPartitionTable;

  /* Parse the DDR partition table, and fill in table for only DDR entries */
  for( i = 0; i < pRamPartTable->num_partitions; i++ )
  {
    if ((pRamPartTable->ram_part_entry[i].partition_type == RAM_PARTITION_SYS_MEMORY) &&
        (pRamPartTable->ram_part_entry[i].partition_category == RAM_PARTITION_SDRAM))
    {
      UINT64 StartAddr = pRamPartTable->ram_part_entry[i].start_address;
      UINT64 Length = pRamPartTable->ram_part_entry[i].available_length;
      UINT64 EndAddr = StartAddr + Length;
      
      if (FixedPcdGet64 (PcdMaxMemory) != 0)
      {
        if (EndAddr > FixedPcdGet64 (PcdMaxMemory)) 
          continue;
      }

      AsciiStrCpy (mRamPartitionTable[i].Name, "RAM Partition");

      mRamPartitionTable[i].MemBase           = StartAddr;
      mRamPartitionTable[i].MemSize           = Length;
      mRamPartitionTable[i].BuildHobOption    = AddMem;
      mRamPartitionTable[i].ResourceType      = EFI_RESOURCE_SYSTEM_MEMORY;
      mRamPartitionTable[i].ResourceAttribute = SYSTEM_MEMORY_RESOURCE_ATTR_CAPABILITIES;
      mRamPartitionTable[i].MemoryType        = EfiConventionalMemory;
      mRamPartitionTable[i].CacheAttributes   = ARM_MEMORY_REGION_ATTRIBUTE_WRITE_BACK;
      mRamPartitionTableEntryCount++;

      ASSERT (mRamPartitionTableEntryCount <= MAX_SUPPORTED_PARTITON_ENTRIES);

    }
  }

  return EFI_SUCCESS;
}
