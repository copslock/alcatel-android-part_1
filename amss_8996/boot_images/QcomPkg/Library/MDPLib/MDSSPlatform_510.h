#ifndef __MDSSPlatform_510_H__
#define __MDSSPlatform_510_H__
/*=============================================================================
 
  File: MDSSPlatform_510.h
 
  Internal header file for MDP library
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "MDPLib_i.h"

/*===========================================================================
                                Defines 
===========================================================================*/
 
 
 /*=========================================================================
       Public Functions
 ==========================================================================*/
 
 /* MDP Clock list for MDP510 (8916)
  * 
  */

 //Clock names for 8916 has changed wrt to other B family chipsets (prefix with gcc_*) 
 
 static MDPExternalClockEntry            sBearDSI0ExtClocks[] = 
 {
   {"gcc_mdss_esc0_clk",     0, 2, 0, 0, 0, 0},  // Index 0 : primary source :XO , Secondary source : dsi pll
   {"gcc_mdss_pclk0_clk",    1, 0, 0, 0, 0, 0},  // Index 1 : Source DSI0_PLL
   {"gcc_mdss_byte0_clk",    1, 0, 0, 0, 0, 0},  // Index 2 : Source DSI0_PLL
   {"\0",                    0, 0, 0, 0, 0, 0},  
 };     

 static MDPClockEntry MDP510Clocks[] =
 {
     {"gcc_mdss_ahb_clk",                 0, NULL},
     {"gcc_mdss_vsync_clk",               0, NULL},
     {"gcc_mdss_mdp_clk",         240000000, NULL},
     {"gcc_mdss_axi_clk",                 0, NULL},
     {"\0", 0, NULL}
 };
 
 
 /* DSI0 Clock list for 8916
  */
 static MDPClockEntry BearDSI0Clocks[] =
 {
     {"gcc_mdss_esc0_clk",   0,         NULL},
     {"gcc_mdss_byte0_clk",  0,         NULL},
     {"gcc_mdss_pclk0_clk",  0,         NULL},
     {"\0", 0, NULL}
 };

 
 /* MDP power domain list
  */
 static MDPPowerDomainEntry MDP510PowerDomain[] = 
 {
     {"VDD_MDSS"},
     {"\0"}
 };
 
 /* MDP resource list for MDP510 (8916)
 */
 static const MDP_ResourceList sMDP510Resources =
 {
     (MDPPowerDomainEntry*)  &MDP510PowerDomain,    /* Power domain    */
     (MDPClockEntry*)        &MDP510Clocks,         /* MDP clocks      */
     (MDPClockEntry*)        &BearDSI0Clocks,       /* DSI0 clocks     */
     NULL,                                          /* DSI1 clocks     */
     NULL,                                          /* EDP clocks      */
     NULL                                           /* HDMI clocks     */
 };

 
 /* MDP External resource list for MDP510
 */
 static MDP_ExtClockResourceList sMDP510ExtClockResources =
 {
     (MDPExternalClockEntry*)    &sBearDSI0ExtClocks, /* DSI0 Ext clocks      */
     NULL,                                            /* DSI1 Ext clocks      */
     NULL,                                            /*  DSI shared clocks for dual DSI     */
 };
 
 /* Display resource list 
 */
 static DisplayResourceList sDisplayMDP510Resources =
 {
    (MDP_ResourceList*)            &sMDP510Resources,               /* MDP Resources           */
    (MDP_ExtClockResourceList*)    &sMDP510ExtClockResources,       /* MDP Ext Resources      */
 };

#endif // __MDSSPlatform_510_H__

