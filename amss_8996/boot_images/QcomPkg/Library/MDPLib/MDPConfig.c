/*=============================================================================
 
  File: MDPLIb.c
 
  Source file for MDP functions
  
 
  Copyright (c) 2011-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include "MDPLib.h"
#include "MDPLib_i.h"
#include "MDPPlatformLib.h"
#include "MDPSystem.h"
#include "hal_mdp.h"


/*=========================================================================
     Default Defines
==========================================================================*/



/*=========================================================================
      MDP Configuration Tables
==========================================================================*/

/* Access macro for gMDP_DisplayControlPathMap */
#define MDP_GET_CTRLPATHMAP(_displayId_, _mixerId_)    (&(gMDP_DisplayControlPathMap[((_displayId_)<MDP_DISPLAY_MAX)?(_displayId_):MDP_DISPLAY_PRIMARY][((_mixerId_)<MDP_DUALPIPE_NUM_MIXERS)?(_mixerId_):0]))

/* Map to find the control path associated with each display */
const MDP_DisplayCtrlPathMapType gMDP_DisplayControlPathMap[MDP_DISPLAY_MAX][MDP_DUALPIPE_NUM_MIXERS] =
{
  // MDP_DISPLAY_PRIMARY
  {
    {HAL_MDP_SOURCE_PIPE_RGB_0, HAL_MDP_CONTROL_PATH_0,    HAL_MDP_LAYER_MIXER_0,    HAL_MDP_DESTINATION_PIPE_0,  HAL_MDP_PINGPONG_0},    // Main control Path in single pipe or Index 0 path in dual pipe
    {HAL_MDP_SOURCE_PIPE_RGB_1, HAL_MDP_CONTROL_PATH_1,    HAL_MDP_LAYER_MIXER_1,    HAL_MDP_DESTINATION_PIPE_1,  HAL_MDP_PINGPONG_1}     // Index 1 path in dual pipe
  },
  // MDP_DISPLAY_SECONDARY
  {
    {HAL_MDP_SOURCE_PIPE_NONE,  HAL_MDP_CONTROL_PATH_1,    HAL_MDP_LAYER_MIXER_1,    HAL_MDP_DESTINATION_PIPE_1,  HAL_MDP_PINGPONG_1},     // Main control Path in single pipe or Index 0 path in dual pipe
    {HAL_MDP_SOURCE_PIPE_NONE,  HAL_MDP_CONTROL_PATH_1,    HAL_MDP_LAYER_MIXER_1,    HAL_MDP_DESTINATION_PIPE_1,  HAL_MDP_PINGPONG_1},     // Index 1 path in dual pipe
  },
  // MDP_DISPLAY_EXTERNAL
  {
    {HAL_MDP_SOURCE_PIPE_RGB_2, HAL_MDP_CONTROL_PATH_2,    HAL_MDP_LAYER_MIXER_2,    HAL_MDP_DESTINATION_PIPE_NONE,  HAL_MDP_PINGPONG_2},     // Main control Path in single pipe or Index 0 path in dual pipe
    {HAL_MDP_SOURCE_PIPE_RGB_3, HAL_MDP_CONTROL_PATH_2,    HAL_MDP_LAYER_MIXER_5,    HAL_MDP_DESTINATION_PIPE_NONE,  HAL_MDP_PINGPONG_3},     // Index 1 path in dual pipe
  }  
};

/* Access macro for gMDP_FlushFlags */
#define MDP_GET_FLUSHFLAGS(_displayId_,_mixerId_)      (gMDP_FlushFlags[((_displayId_)<MDP_DISPLAY_MAX)?(_displayId_):MDP_DISPLAY_PRIMARY][((_mixerId_)<MDP_DUALPIPE_NUM_MIXERS)?(_mixerId_):0])

/* Control Path Flush Flags */
const uint32 gMDP_FlushFlags[MDP_DISPLAY_MAX][MDP_DUALPIPE_NUM_MIXERS] =
{
  // MDP_DISPLAY_PRIMARY
  {
    (HAL_CONTROL_PATH_ATTACH_DETACH | HAL_CONTROL_PATH_FLUSH_RGB0 | HAL_CONTROL_PATH_FLUSH_LM0 | HAL_CONTROL_PATH_FLUSH_DSPP0),
    (HAL_CONTROL_PATH_ATTACH_DETACH | HAL_CONTROL_PATH_FLUSH_RGB1 | HAL_CONTROL_PATH_FLUSH_LM1 | HAL_CONTROL_PATH_FLUSH_DSPP1),
  },  
  // MDP_DISPLAY_SECONDARY
  {
    (0),
    (0),
  },
  // MDP_DISPLAY_EXTERNAL
  {
    (HAL_CONTROL_PATH_ATTACH_DETACH | HAL_CONTROL_PATH_FLUSH_RGB2 | HAL_CONTROL_PATH_FLUSH_LM2 | HAL_CONTROL_PATH_FLUSH_DSPP2),
    (HAL_CONTROL_PATH_ATTACH_DETACH | HAL_CONTROL_PATH_FLUSH_RGB1 | HAL_CONTROL_PATH_FLUSH_LM1 | HAL_CONTROL_PATH_FLUSH_DSPP1),
  }
};

/* Access macro for gMDP_InterfaceModeMap */
#define MDP_GET_INTFMODEHMAP(_displayConnect_, _mixerId_)   (&(gMDP_InterfaceModeMap[((_displayConnect_)<MDP_DISPLAY_CONNECT_MAX)?(_displayConnect_):MDP_DISPLAY_CONNECT_NONE][((_mixerId_)<MDP_DUALPIPE_NUM_MIXERS)?(_mixerId_):0]))

/* Map to find the interface mode and type associated with each display connection type */
const MDP_DisplayInterfaceMapType gMDP_InterfaceModeMap[MDP_DISPLAY_CONNECT_MAX][MDP_DUALPIPE_NUM_MIXERS] =
{
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_NONE
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_NONE
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_EBI2
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_EBI2
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_LCDC},  // MDP_DISPLAY_CONNECT_LCDC
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_LCDC},  // MDP_DISPLAY_CONNECT_LCDC
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_PRIMARY
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_PRIMARY
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_EXTERNAL
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_EXTERNAL
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_ANALOG_TV (QDI_DISPLAY_CONNECT_TV)
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_ANALOG_TV (QDI_DISPLAY_CONNECT_TV)
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_LCDC
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MDDI_LCDC
  },
  {
    {HAL_MDP_INTERFACE_3,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_HDMI},  // MDP_DISPLAY_CONNECT_DTV (QDI_DISPLAY_CONNECT_HDMI)
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_HDMI},  // MDP_DISPLAY_CONNECT_DTV (QDI_DISPLAY_CONNECT_HDMI)
  },
  {
    {HAL_MDP_INTERFACE_1,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO
    {HAL_MDP_INTERFACE_2,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO
  },
  {
    {HAL_MDP_INTERFACE_1,     HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD
    {HAL_MDP_INTERFACE_2,     HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD
  },
  {
    {HAL_MDP_INTERFACE_1,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO
    {HAL_MDP_INTERFACE_2,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO
  },
  {
    {HAL_MDP_INTERFACE_1,     HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD
    {HAL_MDP_INTERFACE_2,     HAL_MDP_INTERFACE_MODE_COMMAND, HAL_MDP_INTERFACE_STANDARD_DSI},   // MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_LVDS},  // MDP_DISPLAY_CONNECT_LVDS
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_LVDS},  // MDP_DISPLAY_CONNECT_LVDS
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_FRAMEBUFFER_WRITEBACK    
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_FRAMEBUFFER_WRITEBACK
  },
  {
    {HAL_MDP_INTERFACE_0,     HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_eDP},  // MDP_DISPLAY_CONNECT_DP
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_VIDEO,   HAL_MDP_INTERFACE_STANDARD_eDP},  // MDP_DISPLAY_CONNECT_DP
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_DBI
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_DBI
  },
  {
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MHL
    {HAL_MDP_INTERFACE_NONE,  HAL_MDP_INTERFACE_MODE_NONE,    HAL_MDP_INTERFACE_STANDARD_NONE},  // MDP_DISPLAY_CONNECT_MHL
  },
};

/* Access macro for gMDP_PixelFormatMap */
#define MDP_GET_PIXELFMTMAP(_pixelFmt_)   (gMDP_PixelFormatMap[((_pixelFmt_)<MDP_PIXEL_FORMAT_MAX)?(_pixelFmt_):MDP_PIXEL_FORMAT_NONE])

/* Pixel format mapping */
const MDP_PixelFormatMapType gMDP_PixelFormatMap[MDP_PIXEL_FORMAT_MAX] = 
{
   {0,   0, FALSE,  HAL_MDP_PIXEL_FORMAT_NONE},                    // MDP_PIXEL_FORMAT_NONE
   {16,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_RGB_565_16BPP},           // MDP_PIXEL_FORMAT_RGB_565_16BPP
   {18,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_RGB_666_18BPP},           // MDP_PIXEL_FORMAT_RGB_666_18BPP
   {24,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_RGB_888_24BPP},           // MDP_PIXEL_FORMAT_RGB_888_24BPP
   {16,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_ARGB_1555_16BPP},         // MDP_PIXEL_FORMAT_ARGB_1555_16BPP
   {32,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_XRGB_8888_32BPP},         // MDP_PIXEL_FORMAT_XRGB_8888_32BPP
   {32,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_ARGB_8888_32BPP},         // MDP_PIXEL_FORMAT_ARGB_8888_32BPP
   {16,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_BGR_565_16BPP},           // MDP_PIXEL_FORMAT_BGR_565_16BPP
   {24,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_BGR_888_24BPP},           // MDP_PIXEL_FORMAT_BGR_888_24BPP
   {16,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_ABGR_1555_16BPP},         // MDP_PIXEL_FORMAT_ABGR_1555_16BPP
   {32,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_XBGR_8888_32BPP},         // MDP_PIXEL_FORMAT_XBGR_8888_32BPP
   {32,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_ABGR_8888_32BPP},         // MDP_PIXEL_FORMAT_ABGR_8888_32BPP
   {12,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CBCR_H2V2_12BPP},       // MDP_PIXEL_FORMAT_Y_CBCR_H2V2_12BPP
   {12,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CRCB_H2V2_12BPP},       // MDP_PIXEL_FORMAT_Y_CRCB_H2V2_12BPP
   {12,  1, TRUE,   HAL_MDP_PIXEL_FORMAT_YCRYCB_H2V1_16BPP},       // MDP_PIXEL_FORMAT_YCRYCB_H2V1_16BPP
   {16,  1, TRUE,   HAL_MDP_PIXEL_FORMAT_YCBYCR_H2V1_16BPP},       // MDP_PIXEL_FORMAT_YCBYCR_H2V1_16BPP
   {16,  1, TRUE,   HAL_MDP_PIXEL_FORMAT_CRYCBY_H2V1_16BPP},       // MDP_PIXEL_FORMAT_CRYCBY_H2V1_16BPP
   {16,  1, TRUE,   HAL_MDP_PIXEL_FORMAT_CBYCRY_H2V1_16BPP},       // MDP_PIXEL_FORMAT_CBYCRY_H2V1_16BPP
   {16,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CRCB_H2V1_16BPP},       // MDP_PIXEL_FORMAT_Y_CRCB_H2V1_16BPP
   {16,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CBCR_H2V1_16BPP},       // MDP_PIXEL_FORMAT_Y_CBCR_H2V1_16BPP
   {12,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CBCR_H2V2_VC1_12BPP},   // MDP_PIXEL_FORMAT_Y_CBCR_H2V2_VC1_12BPP
   {12,  2, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CRCB_H2V2_VC1_12BPP},   // MDP_PIXEL_FORMAT_Y_CRCB_H2V2_VC1_12BPP
   {12,  3, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CR_CB_H2V2_12BPP},      // MDP_PIXEL_FORMAT_Y_CR_CB_H2V2_12BPP
   {16,  3, TRUE,   HAL_MDP_PIXEL_FORMAT_Y_CR_CB_H2V1_16BPP},      // MDP_PIXEL_FORMAT_Y_CR_CB_H2V2_16BPP
   {0,   0, FALSE,  HAL_MDP_PIXEL_FORMAT_NONE},                    // MDP_PIXEL_FORMAT_Y_CBCR_SUPERTILE_4x2_12BPP
   {16,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_RGBA_5551_16BPP},         // MDP_PIXEL_FORMAT_RGBA_5551_16BPP
   {32,  1, FALSE,  HAL_MDP_PIXEL_FORMAT_RGBA_8888_32BPP},         // MDP_PIXEL_FORMAT_RGBA_8888_32BPP
};

/* SMP Client mapping */
const HAL_MDP_SMP_MMBClientId gMDP_SMPClientMap[HAL_MDP_SOURCE_PIPE_MAX] = 
{
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_NONE
     HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y, // HAL_MDP_SOURCE_PIPE_VIG_0
     HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y, // HAL_MDP_SOURCE_PIPE_VIG_1
     HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y, // HAL_MDP_SOURCE_PIPE_VIG_2
     HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y, // HAL_MDP_SOURCE_PIPE_VIG_3
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_VIG_4
     HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH,   // HAL_MDP_SOURCE_PIPE_RGB_0
     HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH,   // HAL_MDP_SOURCE_PIPE_RGB_1
     HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH,   // HAL_MDP_SOURCE_PIPE_RGB_2
     HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH,   // HAL_MDP_SOURCE_PIPE_RGB_3
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_RGB_4
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_DMA_0
     HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH,   // HAL_MDP_SOURCE_PIPE_DMA_1
     HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_Y, // HAL_MDP_SOURCE_PIPE_DMA_2
     HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_Y, // HAL_MDP_SOURCE_PIPE_DMA_3
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_DMA_4
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_CURSOR_0
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_CURSOR_1
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_CURSOR_2
     HAL_MDP_SMP_MMB_CLIENT_NONE,         // HAL_MDP_SOURCE_PIPE_CURSOR_3
     HAL_MDP_SMP_MMB_CLIENT_NONE          // HAL_MDP_SOURCE_PIPE_CURSOR_4
};

/* SMP Pool */
#define MDP_SMP_BLOCKS                    44
#define MDP_DEFAULT_PXO_CLK               19200000

static  HAL_MDP_SourcePipeId              gMDP_SMPAllocations[MDP_SMP_BLOCKS] = {HAL_MDP_SOURCE_PIPE_NONE, };

/* Default values for FBC */
#define MDP_FBC_BLOCKEXTRABUDGET  0x05
#define MDP_FBC_LOSSYMODEIDX      0x03
#define MDP_FBC_LOSSYMODETHD      0xC0
#define MDP_FBC_LOSSLESSMODETHD   0x200

/* Default values for TE */
// default vsync continue threshold for smart panel, same as default in KMD
// by default, the difference between read pointer and write pointer is 5 lines
#define MDP_DEFAULT_VSYNC_CONTINUE_LINES                 (5)
// default line divisor for smart panel to calculate vsync start threshold, same as default in KMD
// by default, TE start_threshold will be 1/4 of panel height
#define MDP_DEFAULT_VSYNC_START_LINE_DIVISOR             (4)

const MDP_FbcProfileMode gFbcProfileModes[MDP_FBC_PROFILEID_MAX] =
{
  /* bBflcEnable  bVlcEnable  bPatEnable  bQErrEnable bPlanar   uCdBias   uLossyRgbThreadhold   uCompressionRatio*/
  {  TRUE,        TRUE,       TRUE,       TRUE,       FALSE,    2,        0,                    2 }, /*ProfileID = 0x00*/
  {  TRUE,        TRUE,       TRUE,       TRUE,       TRUE,     2,        4,                    2 }, /*ProfileID = 0x01*/
  {  TRUE,        TRUE,       FALSE,      TRUE,       FALSE,    2,        0,                    2 }, /*ProfileID = 0x02*/
  {  TRUE,        TRUE,       FALSE,      TRUE,       TRUE,     2,        4,                    2 }, /*ProfileID = 0x03*/
};

/* Access macro for gMDP_DualPipe3DMuxFlag */
#define MDP_GET_DUALPIPEMUX(_displayConnect_)   (gMDP_DualPipe3DMuxFlag[((_displayConnect_)<MDP_DISPLAY_CONNECT_MAX)?(_displayConnect_):MDP_DISPLAY_CONNECT_NONE])

const bool32 gMDP_DualPipe3DMuxFlag[MDP_DISPLAY_CONNECT_MAX] = 
{
  FALSE,      //MDP_DISPLAY_CONNECT_NONE
  FALSE,      //MDP_DISPLAY_CONNECT_EBI2
  FALSE,      //MDP_DISPLAY_CONNECT_LCDC
  FALSE,      //MDP_DISPLAY_CONNECT_MDDI_PRIMARY
  FALSE,      //MDP_DISPLAY_CONNECT_MDDI_EXTERNAL
  FALSE,      //MDP_DISPLAY_CONNECT_ANALOG_TV = MDP_DISPLAY_CONNECT_TV
  FALSE,      //MDP_DISPLAY_CONNECT_MDDI_LCDC
  TRUE,       //MDP_DISPLAY_CONNECT_HDMI = MDP_DISPLAY_CONNECT_DTV
  FALSE,      //MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO
  FALSE,      //MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD
  FALSE,      //MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO
  FALSE,      //MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD
  FALSE,      //MDP_DISPLAY_CONNECT_LVDS
  FALSE,      //MDP_DISPLAY_CONNECT_FRAMEBUFFER_WRITEBACK
  TRUE,       //MDP_DISPLAY_CONNECT_DP
  FALSE,      //MDP_DISPLAY_CONNECT_DBI
  FALSE,      //MDP_DISPLAY_CONNECT_MHL
};
/*=========================================================================
     Local MDP Lib Helper functions
==========================================================================*/
/* -----------------------------------------------------------------------
*
** FUNCTION: GetPingPongCaps()
*/
/*!
** DESCRIPTION:
**      Retrieve the ping-pong caps based on ePingPongID
**
** INPUT:
**      ePingPongID       PP ID
**
**
** ----------------------------------------------------------------------- */
const MDP_PingPongCaps *GetPingPongCaps(HAL_MDP_PingPongId ePingPongID)
{
    const MDP_PingPongCaps *pPPCaps            = NULL;
    MDP_HwPrivateInfo      *psMDPHwPrivateInfo = &gsMDPHwPrivateInfo;

    if (ePingPongID < HAL_MDP_PINGPONG_MAX)
    {
        pPPCaps = (const MDP_PingPongCaps *)&psMDPHwPrivateInfo->pDeviceCaps->pPingPongCaps[ePingPongID];
    }

    return pPPCaps;
}

/* -----------------------------------------------------------------------
*
** FUNCTION: SplitDisplayInit()
*/
/*!
** DESCRIPTION:
**      This function configures params for pingpong buffer split
**
** ----------------------------------------------------------------------- */
static MDP_Status SplitDisplayInit(MDP_Panel_AttrType *pPanelConfig)
{
    MDP_Status           eStatus = MDP_STATUS_OK;

    if ((pPanelConfig->uNumInterfaces) &&
        (pPanelConfig->uNumMixers))
    {
        // one mixer, one ping-pong buffer but two interfaces
        if ((MDP_SINGLEPIPE_NUM_MIXERS == pPanelConfig->uNumMixers) &&
            (MDP_DUALPIPE_NUM_MIXERS   == pPanelConfig->uNumInterfaces))
        {
            // This scenario can be supported by PP Split and should be applied only if PP SPlit is supported by the HW
            HAL_MDP_PingPongId         ePingPongId   = pPanelConfig->sDisplayControlPath[MDP_MASTER_PATH_INDEX].ePingPongId;
            MDP_PingPongCaps          *pPingPongCaps = (MDP_PingPongCaps *)GetPingPongCaps(ePingPongId);

            // Check whether ping-pong split in suspported in the chip
            if (MDP_PINGPONG_CAP_PINGPONG_SPLIT & pPingPongCaps->uPPFeatureFlags)
            {
                HAL_MDP_PingPongConfigType            sPingPongConfig;
                HAL_MDP_PingPong_BufferConfigType     sPingPongBufConfig;

                MDP_OSAL_MEMZERO(&sPingPongConfig, sizeof(HAL_MDP_PingPongConfigType));
                MDP_OSAL_MEMZERO(&sPingPongBufConfig, sizeof(HAL_MDP_PingPong_BufferConfigType));

                // Enable PP Split
                // sDisplayControlPath[1] has the secondary interface info
                sPingPongBufConfig.bSplitFifoEnable = TRUE;
                sPingPongBufConfig.eSecondaryIntfId = pPanelConfig->sDisplayControlPath[MDP_SLAVE_PATH_INDEX].eInterfaceId;
                sPingPongConfig.pPingPongBufConfig  = &sPingPongBufConfig;

                if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_PingPong_Setup(ePingPongId, &sPingPongConfig, 0x00))
                {
                    DEBUG ((EFI_D_WARN, "SplitDisplayInit() failed to configure PP Split! (PingPong ID = %d)\n", ePingPongId));
                    eStatus = MDP_STATUS_FAILED;
                }
            }
            else
            {
                DEBUG((EFI_D_WARN, "SplitDisplayInit() failed: HW does not support ping-pong split (PingPong ID = %d)\n", ePingPongId));
                eStatus = MDP_STATUS_NOT_SUPPORTED;
            }
        }
    }

    return eStatus;
}

/* -----------------------------------------------------------------------
**
** FUNCTION: UpdateDisplayControlpathInfo()
**
** DESCRIPTION:
**      1. Finds number of mixers required for this display
**      2. Assigns control path information
**      3. Configures ping-pong split if required
** ----------------------------------------------------------------------- */
static MDP_Status UpdateDisplayControlpathInfo(MDP_Panel_AttrType *pPanelConfig)
{
    MDP_Status          eStatusRet         = MDP_STATUS_OK;
    MDP_HwPrivateInfo  *psMDPHwPrivateInfo = &gsMDPHwPrivateInfo;

    /* Initialization for single control path */
    pPanelConfig->uNumMixers = MDP_SINGLEPIPE_NUM_MIXERS;

    /* check if ping pong split is available */
    if ((NULL != psMDPHwPrivateInfo->pDeviceCaps) &&
        (NULL != psMDPHwPrivateInfo->pDeviceCaps->pPingPongCaps))
    {
        MDP_PingPongCaps            *pPingPongCaps[MDP_DUALPIPE_NUM_MIXERS];
        MDP_DisplayCtrlPathMapType  *pControlPathMap[MDP_DUALPIPE_NUM_MIXERS];
        uint32                       uMaxPPWidth;

        /* get control path mapping */
        pControlPathMap[MDP_MASTER_PATH_INDEX] = (MDP_DisplayCtrlPathMapType *)MDP_GET_CTRLPATHMAP(pPanelConfig->eDisplayId, MDP_MASTER_PATH_INDEX);
        pControlPathMap[MDP_SLAVE_PATH_INDEX]  = (MDP_DisplayCtrlPathMapType *)MDP_GET_CTRLPATHMAP(pPanelConfig->eDisplayId, MDP_SLAVE_PATH_INDEX);

        /* get the pingpong cap for each path */
        pPingPongCaps[MDP_MASTER_PATH_INDEX] = (MDP_PingPongCaps *)GetPingPongCaps(pControlPathMap[MDP_MASTER_PATH_INDEX]->ePingPongId);
        pPingPongCaps[MDP_SLAVE_PATH_INDEX]  = (MDP_PingPongCaps *)GetPingPongCaps(pControlPathMap[MDP_SLAVE_PATH_INDEX]->ePingPongId);

        /* max pingpong buffer width for the pingpong on master path */
        uMaxPPWidth = pPingPongCaps[MDP_MASTER_PATH_INDEX]->uMaxPPWidth;

        /* if panel width is greater than the pingpong buffer width*/
        if (pPanelConfig->uDisplayWidth > uMaxPPWidth)
        {
            /* in this case, we will need to use two pingpong buffers, check if pingpong buffer is supported on slave path */
            if (FALSE == pPingPongCaps[MDP_SLAVE_PATH_INDEX]->bSupported)
            {
                eStatusRet = MDP_STATUS_NO_RESOURCES;
            }
            else
            {
                /* if two pingpong buffers are supported, two mixers will be used */
                pPanelConfig->uNumMixers = MDP_DUALPIPE_NUM_MIXERS;
            }
        }
        else
        {
            /* for case that panel width is less than pingpong buffer width */
            if (MDP_INTERFACE_SINGLE == pPanelConfig->uNumInterfaces)
            {
                /* if panel only requires single interface, then only use single control path */
                pPanelConfig->uNumMixers = MDP_SINGLEPIPE_NUM_MIXERS;
            }
            else
            {
                /* if panel requires dual interfaces */
                if (MDP_PINGPONG_CAP_PINGPONG_SPLIT & pPingPongCaps[MDP_MASTER_PATH_INDEX]->uPPFeatureFlags)
                {
                    /* if pingpong buffer split is supported, then use single control path with pingpong buffer split */
                    pPanelConfig->uNumMixers = MDP_SINGLEPIPE_NUM_MIXERS;
                }
                else
                {
                    /* since pingpong buffer split is not supported, we will need to use two control paths */
                    if (FALSE == pPingPongCaps[MDP_SLAVE_PATH_INDEX]->bSupported)
                    {
                        eStatusRet = MDP_STATUS_NO_RESOURCES;
                    }
                    else
                    {
                        pPanelConfig->uNumMixers = MDP_DUALPIPE_NUM_MIXERS;
                    }
                }
            }
        }
    }

    if (MDP_STATUS_OK == eStatusRet)
    {
        uint32 uI = 0;

        // Populate the control path info
        // uNumMixers need not be equal to uNumInterfaces
        // Ensure that control path info is updated to take into account for both mixers and interfaces
        for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
        {
            MDP_DisplayCtrlPathMapType     *pDisplayControlPath = (MDP_DisplayCtrlPathMapType *)MDP_GET_CTRLPATHMAP(pPanelConfig->eDisplayId, uI);
            MDP_DisplayInterfaceMapType    *pInterfaceMap       = (MDP_DisplayInterfaceMapType *)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);

            pPanelConfig->sDisplayControlPath[uI].eControlPathId     = pDisplayControlPath->eControlPathId;
            pPanelConfig->sDisplayControlPath[uI].eSourcePipeId      = pDisplayControlPath->eSourcePipeId;
            pPanelConfig->sDisplayControlPath[uI].eLayerMixerId      = pDisplayControlPath->eLayerMixerId;
            pPanelConfig->sDisplayControlPath[uI].eDestinationPipeId = pDisplayControlPath->eDestinationPipeId;
            pPanelConfig->sDisplayControlPath[uI].eInterfaceId       = pInterfaceMap->eInterfaceId;
            pPanelConfig->sDisplayControlPath[uI].ePingPongId        = pDisplayControlPath->ePingPongId;
        }

        // Populate control path with interface info, this is for ping pong split case
        for (uI = 0; uI < pPanelConfig->uNumInterfaces; uI++)
        {
            MDP_DisplayInterfaceMapType    *pInterfaceMap = (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);

            pPanelConfig->sDisplayControlPath[uI].eInterfaceId = pInterfaceMap->eInterfaceId;
        }

        // Scenario requires ping pong split 
        // Identify the slave ping-pong
        if ((MDP_SINGLEPIPE_NUM_MIXERS == pPanelConfig->uNumMixers) &&
            (MDP_DUALPIPE_NUM_MIXERS   == pPanelConfig->uNumInterfaces))
        {
            if (NULL != psMDPHwPrivateInfo->pPPSplitSlaveMap)
            {
                HAL_MDP_PingPongId eSlavePingPongID = psMDPHwPrivateInfo->pPPSplitSlaveMap[pPanelConfig->sDisplayControlPath[MDP_MASTER_PATH_INDEX].ePingPongId];
                pPanelConfig->sDisplayControlPath[MDP_SLAVE_PATH_INDEX].ePingPongId = eSlavePingPongID;
            }
        }

        // Initialize any split display (source split or PP split) configuration if required 
        eStatusRet = SplitDisplayInit(pPanelConfig);
    }

    return eStatusRet;
}


/****************************************************************************
*
** FUNCTION: SMPFindFreeBlock()
*/
/*!
* \brief
*   Local helper function to find a free SMP block
*
* \param  [in]  eClientId   - Source pipe Client ID
*         [out] pBlockId    - Allocated block number  
*
* \retval MDP_Status
*
****************************************************************************/
static bool32 SMPFindFreeBlock(HAL_MDP_SourcePipeId eSourcePipe, uint32 *pBlockId)
{
  uint32 i;
  bool32 bFound = FALSE;
  
  
  for (i=0;i<MDP_SMP_BLOCKS;i++)
  {
    if (HAL_MDP_SOURCE_PIPE_NONE == gMDP_SMPAllocations[i])
    {
        *pBlockId = i;
        gMDP_SMPAllocations[i] = eSourcePipe;
        bFound = TRUE;
        break;
    }
  }


  return bFound;
}

/****************************************************************************
*
** FUNCTION: SMPFreeBlock()
*/
/*!
* \brief
*   Local helper function to free an SMP block
*
* \param  [in]  eClientId   - Source pipe Client ID
*
* \retval MDP_Status
*
****************************************************************************/
static bool32 SMPFreeBlock(HAL_MDP_SourcePipeId eSourcePipe)
{
    bool32 bFound = FALSE;

    if (NULL != gsMDPHwPrivateInfo.pSMPPoolInfo)
    {
        uint32 i;

        for (i = 0; i < MDP_SMP_BLOCKS; i++)
        {
            if (eSourcePipe == gMDP_SMPAllocations[i])
            {
                gMDP_SMPAllocations[i] = HAL_MDP_SOURCE_PIPE_NONE;
                bFound = TRUE;
            }
        }
    }
  
    return bFound;
}


/****************************************************************************
*
** FUNCTION: SMPPoolAllocate()
*/
/*!
* \brief
*   Local helper function for SMP allocation
*
* \param  [in] eSourcePipeId          - Source pipe ID
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status SMPPoolAllocate(HAL_MDP_SourcePipeId eSourcePipeId)
{
    MDP_Status              eStatus = MDP_STATUS_OK;

    /* Istari has no programmable SMP, so nothing needs to be allocated and pSMPPoolInfo has been set to NULL */
    if (NULL != gsMDPHwPrivateInfo.pSMPPoolInfo)
    {
        if (HAL_MDP_SOURCE_PIPE_MAX <= eSourcePipeId)
        {
            eStatus = MDP_STATUS_NOT_SUPPORTED;
        }
        else  if (HAL_MDP_SMP_MMB_CLIENT_NONE == gMDP_SMPClientMap[eSourcePipeId])
        {
            eStatus = MDP_STATUS_NOT_SUPPORTED;
        }
        else
        {
            HAL_MDP_SMP_MMBClientId       eClientId = gMDP_SMPClientMap[eSourcePipeId];
            HAL_MDP_SMP_ConfigType        sSMPConfig[4];
            uint32                        uNumClients = 0;
            uint32                        aReservationPerClient[HAL_MDP_SMP_MMB_CLIENT_MAX];
            const MDP_SMPReservationType *pReservationTable;
            uint32                        i;

            MDP_OSAL_MEMZERO(&sSMPConfig, sizeof(sSMPConfig));

            // this table stores number of reserved blocks per client
            MDP_OSAL_MEMZERO((void *)aReservationPerClient, sizeof(aReservationPerClient));
            pReservationTable = gsMDPHwPrivateInfo.pSMPPoolInfo->pReservationTable;

            for (i = 0; i < (gsMDPHwPrivateInfo.pSMPPoolInfo->uNumReservations); i++)
            {
                aReservationPerClient[pReservationTable[i].eClientId] ++;
            }

            if ((eSourcePipeId >= HAL_MDP_SOURCE_PIPE_RGB_BASE) && (eSourcePipeId <= HAL_MDP_SOURCE_PIPE_RGB_END))
            {
                uint32     i;
                uint32     uNumSMPBlocksToAllocate = 4;

                // If reservations are more than the number of blocks we need , no need to allocate
                if (uNumSMPBlocksToAllocate > aReservationPerClient[eClientId])
                {
                    uNumSMPBlocksToAllocate -= aReservationPerClient[eClientId];
                }
                else
                {
                    uNumSMPBlocksToAllocate = 0;
                }

                for (i = 0; i < uNumSMPBlocksToAllocate; i++)
                {
                    if (TRUE == SMPFindFreeBlock(eSourcePipeId, &sSMPConfig[uNumClients].uMacroBlockNum))
                    {
                        sSMPConfig[uNumClients].eClientId = eClientId;
                        uNumClients++;
                    }
                }

                // Failure check ()

            }
            else
            {
                if (TRUE == SMPFindFreeBlock(eSourcePipeId, &sSMPConfig[uNumClients].uMacroBlockNum))
                {
                    sSMPConfig[uNumClients].eClientId = eClientId;
                    uNumClients++;
                }

                if (TRUE == SMPFindFreeBlock(eSourcePipeId, &sSMPConfig[uNumClients].uMacroBlockNum))
                {
                    sSMPConfig[uNumClients].eClientId = (HAL_MDP_SMP_MMBClientId)(eClientId + 1);
                    uNumClients++;
                }

                if (TRUE == SMPFindFreeBlock(eSourcePipeId, &sSMPConfig[uNumClients].uMacroBlockNum))
                {
                    sSMPConfig[uNumClients].eClientId = (HAL_MDP_SMP_MMBClientId)(eClientId + 2);
                    uNumClients++;
                }

                // Failure check
            }

            // Perform allocation in the HAL
            //
            // ** Client must be idle **
            //
            if (uNumClients > 0)
            {
                if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_SMP_Setup((HAL_MDP_SMP_ConfigType*)&sSMPConfig,
                    uNumClients,
                    0x0))
                {
                    eStatus = MDP_STATUS_FAILED;
                }
            }
        }
    }

    return eStatus;
}

/****************************************************************************
*
** FUNCTION: SetupCroppingRectangle()
*/
/*!
* \brief
*   Local helper function to update the cropping rectangle information in Dual pipe/Single pipe scenario
*
* \param  [in] pPanelConfig             - Panel configuration
* \param  [in] pSurfaceInfo             - The source surface information
* \param  [in\Out] pCropRectInfo     - Cropping rectangle information updated in this function
* \param  [in] uPipeIndex                - Pipe index (only used in case of Dual path)
*
*
* \retval void
*
****************************************************************************/
static void SetupCroppingRectangle(MDP_Panel_AttrType        *pPanelConfig, 
                                   MDPSurfaceInfo            *pSurfaceInfo, 
                                   HAL_MDP_CropRectangleType *pCropRectInfo, 
                                   uint32                     uPipeIndex)
{
  
  if (MDP_DUALPIPE_NUM_MIXERS == pPanelConfig->uNumMixers)
  {
    // We split the source surface into two equal halves
    // Left layer displays the left half and the Right layer displays right half 
    // We need to find the Layer at  the current index is left layer or right layer
    HAL_MDP_LayerMixerId              eLayerMixerId        = pPanelConfig->sDisplayControlPath[uPipeIndex].eLayerMixerId;
    HAL_MDP_LayerMixerId              eOtherLayerMixerId   = pPanelConfig->sDisplayControlPath[(uPipeIndex == 0) ? 1 : 0].eLayerMixerId;
    bool32                            bLeft                = (eLayerMixerId < eOtherLayerMixerId)? TRUE : FALSE;

    pCropRectInfo->sSrcRectConfig.uPosX           = (bLeft == TRUE)? 0 :(pSurfaceInfo->uWidth >> 1);
    pCropRectInfo->sSrcRectConfig.uPosY           = 0;
    pCropRectInfo->sSrcRectConfig.uWidthInPixels  = (pSurfaceInfo->uWidth >> 1);
    pCropRectInfo->sSrcRectConfig.uHeightInPixels = pSurfaceInfo->uHeight;
    
    pCropRectInfo->sDstRectConfig.uPosX           = 0;
    pCropRectInfo->sDstRectConfig.uPosY           = 0;
    pCropRectInfo->sDstRectConfig.uWidthInPixels  = pPanelConfig->uDisplayWidth >> 1;
    pCropRectInfo->sDstRectConfig.uHeightInPixels = pPanelConfig->uDisplayHeight;

  }
  else
  {
    pCropRectInfo->sSrcRectConfig.uPosX           = 0;
    pCropRectInfo->sSrcRectConfig.uPosY           = 0;
    pCropRectInfo->sSrcRectConfig.uWidthInPixels  = pSurfaceInfo->uWidth;
    pCropRectInfo->sSrcRectConfig.uHeightInPixels = pSurfaceInfo->uHeight;
    
    pCropRectInfo->sDstRectConfig.uPosX           = 0;
    pCropRectInfo->sDstRectConfig.uPosY           = 0;
    pCropRectInfo->sDstRectConfig.uWidthInPixels  = pPanelConfig->uDisplayWidth;
    pCropRectInfo->sDstRectConfig.uHeightInPixels = pPanelConfig->uDisplayHeight;
  }

}

/****************************************************************************
*
** FUNCTION: SetupInterface()
*/
/*!
* \brief
*   Local helper function to setup the MDP interface
*
* \param  [in] pPanelConfig          - Panel configuration
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status SetupInterface(MDP_Panel_AttrType *pPanelConfig)
{
  MDP_Status                           eStatus = MDP_STATUS_OK;
  MDP_DisplayInterfaceMapType         *pInterfaceMap;
  HAL_MDP_InterfaceConfigType          sInterfaceConfig;
  HAL_MDP_PhyIntf_TimingGenConfigType  sInterfaceTiming;
  HAL_MDP_Interface_DualInferfaceType  sDualInterface;
  uint32                               uI;

  for (uI = 0; uI < pPanelConfig->uNumInterfaces; uI++)
  {
    pInterfaceMap =  (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);

    if (HAL_MDP_INTERFACE_NONE == pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
    {
      continue;
    }

    // 1. Setup interface configuration
    MDP_OSAL_MEMZERO(&sInterfaceConfig, sizeof(HAL_MDP_InterfaceConfigType));
    sInterfaceConfig.pIntrTimingGenConfig = &sInterfaceTiming;
 
    // 2 Setup interface timing
    MDP_OSAL_MEMZERO(&sInterfaceTiming, sizeof(HAL_MDP_PhyIntf_TimingGenConfigType));
    sInterfaceTiming.eInterfaceMode           = pInterfaceMap->eInterfaceMode;
    sInterfaceTiming.eInterfaceStandard       = pInterfaceMap->eInterfaceStandard;
  
    sInterfaceTiming.eInterfacePixelFormat    = MDP_GET_PIXELFMTMAP(pPanelConfig->eColorFormat).eHALPixelFormat;
    sInterfaceTiming.bInterlacedMode          = pPanelConfig->sActiveTiming.bInterlaced;
    sInterfaceTiming.uVisibleWidthInPixels    = pPanelConfig->uDisplayWidth;
    sInterfaceTiming.uVisibleHeightInPixels   = pPanelConfig->uDisplayHeight;
    sInterfaceTiming.uHsyncFrontPorchInPixels = pPanelConfig->sActiveTiming.uHsyncFrontPorchDclk;
    sInterfaceTiming.uHsyncBackPorchInPixels  = pPanelConfig->sActiveTiming.uHsyncBackPorchDclk;
    sInterfaceTiming.uHsyncPulseInPixels      = pPanelConfig->sActiveTiming.uHsyncPulseWidthDclk;
    sInterfaceTiming.uHysncSkewInPixels       = pPanelConfig->sActiveTiming.uHsyncSkewDclk;
    sInterfaceTiming.uVsyncFrontPorchInLines  = pPanelConfig->sActiveTiming.uVsyncFrontPorchLns;
    sInterfaceTiming.uVsyncBackPorchInLines   = pPanelConfig->sActiveTiming.uVsyncBackPorchLns;
    sInterfaceTiming.uVsyncPulseInLines       = pPanelConfig->sActiveTiming.uVsyncPulseWidthLns;
    sInterfaceTiming.uHLeftBorderInPixels     = pPanelConfig->sActiveTiming.uHLeftBorderDClk;
    sInterfaceTiming.uHRightBorderInPixels    = pPanelConfig->sActiveTiming.uHRightBorderDClk;
    sInterfaceTiming.uVTopBorderInLines       = pPanelConfig->sActiveTiming.uVTopBorderLines;
    sInterfaceTiming.uVBottomBorderInLines    = pPanelConfig->sActiveTiming.uVBottomBorderLines;
    sInterfaceTiming.uBorderColorInRGB888     = pPanelConfig->sActiveTiming.uBorderColorRgb888;
    sInterfaceTiming.uUnderflowColorInRGB888  = 0x0;
    sInterfaceTiming.eDataEnableSignal        = (0==pPanelConfig->sActiveTiming.uDataEnInvertSignal)?HAL_MDP_SIGNAL_POLARITY_LOW:HAL_MDP_SIGNAL_POLARITY_HIGH;
    sInterfaceTiming.eVsyncSignal             = (0==pPanelConfig->sActiveTiming.uVsyncInvertSignal) ?HAL_MDP_SIGNAL_POLARITY_LOW:HAL_MDP_SIGNAL_POLARITY_HIGH;
    sInterfaceTiming.eHsyncSignal             = (0==pPanelConfig->sActiveTiming.uHsyncInvertSignal) ?HAL_MDP_SIGNAL_POLARITY_LOW:HAL_MDP_SIGNAL_POLARITY_HIGH;

    /* Handle interface specific configuration */
    switch (pPanelConfig->ePhysConnect)
    {
      case MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO:
      case MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD:
      case MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO:
      case MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD:
        {
          /* Split DSI display */
          if (MDP_INTERFACE_DUAL == pPanelConfig->uNumInterfaces)
          {
            sInterfaceTiming.uVisibleWidthInPixels >>= 1;
          }

          /* Check for DSI FBC */
          if ( TRUE == pPanelConfig->uAttrs.sDsi.bFBCEnable)
          {
            if (pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio)
            {
              //If Frame Buffer is Enabled, the timing could be reduced by the factor of the compression ratio.
              sInterfaceTiming.uVisibleWidthInPixels    /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uHsyncFrontPorchInPixels /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uHsyncBackPorchInPixels  /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uHsyncPulseInPixels      /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uVsyncFrontPorchInLines  /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uVsyncBackPorchInLines   /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uVsyncPulseInLines       /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uHLeftBorderInPixels     /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
              sInterfaceTiming.uHRightBorderInPixels    /= pPanelConfig->uAttrs.sDsi.uFBCCompressionRatio;
            }
            else
            {
              eStatus = MDP_STATUS_BAD_PARAM;    
            }
          }
          break;
        }
      default:
        // No other handling for other interfaces
        break;
    }

    if( 0 != uI )
    {
        sDualInterface.bEnableDualInterface   = TRUE;
        sDualInterface.eInterfaceMode         = pInterfaceMap->eInterfaceMode;
        sInterfaceConfig.pDualInterfaceConfig = &sDualInterface;
    }

    if( MDP_STATUS_OK == eStatus)    
    {
      if (HAL_MDSS_STATUS_SUCCESS == HAL_MDP_Interface_Setup(pPanelConfig->sDisplayControlPath[uI].eInterfaceId,
                                                            &sInterfaceConfig,
                                                             0x00))
      {
        // Clear and disable all interrupts on this interface
        HAL_MDP_InterruptConfigType  sInterrupts[1];

        // Setup for the blt interrupt
        MDP_OSAL_MEMZERO(&sInterrupts, sizeof(sInterrupts));
        sInterrupts[0].eModuleType            = HAL_MDP_MODULE_INTERFACE;
        sInterrupts[0].eModuleId.eInterfaceId = pPanelConfig->sDisplayControlPath[uI].eInterfaceId;
        sInterrupts[0].eInterruptSrc          = HAL_MDP_INTERRUPT_NONE;

        // Disable all unwanted interrupts
        HAL_MDP_Interrupt_Enable((HAL_MDP_InterruptConfigType*)&sInterrupts, 1, 0x0);

        // Clear all interrupt status
        sInterrupts[0].eInterruptSrc = (HAL_MDP_InterruptType)(HAL_MDP_INTERRUPT_UNDERRUN | HAL_MDP_INTERRUPT_VSYNC); /* TODO: Replace with HAL_MDP_INTERRUPT_ALL (when vailable) */
        HAL_MDP_Interrupt_Clear((HAL_MDP_InterruptConfigType*)&sInterrupts, 1, 0x0);
      }
      else
      {
        eStatus = MDP_STATUS_FAILED;          
      }
    }
  }

  return eStatus;
}   
   
/* -----------------------------------------------------------------------
**
** FUNCTION: displayCommandModeVsyncConfig
**
** DESCRIPTION:
**   This API is used to config vsync counter for smart panel
**
** INPUT:
**   pPanelAttr       - panel attributes
**   uPathIndex       - path index number for dual dsi case
**
** OUTPUT:
**   pVsyncConfig     - vsync counter configuration
**
** RETURN:
**   none
**
** ----------------------------------------------------------------------- */
static void displayCommandModeVsyncConfig(MDP_Panel_AttrType *pPanelAttr, HAL_MDP_PingPong_VsyncConfigType *pVsyncConfig, uint32 uPathIndex)
{
    float                               fRefreshRate       = 0.0;
    float                               fVsyncCount        = 0.0;
    float                               fVsyncHeight       = 0.0;
    float                               fVariance          = 0.0;
    float                               fPXOClock          = 0.0;

    fPXOClock = (float)MDP_DEFAULT_PXO_CLK;

    // auto-refresh info
    pVsyncConfig->bEnableAutoRefresh      = pPanelAttr->uAttrs.sDsi.bEnableAutoRefresh;
    pVsyncConfig->uAutoRefreshFrameNumDiv = pPanelAttr->uAttrs.sDsi.uAutoRefreshFrameNumDiv;
    if(0 == pVsyncConfig->uAutoRefreshFrameNumDiv)
    {
        pVsyncConfig->uAutoRefreshFrameNumDiv = 1;
    }

    // Calculate requested refresh rate
    fRefreshRate = (float)(pPanelAttr->uAttrs.sDsi.uRefreshRate >> 16);

    // Get TE variance
    fVariance = (float)(pPanelAttr->uAttrs.sDsi.sTE.vSyncPercentVariance >> 16);

    // Calculate the require vsync count.
    // Calculation is equal to the number of vsync ticks it takes to complete a single scanline.
    //          MDP_VSYNC_CLK / (display height * refresh rate)
    //
    //  This clock counts at the same refresh as the panel, if TE is enable the TE synchronizes this counter with the panel.
    //
    fVsyncCount = (float)(fPXOClock/(pPanelAttr->uDisplayHeight*fRefreshRate));

    // apply variance on vsync height
    fVsyncHeight = (float)(pPanelAttr->uDisplayHeight + pPanelAttr->sActiveTiming.uVsyncBackPorchLns + pPanelAttr->sActiveTiming.uVsyncFrontPorchLns - 1);
    if (((int)fVariance > 0) && ((int)fVariance < 100))
    {
        fVsyncHeight = (float)fVsyncHeight / (1 - (fVariance / 100));
    }

    pVsyncConfig->uVsyncCount        = (uint32)fVsyncCount;
    pVsyncConfig->uVsyncHeight       = (uint32)fVsyncHeight;
    pVsyncConfig->uVsyncRdPtrIrqLine = (0 == pPanelAttr->uAttrs.sDsi.sTE.vSyncRdPtrIrqLine) ? 1 : pPanelAttr->uAttrs.sDsi.sTE.vSyncRdPtrIrqLine;
    pVsyncConfig->uVsyncWrPtrIrqLine = pPanelAttr->uDisplayHeight;

    // for dual-dsi case, we will need to disable auto-refresh for slave controller
    // because the slave is always configured to be triggered by the master
    if ((MDP_DUALPIPE_NUM_MIXERS - 1) == uPathIndex)
    {
        pVsyncConfig->bEnableAutoRefresh = FALSE;
    }
}

/* -----------------------------------------------------------------------
**
** FUNCTION: displayCommandModeTearCheckConfig
**
** DESCRIPTION:
**   This API is used to get/calculate tear check settings for smart panel
**
** INPUT:
**   pPanelAttr       - panel attributes
**   uPathIndex       - path index number for dual dsi case
**
** OUTPUT:
**   pTEConfig        - TE configuration
**
** RETURN:
**   none
**
** ----------------------------------------------------------------------- */
static void displayCommandModeTearCheckConfig(MDP_Panel_AttrType *pPanelAttr, HAL_MDP_PingPong_TEConfigType *pTEConfig, uint32 uPathIndex)
{
    uint32                              uDivisor       = 1;
    uint32                              uStartPosition = 0;


    // adjust TE continue threshold
    pTEConfig->uContinueThreshold     = (0 == pPanelAttr->uAttrs.sDsi.sTE.vSyncContinueLines)? 
                                       (MDP_DEFAULT_VSYNC_CONTINUE_LINES) : 
                                       (pPanelAttr->uAttrs.sDsi.sTE.vSyncContinueLines);
    if(pTEConfig->uContinueThreshold > 2*pPanelAttr->uDisplayHeight)
    {
        pTEConfig->uContinueThreshold = 0;
    }

    // adjust TE start threshold
    uDivisor = (0 == pPanelAttr->uAttrs.sDsi.sTE.vSyncStartLineDivisor)?
               (MDP_DEFAULT_VSYNC_START_LINE_DIVISOR) : 
               (pPanelAttr->uAttrs.sDsi.sTE.vSyncStartLineDivisor);
    if(uDivisor > 0)
    {
        pTEConfig->uStartThreshold = pPanelAttr->uDisplayHeight/uDivisor;
    }

    /* adjust TE start position */
    uStartPosition         = (0 == pPanelAttr->uAttrs.sDsi.sTE.vSyncStartPos)?
                             (pPanelAttr->sActiveTiming.uVsyncBackPorchLns+1) : 
                             (pPanelAttr->uAttrs.sDsi.sTE.vSyncStartPos);

    /* dedicated pin used */
    pTEConfig->bDedicatedTEPin        = pPanelAttr->uAttrs.sDsi.sTE.bDedicatedTEPin;
    /* external vsync source select */
    pTEConfig->eVsyncSelect           = (HAL_MDP_PingPong_VsyncSelectType)pPanelAttr->uAttrs.sDsi.sTE.vSyncSelect;


    /* For dual DSI case, some panel requires the master controller transfers data on the data lane some time ahead of 
        the slave controller. Current solution is to use the start window in TE block to achieve this, which requires 
        driver to set different start position for the controllers. 

                                (start_pos)      (start_pos+TH)
        master controller       |               |
                                +---------------+

                                                    (start_pos)      (start_pos+TH)
        slave controller                              |               |
                                                      +---------------+

                                                |     |
                                             -->|  x  |<--   (x is the skew lines, which is pPanelAttr->uAttrs.sDsi.iSlaveControllerSkewLines)
                                                |     |
           
        Notes:
            -  This adjustment should be ignored for single DSI case.
            -  For dual DSI case:
                (i)   (iSlaveControllerSkewLines > 0) means master controller transfers data ahead of slave controller
                (ii)  (iSlaveControllerSkewLines == 0) means two controllers can transfer data at same time
                (iii) (iSlaveControllerSkewLines < 0) means master controller transfers data after slave controller
    */

    pTEConfig->uStartPosition = uStartPosition;

    if ((MDP_DUALPIPE_NUM_MIXERS == pPanelAttr->uNumMixers) &&
        (MDP_INTERFACE_DUAL == pPanelAttr->uNumInterfaces))
    {
        /* if this dual pipe case */
        if (pPanelAttr->uAttrs.sDsi.iSlaveControllerSkewLines > 0)
        {
            /* Since iSlaveControllerSkewLines is positive, we need to adjust the start position for slave controller. */
            if ((MDP_DUALPIPE_NUM_MIXERS - 1) == uPathIndex)
            {
                pTEConfig->uStartPosition += (pTEConfig->uStartThreshold + pPanelAttr->uAttrs.sDsi.iSlaveControllerSkewLines);
            }
        }
        else if (pPanelAttr->uAttrs.sDsi.iSlaveControllerSkewLines == 0)
        {
            /* do nothing here. TE window is same for both controllers since there's no skew setting */
        }
        else
        {
            /* Since iSlaveControllerSkewLines is negative, we need to adjust the start position for master controller.*/
            if (0 == uPathIndex)
            {
                pTEConfig->uStartPosition += (pTEConfig->uStartThreshold + (-1)*pPanelAttr->uAttrs.sDsi.iSlaveControllerSkewLines);
            }
        }
    }
}

/****************************************************************************
*
** FUNCTION: SetupPingPong()
*/
/*!
* \brief
*   Local helper function to setup the pingpong block
*
* \param  [in] pPanelConfig          - Panel configuration
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status SetupPingPong(MDP_Panel_AttrType *pPanelConfig)
{
    MDP_Status                          eStatus            = MDP_STATUS_OK;
    MDP_DisplayInterfaceMapType        *pInterfaceMap      = NULL;
    HAL_MDP_PingPong_VsyncConfigType    sVsyncConfig;
    HAL_MDP_PingPong_TEConfigType       sTEConfig;
    HAL_MDP_PingPongConfigType          sPingpongConfig;
    HAL_MDP_PingPong_FBCConfigType      sFBCConfig; 
    uint32                              uI;

    for(uI = 0; uI < pPanelConfig->uNumInterfaces; uI++ )
    {
      pInterfaceMap   = (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);

      if (HAL_MDP_INTERFACE_NONE == pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
      {
        continue;
      }

      /* Setup for command mode only */
      if(HAL_MDP_INTERFACE_MODE_COMMAND == pInterfaceMap->eInterfaceMode)
      {
        MDP_OSAL_MEMZERO(&sPingpongConfig, sizeof(HAL_MDP_PingPongConfigType));
        sPingpongConfig.pIntrVsyncConfig = &sVsyncConfig;
        sPingpongConfig.pIntrTEConfig    = &sTEConfig;

        /* get the vsync counter configuration */
        MDP_OSAL_MEMZERO(&sVsyncConfig, sizeof(HAL_MDP_PingPong_VsyncConfigType));
        displayCommandModeVsyncConfig(pPanelConfig, &sVsyncConfig, uI);

        /* get the TE configuraion */
        MDP_OSAL_MEMZERO(&sTEConfig, sizeof(HAL_MDP_PingPong_TEConfigType));
        displayCommandModeTearCheckConfig(pPanelConfig, &sTEConfig, uI);

        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_PingPong_Setup(pPanelConfig->sDisplayControlPath[uI].ePingPongId, &sPingpongConfig, 0x00))
        {
          eStatus = MDP_STATUS_FAILED;        
        }
      }

      /* Setup Frame Buffer Compression */
      if (MDP_STATUS_OK == eStatus)
      {
        /* Handle interface specific configurations */
        switch (pPanelConfig->ePhysConnect)
        {
          case MDP_DISPLAY_CONNECT_PRIMARY_DSI_VIDEO:
          case MDP_DISPLAY_CONNECT_PRIMARY_DSI_CMD:
          case MDP_DISPLAY_CONNECT_SECONDARY_DSI_VIDEO:
          case MDP_DISPLAY_CONNECT_SECONDARY_DSI_CMD:
          {
            if ( TRUE == pPanelConfig->uAttrs.sDsi.bFBCEnable)
            {
              MDP_OSAL_MEMZERO(&sFBCConfig,       sizeof(HAL_MDP_PingPong_FBCConfigType));
              MDP_OSAL_MEMZERO(&sPingpongConfig,  sizeof(HAL_MDP_PingPongConfigType));
            
              sPingpongConfig.pFBCConfig      = &sFBCConfig;

              if (pPanelConfig->uAttrs.sDsi.uFBCProfileID< MDP_FBC_PROFILEID_MAX)
              {
                uint32              uWithdMod         = 0;
                uint32              uNumOfBlock       = 0;
                uint32              uPartialPx        = 0;
                MDP_FbcProfileMode  *psFbcProfileMode = NULL;

                /* Set FBC Mode*/
                psFbcProfileMode = (MDP_FbcProfileMode*)&(gFbcProfileModes[pPanelConfig->uAttrs.sDsi.uFBCProfileID]);
                sFBCConfig.bFBCEnable           = TRUE;
                sFBCConfig.bBflcEnable          = psFbcProfileMode->bBflcEnable;
                sFBCConfig.bVlcEnable           = psFbcProfileMode->bVlcEnable;
                sFBCConfig.bPatEnable           = psFbcProfileMode->bPatEnable;
                sFBCConfig.bQErrEnable          = psFbcProfileMode->bQErrEnable;
                sFBCConfig.bPlanar              = psFbcProfileMode->bPlanar;
                sFBCConfig.uCdBias              = psFbcProfileMode->uCdBias;
                sFBCConfig.uLossyRGBThd         = psFbcProfileMode->uLossyRgbThreadhold;
                if (MDP_INTERFACE_DUAL == pPanelConfig->uNumInterfaces)
                {
                  sFBCConfig.uScreenWidthInPixels = pPanelConfig->uDisplayWidth>>1;
                }
                else
                {
                  sFBCConfig.uScreenWidthInPixels = pPanelConfig->uDisplayWidth;
                }
                

                sFBCConfig.uLossyModeIdx        = MDP_FBC_LOSSYMODEIDX;
                sFBCConfig.uLossyModeThd        = MDP_FBC_LOSSYMODETHD;
                sFBCConfig.uLosslessModeThd     = MDP_FBC_LOSSLESSMODETHD;
                sFBCConfig.uBlockExtraBudget    = MDP_FBC_BLOCKEXTRABUDGET;

                /* Set FBC Budget */
                /* Compression is done in 8 pixel block, we need to find the pixel number for the last block */
                uWithdMod     = (sFBCConfig.uScreenWidthInPixels)%8;               
                uNumOfBlock   = (sFBCConfig.uScreenWidthInPixels) >>3;
                sFBCConfig.uBlockBudget  = 96 - sFBCConfig.uBlockExtraBudget ;

                /* If the last block is 8 pixels or 6 pixels, Line Extra Budget will be the block extra budget x block number*/
                if ( 0 == uWithdMod || 6 == uWithdMod)
                {
                  sFBCConfig.uLineExtraBudget = sFBCConfig.uBlockExtraBudget * uNumOfBlock;
                }
                else
                {
                  uPartialPx       = sFBCConfig.uScreenWidthInPixels - (uNumOfBlock <<3);
                  sFBCConfig.uLineExtraBudget = sFBCConfig.uBlockExtraBudget * uNumOfBlock -sFBCConfig.uBlockBudget + 12*uPartialPx;
                }

                if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_PingPong_Setup(pPanelConfig->sDisplayControlPath[uI].ePingPongId, &sPingpongConfig, 0x00))
                {
                  eStatus = MDP_STATUS_FAILED;        
                }
              }
              else
              {
                eStatus = MDP_STATUS_BAD_PARAM;
              }
           }
          }
          break;
        default:
          // No other handling for other interfaces
          break;
        }
      }
    }
    return eStatus;
}


/****************************************************************************
*
** FUNCTION: SetupControlPath()
*/
/*!
* \brief
*   Local helper function to setup the MDP control path
*
* \param  [in] pPanelConfig          - Panel configuration
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status SetupControlPath(MDP_Panel_AttrType *pPanelConfig)
{
  MDP_Status                                      eStatus = MDP_STATUS_OK;
  MDP_DisplayInterfaceMapType                    *pInterfaceMap;
  HAL_MDP_ControlPathConfigType                   sControlPathConfig;
  HAL_MDP_ControlPath_MixerConfigType             sMixerConfig;
  HAL_MDP_ControlPath_InterfaceConfigType         sInterfaceCfg;
  HAL_MDP_LayerMixerConfigType                    sLayerMixerCfg;
  HAL_MDP_ControlPath_Mixer_ZOrderConfigType      sZorderConfig;
  HAL_MDP_ControlPath_Mixer_BorderColorConfigType sBorderColorConfig;
  HAL_MDP_Mixer_OutputSizeConfigType              sMixerOutputConfig;
  HAL_MDP_Mixer_BorderColorConfigType             sBorderOutputColorConfig;
  HAL_MDP_Mixer_BlendStageConfigType              sBlendStageConfig;
  uint32                                          uI;

  // Setup Control Path
  for (uI = 0; uI < pPanelConfig->uNumInterfaces; uI++)
  {
      if (HAL_MDP_INTERFACE_NONE != pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
      {
          // Ensure interface is disable before programming    
          if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Interface_Enable(pPanelConfig->sDisplayControlPath[uI].eInterfaceId,
              FALSE,
              0x0))
          {
              DEBUG((EFI_D_WARN, "MDPLib: Failed to disable the interface failed!\n"));
          }
      }
  }

  for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
  {
    pInterfaceMap = (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);
    
    // Setup border color
    MDP_OSAL_MEMZERO(&sBorderColorConfig, sizeof(HAL_MDP_ControlPath_Mixer_BorderColorConfigType));
    sBorderColorConfig.bBorderColorEnable = TRUE;
    
    // Setup the ZOrder
    MDP_OSAL_MEMZERO(&sZorderConfig, sizeof(HAL_MDP_ControlPath_Mixer_ZOrderConfigType));
    sZorderConfig.eSourcePipe[HAL_MDP_BLEND_ORDER_STAGE_0_FG_LAYER] = pPanelConfig->sDisplayControlPath[uI].eSourcePipeId;
    
    // Setup the mixer configuration
    MDP_OSAL_MEMZERO(&sMixerConfig, sizeof(HAL_MDP_ControlPath_MixerConfigType));
    sMixerConfig.eMixerId                 = pPanelConfig->sDisplayControlPath[uI].eLayerMixerId;
    sMixerConfig.psMixerZOrderConfig      = &sZorderConfig;
    sMixerConfig.psMixerBorderColorConfig = &sBorderColorConfig;
    
    // Setup the interface configuration
    if (HAL_MDP_INTERFACE_NONE != pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
    {
      MDP_OSAL_MEMZERO(&sInterfaceCfg, sizeof(HAL_MDP_ControlPath_InterfaceConfigType));
      sInterfaceCfg.eInterfaceId       = pPanelConfig->sDisplayControlPath[uI].eInterfaceId;
      sInterfaceCfg.eInterfaceModeType = pInterfaceMap->eInterfaceMode;
    }

    // Setup 3D Mux ( Only for Dual Pipe Mode )
    if (MDP_DUALPIPE_NUM_MIXERS == pPanelConfig->uNumMixers)
    {
       // Setup 3D Mux
       sInterfaceCfg.b3DMuxEnable         = MDP_GET_DUALPIPEMUX(pPanelConfig->ePhysConnect);
       if (TRUE == sInterfaceCfg.b3DMuxEnable)
       {
         sInterfaceCfg.eMuxPackerModeConfig = HAL_MDP_3DMUX_PACK_MODE_HORZ_ROW_INTERLEAVE;
       }
    }
    
    // Setup mixer output configuration
    MDP_OSAL_MEMZERO(&sMixerOutputConfig, sizeof(HAL_MDP_Mixer_OutputSizeConfigType));
    // In dual pipe scenario each mixer will output width equal to half of the panel width
    if (MDP_DUALPIPE_NUM_MIXERS == pPanelConfig->uNumMixers)
    {
       sMixerOutputConfig.uOutputWidthInPixels = pPanelConfig->uDisplayWidth>> 1;
    }
    else
    {
       sMixerOutputConfig.uOutputWidthInPixels = pPanelConfig->uDisplayWidth;
    }
    sMixerOutputConfig.uOutputHeightInPixels = pPanelConfig->uDisplayHeight;
    
    // Setup the layer mixer config (default to black)
    MDP_OSAL_MEMZERO(&sBorderOutputColorConfig, sizeof(HAL_MDP_Mixer_BorderColorConfigType));
    sBorderOutputColorConfig.uColorValue = 0x0;
    
    // Setup opaque blending (no alpha blending
    MDP_OSAL_MEMZERO(&sBlendStageConfig, sizeof(HAL_MDP_Mixer_BlendStageConfigType));
    // Background is using (1-A) * FG_Alpha
    sBlendStageConfig.sBackGroundAlphaBlend.bConfigure    = TRUE;
    sBlendStageConfig.sBackGroundAlphaBlend.eAlphaSel     = HAL_MDP_ALPHA_SRC_FG_CONST;
    sBlendStageConfig.sBackGroundAlphaBlend.bInverseAlpha = TRUE;
    // Foreground is using 100% * FG_Alpha
    sBlendStageConfig.sForeGroundAlphaBlend.bConfigure    = TRUE;
    sBlendStageConfig.sForeGroundAlphaBlend.eAlphaSel     = HAL_MDP_ALPHA_SRC_FG_CONST;
    sBlendStageConfig.sForeGroundAlphaBlend.uConstant     = 0xFF;
    
    // Setup the layer mixer config
    MDP_OSAL_MEMZERO(&sLayerMixerCfg, sizeof(HAL_MDP_LayerMixerConfigType));
    sLayerMixerCfg.eLayerMixerId         = pPanelConfig->sDisplayControlPath[uI].eLayerMixerId;
    sLayerMixerCfg.psOutputSizeConfig    = &sMixerOutputConfig;
    sLayerMixerCfg.psBorderColorConfig   = &sBorderOutputColorConfig;
    sLayerMixerCfg.psBlendStageConfig[0] = &sBlendStageConfig;
    
    // Setup the control path
    MDP_OSAL_MEMZERO(&sControlPathConfig, sizeof(HAL_MDP_ControlPathConfigType));
    sControlPathConfig.psMixerConfig      = &sMixerConfig;
    sControlPathConfig.psLayerMixerConfig = &sLayerMixerCfg;    
    if (HAL_MDP_INTERFACE_NONE != pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
    {
      sControlPathConfig.psInterfaceConfig  = &sInterfaceCfg;
    }

    
    // Program the control path
    if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Setup(pPanelConfig->sDisplayControlPath[uI].eControlPathId, &sControlPathConfig, 0))
    {
        DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Setup failed!\n"));
        eStatus = MDP_STATUS_FAILED;
    }

  }

  return eStatus;
}  

/****************************************************************************
*
** FUNCTION: SetupPixelExt()
*/
/*!
* \brief
*   Local helper function to setup the pixel extension
*
* \param [out] pPixelExtConfig          - Pointer to the pixel extension structure
*        [in]  pCropRectInfo            - Surface cropping information
*        [in]  uNumMixers               - Number of mixers 
*        [in]  uFlags                   - Reserved flags
*
* \retval none
*
****************************************************************************/
static void SetupPixelExt(HAL_MDP_PixelExtLayerConfigType *pPixelExtConfig, HAL_MDP_CropRectangleType *pCropRectInfo, uint32 uNumMixers, uint32 uFlags)
{
    uint32                                uI;
    HAL_MDP_ScalerPlaneType              *pScalerConfig;
    HAL_MDP_PixelExtOverrideConfigType   *pPixelExtOverrideConfig;


    for (uI = 0; uI < HAL_MDP_SOURCE_PIPE_MAX_COLOR_COMPONENTS; uI++)
    {
        uint64 uLclSrcWidthInPixels  = pCropRectInfo->sSrcRectConfig.uWidthInPixels;
        uint64 uLclSrcHeightInPixels = pCropRectInfo->sSrcRectConfig.uHeightInPixels;

        pScalerConfig           = &pPixelExtConfig->aPixelExtComponentConfig[uI].sScalerPlane;
        pPixelExtOverrideConfig = &pPixelExtConfig->aPixelExtComponentConfig[uI].sPixelExtOverrideConfig;

        // Set initial phase 0
        pScalerConfig->iInitPhaseX = 0;
        pScalerConfig->iInitPhaseY = 0;
        // Set scaler phase step
        if (pCropRectInfo->sSrcRectConfig.uWidthInPixels < pCropRectInfo->sDstRectConfig.uWidthInPixels)
        {
            pScalerConfig->uPhaseStepX = (uint32)((uLclSrcWidthInPixels << MDP_PHASE_STEP_CONSTANT) / pCropRectInfo->sDstRectConfig.uWidthInPixels);
        }
        else
        {
            pScalerConfig->uPhaseStepX = 0;
        }
        if (pCropRectInfo->sSrcRectConfig.uHeightInPixels < pCropRectInfo->sDstRectConfig.uHeightInPixels)
        {
            pScalerConfig->uPhaseStepY = (uint32)((uLclSrcHeightInPixels << MDP_PHASE_STEP_CONSTANT) / pCropRectInfo->sDstRectConfig.uHeightInPixels);
        }
        else
        {
            pScalerConfig->uPhaseStepY = 0;
        }
        // Set scaler horizontal/vertical filter to bilinear
        pScalerConfig->eHorScaleFilter = HAL_MDP_SCALE_FILTER_BILINEAR;
        pScalerConfig->eVerScaleFilter = HAL_MDP_SCALE_FILTER_BILINEAR;

        // Set pixel extension left to 0
        pPixelExtOverrideConfig->iLeftOverFetch    = 0;
        pPixelExtOverrideConfig->uLeftRepeat       = 0;

        // If x position is greater than 0, then this is the right pipe for dual-pipe case.
        // We can only repeat the right side
        if (pCropRectInfo->sSrcRectConfig.uPosX > 0)
        {
            pPixelExtOverrideConfig->iRightOverFetch = 0;
            pPixelExtOverrideConfig->uRightRepeat = 1;
        }
        else
        {
            // If single mixer is used, then this is the only pipe and we can only repeat the right side
            if (uNumMixers == 1)
            {
                pPixelExtOverrideConfig->iRightOverFetch = 0;
                pPixelExtOverrideConfig->uRightRepeat = 1;
            }
            // If two mixers are used, then this is the left pipe for dual-pipe case
            // We can over-fetch the right side
            else
            {
                pPixelExtOverrideConfig->iRightOverFetch = 1;
                pPixelExtOverrideConfig->uRightRepeat = 0;
            }
        }
        pPixelExtOverrideConfig->iTopOverFetch     = 0;
        pPixelExtOverrideConfig->uTopRepeat        = 0;
        pPixelExtOverrideConfig->iBottomOverFetch  = 0;
        pPixelExtOverrideConfig->uBottomRepeat     = 1;
        pPixelExtOverrideConfig->uTopBottomReqPxls = pCropRectInfo->sSrcRectConfig.uHeightInPixels + 1;
        pPixelExtOverrideConfig->uLeftRightReqPxls = pCropRectInfo->sSrcRectConfig.uWidthInPixels  + 1;
    }
    pPixelExtConfig->bEnabled = TRUE;
}

/****************************************************************************
*
** FUNCTION: SetupSourcePath()
*/
/*!
* \brief
*   Local helper function to setup the MDP control path
*
* \param [in] pPanelConfig          - Panel configuration
*        [in] pSurfaceInfo          - Surface configuration
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status SetupSourcePath(MDP_Panel_AttrType *pPanelConfig, MDPSurfaceInfo *pSurfaceInfo)
{
  MDP_Status                      eStatus = MDP_STATUS_OK;
  HAL_MDP_SourcePipeConfigType    sSourcePipeConfig;
  HAL_MDP_CropRectangleType       sCropRectInfo;
  HAL_MDP_SurfaceAttrType         sSurfaceInfo;
  HAL_MDP_PixelExtLayerConfigType sPixelExtConfig;
  uint32                          uI;
  
  for(uI = 0; uI < pPanelConfig->uNumMixers; uI++)
  {    
    MDP_OSAL_MEMZERO(&sSurfaceInfo, sizeof(HAL_MDP_SurfaceAttrType));
    sSurfaceInfo.ePixelFormat   = MDP_GET_PIXELFMTMAP(pSurfaceInfo->ePixelFormat).eHALPixelFormat;
    sSurfaceInfo.uHeightInPixel = pSurfaceInfo->uHeight;
    sSurfaceInfo.uWidthInPixel  = pSurfaceInfo->uWidth;
    sSurfaceInfo.uPlanes.sRgb.uStrideInBytes           = pSurfaceInfo->uPlane0Stride;
    sSurfaceInfo.uPlanes.sRgb.sDeviceAddress.iQuadPart = (int64)pSurfaceInfo->pPlane0Offset;

    MDP_OSAL_MEMZERO(&sCropRectInfo, sizeof(HAL_MDP_CropRectangleType));
    SetupCroppingRectangle(pPanelConfig, pSurfaceInfo, &sCropRectInfo, uI);
    
    MDP_OSAL_MEMZERO(&sPixelExtConfig, sizeof(HAL_MDP_PixelExtLayerConfigType));
    SetupPixelExt(&sPixelExtConfig, &sCropRectInfo, pPanelConfig->uNumMixers, 0x00);

    MDP_OSAL_MEMZERO(&sSourcePipeConfig, sizeof(HAL_MDP_SourcePipeConfigType));
    sSourcePipeConfig.psSurfaceInfo         = &sSurfaceInfo;        
    sSourcePipeConfig.psCropRectInfo        = &sCropRectInfo;
    sSourcePipeConfig.psPixelExtLayerConfig = &sPixelExtConfig;

    // Start Source Pipe configuration (default is RGB)
    if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_SourcePipe_Setup(pPanelConfig->sDisplayControlPath[uI].eSourcePipeId,
                                                            HAL_MDP_LAYER_TYPE_RGB,
                                                            &sSourcePipeConfig,
                                                            0x0))
    {
        DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_SourcePipe_Setup failed!\n"));
        eStatus = MDP_STATUS_FAILED;
    }
    else if (MDP_STATUS_OK != SMPPoolAllocate(pPanelConfig->sDisplayControlPath[uI].eSourcePipeId))
    {
        // Free any allocated blocks on failure
        SMPFreeBlock(pPanelConfig->sDisplayControlPath[uI].eSourcePipeId);
        eStatus = MDP_STATUS_FAILED;
    }

  }

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: StartPingPong()
*/
/*!
* \brief
*   Start pingpong block
*
* \param [in] ePingPongId   - pingpong block id
*        [in] pPanelConfig  - panel configuration info
*
* \retval MDP_Status
*
****************************************************************************/
static MDP_Status StartPingPong(HAL_MDP_PingPongId ePingPongId, MDP_Panel_AttrType *pPanelConfig)
{
    MDP_Status                             eStatus = MDP_STATUS_OK;
    HAL_MDP_PingPong_VsyncEnableType       sVsyncEnable;
    HAL_MDP_PingPong_TEEnableType          sTeEnable;
    HAL_MDP_PingPongConfigType             sPingpongConfig;

    MDP_OSAL_MEMZERO(&sPingpongConfig, sizeof(HAL_MDP_PingPongConfigType));
    MDP_OSAL_MEMZERO(&sVsyncEnable,    sizeof(HAL_MDP_PingPong_VsyncEnableType));
    MDP_OSAL_MEMZERO(&sTeEnable,       sizeof(HAL_MDP_PingPong_TEEnableType));

    sPingpongConfig.pIntrVsyncEnable = &sVsyncEnable;
    sPingpongConfig.pIntrTEEnable    = &sTeEnable;

    // always enable vsync counter for command mode
    sVsyncEnable.bEnable             = TRUE;

    // TE enable is actually controlled in panel configuration
    sTeEnable.bEnable                = pPanelConfig->uAttrs.sDsi.sTE.bTECheckEnable;

    if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_PingPong_Setup(ePingPongId, &sPingpongConfig, 0x00))
    {
        eStatus = MDP_STATUS_FAILED;          
    }

    // TE is not used, therefore, don't need to enable TE
    return eStatus;
}



/*=========================================================================
      MDP Lib APIs
==========================================================================*/



/****************************************************************************
*
** FUNCTION: MDPSetupPipe()
*/
/*!
* \brief
*   Setup the MDP for a basic single layer operation
*
* \param [in] pPanelConfig       - The display configuration to setup
*        [in] pSurfaceInfo       - The source surface to fetch from
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPSetupPipe(MDP_Panel_AttrType *pPanelConfig, MDPSurfaceInfo *pSurfaceInfo)
{
   MDP_Status    eStatus = MDP_STATUS_OK;

   if ((NULL == pPanelConfig) ||
       (NULL == pSurfaceInfo))
   {
      eStatus = MDP_STATUS_BAD_PARAM;
   }
   else if ((pPanelConfig->eDisplayId >= MDP_DISPLAY_MAX) ||
            (pSurfaceInfo->ePixelFormat >= MDP_PIXEL_FORMAT_MAX))
   {
      eStatus = MDP_STATUS_BAD_PARAM;
   }
   else if (TRUE == MDP_GET_PIXELFMTMAP(pSurfaceInfo->ePixelFormat).bYUV)
   {
      // Don't support YUV formats
      eStatus = MDP_STATUS_BAD_PARAM;
   }
   else if (MDP_STATUS_OK != (eStatus = UpdateDisplayControlpathInfo(pPanelConfig)))
   {
       // Control path update failed
   }
   else if (MDP_STATUS_OK != (eStatus = SetupControlPath(pPanelConfig)))
   {
     // Control path failed
   }
   else if (MDP_STATUS_OK != (eStatus = SetupSourcePath(pPanelConfig, pSurfaceInfo)))
   {
     // Source path failed
   }
   else if (MDP_STATUS_OK != (eStatus = SetupInterface(pPanelConfig)))
   {
     // Interface config has failed
   }
   else if (MDP_STATUS_OK != (eStatus = SetupPingPong(pPanelConfig)))
   {
     // PingPong config has failed
   }
   else
   {
      // Everything has passed, commit the changes
      HAL_MDP_ControlPathFlushType  sFlushType;
      uint32                        uI;

      for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
      {        
        MDP_OSAL_MEMZERO(&sFlushType, sizeof(HAL_MDP_ControlPathFlushType));
        sFlushType.uFlushModules = MDP_GET_FLUSHFLAGS(pPanelConfig->eDisplayId, uI);
        
        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Commit(pPanelConfig->sDisplayControlPath[uI].eControlPathId,
                                                                  &sFlushType,
                                                                  HAL_MDP_INTERFACE_TYPE_PHYSICAL_CONNECT,
                                                                  0x0))
        {
            DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Commit failed!\n"));
            eStatus = MDP_STATUS_FAILED;
        }
      }
   }

   return eStatus;
}


/****************************************************************************
*
** FUNCTION: MDPStartPipe()
*/
/*!
* \brief
*   Start the MDP pipe 
*
* \param [in] pPanelConfig       - The panel configuration
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPStartPipe(MDP_Panel_AttrType *pPanelConfig)
{
  MDP_Status    eStatus = MDP_STATUS_OK;

  if ((NULL == pPanelConfig )||
      (MDP_DISPLAY_MAX <= pPanelConfig->eDisplayId) )
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    uint32                                          uI;

    for (uI = 0; uI < pPanelConfig->uNumInterfaces; uI++)
    {
      MDP_DisplayInterfaceMapType *pInterfaceMap    = (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);

      if (HAL_MDP_INTERFACE_NONE == pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
      {
        continue;
      }

      // command mode
      if(HAL_MDP_INTERFACE_MODE_COMMAND == pInterfaceMap->eInterfaceMode)
      {
        // disable interface timing
        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Interface_Enable(pPanelConfig->sDisplayControlPath[uI].eInterfaceId,
                                                                TRUE,
                                                                0x0))
        {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_Interface_Enable failed!\n"));
          eStatus = MDP_STATUS_FAILED;

        }
        else
        {
          // start vsync counter in the pingpong block
          eStatus = StartPingPong(pPanelConfig->sDisplayControlPath[uI].ePingPongId, pPanelConfig);
        }
      }
      // video mode
      else
      {
        // enable the interface timing
        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Interface_Enable(pPanelConfig->sDisplayControlPath[uI].eInterfaceId,
                                                                TRUE,
                                                                0x0))
        {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_Interface_Enable failed!\n"));
          eStatus = MDP_STATUS_FAILED;

        }
      }
    }

    if (MDP_STATUS_OK==eStatus)
    {
      // Everything has passed, commit the changes
      HAL_MDP_ControlPathFlushType  sFlushType;

      for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
      {
        MDP_OSAL_MEMZERO(&sFlushType, sizeof(HAL_MDP_ControlPathFlushType));
        sFlushType.uFlushModules = HAL_CONTROL_PATH_FLUSH_TIMING_1 | HAL_CONTROL_PATH_FLUSH_TIMING_2;

        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Commit(pPanelConfig->sDisplayControlPath[uI].eControlPathId,
                                                                  &sFlushType,
                                                                  HAL_MDP_INTERFACE_TYPE_PHYSICAL_CONNECT,
                                                                  0x0))
        {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Commit failed!\n"));
          eStatus = MDP_STATUS_FAILED;
        }
      }
    }

  }

  return eStatus;
}




/****************************************************************************
*
** FUNCTION: MDPStopPipe()
*/
/*!
* \brief
*   Stop the MDP pipe 
*
* \param [in] pPanelConfig       - The panel configuration
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPStopPipe(MDP_Panel_AttrType *pPanelConfig)
{
  MDP_Status    eStatus = MDP_STATUS_OK;

  if (NULL == pPanelConfig) 
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    HAL_MDP_PingPong_VsyncEnableType       sVsyncEnable;
    HAL_MDP_PingPong_TEEnableType          sTeEnable;
    HAL_MDP_PingPongConfigType             sPingpongConfig;
    uint32                                 uI;

    for (uI = 0; uI < pPanelConfig->uNumInterfaces; uI++)
    {
      if (HAL_MDP_INTERFACE_NONE == pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
      {
        continue;
      }

      // disable interface timing
      if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Interface_Enable(pPanelConfig->sDisplayControlPath[uI].eInterfaceId,
                                                              FALSE,
                                                              0x0))
      {
        DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_Interface_Enable failed!\n"));
      }

      // Disable the ping pong vsync counter and TE
      MDP_OSAL_MEMZERO(&sPingpongConfig, sizeof(HAL_MDP_PingPongConfigType));
      MDP_OSAL_MEMZERO(&sVsyncEnable,    sizeof(HAL_MDP_PingPong_VsyncEnableType));      
      MDP_OSAL_MEMZERO(&sTeEnable,       sizeof(HAL_MDP_PingPong_TEEnableType));      

      sPingpongConfig.pIntrVsyncEnable = &sVsyncEnable;
      sPingpongConfig.pIntrTEEnable    = &sTeEnable;
      sVsyncEnable.bEnable             = FALSE;
      sTeEnable.bEnable                = FALSE;
      if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_PingPong_Setup(pPanelConfig->sDisplayControlPath[uI].ePingPongId, &sPingpongConfig, 0x00))
      {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_PingPong_Setup failed!\n"));
      }
    }

     // Stall for 16ms to guarantee all transfers are complete (auto refresh can only be disabled when the HW is idle)
     gBS->Stall(16*1000);

    // Reset the control path back to an idle state
    for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
    {
      // 
      if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Reset(pPanelConfig->sDisplayControlPath[uI].eControlPathId, 0x0))
      {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Reset failed!\n"));
      }
    }
      
  }

  return eStatus;
}



/****************************************************************************
*
** FUNCTION: MDPOutputConstantColor()
*/
/*!
* \brief
*   Configure MDP to enable/disable constant color output to panel
*
*   COMMAND MODE:
*      bEnabled = TRUE  :  Set up MDP color fill and trigger one frame to panel
*      bEnabled = FALSE :  Remove MDP color fill settings
*   VIDEO MODE:
*      bEnabled = TRUE  :  Set up MDP color fill and enable interface
*      bEnabled = FALSE :  Remove MDP color fill settings
*
* \param [in] pPanelConfig       - The panel configuration
* \param [in] uConstantColor     - Constant color to output
* \param [in] bEnabled           - Flag to enable/disable constant color output
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPOutputConstantColor(MDP_Panel_AttrType *pPanelConfig, uint32 uConstantColor, bool32 bEnabled)
{
  MDP_Status    eStatus = MDP_STATUS_OK;

  if (NULL == pPanelConfig) 
  {
    eStatus = MDP_STATUS_BAD_PARAM;
  }
  else
  {
    uint32  uI;

    for (uI = 0; uI < pPanelConfig->uNumMixers; uI++)
    {
      MDP_DisplayInterfaceMapType *pInterfaceMap    = (MDP_DisplayInterfaceMapType*)MDP_GET_INTFMODEHMAP(pPanelConfig->ePhysConnect, uI);
      uint32                       uFlags           = 0;
      HAL_MDP_ControlPathFlushType sFlushType;

      if (HAL_MDP_INTERFACE_NONE == pPanelConfig->sDisplayControlPath[uI].eInterfaceId)
      {
        continue;
      }
      
      if (TRUE == bEnabled)
      {
        HAL_MDP_ControlPath_Mixer_BorderColorConfigType sBorderColorConfig;
        HAL_MDP_ControlPath_Mixer_ZOrderConfigType      sZorderConfig;
        HAL_MDP_Mixer_BorderColorConfigType             sBorderOutputColorConfig;
        HAL_MDP_LayerMixerConfigType                    sLayerMixerCfg;
        HAL_MDP_ControlPath_MixerConfigType             sMixerConfig;
        HAL_MDP_ControlPathConfigType                   sControlPathConfig;

        /*
         * This function is special for command mode to clear the garbage data in the panel display buffer.
         * Calling sequence:
         *
         * DisplayDxe_SetMode --> MDPSetMode --> MDPSetupPipe
         *                                   --> MDPPanelInit --> DSIDriver_SetMode --> send init sequence
         *
         * So, at this moment, MDP pipe has been setup already, we need to restore the changes after constant color been sent out
         *
         */
        // Setup border color
        MDP_OSAL_MEMZERO(&sBorderColorConfig, sizeof(HAL_MDP_ControlPath_Mixer_BorderColorConfigType));
        sBorderColorConfig.bBorderColorEnable = TRUE;

        // Setup the ZOrder
        MDP_OSAL_MEMZERO(&sZorderConfig, sizeof(HAL_MDP_ControlPath_Mixer_ZOrderConfigType));

        // Setup the layer mixer config for constant color
        MDP_OSAL_MEMZERO(&sBorderOutputColorConfig, sizeof(HAL_MDP_Mixer_BorderColorConfigType));
        sBorderOutputColorConfig.uColorValue = uConstantColor;

        // Setup the mixer configuration
        MDP_OSAL_MEMZERO(&sMixerConfig, sizeof(HAL_MDP_ControlPath_MixerConfigType));
        sMixerConfig.eMixerId                 = pPanelConfig->sDisplayControlPath[uI].eLayerMixerId;
        sMixerConfig.psMixerZOrderConfig      = &sZorderConfig;
        sMixerConfig.psMixerBorderColorConfig = &sBorderColorConfig;

        // Setup the layer mixer config
        MDP_OSAL_MEMZERO(&sLayerMixerCfg, sizeof(HAL_MDP_LayerMixerConfigType));
        sLayerMixerCfg.eLayerMixerId       = pPanelConfig->sDisplayControlPath[uI].eLayerMixerId;
        sLayerMixerCfg.psBorderColorConfig = &sBorderOutputColorConfig;

        // Setup the control path
        MDP_OSAL_MEMZERO(&sControlPathConfig, sizeof(HAL_MDP_ControlPathConfigType));
        sControlPathConfig.psMixerConfig      = &sMixerConfig;
        sControlPathConfig.psLayerMixerConfig = &sLayerMixerCfg;    
        // Program the control path
        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Setup(pPanelConfig->sDisplayControlPath[uI].eControlPathId, &sControlPathConfig, 0))
        {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Setup failed!\n"));
          eStatus = MDP_STATUS_FAILED;
        }

        if (HAL_MDP_INTERFACE_MODE_COMMAND == pInterfaceMap->eInterfaceMode)
        {
          // Flag to force overlay processor start for command mode
          uFlags |= HAL_MDP_FLAGS_COMMIT_FORCE_UPDATE;
        }
      }
      else
      {
        // Setup the control path based on panel config
        SetupControlPath(pPanelConfig);        
        // Don't force overlay processor start, otherwise previous constant color will be overwritten
        // Only update MDP registers.
        uFlags = 0;
      }

      // Commit the changes
      MDP_OSAL_MEMZERO(&sFlushType, sizeof(HAL_MDP_ControlPathFlushType));
      sFlushType.uFlushModules |= MDP_GET_FLUSHFLAGS(pPanelConfig->eDisplayId, 0);
      
      if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_ControlPath_Commit(pPanelConfig->sDisplayControlPath[uI].eControlPathId,
                                                                &sFlushType,
                                                                HAL_MDP_INTERFACE_TYPE_PHYSICAL_CONNECT,
                                                                uFlags))
      {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_ControlPath_Commit failed!\n"));
          eStatus = MDP_STATUS_FAILED;
      } 

      if ((TRUE == bEnabled) && 
          (HAL_MDP_INTERFACE_MODE_VIDEO == pInterfaceMap->eInterfaceMode)) 
      {
        // Enable the interface timing engine.
        if (HAL_MDSS_STATUS_SUCCESS != HAL_MDP_Interface_Enable(pInterfaceMap->eInterfaceId,
                                                                TRUE,
                                                                0x0))
        {
          DEBUG ((EFI_D_WARN, "MDPLib: HAL_MDP_Interface_Enable failed!\n"));
          eStatus = MDP_STATUS_FAILED;  
        }
      }      
    }
  }

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: MDPInitSMPPool()
*/
/*!
* \brief
*   Initialize MDP SMP pool
*
*
* \retval MDP_Status
*
****************************************************************************/
MDP_Status  MDPInitSMPPool(void)
{
    MDP_Status                    eStatus = MDP_STATUS_OK;

    /*
     * Istari has no programmable SMP, so pSMPPoolInfo will be NULL 
     */
    if (NULL != gsMDPHwPrivateInfo.pSMPPoolInfo)
    {
        uint32                        i;
        const MDP_SMPReservationType *pReservationTable = gsMDPHwPrivateInfo.pSMPPoolInfo->pReservationTable;

        // go through all SW defined blocks
        for (i = 0; i < MDP_SMP_BLOCKS; i++)
        {
            // mark blocks available based on HW SMP pool info
            if (i < gsMDPHwPrivateInfo.pSMPPoolInfo->uSMPBlocks)
            {
                gMDP_SMPAllocations[i] = HAL_MDP_SOURCE_PIPE_NONE;
            }
            else
            {
                // mark blocks unavailable based on HW SMP pool info
                gMDP_SMPAllocations[i] = HAL_MDP_SOURCE_PIPE_MAX;
            }
        }

        // check whether there're some blocks reserved by HW, and mark them unavailable for allocation
        for (i = 0; i < gsMDPHwPrivateInfo.pSMPPoolInfo->uNumReservations; i++)
        {
            gMDP_SMPAllocations[pReservationTable[i].uBlockId] = HAL_MDP_SOURCE_PIPE_MAX;
        }
    }

    return eStatus;
}



