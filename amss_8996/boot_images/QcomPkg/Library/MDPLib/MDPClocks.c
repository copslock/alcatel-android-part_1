/*=============================================================================

  File: MDPClocks.c

  Source file for MDP/Panel clock configuration


  Copyright (c) 2011-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
/*=========================================================================
      Include Files
==========================================================================*/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/EFIClock.h>
#include <Protocol/EFIChipInfo.h>
#include <Library/PcdLib.h>
#include "MDPLib_i.h"
#include "MDSSPlatform_5xx.h"
/* Temporary for enabling AXI */
#include "HALhwio.h"

/*=========================================================================
      Defines
==========================================================================*/

extern HAL_HW_VersionType  gsVersionInfo;

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: SetupClock()
**
** DESCRIPTION:
**   Setup a specific clock
**
** ---------------------------------------------------------------------- */
static MDP_Status SetupClock(EFI_CLOCK_PROTOCOL  *mClockProtocol, MDPClockEntry *pClock, UINT32 uFlags)
{
    MDP_Status eStatus = MDP_STATUS_OK;

    if ((NULL != mClockProtocol) && (NULL != pClock))
    {
        UINTN    uClockId;

        if (EFI_SUCCESS != mClockProtocol->GetClockID(mClockProtocol,  pClock->szName, &uClockId))
        {
             DEBUG ((EFI_D_WARN, "MDPLib: Clock \"%a\" not found!\n", pClock->szName));

             eStatus = MDP_STATUS_NO_RESOURCES;
        }
        else
        {
            // Is this clock an external source?
            if (NULL != pClock->pExtSrc)
            {
                // Setup clock as an external source.
                if (EFI_SUCCESS != mClockProtocol->SelectExternalSource(mClockProtocol,
                                                                        uClockId,
                                                                        0,                               // FreqHz: 0 -- No voltage control
                                                                        pClock->pExtSrc->uClkSource,
                                                                        pClock->pExtSrc->nClkDiv,
                                                                        pClock->pExtSrc->uClkPLL_M,      // M
                                                                        pClock->pExtSrc->uClkPLL_N,      // N
                                                                        pClock->pExtSrc->uClkPLL_2D))    // 2*D
                {
                    DEBUG ((EFI_D_WARN, "MDPLib : SelectExternalSource: Clock \"%a\" returned an error!\n", pClock->szName));

                    eStatus = MDP_STATUS_FAILED;
                }
            }
            else if (pClock->nFreqHz > 0)
            {
                UINT32  uClockFreq = 0;

                // Request a specific clock frequency
                if (EFI_SUCCESS != mClockProtocol->SetClockFreqHz(mClockProtocol,
                                                                  uClockId,
                                                                  pClock->nFreqHz,
                                                                  EFI_CLOCK_FREQUENCY_HZ_CLOSEST,
                                                                  (UINT32 *)&uClockFreq))
                {
                    DEBUG ((EFI_D_WARN, "MDPLib : SetClockFreqHz: Clock \"%a\" to %dHz returned an error!\n", pClock->szName, pClock->nFreqHz));

                    eStatus = MDP_STATUS_FAILED;
                }

                /* For debugging enable clock verification */
                if ((MDP_STATUS_OK == eStatus) &&
                    (EFI_D_INFO & PcdGet32(PcdDebugPrintErrorLevel)))
                {
                    UINT32 uClockFreqActual;

                    (void) mClockProtocol->CalcClockFreqHz(mClockProtocol, uClockId, (UINT32 *)&uClockFreqActual);

                    DEBUG ((EFI_D_INFO, "MDPLib: CalcClockFreqHz: Clock  \"%a\" set to %dHz (%dHz)\n",  pClock->szName, uClockFreqActual, uClockFreq));
                }
            }

            // Finally enable the clock (regardless of any errors above)
            if (EFI_SUCCESS != mClockProtocol->EnableClock(mClockProtocol, uClockId))
            {
               DEBUG ((EFI_D_WARN, "MDPLib: Clock \"%a\" cannot be enabled!\n", pClock->szName));

               eStatus = MDP_STATUS_FAILED;
            }
        }
    }
    else
    {
      eStatus = MDP_STATUS_BAD_PARAM;
    }

    return eStatus;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: StrCompare()
**
** DESCRIPTION:
**   String compare helper function
**
** ---------------------------------------------------------------------- */
static INT32 StrCompare(CHAR8 *szSrc, CHAR8 *szDest)
{
    UINT32 uCount;
    INT32 nResult = 1;

    if ((NULL != szSrc) && (NULL != szDest))
    {
        for (uCount=0;uCount<MDPLIB_MAX_CLOCK_NAME;uCount++)
        {
            if (szSrc[uCount] != szDest[uCount])
            {
                break;
            }
            else if (('\0' == szSrc[uCount]) && ('\0' == szDest[uCount]))
            {
                nResult = 0;
                break;
            }
            else if (('\0' == szSrc[uCount]) || ('\0' == szDest[uCount]))
            {
                break;
            }
        }
    }

    return nResult;
}


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: FindExtClock()
**
** DESCRIPTION:
**   Find an external clock configuration that matches the current clock
**
** ---------------------------------------------------------------------- */
static MDPExternalClockEntry *FindExtClock(CHAR8 *szClockName, MDPExternalClockEntry *pExtClockList)
{
    UINT32 uCount = 0;
    MDPExternalClockEntry *pExtClock = NULL;

    // If we have an external clock list, search for a matching clock
    if (pExtClockList)
    {
        while ((pExtClockList[uCount].szName[0] != '\0'))
        {
            if (StrCompare(szClockName, (CHAR8*)pExtClockList[uCount].szName) == 0)
            {
                pExtClock = (MDPExternalClockEntry*)&pExtClockList[uCount];
                break;
            }
            uCount++;
        }
    }

    return pExtClock;
}


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: ConfigurePowerDomain()
**
** DESCRIPTION:
**   Enabled/disable a specific power domain
**
** ---------------------------------------------------------------------- */
static MDP_Status ConfigurePowerDomain(BOOLEAN bEnable, EFI_CLOCK_PROTOCOL  *mClockProtocol, CHAR8 *szDomain)
{
  MDP_Status   eStatus = MDP_STATUS_OK;
  UINTN        uClockPowerDomainId;

  if (EFI_SUCCESS != mClockProtocol->GetClockPowerDomainID(mClockProtocol, szDomain, &uClockPowerDomainId))
  {
    DEBUG ((EFI_D_WARN, "MDPLib: GetClockPowerDomainID failed!\n"));
    eStatus = MDP_STATUS_NO_RESOURCES;
  }
  else if (TRUE == bEnable)
  {
    if (EFI_SUCCESS != mClockProtocol->EnableClockPowerDomain(mClockProtocol, uClockPowerDomainId))
    {
      DEBUG ((EFI_D_WARN, "MDPLib: EnableClockPowerDomain failed!\n"));
      eStatus = MDP_STATUS_NO_RESOURCES;
    }
  }
  else if (FALSE == bEnable)
  {
    if (EFI_SUCCESS != mClockProtocol->DisableClockPowerDomain(mClockProtocol, uClockPowerDomainId))
    {
      DEBUG ((EFI_D_WARN, "MDPLib: DisableClockPowerDomain failed!\n"));
      eStatus = MDP_STATUS_NO_RESOURCES;
    }
  }

  return eStatus;
}


static MDP_Status GetMDPVersionIndex(MDP_HwPrivateInfo  *psMDPHwPrivateInfo)
{
  MDP_Status   eStatus    = MDP_STATUS_OK;

  if (psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion == 0)
  {
    // Default, set 8974 family i.e MDP 1.2
    psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion = 0x01;
    psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion = 0x02;

     // Check if chip family is 8916 or not
    if (psMDPHwPrivateInfo->sEFIChipSetFamily ==  EFICHIPINFO_FAMILY_MSM8916 )
    {
      psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion = 0x01;
      psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion = 0x06;
    }

	// Check if chip family is 8996 or not
    if ((psMDPHwPrivateInfo->sEFIChipSetFamily ==  EFICHIPINFO_FAMILY_APQ8096 ) ||
		(psMDPHwPrivateInfo->sEFIChipSetFamily ==  EFICHIPINFO_FAMILY_MSM8996))
    {
      psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion = 0x01;
      psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion = 0x07;
    }
	
  }
  return eStatus;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: ConfigureClockSourceXO()
**
** DESCRIPTION:
**   Configure the clock source to XO
**
** ---------------------------------------------------------------------- */
static EFI_STATUS ConfigureClockSourceXO(EFI_CLOCK_PROTOCOL *mClockProtocol, UINTN uClockId)
{
    EFI_STATUS   eStatus = EFI_SUCCESS;
    UINT32       uFreq   = 0;

    // Set clock frequency to 19.2Mhz will configure the clock source to XO
    if(EFI_SUCCESS != (eStatus = mClockProtocol->SetClockFreqHz(mClockProtocol, 
                                                                uClockId, 
                                                                XO_DEFAULT_FREQ_IN_HZ, 
                                                                EFI_CLOCK_FREQUENCY_HZ_EXACT, 
                                                                &uFreq)))
    {
        DEBUG ((EFI_D_WARN, "ConfigureClockSourceXO failed to set frequency to %d. Actual is %d!\n", XO_DEFAULT_FREQ_IN_HZ, uFreq));
    }
    return eStatus;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: MDPSetupClocks()
**
** DESCRIPTION:
**   Display Clock configuration.
**
** ---------------------------------------------------------------------- */
MDP_Status MDPSetupClocks(MDPClockTypes eClockType, MDPExternalClockEntry *pExtClockList)
{
  MDP_Status           eStatus             = MDP_STATUS_OK;
  EFI_CLOCK_PROTOCOL  *mClockProtocol;

  if((EFI_PLATFORMINFO_TYPE_VIRTIO == gePlatformType) ||
     (EFI_PLATFORMINFO_TYPE_RUMI   == gePlatformType))
   {
     DEBUG ((EFI_D_WARN, "MDPLib: For VIRTIO or RUMI, ignore this error and continue\n"));
     eStatus = MDP_STATUS_OK;
   }
  // Grab the clock protocol
  else if (EFI_SUCCESS != gBS->LocateProtocol(&gEfiClockProtocolGuid,
                                         NULL,
                                         (VOID **)&mClockProtocol))
  {
    DEBUG ((EFI_D_WARN, "MDPLib: Clock protocol failed!\n"));
    eStatus = MDP_STATUS_NO_RESOURCES;
  }
  else
  {
    MDPClockEntry                  *pClockList          = NULL;
    MDP_HwPrivateInfo              *psMDPHwPrivateInfo  = &gsMDPHwPrivateInfo;
    MDP_HwMajorFamilyResourceList  *pResListMajor;
    MDP_HwMinorFamilyResourceList  *pResListMinor;
    MDP_ResourceList               *pClkResList;


    if (MDP_STATUS_OK !=  GetMDPVersionIndex(psMDPHwPrivateInfo))
    {
      DEBUG ((EFI_D_WARN, "MDPLib:GetMDPVersionIndex  failed!\n"));
    }
    else
    {
      pResListMajor = (MDP_HwMajorFamilyResourceList*)&(asHarwareFamilyMajor[psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion]);
      pResListMinor = (MDP_HwMinorFamilyResourceList*)&(pResListMajor->pMinorFamily[psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion]);
      pClkResList   = (MDP_ResourceList*)pResListMinor->pDisplayPlatfromResources->pResList;

      // if we have a valid resource list
      if (NULL != pClkResList)
      {
        // Handle pre-clock events
        switch (eClockType)
        {
          case MDP_CLOCKTYPE_CORE:
             pClockList = (MDPClockEntry*)(pClkResList->pMDPClocks);

             /* Enable the power domain for access in to the MDSS clocks */
             if(NULL != pClkResList->pPowerDomain)
             {
                UINT32                    uCount = 0;

                /* go through the power domain list to enable it in order */
                while(pClkResList->pPowerDomain[uCount].szName[0] != '\0')
                {
                    ConfigurePowerDomain(TRUE, mClockProtocol, (CHAR8*)pClkResList->pPowerDomain[uCount].szName);
                    uCount++;
                }
             }
             break;
          case MDP_CLOCKTYPE_DSI0:
            pClockList = (MDPClockEntry*)(pClkResList->pDSI0Clocks);
            break;
          case MDP_CLOCKTYPE_DSI1:
             pClockList = (MDPClockEntry*)(pClkResList->pDSI1Clocks);
             break;
          case MDP_CLOCKTYPE_HDMI:
             pClockList = (MDPClockEntry*)(pClkResList->pHDMIClocks);
             break;
          case MDP_CLOCKTYPE_EDP:
             pClockList = (MDPClockEntry*)(pClkResList->pEDPClocks);
             break;
          default:
             break;
        }
      }

      // if we have a valid clock list
      if (NULL != pClockList)
      {
        UINT32 uCount = 0;

        while ((pClockList[uCount].szName[0] != '\0'))
        {
          // Create a local copy of the clock entry
          MDPClockEntry sClockEntry = pClockList[uCount];

          // Override with external clock list (if it is not configured already)
          if (NULL == sClockEntry.pExtSrc)
          {
             sClockEntry.pExtSrc = FindExtClock((CHAR8*)&sClockEntry.szName, pExtClockList);
          }

          // Setup the clock (ignore failures)
          (void)SetupClock(mClockProtocol, &sClockEntry, 0);

          // Go to the next clock
          uCount++;
        }
      }
    }
  }

  return eStatus;
}


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: MDPDisableClocks()
**
** DESCRIPTION:
**   Display Clock configuration
**
** ---------------------------------------------------------------------- */
MDP_Status MDPDisableClocks(MDPClockTypes eClockType)
{
  MDP_Status           eStatus          = MDP_STATUS_OK;
  EFI_CLOCK_PROTOCOL  *mClockProtocol;

  // Grab the clock protocol
  if (EFI_SUCCESS != gBS->LocateProtocol(&gEfiClockProtocolGuid,
                                         NULL,
                                         (VOID **) &mClockProtocol))
  {
    DEBUG ((EFI_D_WARN, "MDPLib: Clock protocol failed!\n"));
    eStatus = MDP_STATUS_NO_RESOURCES;
  }
  else
  {
    MDPClockEntry                  *pClockList          = NULL;
    MDP_HwPrivateInfo              *psMDPHwPrivateInfo  = &gsMDPHwPrivateInfo;
    MDP_HwMajorFamilyResourceList  *pResListMajor;
    MDP_HwMinorFamilyResourceList  *pResListMinor;
    MDP_ResourceList               *pClkResList;


    if (MDP_STATUS_OK !=  GetMDPVersionIndex(psMDPHwPrivateInfo))
    {
      DEBUG ((EFI_D_WARN, "MDPLib: GetMDPVersionIndex  failed!\n"));
    }
    else
    {
      pResListMajor = (MDP_HwMajorFamilyResourceList*)&(asHarwareFamilyMajor[psMDPHwPrivateInfo->sMDPVersionInfo.uMajorVersion]);
      pResListMinor = (MDP_HwMinorFamilyResourceList*)&(pResListMajor->pMinorFamily[psMDPHwPrivateInfo->sMDPVersionInfo.uMinorVersion]);
      pClkResList   = (MDP_ResourceList*)pResListMinor->pDisplayPlatfromResources->pResList;

      if(NULL != pClkResList)
      {
        switch (eClockType)
        {
          case MDP_CLOCKTYPE_CORE:
            /* MDP Core Clocks */
            pClockList = (MDPClockEntry*)(pClkResList->pMDPClocks);

             /* Enable the power domain for access in to the MDSS clocks */
             if(NULL != pClkResList->pPowerDomain)
             {
                INT32          iCount             = 0;
                INT32          iTotalPowerDomains = 0;

                /* Get total number of power domains on the list */
                while(pClkResList->pPowerDomain[iCount++].szName[0] != '\0')
                {
                    iTotalPowerDomains ++;
                }

                /* Turn off power domains in the reserve order */
                for(iCount = (iTotalPowerDomains-1); iCount >= 0; iCount--)
                {
                    ConfigurePowerDomain(FALSE, mClockProtocol, (CHAR8*)pClkResList->pPowerDomain[iCount].szName);
                }
             }
            
            break;
          case MDP_CLOCKTYPE_DSI0:
            /* MDP Core Clocks */
            pClockList = (MDPClockEntry*)(pClkResList->pDSI0Clocks);
            break;
          case MDP_CLOCKTYPE_DSI1:
            /* MDP Core Clocks */
            pClockList = (MDPClockEntry*)(pClkResList->pDSI1Clocks);
            break;
          case MDP_CLOCKTYPE_HDMI:
            /* MDP Core Clocks */
            pClockList = (MDPClockEntry*)((pClkResList->pHDMIClocks));
            break;
          default:
            break;
        }
      }

      // if we have a valid clock list
      if (NULL != pClockList)
      {
        UINT32 uCount = 0;

        while ((pClockList[uCount].szName[0] != '\0'))
        {
          UINTN    uClockId;

          if (EFI_SUCCESS != mClockProtocol->GetClockID(mClockProtocol,  pClockList[uCount].szName, &uClockId))
          {
             DEBUG ((EFI_D_WARN, "MDPLib: Clock \"%a\" not found!\n", pClockList[uCount].szName));
          }
          else if (((MDP_CLOCKTYPE_DSI0 == eClockType) ||
                    (MDP_CLOCKTYPE_DSI1 == eClockType)) &&
                   (EFI_SUCCESS != ConfigureClockSourceXO(mClockProtocol, uClockId)))
          {
             DEBUG ((EFI_D_WARN, "MDPLib: Clock \"%a\" cannot set source to XO!\n", pClockList[uCount].szName));
          }
          else if (EFI_SUCCESS != mClockProtocol->DisableClock(mClockProtocol, uClockId))
          {
             DEBUG ((EFI_D_WARN, "MDPLib: Clock \"%a\" cannot be disabled!\n", pClockList[uCount].szName));
          }

          uCount++;
        }
      }
    }
  }

  return eStatus;
}

