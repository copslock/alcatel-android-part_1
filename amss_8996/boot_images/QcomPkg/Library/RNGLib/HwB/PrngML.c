/**
@file PrngML.c 
@brief PRNG Engine source file 
*/

/*===========================================================================

                     P R N G E n g i n e D r i v e r

DESCRIPTION
  This file contains declarations and definitions for the
  interface between application and PRNG engine driver

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when         who     what, where, why
--------     ---     ----------------------------------------------------------
05/13/14     sk      Modified PrngML getdata name
03/16/14     sk      New crypto code and 64 compatibility
08/21/12     shl     Made UEFI change
09/12/11     nk      Added prng de init functions     
07/25/10     yk      Initial version
============================================================================*/
#include <com_dtypes.h>
#include <PrngCL.h>
#include <PrngML.h>

//Declared in PrngML.h in QcomPkg\include
/*
typedef enum
{
    PRNGML_ERROR_NONE,
    PRNGML_ERROR_BUSY,
    PRNGML_ERROR_FAILED,
    PRNGML_ERROR_INVALID_PARAM,
    PRNGML_ERROR_UNSUPPORTED,
    PRNGML_ERROR_BAD_STATE
} PrngML_Result_Type;
*/

/**
 * @brief This function returns random number and can be used when Mutex,
 *        DAL framework are not yet initialised.
 *
 * @param random_ptr [in/out]Random number pointer
 * @param random_len [in] Length of random number 
 *
 * @return PrngML_Result_Type
 */
PrngML_Result_Type PrngML_getdata_lite
(
  uint8*  random_ptr,
  uint16  random_len
)
{
  PrngML_Result_Type ret_val = PRNGML_ERROR_NONE;
  
  /* Input Sanity check */
  if(!random_ptr || !random_len)
  {
    return PRNGML_ERROR_INVALID_PARAM; 
  }

  ret_val = (PrngML_Result_Type) PrngCL_lite_init();
  if (ret_val != PRNGML_ERROR_NONE) return ret_val;

  /* Get random data */
  ret_val = (PrngML_Result_Type)PrngCL_getdata(random_ptr, random_len);
  
  ret_val = (PrngML_Result_Type) PrngCL_lite_deinit();
  if (ret_val != PRNGML_ERROR_NONE) return ret_val;
  
  return ret_val;
} /* PrngML_getdata_lite() */
