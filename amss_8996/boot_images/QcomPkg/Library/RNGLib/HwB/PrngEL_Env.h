#ifndef __PRNGEL_ENV_H__
#define __PRNGEL_ENV_H__

/*===========================================================================

                       P R N G E n g i n e D r i v e r

                       H e a d e r  F i l e (e x t e r n a l)

DESCRIPTION
  This header file contains HW Crypto specific declarations.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/16/14   sk      New crypto code and 64 compatibility
01/28/13   shl     Warning fix 
08/21/12   shl     Made UEFI change
08/09/11   nk      Added the Mutex Enter and Exit routines
07/25/10   yk      Initial version
============================================================================*/

#include <Include/com_dtypes.h>
#include "PrngCL_DALIntf.h"

#define PRNGEL_MUTEX_ENTER()

#define PRNGEL_MUTEX_EXIT()

#define PRNGCL_ENABLE_CLK() PrngCL_DAL_Clock_Enable()

#define PRNGCL_DISABLE_CLK() PrngCL_DAL_Clock_Disable()

#define PRNGCL_STATUS_CLK( PTRCLKFLAG ) PrngCL_DAL_Clock_Status( PTRCLKFLAG )

#endif /*__PRNGEL_ENV_H__ */
