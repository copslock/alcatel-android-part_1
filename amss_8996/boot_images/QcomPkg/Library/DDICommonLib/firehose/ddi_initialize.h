/*==================================================================
 *
 * FILE:        ddi_initialize.h
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2015 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2015-06-24   sng      Initial checkin
 */

#ifndef DDI_INITIALIZE_H
#define DDI_INITIALIZE_H

#include BOOT_PBL_H
#include "boot_sbl_if.h"

//extern boot_pbl_shared_data_type *pbl_shared_global;

void ddi_entry(boot_pbl_shared_data_type *pbl_shared) __attribute__((noreturn));
void ddi_init_hw();

#endif

