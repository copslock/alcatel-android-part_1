/*===========================================================================

                    BOOT_DDI_TOOL

DESCRIPTION
  This file contains the code for Boot DDR Debug Image tool.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/09/16   jh      Flush cache before warm reboot
05/06/15   sng     Initial Release
=============================================================================*/


#include "ddi_tool.h"
#include "../firehose/ddi_firehose.h"
#include "../firehose/ddi_initialize.h"
#include "../../../XBLLoader/boot_visual_indication.h"
#include "../../../XBLLoader/boot_extern_ddr_interface.h"
#include "../../../XBLLoader/boot_cache_mmu.h"
#include "../../../Msm8996Pkg/Library/XBLLoaderLib/boot_target.h"
#include "../../../Include/api/systemdrivers/pmic/pm_rgb.h"
#include "../../../XBLLoader/boothw_target.h"
#include "../../../Msm8996Pkg/Library/DSFTargetLib/ddrss/header/ddrss.h"
#include "../../../Msm8996Pkg/Library/DSFTargetLib/ddrss/header/ddrss_training.h"
#include "../../../Msm8996Pkg/Library/DSFTargetLib/ddrss/bimc/mc230/header/bimc.h"
#include "../../../Include/api/systemdrivers/ClockBoot.h"




extern uint32 dataBuffer[256];
extern uint16 patternElemCnt;
static char str[128];
DDR_STRUCT *share_data = (DDR_STRUCT *)DDR_GLOBAL_STRUCT_DATARAM_ADDR;
uint8 gSupportedClockLevel ;
uint32 bimc_clk_plan[MAX_CLOCK_FREQ];

typedef struct
{
    int   mr3Val;
    int   mr11Val;
    uint32  freq_switch_range_in_kHz;
}DDI_MR_Struct_t;

DDI_MR_Struct_t MRParams[NUM_ECDT_FREQ_SWITCH_STRUCTS];



boolean ddi_writeMRReg(uint8 ch, uint8 cs, uint8 reg, uint8 regVal)
{
    ch++;
    cs++;
    BIMC_MR_Write(ch,cs,reg,regVal);
    return TRUE;
}


void ddi_readMRReg(uint8 ch, uint8 cs, uint8 reg)
{
    uint32 mrVal = 0;
    mrVal = BIMC_MR_Read(ch, cs, reg);
    sendlogWithInfo("MR REG Value:", mrVal);
    sendPassResponse("responseReadMRReg");

}


boolean ddi_ChangeBIMCClock(int Ix)
{
     if(Clock_SetBIMCSpeed(bimc_clk_plan[Ix]) == TRUE)
         return TRUE;

     return FALSE;

}



void deInitTransport()
{
    shutDownUSB();
}


void update_soc_drive_strength(void)
  {
    uint32 prfs_level =0;

    if( share_data->ddi_buf[2] != 0)
        {

         // Enable broadcast mode for all DQ PHYs on both channels
         HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
                   AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x1E3C);

          for (prfs_level =0; prfs_level < MAX_PRFS_LEVELS ; prfs_level++)
          {
            out_dword(ddr_phy_dq_config_tool[prfs_level] + BROADCAST_BASE, share_data->ddi_buf[prfs_level+2]);
            out_dword(ddr_phy_dq_config_tool[prfs_level+MAX_PRFS_LEVELS] + BROADCAST_BASE, share_data->ddi_buf[prfs_level+10]);

          }
        }
    //while(!temp){}
    if ( share_data->ddi_buf[18] != 0)
      {
         // Enable broadcast mode for all CA PHYs on both channels
        HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
               AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0183);

        for (prfs_level =0; prfs_level < MAX_PRFS_LEVELS ; prfs_level++)
        {
            out_dword(ddr_phy_ca_config_tool[prfs_level] + BROADCAST_BASE, share_data->ddi_buf[prfs_level+18]);
            out_dword(ddr_phy_ca_config_tool[prfs_level+MAX_PRFS_LEVELS] + BROADCAST_BASE, share_data->ddi_buf[prfs_level+26]);
        }
    }

    if (share_data->ddi_buf[2] != 0 || share_data->ddi_buf[18] != 0 )
        {
        // Disable broadcast mode
        HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),
             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);
        }


  }






void ApplyMR(uint8 JEDEC_NUM, uint8 Value)
{

    BIMC_MR_Write(1, 1, JEDEC_NUM, Value );
    BIMC_MR_Write(1, 2, JEDEC_NUM, Value );
    BIMC_MR_Write(2, 1, JEDEC_NUM, Value );
    BIMC_MR_Write(2, 2, JEDEC_NUM, Value );
}

void UpdateFSP(uint8 ix)
{
    uint8 temp = 0;
    temp = (ix << 7) | (ix << 6) | (0 << 5) | (0 << 3) | (0 << 2) | (0 << 0);

    ApplyMR(JEDEC_MR_13, temp);

}



void UpdateMR(uint8 ix)
{
    uint8 temp = 0;
    uint32 MASK = 0xFF << (ix*8);



    if (ix<=3)
    {

        if(((share_data->ddi_buf[34]) & (1<<ix)) != 0)
        {


            // Get MR3 value for this freq..
            temp = ((share_data->ddi_buf[36] & MASK)>>(ix*8));
            if(temp != 0)
            {
                ApplyMR(JEDEC_MR_3, temp);


            }


            // Get MR11 value for this freq..
            temp = ((share_data->ddi_buf[38] & MASK)>>(ix*8));
            if(temp != 0)
            {
                ApplyMR(JEDEC_MR_11, temp);


            }





                // Get MR22 value for this freq..for cs0
            temp = ((share_data->ddi_buf[40] & MASK)>>(ix*8));
            if(temp != 0)
            {
                ApplyMR(JEDEC_MR_22, temp);


            }





//              // Get MR22 value for this freq..for cs1
//          temp = ((share_data->ddi_buf[42] & MASK)>>(ix*8));
//          if(temp != 0)
//          {
//
//              ApplyMR(JEDEC_MR_22, temp);
//
//
//
//          }






        }

    }
    else
    {


        if(((share_data->ddi_buf[34]) & (1<<ix)) != 0)
        {
            //ddr_printf (DDR_NORMAL, "Updating MR Registers!!!!!");

            // Get MR3 value for this freq..
            temp = ((share_data->ddi_buf[37] & MASK)>>((ix-4)*8));
            if(temp != 0)
            {
                ApplyMR(JEDEC_MR_3, temp);


            }


            // Get MR11 value for this freq..
            temp = ((share_data->ddi_buf[39] & MASK)>>((ix-4)*8));
            if(temp != 0)
            {
                ApplyMR(JEDEC_MR_11, temp);


            }




                    // Get MR22 value for this freq..for cs0
                temp = ((share_data->ddi_buf[41] & MASK)>>((ix-4)*8));
                if(temp != 0)
                {
                    ApplyMR(JEDEC_MR_22, temp);


                }




//
//                  // Get MR22 value for this freq..for cs1
//              temp = ((share_data->ddi_buf[43] & MASK)>>((ix-4)*8));
//              if(temp != 0)
//              {
//
//                  ApplyMR(JEDEC_MR_22, temp);
//
//              }






        }


    }






}

/*

  training_freq_table_=_(
    0x000BB800,
    0x000F8700,
    0x0013C680,
    0x0017BB00,
    0x001B8A00)

  training_freq_table = (
    768000,
    1017600,
    1296000,
    1555200,
    1804800)




*/

void update_ddr_clock_table()
{
    int i=0;
    gSupportedClockLevel = share_data->misc.ddr_num_clock_levels;
    if(gSupportedClockLevel <= MAX_CLOCK_FREQ)
    {
        for(i = 0; i < gSupportedClockLevel; i++)
        {
            bimc_clk_plan[i] =  share_data->misc.clock_plan[i].clk_freq_in_khz;
        }


    }
}



void initMRStruct()
{
    uint8 level = sizeof(MRParams)/sizeof(MRParams[0]);

    for (int i  = 0; i < level; i++)
    {
        MRParams[i].mr11Val = -1;
        MRParams[i].mr3Val = -1;
        MRParams[i].freq_switch_range_in_kHz = share_data->extended_cdt_runtime.bimc_freq_switch[i].freq_switch_range_in_kHz;
    }


}




void changeDRAMSettings(uint32 clk_in_khz)
{
    uint8 new_clk_idx = 0;
    uint8 new_params_idx =0;



   for (new_clk_idx = 0; new_clk_idx < share_data->misc.ddr_num_clock_levels; new_clk_idx++)
   {
      if (clk_in_khz <= share_data->misc.clock_plan[new_clk_idx].clk_freq_in_khz)
         break;
   }

   new_params_idx = BIMC_Freq_Switch_Params_Index (share_data, clk_in_khz);


   if (MRParams[new_params_idx].mr3Val != -1 && MRParams[new_params_idx].mr3Val != 0xFF)
   {
       share_data->extended_cdt_runtime.bimc_freq_switch[new_params_idx].MR3 = (uint8)MRParams[new_params_idx].mr3Val;
   }
   if (MRParams[new_params_idx].mr11Val != -1 && MRParams[new_params_idx].mr11Val != 0xFF)
   {
       share_data->extended_cdt_runtime.bimc_freq_switch[new_params_idx].MR11 = (uint8)MRParams[new_params_idx].mr3Val;

   }



}


boolean ddi_ChangeMR11(int freq, int mr11Val )
{
    uint8 i = 0;
    uint8 level = sizeof(MRParams)/sizeof(MRParams[0]);

    for ( i  = 0; i < level; i++)
    {
        if (bimc_clk_plan[freq] <= MRParams[i].freq_switch_range_in_kHz)
        {
            MRParams[i].mr11Val = mr11Val;
            return TRUE;
        }
    }
    return FALSE;
}

//boolean ddi_ChangeMR22(int freq, int CA_ODTD, int CS_ODTE,int CK_ODTD,int CODT )
//{
//
//
//    return FALSE;
//
//}



boolean ddi_ChangeMR3(int freq, int mr3Val )
{
    uint8 i = 0;
    uint8 level = sizeof(MRParams)/sizeof(MRParams[0]);

    for (i  = 0; i < level; i++)
    {
        if (bimc_clk_plan[freq] <= MRParams[i].freq_switch_range_in_kHz)
        {
            MRParams[i].mr3Val = mr3Val;
            return TRUE;
        }
    }
    return FALSE;
}




void ddi_mem_copy(uint32 * source, uint32 * destination, uint32 size, uint32 burst)
{
    uint32 i;

    if(burst)
    {
        /* perform a burst write */
        #ifndef CDVI_SVTB
        __blocksCopy(source, destination, size);
        #endif
    }
    else
    {
        for(i = 0; i < size; i++)
        {
            destination[i] = source[i];
        }
    }

}







boolean ddi_ReadTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity)
{
    uint32 dataBufferSize = 0;
    uint32* base_addr = NULL;
    uint32 cnt = 0;
    boot_log_message("ddi_ReadTest, Start");

    getBaseSize(ch, cs);
    base_addr = (uint32 *)gBase;


    snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
    boot_log_message(str);

    snprintf(str, 128, "param:%d,%d,%d,%d \n", ch, cs, loopcnt, infitinity) ;
    boot_log_message(str);

    dataBufferSize = sizeof(dataBuffer)/sizeof(uint32);

    ddi_mem_copy(dataBuffer, base_addr,dataBufferSize, 1);


    do
    {
        sayHello("responseReadTest");
        while (cnt < loopcnt)
        {

            ddi_mem_copy (base_addr, read_back_pattern, dataBufferSize, 1);
            cnt++;
        }
         boot_log_message("ddi_ReadTest, loop complete");

        cnt = 0;
    }while(infitinity);

     boot_log_message("ddi_ReadTest, END");

     return 1;


}

boolean ddi_ReadWriteTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity)
{
    uint32 dataBufferSize = 0;
    uint32* base_addr = NULL;
    uint32 cnt = 0;
    boot_log_message("ddi_ReadWriteTest, Start");

    getBaseSize(ch, cs);
    base_addr = (uint32*)gBase;


    snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
    boot_log_message(str);

    snprintf(str, 128, "param:%d,%d,%d,%d \n", ch, cs, loopcnt, infitinity) ;
    boot_log_message(str);

    dataBufferSize = sizeof(dataBuffer)/sizeof(uint32);


    do
    {
        sayHello("responseReadWriteTest");

        while (cnt < loopcnt)
        {
            /* convert base address and limit for accessing memory by word */
            base_addr = (uint32 *)gBase;
            ddi_mem_copy(dataBuffer, base_addr,dataBufferSize, 1);
            ddi_mem_copy (base_addr, read_back_pattern, dataBufferSize, 1);
            cnt++;
        }

        boot_log_message("ddi_ReadWriteTest, loop complete");
        cnt = 0;
    }while (infitinity);


     boot_log_message("ddi_ReadWriteTest, END");

     return 1;


}








boolean ddi_WriteTest(uint8 ch, uint8 cs, uint32 loopcnt, boolean infitinity)
{
    uint32 dataBufferSize = 0;
    uint32* base_addr = NULL;
    uint32 cnt = 0;
    boot_log_message("ddi_WriteTest, Start");

    getBaseSize(ch, cs);
    base_addr = (uint32 *)gBase;


    snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
    boot_log_message(str);

    snprintf(str, 128, "param:%d,%d,%d,%d \n", ch, cs, loopcnt, infitinity) ;
    boot_log_message(str);

    dataBufferSize = sizeof(dataBuffer)/sizeof(uint32);


    do
    {
        sayHello("responseWriteTest");
        while (cnt < loopcnt)
        {
            ddi_mem_copy(dataBuffer, base_addr,dataBufferSize, 1);
            cnt++;
        }
         boot_log_message("ddi_WriteTest, loop complete");

        cnt = 0;
    }while(infitinity);

     boot_log_message("ddi_WriteTest, END");

     return 1;


}

ddr_info ddi_DDRInfo;
ddr_size_info ddi_sizeInfo;

void getBaseSize(int ch, int cs)
{
     ddi_DDRInfo = ddr_get_info();
     ddi_sizeInfo = ddr_get_size();

     #if 1
     snprintf(str, 50, "CH0 CS0 Base : 0x%X \n", ddi_sizeInfo.sdram0_cs0_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH0 CS1 Base : 0x%X \n", ddi_sizeInfo.sdram0_cs1_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS0 Base : 0x%X \n", ddi_sizeInfo.sdram1_cs0_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS1 Base : 0x%X \n", ddi_sizeInfo.sdram1_cs1_addr) ;
     boot_log_message(str);


     snprintf(str, 50, "CH0 CS0 Size : 0x%X \n", ddi_sizeInfo.sdram0_cs0) ;
     boot_log_message(str);
     snprintf(str, 50, "CH0 CS1 Size : 0x%X \n", ddi_sizeInfo.sdram0_cs1) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS0 Size : 0x%X \n", ddi_sizeInfo.sdram1_cs0) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS1 Size : 0x%X \n", ddi_sizeInfo.sdram1_cs1) ;
     boot_log_message(str);

     snprintf(str, 50, "CH0 CS0 Base : 0x%X \n", ddi_DDRInfo.ddr_size.sdram0_cs0_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH0 CS1 Base : 0x%X \n", ddi_DDRInfo.ddr_size.sdram0_cs1_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS0 Base : 0x%X \n", ddi_DDRInfo.ddr_size.sdram1_cs0_addr) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS1 Base : 0x%X \n", ddi_DDRInfo.ddr_size.sdram1_cs1_addr) ;
     boot_log_message(str);


     snprintf(str, 50, "CH0 CS0 Size : 0x%X \n", ddi_DDRInfo.ddr_size.sdram0_cs0) ;
     boot_log_message(str);
     snprintf(str, 50, "CH0 CS1 Size : 0x%X \n", ddi_DDRInfo.ddr_size.sdram0_cs1) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS0 Size : 0x%X \n", ddi_DDRInfo.ddr_size.sdram1_cs0) ;
     boot_log_message(str);
     snprintf(str, 50, "CH1 CS1 Size : 0x%X \n", ddi_DDRInfo.ddr_size.sdram1_cs1) ;
     boot_log_message(str);

     #endif

     switch (ch)
     {
     case 0:
         switch (cs)
         {
         case 0:
             gBase = ddi_sizeInfo.sdram0_cs0_addr+DDR_TEST_OFFSET;
             gSize = (uint32)ddi_DDRInfo.ddr_size.sdram0_cs0;
             gSize = gSize << 20;
             gSize = gSize - DDR_TEST_OFFSET;
             boot_log_message("CH0 CS0");

             break;

         case 1:
             gBase = ddi_sizeInfo.sdram0_cs1_addr;
             gSize = (uint32)ddi_DDRInfo.ddr_size.sdram0_cs1;
             gSize = gSize << 20;
             gSize = gSize - DDR_TEST_OFFSET;
             boot_log_message("CH0 CS1");

             break;

            default:
                break;
         }


            break;

     case 1:
         switch (cs)
         {
         case 0:
             gSize = (uint32)ddi_DDRInfo.ddr_size.sdram1_cs0;
             gBase = ddi_sizeInfo.sdram1_cs0_addr + gSize+DDR_TEST_OFFSET;
             gSize = gSize << 20;
             gSize = gSize - DDR_TEST_OFFSET;
             boot_log_message("CH1 CS0");

                break;

         case 1:
             gSize = (uint32) ddi_DDRInfo.ddr_size.sdram1_cs1;
             gBase= ddi_sizeInfo.sdram1_cs1_addr + gSize;
             gSize = gSize << 20;
             gSize = gSize - DDR_TEST_OFFSET;
             boot_log_message("CH1 CS1");

                break;

            default:
                break;
         }

            break;

        default:
            break;
     }


}

void sayHello(char* tag)
{
  //if (getXMLPacket() == FIREHOSE_SUCCESS)
  {
      sendRunningResponse(tag);
  }
}

void InitDDITransport()
{
    deviceprogrammer_zi_buffers ();
    initFirehoseProtocol(); // initializes default values for fh structure
    ddi_init_hw();
    initMRStruct();
}








#define ALIGNX (sizeof(size_t))
#define ONES ((size_t)-1/UCHAR_MAX)
#define HIGHS (ONES * (UCHAR_MAX/2+1))
#define HASZERO(x) ((x)-ONES & ~(x) & HIGHS)

static char *__stpcpy(char *restrict d, const char *restrict s)
{
    size_t *wd;
    const size_t *ws;

    if ((uintptr_t)s % ALIGNX == (uintptr_t)d % ALIGNX) {
        for (; (uintptr_t)s % ALIGNX; s++, d++)
            if (!(*d=*s)) return d;
        wd=(void *)d; ws=(const void *)s;
        for (; !HASZERO(*ws); *wd++ = *ws++);
        d=(void *)wd; s=(const void *)ws;
    }
    for (; (*d=*s); s++, d++);

    return d;
}

char *ddi_strcpy(char *restrict dest, const char *restrict src)
{
#if 1
    __stpcpy(dest, src);
    return dest;
#else
    const unsigned char *s = src;
    unsigned char *d = dest;
    while ((*d++ = *s++));
    return dest;
#endif
}

char *ddi_strcat(char *restrict dest, const char *restrict src)
{
    ddi_strcpy(dest + strlen(dest), src);
    return dest;
}



void temperature_read_helper(){
    TsensResultType tsensResult;
    int32 nDegC;
    uint32 tsens_index=0;
    uint32 numSensors = 0;
    if(Tsens_GetNumSensors(&numSensors) == TSENS_SUCCESS)
    {
        snprintf(str,128,"Num of sensors found on system: %d\r\n",numSensors);
        sendlog(str);
        while(tsens_index<numSensors){
            tsensResult = Tsens_GetTemp(tsens_index,&nDegC);
            if (tsensResult == TSENS_SUCCESS){
                snprintf(str,128,"TSENS %d value %d deci degC\r\n",tsens_index,nDegC);
                sendlog(str);
                }
            else
                sendlog("Temperature Sensor read failure");
            tsens_index=tsens_index+1;
        }
    }
    else
        sendlog("Could not get Number of sensors on system\r\n");

}

void ddi_readPowerSetting()
{
    uint32 VDDQ = 0;
    uint32 VDD1 = 0;

    pm_smps_volt_level_status(0, PM_SMPS_4, &VDD1);
    if (VDD1 != 0)
    {
        VDD1 = VDD1/1000;
        sendlogWithInfo("VDD1 Value(mV)", VDD1);

    }
        pm_smps_volt_level_status(0, PM_SMPS_12, &VDDQ);
    if (VDDQ != 0)
    {
        VDDQ = VDDQ/1000;
        sendlogWithInfo("VDDQ Value(mV)", VDDQ);

    }

    sendPassResponse("responseReadPowerSetting");



    return;

}
void ddi_readTemperature()
{
    temperature_read_helper();
    sendPassResponse("responseReadTemperature");
    return;

}
boolean ddi_ChangeVDDQ(int vddVal)
{
    uint16 retErrFlag = 0;
    uint32 mx=vddVal*1000;

    retErrFlag = pm_smps_volt_level(0, PM_SMPS_12, mx);
    if(retErrFlag !=0)
    {

        sendlog("Failed to set VDDQ!");
        return FALSE;
    }

    pm_smps_volt_level_status(0, PM_SMPS_12, &mx);
    snprintf(str,128,"PX1/VDD2/VDDQ set to:%d\r\n",mx);
    sendlog(str);
    return TRUE;
}


boolean ddi_ChangeVDD1(int vddVal)
{
    uint16 retErrFlag = 0;
    uint32 px3=vddVal*1000;

    retErrFlag = pm_smps_volt_level(0, PM_SMPS_4, px3);
    if(retErrFlag !=0)
    {

        sendlog("Failed to set VDD1!");
        return FALSE;
    }

    pm_smps_volt_level_status(0, PM_SMPS_4, &px3);
    snprintf(str,128,"PX3/VDD1 set to:%d(mV)\r\n",px3);
    sendlog(str);
    return TRUE;
}



void resetDDI()
{
    boot_hw_reset(BOOT_WARM_RESET_TYPE);

}

boolean disableDBI()
{
    out_dword(BIMC_S_DDR0_DPE_CONFIG_9, DPE_CONFIG_9_VAL_DISABLE);
    out_dword(BIMC_S_DDR1_DPE_CONFIG_9, DPE_CONFIG_9_VAL_DISABLE);
    out_dword(BIMC_S_DDR0_DPE_CONFIG_4, DPE_CONFIG_4_VAL_DISABLE);
    out_dword(BIMC_S_DDR1_DPE_CONFIG_4, DPE_CONFIG_4_VAL_DISABLE);

    return 1;


}


void sendTrainingRsp()
{
    sendPassResponse("responseRetrain");

}


void print_training_data(void *buffer, ddr_training_args* targs, training_params_t *training_params_ptr)
{
    training_data *training_data_ptr;

    training_data_ptr = (training_data *)(&share_data->flash_params.training_data);
    uint8 ch = targs->currentChannel;
    uint8 cs = targs->currentChipSelect;
    uint8 ddr_freq_index = targs->currentFreqIndex;
    uint8 training_type= targs->trainingType;
    uint32 FreqInKhz = targs->FreqInKhz;
    uint8 best_cdc,best_vref;

    ddrss_rdwr_dqdqs_local_vars *local_vars_DQ;
    ddrss_ca_vref_local_vars *local_vars_CACK;
    uint8 MaxNumPhy = 0;
    int coarse_vref;
    int coarse_cdc ;
    uint8 num_phy;


    uint8 ix = 0;
    uint8 iy = 0;


    if(training_type == DDR_TRAINING_CACK)
    {
        local_vars_CACK = (ddrss_ca_vref_local_vars *)buffer;
        MaxNumPhy = NUM_CA_PCH;

    }
    else
    {
        local_vars_DQ = (ddrss_rdwr_dqdqs_local_vars *)buffer;
        MaxNumPhy = NUM_DQ_PCH;
    }

    if(share_data->ddi_buf[0] == 0xDD3DEB06 && share_data->ddi_buf[1] == 0xB5B5B5B5)
    {

        for(num_phy =0;num_phy < MaxNumPhy  ;num_phy ++)
        {
            sendlog("FRAME_START");
            memset(xAxisTitle,0, 50);
            memset(yAxisTitle,0, 100);
            sendTrainingArgs(targs);
            sendlogWithInfo("paramByteNum", num_phy);




            if (training_type == DDR_TRAINING_CACK)
            {
                ddi_strcpy((char*)ddiTrainingBuffer,"bandcoarsepasscounttable:" );
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"{");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            }
            else
            {
                ddi_strcat((char*)ddiTrainingBuffer,"bandcoarsepassband:" );
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"{");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            }

            if (training_type == DDR_TRAINING_READ)
            {
                iy = 0;

                for(coarse_vref = 0; coarse_vref < training_params_ptr->rd_dqdqs.coarse_vref_max_value + 1; coarse_vref++)
                {
                    ix = 0;
                    yAxisTitle[iy++] = coarse_vref;
                    for(coarse_cdc = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; coarse_cdc < training_params_ptr->rd_dqdqs.coarse_cdc_max_value + 1; coarse_cdc++)
                    {
                        xAxisTitle[ix++] = coarse_cdc;
                        snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", local_vars_DQ->coarse_schmoo.coarse_dq_passband_info[num_phy][coarse_vref][coarse_cdc]);
                        ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                    }

                    snprintf((char*)srcBuffer,sizeof(srcBuffer),"LF");
                    ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                  }
            }

            if (training_type == DDR_TRAINING_CACK)
            {

                iy = 0;
                for( coarse_vref = 0; coarse_vref <training_params_ptr->ca_vref.coarse_vref_max_value+1  ;coarse_vref ++)
                {
                    ix = 0;
                    yAxisTitle[iy++] = coarse_vref;
                    for(coarse_cdc =training_params_ptr->ca_vref.coarse_cdc_start_value; coarse_cdc < training_params_ptr->ca_vref.coarse_cdc_max_value+1 ;coarse_cdc ++)
                    {
                        xAxisTitle[ix++] = coarse_cdc;
                        snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", local_vars_CACK->coarse_fail_count_table[num_phy][coarse_vref][coarse_cdc]);
                        ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                    }

                    snprintf((char*)srcBuffer,sizeof(srcBuffer),"LF");
                    ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                  }
            }
            if (training_type == DDR_TRAINING_WRITE)
            {
                iy = 0;

                for( coarse_vref = 0; coarse_vref < training_params_ptr->wr_dqdqs.coarse_vref_max_value + 1; coarse_vref++)
                {
                    yAxisTitle[iy++] = coarse_vref;
                    ix = 0;

                    for(coarse_cdc =training_params_ptr->wr_dqdqs.coarse_cdc_start_value; coarse_cdc < training_params_ptr->wr_dqdqs.coarse_cdc_max_value+1 ;coarse_cdc ++)
                    {
                        xAxisTitle[ix++] = coarse_cdc;
                        snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", local_vars_DQ->coarse_schmoo.coarse_dq_passband_info[num_phy][coarse_vref][coarse_cdc]);
                        ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                    }

                    snprintf((char*)srcBuffer,sizeof(srcBuffer),"LF");
                    ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                }
            }

            snprintf((char*)srcBuffer,sizeof(srcBuffer),"}");
            ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            sendTrainingData(ddiTrainingBuffer);
            memset(ddiTrainingBuffer,0, 3072);

            // Send X- axis and Y-axis data..
            ddi_strcpy((char*)ddiTrainingBuffer,"xAxis_title_CDC:{" );
            for (int i = 0; i < ix ; i++)
            {
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", xAxisTitle[i]);
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            }
            snprintf((char*)srcBuffer,sizeof(srcBuffer),"},");
            ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            sendTrainingData(ddiTrainingBuffer);
            memset(ddiTrainingBuffer,0, 3072);
            memset(xAxisTitle,0, 50);

            ddi_strcpy((char*)ddiTrainingBuffer,"yAxis_title_VREF:{" );
            for (int i = 0; i < iy ; i++)
            {
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", yAxisTitle[i]);
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            }
            snprintf((char*)srcBuffer,sizeof(srcBuffer),"},");
            ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
            sendTrainingData(ddiTrainingBuffer);
            memset(ddiTrainingBuffer,0, 3072);
            memset(yAxisTitle,0, 100);

            if(training_type == DDR_TRAINING_READ)
            {
                best_cdc=training_data_ptr->results.rd_dqdqs.coarse_cdc [ddr_freq_index][ch][cs][num_phy];
                if(FreqInKhz < 868000)
                {

                    best_vref = training_data_ptr->results.rd_dqdqs.coarse_vref[0][ch][cs][num_phy];
                }
                else
                {

                    best_vref = training_data_ptr->results.rd_dqdqs.coarse_vref[1][ch][cs][num_phy];
                }
                sendlogWithInfo("resultbestcdc", best_cdc);
                sendlogWithInfo("resultbestvref", best_vref);
            }
            else if (training_type == DDR_TRAINING_WRITE)
            {
                best_cdc=training_data_ptr->results.wr_dqdqs.coarse_cdc [ddr_freq_index][ch][cs][num_phy];
                best_vref = training_data_ptr->results.wr_dqdqs.coarse_vref[ddr_freq_index][ch][cs][num_phy];

                sendlogWithInfo("resultbestcdc", best_cdc);
                sendlogWithInfo("resultbestvref", best_vref);
            }
            else if(training_type == DDR_TRAINING_CACK)
            {
                best_cdc= (uint8) training_data_ptr->results.ca_vref.coarse_cdc[ddr_freq_index][ch][cs][num_phy];
                best_vref = (uint8) training_data_ptr->results.ca_vref.coarse_vref[ddr_freq_index][ch][cs][num_phy];

                sendlogWithInfo("resultbestcdc", best_cdc);
                sendlogWithInfo("resultbestvref", best_vref);
            }

            if (training_type == DDR_TRAINING_WRITE)
            {
                ddi_strcpy((char*)ddiTrainingBuffer,"yAxis_config_HC:" );
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"{");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);

                for(int coarse_vref =training_params_ptr->wr_dqdqs.coarse_vref_start_value; coarse_vref < training_params_ptr->wr_dqdqs.coarse_vref_max_value+1 ;coarse_vref ++)
                {
                    snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", local_vars_DQ->coarse_schmoo.coarse_dq_half_cycle[num_phy][coarse_vref]);
                    ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                }

                snprintf((char*)srcBuffer,sizeof(srcBuffer),"}");

                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"\r\n");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                sendTrainingData(ddiTrainingBuffer);
                memset(ddiTrainingBuffer,0, 3072);

                ddi_strcpy((char*)ddiTrainingBuffer,"yAxis_config_FC:" );
                snprintf((char*)srcBuffer,sizeof(srcBuffer),"{");

                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);

                for(int coarse_vref =training_params_ptr->wr_dqdqs.coarse_vref_start_value; coarse_vref < training_params_ptr->wr_dqdqs.coarse_vref_max_value+1 ;coarse_vref ++)
                {
                      snprintf((char*)srcBuffer,sizeof(srcBuffer),"%d,", local_vars_DQ->coarse_schmoo.coarse_dq_full_cycle[num_phy][coarse_vref]);
                      ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);
                }

                snprintf((char*)srcBuffer,sizeof(srcBuffer),"}");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);

                snprintf((char*)srcBuffer,sizeof(srcBuffer),"\r\n");
                ddi_strcat((char*)ddiTrainingBuffer, (char*)srcBuffer);

                sendTrainingData(ddiTrainingBuffer);
                memset(ddiTrainingBuffer,0, 3072);
            }

            sendlog("FRAME_END");
        }
    }
}





void readPHYSettings()
{
    for(perfLevel = 0; perfLevel < MAX_PRFS_LEVELS ; perfLevel++)
    {
        // do it for DQ
        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQ][perfLevel][ovrDrv][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQCAOvrDrv)>>shiftDQCAOvrDrv;



        }

        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQ][perfLevel][pU][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQCAPu)>>shiftDQCAPu;


        }

        for( int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQ][perfLevel][oDt][regLo][i] = (in_dword(ddr_phy_dq_config_hi_tool[i]) & maskDQCAhiOdt)<<shiftDQCAhiOdt;
            Data[ixDQ][perfLevel][oDt][regLo][i] = (Data[ixDQ][perfLevel][oDt][regLo][i] | ((in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQCAloOdt )>>shiftDQCAloOdt));



        }

        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQ][perfLevel][vOh][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQDQsCACKVoh )>>shiftDQDQsCACKVoh;




        }

        // do it for DQS


        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQs][perfLevel][ovrDrv][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQsCKOvrDrv)>>shiftDQsCKOvrDrv;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQs][perfLevel][pU][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQsCKPu)>>shiftDQsCKPu;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQs][perfLevel][oDt][regLo][i] = (in_dword(ddr_phy_dq_config_hi_tool[i]) & maskDQsCKOdt)>>shiftDQsCKOdt;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixDQs][perfLevel][vOh][regLo][i] = (in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQDQsCACKVoh)>>shiftDQDQsCACKVoh;
        }

        // do it for CA


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCA][perfLevel][ovrDrv][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQCAOvrDrv)>>shiftDQCAOvrDrv;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCA][perfLevel][pU][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQCAPu)>>shiftDQCAPu;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCA][perfLevel][oDt][regLo][i] = (in_dword(ddr_phy_ca_config_hi_tool[i]) & maskDQCAhiOdt)<<shiftDQCAhiOdt;
            Data[ixCA][perfLevel][oDt][regLo][i] = (Data[ixCA][perfLevel][oDt][regLo][i] | ((in_dword(ddr_phy_dq_config_lo_tool[i]) & maskDQCAloOdt )>>shiftDQCAloOdt));

        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCA][perfLevel][vOh][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQDQsCACKVoh)>>shiftDQDQsCACKVoh;
        }

        // DO it for CK

        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCK][perfLevel][ovrDrv][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQsCKOvrDrv)>>shiftDQsCKOvrDrv;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCK][perfLevel][pU][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQsCKPu)>>shiftDQsCKPu;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCK][perfLevel][oDt][regLo][i] = (in_dword(ddr_phy_ca_config_hi_tool[i]) & maskDQsCKOdt)>>shiftDQsCKOdt;
        }


        for(int i=perfLevel ;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
        {
            Data[ixCK][perfLevel][vOh][regLo][i] = (in_dword(ddr_phy_ca_config_lo_tool[i]) & maskDQDQsCACKVoh)>>shiftDQDQsCACKVoh;
        }

    }


}








void HAL_SDRAM_PHY_Update_Drive_Strength(int testline,uint32 prfs_level,uint32 odrv, uint32 pullup, uint32 odt, uint32 VOH)
{
  //uint32 val;
  uint64 mask=0;
  uint8 odt_hi,odt_lo;
  uint32 Test_val = 0;
  uint8 shift_bits = 0;



    if(testline==0)
    {
    for(int i=prfs_level;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
      {
      // setting DQ odrv value
      mask=0x38;
      shift_bits = 3;

      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;




      out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (odrv<<3), in_dword(ddr_phy_dq_config_lo_tool[i]));
      Test_val = 0;
      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;



      // setting DQ pull up value
      mask=0xE00;
      Test_val = 0;

      shift_bits = 9;

      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;


      out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (pullup<<9), in_dword(ddr_phy_dq_config_lo_tool[i]));
      Test_val = 0;

      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;



        //Setting DQ odt value
      odt_lo=odt&0x1;
      mask=0x80000000;
      Test_val = 0;


      Test_val = (in_dword(ddr_phy_dq_config_hi_tool[i]) & 0x3 )<<1;
      Test_val = (Test_val | ((in_dword(ddr_phy_dq_config_lo_tool[i]) & 0x80000000 )>>31));




      out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (odt_lo<<31), in_dword(ddr_phy_dq_config_lo_tool[i]));



      odt_hi=(odt&0x6)>>1;
      mask=0x3;


      out_dword_masked_ns(ddr_phy_dq_config_hi_tool[i], mask, odt_hi, in_dword(ddr_phy_dq_config_hi_tool[i]));

      Test_val = 0;

      Test_val = (in_dword(ddr_phy_dq_config_hi_tool[i]) & 0x3 )<<1;
      Test_val = (Test_val | ((in_dword(ddr_phy_dq_config_lo_tool[i]) & 0x80000000 )>>31));





      mask=0x18000;

      shift_bits = 15;
      Test_val = 0;

      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;



      out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (VOH<<15), in_dword(ddr_phy_dq_config_lo_tool[i]));

      Test_val = 0;

      Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;






    }
  }

  else if(testline==1)
    {
    for(int i=prfs_level;i<(sizeof(ddr_phy_dq_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS)
      {
        // setting DQS odrv value
        mask=0x1C0;
        shift_bits = 6;
        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;


        out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (odrv<<6), in_dword(ddr_phy_dq_config_lo_tool[i]));

        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;



        // setting DQS pull up value
        mask=0x7000;
        shift_bits = 12;
        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;


        out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (pullup<<12), in_dword(ddr_phy_dq_config_lo_tool[i]));

        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;

            // setting DQS odt value
        mask=0x1C;
        shift_bits = 2;
        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_hi_tool[i]) & mask )>>shift_bits;



        out_dword_masked_ns(ddr_phy_dq_config_hi_tool[i], mask, (odt<<2), in_dword(ddr_phy_dq_config_hi_tool[i]));
        Test_val = 0;
        Test_val = (in_dword(ddr_phy_dq_config_hi_tool[i]) & mask )>>shift_bits;


        mask=0x18000;

        shift_bits = 15;
        Test_val = 0;

        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;




        out_dword_masked_ns(ddr_phy_dq_config_lo_tool[i], mask, (VOH<<15), in_dword(ddr_phy_dq_config_lo_tool[i]));
        Test_val = 0;

        Test_val = (in_dword(ddr_phy_dq_config_lo_tool[i]) & mask )>>shift_bits;


      }
    }

    else if(testline == 2){
            for(int i=prfs_level;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS){
                mask=0x38;
                shift_bits = 3;
                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


                out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (odrv<<3), in_dword(ddr_phy_ca_config_lo_tool[i]));

                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


                // setting ca pull up value
                mask=0xE00;

                shift_bits = 9;
                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;



                out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (pullup<<9), in_dword(ddr_phy_ca_config_lo_tool[i]));



                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;



                //Setting ca odt value
                odt_lo=odt&0x1;
                mask=0x80000000;

                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_hi_tool[i]) & 0x3 )<<1;
                Test_val = (Test_val | ((in_dword(ddr_phy_ca_config_lo_tool[i]) & 0x80000000 )>>31));


                out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (odt_lo<<31), in_dword(ddr_phy_ca_config_lo_tool[i]));
                odt_hi=(odt&0x6)>>1;
                mask=0x3;
                out_dword_masked_ns(ddr_phy_ca_config_hi_tool[i], mask, odt_hi, in_dword(ddr_phy_ca_config_hi_tool[i]));

                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_hi_tool[i]) & 0x3 )<<1;
                Test_val = (Test_val | ((in_dword(ddr_phy_ca_config_lo_tool[i]) & 0x80000000 )>>31));


                mask=0x18000;
                shift_bits = 15;
                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


                out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (VOH<<15), in_dword(ddr_phy_ca_config_lo_tool[i]));
                Test_val = 0;

                Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;





        }
  }

    else if(testline == 3)
    {
        for(int i=prfs_level;i<(sizeof(ddr_phy_ca_config_lo_tool)/sizeof(uint32)-1);i+=MAX_PRFS_LEVELS){

        // setting CK odrv value
            mask=0x1C0;
            shift_bits = 6;
            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


            out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (odrv<<6), in_dword(ddr_phy_ca_config_lo_tool[i]));

            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;



        // setting CK pull up value
            mask=0x7000;
            shift_bits = 12;
            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;



            out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (pullup<<12), in_dword(ddr_phy_ca_config_lo_tool[i]));

            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;




    //setting CK odt value
            mask=0x1C;
            shift_bits = 2;
            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_hi_tool[i]) & mask )>>shift_bits;




            out_dword_masked_ns(ddr_phy_ca_config_hi_tool[i], mask, (odt<<2), in_dword(ddr_phy_ca_config_hi_tool[i]));

            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_hi_tool[i]) & mask )>>shift_bits;




            mask=0x18000;
            shift_bits = 15;
            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


            out_dword_masked_ns(ddr_phy_ca_config_lo_tool[i], mask, (VOH<<15), in_dword(ddr_phy_ca_config_lo_tool[i]));
            Test_val = 0;

            Test_val = (in_dword(ddr_phy_ca_config_lo_tool[i]) & mask )>>shift_bits;


        }
  }

} /* HAL_SDRAM_PHY_Update_Drive_Strength */


boolean ddi_ChangePHYSettings(int testline,int prfs,int ovrdrv,int PU,int ODT,int VOH,int ODTE )
{
    int prfs_level = 0;
    int mask = 0;
    HAL_SDRAM_PHY_Update_Drive_Strength(testline,prfs,ovrdrv,PU,ODT,VOH);

    if (testline == 0)
    {
        // DQ - 5th bith (starting from 0) need  to be set to set odt enable
        mask=0x60;

        out_dword_masked_ns(ddr_phy_dq_config_hi_tool[prfs], mask, ODTE<<5, in_dword(ddr_phy_dq_config_hi_tool[prfs]));

    }
    else if (testline == 1)
    {
        mask=0x60;
        // DQ - 6th bith (starting from 0) need  to be set to set odt enable
        out_dword_masked_ns(ddr_phy_dq_config_hi_tool[prfs], mask, ODTE<<6, in_dword(ddr_phy_dq_config_hi_tool[prfs]));

    }



    for(prfs_level=0; prfs_level<MAX_PRFS_LEVELS;prfs_level++){

    share_data->ddi_buf[prfs_level+2] = in_dword (ddr_phy_dq_config_lo_tool[prfs_level]);
    share_data->ddi_buf[prfs_level+10] = in_dword (ddr_phy_dq_config_hi_tool[prfs_level]);
    share_data->ddi_buf[prfs_level+18] = in_dword (ddr_phy_ca_config_lo_tool[prfs_level]);
    share_data->ddi_buf[prfs_level+26] = in_dword (ddr_phy_ca_config_hi_tool[prfs_level]);

  }



    return TRUE;



}

void ddi_ReadPHYSettings(int testline,int prfs )
{

    readPHYSettings();

    // 4 responses...
    sendPHYData(Data[testline][prfs][ovrDrv][regLo][prfs],"OverDrive" );
    sendPHYData(Data[testline][prfs][oDt][regLo][prfs],"ODT" );
    sendPHYData(Data[testline][prfs][pU][regLo][prfs],"PullUp" );
    sendPHYData(Data[testline][prfs][vOh][regLo][prfs],"VoH" );
    sendDDIResponse(ACK);


}


void displayData(void * buffer, uint32 size, void* tArgs, void *training_params_ptr)
{


    print_training_data(buffer,(ddr_training_args*)tArgs, training_params_ptr);

    sendResponseWithTag(ACK, "responseRetrain");




}

boolean ddi_DDRAddressLinesTest(int ch , int cs, char* tag)
{
    uint32 addr_line;
    uint32 data[32] = {0};
    uint32 i;
    uint64* base_addr = NULL;
    uint32 limit = 0;

    boot_log_message("ddi_DDRAddressLinesTest, Start");

    getBaseSize(ch, cs);
    base_addr = (uint64 *)gBase;
    limit = (uint32)gSize;

    snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
    boot_log_message(str);

    limit >>= 2;

    snprintf(str, 50, "Size : 0x%X \n", limit) ;
    boot_log_message(str);

    /* write negation of address to each word at (0x1 << n) */
    for (addr_line = 0x1, i = 1; addr_line <= limit; addr_line <<= 1, i++)
    {
    /* save the address prior to overwriting it */
    data[i] = base_addr[addr_line];
    base_addr[addr_line] = ~addr_line;
    }

    /*
    save content at base */
    data[0] = base_addr[0x0];

    /* write negation of address to word at 0x0 */
    base_addr[0x0] = ~0x0;

    /* check if same value is read back from each word */
    for (addr_line = 0x1, i = 1; addr_line <= limit; addr_line <<= 1, i++)
    {
    if (base_addr[addr_line] != ~addr_line)
    {
      boot_log_message("ddi_DDRAddressLinesTest, END");
      return FALSE;
    }
    else
    {
        /* restore the address line's original content */
         base_addr[addr_line] = data[i];
    }
    }

    /* restore base content */
    base_addr[0x0] = data[0];
    boot_log_message("ddi_DDRAddressLinesTest, END");
    return TRUE;




}

boolean ddi_DDRDataLinesTest(int ch , int cs, char* tag)
{
     uint32 pattern;
     uint32 data0, data1;
     uint64* base_addr = NULL;
     uint32 limit = 0;
     boot_log_message("ddi_DDRDataLinesTest, Start");

    getBaseSize(ch, cs);
    base_addr = (uint64 *)gBase;
    limit = (uint32)gSize;

    snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
    boot_log_message(str);

    limit >>= 2;

    snprintf(str, 50, "Size : 0x%X \n", limit) ;
    boot_log_message(str);

    /* save the data before writing to it for ram dump case */
    data0 = base_addr[0];
    data1 = base_addr[limit];

    /* test data lines by walking-ones */
    for (pattern = 0x1; pattern != 0x0; pattern <<= 1)
    {
        /* write (0x1 << n) to first word */
        base_addr[0] = pattern;

        /* write ~(0x1 << n) to last word to clear data lines */
        base_addr[limit] = ~pattern;

        /* check if same value is read back */
        if (base_addr[0] != pattern)
        {
            boot_log_message("ddi_DDRDataLinesTest, END");

            return FALSE;
        }

    }

    /* restore the data for ram dump*/
    base_addr[0] = data0;
    base_addr[limit]= data1;

    boot_log_message("ddi_DDRDataLinesTest, END");

    return TRUE;





}


boolean ddi_DDRTestOwnAddr(int ch , int cs, char* tag)
{

  uint32 offset;
  uint64* base_addr = NULL;
  uint32 size = 0;




  boot_log_message("ddi_DDRTestOwnAddr, Start");

  getBaseSize(ch, cs);
  base_addr = (uint64 *)gBase;
  size = (uint32)gSize;

  snprintf(str, 50, "Base : 0x%X \n", base_addr) ;
  boot_log_message(str);

  size >>= 2;

  snprintf(str, 50, "Size : 0x%X \n", size) ;
  boot_log_message(str);


  //1011 1111 1111 1111 1111 1111 1111 1111
  //  10 1111 1111 1111 1111 1111 1111 1111



  /* write each word with its own address */
  for (offset = 0; offset <= size; offset++)
  {
      *(base_addr+offset) = offset;


  }

  sayHello(tag);

  /* check if same value is read back from each word */
  for (offset = 0; offset <= size; offset++)
  {

    if (*(base_addr+offset) != offset)
    {

      return FALSE;
    }
  }

 sayHello(tag);
  /* write each word with negation of address */
  for (offset = 0; offset <= size; offset++)
  {

    *(base_addr+offset) = ~offset;

  }
  sayHello(tag);
  /* check if same value is read back from each word */
  for (offset = 0; offset <= size; offset++)
  {

    if (*(base_addr+offset) != ~offset)
    {

      return FALSE;
    }

  }


  boot_log_message("ddi_DDRTestOwnAddr, End");
  return TRUE;
}


void clearFlashParams()
{
    memset(&share_data->flash_params, 0x0, sizeof(struct ddr_params_partition));


}


void clearDDIBuff()
{

    memset(&share_data->ddi_buf, 0x0, sizeof(share_data->ddi_buf));
    share_data->ddi_buf[0] = 0xE12A3456;


}


void applyMRVal()
{
    uint8 level = sizeof(MRParams)/sizeof(MRParams[0]);
    uint8 j = 0;
    for (int  i  = 0; i < level; i++)
    {
        //MRParams[i].freq_switch_range_in_kHz = share_data->ddi_buf[j+36];
        if ( (share_data->ddi_buf[(j + 1) + 36]&0xFF) != 0xFF && (share_data->ddi_buf[(j + 1) + 36]&0xFF) != 0 )
        {
             MRParams[i].mr3Val = share_data->ddi_buf[(j + 1) + 36] & 0xFF;
        }
        else
        {
             MRParams[i].mr3Val = -1;
        }

        if ( ((share_data->ddi_buf[(j + 1) + 36]&0xFF00)>>8) != 0xFF && ((share_data->ddi_buf[(j + 1) + 36]&0xFF00)>>8) != 0)
        {
             MRParams[i].mr11Val = (share_data->ddi_buf[(j + 1) + 36] & 0xFF00)>>8;
        }
        else
        {
             MRParams[i].mr11Val = -1;
        }

        j = j+2;



    }

    // set the MR values
    for (int  i  = 0; i < level; i++) {
        if (MRParams[i].mr3Val != -1) {
            share_data->extended_cdt_runtime.bimc_freq_switch[i].MR3 = MRParams[i].mr3Val;
        }
        if (MRParams[i].mr11Val != -1) {
            share_data->extended_cdt_runtime.bimc_freq_switch[i].MR11 = MRParams[i].mr11Val;
        }
    }
}


void retrainDDR()
{
    // save MR values...
    uint8 level = sizeof(MRParams)/sizeof(MRParams[0]);
    uint8 j = 0;

    for (int  i  = 0; i < level; i++)
    {

        share_data->ddi_buf[j+36] = MRParams[i].freq_switch_range_in_kHz;
        if (MRParams[i].mr3Val != -1 && MRParams[i].mr3Val != 0xFFFFFFFF )
        {
             share_data->ddi_buf[(j + 1) + 36] = MRParams[i].mr3Val & 0xFF;
        }
        else
        {
            share_data->ddi_buf[(j + 1) + 36] = 0xFF;
        }

        if (MRParams[i].mr11Val != -1 && MRParams[i].mr3Val != 0xFFFFFFFF)
        {
            share_data->ddi_buf[(j+1)+36] = share_data->ddi_buf[(j+1)+36] | ((MRParams[i].mr11Val & 0xFF)<<8);
        }
        else
        {
            share_data->ddi_buf[(j+1)+36] = share_data->ddi_buf[(j+1)+36] | (0xFF<<8);
        }
        j = j+2;
    }

    share_data->ddi_buf[0] = 0xDD3DEB06;
    share_data->ddi_buf[1] = 0xb5b5b5b5;

    // flush the region containing the share data sctructure
    dcache_flush_region((void *)share_data, sizeof(*share_data));

    //busywait(10000);
    deInitTransport();
    //busywait(10000);
    boot_hw_reset(BOOT_WARM_RESET_TYPE);
     //boot_enable_led(DDR_TRAINING_LED, TRUE);
    // boot_ddr_do_ddr_training();
     //boot_enable_led(DDR_TRAINING_LED, FALSE);
}


















