/* @file RpmbCg.c
   
  This file implements the SDCC Content Generator. The Content Generator formats
  the 512-byte Data Frame as defined by the eMMC spec for RPMB Partition Access.
  
  Copyright (c) 2011-2013, 2015 Qualcomm Technologies Inc. All rights reserved.  
   
**/

/*=============================================================================
                              EDIT HISTORY


when         who     what, where, why
----------   -----   -----------------------------------------------------------
2015-04-20   jt/rm   Add support for UFS   
2013-01-17   vk      Fix warning
2012-07-11   jt      Added support to use the PCD to hold the key 
2012-07-10   bn      Code clean up 
2012-04-25   ag      Use new RNGDxe apis 
2012-04-03   bn      Changed the key to match /w the new TZ generated key. 
                     Changed the Hash function used to calculate HMAC 
2011-11-10   bn      Updated Write Counter handling. Code clean up. 
                     Added support for Reliable Write Count greater than 1
2011-10-26   bn/jt   Initial version 

=============================================================================*/
#include <Uefi.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include "RpmbCg.h"
#include "HmacLib.h"
#include <Protocol/EFIRng.h>
#include <Library/PcdLib.h>

static UINT32   g_WriteCounter = 0;
static UINT32   g_ReliableWrite = 1; 
static BOOLEAN  ReliableWriteInitialized = FALSE;
static BOOLEAN  bLockGetKeyFunction = FALSE;
static BOOLEAN  CounterInitialized = FALSE;
static BOOLEAN  pKeyInitialized = FALSE; 
static UINT8    pKey[KEY_FIELD_LEN]; 
static BOOLEAN  IsUFSDevice = FALSE; 

static EFI_QCOM_RNG_PROTOCOL *RngProtocol = NULL;
                                 
#define REVERSE_WORD(x)     ((x & 0xFF) << 8) | ((x >> 8) & 0xFF)

#define REVERSE_DWORD(x)    (((x & 0xFF) << 24) | \
                             ((x & 0xFF000000) >> 24) | \
                             ((x & 0xFF00) << 8) | \
                             ((x & 0xFF0000) >> 8))                    

/******************************************************************************
* Name: HMACGen
*
* Description:
* This function generates the HMAC using the MAC Key and the provided Message
*
* Arguments:                       
*    pMessage [IN]:    Pointer to the Message to be used with
*                      the Key for the HMAC calculation.
*    MessageLen [IN]:  The length of the input Message used
*                      for the HMAC calculation.
*    pHmac [OUT]:      The resulting HMAC calculated over the Key
*                      and the input message
*  
* Returns:
*    TRUE:             HMAC successfully generated
*    FALSE:            Failed to generate the HMAC
* 
******************************************************************************/
static BOOLEAN HMACGen (UINT8 *pMessage, UINT32 MessageLen, UINT8 *pHmac)
{
  /* Clear the pHmac */
  SetMem(pHmac, MAC_FIELD_LEN, 0x00);

  /* Get the key from the Pcd, if necessary */
  if (FALSE == pKeyInitialized)
  {
     CopyMem(pKey, (UINT8 *)PcdGetPtr(SDCCRpmbKey), KEY_FIELD_LEN); 
     pKeyInitialized = TRUE; 
  }

  /* Calculate HMAC digest */
  if (EFI_SUCCESS != HmacLib(pKey, KEY_FIELD_LEN, pMessage,MessageLen, pHmac))                       
  {
     return FALSE;
  }
  
  return TRUE;  
}
/******************************************************************************
* Name: RandomGen
*
* Description:
* This function generates random numbers
*
* Arguments:                       
*    RandomSize [IN]:  Number of bytes of random number to generate
*    pNonce [OUT]:     Pointer to the resulting random number generated
*  
* Returns:
*    TRUE:             Random number successfully generated
*    FALSE:            Failed to generate the random number
* 
******************************************************************************/
static BOOLEAN RandomGen (UINT8 *pNonce, UINT32 RandomSize)
{
   UINT16 RandomLen = RandomSize;
   EFI_STATUS Status;
   EFI_GUID AlgoID;
   UINTN AlgoIDSize = sizeof(AlgoID);

   if (NULL == RngProtocol)
   {
      gBS->LocateProtocol(&gQcomRngProtocolGuid, NULL, (VOID **) &RngProtocol);

      if (NULL == RngProtocol)
      {
         return FALSE;
      }
   }

   Status = RngProtocol->GetInfo(RngProtocol, &AlgoIDSize, &AlgoID);

   if (EFI_SUCCESS != Status)
   {
      return FALSE;
   }

   Status = RngProtocol->GetRNG(RngProtocol, &AlgoID, RandomLen, pNonce);
   
   if (EFI_SUCCESS != Status)
   {
      return FALSE;
   }

   return TRUE;
}

/******************************************************************************
* Name: GetRPMBKeyProvisionPkt
*
* Description:
* This function retrieves a Key from TZ then it creates the KeyProvision packet
* with this Key. The packet will be sent to the eMMC to program the eMMC's Key
*
* Arguments:                       
*    PktBuffer  [OUT]: Pointer to a 512-byte formatted packet for Key provision.
* 
* Returns:
*    RPMB_CG_NO_ERROR:            Provision Key packet created successfully
*    RPMB_CG_ERR_INVALID_PARAM:   Invalid pointer to the passed in Packet buffer
*    RPMB_CG_ERR_INVALID_ACCESS:  This API is locked from access
* 
******************************************************************************/
int GetRPMBKeyProvisionPkt (UINT8* PktBuffer)
{ 
   RPMB_DATA_FRAME *RpmbDataFrame = (RPMB_DATA_FRAME *)PktBuffer;
   UINT16 RequestResponse;
   UINT16 Status = RPMB_CG_NO_ERROR;

   do
   {
      if (TRUE == bLockGetKeyFunction)
      {
         Status = RPMB_CG_ERR_INVALID_ACCESS;
         break; 
      }

      if (NULL == RpmbDataFrame)
      {
         Status = RPMB_CG_ERR_INVALID_PARAM;
         break;
      }

      /* Get the key from the Pcd, if necessary */
      if (FALSE == pKeyInitialized)
      {
         CopyMem(pKey, (UINT8 *)PcdGetPtr(SDCCRpmbKey), KEY_FIELD_LEN); 
         pKeyInitialized = TRUE; 
      }

      SetMem(RpmbDataFrame, sizeof(RPMB_DATA_FRAME), 0x00);

      RequestResponse = PROG_KEY_REQ;
      
      /* RPMB data frame is Big Endian. Reverse the byte order if
         system is Little Endian */
      RequestResponse = 
         IS_BIG_ENDIAN() ? RequestResponse : REVERSE_WORD(RequestResponse);
      
      CopyMem(RpmbDataFrame->RequestResponse, &RequestResponse, 
              sizeof(RpmbDataFrame->RequestResponse));

      CopyMem(RpmbDataFrame->KeyMAC, pKey, sizeof(RpmbDataFrame->KeyMAC));

   } while (0);
     
   return Status;
}

/******************************************************************************
* Name: ProcessRPMBResponsePkt
*
* Description:
* This function validates the Write Response packet by authenticating
* the packet's HMAC. If authentication passes, it initializes the Write Counter
* on the first call. On subsequent calls, it validates the eMMC's Write Counter
* embedded in the Response packet and the CG's local Write Counter.
* return error code if the Write Counters do not match. 
*
* Arguments:
*    PktBuffer  [IN]: Pointer to a Write Response packet
* 
* Returns:
*    RPMB_CG_NO_ERROR:              The HMAC authentication and Write Counter
*                                   validation succeeded
*    RPMB_CG_ERR_INVALID_PARAM:     Invalid pointer to the passed in Packet buffer
*    RPMB_CG_ERR_MAC_GENERATION:    The HMAC generation failed.
*    RPMB_CG_ERR_MAC:               The HMAC authentication failed
*    RPMB_CG_ERR_WRITE_COUNTER:     The Write Counters validation failed
* 
******************************************************************************/
int ProcessRPMBResponsePkt (UINT8* PktBuffer)
{
   UINT8  pHmac[MAC_FIELD_LEN];
   UINT32 WriteCounter, MessageLen;
   UINT16 Status = RPMB_CG_NO_ERROR;
   UINT16 Result;

   RPMB_DATA_FRAME *RpmbDataFrame = (RPMB_DATA_FRAME *)PktBuffer;

   do
   {
      if (NULL == PktBuffer)
      {
         Status = RPMB_CG_ERR_INVALID_PARAM;
         break;
      }
    
      /* The HMAC is calculated over the entire frame minus the StuffBytes
         and KeyMAC fields */
      MessageLen = sizeof(RPMB_DATA_FRAME) - sizeof(RpmbDataFrame->KeyMAC) - 
         sizeof(RpmbDataFrame->StuffBytes);

      /* Generate the HMAC of the passed in Write Response packet */
      if (FALSE == HMACGen(RpmbDataFrame->Data, MessageLen, pHmac))
      {
         Status = RPMB_CG_ERR_MAC_GENERATION; 
         break;
      }
 
      /* Authenticate the Write Response packet by comparing the locally
         generated HMAC with the one embedded in the packet by eMMC */
      if (CompareMem(pHmac, RpmbDataFrame->KeyMAC, sizeof(RpmbDataFrame->KeyMAC)))
      {
         Status = RPMB_CG_ERR_MAC;
         break; 
      }
 
      /* Get the 2-byte Result field from the Response packet */
      CopyMem(&Result, RpmbDataFrame->Result, sizeof(RpmbDataFrame->Result));
   
      /* RPMB Data Frame uses Big Endian. Reverse the byte order if needed */
      Result = IS_BIG_ENDIAN() ? Result : REVERSE_WORD(Result);

      if (Result != 0x00)
      {
         Status = RPMB_CG_ERR_INVALID_RESULT;
         break;
      }
   
      /* Get the 4-byte Write Counter field */ 
      CopyMem(&WriteCounter, RpmbDataFrame->WriteCounter, 
              sizeof(RpmbDataFrame->WriteCounter));

      /* RPMB Data Frame is Big Endian format. Reverse the byte order if needed */
      WriteCounter = IS_BIG_ENDIAN() ? WriteCounter : REVERSE_DWORD(WriteCounter);         
      
      /* HMAC Authentication passed. Result field is good. On first boot up, initialize
         the CG's local Write Counter with the Write Counter in the Response packet */
      if (FALSE == CounterInitialized)
      {
         g_WriteCounter = WriteCounter;
         CounterInitialized = TRUE; 

         Status = RPMB_CG_NO_ERROR;
         break;
      }
   
      /* On subsequent calls, check if the CG's local Counter matches
         the Write Counter in eMMC */
      if (g_WriteCounter != WriteCounter)
      {
         Status = RPMB_CG_ERR_WRITE_COUNTER;
         break;
      }

   } while (0);

   return Status;
}

/******************************************************************************
* Name: GenerateRPMBReadPkts
*
* Description:
* This function generates a read command packet to do an authenticated read.
*
* Arguments:
*    StartSector   [IN] : Starting sector to read from 
*    PktBuffer     [OUT]: Pointer to a formatted packet to be sent to eMMC
* 
* Returns:
*    RPMB_CG_NO_ERROR:            The Read packet generation succeeded
*    RPMB_CG_ERR_INVALID_PARAM:   Invalid passed in pointer
*    RPMB_CG_ERR_RNG_GENERATION:  Error in generating the Random number
*                                 (used by the Nonce field)
* 
******************************************************************************/
int GenerateRPMBReadPkts (UINT32 StartSector, UINT32 Count, UINT8* PktBuffer)
{
   UINT8 pNonce[NONCE_FIELD_LEN]; 
   UINT16 TempStartSector = 0;
   //RPMB_DATA_FRAME *RpmbDataFrame = (RPMB_DATA_FRAME *)PktBuffer;
   UINT16 Status = RPMB_CG_NO_ERROR;
   UINT16 RequestResponse;
   UINT32 HalfSectorCount = Count*2, HalfSectorsRemaining = Count*2;
   UINT32 i = 0, NumReqFrames = 1; 
   UINT32 TransferCount; 

   StartSector = StartSector*2;

   if (NULL == PktBuffer)
   {
      return RPMB_CG_ERR_INVALID_PARAM;
   }

   /* Figure out how many read request frames are needed (UFS only) */
   if (IsUFSDevice)
   {
      NumReqFrames = HalfSectorCount / g_ReliableWrite; 
      if (HalfSectorCount % g_ReliableWrite)
      {
         NumReqFrames++; 
      }
   }

   SetMem(PktBuffer, NumReqFrames * sizeof(RPMB_DATA_FRAME), 0x00);

   /* Set the Request type to Data Read */ 
   RequestResponse = READ_DATA_REQ;
      
   /* RPMB data frame is Big Endian. Reverse the byte order if the
      system is Little Endian */
   RequestResponse = 
      IS_BIG_ENDIAN() ? RequestResponse : REVERSE_WORD(RequestResponse);

   if (FALSE == RandomGen(pNonce, NONCE_FIELD_LEN))
   {
      return RPMB_CG_ERR_RNG_GENERATION;
   }

   for (i = 0; i < NumReqFrames; i++)
   {
      /* 2-byte Request/Response field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + REQ_INDEX, 
              &RequestResponse, REQ_FIELD_LEN);  

      /* Format the 2-byte Address field of the RPMB Data Frame */
      TempStartSector = 
         IS_BIG_ENDIAN() ? StartSector : REVERSE_WORD(StartSector);

      /* 2-byte Starting Address field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + ADDR_INDEX, 
              &TempStartSector, ADDR_FIELD_LEN); 
     
      /* Format the Nonce field of the RPMB Data Frame */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + NONCE_INDEX, 
              pNonce, NONCE_FIELD_LEN);

      /* 2-byte Block Count field - Fill in only for UFS device */
      if (IsUFSDevice)
      {
         if (HalfSectorsRemaining > g_ReliableWrite)
         {
            TransferCount = g_ReliableWrite;
            HalfSectorsRemaining -= g_ReliableWrite; 
         }
         else
         {
            TransferCount = HalfSectorsRemaining; 
         }

         TransferCount = IS_BIG_ENDIAN() ? TransferCount : REVERSE_WORD(TransferCount);

         CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + BLK_CNT_INDEX, 
                 &TransferCount, BLK_CNT_FIELD_LEN);

         /* Update StartSector for next iteration */
         StartSector += g_ReliableWrite; 
      }
   } 
   
   return Status;
}

/******************************************************************************
* Name: GenerateRPMBWritePkts
*
* Description:
* This function takes the Response packet from previous Write. It generates the
* HMAC for the Response packet and compares the generated HMAC with the packet's
* HMAC. The packet is authenticated if the HMACs match.
* If the packet authentication succeeds, this function then checks the Result field
* of the Response packet. If the Result field is good, it saves the Write Counter
* embedded in the Response packet to the local Write Counter in CG.
* It then generates RPMB Data Frames as defined by the eMMC specification. Depending
* on the value of the Reliable Write Count (RW), the RPMB frames will be generated as follows:
* a. If RW > 1 and (2*Count) is a multiple of RW, the RPMB frames will be created so that
*    the driver must write to the RPMB partition with Write transfer size equals the RW Count value.
*    For example, if RW Count is 4 and Count = 8 sectors of data, 16 512-byte frames buffer
*    will be created. The driver must write 4 512-byte frames to the RPMB each write transfer
*    for a total of 4 transfers.
* b. If RW == 1 or (2*Count) is not a multiple of RW Count, the RPMB frames will be created so that
*    the driver must write a single 512-byte frame for each write transfer.
*
* Arguments:
*    StartSector   [IN]: Starting Sector
*    Count         [IN]: Number of sectors to write 
*    TableData     [IN]: Pointer to Data to be written to RPMB partition
*    RespPktBuffer [IN]: Pointer to privious Write Response packet. This
*                        packet is used to validate the Write Counter
*    PktBuffer    [OUT]: Pointer to a packet that CG has formated to Write
*                        Block command.
* 
* Returns:
*    RPMB_CG_NO_ERROR:              Write Packet generated successfully
*    RPMB_CG_ERR_INVALID_PARAM:     Invalid passed in pointer
*    RPMB_CG_ERR_MAC_GENERATION:    Error when generating the HMAC
*    RPMB_CG_ERR_MAC:               Generated HMAC mismatch with the HMAC in the
*                                   Write Response packet
*    RPMB_CG_ERR_WRITE_COUNTER:     CG's Counter mismatch with Write Counter in
*                                   the Write Response packet
* 
******************************************************************************/
int GenerateRPMBWritePkts (UINT32 StartSector, UINT32 Count, UINT8* TableData, 
                           UINT8 *RespPktBuffer, UINT8* PktBuffer)
{
   UINT8  pMessage[MAC_INPUT_LEN*RPMB_MAX_HALF_BLOCK_WRITE];
   UINT8  pHmac[MAC_FIELD_LEN];
   UINT16 BlkCount = 0, BlkCountBuff = 0, StartSectorBuff = 0, Result = 0;
   UINT32 WriteCounterBuff = 0, WriteCounter = 0;
   UINT32 i = 0, MessageLen = 0, MessageCount = 0, RpmbStartFrame = 0;
   UINT32 HalfSectorCount = Count*2;
   RPMB_DATA_FRAME *RpmbDataFrame = (RPMB_DATA_FRAME *)RespPktBuffer;

   UINT16 RequestResponse;

   if ((NULL == TableData) || (NULL == RpmbDataFrame) || (NULL == PktBuffer))
   {
      return RPMB_CG_ERR_INVALID_PARAM;
   }

   /* The HMAC is calculated over the entire frame minus the StuffBytes
     and KeyMAC fields */
   MessageLen = sizeof(RPMB_DATA_FRAME) - sizeof(RpmbDataFrame->KeyMAC) - 
                sizeof(RpmbDataFrame->StuffBytes);

   /* Generate the HMAC of the passed in Write Response packet */
   if (FALSE == HMACGen(RpmbDataFrame->Data, MessageLen, pHmac))
   {
      return RPMB_CG_ERR_MAC_GENERATION; 
   }

   /* Authenticate the Response packet by comparing the HMACs */
   if (CompareMem(pHmac, RpmbDataFrame->KeyMAC, sizeof(RpmbDataFrame->KeyMAC)))
   {
      return RPMB_CG_ERR_MAC; 
   }

   /* Get the 2-byte Result field from the Response packet */
   CopyMem(&Result, RpmbDataFrame->Result, sizeof(RpmbDataFrame->Result));
   
   /* RPMB Data Frame uses Big Endian. Reverse the byte order if needed */
   Result = IS_BIG_ENDIAN() ? Result : REVERSE_WORD(Result);
   
   if (Result != 0x00)
   {
      return RPMB_CG_ERR_INVALID_RESULT;
   }

   /* HMACs match. Validate the Write Counters */
   CopyMem(&WriteCounter, RpmbDataFrame->WriteCounter, 
           sizeof(RpmbDataFrame->WriteCounter));

   /* RPMB Data Frame uses Big Endian. Reverse the byte order if needed */
   WriteCounter = IS_BIG_ENDIAN() ? WriteCounter : REVERSE_DWORD(WriteCounter);

   /* Response packet passed HMAC and Result field validation.
      Update CG's Write Counter with the Write Counter in the response packet */
   g_WriteCounter = WriteCounter;

   CounterInitialized = TRUE; 

   /* For eMMC, if the size of the buffer is multiple of RW, format the frames so that
      it will be written with the write size equals RW size. Only the last frame has
      the HMAC. If RW equals 1 or the buffer size is not mulitple of RW, format the
      RPMB frame so that it will be single frame write. Every RPMB frame has its own HMAC
    
      For UFS, we can write up to bRPMB_ReadWriteSize. If the size of the buffer
      is larger than that, we will break up the writes. */
   if (IsUFSDevice)
   {
      if (HalfSectorCount > g_ReliableWrite)
      {
         BlkCount = g_ReliableWrite; 
      }
      else
      {
         BlkCount = HalfSectorCount; 
      }
   }
   else 
   {
      if ((HalfSectorCount % g_ReliableWrite) == 0x00)
      {
         BlkCount = g_ReliableWrite;
      }
      else
      {
         BlkCount = 1;
      }
   }

   /* RPMB Address is the serial number of accessed half sector */
   RpmbStartFrame = StartSector * 2;

   /* Number of messages to be used for HMAC calculation */
   MessageCount= 0;

   RequestResponse = WRITE_DATA_REQ;
   RequestResponse = 
      IS_BIG_ENDIAN() ? RequestResponse: REVERSE_WORD(RequestResponse);

   /* Clear the entire buffer. We'll then set the fields of interest */
   SetMem(PktBuffer, (sizeof(RPMB_DATA_FRAME) * HalfSectorCount), 0x00);    

   for (i = 0; i < HalfSectorCount; i++)
   {
      /* RPMB Data frame uses Big endian format. Reverse the byte order if needed */
      BlkCountBuff = IS_BIG_ENDIAN() ? BlkCount : REVERSE_WORD(BlkCount);   

      StartSectorBuff = 
         IS_BIG_ENDIAN() ? RpmbStartFrame : REVERSE_WORD(RpmbStartFrame);

      WriteCounterBuff = 
         IS_BIG_ENDIAN() ? g_WriteCounter : REVERSE_DWORD(g_WriteCounter); 

      /* 2-byte Request/Response field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + REQ_INDEX, 
              &RequestResponse, REQ_FIELD_LEN); 

      /* 2-byte Block Count field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + BLK_CNT_INDEX, 
              &BlkCountBuff, BLK_CNT_FIELD_LEN); 

      /* 2-byte Starting Address field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + ADDR_INDEX, 
              &StartSectorBuff, ADDR_FIELD_LEN); 

      /* 4-byte Write Counter field */
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + 
              WRITE_COUNTER_INDEX, &WriteCounterBuff, WRITE_COUNTER_FIELD_LEN);

      /* Copy 256 bytes from caller's Data buffer to the RPMB Data Frame*/
      CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + DATA_INDEX, 
              (UINT8 *)TableData + (sizeof(RpmbDataFrame->Data) * i), 
              DATA_FIELD_LEN); 

      CopyMem(pMessage + (MAC_INPUT_LEN * MessageCount), (UINT8 *)PktBuffer + 
              (sizeof(RPMB_DATA_FRAME) * i) + DATA_INDEX, MAC_INPUT_LEN); 

      MessageCount += 1;

      /* In multi block RPMB write, the Address, Block Count are the same for
         the individual frame of the transfer and only the last frame has the HMAC
         which has been calculated for the entire transfer */
      if (MessageCount == BlkCount)
      {     
         if (FALSE == HMACGen(pMessage, BlkCount * MAC_INPUT_LEN, pHmac))
         {
            return RPMB_CG_ERR_MAC_GENERATION; 
         }

         /* 32-byte HMAC. Copy the generated HMAC to the RPMB Data Frame */
         CopyMem((UINT8 *)PktBuffer + (sizeof(RPMB_DATA_FRAME) * i) + MAC_INDEX, 
                  pHmac, MAC_FIELD_LEN);

         /* Update the Address and Write Counter for next transaction */
         RpmbStartFrame += BlkCount;
         g_WriteCounter += 1;
         MessageCount = 0;

         /* For UFS, update the BlkCount if the remaining half sector count is
            less than bRPMB_ReadWriteSize */
         if (IsUFSDevice && (HalfSectorCount - (i + 1) < g_ReliableWrite))
         {
            BlkCount = HalfSectorCount - (i + 1); 
         }
      } 
   }

   return RPMB_CG_NO_ERROR;
}

/******************************************************************************
* Name: SetRPMBConfigInfo
*
* Description:
* This function sends the Reliable Write Count parameter that is supported by eMMC. 
* 
* Arguments:                       
*    ReliableWriteCount [IN]: Reliable Write Count supported by eMMC
* 
* Returns:
*    RPMB_CG_NO_ERROR
* 
******************************************************************************/
int SetRPMBConfigInfo (UINT32 ReliableWriteCount)
{
   /* Only initialized once at boot up */
   if (FALSE == ReliableWriteInitialized)
   {
      g_ReliableWrite = ReliableWriteCount;
      ReliableWriteInitialized = TRUE;
   }

   return RPMB_CG_NO_ERROR;
}

/******************************************************************************
* Name: LockRPMBKeyProvision
*
* Description:
* This function prevents any future access to the GetRPMBProvisionKeyPkt API
* 
* Returns:
*    RPMB_CG_NO_ERROR:   GetRPMBKeyProvisionPkt is locked from future access
* 
******************************************************************************/
int LockRPMBKeyProvision ()
{
   bLockGetKeyFunction = TRUE;
   return RPMB_CG_NO_ERROR;
}

/******************************************************************************
* Name: SetDeviceType
*
* Description:
* This function sets the device type (eMMC/UFS) in the CG
* 
* Returns:
*    None
* 
******************************************************************************/
void SetDeviceType (UINT32 MediaId)
{
   if (SIGNATURE_32('u','f','s',' ') == MediaId)
   {
      IsUFSDevice = TRUE; 
   }
}

