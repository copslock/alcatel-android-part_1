/*********************************************************************
 * @file OfflineCrashDumpLib.c
 *
 * @brief Offline Crash Dump support
 *
 * Copyright (c) 2013-2014 by Qualcomm Technologies, Inc. All Rights Reserved.
 *
 *********************************************************************/
/*=======================================================================
                        Edit History

 when       who     what, where, why
 --------   ----    ---------------------------------------------------
 05/27/15   ao      update dload cookie set/clr/isSet 
 10/22/14   aus     Added support for EDL mode
 10/03/14   na      Fixing size of input address for 64-bit
 08/15/14   sm      Switched to new SCM API
 04/22/14   aus     Call into TZ to write to the DLOAD cookie address
 04/10/14   niting  Fixed warnings
 03/12/14   aus     Mass storage mode support
 04/15/13   yg      Remove Stalls
 04/03/13   niting  Moved offline crash dump support into OfflineCrashDumpLib 
 03/23/13   niting  Initial revision.
========================================================================*/

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/OfflineCrashDump.h>
#include <Library/SerialPortShLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/TzRuntimeLib.h>
#include <Protocol/EFIScm.h>
#include <Include/scm_sip_interface.h>

EFI_STATUS
EFIAPI
GetMemoryCaptureMode(
  UINT32 *MemoryCaptureMode
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 SharedIMEMBaseAddr = 0;
  UINT32 MemoryCaptureModeOffset = 0;
  volatile UINT32* mMemoryCaptureMode = NULL;
  if (MemoryCaptureMode == NULL)
    return EFI_INVALID_PARAMETER;

  Status = GetConfigValue ("SharedIMEMBaseAddr", &SharedIMEMBaseAddr);
  if ((Status != EFI_SUCCESS) || (SharedIMEMBaseAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "SharedIMEMBaseAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }

  Status = GetConfigValue ("MemoryCaptureModeOffset", &MemoryCaptureModeOffset);
  if ((Status != EFI_SUCCESS) || (MemoryCaptureModeOffset == 0))
  {
    DEBUG ((EFI_D_ERROR, "MemoryCaptureModeOffset not found in uefiplat.cfg\r\n"));
    return Status;
  }

  mMemoryCaptureMode = (UINT32* )((UINTN)SharedIMEMBaseAddr + MemoryCaptureModeOffset);
  *MemoryCaptureMode = *mMemoryCaptureMode;
  return Status;
}

EFI_STATUS
EFIAPI
GetMemoryCaptureModeRuntime(
  UINTN MemoryCaptureModeAddr,
  UINT32 *MemoryCaptureMode
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  if ((MemoryCaptureMode == NULL) || (MemoryCaptureModeAddr == 0))
    return EFI_INVALID_PARAMETER;

  *MemoryCaptureMode = MmioRead32(MemoryCaptureModeAddr);
  return Status;
}

EFI_STATUS
SetMemoryCaptureMode(
  UINT32 Value
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 SharedIMEMBaseAddr = 0;
  UINT32 MemoryCaptureModeOffset = 0;
  volatile UINT32* MemoryCaptureMode = NULL;

  Status = GetConfigValue ("SharedIMEMBaseAddr", &SharedIMEMBaseAddr);
  if ((Status != EFI_SUCCESS) || (SharedIMEMBaseAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "SharedIMEMBaseAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }

  Status = GetConfigValue ("MemoryCaptureModeOffset", &MemoryCaptureModeOffset);
  if ((Status != EFI_SUCCESS) || (MemoryCaptureModeOffset == 0))
  {
    DEBUG ((EFI_D_ERROR, "MemoryCaptureModeOffset not found in uefiplat.cfg\r\n"));
    return Status;
  }

  MemoryCaptureMode = (UINT32* )((UINTN)SharedIMEMBaseAddr + MemoryCaptureModeOffset);
  *MemoryCaptureMode = Value;
  
  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
SetMemoryCaptureModeRuntime(
  UINTN MemoryCaptureModeAddr,
  UINT32 Value
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (MemoryCaptureModeAddr == 0)
    return EFI_INVALID_PARAMETER;

  MmioWrite32(MemoryCaptureModeAddr, Value);
  
  return Status;
}

BOOLEAN
EFIAPI
IsMemoryCaptureModeValid(
  UINT32 MemoryCaptureMode
  )
{
  if ((MemoryCaptureMode == OFFLINE_CRASH_DUMP_DISABLE) || 
      (MemoryCaptureMode == OFFLINE_CRASH_DUMP_LEGACY_ENABLE) || 
      (MemoryCaptureMode == OFFLINE_CRASH_DUMP_ENABLE))
    return TRUE;
  else
    return FALSE;
}

EFI_STATUS
EFIAPI
GetAbnormalResetOccurred(
  UINT32 *AbnormalResetOccurred
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 SharedIMEMBaseAddr = 0;
  UINT32 AbnormalResetOccurredOffset = 0;
  volatile UINT32* mAbnormalResetOccurred = NULL;

  if (AbnormalResetOccurred == NULL)
    return EFI_INVALID_PARAMETER;

  Status = GetConfigValue ("SharedIMEMBaseAddr", &SharedIMEMBaseAddr);
  if ((Status != EFI_SUCCESS) || (SharedIMEMBaseAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "SharedIMEMBaseAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }

  Status = GetConfigValue ("AbnormalResetOccurredOffset", &AbnormalResetOccurredOffset);
  if ((Status != EFI_SUCCESS) || (AbnormalResetOccurredOffset == 0))
  {
    DEBUG ((EFI_D_ERROR, "AbnormalResetOccurredOffset not found in uefiplat.cfg\r\n"));
    return Status;
  }

  mAbnormalResetOccurred = (UINT32* )((UINTN)SharedIMEMBaseAddr + AbnormalResetOccurredOffset);
  *AbnormalResetOccurred = *mAbnormalResetOccurred;

  return Status;
}

EFI_STATUS
SetAbnormalResetOccurred(
  UINT32 Value
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 SharedIMEMBaseAddr = 0;
  UINT32 AbnormalResetOccurredOffset = 0;
  volatile UINT32* AbnormalResetOccurred = NULL;

  Status = GetConfigValue ("SharedIMEMBaseAddr", &SharedIMEMBaseAddr);
  if ((Status != EFI_SUCCESS) || (SharedIMEMBaseAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "SharedIMEMBaseAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }

  Status = GetConfigValue ("AbnormalResetOccurredOffset", &AbnormalResetOccurredOffset);
  if ((Status != EFI_SUCCESS) || (AbnormalResetOccurredOffset == 0))
  {
    DEBUG ((EFI_D_ERROR, "AbnormalResetOccurredOffset not found in uefiplat.cfg\r\n"));
    return Status;
  }

  AbnormalResetOccurred = (UINT32* )((UINTN)SharedIMEMBaseAddr + AbnormalResetOccurredOffset);
  *AbnormalResetOccurred = Value;
  
  return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
SetAbnormalResetOccurredRuntime(
  UINTN AbnormalResetOccurredAddr,
  UINT32 Value
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (AbnormalResetOccurredAddr == 0)
    return EFI_INVALID_PARAMETER;
 
  MmioWrite32(AbnormalResetOccurredAddr, Value);

  return Status;
}

BOOLEAN
EFIAPI
IsAbnormalResetOccurredValid(
  UINT32 AbnormalResetOccurred
  )
{
  if ((AbnormalResetOccurred == ABNORMAL_RESET_DISABLE) || (AbnormalResetOccurred == ABNORMAL_RESET_ENABLE))
    return TRUE;
  else
    return FALSE;
}

EFI_STATUS
EFIAPI
SetDLOADCookie(VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 mMassStorageCookieAddr = 0;
  UINT64 MassStorageCookieValue[2];

  Status = GetConfigValue ("MassStorageCookieAddr", &mMassStorageCookieAddr);
  if ((Status != EFI_SUCCESS) || (mMassStorageCookieAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "MassStorageCookieAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }
    /* Only write the bit indicated by the cookie */
	/*Bits 20 to 23 are reserved for XBL dload cookies
      val = MmioRead32(mMassStorageCookieAddr);
      val &= 0xFF0FFFFFUL;
      val |= ((PcdGet32(PcdMassStorageCookie0) & 0x00F0)<<16);
     MmioWrite32(mMassStorageCookieAddr, val); */

/* For SW that supports new SCM call, we have the cookie size set to 0 */
    MassStorageCookieValue[0] = mMassStorageCookieAddr;
    MassStorageCookieValue[1] = (PcdGet32(PcdMassStorageCookie0) & 0xFFFF);
    Status = TzFastcall (TZ_IO_ACCESS_WRITE_ID, TZ_IO_ACCESS_WRITE_ID_PARAM_ID, MassStorageCookieValue, 2);
  
  return Status;
}

EFI_STATUS
EFIAPI
ClearDLOADCookie(VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 mMassStorageCookieAddr = 0;
  UINT64 MassStorageCookieValue[2];
    
  Status = GetConfigValue ("MassStorageCookieAddr", &mMassStorageCookieAddr);
  if ((Status != EFI_SUCCESS) || (mMassStorageCookieAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "MassStorageCookieAddr not found in uefiplat.cfg\r\n"));
    return Status;
  }

  /* Only clear the bits indicated by the cookie */
  /*Bits 20 to 23 are reserved for XBL dload cookies
   val = MmioRead32(mMassStorageCookieAddr);
   val &= 0xFF0FFFFFUL;
   MmioWrite32(mMassStorageCookieAddr, val);*/
   
   /* For SW that supports new SCM call, we have the cookie size set to 0 */
    MassStorageCookieValue[0] = mMassStorageCookieAddr;
    MassStorageCookieValue[1] = 0x0;
    Status = TzFastcall (TZ_IO_ACCESS_WRITE_ID, TZ_IO_ACCESS_WRITE_ID_PARAM_ID, MassStorageCookieValue, 2);

  return Status;
}

EFI_STATUS
EFIAPI
ClearDLOADCookieRuntime(
  UINTN mMassStorageCookieAddr,
  UINT32 DloadCookieSize
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT64 MassStorageCookieValue[2];  

  if ((mMassStorageCookieAddr == 0) || (DloadCookieSize == 0))
    return EFI_INVALID_PARAMETER;
 
  if (DloadCookieSize == 4){
     MassStorageCookieValue[0] = 0;
     MassStorageCookieValue[1] = 0;
     Status = TzFastcall (TZ_FORCE_DLOAD_ID, TZ_FORCE_DLOAD_ID_PARAM_ID, MassStorageCookieValue, 2);
  }
  else {
    MmioWrite32(mMassStorageCookieAddr, 0);
    MmioWrite32(mMassStorageCookieAddr + 4, 0);
  }
  return Status;
}

BOOLEAN
EFIAPI
IsDLOADCookieSet ( VOID )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 mMassStorageCookieAddr = 0;
  //UINT64 MassStorageCookieValue[2];
  QCOM_SCM_PROTOCOL *pQcomScmProtocol = NULL;
  UINT64            Parameters[SCM_MAX_NUM_PARAMETERS] = {0};
  UINT64            Results[SCM_MAX_NUM_RESULTS] = {0};
  
  Status = GetConfigValue ("MassStorageCookieAddr", &mMassStorageCookieAddr);
  if ((Status != EFI_SUCCESS) || (mMassStorageCookieAddr == 0))
  {
    DEBUG ((EFI_D_ERROR, "MassStorageCookieAddr not found in uefiplat.cfg\r\n"));
    return FALSE;
  }
  

  /* Locate QCOM_SCM_PROTOCOL */
  Status = gBS->LocateProtocol ( &gQcomScmProtocolGuid, NULL, (VOID **)&pQcomScmProtocol);
  if( EFI_ERROR(Status)) 
  {
    DEBUG(( EFI_D_ERROR, " Locate SCM Protocol failed, Status =  (0x%x)\r\n", Status));
    return FALSE;
  }

  Parameters[0] = mMassStorageCookieAddr;
  
  /* Make a SCM Sys call */
  Status = pQcomScmProtocol->ScmSipSysCall (pQcomScmProtocol,
                                            TZ_IO_ACCESS_READ_ID,
                                            TZ_IO_ACCESS_READ_ID_PARAM_ID,
                                            Parameters,
                                            Results
                                            );
  if (EFI_ERROR (Status)) 
  {
    DEBUG(( EFI_D_ERROR, "ScmSipSysCall() failed, Status = (0x%x)\r\n", Status));
    return FALSE;
  }
  
  
  
  /* Only check the bits indicated by the cookie */
  /*Bits 20 to 23 are reserved for XBL dload cookies
   MassStorageCookieValue[0] = mMassStorageCookieAddr;
   MassStorageCookieValue[1] = 0x0;
   Status = TzFastcall (TZ_IO_ACCESS_READ_ID, TZ_IO_ACCESS_READ_ID_PARAM_ID, MassStorageCookieValue, 1);
  */
  
  Results[0] &= 0x0000000000FF0000UL;

  if((Results[0]>>16 ) == PcdGet32(PcdMassStorageCookie0))
	return TRUE;
  else
	return FALSE;
  
}


EFI_STATUS
EFIAPI
SetEDLCookie(VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 mEDLCookieAddr = 0;
  UINT64 EDLCookieValue[2];

  Status = GetConfigValue ("EDLCookieAddr", &mEDLCookieAddr);
  if ((Status != EFI_SUCCESS) || (mEDLCookieAddr == 0))
  {
    DEBUG ((EFI_D_WARN, "EDLCookieAddr not found in uefiplat.cfg. Using PcdEDLCookieAddr.\r\n"));
	mEDLCookieAddr = PcdGet32(PcdEDLCookieAddr);
  }
  
  if(PcdGet32(PcdEDLCookieSize) == 4) {
    /* Only write the bit indicated by the cookie */
    EDLCookieValue[0] = MmioRead32(mEDLCookieAddr) | PcdGet32(PcdEDLCookie0);
    EDLCookieValue[1] = 0;
    Status = TzFastcall (TZ_FORCE_DLOAD_ID, TZ_FORCE_DLOAD_ID_PARAM_ID, EDLCookieValue, 2);
  }
  else {
    /* Write magic numbers to enable mass storage on reset */
    MmioWrite32(mEDLCookieAddr,
                PcdGet32(PcdEDLCookie0));
    MmioWrite32(mEDLCookieAddr + 4,
                PcdGet32(PcdEDLCookie1));
    MmioWrite32(mEDLCookieAddr + 8,
                PcdGet32(PcdEDLCookie2));
  }

  return Status;
}

EFI_STATUS
EFIAPI
ClearEDLCookie(VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 mEDLCookieAddr = 0;
  UINT64 PcdEDLCookie[2];
  
  Status = GetConfigValue ("EDLCookieAddr", &mEDLCookieAddr);
  if ((Status != EFI_SUCCESS) || (mEDLCookieAddr == 0))
  {
    DEBUG ((EFI_D_WARN, "EDLCookieAddr not found in uefiplat.cfg. Using PcdEDLCookieAddr.\r\n"));
	mEDLCookieAddr = PcdGet32(PcdEDLCookieAddr);
  }

  if(PcdGet32(PcdEDLCookieSize) == 4) {
    /* Only clear the bit indicated by the cookie */
    PcdEDLCookie[0] = MmioRead32(mEDLCookieAddr) & ~PcdGet32(PcdEDLCookie0);
    PcdEDLCookie[1] = 0;
    Status = TzFastcall (TZ_FORCE_DLOAD_ID, TZ_FORCE_DLOAD_ID_PARAM_ID, PcdEDLCookie, 2);
  }
  else {
    /* Clear the cookies */
    MmioWrite32(mEDLCookieAddr, 0);
    MmioWrite32(mEDLCookieAddr + 4, 0);
    MmioWrite32(mEDLCookieAddr + 8, 0);
  }

  return Status;
}

VOID
EFIAPI
PrintOfflineCrashDumpValues ( VOID )
{
#ifdef ENABLE_OCD_DEBUG
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 MemoryCaptureMode = 0;
  UINT32 AbnormalResetOccurred = 0;
  BOOLEAN DLOADCookieSet = FALSE;

  GetMemoryCaptureMode(&MemoryCaptureMode);

  GetAbnormalResetOccurred(&AbnormalResetOccurred);

  DLOADCookieSet = IsDLOADCookieSet();

  DEBUG((EFI_D_ERROR, "************Printing Offline Crash Dump Values**************\r\n"));

  DEBUG((EFI_D_ERROR, "  Offline Crash Dump Values:\r\n"));

  Status = GetMemoryCaptureMode(&MemoryCaptureMode);
  if (Status == EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "    MemoryCaptureMode: 0x%08x\r\n", MemoryCaptureMode));
  }

  Status = GetAbnormalResetOccurred(&AbnormalResetOccurred);
  if (Status == EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "    AbnormalResetOccurred: 0x%08x\r\n", AbnormalResetOccurred));
  }

  DLOADCookieSet = IsDLOADCookieSet();
  DEBUG((EFI_D_ERROR, "    Is DLOADCookieSet?: %a\r\n", (DLOADCookieSet == TRUE) ? "Set" : "Cleared" ));
  DEBUG((EFI_D_ERROR, "************************************************************\r\n"));
#endif
}

