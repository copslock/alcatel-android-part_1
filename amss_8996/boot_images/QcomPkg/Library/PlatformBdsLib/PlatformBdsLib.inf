#/** @file
#  
#  Qualcomm implementation for PlatformBdsLib.
#  
#  Copyright (c) 2011-2015, Qualcomm Technologies, Inc. All Rights Reserved.
#  Qualcomm Technologies Proprietary and Confidential.
#  Portions Copyright (c) 2007 - 2010, Intel Corporation. All rights reserved.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution.  The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#**/

#==============================================================================
#                              EDIT HISTORY
#
#
# when       who     what, where, why
# --------   ---     ----------------------------------------------------------
# 06/04/15   wg      Added PcdBootLogAddr & Size PCDs
# 03/04/15   ah	     Added gQcomPcieInitProtocolGuid
# 02/10/15   an	     Added GLINK protocol GUID
# 12/11/14   bh	     Removed BdsLoader.c and PlatformData.c
# 10/14/14   bn      Added gEfiSdccHCModeEnableProtocolGuid
# 07/31/14   bn      Removed gEfiSdccHCModeEnableProtocolGuid
# 11/01/13   aus     Added PCDs for FD size and FD base
# 10/25/13   vk      Remove warning as error
# 10/15/13   vk      Disable FBPT
# 08/01/13   wuc     Added Encryption Procotol Guid for setting HDCP key
# 07/11/13   yg      Move menu implementation to utils lib
# 04/29/13   yg      Add Clock Protocol Guid
# 04/24/13   rli     Added gEfiNpaProtocolGuid
# 04/08/13   aus     Added support for loading apps FV before BDS menu entry
# 04/22/13 bmuthuku Send signal to fTPM to disable NV flushing and call ScmExitbootservices 
#					 callback from BDS to maintain order of operation for variables flush and listener deregister.
# 04/05/13   vk      Support SDHC mode switch
# 03/20/13   nk      Change to use TSENS driver and not TSENS library
# 03/19/13   vk      Add QcomBaseLib
# 03/15/13   shl     Added Tool support protocol
# 03/14/13   shl     Added MorPpiLib for keyboard PPI test support for Microsoft
# 03/12/13   nk      Added support for Tsens logging
# 02/28/13   niting  Added UefiCfgLib support
# 02/19/13   vk      Remove unused PCDs
# 02/15/13   niting  Enable and disable display based on charging event
# 02/12/13   niting  Added capsule protocol to flush before write protection is enabled
# 01/31/13   niting  Updated POST time display
# 07/25/12   yg      Added PCD Flag for Disabling write protection
# 07/17/12 vishalo   Added DispInfo support
# 07/13/12   yg      Added Hotkey file
# 05/17/12   MiC     Added firmware update and firmware provisioning into core UEFI
# 05/15/12   vishalo Enable warning as error
# 05/08/12   yg      Added Variable services GUID
# 05/03/12   nk      Added support for write protect GPT partitions
# 04/24/12   yg      Add version display control PCD
# 03/08/12   vishalo Pcds for Charger Task
# 02/02/12   sy      Adding BSD license info
# 01/26/12   yg      Remove Platform and Chip info dependency
# 11/15/11   vishalo Run image from specified partition 
# 11/13/11   shl     Changed SecBootRegLib to SecBootSigLib 
# 10/28/11   jz      Added to support getting platform info and chip info
# 09/15/11   yg      Added SecBoot related lib
# 08/31/11   jz      Added gQcomTokenSpaceGuid
# 08/30/11   niting  Load ACPI tables in BDS
# 07/07/11   niting  Added entering of mass storage mode on hot key push
# 07/07/11   niting  Initial revision
#
#==============================================================================

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PlatformBdsLib
  FILE_GUID                      = F4BD20A8-CFC6-4CF4-A97D-8236CD4F038A
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  LIBRARY_CLASS                  = PlatformBdsLib|DXE_DRIVER   

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = IA32 X64 IPF EBC
#

[Sources]
  BdsPlatform.c
  BdsPlatform.h

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  IntelFrameworkModulePkg/IntelFrameworkModulePkg.dec
  EmbeddedPkg/EmbeddedPkg.dec
  QcomPkg/QcomPkg.dec
  ArmPkg/ArmPkg.dec
  
[LibraryClasses]
  BaseLib
  MemoryAllocationLib
  UefiBootServicesTableLib
  BaseMemoryLib
  DebugLib
  PcdLib
  GenericBdsLib
  EfiFileLib
  QcomBaseLib
  QcomLib
  QcomUtilsLib
  SecBootSigLib
  FwCommonLib
  FwUpdateLib
  FwProvisionLib
  ParserLib
  ProcLib
  UefiCfgLib
  FuseControlLib
  OfflineCrashDumpLib
  LzmaDecompressLib
  ExtractGuidedSectionLib

[Protocols]
  gEfiPartitionRecordGuid
  gEfiEmmcWpProtocolGuid
  gEfiVariableServicesProtocolGuid        ## CONSUMES
  gEfiCapsuleProtocolGuid                 ## CONSUMES
  gQcomDisplayPwrCtrlProtocolGuid         ## CONSUMES
  gQcomToolSupportProtocolGuid
  gEfiTsensProtocolGuid
  gEfiSdccConfigProtocolGuid        
  gEfiFirmwareVolumeBlockProtocolGuid     
  gEfiDevicePathProtocolGuid  
  gQcomScmProtocolGuid  
  gEfiNpaProtocolGuid
  gEfiGLINKProtocolGuid
  gEfiClockProtocolGuid
  gEfiEncryptionProtocolGuid
  gQcomPcieInitProtocolGuid

[Guids]
  gEfiACPITableLoadGuid
  gEfiEmmcUserPartitionGuid
  gEfiPartTypeSystemPartGuid
  gEfiEmmcGppPartition1Guid
  gEfiPlatPartitionTypeGuid
  gEfiSdRemovableGuid
  gEfiEmmcBootPartition1Guid
  gEfiEventExitBootServicesGuid
  gEfiEventChargerEnableGuid
  gEfiEventChargerDisableGuid
  gEfiLogFSPartitionGuid

[Pcd]
  gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdLogoFile
  gQcomTokenSpaceGuid.VersionDisplay
  gQcomTokenSpaceGuid.PcdKraitFrequencyMhz
  gQcomTokenSpaceGuid.DisableWriteProtect
  gEmbeddedTokenSpaceGuid.PcdEmbeddedFdSize
  gEmbeddedTokenSpaceGuid.PcdEmbeddedFdBaseAddress
  gQcomTokenSpaceGuid.PcdBootLogAddrPtr
  gQcomTokenSpaceGuid.PcdBootLogAddrPtr

[Guids.common]
  gQcomTokenSpaceGuid

