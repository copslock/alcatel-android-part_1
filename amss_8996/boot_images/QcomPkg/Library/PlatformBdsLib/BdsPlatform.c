/** @file
  This file include all platform action which can be customized by IBV/OEM.

  Copyright (c) 2010-2015, Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  Portions copyright (c) 2004 - 2008, Intel Corporation. All rights reserved.

  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 06/04/15   wg      Made BootCycleCount a global to work properly at ExitBootServicesCallBack
 05/13/15   ts      Enable PCIe callback for V2 8996 and UP
 03/17/15   vk      Disable PCIe callback
 03/16/15   vk      Enabled PCIe callback after making necessary modifications in PCIe Init
 03/11/15   vk      Disable PCIe callback
 02/25/15   bh      use "MTC" uefi variable for boot cycles count, remove "BSBootCycles" variable
 03/04/15   ah      Added PCIe protocol
 02/10/15   an      Added GLink protocol connection and teardown upon Platform exit 
 12/12/14   bh      Drain Serial port later, at end of PlatformBdsInit
 12/11/14   bh      LoadDebugFv when dropping into shell in BootHalt
 11/11/14   bh      Flag and input key passed to keep track for new QcomBds
 11/07/14   jjo     Get max TSENS temperature.
 10/14/14   bn      Added SdccConfigProtocol support
 09/05/14   vk      KW fixes
 07/31/14   bn      Removed SdhciEnableProtocol. SDHCi is enabled by default.
 07/21/14   jjo     TSENS GetTemp change.
 02/28/14   vk      Warning cleanup
 01/16/14   vk      Remove InCarveout
 01/13/14   vk      Sync to relevant 2.1 changes
 11/16/13   vj      Give back the first 1MB excluding th e1st page to OS.
 11/01/13   vk      Fix 64 bit compiler warnings
 10/15/13   vk      Add go to shell for virtio, based on cfg value
 10/07/13   yg      Print Sync duration
 10/02/13   ck      Add the support to set delay for usb enumeration in BDS menu
 09/24/13   niting  Update the addition of generic USB class entry to BootOrder
 09/13/13   shl     Reorged security calls so there is less code exposed in BdsPlatform.c
 09/11/13   shl     Added Tz fuse mile stone call
 08/01/13   wuc     Added Hdcp provisionnig data set support
 07/24/13   yg      Reduce logging
 07/11/13   yg      Use lib for Menu implementation
 06/28/13   niting  Added connect and delay for USB keyboard hotkey detection
 06/25/13   niting  Enable synchronous serial I/O only in non-PRODMODE
 06/06/13   yg      Add Low threashold for thermal mitigation
 05/31/13   shl     Added Qcom PK protection support
 05/29/13   niting  Optimized USB boot to only enumerate when required
 05/13/13   niting  Added option to force BDS menu
 05/08/13   aus     Print warning message if LoadDebugFV fails
 05/07/13   vk      Fix thermal mitigation check for negative values
 04/29/13   yg      Low power mode during charging
 04/24/13   rli     Added Npa Deinit in exit BS
 04/08/13   aus     Added support for loading apps FV before BDS menu entry
 04/19/13   yg      Print more bootorder info
 04/22/13  bmuthuku Call into ScmDxe to perform ExitBootServices fuctionality.
 04/09/13   yg      Cfg Enable/Disable Display switching during charging
 04/03/13   yg      Key turn display ON during charging
 04/05/13   vk      Support SDHC mode switch
 03/25/13   yg      Some time measurement cleanup
 03/20/13   nk      Change to use TSENS driver and not TSENS library
 03/19/13   vk      Display long version names
 03/15/13   shl     Added Tool support protocol for calling ProcessMorPpi fucntion
 03/14/13   shl     Added MorPpiLib for keyboard PPI test support for Microsoft
 03/13/13   yg      Cleanup Tsens code
 03/12/13   nk      Added support for Tsens logging
 03/01/13   niting  Only turn off display when charging
 02/28/13   niting  Read EnableShell flag from UefiCfgLib
 02/26/13   niting  Added some error print when syncing fails
 02/25/13   jd      Moved FwProvision after ACPI loading
 02/25/13   niting  Removed unneeded measuring signals
 02/20/13   niting  Added flag to toggle volatile boot options
 02/20/13   niting  Support EmuVariable
 02/15/13   niting  Enable and disable display based on charging event
 02/12/13   niting  Ensure Capsules get flushed before write protection is enabled.
 01/31/13   niting  Added BootOrder support; updated POST time display
 01/24/13   yg      Enable synchronous logs after BDS
 01/22/13   yg      Added some warning security alert comments
 01/17/13   niting  Moving Shell.efi to fv2
 12/21/12   shl     move set morppi signal after ACPI table is loaded
 12/20/12   yg      Force Menu for debugging option
 12/14/12   yg      Make Ebl default shell
 12/11/12   yg      Display BDS Time
 08/17/12   mic     Updateded to do FwUpdate/FwProvision before ACPI loading
 08/17/12   vishalo Fix DisplayBootTime overflow
 08/10/12   yg      Use Generic ReadAnyKey API to read hotkey
 07/27/12   yg      Cleanup WriteProtect logic
 07/24/12   yg      Enable WriteProtect based on Flag and Prod mode
 07/20/12   vishalo Disable BootOrder
 07/18/12   plc     Only enumerate all options if bootorder is empty, don't
                    boot from legacy algorithm until after BootOrder is first
                    attempted
 07/17/12   vishalo Display boot time and version info depending on variable
 07/17/12   yg      Restore launching Shell/Ebl
 07/13/12   yg      Add hotkey menu option
 07/10/12   niting  Disable removable media boot option in production mode; Enable removable media in non production.
 07/09/12   vishalo Assert if WP_GRP_SIZE is not a power of 2
 06/20/12   jz      Skip ProcessBGRT if in carveout mode
 06/11/12   niting  Disable features based on production mode.
 06/11/12   jz      Do not SetMorPpiSignal if in carveout mode
 06/11/12   niting  Changed scan code for mass storage mode
 05/31/12   niting  Changed order of update vars.
 05/25/12   sr      Updated hot keys.
 05/16/12   jz      Re-enable MorPpi
 05/17/12   MiC     Added firmware update and firmware provisioning into core UEFI
 05/17/12   jz      Re-enable MorPpi
 05/17/12   niting  Fix printing of cycles.
 05/15/12   vishalo Cleanup warnings
 05/15/12   vishalo Disable MorPpi
 05/10/12   yg      Sync variables only if the protocol is available
 05/09/12   yg      Keep the write protection off until the reset problem is resolved
 05/08/12   yg      Sync Variable tables during exit BS before write protection
 05/02/12   nk      Added support for write protect GPT partitions
 04/23/12   niting  Added more debug logging
 04/27/12   sr      Changes to resolve key scan flooding issue for MSFT
 04/26/12   yg      Change Hotkey mapping to ESC key (Cam + Vol-)
 04/24/12   yg      Control Version display to LCD screen
 04/20/12   aus     Signal ReadyToBoot event before hotkey entry to shell
 04/19/12   leungm  Added support for BGRT
 04/12/12   nk      Removed call to function RunChargerTask
 03/21/12   niting  Disable PXE Boot
 03/08/12   vishalo Launch Charger Task
 03/02/12   niting  Changed hotkey entry to not run startup.nsh
 02/25/12   niting  Added hotkey for shell entry
 02/22/12   jz      Fixed ST CRC issue, call PlatformBdsBootSuccess in PlatformBdsInit
 02/16/12   jz      Removed the SetTime workaround in PlatformBdsInit
 02/06/12   jz      Added PXE boot support
 01/26/12   yg      Move platform related code to Qcomlib
 01/23/12   jz      Temporarily skip logo display
 01/22/12   shl     Added MOR/PPI feture signal call
 12/20/11   niting  Added build version number to SmBios string
 12/19/11   yg      Platform info cleanup
 12/1/11    vishalo bootarm.efi from HDD (Non-removable + mounted FS)
 11/22/11   shl     Added measure boot support
 11/15/11   vishalo Run image from specified partition
 11/13/11   shl     Changed register secure boot function
 11/10/11   yg      Perf related fixes and reduce Shell delay
 10/28/11   jz      Added support to display platform and chip info
 09/19/11   niting  Boot HLOS first then drop into shell
 09/15/11   yg      PostSignal to enable Image Authentication
 09/14/11   yg      Added SetupSecurity Environment related code
 09/13/11   jz      Added UEFI shell support and gST->FirmwareRevision
 08/31/11   jz      Added support to display fw version
 08/30/11   niting  Load ACPI tables in BDS.
 07/27/11   yg      Send ReadyToBoot event if List was Empty.
 07/07/11   niting  Added hotkey detection for mass storage mode.
 07/07/11   niting  Initial revision.

=============================================================================*/

#include <PiDxe.h>

#include <Guid/EventGroup.h>

#include <Protocol/BlockIo.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/FirmwareVolume2.h>
#include <Protocol/LoadFile.h>
#include <Protocol/FirmwareVolumeBlock.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/PciIo.h>
#include <Protocol/DevicePath.h>
#include <Protocol/EFICapsule.h>
#include <Protocol/EFIDisplayPwrCtrl.h>
#include <Protocol/EFINpa.h>
#include <Protocol/EFIGLink.h>
#include <Protocol/EFIPCIeInit.h>

#include <Library/BaseLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DevicePathLib.h>
#include <Library/PrintLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PerformanceLib.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/EfiFileLib.h>
#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiLib.h>
#include <Library/QcomLib.h>
#include <Library/QcomUtilsLib.h>
#include <Library/UefiSigDb.h>
#include <Library/QcomTargetLib.h>
#include <Library/ProcAsmLib.h>
#include <Library/FuseControlLib.h>
#include <Library/GenericBdsLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/QcomBaseLib.h>
#include <Include/UefiInfoBlk.h>

#include <Guid/GlobalVariable.h>
#include <Library/SecBootSigLib.h>
#include <Guid/Gpt.h>
#include "Library/SerialPortShLib.h"
#include "BdsPlatform.h"

#include <Protocol/EFISdccConfig.h>
#include <Protocol/EFIVariableServices.h>

#include <Library/FwUpdateLib.h>
#include <Library/FwProvisionLib.h>
#include <Protocol/EFIEncryption.h>

#include <Protocol/EFITsens.h>

#include <Protocol/EFIToolSupport.h>
#include <Protocol/EFIScm.h>
#include <Protocol/EFIClock.h>
#include "ReleaseInfo.h"
#include "DBIDump.h"

#define ADDR_1MB 0x100000

extern VOID BdsBootDeviceSelect ( VOID );
extern EFI_STATUS BdsWaitForSingleEvent (IN UINT64 Timeout);
extern EFI_STATUS LoadDebugFv( VOID );

//Partition Guid
extern EFI_GUID gEfiEmmcGppPartition1Guid;
extern EFI_GUID gEfiEmmcUserPartitionGuid;
extern EFI_GUID gEfiEmmcBootPartition1Guid;
extern EFI_GUID gEfiLogFSPartitionGuid;

//Partition Type
extern EFI_GUID gEfiPartTypeSystemPartGuid;
extern EFI_GUID gEfiPlatPartitionTypeGuid;

extern EFI_GUID gEfiSdRemovableGuid;
extern EFI_GUID gEfiACPITableLoadGuid;

// Exit Boot Services guid
extern EFI_GUID gEfiEventExitBootServicesGuid;

//SCMDxe guid
extern EFI_GUID gQcomScmProtocolGuid;

// Charging Event GUID
extern EFI_GUID gEfiEventChargerEnableGuid;
extern EFI_GUID gEfiEventChargerDisableGuid;
EFI_EVENT EventChargingStarted = NULL;
EFI_EVENT EventChargingEnd = NULL;
EFI_QCOM_DISPLAY_PWR_CTRL_PROTOCOL *gDisplayPwrCtrlProtocol = NULL;
STATIC BOOLEAN DeviceCharging = FALSE;
STATIC BOOLEAN DispOffTimerActive = FALSE;
STATIC EFI_CLOCK_PROTOCOL  *gClockProtocol = NULL;

EFI_EVENT ACPITableLoadEvent  = NULL;
EFI_EVENT EfiExitBootServicesEvent = (EFI_EVENT)NULL;
EFI_VARIABLESERVICES_PROTOCOL *VariableServicesProtocol = NULL;
EFI_CAPSULE_PROTOCOL *CapsuleProtocol = NULL;
EFI_SDCC_CONFIG_PROTOCOL *SdccConfigProtocol = NULL;
QCOM_SCM_PROTOCOL *pSCMProtocol = NULL;
QCOM_PCIE_PROTOCOL *PcieInitProtocol = NULL;
EFI_NPA_PROTOCOL *NpaProtocol = NULL;
EFI_GLINK_PROTOCOL *GlinkProtocol = NULL;

STATIC HandleInfo HandleInfoList[32];
STATIC PartiSelectFilter HandleFilter;
STATIC UINT32 LcdDebugFlag = 0;
STATIC UINTN  EnableWriteProtectFlag = 1;
STATIC UINT8  EnableShellFlag = 0;
STATIC UINT8 VolatileTables = 0;
STATIC UINT8 EnableVolatileBootOptions = 0;

STATIC UINT32 BootCycleCount = 0;

EFI_STATUS
BdsStartCmd (
  IN UINTN  Argc,
  IN CHAR8  **Argv
  );

VOID
EFIAPI
PlatformBdsLoadShellForNonProdMode (
  IN  BDS_COMMON_OPTION *Option
  );

EFI_STATUS
EFIAPI
PlatformBdsPreLoadBootOption (
  IN  BDS_COMMON_OPTION *Option
  );

EFI_STATUS
EFIAPI
WriteLogBufToPartition (VOID);
  
STATIC
EFI_STATUS
DisplayBootTime (CHAR8* Msg, BOOLEAN OnLCD);

/**
  Displays POST Time.

**/
VOID
EFIAPI
DisplayPOSTTime ( VOID )
{
  DisplayBootTime("POST Time :", TRUE);
}

#define DISPLAY_STATE_NONE  0
#define DISPLAY_STATE_OFF   1
#define DISPLAY_STATE_ON    2

/* Keep the Display power status current all the time */
STATIC UINTN DisplayPowerState = DISPLAY_STATE_NONE;

/* Turn ON display if its already NOT */
EFI_STATUS
EFIAPI
TurnOnDisplay ( VOID )
{
  EFI_STATUS  Status;
  EFI_DISPLAY_TYPE eDisplayType = EFI_DISPLAY_TYPE_PRIMARY;
  EFI_DISPLAY_POWER_CTRL_STATE ePowerState = EFI_DISPLAY_POWER_STATE_ON;

  if (gDisplayPwrCtrlProtocol == NULL)
  {
    Status = gBS->LocateProtocol(&gQcomDisplayPwrCtrlProtocolGuid,
                                 NULL,
                                 (VOID**) &gDisplayPwrCtrlProtocol);

    if ((Status != EFI_SUCCESS) || (gDisplayPwrCtrlProtocol == NULL))
      DEBUG ((EFI_D_WARN, "Display Power Control protocol not available\r\n"));

    return Status;
  }

  if (DisplayPowerState == DISPLAY_STATE_ON)
    return EFI_SUCCESS;

  Status = gDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState);

  if (Status == EFI_SUCCESS)
  {
    DisplayPowerState = DISPLAY_STATE_ON;
    DEBUG(( EFI_D_WARN, "Turned display ON\r\n"));
  }
  else
  {
    DEBUG(( EFI_D_ERROR, "Display turn ON failed\r\n"));
  }

  return Status;
}

/* Turn Off display if its already NOT */
EFI_STATUS
EFIAPI
TurnOffDisplay (VOID)
{
  EFI_STATUS Status;
  EFI_DISPLAY_TYPE eDisplayType = EFI_DISPLAY_TYPE_PRIMARY;
  EFI_DISPLAY_POWER_CTRL_STATE ePowerState = EFI_DISPLAY_POWER_STATE_OFF;

  if (gDisplayPwrCtrlProtocol == NULL)
  {
    Status = gBS->LocateProtocol(&gQcomDisplayPwrCtrlProtocolGuid,
                                 NULL,
                                 (VOID**) &gDisplayPwrCtrlProtocol);

    if ((Status != EFI_SUCCESS) || (gDisplayPwrCtrlProtocol == NULL))
    {
      DEBUG ((EFI_D_WARN, "Display Power Control protocol not available\r\n"));
      return Status;
    }
  }

  if (DisplayPowerState == DISPLAY_STATE_OFF)
    return EFI_SUCCESS;

  Status = gDisplayPwrCtrlProtocol->DisplayPanelPowerControl(eDisplayType, ePowerState);

  if (Status == EFI_SUCCESS)
  {
    DisplayPowerState = DISPLAY_STATE_OFF;
    DEBUG(( EFI_D_WARN, "Turned display OFF\r\n"));
  }
  else
  {
    DEBUG(( EFI_D_ERROR, "Display turn OFF Failed\r\n"));
  }

  return Status;
}

EFI_STATUS
EFIAPI
EnterLowPowerMode (VOID)
{
  EFI_STATUS Status;

  if (gClockProtocol == NULL)
  {
    Status = gBS->LocateProtocol (&gEfiClockProtocolGuid, NULL, (VOID **)&gClockProtocol);
    if (Status != EFI_SUCCESS)
      return Status;
  }
  Status = gClockProtocol->EnterLowPowerMode(gClockProtocol);

  return Status;
}

EFI_STATUS
EFIAPI
ExitLowPowerMode (VOID)
{
  EFI_STATUS Status;

  if (gClockProtocol == NULL)
  {
    Status = gBS->LocateProtocol (&gEfiClockProtocolGuid, NULL, (VOID **)&gClockProtocol);
    if (Status != EFI_SUCCESS)
      return Status;
  }
  Status = gClockProtocol->ExitLowPowerMode(gClockProtocol);

  return Status;
}

VOID
EFIAPI
TurnOffDisplayIfCharging (IN EFI_EVENT        Event,
                          IN VOID             *Context);

VOID
DelayedDisplayOff (UINTN Secs)
{
  EFI_STATUS        Status;
  STATIC EFI_EVENT  EventDisplayOff = NULL;

  /* Create event to turn off display after specified time */
  UINT64 DisplayOffTimer = Secs * 10000000;

  /* If the timer is already active to turn Disp off it will happen */
  if (DispOffTimerActive == TRUE)
    return;

  Status = gBS->CreateEvent (
                  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  TurnOffDisplayIfCharging,
                  NULL,
                  &EventDisplayOff
                  );

  if(EFI_ERROR (Status)) {
    DEBUG(( EFI_D_WARN, "Could not register for display timer off event\r\n"));
    return;
  }

  DEBUG(( EFI_D_WARN, "Turning off display in %d seconds\r\n", Secs));

  Status = gBS->SetTimer(EventDisplayOff, TimerRelative, DisplayOffTimer);

  if(EFI_ERROR (Status)) {
    DEBUG(( EFI_D_WARN, "Could not set timer to turn off display\r\n"));
    return;
  }
  DispOffTimerActive = TRUE;
}

/* How long the Display should be kept ON before turning OFF */
#define  DISPLAY_OFF_DELAY_DURATION_SEC       7

/* Uncomment this to disable Vol UP/Down keys turning ON Display during charging  */
// #define DISABLE_VOL_KEY_DISP_PWR_CONTROL

EFI_STATUS
KeyNotifyFn (IN EFI_KEY_DATA     *KeyData)
{
  DEBUG ((EFI_D_WARN, "Key Notify %d\n", KeyData->Key.ScanCode));

  /* Check if its power key */
  if ((KeyData->Key.ScanCode == SCAN_SUSPEND)
#ifndef DISABLE_VOL_KEY_DISP_PWR_CONTROL
      || (KeyData->Key.ScanCode == SCAN_UP)
      || (KeyData->Key.ScanCode == SCAN_DOWN)
#endif
      )
  {
    if (DeviceCharging == TRUE)
    {
      TurnOnDisplay ();
      DelayedDisplayOff (DISPLAY_OFF_DELAY_DURATION_SEC);
    }
  }

  return EFI_SUCCESS;
}

VOID
KeyDisplayControl (BOOLEAN EnableKeyControl)
{
  EFI_STATUS        Status;
  EFI_KEY_DATA      NotifyKeyData;

  STATIC EFI_HANDLE        PwrKeyNotifyHandle = NULL;
  STATIC EFI_HANDLE        VolUpKeyNotifyHandle = NULL;
  STATIC EFI_HANDLE        VolDwnKeyNotifyHandle = NULL;
  STATIC EFI_SIMPLE_TEXT_INPUT_EX_PROTOCOL *SimpleInputEx = NULL;

  /* Get protocol handle if we don't have */
  if (SimpleInputEx == NULL)
  {
    Status = GetNativeKeypad (&SimpleInputEx);
    if (Status != EFI_SUCCESS)
    {
      DEBUG(( EFI_D_WARN, "Couldn't locate SimpleInputEx protocol\r\n"));
      return;
    }
  }

  if (EnableKeyControl)
  {
    /* If we have already registered for notification
     * no more action needed */
    if (PwrKeyNotifyHandle != NULL)
      return;

    NotifyKeyData.Key.UnicodeChar = 0;
    NotifyKeyData.KeyState.KeyShiftState = 0;
    NotifyKeyData.KeyState.KeyToggleState = 0;

    NotifyKeyData.Key.ScanCode = SCAN_SUSPEND;
    Status = SimpleInputEx->RegisterKeyNotify (SimpleInputEx,
                                               &NotifyKeyData,
                                               KeyNotifyFn,
                                               &PwrKeyNotifyHandle);
#ifndef DISABLE_VOL_KEY_DISP_PWR_CONTROL
    NotifyKeyData.Key.ScanCode = SCAN_UP;
    Status = SimpleInputEx->RegisterKeyNotify (SimpleInputEx,
                                               &NotifyKeyData,
                                               KeyNotifyFn,
                                               &VolUpKeyNotifyHandle);

    NotifyKeyData.Key.ScanCode = SCAN_DOWN;
    Status = SimpleInputEx->RegisterKeyNotify (SimpleInputEx,
                                               &NotifyKeyData,
                                               KeyNotifyFn,
                                               &VolDwnKeyNotifyHandle);
#endif
    if (Status != EFI_SUCCESS)
      DEBUG(( EFI_D_WARN, "Couldn't Register for Key Notification\r\n"));
  }
  else
  {
    Status = SimpleInputEx->UnregisterKeyNotify (SimpleInputEx,
                                                 PwrKeyNotifyHandle);
#ifndef DISABLE_VOL_KEY_DISP_PWR_CONTROL
    Status = SimpleInputEx->UnregisterKeyNotify (SimpleInputEx,
                                                 VolUpKeyNotifyHandle);
    Status = SimpleInputEx->UnregisterKeyNotify (SimpleInputEx,
                                                 VolDwnKeyNotifyHandle);
#endif
    if (Status != EFI_SUCCESS)
      DEBUG(( EFI_D_WARN, "Couldn't UnRegister Key Notification\r\n"));
  }
}

VOID
EFIAPI
TurnOffDisplayIfCharging (IN EFI_EVENT        Event,
                          IN VOID             *Context)
{
  if (DeviceCharging == TRUE)
  {
    KeyDisplayControl (TRUE);
    TurnOffDisplay ();
  }
  DispOffTimerActive = FALSE;
}

/*
 * This function processes Charging notifications
 */
VOID
EFIAPI
ChargingEventNotifyFn (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{

  KeyDisplayControl (TRUE);

  if (Event == EventChargingStarted)
  {
    DelayedDisplayOff (DISPLAY_OFF_DELAY_DURATION_SEC);
    EnterLowPowerMode ();

    DeviceCharging = TRUE;
  }
  else if (Event == EventChargingEnd)
  {
    ExitLowPowerMode ();
    /* Turn On Display */
    TurnOnDisplay();

    KeyDisplayControl (FALSE);

    DeviceCharging = FALSE;
  }
  else
    DEBUG(( EFI_D_WARN, "Unknown event. Could not toggle display\r\n"));

  return;
}

VOID
PrepareChargingDisplayControl (VOID)
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT8 Flag = 0;
  UINTN VarSize = sizeof(Flag);
  UINT32 ControlEnabled = 0;

  Status = GetConfigValue("ChargingDispControl", &ControlEnabled);

  /* See if this feature is enabled */
  if ((Status != EFI_SUCCESS) || (ControlEnabled == 0))
    return;

  /* Check if this feature is overrided */
  Status = gRT->GetVariable (L"DispCharging", &gQcomTokenSpaceGuid,
                             NULL, &VarSize, &Flag);

  /* default behaviour is to enable toggling upon charging */
  if (((Status != EFI_SUCCESS) && (Status != EFI_NOT_FOUND)) || (Flag == 1))
    return;

  /* Register Charging Events */
  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               ChargingEventNotifyFn,
                               NULL,
                               &gEfiEventChargerEnableGuid,
                               &EventChargingStarted);
  ASSERT_EFI_ERROR (Status);

  Status = gBS->CreateEventEx (EVT_NOTIFY_SIGNAL,
                               TPL_NOTIFY,
                               ChargingEventNotifyFn,
                               NULL,
                               &gEfiEventChargerDisableGuid,
                               &EventChargingEnd);
  ASSERT_EFI_ERROR (Status);

  return;
}

#define VAR_PRINT_LINE_LENGTH      16             /* 16 bytes */

/** Prints out a variable to screen - VariableIndex is optional but can
    be useful if printing out multiple variables */
VOID
EFIAPI
PrintVariableValue (
  IN      CHAR16            *VariableName,
  IN      EFI_GUID          *VendorGuid
 )
{
  STATIC UINT8  *DataBuffer = NULL;
  UINTN          DataSize;
  UINT32         MaxVarSize = 65536;
  UINT32         Attributes;
  EFI_STATUS Status = EFI_SUCCESS;

  UINT32 Index;
  UINT32 LineIndex = 0;
  UINT8  LineStr[VAR_PRINT_LINE_LENGTH + 1]; // hold 16 characters + NULL char

  if (DataBuffer == NULL)
  {
    DataSize = MaxVarSize; /* 64KB */
    Status = gBS->AllocatePool(EfiBootServicesData,
                               DataSize,
                               (VOID**)&DataBuffer);
    if ((Status != EFI_SUCCESS) || (DataBuffer == NULL))
    {
      DEBUG ((DEBUG_WARN, "Could not allocate memory to print Boot Order: 0x%08x", Status));
      return;
    }
    SetMem(DataBuffer, DataSize, 0xFF);
  }

  DataSize = MaxVarSize;
  Status = gRT->GetVariable(VariableName, VendorGuid, &Attributes, &DataSize, DataBuffer);
  if (Status != EFI_SUCCESS)
  {
    DEBUG ((DEBUG_WARN, "  Name: %s, Not available:0x%08x\n",
            VariableName, Status));
    return;
  }

  DEBUG ((DEBUG_WARN, "  Name: %s, Data Size (bytes):%d\n", VariableName, DataSize));
  DEBUG ((DEBUG_WARN, "  Data Values in Hex and converted to ASCII:\n"));
  Index = 0;
  LineIndex = 0;
  LineStr[VAR_PRINT_LINE_LENGTH] = 0; // Setup NULL char
  SetMem(LineStr, VAR_PRINT_LINE_LENGTH, ' '); // fill with spaces
  while (Index < DataSize)
  {
    DEBUG ((DEBUG_WARN, "%02x ", DataBuffer[Index]));
    if ((DataBuffer[Index] < 0x20) || (DataBuffer[Index] > 0x7E))
    {
      LineStr[LineIndex] = '.'; // print a . for any unreadable chars
    }
    else
    {
      LineStr[LineIndex] = DataBuffer[Index];
    }

    LineIndex++;
    if (LineIndex == VAR_PRINT_LINE_LENGTH)
    {
      // Print ascii string
      DEBUG ((DEBUG_WARN, " | %a\n", LineStr));
      SetMem(LineStr, VAR_PRINT_LINE_LENGTH, ' '); // fill with spaces
      LineIndex = 0;
    }

    Index++;
  }
  if (LineIndex > 0)
  {
    for (Index = LineIndex; Index < VAR_PRINT_LINE_LENGTH; Index++)
    {
      DEBUG ((DEBUG_WARN, "   ")); // fill remaining chars with spaces
    }
    DEBUG ((DEBUG_WARN, " | %a\n", LineStr));
  }

  DEBUG ((DEBUG_WARN, "\n"));
  SetMem(DataBuffer, DataSize, 0xFF);
}

EFI_STATUS
EFIAPI
PrintBootOrder ( VOID )
{
  EFI_GUID   *VendorGuid;

  VendorGuid = &gEfiGlobalVariableGuid;
  DEBUG ((DEBUG_WARN, "-----Boot Order Variables-----\n"));
  PrintVariableValue(L"BootOrder", VendorGuid);
  PrintVariableValue(L"BootCurrent", VendorGuid);
  PrintVariableValue(L"BootNext", VendorGuid);
  PrintVariableValue(L"BootOptionSupport", VendorGuid);
  PrintVariableValue(L"Boot0000", VendorGuid);
  PrintVariableValue(L"Boot0001", VendorGuid);
  PrintVariableValue(L"Boot0002", VendorGuid);
  PrintVariableValue(L"Boot0003", VendorGuid);
  PrintVariableValue(L"Boot0004", VendorGuid);
  PrintVariableValue(L"Boot0005", VendorGuid);
  PrintVariableValue(L"Boot0006", VendorGuid);
  PrintVariableValue(L"Boot0007", VendorGuid);
  PrintVariableValue(L"Boot0008", VendorGuid);
  PrintVariableValue(L"Boot0009", VendorGuid);
  PrintVariableValue(L"Boot000A", VendorGuid);
  PrintVariableValue(L"Boot000B", VendorGuid);
  DEBUG ((DEBUG_WARN, "------------------------------\n"));

  return EFI_SUCCESS;
}

EFI_STATUS
BdsDeleteAllInvalidEfiBootOption (
  VOID
  );

STATIC BOOLEAN OptionFound = FALSE;
EFI_STATUS
BdsLibValidNonRemovableOptionFound ( VOID )
{
  OptionFound = TRUE;
  return EFI_SUCCESS;
}

EFI_STATUS
BdsLibEnumerateValidOptions ( VOID )
{
  /* First remove all invalid non-removable boot options */
  BdsDeleteAllInvalidEfiBootOption();

  /* If a valid non-removable option is not found, enumerate all options */
  if (!OptionFound)
  {
    LIST_ENTRY        BootLists;
    InitializeListHead (&BootLists);
    BdsLibBuildOptionFromVar (&BootLists, L"BootOrder");
    BdsLibEnumerateAllBootOption(&BootLists);
  }

  return EFI_SUCCESS;
}

/*
 * Bit 60 should be checked for readonly attribute.
 * But in the currently deployed partition in field have the Bit 3 set erroneously
 * So need to use Bit 3 as well until the Bit 60 get populated across.
 * */
#define PARTITION_READONLY_ATTRIBUTES               ((1ULL << 60) | (1ULL << 3))

#define INVALID_LBA                                  0xFFFFFFFF

#define ALIGN_START(lba,grpsz)                      ((lba) & (~((grpsz) - 1)))
#define ALIGN_END(lba,grpsz)                        (((lba) + ((grpsz) - 1)) & (~((grpsz) - 1)))

/**
 * Write protect the GPP partition for which read only bit is
 * set.
 * @return EFI_STATUS
 */
EFI_STATUS
GPPWriteProtectPartition (
  VOID
  )
{
  EFI_STATUS                          Status;
  UINTN                               Index;
  EFI_PARTITION_ENTRY                 *PartEntry;
  UINT32                              Attrib = 0;
  UINT32                              MaxHandles;
  EFI_EMMC_WP_PROTOCOL                *EmmcWriteProtect;
  EFI_LBA                             StartingLBA = 0;
  EFI_LBA                             EndingLBA = 0;
  UINT64                              ReadOnly = PARTITION_READONLY_ATTRIBUTES;
  EFI_EMMC_WP_CAPABILITIES            Capabilities;


  //Get the write protect protocol handle for GPP device
  Attrib |= BLK_IO_SEL_PARTITIONED_GPT;
  Attrib |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;
  Attrib |= BLK_IO_SEL_MATCH_ROOT_DEVICE;

  //GPP Partition
  HandleFilter.PartitionType = 0;
  HandleFilter.RootDeviceType = &gEfiEmmcGppPartition1Guid;
  HandleFilter.VolumeName = 0;

  MaxHandles = sizeof(HandleInfoList)/sizeof(*HandleInfoList);

  // BLKIO Handles for GPT Partition
  Status = GetBlkIOHandles(Attrib, &HandleFilter, HandleInfoList, &MaxHandles);

  if (MaxHandles == 0)
    return EFI_NO_MEDIA;

  if (HandleInfoList == NULL)
    return EFI_NO_MEDIA;

  /* Get the Writeprotect protocol handle for this hw partition */
  Status = gBS->HandleProtocol (HandleInfoList[0].Handle,
                                   &gEfiEmmcWpProtocolGuid,
                                   (VOID **) &EmmcWriteProtect);
  if (EFI_ERROR (Status))
    return Status;

  // Get WP_GRP_SIZE
  SetMem (&Capabilities, sizeof(Capabilities), 0);

  Status = EmmcWriteProtect->GetDeviceCapabilities (EmmcWriteProtect, &Capabilities);
  if (EFI_ERROR (Status))
    return (Status);

  // WP_GRP_SIZE should be power of 2
  if (( ( (Capabilities.WriteProtectGroupSize) & (Capabilities.WriteProtectGroupSize - 1)) != 0) ||
      (Capabilities.WriteProtectGroupSize == 0))
  {
    AsciiPrint("\nERROR: WP_GRP_SIZE is not a power of 2\n");
    ASSERT_EFI_ERROR (EFI_DEVICE_ERROR);
    return EFI_DEVICE_ERROR;
  }

  /* Start with invalid LBA */
  StartingLBA = INVALID_LBA;

  // loop through all handles
  for (Index = 0; Index < MaxHandles; Index++)
  {
    // Handles with attribute guid
    Status = gBS->HandleProtocol (HandleInfoList[Index].Handle,
                                   &gEfiPartitionRecordGuid,
                                   (VOID **) &PartEntry);
    if (EFI_ERROR (Status))
      continue;

    /* If readonly flag is set */
    if (PartEntry->Attributes & ReadOnly)
    {
      if (StartingLBA == INVALID_LBA)
      {
        StartingLBA = PartEntry->StartingLBA;
        StartingLBA = ALIGN_START (StartingLBA, Capabilities.WriteProtectGroupSize);
      }

      EndingLBA = PartEntry->EndingLBA;
      EndingLBA = ALIGN_END (EndingLBA, Capabilities.WriteProtectGroupSize);
    }
    else
    {
      if (StartingLBA != INVALID_LBA)
      {
        UINTN LBACount = EndingLBA - StartingLBA;
        //  Write Protect the range that we know
        Status = EmmcWriteProtect->SetWriteProtect (EmmcWriteProtect, StartingLBA, LBACount);
        if (EFI_ERROR (Status))
          return Status;

        StartingLBA = INVALID_LBA;
      }
    }
  }

  /* If we still didn't write protect the known range do it now */
  if (StartingLBA != INVALID_LBA)
  {
    UINTN LBACount = EndingLBA - StartingLBA;
    Status = EmmcWriteProtect->SetWriteProtect (EmmcWriteProtect, StartingLBA, LBACount);
    if (EFI_ERROR (Status))
      return Status;
  }

  return Status;
}

/**
 * Write protect the BOOT partition for which read only bit is
 * set.
 * @return EFI_STATUS
 */

EFI_STATUS
BootWriteProtectPartition (
  VOID
  )
{
  EFI_STATUS                          Status;
  UINT32                              Attrib = 0;
  UINT32                              MaxHandles;
  EFI_EMMC_WP_PROTOCOL                *EmmcWriteProtect;
  EFI_LBA                             StartingLBA = 0;
  EFI_LBA                             EndingLBA = 0;

  //Get the write protect protocol handle for GPP device
  Attrib |= BLK_IO_SEL_MEDIA_TYPE_NON_REMOVABLE;
  Attrib |= BLK_IO_SEL_MATCH_ROOT_DEVICE;

  //BOOT 1 Partition
  HandleFilter.PartitionType = 0;
  HandleFilter.RootDeviceType = &gEfiEmmcBootPartition1Guid;
  HandleFilter.VolumeName = 0;

  MaxHandles = sizeof(HandleInfoList)/sizeof(*HandleInfoList);

  // BLKIO Handles for BOOT 1 Partition
  Status = GetBlkIOHandles(Attrib, &HandleFilter, HandleInfoList, &MaxHandles);

  if (MaxHandles == 0)
    return EFI_NO_MEDIA;

  /* Get the Writeprotect protocol handle for this hw partition */
  Status = gBS->HandleProtocol (HandleInfoList[0].Handle,
                                &gEfiEmmcWpProtocolGuid,
                                (VOID **) &EmmcWriteProtect);
  if (EFI_ERROR (Status))
    return Status;

  EndingLBA = HandleInfoList[0].BlkIo->Media->LastBlock;

  // Write Protect the whole Boot 1 Partition (also write protects the
  // other boot partition as well)
  Status = EmmcWriteProtect->SetWriteProtect (EmmcWriteProtect, StartingLBA,
                                              (EndingLBA - StartingLBA) + 1);
  if (EFI_ERROR (Status))
    return Status;

  return Status;
}

/**
 * Write protection call for the GPP and Boot partitions
 *@param  Event                 Event whose notification
 *                              function is being invoked.
 *
 *@param  Context               The pointer to the notification
 *                              function's context which is
 *                              implementation-dependent.
 */
VOID
EFIAPI
ExitBootServicesCallBack (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (NULL != EventChargingEnd)
     ChargingEventNotifyFn (EventChargingEnd, NULL);

  DisplayBootTime("Exit Boot Services :", TRUE);

  if (PRODMODE_ENABLED || JTAGDebugDisableFusesBlown()) {
    /* Flush serial buffer in production mode */
    SerialPortFlush();
  }

  /* Flush the NV Storage tables before turning ON write protection
   * Do this only if the protocol available. During initial stages the
   * the protocol might not be installed */
  if (VariableServicesProtocol != NULL)
  {
    UINT32 SyncEndTime, SyncStartTime = GetTimerCountms();

    Status = VariableServicesProtocol->FlushVariableNV(VariableServicesProtocol);
    SyncEndTime = GetTimerCountms();

    DEBUG ((EFI_D_WARN, "Sync Duration = %d ms\r\n", SyncEndTime - SyncStartTime));
  }

  if (CapsuleProtocol != NULL)
  {
    Status = CapsuleProtocol->FlushCapsulePartition(CapsuleProtocol);
    if (Status != EFI_SUCCESS)
    {
      DEBUG(( EFI_D_ERROR, "Failed to flush capsule partition, Status = 0x%08x\r\n", Status));
    }
  }

  /* This is added after the flushing of NV vars as this callback will deregister the Listeners,
   * hence FLushing to RPMB/GPT will fail after this point until HLOS SCM driver (only RPMB is supported) is loaded.
   */
  if (pSCMProtocol != NULL)
  {
    Status = pSCMProtocol->ScmExitBootServicesHandler (pSCMProtocol);
  }

  // Turn ON Write Protection for GPP and Boot partitions
  if (PRODMODE_ENABLED || EnableWriteProtectFlag || JTAGDebugDisableFusesBlown())
  {
    GPPWriteProtectPartition();
    BootWriteProtectPartition();
  }

  // Enable SDHCi Mode
  if (SdccConfigProtocol != NULL)
  {
    Status = SdccConfigProtocol->SdccEnableSdhciMode(SdccConfigProtocol);
  }

  if (PcieInitProtocol != NULL) 
  {
     Status = PcieInitProtocol->PCIeInitHardware(PcieInitProtocol);
  }

  // If Npa Driver is installed call deinit to close channel with RPM
  if (NpaProtocol != NULL)
  {
    NpaProtocol->NpaDeinit();
  }

  // If Glink Driver is installed call exit to tear down connections.
  // It should happen after Npa deinit.
  if (GlinkProtocol != NULL)
  {
    GlinkProtocol->GlinkRpmTeardown();
  }

}


//
// BDS Platform Functions
//
/**
 Detect hot key push for entering
   - Mass storage mode
   - UEFI Shell 2.0/EBL
  */
STATIC
EFI_STATUS
EFIAPI
PlatformBdsDetectHotKey (
  VOID
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_INPUT_KEY Key;
  UINT32   ReadKeyAttrib = 0;
  UINTN   DataSize;
  UINT8    Flag = 0;
  STATIC volatile UINT32 ForceMenuForJTAG = 0;

  //PrintBootOrder();

  DataSize = sizeof(Flag);
  Status = gRT->GetVariable (BDS_HOTKEY_STATE_VARNAME,
                             &gQcomTokenSpaceGuid,
                             NULL,
                             &DataSize,
                             &Flag);

  /* Disable hotkey menu if BDSHotKeyState = 1 */
  if ((Status == EFI_SUCCESS) && (Flag == BDSHotKeyStateDisableDetect)  && (ForceMenuForJTAG == 0))
  {
    DEBUG ((EFI_D_WARN, "HotKey Menu Disabled. Skipping HotKey Detection.\r\n"));
    return EFI_UNSUPPORTED;
  }

  ReadKeyAttrib |= READ_KEY_ATTRIB_RESET_AFTER_READ;
  ReadKeyAttrib |= READ_KEY_ATTRIB_NO_BLOCKING;

  /* Init for KW */
  Key.ScanCode = SCAN_NULL;
  Key.UnicodeChar = CHAR_NULL;


  /* Force hotkey menu if BDSHotKeyState = 2 */
  if (ForceMenuForJTAG || (Flag == BDSHotKeyStateForceMenu))
  {
    Status = EFI_SUCCESS;
    Key.ScanCode = SCAN_HOME;
  }
  else
  {
    // Check if HotKey is found
    Status = ReadAnyKey (&Key, ReadKeyAttrib);
  }

  if (Status == EFI_SUCCESS) {
    DEBUG(( EFI_D_INFO, "Key Detected: ScanCode = (0x%x), UnicodeChar =(0x%x) \r\n",
            Key.ScanCode, Key.UnicodeChar));

    EnableSynchronousSerialPortIO ();

    if (Key.ScanCode == SCAN_DELETE)
    {
      DEBUG(( EFI_D_WARN, "SCAN_DELETE detected, entering mass storage mode"));

      // This does not return if supported
      Status = EnterMassStorageMode ();
    }
    else if (Key.ScanCode == SCAN_HOME)
    {
      DEBUG(( EFI_D_WARN, "Hotkey detected, entering Menu\n"));

      // Signal read to boot event
      EfiSignalEventReadyToBoot();

      // End Perf marker
      PERF_END   (NULL, "BDS", NULL, 0);

      // Load Debug FV image here
      Status = LoadDebugFv ();
      if (Status != EFI_SUCCESS) {
         DEBUG(( EFI_D_WARN, "WARNING: Debug FV was not loaded"));
      }
      
      SerialPortDrain();
      EnableSynchronousSerialPortIO();

      // Launch Bds Menu
      LaunchMenu ("BDS_Menu.cfg");

      // Drop into EBL Shell
     // if (Status == EFI_SUCCESS)
      {
        CHAR8* argv[] = {"fv1:Ebl"};
        DEBUG(( EFI_D_WARN, "Attempting to start: fv1:Ebl\n"));
        BdsStartCmd (sizeof(argv)/sizeof(*argv), argv);
      }
      // Drop to UEFI Shell 2.0 with no startup script
      // Now we can easily go to Shell from Ebl, just scroll up and enter
      if (0){
        CHAR8* argv[] = {"fv2:Shell", "-nomap -nostartup"};
        DEBUG(( EFI_D_WARN, "Attempting to start: fv2:Shell\n"));
        BdsStartCmd (sizeof(argv)/sizeof(*argv), argv);
      }
    }
  }

  DEBUG(( EFI_D_INFO, "Continuing with BDS initialization\n"));
  return Status;
}

STATIC
EFI_STATUS
EFIAPI
PlatformBdsDetectBootHotKey (
  BDS_INIT_OPTION *InitOption
  )
{
  EFI_INPUT_KEY Key = {SCAN_NULL, 0};
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32   ReadKeyAttrib = 0;

  ReadKeyAttrib |= READ_KEY_ATTRIB_RESET_AFTER_READ;
  ReadKeyAttrib |= READ_KEY_ATTRIB_NO_BLOCKING;
  
  Status = ReadAnyKey (&Key, ReadKeyAttrib);
  
  if (Key.ScanCode == SCAN_DOWN) {
    *InitOption = BootFromRemovableMedia;
  }
  else {
    *InitOption = OptionNone; 
  }

  return Status;
}

VOID AskForShutdown(VOID)
{
  EFI_INPUT_KEY  Key;
  EFI_STATUS Status;
  Print(L"Press any key to shutdown\n");
  Status = ReadAnyKey(&Key, 0);
  if (Status == EFI_SUCCESS)
  {
    Print(L"Key detected, shutting down\n");
    gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL);
  }
}


/**
  This is called after the BDS exhuasts all options, either 
  through enumeration or attempts to boot
**/
VOID
PlatformBdsBootHalt(
  VOID
  )
{
  //Last resort if no other bootable option exists
  if (PRODMODE_DISABLED && (!JTAGDebugDisableFusesBlown()) && EnableShellFlag) {
    LoadDebugFv();
    PlatformBdsLoadShellForNonProdMode(NULL);
  }
  AskForShutdown();
}

/**
  An empty function to pass error checking of CreateEventEx ().

  @param  Event                 Event whose notification function is being invoked.
  @param  Context               The pointer to the notification function's context,
                                which is implementation-dependent.

**/
VOID
EFIAPI
ACPITableLoadEmptyFuntion (
  IN EFI_EVENT                Event,
  IN VOID                     *Context
  )
{
  return;
}

VOID
InitLcdDebugFlag(VOID)
{
  EFI_STATUS                Status;
  UINTN                     VarSize;

  if (PRODMODE_ENABLED || JTAGDebugDisableFusesBlown()) {
    LcdDebugFlag = 0;  /* Production mode - no messages */
    EnableShellFlag = 0;  /* Production mode - no shell    */
  }
  else
  {
    /* Read DispDebugInfo variable, and handle if it is not present, default no display */
    VarSize = sizeof(UINT32);
    Status = gRT->GetVariable(L"DispDebugInfo", &gQcomTokenSpaceGuid, NULL, &VarSize, &LcdDebugFlag);

    if (EFI_ERROR(Status))
      LcdDebugFlag = 0;  /* Default no info on LCD */

    /* Read EnableShell variable, and handle if it is not present, default no shell */
    VarSize = sizeof(EnableShellFlag);
    Status = gRT->GetVariable(L"EnableShell", &gQcomTokenSpaceGuid, NULL, &VarSize, &EnableShellFlag);

    if (EFI_ERROR(Status))
      EnableShellFlag = 0; /* Default no Shell*/
  }
}


#define VERSION_STR_LEN           96

/**
  Retrieve version string and send to the console
**/
STATIC
EFI_STATUS
DisplayVersion (
  VOID
  )
{
  EFI_STATUS                Status;
  CHAR8                     VersionStr[VERSION_STR_LEN];
  UINTN                     StrSize = VERSION_STR_LEN;
  BOOLEAN                   DispVer = PcdGetBool (VersionDisplay);

  /* Override compile time setting */
  if (LcdDebugFlag == 1) {
    DispVer = TRUE;
  }
  else {
    DispVer = FALSE;
  }

  Status = gRT->GetVariable (L"FwVerStr", &gQcomTokenSpaceGuid,
    NULL, &StrSize, VersionStr);
  if (EFI_ERROR (Status))
  {
    if (DispVer)
      AsciiPrint("\nQualcomm UEFI FW Version : Invalid or Unset\n");
    else
      DEBUG ((EFI_D_ERROR, "Qualcomm UEFI FW Version : Invalid or Unset\n"));
  }
  else
  {
    if (DispVer)
      AsciiPrint("\nQualcomm UEFI FW Version : %a\n", VersionStr);
    else
      DEBUG ((EFI_D_ERROR, "Qualcomm UEFI FW Version : %a\n", VersionStr));
  }

  if (REL_LABEL != NULL)
  {
    if (DispVer)
      AsciiPrint("UEFI AU   : %a\n", REL_LABEL);
    else
      DEBUG ((EFI_D_ERROR, "UEFI AU   : %a\n", REL_LABEL));
  }

  return EFI_SUCCESS;
}


/**
  Convert ARM Performance Monitor counter cycle
  count to time in milli seconds.

  @param CycleCount     Number of PMon cycles
  @return Time Milliseconds
**/
UINT64
CycleCountInMilliSec(UINT32 CycleCount)
{
  UINT64 Time;
  /* Cycle counter set to count every 64 cycles */
  Time = ((UINT64) CycleCount * 64)/ (PcdGet32(PcdKraitFrequencyMhz) * 1000);
  return  Time;
}

/**
 Display Boot Time in milli seconds
 **/

STATIC
EFI_STATUS
DisplayBootTime (CHAR8* Msg, BOOLEAN OnLcd)
{
  UINT64 Time;

  Time = GetTimerCountms ();

  if(LcdDebugFlag && OnLcd) {
    AsciiPrint("%a %lld ms\n", Msg, Time);
  }
  else {
    DEBUG(( EFI_D_ERROR, "%a %lld ms\n", Msg, Time));
  }

  return EFI_SUCCESS;
}

/**
  Retrieve platform info and send to the console
**/
STATIC
EFI_STATUS
DisplayPlatformInfo (
  VOID
  )
{
  EFI_STATUS         Status;
  CHAR8              DestBuffer[128];
  CHAR8*             PlatTypePtr;
  CHAR8*             ChipNamePtr;
  CHAR8*             ChipVerPtr;
  CHAR8              AsciiString[50];
  UINTN              Sz;
  BOOLEAN            DispVer = PcdGetBool (VersionDisplay);

  /* Override compile time setting */
  if( LcdDebugFlag == 1)
    DispVer = TRUE;
  else
    DispVer = FALSE;

  Status = GetPlatformStrings (DestBuffer, sizeof(DestBuffer),
                               &PlatTypePtr, &ChipNamePtr, &ChipVerPtr);

  if (Status != EFI_SUCCESS)
    return Status;

  /* Display to LCD Screen only if needed during development time */
  if (DispVer)
  {
    AsciiPrint("Platform  : %a\n", PlatTypePtr);
    AsciiPrint("Chip Name : %a\n", ChipNamePtr);
    AsciiPrint("Chip Ver  : %a\n", ChipVerPtr);
  }
  else
  {
    DEBUG ((EFI_D_ERROR, "Platform  : %a\n", PlatTypePtr));
    DEBUG ((EFI_D_ERROR, "Chip Name : %a\n", ChipNamePtr));
    DEBUG ((EFI_D_ERROR, "Chip Ver  : %a\n", ChipVerPtr));
  }

  DisplayBootTime ("BDS Time  :", TRUE);

  Sz = AsciiSPrint(AsciiString, sizeof (AsciiString),
                   "\nPlatform : %a\n", PlatTypePtr);

  /* For nul termination */
  ++Sz;

  gRT->SetVariable(L"PlatformInfo", &gQcomTokenSpaceGuid,
                   EFI_VARIABLE_BOOTSERVICE_ACCESS, Sz, AsciiString);

  return Status;
}

/**
  Updates the number of boot cycles and prints boot information stored in NV.

**/
VOID
EFIAPI
UpdateNVVars (
  VOID
  )
{
  EFI_STATUS Status;
  UINTN DataSize;
  UINT32 VarData = 0;

  // Print Boot Cycles
  DataSize = sizeof(BootCycleCount);
  Status = gRT->GetVariable (L"MTC",
                             &gQcomTokenSpaceGuid,
                             NULL,
                             &DataSize,
                             &BootCycleCount);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_WARN, "Boot Cycles: not set\n"));
    BootCycleCount = 0;
  }
  DEBUG((EFI_D_WARN, "Boot Cycles: %d\n", BootCycleCount));


  // Print Run Cycles
  DataSize = sizeof(UINT32);
  Status = gRT->GetVariable (L"RunCycles",
                             &gQcomTokenSpaceGuid,
                             NULL,
                             &DataSize,
                             &VarData);
  if (Status == EFI_NOT_FOUND)
  {
    VarData = 0;
  }
  else
  {
    ASSERT_EFI_ERROR (Status);
  }

  ++VarData;

  Status = gRT->SetVariable (L"RunCycles",
                             &gQcomTokenSpaceGuid,
                             (EFI_VARIABLE_NON_VOLATILE |
                              EFI_VARIABLE_BOOTSERVICE_ACCESS |
                              EFI_VARIABLE_RUNTIME_ACCESS),
                             DataSize,
                             &VarData);
  ASSERT_EFI_ERROR (Status);

  DataSize = sizeof(UINT32);
  Status = gRT->GetVariable (L"RunCycles",
                             &gQcomTokenSpaceGuid,
                             NULL,
                             &DataSize,
                             &VarData);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_WARN, "Run Cycles: not set\n"));
    VarData = 0;
  }
  DEBUG((EFI_D_WARN, "Run Cycles: %d\n", VarData));

  /* Check for presence of EnableShell in config file and set variable accordingly
     Note that this will override whatever option is selected/toggled by BDS Menu */
  VarData = 0;
  Status = GetConfigValue("EnableShell", &VarData);
  if (Status == EFI_SUCCESS)
  {
    UINT8 Flag = (UINT8)(VarData & 0xFF); /* Mask of upper bits - variable is only 1 byte */
    DataSize = sizeof(Flag);
    Status = gRT->SetVariable (L"EnableShell",
                               &gQcomTokenSpaceGuid,
                               (EFI_VARIABLE_NON_VOLATILE |
                                EFI_VARIABLE_BOOTSERVICE_ACCESS),
                               DataSize,
                               &Flag);
  }
}

/* Setup Platform security related environment */
VOID SetupPlatformSecurity (VOID)
{
  EFI_STATUS    Status;
  UINTN         VarSize;
  UINT8*        Buffer = NULL;
  UINT8         SetupMode, SecureBoot;
  UINT32        TypeGUIDBufferSize;

  /* Assume by default we are in setupmode and Secure Boot is not enabled */
  SetupMode = 1;
  SecureBoot = 0;

  VarSize = sizeof(SetupMode);
  Status = gRT->GetVariable (L"SetupMode", &gEfiGlobalVariableGuid,
                             NULL, &VarSize, &SetupMode);
  if (EFI_ERROR(Status))
  {
    DEBUG(( EFI_D_WARN, "SetupMode Variable does not exist. Will not enable Secure Boot\n"));
  }

  if (SetupMode == 1)
  {
    /* Disable secure boot if in setup mode */
    SecureBoot = 0;
    Status = gRT->SetVariable (L"SecureBoot", &gEfiGlobalVariableGuid,
                               EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                               VarSize, &SecureBoot);
  }
  else
  {
    /* Enable Secure Boot if not in setup mode */
    SecureBoot = 1;
    Status = gRT->SetVariable (L"SecureBoot", &gEfiGlobalVariableGuid,
                               EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                               VarSize, &SecureBoot);
  }

  /* Post signal here to enable Image Authentication */
  if (SecureBoot == 1)
    SetSecBootRegSignal();

  /* Set the variable indicating the signature types supported. If Auth routines support
   * more than SigDB lib supports, then the GUID's from Auth routines also need to be appended
   * to this list */
  TypeGUIDBufferSize = 0;
  if ((GetSupportedSignatureTypes (0, &TypeGUIDBufferSize) == SIG_DB_ENUM_BUFFER_SIZE_INSUFFICIENT) &&
     (TypeGUIDBufferSize > 0))
  {
    Buffer = AllocatePool(TypeGUIDBufferSize);

    if (Buffer)
    {
      if (GetSupportedSignatureTypes (Buffer, &TypeGUIDBufferSize) == SIG_DB_ENUM_SUCCESS)
      {
        VarSize = TypeGUIDBufferSize;
        Status = gRT->SetVariable (L"SignatureSupport", &gEfiGlobalVariableGuid,
                                   EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                                   VarSize, Buffer);
      }
      FreePool (Buffer);
    }
  }
}

EFI_STATUS
CheckThermalMitigation (VOID)
{
  EFI_STATUS Status;
  EFI_TSENS_PROTOCOL *Tsens = NULL;
  INT32 nTempDeciDegC, nTempDegC;
  INT32 nHighTemp, nLowTemp;
  UINT32 uSleepTimeus;

  Status = GetConfigValue("TsensHighTemp", ((UINT32*)&nHighTemp));
  if (Status != EFI_SUCCESS)
    return Status;

  Status = GetConfigValue("TsensLowTemp", ((UINT32*)&nLowTemp));
  if (Status != EFI_SUCCESS)
    nLowTemp = nHighTemp;

  Status = GetConfigValue("TsensWaitTimeus", &uSleepTimeus);
  if (Status != EFI_SUCCESS)
    return Status;

  Status = gBS->LocateProtocol(&gEfiTsensProtocolGuid,
                               NULL,
                               (VOID**)&Tsens);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR,"TSENS Protocol Locate Failed"));
    return Status;
  }

  /* Loop if current temperature of device is greater than max */
  Status = Tsens->GetMaxTemp(&nTempDeciDegC);
  if (EFI_SUCCESS != Status)
  {
     return Status;
  }

  nTempDegC = nTempDeciDegC / 10;

  if (nTempDegC >= nHighTemp)
  {
    DEBUG((EFI_D_ERROR,"\n\nALERT: TSENS temp triggered mitigation\nCurrent Temp = %d, Max = %d, Min = %d\n", nTempDegC, nHighTemp, nLowTemp));

    /* Display image here on screen for user info */

    /* Spin here until the temp comes below set lower threshold */
    while (nTempDegC >= nLowTemp)
    {
      gBS->Stall(uSleepTimeus);
      Status = Tsens->GetMaxTemp(&nTempDeciDegC);
      if (EFI_SUCCESS != Status)
      {
         return Status;
      }

      nTempDegC = nTempDeciDegC / 10;
    }
    DEBUG((EFI_D_ERROR,"Continuing, Current Temp = %d\n\n", nTempDegC));
  }

  return Status;
}

/**
  Platform Bds init. Include the platform firmware vendor, revision
  and so crc check.

**/
VOID
PlatformBdsInit(VOID)
{
  BDS_INIT_OPTION InitOption = OptionNone;
  PlatformBdsInitEx(&InitOption);
}

/** 
  Platform Bds init with init options passed for removable boot detect 
**/
VOID
EFIAPI
PlatformBdsInitEx (BDS_INIT_OPTION *InitOption)
{
  UINT32                         FwVer;
  EFI_STATUS                     Status;
  UINTN                          DataSize;
  UINTN                          DisableWP = PcdGetBool (DisableWriteProtect);
  EFI_QCOM_TOOLSUPPORT_PROTOCOL *ToolSupportProtocol = NULL;
  EFI_ENCRYPTION_PROTOCOL       *Encryption = NULL;

  DEBUG ((DEBUG_ERROR, "-----------------------------\nPlatform Init Start : %d\n",
                      GetTimerCountms ()));

  CheckThermalMitigation ();

  //PrintBootOrder();

  /* If the build system didn't enforce, then get the value from variable */
  if (DisableWP == FALSE)
  {
    UINTN VarSize = sizeof(DisableWP);
    Status = gRT->GetVariable(L"DisableWriteProtect", &gQcomTokenSpaceGuid, NULL, &VarSize, &DisableWP);

    if (Status != EFI_SUCCESS)
      DisableWP = 0;
  }

  /* Now check what's the final value */
  if (DisableWP)
    EnableWriteProtectFlag = 0;
  else
    EnableWriteProtectFlag = 1;

  /* Look up Variable Services protocol to be used to flush Variable tables
   * during Exit Bootservices. Cache the protocol handle to use just in case
   * if somebody else installs the protocol to spoof and hijack.
   * Its Ok for this call to fail if the protocol is not installed. */
  Status = gBS->LocateProtocol(&gEfiVariableServicesProtocolGuid,
                               NULL,
                               (VOID**) &VariableServicesProtocol);
  if (Status != EFI_SUCCESS)
  {
    DEBUG(( EFI_D_ERROR, "WARNING: Updates to UEFI variables will not persist\r\n"));
  }
  /* Look up GLink protocol to be used to tear down connections gracefully
   * during Exit Bootservices.
   * Its Ok for this call to fail if the protocol is not installed. */
  Status = gBS->LocateProtocol(&gEfiGLINKProtocolGuid, NULL, (void**)&GlinkProtocol);

  /* Look up NPA protocol to be used to deinit NPA framework gracefully
   * during Exit Bootservices.
   * Its Ok for this call to fail if the protocol is not installed. */
  Status = gBS->LocateProtocol(&gEfiNpaProtocolGuid, NULL, (void**)&NpaProtocol);

  /* Check if UEFI NV tables are volatile */
  DataSize = sizeof(VolatileTables);
  Status = gRT->GetVariable (L"VolatileTables",
                             &gQcomTokenSpaceGuid,
                             NULL,
                             &DataSize,
                             &VolatileTables);
  if ((Status == EFI_SUCCESS) && (VolatileTables != 0) &&
      (PRODMODE_ENABLED || JTAGDebugDisableFusesBlown()))
  {
    /* In production mode, Shutdown if tables are volatile;
       this indicates an error in reading UEFI NV tables */
    DEBUG(( EFI_D_ERROR, "ERROR: UEFI NV Variables not properly initialized. Shutting down.\r\n"));
    gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL);

    /* Should not reach here */
    CpuDeadLoop();
  }
  else if (VolatileTables != 0)
  {
    DEBUG(( EFI_D_WARN, "WARNING: UEFI NV tables are enabled as volatile.\r\n"));

    /* If volatile tables are initialized for NV tables,
       check if manual flag is set to enable UEFI NV tables as volatile */
    DataSize = sizeof(EnableVolatileBootOptions);
    Status = gRT->GetVariable (L"EnableVolatileBootOptions",
                               &gQcomTokenSpaceGuid,
                               NULL,
                               &DataSize,
                               &EnableVolatileBootOptions);
  }


  Status = gBS->LocateProtocol(&gEfiCapsuleProtocolGuid,
                               NULL,
                               (VOID**) &CapsuleProtocol);
  //ASSERT_EFI_ERROR (Status);

 Status = gBS->LocateProtocol(&gQcomPcieInitProtocolGuid,
                               NULL,
                               (VOID**) &PcieInitProtocol);

  Status = gBS->LocateProtocol(&gEfiSdccConfigProtocolGuid,
                               NULL,
                               (VOID**) &SdccConfigProtocol);

  //Register to Exit Boot Service Event
  Status = gBS->CreateEventEx ( EVT_NOTIFY_SIGNAL,
                                TPL_NOTIFY,
                                ExitBootServicesCallBack,
                                NULL,
                                &gEfiEventExitBootServicesGuid,
                                &EfiExitBootServicesEvent);
  ASSERT_EFI_ERROR (Status);

  // BdsLibConnectAllDriversToAllControllers ();
  BdsLibConnectAllConsoles ();

  /* Initiate Display control during charging */
  PrepareChargingDisplayControl ();

  UpdateNVVars();

  InitLcdDebugFlag();

  DisplayVersion();
  DisplayPlatformInfo();

  /************************************************************************************
   * WARNING:   START
   *
   * NOTE: Security Alert..!!
   *
   * This part of the code at this point (until the function call SetupPlatformSecurity)
   *   is running when UEFI security is not enabled yet. So in production image any
   *   menu or shell running will be a security hole. So do NOT enable this following
   *   code context in Production image.
   */
  if ((PRODMODE_DISABLED) && (!JTAGDebugDisableFusesBlown()))
  {
    /* Detect hotkey for development purposes
     * If enabled in PROD image this would be a SECURITY HOLE
     * So do not enable this */
    PlatformBdsDetectHotKey ();
  }

  /* This is the place where UEFI Security is enabled
   *   This include Image Authentication so any image run before this call will NOT
   *   be authenticated. So any facility to launch any app before this point will be
   *   a security hole gateway. */
  SetupPlatformSecurity ();

  /*
   * WARNING:   END
   *
   *********************************************************************************/
  
  PlatformBdsDetectBootHotKey (InitOption);
  //It is necessary to run SetupPlatformSecurity() before RunFwUpdateMain() and RunFwProvisionMain()
  //so that payload would be authenticated. It is also necessary to run RunFwUpdateMain() before
  //ACPI loading so that FwUpdate is more robust.
  DEBUG(( EFI_D_INFO, "Attempting to start: Firmware update\n"));
  Status = RunFwUpdateMain(gImageHandle, gST);
  if (Status != EFI_SUCCESS)
  {
    DEBUG(( EFI_D_WARN, "Firmware update failed\n"));
  }

  // Signal ACPI Table Loading
  Status = gBS->CreateEventEx(EVT_NOTIFY_SIGNAL,
                              TPL_CALLBACK,
                              ACPITableLoadEmptyFuntion,
                              NULL,
                              &gEfiACPITableLoadGuid,
                              &ACPITableLoadEvent);
  ASSERT_EFI_ERROR (Status);

  Status = gBS->SignalEvent(ACPITableLoadEvent);
  ASSERT_EFI_ERROR (Status);

  //ACPI tables should be loaded at this point.
  //It is necessary to run RunFwProvisionMain() after
  //ACPI loading so that provisioning routine can access
  //RSDP pointer in EFI configuration table and save it
  //for HLOS app to consume.
  DEBUG(( EFI_D_INFO, "Attempting to start: Firmware provisioning\n"));
  Status = RunFwProvisionMain(gImageHandle, gST);
  if (Status != EFI_SUCCESS)
  {
    DEBUG(( EFI_D_WARN, "Firmware provisioning failed\n"));
  }

  // Locate EFI_ENCRYPTION_PROTOCOL.
  Status = gBS->LocateProtocol(&gEfiEncryptionProtocolGuid,
                                NULL,
                               (VOID**)&Encryption);
  if ( EFI_ERROR(Status))
  {
    Encryption = NULL;
  }

  // call SetHdcp to set hdcp provisioning data
  if( Encryption != NULL)
  {
    Status = Encryption->SetHdcp();
    if ( EFI_ERROR(Status))
    {
      DEBUG(( EFI_D_INFO, "Set Hdcp Data failed, Status =  (0x%x)\r\n", Status));
    }
  }

  // Locate EFI_TOOL_SUPPORT_PROTOCOL.
  Status = gBS->LocateProtocol(&gQcomToolSupportProtocolGuid,
                                NULL,
                               (VOID**)&ToolSupportProtocol);
  if ( EFI_ERROR(Status))
  {
    ToolSupportProtocol = NULL;
  }

  // Security calls
  if( ToolSupportProtocol != NULL )
  {
    if(PRODMODE_ENABLED || JTAGDebugDisableFusesBlown())
    {
      ToolSupportProtocol->HandleMorPpi(ToolSupportProtocol);
    }

    if((ToolSupportProtocol->TzFuseMilestone(ToolSupportProtocol) != EFI_SUCCESS ) ||
       (ToolSupportProtocol->NeedQcomPkProtection(ToolSupportProtocol) == TRUE     ))
    { // reboot device
      // Disabling for pre-sil
      //gRT->ResetSystem (EfiResetShutdown, EFI_SUCCESS, 0, NULL);
      /* Should not reach here */
      //CpuDeadLoop();
    }
  }

  // ACPI tables should be loaded before and SetMeasureBootStartSignal()
  // and SetMeasureBootRegSignal()
  // start measuring
  SetMeasureBootStartSignal();

  // register measure boot
  SetMeasureBootRegSignal();

  DataSize = sizeof (FwVer);
  Status = gRT->GetVariable (L"FwVerHex", &gQcomTokenSpaceGuid,
                             NULL, &DataSize, &FwVer);
  if (!EFI_ERROR (Status))
  {
    gST->FirmwareRevision = FwVer;
  }

  // Fix System Table CRC after updating Firmware Revision
  gST->Hdr.CRC32 = 0;
  gBS->CalculateCrc32 ((VOID *)gST, sizeof(EFI_SYSTEM_TABLE), &gST->Hdr.CRC32);

  Status = gBS->LocateProtocol(&gQcomScmProtocolGuid,
                                  NULL,
                                 (VOID**)&pSCMProtocol);
  if (EFI_ERROR(Status))
  {
     DEBUG(( EFI_D_ERROR, "Register SCM protocol failed, Status =  (0x%x)\r\n", Status));
  }

  DEBUG ((DEBUG_ERROR, "Platform Init End : %d\n-----------------------------\n",
                      GetTimerCountms ()));

  /* Give back first MB (except for 1st page) to HLOS -
     still want to catch NULL Pointer exceptions */
  //RestoreReservedMemory(DBI_DUMP_END_ADDRESS, (ADDR_1MB-DBI_DUMP_END_ADDRESS));
  SerialPortDrain();
}

/**
  Connect the predefined platform default console device. Always try to find
  and enable the vga device if have.

  @param PlatformConsole          Predefined platform default console device array.

  @retval EFI_SUCCESS             Success connect at least one ConIn and ConOut
                                  device, there must have one ConOut device is
                                  active vga device.
  @return Return the status of BdsLibConnectAllDefaultConsoles ()

**/
EFI_STATUS
PlatformBdsConnectConsole (
  IN BDS_CONSOLE_CONNECT_ENTRY   *PlatformConsole
  )
{
  return EFI_SUCCESS;
}

/**
  Connect with predefined platform connect sequence,
  the OEM/IBV can customize with their own connect sequence.
**/
VOID
PlatformBdsConnectSequence (
  VOID
  )
{
  //
  // Just use the simple policy to connect all devices
  //
  BdsLibConnectAll ();
}

/**
  Load the predefined driver option, OEM/IBV can customize this
  to load their own drivers

  @param BdsDriverLists  - The header of the driver option link list.

**/
VOID
PlatformBdsGetDriverOption (
  IN OUT LIST_ENTRY              *BdsDriverLists
  )
{
}

/**
  Perform the platform diagnostic, such like test memory. OEM/IBV also
  can customize this function to support specific platform diagnostic.

  @param MemoryTestLevel  The memory test intensive level
  @param QuietBoot        Indicate if need to enable the quiet boot
  @param BaseMemoryTest   A pointer to BdsMemoryTest()

**/
VOID
PlatformBdsDiagnostics (
  IN EXTENDMEM_COVERAGE_LEVEL    MemoryTestLevel,
  IN BOOLEAN                     QuietBoot,
  IN BASEM_MEMORY_TEST           BaseMemoryTest
  )
{
}

/**
  The function will execute with as the platform policy, current policy
  is driven by boot mode. IBV/OEM can customize this code for their specific
  policy action.

  @param  DriverOptionList        The header of the driver option link list
  @param  BootOptionList          The header of the boot option link list
  @param  ProcessCapsules         A pointer to ProcessCapsules()
  @param  BaseMemoryTest          A pointer to BaseMemoryTest()

**/
VOID
EFIAPI
PlatformBdsPolicyBehavior (
  IN LIST_ENTRY                      *DriverOptionList,
  IN LIST_ENTRY                      *BootOptionList,
  IN PROCESS_CAPSULES                ProcessCapsules,
  IN BASEM_MEMORY_TEST               BaseMemoryTest
  )
{
  LIST_ENTRY        *Link;

  if (PRODMODE_ENABLED || JTAGDebugDisableFusesBlown()) {
    /* Drain serial buffer in production mode */
    SerialPortDrain();
  }
  else
  {
    SerialPortDrain();
    EnableSynchronousSerialPortIO ();
  }

  // Ensure one non removable option exists in BootOrder if available
  BdsLibEnumerateValidOptions();

  //PrintBootOrder();

  //
  // Check BootOrder to see if we need to attempt enumerating BootOrder options
  // Parse the boot order to get boot option
  //
  BdsLibBuildOptionFromVar (BootOptionList, L"BootOrder");
  Link = BootOptionList->ForwardLink;

  //
  // Parameter check, make sure the loop will be valid
  //
  if (Link == NULL) {
    return ;
  }
  
  //
  // If BootLists is empty, try to enumerate all boot options
  //
  else if (Link == BootOptionList) {
    BdsLibEnumerateAllBootOption (BootOptionList);
  }
}

/**
  Start an EFI image (PE32+ with EFI defined entry point).

  Argv[0] - device name and path
  Argv[1] - "" string to pass into image being started

  fs1:\Temp\Fv.Fv "arg to pass" ; load an FV from the disk and pass the
                                ; ascii string arg to pass to the image
  fv0:\FV                       ; load an FV from an FV (not common)
  LoadFile0:                    ; load an FV via a PXE boot

  @param  Argc   Number of command arguments in Argv
  @param  Argv   Array of strings that represent the parsed command line.
                 Argv[0] is the App to launch

  @return EFI_SUCCESS

**/
EFI_STATUS
BdsStartCmd (
  IN UINTN  Argc,
  IN CHAR8  **Argv
  )
{
  EFI_STATUS                  Status;
  EFI_OPEN_FILE               *File;
  EFI_DEVICE_PATH_PROTOCOL    *DevicePath;
  EFI_HANDLE                  ImageHandle;
  UINTN                       ExitDataSize;
  CHAR16                      *ExitData;
  VOID                        *Buffer;
  UINTN                       BufferSize;
  EFI_LOADED_IMAGE_PROTOCOL   *ImageInfo;

  ImageHandle = NULL;

  if (Argc < 1)
    return EFI_INVALID_PARAMETER;

  File = EfiOpen (Argv[0], EFI_FILE_MODE_READ, 0);
  if (File == NULL)
    return EFI_INVALID_PARAMETER;

  DevicePath = File->DevicePath;
  if (DevicePath != NULL) {
    // check for device path form: blk, fv, fs, and loadfile
    Status = gBS->LoadImage (FALSE, gImageHandle, DevicePath, NULL, 0, &ImageHandle);
  } else {
    // Check for buffer form: A0x12345678:0x1234 syntax.
    // Means load using buffer starting at 0x12345678 of size 0x1234.

    Status = EfiReadAllocatePool (File, &Buffer, &BufferSize);
    if (EFI_ERROR (Status)) {
      EfiClose (File);
      return Status;
    }

    if (Buffer == NULL)
      return EFI_OUT_OF_RESOURCES;

    Status = gBS->LoadImage (FALSE, gImageHandle, DevicePath, Buffer, BufferSize, &ImageHandle);

    if (Buffer != NULL)
      FreePool (Buffer);
  }

  EfiClose (File);

  if (!EFI_ERROR (Status)) {
    if (Argc >= 2) {
      // Argv[1] onwards are strings that we pass directly to the EFI application
      // We don't pass Argv[0] to the EFI Application, just the args
      Status = gBS->HandleProtocol (ImageHandle, &gEfiLoadedImageProtocolGuid, (VOID **)&ImageInfo);
      ASSERT_EFI_ERROR (Status);

      if (ImageInfo == NULL)
        return EFI_NOT_FOUND;

      /* Need WideChar string as CmdLineArgs */
      ImageInfo->LoadOptionsSize = 2 * (UINT32)AsciiStrSize (Argv[1]);
      ImageInfo->LoadOptions     = AllocatePool (ImageInfo->LoadOptionsSize);
      if (ImageInfo->LoadOptions == NULL)
        return EFI_OUT_OF_RESOURCES;
      AsciiStrToUnicodeStr (Argv[1], ImageInfo->LoadOptions);
    }

    // Transfer control to the EFI image we loaded with LoadImage()
    Status = gBS->StartImage (ImageHandle, &ExitDataSize, &ExitData);
  }

  return Status;
}

/**
  Hook point after a boot attempt succeeds. We don't expect a
  boot option to return, so the UEFI 2.0 specification defines
  that you will default to an interactive mode and stop
  processing the BootOrder list in this case. This is also a
  platform implementation and can be customized by IBV/OEM.

  @param  Option                  Pointer to Boot Option that succeeded to boot.

**/
VOID
EFIAPI
PlatformBdsBootSuccess (
  IN  BDS_COMMON_OPTION *Option
  )
{
  // measure this string.
  SetMeasureReturnFromEfiAppFromBootOptionSignal();

  DEBUG(( EFI_D_WARN, "Successfully booted %S\n", Option->Description));
}

/**
  Hook point after a boot attempt fails.

  @param  Option                  Pointer to Boot Option that failed to boot.
  @param  Status                  Status returned from failed boot.
  @param  ExitData                Exit data returned from failed boot.
  @param  ExitDataSize            Exit data size returned from failed boot.

**/
VOID
EFIAPI
PlatformBdsBootFail (
  IN  BDS_COMMON_OPTION  *Option,
  IN  EFI_STATUS         Status,
  IN  CHAR16             *ExitData,
  IN  UINTN              ExitDataSize
  )
{
  // measure this string.
  SetMeasureReturnFromEfiAppFromBootOptionSignal();

  DEBUG(( EFI_D_ERROR, "Failed to boot %S\n Status = 0x%08x\n", Option->Description, Status));
}

/**
  This function is remained for IBV/OEM to do some platform action,
  if there no console device can be connected.

  @return EFI_SUCCESS      Direct return success now.

**/
EFI_STATUS
PlatformBdsNoConsoleAction (
  VOID
  )
{
  return EFI_SUCCESS;
}

/**
  This function locks platform flash that is not allowed to be updated during normal boot path.
  The flash layout is platform specific.
**/
VOID
EFIAPI
PlatformBdsLockNonUpdatableFlash (
  VOID
  )
{
  return;
}


/**
  Lock the ConsoleIn device in system table. All key
  presses will be ignored until the Password is typed in. The only way to
  disable the password is to type it in to a ConIn device.

  @param  Password        Password used to lock ConIn device.

  @retval EFI_SUCCESS     lock the Console In Spliter virtual handle successfully.
  @retval EFI_UNSUPPORTED Password not found

**/
EFI_STATUS
EFIAPI
LockKeyboards (
  IN  CHAR16    *Password
  )
{
  return EFI_UNSUPPORTED;
}

/**
  Hook point to boot from eMMC, if booting from BootOrder
  failed. We don't expect a boot option to return, so the UEFI
  2.0 specification defines that you will default to an
  interactive mode and stop processing the BootOrder list in
  this case. This is also a platform implementation and can be
  customized by IBV/OEM.

  @param  Option                  Pointer to Boot Option that succeeded to boot.

**/
VOID
EFIAPI
PlatformBdsLoadShellForNonProdMode (
  IN  BDS_COMMON_OPTION *Option
  )
{
  /* End Perf marker */
  PERF_END   (NULL, "BDS", NULL, 0);

  SerialPortDrain();
  EnableSynchronousSerialPortIO ();
  
  // Drop to UEFI Shell
  CHAR8* arg_ebl[] = {"fv1:Ebl"};
  CHAR8* argv[] = {"fv2:Shell", "-nomap -nostartup"};

  DisplayPOSTTime();

  //PrintBootOrder();

  DEBUG(( EFI_D_WARN, "Attempting to start: %a\n", arg_ebl[0]));
  BdsStartCmd (sizeof(arg_ebl)/sizeof(*arg_ebl), arg_ebl);

  DEBUG(( EFI_D_WARN, "Attempting to start: %a\n", argv[0]));
  BdsStartCmd (sizeof(argv)/sizeof(*argv), argv);
}

/**
  Function that executes just before loading a boot option image.

  @param  Option                  Pointer to Boot Option that succeeded to boot.

**/
EFI_STATUS
EFIAPI
PlatformBdsPreLoadBootOption (
  IN  BDS_COMMON_OPTION *Option
  )
{
  BOOLEAN ProdModeEnabled = (PRODMODE_ENABLED || JTAGDebugDisableFusesBlown());
  /* Do not allow boot option to boot if in production scenario */
  if ((VolatileTables != 0) && (ProdModeEnabled || (!EnableVolatileBootOptions)))
    return EFI_DEVICE_ERROR;

  if (Option == NULL)
    return EFI_INVALID_PARAMETER;

  if (Option->Description == NULL)
    DEBUG ((DEBUG_INFO | DEBUG_LOAD | DEBUG_ERROR, "Booting option %d:(Boot%04x) from unknown device path\n", Option->OptionNumber, Option->BootCurrent));
  else
    DEBUG ((DEBUG_INFO | DEBUG_LOAD | DEBUG_ERROR, "Booting option %d:(Boot%04x) \"%S\"\n", Option->OptionNumber, Option->BootCurrent, Option->Description));

  DisplayPOSTTime();

  //PrintBootOrder();

  return EFI_SUCCESS;
}

/**
  This Function writes UEFI Log Buffer to LOGFS Partition

  @retval EFI_SUCCESS     File was written successfully
**/
#define MAX_LEN 13
#define FILE_EXT L".txt"
EFI_STATUS
EFIAPI
WriteLogBufToPartition (VOID)
{
  EFI_STATUS Status;
  UINT32 VarData;
  CHAR16 LogBufFile[MAX_LEN] = L"UefiLog";
  UefiInfoBlkType* UefiInfoBlockPtr = NULL;
  
  UefiInfoBlockPtr = (UefiInfoBlkType*)GetInfoBlkPtr();
  
  /* Only keep Logs of past 5 Boot Cycles 
     File number corresponds to current BootCycle (0-4) */
  VarData = BootCycleCount % 5;
  VarData = VarData + '0';  /* Convert int to char */
  /* Construct File name */
  StrCat(LogBufFile, (CHAR16*)&VarData);
  StrCat(LogBufFile, FILE_EXT);
  
  /* Write Logs to Partition */
  Status = WriteFile (LogBufFile,
                      NULL,
                      &gEfiLogFSPartitionGuid,
                      TRUE,
                      NULL,
                      &UefiInfoBlockPtr->UartLogBufferLen,
                      0,
                      (UINT8*)UefiInfoBlockPtr->UartLogBufferPtr,
                      UefiInfoBlockPtr->UartLogBufferLen);  
  return Status; 
}
