/***********************************************************************
 * access_control.h
 *
 * Placeholder for real ACLib header file.
 *
 * Copyright (C) 2015 Qualcomm Technologies, Inc.
 *
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when         who   what, where, why
----------   ---   ---------------------------------------------------------
03/25/15     ck    Initial creation

===========================================================================*/

#ifndef __ACCESS_CONTROL_H__
#define __ACCESS_CONTROL_H__

boolean ACXblInit(void);

#endif /* __ACCESS_CONTROL__ */
