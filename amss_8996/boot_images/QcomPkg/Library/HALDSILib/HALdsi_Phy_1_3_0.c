/*=============================================================================

  File: HALdsi_Phy.c
  

  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/

/*============================================================================
*                         INCLUDE FILES
============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif 

#include "HALdsi.h"
#include "HALdsi_Phy.h"
#include "HALdsi_Pll.h"
#include "dsiHostSystem.h"
#include "HALdsi_phy_1_3_0.h"

/* -----------------------------------------------------------------------
** Local defines
** ----------------------------------------------------------------------- */

/* timeout counter values used in iterations of polling PLL ready status; 
 * each polling iteration contains a delay of 100us;                       */
#define HAL_DSI_PHY_PLL_READY_TIMEOUT           10           /* ~1 ms */

/* Generic parameters that applies to all parameter calculations */
#define HAL_DSI_PHY_GENERIC_SHIFT_OFFSET        0x02
#define HAL_DSI_PHY_GENERIC_TIMING_MAX          0xFF
#define HAL_DSI_PHY_TIPX_NUMERATOR              1000000000.0f                    /* numerator for the TIPX formula */
#define HAL_DSI_PHY_PERCENT_DENOMENATOR         100.0f
#define HAL_DSI_PHY_TREOT                       20                               /* t_reot */
 
/* Range factor applied */
#define HAL_DSI_PHY_RANGEFACTOR_1               1.0f
#define HAL_DSI_PHY_RANGEFACTOR_5               5.0f
#define HAL_DSI_PHY_RANGEFACTOR_10              10.0f
#define HAL_DSI_PHY_RANGEFACTOR_15              15.0f
#define HAL_DSI_PHY_RANGEFACTOR_40              40.0f
#define HAL_DSI_PHY_RANGEFACTOR_50              50.0f
#define HAL_DSI_PHY_RANGEFACTOR_90              90.0f
#define HAL_DSI_PHY_BITCLK_RANGE_FREQ_1         180000000                        /* bit clk frequency in the unit of Hz */
#define HAL_DSI_PHY_BITCLK_RANGE_FREQ_2         1200000000                       /* bit clk frequency in the unit of Hz */


/* Recommended values for TCLK_PREPARE formula */
#define HAL_DSI_PHY_TCLK_PREPARE_PHY_MIN        38.0f
#define HAL_DSI_PHY_TCLK_PREPARE_PHY_MAX        95.0f

/* Recommended Values for TCLK_ZERO formula */
#define HAL_DSI_PHY_TCLK_ZERO_PARAM1            300.0f

/* Recommended Values for TCLK_TRAIL formula */
#define HAL_DSI_PHY_TCLK_TRAIL_MIN              60.0f

/* Recommended Values for T_HS_EXIT formula */
#define HAL_DSI_PHY_T_HS_EXIT_MIN               100.0f

/* Recommended Values for T_HS_ZERO formula */
#define HAL_DSI_PHY_T_HS_ZERO_PARAM1            0x91
#define HAL_DSI_PHY_T_HS_ZERO_PARAM2            0x0A
#define HAL_DSI_PHY_T_HS_ZERO_PARAM3            0x18

/* Recommended Values for T_HS_PREPARE formula */
#define HAL_DSI_PHY_T_HS_PREPARE_PARAM1         0x28
#define HAL_DSI_PHY_T_HS_PREPARE_PARAM2         0x04
#define HAL_DSI_PHY_T_HS_PREPARE_PARAM3         0x55
#define HAL_DSI_PHY_T_HS_PREPARE_PARAM4         0x06

/* Recommended Values for T_HS_TRAIL formula */
#define HAL_DSI_PHY_T_HS_TRAIL_PARAM1           0x3C
#define HAL_DSI_PHY_T_HS_TRAIL_PARAM2           0x04

/* Recommended Values for T_HS_RQST formula */
#define HAL_DSI_PHY_T_HS_RQST_PARAM1            0x19

/* Recommended Values for T_TA_GO formula */
#define HAL_DSI_PHY_T_TA_GO_PARAM1              0x03

/* Recommended Values for T_TA_SURE formula */
#define HAL_DSI_PHY_T_TA_SURE_PARAM1            0x00

/* Recommended Values for T_TA_GET formula */
#define HAL_DSI_PHY_T_TA_GET_PARAM1             0x04

/* Recommended Values for TEOT formula */
#define HAL_DSI_PHY_TEOT_PARAM1                 0x69
#define HAL_DSI_PHY_TEOT_PARAM2                 0x0C

/* Recommended Values for T_CLK_PRE formula */
#define HAL_DSI_PHY_T_CLK_PRE_PARAM1            0x08
#define HAL_DSI_PHY_T_CLK_PRE_PARAM2            0x08
#define HAL_DSI_PHY_T_CLK_PRE_PARAM3            0x01
#define HAL_DSI_PHY_T_CLK_PRE_MAX               0x3F

/* Recommended Values for T_CLK_POST formula */
#define HAL_DSI_PHY_T_CLK_POST_PARAM1           0x3C
#define HAL_DSI_PHY_T_CLK_POST_PARAM2           0x34
#define HAL_DSI_PHY_T_CLK_POST_PARAM3           0x18
#define HAL_DSI_PHY_T_CLK_POST_PARAM4           0x08
#define HAL_DSI_PHY_T_CLK_POST_PARAM5           0x01
#define HAL_DSI_PHY_T_CLK_POST_MAX              0x3F


#define HAL_DSI_PHY_TIMING_CTRL_11              0xA0

#define HAL_DSI_PHY_SETPHYCTRL0_STEP1           1
#define HAL_DSI_PHY_SETPHYCTRL0_STEP2           2

#define HAL_DSI_PHY_PLLSEQUENCE_MAXTRY          3
#define HAL_DSI_PHY_PLL_LOCK_MASK               0x00000060
#define HAL_DSI_PRE_DIVIDER                     2.0f

#define HAL_DSI_PHY_PLL_LOCKRNG                 1
#define HAL_DSI_PHY_PLL_LOCKCNT                 2

#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_EN_BMSK                               0x80
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_EN_SHFT                                0x7
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_BYPASS_LPDIV_BMSK                     0x20
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_BYPASS_LPDIV_SHFT                      0x5
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_N_DIV_BMSK                            0x1F
#define HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_N_DIV_SHFT                             0x0

/* -----------------------------------------------------------------------
** Local Data Types
** ----------------------------------------------------------------------- */

/*!
 * \struct _HAL_DSI_PllSettingType
 *
 * PLL parameters
 */
typedef struct _HAL_DSI_PllSettingType
{
   uint32   uActBitClkFreq;          /* actual bit clock frequency generated in Hz */
   uint32   uVcoClkFreq;             /* VCO output frequency in Hz */
   
   uint8    uPllRefClkDiv;           /* DSI_x_PHY_PLL_UNIPHY_PLL_REFCLK_CFG.PLL_REFCLK_DIV */
   uint8    uPllRefClkDblr;          /* DSI_x_PHY_PLL_UNIPHY_PLL_REFCLK_CFG.PLL_REFCLK_DBLR */
   uint8    uPllSdmByp;              /* DSI_x_PHY_PLL_UNIPHY_PLL_SDM_CFG0.PLL_SDM_BYP */
   uint8    uPllSdmBypDiv;           /* DSI_x_PHY_PLL_UNIPHY_PLL_SDM_CFG0.PLL_SDM_BYP_DIV */
   uint8    uPllSdmDcOffset;         /* DSI_x_PHY_PLL_UNIPHY_PLL_SDM_CFG1.PLL_SDM_DC_OFFSET */
   uint8    uPllSdmFreqSeed_7_0;     /* DSI_x_PHY_PLL_UNIPHY_PLL_SDM_CFG2.PLL_SDM_FREQ_SEED_7_0 */
   uint8    uPllSdmFreqSeed_15_8;    /* DSI_x_PHY_PLL_UNIPHY_PLL_SDM_CFG3.PLL_SDM_FREQ_SEED_15_8 */
   
   uint8    uPllPostDiv1;            /* DSI_x_PHY_PLL_UNIPHY_PLL_POSTDIV1_CFG.PLL_POSTDIV1 */
   uint8    uPllPostDiv1BypassB;     /* DSI_x_PHY_PLL_UNIPHY_PLL__VREG_CFG.PLL_POSTDIV1_BYPASS_B */
   uint8    uPllPostDiv2;            /* DSI_x_PHY_PLL_UNIPHY_PLL_POSTDIV2_CFG.PLL_POSTDIV2 */
   uint8    uPllPostDiv3;            /* DSI_x_PHY_PLL_UNIPHY_PLL_POSTDIV3_CFG.PLL_POSTDIV3 */
   
   uint8    uPllCpIdac;              /* DSI_x_PHY_PLL_UNIPHY_PLL_CHGPUMP_CFG.PLL_CP_IDAC */
   uint8    uPllLpfR;                /* DSI_x_PHY_PLL_UNIPHY_PLL_LPFR_CFG.PLL_LPF_R */   
   uint8    uPllLpfC1;               /* DSI_x_PHY_PLL_UNIPHY_PLL_LPFC1_CFG.PLL_LPF_C1 */
   uint8    uPllLpfC2;               /* DSI_x_PHY_PLL_UNIPHY_PLL_LPFC2_CFG.PLL_LPF_C2 */
   uint8    uPllC3Sel;               /* DSI_x_PHY_PLL_UNIPHY_PLL_VCOLPF_CFG.PLL_C3_SEL */
   uint8    uPllBypassP3;            /* DSI_x_PHY_PLL_UNIPHY_PLL_VCOLPF_CFG.PLL_BYPASS_P3 */
} HAL_DSI_PllSettingType;




/* -----------------------------------------------------------------------
** Local functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_PllLockDetect()
*/
/*!
* \brief
*     Wait and check if PLL locked.
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_DSI_Phy_20nm_1_0_PllLockDetect(DSI_Device_IDType    eDeviceId)
{
  HAL_MDSS_ErrorType   eStatus   = HAL_MDSS_STATUS_SUCCESS;
  uintPtr              uOffset   = HAL_DSI_GetRegBaseOffset(eDeviceId);
  uint32               uTimeout  = HAL_DSI_PHY_PLL_READY_TIMEOUT;
  uint32               uPllStatus;

  uPllStatus = in_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_ADDR) & HAL_DSI_PHY_PLL_LOCK_MASK;
  while ((0 == uPllStatus) && (uTimeout))
  {
    DSI_OSAL_SleepUs(100);  /*delay ~100us*/
    uPllStatus = in_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESET_SM_ADDR) & HAL_DSI_PHY_PLL_LOCK_MASK;
    uTimeout--;
  }

  if (0 == (uPllStatus & HAL_DSI_PHY_PLL_LOCK_MASK))
  {
    /* timeout while polling the lock status */
    eStatus = HAL_MDSS_DSI_FAILED_UNABLE_TO_INIT_HW;
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_SetAnaLaneConfig()
*/
/*!
* \brief
*     Set analog lane config.
*
* \param [in]  eDeviceId     - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_SetAnaLaneConfig(DSI_Device_IDType    eDeviceId)
{
  uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(0),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(1),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(2),      0x0040);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG3_ADDR(3),      0x0040);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG3_ADDR,        0x0080);

  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(0), 0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(1), 0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(2), 0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR0_ADDR(3), 0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR0_ADDR,   0x0001);

  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(0), 0x0088);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(1), 0x0088);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(2), 0x0088);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNn_TEST_STR1_ADDR(3), 0x0088);
  out_dword(uOffset + HWIO_MMSS_DSI_1_PHY_DSIPHY_LNCK_TEST_STR1_ADDR,   0x0088);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(0),      0x0002);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(1),      0x0002);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(2),      0x0002);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG0_ADDR(3),      0x0002);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG0_ADDR,        0x0000);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(0),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(1),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(2),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG1_ADDR(3),      0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG1_ADDR,        0x0000);

  return;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_Roundup()
*/
/*!
* \brief
*     Rounds up to the nearest integer.
*
*
* \param [in]  fFloatVal - float number to be processed;
*
* \retval  rounded up integer;
*
****************************************************************************/
static int32 HAL_DSI_Phy_20nm_1_0_Roundup(float fFloatVal)
{
  int32  iRoundupVal = (int32)fFloatVal;

  if(fFloatVal - iRoundupVal > 0)
    iRoundupVal ++;
  else if(fFloatVal - iRoundupVal < 0)
    iRoundupVal --;

  return iRoundupVal;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_roundDown()
*/
/*!
* \brief
*     Rounds down to the nearest integer for positive number, 0 if it is negative number.
*
*
* \param [in]  fFloatVal - float number to be processed;
*
* \retval  rounded down integer;
*
****************************************************************************/
uint32 HAL_DSI_Phy_20nm_1_0_roundDown(double pFloatValue)
{
  uint32     roundupValue;

  if (pFloatValue > 0)
  {
    roundupValue = (uint32)pFloatValue;
  }
  else 
  {
    roundupValue = 0;
  }

  return roundupValue;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_SetGlobalTestCtrl()
*/
/*!
* \brief
*     Set  global test control.
*
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_SetGlobalTestCtrl(DSI_Device_IDType    eDeviceId)
{
   uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);
          
   /* global test control */ 
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, 
            HWIO_SET_FLD(DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL, TEST_ESC_CLK_SEL,     0) |
            HWIO_SET_FLD(DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL, TEST_BYTECLK_SEL,     0) |
            HWIO_SET_FLD(DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL, DSIPHY_BITCLK_HS_SEL, 0) );  /* to be tested: 0 or 1 for DSI_1 */

   return;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_SetRegBistOff()
*/
/*!
* \brief
*     Set BIST off
*
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_SetRegBistOff(DSI_Device_IDType    eDeviceId)
{
  uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  /* BIST off */
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL0_ADDR, 0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL1_ADDR, 0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL2_ADDR, 0x00B1);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL3_ADDR, 0x00FF);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR, 0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL5_ADDR, 0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_BIST_CTRL4_ADDR, 0x0000);

  return;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_SetLdoRegulator()
*/
/*!
* \brief
*     
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_SetLdoRegulator(DSI_Device_IDType    eDeviceId, uint32 uVal)   
{
  uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LDO_CNTRL_ADDR,  uVal);
  
  return;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_DisableDcdcRegulator()
*/
/*!
* \brief
*     
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_DisableDcdcRegulator(DSI_Device_IDType    eDeviceId)
{
  uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,       0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR,  0x0000);
  DSI_OSAL_SleepUs(1000);          /* delay ~1ms */
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,       0x0000);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,       0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,       0x0001);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,       0x0020);

  return;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_EnableDcdcRegulator()
*/
/*!
* \brief
*           Enables DCDC Regulator, disables resistor divider   
*
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_EnableDcdcRegulator(DSI_Device_IDType    eDeviceId)
{
   uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,      0x0000);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, 0x0001);
   DSI_OSAL_SleepUs(1000);          /* delay ~1ms */
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CAL_PWR_CFG_ADDR, 0x0001);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_1_ADDR,      0x0002);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_2_ADDR,      0x0003);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_3_ADDR,      0x0000);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_4_ADDR,      0x0010);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_REG_DSIPHY_REGULATOR_CTRL_0_ADDR,      0x0003);

   return;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_ResetPhy()
*/
/*!
* \brief   
*
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval None
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_ResetPhy(DSI_Device_IDType    eDeviceId)
{
    uintPtr uOffset              = HAL_DSI_GetRegBaseOffset(eDeviceId);
    uint32  uVcoTailEnReg        = in_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR);
    uint32  uBiasEnClkBuflrEnReg = in_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR);
    uint32  uResetsmCntrl3Reg    = in_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR);

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_SW_RESET_ADDR, 0x1);
    DSI_OSAL_SleepUs(1000);          /* delay ~1ms */
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_SW_RESET_ADDR, 0x0);

    /* Restore SW Workaround to preserve original registers value to disable the Phy hardware enable pins
       to avoid ~15mA leakage on the Phy PLL. Hardware does not reset the pins to the correct state
       after a core reset. */
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR,      uVcoTailEnReg);
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, uBiasEnClkBuflrEnReg);
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR,      uResetsmCntrl3Reg);

    return;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_powerUpCommon()
*/
/*!
* \brief
*     Programming PLL common registers as recommended.
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval none
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_powerUpCommon(DSI_Device_IDType    eDeviceId)
{
  uintPtr   uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_SYS_CLK_CTRL_ADDR,          0x40);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR,        0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_CMN_MODE_ADDR,              0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_IE_TRIM_ADDR,               0x0F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_IP_TRIM_ADDR,               0x0F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_CONTROL_ADDR,     0x08);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_IPTAT_TRIM_VCCA_TX_SEL_ADDR,0x0E);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_PHSEL_DC_ADDR,          0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_CORE_CLK_IN_SYNC_SEL_ADDR,  0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR,   0x08);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR,   0x3F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL1_ADDR,              0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_ATB_SEL2_ADDR,              0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_SYSCLK_EN_SEL_TXBAND_ADDR,  0x4B);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF1_ADDR,              0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DIV_REF2_ADDR,              0x01);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_KVCO_COUNT1_ADDR,           0x8A);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CAL_CNTRL_ADDR,        0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_KVCO_CODE_ADDR,             0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG1_ADDR,             0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG2_ADDR,             0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG3_ADDR,             0x10);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_VREF_CFG4_ADDR,             0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_BGTC_ADDR,                  0x0F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_TEST_UPDN_ADDR,         0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCO_TUNE_ADDR,          0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_AMP_OS_ADDR,            0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_SSC_EN_CENTER_ADDR,         0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_UP_ADDR,           0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_DN_ADDR,           0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR,      0x77);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_CAL_CSR_ADDR,      0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_EN_VCOCALDONE_ADDR,0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR,               0x00);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_FAUX_EN_ADDR,               0x0C);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_RXTXEPCLK_EN_ADDR,      0x0F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_LOW_POWER_RO_CONTROL_ADDR,  0x0F);
  
  return;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_FindVco()
*/
/*!
* \brief
*     Find Desired VCO clock based on Bit clock.
*
* \param [in]  uDesiredBitClk - desired Bit clock in Hz
*
* \retval uint32
*
****************************************************************************/
static uint32 HAL_DSI_Phy_FindVco(uint32 uDesiredBitClk)
{
  uint32 uDesiredVcoClkInHz = 0;

  if (uDesiredBitClk >= 1000000000)
  {
    uDesiredVcoClkInHz = uDesiredBitClk;
  }
  else if ((uDesiredBitClk >= 500000000) && (uDesiredBitClk < 1000000000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 2;
  }
  else if ((uDesiredBitClk >= 250000000) && (uDesiredBitClk < 500000000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 4;
  }
  else if ((uDesiredBitClk >= 166600000) && (uDesiredBitClk < 250000000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 6;
  }
  else if ((uDesiredBitClk >= 125000000) && (uDesiredBitClk < 166.600000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 8;
  }
  else if ((uDesiredBitClk >= 100000000) && (uDesiredBitClk < 125000000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 10;
  }
  else if ((uDesiredBitClk >= 83400000) && (uDesiredBitClk < 100000000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 12;
  }
  else if ((uDesiredBitClk >= 80000000) && (uDesiredBitClk < 83400000))
  {
    uDesiredVcoClkInHz = uDesiredBitClk * 14;
  }

  return uDesiredVcoClkInHz;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_FindPostDividerControl()
*/
/*!
* \brief
*     Find Post divider value
*
* \param [in]  uDesiredBitClk -Half of desired Bit clock in Hz
*
* \retval uint32
*
****************************************************************************/
static uint32 HAL_DSI_Phy_FindPostDividerControl(uint32 uHalfBitClkInHz)
{
  uint32 nBypassLpDiv = 1;

  if (uHalfBitClkInHz >= 500000000) 
  {
    nBypassLpDiv = 0;
  }

  return nBypassLpDiv;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_FindNdiv()
*/
/*!
* \brief
*     Find N Divder value
*
* \param [in]  uDesiredBitClk -Half of desired Bit clock in Hz
*
* \retval uint32
*
****************************************************************************/
static uint32 HAL_DSI_Phy_FindNdiv(uint32 uHalfBitClkInHz)
{
  uint32 foundNDiv = 0;

  if (uHalfBitClkInHz >= 250000000) 
  {
    foundNDiv = 1;
  }
  else if ((uHalfBitClkInHz >= 125000000) && (uHalfBitClkInHz < 250000000))
  {
    foundNDiv = 2;
  }
  else if ((uHalfBitClkInHz >= 83300000) && (uHalfBitClkInHz < 125000000))
  {
    foundNDiv = 3;
  }
  else if ((uHalfBitClkInHz >= 62500000) && (uHalfBitClkInHz < 83300000))
  {
    foundNDiv = 4;
  }
  else if ((uHalfBitClkInHz >= 50000000) && (uHalfBitClkInHz < 62500000))
  {
    foundNDiv = 5;
  }
  else if ((uHalfBitClkInHz >= 41700000) && (uHalfBitClkInHz < 50000000))
  {
    foundNDiv = 6;
  }
  else if ((uHalfBitClkInHz >= 40000000) && (uHalfBitClkInHz < 41700000))
  {
    foundNDiv = 7;
  }

  return foundNDiv;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_CalcPixelClockDivider()
*/
/*!
* \brief
*     Calculate Pixel Clock divider.
*
* \param [in]  uHalfBitClkInHz      - Half of desired Bit clock
* \param [in]  uNumOfDataLanes      - number of data lanes
* \param [in]  uBitsPerPixel        - BPP of the panel
* \param [out] psDsiPhyConfigInfo   - PHY Config Info
*
* \retval uint32
*
****************************************************************************/
static uint32 HAL_DSI_Phy_CalcPixelClockDivider(uint32 uHalfBitClkInHz, uint32 uNumOfDataLanes, uint32 uBitsPerPixel, HAL_DSI_PhyConfigInfoType *psDsiPhyConfigInfo)
{
  uint32 uM_Val, uN_Val;
  uint32 uN_Div;
  uint32 uHrOclk2 = 4;
  uint32 uHrOclk3 = 0;

  /* init numerator and denominator of divider ratio in CC to 1 */
  uM_Val = 1;
  uN_Val = 1;

  /* determine divider numerator and denominator for exception cases */
  if (uBitsPerPixel == 18)
  {
    switch (uNumOfDataLanes)
    {
    case 1:
      uM_Val = 2;
      uN_Val = 9;
      break;

    case 2:
      uM_Val = 2;
      uN_Val = 9;
      break;

    case 4:
      uM_Val = 4;
      uN_Val = 9;
      break;

    default:
      break;
    }
  }
  else if ((uBitsPerPixel == 16) && (uNumOfDataLanes == 3))
  {
    uM_Val = 3;
    uN_Val = 8;
  }

  if (uHalfBitClkInHz >= 500000000)
  {
    uHrOclk3 = (uint32)((1.0*uM_Val / uN_Val) * uHrOclk2 * (uBitsPerPixel / (8.0*uNumOfDataLanes)));
  }
  else
  {
    uN_Div = HAL_DSI_Phy_FindNdiv(uHalfBitClkInHz);
    uHrOclk3 = (uint32)((1.0*uM_Val / uN_Val) * uHrOclk2 * uN_Div * uBitsPerPixel * 2.0 / (8.0*uNumOfDataLanes));
  }
  /* Save M/N info */
  psDsiPhyConfigInfo->uPClkDivNumerator = uM_Val;
  psDsiPhyConfigInfo->uPClkDivDenominator = uN_Val;

  return uHrOclk3;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_pllDphyHw_pllLoopBw()
*/
/*!
* \brief
*     Programming PLL LoopBW registers as recommended.
*
* \param [in]  eDeviceId - DSI core ID
*
* \retval none
*
****************************************************************************/
static void HAL_DSI_Phy_20nm_1_0_pllDphyHw_pllLoopBw(DSI_Device_IDType    eDeviceId)
{
  uintPtr uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETI_ADDR, 0x03);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETI_ADDR, 0x3F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_IP_SETP_ADDR, 0x03);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_CP_SETP_ADDR, 0x1F);
  out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_CRCTRL_ADDR,  0x77);
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_Setup()
*/
/*!
* \brief
*     Set up DSI Phy alone.
*
* \param [in]  eDeviceId       - DSI core ID
* \param [in]  psDsiPhyConfig  - Phy configuration
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_Phy_20nm_1_0_Setup(DSI_Device_IDType          eDeviceId,
                                              HAL_DSI_PhyConfigType     *psDsiPhyConfig)
{
  HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

  /* validate input parameters */
  if ((eDeviceId != DSI_DeviceID_0) &&
      (eDeviceId != DSI_DeviceID_1))
  {
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else
  {
    uintPtr uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);
    uint32  n;

    HAL_DSI_Phy_20nm_1_0_ResetPhy(eDeviceId);

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_1_ADDR, 0x6);

    if (0 == psDsiPhyConfig->uDataStrengthLP)
    {
      // Default LP strength
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, 0x77);
    }
    else
    {
      uint32 uStrengthVal = psDsiPhyConfig->uDataStrengthLP - 1; // Subtract one to be one based.
      uint32 uStrength = ((uStrengthVal << HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_SHFT) & HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_BMSK) |
        ((uStrengthVal << HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_SHFT) & HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_N_BMSK);

      // Positive first
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, uStrength & HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_DSIPHY_STR_LP_P_BMSK);

      // Positive & Negative     
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_STRENGTH_CTRL_0_ADDR, uStrength);
    }

    for (n = 0; n < HAL_DSI_MAX_DATA_LANE_NUM; n++)
    {
      // Data lanes
      if (0 == psDsiPhyConfig->uDataStrengthHS)
      {
        out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), 0x75);
      }
      else
      {
        uint32 uDataStrengthVal = psDsiPhyConfig->uDataStrengthHS - 1;  // Subtract one to be 0 based

        // Override pull up and pull down strength
        out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_ADDR(n), uDataStrengthVal &
          (HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK | HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK));
      }
    }

    // Clock lane
    if (0 == psDsiPhyConfig->uClockStrengthHS)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, 0x97);
    }
    else
    {
      uint32 uClockStrengthVal = psDsiPhyConfig->uClockStrengthHS - 1;  // Subtract one to be 0 based

      // Override pull up and pull down strength
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_ADDR, uClockStrengthVal &
        (HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSTOP_BMSK | HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_TEST_STR1_DSIPHY_HSTX_STR_HSBOT_BMSK));
    }

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, 0x80); //RESET for Digital blocks except control registers - Assertion

    if (psDsiPhyConfig->bDCDCMode)
    {
      /* enable DCDC regulator */
      HAL_DSI_Phy_20nm_1_0_EnableDcdcRegulator(psDsiPhyConfig->eDeviceId);
      HAL_DSI_Phy_20nm_1_0_SetLdoRegulator(psDsiPhyConfig->eDeviceId, 0x0);
    }
    else
    {
      /* enable LDO regulator */
      HAL_DSI_Phy_20nm_1_0_DisableDcdcRegulator(psDsiPhyConfig->eDeviceId);
      HAL_DSI_Phy_20nm_1_0_SetLdoRegulator(psDsiPhyConfig->eDeviceId, 0x1D);
    }

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, 0x00);

    HAL_DSI_Phy_20nm_1_0_SetGlobalTestCtrl(eDeviceId);
    HAL_DSI_Phy_20nm_1_0_SetAnaLaneConfig(eDeviceId);

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x7F);

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(0), 0x20); //Lane Setting for T_HS_ZERO
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(1), 0x40); //Lane Setting for T_HS_ZERO
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(2), 0x20); //Lane Setting for T_HS_ZERO
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNn_CFG4_ADDR(3), 0x00); //Lane Setting for T_HS_ZERO
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_LNCK_CFG4_ADDR, 0x00);
    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_2_ADDR, 0x00); //Duty Cycle Correction Setting
    HAL_DSI_Phy_20nm_1_0_SetRegBistOff(eDeviceId);

    out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_GLBL_TEST_CTRL_ADDR, 0x00); //Duty Cycle Correction Setting

  }

  return eStatus;
}
/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_PllSetup()
*/
/*!
* \brief
*     Set up DSI PLL, pass back some config parameters, such as VCO output frequency,
*     PCLK divider ratio for CC in the form of numerator and denominator, etc.
*
* \param [in]   psDsiPhyConfig     - Phy config info
* \param [out]  psDsiPhyConfigInfo - Phy & PLL config pass back info
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_Phy_20nm_1_0_PllSetup(HAL_DSI_PhyConfigType       *psDsiPhyConfig,
  HAL_DSI_PhyConfigInfoType   *psDsiPhyConfigInfo)
{
  HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

  /* validate input parameters */
  if ((psDsiPhyConfig->eDeviceId != DSI_DeviceID_0) &&
    (psDsiPhyConfig->eDeviceId != DSI_DeviceID_1))
  {
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else if ((psDsiPhyConfig->uBitsPerPixel != 16) &&
    (psDsiPhyConfig->uBitsPerPixel != 18) &&
    (psDsiPhyConfig->uBitsPerPixel != 24))
  {
    /* unsupported pixel bit depth */
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else if ((psDsiPhyConfig->uNumOfDataLanes == 0) ||
    (psDsiPhyConfig->uNumOfDataLanes > 4))
  {
    /* illegal number of DSI data lanes */
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else
  {
    float   fDividerRatio   = 0.0;
    float   fActualDivider  = 0.0;
    uintPtr uOffset         = HAL_DSI_GetRegBaseOffset(psDsiPhyConfig->eDeviceId);
    uint32  uRegVal         = 0;
    uint32  uTemp1          = 0;
    uint32  uTemp2          = 0;
    uint32  uIntegerPart    = 0;
    uint32  uFractionalPart = 0;
    uint32  uDuration       = 0;
    uint32  uPllCompVal     = 0;
    uint32  uDesiredBitClkInHz = psDsiPhyConfig->uDesiredBitClkFreq;
    uint32  uHalfBitClkInHz = uDesiredBitClkInHz >> 1;
    uint32  uDesiredVcoClkInHz = HAL_DSI_Phy_FindVco(uDesiredBitClkInHz);
    uint32  uHrOclk2 = 4;
    uint32  uHrOclk3 = HAL_DSI_Phy_CalcPixelClockDivider(uHalfBitClkInHz, psDsiPhyConfig->uNumOfDataLanes, psDsiPhyConfig->uBitsPerPixel, psDsiPhyConfigInfo);;

    //Save Clock info
    psDsiPhyConfigInfo->uBitClkFreq = uDesiredBitClkInHz;
    psDsiPhyConfigInfo->uPllVcoOutputFreq = uDesiredVcoClkInHz;

    if ((HAL_DSI_PLL_CONFIG_SPLIT_SOURCE == psDsiPhyConfig->ePLLConfigSource) &&
        (DSI_DeviceID_1 == psDsiPhyConfig->eDeviceId))
    {
      /* Skip PLL1 programming in dual DSI split mode, both DSI controller are driven by DSI PLL0*/
    }
    else
    {
      HAL_DSI_Phy_20nm_1_0_powerUpCommon(psDsiPhyConfig->eDeviceId);

      fDividerRatio = (float)(uDesiredVcoClkInHz / (HAL_DSI_ESCCLK_SRC*1000000.0));
      fActualDivider = fDividerRatio / HAL_DSI_PRE_DIVIDER;
      uIntegerPart = HAL_DSI_Phy_20nm_1_0_roundDown(fActualDivider);
      uFractionalPart = HAL_DSI_Phy_20nm_1_0_roundDown((fActualDivider - uIntegerPart) * 0x100000);

      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_POST_DIVIDER_CONTROL, EN, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_POST_DIVIDER_CONTROL, BYPASS_LPDIV, HAL_DSI_Phy_FindPostDividerControl(uHalfBitClkInHz));
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_POST_DIVIDER_CONTROL, N_DIV, HAL_DSI_Phy_FindNdiv(uHalfBitClkInHz));
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_POST_DIVIDER_CONTROL_ADDR, uRegVal);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK2_DIVIDER_ADDR, uHrOclk2 - 1);

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_HR_OCLK3_DIVIDER_ADDR, uHrOclk3 - 1);

      HAL_DSI_Phy_20nm_1_0_pllDphyHw_pllLoopBw(psDsiPhyConfig->eDeviceId);

      //FRAC START
      uTemp1 = uFractionalPart & 0x7F;
      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_DIV_FRAC_START1, DIV_FRAC_START1_MUX, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DIV_FRAC_START1, DIV_FRAC_START1, uTemp1);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START1_ADDR, uRegVal);

      uTemp2 = uFractionalPart >> 7;
      uTemp1 = uTemp2 & 0x7F;
      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_DIV_FRAC_START2, DIV_FRAC_START2_MUX, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DIV_FRAC_START2, DIV_FRAC_START2, uTemp1);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START2_ADDR, uRegVal);

      uTemp2 = uFractionalPart >> 14;
      uTemp1 = uTemp2 & 0x3F;
      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_DIV_FRAC_START3, DIV_FRAC_START3_MUX, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DIV_FRAC_START3, DIV_FRAC_START3, uTemp1);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DIV_FRAC_START3_ADDR, uRegVal);

      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DEC_START1, DEC_START1_MUX, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DEC_START1, DEC_START1, uIntegerPart & 0x7F);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DEC_START1_ADDR, uRegVal);

      uTemp1 = uIntegerPart & 0x80;
      uTemp2 = uTemp1 >> 7;
      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_DEC_START2, DEC_START2_MUX, 1);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_DEC_START2, DEC_START2, uTemp2);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_DEC_START2_ADDR, uRegVal);

      uDuration = 128;
      uPllCompVal = (uint32)((fDividerRatio * (uDuration - 1)) / 10);
      uTemp1 = uPllCompVal & 0xFF;
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP1_ADDR, uTemp1);

      uTemp2 = uPllCompVal >> 8;
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP2_ADDR, uTemp2);

      uTemp1 = uPllCompVal >> 16;
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP3_ADDR, uTemp1);

      uRegVal = HWIO_OUT_FLD(0, DSI_0_PHY_PLL_PLLLOCK_CMP_EN, PLLLOCK_RNG, HAL_DSI_PHY_PLL_LOCKRNG);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_PLLLOCK_CMP_EN, PLLLOCK_CNT, HAL_DSI_PHY_PLL_LOCKCNT);
      uRegVal = HWIO_OUT_FLD(uRegVal, DSI_0_PHY_PLL_PLLLOCK_CMP_EN, PLLLOCK_CMP_EN, 1);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLLLOCK_CMP_EN_ADDR, uRegVal);

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_CNTRL_ADDR, 0x00000007);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_PLL_BKG_KVCO_CAL_EN_ADDR, 0x00000000);

      /* Resistor Calibration Linear Search  */
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG1_ADDR, 0x64);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_CODE_START_SEG2_ADDR, 0x64);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RES_TRIM_CONTROL_ADDR, 0x15);

      /* Reset State Machine Control */
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL_ADDR,  0x20);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL2_ADDR, 0x07);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR, 0x02);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR, 0x03);

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, 0x80); //RESET for Digital blocks except control registers - Assertion

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_1_ADDR, 0x00);

      //POLL if PLL is locked or not.
      eStatus = HAL_DSI_Phy_20nm_1_0_PllLockDetect(psDsiPhyConfig->eDeviceId);
    }
  }

  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_Phy_20nm_1_0_SetupTimingParams()
*/
/*!
* \brief
*     Calculate PHY timing parameters.
*
* \param [in]  pTimingParameters - DSI core ID
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_Phy_20nm_1_0_SetupTimingParams(HAL_DSI_TimingSettingType *pTimingParameters)
{
  HAL_MDSS_ErrorType      eStatus = HAL_MDSS_STATUS_SUCCESS;
  DSI_TimingOverrideType *pPreDefinedTimings = pTimingParameters->pPreDefinedTimings;
  uintPtr                 uOffset = HAL_DSI_GetRegBaseOffset(pTimingParameters->eDeviceId);
  uint32                  uRegVal = 0;

  /* validate input parameters */
  if ((pTimingParameters->eDeviceId != DSI_DeviceID_0) &&
      (pTimingParameters->eDeviceId != DSI_DeviceID_1))
  {
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else
  {
    float fT_clk_prepare_actual, fT_clk_zero_actual, fT_hs_prepare_actual, fT_hs_exit_actual;
    float fT_clk_zero_min, fT_hs_prepare_min, fT_hs_prepare_max, fT_hs_zero_min, fT_hs_trail_min, fT_clk_post_min, fT_clk_pre_min;

    uint32 uT_clk_prepare, uT_clk_zero, uT_clk_trail, uT_hs_prepare, uT_hs_zero, uT_hs_trail;
    uint32 uT_hs_request, uT_hs_exit, uT_clk_post, uT_clk_pre;
    uint32 uT_clk_zero_msb = 0;

    int32  iMinVal = 0;
    int32  iMaxVal = 0;
    float  fMidVal = 0.0f;
    float  fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_10;
    float  fEscclk = (pTimingParameters->uEscapeFreq <= 1000000) ? (HAL_DSI_ESCCLK_SRC * 1000000) : (pTimingParameters->uEscapeFreq);  /* in Hz */
    float  fTlpx = HAL_DSI_PHY_TIPX_NUMERATOR / fEscclk;
    float  fUiBitclk = HAL_DSI_PHY_TIPX_NUMERATOR / (float)pTimingParameters->uBitclock;

    /**********************************************************************************/
    // T_CLK_PREPARE calculation
    iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(HAL_DSI_PHY_TCLK_PREPARE_PHY_MIN / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
    iMaxVal = HAL_DSI_Phy_20nm_1_0_Roundup(HAL_DSI_PHY_TCLK_PREPARE_PHY_MAX / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
    fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
    uT_clk_prepare = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
    if ((uT_clk_prepare % 2) != 0)
    {
      uT_clk_prepare = uT_clk_prepare - 1;
    }
    if (pPreDefinedTimings->bTimingCLKPrepareOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR, pPreDefinedTimings->uTimingCLKPrepareValue);
    }
    else
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_2_ADDR, uT_clk_prepare);
    }
    fT_clk_prepare_actual = ((uT_clk_prepare / 2) + 1) * 2 * fUiBitclk;

    /**********************************************************************************/
    // T_HS_RQST is user entry based on UI/ESCCLK
    uT_hs_request = (uint32)(fTlpx / fUiBitclk);
    if (!(uT_hs_request % 2))
    {
      uT_hs_request -= HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
    }
    if (pPreDefinedTimings->bTimingHSRequestOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR, pPreDefinedTimings->uTimingHSRequestValue);
    }
    else
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_8_ADDR, uT_hs_request);
    }

    /**********************************************************************************/
    // T_CLK_ZERO calculation
    fT_clk_zero_min = HAL_DSI_PHY_TCLK_ZERO_PARAM1 - fT_clk_prepare_actual;
    iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(fT_clk_zero_min / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET; //Digit to round to be similar Excel SS

    if (HAL_DSI_PHY_GENERIC_TIMING_MAX < iMinVal)
    {
      iMaxVal = HAL_DSI_PHY_GENERIC_TIMING_MAX * 2 + 1;
      uT_clk_zero_msb = 1;
    }
    else
    {
      iMaxVal = HAL_DSI_PHY_GENERIC_TIMING_MAX;
      uT_clk_zero_msb = 0;
    }
    fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_10;
    fMidVal = (iMaxVal == HAL_DSI_PHY_GENERIC_TIMING_MAX) ? ((iMaxVal - iMinVal)*(fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal) :
      (iMinVal * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal);
    uT_clk_zero = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);

    if ((uT_clk_zero % 2) != 0)
    {
      uT_clk_zero = uT_clk_zero - 1;
    }

    while (((uT_clk_prepare + uT_hs_request + uT_clk_zero) % 8) != 0)
    {
      uT_clk_zero++;
    }

    if (HAL_DSI_PHY_GENERIC_TIMING_MAX != iMaxVal)
    {
      uT_clk_zero -= HAL_DSI_PHY_GENERIC_TIMING_MAX;
    }

    if (pPreDefinedTimings->bTimingCLKZeroOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR, (pPreDefinedTimings->uTimingCLKZeroValue & 0xFF));      /* bit 0-7 */
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR, (pPreDefinedTimings->uTimingCLKZeroValue & 0x100) >> 8);  /* bit 8   */
    }
    else
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_0_ADDR, (uT_clk_zero));
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_3_ADDR, uT_clk_zero_msb);
    }
    fT_clk_zero_actual = ((uT_clk_zero >> 1) + 1) * 2 * fUiBitclk;

    /**********************************************************************************/
    // T_CLK_TRAIL calculation
    if (pPreDefinedTimings->bTimingCLKTrailOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR, pPreDefinedTimings->uTimingCLKTrailValue);
    }
    else
    {
      iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(HAL_DSI_PHY_TCLK_TRAIL_MIN / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
      iMaxVal = HAL_DSI_Phy_20nm_1_0_Roundup((HAL_DSI_PHY_TEOT_PARAM1 + HAL_DSI_PHY_TEOT_PARAM2*fUiBitclk - HAL_DSI_PHY_TREOT) / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
      fRangeFactor = (pTimingParameters->uBitclock > HAL_DSI_PHY_BITCLK_RANGE_FREQ_1) ? HAL_DSI_PHY_RANGEFACTOR_10 : HAL_DSI_PHY_RANGEFACTOR_40;
      fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
      uT_clk_trail = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
      if ((uT_clk_trail % 2) != 0)
      {
        uT_clk_trail = uT_clk_trail - 1;
      }
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_1_ADDR, uT_clk_trail);
    }

    /**********************************************************************************/
    // T_HS_PREPARE calculation
    fT_hs_prepare_min = HAL_DSI_PHY_T_HS_PREPARE_PARAM1 + (HAL_DSI_PHY_T_HS_PREPARE_PARAM2 * fUiBitclk);
    fT_hs_prepare_max = HAL_DSI_PHY_T_HS_PREPARE_PARAM3 + (HAL_DSI_PHY_T_HS_PREPARE_PARAM4 * fUiBitclk);
    iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(fT_hs_prepare_min / fUiBitclk) - 2;
    iMaxVal = HAL_DSI_Phy_20nm_1_0_Roundup(fT_hs_prepare_max / fUiBitclk) - 2;
    fRangeFactor = (pTimingParameters->uBitclock > HAL_DSI_PHY_BITCLK_RANGE_FREQ_2) ? HAL_DSI_PHY_RANGEFACTOR_15 : HAL_DSI_PHY_RANGEFACTOR_10;
    fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
    uT_hs_prepare = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
    if ((uT_hs_prepare % 2) != 0)
    {
      uT_hs_prepare = uT_hs_prepare - 1;
    }
    if (pPreDefinedTimings->bTimingHSPrepareOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR, pPreDefinedTimings->uTimingHSPrepareValue);
    }
    else
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_6_ADDR, uT_hs_prepare);
    }
    fT_hs_prepare_actual = (((uT_hs_prepare / 2) + 1) * 2 * fUiBitclk) + (2 * fUiBitclk);

    /**********************************************************************************/
    // T_HS_ZERO calculation
    if (pPreDefinedTimings->bTimingHSZeroOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR, pPreDefinedTimings->uTimingHSZeroValue);
    }
    else
    {
      fT_hs_zero_min = HAL_DSI_PHY_T_HS_ZERO_PARAM1 + (HAL_DSI_PHY_T_HS_ZERO_PARAM2 * fUiBitclk) - fT_hs_prepare_actual;
      iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(fT_hs_zero_min / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
      iMaxVal = HAL_DSI_PHY_GENERIC_TIMING_MAX;
      fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_10;
      fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
      uT_hs_zero = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
      if ((uT_hs_zero % 2) != 0)
      {
        uT_hs_zero = uT_hs_zero - 1;
      }
      if (HAL_DSI_PHY_T_HS_ZERO_PARAM3 > uT_hs_zero)
      {
        uT_hs_zero = HAL_DSI_PHY_T_HS_ZERO_PARAM3;
      }
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_5_ADDR, uT_hs_zero);
    }

    /**********************************************************************************/
    // T_HS_TRAIL calculation
    if (pPreDefinedTimings->bTimingHSTrailOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR, (uint32)pPreDefinedTimings->uTimingHSTrailValue);
    }
    else
    {
      fT_hs_trail_min = HAL_DSI_PHY_T_HS_TRAIL_PARAM1 + (HAL_DSI_PHY_T_HS_TRAIL_PARAM2 * fUiBitclk);
      iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(fT_hs_trail_min / fUiBitclk) - 2;
      iMaxVal = HAL_DSI_Phy_20nm_1_0_Roundup((HAL_DSI_PHY_TEOT_PARAM1 + HAL_DSI_PHY_TEOT_PARAM2*fUiBitclk - HAL_DSI_PHY_TREOT) / fUiBitclk) - HAL_DSI_PHY_GENERIC_SHIFT_OFFSET;
      fRangeFactor = (pTimingParameters->uBitclock > HAL_DSI_PHY_BITCLK_RANGE_FREQ_1) ? HAL_DSI_PHY_RANGEFACTOR_10 : HAL_DSI_PHY_RANGEFACTOR_40;
      fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
      uT_hs_trail = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
      if ((uT_hs_trail % 2) != 0)
      {
        uT_hs_trail = uT_hs_trail - 1;
      }

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_7_ADDR, uT_hs_trail);
    }

    /**********************************************************************************/
    // T_HS_EXIT calculation
    iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup(HAL_DSI_PHY_T_HS_EXIT_MIN / fUiBitclk) - 2;
    iMaxVal = HAL_DSI_PHY_GENERIC_TIMING_MAX;
    fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_10;
    fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
    uT_hs_exit = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
    if ((uT_hs_exit % 2) != 0)
    {
      uT_hs_exit = uT_hs_exit - 1;
    }
    fT_hs_exit_actual = ((uT_hs_exit / 2) + 1) * 2 * fUiBitclk;
    if (pPreDefinedTimings->bTimingHSExitOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR, pPreDefinedTimings->uTimingHSExitValue);
    }
    else
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_4_ADDR, uT_hs_exit);
    }

    /**********************************************************************************/
    // T_TA_GO calculation T_TA_SURE calculation (Hard coded values)
    {
      uint32 uTAGo = HAL_DSI_PHY_T_TA_GO_PARAM1;
      uint32 uTASure = HAL_DSI_PHY_T_TA_SURE_PARAM1;
      if (pPreDefinedTimings->bTimingTAGoOverride)
      {
        uTAGo = pPreDefinedTimings->uTimingTAGoValue;
      }

      if (pPreDefinedTimings->bTimingTASureOverride)
      {
        uTASure = pPreDefinedTimings->uTimingTASureValue;
      }

      uRegVal = HWIO_SET_FLD(DSI_0_PHY_DSIPHY_TIMING_CTRL_9, DSIPHY_T_TA_GO, uTAGo) |
        HWIO_SET_FLD(DSI_0_PHY_DSIPHY_TIMING_CTRL_9, DSIPHY_T_TA_SURE, uTASure);

      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_9_ADDR, uRegVal);
    }

    /**********************************************************************************/
    // T_TA_GET calculation (Hard coded value)
    if (pPreDefinedTimings->bTimingTAGetOverride)
    {
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR, pPreDefinedTimings->uTimingTAGetValue);
    }
    else
    {
      uRegVal = HWIO_SET_FLD(DSI_0_PHY_DSIPHY_TIMING_CTRL_10, DSIPHY_T_TA_GET, HAL_DSI_PHY_T_TA_GET_PARAM1);
      out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_TIMING_CTRL_10_ADDR, uRegVal);
    }

    /**********************************************************************************/
    // T_CLK_POST calculation and T_CLK_PRE calculation
    if (pPreDefinedTimings->bTimingCLKPostOverride)
    {
      uT_clk_post = pPreDefinedTimings->uTimingCLKPostValue;
    }
    else
    {
      fT_clk_post_min = HAL_DSI_PHY_T_CLK_POST_PARAM1 + (HAL_DSI_PHY_T_CLK_POST_PARAM2 * fUiBitclk);
      iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup((fT_clk_post_min - (HAL_DSI_PHY_T_CLK_POST_PARAM3 * fUiBitclk) - fT_hs_exit_actual) /
        (HAL_DSI_PHY_T_CLK_POST_PARAM4 * fUiBitclk)) - HAL_DSI_PHY_T_CLK_POST_PARAM5;
      iMaxVal = HAL_DSI_PHY_T_CLK_POST_MAX; // Recommended max value
      fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_5;
      fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
      uT_clk_post = HAL_DSI_Phy_20nm_1_0_roundDown(fMidVal);
    }

    if (pPreDefinedTimings->bTimingCLKPreOverride)
    {
      uT_clk_pre = pPreDefinedTimings->uTimingCLKPreValue;
    }
    else
    {
      fT_clk_pre_min = HAL_DSI_PHY_T_CLK_PRE_PARAM1 * fUiBitclk;
      iMinVal = HAL_DSI_Phy_20nm_1_0_Roundup((fT_clk_pre_min + fT_clk_prepare_actual + fT_clk_zero_actual + fTlpx) / (HAL_DSI_PHY_T_CLK_PRE_PARAM2 * fUiBitclk)) - HAL_DSI_PHY_T_CLK_PRE_PARAM3;
      iMaxVal = HAL_DSI_PHY_T_CLK_PRE_MAX; // Recommended max value
      fRangeFactor = HAL_DSI_PHY_RANGEFACTOR_1;
      if (iMinVal > iMaxVal)
      {
        fMidVal = ((iMaxVal * 2) - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
        uT_clk_pre = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
        uT_clk_pre = HAL_DSI_Phy_20nm_1_0_roundDown(uT_clk_pre / 2);
      }
      else {
        fMidVal = (iMaxVal - iMinVal) * (fRangeFactor / HAL_DSI_PHY_PERCENT_DENOMENATOR) + iMinVal;
        uT_clk_pre = HAL_DSI_Phy_20nm_1_0_Roundup(fMidVal);
      }
    }
    uRegVal = HWIO_SET_FLD(DSI_0_CLKOUT_TIMING_CTRL, T_CLK_PRE, uT_clk_pre) |
              HWIO_SET_FLD(DSI_0_CLKOUT_TIMING_CTRL, T_CLK_POST, uT_clk_post);
    out_dword(uOffset + HWIO_MMSS_DSI_0_CLKOUT_TIMING_CTRL_ADDR, uRegVal);
  }

  return eStatus;
}
/* -----------------------------------------------------------------------
** Public functions
** ----------------------------------------------------------------------- */

/****************************************************************************
*
** FUNCTION: HAL_DSI_1_3_0_PhyDisable()
*/
/*!
* \brief
*     Disables DSI Phy.
*
* \param [in]   eDeviceId   - DSI core ID
*
* \retval None
*
****************************************************************************/
void HAL_DSI_1_3_0_PhyDisable( DSI_Device_IDType   eDeviceId )
{
   uintPtr  uOffset = HAL_DSI_GetRegBaseOffset(eDeviceId);

   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x0000006F);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x0000000F);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x00000007);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x00000003);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x00000001);
   out_dword(uOffset + HWIO_MMSS_DSI_0_PHY_DSIPHY_CTRL_0_ADDR, 0x00000000);

   return;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_1_3_0_PhyPllPowerCtrl()
*/
/*!
* \brief
*     Power up/down PLL, LDO and powergen.
*
*
* \param [in]  eDeviceId    - DSI core ID
* \param [in]  bPllPowerUp  - TRUE: power up, FALSE: power down;
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_1_3_0_PhyPllPowerCtrl( DSI_Device_IDType   eDeviceId,
                                                  bool32              bPllPowerUp )
{
   HAL_MDSS_ErrorType   eStatus     = HAL_MDSS_STATUS_SUCCESS;
   return eStatus;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_1_3_0_PhySetup()
*/
/*!
* \brief
*     Set up DSI Phy alone.
*
* \param [in]  eDeviceId          - DSI core ID
* \param [IN]  psDsiPhyConfig     - Phy configuration
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_1_3_0_PhySetup(DSI_Device_IDType          eDeviceId,
                                          HAL_DSI_PhyConfigType     *psDsiPhyConfig)
{
  HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

  HAL_DSI_Phy_20nm_1_0_Setup(eDeviceId, psDsiPhyConfig);
  
  return eStatus;
}

/****************************************************************************
*
** FUNCTION: HAL_DSI_1_3_0_PhyPllSetup()
*/
/*!
* \brief
*     Set up DSI PLL, pass back some config parameters, such as VCO output frequency, 
*     PCLK divider ratio for CC in the form of numerator and denominator, etc.
*
* \param [in]   psDsiPhyConfig     - Phy config info
* \param [out]  psDsiPhyConfigInfo - Phy & PLL config pass back info
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_1_3_0_PhyPllSetup(HAL_DSI_PhyConfigType       *psDsiPhyConfig,
                                             HAL_DSI_PhyConfigInfoType   *psDsiPhyConfigInfo)
{
  HAL_MDSS_ErrorType        eStatus = HAL_MDSS_STATUS_SUCCESS;

  /* validate input parameters */
  if ((psDsiPhyConfig->eDeviceId != DSI_DeviceID_0) &&
      (psDsiPhyConfig->eDeviceId != DSI_DeviceID_1))
  {
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else if ((psDsiPhyConfig->uBitsPerPixel != 16) &&
           (psDsiPhyConfig->uBitsPerPixel != 18) &&
           (psDsiPhyConfig->uBitsPerPixel != 24))
  {
    /* unsupported pixel bit depth */
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }
  else if ((psDsiPhyConfig->uNumOfDataLanes == 0) ||
           (psDsiPhyConfig->uNumOfDataLanes > 4))
  {
    /* illegal number of DSI data lanes */
    eStatus = HAL_MDSS_DSI_FAILED_INVALID_INPUT_PARAMETER;
  }

  if (eStatus == HAL_MDSS_STATUS_SUCCESS)
  {
      eStatus = HAL_DSI_Phy_20nm_1_0_PllSetup(psDsiPhyConfig, psDsiPhyConfigInfo);
  }

  return eStatus;
}


/****************************************************************************
*
** FUNCTION: HAL_DSI_1_3_0_PhySetupTimingParams()
*/
/*!
* \brief
*     Calculate PHY timing parameters.
*
* \param [in]  pTimingParameters - DSI core ID
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_DSI_1_3_0_PhySetupTimingParams(HAL_DSI_TimingSettingType *pTimingParameters)
{
    HAL_MDSS_ErrorType eStatus = HAL_MDSS_STATUS_SUCCESS;

    eStatus = HAL_DSI_Phy_20nm_1_0_SetupTimingParams(pTimingParameters);

    return eStatus;
}


HAL_MDSS_ErrorType HAL_DSI_1_3_0_PhyReConfigure(DSI_Device_IDType eDeviceID,uint32 RefreshRate)
{
    // Reconfiguring PLL for new refresh rate - To be Implemented 
    return HAL_MDSS_STATUS_FAILED_NOT_SUPPORTED;
}

void HAL_DSI_1_3_0_PhyPllInitialize(void)
{
    /*  Disable the Phy hardware enable pins to avoid ~15mA leakage on the Phy PLL.
    *  Hardware does not reset the pins to the correct state after a core reset.
    */
    out_dword(HWIO_MMSS_DSI_0_PHY_PLL_PLL_VCOTAIL_EN_ADDR,      0x42);
    out_dword(HWIO_MMSS_DSI_0_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, 0x02);
    out_dword(HWIO_MMSS_DSI_0_PHY_PLL_RESETSM_CNTRL3_ADDR,      0x02);

    out_dword(HWIO_MMSS_DSI_1_PHY_PLL_PLL_VCOTAIL_EN_ADDR,      0x42);
    out_dword(HWIO_MMSS_DSI_1_PHY_PLL_BIAS_EN_CLKBUFLR_EN_ADDR, 0x02);
    out_dword(HWIO_MMSS_DSI_1_PHY_PLL_RESETSM_CNTRL3_ADDR,      0x02);
}

#ifdef __cplusplus
}
#endif
