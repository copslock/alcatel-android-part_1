/*! \file */

/*
===========================================================================

FILE:         hal_vbif_mdp_1_3_0.h

DESCRIPTION:  Header file for VBIF HW settings
  

===========================================================================
Copyright (c) 2012- 2013 Qualcomm Technologies, Inc. All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/
#ifndef _HAL_VBIF_MDP_1_3_0_H
#define _HAL_VBIF_MDP_1_3_0_H

// Recommended VBIF settings for Gandalf-MDSS
#define VBIF_SMMU_IMPLDEF_PREDICTION_DISABLE0_DEFAULT_MDP_1_3_0          0x007FFFFF
#define VBIF_SMMU_IMPLDEF_S1L1_BFB_WIDTH0_DEFAULT_MDP_1_3_0              0x00000000
#define VBIF_SMMU_IMPLDEF_S1L2_BFB_WIDTH0_DEFAULT_MDP_1_3_0              0x00000004
#define VBIF_SMMU_IMPLDEF_S1L3_BFB_WIDTH0_DEFAULT_MDP_1_3_0              0x00000010
#define VBIF_SMMU_IMPLDEF_TLB_LOWER_POINTERS_0_DEFAULT_MDP_1_3_0         0x00005000
#define VBIF_SMMU_IMPLDEF_S1L1_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_3_0    0x000182c1
#define VBIF_SMMU_IMPLDEF_S1L2_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_3_0    0x00005a1d
#define VBIF_SMMU_IMPLDEF_S1L3_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_3_0    0x0001822d
#define VBIF_SMMU_IMPLDEF_TLB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_3_0          0x00000000
#define VBIF_SMMU_IMPLDEF_S1L1_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_3_0     0x00000000
#define VBIF_SMMU_IMPLDEF_S1L2_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_3_0     0x00000028
#define VBIF_SMMU_IMPLDEF_S1L3_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_3_0     0x00000068
#define VBIF_SMMU_IMPLDEF_SS2TBN_0_DEFAULT_MDP_1_3_0                     0x00000000
#define VBIF_SMMU_IMPLDEF_SS2TBN_1_DEFAULT_MDP_1_3_0                     0x00000000
#define VBIF_SMMU_IMPLDEF_SS2TBN_2_DEFAULT_MDP_1_3_0                     0x00000000
#define VBIF_SMMU_IMPLDEF_SS2TBN_3_DEFAULT_MDP_1_3_0                     0x00000000
#define VBIF_SMMU_IMPLDEF_SS2TBN_4_DEFAULT_MDP_1_3_0                     0x00000000
#define VBIF_VBIF_IN_RD_LIM_CONF0_DEFAULT_MDP_1_3_0                      0x00080808
#define VBIF_VBIF_IN_RD_LIM_CONF1_DEFAULT_MDP_1_3_0                      0x08000808
#define VBIF_VBIF_IN_RD_LIM_CONF2_DEFAULT_MDP_1_3_0                      0x00080808
#define VBIF_VBIF_IN_RD_LIM_CONF3_DEFAULT_MDP_1_3_0                      0x00000808
#define VBIF_VBIF_IN_WR_LIM_CONF0_DEFAULT_MDP_1_3_0                      0x10000000
#define VBIF_VBIF_IN_WR_LIM_CONF1_DEFAULT_MDP_1_3_0                      0x00100000
#define VBIF_VBIF_IN_WR_LIM_CONF2_DEFAULT_MDP_1_3_0                      0x10000000
#define VBIF_VBIF_IN_WR_LIM_CONF3_DEFAULT_MDP_1_3_0                      0x00000000
#define VBIF_VBIF_ABIT_SORT_DEFAULT_MDP_1_3_0                            0x00013FFF
#define VBIF_VBIF_ABIT_SORT_CONF_DEFAULT_MDP_1_3_0                       0x000000A4
#define VBIF_VBIF_GATE_OFF_WRREQ_EN_DEFAULT_MDP_1_3_0                    0x00003FFF

#endif // _HAL_VBIF_MDP_1_3_0_H
