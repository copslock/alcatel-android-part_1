/*! \file */

/*
===========================================================================

FILE:         hal_mdp_1_6_0.c

DESCRIPTION:  HAL changes like caps, offsets etc, specific to MDSS 1.6.0 
  
===========================================================================
===========================================================================
Copyright (c) 2013 - 2014 Qualcomm Technologies, Inc. All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "hal_mdp_i.h"
#include "mdssreg_1.6.0.h"


#ifdef __cplusplus
extern "C" {
#endif


/*------------------------------------------------------------------------------
 * Defines
 *----------------------------------------------------------------------------*/
/** Format: HAL_MDP_<MAJOR>_<MINOR>_<STEP>_XXX */
#define  HAL_MDP_1_6_0_NUM_OF_RGB_LAYERS                       2     /** RGB Source Surface Pixel Processors      */
#define  HAL_MDP_1_6_0_NUM_OF_VIG_LAYERS                       1     /** VIG Source Surface Pixel Processors      */
#define  HAL_MDP_1_6_0_NUM_OF_DMA_LAYERS                       1     /** DMA Source Surface Pixel Processors      */
#define  HAL_MDP_1_6_0_NUM_OF_CURSOR_LAYERS                    1     /** CURSOR Source Surface Pixel Processors   */
#define  HAL_MDP_1_6_0_NUM_OF_LAYER_MIXERS                     2     /** Layer Mixers */
#define  HAL_MDP_1_6_0_NUM_OF_DSPP                             1     /** Destination Surface Pixel Processor      */
#define  HAL_MDP_1_6_0_NUM_OF_PHYSICAL_INTERFACES              1     /** INTF_0                                   */
#define  HAL_MDP_1_6_0_NUM_OF_WRITEBACK_INTERFACES             2     /** WB_0                                     */
#define  HAL_MDP_1_6_0_NUM_OF_CONTROL_PATHS                    3     /** MDP_CTL_x (x = 0, 1 and 2)                  */
#define  HAL_MDP_1_6_0_NUM_OF_DATA_PATHS                       3     /** MDP_WB_x (x = 0, 1 and 2)                   */
#define  HAL_MDP_1_6_0_NUM_OF_PINGPONGS                        1     /** Pingpong blocks                          */
#define  HAL_MDP_1_6_0_NUM_OF_WATCHDOGS                        2     /** Watch Dogs                               */

/** RGB and VG and DMA IGC LUT Size */
#define  HAL_MDP_1_6_0_SSPP_IGC_LUT_SIZE                       256
#define  HAL_MDP_1_6_0_SSPP_IGC_NUM_OF_COLOR_COMPONENTS        3     /** Color 0, 1 and 2                         */

/** DSPP IGC LUT Size */
#define  HAL_MDP_1_6_0_DSPP_IGC_LUT_SIZE                       256
#define  HAL_MDP_1_6_0_DSPP_IGC_NUM_OF_COLOR_COMPONENTS        3     /** Color 0, 1 and 2                         */

/** LAYER MIXER */
#define  HAL_MDP_1_6_0_LAYER_MIXER_MAX_BLEND_STAGES            3     /** Blend Stage #0 and 1             */

/** QOS */
#define HAL_MDP_1_6_0_MAX_MDPCORE_CLK_FREQ                     320000000   /** MDP core maximum working clock frequency in Hz */

/* QoS priority re-mapping for real time read clients of resolution less than 1080p; real time clients are ViG, RGB, and DMA in line mode;*/
#define HAL_MDP_1_6_0_QOS_REMAPPER_REALTIME_CLIENTS            HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_REALTIME,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_ULTRA_LOW_LATENCY )

/* QoS priority re-mapping for non-real time read clients; non-real time clients are DMA in block mode;*/
#define HAL_MDP_1_6_0_QOS_REMAPPER_NONREALTIME_CLIENTS         HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT )

/** VBIF max burst length */
#define HAL_MDP_1_6_0_VBIF_ROUND_ROBIN_QOS_ARB                 (HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_RR_QOS_EN_BMSK |\
                                                                HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_MMU_RR_QOS_EN_BMSK)
  /**QSEED Scale Filter caps */
#define  HAL_MDP_1_6_0_QSEED_FILTER_CAPS                       (HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_NEAREST_NEIGHBOR_FILTER) | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_BILINEAR_FILTER)         | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_PCMN_FILTER)             | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_CAF_FILTER))

/*------------------------------------------------------------------------
 * Global Data Definitions
 *------------------------------------------------------------------------ */

/* SMP client to h/w client id mapping */
const uint32 gMDP_SMPClientMap_1_6_0[HAL_MDP_SMP_MMB_CLIENT_MAX] = 
{
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_NONE  
  0x1,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y,
  0x2,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CR,
  0x3,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CB,
  0x4,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_Y,
  0x5,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CR,
  0x6,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CB,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_Y,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CR,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CB,
  0x7,     //HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH,
  0x8,     //HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH,
  0x0,     //HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH,
};

/* QoS priority output for write back paths; */
uint32 gMDP_Qos_WB_Paths_Priority_1_6_0[]    = {HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                                HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                                HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                                HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                                HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT };


/****************************************************************************
*
** FUNCTION: HAL_MDP_SetHWBlockRegOffsets_1_6_0()
*/
/*!
* \brief
*     set MDP HW block register offsets
*
*
****************************************************************************/
static void HAL_MDP_SetHWBlockRegOffsets_1_6_0(void)
{
  /* Control path HW block register offset */
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_NONE]           = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_0]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_1]              = (MMSS_MDP_CTL_1_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_2]              = (MMSS_MDP_CTL_2_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_3]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_4]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_5]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_6]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_7]              = 0x00000000;

  /* Data path HW block register offset */
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_NONE]                 = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_0]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_1]                    = (MMSS_MDP_WB_1_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_2]                    = (MMSS_MDP_WB_2_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_3]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_4]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_5]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_6]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_7]                    = 0x00000000;

  /* Destination (DSPP) HW block register offset */
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_NONE]              = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_0]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_1]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_2]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_3]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_4]                 = 0x00000000;

  /* AD Core (AADC) HW block register offset */
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_NONE]              = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_0]                 = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_1]                 = (MMSS_MDP_AADC_1_REG_BASE_OFFS - MMSS_MDP_AADC_0_REG_BASE_OFFS);
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_2]                 = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_3]                 = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_4]                 = 0x00000000;

  /* Physical interface HW block register offset */
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE] = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]       = (MMSS_MDP_INTF_1_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]       = 0x00000000;

  /* Layer mixer HW block register offset */
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_NONE]             = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_0]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_1]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_2]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_3]                = (MMSS_MDP_VP_0_LAYER_3_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_4]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_5]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_6]                = 0x00000000;

  /* Layer mixer blending stage register offset */
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_0]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_1]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND1_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_2]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND2_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_3]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_4]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_5]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_CURSOR] = 0x00000000;

  /* PingPong HW block register offset */
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_NONE]                  = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_0]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_1]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_2]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_3]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_4]                     = 0x00000000;

  /* Source (SSPP) HW block register offset */
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (MMSS_MDP_VP_0_RGB_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (MMSS_MDP_VP_0_RGB_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = (MMSS_MDP_VP_0_DMA_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;

  /* Source (SSPP) Layer (Scalar) HW block register offset */
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (HWIO_MMSS_MDP_VP_0_RGB_0_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (HWIO_MMSS_MDP_VP_0_RGB_1_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;

  /*  video misr ctrl register offset */
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_VIDEO_MISR_CTRL_OFFS) ;  
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /* video misr signature register offset */
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_VIDEO_MISR_SIGNATURE_OFFS) ;  
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /*  CMD misr ctrl register offset */
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_CMD_0_MISR_CTRL_OFFS) ;  
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = 0x00000000 ;  
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /* CMD misr signature register offset */
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_CMD_0_MISR_SIGNATURE_OFFS) ;  
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /*DSI ULPS clamp controller register offset*/  
  //For 8916 this register lies on TCSR_TCSR_REGS module address space with physcial address of 0x0193E020
  MMSS_DSI_ULPS_CLAMP_CTRL_OFFSET                                          = 0x00000000;
}



/* Map for VBIF clients for QOS remapper values for Realitime clients (primary) */
static HAL_MDP_VBIFQOSRemappersType gSVBIFQOSRemapPrimary_1_6_0[HAL_MDP_VBIF_CLIENT_MAX] = 
{
   /*          uVBIFQosRemapper00                                    uVBIFQosRemapper01                                  uVBIFQosRemapper10                          uVBIFQosRemapper11*/ 
   //Client C0:    HAL_MDP_VBIF_CLIENT_VIG0
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C1: HAL_MDP_VBIF_CLIENT_RGB0
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C2 : HAL_MDP_VBIF_CLIENT_DMA0
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C3 : HAL_MDP_VBIF_CLIENT_WB0
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C4 : HAL_MDP_VBIF_CLIENT_VIG1
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME},
   //Client C5 : HAL_MDP_VBIF_CLIENT_RGB1
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C6 : HAL_MDP_VBIF_CLIENT_WB2
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C7 : HAL_MDP_VBIF_CLIENT_CURSOR
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C8 : HAL_MDP_VBIF_CLIENT_VIG2
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C9 : HAL_MDP_VBIF_CLIENT_RGB2
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C10 : HAL_MDP_VBIF_CLIENT_DMA1
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C11: HAL_MDP_VBIF_CLIENT_WB1
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C12 : HAL_MDP_VBIF_CLIENT_VIG3
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
   //Client C13 : HAL_MDP_VBIF_CLIENT_RGB3
   {HAL_MDP_TRFCTRL_LATENCY_REALTIME,      HAL_MDP_TRFCTRL_LATENCY_REALTIME,        HAL_MDP_TRFCTRL_LATENCY_REALTIME,     HAL_MDP_TRFCTRL_LATENCY_REALTIME}, 
};

/* Map for VBIF clients for QOS remapper values for non Realitime clients (WriteBack/WFD)*/
static HAL_MDP_VBIFQOSRemappersType gSVBIFQOSRemapWriteBack_1_6_0[HAL_MDP_VBIF_CLIENT_MAX] = 
{
   /*          uVBIFQosRemapper00                                    uVBIFQosRemapper01                                  uVBIFQosRemapper10                          uVBIFQosRemapper11*/ 
   //Client C0:    VG0
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C1: RGB0
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C2 : DMA_0_RD
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C3 : WB0_WR
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME},
   //Client C4 : VG1
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME},
   //Client C5 : RGB1
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C6 : WB2_WR
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME},
   //Client C7 : CURSOR
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C8 : VG2
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C9 : RGB2
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME},
   //Client C10 : DMA_1_RD
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C11: WB2_WR
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME},
   //Client C12 : VG3
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
   //Client C13 : RGB3
   {HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,  HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,    HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME, HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME}, 
};

/****************************************************************************
*
** FUNCTION: HAL_MDP_ReadHardwareInfo_1_6_0()
*/
/*!
* \brief
*     Reads the hardware capabilities for the given MDP Version(1.6.0)
*
* \param [out]  psHwInfo            - Hardware information
* \param [out]  psMdpHwInfo         - MDP hardware information 
*
* \retval NONE
*
****************************************************************************/
void HAL_MDP_ReadHardwareInfo_1_6_0(HAL_MDP_HwInfo             *psHwInfo,
                                    HAL_MDP_Hw_Private_Info    *psMdpHwInfo)
 {
   // Assign HW block register base. Values come from auto-generated mdssreg.h 
   MMSS_MDP_REG_BASE                                      = (MMSS_MDSS_REG_BASE + 0x00001000);
   MMSS_MDP_SSPP_TOP0_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x00001000);
   MMSS_MDP_SMP_TOP0_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00001030);
   MMSS_MDP_SSPP_TOP1_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x000011e8);
   MMSS_MDP_SMP_TOP1_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x000012fc);
   MMSS_MDP_DSPP_TOP0_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x00001300);
   MMSS_MDP_SSPP_IGC_LUT_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00001200);
   MMSS_MDP_PERIPH_TOP0_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00001380);
   MMSS_MDP_SSPP_TOP2_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x000013a8);
   MMSS_MDP_PERIPH_TOP1_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x000013f0);
   MMSS_MDP_CTL_0_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00002000);
   MMSS_MDP_CTL_1_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00002200);
   MMSS_MDP_CTL_2_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00002400);
   MMSS_MDP_CTL_3_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00002600);
   MMSS_MDP_CTL_4_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00002800);
   MMSS_MDP_VP_0_VIG_0_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00005200);
   MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00005000);
   MMSS_MDP_VP_0_VIG_1_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00007200);
   MMSS_MDP_VP_0_VIG_1_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00007000);
   MMSS_MDP_VP_0_VIG_2_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00009200);  
   MMSS_MDP_VP_0_VIG_2_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00009000); 
   MMSS_MDP_VP_0_RGB_0_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00015200);
   MMSS_MDP_VP_0_RGB_0_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00015000);  
   MMSS_MDP_VP_0_RGB_1_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00017200);
   MMSS_MDP_VP_0_RGB_1_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00017000);  
   MMSS_MDP_VP_0_RGB_2_REG_BASE                           = (MMSS_MDSS_REG_BASE + 0x00019200);
   MMSS_MDP_VP_0_RGB_2_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00019000);  
   MMSS_MDP_VP_0_DMA_0_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00025000);
   MMSS_MDP_VP_0_DMA_1_SSPP_REG_BASE                      = (MMSS_MDSS_REG_BASE + 0x00027000);  
   MMSS_MDP_VP_0_LAYER_0_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00045000);
   MMSS_MDP_VP_0_LAYER_1_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00046000);
   MMSS_MDP_VP_0_LAYER_2_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00047000); 
   MMSS_MDP_VP_0_LAYER_3_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00048000);
   MMSS_MDP_VP_0_LAYER_4_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x00049000);  
   MMSS_MDP_VP_0_DSPP_0_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00055000);
   MMSS_MDP_VP_0_DSPP_1_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00057000);
   MMSS_MDP_VP_0_DSPP_2_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00059000);  
   MMSS_MDP_WB_0_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00065000);
   MMSS_MDP_WB_1_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00065800);  
   MMSS_MDP_WB_2_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00066000);
   MMSS_MDP_WB_3_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00066800);  
   MMSS_MDP_WB_4_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00067000);  
   MMSS_MDP_INTF_0_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0006b000);
   MMSS_MDP_INTF_1_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0006b800);
   MMSS_MDP_INTF_2_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0006c000);  
   MMSS_MDP_INTF_3_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0006c800);  
   MMSS_MDP_PP_0_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00071000);
   MMSS_MDP_PP_1_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00071800);
   MMSS_MDP_PP_2_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00072000);  
   MMSS_DSI_0_DSI_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00098000);
   MMSS_DSI_0_PHY_PLL_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x00098300);
   MMSS_DSI_0_PHY_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00098500);
   MMSS_DSI_0_PHY_REG_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x00098780);  
   MMSS_DSI_1_DSI_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000a0000);
   MMSS_DSI_1_PHY_PLL_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x000a0300);
   MMSS_DSI_1_PHY_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000a0500);
   MMSS_DSI_1_PHY_REG_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x000a0780);  
   MMSS_VBIF_VBIF_MDP_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x000c8000);  
   MMSS_MDP_AADC_0_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00079000);
   MMSS_MDP_AADC_1_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00079800);
   MMSS_MDP_ENCR_NS_0_REG_BASE                            = (MMSS_MDSS_REG_BASE + 0x00077000);
   MMSS_MDP_ENCR_S_0_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00077400);
 
   HAL_MDP_SetHWBlockRegOffsets_1_6_0();
   /*
    * Notes: This file only contains differences between MDP_1.0.0 and MDP_1.6.0
    */
    if(NULL != psHwInfo)
    {
       psHwInfo->uNumOfRGBLayers              = HAL_MDP_1_6_0_NUM_OF_RGB_LAYERS;
       psHwInfo->uNumOfVIGLayers              = HAL_MDP_1_6_0_NUM_OF_VIG_LAYERS;
       psHwInfo->uNumOfDMALayers              = HAL_MDP_1_6_0_NUM_OF_DMA_LAYERS;
       psHwInfo->uNumOfCursorLayers           = HAL_MDP_1_6_0_NUM_OF_CURSOR_LAYERS;
       psHwInfo->uNumOfLayerMixers            = HAL_MDP_1_6_0_NUM_OF_LAYER_MIXERS;
       psHwInfo->uNumOfDSPPs                  = HAL_MDP_1_6_0_NUM_OF_DSPP;
       psHwInfo->uNumOfBltEngines             = HAL_MDP_1_6_0_NUM_OF_WRITEBACK_INTERFACES;
       psHwInfo->uNumOfControlPaths           = HAL_MDP_1_6_0_NUM_OF_CONTROL_PATHS;
       psHwInfo->uNumOfDataPaths              = HAL_MDP_1_6_0_NUM_OF_DATA_PATHS;
       psHwInfo->uNumOfLayerMixerBlendStages  = HAL_MDP_1_6_0_LAYER_MIXER_MAX_BLEND_STAGES;              // BLEND STAGES
       psHwInfo->uNumOfPhyInterfaces          = HAL_MDP_1_6_0_NUM_OF_PHYSICAL_INTERFACES;
       psHwInfo->uNumOfPingPongs              = HAL_MDP_1_6_0_NUM_OF_PINGPONGS;
       psHwInfo->uNumOfWatchDogs              = HAL_MDP_1_6_0_NUM_OF_WATCHDOGS;
    }
 
    if(NULL != psMdpHwInfo)
    {
       psMdpHwInfo->uMaxMdpCoreClkFreq                 = HAL_MDP_1_6_0_MAX_MDPCORE_CLK_FREQ;
   
       //VBIF
       psMdpHwInfo->uVBIFRoundRobinQosArb              = HAL_MDP_1_6_0_VBIF_ROUND_ROBIN_QOS_ARB;
       psMdpHwInfo->pVBIFPrimaryQOSRemappers           = (HAL_MDP_VBIFQOSRemappersType *)&gSVBIFQOSRemapPrimary_1_6_0;
       psMdpHwInfo->pVBIFWBQOSRemappers                = (HAL_MDP_VBIFQOSRemappersType *)&gSVBIFQOSRemapWriteBack_1_6_0;
       
       //QoS
       psMdpHwInfo->uQosRemapperRealTimeClients        = HAL_MDP_1_6_0_QOS_REMAPPER_REALTIME_CLIENTS;
       psMdpHwInfo->uQosRemapperNonRealTimeClients     = HAL_MDP_1_6_0_QOS_REMAPPER_NONREALTIME_CLIENTS;
       
       psMdpHwInfo->pQosWBPathsPriority                = (uint32 *)gMDP_Qos_WB_Paths_Priority_1_6_0;
       
       //DST PACK PATTERN
       psMdpHwInfo->pDstPackPatternInfo                = (uint32 *)&guSrcUnpackInfo;
 
       // SMP client ID mapping
       psMdpHwInfo->pClientToHWClientMap               = (uint32 *)&gMDP_SMPClientMap_1_6_0;      
 
       // Histogram config function
       psMdpHwInfo->sIpFxnTbl.HistogramConfig          = HAL_MDP_DSPP_HistogramV2_Config;
 
       // Histogram lock function
       psMdpHwInfo->sIpFxnTbl.HistogramLock            = HAL_MDP_DSPP_HistogramV2_Lock;
       
       // MISR config function
       psMdpHwInfo->eMisrCfgMethod                     = HAL_MDP_MISR_CONFIG_METHOD_1;

       // QSEED Scale Filter caps
       psMdpHwInfo->uQSEEDSupportedScaleFilters        = HAL_MDP_1_6_0_QSEED_FILTER_CAPS;
    }
 }

#ifdef __cplusplus
}
#endif
