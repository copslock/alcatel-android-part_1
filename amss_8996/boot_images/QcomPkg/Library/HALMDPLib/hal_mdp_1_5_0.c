/*! \file */

/*
===========================================================================

FILE:         hal_mdp_1_5_0.c

DESCRIPTION:  MDP Initialization
  
===========================================================================
===========================================================================
Copyright (c) 2012 - 2013 Qualcomm Technologies, Inc.
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "hal_mdp_i.h"
#include "mdssreg_1.5.0.h"

#ifdef __cplusplus
extern "C" {
#endif


/*------------------------------------------------------------------------------
 * Defines
 *----------------------------------------------------------------------------*/
#define  HAL_MDP_1_5_0_NUM_OF_RGB_LAYERS                       4     /** RGB Source Surface Pixel Processors      */
#define  HAL_MDP_1_5_0_NUM_OF_VIG_LAYERS                       4     /** VIG Source Surface Pixel Processors      */
#define  HAL_MDP_1_5_0_NUM_OF_CURSOR_LAYERS                    2     /** CURSOR Source Surface Pixel Processors  */
#define  HAL_MDP_1_5_0_NUM_OF_LAYER_MIXERS                     6     /** Layer Mixers */
#define  HAL_MDP_1_5_0_NUM_OF_DSPP                             4     /** Destination Surface Pixel Processor           */
#define  HAL_MDP_1_5_0_NUM_OF_PHYSICAL_INTERFACES              3     /** INTF_1, INTF_2 and INTF_3        */
#define  HAL_MDP_1_5_0_NUM_OF_PINGPONGS                        4     /** Pingpong blocks                          */
#define  HAL_MDP_1_5_0_NUM_OF_WATCHDOGS                        2     /** Watch Dogs                               */
#define  HAL_MDP_1_5_0_NUM_OF_AXI_PORTS                        2     /** Number of AXI ports                      */
#define  HAL_MDP_1_5_0_NUM_OF_VBIF_CLIENTS                     14    /** Number of VBIF clients                   */
#define  HAL_MDP_1_5_0_NUM_OF_CONTROL_PATHS                    5
#define  HAL_MDP_1_5_0_NUM_OF_DATA_PATHS                       5

/** LAYER MIXER */
#define  HAL_MDP_1_5_0_LAYER_MIXER_MAX_BLEND_STAGES            7     /** Blend Stage #0, 1, 2, 3, 4, 5, 6           */

/** VBIF max burst length */
#define HAL_MDP_1_5_0_VBIF_ROUND_ROBIN_QOS_ARB                 (HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_RR_QOS_EN_BMSK |\
                                                                HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_MMU_RR_QOS_EN_BMSK)

/* QoS priority re-mapping for real time read clients; real time clients are ViG, RGB, and DMA in line mode;*/
#define HAL_MDP_1_5_0_QOS_REMAPPER_REALTIME_CLIENTS            HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,      \
                                                               HAL_MDP_TRFCTRL_LATENCY_NON_REALTIME,     \
                                                               HAL_MDP_TRFCTRL_LATENCY_REALTIME,         \
                                                               HAL_MDP_TRFCTRL_LATENCY_REALTIME )

/* QoS priority re-mapping for non-real time read clients; non-real time clients are DMA in block mode;*/
#define HAL_MDP_1_5_0_QOS_REMAPPER_NONREALTIME_CLIENTS         HAL_MDP_QOS_REMAPPER_INFO(                \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,      \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,      \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,      \
                                                               HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT)
/**QSEED Scale Filter caps */
#define  HAL_MDP_1_5_0_QSEED_FILTER_CAPS                       (HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_NEAREST_NEIGHBOR_FILTER) | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_BILINEAR_FILTER)         | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_PCMN_FILTER)             | \
                                                                HAL_MDP_QSEED_FILTER_CAP(HAL_MDP_SCALAR_CAF_FILTER))

/** Highest bank bit configuration */
#define HAL_MDP_1_5_0_HIGHEST_BANK_BIT_DEFAULT                 2 // This corresponds to value 15

/* PanicLUT setting on how fill levels (FL0 to FL7) will trigger the panic level */
#define  HAL_MDP_1_5_0_PANIC_LUT0_SETTING                     (HAL_MDP_PANIC_LUT0_INFO( HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LESS_URGENT))

/* PanicLUT setting on how fill levels (FL8 to FL15) will trigger the panic level */
#define  HAL_MDP_1_5_0_PANIC_LUT1_SETTING                     (HAL_MDP_PANIC_LUT0_INFO( HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT, \
                                                                                        HAL_MDP_PANIC_LEVEL_LEAST_URGENT))

/* Robust LUT setting on how fill levels (FL0 to FL15) will trigger the robust on/off signal*/
#define  HAL_MDP_1_5_0_ROBUST_LUT_SETTING                     (HAL_MDP_ROBUST_LUT_INFO( HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_OFF, \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON,  \
                                                                                        HAL_MDP_ROBUST_SIGNAL_ON))

/*------------------------------------------------------------------------
 * Global Data Definitions
 *------------------------------------------------------------------------ */
/* SMP client to h/w client id mapping */
const uint32 gMDP_SMPClientMap_1_5_0[HAL_MDP_SMP_MMB_CLIENT_MAX] = 
{
   0x0,     //HAL_MDP_SMP_MMB_CLIENT_NONE  
   0x1,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_Y,
   0x2,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CR,
   0x3,     //HAL_MDP_SMP_MMB_CLIENT_VIG0_FETCH_CB,
   0x4,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_Y,
   0x5,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CR,
   0x6,     //HAL_MDP_SMP_MMB_CLIENT_VIG1_FETCH_CB,
   0x7,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_Y,
   0x8,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CR,
   0x9,     //HAL_MDP_SMP_MMB_CLIENT_VIG2_FETCH_CB,
   0x13,    //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_Y,
   0x14,    //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CR,
   0x15,    //HAL_MDP_SMP_MMB_CLIENT_VIG3_FETCH_CB,
   0xA,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_Y,
   0xB,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CR,
   0xC,     //HAL_MDP_SMP_MMB_CLIENT_DMA0_FETCH_CB,
   0xD,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_Y,
   0xE,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CR,
   0xF,     //HAL_MDP_SMP_MMB_CLIENT_DMA1_FETCH_CB,
   0x10,    //HAL_MDP_SMP_MMB_CLIENT_RGB0_FETCH,
   0x11,    //HAL_MDP_SMP_MMB_CLIENT_RGB1_FETCH,
   0x12,    //HAL_MDP_SMP_MMB_CLIENT_RGB2_FETCH,
   0x16,    //HAL_MDP_SMP_MMB_CLIENT_RGB3_FETCH,
};

/* QoS priority output for write back paths; */
uint32 gMDP_Qos_WB_Paths_Priority_1_5_0[] = { HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                              HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                              HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                              HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT,
                                              HAL_MDP_TRFCTRL_LATENCY_BEST_EFFORT };

/* Panic/Robust LUT setting on how fill level will trigger the panic/robust signal */
HAL_MDP_PanicRobustSettingType   sPanicRobustSetting_1_5_0 = { HAL_MDP_1_5_0_PANIC_LUT0_SETTING,
                                                               HAL_MDP_1_5_0_PANIC_LUT1_SETTING,
                                                               HAL_MDP_1_5_0_ROBUST_LUT_SETTING};

/****************************************************************************
*
** FUNCTION: HAL_MDP_SetHWBlockRegOffsets_1_5_0()
*/
/*!
* \brief
*     set MDP HW block register offsets
*
*
****************************************************************************/
static void HAL_MDP_SetHWBlockRegOffsets_1_5_0(void)
{
  /* Control path HW block register offset */
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_NONE]           = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_0]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_1]              = (MMSS_MDP_CTL_1_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_2]              = (MMSS_MDP_CTL_2_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_3]              = (MMSS_MDP_CTL_3_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_4]              = (MMSS_MDP_CTL_4_REG_BASE_OFFS - MMSS_MDP_CTL_0_REG_BASE_OFFS);
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_5]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_6]              = 0x00000000;
  uMDPControlPathRegBaseOffset[HAL_MDP_CONTROL_PATH_7]              = 0x00000000;

  /* Data path HW block register offset */
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_NONE]                 = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_0]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_1]                    = (MMSS_MDP_WB_1_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_2]                    = (MMSS_MDP_WB_2_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_3]                    = (MMSS_MDP_WB_3_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_4]                    = (MMSS_MDP_WB_4_REG_BASE_OFFS - MMSS_MDP_WB_0_REG_BASE_OFFS);
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_5]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_6]                    = 0x00000000;
  uMDPDataPathRegBaseOffset[HAL_MDP_DATA_PATH_7]                    = 0x00000000;

  /* Destination (DSPP) HW block register offset */
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_NONE]              = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_0]                 = 0x00000000;
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_1]                 = (MMSS_MDP_VP_0_DSPP_1_REG_BASE_OFFS - MMSS_MDP_VP_0_DSPP_0_REG_BASE_OFFS);
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_2]                 = (MMSS_MDP_VP_0_DSPP_2_REG_BASE_OFFS - MMSS_MDP_VP_0_DSPP_0_REG_BASE_OFFS);
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_3]                 = (MMSS_MDP_VP_0_DSPP_3_REG_BASE_OFFS - MMSS_MDP_VP_0_DSPP_0_REG_BASE_OFFS);
  uMDPDSPPRegBaseOffset[HAL_MDP_DESTINATION_PIPE_4]                 = 0x00000000;

  /* AD Core (AADC) HW block register offset */
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_NONE]              = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_0]                 = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_1]                 = (MMSS_MDP_AADC_1_REG_BASE_OFFS - MMSS_MDP_AADC_0_REG_BASE_OFFS);
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_2]                 = (MMSS_MDP_AADC_2_REG_BASE_OFFS - MMSS_MDP_AADC_1_REG_BASE_OFFS);
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_3]                 = 0x00000000;
  uMDPAADCRegBaseOffset[HAL_MDP_DESTINATION_PIPE_4]                 = 0x00000000;

  /* Physical interface HW block register offset */
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE] = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]       = 0x00000000;
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]       = (MMSS_MDP_INTF_1_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]       = (MMSS_MDP_INTF_2_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]       = (MMSS_MDP_INTF_3_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]       = (MMSS_MDP_INTF_4_REG_BASE_OFFS - MMSS_MDP_INTF_0_REG_BASE_OFFS);
  uMDPPhyInterfaceRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]       = 0x00000000;

  /* Layer mixer HW block register offset */
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_NONE]             = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_0]                = 0x00000000;
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_1]                = (MMSS_MDP_VP_0_LAYER_1_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_2]                = (MMSS_MDP_VP_0_LAYER_2_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_3]                = (MMSS_MDP_VP_0_LAYER_3_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_4]                = (MMSS_MDP_VP_0_LAYER_4_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_5]                = (MMSS_MDP_VP_0_LAYER_5_REG_BASE_OFFS - MMSS_MDP_VP_0_LAYER_0_REG_BASE_OFFS);
  uMDPLayerMixerRegBaseOffset[HAL_MDP_LAYER_MIXER_6]                = 0x00000000;

  /* Layer mixer blending stage register offset */
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_0]      = 0x00000000;
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_1]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND1_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_2]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND2_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_3]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND3_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_4]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND4_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_5]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND5_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_6]      = (HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND6_OP_OFFS - HWIO_MMSS_MDP_VP_0_LAYER_0_BLEND0_OP_OFFS);
  uMDPLayerMixerBlendStateRegBaseOffset[HAL_MDP_BLEND_STAGE_CURSOR] = 0x00000000;

  /* PingPong HW block register offset */
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_NONE]                  = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_0]                     = 0x00000000;
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_1]                     = (MMSS_MDP_PP_1_REG_BASE_OFFS - MMSS_MDP_PP_0_REG_BASE_OFFS);
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_2]                     = (MMSS_MDP_PP_2_REG_BASE_OFFS - MMSS_MDP_PP_0_REG_BASE_OFFS);
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_3]                     = (MMSS_MDP_PP_3_REG_BASE_OFFS - MMSS_MDP_PP_0_REG_BASE_OFFS);
  uMDPPingPongRegBaseOffset[HAL_MDP_PINGPONG_4]                     = 0x00000000;

  /* Source (SSPP) HW block register offset */
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = (MMSS_MDP_VP_0_VIG_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = (MMSS_MDP_VP_0_VIG_2_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = (MMSS_MDP_VP_0_VIG_3_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (MMSS_MDP_VP_0_RGB_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (MMSS_MDP_VP_0_RGB_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = (MMSS_MDP_VP_0_RGB_2_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = (MMSS_MDP_VP_0_RGB_3_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = (MMSS_MDP_VP_0_DMA_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = (MMSS_MDP_VP_0_DMA_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = (MMSS_MDP_VP_0_CURSOR_0_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = (MMSS_MDP_VP_0_CURSOR_1_SSPP_REG_BASE_OFFS - MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE_OFFS);
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPSSPPRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;

  /* Source (SSPP) Layer (Scalar) HW block register offset */
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_NONE]                   = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_1]                  = (HWIO_MMSS_MDP_VP_0_VIG_1_OP_MODE_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_OP_MODE_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_2]                  = (HWIO_MMSS_MDP_VP_0_VIG_2_OP_MODE_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_OP_MODE_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_3]                  = (HWIO_MMSS_MDP_VP_0_VIG_3_OP_MODE_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_OP_MODE_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_VIG_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_0]                  = (HWIO_MMSS_MDP_VP_0_RGB_0_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_1]                  = (HWIO_MMSS_MDP_VP_0_RGB_1_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_2]                  = (HWIO_MMSS_MDP_VP_0_RGB_2_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_3]                  = (HWIO_MMSS_MDP_VP_0_RGB_3_SCALE_CONFIG_OFFS - HWIO_MMSS_MDP_VP_0_VIG_0_QSEED2_CONFIG_OFFS);
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_RGB_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_0]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_1]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_2]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_3]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_DMA_4]                  = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_0]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_1]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_2]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_3]               = 0x00000000;
  uMDPLayerRegBaseOffset[HAL_MDP_SOURCE_PIPE_CURSOR_4]               = 0x00000000;
  
  /*  video misr ctrl register offset */
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_0_VIDEO_MISR_CTRL_OFFS) ; 
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_VIDEO_MISR_CTRL_OFFS) ;  
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_2_VIDEO_MISR_CTRL_OFFS) ;  
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_3_VIDEO_MISR_CTRL_OFFS) ;  
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;  
  uMDPInterfaceVideoMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /* video misr signature register offset */
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_0_VIDEO_MISR_SIGNATURE_OFFS); 
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_VIDEO_MISR_SIGNATURE_OFFS);  
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_2_VIDEO_MISR_SIGNATURE_OFFS);  
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_3_VIDEO_MISR_SIGNATURE_OFFS);  
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceVideoMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /*  CMD misr ctrl register offset */
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_0_CMD_0_MISR_CTRL_OFFS);
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_CMD_0_MISR_CTRL_OFFS);  
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_2_CMD_0_MISR_CTRL_OFFS);
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_3_CMD_0_MISR_CTRL_OFFS);  
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceCMDMISRCTRLRegBaseOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
  
  /* CMD misr signature register offset */
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_ID_NONE]  = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_0]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_0_CMD_0_MISR_SIGNATURE_OFFS); 
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_1]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_1_CMD_0_MISR_SIGNATURE_OFFS);  
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_2]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_2_CMD_0_MISR_SIGNATURE_OFFS); 
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_3]        = (MMSS_MDSS_REG_BASE + HWIO_MMSS_MDP_INTF_3_CMD_0_MISR_SIGNATURE_OFFS); 
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_4]        = 0x00000000 ;
  uMDPInterfaceCMDMISRSignatureOffset[HAL_MDP_PHYSICAL_INTERFACE_5]        = 0x00000000 ;
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_ReadHardwareInfo_1_5_0()
*/
/*!
* \brief
*     Reads the hardware capabilities for the given MDP Version(1.5.0)
*
* \param [out]  psHwInfo            - Hardware information
* \param [out]  psMdpHwInfo         - MDP hardware information 
*
* \retval NONE
*
****************************************************************************/
void HAL_MDP_ReadHardwareInfo_1_5_0(HAL_MDP_HwInfo             *psHwInfo,
                                    HAL_MDP_Hw_Private_Info    *psMdpHwInfo)
{
   /*
    * Notes: This file only contains differences between MDP_1.0.0 and MDP_1.5.0
    */
   MMSS_MDP_REG_BASE                                          = (MMSS_MDSS_REG_BASE + 0x00001000);
   MMSS_MDP_SSPP_TOP0_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00001000);
   MMSS_MDP_SMP_TOP0_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00001030);
   MMSS_MDP_SSPP_TOP1_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000011e8);
   MMSS_MDP_SMP_TOP1_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x000012fc);
   MMSS_MDP_DSPP_TOP0_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00001300);
   MMSS_MDP_SSPP_IGC_LUT_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00001200);
   MMSS_MDP_PERIPH_TOP0_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x00001380);
   MMSS_MDP_SSPP_TOP2_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000013a8);
   MMSS_MDP_PERIPH_TOP1_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x000013f0);
   MMSS_MDP_CTL_0_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00002000);
   MMSS_MDP_CTL_1_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00002200);
   MMSS_MDP_CTL_2_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00002400);
   MMSS_MDP_CTL_3_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00002600);
   MMSS_MDP_CTL_4_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00002800);
   MMSS_MDP_VP_0_CURSOR_0_SSPP_REG_BASE                       = (MMSS_MDSS_REG_BASE + 0x00035000);
   MMSS_MDP_VP_0_CURSOR_1_SSPP_REG_BASE                       = (MMSS_MDSS_REG_BASE + 0x00037000);
   MMSS_MDP_VP_0_VIG_0_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00005200);
   MMSS_MDP_VP_0_VIG_0_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00005000);
   MMSS_MDP_VP_0_VIG_1_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00007200);
   MMSS_MDP_VP_0_VIG_1_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00007000);
   MMSS_MDP_VP_0_VIG_2_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00009200);
   MMSS_MDP_VP_0_VIG_2_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00009000);
   MMSS_MDP_VP_0_VIG_3_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0000b200);
   MMSS_MDP_VP_0_VIG_3_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x0000b000);
   MMSS_MDP_VP_0_RGB_0_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00015200);
   MMSS_MDP_VP_0_RGB_0_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00015000);
   MMSS_MDP_VP_0_RGB_1_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00017200);
   MMSS_MDP_VP_0_RGB_1_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00017000);
   MMSS_MDP_VP_0_RGB_2_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x00019200);
   MMSS_MDP_VP_0_RGB_2_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00019000);
   MMSS_MDP_VP_0_RGB_3_REG_BASE                               = (MMSS_MDSS_REG_BASE + 0x0001b200);
   MMSS_MDP_VP_0_RGB_3_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x0001b000);
   MMSS_MDP_VP_0_DMA_0_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00025000);
   MMSS_MDP_VP_0_DMA_1_SSPP_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x00027000);
   MMSS_MDP_VP_0_LAYER_0_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00045000);
   MMSS_MDP_VP_0_LAYER_1_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00046000);
   MMSS_MDP_VP_0_LAYER_2_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00047000);
   MMSS_MDP_VP_0_LAYER_3_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00048000);
   MMSS_MDP_VP_0_LAYER_4_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x00049000);
   MMSS_MDP_VP_0_LAYER_5_REG_BASE                             = (MMSS_MDSS_REG_BASE + 0x0004a000);
   MMSS_MDP_VP_0_DSPP_0_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x00055000);
   MMSS_MDP_VP_0_DSPP_1_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x00057000);
   MMSS_MDP_VP_0_DSPP_2_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x00059000);
   MMSS_MDP_VP_0_DSPP_3_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x0005b000);
   MMSS_MDP_WB_0_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00065000);
   MMSS_MDP_WB_1_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00065800);
   MMSS_MDP_WB_2_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00066000);
   MMSS_MDP_WB_3_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00066800);
   MMSS_MDP_WB_4_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00067000);
   MMSS_MDP_INTF_0_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0006b000);
   MMSS_MDP_INTF_1_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0006b800);
   MMSS_MDP_INTF_2_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0006c000);
   MMSS_MDP_INTF_3_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0006c800);
   MMSS_MDP_INTF_4_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0006d000);
   MMSS_MDP_PP_0_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00071000);
   MMSS_MDP_PP_1_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00071800);
   MMSS_MDP_PP_2_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00072000);
   MMSS_MDP_PP_3_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x00072800);
   MMSS_HDMI_REG_BASE                                         = (MMSS_MDSS_REG_BASE + 0x000a8000);
   MMSS_HDMI_PHY_REG_BASE                                     = (MMSS_MDSS_REG_BASE + 0x000a9200);
   MMSS_HDMI_PHY_QSERDES_COM_QSERDES_COM_HDMI_COM_REG_BASE    = (MMSS_MDSS_REG_BASE + 0x000a8600);
   MMSS_HDMI_PHY_QSERDES_TX_L0_QSERDES_TX_L0_HDMI_TX_REG_BASE = (MMSS_MDSS_REG_BASE + 0x000a8a00);
   MMSS_HDMI_PHY_QSERDES_TX_L1_QSERDES_TX_L1_HDMI_TX_REG_BASE = (MMSS_MDSS_REG_BASE + 0x000a8c00);
   MMSS_HDMI_PHY_QSERDES_TX_L2_QSERDES_TX_L2_HDMI_TX_REG_BASE = (MMSS_MDSS_REG_BASE + 0x000a8e00);
   MMSS_HDMI_PHY_QSERDES_TX_L3_QSERDES_TX_L3_HDMI_TX_REG_BASE = (MMSS_MDSS_REG_BASE + 0x000a9000);
   MMSS_DSI_0_DSI_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00098000);
   MMSS_DSI_0_PHY_PLL_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00098300);
   MMSS_DSI_0_PHY_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x00098500);
   MMSS_DSI_0_PHY_REG_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00098780);
   MMSS_DSI_1_DSI_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x000a0000);
   MMSS_DSI_1_PHY_PLL_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000a0300);
   MMSS_DSI_1_PHY_REG_BASE                                    = (MMSS_MDSS_REG_BASE + 0x000a0500);
   MMSS_DSI_1_PHY_REG_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000a0780);
   MMSS_VBIF_VBIF_MDP_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x000c8000);
   MMSS_VBIF_VBIF_VBIF_APU0132_13_QR_REG_BASE                 = (MMSS_MDSS_REG_BASE + 0x000c9000);
   MMSS_VBIF_CB0_CB0_MM_MMU_CONTEXT_BANK_REG_BASE             = (MMSS_MDSS_REG_BASE + 0x000d4000);
   MMSS_VBIF_CB1_CB1_MM_MMU_CONTEXT_BANK_REG_BASE             = (MMSS_MDSS_REG_BASE + 0x000d5000);
   MMSS_VBIF_CB2_CB2_MM_MMU_CONTEXT_BANK_REG_BASE             = (MMSS_MDSS_REG_BASE + 0x000d6000);
   MMSS_VBIF_MM_MMU_GLOBAL0_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x000cc000);
   MMSS_VBIF_MM_MMU_GLOBAL1_REG_BASE                          = (MMSS_MDSS_REG_BASE + 0x000cd000);
   MMSS_VBIF_MM_MMU_IMPL_DEF_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x000ce000);
   MMSS_VBIF_MM_MMU_PERF_MON_REG_BASE                         = (MMSS_MDSS_REG_BASE + 0x000cf000);
   MMSS_VBIF_MM_MMU_SSD_REG_BASE                              = (MMSS_MDSS_REG_BASE + 0x000d0000);
   MMSS_MDP_AADC_0_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x00079000);
   MMSS_MDP_AADC_1_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x00079800);
   MMSS_MDP_AADC_2_REG_BASE                                   = (MMSS_MDSS_REG_BASE + 0x0007a000);
   MMSS_MDP_ENCR_NS_0_REG_BASE                                = (MMSS_MDSS_REG_BASE + 0x00077000);
   MMSS_MDP_ENCR_S_0_REG_BASE                                 = (MMSS_MDSS_REG_BASE + 0x00077400);

   HAL_MDP_SetHWBlockRegOffsets_1_5_0();

   if(NULL != psHwInfo)
   {
      psHwInfo->uNumOfRGBLayers              = HAL_MDP_1_5_0_NUM_OF_RGB_LAYERS;
      psHwInfo->uNumOfVIGLayers              = HAL_MDP_1_5_0_NUM_OF_VIG_LAYERS;
      psHwInfo->uNumOfCursorLayers           = HAL_MDP_1_5_0_NUM_OF_CURSOR_LAYERS;
      psHwInfo->uNumOfLayerMixers            = HAL_MDP_1_5_0_NUM_OF_LAYER_MIXERS;
      psHwInfo->uNumOfDSPPs                  = HAL_MDP_1_5_0_NUM_OF_DSPP;
      psHwInfo->uNumOfControlPaths           = HAL_MDP_1_5_0_NUM_OF_CONTROL_PATHS;
      psHwInfo->uNumOfDataPaths              = HAL_MDP_1_5_0_NUM_OF_DATA_PATHS;
      psHwInfo->uNumOfLayerMixerBlendStages  = HAL_MDP_1_5_0_LAYER_MIXER_MAX_BLEND_STAGES;              // BLEND STAGES
      psHwInfo->uNumOfPhyInterfaces          = HAL_MDP_1_5_0_NUM_OF_PHYSICAL_INTERFACES;
      psHwInfo->uNumOfPingPongs              = HAL_MDP_1_5_0_NUM_OF_PINGPONGS;
      psHwInfo->uNumOfWatchDogs              = HAL_MDP_1_5_0_NUM_OF_WATCHDOGS;
      psHwInfo->uNumOfAxiPorts               = HAL_MDP_1_5_0_NUM_OF_AXI_PORTS;
      psHwInfo->uNumOfVbifClients            = HAL_MDP_1_5_0_NUM_OF_VBIF_CLIENTS;
   }

   if(NULL != psMdpHwInfo)
   {
      //VBIF
      psMdpHwInfo->uVBIFRoundRobinQosArb              = HAL_MDP_1_5_0_VBIF_ROUND_ROBIN_QOS_ARB;

      //QoS
      psMdpHwInfo->uQosRemapperRealTimeClients        = HAL_MDP_1_5_0_QOS_REMAPPER_REALTIME_CLIENTS;
      psMdpHwInfo->uQosRemapperNonRealTimeClients     = HAL_MDP_1_5_0_QOS_REMAPPER_NONREALTIME_CLIENTS;

      psMdpHwInfo->pQosWBPathsPriority                = (uint32 *)gMDP_Qos_WB_Paths_Priority_1_5_0;

      // Panic Safe
      psMdpHwInfo->psPanicRobustSetting               = &sPanicRobustSetting_1_5_0;

      //DST PACK PATTERN
      psMdpHwInfo->pDstPackPatternInfo                = (uint32 *)&guSrcUnpackInfo;

      // Histogram config function
      psMdpHwInfo->sIpFxnTbl.HistogramConfig          = HAL_MDP_DSPP_HistogramV2_Config;

      // Histogram lock function
      psMdpHwInfo->sIpFxnTbl.HistogramLock            = HAL_MDP_DSPP_HistogramV2_Lock;
      
      // VBIF default config function
      psMdpHwInfo->sIpFxnTbl.Vbif_HwDefaultConfig     = HAL_MDP_VBif_HwDefaults_MDP_1_5_0;
      
      // MISR status function
      psMdpHwInfo->eMisrCfgMethod                     = HAL_MDP_MISR_CONFIG_METHOD_1;

      // QSEED
      psMdpHwInfo->uQSEEDSupportedScaleFilters        = HAL_MDP_1_5_0_QSEED_FILTER_CAPS;      

      // SMP client ID mapping
      psMdpHwInfo->pClientToHWClientMap               = (uint32 *)&gMDP_SMPClientMap_1_5_0;

      /* Cursor layer type, order will be defined by HAL user */
      psMdpHwInfo->eCursorIntLayerType                = HAL_MDP_LAYER_TYPE_SSPP_CURSOR;

      psMdpHwInfo->uDefaultHighestBankBit             = HAL_MDP_1_5_0_HIGHEST_BANK_BIT_DEFAULT;

      // Enable hardware workaround
      psMdpHwInfo->uHWWorkaroundEnableBit            |= HAL_MDP_WORKAROUND_QCTDD00872136;

      // Supported Macro tile format
      psMdpHwInfo->eMacroTileFormat                   = HAL_MDP_MACRO_TILE_A4X;
      
      psMdpHwInfo->eBWC                               = HAL_MDP_COMPRESSION_BWC;

   }
}


#ifdef __cplusplus
}
#endif
