/*! \file */

/*
===========================================================================

FILE:        hal_mdp_osal.c

DESCRIPTION: HAL_MDP OS abstraction
  
===========================================================================
Copyright (c) 2012-2014 QUALCOMM Technology Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================
*/

/* -----------------------------------------------------------------------
 * Includes
 * ----------------------------------------------------------------------- */

#include "MDPSystem.h"


#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------------------------------------------------------------------
 * Local defines 
 * ------------------------------------------------------------------------------------ */

/* -------------------------------------------------------------------------------------
 * Static data declarations and functions
 * ------------------------------------------------------------------------------------ */

/* -------------------------------------------------------------------------------------
 * Public functions
 * ------------------------------------------------------------------------------------ */
/****************************************************************************
*
** FUNCTION: HAL_MDP_OSAL_SleepMs()
*/
/*!
* \brief
*     Sleep in milliseconds
*
* \param [in] uMilliSeconds           - Time in milliseconds to be waited
*
* \return NONE
*
****************************************************************************/
void HAL_MDP_OSAL_SleepMs(uint32 ms)
{
	MDP_OSAL_DELAYMS(ms);
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_OSAL_MemSet()
*/
/*!
* \brief
*   The \b HAL_MDP_OSAL_MemSet function fills the buffer with the uFillValue
*
* \param [in]   pBuffer           - Input buffer pointer
* \param [in]   uFillValue        - Value to be filled into the buffer
* \param [in]   uSizeInBytes      - Buffer size to be filled
*
* \return NONE
*
****************************************************************************/
void HAL_MDP_OSAL_MemSet(void *pBuffer, uint8 uFillValue, uint32 uSizeInBytes)
{
	MDP_OSAL_MEMSET(pBuffer, uFillValue, uSizeInBytes);
}


#ifdef __cplusplus
}
#endif
