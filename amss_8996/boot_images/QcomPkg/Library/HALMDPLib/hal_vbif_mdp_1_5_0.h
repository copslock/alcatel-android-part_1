/*! \file */

/*
===========================================================================

FILE:         hal_vbif_mdp_1_5_0.h

DESCRIPTION:  Header file for VBIF HW settings
  

===========================================================================
Copyright (c) 2012- 2013 Qualcomm Technologies, Inc.
All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================
*/
#ifndef _HAL_VBIF_MDP_1_5_0_H
#define _HAL_VBIF_MDP_1_5_0_H





// Recommended VBIF settings for Elessar-MDSS
#define VBIF_SMMU_IMPLDEF_PREDICTION_DISABLE0_DEFAULT_MDP_1_5_0               0x7fffff
#define VBIF_SMMU_IMPLDEF_HTW_AREQ_CTLR_DEFAULT_MDP_1_5_0                     0x1555
#define VBIF_SMMU_IMPLDEF_S1GB_BFB_WIDTH0_DEFAULT_MDP_1_5_0                   0x00000000
#define VBIF_SMMU_IMPLDEF_S1MB_BFB_WIDTH0_DEFAULT_MDP_1_5_0                   0x00000004
#define VBIF_SMMU_IMPLDEF_S1KB_BFB_WIDTH0_DEFAULT_MDP_1_5_0                   0x00000010
#define VBIF_SMMU_IMPLDEF_TLB_LOWER_POINTERS_0_DEFAULT_MDP_1_5_0              0x2400
#define VBIF_SMMU_IMPLDEF_S1GB_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_5_0         0x00182c1
 #define VBIF_SMMU_IMPLDEF_S1MB_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_5_0        0x0005a1d
 #define VBIF_SMMU_IMPLDEF_S1KB_BFB_LOWER_POINTERS_0_DEFAULT_MDP_1_5_0        0x0001822d
#define VBIF_SMMU_IMPLDEF_TLB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_5_0               0x0
#define VBIF_SMMU_IMPLDEF_S1GB_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_5_0          0x000000
#define VBIF_SMMU_IMPLDEF_S1MB_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_5_0          0x000028
#define VBIF_SMMU_IMPLDEF_S1KB_BFB_SRAM_BASE_ADDR0_DEFAULT_MDP_1_5_0          0x00000068
#define VBIF_SMMU_IMPLDEF_SS2TBN_0_DEFAULT_MDP_1_5_0                          0x0
#define VBIF_SMMU_IMPLDEF_SS2TBN_1_DEFAULT_MDP_1_5_0                          0x0
#define VBIF_SMMU_IMPLDEF_SS2TBN_2_DEFAULT_MDP_1_5_0                          0x0
#define VBIF_SMMU_IMPLDEF_SS2TBN_3_DEFAULT_MDP_1_5_0                          0x0
#define VBIF_SMMU_IMPLDEF_SS2TBN_4_DEFAULT_MDP_1_5_0                          0x0
#define VBIF_VBIF_ROUND_ROBIN_QOS_ARB_DEFAULT_MDP_1_5_0                       0x3
 
#endif // _HAL_VBIF_MDP_1_5_0_H

