/*! \file */

/*
===========================================================================

FILE:        hal_mdp_vbif.c

DESCRIPTION: Configures VBIF settings for MDSS.
  
===========================================================================
Copyright (c) 2012 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================
*/

/* -----------------------------------------------------------------------
 * Includes
 * ----------------------------------------------------------------------- */

#include "hal_mdp_i.h"
#include "hal_vbif_mdp_1_3_0.h"
#include "hal_vbif_mdp_1_5_0.h"


#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------------------------------------------------------------------
 * Local defines 
 * ------------------------------------------------------------------------------------ */



/* -------------------------------------------------------------------------------------
 * Static data declarations and functions
 * ------------------------------------------------------------------------------------ */
    
   
static HAL_MDSS_ErrorType HAL_MDP_VBif_QOSRemapper(HAL_MDP_VBIFQOSRemapperType  *psTrfCtrlSetting,
                                                              uint32                       uFlags );


/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_Config_ClockGating()
*/
/*!
* \brief
*     Configures clock gating on the VBIF
*
* \param [in] bEnableGating  - TRUE: enable clock gating AXI & Test Bus Domain; otherwise disable;
* \param [in] uFlags         - reserved
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_MDP_Vbif_Config_ClockGating(
                              bool32   bEnableGating,
                              uint32   uFlags )
{
   uint32 uRegValue       = 0;
   uint32 uAxiForceOn     = 0;
   uint32 uTestBusForceOn = 0;

   if (FALSE == bEnableGating)
   {
     uAxiForceOn     = 1;
     uTestBusForceOn = 1;
   }
   
   uRegValue = HWIO_OUT_FLD(uRegValue, VBIF, VBIF_CLKON_FORCE_ON, uAxiForceOn);
   uRegValue = HWIO_OUT_FLD(uRegValue, VBIF, VBIF_CLKON_FORCE_ON_TESTBUS, uTestBusForceOn);
   uRegValue = HWIO_OUT_FLD(uRegValue, VBIF, VBIF_CLKON_HYSTERESIS_VALUE, 0);
   
   out_dword(HWIO_MMSS_VBIF_VBIF_CLKON_ADDR, uRegValue);

   return HAL_MDSS_STATUS_SUCCESS;
}


/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_BurstLimitsConfig()
*/
/*!
* \brief
*     Configures Vbif burst size limits
*
* \param [in] uRead          - Maximum read burst size
* \param [in] uWrite         - Maximum write burst size
* \param [in] uFlags         - reserved
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_MDP_Vbif_BurstLimitsConfig(
                              uint32   uRead,
                              uint32   uWrite,
                              uint32   uFlags )
{
   uint32 uRegValue = 0;

   // Configure VBIF max read/write burst size
   uRegValue = HWIO_OUT_FLD(uRegValue, VBIF_VBIF_DDR_OUT_MAX_BURST, DDR_OUT_MAX_WR_BURST, uWrite);
   uRegValue = HWIO_OUT_FLD(uRegValue, VBIF_VBIF_DDR_OUT_MAX_BURST, DDR_OUT_MAX_RD_BURST, uRead);

   out_dword(HWIO_MMSS_VBIF_VBIF_DDR_OUT_MAX_BURST_ADDR, uRegValue);

   return HAL_MDSS_STATUS_SUCCESS;
}


/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_AoooCtrlConfig()
*/
/*!
* \brief
*     Enable/Disable SW controlling AOOOWR/RD signals on AXI protocol.
*
* \param [in] bEnable - TRUE: enable SW controlling AOOOWR/RD siganls; otherwise disable;
* \param [in] uFlags  - reserved
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_MDP_Vbif_AoooCtrlConfig(
                              bool32   bEnable,
                              uint32   uFlags )
{
   if (TRUE == bEnable)
   {
      out_dword(HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_EN_ADDR, HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_EN_RMSK);
      out_dword(HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_ADDR, HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_RMSK);
   }
   else
   {
      out_dword(HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_EN_ADDR, 0);
      out_dword(HWIO_MMSS_VBIF_VBIF_OUT_AXI_AOOO_ADDR, 0);
   }
   
   return HAL_MDSS_STATUS_SUCCESS;
}


/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_RoundRobinQoSArbiConfig()
*/
/*!
* \brief
*     Enable/Disable Round-Robin Arbiter with QoS signal.
*
* \param [in] bEnable - TRUE: Round-Robin Arbiter with QoS signal; otherwise disable;
* \param [in] uFlags  - reserved
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_MDP_Vbif_RoundRobinQoSArbiConfig(
                              bool32   bEnable,
                              uint32   uFlags )
{
   if (TRUE == bEnable)
   {
      out_dword(HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_ADDR, (HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_RR_QOS_EN_BMSK      |
                                                               HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_MMU_RR_QOS_EN_BMSK) );
   }
   else
   {
      out_dword(HWIO_MMSS_VBIF_VBIF_ROUND_ROBIN_QOS_ARB_ADDR, 0);
   }
   
   return HAL_MDSS_STATUS_SUCCESS;
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_AxiPortsConfig()
*/
/*!
* \brief
*     Config AXI fixed arbiter port assignement
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
static HAL_MDSS_ErrorType HAL_MDP_Vbif_AxiPortsConfig(void)
{
    if((2 == HAL_MDP_GET_AXIPORT_COUNT()) &&
       (NULL != pgsMdpHwInfo->pAxiPortAssignment))
    {
        uint32             uRegSelVal = 0;
        uint32             uRegEnVal  = 0;
        uint32             uXinIndex  = 0;

        for(uXinIndex=0; uXinIndex<(uint32)HAL_MDP_GET_VBIFCLIENT_COUNT(); uXinIndex++)
        {
          uRegSelVal |= ((pgsMdpHwInfo->pAxiPortAssignment[uXinIndex] & HWIO_MMSS_VBIF_VBIF_FIXED_SORT_SEL0_FIXED_SORT_SEL_C0_BMSK) 
                          << (uXinIndex << 1));
          uRegEnVal  |= (pgsMdpHwInfo->pAxiPortAssignment[uXinIndex] << uXinIndex);
        }
        out_dword(HWIO_MMSS_VBIF_VBIF_FIXED_SORT_SEL0_ADDR, uRegSelVal);
        out_dword(HWIO_MMSS_VBIF_VBIF_FIXED_SORT_EN_ADDR,   uRegEnVal);
    }
    return HAL_MDSS_STATUS_SUCCESS;
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_VBif_HwDefaults_MDP_1_3_0()
*/
/*!
* \brief
*     Recommended HW default settings
*
* \retval None
*
****************************************************************************/
void HAL_MDP_VBif_HwDefaults_MDP_1_3_0(void)
{
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_RD_LIM_CONF0_ADDR,                   VBIF_VBIF_IN_RD_LIM_CONF0_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_RD_LIM_CONF1_ADDR,                   VBIF_VBIF_IN_RD_LIM_CONF1_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_RD_LIM_CONF2_ADDR,                   VBIF_VBIF_IN_RD_LIM_CONF2_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_RD_LIM_CONF3_ADDR,                   VBIF_VBIF_IN_RD_LIM_CONF3_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_WR_LIM_CONF0_ADDR,                   VBIF_VBIF_IN_WR_LIM_CONF0_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_WR_LIM_CONF1_ADDR,                   VBIF_VBIF_IN_WR_LIM_CONF1_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_WR_LIM_CONF2_ADDR,                   VBIF_VBIF_IN_WR_LIM_CONF2_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_IN_WR_LIM_CONF3_ADDR,                   VBIF_VBIF_IN_WR_LIM_CONF3_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_ABIT_SORT_ADDR,                         VBIF_VBIF_ABIT_SORT_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_ABIT_SORT_CONF_ADDR,                    VBIF_VBIF_ABIT_SORT_CONF_DEFAULT_MDP_1_3_0);
    out_dword(HWIO_MMSS_VBIF_VBIF_GATE_OFF_WRREQ_EN_ADDR,                 VBIF_VBIF_GATE_OFF_WRREQ_EN_DEFAULT_MDP_1_3_0);
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_VBif_HwDefaults_MDP_1_5_0()
*/
/*!
* \brief
*     Recommended HW default settings
*
* \retval None
*
****************************************************************************/
void HAL_MDP_VBif_HwDefaults_MDP_1_5_0(void)
{
  // As per the HW suggestion, no Specific setting needs to applied
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_VBif_HwDefaults_MDP_1_7_0()
*/
/*!
* \brief
*     Recommended HW default settings
*
* \retval None
*
****************************************************************************/
void HAL_MDP_VBif_HwDefaults_MDP_1_7_0(void)
{
}

/****************************************************************************
*
** FUNCTION: HAL_MDP_VBif_QOSSetup()
*/
/*!
* \brief
*     VBIF QOS setup
*
* \retval None
*
****************************************************************************/

HAL_MDSS_ErrorType HAL_MDP_VBif_QOSSetup(HAL_MDP_VBIF_ClientIDType         eClientId,
                                                  HAL_MDP_InterfaceId               eInterfaceId,
                                                   uint32                           uFlags )
{
   HAL_MDSS_ErrorType            eStatus     = HAL_MDSS_STATUS_SUCCESS;
   HAL_MDP_VBIFQOSRemapperType   sVBIFQOSRemapperSetting;
   
   if((NULL != pgsMdpHwInfo->pVBIFPrimaryQOSRemappers) && 
       (NULL != pgsMdpHwInfo->pVBIFWBQOSRemappers))
   {
      sVBIFQOSRemapperSetting.eClientId = eClientId;
      if(eInterfaceId > HAL_MDP_INTERFACE_WRITEBACK_TYPE_BASE)
      {
         //if interface is WB then take the settings from WB QOS remapper table  
         //here we need to programm layer client + WB client
         sVBIFQOSRemapperSetting.pVBIFQOSRemappers = (HAL_MDP_VBIFQOSRemappers *)&pgsMdpHwInfo->pVBIFWBQOSRemappers[eClientId];
      }
      else
      {  
        //if interface is not WB i.e its going to primary then take the settings from primary table
        //here only layer client programming is enough and interface does not write it back to memory
        sVBIFQOSRemapperSetting.pVBIFQOSRemappers = (HAL_MDP_VBIFQOSRemappers *)&pgsMdpHwInfo->pVBIFPrimaryQOSRemappers[eClientId];
      }
      eStatus = HAL_MDP_VBif_QOSRemapper(&sVBIFQOSRemapperSetting, uFlags);
   }
   
   return eStatus;

}
/****************************************************************************
*
** FUNCTION: HAL_MDP_VBif_QOSRemapper()
*/
/*!
* \brief
*     Recommended HW default settings for VBIF QOS remappers.
*
* \retval None
*
****************************************************************************/

HAL_MDSS_ErrorType HAL_MDP_VBif_QOSRemapper(HAL_MDP_VBIFQOSRemapperType  *psTrfCtrlSetting,
                                                       uint32                       uFlags )
{
  HAL_MDSS_ErrorType      eStatus     = HAL_MDSS_STATUS_SUCCESS;

  //Read existing values 
  uint32 uVBIFQOSRemap_00 = HWIO_MMSS_VBIF_VBIF_QOS_REMAP_00_IN;
  uint32 uVBIFQOSRemap_01 = HWIO_MMSS_VBIF_VBIF_QOS_REMAP_01_IN;
  uint32 uVBIFQOSRemap_10 = HWIO_MMSS_VBIF_VBIF_QOS_REMAP_10_IN;
  uint32 uVBIFQOSRemap_11 = HWIO_MMSS_VBIF_VBIF_QOS_REMAP_11_IN;
 
  //Apply settings based on VBIF client ID..
  switch (psTrfCtrlSetting->eClientId)
  {
     case HAL_MDP_VBIF_CLIENT_VIG0:     
         uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                                  VBIF_VBIF_QOS_REMAP_00,
                                  QOS_REMAP_00_C0,
                                  psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
         uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                                  VBIF_VBIF_QOS_REMAP_01,
                                  QOS_REMAP_01_C0,
                                  psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
         uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                                  VBIF_VBIF_QOS_REMAP_10,
                                  QOS_REMAP_10_C0,
                                  psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
         uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                                  VBIF_VBIF_QOS_REMAP_11,
                                  QOS_REMAP_11_C0,
                                  psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_RGB0:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C1,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C1,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C1,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C1,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_DMA0:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C2,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C2,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C2,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C2,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_WB0:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C3,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C3,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C3,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C3,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_VIG1:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C4,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C4,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C4,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C4,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_RGB1:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C5,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C5,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C5,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C5,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_WB2:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C6,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C6,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C6,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C6,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_CURSOR:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C7,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C7,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C7,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C7,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_VIG2:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C8,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C8,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C8,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C8,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_RGB2:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C9,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C9,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C9,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C9,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_DMA1:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C10,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C10,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C10,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C10,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_WB1:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C11,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C11,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C11,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C11,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_VIG3:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C12,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C12,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C12,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C12,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     case HAL_MDP_VBIF_CLIENT_RGB3:
      uVBIFQOSRemap_00 = HWIO_OUT_FLD(uVBIFQOSRemap_00,
                               VBIF_VBIF_QOS_REMAP_00,
                               QOS_REMAP_00_C13,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper00);
      uVBIFQOSRemap_01 = HWIO_OUT_FLD(uVBIFQOSRemap_01,
                               VBIF_VBIF_QOS_REMAP_01,
                               QOS_REMAP_01_C13,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper01);
      uVBIFQOSRemap_10 = HWIO_OUT_FLD(uVBIFQOSRemap_10,
                               VBIF_VBIF_QOS_REMAP_10,
                               QOS_REMAP_10_C13,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper10);
      uVBIFQOSRemap_11 = HWIO_OUT_FLD(uVBIFQOSRemap_11,
                               VBIF_VBIF_QOS_REMAP_11,
                               QOS_REMAP_11_C13,
                               psTrfCtrlSetting->pVBIFQOSRemappers->uVBIFQosRemapper11);
     break;
     default:
     break; 
  }
    //write into the registers
    HWIO_MMSS_VBIF_VBIF_QOS_REMAP_00_OUT(uVBIFQOSRemap_00);
    HWIO_MMSS_VBIF_VBIF_QOS_REMAP_01_OUT(uVBIFQOSRemap_01);
    HWIO_MMSS_VBIF_VBIF_QOS_REMAP_10_OUT(uVBIFQOSRemap_10);
    HWIO_MMSS_VBIF_VBIF_QOS_REMAP_11_OUT(uVBIFQOSRemap_11);
  
 return eStatus; 
}

/* -------------------------------------------------------------------------------------
 * Public functions
 * ------------------------------------------------------------------------------------ */

/****************************************************************************
*
** FUNCTION: HAL_MDP_Vbif_Init()
*/
/*!
* \brief
*     Initializes VBIF settings.
*
*
* \param [in] uFlags - reserved flags
*
* \retval HAL_MDSS_ErrorType
*
****************************************************************************/
HAL_MDSS_ErrorType HAL_MDP_Vbif_Init(uint32   uFlags)
{
   /* The settings used here are from recommendations of System Performance.
    * Some of the recommended settings are equal to the power-on defaults, 
    * such as:
    *         VBIF_ARB_CTL = 0x10 (MDP has only 1 AXI port, the setting should be 0x10 instead of 0x30)
    *         VBIF_OUT_AXI_AMEMTYPE_CONF0 = 0x22222222
    *         VBIF_OUT_AXI_AMEMTYPE_CONF1 = 0x00002222
    *         VBIF_IN_RD_LIM_CONF0 = 0x08080808
    *         VBIF_IN_RD_LIM_CONF1 = 0x08080808
    *         VBIF_IN_RD_LIM_CONF2 = 0x08080808
    *         VBIF_IN_WR_LIM_CONF0 = 0x10101010
    *         VBIF_IN_WR_LIM_CONF1 = 0x10101010
    *         VBIF_IN_WR_LIM_CONF2 = 0x10101010
    *         VBIF_OUT_RD_LIM_CONF0 = 0x00000010
    *         VBIF_OUT_WR_LIM_CONF0 = 0x00000010
    * these registers are hence not touched here, and left at their own power-on defaults.
    */

   /* configure VBIF clock gating */
   HAL_MDP_Vbif_Config_ClockGating(((HAL_MDP_VBIF_ENABLE_CLOCKGATING & uFlags) ? TRUE : FALSE), 0);
   
   /* configure VBIF burst limits */
   HAL_MDP_Vbif_BurstLimitsConfig(pgsMdpHwInfo->uVBIFMaxDdrRdBurstSize, pgsMdpHwInfo->uVBIFMaxDdrWrBurstSize, 0);

   /* enable SW controlling AOOOWR/RD signals on AXI protocol */
   HAL_MDP_Vbif_AoooCtrlConfig(TRUE, 0);

   /* enable Round-Robin Arbiter with QoS signal */
   HAL_MDP_Vbif_RoundRobinQoSArbiConfig(TRUE, 0);

   /* config AXI ports */
   HAL_MDP_Vbif_AxiPortsConfig();

   // Recommended VBIF settings for Gandalf MDSS
   if(NULL != pgsMdpHwInfo->sIpFxnTbl.Vbif_HwDefaultConfig)
   {
      pgsMdpHwInfo->sIpFxnTbl.Vbif_HwDefaultConfig();
   }

   return HAL_MDSS_STATUS_SUCCESS;
}



#ifdef __cplusplus
}
#endif
