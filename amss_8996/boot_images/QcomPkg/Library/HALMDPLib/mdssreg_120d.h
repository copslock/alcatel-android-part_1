/*! \file */

/*
===========================================================================

FILE:         mdssreg_120d.h

===========================================================================
Copyright (c) 2012-2013 QUALCOMM Technologies Incorporated. 
All rights reserved. Licensed Material - Restricted Use.
Qualcomm Confidential and Proprietary.
===========================================================================
*/

#ifndef _MDSSREG_120D_H_
#define _MDSSREG_120D_H_


/*----------------------------------------------------------------------------
 * MODULE: MMSS_MDP_VP_0_DSPP_0
 *--------------------------------------------------------------------------*/

#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_HIST_LUT_EN_BMSK                                0x80000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_HIST_LUT_EN_SHFT                                   0x13
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_HIST_CTL_BMSK                                   0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_OP_MODE_HIST_CTL_SHFT                                      0x11

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_STOP_BMSK                                  0x4
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_STOP_SHFT                                  0x2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_CANCEL_BMSK                                0x2
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_CANCEL_SHFT                                0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_START_BMSK                                 0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_COLLECT_CTL_START_SHFT                                 0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_ADDR                            (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000214)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_PHYS                            (MMSS_MDP_VP_0_DSPP_0_REG_BASE_PHYS + 0x00000214)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_RMSK                                   0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_START_BMSK                             0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_RESET_SEQ_START_START_SHFT                             0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_ADDR                                  (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000218)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_PHYS                                  (MMSS_MDP_VP_0_DSPP_0_REG_BASE_PHYS + 0x00000218)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_RMSK                                        0x3f
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_IN          \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_ADDR, HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_RMSK)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_INM(m)      \
        in_dword_masked(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_ADDR, m)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_ADDR,m,v,HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_IN)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_COUNT_BMSK                                  0x3f
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_FRAME_CNT_COUNT_SHFT                                   0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_ADDR                                       (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_PHYS                                       (MMSS_MDP_VP_0_DSPP_0_REG_BASE_PHYS + 0x00000230)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_RMSK                                          0x3ffff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_VALUE_UPDATEN_BMSK                            0x20000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_VALUE_UPDATEN_SHFT                               0x11
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_INDEX_UPDATE_BMSK                             0x10000
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_INDEX_UPDATE_SHFT                                0x10
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_INDEX_BMSK                                     0xff00
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_INDEX_SHFT                                        0x8
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_VALUE_BMSK                                       0xff
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUTN_VALUE_SHFT                                        0x0

#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_ADDR                                   (MMSS_MDP_VP_0_DSPP_0_REG_BASE      + 0x00000234)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_PHYS                                   (MMSS_MDP_VP_0_DSPP_0_REG_BASE_PHYS + 0x00000234)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_RMSK                                          0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_OUT(v)      \
        out_dword(HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_ADDR,v)
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_SWAP_BMSK                                     0x1
#define HWIO_MMSS_MDP_VP_0_DSPP_0_HIST_LUT_SWAP_SWAP_SHFT                                     0x0

#endif /* _MDSSREG_120D_H_ */

