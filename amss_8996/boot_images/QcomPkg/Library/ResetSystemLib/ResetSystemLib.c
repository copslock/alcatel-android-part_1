/** @file ResetSystemLib.c
   
  Library implementation to support ResetSystem Runtime call.

  Copyright (c) 2011-2014, Qualcomm Technologies, Inc. All rights reserved.
  Portions Copyright (c) 2008 - 2010, Apple Inc. All rights reserved.<BR>

  This program and the accompanying materials                          
  are licensed and made available under the terms and conditions of the BSD License         
  which accompanies this distribution.  The full text of the license may be found at        
  http://opensource.org/licenses/bsd-license.php                                            

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,                     
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.           
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 10/14/14   na      Fix typecast mismatch for variables
 10/03/14   na      Fixing address types for 64-bit
 08/15/14   sm      Switched to new SCM API
 06/06/14   vk      Remove SPMI shutdown available check
 06/06/14   Moh     Add Mass storage fixes
 06/04/14   alal    Enable reset
 05/03/14   sv      Position of SpmiBus_init() call updated.(CR-672559)
 05/29/14   aus     Fix for warm reset when a capsule dump is performed.
 05/03/14   sv      initialising spmi during LibInitializeResetSystem. (CR-652148)
 03/14/14   aus     Allocate memory for the Wdog Sys call during initialization
 03/12/14   aus     Mass storage mode support 
 10/28/13   niting  Skip hard reset when doing an UpdateCapsule at runtime
 10/07/13   vk      Deadloop if SPMI shutdown fails
 10/01/13   vk      Shutdown on ASSERT
 09/25/13   vk      Shutdown SPMI if TZ supports it
 03/27/13   niting  Added offline crash dump support
 02/13/13   yg      Flush UART Log buffer before reset
 02/11/13   niting  Use UefiCfgLib for mass storage cookie address
 01/10/13   niting  Fixed reset issue.
 10/23/12   nk      Added support to clear MassStorage cookies when device resets
 08/01/12   niting  Added support for 8974.
 07/27/12   niting  Fixed page allocation size
 07/09/12   vishalo Remove Doad cookie handling
 07/07/12   rks     Abstract out watchdog code into UEFI lib
 06/15/12   niting  Call ExitBootServices in shutdown case
 05/08/12   yg      Disable variable sync from this module
 05/02/12   vishalo Set watchdog BITE register to known value 
 03/28/12   jz      Enable setting of Dload cookie in ExitBootServicesCallback()
 03/20/12   jz      Temporarily comment out setting Dload cookie in ExitBootServicesCallback()
 03/15/12   jz      Fix to clear Dload cookies if not in mass storage mode
 03/09/12   jz      Added support to set Dload cookies at exit boot services
 02/08/11   jdt     Added data cache flush at runtime to support UpdateCapsule.
 11/30/11   niting  Removed runtime flush.
 09/13/11   niting  Added delay before flush as workaround to runtime crash
                    during HLOS shutdown.
 08/31/11   niting  Added flushing of variable storage tables on
                    reset/shutdown.
 08/23/11   niting  Added shutdown support.
 07/07/11   niting  Cleanup.
 05/25/11   niting  Updates for Msm8960.
 01/14/11   niting  Initial revision.

=============================================================================*/

#include <PiDxe.h>

#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/UefiRuntimeLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/CacheMaintenanceLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/ArmLib.h>
#include <Library/EfiResetSystemLib.h>
#include <Library/PmicShutdown.h>
#include <Library/PmicRuntimeLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/QcomLib.h>
#include <Library/SerialPortShLib.h>
#include <Library/PcdLib.h>
#include <Library/OfflineCrashDump.h>
#include <Library/UefiLib.h>
#include <Library/UncachedMemoryAllocationLib.h>
 
#include <Include/scm_sip_interface.h>

#include <Guid/EventGroup.h>

#include <Protocol/EFIHWIO.h>
#include <Protocol/EFIScm.h>
#include "SpmiBus.h"

/* Round off to 4KB pages */
#define ROUND_TO_PAGE(x) (x & 0xfffff000)

UINTN    gDloadIdAddress = 0;
UINT32    DloadCookieSize = 0;
UINTN    gSharedIMEMAddress = 0;
UINTN    gPSHoldAddress = 0;
UINT8    *PSHoldBaseAddr = 0;
UINT32    PSHoldOffset = 0;
UINT32    PSHoldSHFT = 0;

STATIC UINT32 MemoryCaptureModeOffset = 0;
STATIC UINT32 AbnormalResetOccurredOffset = 0;
STATIC BOOLEAN SkipHardReset = FALSE;

EFI_EVENT VirtualAddressChangeEvent = NULL;


/**
  Virtual address change notification call back. It converts global pointer
  to virtual address.

  @param  Event         Event whose notification function is being invoked.
  @param  Context       Pointer to the notification function's context, which is
                        always zero in current implementation.

**/
VOID
EFIAPI
VirtualAddressChangeCallBack (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
  gRT->ConvertPointer(0, (VOID**)&gPSHoldAddress);
  gRT->ConvertPointer(0, (VOID**)&gRT);
  gRT->ConvertPointer(0, (VOID**)&gDloadIdAddress);
  gRT->ConvertPointer(0, (VOID**)&gSharedIMEMAddress);
}


/**
  Reset the MSM. 
   
**/
VOID
EFIAPI
EfiHwReset (
  VOID
  )
{
  /* This register controls the PSHOLD value. 
     1: Asserted
     0: Deasserted */
  MmioWrite32( gPSHoldAddress, 0 << PSHoldSHFT );

  /* Loop here until reset is complete */
  CpuDeadLoop ();
}

/**
  Power off the MSM. 
   
**/
VOID
EFIAPI
EfiHwPowerOff (
  VOID
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  // Shutdown support
  Status = PmicShutdown();

  
  if (EFI_SUCCESS == Status) 
  {

    /* This register controls the PSHOLD value. 
       1: Asserted
       0: Deasserted */
    MmioWrite32( gPSHoldAddress, 0 << PSHoldSHFT );

    /* Loop here until shutdown is complete */
    CpuDeadLoop ();
  }
}

/**
  Shuts down EFI boot environment and frees up memory. 
   
**/
VOID
ShutdownEfi (
  VOID
  )
{
  EFI_STATUS              Status;
  UINTN                   MemoryMapSize = 0;
  EFI_MEMORY_DESCRIPTOR   *MemoryMap = NULL;
  UINTN                   MapKey = 0;
  UINTN                   DescriptorSize = 0;
  UINT32                  DescriptorVersion = 0;
  UINTN                   Pages;

  /* Contents of this function doesn't work in runtime */
  if (EfiAtRuntime() != 0)
    return;

  /* Wait until all the data from UART FIFO is drained */
  DEBUG((EFI_D_WARN, "\n"));

  do {
    Status = gBS->GetMemoryMap (
                    &MemoryMapSize,
                    MemoryMap,
                    &MapKey,
                    &DescriptorSize,
                    &DescriptorVersion
                    );
    if (Status == EFI_BUFFER_TOO_SMALL) {
      Pages = EFI_SIZE_TO_PAGES (MemoryMapSize) + 1;
      MemoryMap = AllocatePages (Pages);
      MemoryMapSize = EFI_PAGES_TO_SIZE(Pages);

      /*
       * Get System MemoryMap
       */
      Status = gBS->GetMemoryMap (
                      &MemoryMapSize,
                      MemoryMap,
                      &MapKey,
                      &DescriptorSize,
                      &DescriptorVersion
                      );
      /* Don't do anything between the GetMemoryMap() and ExitBootServices() */
      if (!EFI_ERROR (Status)) {
        Status = gBS->ExitBootServices (gImageHandle, MapKey);
        if (EFI_ERROR (Status)) {
          FreePages (MemoryMap, Pages);
          MemoryMap = NULL;
          MemoryMapSize = 0;
        }
      }
    }
  } while (EFI_ERROR (Status));

  /* Clean and invalidate caches */
  WriteBackInvalidateDataCache();
  InvalidateInstructionCache();

  /* Turn off caches and MMU */
  ArmDisableDataCache ();
  ArmDisableInstructionCache ();
  ArmDisableMmu ();
}


/**
  Updates NV Variables to keep track of the number of resets
  and shutdowns.

**/
EFI_STATUS
EFIAPI
UpdateNVVariables ( VOID )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN DataSize;
  UINT32 VarData;

  DataSize = sizeof(VarData);

  if (!EfiAtRuntime())
  {
    Status = gRT->GetVariable (L"BSPowerCycles", 
                               &gQcomTokenSpaceGuid, 
                               NULL,
                               &DataSize, 
                               &VarData);

    if (Status != EFI_SUCCESS)
    {
      VarData = 0;
    }

    ++VarData;

    Status = gRT->SetVariable (L"BSPowerCycles", 
                               &gQcomTokenSpaceGuid, 
                               (EFI_VARIABLE_NON_VOLATILE | 
                                EFI_VARIABLE_BOOTSERVICE_ACCESS),
                               DataSize, 
                               &VarData);
  }
  
  return Status;
}

/** 
 
  SCM Sys Call to disable WDOG_EN
   
**/
EFI_STATUS
EFIAPI
WdogDisableSysCall()
{ 
  QCOM_SCM_PROTOCOL                 *pQcomScmProtocol                  = NULL;
  EFI_STATUS                        Status                             = EFI_SUCCESS;
  UINT64                            Parameters[SCM_MAX_NUM_PARAMETERS] = {0};
  UINT64                            Results[SCM_MAX_NUM_RESULTS]       = {0};
  tz_config_hw_for_ram_dump_req_t   *SysCallReq                        = (tz_config_hw_for_ram_dump_req_t*)Parameters;
  tz_syscall_rsp_t                  *SysCallRsp                        = (tz_syscall_rsp_t*)Results;

  if (EfiAtRuntime())
  {
    return EFI_UNSUPPORTED;
  }
  
  //Initializing cmd structure for scm sys call
  SysCallReq->disable_wd_dbg        = 1;
  SysCallReq->boot_partition_sel    = 0;

  // Locate QCOM_SCM_PROTOCOL. 
  Status = gBS->LocateProtocol ( &gQcomScmProtocolGuid, 
                                 NULL, 
                                 (VOID **)&pQcomScmProtocol
                               );
  if( EFI_ERROR(Status)) 
  {
    DEBUG(( EFI_D_INFO, " Locate SCM Protocol failed for Wdog disable, Status =  (0x%x)\r\n", Status));
    goto ErrorExit;
  }

  // make a SCM Sys call 
  Status = pQcomScmProtocol->ScmSipSysCall (pQcomScmProtocol,
                                            TZ_CONFIG_HW_FOR_RAM_DUMP_ID,
                                            TZ_CONFIG_HW_FOR_RAM_DUMP_ID_PARAM_ID,
                                            Parameters,
                                            Results
                                            );
  if (EFI_ERROR (Status)) 
  {
    DEBUG(( EFI_D_ERROR, "ScmSipSysCall() failed for Wdog disable, Status = (0x%x)\r\n", Status));
    goto ErrorExit;
  }
  if (SysCallRsp->status != 1)
  {    
    Status = EFI_DEVICE_ERROR;
    DEBUG(( EFI_D_ERROR, "TZ_CONFIG_HW_FOR_RAM_DUMP failed, Status = (0x%x)\r\n", SysCallRsp->status));
    goto ErrorExit;
  }

ErrorExit:
  return Status;
}


/**
  Resets the entire platform.

**/
EFI_STATUS
EFIAPI
ResetCrashDumpParams ( 
  IN CHAR16           *ResetData OPTIONAL
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINTN MemoryCaptureModeAddr = 0;
  UINT32 MemoryCaptureMode = OFFLINE_CRASH_DUMP_DISABLE;
  UINTN AbnormalResetOccurredAddr = 0;

  if (!EfiAtRuntime())
  {
    PrintOfflineCrashDumpValues();

    /* Do not clear cookies if forcing crash */
    if ((ResetData != NULL)&&(StrnCmp(ResetData, FORCE_CRASH_STRING, StrSize(FORCE_CRASH_STRING)) == 0))
    {
      return Status;
    }
  }

  MemoryCaptureModeAddr = gSharedIMEMAddress + MemoryCaptureModeOffset;
  if (((ResetData == NULL) && (MmioRead32(MemoryCaptureModeAddr) != OFFLINE_CRASH_DUMP_LEGACY_ENABLE )) || 
     ((ResetData != NULL)&&(StrnCmp(ResetData, MASS_STORAGE_STRING, StrSize(MASS_STORAGE_STRING)) != 0)))
  {
    if (!SkipHardReset)
    {
      /* Do a Hard Reset */
      PmicHardReset();
    }
    Status = ClearDLOADCookieRuntime(gDloadIdAddress, DloadCookieSize);
    if (Status != EFI_SUCCESS)
    {
      DEBUG((EFI_D_ERROR, "Could not clear DLOAD cookie\r\n"));
      return Status;
    }
  }

  Status = GetMemoryCaptureModeRuntime(MemoryCaptureModeAddr, &MemoryCaptureMode);
  if ((Status == EFI_SUCCESS) && (MemoryCaptureMode == OFFLINE_CRASH_DUMP_ENABLE))
  {
    Status = SetMemoryCaptureModeRuntime(MemoryCaptureModeAddr, OFFLINE_CRASH_DUMP_DISABLE);
    if (Status != EFI_SUCCESS)
    {
      DEBUG((EFI_D_ERROR, "Could not clear Memory Capture Mode cookie\r\n"));
      return Status;
    }

    /* Clear cookies if offline crash dump enabled and not entering mass storage mode and 
       not skipping hard reset */
    if ((ResetData != NULL)&&(StrnCmp(ResetData, MASS_STORAGE_STRING, StrSize(MASS_STORAGE_STRING)) != 0)&&(!SkipHardReset))
    {
      /* Do a Hard Reset */
      PmicHardReset();
      Status = ClearDLOADCookieRuntime(gDloadIdAddress, DloadCookieSize);
      if (Status != EFI_SUCCESS)
      {
        DEBUG((EFI_D_ERROR, "Could not clear DLOAD cookie\r\n"));
        return Status;
      }
    }
  }

  /* Clear AbnormalResetOccurred */
  AbnormalResetOccurredAddr = gSharedIMEMAddress + AbnormalResetOccurredOffset;
  Status = SetAbnormalResetOccurredRuntime(AbnormalResetOccurredAddr, ABNORMAL_RESET_DISABLE);

  if (!EfiAtRuntime())
  {
    PrintOfflineCrashDumpValues();
  }

  return Status;
}

/**
  Resets the entire platform.

  @param  ResetType             The type of reset to perform.
  @param  ResetStatus           The status code for the reset.
  @param  DataSize              The size, in bytes, of WatchdogData.
  @param  ResetData             For a ResetType of EfiResetCold, EfiResetWarm, or
                                EfiResetShutdown the data buffer starts with a Null-terminated
                                Unicode string, optionally followed by additional binary data.

**/
EFI_STATUS
EFIAPI
LibResetSystem (
  IN EFI_RESET_TYPE   ResetType,
  IN EFI_STATUS       ResetStatus,
  IN UINTN            DataSize,
  IN CHAR16           *ResetData OPTIONAL
  )
{
  volatile UINT32 count = 0;
    //initialise SPMI
  SpmiBus_Init();
  /* Delay loop required for proper reset/shutdown */
  if (EfiAtRuntime())
  {
    while (count < 0x1000000)
    {
      count++;
    }
  }
  else
  {
    /* Flush the buffer to serial port and enable
	   * synchronous transfer */
    SerialPortFlush ();
    EnableSynchronousSerialPortIO ();
    gBS->Stall(300); /* 300 uSeconds*/
  }
  


  if (ResetData != NULL)
  {
    if (StrnCmp(ResetData, CAPSULE_RESET_STRING, StrSize(CAPSULE_RESET_STRING)) == 0)
    {
      SkipHardReset = TRUE;
    }
  }

  ResetCrashDumpParams(ResetData);
  UpdateNVVariables();

  switch (ResetType) {
    case EfiResetShutdown:
      if(EfiAtRuntime () == 0) 
      {
        DEBUG((EFI_D_WARN, "Received shutdown request. Shutting down system.\n"));
        WdogDisableSysCall();
        ShutdownEfi ();
      }
      else
      {
        WriteBackInvalidateDataCache();
      }

      EfiHwPowerOff();
      break;
    case EfiResetCold:
    case EfiResetWarm:
    default:
      if(EfiAtRuntime () == 0) 
      {
        DEBUG((EFI_D_WARN, "Received reset request. Resetting system.\n"));
        WdogDisableSysCall();
        ShutdownEfi ();
      }
      else
      {
        WriteBackInvalidateDataCache();
      }
      
      /* Perform cold reset of the system - should not return */
      EfiHwReset();
      break;
  }

  /*
   * If we reset, we would not have returned...
   */
  return EFI_DEVICE_ERROR;
}

EFI_STATUS
EFIAPI
SetupConfigParams ( VOID )
{
  EFI_STATUS Status = EFI_SUCCESS;
  UINT32 tempAddrHolder = 0;
  
  /******** Setup parameters for offline crash dump support ********/
  Status = GetConfigValue("MassStorageCookieAddr", &tempAddrHolder);
  gDloadIdAddress = (UINTN) tempAddrHolder;
  if ((Status != EFI_SUCCESS) || (gDloadIdAddress == 0))
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve MassStorageCookieAddr config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  Status = GetConfigValue("MassStorageCookieSize", &DloadCookieSize);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve MassStorageCookieSize config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  
  Status = GetConfigValue("SharedIMEMBaseAddr", &tempAddrHolder);
  gSharedIMEMAddress = (UINTN) tempAddrHolder;
  if ((Status != EFI_SUCCESS) || (gSharedIMEMAddress == 0))
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve SharedIMEMBaseAddr config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }  
  
  Status = GetConfigValue("MemoryCaptureModeOffset", &MemoryCaptureModeOffset);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve MemoryCaptureModeOffset config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  Status = GetConfigValue("AbnormalResetOccurredOffset", &AbnormalResetOccurredOffset);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve AbnormalResetOccurredOffset config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  /******** Setup PSHold ********/
  Status = GetConfigValue("PSHoldOffset", &PSHoldOffset);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve PSHoldOffset config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  Status = GetConfigValue("PSHoldSHFT", &PSHoldSHFT);
  if (Status != EFI_SUCCESS)
  {
    DEBUG((EFI_D_ERROR, "Could not retrieve PSHoldSHFT config value\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  return Status;
}


/**
  Initialize any infrastructure required for LibResetSystem () to function.

  @param  ImageHandle   The firmware allocated handle for the EFI image.
  @param  SystemTable   A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS   The constructor always returns EFI_SUCCESS.

**/
EFI_STATUS
EFIAPI
LibInitializeResetSystem (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS Status = EFI_SUCCESS;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR  PSHoldMemoryDescriptor;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR  DloadIdMemoryDescriptor;
  EFI_GCD_MEMORY_SPACE_DESCRIPTOR  SharedImemDescriptor;  
  EFI_HWIO_PROTOCOL *HWIOProtocol = NULL;

  Status = SetupConfigParams();
  if (Status != EFI_SUCCESS)
    return Status;
  

  
  /** Setup PSHold */
  Status = gBS->LocateProtocol(&gEfiHwioProtocolGuid, NULL,
    (void**)&HWIOProtocol);
  if ((EFI_SUCCESS != Status) || (HWIOProtocol == NULL))
  {
    DEBUG ((EFI_D_ERROR, "ERROR: Failed to locate HWIO Protocol: 0x%08x\n", Status));
    return EFI_DEVICE_ERROR;
  }

  Status = HWIOProtocol->MapRegion(HWIOProtocol, "MPM2_MPM", (UINT8**) &PSHoldBaseAddr);
  if (EFI_SUCCESS != Status)
  {
    DEBUG ((EFI_D_ERROR, "ERROR: Failed to map MPM2_MPM base address: 0x%08x\n", Status));
    return EFI_DEVICE_ERROR;
  }

  gPSHoldAddress = (UINTN)PSHoldBaseAddr + (UINTN)PSHoldOffset;
  if (gPSHoldAddress == 0)
  {
    DEBUG((EFI_D_ERROR, "Reset not supported\n", Status));
    return EFI_UNSUPPORTED;
  }

  if(gPSHoldAddress !=  PcdGet32 (PcdPsHoldAddress))
  {
    DEBUG((EFI_D_ERROR, "PsHoldAddress PCD not in sync with cfg file \n"));
	//TODO: Cleanup, only one place should have this address cfg or pcd
	#if 0
    ASSERT (gPSHoldAddress ==  PcdGet32 (PcdPsHoldAddress));
	#endif
  }

  /******** Setup HWIO access for runtime ********/
  /*
   * Get the GCD Memory Descriptor specified by gPSHoldAddress page boundary
   */
  Status = gDS->GetMemorySpaceDescriptor (ROUND_TO_PAGE(gPSHoldAddress), 
                                          &PSHoldMemoryDescriptor);
  ASSERT_EFI_ERROR (Status);

  /*
   * Mark the 4KB region as EFI_RUNTIME_MEMORY so the OS
   * will allocate a virtual address range.
   */
  Status = gDS->SetMemorySpaceAttributes (
                  ROUND_TO_PAGE(gPSHoldAddress), 
                  EFI_PAGE_SIZE, 
                  PSHoldMemoryDescriptor.Attributes | EFI_MEMORY_RUNTIME);
  ASSERT_EFI_ERROR (Status);

  /*
   * Get the GCD Memory Descriptor specified by gDloadIdAddress page boundary
   */
  Status = gDS->GetMemorySpaceDescriptor (ROUND_TO_PAGE(gDloadIdAddress), 
                                          &DloadIdMemoryDescriptor);
  ASSERT_EFI_ERROR (Status);

  /*
   * Mark the 4KB region as EFI_RUNTIME_MEMORY so the OS
   * will allocate a virtual address range.
   */
  Status = gDS->SetMemorySpaceAttributes (
                  ROUND_TO_PAGE(gDloadIdAddress), 
                  EFI_PAGE_SIZE, 
                  DloadIdMemoryDescriptor.Attributes | EFI_MEMORY_RUNTIME);
  ASSERT_EFI_ERROR (Status);

  /*
   * Get the GCD Memory Descriptor specified by gSharedIMEMAddress page boundary
   */
  Status = gDS->GetMemorySpaceDescriptor (ROUND_TO_PAGE(gSharedIMEMAddress), 
                                          &SharedImemDescriptor);
  ASSERT_EFI_ERROR (Status);

  /*
   * Mark the 4KB region as EFI_RUNTIME_MEMORY so the OS
   * will allocate a virtual address range.
   */
  Status = gDS->SetMemorySpaceAttributes (
                  ROUND_TO_PAGE(gSharedIMEMAddress), 
                  EFI_PAGE_SIZE, 
                  SharedImemDescriptor.Attributes | EFI_MEMORY_RUNTIME);
  ASSERT_EFI_ERROR (Status);
  
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  VirtualAddressChangeCallBack,
                  NULL,
                  &gEfiEventVirtualAddressChangeGuid,
                  &VirtualAddressChangeEvent
                  );
  ASSERT_EFI_ERROR (Status);

  /* Initialize Runtime PMIC */
  PmicRuntimeLibInitialize(NULL, NULL);

  /* Clear WDOG_EN if DUMPS are NOT allowed */
  if (!IsMemoryDumpEnabled())
  {
     DEBUG((EFI_D_INFO, "Disabling WDOG EN as RAM DUMPS are NOT SUPPORTED\n"));
     WdogDisableSysCall();
  }

  return EFI_SUCCESS;
}


