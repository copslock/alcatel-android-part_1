/**
 @file FBPTLib.c
 
 @brief FBPT related functions

 Copyright (c) 2012-2014 by Qualcomm Technologies, Inc. 
 All Rights Reserved.
 
 **/

/*=======================================================================
                        Edit History

when       who     what, where, why
--------   ----    ---------------------------------------------------
10/27/14   jb      Change FBPT base to uint64
07/09/14   vk      Fix crash reading 8 byte from MPM register
05/08/14   vk      Add 64 bit support  
04/09/14   vk      Add basic table
10/31/13   vk      Stub out
06/20/12   vishalo Initial checkin
========================================================================*/

#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/ProcAsmLib.h>
#include <Library/FBPT.h>

#define HWIO_IN(x) (*((volatile UINT32*)(x)))

/* Per SWI, timer will overflow after 2^31-1 = 2147483647 */
#define LOG_MAX_COUNT_VAL  2147483647
#define COUNTER_OVERFLOW   0xFFFFFFFF


/**
  Get number of cycles from power on

  @return Number of cycles
          0xFFFFFFFF if overflow detected
**/
UINT32 
BootGetCounter(VOID)
{
  volatile UINT32 curr_count;
  volatile UINT32 last_count;

  /*Grab current time count*/
  curr_count = HWIO_IN((UINTN)PcdGet32(PcdTimeTickClkAddress));

  /*Keep grabbing the time until a stable count is given*/
  do 
  {
    last_count = curr_count;
    curr_count = HWIO_IN((UINTN)PcdGet32(PcdTimeTickClkAddress));
  } while (curr_count != last_count);

  if(curr_count < LOG_MAX_COUNT_VAL)
  {
    return curr_count;
  }
  else 
    return COUNTER_OVERFLOW;
}

/**
  Get Time from power on 

  @return Time in nanoseconds
          0 if overflow detected
**/
UINT32 
BootGetTimeNanoSec(VOID)
{
  UINT32 TimeTickCount;
  TimeTickCount = BootGetCounter();

  if(TimeTickCount == COUNTER_OVERFLOW)
    return 0;

  /*
  Clock frequency is 32.768 KHz
  1 / (32.768 KHz) = 30517.5781 nanoseconds ~= 30518 ns
  */
  return (TimeTickCount * 30518);
}

/**
  Convert ARM Performance Monitor counter cycle
  count to time in nano seconds.
 
  @param CycleCount     Number of PMon cycles
  @return Time NanoSeconds
**/
UINT64
CycleToNanoSec(UINT32 CycleCount)
{
  UINT64 Time;
  UINT64 Scale;
  UINT32 KraitClkMhz;
  
  /*  We are at 918 MHz -> Time period = 1.0893 nano seconds ~= 1 nano second, here Scale ~= 1    */
  /*  Time in nseconds = Number of cycles * Scale * 64 because cycle couter set to count every 64 */
  KraitClkMhz = PcdGet32(PcdKraitFrequencyMhz);
  Scale = (1000000000/( KraitClkMhz * 1000000));
  Time = ( ((UINT64) CycleCount * Scale) << 6); /* Left shift 6 = multiply by 64 */
  return  Time;
}

/**
  Creates FBPT structure, and initialize Reset Time field
 

  @return EFI_STATUS    Success or failure
**/
EFI_STATUS
InitializeFBPT (VOID)
{
  STATIC UefiPerfRecord_t *UefiPerfRecord;
  STATIC UINTN FBPTPayloadMemoryBase = 0;

  FBPTPayloadMemoryBase =  (UINTN)PcdGet64(PcdFBPTPayloadBaseOffset);
  UefiPerfRecord =  (UefiPerfRecord_t*) FBPTPayloadMemoryBase;

  /* Fill FBPT Header */
  UefiPerfRecord->UefiFBPTHeader.Signature[0] = 'F';
  UefiPerfRecord->UefiFBPTHeader.Signature[1] = 'B';
  UefiPerfRecord->UefiFBPTHeader.Signature[2] = 'P';
  UefiPerfRecord->UefiFBPTHeader.Signature[3] = 'T';
  UefiPerfRecord->UefiFBPTHeader.Length = sizeof(UefiPerfRecord_t);
 
  /* Perf Data Record */
  UefiPerfRecord->UefiFBBPDataRecord.PerformanceRecordType = 2;
  UefiPerfRecord->UefiFBBPDataRecord.RecordLength = 48;
  UefiPerfRecord->UefiFBBPDataRecord.Revision = 2;
  UefiPerfRecord->UefiFBBPDataRecord.Reserved = 0;
  UefiPerfRecord->UefiFBBPDataRecord.ResetEnd = CycleToNanoSec(ReadCycleCntReg()); 

  /* Dummy values for now */
  UefiPerfRecord->UefiFBBPDataRecord.OSLoaderLoadImageStart =  0ULL;
  UefiPerfRecord->UefiFBBPDataRecord.OSLoaderStartImageStart = 0ULL;
  UefiPerfRecord->UefiFBBPDataRecord.ExitBootServicesEntry =   0ULL;
  UefiPerfRecord->UefiFBBPDataRecord.ExitBootServicesExit =    0ULL;
  
  return EFI_SUCCESS;
}
