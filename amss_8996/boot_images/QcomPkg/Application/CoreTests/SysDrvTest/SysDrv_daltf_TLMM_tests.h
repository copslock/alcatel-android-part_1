/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_TLMM_tests.h
  DESCRIPTION:	Header File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _SysDrv_DALTF_TLMM_TESTS_
#define _SysDrv_DALTF_TLMM_TESTS_

#include "SysDrvTest.h"
#include <DDITlmm.h>

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 TLMMUEFITest_ConfigGPIO(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TLMMUEFITest_ConfigGPIOGroup(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 TLMMUEFITest_GPIOReadWriteFeedback(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TLMMUEFITest_GetGPIONumber(UINT32 dwArg, CHAR8 *pszArg[]);  
UINT32 TLMMUEFITest_GPIOGetOutput(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 TLMMUEFITest_GpioOutGroup(UINT32 dwArg, CHAR8 *pszArg[]); 
 
//Initialize adc daltf tests
DALResult TLMM_daltf_init(VOID);

//De-Initialize adc daltf tests
DALResult TLMM_daltf_deinit(VOID);

#endif
