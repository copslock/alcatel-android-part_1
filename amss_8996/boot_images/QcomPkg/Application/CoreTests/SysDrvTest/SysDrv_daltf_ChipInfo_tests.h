/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ChipInfo_tests.h
  DESCRIPTION:	Header File for all UEFI APT Chip Info Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _SysDrv_DALTF_CHIPINFO_TESTS_
#define _SysDrv_DALTF_CHIPINFO_TESTS_

#include "SysDrvTest.h"

#include "DDIClock.h"
#include "DDIChipInfo.h"

#define DALCHIPINFO_MODEM_1XEVDO       (DALCHIPINFO_MODEM_1X | DALCHIPINFO_MODEM_EVDO)
#define DALCHIPINFO_MODEM_1XEVDO_REVA  (DALCHIPINFO_MODEM_1XEVDO | DALCHIPINFO_MODEM_EVDO_REVA)
#define DALCHIPINFO_MODEM_1XEVDO_REVB  (DALCHIPINFO_MODEM_1XEVDO_REVA | DALCHIPINFO_MODEM_EVDO_REVB)
#define DALCHIPINFO_MODEM_WWAN         (DALCHIPINFO_MODEM_1XEVDO_REVB | DALCHIPINFO_MODEM_GSM | DALCHIPINFO_MODEM_UMTS)

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 ChipInfoUEFITest_GetChipVersion(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetRawChipVersion(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetChipId(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetRawChipId(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetChipIdString(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetChipFamily(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetChipModemSupport(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 ChipInfoUEFITest_GetProcessorNameString(UINT32 dwArg, CHAR8 *pszArg[]);

//Initialize Chip Info daltf tests
DALResult ChipInfo_daltf_init(VOID);

//De-Initialize Chip Info daltf tests
DALResult ChipInfo_daltf_deinit(VOID);

#endif
