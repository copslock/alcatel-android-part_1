/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ChipInfo_tests.c
  DESCRIPTION:	Source File for all UEFI APT Chip Info Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#include "SysDrv_daltf_ChipInfo_tests.h"

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetChipVersionParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetChipVersion Major Number",        "Major number of Chip Version\n"},
	{TF_PARAM_UINT32,   "ChipInfo UEFITest GetChipVersion Minor Number",        "Minor number of Chip Version\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetRawChipVersionParam[] =
{
   {TF_PARAM_STRING,   "ChipInfo UEFITest GetRawChipVersion Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetChipIdParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetChipId Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetRawChipIdParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetRawChipId Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetChipIdStringParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetChipId Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetChipFamilyParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetChipId Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetModemSupportParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetChipId Target Name",        "Chip Target Name\n"}
};

STATIC CONST TF_ParamDescr ChipInfoUEFITest_GetProcessorNameStringParam[] =
{
    {TF_PARAM_STRING,   "ChipInfo UEFITest GetProcessorNameString Target Name",        "Chip Target Name\n"}
};

/*====================================================
** DATA **
====================================================*/

//CONST TF_ContextDescr context1 = { (uint8)DIAG_SUBSYS_COREBSP, 0 };

//UINT32 atoint (CHAR8 *st);


UINT32 ChipInfoUEFITest_GetChipVersion(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *chipInfoHandle = NULL;
  DalChipInfoVersionType    nVersion;
  UINT32 umajor;
  UINT32 uminor;
 
  umajor    = atoint(pszArg[1]);
  uminor    = atoint(pszArg[2]);    

  eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, 
                            &chipInfoHandle);
			    
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  DalChipInfo_GetChipVersion(chipInfoHandle, &nVersion);
  AsciiPrint(PRINTSIG"ChipInfo:  Chip Version :%x \n", nVersion);
  
  if (nVersion == DALCHIPINFO_VERSION(umajor,uminor)) {
  AsciiPrint(PRINTSIG"ChipInfo: Get Chip Version Test successful.\n");
  }
  else {
	  AsciiPrint(PRINTSIG"ChipInfo: Get Chip Version Test unsuccessful.\n");
	  AsciiPrint(PRINTSIG"ChipInfo: Get Chip Version Test unsuccessful.\n ");
	  return(TF_RESULT_CODE_FAILURE);
  }
  return(TF_RESULT_CODE_SUCCESS);
}

UINT32 ChipInfoUEFITest_GetRawChipVersion(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_ERROR;
	DalDeviceHandle           *chipInfoHandle = NULL;
	UINT32                    nRawVersion;

	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO,&chipInfoHandle);
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	eResult = DalChipInfo_GetRawChipVersion(chipInfoHandle, &nRawVersion);
	AsciiPrint(PRINTSIG"ChipInfo:  Raw Chip Version :%x \n", nRawVersion);
  
	if(eResult == DAL_SUCCESS)
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Version Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Raw Chip Version Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);
}

UINT32 ChipInfoUEFITest_GetChipId(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_SUCCESS;
	DalDeviceHandle           *chipInfoHandle = NULL;
	DalChipInfoIdType         eId;
	CHAR8   *szTargetName;
	szTargetName    = pszArg[0]; 
	
	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);	    
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	DalChipInfo_GetChipId(chipInfoHandle, &eId);
	AsciiPrint(PRINTSIG"ChipInfo:  Chip Id:%d \n", eId);
	
	if(AsciiStrnCmp (szTargetName, "MSM8974", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8974) 
			eResult = DAL_ERROR;
	}
	
	else if(AsciiStrnCmp (szTargetName, "MSM8974AA", 9) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8974_AA)
			eResult = DAL_ERROR;
	}
	
	else if(AsciiStrnCmp (szTargetName, "MSM8974AB", 9) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8974_AB)
			eResult = DAL_ERROR;
	}
  
	else if(AsciiStrnCmp (szTargetName, "MSM8974PRO", 10) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8974_PRO)
			eResult = DAL_ERROR;
	}
  
	else if (AsciiStrnCmp (szTargetName, "MSM8626", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8626)
			eResult = DAL_ERROR;
	}
  
	else if (AsciiStrnCmp (szTargetName, "MSM8610", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8610)
			eResult = DAL_ERROR;
	}
  
	else if (AsciiStrnCmp (szTargetName, "MSM8612", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8612)
			eResult = DAL_ERROR;
	}
  
	else if (AsciiStrnCmp (szTargetName, "MSM8962", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8962)
			eResult = DAL_ERROR;
  
	}

	else if (AsciiStrnCmp (szTargetName, "APQ8084", 7) == 0) {
		if (eId != DALCHIPINFO_ID_APQ8084)
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8994", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8994)
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8916", 7) == 0) {
		if (eId != DALCHIPINFO_ID_MSM8916)
			eResult = DAL_ERROR;
	}
  
	else
		eResult = DAL_ERROR;

	if (eResult == DAL_SUCCESS)
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Id Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Id Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}

	return(TF_RESULT_CODE_SUCCESS);	
}

UINT32 ChipInfoUEFITest_GetRawChipId(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_ERROR;
	DalDeviceHandle           *chipInfoHandle = NULL;
	UINT32                    nId;

	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);
			    
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	eResult =  DalChipInfo_GetRawChipId(chipInfoHandle, &nId);
	AsciiPrint(PRINTSIG"ChipInfo:  Raw ChipId:0x%x \n", nId);
  
	if(eResult == DAL_SUCCESS)
		AsciiPrint(PRINTSIG"ChipInfo: Get Raw Chip Id Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Raw Chip Id Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);
}

UINT32 ChipInfoUEFITest_GetChipIdString(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_SUCCESS;
	DalDeviceHandle           *chipInfoHandle = NULL;
	CHAR8                      szIdString[DALCHIPINFO_MAX_ID_LENGTH];
	CHAR8   *szTargetName;
	szTargetName    = pszArg[0];    

	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);			    
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	DalChipInfo_GetChipIdString(chipInfoHandle, (CHAR8*)&szIdString, DALCHIPINFO_MAX_ID_LENGTH);
	AsciiPrint(PRINTSIG"ChipInfo:  ChipIdString:%a \n", szIdString);  
	if(AsciiStrCmp (szTargetName, szIdString) == 0) 
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Id String Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Id String Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);
}

UINT32 ChipInfoUEFITest_GetChipFamily(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_SUCCESS;
	DalDeviceHandle           *chipInfoHandle = NULL;
	DalChipInfoFamilyType     eFamily;
	CHAR8   *szTargetName;
	szTargetName    = pszArg[0];    

	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);			    
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	DalChipInfo_GetChipFamily(chipInfoHandle, &eFamily);
	AsciiPrint(PRINTSIG"ChipInfo:  ChipFamily:0x%x \n", eFamily);  

	if(AsciiStrnCmp (szTargetName, "MSM8974", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8974)
			eResult = DAL_ERROR;
	}

	else if(AsciiStrnCmp (szTargetName, "MSM8974PRO", 10) == 0 || AsciiStrnCmp (szTargetName, "MSM8974AA", 9) == 0 || AsciiStrnCmp (szTargetName, "MSM8974AB", 9) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8974_PRO)
			eResult = DAL_ERROR;
	}

	else if (AsciiStrnCmp (szTargetName, "MSM8626", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8x26)
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8610", 7) == 0 || AsciiStrnCmp (szTargetName, "MSM8612", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8x10)
			eResult = DAL_ERROR;
	}

	else if (AsciiStrnCmp (szTargetName, "MSM8962", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8x62)
			eResult = DAL_ERROR;
	}

	else if (AsciiStrnCmp (szTargetName, "APQ8084", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_APQ8x94)
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8994", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8994)
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8916", 7) == 0)
	{
		if (eFamily != DALCHIPINFO_FAMILY_MSM8916)
			eResult = DAL_ERROR;
	}

	else
		eResult = DAL_ERROR;

	if (eResult == DAL_SUCCESS)
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Family Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Family Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);	
}


UINT32 ChipInfoUEFITest_GetModemSupport(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_SUCCESS;
	DalDeviceHandle           *chipInfoHandle = NULL;
	DalChipInfoModemType      nModem;
	CHAR8   *szTargetName;
	szTargetName    = pszArg[0];    

	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);

	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	DalChipInfo_GetModemSupport(chipInfoHandle, &nModem);
	AsciiPrint(PRINTSIG"ChipInfo:  GetModemSupport:0x%x \n", nModem); 

	if(AsciiStrnCmp (szTargetName, "MSM8974", 7) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) ) 
			eResult = DAL_ERROR;
	}

	else if(AsciiStrnCmp (szTargetName, "MSM8974PRO", 10) == 0 || AsciiStrnCmp (szTargetName, "MSM8974AA", 9) == 0 || AsciiStrnCmp (szTargetName, "MSM8974AB", 9) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) )
			eResult = DAL_ERROR;
	}

	else if (AsciiStrnCmp (szTargetName, "MSM8626", 7) == 0 || AsciiStrnCmp (szTargetName, "MSM8926", 7) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) )
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8610", 7) == 0 || AsciiStrnCmp (szTargetName, "MSM8612", 7) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) )
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8994", 7) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) )
			eResult = DAL_ERROR;
	}
	
	else if (AsciiStrnCmp (szTargetName, "MSM8916", 7) == 0)
	{
		if (nModem != (DALCHIPINFO_MODEM_WWAN | DALCHIPINFO_MODEM_CGPS | DALCHIPINFO_MODEM_LTE) )
			eResult = DAL_ERROR;
	}

	else
		eResult = DAL_ERROR;

	if (eResult == DAL_SUCCESS)
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Modem Support Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Chip Modem Support Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);	
}

UINT32 ChipInfoUEFITest_GetProcessorNameString(UINT32 dwArg, CHAR8 *pszArg[])
{
	DALResult           	  eResult  = DAL_SUCCESS;
	DalDeviceHandle           *chipInfoHandle = NULL;
	CHAR8                      szProcNameString[DALCHIPINFO_MAX_NAME_LENGTH];    
	CHAR8   *szProcName;
	szProcName    = pszArg[0]; 
	eResult = DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &chipInfoHandle);			    
	if(eResult != DAL_SUCCESS)
	{
		AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
		return(TF_RESULT_CODE_FAILURE);
	}
  
	DalChipInfo_GetProcessorNameString(chipInfoHandle, (CHAR8*)&szProcNameString,  DALCHIPINFO_MAX_NAME_LENGTH);
	AsciiPrint(PRINTSIG"ChipInfo:  ChipProcNameString:%a \n", szProcNameString);  
	if(AsciiStrCmp (szProcName, szProcNameString) == 0) 
		AsciiPrint(PRINTSIG"ChipInfo: Get Processor Name String Test successful.\n");
	else {
		AsciiPrint(PRINTSIG"ChipInfo: Get Processor Name String Test unsuccessful.\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	return(TF_RESULT_CODE_SUCCESS);
}


CONST TF_HelpDescr ChipInfoUEFITest_GetChipVersionHelp =
{
	"ChipInfoUEFITest_GetChipVersion",
    sizeof(ChipInfoUEFITest_GetChipVersionParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetChipVersionParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetChipVersionTest =
{
	PRINTSIG"ChipInfoUEFITest_GetChipVersion",
	ChipInfoUEFITest_GetChipVersion,
	&ChipInfoUEFITest_GetChipVersionHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetRawChipVersionHelp =
{
    "ChipInfoUEFITest_GetRawChipVersion",
    sizeof(ChipInfoUEFITest_GetRawChipVersionParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetRawChipVersionParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetRawChipVersionTest =
{
	PRINTSIG"ChipInfoUEFITest_GetRawChipVersion",
	ChipInfoUEFITest_GetRawChipVersion,
	&ChipInfoUEFITest_GetRawChipVersionHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetChipIdHelp =
{
    "ChipInfoUEFITest_GetChipId",
    sizeof(ChipInfoUEFITest_GetChipIdParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetChipIdParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetChipIdTest =
{
	PRINTSIG"ChipInfoUEFITest_GetChipId",
	ChipInfoUEFITest_GetChipId,
	&ChipInfoUEFITest_GetChipIdHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetRawChipIdHelp =
{
    "ChipInfoUEFITest_GetRawChipId",
    sizeof(ChipInfoUEFITest_GetRawChipIdParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetRawChipIdParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetRawChipIdTest =
{
	PRINTSIG"ChipInfoUEFITest_GetRawChipId",
	ChipInfoUEFITest_GetRawChipId,
	&ChipInfoUEFITest_GetRawChipIdHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetChipIdStringHelp =
{
    "ChipInfoUEFITest_GetChipIdString",
    sizeof(ChipInfoUEFITest_GetChipIdStringParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetChipIdStringParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetChipIdStringTest =
{
	PRINTSIG"ChipInfoUEFITest_GetChipIdString",
	ChipInfoUEFITest_GetChipIdString,
	&ChipInfoUEFITest_GetChipIdStringHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetChipFamilyHelp =
{
    "ChipInfoUEFITest_GetChipFamily",
    sizeof(ChipInfoUEFITest_GetChipFamilyParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetChipFamilyParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetChipFamilyTest =
{
	PRINTSIG"ChipInfoUEFITest_GetChipFamily",
	ChipInfoUEFITest_GetChipFamily,
	&ChipInfoUEFITest_GetChipFamilyHelp,
	&context1,
	0
};


CONST TF_HelpDescr ChipInfoUEFITest_GetModemSupportHelp =
{
    "ChipInfoUEFITest_GetModemSupport",
    sizeof(ChipInfoUEFITest_GetModemSupportParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetModemSupportParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetModemSupportTest =
{
	PRINTSIG"ChipInfoUEFITest_GetModemSupport",
	ChipInfoUEFITest_GetModemSupport,
	&ChipInfoUEFITest_GetModemSupportHelp,
	&context1,
	0
};

CONST TF_HelpDescr ChipInfoUEFITest_GetProcessorNameStringHelp =
{
    "ChipInfoUEFITest_GetProcessorNameString",
    sizeof(ChipInfoUEFITest_GetProcessorNameStringParam) / sizeof(TF_ParamDescr),
    ChipInfoUEFITest_GetProcessorNameStringParam
};

CONST TF_TestFunction_Descr ChipInfoUEFITest_GetProcessorNameStringTest =
{
   PRINTSIG"ChipInfoUEFITest_GetProcessorNameString",
   ChipInfoUEFITest_GetProcessorNameString,
   &ChipInfoUEFITest_GetProcessorNameStringHelp,
   &context1,
   0
};

CONST TF_TestFunction_Descr* ChipInfo_daltf_tests[] = {
	&ChipInfoUEFITest_GetChipVersionTest,
	&ChipInfoUEFITest_GetRawChipVersionTest,
	&ChipInfoUEFITest_GetChipIdTest,
	&ChipInfoUEFITest_GetRawChipIdTest,
	&ChipInfoUEFITest_GetChipIdStringTest,
	&ChipInfoUEFITest_GetChipFamilyTest,
	&ChipInfoUEFITest_GetModemSupportTest,
    &ChipInfoUEFITest_GetProcessorNameStringTest
};

DALResult ChipInfo_daltf_init(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(ChipInfo_daltf_tests) / sizeof(ChipInfo_daltf_tests[0]));
	
	dalResult = tests_daltf_add_tests(ChipInfo_daltf_tests, TF_NumberTests);
	return dalResult;
}

DALResult ChipInfo_daltf_deinit(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(ChipInfo_daltf_tests) / sizeof(ChipInfo_daltf_tests[0]));
	
	dalResult = tests_daltf_remove_tests(ChipInfo_daltf_tests, TF_NumberTests);
	return dalResult;
}




