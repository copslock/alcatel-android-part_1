/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ChipInfo_tests.h
  DESCRIPTION:	Header File for all UEFI APT PlatformInfo Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _SYSTEMDRIVERS_DALTF_PLATFORMINFO_TEST_
#define _SYSTEMDRIVERS_DALTF_PLATFORMINFO_TEST_

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "SysDrvTest.h"
#include "DDIPlatformInfo.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/*=========================================================================
      Constants and Macros 
==========================================================================*/

/*Shifting the major number by 16 bits to 
  the left to generate the version number*/
#define TEST_PLATFORMINFO_PLATFORM_VER_NO(major, minor)  (((major) * 65536) | (minor))

/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/
//extern CONST TF_ContextDescr context1;
//UINT32 atoint (CHAR8 *st);

DALResult systemdrivers_daltf_init_platforminfo(VOID);
DALResult systemdrivers_daltf_deinit_platforminfo(VOID);

#endif // _SYSTEMDRIVERS_DALTF_PLATFORMINFO_TEST_

