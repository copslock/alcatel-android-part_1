/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrvTest.c
  DESCRIPTION:	Main entry file for all SysDrv Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  02/13/15   nc      Changes to account for new DAL-TF command handler
  05/17/14   nc      Created
================================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "SysDrvTest.h"

/* ============================================================================
**  Function : SysDrvInitTestFunc
** ========================================================================== */
/*!
   @brief
   An init DalTF test case.  Add driver SysDrv tests into DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 SysDrvInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Add SysDrv tests\n");
   
   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
    	  if ( DAL_SUCCESS != ClockRegime_daltf_init())
    	  {
    	    AsciiPrint(PRINTSIG"ClockRegime_daltf_init failed\n");
    	    return 1;
    	  }


    	  if ( DAL_SUCCESS != ChipInfo_daltf_init())
    	  {
    	     AsciiPrint(PRINTSIG"ChipInfo_daltf_init failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != TLMM_daltf_init())
    	  {
    	     AsciiPrint(PRINTSIG"TLMM_daltf_init failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != systemdrivers_daltf_init_platforminfo())
    	  {
    	     AsciiPrint(PRINTSIG"systemdrivers_daltf_init_platforminfo failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != systemdrivers_daltf_init_dal_intctl())
    	  {
    	     AsciiPrint(PRINTSIG"systemdrivers_daltf_init_dal_intctl failed\n");
    	     return 1;
    	  }
      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// SysDrv init test's Parameters
static const TF_ParamDescr SysDrvInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - add all SysDrv test"}
};
// SysDrv Init Test's Help Data
static const TF_HelpDescr SysDrvInitTestHelp =
{
    "SysDrv Init",
    sizeof(SysDrvInitTestParam) / sizeof(TF_ParamDescr),
    SysDrvInitTestParam
};


const TF_TestFunction_Descr SysDrvInitTest =
{
   PRINTSIG"SysDrvInitTest",
   SysDrvInitTestFunc,
   &SysDrvInitTestHelp,
   &context1
};

/* ============================================================================
**  Function : SysDrvDeInitTestFunc
** ========================================================================== */
/*!
   @brief
   A de-init DalTF test case.  Remove driver SysDrv tests from DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 SysDrvDeInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Remove SysDrv tests\n");

   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
    	  if ( DAL_SUCCESS != ClockRegime_daltf_deinit())
    	  {
    	    AsciiPrint(PRINTSIG"ClockRegime_daltf_deinit failed\n");
    	    return 1;
    	  }


    	  if ( DAL_SUCCESS != ChipInfo_daltf_deinit())
    	  {
    	     AsciiPrint(PRINTSIG"ChipInfo_daltf_deinit failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != TLMM_daltf_deinit())
    	  {
    	     AsciiPrint(PRINTSIG"TLMM_daltf_deinit failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != systemdrivers_daltf_deinit_platforminfo())
    	  {
    	     AsciiPrint(PRINTSIG"systemdrivers_daltf_deinit_platforminfo failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != systemdrivers_daltf_deinit_dal_intctl())
    	  {
    	     AsciiPrint(PRINTSIG"systemdrivers_daltf_deinit_dal_intctl failed\n");
    	     return 1;
    	  }

      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// SysDrv deinit test's Parameters
static const TF_ParamDescr SysDrvDeInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - remove all SysDrv test"}
};
// SysDrvDeInitTest's Help Data
static const TF_HelpDescr SysDrvDeInitTestHelp =
{
    "Sanity De-init",
    sizeof(SysDrvDeInitTestParam) / sizeof(TF_ParamDescr),
    SysDrvDeInitTestParam
};

const TF_TestFunction_Descr SysDrvDeInitTest =
{
   PRINTSIG"SysDrvDeInitTest",
   SysDrvDeInitTestFunc,
   &SysDrvDeInitTestHelp,
   &context1
};

