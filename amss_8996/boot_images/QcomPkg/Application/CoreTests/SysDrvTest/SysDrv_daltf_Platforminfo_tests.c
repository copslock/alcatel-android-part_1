/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_ChipInfo_tests.c
  DESCRIPTION:	Source File for all UEFI APT Platforminfo Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/


/*==========================================================================

                     INCLUDE FILES FOR TIMETICK DRIVER TESTS

==========================================================================*/

#include "SysDrv_daltf_Platforminfo_tests.h"
/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      Constants and Macros 
==========================================================================*/

// PlatformInfo driver tests' Parameters
CONST TF_ParamDescr platforminfoGetPltfInfoTestParam[] =
{
    {TF_PARAM_INT32, "PlatforminfoPlatformType", "0 - UNKNOWN \n"
                                                 "1 - SURF \n"
                                                 "2 - FFA \n"
                                                 "3 - FLUID \n"
                                                 "4 - FUSION \n"
                                                 "5 - OEM \n"
                                                 "6 - QT \n"
                                                 "7 - MDM MTP \n"
                                                 "8 - MSM MTP \n"
                                                 "9 - LIQUID \n"
                                                 "10 - DRAGON BOARD \n"
                                                 "15 - RUMI \n"
                                                 "16 - VIRTIO "},
    {TF_PARAM_UINT32, "PlatforminfoPlatformVersion", "0 - DEFAULT (MIN. H/W VERSION NUM)"},
};

CONST TF_ParamDescr platforminfoGetPltfTestParam[] =
{
    {TF_PARAM_INT32, "PlatforminfoPlatformType", "0 - UNKNOWN \n"
                                                 "1 - SURF \n"
                                                 "2 - FFA \n"
                                                 "3 - FLUID \n"
                                                 "4 - FUSION \n"
                                                 "5 - OEM \n"
                                                 "6 - QT \n"
                                                 "7 - MDM MTP \n"
                                                 "8 - MSM MTP \n"
                                                 "9 - LIQUID \n"
                                                 "10 - DRAGON BOARD \n"
                                                 "15 - RUMI \n"
                                                 "16 - VIRTIO "},
};

/*CONST TF_ParamDescr platforminfoGetFusedTestParam[] =
{
    {TF_PARAM_INT32, "PlatforminfoFusedChipId", "0 - NON FUSED CHIP \n"
                                                "1 - FUSED TO 8660"},
};*/

/*=========================================================================
      Functions
==========================================================================*/

//Sub functions called internally
UINT32 platforminfoTestOpen(DalDeviceHandle **hPlatforminfo);
UINT32 platforminfoTestClose(DalDeviceHandle *hPlatforminfo);

//input config functions
UINT32 platforminfoGetPlatformInfoTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                           DalPlatformInfoPlatformType *pExpPlatformType,
                                           UINT32 *pExpPlatformVersion);

UINT32 platforminfoGetPlatformTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                       DalPlatformInfoPlatformType *pExpPlatformType);

/*UINT32 platforminfoGetFusedChipTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                        DalChipInfoIdType *pExpChipInfoId);*/

//helper functions
UINT32 platforminfoGetPlatformInfoTest(DalDeviceHandle *hPlatforminfo,
                                       DalPlatformInfoPlatformType eExpPlatformType,
                                       UINT32 nExpPlatformVersion);

UINT32 platforminfoGetPlatformTest(DalDeviceHandle *hPlatforminfo,
                                   DalPlatformInfoPlatformType eExpPlatformType);

UINT32 platforminfoPlatformTest(DalPlatformInfoPlatformType eExpPlatformType);

/*UINT32 platforminfoGetFusedChipTest(DalDeviceHandle *hPlatforminfo,
                                   DalChipInfoIdType eExpChipInfoId);*/


/*==========================================================================

                         FUNCTIONS FOR MODULE

==========================================================================*/

/* ============================================================================
**  Function : sysdrvPlatforminfoAttach
** ============================================================================
*/
/*!
    @brief
    A platforminfo driver DalTF test case to test the DalPlatforminfo_Attach API.
    
    @details
    This function calls the platforminfoAttachTest function where the logic for
    testing the DalPlatforminfo_Attach API is implemented

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters
                                  
   
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure
    
    @sa None
*/

UINT32 sysdrvPlatforminfoAttach(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hPlatforminfo = NULL;

   AsciiPrint(PRINTSIG"Entering sysdrvPlatforminfoAttach function");

   /*This function connects to platform info device and returns a handle*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestOpen(&hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the platform info device*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"Test Passed");       
   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvPlatforminfoAttachTest's Help Data
CONST TF_HelpDescr sysdrvPlatforminfoAttachHelp =
{
   "sysdrvPlatforminfoAttach", 0, NULL
};


CONST TF_TestFunction_Descr sysdrvPlatforminfoAttachTest =
{
   PRINTSIG"sysdrvPlatforminfoAttach",
   sysdrvPlatforminfoAttach,
   &sysdrvPlatforminfoAttachHelp,
   &context1,
   0
};


/* ============================================================================
**  Function : sysdrvPlatforminfoGetPlatformInfo
** ============================================================================
*/
/*!
    @brief
    A platforminfo driver DalTF test case to test the DalPlatforminfo_GetPlatformInfo API.
    
    @details
    This function calls the platforminfoGetPlatformInfoTest function where the logic for
    testing the DalPlatforminfo_GetPlatformInfo API is implemented

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters
                                  
   
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure
            returns TF_RESULT_CODE_BAD_PARAM on Bad Params
    
    @sa None
*/

UINT32 sysdrvPlatforminfoGetPlatformInfo(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hPlatforminfo = NULL;
   DalPlatformInfoPlatformType eExpPlatformType;
   UINT32 nExpPlatformVersion = 0;
   UINT32 testResult;

   AsciiPrint(PRINTSIG"Entering sysdrvPlatforminfoGetPlatformInfo function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = platforminfoGetPlatformInfoTestInit(dwArg,pszArg,&eExpPlatformType,&nExpPlatformVersion);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to platform info device and returns a handle*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestOpen(&hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }


   /*This function verifies the platform information returned by API*/
   testResult = platforminfoGetPlatformInfoTest(hPlatforminfo,eExpPlatformType,nExpPlatformVersion);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the platform info device*/
      if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the platform info device*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }
       
   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvPlatforminfoGetPlatformInfoTest's Help Data
CONST TF_HelpDescr sysdrvPlatforminfoGetPlatformInfoHelp =
{
   "sysdrvPlatforminfoGetPlatformInfo",
   sizeof(platforminfoGetPltfInfoTestParam) / sizeof(TF_ParamDescr), 
   platforminfoGetPltfInfoTestParam
};


CONST TF_TestFunction_Descr sysdrvPlatforminfoGetPlatformInfoTest =
{
   PRINTSIG"sysdrvPlatforminfoGetPlatformInfo",
   sysdrvPlatforminfoGetPlatformInfo,
   &sysdrvPlatforminfoGetPlatformInfoHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvPlatforminfoGetPlatform
** ============================================================================
*/
/*!
    @brief
    A platforminfo driver DalTF test case to test the DalPlatforminfo_GetPlatform API.
    
    @details
    This function calls the platforminfoGetPlatformTest function where the logic for
    testing the DalPlatforminfo_GetPlatform API is implemented

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters
                                  
   
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure
            returns TF_RESULT_CODE_BAD_PARAM on Bad Params
    
    @sa None
*/

UINT32 sysdrvPlatforminfoGetPlatform(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hPlatforminfo = NULL;
   DalPlatformInfoPlatformType eExpPlatformType;
   UINT32 testResult;

   AsciiPrint(PRINTSIG"Entering sysdrvPlatforminfoGetPlatform function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = platforminfoGetPlatformTestInit(dwArg,pszArg,&eExpPlatformType);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function connects to platform info device and returns a handle*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestOpen(&hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }

   /*This function verifies the platform information returned by API*/
   testResult = platforminfoGetPlatformTest(hPlatforminfo,eExpPlatformType);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      /*This function detaches the handle from the platform info device*/
      if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   /*This function detaches the handle from the platform info device*/
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }
       
   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvPlatforminfoGetPlatformTest's Help Data
CONST TF_HelpDescr sysdrvPlatforminfoGetPlatformHelp =
{
   "sysdrvPlatforminfoGetPlatform",
   sizeof(platforminfoGetPltfTestParam) / sizeof(TF_ParamDescr), 
   platforminfoGetPltfTestParam
};


CONST TF_TestFunction_Descr sysdrvPlatforminfoGetPlatformTest =
{
   PRINTSIG"sysdrvPlatforminfoGetPlatform",
   sysdrvPlatforminfoGetPlatform,
   &sysdrvPlatforminfoGetPlatformHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvPlatforminfoPlatform
** ============================================================================
*/
/*!
    @brief
    A platforminfo driver DalTF test case to test the DalPlatforminfo_Platform API.
    
    @details
    This function calls the platforminfoPlatformTest function where the logic for
    testing the DalPlatforminfo_Platform API is implemented

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters
                                  
   
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure
            returns TF_RESULT_CODE_BAD_PARAM on Bad Params
    
    @sa None
*/

UINT32 sysdrvPlatforminfoPlatform(UINT32 dwArg, CHAR8* pszArg[])
{
   DalPlatformInfoPlatformType eExpPlatformType;
   UINT32 testResult;

   AsciiPrint(PRINTSIG"Entering sysdrvPlatforminfoPlatform function");

   /*This function verifies for bad and out of range input parameters*/
   testResult = platforminfoGetPlatformTestInit(dwArg,pszArg,&eExpPlatformType);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*This function verifies the platform information returned by API*/
   testResult = platforminfoPlatformTest(eExpPlatformType);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_FAILURE;
   }       
   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvPlatforminfoPlatformTest's Help Data
CONST TF_HelpDescr sysdrvPlatforminfoPlatformHelp =
{
   "sysdrvPlatforminfoPlatform",
   sizeof(platforminfoGetPltfTestParam) / sizeof(TF_ParamDescr), 
   platforminfoGetPltfTestParam
};


CONST TF_TestFunction_Descr sysdrvPlatforminfoPlatformTest =
{
   PRINTSIG"sysdrvPlatforminfoPlatform",
   sysdrvPlatforminfoPlatform,
   &sysdrvPlatforminfoPlatformHelp,
   &context1,
   0
};

/* ============================================================================
**  Function : sysdrvPlatforminfoGetFusedChip
** ============================================================================
*/
/*!
    @brief
    A platforminfo driver DalTF test case to test the DalPlatforminfo_GetFusedChip API.
    
    @details
    This function calls the platforminfoGetFusedChipTest function where the logic for
    testing the DalPlatforminfo_GetFusedChip API is implemented

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters
                                  
   
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure
            returns TF_RESULT_CODE_BAD_PARAM on Bad Params
    
    @sa None
*/

/*UINT32 sysdrvPlatforminfoGetFusedChip(UINT32 dwArg, CHAR8* pszArg[])
{
   STATIC DalDeviceHandle *hPlatforminfo = NULL;
   DalChipInfoIdType eExpChipInfoId;
   UINT32 testResult;
   
   AsciiPrint(PRINTSIG"Entering sysdrvPlatforminfoGetFusedChip function");

   //This function verifies for bad and out of range input parameters
   testResult = platforminfoGetFusedChipTestInit(dwArg,pszArg,&eExpChipInfoId);
   if(TF_RESULT_CODE_SUCCESS != testResult) {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   //This function connects to platform info device and returns a handle
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestOpen(&hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }


   //This function verifies the fused chip information returned by API
   testResult = platforminfoGetFusedChipTest(hPlatforminfo,eExpChipInfoId);
   if(TF_RESULT_CODE_SUCCESS != testResult) {

      //This function detaches the handle from the platform info device
      if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
         return TF_RESULT_CODE_FAILURE;
      }
      return TF_RESULT_CODE_FAILURE;
   }

   //This function detaches the handle from the platform info device
   if(TF_RESULT_CODE_SUCCESS != platforminfoTestClose(hPlatforminfo)) {
       return TF_RESULT_CODE_FAILURE;
   }
       
   return TF_RESULT_CODE_SUCCESS;
}


// sysdrvPlatforminfoGetFusedChipTest's Help Data
CONST TF_HelpDescr sysdrvPlatforminfoGetFusedChipHelp =
{
   "sysdrvPlatforminfoGetFusedChip",
   sizeof(platforminfoGetFusedTestParam) / sizeof(TF_ParamDescr), 
   platforminfoGetFusedTestParam
};


CONST TF_TestFunction_Descr sysdrvPlatforminfoGetFusedChipTest =
{
   PRINTSIG"sysdrvPlatforminfoGetFusedChip",
   sysdrvPlatforminfoGetFusedChip,
   &sysdrvPlatforminfoGetFusedChipHelp,
   &context1
};*/


/* INPUT PARAM CHECK FUNCTIONS*/

/*==========================================================================

  FUNCTION platforminfoGetPlatformInfoTestInit

  DESCRIPTION
  This function verifies the input parameters for Platform Info driver
  GetPlatformInfo API test.  

  PARAMETERS
  dwArg              [IN]  number of parameters
  pszArg             [IN]  testing parameters 
  pExpPlatformType   [IN]  expected platform type pointer
  pPlatformVersion   [IN]  expected platform version pointer

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_BAD_PARAM on entering bad parameters

  SIDE EFFECTS
  None

==========================================================================*/
UINT32 platforminfoGetPlatformInfoTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                           DalPlatformInfoPlatformType *pExpPlatformType,
                                           UINT32 *pExpPlatformVersion)
{

   //CHAR8 *pStrToLongEnd  = NULL;
   int nParamFlag = 0;
   UINT32 nParamFlag1 = 0;

   UINT32 exParmCount = sizeof(platforminfoGetPltfInfoTestParam) / sizeof(TF_ParamDescr);
   if ( dwArg != exParmCount ){
      AsciiPrint(PRINTSIG"Number of input parameters incorrect: exp %d got %d", exParmCount, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /* verify platforminfo driver GetPlatformInfo API's test input parameters*/
  /* nParamFlag = (int)strtol(pszArg[0], &pStrToLongEnd, 10);
   if (NULL != *pStrToLongEnd){
      AsciiPrint(PRINTSIG"Bad Parameter on PlatforminfoPlatformType");
      return TF_RESULT_CODE_BAD_PARAM;
   } */

   nParamFlag = atoint(pszArg[0]);
   
   /* verify if parameters are out of permissible range*/
   if(0 > nParamFlag || DALPLATFORMINFO_NUM_TYPES <= nParamFlag){
      AsciiPrint(PRINTSIG"Parameter out of range on PlatforminfoPlatformType");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /* verify platforminfo driver GetPlatformInfo API's test input parameters*/
   /*nParamFlag1 = (UINT32) strtol(pszArg[1], &pStrToLongEnd, 10);
   if (NULL != *pStrToLongEnd){
      AsciiPrint(PRINTSIG"Bad Parameter on PlatforminfoPlatformVersion");
      return TF_RESULT_CODE_BAD_PARAM;
   }*/
   
   nParamFlag1 = atoint(pszArg[1]);

   /* verify if parameters are out of permissible range*/
   if(0 != nParamFlag1){
      AsciiPrint(PRINTSIG"Parameter out of range on PlatforminfoPlatformVersion");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   *pExpPlatformType = (DalPlatformInfoPlatformType)nParamFlag;
   *pExpPlatformVersion = TEST_PLATFORMINFO_PLATFORM_VER_NO(nParamFlag1, 0);

   AsciiPrint(PRINTSIG"PlatformType Expected: %d", *pExpPlatformType);
   AsciiPrint(PRINTSIG"Minimum PlatformVersion Expected: %u", *pExpPlatformVersion);

   return TF_RESULT_CODE_SUCCESS;
}

/*==========================================================================

  FUNCTION platforminfoGetPlatformTestInit

  DESCRIPTION
  This function verifies the input parameters for Platform Info driver
  GetPlatform and Platform APIs test.   

  PARAMETERS
  dwArg              [IN]  number of parameters
  pszArg             [IN]  testing parameters 
  pExpPlatformType   [IN]  expected platform type pointer

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_BAD_PARAM on entering bad parameters

  SIDE EFFECTS
  None

==========================================================================*/
UINT32 platforminfoGetPlatformTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                       DalPlatformInfoPlatformType *pExpPlatformType)
{

  // CHAR8 *pStrToLongEnd  = NULL;
   int nParamFlag = 0;

   UINT32 exParmCount = sizeof(platforminfoGetPltfTestParam) / sizeof(TF_ParamDescr);
   if ( dwArg != exParmCount ){
      AsciiPrint(PRINTSIG"Number of input parameters incorrect: exp %d got %d", exParmCount, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /* verify platforminfo driver GetPlatform and Platform APIs test input parameters*/
   /*nParamFlag = (int)strtol(pszArg[0], &pStrToLongEnd, 10);
   if (NULL != *pStrToLongEnd){
      AsciiPrint(PRINTSIG"Bad Parameter on PlatforminfoPlatformType");
      return TF_RESULT_CODE_BAD_PARAM;
   }*/

   nParamFlag = atoint(pszArg[0]);
   
   /* verify if parameters are out of permissible range*/
   if(0 > nParamFlag || DALPLATFORMINFO_NUM_TYPES <= nParamFlag){
      AsciiPrint(PRINTSIG"Parameter out of range on PlatforminfoPlatformType");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   *pExpPlatformType = (DalPlatformInfoPlatformType)nParamFlag;

   AsciiPrint(PRINTSIG"PlatformType Expected: %d", *pExpPlatformType);

   return TF_RESULT_CODE_SUCCESS;
}

/*==========================================================================

  FUNCTION platforminfoGetFusedChipTestInit

  DESCRIPTION
  This function verifies the input parameters for Platform Info driver
  GetFusedChip API test.    

  PARAMETERS
  dwArg              [IN]  number of parameters
  pszArg             [IN]  testing parameters 
  pExpChipInfoId     [IN]  expected fused chip info pointer

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_BAD_PARAM on entering bad parameters

  SIDE EFFECTS
  None

==========================================================================*/
/*UINT32 platforminfoGetFusedChipTestInit(UINT32 dwArg, CHAR8* pszArg[],
                                        DalChipInfoIdType *pExpChipInfoId)
{
   CHAR8 *pStrToLongEnd  = NULL;
   int nParamFlag = 0;

   int exParmCount = sizeof(platforminfoGetFusedTestParam) / sizeof(TF_ParamDescr);
   if ( dwArg != exParmCount ){
      AsciiPrint(PRINTSIG"Number of input parameters incorrect: exp %d got %d", exParmCount, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }

   // verify platforminfo driver GetPlatformInfo API's test input parameters
   nParamFlag = (int)strtol(pszArg[0], &pStrToLongEnd, 10);
   if (NULL != *pStrToLongEnd){
      AsciiPrint(PRINTSIG"Bad Parameter on PlatforminfoFusedChipId");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   // verify if parameters are out of permissible range
   if(0 != nParamFlag && 1 != nParamFlag){
      AsciiPrint(PRINTSIG"Parameter out of range on PlatforminfoFusedChipId");
      return TF_RESULT_CODE_BAD_PARAM;
   }

   if(nParamFlag == 1){
      *pExpChipInfoId = DALCHIPINFO_ID_MSM8660;
   }else{
      *pExpChipInfoId = DALCHIPINFO_ID_UNKNOWN;
   }

   AsciiPrint(PRINTSIG"ChipInfoId Expected: %d", *pExpChipInfoId);

   return TF_RESULT_CODE_SUCCESS;
}*/


/* HELPER FUNCTIONS*/

/*========================================================================== 
 
  FUNCTION platforminfoGetPlatformInfoTest

  DESCRIPTION
  This function is called to test the DalPlatforminfo_GetPlatformInfo API.

  PARAMETERS
  *hPlatforminfo       [IN]  platforminfo device handle
  eExpPlatformType     [IN]  expected platform type
  nExpPlatformVersion  [IN]  expected platform version

  DEPENDENCIES
  platforminfoTestOpen function must have been called successfully

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 platforminfoGetPlatformInfoTest(DalDeviceHandle *hPlatforminfo,
                                       DalPlatformInfoPlatformType eExpPlatformType,
                                       UINT32 nExpPlatformVersion)
{
   UINT32 bFlag = 1;
   DALResult eDalResult = DAL_SUCCESS;
   DalPlatformInfoPlatformInfoType sPlatformInfoType; 
   DalPlatformInfoPlatformType ePlatformType;

   UINT32 nPlatformVersion = 0;

   /* Null pointer exception check on platform info device handle */
   if(NULL == hPlatforminfo) {
      AsciiPrint(PRINTSIG"Null pointer exception on hPlatforminfo handle");
      return TF_RESULT_CODE_FAILURE;
   }
   
   /* Get platform info */
   eDalResult = DalPlatformInfo_GetPlatformInfo(hPlatforminfo,&sPlatformInfoType);
   if ( DAL_SUCCESS != eDalResult){
      AsciiPrint(PRINTSIG"DalPlatformInfo_GetPlatformInfo: Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   ePlatformType = sPlatformInfoType.platform;
   nPlatformVersion = sPlatformInfoType.version;

   AsciiPrint(PRINTSIG"PlatformType Returned: %d", ePlatformType);
   AsciiPrint(PRINTSIG"PlatformVersion Returned: %u", nPlatformVersion);

   /* Verify whether the p/f type returned by API is same as expected */
   if(eExpPlatformType != ePlatformType) {
      AsciiPrint(PRINTSIG"Error on Platform Type returned by API");
      bFlag = FALSE;
   }

   /* Verify whether the p/f version returned by API is same as expected */
   if(nExpPlatformVersion > nPlatformVersion) {
      AsciiPrint(PRINTSIG"Error on Platform Version returned by API");
      bFlag = FALSE;
   }

   if(0 == bFlag){
      return TF_RESULT_CODE_FAILURE;
   }
   return TF_RESULT_CODE_SUCCESS;
}

/*========================================================================== 
 
  FUNCTION platforminfoGetPlatformTest

  DESCRIPTION
  This function is called to test the DalPlatforminfo_GetPlatform API.

  PARAMETERS
  *hPlatforminfo    [IN]  platforminfo device handle
  eExpPlatformType  [IN]  expected platform type

  DEPENDENCIES
  platforminfoTestOpen function must have been called successfully

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 platforminfoGetPlatformTest(DalDeviceHandle *hPlatforminfo,DalPlatformInfoPlatformType eExpPlatformType)
{
   UINT32 bFlag = 1;
   DALResult eDalResult = DAL_SUCCESS;
   DalPlatformInfoPlatformType ePlatformType;

   /* Null pointer exception check on platform info device handle */
   if(NULL == hPlatforminfo) {
      AsciiPrint(PRINTSIG"Null pointer exception on hPlatforminfo handle");
      return TF_RESULT_CODE_FAILURE;
   }
   
   /* Get platform info */
   eDalResult = DalPlatformInfo_GetPlatform(hPlatforminfo,&ePlatformType);
   if ( DAL_SUCCESS != eDalResult){
      AsciiPrint(PRINTSIG"DalPlatformInfo_GetPlatform: Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"PlatformType Returned: %d", ePlatformType);

   /* Verify whether the p/f type returned by API is same as expected */
   if(eExpPlatformType != ePlatformType) {
      AsciiPrint(PRINTSIG"Error on Platform Type returned by API");
      bFlag = FALSE;
   }

   if(0 == bFlag){
      return TF_RESULT_CODE_FAILURE;
   }
   return TF_RESULT_CODE_SUCCESS;
}

/*========================================================================== 
 
  FUNCTION platforminfoPlatformTest

  DESCRIPTION
  This function is called to test the DalPlatforminfo_Platform API.

  PARAMETERS
  eExpPlatformType  [IN]  expected platform type

  DEPENDENCIES
  platforminfoTestOpen function must have been called successfully

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 platforminfoPlatformTest(DalPlatformInfoPlatformType eExpPlatformType)
{
   UINT32 bFlag = 1;
   DalPlatformInfoPlatformType ePlatformType;
   
   /* Get platform info */
   ePlatformType = DalPlatformInfo_Platform();

   AsciiPrint(PRINTSIG"PlatformType Returned: %d", ePlatformType);

   /* Verify whether the p/f type returned by API is same as expected */
   if(eExpPlatformType != ePlatformType) {
      AsciiPrint(PRINTSIG"Error on Platform Type returned by API");
      bFlag = FALSE;
   }

   if(0 == bFlag){
      return TF_RESULT_CODE_FAILURE;
   }
   return TF_RESULT_CODE_SUCCESS;
}

/*========================================================================== 
 
  FUNCTION platforminfoGetFusedChipTest

  DESCRIPTION
  This function is called to test the DalPlatforminfo_GetFusedChip API.

  PARAMETERS
  *hPlatforminfo    [IN]  platforminfo device handle
  eExpPlatformType  [IN]  expected platform type

  DEPENDENCIES
  platforminfoTestOpen function must have been called successfully

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

/*UINT32 platforminfoGetFusedChipTest(DalDeviceHandle *hPlatforminfo,DalChipInfoIdType eExpChipInfoId)
{
   boolean bFlag = TRUE;
   DALResult eDalResult = DAL_SUCCESS;
   DalChipInfoIdType eChipInfoId = 0;

   // Null pointer exception check on platform info device handle
   if(NULL == hPlatforminfo) {
      AsciiPrint(PRINTSIG"Null pointer exception on hPlatforminfo handle");
      return TF_RESULT_CODE_FAILURE;
   }
   
   // Get fused chip info from the API
   eDalResult = DalPlatformInfo_GetFusedChip(hPlatforminfo,&eChipInfoId);
   if ( DAL_SUCCESS != eDalResult){
      AsciiPrint(PRINTSIG"DalPlatformInfo_GetFusedChip: Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"ChipInfoId Returned: %d", eChipInfoId);

   // Verify whether the fused chip id returned by API is same as expected
   if(eExpChipInfoId != eChipInfoId) {
      AsciiPrint(PRINTSIG"Error on ChipInfo Id returned by API");
      bFlag = FALSE;
   }

   if(0 == bFlag){
      return TF_RESULT_CODE_FAILURE;
   }
   return TF_RESULT_CODE_SUCCESS;
}*/

/*==========================================================================

  FUNCTION platforminfoTestOpen

  DESCRIPTION
  This function connects to platforminfo DAL and gives the caller a handle to the
  DAL by calling the DalPlatforminfo_Attach API.

  PARAMETERS
  **hPlatforminfo    [IN]  pointer to the handle of the platforminfo device 

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 platforminfoTestOpen(DalDeviceHandle **hPlatforminfo)
{
   DALResult eDalResult = DAL_SUCCESS;

   /*Attach to platform info device and return a handle*/
   eDalResult = DAL_PlatformInfoDeviceAttach(DALDEVICEID_PLATFORMINFO, hPlatforminfo);
   if ( DAL_SUCCESS != eDalResult){
      AsciiPrint(PRINTSIG"DAL_PlatformInfoDeviceAttach: Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*==========================================================================

  FUNCTION platforminfoTestClose

  DESCRIPTION
  This function detaches the handle from the platforminfo dal device by calling
  DAL_DeviceDetach API.

  PARAMETERS
  *hPlatforminfo    [IN]  platforminfo device handle

  DEPENDENCIES
  None

  RETURN VALUE
  returns TF_RESULT_CODE_SUCCESS on Success
  returns TF_RESULT_CODE_FAILURE on Failure

  SIDE EFFECTS
  None

==========================================================================*/

UINT32 platforminfoTestClose(DalDeviceHandle *hPlatforminfo)
{
   DALResult eDalResult = DAL_SUCCESS;

   /*Detach the platform info device from the handle*/
   eDalResult = DAL_DeviceDetach(hPlatforminfo);
   if(DAL_SUCCESS != eDalResult) {
      AsciiPrint(PRINTSIG"DAL_DeviceDetach:Failed due to Dal result %d", eDalResult);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

/*----------------------------------------------------------------------------
  Global variables
----------------------------------------------------------------------------*/


CONST TF_TestFunction_Descr* PlatformInfo_daltf_tests[] = {
   &sysdrvPlatforminfoAttachTest,
   &sysdrvPlatforminfoGetPlatformInfoTest,
   &sysdrvPlatforminfoGetPlatformTest,
   &sysdrvPlatforminfoPlatformTest,
   //&sysdrvPlatforminfoGetFusedChipTest,
};

/*===========================================================================
  FUNCTION:  systemdrivers_daltf_init_platforminfo
===========================================================================*/

DALResult systemdrivers_daltf_init_platforminfo(VOID)
{
  DALResult dalResult = 0;

  UINT16 TF_NumberTests =
    (sizeof(PlatformInfo_daltf_tests) / sizeof(PlatformInfo_daltf_tests[0]));

  dalResult = tests_daltf_add_tests(PlatformInfo_daltf_tests, TF_NumberTests);
  return dalResult;
}



/*===========================================================================
  FUNCTION:  systemdrivers_daltf_deinit_platforminfo
===========================================================================*/

DALResult systemdrivers_daltf_deinit_platforminfo(VOID)
{
  DALResult dalResult = 0;

  UINT16 TF_NumberTests =
    (sizeof(PlatformInfo_daltf_tests) / sizeof(PlatformInfo_daltf_tests[0]));

  dalResult = tests_daltf_remove_tests(PlatformInfo_daltf_tests, TF_NumberTests);
  return dalResult;
}


