/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		SysDrv_daltf_TLMM_tests.c
  DESCRIPTION:	Source File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#include "SysDrv_daltf_TLMM_tests.h"
#ifndef DIAG_SUBSYS_COREBSP
#define DIAG_SUBSYS_COREBSP 1
#endif

/**
  Handle to the DAL TLMM Device context. 
 */
DalDeviceHandle * tlmm_handle;

CONST TF_ParamDescr TLMMUEFITest_ConfigGPIOParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"},
	{TF_PARAM_UINT32,   "TLMM Test HIGH/LOW write value",   "HIGH/LOW write value"}
};

CONST TF_ParamDescr TLMMUEFITest_ConfigGPIOGroupParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"}
};

CONST TF_ParamDescr TLMMUEFITest_GPIOReadWriteFeedbackParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"}
};

CONST TF_ParamDescr TLMMUEFITest_GetGPIONumberParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"}
};

CONST TF_ParamDescr TLMMUEFITest_GPIOGetOutputParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"}
};

/*CONST TF_ParamDescr TLMMUEFITest_GpioOutGroupParam[] =
{
	{TF_PARAM_UINT32,   "TLMM Test GPIO pin number",        "GPIO pin number: 0 - 172\n"}
};
*/
/*====================================================page t
** DATA **
====================================================*/

//CONST TF_ContextDescr context1 = { (uint8)DIAG_SUBSYS_COREBSP, 0 };



/************************************************************************************************/
/*=========================================================================
      Functions
==========================================================================*/

UINT32 TLMMUEFITest_ConfigGPIO(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioNum = 0;
  
  nGpioNum    = atoint(pszArg[0]);
  
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);
			    
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }

  DALGpioSignalType config = 
    (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);

  eResult = DalTlmm_ConfigGpio(TLMMHandle, config, DAL_TLMM_GPIO_ENABLE);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO.\n ");
	AsciiPrint (PRINTSIG"TLMM: Configure GPIO Test unsuccessful.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  else {
   AsciiPrint (PRINTSIG"TLMM: Configure GPIO Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}

UINT32 TLMMUEFITest_ConfigGPIOGroup(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioArr[] = {8, 12, 16, 27, 49, 92};
  UINT32 NumGpio = 6;
  DALGpioSignalType GpioConfigGroup[] = {(DAL_GPIO_CFG(nGpioArr[0], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[1], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[2], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[3], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[4], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[5], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA))}; 
	
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);
			    
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }
	eResult = DalTlmm_ConfigGpioGroup(TLMMHandle, DAL_TLMM_GPIO_ENABLE, GpioConfigGroup, NumGpio);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO Group.\n ");
	AsciiPrint (PRINTSIG"TLMM: Configure GPIO Group Test unsuccessful.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  else {
   AsciiPrint (PRINTSIG"TLMM: Configure GPIO Group Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}

UINT32 TLMMUEFITest_GPIOReadWriteFeedback(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioNum = 0;
  DALGpioValueType valueWrite;
  DALGpioValueType valueRead;
  UINT32 GPIOValue;
  nGpioNum    = atoint(pszArg[0]);
  GPIOValue = atoint(pszArg[1]);
  valueWrite = (GPIOValue>0) ? DAL_GPIO_HIGH_VALUE : DAL_GPIO_LOW_VALUE;	  
  
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);     
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }
  
   DALGpioSignalType config = (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);  

  eResult = DalTlmm_ConfigGpio(TLMMHandle, config, DAL_TLMM_GPIO_ENABLE);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO to output.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to configure the GPIO to output.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  eResult = DalTlmm_GpioOut(TLMMHandle, config, valueWrite);
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to write value to GPIO: Invalid GPIO  number.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to write value to GPIO: Invalid GPIO  number.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  config = (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_INPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);
	
  eResult = DalTlmm_ConfigGpio(TLMMHandle, config, DAL_TLMM_GPIO_ENABLE);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO to input.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to configure the GPIO to input.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  eResult = DalTlmm_GpioIn(TLMMHandle, config, &valueRead);
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to read value from GPIO: Invalid GPIO  number.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to read value from GPIO: Invalid GPIO  number.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  if(valueWrite != valueRead)
  {
    AsciiPrint(PRINTSIG"TLMM: Read-Write Feedback Test failed.\n ");
	AsciiPrint (PRINTSIG"TLMM: Read-Write Feedback Test failed.\n");
    return(TF_RESULT_CODE_FAILURE);
  }  
  else {
   AsciiPrint (PRINTSIG"TLMM: GPIO Read-Write Feedback Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}

UINT32 TLMMUEFITest_GetGPIONumber(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioNum = 0;
  UINT32 outGpioNum = 0; 
  
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);
			    
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }

  DALGpioSignalType config = 
    (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);

  eResult = DalTlmm_GetGpioNumber(TLMMHandle, config, &outGpioNum);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to get the GPIO number.\n ");
	AsciiPrint (PRINTSIG"TLMM: Get GPIO Number Test unsuccessful.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  else {
   AsciiPrint (PRINTSIG"TLMM: Get GPIO Number Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}

UINT32 TLMMUEFITest_GPIOGetOutput(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioNum = 0;
  DALGpioValueType valueWrite;
  DALGpioValueType valueGpioOutput;
  UINT32 GPIOValue;
  nGpioNum    = atoint(pszArg[0]);
  GPIOValue = atoint(pszArg[1]);
  valueWrite = (GPIOValue>0) ? DAL_GPIO_HIGH_VALUE : DAL_GPIO_LOW_VALUE;	  
  
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);     
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }
  
   DALGpioSignalType config = (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);  

  eResult = DalTlmm_ConfigGpio(TLMMHandle, config, DAL_TLMM_GPIO_ENABLE);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO to output.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to configure the GPIO to output.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  eResult = DalTlmm_GpioOut(TLMMHandle, config, valueWrite);
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to write value to GPIO: Invalid GPIO  number.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to write value to GPIO: Invalid GPIO  number.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  //config = (DALGpioSignalType)DAL_GPIO_CFG(nGpioNum, 0, DAL_GPIO_INPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA);
	
  eResult = DalTlmm_GetOutput(TLMMHandle, nGpioNum, &valueGpioOutput);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to retrieve the current output value of the GPIO.\n ");
	AsciiPrint (PRINTSIG"TLMM: Failed to retrieve the current output value of the GPIO.\n");
    return(TF_RESULT_CODE_FAILURE);
  }  
  
  if(valueWrite != valueGpioOutput)
  {
    AsciiPrint(PRINTSIG"TLMM: Get GPIO Output Test failed.\n ");
	AsciiPrint (PRINTSIG"TLMM: Get GPIO Output Test failed.\n");
    return(TF_RESULT_CODE_FAILURE);
  }  
  else {
   AsciiPrint (PRINTSIG"TLMM: Get GPIO Output Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}

/*UINT32 TLMMUEFITest_GpioOutGroup(UINT32 dwArg, CHAR8 *pszArg[])
{
  DALResult           	    eResult  = DAL_ERROR;
  DalDeviceHandle           *TLMMHandle = NULL;
  UINT32 nGpioArr[] = {8, 12, 16, 27, 49, 92};
  UINT32 NumGpio = 6;
  DALGpioSignalType GpioConfigGroup[] = {(DAL_GPIO_CFG(nGpioArr[0], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[1], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[2], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[3], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[4], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA)), (DAL_GPIO_CFG(nGpioArr[5], 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA))}; 
  DALGpioValueType valueWrite;
  UINT32 GPIOValue;
  GPIOValue = atoint(pszArg[0]);
  valueWrite = (GPIOValue>0) ? DAL_GPIO_HIGH_VALUE : DAL_GPIO_LOW_VALUE;
  
  eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &TLMMHandle);
			    
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to attach DAL. Exiting... \n ");
    return(TF_RESULT_CODE_FAILURE);
  }
	eResult = DalTlmm_ConfigGpioGroup(TLMMHandle, DAL_TLMM_GPIO_ENABLE, GpioConfigGroup, NumGpio);
  
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to configure the GPIO Group.\n ");
	AsciiPrint (PRINTSIG"TLMM: GPIO Out Group Test unsuccessful.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  eResult = DalTlmm_GpioOutGroup(TLMMHandle, GpioConfigGroup, NumGpio);
  if(eResult != DAL_SUCCESS)
  {
    AsciiPrint(PRINTSIG"Failed to write to GPIO Group.\n ");
	AsciiPrint (PRINTSIG"TLMM: GPIO Out Group Test unsuccessful.\n");
    return(TF_RESULT_CODE_FAILURE);
  }
  
  else {
   AsciiPrint (PRINTSIG"TLMM: Write Output GPIO Group Test successful.\n");
  }
  return(TF_RESULT_CODE_SUCCESS);
}
*/

CONST TF_HelpDescr TLMMUEFITest_ConfigGPIOHelp =
{
    "TLMMUEFITest_ConfigGPIO",
    sizeof(TLMMUEFITest_ConfigGPIOParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_ConfigGPIOParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_ConfigGPIOTest =
{
   PRINTSIG"TLMMUEFITest_ConfigGPIO",
   TLMMUEFITest_ConfigGPIO,
   &TLMMUEFITest_ConfigGPIOHelp,
   &context1,
   0
};

CONST TF_HelpDescr TLMMUEFITest_ConfigGPIOGroupHelp =
{
    "TLMMUEFITest_ConfigGPIOGroup",
    sizeof(TLMMUEFITest_ConfigGPIOGroupParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_ConfigGPIOGroupParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_ConfigGPIOGroupTest =
{
   PRINTSIG"TLMMUEFITest_ConfigGPIOGroup",
   TLMMUEFITest_ConfigGPIOGroup,
   &TLMMUEFITest_ConfigGPIOGroupHelp,
   &context1,
   0
};

CONST TF_HelpDescr TLMMUEFITest_GPIOReadWriteFeedbackHelp =
{
    "TLMMUEFITest_GPIOReadWriteFeedback",
    sizeof(TLMMUEFITest_GPIOReadWriteFeedbackParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_GPIOReadWriteFeedbackParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_GPIOReadWriteFeedbackTest =
{
   PRINTSIG"TLMMUEFITest_GPIOReadWriteFeedback",
   TLMMUEFITest_GPIOReadWriteFeedback,
   &TLMMUEFITest_GPIOReadWriteFeedbackHelp,
   &context1,
   0
};

CONST TF_HelpDescr TLMMUEFITest_GetGPIONumberHelp =
{
    "TLMMUEFITest_GetGPIONumber",
    sizeof(TLMMUEFITest_GetGPIONumberParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_GetGPIONumberParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_GetGPIONumberTest =
{
   PRINTSIG"TLMMUEFITest_GetGPIONumber",
   TLMMUEFITest_GetGPIONumber,
   &TLMMUEFITest_GetGPIONumberHelp,
   &context1,
   0
};

CONST TF_HelpDescr TLMMUEFITest_GPIOGetOutputHelp =
{
    "TLMMUEFITest_GPIOGetOutput",
    sizeof(TLMMUEFITest_GPIOGetOutputParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_GPIOGetOutputParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_GPIOGetOutputTest =
{
   PRINTSIG"TLMMUEFITest_GPIOGetOutput",
   TLMMUEFITest_GPIOGetOutput,
   &TLMMUEFITest_GPIOGetOutputHelp,
   &context1,
   0
};

/*CONST TF_HelpDescr TLMMUEFITest_GPIOOutGroupHelp =
{
    "TLMMUEFITest_GPIOOutGroup",
    sizeof(TLMMUEFITest_GPIOOutGroupParam) / sizeof(TF_ParamDescr),
    TLMMUEFITest_GPIOOutGroupParam
};

CONST TF_TestFunction_Descr TLMMUEFITest_GPIOOutGroupTest =
{
   PRINTSIG"TLMMUEFITest_GPIOOutGroup",
   TLMMUEFITest_GPIOOutGroup,
   &TLMMUEFITest_GPIOOutGroupHelp,
   &context1
};
*/
CONST TF_TestFunction_Descr* TLMM_daltf_tests[] = {
  &TLMMUEFITest_ConfigGPIOTest,
  &TLMMUEFITest_ConfigGPIOGroupTest,
  &TLMMUEFITest_GPIOReadWriteFeedbackTest,
  &TLMMUEFITest_GetGPIONumberTest,
  &TLMMUEFITest_GPIOGetOutputTest
  //&TLMMUEFITest_GPIOOutGroup
};

DALResult TLMM_daltf_init(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(TLMM_daltf_tests) / sizeof(TLMM_daltf_tests[0]));
	
	dalResult = tests_daltf_add_tests(TLMM_daltf_tests, TF_NumberTests);
	return dalResult;
}

DALResult TLMM_daltf_deinit(VOID)
{
	DALResult dalResult = 0;
	UINT16 TF_NumberTests = (sizeof(TLMM_daltf_tests) / sizeof(TLMM_daltf_tests[0]));
	
	dalResult = tests_daltf_remove_tests(TLMM_daltf_tests, TF_NumberTests);
	return dalResult;
}
