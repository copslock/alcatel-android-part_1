/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		HWEnginesTest.h
  DESCRIPTION:	Common headers included for all HWEngines files
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   jd      Created
================================================================================*/

#ifndef _HWEngines_TESTS_
#define _HWEngines_TESTS_

#include "tests_daltf_common.h"   /* Common definitions */

#include "DDIHWIO.h"
#include "DDIClock.h"

#ifdef USE_DIAG
#include "diagcmd.h"
#endif

#if 0
#define  CONSUMER_PIPE 0
#define  PRODUCER_PIPE 1
#ifdef BAM_MBA
#include "msmhwiobase.h"
#endif
#endif

#include <Protocol/EFIAdc.h>
#include <Protocol/EFITsens.h>

#include "hwengines_daltf_adc_tests.h"
#include "hwengines_daltf_tsens_tests.h"
//#include "hwengines_daltf_bam_tests.h"

#endif
