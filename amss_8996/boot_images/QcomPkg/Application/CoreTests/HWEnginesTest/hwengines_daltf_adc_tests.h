/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		hwengines_daltf_adc_tests.h
  DESCRIPTION:	Header File for all UEFI APT ADC Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   jd      Created
================================================================================*/

#ifndef _HWEngines_DALTF_ADC_TESTS_
#define _HWEngines_DALTF_ADC_TESTS_

#include "HWEnginesTest.h"
#include "AdcInputs.h"

/*====================================================
** STRUCTURES **
====================================================*/
typedef struct
{
  UINT8       uAdcInputCh;
  CHAR8        *aInputName;
  UINT8       uInputSize;
  INT32       uMin;
  INT32       uMax;
}AdcInputParam;

/*====================================================
** Support Functions **
====================================================*/
//Configures the ADC Input Channels
UINT32 configADCInputChannel
(UINT8 uAdcInputCh,
 AdcInputParam *pInputParam);

 EFI_STATUS LocateProtocol(EFI_ADC_PROTOCOL **);
 /*
 UINT32 ADCUEFIGetChannel (UINT32 dwArg, CHAR8* pszArg[]);
 UINT32 ADCUEFIRead(UINT32 dwArg, CHAR8 *pszArg[]);
 UINT32 ADCUEFIRecalibrateChannel(UINT32 dwArg, CHAR8 *pszArg[]);
 UINT32 ADCUEFISynchronousRead(UINT32 dwArg, CHAR8 *pszArg[]);*/
 
//Initialize adc daltf tests
DALResult adc_daltf_init(VOID);

//De-Initialize adc daltf tests
DALResult adc_daltf_deinit(VOID);

#endif
