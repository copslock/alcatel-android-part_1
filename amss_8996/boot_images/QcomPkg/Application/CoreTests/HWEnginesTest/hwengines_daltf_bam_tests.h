/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		hwengines_daltf_bam_tests.h
  DESCRIPTION:	Header File for all UEFI APT BAM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   jd      Created
================================================================================*/

#ifndef _HWEngines_DALTF_Bam_TESTS_
#define _HWEngines_DALTF_Bam_TESTS_

#include "HWEnginesTest.h"
#include <Library/UncachedMemoryAllocationLib.h>

#include "bam.h"

/*====================================================
** CONSTANTS **
====================================================*/

#define TEST_BAM_DISABLE_FLAG                   1

#define TEST_BAM_DISABLE_IRQ                    0
#define TEST_BAM_ENABLE_IRQ                     1

#define TEST_BAM_PIPE_STRESS_ITER               100

#define TEST_BAM_TEST                           0
#define TEST_BAM_BULK_TEST                      1


#define TEST_BAM_SRC_BUFFER_DEFAULT_VALUE       0xAA
#define TEST_BAM_DST_BUFFER_DEFAULT_VALUE       0xCC

#define TEST_BAM_BUF_SZ                         0x800 //0x800
#define TEST_BAM_DESC_FIFO_SZ                   0x800 //0x800
#define TEST_BAM_DATA_FIFO_SZ                   0x100 //0x100
#define TEST_BAM_RXBUF_OFFSET                   0
#define TEST_BAM_TXBUF_OFFSET                   TEST_BAM_BUF_SZ

#define TEST_BAM_PIPEDESCOFFSET(n) ((TEST_BAM_BUF_SZ * 2) + (TEST_BAM_DESC_FIFO_SZ*n))

#define TEST_BAM_PIPEDATAOFFSET TEST_BAM_PIPEDESCOFFSET(4)

#define TEST_BAM_BULK_BUF_SZ                    0x20000
#define TEST_BAM_BULK_DESC_FIFO_SZ              0x100

#define TEST_BAM_BULK_PIPEDESCOFFSET(n) ((TEST_BAM_BULK_BUF_SZ * 2) + (TEST_BAM_BULK_DESC_FIFO_SZ*n))

/*====================================================
** DATA **
====================================================*/
typedef long testVirtualAddr;
typedef long testPhysicalAddr;

/*====================================================
** STRUCTURES **
====================================================*/
/*Test Parameters*/
typedef struct 
{  
    testVirtualAddr         vAddress;   /* virtual address */
    testPhysicalAddr        pAddress;   /* physical address */
    UINT32            size;
} BamBufferAddress;

typedef struct
{
  BamBufferAddress txBuffer;
  BamBufferAddress rxBuffer;
} BamTxBuffer;

typedef struct 
{  
    testVirtualAddr         vAddress;   /* virtual address */
    testPhysicalAddr        pAddress;   /* physical address */
    UINT32            size;
	VOID 					*handle;
} BamBuffAddressNonDal;


/*====================================================
** Support Functions **
====================================================*/
/*
UINT32 BamChannelInit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamChannelReset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeInit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeDeinit (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeSetIRQMode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeGetFreeCount (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeDisable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeIsEmpty (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeTransferPoll (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeBulkTransferPoll(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamPipeStress(UINT32 dwArg, CHAR8* pszArg[]);
UINT32 BamGetCurrentDescriptorInfo(UINT32 dwArg, CHAR8* pszArg[]);
*/

//Initialize bam daltf tests
DALResult bam_daltf_init(VOID);
//DALResult bam_daltf_init(VOID);

//De-Initialize bam daltf tests
DALResult bam_daltf_deinit(VOID);

#endif
