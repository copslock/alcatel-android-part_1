/*==========================================================================

@file   mproc_daltf_common
 
@brief  MPROC Common Test Function file

                Copyright (c) 2011 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
=============================================================================*/
/*===========================================================================
EDIT HISTORY FOR FILE
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_common.c#2 $
 
when        who         what, where, why
--------    ---         ---------------------------------------------------- 
04/28/2015  pvadlama    Initial version. 
=============================================================================*/
#include "mproc_daltf_common.h"

/*=============================================================================
 
                                PUBLIC API's
===============================================================================*/

/* ============================================================================
**  Function : mprocTestUintToString
** ===========================================================================*/
/*
   @brief : convert integer to string
 
   @param arg[0]  - iNumber
   @param arg[1]  - pStringVal 

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)

*/
uint32 mprocTestUintToString(uint32 iNumber, char *pStringVal ) {
    uint32 iDigit;
    char cArray[10]; 
    uint32 iCount =0;
    uint32 iReverseCount = 0;

    if (NULL == pStringVal ) {
        AsciiPrint (PRINTSIG"Error: Destination Buffer Null");
        return TF_RESULT_CODE_FAILURE;
    }
    while (iNumber) {
        iDigit = iNumber%10;
        iNumber = iNumber/10;
        cArray[iCount] = (char)(iDigit + 48);
        iCount++;
    }
    cArray[iCount] = '\0';
    while ( cArray[iReverseCount] != '\0') {
        *(pStringVal+iCount-(iReverseCount+1)) = cArray[iReverseCount];
        iReverseCount++;
    }
    *(pStringVal+iReverseCount)= '\0';
    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : mprocTestUintNumOfDigits
** ===========================================================================*/
/*
   @brief : return the number of digits in a unsigned number.
 
   @param arg[0]  - iNumber

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   uint32

*/
uint32 mprocTestUintNumOfDigits(uint32 iNumber) {
    uint32 iCount =0;
    while (iNumber) {
        iNumber = iNumber/10;
        iCount++;
    }
    return iCount;
}

/* ============================================================================
**  Function : mprocTestMemAllocInit
** ========================================================================== */
/*
   @brief
   * Allocate memory to client buffer
   * Initialize with sequential data

   @param dwArg      - [IN] # parameters
   @param 0          - [IN] pTestBuf : pointer to client buffer
   @param 1          - [IN] iBufSz : size of the buffer requested.
   @param 2          - [IN] iBufLen : length of the buffer.

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 mprocTestMemAllocInit(uint32  **pTestBuf, 
                             uint32  iBufSz,
                             uint32  iBufLen) {

    uint32    iBufValue = 0;
    uint32    iCount    = 0;

    /*Allocate Buffer*/
    //*pTestBuf = (uint32 *)Core_Malloc(iBufSz);
	*pTestBuf = (uint32 *)AllocatePool(iBufSz);
    if (NULL == *pTestBuf) {
        AsciiPrint (PRINTSIG"Error: Allocating local buffer, malloc returned");
        return TF_RESULT_CODE_FAILURE;
    }
     
    /* fill up a test buffer */
    iBufValue = 0;                                      
    for ( iCount = 0; iCount < iBufLen; iCount++ ) {
        *(*pTestBuf + iCount) = iBufValue++;                        
    }

    return TF_RESULT_CODE_SUCCESS;
}


/* ============================================================================
**  Function : mprocTestMemFree
** ========================================================================== */
/*
   @brief
   * Free local buffer allocated for data transfer

   @param dwArg      - [IN] # parameters
   @param 0          - [IN] pTestBuf : pointer to client buffer

   @par Dependencies
   None

   @par Side Effects
   None

   @return
   TF_RESULT_CODE_SUCCESS if "pass",
   TF_RESULT_CODE_FAILURE if  "fail"
*/

uint32 mprocTestMemFree(void *pTestBuf) {

    if (NULL == pTestBuf) {
        AsciiPrint (PRINTSIG"Error: Buffer to be destroyed is NULL");
        return TF_RESULT_CODE_FAILURE;
    }
    FreePool(pTestBuf);
   
    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : mprocTestDestroyEventObj
** ========================================================================== */
/*
   @brief
   Destroy Dal Event Handles

   @param arg[0]    - [IN] dalTestEvent : Handle to dal Event array
   @param arg[1]    - [IN] iNumofObj : number of event objects.
 
   @par Dependencies
   None

   @par Side Effects
   None

   @return  
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)
 
*/

uint32 mprocTestDestroyEventObj(DALSYSEventHandle *dalTestEvent,
                              uint32              iNumofObj ) {
    uint32     iDalLoop   = 0;
    DALResult  dalResult  = DAL_SUCCESS; 

    if (NULL == dalTestEvent) {
        AsciiPrint (PRINTSIG"Error: dalTestEvent Handle to be destroyed is NULL");
        return TF_RESULT_CODE_FAILURE;
    }

    /* Destroy the DalEvent Object */
    for (iDalLoop = 0 ; iDalLoop < iNumofObj; iDalLoop++) {
        dalResult = DALSYS_DestroyObject(*(dalTestEvent + iDalLoop));
        if ( DAL_SUCCESS != dalResult ) {
            AsciiPrint (PRINTSIG"Error: Destroy Object Failed : %d ", dalResult);
            return TF_RESULT_CODE_FAILURE;
        }
    }
    return TF_RESULT_CODE_SUCCESS;
}

/* ============================================================================
**  Function : mprocTestCreateEventObj
** ========================================================================== */
/*
   @brief
   create multiple DalEvent object based on args

   @param arg[0]    - [IN] dalTestEvent : Handle to dalEvent Array(default,timeout)
   @param arg[1]    - [IN] iEvtAttrib : Event type (default,timeout)
   @param arg[2]    - [IN] iNumofObj : number of event to be created.
 
   @par Dependencies
   None

   @par Side Effects
   None

   @return  
   uint32 : Success(TF_RESULT_CODE_SUCCESS)
   uint32 : exceptions(TF_RESULT_CODE_FAILURE)
 
*/

uint32 mprocTestCreateEventObj(DALSYSEventHandle *dalTestEvent,
                             uint32              *iEvtAttrib, 
                             uint32              iNumofObj ) {
    uint32    iDalLoop  = 0;
    DALResult dalResult = DAL_SUCCESS;

    if (NULL == dalTestEvent || NULL == iEvtAttrib) {
        AsciiPrint (PRINTSIG"Error: Null pointer exception in mprocTestCreateEventObj");
        return TF_RESULT_CODE_FAILURE;
    }

    for (iDalLoop = 0 ; iDalLoop < iNumofObj; iDalLoop++) {
        dalResult = DALSYS_EventCreate(*(iEvtAttrib + iDalLoop) , dalTestEvent + iDalLoop , NULL);
        if ( DAL_SUCCESS != dalResult ) {
            /* Destroy the previous objects created */
            mprocTestDestroyEventObj(dalTestEvent,iDalLoop);
            AsciiPrint (PRINTSIG"Error: DalSys Event Create Failed Err code: %d", dalResult);
            return TF_RESULT_CODE_FAILURE;
        }
    }
    return TF_RESULT_CODE_SUCCESS ; 
}


