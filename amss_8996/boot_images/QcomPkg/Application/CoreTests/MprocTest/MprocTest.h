/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		MprocTest.h
  DESCRIPTION:	Common headers included for all Mproc files
  
  REVISION HISTORY
  when       who     	what, where, why
  --------   ---     	---------------------------------------------------------
  04/28/15   pvadlama   Initial Version
================================================================================*/

#ifndef _Mproc_TESTS_
#define _Mproc_TESTS_

#include "tests_daltf_common.h"   /* Common definitions */

#ifdef USE_DIAG
#include "diagcmd.h"
#endif

#include "mproc_daltf_common.h"
#include "mproc_daltf_glink_test.h"
#include "mproc_daltf_glink_internal_test.h"
#include "mproc_daltf_smem_test.h"


/*============================================================================
                FUNCTION PROTOTYPES/FORWARD DECLARATION
==============================================================================*/
                 
/* ============================================================================
**  Function : mprocInitTestFunc
* ============================================================================*/
/** 
@brief 
This function is called to list mproc test in URAD.

@param [in] dwarg
@param [in] pszArg[]
@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.
@dependencies None.
@par Test Type
Automated.
@par Detail Param List
MprocTestList:\n
<ul>
<li>all - all mproc test</li>
<li>auto - automated test</li>
<li>adv - manual test</li>
</ul> 
@par Processor Information
Applicable to all Processors.
@par Target Information
Applicable to all Targets.
*/
static uint32 mprocInitTestFunc(uint32 dwArg, char* pszArg[]);                 

/* ============================================================================
  Function : mprocDeInitTestFunc
*=============================================================================*/
/** 
@brief 
This function is called to clean/remove all mproc test in URAD.

@param [in] dwarg
@param [in] pszArg[]
@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.
@dependencies None.
@par Test Type
Automated.
@par Detail Param List
MprocTestList:\n
<ul>
<li>all - all mproc test</li>
<li>auto - automated test</li>
<li>adv - manual test</li>
</ul> 
@par Processor Information
Applicable to all Processors.
@par Target Information
Applicable to all Targets.
*/
static uint32 mprocDeInitTestFunc(uint32 dwArg, char* pszArg[]); 

#endif
