
/*===========================================================================

                    Glink Test Source File


 Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights
 Reserved.
===========================================================================*/

/*===========================================================================
  $Id: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/MprocTest/mproc_daltf_glink_test.c#1 $
 
EDIT HISTORY FOR FILE 

when        who     	what, where, why
--------    ---     	----------------------------------------------------
04/28/2015  pvadlama    Initial version.  
===========================================================================*/
#include "mproc_daltf_glink_test.h"

/*=============================================================================
     Global declarations
===============================================================================*/
static const TF_ContextDescr gGlinkTestCmnContext= { (uint8)DIAG_SUBSYS_COREBSP, 0};
GlinkTestChannelInfoType     gGlinkTestChannelInfoObj;
uint32						 gDummyData = 10;
GlinkEventPool               gDalEvent;

/*MultiEdgePort global params*/
DALSYSEventHandle			 gGlinkMultiChannelEventHandle[MAX_MULTI_CHANNEL]; 
DALSYSWorkLoopHandle    	 gGlinkMultiChannelWorkLoop[MAX_MULTI_CHANNEL];
DALSYSEventHandle  			 dalTestThreadWait[MAX_MULTI_CHANNEL][TEST_MPROC_NUM_OF_EVENTS];

/*Echoback server global params*/
DALSYSEventHandle            gGlinkEchobackEventHandle;
DALSYSWorkLoopHandle         gGlinkEchobackWorkLoop;
GlinkTestChannelInfoType	 gGlinkEchobackInfoObj;
GlinkEventPool    		     gDalEchobackEvent;
char 						 gGlinkTransp[32], gGlinkRemoteSS[32], gGlinkChName[32];
uint32             			 gPrivData = 2;

/*=============================================================================
                                INTERNAL API's
===============================================================================*/

/* Parameter Descriptor for GlinkOpenTestParam*/
static const TF_ParamDescr GlinkOpenTestParam[] = {
   { TF_PARAM_STRING, "Transport",     	    "Enter the desired transport or NULL"},
   { TF_PARAM_STRING, "Remote Subsystem",   "Enter the name of Remote Subsystem"},
   { TF_PARAM_STRING, "Channel Name",       "Enter the name of Channel"},
   { TF_PARAM_UINT32, "Options",            "Enter transport specific options (Optional)"},
   { TF_PARAM_UINT32, "Iterations",    	    "Number of iterations"}
};

/* Parameter Descriptor for GlinkDataTransferTestParam*/
static const TF_ParamDescr GlinkDataTransferTestParam[] = {
   { TF_PARAM_STRING, "Transport",     	    "Enter the desired transport or NULL"},
   { TF_PARAM_STRING, "Remote Subsystem",   "Enter the name of Remote Subsystem"},
   { TF_PARAM_STRING, "Channel Name",       "Enter the name of Channel"},
   { TF_PARAM_UINT32, "Options",            "Enter transport specific options (Optional)"},
   { TF_PARAM_UINT32, "Request Intent",     "1 - Blocking, 0 - Non Blocking"},
   { TF_PARAM_UINT32, "Data Size",          "Enter size of data to be transmitted"},
   { TF_PARAM_UINT32, "Iterations",    	    "Number of iterations"}
};

/* Parameter Descriptor for GlinkDataTransferSSRTestParam*/
static const TF_ParamDescr GlinkDataTransferSSRTestParam[] = {
   { TF_PARAM_STRING, "Transport",     	    "Enter the desired transport or NULL"},
   { TF_PARAM_STRING, "Remote Subsystem",   "Enter the name of Remote Subsystem"},
   { TF_PARAM_STRING, "Channel Name",       "Enter the name of Channel"},
   { TF_PARAM_UINT32, "Options",            "Enter transport specific options (Optional)"},
   { TF_PARAM_UINT32, "Request Intent",     "1 - Blocking, 0 - Non Blocking"},
   { TF_PARAM_UINT32, "Data Size",          "Enter size of data to be transmitted"},
   { TF_PARAM_UINT32, "Iterations",    	    "Number of iterations"},
   { TF_PARAM_UINT32, "SSR",          		"Enter number of SSRs"}
};

/* Parameter Descriptor for GlinkOpenAPITestParam*/
static const TF_ParamDescr GlinkOpenAPITestParam[] = {
   { TF_PARAM_STRING, "Transport",     	    "Enter the desired transport or NULL"},
   { TF_PARAM_STRING, "Remote Subsystem",   "Enter the name of Remote Subsystem"},
   { TF_PARAM_STRING, "Channel Name",       "Enter the name of Channel"},
   { TF_PARAM_UINT32, "Options",            "Enter transport specific options (Optional)"},
   { TF_PARAM_UINT8,  "Adv Flag",    	    "1 - Adversarial, 0 - Nominal"}
};

/* Parameter Descriptor for GlinkRxDoneAPITestParam*/
static const TF_ParamDescr GlinkRxDoneAPITestParam[] = {
   { TF_PARAM_UINT8, "Use Handle",      "1 - Use existing handle, 0 - NULL"},
   { TF_PARAM_UINT8, "Received Buffer", "1 - Valid, 0 - NULL"},
   { TF_PARAM_UINT8, "Reuse Buffer",    "1 - True, 0 - False"}
}; 

/* Parameter Descriptor for GlinkAPICmnTestParam*/
static const TF_ParamDescr GlinkAPICmnTestParam[] = {
   { TF_PARAM_UINT8, "Use Handle",   "1 - Use existing handle, 0 - NULL" }
}; 

/* Parameter Descriptor for GlinkSigSetAPITestParam*/
static const TF_ParamDescr GlinkSigSetAPITestParam[] = {
   { TF_PARAM_UINT8, "Use Handle",   "1 - Use existing handle, 0 - NULL"},
   { TF_PARAM_UINT8, "Signal Value", "1 - Valid, 0 - NULL"}
}; 
 
/* Parameter Descriptor for GlinkTxCmnTestParam*/
static const TF_ParamDescr GlinkTxCmnTestParam[] = {
   { TF_PARAM_UINT8,  "Use Handle",                "1 - Use existing handle, 0 - NULL"}, 
   { TF_PARAM_UINT8,  "Data",                      "1 - Valid, 0 - NULL "},
   { TF_PARAM_UINT32, "Data Size",                 "Enter size of data to be transmitted"}, 
   { TF_PARAM_UINT8,  "Request Intent",            "1 - Blocking, 0 - Non Blocking"}
};
 
/* Parameter Descriptor for GlinkQueueIntentTestParam*/
static const TF_ParamDescr GlinkQueueIntentTestParam[] = {
   { TF_PARAM_UINT8,  "Use Handle",                "1 - Use existing handle, 0 - NULL" },
   { TF_PARAM_UINT32, "Data Size",                 "Enter size of data to be requested"}
}; 

/*=============================================================================
                                INTERNAL FUNCTIONS
===============================================================================*/

/* ============================================================================
**  Function : GlinkEchobackThread
** ========================================================================== */
   /** 
   @brief 
   GlinkEchobackThread function runs in a thread/workloop. 
   Opens Glink port, and loops back data continously and requests 
   intents as and when remote side requests (with reuse set to 1). 
   When remote side closes port, server will automatically close. 
   */

DALResult GlinkEchobackThread( DALSYSEventHandle hEvent, void *pdata) {
    int                     	iErrCheck  = 0;
	glink_err_type         		glinkRetCode;
    glink_open_config_type  	glinkOpenCfg;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
								DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	uint32						i, numRequests = 0;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	/*Number of requests the server will support*/
	numRequests = *(uint32* )pdata;
	
    /*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&gDalEchobackEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return DAL_WORK_LOOP_EXIT;
	}

	/* populate channel information */
	glinkOpenCfg.transport 			    = gGlinkEchobackInfoObj.transport;
	glinkOpenCfg.remote_ss 			    = gGlinkEchobackInfoObj.remote_ss;
	glinkOpenCfg.name 			        = gGlinkEchobackInfoObj.name;
	glinkOpenCfg.options		    	= gGlinkEchobackInfoObj.iOptions;
	glinkOpenCfg.priv 		    	    = (void *)&gGlinkEchobackInfoObj;
	glinkOpenCfg.notify_rx  	        = glinkEchobackRxNotifCb;
	glinkOpenCfg.notify_rxv 	        = NULL; 				
	glinkOpenCfg.notify_tx_done         = glinkEchoBackTxNotifCb;
	glinkOpenCfg.notify_state           = glinkEchobackStateNotifCb;
	glinkOpenCfg.notify_rx_intent_req   = glinkEchobackRxIntentReqCb;
	glinkOpenCfg.notify_rx_intent       = glinkEchobackNotifyRxIntentCb;
	glinkOpenCfg.notify_rx_sigs         = glinkTestLbNotifyRxSigsCb; /*Dummy Callback*/
	glinkOpenCfg.notify_rx_abort         = NULL;
	glinkOpenCfg.notify_tx_abort         = NULL; 
	gGlinkEchobackInfoObj.dalTestEvent 	= &gDalEchobackEvent;

	/*Open a Glink channel*/
	Status = glink->GlinkOpen(&glinkOpenCfg, &gGlinkEchobackInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Svc:Error: Glink Channel open failed. Returned %d\n", glinkRetCode);	
		GlinkTestDestroyEvents(&gDalEchobackEvent);
		return DAL_WORK_LOOP_EXIT;
	}
	
	/* Wait for GLINK_CONNECTED event */
	iErrCheck = GlinkTestWaitEvent(&gGlinkEchobackInfoObj, TEST_GLINK_CONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		GlinkTestDestroyEvents(&gDalEchobackEvent);
		return DAL_WORK_LOOP_EXIT;
	}
		
	for(i = 0; i < numRequests; i++){
        
		/*Wait for Data from client */
        iErrCheck = GlinkTestWaitEvent(&gGlinkEchobackInfoObj, TEST_GLINK_RX_NOTIFICATION);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto CleanUp;
		}
		/*Reset the dalTestRxCbEvent*/
		GlinkTestResetEvent(gGlinkEchobackInfoObj.dalTestEvent->dalTestRxCbEvent);
		
		/*Transmit data*/
		Status = glink->GlinkTx(gGlinkEchobackInfoObj.hGlinkHandle,
                                "svc",
                                (void *)gGlinkEchobackInfoObj.TransmitData,
                                gGlinkEchobackInfoObj.iTxDataSize,
                                1,
								&glinkRetCode); 
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){			
			AsciiPrint (PRINTSIG"Svc:Error: Glink Channel tx failed. Returned %d\n", glinkRetCode);
			goto CleanUp;
		}
		
		/*Wait for Tx Notification event */
        iErrCheck = GlinkTestWaitEvent(&gGlinkEchobackInfoObj, TEST_GLINK_TX_NOTIFICATION);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto CleanUp;
		}
		/*Reset the dalTestTxCbEvent */
		GlinkTestResetEvent(gGlinkEchobackInfoObj.dalTestEvent->dalTestTxCbEvent);
		
		/*Releasing the intent*/
		Status = glink->GlinkRxDone(gGlinkEchobackInfoObj.hGlinkHandle, gGlinkEchobackInfoObj.TransmitData, 1, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Svc:Error: Glink Rx done failed. Returned %d\n", glinkRetCode);	
			goto CleanUp;
		}
	}

	/*Wait for Client to disconnect*/
	iErrCheck = GlinkTestWaitEvent(&gGlinkEchobackInfoObj, TEST_GLINK_REMOTE_DISCONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		AsciiPrint (PRINTSIG"Svc:Error: Glink Channel Remote close failed. Returned %d\n", iErrCheck);
	}

CleanUp:
		
	/*Close the local port*/
	Status = glink->GlinkClose(gGlinkEchobackInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Svc:Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
		GlinkTestDestroyEvents(&gDalEchobackEvent);
		return DAL_WORK_LOOP_EXIT;
	}
	else{
		/* Wait for GLINK_LOCAL_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&gGlinkEchobackInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&gDalEchobackEvent);
			return DAL_WORK_LOOP_EXIT;
		}
		AsciiPrint (PRINTSIG"Svc: Glink Channel closed successfully\n");
	}
	
	/* Destroy the DalEvent Object , NUM_OF_EVENT = 2 */	
	GlinkTestDestroyEvents(&gDalEchobackEvent);
	return DAL_WORK_LOOP_EXIT;
}

/*=============================================================================
                                PUBLIC API's
===============================================================================*/

/*=============================================================================
                                NOMINAL TESTS
===============================================================================*/


/* ============================================================================
**  Function : GlinkLBTest
** ========================================================================== */
   /** 
   @brief 
   GlinkLBTest function verifies basic control channel functionalities of dev
   supported looback Glink server (SMEM native transport).
   
   @param [in] dwarg
   @param [in] pszArg[]
   
   @return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.
   
   @dependencies
   None.
   
   @par Test Type
   Automated.
   */

uint32 GlinkLBTest(uint32 dwArg, char *pszArg[]) {
   uint32        		iNumParam  = 0;
   int                     	iErrCheck = 0;
   GlinkTestChannelInfoType	glinkChannelInfoObj;
   loop_request                 pkt;
   uint16                       length;
   GlinkEventPool    		dalTestEvent;
   uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = { DALSYS_EVENT_ATTR_NORMAL,
                                                                        DALSYS_EVENT_ATTR_TIMEOUT_EVENT };

   /*Input param check*/
   iNumParam = sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr);
   if (dwArg != iNumParam) {
      AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                iNumParam, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }

   /*Input handler*/
   iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
                                     dwArg,
                                     pszArg,
                                     iNumParam,
                                     GLINK_TEST_TYPE_1);
   if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
      return TF_RESULT_CODE_FAILURE;
   }
   
   /*Create DalEvent objects, NUM_OF_EVENT = 2 */
   iErrCheck = GlinkTestCreateEvents(&dalTestEvent, iEvtAttrib); 
   if (TF_RESULT_CODE_FAILURE == iErrCheck) {
      return TF_RESULT_CODE_FAILURE;
   }
   
   glinkChannelInfoObj.dalTestEvent = &dalTestEvent;
   glinkChannelInfoObj.bVector = 0;
   length = strlen(glinkChannelInfoObj.name);

   /*Open a Glink control channel*/
   iErrCheck = glink_loopback_open(&glinkChannelInfoObj);
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
       return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink open Control Channel successful\n");
   
   /*Construct payload*/
   pkt.payload.open.delay_ms = 0;
   strlcpy(pkt.payload.open.name,glinkChannelInfoObj.name, (length+1));
   pkt.payload.open.name_len = length; 
   
   iErrCheck = glink_loopback_send_request(&glinkChannelInfoObj, &pkt, OPEN);
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
	  glink_loopback_close(&glinkChannelInfoObj);
	  GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink Send Request OPEN successful\n");
   
   pkt.payload.tx_conf.delay_ms = 0;
   pkt.payload.tx_conf.echo_count = 1;
   strlcpy(pkt.payload.tx_conf.name, glinkChannelInfoObj.name, (length + 1));
   pkt.payload.tx_conf.name_len = length;
   pkt.payload.tx_conf.random_delay = 0;
   pkt.payload.tx_conf.transform_type = NO_TRANSFORM;
   
   iErrCheck = glink_loopback_send_request(&glinkChannelInfoObj, &pkt, TX_CONFIG);
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
	   glink_loopback_send_request(&glinkChannelInfoObj, &pkt, CLOSE);
	   glink_loopback_close(&glinkChannelInfoObj);
	   GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink Send Request TX_CONFIG successful\n");
   
   pkt.payload.rx_done_conf.delay_ms = 0;
   strlcpy(pkt.payload.rx_done_conf.name, glinkChannelInfoObj.name, (length + 1));
   pkt.payload.rx_done_conf.name_len = length;
   pkt.payload.rx_done_conf.random_delay = 0;
   
   iErrCheck = glink_loopback_send_request(&glinkChannelInfoObj, &pkt, RX_DONE_CONFIG); 
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
	glink_loopback_send_request(&glinkChannelInfoObj, &pkt, CLOSE);
	   glink_loopback_close(&glinkChannelInfoObj);
	   GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink Send Request RX DONE CONFIG successful\n");
   
   pkt.payload.q_rx_int_conf.delay_ms = 0;
   pkt.payload.q_rx_int_conf.intent_size = 512;
   strlcpy(pkt.payload.q_rx_int_conf.name, glinkChannelInfoObj.name, (length + 1)); 
   pkt.payload.q_rx_int_conf.name_len = length;
   pkt.payload.q_rx_int_conf.num_intents = 1;
   pkt.payload.q_rx_int_conf.random_delay = 0;
   
   iErrCheck = glink_loopback_send_request(&glinkChannelInfoObj, &pkt, Q_RX_INT_CONFIG);
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		glink_loopback_send_request(&glinkChannelInfoObj, &pkt, CLOSE);
	   glink_loopback_close(&glinkChannelInfoObj);
	   GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink Send Request Q RX INTENT CONFIG successful\n");
   
   pkt.payload.close.delay_ms = 0;
   pkt.payload.close.name_len = length;
   strlcpy(pkt.payload.close.name, glinkChannelInfoObj.name, (length + 1));
   
   iErrCheck = glink_loopback_send_request(&glinkChannelInfoObj, &pkt, CLOSE);
   if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		glink_loopback_close(&glinkChannelInfoObj);
	   GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink Send Request CLOSE successful\n");
   
   iErrCheck = glink_loopback_close(&glinkChannelInfoObj);
   if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint (PRINTSIG"Glink CLOSE Control Channel successful\n"); 
   
   GlinkTestDestroyEvents(&dalTestEvent);
   
   return TF_RESULT_CODE_SUCCESS;
}    
   
   /* Help Descriptor for GlinkLBTest */
static const TF_HelpDescr GlinkLBTestHelp = {
      "GlinkLBTest: MPROC_GLINK_L1", 
      sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr),
      GlinkOpenTestParam
   }; 
   
   /* TestFunction Descriptor for GlinkLBTest */
   const TF_TestFunction_Descr GlinkLBTester = {
      PRINTSIG "GlinkLBTest", 
      GlinkLBTest,
      &GlinkLBTestHelp, 
      &gGlinkTestCmnContext
   }; 


/* ============================================================================
**  Function : GlinkOpenSingleLocal
** ========================================================================== */
/** 
@brief 
This function is called to open and close a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkOpenSingleLocal(uint32 dwArg, char* pszArg[]){
	uint32        			iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0 ;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    			dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_1);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Channel Parameters are set\n");
	
	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	for(i = 0; i < glinkChannelInfoObj.iterations; i++){
	
		/* Reset the dalTestLocalDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestLocalDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		/*Open a Glink channel*/
		Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
	
		AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");
		DALSYS_BusyWait(1000000); /* Wait for 1 second after channel opens*/
		
		/*Close Glink channel*/
		Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Waiting for GLINK_LOCAL_DISCONNECTED event\n");
		
		/* Wait for GLINK_LOCAL_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel closed successfully:%d\n", i);
	}
	
	/* Destroy the DalEvent Object , NUM_OF_EVENT = 2 */	
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS; 
}

/* Help Descriptor for GlinkOpenSingleLocal */
static const TF_HelpDescr GlinkOpenSingleLocalHelp = {
    "GlinkOpenSingleLocal: MPROC_GLINK_L1",
     sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr),
     GlinkOpenTestParam
};

/* TestFunction Descriptor for GlinkOpenSingleLocal */
const TF_TestFunction_Descr GlinkOpenSingleLocalTest = {
    PRINTSIG "GlinkOpenSingleLocal",
    GlinkOpenSingleLocal,
    &GlinkOpenSingleLocalHelp,
    &gGlinkTestCmnContext
};

/* ============================================================================
**  Function : GlinkDataTransfer
** ========================================================================== */
/** 
@brief 
This function is called to open and close a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkDataTransfer(uint32 dwArg, char* pszArg[]){
	uint32        			iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0, iErrReturn = TF_RESULT_CODE_FAILURE;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    			dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_2);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Check for buffer size*/
	if(10000 < glinkChannelInfoObj.iTxDataSize){
		AsciiPrint (PRINTSIG"Error: Too large buffer size\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}
	
	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Channel Parameters are set\n");
	
	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Creating data according to user input */
    iErrCheck = mprocTestMemAllocInit(&glinkChannelInfoObj.TransmitData,
                                      glinkChannelInfoObj.iTxDataSize,
                                      glinkChannelInfoObj.iTxDataSize / sizeof(uint32)); 
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}

	/*Open a Glink channel*/
	Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);	
		GlinkTestDestroyEvents(&dalTestEvent);
		mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Wait for GLINK_CONNECTED event */
	iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_CONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		GlinkTestDestroyEvents(&dalTestEvent);
		mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");
		
	for(i = 0; i < glinkChannelInfoObj.iterations; i++){
	
		/* Reset the dalTestTxCbEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestTxCbEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			goto cleanUp;
		}
		
		/* Reset the dalTestRxCbEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestRxCbEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			goto cleanUp;
		}
		
		/*queue the intent for the apps side to send the data*/
		Status = glink->GlinkQueueRxIntent(glinkChannelInfoObj.hGlinkHandle,
											"test",
											glinkChannelInfoObj.iTxDataSize,
											&glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Queue_Rx_Intent failed with error: %d\n", glinkRetCode);
			goto cleanUp;
		}
		
		/*Transmit data*/
		Status = glink->GlinkTx(glinkChannelInfoObj.hGlinkHandle,
        						"test",
                                (void *)glinkChannelInfoObj.TransmitData,
                                glinkChannelInfoObj.iTxDataSize,
                                glinkChannelInfoObj.bReqIntent,
								&glinkRetCode); 
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel tx failed. Returned %d\n", glinkRetCode);
			goto cleanUp;
		}
		
		/* Wait for TEST_GLINK_TX_NOTIFICATION event */
        iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_TX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp;
		}
		
		AsciiPrint (PRINTSIG"Data transmitted successfully\n");
		
		/* Wait for TEST_GLINK_RX_NOTIFICATION event */
        iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_RX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp;
		}
		AsciiPrint (PRINTSIG"Data Received and compared\n");
		//Data Compare here
	}
	iErrReturn = TF_RESULT_CODE_SUCCESS;
cleanUp:	
	/*Deallocate memory allocated for data transfer*/
	iErrCheck = mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
		
	Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	else{
		/* Wait for GLINK_LOCAL_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			iErrReturn = TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* Destroy the DalEvent Object , NUM_OF_EVENT = 2 */	
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	return iErrReturn;
}

/* Help Descriptor for GlinkDataTransfer */
static const TF_HelpDescr GlinkDataTransferHelp = {
    "GlinkDataTransfer: MPROC_GLINK_L1",
     sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr),
     GlinkDataTransferTestParam
};

/* TestFunction Descriptor for GlinkDataTransfer */
const TF_TestFunction_Descr GlinkDataTransferTest = {
    PRINTSIG "GlinkDataTransfer",
    GlinkDataTransfer,
    &GlinkDataTransferHelp,
    &gGlinkTestCmnContext
};

/* ============================================================================
**  Function : GlinkDataTransferWithLoopback
** ========================================================================== */
/** 
@brief 
This function is called to open and close a local Glink port with user 
defined parameters.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkDataTransferWithLoopback(uint32 dwArg, char* pszArg[]){
	uint32        				iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0, iErrReturn = TF_RESULT_CODE_FAILURE;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkDataChannelInfoObj, glinkCtrlChannelInfoObj;
	GlinkEventPool    			dataChannelEvent, ctrlChannelEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	loop_request                 	pkt;
	uint16                       	length;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}
	
	/*Input handler for data channel*/
	iErrCheck = GlinkTestInputHandler(&glinkDataChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_2);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Populate channel information from user input and map callbacks for data channel*/
	iErrCheck = GlinkTestSetChannelInfo(&glinkDataChannelInfoObj, &glinkOpenCfg, &dataChannelEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Create data channel DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dataChannelEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}	
	
	/*Input handler for control channel*/
	iErrCheck = GlinkTestInputHandler(&glinkCtrlChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_1);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		GlinkTestDestroyEvents(&dataChannelEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Check for buffer size*/
	if(10000 < glinkDataChannelInfoObj.iTxDataSize){
		AsciiPrint (PRINTSIG"Error: Too large buffer size\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}
	glinkCtrlChannelInfoObj.dalTestEvent = &ctrlChannelEvent;
	glinkCtrlChannelInfoObj.bVector = 0;
	
	/*Create control channel DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&ctrlChannelEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dataChannelEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	length = strlen(glinkDataChannelInfoObj.name);
	
	/*Open a Glink control channel*/
	iErrCheck = glink_loopback_open(&glinkCtrlChannelInfoObj);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dataChannelEvent);
		GlinkTestDestroyEvents(&ctrlChannelEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Glink Control Channel opened successfully\n");
	
	/*Construct payload*/
	pkt.payload.open.delay_ms = 0;
	strlcpy(pkt.payload.open.name,glinkDataChannelInfoObj.name, (length+1));
	pkt.payload.open.name_len = length; 
	
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, OPEN);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		//Close control channel
		iErrCheck = glink_loopback_close(&glinkCtrlChannelInfoObj);
		if (TF_RESULT_CODE_SUCCESS != iErrCheck){
			return TF_RESULT_CODE_FAILURE;
		}
		GlinkTestDestroyEvents(&dataChannelEvent);
		GlinkTestDestroyEvents(&ctrlChannelEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Glink Send Request OPEN successful\n");
	
	/*Open a Glink data channel*/
	Status = glink->GlinkOpen(&glinkOpenCfg, &glinkDataChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		goto cleanUp_remoteChannels;
	}

	/* Wait for GLINK_CONNECTED event */
	iErrCheck = GlinkTestWaitEvent(&glinkDataChannelInfoObj, TEST_GLINK_CONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		goto cleanUp_remoteChannels;
	}
	AsciiPrint (PRINTSIG"Glink Data Channel opened successfully\n");
	
	/* Creating data according to user input */
	iErrCheck = mprocTestMemAllocInit(&glinkDataChannelInfoObj.TransmitData,
									  glinkDataChannelInfoObj.iTxDataSize,
									  glinkDataChannelInfoObj.iTxDataSize / sizeof(uint32)); 
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		goto cleanUp_allChannels;
	}
	
	pkt.payload.tx_conf.delay_ms = 0;
	pkt.payload.tx_conf.echo_count = 1;
	strlcpy(pkt.payload.tx_conf.name, glinkDataChannelInfoObj.name, (length + 1));
	pkt.payload.tx_conf.name_len = length;
	pkt.payload.tx_conf.random_delay = 0;
	pkt.payload.tx_conf.transform_type = NO_TRANSFORM;
	
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, TX_CONFIG);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		goto cleanUp_all;
	}
	AsciiPrint (PRINTSIG"Glink Send Request TX_CONFIG successful\n");
	
	pkt.payload.rx_done_conf.delay_ms = 0;
	strlcpy(pkt.payload.rx_done_conf.name, glinkDataChannelInfoObj.name, (length + 1));
	pkt.payload.rx_done_conf.name_len = length;
	pkt.payload.rx_done_conf.random_delay = 0;
	
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, RX_DONE_CONFIG); 
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		goto cleanUp_all;
	}
	AsciiPrint (PRINTSIG"Glink Send Request RX DONE CONFIG successful\n");
	
	pkt.payload.q_rx_int_conf.delay_ms = 0;
	pkt.payload.q_rx_int_conf.intent_size = 512;
	strlcpy(pkt.payload.q_rx_int_conf.name, glinkDataChannelInfoObj.name, (length + 1)); 
	pkt.payload.q_rx_int_conf.name_len = length;
	pkt.payload.q_rx_int_conf.num_intents = 1;
	pkt.payload.q_rx_int_conf.random_delay = 0;
	
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, Q_RX_INT_CONFIG);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		goto cleanUp_all;
	}
	AsciiPrint (PRINTSIG"Glink Send Request Q RX INTENT CONFIG successful\n");
		
	for(i = 0; i < glinkDataChannelInfoObj.iterations; i++){
	
		/* Reset the dalTestTxCbEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkDataChannelInfoObj.dalTestEvent->dalTestTxCbEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			goto cleanUp_all;
		}
		
		/* Reset the dalTestRxCbEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkDataChannelInfoObj.dalTestEvent->dalTestRxCbEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			goto cleanUp_all;
		}
		
		/*queue the intent for the apps side to send the data*/
		Status = glink->GlinkQueueRxIntent(glinkDataChannelInfoObj.hGlinkHandle,
											"test",
											glinkDataChannelInfoObj.iTxDataSize,
											&glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Queue_Rx_Intent failed with error: %d\n", glinkRetCode);
			goto cleanUp_all;
		}
		
	    AsciiPrint (PRINTSIG"glinkTestNotifyRxIntentReqCb: Rx_Intent Queued successfully\n");
		
		/*Transmit data*/
		Status = glink->GlinkTx(glinkDataChannelInfoObj.hGlinkHandle,
                                "test",
                                (void *)glinkDataChannelInfoObj.TransmitData,
                                glinkDataChannelInfoObj.iTxDataSize,
                                glinkDataChannelInfoObj.bReqIntent,
								&glinkRetCode);
				  
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			goto cleanUp_all;
		}
		/* Wait for TEST_GLINK_TX_NOTIFICATION event */
        iErrCheck = GlinkTestWaitEvent(&glinkDataChannelInfoObj, TEST_GLINK_TX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp_all;
		}
		AsciiPrint (PRINTSIG"Data transmitted successfully\n");

		/* Wait for TEST_GLINK_RX_NOTIFICATION event */
        iErrCheck = GlinkTestWaitEvent(&glinkDataChannelInfoObj, TEST_GLINK_RX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp_all;
		}
		AsciiPrint (PRINTSIG"Data Received and compared\n");
		//Data Compare here
		
	}
	iErrReturn = TF_RESULT_CODE_SUCCESS;
	
cleanUp_all:

	/*Deallocate memory allocated for data transfer*/
	iErrCheck = mprocTestMemFree((void *)glinkDataChannelInfoObj.TransmitData);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}

cleanUp_allChannels:
	//Close data channel on remote
	pkt.payload.close.delay_ms = 0;
	pkt.payload.close.name_len = length;
	strlcpy(pkt.payload.close.name, glinkDataChannelInfoObj.name, (length + 1));
	
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, CLOSE);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		AsciiPrint (PRINTSIG"Error: Glink Data channel remote close Failed\n");
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	else{
		/* Wait for GLINK_REMOTE_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&glinkDataChannelInfoObj, TEST_GLINK_REMOTE_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			iErrReturn = TF_RESULT_CODE_FAILURE;
		}
	}
	
	//Close data channel on local
	Status = glink->GlinkClose(glinkDataChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Error: Glink Data Channel local close failed. Returned %d\n", glinkRetCode);
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	else{
		/* Wait for GLINK_LOCAL_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&glinkDataChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			iErrReturn = TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Data Channel closed on local successfully\n");
	}
	
	//Close control channel
	iErrCheck = glink_loopback_close(&glinkCtrlChannelInfoObj);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}

	/* Destroy the DalEvent Objects , NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestDestroyEvents(&dataChannelEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	iErrCheck = GlinkTestDestroyEvents(&ctrlChannelEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		iErrReturn = TF_RESULT_CODE_FAILURE;
	}
	return iErrReturn;
	
cleanUp_remoteChannels:
	//Close data channel on remote
	iErrCheck = glink_loopback_send_request(&glinkCtrlChannelInfoObj, &pkt, CLOSE);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		AsciiPrint (PRINTSIG"Error: Glink Data channel remote close Failed");
	}
	//Close control channel
	iErrCheck = glink_loopback_close(&glinkCtrlChannelInfoObj);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck){
		AsciiPrint (PRINTSIG"Error: Glink Control channel close Failed");
	}
	GlinkTestDestroyEvents(&dataChannelEvent);
	GlinkTestDestroyEvents(&ctrlChannelEvent);
	return iErrReturn;
}

/* Help Descriptor for GlinkDataTransferWithLoopback */
static const TF_HelpDescr GlinkDataTransferWithLoopbackHelp = {
	"GlinkDataTransferWithLoopback: MPROC_GLINK_L1",
	sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr),
	GlinkDataTransferTestParam
};

/* TestFunction Descriptor for GlinkDataTransferWithLoopback */
const TF_TestFunction_Descr GlinkDataTransferWithLoopbackTest = {
	PRINTSIG "GlinkDataTransferWithLoopback",
	GlinkDataTransferWithLoopback,
	&GlinkDataTransferWithLoopbackHelp,
	&gGlinkTestCmnContext
};


/* ============================================================================
**  Function : GlinkStressOpenClosePort
** ========================================================================== */
/** 
@brief 
Stress Open/Close Glink ports

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkStressOpenClosePort(uint32 dwArg, char* pszArg[]){
	uint32        			iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    		dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_1);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Channel Parameters are set\n");
	
	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Loop here until the iterations to open and close ports*/
	for(i = 0; i < glinkChannelInfoObj.iterations; i++){
	
		/* Reset the dalTestLocalDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestLocalDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		/* Reset the dalTestConnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestConnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		/* Reset the dalTestRemoteDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestRemoteDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		/*Open a Glink channel*/
		Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
	
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_CONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");
		
		/*wait for remote port to receive and process events*/
		DALSYS_BusyWait(1000000);
		
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_REMOTE_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
			if(GLINK_STATUS_SUCCESS != glinkRetCode){
				AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
				GlinkTestDestroyEvents(&dalTestEvent);
				return TF_RESULT_CODE_FAILURE;
			}
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel closed successfully\n");
		/*wait for remote port to receive and process events*/
		DALSYS_BusyWait(1000000);

	}
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;
}

/* Help Descriptor for GlinkStressOpenClosePort */
static const TF_HelpDescr GlinkStressOpenClosePortHelp = {
    "GlinkStressOpenClosePort: MPROC_GLINK_L1",
     sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr),
     GlinkOpenTestParam
};

/* TestFunction Descriptor for GlinkStressOpenClosePort */
const TF_TestFunction_Descr GlinkStressOpenClosePortTest = {
    PRINTSIG "GlinkStressOpenClosePort",
    GlinkStressOpenClosePort,
    &GlinkStressOpenClosePortHelp,
    &gGlinkTestCmnContext
};

/* ============================================================================
**  Function : GlinkStressOpenCloseServ
** ========================================================================== */
/** 
@brief 
Stress Open/Close Glink ports

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkStressOpenCloseServ(uint32 dwArg, char* pszArg[]){
	uint32        			iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    		dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_1);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Channel Parameters are set\n");
	
	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Loop here until the iterations to open and close ports*/
	for(i = 0; i < glinkChannelInfoObj.iterations; i++){
	
		/* Reset the dalTestLocalDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestLocalDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}

		/* Reset the dalTestConnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestConnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}

		/*Open a Glink channel*/
		Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_CONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");
		DALSYS_BusyWait(1000000); /* Wait for 1 second after channel opens*/
		
		/*Close Glink channel*/
		Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		/* Wait for GLINK_LOCAL_DISCONNECTED event */
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel closed successfully\n");
		/*wait for remote port to receive and process events*/
		DALSYS_BusyWait(1000000);
	}

	/* Destroy the DalEvent Object , NUM_OF_EVENT = 2 */	
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS; 
}

/* Help Descriptor for GlinkStressOpenCloseServ */
static const TF_HelpDescr GlinkStressOpenCloseServHelp = {
    "GlinkStressOpenCloseServ: MPROC_GLINK_L1",
     sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr),
     GlinkOpenTestParam
};

/* TestFunction Descriptor for GlinkStressOpenCloseServ */
const TF_TestFunction_Descr GlinkStressOpenCloseServTest = {
    PRINTSIG "GlinkStressOpenCloseServ",
    GlinkStressOpenCloseServ,
    &GlinkStressOpenCloseServHelp,
    &gGlinkTestCmnContext
};

/* ============================================================================
**  Function : GlinkDataTrsfrServ
** ========================================================================== */
/** 
@brief 
GlinkDataTrsfrServ is an echoback glink data transfer server. Remote proc 
side needs to use GlinkDataTransfer test to connect to this server. 
An optional server in the following list: 
1. SMDL Echoback server (Glink -> SMDL) 
2. GLINK Loopback server (Glink -> Glink (using control and data channels)) 
3. GLINK Echoback server (Glink -> Glink (only one standard loopback channel)) 

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkDataTrsfrServ(uint32 dwArg, char* pszArg[]){
    DALResult				    dalResult = DAL_SUCCESS;
	uint32        			    iNumParam  = 0;
	char          				*pEndChar  = NULL;
	
	/*Input param check*/
	iNumParam = sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}
	/*Copy the names*/
	strlcpy(gGlinkTransp, pszArg[0], 32);
	strlcpy(gGlinkRemoteSS, pszArg[1], 32);
	strlcpy(gGlinkChName, pszArg[2], 32);
	
	gGlinkEchobackInfoObj.transport 		= gGlinkTransp;
	gGlinkEchobackInfoObj.remote_ss 		= gGlinkRemoteSS;
	gGlinkEchobackInfoObj.name 	  			= gGlinkChName;
	gGlinkEchobackInfoObj.iOptions  		= (uint32)strtol(pszArg[3], &pEndChar, 10);
	gGlinkEchobackInfoObj.bReqIntent  		= (uint32)strtol(pszArg[4], &pEndChar, 10); 
	gGlinkEchobackInfoObj.iTxDataSize  		= (uint32)strtol(pszArg[5], &pEndChar, 10);
	/*Check for buffer size*/
	if(10000 < gGlinkEchobackInfoObj.iTxDataSize){
		AsciiPrint (PRINTSIG"Error: Too large buffer size\n");
		return TF_RESULT_CODE_BAD_PARAM;
	}
	gGlinkEchobackInfoObj.iterations 		= (uint32)strtol(pszArg[6], &pEndChar, 10);

    /*Create Event Handle*/
    dalResult= DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,&gGlinkEchobackEventHandle,NULL);
    if (DAL_SUCCESS != dalResult) {
        AsciiPrint (PRINTSIG"Svc:Error: Failed to Create gGlinkEchobackEventHandle\n");
        return TF_RESULT_CODE_FAILURE;
    }

    /*Create/Register WorkLoop */
    dalResult = DALSYS_RegisterWorkLoop(0, 2, &gGlinkEchobackWorkLoop, NULL);
    if (DAL_SUCCESS != dalResult) {
        AsciiPrint (PRINTSIG"Svc:Error: Failed to Register gGlinkEchobackWorkLoop\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
	/* Register GlinkDataTrsferServ to workloop */
    dalResult = DALSYS_AddEventToWorkLoop(gGlinkEchobackWorkLoop,
                                          GlinkEchobackThread,
                                          (void *)&(gGlinkEchobackInfoObj.iterations),
                                          gGlinkEchobackEventHandle,
                                          NULL);
    if (DAL_SUCCESS != dalResult) {
        AsciiPrint (PRINTSIG"Svc:Error: Failed to Create Dalsys WorkLoop to run server thread\n");
        return TF_RESULT_CODE_FAILURE;
    }
    
	/* Trigger workloop */
    dalResult = DALSYS_EventCtrl(gGlinkEchobackEventHandle, DALSYS_EVENT_CTRL_TRIGGER);
    if (DAL_SUCCESS != dalResult) {
        AsciiPrint (PRINTSIG"Svc:Error: Failed to trigger workloop\n");
        return TF_RESULT_CODE_FAILURE;
    }
    AsciiPrint (PRINTSIG"Svc:Workloop triggered\n");
    return TF_RESULT_CODE_SUCCESS;
}

/* Help Descriptor for GlinkDataTrsfrServ */
static const TF_HelpDescr GlinkDataTrsfrServHelp = {
    "GlinkDataTrsfrServ: MPROC_GLINK_L2",
     sizeof(GlinkDataTransferTestParam) / sizeof(TF_ParamDescr),
     GlinkDataTransferTestParam
};

/* TestFunction Descriptor for GlinkDataTrsfrServ */
const TF_TestFunction_Descr GlinkDataTrsfrServTest = {
    PRINTSIG "GlinkDataTrsfrServ",
    GlinkDataTrsfrServ,
    &GlinkDataTrsfrServHelp,
    &gGlinkTestCmnContext
};

/*===========================================================================
  FUNCTION  glinkTestLinkStateNotifCb
===========================================================================*/
/**
  glinkTestLinkStateNotifCb

  @param[in] handle

  @return
*/
/*==========================================================================*/
void glinkTestLinkStateNotifCb(glink_link_info_type *link_info,
								void* priv){
	uint32				*pLinkState;
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTestLinkStateNotifCb: Private Info passed is NULL\n");
		return;
	}
	
	pLinkState = (uint32 *)priv;

	AsciiPrint (PRINTSIG"glinkTestLinkStateNotifCb: remote_ss: %s\n", link_info->remote_ss);
	AsciiPrint (PRINTSIG"glinkTestLinkStateNotifCb: transport: %s\n", link_info->xport);
	AsciiPrint (PRINTSIG"glinkTestLinkStateNotifCb: link state: %d\n", link_info->link_state);
	
	*pLinkState = link_info->link_state;
}

/* ============================================================================
**  Function : GlinkDataTransferSSR
** ========================================================================== */
/** 
@brief 
This function is called to handle SSR during data transfer.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkDataTransferSSR(uint32 dwArg, char* pszArg[]){
	uint32        				iNumParam  = 0, i = 0;
	int                     	iErrCheck = 0;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    			dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	uint32						iSSRCount = 0;
	char          				*pEndChar  = NULL;
	glink_link_id_type 			testLinkId;
	//glink_link_handle_type    	linkHandle = 0;
	uint32						iLinkState = 1;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkDataTransferSSRTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_2);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}

	iSSRCount  = (uint32)strtol(pszArg[7], &pEndChar, 10);

	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	testLinkId.version 			= 	GLINK_LINK_ID_VER;
	testLinkId.xport 			= 	glinkChannelInfoObj.transport;
	testLinkId.remote_ss 		= 	glinkChannelInfoObj.remote_ss;
	testLinkId.link_notifier 	= 	glinkTestLinkStateNotifCb;
	//testLinkId.handle			=	linkHandle;

	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
/*Loop here until the iterations to open and close ports*/
	for(i = 0; i < glinkChannelInfoObj.iterations + iSSRCount; i++){

		Status = glink->GlinkRegisterLinkStateCB(&testLinkId, &iLinkState, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink register link state cb failed. Returned %d", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		AsciiPrint (PRINTSIG"Updated linkState value: %d\n", iLinkState);

		/* Reset the dalTestLocalDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestLocalDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}

		/* Reset the dalTestConnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestConnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}

		/* Reset the dalTestRemoteDisconnEvent handle */
		iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestRemoteDisconnEvent);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}

		/*Open a Glink channel*/
		Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
	
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_CONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");
		
		/*wait for remote port to receive and process events*/
		DALSYS_BusyWait(1000000);
		
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_REMOTE_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
			if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
				AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
				GlinkTestDestroyEvents(&dalTestEvent);
				return TF_RESULT_CODE_FAILURE;
			}
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		Status = glink->GlinkDeregisterLinkStateCB(testLinkId.handle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink register link state cb failed. Returned %d\n", glinkRetCode);
			//close port here
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			GlinkTestDestroyEvents(&dalTestEvent);
			return TF_RESULT_CODE_FAILURE;
		}
		AsciiPrint (PRINTSIG"Glink Channel closed successfully\n");
		/*wait for remote port to receive and process events*/
		DALSYS_BusyWait(1000000);

	}
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;
}

/* Help Descriptor for GlinkDataTransferSSR */
static const TF_HelpDescr GlinkDataTransferSSRHelp = {
    "GlinkDataTransferSSR: MPROC_GLINK_L2",
     sizeof(GlinkDataTransferSSRTestParam) / sizeof(TF_ParamDescr),
     GlinkDataTransferSSRTestParam
};

/* TestFunction Descriptor for GlinkDataTransferSSR */
const TF_TestFunction_Descr GlinkDataTransferSSRTest = {
    PRINTSIG "GlinkDataTransferSSR",
    GlinkDataTransferSSR,
    &GlinkDataTransferSSRHelp,
    &gGlinkTestCmnContext
};

/*===========================================================================
  FUNCTION  glinkTest_Custom_RxNotificationCb
===========================================================================*/
/**
  glinkTest_Custom_RxNotificationCb

  @param[in] 

  @return 
*/
/*==========================================================================*/
void glinkTest_Custom_RxNotificationCb(glink_handle_type handle,
				const void        *priv,      
				const void        *pkt_priv,
				const void        *ptr,
				size_t            size,
				size_t            intent_used){

    DALResult        dalResult     	= DAL_ERROR; 
	GlinkTestChannelInfoType		*pGlinkChannelInfoObj = NULL;
	glink_err_type         		   glinkRetCode;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return;
	}
	
	if(NULL == priv){
		AsciiPrint (PRINTSIG"Error: glinkTest_Custom_RxNotificationCb: Private Info passed is NULL\n");
		return;
	}
	
	pGlinkChannelInfoObj = (GlinkTestChannelInfoType* )priv;

	Status = glink->GlinkRxDone(pGlinkChannelInfoObj->hGlinkHandle, ptr, 1, &glinkRetCode);
	
	/*Received data, triggering the event complete*/
	dalResult = DALSYS_EventCtrl(*(pGlinkChannelInfoObj->dalTestEvent->dalTestRxCbEvent),
                                         DALSYS_EVENT_CTRL_TRIGGER);
	if(DAL_SUCCESS != dalResult){
		AsciiPrint (PRINTSIG"Error: glinkTest_Custom_RxNotificationCb: Failed to trigger dalTestRxCbEvent\n");
		return;
	}
	return;
}

/* ============================================================================
**  Function : GlinkSimultaenousTX
** ========================================================================== */
/** 
@brief 
This function does glink data transfer to the remote client continously.
GlinkSimultaenousTX has to be run on the remote side as well to create
Simultaenous data transfer between the clients scenario.

@param [in] dwarg
@param [in] pszArg[]

@return
TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
TF_RESULT_CODE_FAILURE   - If test failed\n
TF_RESULT_CODE_SUCCESS   - If test passed.

@dependencies
None.

@par Test Type
Automated.
*/

uint32 GlinkSimultaenousTX(uint32 dwArg, char* pszArg[]){
	uint32        				iNumParam  = 0, i = 0, j = 0;
	int                     	iErrCheck = 0;
	glink_open_config_type  	glinkOpenCfg;
	glink_err_type         		glinkRetCode;
	GlinkTestChannelInfoType	glinkChannelInfoObj;
	GlinkEventPool    			dalTestEvent;
	uint32                  	iEvtAttrib[TEST_MPROC_NUM_OF_EVENTS] = {DALSYS_EVENT_ATTR_NORMAL,
									DALSYS_EVENT_ATTR_TIMEOUT_EVENT};
	uint32						iDataSize = 1024, iTotalDataInCore = 0;
	DALResult                   testResult = TF_RESULT_CODE_FAILURE;
	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}
	
	/*Input param check*/
	iNumParam = sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr);
	if (dwArg != iNumParam) {
		AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
			iNumParam, dwArg);
		return TF_RESULT_CODE_BAD_PARAM;
	}

	/*Input handler*/
	iErrCheck = GlinkTestInputHandler(&glinkChannelInfoObj,
						dwArg,
						pszArg,
						iNumParam,
						GLINK_TEST_TYPE_1);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
		return TF_RESULT_CODE_FAILURE;
	}

	/*Populate channel information from user input and map callbacks */
	iErrCheck = GlinkTestSetChannelInfo(&glinkChannelInfoObj, &glinkOpenCfg, &dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	glinkOpenCfg.notify_rx = glinkTest_Custom_RxNotificationCb;
	
	/*Create DalEvent objects, NUM_OF_EVENT = 2 */
	iErrCheck = GlinkTestCreateEvents(&dalTestEvent,iEvtAttrib);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Reset the dalTestLocalDisconnEvent handle */
	iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestLocalDisconnEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Reset the dalTestConnEvent handle */
	iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestConnEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	/* Reset the dalTestRemoteDisconnEvent handle */
	iErrCheck = GlinkTestResetEvent(glinkChannelInfoObj.dalTestEvent->dalTestRemoteDisconnEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	/*Open a Glink channel*/
	Status = glink->GlinkOpen(&glinkOpenCfg, &glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Error: Glink Channel open failed. Returned %d\n", glinkRetCode);
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_CONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Glink Channel opened successfully\n");

	/* Creating data for tx */
	iErrCheck = mprocTestMemAllocInit(&glinkChannelInfoObj.TransmitData,
									iDataSize,
									iDataSize / sizeof(uint32));
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
		GlinkTestDestroyEvents(&dalTestEvent);
		return TF_RESULT_CODE_FAILURE;
	}
	
	for(i = 0; i < glinkChannelInfoObj.iterations; i++){
	
		if( 0 == i) {
			j = 1;
		}
		
		else {
			j = 0;
		}
		/*Transmit data*/
		Status = glink->GlinkTx(glinkChannelInfoObj.hGlinkHandle,
								"test",
								(void *)glinkChannelInfoObj.TransmitData,
								iDataSize,
								j,
								&glinkRetCode); 
		if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
			goto cleanUp;
		}
		
		/* Wait for TEST_GLINK_TX_NOTIFICATION event */
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_TX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp;
		}
		
		iTotalDataInCore += iDataSize;
		
		/* Wait for TEST_GLINK_RX_NOTIFICATION event */
		iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_RX_NOTIFICATION); 
		if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
			goto cleanUp;
		}
		iTotalDataInCore += iDataSize;
		if(0 == i%10) {
			AsciiPrint (PRINTSIG"Data in core: %d & iteration: %d\n", iTotalDataInCore, i);
		}
	}
	testResult = TF_RESULT_CODE_SUCCESS;
cleanUp:
	iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_REMOTE_DISCONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		testResult = TF_RESULT_CODE_FAILURE;
	}

	Status = glink->GlinkClose(glinkChannelInfoObj.hGlinkHandle, &glinkRetCode);
	if((EFI_SUCCESS != Status) || (GLINK_STATUS_SUCCESS != glinkRetCode)){
		AsciiPrint (PRINTSIG"Error: Glink Channel close failed. Returned %d\n", glinkRetCode);
		GlinkTestDestroyEvents(&dalTestEvent);
		mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
		return TF_RESULT_CODE_FAILURE;
	}
	
	iErrCheck = GlinkTestWaitEvent(&glinkChannelInfoObj, TEST_GLINK_LOCAL_DISCONNECTED);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		GlinkTestDestroyEvents(&dalTestEvent);
		mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint (PRINTSIG"Glink Channel closed successfully\n");
	iErrCheck = GlinkTestDestroyEvents(&dalTestEvent);
	if (TF_RESULT_CODE_SUCCESS  != iErrCheck){
		testResult = TF_RESULT_CODE_FAILURE;
	}

	mprocTestMemFree((void *)glinkChannelInfoObj.TransmitData);
	
	return testResult;
}

/* Help Descriptor for GlinkSimultaenousTX */
static const TF_HelpDescr GlinkSimultaenousTXHelp = {
    "GlinkSimultaenousTX: MPROC_GLINK_L2",
     sizeof(GlinkOpenTestParam) / sizeof(TF_ParamDescr),
     GlinkOpenTestParam
};

/* TestFunction Descriptor for GlinkSimultaenousTX */
const TF_TestFunction_Descr GlinkSimultaenousTXTest = {
    PRINTSIG "GlinkSimultaenousTX",
    GlinkSimultaenousTX,
    &GlinkSimultaenousTXHelp,
    &gGlinkTestCmnContext
};


/*=============================================================================
                                API TESTS
===============================================================================*/

/* ============================================================================
**  Function : GlinkOpenAPI
** ========================================================================== */
/**
   @brief
   This function is called to open a Glink logical channel.

   @param pszArg[1]  - [IN] dwArg  - Number of inputs passed.
   @param pszArg[2]  - [IN] pszArg - array to input.

   @par Dependencies
   None

   @par Side Effects
   Allocates a channel resource and does not close it.

   @return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.

*/

uint32 GlinkOpenAPI(uint32 dwArg, char *pszArg[]) {

   	glink_open_config_type  glinkOpenCfg;
   	uint32        	   		iNumParam      = 0;
   	char                    *pEndChar      = NULL; 
   	uint32                  iErrCheck      = 0;
   	boolean                 bIsAdversarial = 0;  
   	glink_err_type          glinkRetCode   = GLINK_STATUS_FAILURE; 
   	EFI_STATUS               	Status = EFI_SUCCESS;
   	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

   iNumParam = sizeof(GlinkOpenAPITestParam) / sizeof(TF_ParamDescr); 
   if (dwArg != iNumParam) {
      AsciiPrint (PRINTSIG"\nError: # of input parameters incorrect: exp %d got %d!\n",
                iNumParam, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }
   
  /*Input parameters*/
   iErrCheck = GlinkTestInputHandler(&gGlinkTestChannelInfoObj,
                                     dwArg,
                                     pszArg,
                                     iNumParam,
                                     GLINK_TEST_TYPE_API); 

   if (TF_RESULT_CODE_SUCCESS != iErrCheck ) {
        return TF_RESULT_CODE_FAILURE;
    }
   
   /*Populate Channel Information*/
   iErrCheck = GlinkTestSetChannelInfo(&gGlinkTestChannelInfoObj, &glinkOpenCfg, &gDalEvent); 
   if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
        return TF_RESULT_CODE_FAILURE;
    } 
   
   AsciiPrint (PRINTSIG"Glink Channel Parameters are set\n");

   /*Adversarial flag choice*/
   bIsAdversarial = (uint8)strtol(pszArg[4], &pEndChar, 10);
   AsciiPrint (PRINTSIG"Adversarial Flag status: %d\n", bIsAdversarial);  
 
   /*Calling Glink Open API*/
   Status = glink->GlinkOpen(&glinkOpenCfg, &(gGlinkTestChannelInfoObj.hGlinkHandle), &glinkRetCode);
   return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial);
}

/* Help Descriptor for GlinkOpenAPI */
static const TF_HelpDescr GlinkOpenAPIHelp = {
   "GlinkOpenAPI: MPROC_GLINK_L1",
   sizeof(GlinkOpenAPITestParam) / sizeof(TF_ParamDescr),
   GlinkOpenAPITestParam
}; 

/* TestFunction Descriptor for GlinkOpenAPI */
const TF_TestFunction_Descr GlinkOpenAPITest = {
   PRINTSIG "GlinkOpenAPI",
   GlinkOpenAPI,
   &GlinkOpenAPIHelp, 
   &gGlinkTestCmnContext
}; 
   
/* ============================================================================
**  Function : GlinkCloseAPI
** ========================================================================== */
/**
   @brief
   This function is called to close a Glink logical channel.

   @param pszArg[1]  - [IN] dwArg - Number of inputs passed.
   @param pszArg[2]  - [IN] pszArg - array to input.

   @par Dependencies
   None

   @par Side Effects
   Allocates channel resources and informs remote host about
   channel open.

   @return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.

*/

uint32 GlinkCloseAPI(uint32 dwArg, char *pszArg[]) {

   uint8             handleChoice = 0; 
   uint32            iNumParam    = 0;
   char              *pEndChar    = NULL;
   glink_err_type    glinkRetCode = GLINK_STATUS_FAILURE;
   	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	} 
   
   iNumParam = sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr);
   if (dwArg != iNumParam) {
      AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                iNumParam, dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }
   
   /*Input parameters*/
   
   /*Handle Type Choice*/
   handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
   AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice);
    

   if (!handleChoice) {
      AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");

      /*Calling glink close API with NULL handle*/
      Status = glink->GlinkClose(NULL, &glinkRetCode);
      return GlinkAdvOutputHandler(glinkRetCode, 1);
   } else {
      AsciiPrint (PRINTSIG"Passing valid handle to API as user requested\n");

      /*Calling glink close API with valid handle*/
      Status = glink->GlinkClose(gGlinkTestChannelInfoObj.hGlinkHandle, &glinkRetCode); 
      return GlinkAdvOutputHandler(glinkRetCode, 0);
   }
}
/* Help Descriptor for GlinkCloseAPI */
   static const TF_HelpDescr GlinkCloseAPIHelp = {
      "GlinkCloseAPI: MPROC_GLINK_L1",
      sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr),
      GlinkAPICmnTestParam
   };

/* TestFunction Descriptor for GlinkCloseAPI */
   const TF_TestFunction_Descr GlinkCloseAPITest = {
      PRINTSIG "GlinkCloseAPI",
      GlinkCloseAPI,
      &GlinkCloseAPIHelp,
      &gGlinkTestCmnContext
   };
   
/* ============================================================================
**  Function : GlinkTxAPI
** ========================================================================== */
/**
  @brief
  This function is called to transmit data over a Glink logical channel.

  @param[in]   size       Size of buffer
 
  @param[in]   req_intent Whether to block and request for remote rx intent in
                          case it is not available for this pkt tx
 
  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/
   uint32 GlinkTxAPI(uint32 dwArg, char *pszArg[]) {

      uint8             iDataChoice     = 0; 
      //uint8             iReqIntent      = 0;
      uint8             handleChoice    = 0; 
      uint32            iNumParam       = 0;
      char              *pEndChar       = NULL;
      uint32            *iPrivData      = NULL;
      uint32            privData        = 2;
      uint32            iDataLen        = 0;
      //uint32            iDataSize       = 0;
      int               iErrCheck       = 0;
      boolean           bIsAdversarial  = 0;   
      glink_err_type    glinkRetCode    = GLINK_STATUS_FAILURE;
	  EFI_STATUS               	Status = EFI_SUCCESS;
	  STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	} 

      iNumParam = sizeof(GlinkTxCmnTestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }
      
      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice); 
      
     /*Data Type Choice*/
      iDataChoice = (uint8)strtol(pszArg[1], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Data Choice entered:%d\n", iDataChoice); 
      
      /*Data Size*/
      gGlinkTestChannelInfoObj.iTxDataSize = (uint32)strtol(pszArg[2], &pEndChar, 10);
	  if(32768 < gGlinkTestChannelInfoObj.iTxDataSize){
	      AsciiPrint (PRINTSIG"Error: Please enter valid data size\n");
		  return TF_RESULT_CODE_BAD_PARAM;
	  }
      AsciiPrint (PRINTSIG"Data Size entered:%d\n", gGlinkTestChannelInfoObj.iTxDataSize); 
      if (iDataChoice) {
         iPrivData = &privData; 
         if (gGlinkTestChannelInfoObj.iTxDataSize) {
            /*Calculate the corresponding data length needed*/
            iDataLen = gGlinkTestChannelInfoObj.iTxDataSize / sizeof(uint32); 

            /*Allocate data buffer, create and fill the data*/
            iErrCheck = mprocTestMemAllocInit(&gGlinkTestChannelInfoObj.TransmitData, gGlinkTestChannelInfoObj.iTxDataSize, iDataLen); 
            if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
               return TF_RESULT_CODE_FAILURE;
            }
        } else {
            bIsAdversarial = 1; 
            AsciiPrint (PRINTSIG"Passing Data Size as 0 to API as user requested\n"); 
            gGlinkTestChannelInfoObj.TransmitData = &gDummyData;
         }
      } else {
         bIsAdversarial = 1; 
         AsciiPrint (PRINTSIG"Passing Data as NULL to API as user requested\n"); 
         if (!(gGlinkTestChannelInfoObj.iTxDataSize)) {
            AsciiPrint (PRINTSIG"Passing Data Size as 0 to API as user requested\n");
         }
      }
      
      /*Request Intent Choice*/
      gGlinkTestChannelInfoObj.bReqIntent = (uint8)strtol(pszArg[3], &pEndChar, 10); 
      AsciiPrint (PRINTSIG"Request Intent Choice:%d\n", gGlinkTestChannelInfoObj.bReqIntent); 
          
      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
		
         /*Calling glink_tx with null handle*/
         Status = glink->GlinkTx(NULL,
		 						iPrivData,
								gGlinkTestChannelInfoObj.TransmitData,
								gGlinkTestChannelInfoObj.iTxDataSize,
								gGlinkTestChannelInfoObj.bReqIntent,
								&glinkRetCode); 
         return GlinkAdvOutputHandler(glinkRetCode, 1);
      } else {
         AsciiPrint (PRINTSIG"Passing valid handle to API as user requested\n");

         /*Calling glink_tx with valid handle*/
         Status = glink->GlinkTx(gGlinkTestChannelInfoObj.hGlinkHandle,
		 						iPrivData,
								gGlinkTestChannelInfoObj.TransmitData,
								gGlinkTestChannelInfoObj.iTxDataSize,
								gGlinkTestChannelInfoObj.bReqIntent,
								&glinkRetCode); 
         return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial); 
         }
      }
    /* Help Descriptor for GlinkTXAPI */
   static const TF_HelpDescr GlinkTxAPIHelp = {
   "GlinkTXAPI: MPROC_GLINK_L1",
   sizeof(GlinkTxCmnTestParam) / sizeof(TF_ParamDescr),
   GlinkTxCmnTestParam
    };

   /* TestFunction Descriptor for GlinkTxAPI */
   const TF_TestFunction_Descr GlinkTxAPITest = {
      PRINTSIG "GlinkTxAPI",
      GlinkTxAPI,
      &GlinkTxAPIHelp,
      &gGlinkTestCmnContext
   };
   
/* ============================================================================
**  Function : GlinkTxvAPI
** ========================================================================== */
/**
  @brief
  This function is called to transmit data in a vector buffer over a Glink logical channel.

  @param[in]   size       Size of buffer
 
  @param[in]   req_intent Whether to block and request for remote rx intent in
                          case it is not available for this pkt tx
 
  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/

   uint32 GlinkTxvAPI(uint32 dwArg, char *pszArg[]) {

      uint8             iDataChoice     = 0;
      //uint8             iReqIntent      = 0;
      uint8             handleChoice    = 0;
      uint32            iNumParam       = 0;
      char              *pEndChar       = NULL;
      uint32            *iPrivData      = NULL;
      //uint32            *iDataVBuffer    = NULL;
      uint32            iDataLen        = 0;
      //uint32            iDataSize       = 0;
      int               iErrCheck       = 0;
      boolean           bIsAdversarial  = 0;
      glink_err_type    glinkRetCode    = GLINK_STATUS_FAILURE;
	  EFI_STATUS               	Status = EFI_SUCCESS;
	  STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

      iNumParam = sizeof(GlinkTxCmnTestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }

      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice);

      /*Data Type Choice*/
      iDataChoice = (uint8)strtol(pszArg[1], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Data Choice entered:%d\n", iDataChoice);

      /*Data Size*/
      gGlinkTestChannelInfoObj.iTxDataSize = (uint32)strtol(pszArg[2], &pEndChar, 10);
	  if(32768 < gGlinkTestChannelInfoObj.iTxDataSize){
		  AsciiPrint (PRINTSIG"Error: Please enter valid data size\n");
		  return TF_RESULT_CODE_BAD_PARAM;
	  }	  
      AsciiPrint (PRINTSIG"Data Size entered:%d\n", gGlinkTestChannelInfoObj.iTxDataSize); 

      if (iDataChoice) {
         iPrivData = &gPrivData;
         if (gGlinkTestChannelInfoObj.iTxDataSize) {
            /*Calculate the corresponding data length needed*/
            iDataLen = gGlinkTestChannelInfoObj.iTxDataSize / sizeof(uint32); 

            /*Allocate data buffer, create and fill the data*/
            iErrCheck = mprocTestMemAllocInit(&gGlinkTestChannelInfoObj.TransmitData, gGlinkTestChannelInfoObj.iTxDataSize, iDataLen); 
            if (TF_RESULT_CODE_SUCCESS  != iErrCheck) {
               return TF_RESULT_CODE_FAILURE;
            }
         } else {
            bIsAdversarial = 1;
            AsciiPrint (PRINTSIG"Passing Data Size as 0 to API as user requested\n");
            gGlinkTestChannelInfoObj.TransmitData = &gPrivData; 
         }
      } else {
         bIsAdversarial = 1;
         AsciiPrint (PRINTSIG"Passing Data as NULL to API as user requested\n");
         if (!gGlinkTestChannelInfoObj.iTxDataSize) {
            AsciiPrint (PRINTSIG"Passing Data Size as 0 to API as user requested\n");
         }
      }

      /*Request Intent Choice*/
      gGlinkTestChannelInfoObj.bReqIntent = (uint8)strtol(pszArg[3], &pEndChar, 10); 
      AsciiPrint (PRINTSIG"Request Intent Choice:%d\n", gGlinkTestChannelInfoObj.bReqIntent); 

      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
        
         /*Calling glink_tx with null handle*/
         Status = glink->GlinkTxV(NULL,
		 						iPrivData,
								gGlinkTestChannelInfoObj.TransmitData,
								gGlinkTestChannelInfoObj.iTxDataSize,
								NULL,
								NULL,
								gGlinkTestChannelInfoObj.bReqIntent,
								&glinkRetCode); 
         return GlinkAdvOutputHandler(glinkRetCode, 1);
      } else {
         AsciiPrint (PRINTSIG"Passing valid handle to API as user requested\n");

         /*Calling glink_tx with valid handle*/
         Status = glink->GlinkTxV(gGlinkTestChannelInfoObj.hGlinkHandle,
		 							iPrivData,
									gGlinkTestChannelInfoObj.TransmitData,
									gGlinkTestChannelInfoObj.iTxDataSize,
									NULL,
									NULL,
									gGlinkTestChannelInfoObj.bReqIntent,
									&glinkRetCode); 
         return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial);
      }
   }
   /* Help Descriptor for GlinkTxvAPI */
   static const TF_HelpDescr GlinkTxvAPIHelp = {
      "GlinkTxvAPI: MPROC_GLINK_L1",
      sizeof(GlinkTxCmnTestParam) / sizeof(TF_ParamDescr),
      GlinkTxCmnTestParam
   };

   /* TestFunction Descriptor for GlinkTxvAPI */
   const TF_TestFunction_Descr GlinkTxvAPITest = {
      PRINTSIG "GlinkTxvAPI",
      GlinkTxvAPI,
      &GlinkTxvAPIHelp,
      &gGlinkTestCmnContext
   }; 

/* ============================================================================
**  Function : GlinkQueueAPI
** ========================================================================== */
/**
  @brief
  This function is called to queue an Rx intent over a Glink logical channel.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
     		           case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/

   uint32 GlinkQueueAPI(uint32 dwArg, char *pszArg[]) {

      uint8             handleChoice  = 0; 
      uint32            iNumParam     = 0;
      char              *pEndChar     = NULL;
      uint32            *iPrivData    = NULL;
      uint32            privData      = 2;    
      uint32            iDataSize     = 0;
      boolean              bIsAdversarial = 0;
      glink_err_type    glinkRetCode  = GLINK_STATUS_FAILURE; 
	  EFI_STATUS               	Status = EFI_SUCCESS;
	  STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}

      iNumParam = sizeof(GlinkQueueIntentTestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }

      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice); 

      /*Data Size*/
      iDataSize = (uint32)strtol(pszArg[1], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Data Size entered:%d\n", iDataSize);
      if (!iDataSize) {
         AsciiPrint (PRINTSIG"Passing data size as 0 as user requested\n");
         bIsAdversarial = 1; 
         iPrivData = &privData;
      }

      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");

         Status = glink->GlinkQueueRxIntent(NULL, iPrivData, iDataSize, &glinkRetCode);

         return GlinkAdvOutputHandler(glinkRetCode, 1);
      } else {
         AsciiPrint (PRINTSIG"Passing valid handle to API as user requested"); 

         /*Calling glink_tx with valid handle*/
         Status = glink->GlinkQueueRxIntent(gGlinkTestChannelInfoObj.hGlinkHandle, iPrivData, iDataSize, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial); 
      }
  }     
      
   /* Help Descriptor for GlinkQueueAPI */
      static const TF_HelpDescr GlinkQueueAPIHelp = {
         "GlinkQueueAPI: MPROC_GLINK_L1", 
         sizeof(GlinkQueueIntentTestParam) / sizeof(TF_ParamDescr),
         GlinkQueueIntentTestParam
      }; 

      /* TestFunction Descriptor for GlinkQueueAPI */
      const TF_TestFunction_Descr GlinkQueueAPITest = {
         PRINTSIG "GlinkQueueAPI",
         GlinkQueueAPI,
         &GlinkQueueAPIHelp, 
         &gGlinkTestCmnContext
      };    

/* ============================================================================
**  Function : GlinkRxDoneAPI
** ========================================================================== */
/**
  @brief
  This function is called to a signal that Rx operation is completed.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/

   uint32 GlinkRxDoneAPI(uint32 dwArg, char *pszArg[]) {

      uint8             iReUseChoice        = 0;
      uint8             iRecvdBufferChoice  = 0;
      uint8             handleChoice        = 0; 
      uint32            iNumParam           = 0;
      char              *pEndChar  	    = NULL; 
      uint32            *iRecvDataBuffer    = NULL;
      boolean           bIsAdversarial      = 0; 
      glink_err_type    glinkRetCode        = GLINK_STATUS_FAILURE; 
	  	EFI_STATUS               	Status = EFI_SUCCESS;
	STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}


      iNumParam = sizeof(GlinkRxDoneAPITestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }

      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice);

      /*Received Buffer Choice*/
      iRecvdBufferChoice = (uint8)strtol(pszArg[1], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Received Buffer Choice:%d\n", iRecvdBufferChoice);

      if (!iRecvdBufferChoice) {
         AsciiPrint (PRINTSIG"Passing received data buffer as NULL to API as user requested\n");
         bIsAdversarial = 1; 
      } else {
         iRecvDataBuffer = gGlinkTestChannelInfoObj.ReceivedData; 
         AsciiPrint (PRINTSIG"Passing received data buffer as a valid one to API as user requested\n");
      }

      /*Buffer Reuse Choice*/
      iReUseChoice = (uint8)strtol(pszArg[2], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Buffer Reuse Choice:%d\n", iReUseChoice);

      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
         Status = glink->GlinkRxDone(NULL, iRecvDataBuffer, iReUseChoice, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, 1);
     } else {
         AsciiPrint (PRINTSIG"Passing valid handle to API as user requested"); 

         /*Calling glink_tx with valid handle*/
         Status = glink->GlinkRxDone(gGlinkTestChannelInfoObj.hGlinkHandle, iRecvDataBuffer, iReUseChoice, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial); 
      }
   }

   /* Help Descriptor for GlinkRxDoneAPI */
   static const TF_HelpDescr GlinkRxDoneAPIHelp = {
      "GlinkRxDoneAPI: MPROC_GLINK_L1",
      sizeof(GlinkRxDoneAPITestParam) / sizeof(TF_ParamDescr),
      GlinkRxDoneAPITestParam
   }; 

   /* TestFunction Descriptor for GlinkTxAPI */
   const TF_TestFunction_Descr GlinkRxDoneAPITest = {
      PRINTSIG "GlinkRxDoneAPI",
      GlinkRxDoneAPI,
      &GlinkRxDoneAPIHelp, 
      &gGlinkTestCmnContext
   };    

/*============================================================================
 Function : GlinkSigSetAPI
========================================================================== */
/**
   brief
   This function is called to set the 32 bit control signal field.

   param[in]   size        Size of buffer

   param[in]   req_intent  Whether to block and request for remote rx intent in
                    case it is not available for this pkt tx

   par Dependencies
   None

   par Side Effects
   causes remote host to wake-up and process rx pkt

   return
   TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
   TF_RESULT_CODE_FAILURE   - If test failed\n
   TF_RESULT_CODE_SUCCESS   - If test passed.

*/

   uint32 GlinkSigSetAPI(uint32 dwArg, char *pszArg[]) {

      uint8             handleChoice    = 0; 
      uint32            iNumParam       = 0;
      char              *pEndChar       = NULL;
      boolean              bIsAdversarial  = 0;
      uint32            iSigValue       = 0; 
      glink_err_type    glinkRetCode    = GLINK_STATUS_FAILURE; 
      EFI_STATUS               	Status = EFI_SUCCESS;
	  STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
	/*Locate the respective protocol for use*/
	Status = GlinkLocateProtocol(&glink);
	if(EFI_ERROR(Status)){
		AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
		return TF_RESULT_CODE_FAILURE;
	}
	
	if(NULL == glink){
		AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
		return(TF_RESULT_CODE_FAILURE);
	}


      iNumParam = sizeof(GlinkSigSetAPITestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }

      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice); 

      iSigValue = (uint32)strtol(pszArg[1], &pEndChar, 10);
      /*TODO: Get the invalid option values for particular transport
	  to switch adversarial flag to true.
	  */
      AsciiPrint (PRINTSIG"Signal value entered: %d\n", iSigValue);
      
      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
         Status = glink->GlinkSigsSet(NULL, iSigValue, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, 1);
     } else {
         AsciiPrint (PRINTSIG"Passing valid handle to API as user requested");
         Status = glink->GlinkSigsSet(gGlinkTestChannelInfoObj.hGlinkHandle, iSigValue, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, bIsAdversarial); 
      }
   }  
   /* Help Descriptor for GlinkRxDoneAPI */
   static const TF_HelpDescr GlinkSigSetAPIHelp = {
      "GlinkSigSetAPI: MPROC_GLINK_L1",
      sizeof(GlinkSigSetAPITestParam) / sizeof(TF_ParamDescr),
      GlinkSigSetAPITestParam
   };

   /* TestFunction Descriptor for GlinkTxAPI */
   const TF_TestFunction_Descr GlinkSigSetAPITest = {
      PRINTSIG "GlinkSigSetAPI",
      GlinkSigSetAPI,
      &GlinkSigSetAPIHelp,
      &gGlinkTestCmnContext
   };


/* ============================================================================
**  Function : GlinkSigLocalGetAPI
** ========================================================================== */
/**
  @brief
  This function is called to get the local 32 bit control signal field.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/

      uint32 GlinkSigLocalGetAPI(uint32 dwArg, char *pszArg[]) {

         uint8             handleChoice    = 0; 
         uint32            iNumParam       = 0;
         char              *pEndChar	   = NULL;
         uint32            iSigValue       = 0; 
         glink_err_type    glinkRetCode    = GLINK_STATUS_FAILURE; 
         EFI_STATUS               	Status = EFI_SUCCESS;
		 STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
		/*Locate the respective protocol for use*/
		Status = GlinkLocateProtocol(&glink);
		if(EFI_ERROR(Status)){
			AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
			return TF_RESULT_CODE_FAILURE;
		}
	
		if(NULL == glink){
			AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
			return(TF_RESULT_CODE_FAILURE);
		}


         iNumParam = sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr);
         if (dwArg != iNumParam) {
            AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                      iNumParam, dwArg);
            return TF_RESULT_CODE_BAD_PARAM;
         }

         /*Handle Type Choice*/
         handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
         AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice); 

         if (!handleChoice) {
            AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
            Status = glink->GlinkSigsLocalGet(NULL, &iSigValue, &glinkRetCode);
            return GlinkAdvOutputHandler(glinkRetCode, 1);
         } else {
            AsciiPrint (PRINTSIG"Passing valid handle to API as user requested\n"); 
            Status = glink->GlinkSigsLocalGet(gGlinkTestChannelInfoObj.hGlinkHandle, &iSigValue, &glinkRetCode); 
            return GlinkAdvOutputHandler(glinkRetCode, 0);
         }
      }   
   /* Help Descriptor for GlinkRxDoneAPI */
   static const TF_HelpDescr GlinkSigLocalGetAPIHelp = {
      "GlinkSigLocalGetAPI: MPROC_GLINK_L1",
      sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr),
      GlinkAPICmnTestParam
   };
   
   /* TestFunction Descriptor for GlinkTxAPI */
   const TF_TestFunction_Descr GlinkSigLocalGetAPITest = {
      PRINTSIG "GlinkSigLocalGetAPI",
      GlinkSigLocalGetAPI,
      &GlinkSigLocalGetAPIHelp,
      &gGlinkTestCmnContext
   };

/* ============================================================================
**  Function : GlinkSigRemoteGetAPI
** ========================================================================== */
/**
  @brief
  This function is called to get the remote 32 bit control signal field.

  @param[in]   size        Size of buffer

  @param[in]   req_intent  Whether to block and request for remote rx intent in
                       case it is not available for this pkt tx

  @par Dependencies
  None

  @par Side Effects
  Causes remote host to wake-up and process rx pkt

  @return
  TF_RESULT_CODE_BAD_PARAM - If Bad Params passed to test\n
  TF_RESULT_CODE_FAILURE   - If test failed\n
  TF_RESULT_CODE_SUCCESS   - If test passed.

*/

   uint32 GlinkSigRemoteGetAPI(uint32 dwArg, char *pszArg[]) {

      uint8             handleChoice   = 0;
      uint32            iNumParam      = 0;
      char              *pEndChar      = NULL;
      uint32            iSigValue      = 0;
      glink_err_type    glinkRetCode   = GLINK_STATUS_FAILURE; 
	  EFI_STATUS               	Status = EFI_SUCCESS;
	  STATIC EFI_GLINK_PROTOCOL 	*glink = NULL;
 
		/*Locate the respective protocol for use*/
		Status = GlinkLocateProtocol(&glink);
		if(EFI_ERROR(Status)){
			AsciiPrint (PRINTSIG"Error: Protocol Locate failure\n");
			return TF_RESULT_CODE_FAILURE;
		}
	
		if(NULL == glink){
			AsciiPrint(PRINTSIG"Error: glink received NULL handle\n");
			return(TF_RESULT_CODE_FAILURE);
		}

      iNumParam = sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr);
      if (dwArg != iNumParam) {
         AsciiPrint (PRINTSIG"Error: # of input parameters incorrect: exp %d got %d!\n",
                   iNumParam, dwArg);
         return TF_RESULT_CODE_BAD_PARAM;
      }

      /*Handle Type Choice*/
      handleChoice = (uint8)strtol(pszArg[0], &pEndChar, 10);
      AsciiPrint (PRINTSIG"Handle Choice:%d\n", handleChoice); 

      if (!handleChoice) {
         AsciiPrint (PRINTSIG"Passing handle as NULL to API as user requested\n");
         Status = glink->GlinkSigsRemoteGet(NULL, &iSigValue, &glinkRetCode);
         return GlinkAdvOutputHandler(glinkRetCode, 1);
      } else {
         Status = glink->GlinkSigsRemoteGet(gGlinkTestChannelInfoObj.hGlinkHandle, &iSigValue, &glinkRetCode); 
         return GlinkAdvOutputHandler(glinkRetCode, 0);
      }
   }
    /* Help Descriptor for GlinkRxDoneAPI */
    static const TF_HelpDescr GlinkSigRemoteGetAPIHelp = {
       "GlinkSigRemoteGetAPI: MPROC_GLINK_L1",
       sizeof(GlinkAPICmnTestParam) / sizeof(TF_ParamDescr),
       GlinkAPICmnTestParam
    };

    /* TestFunction Descriptor for GlinkTxAPI */
    const TF_TestFunction_Descr GlinkSigRemoteGetAPITest = {
       PRINTSIG "GlinkSigRemoteGetAPI",
       GlinkSigRemoteGetAPI,
       &GlinkSigRemoteGetAPIHelp,
       &gGlinkTestCmnContext
    };


    /*Array of functions that needed to be added during init process*/
    const TF_TestFunction_Descr *paGlinkDaltfTests[] = {
       &GlinkLBTester,				      //MPROC_GLINK_L1
       &GlinkOpenSingleLocalTest,         //MPROC_GLINK_L1
       &GlinkDataTransferTest,	          //MPROC_GLINK_L1
       &GlinkStressOpenClosePortTest,     //MPROC_GLINK_L1
       &GlinkStressOpenCloseServTest,     //MPROC_GLINK_L1
       &GlinkDataTransferWithLoopbackTest, //MPROC_GLINK_L1
       &GlinkDataTrsfrServTest,           //MPROC_GLINK_L2
	   &GlinkDataTransferSSRTest,         //MPROC_GLINK_L2
	   &GlinkSimultaenousTXTest,         //MPROC_GLINK_L2

       &GlinkOpenAPITest,                 //MPROC_GLINK_L1
       &GlinkCloseAPITest,                //MPROC_GLINK_L1
       &GlinkTxAPITest,                   //MPROC_GLINK_L1
       &GlinkTxvAPITest,                  //MPROC_GLINK_L1
       &GlinkQueueAPITest,                //MPROC_GLINK_L1
       &GlinkRxDoneAPITest,               //MPROC_GLINK_L1
       &GlinkSigSetAPITest,               //MPROC_GLINK_L1
       &GlinkSigLocalGetAPITest,          //MPROC_GLINK_L1
       &GlinkSigRemoteGetAPITest          //MPROC_GLINK_L1

    };

   /* ============================================================================
        **  Function : glink_daltf_init
   ** ========================================================================== */
   /**
      @brief
      Inits the Glink tests.

      @param
      void

      @par Dependencies
      None

      @par Side Effects
      None

      @return
      DAL_SUCCESS if "pass",
      TF_RESULT_CODE_FAILURE if  "fail"
      TF_RESULT_CODE_BAD_PARAM otherwise.
   */

   DALResult glink_daltf_init(void) {

       DALResult dalResult = DAL_SUCCESS;
       uint16 TF_NumberTests =(sizeof(paGlinkDaltfTests) / sizeof(paGlinkDaltfTests[0]));
       dalResult = tests_daltf_add_tests(paGlinkDaltfTests, TF_NumberTests);
       return dalResult;
   }

   /* ============================================================================
   **  Function : glink_daltf_deinit
   ** ========================================================================== */
   /**
      @brief
      Deinits the Glink tests.

	 @param 
      void

      @par Dependencies
      None

      @par Side Effects
      None

      @return
      DAL_SUCCESS if "pass",
      TF_RESULT_CODE_FAILURE if  "fail"
      TF_RESULT_CODE_BAD_PARAM otherwise.
*/

DALResult glink_daltf_deinit(void) {

    DALResult dalResult = DAL_SUCCESS;
    uint16 TF_NumberTests =(sizeof(paGlinkDaltfTests) / sizeof(paGlinkDaltfTests[0]));
    dalResult = tests_daltf_remove_tests(paGlinkDaltfTests, TF_NumberTests);
    return dalResult;
}
