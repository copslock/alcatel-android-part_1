/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		tests_daltf_common.c
  DESCRIPTION:	DAL TF  Tests Common Definitions
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  03/17/15   jz      Created
================================================================================*/

#include "tests_daltf_common.h" /* Common definitions      */

/*====================================================
  data declaration
====================================================*/

CONST TF_ContextDescr context1 = { (UINT8)DIAG_SUBSYS_COREBSP, 0 };

/*===========================================================================
  FUNCTION:  tests_daltf_add_tests
===========================================================================*/
/**
  add test cases into test framework

  This function will add tests in the list given by user

  @param  testDescr     [in]          test list to be added.
  @param  numberOfTests [in]          total number of tests in the list

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_PROTOCOL_ERROR    The function had a protocol error.
**/

DALResult tests_daltf_add_tests(CONST TF_TestFunction_Descr **testDescr, UINTN numberOfTests)
{
  EFI_STATUS          eResult;
  EFI_DALTF_PROTOCOL  *DALTFProtocol;

  eResult = gBS->LocateProtocol (
                  &gEfiDaltfProtocolGuid,
                  NULL,
                  (VOID **) &DALTFProtocol
                  );
  if(eResult == EFI_SUCCESS){
	if (NULL != testDescr){
	  eResult = DALTFProtocol->AddTests(testDescr, numberOfTests);
	  if (eResult != EFI_SUCCESS)
	    AsciiPrint("DALTF Add Tests error 0x%X\n", eResult);
	}
	else
	  AsciiPrint("Invalid test function description\n");
  }
  else
	AsciiPrint("failed to locate DALTFProtocol\n");
  return eResult;
}


/*===========================================================================
  FUNCTION:  tests_daltf_remove_tests
===========================================================================*/
/**
  remove test cases into test framework

  This function will remove tests in the list given by user

  @param  testDescr     [in]          test list to be added.
  @param  numberOfTests [in]          total number of tests in the list

  @return EFI_SUCCESS           The function completed successfully.
  @return EFI_PROTOCOL_ERROR    The function had a protocol error.
**/
DALResult tests_daltf_remove_tests
(
  CONST TF_TestFunction_Descr **testDescr,
  UINTN numberOfTests
)
{
  EFI_STATUS          eResult;
  EFI_DALTF_PROTOCOL  *DALTFProtocol;

  eResult = gBS->LocateProtocol (
				  &gEfiDaltfProtocolGuid,
				  NULL,
				  (VOID **) &DALTFProtocol
				  );
  if(eResult == EFI_SUCCESS){
	if (NULL != testDescr){
	  eResult = DALTFProtocol->RemoveTests(testDescr, numberOfTests);
	  if (eResult != EFI_SUCCESS)
		AsciiPrint("DALTF Remove Tests error 0x%X\n", eResult);
	}
	else
	  AsciiPrint("Invalid test function description\n");
  }
  else
	AsciiPrint("failed to locate DALTFProtocol\n");
  return eResult;
}

