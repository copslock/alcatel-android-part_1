/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelDalsysTest.h
  DESCRIPTION:	Common headers included for all SysDrv files
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _Kernel_Dalsys_TESTS_
#define _Kernel_Dalsys_TESTS_

#include "tests_daltf_common.h"   /* Common definitions */

#include <Protocol/EFIDALSYSProtocol.h>

#include "kernel_dal_daltf_dalsys_tests.h"
#include "kernel_dal_daltf_dalsys_sync_tests.h"
#include "kernel_dal_daltf_dalsys_mem_tests.h"
#include "kernel_dal_daltf_dalsys_events_tests.h"
#include "kernel_dal_daltf_dalsys_events_tests.h"
#include "systemfeatures_deviceconfig_daltf_tests.h"

#endif