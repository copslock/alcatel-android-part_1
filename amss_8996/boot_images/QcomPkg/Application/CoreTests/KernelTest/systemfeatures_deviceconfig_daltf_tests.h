/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelDalsysTest.h
  DESCRIPTION:	Header File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _KERNEL_DALTF_Devcfg_TESTS_
#define _KERNEL_DALTF_Devcfg_TESTS_

#include "KernelDalsysTest.h"
#include <DALSys.h>

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 DevicePropertyDisplay(UINT32 dwArg, CHAR8 *pszArg[]); 
UINT32 DevicePropertyTesting(UINT32 dwArg, CHAR8 *pszArg[]);
UINT32 DevicePropertyNegativeTesting(UINT32 dwArg, CHAR8 *pszArg[]); 
 
//Initialize devcfg daltf tests
DALResult kernel_daltf_devcfg_init_all(VOID);

//De-Initialize devcfg daltf tests
DALResult kernel_daltf_devcfg_deinit_all(VOID);

#endif