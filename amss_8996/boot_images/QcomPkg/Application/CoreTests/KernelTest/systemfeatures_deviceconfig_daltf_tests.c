/*============================================================================
@file         systemfeatures_deviceconfig_daltf_tests.c

@brief        DALTF based Dalsys tests

GENERAL DESCRIPTION
  This file contains the basic tests for Dalsys APIs.

Copyright (c) 2011 Qualcomm Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
============================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/KernelTest/systemfeatures_deviceconfig_daltf_tests.c#1 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
04/08/2011   rt     Intial Version
05/08/2011   rt     Updated tests for 8960 bring up
09/02/2011   rt     Code clean up and update to work with RIVA processor

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR CHIPINFO DRIVER TESTS

==========================================================================*/
#include "systemfeatures_deviceconfig_daltf_tests.h"
#include "DALSys.h"
#include "DDITimer.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      CONSTants and Macros
==========================================================================*/
CONST TF_ParamDescr ConfigParamDisplay[] =
{
   {TF_PARAM_STRING, "Device ID:", "UINT32 and String Device ID "}, 
   {TF_PARAM_STRING, "Property Name:", "Property Name:"}
};

CONST TF_ParamDescr ConfigParamTest[] =
{
   {TF_PARAM_STRING, "Device ID:", "UINT32 and String Device ID "}, 
   {TF_PARAM_STRING, "Property Name:", "Property Name:"},
   {TF_PARAM_STRING, "Type: ","DALPROP_ATTR_TYPE_UINT32_SEQ_PTR"
                              "DALPROP_ATTR_TYPE_BYTE_SEQ_PTR"
                              "DALPROP_ATTR_TYPE_STRING_PTR"
                              "DALPROP_ATTR_TYPE_UINT32" 
   },
   {TF_PARAM_STRING, "Length", "The value Length."},
   {TF_PARAM_STRING, "Value", "List of values of the property seperate with space."}
};

CHAR8 * ConvertDeviceType(UINT32 dwType)
{
   switch(dwType){
      case 18: return "DALPROP_ATTR_TYPE_UINT32_SEQ_PTR";
      case 17: return "DALPROP_ATTR_TYPE_BYTE_SEQ_PTR";
      case 16: return "DALPROP_ATTR_TYPE_STRING_PTR";
      case 2: return "DALPROP_ATTR_TYPE_UINT32";
   }
   return NULL;
}

UINT32 DataComp(DALSYSPropertyVar PropVar, CHAR8* Data[])
{
   typedef unsigned char byte;
   int dwIndex;
   CHAR8 * pEnd;
   byte bElm;
   UINT32 dwElm;
   switch(PropVar.dwType){
      case 18: 
         // "DALPROP_ATTR_TYPE_UINT32_SEQ_PTR";
         dwElm = strtol (Data[4],&pEnd,0);
         for(dwIndex=0;dwIndex<PropVar.dwLen;dwIndex++)
         {
            if(dwElm != PropVar.Val.pdwVal[dwIndex])
            {
               AsciiPrint(PRINTSIG"The Property Value does not match:\n\tThe Property Value Received  :%d \n\tThe Property Value Expected: %d", PropVar.Val.pdwVal[dwIndex], dwElm);
               return TF_RESULT_CODE_FAILURE;
            }
            dwElm = strtol (pEnd,&pEnd,0);
         }
         break;
      case 17: 
         // "DALPROP_ATTR_TYPE_BYTE_SEQ_PTR";
         bElm = strtol (Data[4],&pEnd,0);
         for(dwIndex=0;dwIndex<PropVar.dwLen;dwIndex++)
         {
            if(bElm != PropVar.Val.pbVal[dwIndex])
            {
               AsciiPrint(PRINTSIG"The Property Value does not match:\n\tThe Property Value Received :%x \n\tThe Property Value Expected: %x", PropVar.Val.pbVal[dwIndex], bElm);
               return TF_RESULT_CODE_FAILURE;
            }
            bElm = strtol (pEnd,&pEnd,0);
         }
         break;
      case 16: 
         // "DALPROP_ATTR_TYPE_STRING_PTR";
         if (strncmp(PropVar.Val.pszVal,Data[4],(strlen(PropVar.Val.pszVal)>strlen(Data[4]))?strlen(PropVar.Val.pszVal):strlen(Data[4])) != 0)
         {
            AsciiPrint(PRINTSIG"The Property Value does not match:\n\tThe Property Value Received:%s \n\tThe Property Value Expected: %s", PropVar.Val.pszVal,Data[4]);
            return TF_RESULT_CODE_FAILURE;
         }
         break;
      case 2: 
         // "DALPROP_ATTR_TYPE_UINT32";
         if (PropVar.Val.dwVal != strtol(Data[4], &pEnd,0))
         {
            AsciiPrint(PRINTSIG"The Property Value does not match:\n\tThe Property Value Received:%d \n\tThe Property Value Expected:%d", PropVar.Val.dwVal,strtol(Data[4], &pEnd, 0));
            return TF_RESULT_CODE_FAILURE;
         }
         break;
   }
   return TF_RESULT_CODE_SUCCESS;
}

VOID Display(DALSYSPropertyVar PropVar, CHAR8* Data[])
{
   int dwIndex;
   AsciiPrint(PRINTSIG"=====================\n The Property Length and Value:");
   AsciiPrint(PRINTSIG" The Property Length Received:\t%d",PropVar.dwLen);
   AsciiPrint(PRINTSIG"The Property Value Received:");
   switch(PropVar.dwType){
      case 18: 
         // "DALPROP_ATTR_TYPE_UINT32_SEQ_PTR";
         for(dwIndex=0;dwIndex<PropVar.dwLen;dwIndex++)
         {
            AsciiPrint(PRINTSIG"\t\t%d:%d",dwIndex+1, PropVar.Val.pdwVal[dwIndex]);
         }
         break;
      case 17: 
         // "DALPROP_ATTR_TYPE_BYTE_SEQ_PTR";
         for(dwIndex=0;dwIndex<PropVar.dwLen;dwIndex++)
         {
            AsciiPrint(PRINTSIG"\t\t%d:%x",dwIndex+1, PropVar.Val.pbVal[dwIndex]);
         }
         break;
      case 16: 
         // "DALPROP_ATTR_TYPE_STRING_PTR";
         AsciiPrint(PRINTSIG"\t\t1:%s", PropVar.Val.pszVal);
         break;
      case 2: 
         // "DALPROP_ATTR_TYPE_UINT32";
         AsciiPrint(PRINTSIG"\t\t%d", PropVar.Val.dwVal);
         break;
   }
}

UINT32 DevicePropertyTesting_help(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYSPropertyVar PropVar;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProps);
   CHAR8 * pEnd;
   UINT32 dwDeviceName;
   UINT32 dwlen;
   CHAR8 * pDeviceName = pszArg[0];
   CHAR8 * pDeviceProp = pszArg[1];
   CHAR8 * pType = pszArg[2];
   CHAR8 * arg_len = pszArg[3];
   // DALDEVICEID DeviceId = 0;


   if (dwArg != (sizeof(ConfigParamTest) / sizeof(TF_ParamDescr))){
      AsciiPrint(PRINTSIG"Number of input parameters incorrect: exp %d got %d", sizeof(ConfigParamTest) / sizeof(TF_ParamDescr), dwArg);
      return TF_RESULT_CODE_BAD_PARAM;
   }
  
   if ( NULL == pDeviceName ) 
   {
      AsciiPrint(PRINTSIG"No device Id");
      return TF_RESULT_CODE_BAD_PARAM;
   }
   
   // The keyboard "NULL" is replaced by NULL
   // if (strncmp(pDeviceName,"NULL",4)==0)
   // {
       // if (DAL_SUCCESS != DALSYS_GetDALPropertyHandle(DeviceId, hProps))
     // {
         // AsciiPrint(PRINTSIG"Could not have NULL pointer");
         // return TF_RESULT_CODE_FAILURE;
     // }
   // }
  
   AsciiPrint(PRINTSIG"\n=================\n Testing Device :\t%s",pDeviceName);
   //Checking the device type for correct API 
   // UINT32 Device ID
   if(pDeviceName!=NULL &&'0'==pDeviceName[0] && 'x'==pDeviceName[1])
   {
      AsciiPrint(PRINTSIG"****UINT32 Device****");
      pEnd = NULL;
      dwDeviceName=strtol(pDeviceName, &pEnd, 0);
     if (DAL_SUCCESS != DALSYS_GetDALPropertyHandle(dwDeviceName, hProps))
     {
         AsciiPrint(PRINTSIG"Could not find the UINT32 Device: %x",dwDeviceName);
         return TF_RESULT_CODE_FAILURE;
     }
   }else
   {
      AsciiPrint(PRINTSIG"****String Device****");
      if (DAL_SUCCESS!= DALSYS_GetDALPropertyHandleStr(pDeviceName, hProps)){
         AsciiPrint(PRINTSIG"Could not find the String Device: %s",pszArg[0]);
         return TF_RESULT_CODE_FAILURE;
      }
   }
    AsciiPrint(PRINTSIG"Device Name[1/5]: \tOK");
    
   // Property Name
   if (DAL_SUCCESS!= DALSYS_GetPropertyValue(hProps,pDeviceProp, 0, &PropVar)){
      AsciiPrint(PRINTSIG"Could not find the Device Property: %s",pDeviceProp);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Property Name[2/5]: \tOK");
   
   if ( NULL == pType) 
   {
		AsciiPrint(PRINTSIG"NULL device type",NULL);
		return TF_RESULT_CODE_FAILURE;
   }
   
   // Type is zero .. Display
   if (!strncmp( "0",pType,1))
   {
      AsciiPrint(PRINTSIG"The Property Type Received :%s",ConvertDeviceType(PropVar.dwType));
      Display(PropVar, pszArg);
      return TF_RESULT_CODE_SUCCESS;
   }
   
   if(NULL==ConvertDeviceType(PropVar.dwType))
   {
      AsciiPrint(PRINTSIG"The Property Type %d is not supported for testing", PropVar.dwType);
      return TF_RESULT_CODE_FAILURE;
   }
   if (NULL ==pType || strncmp( ConvertDeviceType(PropVar.dwType),pType,(strlen(ConvertDeviceType(PropVar.dwType))>strlen(pType))?strlen(ConvertDeviceType(PropVar.dwType)):strlen(pType)))
   {
      AsciiPrint(PRINTSIG"The Property Type does not match:\n\tThe Property Type Received :%s \n\tThe Property Type Expected:%s",ConvertDeviceType(PropVar.dwType),pType);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Property Type[3/5]: \tOK");
   
   // Property length
   dwlen =strtol(arg_len, &pEnd, 10);

   if (dwlen!= PropVar.dwLen)
   {
      AsciiPrint(PRINTSIG"The Property Length does not match:\n\tThe Property Length Received :%d \n\tThe Property Length Expected:%d",PropVar.dwLen,dwlen);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Property Length[4/5]: \tOK");

   // Property Value
   if (TF_RESULT_CODE_SUCCESS !=DataComp(PropVar, pszArg))
   {
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Property Values[5/5]: \tOK");
   AsciiPrint(PRINTSIG"\n=================");
   return TF_RESULT_CODE_SUCCESS;
}
/* ============================================================================
**  Function : DevicePropertyTesting
** ============================================================================
*/
/*!
    @brief
    A Device_Property_testing  to test the DALSYS_GetDALPropertyHandleStr API.
    
    @details
    DALSYS_GetDALPropertyHandleStr is a String version of 
    DALSYS_GetDALPropertyHandle.  Just like DALSYS_GetDALPropertyHandle
    it returns DAL_SUCCESS if it could find the String ID or DAL_ERROR if not found. 
    This function will select correct API based on Device type and compares the valus
    Error occurred : Log file will describe the failure reason
    If Length set to 0, it will print the data receive from device and would not confirm 
    the value and length . 
    @param[in] dwArg   5
    @param[in] pszArg  
        pszArg[0]   Driver ID           String / UINT32 Driver ID
        pszArg[1]   Property Name       Property Name   
        pszArg[2]   Type                Property Type
                                            DALPROP_ATTR_TYPE_UINT32_SEQ_PTR
                                            DALPROP_ATTR_TYPE_BYTE_SEQ_PTR
                                            DALPROP_ATTR_TYPE_STRING_PTR
                                            DALPROP_ATTR_TYPE_UINT32
        pszArg[3]   Length              Property Value Length 
        pszArg[4]   Value               Property Value  
                                  
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns 
                                            returns TF_RESULT_CODE_SUCCESS on Success
                                            returns TF_RESULT_CODE_FAILURE on Failure
    @sa None
*/

UINT32 DevicePropertyTesting(UINT32 dwArg, CHAR8* pszArg[])
{
   return DevicePropertyTesting_help(dwArg,pszArg);
}

/* ============================================================================
**  Function : DevicePropertyDisplay
** ============================================================================
*/
/*!
    @brief
    A DevicePropertyDisplay to display the DALSYS_GetDALPropertyHandleStr API 
    return values.
    
    @details
    DALSYS_GetDALPropertyHandleStr is a String version of 
    DALSYS_GetDALPropertyHandle.  
    @param[in] dwArg   2
    @param[in] pszArg  
        pszArg[0]   Driver ID           String / UINT32 Driver ID
        pszArg[1]   Property Name       Property Name   
                        
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns 
                                            returns TF_RESULT_CODE_SUCCESS on Success
                                            returns TF_RESULT_CODE_FAILURE on Failure
    @sa None
*/
UINT32 DevicePropertyDisplay(UINT32 dwArg, CHAR8* pszArg[])
{
   CHAR8 *pArg[5];
   pArg[0] = pszArg[0];
   pArg[1] = pszArg[1];
   pArg[2] = "0";
   pArg[3] = NULL;
   pArg[4] = NULL;
   return DevicePropertyTesting_help(5,pArg);
}

// static UINT32 DeviceAttachTesting_help(UINT32 dwArg, CHAR8* pszArg[]){
   
   // DalDeviceHandle  * phSTRING;
   // CHAR8 * pEnd;
   // UINT32 wdCounter = 0;
   // UINT32 dwTry =0;
   // UINT32 dwHandle = 0;
   // CHAR8 *pTry = pszArg[0];
   // CHAR8 *pDeviceName = pszArg[1];
   // // CHAR8 *pAttachType = pszArg[2];
   // DALResult eDalResult = DAL_SUCCESS;
   
   // if (dwArg != (sizeof(ConfigParamAttach) / sizeof(TF_ParamDescr))){
      // DalTF_Msg(PRINTSIG"Number of input parameters incorrect: exp %d got %d", sizeof(ConfigParamAttach) / sizeof(TF_ParamDescr), dwArg);
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   // dwTry = strtol(pTry, &pEnd, 10);
   // if (( NULL !=pDeviceName && '0'==pDeviceName[0] && 'x'==pDeviceName[1]) )
   // {
      // AsciiPrint(PRINTSIG"UINT32 Devices are not supported.");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if ( NULL == pDeviceName ) 
   // {
      // AsciiPrint(PRINTSIG"No device Id");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
      // // The keyboard "NULL" is replaced by NULL
   // if (strncmp(pDeviceName,"NULL",4)==0)
   // {
       // eDalResult = DAL_StringDeviceAttach(NULL, &phSTRING);
       
       // if (DAL_SUCCESS !=eDalResult)
       // {
         // AsciiPrint(PRINTSIG"Could not have NULL pointer");
         // return TF_RESULT_CODE_FAILURE;
       // }
   // }
   
   // AsciiPrint(PRINTSIG"****String Device[%s]: ****\nNumber of Handles: %d\nNumber of Tries : %d",pszArg[1],dwHandle,dwTry);   
   // for (wdCounter=0;wdCounter<dwTry;wdCounter++)
   // {
         // if (DAL_SUCCESS != DAL_StringDeviceAttach(pDeviceName, &phSTRING))
         // {   
            // AsciiPrint(PRINTSIG"Error: Attaching to device: [%s] ",pszArg[1]);
            // return TF_RESULT_CODE_FAILURE;
         // }
   
      // if (DAL_SUCCESS !=DAL_DeviceDetach(phSTRING))
      // {
         // AsciiPrint(PRINTSIG"Error: Detach:  [%s] ",pszArg[1]);
         // return TF_RESULT_CODE_FAILURE;
      // }
   // }
   // return TF_RESULT_CODE_SUCCESS;
// }

// /* ============================================================================
// **  Function : DeviceAttachTesting
// ** ============================================================================
// */
// /*!
    // @brief
    // DeviceAttachTesting to test DAL_StringDeviceAttach API.
    
    // @details
    // DAL_StringDeviceAttach is a String version of 
    // DAL_DeviceAttach.  Just like DAL_DeviceAttach
    // it returns DAL_SUCCESS if it could find the String ID or DAL_ERROR if not found. 
    // This function Attaches to a device multiple times with multiple handles.  

    
    // @param[in] dwArg   4
    // @param[in] pszArg  
        // pszArg[0]   Number of Handle       
        // pszArg[1]   String Device IDs
        // pszArg[2]   Attach function 
    // @par Dependencies
    // None
    
    // @par Side Effects
    // None
    
    // @retval returns 
                                            // returns TF_RESULT_CODE_SUCCESS on Success
                                            // returns TF_RESULT_CODE_FAILURE on Failure
                                            
    
    // @sa None
// */

// static UINT32 DeviceAttachTesting(UINT32 dwArg, CHAR8* pszArg[]){
   // return DeviceAttachTesting_help(dwArg,pszArg);
// }

/*!
    @brief
    Negative testing of DevicePropertyTesting
  */  
UINT32 DevicePropertyNegativeTesting(UINT32 dwArg, CHAR8* pszArg[]){
    if(TF_RESULT_CODE_SUCCESS==DevicePropertyTesting_help(dwArg,pszArg))
        return TF_RESULT_CODE_FAILURE;
    return TF_RESULT_CODE_SUCCESS;
}
/*!
    @brief
    Negative testing of DeviceAttachTesting
  */ 
// static UINT32 DeviceAttachNegativeTesting(UINT32 dwArg, CHAR8* pszArg[]){

   // if(TF_RESULT_CODE_SUCCESS==DeviceAttachTesting_help(dwArg,pszArg))
      // return TF_RESULT_CODE_FAILURE;
   // return TF_RESULT_CODE_SUCCESS;

// }

/*!
    @brief
    Measuring the Device DALSYS_GetDALPropertyHandleStr and 
    DALSYS_GetPropertyValue APIs performance
    @param[in] dwArg   4
    @param[in] pszArg  
        pszArg[0]   Number of Tries       
        pszArg[1]   UINT32 or String Device IDs
        pszArg[2]   Device Property 
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns 
                                            returns TF_RESULT_CODE_SUCCESS on Success
                                            returns TF_RESULT_CODE_FAILURE on Failure  
    */ 
// static UINT32 DevicePropertyPerformanceTesting(UINT32 dwArg, CHAR8* pszArg[]){

   
   // CHAR8 * pEnd = NULL;
   // UINT32 dwTry, dwCounter;
   // CHAR8 * pDevice = pszArg[1];
   // CHAR8 * pProp = pszArg[2];
   // UINT32 dwDevice = 0;
   // DalDeviceHandle *hTimetick = NULL;
   // DalTimetickTime32Type firstCountTick = 0;
   // DALSYSPropertyVar PropVar;
   // DALResult eDalResult = DAL_SUCCESS;
   // DalTimetickTime32Type diffCount = 0;
   // DALSYS_PROPERTY_HANDLE_DECLARE(hProps);
   // if (dwArg != (sizeof(ConfigParamPropPre) / sizeof(TF_ParamDescr))){
      // DalTF_Msg(PRINTSIG"Number of input parameters incorrect: exp %d got %d", sizeof(ConfigParamPropPre) / sizeof(TF_ParamDescr), dwArg);
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   // dwTry = strtol(pszArg[0], &pEnd, 10);
   // if (dwTry<=0)
   // {
      // DalTF_Msg(PRINTSIG"Number of try parameters needs to be greater than zero");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if (dwTry > 5000)
   // {
      // DalTF_Msg(PRINTSIG"Number of try param is too large");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if ('0'==pDevice[0] && 'x'==pDevice[1])
   // {
      // dwDevice  = strtol(pDevice, &pEnd, 10);
   // }
   
   // if(DAL_SUCCESS == DalTimetick_Attach("SystemTimer", &hTimetick))
   // {
      // if (0==dwDevice && DAL_SUCCESS ==DalTimetick_Get(hTimetick, &firstCountTick))
      // {
         // for (dwCounter=0;dwCounter<dwTry;dwCounter++)
            // eDalResult = DALSYS_GetDALPropertyHandleStr(pDevice, hProps);
         
      // }else if (DAL_SUCCESS ==DalTimetick_Get(hTimetick, &firstCountTick))
      // {
         // for (dwCounter=0;dwCounter<dwTry;dwCounter++)
            // eDalResult = DALSYS_GetDALPropertyHandle(dwDevice, hProps);
      // }else
      // {
         // AsciiPrint(PRINTSIG"DalTimetick_Get failed");
         // return TF_RESULT_CODE_FAILURE;
      // }
      
      // if ( DAL_SUCCESS == DalTimetick_GetElapsed(hTimetick, firstCountTick, T_TICK, &diffCount))
      // {
            // AsciiPrint(PRINTSIG"It took %u TICKES to %s for %d tries.\nAvg =%f",diffCount,(eDalResult!=DAL_SUCCESS)?"fail getting the handle":"successfully get the handle",dwTry,((float) diffCount) / ((float) dwTry));
      // }
      
      // if (DAL_SUCCESS !=eDalResult)
      // {
         // return TF_RESULT_CODE_SUCCESS;
      // }
      
      // if(DAL_SUCCESS == DalTimetick_Get(hTimetick,&firstCountTick))
      // {
         // for (dwCounter=0;dwCounter<dwTry;dwCounter++)
            // eDalResult = DALSYS_GetPropertyValue(hProps,pProp, 0, &PropVar);
         // if ( DAL_SUCCESS == DalTimetick_GetElapsed(hTimetick, firstCountTick, T_TICK, &diffCount))
         // {
            // AsciiPrint(PRINTSIG"It took %u TICKES to %s for %d tries.\nAvg= %f",diffCount,(eDalResult!=DAL_SUCCESS)?"fail getting the property":"successfully get the property",dwTry,((float) diffCount) / ((float) dwTry) );
            // return TF_RESULT_CODE_SUCCESS;
         // }else
         // {
            // AsciiPrint(PRINTSIG"DalTimetick_Get failed");
            // return TF_RESULT_CODE_FAILURE;
         // }
      // }else
      // {
         // AsciiPrint(PRINTSIG"DalTimetick_Get failed");
         // return TF_RESULT_CODE_FAILURE;
      // }
   // }
   // AsciiPrint(PRINTSIG"Could not get Clock Handle");
   // return TF_RESULT_CODE_FAILURE;
// }

/*!
    @brief
    Measuring the Device DAL_StringDeviceAttach API performance
    @param[in] dwArg   4
    @param[in] pszArg  
        pszArg[0]   Number of Tries       
        pszArg[1]   String Device IDs
    @par Dependencies
    None
    
    @par Side Effects
    None
    
    @retval returns 
                                            returns TF_RESULT_CODE_SUCCESS on Success
                                            returns TF_RESULT_CODE_FAILURE on Failure
    */
// static UINT32 DeviceAttachPerformanceTesting(UINT32 dwArg, CHAR8* pszArg[]){
   // DalDeviceHandle  * phSTRING;
   // UINT32 dwCounter = 0;
   // CHAR8 *pEnd;
   // UINT32 dwTry =0;
   // DalTimetickTime32Type diffCount = 0;
   // DalDeviceHandle *hTimetick = NULL;
   // DalTimetickTime32Type firstCountTick = 0;
   // CHAR8 * pDevice = pszArg[1];
   // if (dwArg != (sizeof(ConfigParamAttachPre) / sizeof(TF_ParamDescr))){
      // DalTF_Msg(PRINTSIG"Number of input parameters incorrect: exp %d got %d", sizeof(ConfigParamAttachPre) / sizeof(TF_ParamDescr), dwArg);
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   // dwTry = strtol(pszArg[0], &pEnd, 10);
   // if (dwTry<=0)
   // {
      // DalTF_Msg(PRINTSIG"Number of try parameters needs to be greater than zero");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if (dwTry > 5000)
   // {
      // DalTF_Msg(PRINTSIG"Number of try param is too large");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if ('0'==pDevice[0] && 'x'==pDevice[1])
   // {
      // DalTF_Msg(PRINTSIG"UINT32 Devices are not supported");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   // // Know the reasult before hand
   // if (DAL_SUCCESS != DAL_StringDeviceAttach(pDevice, &phSTRING))
   // {
      // DalTF_Msg(PRINTSIG"The Device does not exist");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   // if (DAL_SUCCESS != DAL_DeviceDetach(phSTRING))
   // {
      // DalTF_Msg(PRINTSIG"The Device Detach failed");
      // return TF_RESULT_CODE_BAD_PARAM;
   // }
   
   // if(DAL_SUCCESS == DalTimetick_Attach("SystemTimer", &hTimetick))
   // {
      // if (DAL_SUCCESS ==DalTimetick_Get(hTimetick, &firstCountTick))
      // {
         // for (dwCounter=0;dwCounter<dwTry;dwCounter++)
         // {
            // DAL_StringDeviceAttach(pDevice, &phSTRING);
            // DAL_DeviceDetach(phSTRING);
         // }
         // if (DAL_SUCCESS != DalTimetick_GetElapsed(hTimetick, firstCountTick, T_TICK, &diffCount))
         // {
            // AsciiPrint(PRINTSIG"Could not get Clock Elapsed");
            // return TF_RESULT_CODE_FAILURE;
         // }else
         // {
            // AsciiPrint(PRINTSIG"It took %u TICKES to successfully Attach and Detach for %d tries.\nAvg = %f",diffCount,dwTry,((float) diffCount) / ((float) dwTry));
            // return TF_RESULT_CODE_SUCCESS;
         // }
      // }else{
         // AsciiPrint(PRINTSIG"Could not get Clock Tick");
         // return TF_RESULT_CODE_FAILURE;
      // }
   // }
   // AsciiPrint(PRINTSIG"Could not get Clock Handle");
   // return TF_RESULT_CODE_FAILURE;
// }

CONST TF_HelpDescr TestDeviceConfighelp_0 =
{
    "DAL Property",
    sizeof(ConfigParamDisplay) / sizeof(TF_ParamDescr),
    ConfigParamDisplay
};

CONST TF_HelpDescr TestDeviceConfighelp_1 =
{
    "DAL Property",
    sizeof(ConfigParamTest) / sizeof(TF_ParamDescr),
    ConfigParamTest
};

CONST TF_TestFunction_Descr Descr_0 =
{
   PRINTSIG"StringUINT32DevicePropertyDisplay",
   DevicePropertyDisplay,
   &TestDeviceConfighelp_0,
   &context1
};
CONST TF_TestFunction_Descr Descr_1 =
{
   PRINTSIG"StringUINT32DevicePropTest",
   DevicePropertyTesting,
   &TestDeviceConfighelp_1,
   &context1
};

CONST TF_TestFunction_Descr Descr_3 =
{
   PRINTSIG"StringUINT32DevicePropNegTest",
   DevicePropertyNegativeTesting,
   &TestDeviceConfighelp_1,
   &context1
};



const TF_TestFunction_Descr* kernel_daltf_devcfg_tests[] = {
  &Descr_0,
  &Descr_1,
  &Descr_3,
};

/*===========================================================================
  FUNCTION:  kernel_daltf_devcfg_init_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_daltf_devcfg_init_all(VOID)
{
  DALResult dalResult;

  UINT16 TF_NumberTests =
    (sizeof(kernel_daltf_devcfg_tests) / sizeof(kernel_daltf_devcfg_tests[0]));

  dalResult = tests_daltf_add_tests(kernel_daltf_devcfg_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  kernel_daltf_devcfg_deinit_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_daltf_devcfg_deinit_all(VOID)
{
  DALResult dalResult;
  UINT16 TF_NumberTests =
    (sizeof(kernel_daltf_devcfg_tests) / sizeof(kernel_daltf_devcfg_tests[0]));

  dalResult = tests_daltf_remove_tests(kernel_daltf_devcfg_tests, TF_NumberTests);
  return dalResult;
}