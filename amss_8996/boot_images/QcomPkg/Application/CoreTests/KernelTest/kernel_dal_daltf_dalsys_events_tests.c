/*============================================================================
@file         kernel_dal_daltf_dalsys_events_tests.c

@brief        DALTF based Dalsys events API L1,L2,L3 tests

GENERAL DESCRIPTION
  This file contains L1,L2 and L3 tests for DALSys events related APIs

Copyright (c) 2012 Qualcomm Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
============================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/KernelTest/kernel_dal_daltf_dalsys_events_tests.c#1 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
04/12/12    vn     Intial Version
04/20/12    vn	   Add Callback L2 test and code cleanup
04/27/12    vn     Add EventsL1 test
05/10/12    vn	   Add MultEvent test
05/14/12    vn	   Write cleanup functions and other changes for code review
==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR CHIPINFO DRIVER TESTS

==========================================================================*/
#include "kernel_dal_daltf_dalsys_events_tests.h"
#include "DALSys.h"
#include "DDITimer.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      CONSTants and Macros
==========================================================================*/
#define TEST_DALSYS_NUM_OF_CB_EVENTS 2
#define TEST_DALSYS_NUM_OF_WL_EVENTS 5
#define TEST_DALSYS_PRIO_OF_WL 0
#define TEST_DALSYS_NUM_OF_EVENT_OBJECTS 5
#define TEST_DALSYS_WL_STACK_SIZE 0x00001000

CONST TF_ParamDescr DalsysEventsTestParam[] =
{
    {TF_PARAM_UINT32, "Iterations",		"Iterations (Optional)\n"},
};

CONST TF_ParamDescr DalsysEventsL1TestParam[] =
{
    {TF_PARAM_UINT32, "Iterations",								"Iterations (Optional)\n"},
	{TF_PARAM_UINT32, "Options for Register WorkLoop API",		"1 - DALSYS_RegisterWorkLoop\n"},
	{TF_PARAM_UINT32, "Allocate Obj Static or Dynamic",			"0 - Dynamic Allocate\n"},															
};

CONST TF_ParamDescr DalsysMultWaitTestParam[] =
{
    {TF_PARAM_UINT32, "Timeout value",		"Timeout in microsec Ex: 100,200\n"},
};

CONST TF_ParamDescr DalsysEventNegTestParam[] =
{
    {TF_PARAM_UINT32, "Override",		"Optional\n"},
};

/* ============================================================================
                         Global Data for TestDalsysWLEvents1 Test
** ===========================================================================*/


static UINT32 uValue;				


/*
===============================================================================
**  Function : TestDalsysWLEventsL1
** ============================================================================
*/
/*!
    @brief
    Sanity check for workloop events and workloops
    @details
	Create an Workloop and an workloop event. 
	Add the event to the workloop. Trigger the workloop event. 
	Destroy the workloop objects
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_EventCreate
	DALSYS_RegisterWorkLoopEx
	DALSYS_EventCtrl
	DALSYS_DestroyObject
	
    @par Side Effects

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

/*  WL Fucntion that is triggered, increments the	
	count and triggers the waiting event hTEventL1  */
DALResult workLoopFcn1_events(DALSYSEventHandle hEvent, VOID * pCtxt)
{
	DALSYSEventHandle hEventToTrig = (UINT32 *) pCtxt;
	uValue++;
	
	AsciiPrint(PRINTSIG"In workloop 1\n");
	
	if (DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER) != DAL_SUCCESS) {
		AsciiPrint(PRINTSIG"In Workloop1 Trigger FAILED\n" );
		return DAL_ERROR;
	}
	
	return DAL_WORK_LOOP_EXIT;    
}

// Cleanup function for TestDalsysWLEventsL1 and TestDalsysCBEventsL2
UINT32 DALTEST_CleanUpEvents(DALSYSEventHandle hEvent1, DALSYSEventHandle hEvent2, DALSYSWorkLoopHandle hWLHandle) {
	 UINT32 uRet, uIs_Fail=0;
	 if (hEvent1) {
		 uRet = DALSYS_DestroyObject(hEvent1);
		 if (DAL_SUCCESS != uRet) {
			 AsciiPrint(PRINTSIG"Destroy failed hEvent1\n");
			 uIs_Fail = 1;
		 }
	 }
	 if (hEvent2) {
		 uRet = DALSYS_DestroyObject(hEvent2);
		 if (DAL_SUCCESS != uRet) {
			 AsciiPrint(PRINTSIG"Destroy failed hEvent1\n");
			 uIs_Fail = 1;
		 }
	 }
	 if (hWLHandle) {
		 uRet = DALSYS_DestroyObject(hWLHandle);
		 if (DAL_SUCCESS != uRet) {
			 AsciiPrint(PRINTSIG"Destroy failed WLHandle\n");
			 uIs_Fail = 1;
		 }
	 }	
	 if (! uIs_Fail) 
		 return TF_RESULT_CODE_SUCCESS;
	 else 
		 return TF_RESULT_CODE_FAILURE;
}

UINT32 TestDalsysWLEventsL1(UINT32 dwArg, CHAR8* pszArg[])
{	
	//UINT32 opt;			// Variable for number of iterations
	static DALSYSEventHandle hTEventL1 = NULL;		// Handle for the waiting event in WL events L1 Test
	static DALSYSWorkLoopHandle hWorkLoop = NULL;		// Handle for Workloop
	static DALSYSEventHandle hWLEvent = NULL;		// Handle for WorkLoop Event
    //CHAR8 *endptr;
	// CHAR8 *WLName = "Test_Dalsys_WLEvents_Test";
	uValue = 0;

	if ( dwArg != (sizeof(DalsysEventsL1TestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysEventsL1TestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }
	
	//opt = (UINT32)strtol(pszArg[1], &endptr, 10);
	// if (*endptr != '\0') {
		// AsciiPrint(PRINTSIG"Conversion for param 2 in strtol failed %s", *endptr);
		// return TF_RESULT_CODE_BAD_PARAM;
	// }
		
	// Create an Waiting Event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
										  &hTEventL1,
										  NULL)) {
		AsciiPrint(PRINTSIG"Dalsys Event create failed\n");
		return TF_RESULT_CODE_FAILURE;
	}

	// Check for NULL handle
	if (NULL == hTEventL1) {
		return TF_RESULT_CODE_FAILURE;
	}
	
	// Create an WL Event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT, // Attribute
										  &hWLEvent,	// Event handle
										  NULL)) {	
		AsciiPrint(PRINTSIG"Dalsys WorkLoop Event create failed\n");
		if ((TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1, NULL, NULL)) || (NULL == hWLEvent)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}

	/* Register a WorkLoop, 0 - Priority, 2 - max events, statically allocate WL Obj*/
	if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(TEST_DALSYS_PRIO_OF_WL,
											     TEST_DALSYS_NUM_OF_WL_EVENTS,
											     &hWorkLoop,
											     NULL)) {
			AsciiPrint(PRINTSIG"DALSYS_RegisterWorkloop failed\n");
			// destroy objects
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	// Add the WL event to the WorkLoop
	if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(hWorkLoop,		// Handle to the registered work loop
												 workLoopFcn1_events,		// Function to be invoked
											     (VOID *) hTEventL1,	// Argument to be passed to WL function
												 hWLEvent,		// Handle to the event to add
											     NULL)) {		// Handle to the synchronization object
		AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop failed\n");
		// destroy objects
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	 // // Acquire ownership of waiting event 
	if (DAL_SUCCESS != DALSYS_EventCtrl(hTEventL1, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP)) {
		  AsciiPrint(PRINTSIG"DALSYS_EVENT_ACCQUIRE_OWNERSHIP FAILED\n" );
		  if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			  return TF_RESULT_CODE_FAILURE;
		  }
	 }
	
	 // Reset the Waiting Event
	 if (DAL_SUCCESS != DALSYS_EventCtrl(hTEventL1, DALSYS_EVENT_CTRL_RESET)) {
		 AsciiPrint(PRINTSIG"DALSYS_EVENT_CTRL_RESET FAILED\n" );
		 if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			 return TF_RESULT_CODE_FAILURE;
		 }
	 }
	
	// Trigger the WL Event
	if (DAL_SUCCESS != DALSYS_EventCtrl(hWLEvent, DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"Trigger WL Event failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	// Wait for completion of WL
	if (DAL_SUCCESS != DALSYS_EventWait(hTEventL1)) {
		AsciiPrint(PRINTSIG"TEventWait Failed");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}

	if (!uValue) {
		AsciiPrint(PRINTSIG"WL did not update the variable: uValue %d\n", uValue);
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}

	DALSYS_DestroyObject(hTEventL1);
	DALSYS_DestroyObject(hWLEvent);
	DALSYS_DestroyObject(hWorkLoop);
	// Cleanup the Events and WorkLoop
	// if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventL1,hWLEvent,hWorkLoop)) {
		// return TF_RESULT_CODE_FAILURE;
	// }
	AsciiPrint(PRINTSIG"End of TestDalsysWLEventsL1 Test\n");
	return TF_RESULT_CODE_SUCCESS;
}

// TestDalsysWLEventsL1's Help Data
CONST TF_HelpDescr TestDalsysWLEventsL1Help =
{
    "TestDalsysWLEventsL1",
    sizeof(DalsysEventsL1TestParam) / sizeof(TF_ParamDescr),
    DalsysEventsL1TestParam
};

CONST TF_TestFunction_Descr TestDalsysWLEventsL1Test =
{
   PRINTSIG"TestDalsysWLEventsL1",
   TestDalsysWLEventsL1,
   &TestDalsysWLEventsL1Help,
   &context1
};

/* ************* End of TestDalsysWLEventsL1**************** */


// WorkLoop function called inside the callback function
DALResult workLoopFcnCB(DALSYSEventHandle hEvent, VOID * pCtxt)
{
	DALSYSEventHandle hEventToTrig = (UINT32 *) pCtxt;
	
	AsciiPrint(PRINTSIG"In workloop 1\n");
	
	if (DAL_SUCCESS != DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"In Workloop1 Trigger FAILED\n" );
		return DAL_ERROR;
	}
	
	return DAL_WORK_LOOP_EXIT;    
}

// Callback function to be called when Event Triggered
VOID CallbackFunc1(VOID *ctx, UINT32 dwparam, VOID *buf, UINT32 bufsize) 
{
	UINT32 * pctx = (UINT32 *) ctx;
	DALSYSEventHandle *cbPayLoad = (DALSYSEventHandle *)buf;

	AsciiPrint(PRINTSIG"Inside Callback Function 1\n" );
	(*pctx)++;

	if (cbPayLoad) {
		if (DAL_SUCCESS != DALSYS_EventCtrl (cbPayLoad, DALSYS_EVENT_CTRL_TRIGGER))
			AsciiPrint(PRINTSIG"Trigger CB event inside CB failed\n" );
	}
		
}

// Triggered inside CallbackFunc1 
UINT32 CallbackFunc2(VOID *ctx, UINT32 dwparam, VOID *buf, UINT32 bufsize) 
{
	// CHAR8 *WLName = "Test_Dalsys_CBEventsL2_Test";
	DALSYSWorkLoopHandle hWorkLoop = NULL;		// Handle for Workloop
	DALSYSEventHandle hWLEvent 	   = NULL;		// Handle for WorkLoop Event
	DALSYS_WORKLOOP_OBJECT(WLObj);		// Create a Static WL Object
	DALSYSWorkLoopObj *pWLObj      = &WLObj;
	DALSYSEventHandle hTEventCB    = NULL;		// Handle for the waiting event in CallBack Events test

	AsciiPrint(PRINTSIG"Inside Callback 2" );

	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
						&hTEventCB,
						NULL)) {
		AsciiPrint(PRINTSIG"Dalsys Event create failed\n");
		return TF_RESULT_CODE_FAILURE;
	}
	 
	if (NULL == hTEventCB) {
		AsciiPrint(PRINTSIG"NULL event handle\n");
		return TF_RESULT_CODE_FAILURE;
	}

	// Create an WL Event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT, // Attribute
										  &hWLEvent,	// Event handle
										  NULL)) {	// Dynamic allocation 
		AsciiPrint(PRINTSIG"Dalsys WorkLoop Event create failed\n");
		if ((TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB, NULL, NULL)) || (NULL == hWLEvent)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	/* Register a WorkLoop, 0 - Priority, 2 - max events, statically allocate WL Obj*/
	if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(0, 2, &hWorkLoop, pWLObj)) {
		AsciiPrint(PRINTSIG"DALSYS_RegisterWorkLoop failed\n");
		// destroy objects
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB,hWLEvent, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	// Add the WL event to the WorkLoop
	if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(hWorkLoop,		// Handle to the registered work loop
												 workLoopFcnCB,		// Function to be invoked
												 (VOID *) hTEventCB,	// Argument to be passed to WL function
												 hWLEvent,		// Handle to the event to add
												 NULL)) {		// Handle to the synchronization object
		AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop failed\n");
		// destroy objects
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}

	// Trigger the WL Event
	if (DAL_SUCCESS != DALSYS_EventCtrl(hWLEvent,
									    DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"Trigger WL Event failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	// Wait for completion of WL
	if (DAL_SUCCESS != DALSYS_EventWait(hTEventCB)) {
		AsciiPrint(PRINTSIG"TEventWait Failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB,hWLEvent,hWorkLoop)) {
			return TF_RESULT_CODE_FAILURE;
		}
	}

	if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hTEventCB,hWLEvent,hWorkLoop)) {
		return TF_RESULT_CODE_FAILURE;
	}

	AsciiPrint(PRINTSIG"End of Callback 2\n");
	return TF_RESULT_CODE_SUCCESS;
}

/*
===============================================================================
**  Function : TestDalsysCBEventsL2
** ============================================================================
*/
/*!
    @brief
    Callback functions API test
    @details
	Create a callback function and trigger it. 
	Verify callback function executes.
	Nested callback.
	Trigger Workloop inside a Callback.
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_EventCreate
	DALSYS_SetupCallbackEvent
	DALSYS_EventCtrl
	DALSYS_DestroyObject
	
    @par Side Effects
	None.

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysCBEventsL2(UINT32 dwArg, CHAR8* pszArg[])
{	
	UINT32 uIter,uIndex;				// Variable for number of iterations
	volatile UINT32 ctx;			// Variable to pass to CB func
	DALSYSEventHandle hCBEvent[2]={NULL};	// Handler for CB Event
	CHAR8 * endptr;
	
	uIter = strtol(pszArg[0], &endptr, 10);
	if(*endptr != '\0') {
		AsciiPrint(PRINTSIG"Param Conversion not successful %c\n", *endptr);
		return(TF_RESULT_CODE_BAD_PARAM);
	}
	if(uIter == 0)
		uIter++;					// Run the test atleast once
	
	for (uIndex=0; uIndex < uIter; uIndex++) {
		ctx = 0;
		// Create a CB Event
		if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
										      &hCBEvent[0],
											  NULL)) {
			AsciiPrint(PRINTSIG"Create Callback 0 event failed\n" );
        		return TF_RESULT_CODE_FAILURE;
		}
		if (NULL == hCBEvent[0]) {
			AsciiPrint(PRINTSIG"Create Callback 0 NULL callback\n" );
        		return TF_RESULT_CODE_FAILURE;
		}

		if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
											  &hCBEvent[1],
											  NULL)) {
			AsciiPrint(PRINTSIG"Create Callback 1 event failed\n" );
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0], NULL, NULL)) {
				return TF_RESULT_CODE_FAILURE;
			}
		}
		if (NULL == hCBEvent[1]) {
			AsciiPrint(PRINTSIG"Create Callback 1 NULL callback\n" );
        		return TF_RESULT_CODE_FAILURE;
		}
		
		// Setup callback function		
		if (DAL_SUCCESS != DALSYS_SetupCallbackEvent (hCBEvent[0],		// CB event
				       	(DALSYSCallbackFunc) CallbackFunc1,				// CB Func
				       	(DALSYSCallbackFuncCtx) &ctx))					// ctx to pass to CB Func
	       	{
			AsciiPrint(PRINTSIG"setup callback event failed\n" );
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0],hCBEvent[1], NULL)) {
				return TF_RESULT_CODE_FAILURE;
			}
		}

		// Setup callback function				
		if (DAL_SUCCESS != DALSYS_SetupCallbackEvent (hCBEvent[1],							// CB event
													 (DALSYSCallbackFunc) CallbackFunc2,	// CB Func
													 (DALSYSCallbackFuncCtx) &ctx))			// ctx to pass to CB Func
	       	{
			AsciiPrint(PRINTSIG"setup callback event failed\n" );
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0],hCBEvent[1], NULL)) {
				return TF_RESULT_CODE_FAILURE;
			}
		}

		// Trigger the CB Event
		if (DAL_SUCCESS != DALSYS_EventCtrl (hCBEvent[0],
											 DALSYS_EVENT_CTRL_TRIGGER)) {
			AsciiPrint(PRINTSIG"callback event trigger failed\n" );
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0],hCBEvent[1], NULL)) {
				return TF_RESULT_CODE_FAILURE;
			}
		}

		// Validate if CB Function called
		if (1 != ctx) {
			AsciiPrint(PRINTSIG"Callback function not invoked properly\n" );
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0],hCBEvent[1], NULL)) {
				return TF_RESULT_CODE_FAILURE;
			}
		}

	    	/* trigger it again this time with payload */
		if (DALSYS_EventCtrlEx(hCBEvent[0],
							   DALSYS_EVENT_CTRL_TRIGGER,
							   1,
							   (VOID *)hCBEvent[1],
							   sizeof(hCBEvent[1])) != DAL_SUCCESS) {
			AsciiPrint(PRINTSIG"Callback function not invoked properly 2nd time\n" );
        		return TF_RESULT_CODE_FAILURE;
    		}	
	}
	if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEvents(hCBEvent[0],hCBEvent[1], NULL)) {
		return TF_RESULT_CODE_FAILURE;
	}
	AsciiPrint(PRINTSIG"End of TestDalsysCBEventsL1");
	return TF_RESULT_CODE_SUCCESS;
}



// TestDalsysWLEventsL1's Help Data
CONST TF_HelpDescr TestDalsysCBEventsL2Help =
{
    "TestDalsysCBEventsL2",
    sizeof(DalsysEventsTestParam) / sizeof(TF_ParamDescr),
    DalsysEventsTestParam
};

CONST TF_TestFunction_Descr TestDalsysCBEventsL2Test =
{
   PRINTSIG"TestDalsysCBEventsL2",
   TestDalsysCBEventsL2,
   &TestDalsysCBEventsL2Help,
   &context1
};

/************** End of TestDalsysCBEventsL2 ******************/


// TestDalsysEventsL1 workloop callback function
// Workloop function to be called when hWLEvent is triggered 
DALResult workLoopFcn(DALSYSEventHandle hEvent, VOID * pCtxt)
{
	DALSYSEventHandle hEventToTrig = (UINT32 *) pCtxt;
	
	AsciiPrint(PRINTSIG"In workloop 1\n");

	// Try to acquire ownership of an CLIENT_DEFAULT_EVENT
	//if ( DAL_SUCCESS != DALSYS_EventCtrl (hEventToTrig, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP)) {
	//	AsciiPrint(PRINTSIG"In Workloop1 Trigger FAILED" );
	//	return DAL_ERROR;
	//}
	
	// Trigger the waiting event
	if (DAL_SUCCESS != DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"In Workloop1 Trigger FAILED\n" );
		return DAL_ERROR;
	}
	
	return DAL_WORK_LOOP_EXIT;    
}

// TestDalsysEventsL1 workloop callback function
// Workloop function to be called when hWLEvent is triggered 
DALResult workLoopFcn3(DALSYSEventHandle hEvent, VOID * pCtxt)
{
	DALSYSEventHandle hEventToTrig = (UINT32 *) pCtxt;
	
	AsciiPrint(PRINTSIG"In workloop 3\n");

	// Trigger the waiting event
	if (DAL_SUCCESS != DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"In Workloop1 Trigger FAILED\n" );
		return DAL_ERROR;
	}
	
	return DAL_WORK_LOOP_EXIT;    
}

// Cleanup function for TestDalsysEventsL1 test
// This routine is specific to DalsysEventsL1 test cleanup
// In Arg: Eventhandle array, Number of elements in the array, WorkLoop object 1(optional), WOrkloop Object 2(optional)
// Out: TF_RESULT_CODE_FAILURE/TF_RESULT_CODE_SUCCES
 
UINT32 DALTEST_CleanUpEventsL1(DALSYSEventHandle *hEventhandle, int iNum,
				DALSYSWorkLoopHandle hWorkLoop1,
			       	DALSYSWorkLoopHandle hWorkLoop2) {
	int iLoop = 0;
	DALResult  dalResult  = DAL_SUCCESS;
	UINT32 uIs_Fail = 0;

	if (NULL == hEventhandle) {
        	AsciiPrint(PRINTSIG"Event Handle to be destroyed is NULL\n");
        	return TF_RESULT_CODE_FAILURE;
    	}

	for (iLoop = 0 ; iLoop < iNum; iLoop++) {
		dalResult = DALSYS_DestroyObject(hEventhandle[iLoop]);
       		if ( DAL_SUCCESS != dalResult ) {
			AsciiPrint(PRINTSIG"Destroy Event Object Failed : %d; iLoop: %d\n ", dalResult, iLoop);
			uIs_Fail = 1;
       		}
    	}
	
	if(NULL != hWorkLoop1) {
		dalResult = DALSYS_DestroyObject(hWorkLoop1);
		if (DAL_SUCCESS != dalResult) {
        		AsciiPrint(PRINTSIG"WorkLoop Handle 1 destroy failed\n");
			uIs_Fail = 1;
		}
	}

	if(NULL != hWorkLoop2) {
		dalResult = DALSYS_DestroyObject(hWorkLoop2);
		if (DAL_SUCCESS != dalResult) {
        		AsciiPrint(PRINTSIG"WorkLoop Handle 2 destroy failed\n");
			uIs_Fail = 1;
		}
	}

	if (uIs_Fail)
		return TF_RESULT_CODE_FAILURE;
	else 
		return TF_RESULT_CODE_SUCCESS;
}

// This routine can be used to cleanup any Event Objects
// In Arg: Eventobject Array, number of events 
// Out: TF_RESULT_CODE_FAILURE/TF_RESULT_CODE_SUCCESS

UINT32 DALTEST_CleanupWLArray(DALSYSWorkLoopHandle *hWLHandle, int iNum) {
	UINT32 uIndex = 0;
	
	if (NULL == hWLHandle) {
        	AsciiPrint(PRINTSIG"WorkLoop Handle to be destroyed is NULL\n");
        	return TF_RESULT_CODE_FAILURE;
    	}

	// Cleanup the array of WL Events
	for (uIndex = 0; uIndex < iNum; uIndex++) {
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWLHandle[uIndex])) {
			AsciiPrint(PRINTSIG"Destroying WL events failed\n");
			return TF_RESULT_CODE_FAILURE;
		}
	}

	return TF_RESULT_CODE_SUCCESS;
}

/*
===============================================================================
**  Function : TestDalsysEventsL1
** ============================================================================
*/
/*!
    @brief
    	Events related L1 tests
    @details
	Change ownership of a CB,WL, Timeout and Timer Events
	Reset WL,CB events
	Wait on a CB,WL Events
	Delete Events randomly and check if un-deleted events can be triggered
	Check mutual exclusion of a Timer/Timeout event
	Attach a Non-WL event to a WL
	Wait on a Event from a non-owner
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_EventCreate
	DALSYS_SetupCallbackEvent
	DALSYS_EventCtrl
	DALSYS_DestroyObject

	
    @par Side Effects
	None.

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/



UINT32 TestDalsysEventsL1(UINT32 dwArg, CHAR8* pszArg[])
{
	// static CHAR8 *WLName1 = "DALTF_DALSYS_EventsL1_WL1";
	// static CHAR8 *WLName2 = "DALTF_DALSYS_EventsL1_WL2";
	UINT32 uIndex=0, uArray = 0;
	DALSYSEventHandle hEvent = NULL;				// Handle for Client Default Event
	DALSYSEventHandle hWLEvent = NULL;			// Handle for WorkLoop Event
	DALSYSEventHandle hWLEvents[TEST_DALSYS_NUM_OF_WL_EVENTS];	// Array of Handles for WL Event
	DALSYSEventHandle hCBEvent = NULL;			// Handle for Callback Event
	DALSYSEventHandle hTEvent = NULL;				// Handle for Timer Event
	DALSYSEventHandle hToutEvent = NULL;			// Handle for Timeout Event
	DALSYSEventHandle hEvents[TEST_DALSYS_NUM_OF_EVENT_OBJECTS];// Create an Array to store handles

	DALSYSWorkLoopHandle hWorkLoop = NULL;			// Handle for Workloop 1
	DALSYSWorkLoopHandle hWorkLoop2 = NULL;			// Handle for Workloop 2

	// Create Handle for WL event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
										  &hWLEvent,
										  NULL)) {
		AsciiPrint(PRINTSIG"Create WL event failed\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hWLEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	hEvents[uArray++] = hWLEvent;

	// Create Handle for Client default event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
										  &hEvent,
										  NULL)) {
		AsciiPrint(PRINTSIG"Create CLIENT DEFAULT failed\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	hEvents[uArray++] = hEvent;

	// Create handle for Timer event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMER_EVENT,
										  &hTEvent,
										  NULL)) {
		AsciiPrint(PRINTSIG"Create Timer event failed\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hTEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	hEvents[uArray++] = hTEvent;

	// Create Handle for Timeout event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
						&hToutEvent,
						NULL)) {
		AsciiPrint(PRINTSIG"Create Timeout event failed\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hToutEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	hEvents[uArray++] = hToutEvent;
	
	// Create Handle for CB event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
						&hCBEvent,
						NULL)) {
		AsciiPrint(PRINTSIG"Create Callback event failed\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hCBEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
       		return TF_RESULT_CODE_FAILURE;
	}

	hEvents[uArray++] = hCBEvent;

	// Create the 1st Workloop
	if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(TEST_DALSYS_PRIO_OF_WL,
											     TEST_DALSYS_NUM_OF_WL_EVENTS,
											     &hWorkLoop,
											     NULL)) {
		AsciiPrint(PRINTSIG"Register WL failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Create a 2nd Workloop
	if (DAL_SUCCESS != DALSYS_RegisterWorkLoop(TEST_DALSYS_PRIO_OF_WL,
												 TEST_DALSYS_NUM_OF_WL_EVENTS,
      											 &hWorkLoop2,
												 NULL)) {
		AsciiPrint(PRINTSIG"Register 2nd WL failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Add an WL Event to the 1st WorkLoop
	if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop (hWorkLoop,
												  workLoopFcn,
												  hEvents[1],
												  hEvents[0],
												  NULL)) {
		AsciiPrint(PRINTSIG"AddEventToWorkloop failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Delete above added WL event from WL
	if (DAL_SUCCESS != DALSYS_DeleteEventFromWorkLoop(hWorkLoop, hWLEvent)) {
		AsciiPrint(PRINTSIG"Delete from WL failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Add WL Event to the WorkLoop again after Delete
	if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop (hWorkLoop,workLoopFcn,hEvents[1],hEvents[0], NULL)) {
		AsciiPrint(PRINTSIG"AddEventToWorkloop failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Trigger WL Event
	if ( DAL_SUCCESS != DALSYS_EventCtrl (hEvents[0], DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"Cannot trigger WL event after Reset\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Wait for the signal
	if (DAL_SUCCESS != DALSYS_EventWait(hEvent)) {
		AsciiPrint(PRINTSIG"Eventwait Failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// ACQUIRE ownership and Reset the waiting Event to reuse later
	if (DAL_SUCCESS != DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP)) {
		AsciiPrint(PRINTSIG"Acquire ownership failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	if (DAL_SUCCESS != DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_RESET)) {
		AsciiPrint(PRINTSIG"Reset Event failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Test to check if Deleting events from WL works
	// Create WL Events and add all WL Events to the WL2
	for (uIndex=0; uIndex < TEST_DALSYS_NUM_OF_WL_EVENTS; uIndex++) {
		if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT, &hWLEvents[uIndex], NULL)) {
			AsciiPrint(PRINTSIG"WL Event create failed\n");
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
				return TF_RESULT_CODE_FAILURE;
			}
			return TF_RESULT_CODE_FAILURE;
		}
	}
	
	// Add multiple WL events to an WorkLoop to do WL related operations
	for (uIndex = 0; uIndex< TEST_DALSYS_NUM_OF_WL_EVENTS; uIndex++) {
		if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop (hWorkLoop2, 		// Handler for WL 2
													  workLoopFcn3,		// CB function
													  hEvent,			// Waiting Event
													  hWLEvents[uIndex],	// WL events to add
													  NULL)) {		// Sync object 
			AsciiPrint(PRINTSIG"AddEventToWorkloop failed %d\n", uIndex);
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
				return TF_RESULT_CODE_FAILURE;
			}
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
				AsciiPrint(PRINTSIG"Destroying WL object failed\n");
				return TF_RESULT_CODE_FAILURE;
			}
			return TF_RESULT_CODE_FAILURE;
		}
	}

	// Delete all the events from WL except for the last event
	for (uIndex=0; uIndex < TEST_DALSYS_NUM_OF_WL_EVENTS-1; uIndex++) {
		if (DAL_SUCCESS != DALSYS_DeleteEventFromWorkLoop (hWorkLoop2, hWLEvents[uIndex])) {
			AsciiPrint(PRINTSIG"Delete from Workloop failed %d\n ", uIndex);
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
				return TF_RESULT_CODE_FAILURE;
			}
			if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
				AsciiPrint(PRINTSIG"Destroying WL object failed\n");
				return TF_RESULT_CODE_FAILURE;
			}
			return TF_RESULT_CODE_FAILURE;
		}
	}

	// Trigger the last WL event
	if ( DAL_SUCCESS != DALSYS_EventCtrl (hWLEvents[TEST_DALSYS_NUM_OF_WL_EVENTS-1], DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"Cannot trigger WL event\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
			AsciiPrint(PRINTSIG"Destroying WL object failed\n");
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Wait for signal from WL event
	if (DAL_SUCCESS != DALSYS_EventWait(hEvent)) {
		AsciiPrint(PRINTSIG"Eventwait Failed\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
			AsciiPrint(PRINTSIG"Destroying WL object failed\n");
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	// Trigger an Deleted Event
	if (DAL_ERROR != DALSYS_EventCtrl (hWLEvents[0], DALSYS_EVENT_CTRL_TRIGGER)) {
		AsciiPrint(PRINTSIG"Triggering deleted event successful unexpectedly\n");
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
			return TF_RESULT_CODE_FAILURE;
		}
		if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
			AsciiPrint(PRINTSIG"Destroying WL object failed\n");
			return TF_RESULT_CODE_FAILURE;
		}
		return TF_RESULT_CODE_FAILURE;
	}

	AsciiPrint(PRINTSIG"End of TestDalsysEventsL1\n");
	
	// Cleanup all the 5 events objects and 2 WorkLoop objects 
	if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpEventsL1(hEvents,uArray,hWorkLoop,hWorkLoop2)) {
		return TF_RESULT_CODE_FAILURE;
	}

	// Call the generic WLevents destructor, pass the array of event Objects
	if(TF_RESULT_CODE_SUCCESS != DALTEST_CleanupWLArray(hWLEvents,TEST_DALSYS_NUM_OF_WL_EVENTS )) {
		AsciiPrint(PRINTSIG"Destroying WL object failed\n");
		return TF_RESULT_CODE_FAILURE;
	}

	return TF_RESULT_CODE_SUCCESS;
}

// TestDalsysEventsL1's Help Data
CONST TF_HelpDescr TestDalsysEventsL1Help =
{
    "TestDalsysCBEventsL2",
    sizeof(DalsysEventsTestParam) / sizeof(TF_ParamDescr),
    DalsysEventsTestParam
};

CONST TF_TestFunction_Descr TestDalsysEventsL1Test =
{
   PRINTSIG"TestDalsysEventsL1",
   TestDalsysEventsL1,
   &TestDalsysEventsL1Help,
   &context1
};
/* ************* End of TestDalsysEventsL1 Test ***************** */


/* Cleanup function for TestDalsysMultWaitL1 */ 
static UINT32 DALTEST_CleanUpMultWaitL1(DALSYSEventHandle hEvent1,
	       				DALSYSEventHandle hEvent2,
	       				DALSYSEventHandle hEvent3,
				       	DALSYSEventHandle hEvent4,
				       	DALSYSEventHandle hEvent5) {
	UINT32 uRet, uIs_Fail=0, uIndex;
    	DALSYSEventHandle hEvents[5];
	hEvents[0] = hEvent1;
	hEvents[1] = hEvent2;
	hEvents[2] = hEvent3;
	hEvents[3] = hEvent4;
	hEvents[4] = hEvent5;

	for (uIndex=0; uIndex < 5; uIndex++) {
		if (hEvents[uIndex]) {
			uRet = DALSYS_DestroyObject(hEvents[uIndex]);
			if (DAL_SUCCESS != uRet) {
				AsciiPrint(PRINTSIG"Destroy failed hEvents[%i]\n", uIndex);
				uIs_Fail = 1;
			}
		}
	}
	if (! uIs_Fail) 
		return TF_RESULT_CODE_SUCCESS;
	else 
		return TF_RESULT_CODE_FAILURE;
}

/*
===============================================================================
**  Function : TestDalsysMultWaitL1
** ============================================================================
*/
/*!
    @brief
    	Sanity test for DALSYS_EventMultipleWait API
    @details
    	Wait on CB/WL events
	Timeout occurs before CLIENT_DEF event is triggered and vice-versa
	check if idx value of triggered event is set properly
   	Zero timeout scenario should return immediately 	
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_EventCreate
	DALSYS_EventCtrl
	DALSYS_DestroyObject
	DALSYS_EventMultipleWait
	
    @par Side Effects

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/


UINT32 TestDalsysMultWaitL1(UINT32 dwArg, CHAR8* pszArg[])
{
	DALSYSEventHandle h1Event = NULL;			// Handle for Client Default Event 1
	DALSYSEventHandle h2Event = NULL;			// Handle for Client Default Event 2
	DALSYSEventHandle hWLEvent = NULL;			// Handle for WorkLoop Event
	DALSYSEventHandle hCBEvent = NULL;			// Handle for Callback Event
	DALSYSEventHandle hToutEvent = NULL;		// Handle for Timeout Event
	DALSYSEventHandle hEvents[TEST_DALSYS_NUM_OF_EVENT_OBJECTS];// Create an Array to store handles
	UINT32 eventIdx;			 				// To store the id of event
	int iTimer;									// To store timer value
	int iRet;
	CHAR8 *endptr;

	if ( dwArg != (sizeof(DalsysMultWaitTestParam) / sizeof(TF_ParamDescr)))
	{
		AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
		sizeof(DalsysMultWaitTestParam) / sizeof(TF_ParamDescr), dwArg);
		return(TF_RESULT_CODE_BAD_PARAM);
	}

	iTimer = strtol(pszArg[0], &endptr, 10);
	if (*endptr != '\0') {
		AsciiPrint(PRINTSIG"string conversion failed %s\n", *endptr);
       		return TF_RESULT_CODE_FAILURE;
	}

	// Create Handle for CB event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
						&hCBEvent,
						NULL)) {
		AsciiPrint(PRINTSIG"Create Callback event failed\n" );
       		return TF_RESULT_CODE_FAILURE;
	}
	if (NULL == hCBEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}


	// Create Handle for WL event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
						&hWLEvent,
						NULL)) {
		AsciiPrint(PRINTSIG"Create WL event failed\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent, NULL, NULL, NULL, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}
	if (NULL == hWLEvent) {
		AsciiPrint(PRINTSIG"Create WL event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Create Handle for Client default event 1
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
						&h1Event,
						NULL)) {
		AsciiPrint(PRINTSIG"Create CLIENT DEFAULT failed\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent, NULL, NULL, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}
	if (NULL == h1Event) {
		AsciiPrint(PRINTSIG"Create Client def event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Create Handle for Client default event 2
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
						&h2Event,
						NULL)) {
		AsciiPrint(PRINTSIG"Create CLIENT DEFAULT failed\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,h1Event, NULL, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}
	if (NULL == h2Event) {
		AsciiPrint(PRINTSIG"Create Client def event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Create Handle for Timeout event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
						&hToutEvent,
						NULL)) {
		AsciiPrint(PRINTSIG"Create Timeout event failed\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,h1Event,h2Event, NULL)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}
	if (NULL == hToutEvent) {
		AsciiPrint(PRINTSIG"Timeout event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Add Client def and timeout event to an array
	hEvents[0] = h1Event;			// CLIENT_DEFAULT Event
	hEvents[1] = hToutEvent;		// Timeout Event
	hEvents[2] = h2Event;			// CLIENT_DEFAULT Event

	// Trigger 1st Event
	iRet = DALSYS_EventCtrl(h1Event, DALSYS_EVENT_CTRL_TRIGGER);
	AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER  returned %d\n", iRet);

	// Trigger 2nd Event
	iRet = DALSYS_EventCtrl(h2Event, DALSYS_EVENT_CTRL_TRIGGER);
	AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER  returned %d\n", iRet);

	// Reset 1st Event
	iRet = DALSYS_EventCtrl(h1Event, DALSYS_EVENT_CTRL_RESET);
	AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_RESET  returned %d\n", iRet);

	// Wait on WL event should fail
	eventIdx = 0XFFFFFFFF;
	iRet = DALSYS_EventMultipleWait (&hWLEvent, 1, iTimer, &eventIdx);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %d\n", eventIdx);

	if (DAL_ERROR != iRet) {	
		AsciiPrint(PRINTSIG"Event Multiple wait on WL event succeeded unexpectedly\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}

	// Wait on CB event should fail
	eventIdx = 0XFFFFFFFF;
	iRet = DALSYS_EventMultipleWait (&hCBEvent, 1, iTimer, &eventIdx);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %d\n", eventIdx);

	if (DAL_ERROR != iRet) {	
		AsciiPrint(PRINTSIG"Event Multiple wait on CB event succeeded unexpectedly\n" );
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}

	// This should pass and eventidx value should be set to 2
	eventIdx = 0XFFFFFFFF;
	iRet = DALSYS_EventMultipleWait(hEvents, 3, iTimer, &eventIdx);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %d\n", eventIdx);

	if(DAL_SUCCESS != iRet){
		AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed\n");
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}

	if (eventIdx != 2) {
		AsciiPrint(PRINTSIG"Event Id not set properly\n");
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
			return TF_RESULT_CODE_FAILURE;
		}	
	}

	// Reset 2nd Event
	iRet = DALSYS_EventCtrl(h2Event, DALSYS_EVENT_CTRL_RESET);
	AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_RESET  returned %d\n", iRet);

	// Zero Timeout, should timeout and iRet should be DAL_ERROR_TIMEOUT 
	eventIdx = 0XFFFFFFFF;
	iRet = DALSYS_EventMultipleWait(hEvents, 3, 0, &eventIdx);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
	AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %d\n", eventIdx);

	if (DAL_ERROR_TIMEOUT != iRet) {
		AsciiPrint(PRINTSIG"Did not return with DAL_ERROR_TIMEOUT\n");
		if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
				return TF_RESULT_CODE_FAILURE;
		}	
	}

	// Cleanup objects
	if (TF_RESULT_CODE_SUCCESS != DALTEST_CleanUpMultWaitL1(hCBEvent,hWLEvent,hToutEvent,h1Event,h2Event)) {
			return TF_RESULT_CODE_FAILURE;
	}

	return TF_RESULT_CODE_SUCCESS;
}

// TestDalsysEventsL1's Help Data
CONST TF_HelpDescr TestDalsysMultWaitL1Help =
{
    "TestDalsysMultWaitL1",
    sizeof(DalsysMultWaitTestParam) / sizeof(TF_ParamDescr),
    DalsysMultWaitTestParam
};

CONST TF_TestFunction_Descr TestDalsysMultWaitL1Test =
{
   PRINTSIG"TestDalsysMultWaitL1",
   TestDalsysMultWaitL1,
   &TestDalsysMultWaitL1Help,
   &context1
};

/************** End of TestDalsysMultWaitL1 Test ******************/

/*
===============================================================================
**  Function : TestDalsysEventsNeg
** ============================================================================
*/
/*!
    @brief
    Negative test for Events
    @details
    Add non-WL events to Workloop
	Add non-Callback events to DALSYS_SetupCallbackEvent 
	
    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

	@par API Tested 
	DALSYS_EventCreate
	DALSYS_EventCtrl
	DALSYS_DestroyObject
		
    @par Side Effects

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/


UINT32 TestDalsysEventsNeg(UINT32 dwArg, CHAR8* pszArg[])
{
	// static CHAR8 *WLName = "DALTF_DALSYS_EventsNeg";
	DALSYSEventHandle hWLEvent = NULL;			// Handle for WorkLoop Event
	DALSYSEventHandle hCBEvent = NULL;			// Handle for Callback Event
	DALSYSWorkLoopHandle hWorkLoop = NULL;		// Handle for Workloop
		
	// Create Handle for WL event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
										  &hWLEvent,
										  NULL)) {
		AsciiPrint(PRINTSIG"Create WL event failed\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	if (NULL == hWLEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Create Handle for CB event
	if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
										  &hCBEvent,
										  NULL)) {
		AsciiPrint(PRINTSIG"Create Callback event failed\n" );
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWLEvent)) {
			AsciiPrint(PRINTSIG"Destroy hWLEvent Object failed\n" );
		}
    	return TF_RESULT_CODE_FAILURE;
	}
	
	if (NULL == hCBEvent) {
		AsciiPrint(PRINTSIG"Create Callback event NULL handle\n" );
       		return TF_RESULT_CODE_FAILURE;
	}

	// Pass a non-Callback event to DALSYS_SetupCallbackEvent (Must fail)
	if (DAL_ERROR != DALSYS_SetupCallbackEvent(hWLEvent,	// Event handle
											   (DALSYSCallbackFunc) workLoopFcn, // Callback function to call
											   NULL))		// User context
   	{
		AsciiPrint(PRINTSIG"SetupCallbackEvent passed unexpectedly\n");
		return TF_RESULT_CODE_FAILURE;
	}	
	
	// Create an WorkLoop
	if (DAL_SUCCESS != DALSYS_RegisterWorkLoop( TEST_DALSYS_PRIO_OF_WL,		 // 0
											      TEST_DALSYS_NUM_OF_WL_EVENTS, // 5
											      &hWorkLoop,					 // WorkLoop Handle
											      NULL))						 // Dynamically allocated
    {
		AsciiPrint(PRINTSIG"Register WL failed\n");
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWLEvent)) {
			AsciiPrint(PRINTSIG"Destroy hWLEvent Object failed\n" );
		}
		if (DAL_SUCCESS != DALSYS_DestroyObject(hCBEvent)) {
			AsciiPrint(PRINTSIG"Destroy hCBEvent Object failed\n" );
		}
		return TF_RESULT_CODE_FAILURE;
	}
	
	// Add a non-WorkLoop event to a WorkLoop (Must fail)
	if (DAL_ERROR != DALSYS_AddEventToWorkLoop (hWorkLoop,		// WorkLoop Handle
												workLoopFcn,	// Callback function name
												NULL,			// Context
												hCBEvent,		// Event to add to Workloop
												NULL)) {		// Sync object
		AsciiPrint(PRINTSIG"AddEventToWorkloop passed unexpectedly\n");
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWLEvent)) {
			AsciiPrint(PRINTSIG"Destroy hWLEvent Object failed\n" );
		}
		if (DAL_SUCCESS != DALSYS_DestroyObject(hCBEvent)) {
			AsciiPrint(PRINTSIG"Destroy hCBEvent Object failed\n" );
		}
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWorkLoop)) {
			AsciiPrint(PRINTSIG"Destroy hWorkLoop Object failed\n" );
		}
		return TF_RESULT_CODE_FAILURE;
	}
	
	AsciiPrint(PRINTSIG"TestDalsysEventsNeg Completed\n" );
	
	AsciiPrint(PRINTSIG"TestDalsysEventsNeg Cleanup\n" );
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWLEvent)) {
			AsciiPrint(PRINTSIG"Destroy hWLEvent Object failed\n" );
		}
		if (DAL_SUCCESS != DALSYS_DestroyObject(hCBEvent)) {
			AsciiPrint(PRINTSIG"Destroy hCBEvent Object failed\n" );
		}
		if (DAL_SUCCESS != DALSYS_DestroyObject(hWorkLoop)) {
			AsciiPrint(PRINTSIG"Destroy hWorkLoop Object failed\n" );
		}
		
	return TF_RESULT_CODE_SUCCESS;
}

// TestDalsysEventsNeg's Help Data
CONST TF_HelpDescr TestDalsysEventsNegHelp = 
{
    "TestDalsysEventsNeg",
    sizeof(DalsysEventNegTestParam) / sizeof(TF_ParamDescr),
    DalsysEventNegTestParam
};

CONST TF_TestFunction_Descr TestDalsysEventsNegTest =
{
   PRINTSIG"TestDalsysEventsNeg",
   TestDalsysEventsNeg,
   &TestDalsysEventsNegHelp,
   &context1
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_events_tests
===========================================================================*/

const TF_TestFunction_Descr* kernel_dal_daltf_dalsys_events_tests[] = {
  &TestDalsysWLEventsL1Test,
  &TestDalsysCBEventsL2Test,
  &TestDalsysEventsL1Test,
  &TestDalsysMultWaitL1Test,
  &TestDalsysEventsNegTest,
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_mem_init_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_events_init_all(VOID)
{
  DALResult dalResult;

  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_events_tests) / sizeof(kernel_dal_daltf_dalsys_events_tests[0]));

  dalResult = tests_daltf_add_tests(kernel_dal_daltf_dalsys_events_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_mem_deinit_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_events_deinit_all(VOID)
{
  DALResult dalResult;
  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_events_tests) / sizeof(kernel_dal_daltf_dalsys_events_tests[0]));

  dalResult = tests_daltf_remove_tests(kernel_dal_daltf_dalsys_events_tests, TF_NumberTests);
  return dalResult;
}
