/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		KernelDalsysTest.h
  DESCRIPTION:	Header File for all UEFI APT TLMM Tests
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  05/17/14   nc      Created
================================================================================*/

#ifndef _KERNEL_DALTF_Dalsys_MEM_TESTS_
#define _KERNEL_DALTF_Dalsys_MEM_TESTS_

#include "KernelDalsysTest.h"
#include <DALSys.h>

/*====================================================
** STRUCTURES **
====================================================*/

UINT32 TestDalsysMallocZero(UINT32 dwArg, CHAR8* pszArg[]); 
UINT32 TestDalsysMalloc(UINT32 dwArg, CHAR8* pszArg[]); 
UINT32 TestDalsysMemReg(UINT32 dwArg, CHAR8* pszArg[]); 
UINT32 TestDalsysCache(UINT32 dwArg, CHAR8* pszArg[]); 
 
//Initialize dalsys sync daltf tests
DALResult kernel_dal_daltf_dalsys_mem_init_all(VOID);

//De-Initialize dalsys sync daltf tests
DALResult kernel_dal_daltf_dalsys_mem_deinit_all(VOID);

#endif