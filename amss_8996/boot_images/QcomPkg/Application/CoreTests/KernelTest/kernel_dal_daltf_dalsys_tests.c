/*============================================================================
@file         kernel_dal_daltf_dalsys_tests.c

@brief        DALTF based Dalsys tests

GENERAL DESCRIPTION
  This file contains the basic tests for Dalsys APIs.

Copyright (c) 2011 Qualcomm Technologies Inc. All Rights Reserved. Qualcomm Confidential and Proprietary
============================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Application/CoreTests/KernelTest/kernel_dal_daltf_dalsys_tests.c#1 $
  $DateTime: 2015/06/01 21:35:43 $
  $Author: pwbldsvc $

when         who     what, where, why
--------     ---     ---------------------------------------------------------
03/13/2015    kk     Porting dalsys tests to XBL

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR CHIPINFO DRIVER TESTS

==========================================================================*/
// #include "diagcmd.h"
#include "kernel_dal_daltf_dalsys_tests.h"
// #include "DALDeviceId.h"
#include "DALSys.h"
#include "DDITimer.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TEST_DALSYS_WL_STACK_SIZE 0x00001000
#define DWPRIORITY 0
#define DWMAXNUMEVENTS 10

/*==========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

==========================================================================*/

/*=========================================================================
      Constants and Macros
==========================================================================*/

CONST TF_ParamDescr DalsysSyncTestParam[] =
{
    {TF_PARAM_UINT32, "Allocate Object",              "0 - Not Use Pre-Allocated Memory\n"
                                                      "Otherwise - Use Pre-Allocated Memory\n"},
    {TF_PARAM_UINT32, "Sync Attribute",               "0x00000010 - DALSYS_SYNC_ATTR_RESOURCE \n"
                                                      "0x00000020 - DALSYS_SYNC_ATTR_RESOURCE_INTERRUPT\n"
                                                      "0x00000040 - DALSYS_SYNC_ATTR_NO_PREEMPTION\n"
                                                      "0x00000080  - DALSYS_SYNC_ATTR_HARD_NO_PREEMPTION\n"},
};

CONST TF_ParamDescr DalsysMemTestParam[] =
{
    {TF_PARAM_UINT32, "Memory  Size",               " Recommend between 0 and 64 bytes \n"},
};

CONST TF_ParamDescr DalsysMemRegionTestParam[] =
{
    {TF_PARAM_UINT32, "Memory Type",               " 1 - DALSYS_MEM_PROPS_NORMAL\n"
                                                   " 2 - DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT (DMA)\n"
												   " or other valid mem region type \n"},
    {TF_PARAM_UINT32, "Memory  Size",              "Recommend between 0 and 64 bytes "},
    {TF_PARAM_UINT32, "Memory Free",               " 0 - not destroy MemRegion object not supported"},
};

CONST TF_ParamDescr DalsysTestParam[] =
{
    {TF_PARAM_UINT32, "Allocate Object",               "0 - Not Use Pre-Allocated Object\n"
                                                      "Otherwise - Use Pre-Allocated Memory\n"},
    {TF_PARAM_UINT32, "Timer",                         "timer in us"},
};

/* ============================================================================
**  Function : TestDalsysSync
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_SyncCreate DALSYS_SyncEnter DALSYS_SyncLeave API.

    @details
    Test the DALSYS_SyncCreate API with different type of sync object types, and with/without
    pre-allocating memory for the sync object.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters


    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/
UINT32 TestDalsysSync(UINT32 dwArg, CHAR8* pszArg[])
{
   // allocate DALSYSSyncObj object space
   DALSYS_SYNC_OBJECT(SyncObject);
   DALSYSSyncObj *pSyncObj = &SyncObject;
   DALSYSSyncHandle hSync;
   DALResult iRet;
   UINT32 iAllocObject;
   UINT32 iSyncAttr;
   char * endptr;

   AsciiPrint(PRINTSIG"TestDalsysSyncTest start\n");

   if ( dwArg != (sizeof(DalsysSyncTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysSyncTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10);

   if(0 == iAllocObject) {
      // Not use pre-aollocated sync object
      pSyncObj = NULL;
   }
   // Input should be a valid sync attr
   iSyncAttr = strtol(pszArg[1], &endptr, 10);

   if (DALSYS_SYNC_ATTR_RESOURCE == iSyncAttr ||
       DALSYS_SYNC_ATTR_RESOURCE_INTERRUPT == iSyncAttr ||
	   DALSYS_SYNC_ATTR_NO_PREEMPTION == iSyncAttr ||
       DALSYS_SYNC_ATTR_HARD_NO_PREEMPTION == iSyncAttr){
       AsciiPrint(PRINTSIG"Sync Type %d", iSyncAttr);
	} else {
	   AsciiPrint(PRINTSIG"Bad Param on Sync Type is entered\n");
	   return TF_RESULT_CODE_BAD_PARAM;

	}

    AsciiPrint(PRINTSIG"DALSYS_SyncCreate");
    iRet = DALSYS_SyncCreate(iSyncAttr, &hSync, pSyncObj);
	if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_SyncCreate failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }
   if(NULL == hSync) {
      AsciiPrint(PRINTSIG"DALSYS_SyncCreate failed, Sync handle is NULL\n");
      return TF_RESULT_CODE_FAILURE;
   }

   // Call SyncEnter
   AsciiPrint(PRINTSIG"DALSYS_SyncEnter\n");
   DALSYS_SyncEnter(hSync);
   // Call SyncLeave
   AsciiPrint(PRINTSIG"DALSYS_SyncLeave\n");
   DALSYS_SyncLeave(hSync);
   iRet = DALSYS_DestroyObject(hSync);
   if(DAL_SUCCESS !=  iRet) {
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysSyncTest end\n");
   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysSyncHelp =
{
    "TestDalsysSync",
    sizeof(DalsysSyncTestParam) / sizeof(TF_ParamDescr),
    DalsysSyncTestParam
};

CONST TF_TestFunction_Descr TestDalsysSyncTest =
{
   PRINTSIG"TestDalsysSync",
   TestDalsysSync,
   &TestDalsysSyncHelp,
   &context1
};


/* ============================================================================
**  Function : TestDalsysMemObjs1
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_Malloc DALSYS_Free API.

    @details
    Test the DALSYS_Malloc and DALSYS_Free APIs.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/
UINT32 TestDalsysMemObjs1(UINT32 dwArg, CHAR8* pszArg[])
{
   UINT32 * pMem;
   char * endptr;
   UINT32 iMemSize;
   DALResult iRet;
   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysMemObjs1 start\n");

   if ( dwArg != (sizeof(DalsysMemTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d",
               sizeof(DalsysMemTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iMemSize = strtol(pszArg[0], &endptr, 10);

   //2nd parameter is not required for this test.

   AsciiPrint(PRINTSIG"Memory size to be allocated %d\n", iMemSize);

	AsciiPrint(PRINTSIG"DALSYS_Malloc");
   // Allocate simple memory
    iRet = DALSYS_Malloc( iMemSize, (VOID **)&pMem);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_Malloc failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   if(NULL == pMem) {
      AsciiPrint(PRINTSIG"DALSYS_Malloc failed, pMem is NULL\n");
      return TF_RESULT_CODE_FAILURE;
   }

   *pMem = 0x12345678;
   DALSYS_Free(pMem);

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysMemObjs1Help =
{
    "TestDalsysMemObjs1",
    sizeof(DalsysMemTestParam) / sizeof(TF_ParamDescr),
    DalsysMemTestParam
};

CONST TF_TestFunction_Descr TestDalsysMemObjs1Test =
{
   PRINTSIG"TestDalsysMemObjs1",
   TestDalsysMemObjs1,
   &TestDalsysMemObjs1Help,
   &context1
};

/* ============================================================================
**  Function : TestDalsysMemObjs2
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_MemRegionAlloc DALSYS_MemInfo
	DALSYS_DestroyObject API.

    @details
    Test the DALSYS_MemRegionAlloc DALSYS_MemInfo DALSYS_DestroyObject APIs for
    different mem region type.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/
UINT32 TestDalsysMemObjs2(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYSMemHandle hNormMem;
   DALSYSMemInfo MemInfo;
   UINT32 * pMem;
   char * endptr;
   UINT32 iMemType;
   UINT32 iMemSize;
   UINT32 iMemFree;
   DALResult iRet;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysMemObjs2 start\n");

   if ( dwArg != (sizeof(DalsysMemRegionTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysMemRegionTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iMemType = strtol(pszArg[0], &endptr, 10);
   iMemSize = strtol(pszArg[1], &endptr, 10);
   iMemFree = strtol(pszArg[2], &endptr, 10);

   switch (iMemType)
   {
   case 1:
    AsciiPrint(PRINTSIG"DALSYS_MemRegionAlloc paged aligned memory region\n");
    iRet = DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_NORMAL,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED, iMemSize, &hNormMem,
                       NULL);
	break;

	case 2:
	AsciiPrint(PRINTSIG"DALSYS_MemRegionAlloc Allocate physically contiguous uncached mem (i.e. can be used for DMA)\n");
    iRet = DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED | DALSYS_MEM_PROPS_PHYS_CONT,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED, iMemSize, &hNormMem,
                       NULL);
	break;

	default:
	AsciiPrint(PRINTSIG"DALSYS_MemRegionAlloc for memory type %d\n", iMemType);
	iRet = DALSYS_MemRegionAlloc(iMemType,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED,
                       DALSYS_MEM_ADDR_NOT_SPECIFIED, iMemSize, &hNormMem,
                       NULL);
	break;

   }

   if (DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_MemRegionAlloc failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   if(NULL == hNormMem){
      AsciiPrint(PRINTSIG"DALSYS_MemRegionAlloc return  NULL memory handle\n");
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"Call DALSYS_MemInfo\n");
   iRet = DALSYS_MemInfo(hNormMem, &MemInfo);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_MemInfo failed, returned %d\n", iRet);
	  // in some environment, e.g. REX based, DALSYS_DestroyObject not supported
	  if (0 != iMemFree){
         DALSYS_DestroyObject(hNormMem);
	  }
      return TF_RESULT_CODE_FAILURE;
   }

   pMem = (UINT32 *)MemInfo.VirtualAddr;
   *pMem = 0x12345678;  // access mem

   if (0 != iMemFree){
      iRet = DALSYS_DestroyObject(hNormMem);
	  if(DAL_SUCCESS != iRet) {
          AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
          return TF_RESULT_CODE_FAILURE;
      }
	}
   AsciiPrint(PRINTSIG"TestDalsysMemObjs2 complete\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysMemObjs2Help =
{
    "TestDalsysMemObjs2",
    sizeof(DalsysMemRegionTestParam) / sizeof(TF_ParamDescr),
    DalsysMemRegionTestParam
};

CONST TF_TestFunction_Descr TestDalsysMemObjs2Test =
{
   PRINTSIG"TestDalsysMemObjs2",
   TestDalsysMemObjs2,
   &TestDalsysMemObjs2Help,
   &context1
};

/* ============================================================================
**  Function : TestDalsysEvents1
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtl
	DALSYS_EventWait API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtl DALSYS_EventWait APIs for
    a single event.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysEvents1(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventHandle hEvent;
   UINT32 iAllocObject;
   UINT32 iRet;
   char * endptr;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysEvents1 Start");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
   }

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_NORMAL,
                               &hEvent,
                               pEventObj);

   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_NORMAL returned %d\n", iRet);

   // DALSYS_EVENT_ATTR_NORMAL
   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_NORMAL failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   //-----------------------------------------------------------
   // Trigger it

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtl DALSYS_EVENT_CTRL_TRIGGER returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCtl DALSYS_EVENT_CTRL_TRIGGER failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventWait(hEvent);
   AsciiPrint(PRINTSIG"DALSYS_EventWait returned %d\n", iRet);

   // Wait on it, it sould immediately return
   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventWait failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Test cleaning up\n");
   iRet = DALSYS_DestroyObject(hEvent);
   AsciiPrint(PRINTSIG"DALSYS_DestroyObject returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysEvents1 end\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysEvents1Help =
{
    "TestDalsysEvents1",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysEvents1Test =
{
   PRINTSIG"TestDalsysEvents1",
   TestDalsysEvents1,
   &TestDalsysEvents1Help,
   &context1
};



/* ============================================================================
**  Function : TestDalsysEvents2
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtl
	DALSYS_EventWait DALSYS_EventMultipleWait API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtl DALSYS_EventWait APIs for
    a timeout event.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysEvents2(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   DALSYS_EVENT_OBJECT(TimeoutEventObject);
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventObj *pTimeoutEventObj = &TimeoutEventObject;
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hTimeoutEvent;
   DALSYSEventHandle hEvents[2]; // used in multiple wait
   UINT32 eventIdx;
   UINT32 iAllocObject;
   UINT32 iTimer;
   UINT32 iRet;
   char * endptr;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysEvents2 Start\n");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
      pTimeoutEventObj = NULL;
   }

   iTimer = strtol(pszArg[1], &endptr, 10); // timeout timer in this test

   // Create a timeout event object, used later on
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
                               &hTimeoutEvent,
                               pTimeoutEventObj);

   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT failed\n");
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &hEvent,
                               pEventObj);
   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {

      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   //-----------------------------------------------------------
   // Trigger it

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER  returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
     AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventWait(hEvent);
   AsciiPrint(PRINTSIG"DALSYS_EventWait  returned %d\n", iRet);

   // Wait on it, it sould immediately return
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventWait failed");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Wait on it again with timeout, it sould timeout
   hEvents[0] = hEvent;
   hEvents[1] = hTimeoutEvent;

   iRet = DALSYS_EventMultipleWait(hEvents, 2, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %\nd", eventIdx);

   if(DAL_ERROR_TIMEOUT != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Test cleaning up");

   iRet = DALSYS_DestroyObject(hEvent);
  if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
       DALSYS_DestroyObject(hTimeoutEvent);
        return TF_RESULT_CODE_FAILURE;
   }
   iRet =  DALSYS_DestroyObject(hTimeoutEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysEvents2 end\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysEvents2Help =
{
    "TestDalsysEvents2",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysEvents2Test =
{
   PRINTSIG"TestDalsysEvents2",
   TestDalsysEvents2,
   &TestDalsysEvents2Help,
   &context1
};


/* ============================================================================
**  Function : TestDalsysEvents3
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtl
	DALSYS_EventWait DALSYS_EventMultipleWait API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtl DALSYS_EventWait APIs for
    DALSYS_EVENT_CTL_RESET.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/


UINT32 TestDalsysEvents3(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   DALSYS_EVENT_OBJECT(TimeoutEventObject);
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventObj *pTimeoutEventObj = &TimeoutEventObject;
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hTimeoutEvent;
   DALSYSEventHandle hEvents[2]; // used in multiple wait
   UINT32 eventIdx;
   UINT32 iAllocObject;
   UINT32 iTimer;
   UINT32 iRet;
   char * endptr;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysEvents3 Start\n");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
      pTimeoutEventObj = NULL;
   }

   iTimer = strtol(pszArg[1], &endptr, 10); // timeout timer in this test

   // Create a timeout event object, used later on
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
                               &hTimeoutEvent,
                               pTimeoutEventObj);

   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT failed\n");
      return TF_RESULT_CODE_FAILURE;
   }


   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &hEvent,
                               pEventObj);
   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {

      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      return TF_RESULT_CODE_FAILURE;
   }


   //-----------------------------------------------------------
   // Trigger it

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER  returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
     AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventWait(hEvent);
   AsciiPrint(PRINTSIG"DALSYS_EventWait  returned %d\n", iRet);

   // Wait on it, it sould immediately return
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   //-----------------------------------------------------------
   // Trigger it again
   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_RESET);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_RESET returned %d\n", iRet);

   // Reset it
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_RESET failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Wait on it again with timeout, it sould timeout
   hEvents[0] = hEvent;
   hEvents[1] = hTimeoutEvent;

   iRet = DALSYS_EventMultipleWait(hEvents, 2, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventId %d\n", eventIdx);

   // Wait on it again with timeout, it should timeout
   if(DAL_ERROR_TIMEOUT != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Test cleaning up");
   iRet = DALSYS_DestroyObject(hEvent);
  if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
       DALSYS_DestroyObject(hTimeoutEvent);
       return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_DestroyObject(hTimeoutEvent);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysEvents3 end\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysEvents3Help =
{
    "TestDalsysEvents3",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysEvents3Test =
{
   PRINTSIG"TestDalsysEvents3",
   TestDalsysEvents3,
   &TestDalsysEvents3Help,
   &context1
};


/* ============================================================================
**  Function : TestDalsysEvents4
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtl
	DALSYS_EventWait DALSYS_EventMultipleWait API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtl DALSYS_EventWait APIs for
    DALSYS_EVENT_CTRL_ACQUIRE_OWNERSHIP.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/


UINT32 TestDalsysEvents4(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   DALSYS_EVENT_OBJECT(TimeoutEventObject);
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventObj *pTimeoutEventObj = &TimeoutEventObject;
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hTimeoutEvent;
   DALSYSEventHandle hEvents[2]; // used in multiple wait
   UINT32 eventIdx;
   UINT32 iAllocObject;
   UINT32 iTimer;
   UINT32 iRet;
   char * endptr;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysEvents4 Start\n");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
      pTimeoutEventObj = NULL;
   }

   iTimer = strtol(pszArg[1], &endptr, 10); // timeout timer in this test

   // Create a timeout event object, used later on
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
                               &hTimeoutEvent,
                               pTimeoutEventObj);

   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT failed\n");
      return TF_RESULT_CODE_FAILURE;
   }


   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &hEvent,
                               pEventObj);
   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {

      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   //-----------------------------------------------------------
   // Trigger it

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER  returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
     AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventWait(hEvent);
   AsciiPrint(PRINTSIG"DALSYS_EventWait  returned %d\n", iRet);

   // Wait on it, it sould immediately return
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Trigger it again
   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP);
   AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP returned %d\n", iRet);

   // Acquire ownership (redundant as already owner)
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Wait on it again with timeout, it sould NOT timeout
   hEvents[0] = hEvent;
   hEvents[1] = hTimeoutEvent;

   eventIdx = 0xFFFFFFFF;  // make sure it gets overwritten
   iRet = DALSYS_EventMultipleWait(hEvents, 2, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait eventID returned %d\n", eventIdx);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed, expect to see event triggered\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   if(0 != eventIdx){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed, eventId expected to be 0\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
    AsciiPrint(PRINTSIG"Test cleaning up\n");
	iRet = DALSYS_DestroyObject(hEvent);
  if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
       DALSYS_DestroyObject(hTimeoutEvent);
        return TF_RESULT_CODE_FAILURE;
   }

    iRet = DALSYS_DestroyObject(hTimeoutEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysEvents4 end\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysEvents4Help =
{
    "TestDalsysEvents4",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysEvents4Test =
{
   PRINTSIG"TestDalsysEvents4",
   TestDalsysEvents4,
   &TestDalsysEvents4Help,
   &context1
};


/* ============================================================================
**  Function : TestDalsysEvents5
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtl
	DALSYS_EventWait DALSYS_EventMultipleWait API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtl DALSYS_EventWait APIs for
    a multiple events and timeout.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/

UINT32 TestDalsysEvents5(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   DALSYS_EVENT_OBJECT(TimeoutEventObject);
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventObj *pTimeoutEventObj = &TimeoutEventObject;
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hEvent2;
   DALSYSEventHandle hTimeoutEvent;
   DALSYSEventHandle hEvents[3]; // used in multiple wait
   UINT32 eventIdx;
   UINT32 iAllocObject;
   UINT32 iTimer;
   UINT32 iRet;
   char * endptr;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysEvents5 Start\n");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
      pTimeoutEventObj = NULL;
   }

   iTimer = strtol(pszArg[1], &endptr, 10); // timeout timer in this test

   // Create a timeout event object, used later on
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
                               &hTimeoutEvent,
                               pTimeoutEventObj);

   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_TIMEOUT_EVENT failed\n");
      return TF_RESULT_CODE_FAILURE;
   }


   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &hEvent,
                               pEventObj);
   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {

      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                               &hEvent2,
                               NULL);
   AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT for 2nd events returned %d\n", iRet);

   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate DALSYS_EVENT_ATTR_CLIENT_DEFAULT failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }


   hEvents[0] = hEvent;
   hEvents[1] = hEvent2;
   hEvents[2] = hTimeoutEvent;

   iRet =  DALSYS_EventMultipleWait(hEvents, 3, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);

   // this should timeout
   if(DAL_ERROR_TIMEOUT != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }


   iRet = DALSYS_EventCtrl(hEvent2, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"Trigger 2nd event DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER returned %d\n", iRet);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet =  DALSYS_EventMultipleWait(hEvents, 3, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
   AsciiPrint(PRINTSIG"2nd event should be triggered:  eventId %d\n", eventIdx);

   // [1] should be triggered
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }

   if(eventIdx != 1){
      AsciiPrint(PRINTSIG"incorrect event triggered\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
   AsciiPrint(PRINTSIG"Trigger first event DALSYS_EventCtrl DALSYS_EVENT_CTRL_TRIGGER returned %d\n", iRet);


   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }


   iRet =  DALSYS_EventMultipleWait(hEvents, 3, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);
   AsciiPrint(PRINTSIG"1st event should be triggered:  eventId %d\n", eventIdx);

   // [0] should be triggered
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed, failed\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }
   if(eventIdx != 0){
      AsciiPrint(PRINTSIG"incorrect event triggered\n");
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }


   iRet =  DALSYS_EventMultipleWait(hEvents, 3, iTimer, &eventIdx);
   AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait returned %d\n", iRet);

   // this should timeout
   if(DAL_ERROR_TIMEOUT != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventMultipleWait failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hEvent2);
      return TF_RESULT_CODE_FAILURE;
   }
   AsciiPrint(PRINTSIG"Test cleaning up\n");

   iRet = DALSYS_DestroyObject(hEvent);
   if(DAL_SUCCESS != iRet ){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
       DALSYS_DestroyObject(hTimeoutEvent);
      DALSYS_DestroyObject(hEvent2);
        return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hEvent2);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
        DALSYS_DestroyObject(hTimeoutEvent);
       return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hTimeoutEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"TestDalsysEvents5 end\n");

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysEvents5Help =
{
    "TestDalsysEvents5",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysEvents5Test =
{
   PRINTSIG"TestDalsysEvents5",
   TestDalsysEvents5,
   &TestDalsysEvents5Help,
   &context1
};


/* ============================================================================
**  Function : TestDalsysCallbackEvents
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_EventCtlEx
	DALSYS_SetupCallbackEvent API.

    @details
    Test the DALSYS_EventCreate DALSYS_EventCtlEx DALSYS_SetupCallbackEvent APIs.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/



VOID NormalEventCallBack(VOID * ctx, UINT32 param, VOID * buf, UINT32 bufsize)
{
   UINT32 *p = (UINT32 *)ctx;
   UINT32 *b = (UINT32 *)buf;
   (*p)++;
   if(param)
   {
      *b=bufsize;
   }
}



UINT32 TestDalsysCallbackEvents(UINT32 dwArg, CHAR8* pszArg[])
{
   DALSYS_EVENT_OBJECT(EventObject);
   // Reuse these vars
   DALSYSEventObj *pEventObj = &EventObject;
   DALSYSEventHandle hEvent;
   volatile UINT32 testCount=0;
   volatile UINT32 buf[4];
   UINT32 iAllocObject;
   UINT32 iRet;
   char * endptr;

   AsciiPrint(PRINTSIG"TestDalsysCallbackEvents\n");
   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
               sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   iAllocObject = strtol(pszArg[0], &endptr, 10); // 0/1

   if(0 == iAllocObject)
   {
      pEventObj = NULL;
   }

   // Create a normal callback event object
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
                               &hEvent,
                               pEventObj);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;

   }
   iRet = DALSYS_SetupCallbackEvent(hEvent,
               (DALSYSCallbackFunc)NormalEventCallBack,
               (DALSYSCallbackFuncCtx)&testCount);
   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_SetupCallbackEvent failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   testCount = 1;

   // trigger it (i.e. call the callback
   iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);

   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // now testCount should be 1+1 = 2
   if(testCount != 2) {
      AsciiPrint(PRINTSIG"Callback not getting invoked, data is not updated\n");
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // trigger it again this time with params
   iRet =DALSYS_EventCtrlEx(hEvent, DALSYS_EVENT_CTRL_TRIGGER, 1, (VOID *)buf, sizeof(buf));
   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCtrlEx failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }


   // now testCount should be 3
   if(testCount != 3) {
      AsciiPrint(PRINTSIG"Callback not getting invoked, data is not updated\n");
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // buf[0] should equal sizeof(buf)
   if(buf[0] != sizeof(buf)){
      AsciiPrint(PRINTSIG"DALSYS_EventCtrl failed\n");
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"Test cleaning up");
   iRet = DALSYS_DestroyObject(hEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysCallbackEventsHelp =
{
    "TestDalsysCallbackEvents",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};



CONST TF_TestFunction_Descr TestDalsysCallbackEventsTest =
{
   PRINTSIG"TestDalsysCallbackEvents",
   TestDalsysCallbackEvents,
   &TestDalsysCallbackEventsHelp,
   &context1
};



/* ============================================================================
**  Function : TestDalsysTimerEvents
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DalTimer_Register
	 API.

    @details
    Test the DALSYS_EventCreate DalTimer_Register APIs.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/



VOID TimerEventCallBack(VOID * ctx, UINT32 param, VOID * buf, UINT32 bufsize)
{
   static UINT32 i;
   DALResult iRet;
   DALSYSEventHandle hEvent = (DALSYSEventHandle *)ctx;
   AsciiPrint(PRINTSIG"In Event Timer Callbacks");
   if(++i == 5)
   {
      // notify other thread that this has happened 5 times
	  iRet = DALSYS_EventCtrl(hEvent, DALSYS_EVENT_CTRL_TRIGGER);
      if(DAL_SUCCESS != iRet){
         AsciiPrint(PRINTSIG"DALSYS_EventCtrl failed");
	  } else {
         AsciiPrint(PRINTSIG"DALSYS_EventCtrl succeeded");
	  }

   }
}

UINT32 TestDalsysTimerEvents(UINT32 dwArg, char* pszArg[])
{

   // Reuse these vars
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hTimerEvent;
   DalDeviceHandle *hTimer;
   UINT32 iTimer;
   UINT32 iRet;
   char * endptr;

   AsciiPrint(PRINTSIG"TestDalsysTimerEvents\n");

   if ( dwArg != (sizeof(DalsysTestParam) / sizeof(TF_ParamDescr)))
   {
      AsciiPrint(PRINTSIG"#of input parameters incorrect: exp %d, got %d\n",
                sizeof(DalsysTestParam) / sizeof(TF_ParamDescr), dwArg);
      return(TF_RESULT_CODE_BAD_PARAM);
   }

   // pszArg[0] is not currently used in this test

   iTimer = strtol(pszArg[1], &endptr, 10); // timer for timer call back

   // Get access to timer DAL
   iRet = DAL_DeviceAttach(DALDEVICEID_TIMER, &hTimer);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DAL_DeviceAttach Timer failed, returned %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   // Create an event which the call triggers notifying this thread
   // that its done
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                              &hEvent,
                              NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed, return %d\n", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   // Create a timer callback event object
   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMER_EVENT|DALSYS_EVENT_ATTR_CALLBACK_EVENT,
                               &hTimerEvent,
                               NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_SetupCallbackEvent(hTimerEvent,
              (DALSYSCallbackFunc)TimerEventCallBack,
              (DALSYSCallbackFuncCtx) hEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_SetupCallbackEvent failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // register this event callback to be called when timer expires
   iRet = DalTimer_Register(hTimer, hTimerEvent, iTimer);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DalTimer_Register failed on start timer, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hTimerEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Wait until callback tests us its done
   iRet = DALSYS_EventWait(hEvent);
   if(DAL_SUCCESS != iRet ){
      AsciiPrint(PRINTSIG"DALSYS_EventWait failed, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hTimerEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // stop timer
   iRet = DalTimer_Register(hTimer, hTimerEvent, 0);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DalTimer_Register failed on cancel time, returned %d\n", iRet);
      DALSYS_DestroyObject(hEvent);
      DALSYS_DestroyObject(hTimerEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   AsciiPrint(PRINTSIG"Test cleaning up");
   iRet = DALSYS_DestroyObject(hEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DalTimer_DestoryObject failed\n");
      DALSYS_DestroyObject(hTimerEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hTimerEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DalTimer_DestoryObject failed\n");
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysTimerEventsHelp =
{
    "TestDalsysTimerEvents",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysTimerEventsTest =
{
   PRINTSIG"TestDalsysTimerEvents",
   TestDalsysTimerEvents,
   &TestDalsysTimerEventsHelp,
   &context1
};

/* ============================================================================
**  Function : TestDalsysWorkloop
** ============================================================================
*/
/*!
    @brief
    A Dalsys DalTF test case to test the DALSYS_EventCreate DALSYS_RegisterWorkLoop
	DALSYS_AddEventToWorkLoop API.

    @details
    Test the DALSYS_EventCreate DALSYS_AddEventToWorkLoop DALSYS_RegisterWorkLoop APIs.

    @param[in] dwArg   number of parameters
    @param[in] pszArg  testing parameters

    @par Dependencies
    None

    @par Side Effects
    None

    @retval returns TF_RESULT_CODE_BAD_PARAM on Bad Input Param
			returns TF_RESULT_CODE_SUCCESS on Success
            returns TF_RESULT_CODE_FAILURE on Failure

    @sa None
*/



#define NUM_ITER_WL_TEST   5

// identical functions with static cnt, for simplicity
DALResult workLoopFcn1( DALSYSEventHandle hEvent, VOID * pCtxt )
{
   static UINT32 cnt;

   DALSYSEventHandle hEventToTrig = (DALSYSEventHandle)pCtxt;
   AsciiPrint(PRINTSIG"In Workloop1");
   // this should trigger workloop2
   DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER);
   if(++cnt == NUM_ITER_WL_TEST)
   {
         return DAL_WORK_LOOP_EXIT;    //exit workloop
   }
   return DAL_SUCCESS;
}

DALResult workLoopFcn2( DALSYSEventHandle hEvent, VOID * pCtxt )
{
   static UINT32 cnt;

   DALSYSEventHandle hEventToTrig = (DALSYSEventHandle)pCtxt;
   AsciiPrint(PRINTSIG"In Workloop2");
   // this should trigger main thread
   // note main thread can only destroy events and workloop
   // once it receives this signal
   DALSYS_EventCtrl(hEventToTrig, DALSYS_EVENT_CTRL_TRIGGER);
   if(++cnt == NUM_ITER_WL_TEST)
   {
         return DAL_WORK_LOOP_EXIT;    //exit workloop
   }
   return DAL_SUCCESS;
}

UINT32 TestDalsysWorkloop(UINT32 dwArg, CHAR8* pszArg[])
{
   // static char* WLName1 = "DALTF_DALSYS_WorkLoop_WL1";
   // static char* WLName2 = "DALTF_DALSYS_WorkLoop_WL2";
   DALSYSWorkLoopHandle hWorkLoop1;
   DALSYSWorkLoopHandle hWorkLoop2;
   DALSYSEventHandle hEvent;
   DALSYSEventHandle hWLEvent1;
   DALSYSEventHandle hWLEvent2;
   //char * endptr;
   int i;
   UINT32 iRet;

   // Send to log
   AsciiPrint(PRINTSIG"TestDalsysWorkloop");

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                                        &hEvent,
                                        NULL);
   if(DAL_SUCCESS != iRet) {
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed,returned %d\n ", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                        &hWLEvent1,
                                        NULL);
    if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed,returned %d\n ", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                                        &hWLEvent2,
                                        NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_EventCreate failed, returned %d \n", iRet);
      DALSYS_DestroyObject(hWLEvent1);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   // Normal priority and up to 10 unique events in queue
   iRet = DALSYS_RegisterWorkLoop(DWPRIORITY,
								  DWMAXNUMEVENTS,
								  &hWorkLoop1,
								  NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_RegisterWorkloop 1 failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWLEvent1);
      DALSYS_DestroyObject(hWLEvent2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_RegisterWorkLoop(DWPRIORITY,
								  DWMAXNUMEVENTS,
								  &hWorkLoop2,
								  NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_RegisterWorkloop 2 failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWLEvent1);
      DALSYS_DestroyObject(hWLEvent2);
      DALSYS_DestroyObject(hWorkLoop1);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_AddEventToWorkLoop(hWorkLoop1,
                    workLoopFcn1,
                    (VOID *)hWLEvent2,
                    hWLEvent1,
                    NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop 1 failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWLEvent1);
      DALSYS_DestroyObject(hWLEvent2);
      DALSYS_DestroyObject(hWorkLoop1);
      DALSYS_DestroyObject(hWorkLoop2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   iRet = DALSYS_AddEventToWorkLoop(hWorkLoop2,
                    workLoopFcn2,
                    (VOID *)hEvent,
                    hWLEvent2,
                    NULL);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_AddEventToWorkLoop 2 failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWLEvent1);
      DALSYS_DestroyObject(hWLEvent2);
      DALSYS_DestroyObject(hWorkLoop1);
      DALSYS_DestroyObject(hWorkLoop2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }

   for(i = 0; i< NUM_ITER_WL_TEST; i++)
   {
      iRet = DALSYS_EventCtrl(hWLEvent1, DALSYS_EVENT_CTRL_TRIGGER);
      if(DAL_SUCCESS != iRet){
         AsciiPrint(PRINTSIG"DALSYS_EventCtrl failed, returned %d\n ", iRet);
         DALSYS_DestroyObject(hWLEvent1);
         DALSYS_DestroyObject(hWLEvent2);
         DALSYS_DestroyObject(hWorkLoop1);
         DALSYS_DestroyObject(hWorkLoop2);
         DALSYS_DestroyObject(hEvent);
         return TF_RESULT_CODE_FAILURE;
      }
	  iRet = DALSYS_EventWait(hEvent);
      if(DAL_SUCCESS != iRet){
         AsciiPrint(PRINTSIG"DALSYS_EventWait failed, returned %d\n ", iRet);
         DALSYS_DestroyObject(hWLEvent1);
         DALSYS_DestroyObject(hWLEvent2);
         DALSYS_DestroyObject(hWorkLoop1);
         DALSYS_DestroyObject(hWorkLoop2);
         DALSYS_DestroyObject(hEvent);
         return TF_RESULT_CODE_FAILURE;
      }
   }
    AsciiPrint(PRINTSIG"Test cleaning up");
   iRet = DALSYS_DestroyObject(hWLEvent1);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWLEvent2);
      DALSYS_DestroyObject(hWorkLoop1);
      DALSYS_DestroyObject(hWorkLoop2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hWLEvent2);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWorkLoop1);
      DALSYS_DestroyObject(hWorkLoop2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hWorkLoop1);
   if(DAL_SUCCESS !=  iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hWorkLoop2);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   iRet = DALSYS_DestroyObject(hWorkLoop2);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n ", iRet);
      DALSYS_DestroyObject(hEvent);
      return TF_RESULT_CODE_FAILURE;
   }
   iRet =  DALSYS_DestroyObject(hEvent);
   if(DAL_SUCCESS != iRet){
      AsciiPrint(PRINTSIG"DALSYS_DestroyObject failed, returned %d\n ", iRet);
      return TF_RESULT_CODE_FAILURE;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// Dalsys Test's Help Data
CONST TF_HelpDescr TestDalsysWorkloopHelp =
{
    "TestDalsysWorkloop",
    sizeof(DalsysTestParam) / sizeof(TF_ParamDescr),
    DalsysTestParam
};

CONST TF_TestFunction_Descr TestDalsysWorkloopTest =
{
   PRINTSIG"TestDalsysWorkloop",
   TestDalsysWorkloop,
   &TestDalsysWorkloopHelp,
   &context1
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_tests
===========================================================================*/

const TF_TestFunction_Descr* kernel_dal_daltf_dalsys_tests[] = {
  &TestDalsysSyncTest,
  &TestDalsysMemObjs1Test,
  &TestDalsysMemObjs2Test,
  &TestDalsysEvents1Test,
  &TestDalsysEvents2Test,
  &TestDalsysEvents3Test,
  &TestDalsysEvents4Test,
  &TestDalsysEvents5Test,
  &TestDalsysCallbackEventsTest,
  &TestDalsysTimerEventsTest,
  &TestDalsysWorkloopTest,
};

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_init_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_init_all(VOID)
{
  DALResult dalResult;

  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_tests) / sizeof(kernel_dal_daltf_dalsys_tests[0]));

  dalResult = tests_daltf_add_tests(kernel_dal_daltf_dalsys_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  kernel_dal_daltf_dalsys_deinit_all
===========================================================================*/
/**
  See documentation in header file
*/
DALResult kernel_dal_daltf_dalsys_deinit_all(VOID)
{
  DALResult dalResult;
  UINT16 TF_NumberTests =
    (sizeof(kernel_dal_daltf_dalsys_tests) / sizeof(kernel_dal_daltf_dalsys_tests[0]));

  dalResult = tests_daltf_remove_tests(kernel_dal_daltf_dalsys_tests, TF_NumberTests);
  return dalResult;
}