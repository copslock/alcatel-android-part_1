#ifndef _PMIC_DALTF_RTC_TEST_
#define _PMIC_DALTF_RTC_TEST_

#define FOR_8974
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_rtc_tests.h
@brief        System Drivers Subsystem RTC driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_rtc_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#ifdef USE_DIAG
#include "diagcmd.h"
#endif
#ifdef USE_STD_C_LIB
#include <string.h>
#endif
#ifdef USE_COMMON
#include "customer.h"
#endif
#include "DDITF.h"
#include "tests_daltf_common.h"   /* Common definitions*/
#include <Library/UefiLib.h>

DALResult pmic_daltf_init_rtc(VOID);
DALResult pmic_daltf_deinit_rtc(VOID);
//UINT32 atoint (CHAR8 *st);

#endif // _PMIC_DALTF_RTC_TEST_

