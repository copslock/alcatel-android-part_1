#ifndef _PMIC_DALTF_RGB_TEST_
#define _PMIC_DALTF_RGB_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_rgb_tests.h
@brief        System Drivers Subsystem RGB driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_rgb_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicRgbSetVoltageSource (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicRgbEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicRgbEnableAtc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicRgbL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_rgb(VOID);
#endif // _PMIC_DALTF_RGB_TEST_

