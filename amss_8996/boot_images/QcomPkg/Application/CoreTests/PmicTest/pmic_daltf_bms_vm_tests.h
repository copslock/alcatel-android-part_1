#ifndef _PMIC_DALTF_BMS_VM_TEST_
#define _PMIC_DALTF_BMS_VM_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_bms_tests.h
@brief        System Drivers Subsystem BMS driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_bms_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "pm_uefi_lib_err.h"

UINT32 pmicBmsvmReadOutputData (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetOcvThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetVseneseThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetCcIntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetSwccIntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetCntThr (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetOcvThrEn (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetStateDlyCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmEnOcvUpdateAndStopBmsUpdate (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmEnBmsAndCcReset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetPonBattAndSocAndAvg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetOverride (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetErrTol (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetAveSampleCtl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetIrq (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetRsense (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetLastSoc (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetRtcSec (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetFormatVersion (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetFabId (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmSetAndGetLastGoodOcv (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicBmsvmL4 (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_bms_vm(VOID);
#endif

