#ifndef _PMIC_DALTF_PAM_TEST_
#define _PMIC_DALTF_PAM_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*============================================================================
  @file         pmic_daltf_pam_tests.h

  @brief        PAM pmic test file

                Copyright (c) 2013 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
 
============================================================================*/

#ifdef USE_DIAG
#include "diagcmd.h"
#endif
#include "tests_daltf_common.h"   /* Common definitions*/
#include "DDIChipInfo.h"
#include "DALDeviceId.h"

DALResult pmic_daltf_init_pam(VOID);
DALResult pmic_daltf_deinit_pam(VOID);

#endif




