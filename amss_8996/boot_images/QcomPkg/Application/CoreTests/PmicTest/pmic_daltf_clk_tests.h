#ifndef _PMIC_DALTF_CLK_TEST_
#define _PMIC_DALTF_CLK_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_clk_buff_tests.h
@brief        System Drivers Subsystem CLK_BUFF driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_clk_buff_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicClkDriveStrength (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkPinCtrlEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkPullDown (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicClkSetOutClkDiv (UINT32 dwArg, CHAR8* pszArg[]);

UINT32 pmclksleepsmpl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmclksleepcalrcenable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmclksleepcoincellvalid (UINT32 dwArg, CHAR8* pszArg[]);

UINT32 pmclkxomode (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmclkxotrim (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_clk(VOID);
#endif

