#ifndef _PMIC_DALTF_MPPS_TEST_
#define _PMIC_DALTF_MPPS_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_mpps_tests.h
@brief        System Drivers Subsystem MPPS driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_mpps_tests.c module.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicMppsStatusGet (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigDigitalInput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigDigitalOutput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigDigitalInOut (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigDtestOutput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigAnalogInput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigAnalogOutput (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigISink (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicMppsConfigATest (UINT32 dwArg, CHAR8* pszArg[]);
//UINT32 pmicMppsSetandGetListWithShuntCap (UINT32 dwArg, CHAR8* pszArg[]);
DALResult pmic_daltf_init_mpps(VOID);
UINT32 pmicMppsL4 (UINT32 dwArg, CHAR8* pszArg[]);

#endif // _PMIC_DALTF_MPPS_TEST_

