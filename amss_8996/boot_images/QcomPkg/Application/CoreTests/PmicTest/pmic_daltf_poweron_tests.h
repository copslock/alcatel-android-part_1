#ifndef _PMIC_DALTF_POWERON_TEST_
#define _PMIC_DALTF_POWERON_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_poweron_tests.h
@brief        System Drivers Subsystem POWERON driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_poweron_tests.c module.


==========================================================================*/

#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "pm_uefi_lib_err.h"
UINT32 pmicPoweronHardResetEnable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPoweronHardResetAutopwrup (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPoweronGetPoweronTrigger (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPoweronBatteryRemovalStatus (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPoweronConfigPonTrigger (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicPoweronWdogFeature (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_poweron(VOID);
#endif

