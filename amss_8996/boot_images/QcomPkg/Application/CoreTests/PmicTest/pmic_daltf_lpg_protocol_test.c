/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/
/*=========================================================================

@file         pmic_daltf_lpg_protocol_test.c
@brief        PMIC Subsystem LPG protocol level DAL-TF test

GENERAL DESCRIPTION
  This file contains the common code for testing the APIs in LPG protocol.


==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR LPG PROTOCOL TESTS

==========================================================================*/
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "pmic_daltf_lpg_protocol_test.h"
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

#include <Protocol/EFIPmicVersion.h>
#include <Protocol/EFIPmicLpg.h>

#include "com_dtypes.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
STATIC UINT32 pmicLpgProtocolLpgConfig (UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol);
STATIC UINT32 pmicLpgProtocolLpgSetPWMValue (UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol);
STATIC UINT32 pmicLpgProtocolLpgPwmEnable (UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol);
///////////////////////////////////////////////////////////////////////////////////////////////////////

UINT16 PWM_Values[]={
						0x0000, 
						0x0001,
						0X000A,
						0x0102,
						0x001F,
						0xFFFE,
						0xFFFF, // always add extra values before this value
					};
UINT8 PWM_Values_size = sizeof(PWM_Values)/sizeof(PWM_Values[0]);
					
CONST TF_ParamDescr pmicLpgProtocolTestParam[] =
{
    {TF_PARAM_UINT32, "device index [0]", "[0]"},
};

CONST TF_HelpDescr pmicLpgProtocolTestHelp =
{
   "pmicLpgProtocolTest",
    sizeof(pmicLpgProtocolTestParam) / sizeof(TF_ParamDescr),
    pmicLpgProtocolTestParam
};

CONST TF_TestFunction_Descr pmicLpgProtocol_Test =
{
   PRINTSIG"pmicLpgProtocol_Test",
   pmicLpgProtocolTest,
   &pmicLpgProtocolTestHelp,
   &context1,
	0
};


UINT32 pmicLpgProtocolTest(UINT32 dwArg, CHAR8* pszArg[])
{
   AsciiPrint(PRINTSIG"LPG Protocol TEST\n");
   
   EFI_STATUS  Status = EFI_SUCCESS;
   EFI_QCOM_PMIC_LPG_PROTOCOL           *PmicLpgProtocol = NULL;
   DALResult ret1,ret2,ret3;
   
   UINT32 deviceIndex = 0;
   // EFI_PM_MPP_WHICH_TYPE mpp_type;
   
   if (dwArg < 1) {
      AsciiPrint(PRINTSIG"Invalid Params");
      return TF_RESULT_CODE_FAILURE;
   }
   //deviceIndex = atoi (pszArg[0]);   
   deviceIndex = atoint (pszArg[0]); 
   
   Status = gBS->LocateProtocol( &gQcomPmicLpgProtocolGuid,
								 NULL,
								(VOID **)&PmicLpgProtocol
	);

   if (Status != EFI_SUCCESS){
		AsciiPrint(PRINTSIG"LPG Protocol not present\n");
		return TF_RESULT_CODE_FAILURE;
    }
	
    ret1 = pmicLpgProtocolLpgConfig (deviceIndex, PmicLpgProtocol);
    ret2 = pmicLpgProtocolLpgSetPWMValue (deviceIndex, PmicLpgProtocol);
    ret3 = pmicLpgProtocolLpgPwmEnable (deviceIndex, PmicLpgProtocol);
    
	
	if (ret1 != TF_RESULT_CODE_SUCCESS || ret2 != TF_RESULT_CODE_SUCCESS || ret3 != TF_RESULT_CODE_SUCCESS){
		AsciiPrint(PRINTSIG"ret1:%d, ret2:%d, ret3:%d\n",ret1,ret2,ret3);
		return TF_RESULT_CODE_FAILURE;
	}
	return TF_RESULT_CODE_SUCCESS;	
}


UINT32 pmicLpgProtocolLpgConfig(UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol)
{
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgConfig");
	
	EFI_PM_LPG_CHAN_TYPE           WhichLpg_index;
	// PWM_Values		               PwmValue_index;
	EFI_PM_LPG_PWM_PRE_DIVIDE_TYPE PreDiv_index;
	EFI_PM_LPG_PWM_FREQ_EXPO_TYPE  PreDivExponent_index;
	EFI_PM_LPG_PWM_CLOCK_TYPE      Clock_index;
	EFI_PM_LPG_PWM_BIT_SIZE_TYPE   Size_index;
	
	// EFI_PM_MPP_STATUS_TYPE					   MppStatus;
	// STATIC UINT32 test_attempt=0;
	
	EFI_STATUS  Status = EFI_SUCCESS;
	UINT32 errors = 0;
	
	// PwmValue_index = 0x1111;
	
	WhichLpg_index = EFI_PM_LPG_CHN_NONE;
	while (WhichLpg_index < EFI_PM_LPG_CHAN_INVALID){
		PreDiv_index = EFI_PM_LPG_PWM_PRE_DIV_1;
		while (PreDiv_index < EFI_PM_LPG_PWM_PRE_DIV_MAX){
			PreDivExponent_index = EFI_PM_LPG_PWM_FREQ_EXPO_0;
			while (PreDivExponent_index < EFI_PM_LPG_PWM_FREQ_EXPO_MAX){
				Clock_index = EFI_PM_LPG_PWM_CLOCK_OFF;
				while (Clock_index < EFI_PM_LPG_PWM_CLOCK_MAX){
					Size_index = EFI_PM_LPG_PWM_6BIT;
					while (Size_index < EFI_PM_LPG_PWM_SIZE_MAX){	
						
						UINT8 i=0;
						while (i < PWM_Values_size){ // PWM Values defined in array
						// AsciiPrint(PRINTSIG"size:%d,i=%d,pwm:%d\n",PWM_Values_size,i,PWM_Values[i]);
						// while (PWM_Values[i] < PWM_Values_size){ // PWM Values defined in array
							// if (PWM_Values[i] == 0xFFFF){ j=1;}
							// test_attempt++;
							Status = PmicLpgProtocol->LpgConfig(deviceIndex, (EFI_PM_LPG_CHAN_TYPE) WhichLpg_index, PWM_Values[i], (EFI_PM_LPG_PWM_PRE_DIVIDE_TYPE) PreDiv_index, (EFI_PM_LPG_PWM_FREQ_EXPO_TYPE) PreDivExponent_index, (EFI_PM_LPG_PWM_CLOCK_TYPE) Clock_index, (EFI_PM_LPG_PWM_BIT_SIZE_TYPE) Size_index);
							if (Status != EFI_SUCCESS){
								if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
									// AsciiPrint(PRINTSIG"Expected Failure on EFI_PM_LPG_CHN_NONE\n");
								}else{
									// AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgConfig-FAIL deviceIndex:%d,WhichLpg_index:%d,PWM_Values:%d,PreDiv_index:%d,PreDivExponent_index:%d,Clock_index:%d,Size_index:%d\n",
									// deviceIndex, WhichLpg_index,PWM_Values[i],PreDiv_index,PreDivExponent_index,Clock_index,Size_index);
									// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
									errors++;
								}
							}else{
								if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
									AsciiPrint(PRINTSIG"!!!!EFI_PM_LPG_CHN_NONE is PASSING which supposed to FAIL hence test FAILED!!!!");
									AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgConfig-FAIL deviceIndex:%d,WhichLpg_index:%d,PWM_Values:%d,PreDiv_index:%d,PreDivExponent_index:%d,Clock_index:%d,Size_index:%d\n",
									deviceIndex, WhichLpg_index,PWM_Values[i],PreDiv_index,PreDivExponent_index,Clock_index,Size_index);
									// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
									errors++;
								}else{
									// AsciiPrint(PRINTSIG">>>333333<<<\n");
								// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 		
								}
							}
							i++;
						}
						Size_index++;
						
					}
					Clock_index++;
				}
				PreDivExponent_index++;
			}
			PreDiv_index++;
		}
		WhichLpg_index++;		
	}
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicLpgProtocolLpgConfig TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgConfig TEST: failed\n");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicLpgProtocolLpgSetPWMValue(UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol)
{
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgSetPWMValue");
	
	EFI_PM_LPG_CHAN_TYPE WhichLpg_index;
    // UINT16               PwmValue_index=0x1111;
	
	// EFI_PM_MPP_STATUS_TYPE					   MppStatus;
	EFI_STATUS  Status = EFI_SUCCESS;
	
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	WhichLpg_index = EFI_PM_LPG_CHN_NONE;
	while (WhichLpg_index < EFI_PM_LPG_CHAN_INVALID){
		
		UINT8 i=0;
		while (i < PWM_Values_size){		
			// test_attempt++;
			Status = PmicLpgProtocol->LpgSetPWMValue(deviceIndex, WhichLpg_index, PWM_Values[i]);
			
			if (Status != EFI_SUCCESS){	
				if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
										// AsciiPrint(PRINTSIG"Expected Failure on EFI_PM_LPG_CHN_NONE\n");
				}else{
					AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgSetPWMValue-FAIL deviceIndex:%d,WhichLpg_index:%d,PwmValue:%d\n",deviceIndex,WhichLpg_index, PWM_Values[i]);
					// AsciiPrint(PRINTSIG"PmicLpgProtocol->ConfigDigitalOutput -fail attempt %d\n",test_attempt );
					// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
					errors++;						
				}
			}else{	
				if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
					AsciiPrint(PRINTSIG"!!!!EFI_PM_LPG_CHN_NONE is PASSING which supposed to FAIL hence test FAILED!!!!");
					AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgSetPWMValue-FAIL deviceIndex:%d,WhichLpg_index:%d,PwmValue:%d\n",deviceIndex,WhichLpg_index, PWM_Values[i]);
					// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
					errors++;
				}else{
					// AsciiPrint(PRINTSIG">>>333333<<<\n");
				// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 		
				}
			}
			i++;
		}
		WhichLpg_index++;
	}
	
	
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicLpgProtocolLpgSetPWMValue TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgSetPWMValue TEST: failed\n");
	return TF_RESULT_CODE_FAILURE;
}

UINT32 pmicLpgProtocolLpgPwmEnable(UINT32 deviceIndex, EFI_QCOM_PMIC_LPG_PROTOCOL *PmicLpgProtocol)
{
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgPwmEnable");
	
	EFI_PM_LPG_CHAN_TYPE  WhichLpg_index;
    BOOLEAN               Enable_index;
	
	// EFI_PM_MPP_STATUS_TYPE			   MppStatus;
	EFI_STATUS  Status =  EFI_SUCCESS;
	
	// STATIC UINT32 test_attempt=0;
	UINT32 errors = 0;
	
	WhichLpg_index = EFI_PM_LPG_CHN_NONE;
	while (WhichLpg_index < EFI_PM_LPG_CHAN_INVALID){
		Enable_index = 0;
		while (Enable_index < 2){
			// test_attempt++;
			Status = PmicLpgProtocol->LpgPwmEnable(deviceIndex, WhichLpg_index, Enable_index);
			if (Status != EFI_SUCCESS){
				if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
					// AsciiPrint(PRINTSIG"Expected Failure on EFI_PM_LPG_CHN_NONE\n");
				}else{
					AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgPwmEnable-FAIL deviceIndex:%d,WhichLpg_index:%d,Enable_index:%d\n",deviceIndex,WhichLpg_index, Enable_index);
					// AsciiPrint(PRINTSIG"PmicLpgProtocol->ConfigDigitalInOut -fail attempt %d\n",test_attempt );
					// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
					errors++;						
				}
			}else{				
				if (WhichLpg_index == EFI_PM_LPG_CHN_NONE){
					AsciiPrint(PRINTSIG"!!!!!EFI_PM_LPG_CHN_NONE is PASSING which supposed to FAIL hence test FAILED!!!!");
					AsciiPrint(PRINTSIG"PmicLpgProtocol->LpgPwmEnable-FAIL deviceIndex:%d,WhichLpg_index:%d,Enable_index:%d\n",deviceIndex,WhichLpg_index, Enable_index);
					// AsciiPrint(PRINTSIG"MPP Protocol not present\n");
					errors++;
				}else{
					// AsciiPrint(PRINTSIG">>>333333<<<\n");
					// AsciiPrint(PRINTSIG"Passed Input: EFI_PM_GPIO_INPUT_ON,%d,%d,%d,%d\n",ISourcePulls_index,VoltageSource_index,OutBufferStrength_index,Source_index); 		
				}
			}			
			Enable_index++;
		}
		WhichLpg_index++;
	}
				
	if (errors == 0) {
      AsciiPrint(PRINTSIG"pmicLpgProtocolLpgPwmEnable TEST: passed\n");
      return TF_RESULT_CODE_SUCCESS;
	}
	AsciiPrint(PRINTSIG"pmicLpgProtocolLpgPwmEnable TEST: failed\n");
	return TF_RESULT_CODE_FAILURE;
}

STATIC CONST TF_TestFunction_Descr* pmic_daltf_lpg_protocol_tests[] = {
   &pmicLpgProtocol_Test
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
DALResult pmic_daltf_init_lpgprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests = (sizeof(pmic_daltf_lpg_protocol_tests) / sizeof(pmic_daltf_lpg_protocol_tests[0]));
  dalResult = tests_daltf_add_tests(pmic_daltf_lpg_protocol_tests, TF_NumberTests);
  return dalResult;
}

/*===========================================================================
  FUNCTION:  pmic_daltf_deinit_lpgprotocol
===========================================================================*/

DALResult pmic_daltf_deinit_lpgprotocol(VOID)
{
  DALResult dalResult = 0;
  UINT16 TF_NumberTests =
    (sizeof(pmic_daltf_lpg_protocol_tests) / sizeof(pmic_daltf_lpg_protocol_tests[0]));

  dalResult = tests_daltf_remove_tests(pmic_daltf_lpg_protocol_tests, TF_NumberTests);
  return dalResult;
}
