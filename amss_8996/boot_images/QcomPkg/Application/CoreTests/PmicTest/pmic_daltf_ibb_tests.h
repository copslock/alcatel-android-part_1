#ifndef _PMIC_DALTF_IBB_TEST_
#define _PMIC_DALTF_IBB_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_ibb_tests.h
@brief        System Drivers Subsystem IBB driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_ibb_tests.c module.


==========================================================================*/


#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicIbbLcdAmoledSel (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicIbbModuleRdy (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicIbbConfigIbbCtrl (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicIbbSetSoftChgrResistor (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicIbbSetSwireOutputPulse (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmicIbbConfigOutputVolt (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_ibb(VOID);
#endif

