#ifndef _PMIC_DALTF_GPIO_PROTOCOL_TEST_
#define _PMIC_DALTF_GPIO_PROTOCOL_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_gpio_protocol_test.h
@brief        System Drivers Subsystem GPIO PROTOCOL DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_gpio_protocol_test.c module.


==========================================================================*/


/*==========================================================================

                     INCLUDE FILES COMMON FOR ALL THE TESTS

==========================================================================*/
#ifdef USE_DIAG
#include "diagcmd.h"
#endif
#ifdef USE_STD_C_LIB
#include <string.h>
#endif
#ifdef USE_COMMON
#include "customer.h"
#endif
#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmicGpioProtocolTest(UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_gpioprotocol(VOID);
DALResult pmic_daltf_deinit_gpioprotocol(VOID);
//int atoi (CONST CHAR8 *st);
//extern CONST TF_ContextDescr context1;

#endif // _PMIC_DALTF_GPIO_PROTOCOL_TEST_

