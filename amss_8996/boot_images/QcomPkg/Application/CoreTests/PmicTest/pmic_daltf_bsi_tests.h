#ifndef _PMIC_DALTF_BSI_TEST_
#define _PMIC_DALTF_BSI_TEST_
/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------

================================================================================*/

/*=========================================================================

@file         pmic_daltf_bsi_tests.h
@brief        System Drivers Subsystem BSI driver DAL-TF test cases header file

GENERAL DESCRIPTION
  This header file contains the public APIs for the pmic_daltf_bsi_tests.c module.


==========================================================================*/


#include "DDITF.h"
#include "tests_daltf_common.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include <Include/api/systemdrivers/pmic/pm_err_flags.h>

UINT32 pmbsienable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsisettaucfg (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsisetrxdataformat (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsisettxdataformat (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsirxenable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsitxenable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsireadrxdata (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsiwritetxdata (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsitxgo (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsisetsamplingrate (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsiclearerror (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsisetbclforcelow (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsipullupenable (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsifsmreset (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsicfgtxdly (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsigetrxfifoword (UINT32 dwArg, CHAR8* pszArg[]);
UINT32 pmbsibatgonecfg (UINT32 dwArg, CHAR8* pszArg[]);

DALResult pmic_daltf_init_bsi(VOID);
#endif


  
