/*===============================================================================
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  
  FILE: 		
  DESCRIPTION:	
  
  REVISION HISTORY
  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
#  02/13/15   nc      Changes to account for new DAL-TF command handler
#  01/23/15   nc      Changes to fix PMIC Test compilation errors  
#  11/03/14   nc      Changed/commented out included files for PMIC test application
================================================================================*/

/** @file PmicTest.c
   
  This file contains test clock driver entry point.  On UEFI,
  there are two different ways to use clock driver: (1) DAL 
  framework (2) EFI clock protocol.  This application shows 
  sample code for both options. 
 
**/

/*=========================================================================
      Include Files
==========================================================================*/

#include "tests_daltf_common.h"   /* Common definitions */
#include "pmic_daltf_rtc_tests.h"

/*#include "pmic_daltf_gpio_tests.h"
#include "pmic_daltf_mpps_tests.h"
#include "pmic_daltf_wled_tests.h"
#include "pmic_daltf_rgb_tests.h"*/
//#include "pmic_daltf_vib_tests.h"
// #include "pmic_daltf_bms_tests.h" //deprecated from 8916 onwards. Also 8994
// #include "pmic_daltf_smbb_tests.h" //deprecated from 8916 onwards. Also 8994
/*#include "pmic_daltf_lpg_tests.h"
#include "pmic_daltf_pwr_tests.h"
#include "pmic_daltf_clk_tests.h"
#include "pmic_daltf_ibb_tests.h"
#include "pmic_daltf_lab_tests.h"*/
//#include "pmic_daltf_bsi_tests.h"
//#include "pmic_daltf_smbchg_tests.h"
// #include "pmic_daltf_lbc_tests.h"
// #include "pmic_daltf_clk_buff_tests.h"
//#include "pmic_daltf_pam_tests.h"

#include "pmic_daltf_pmversion_protocol_test.h"
#include "pmic_daltf_gpio_protocol_test.h"
#include "pmic_daltf_mpp_protocol_test.h"
#include "pmic_daltf_lpg_protocol_test.h"
#include "pmic_daltf_clkbuff_protocol_test.h"
//#include "pmic_daltf_vib_protocol_test.h"
#include "pmic_daltf_rgbled_protocol_test.h"
// #include "pmic_daltf_wled_protocol_test.h" //API changes in uefi3.0
//#include "pmic_daltf_pwm_protocol_test.h"
#include "pmic_daltf_rtc_protocol_test.h"
// #include "pmic_daltf_mipibif_protocol_test.h" //API changes in uefi3.0


/* ============================================================================
**  Function : PmicInitTestFunc
** ========================================================================== */
/*!
   @brief
   An init DalTF test case.  Add driver Pmic tests into DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 PmicInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Add Pmic tests\n");
   
   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
	  /*
    	  if ( DAL_SUCCESS != pmic_daltf_init_rtc())
    	  {
    	    AsciiPrint(PRINTSIG"pmic_daltf_init_rtc failed\n");
    	    return 1;
    	  }

		
    	  if ( DAL_SUCCESS != pmic_daltf_init_gpio())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_gpio failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != pmic_daltf_init_mpps())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_mpps failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_init_wled())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_wled failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_init_rgb())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_rgb failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_init_lpg())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_lpg failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_pwr())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_pwr failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_clk())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_clk failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_ibb())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_ibb failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_lab())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_lab failed\n");
    	     return 1;
    	  }
		  */
		   if ( DAL_SUCCESS != pmic_daltf_init_pmicversionprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_pmicversionprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_gpioprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_gpioprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_mppprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_mppprotocol failed\n");
    	     return 1;
    	  }
		   if ( DAL_SUCCESS != pmic_daltf_init_lpgprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_lpgprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_clkbuffprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_clkbuffprotocol failed\n");
    	     return 1;
    	  }
		  /*		  
		   if ( DAL_SUCCESS != pmic_daltf_init_rgbledprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_rgb failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_init_pwmprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_pwmprotocol failed\n");
    	     return 1;
    	  }
		  */
		   if ( DAL_SUCCESS != pmic_daltf_init_rtcprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_init_rtcprotocol failed\n");
    	     return 1;
    	  }
		  
      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// Pmic init test's Parameters
static const TF_ParamDescr PmicInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - add all Pmic test"}
};
// Pmic Init Test's Help Data
static const TF_HelpDescr PmicInitTestHelp =
{
    "Pmic Init",
    sizeof(PmicInitTestParam) / sizeof(TF_ParamDescr),
    PmicInitTestParam
};


const TF_TestFunction_Descr PmicInitTest =
{
   PRINTSIG"PmicInitTest",
   PmicInitTestFunc,
   &PmicInitTestHelp,
   &context1
};

/* ============================================================================
**  Function : PmicDeInitTestFunc
** ========================================================================== */
/*!
   @brief
   A de-init DalTF test case.  Remove driver Pmic tests from DALTF.

   @param dwArg      - [IN] # parameters
   @param pszArg     - [IN] testing parameters

   @par Dependencies
   None

   @par Side Effects
   None

   @return

*/
static uint32 PmicDeInitTestFunc(uint32 dwArg, char* pszArg[])
{
   AsciiPrint(PRINTSIG"Remove Pmic tests\n");

   if(1 == dwArg)
   {
      if(0 == AsciiStrnCmp("all", pszArg[0], 3))
      {
		/*
    	  if ( DAL_SUCCESS != pmic_daltf_deinit_rtc())
    	  {
    	    AsciiPrint(PRINTSIG"pmic_daltf_deinit_rtc failed\n");
    	    return 1;
    	  }

		
    	  if ( DAL_SUCCESS != pmic_daltf_deinit_gpio())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_gpio failed\n");
    	     return 1;
    	  }

    	  if ( DAL_SUCCESS != pmic_daltf_deinit_mpps())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_mpps failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_deinit_wled())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_wled failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_deinit_rgb())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_rgb failed\n");
    	     return 1;
    	  }
		  
		  if ( DAL_SUCCESS != pmic_daltf_deinit_lpg())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_lpg failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_pwr())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_pwr failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_clk())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_clk failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_ibb())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_ibb failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_lab())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_lab failed\n");
    	     return 1;
    	  }
		  */
		   if ( DAL_SUCCESS != pmic_daltf_deinit_pmicversionprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_pmicversionprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_gpioprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_gpioprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_mppprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_mppprotocol failed\n");
    	     return 1;
    	  }
		   if ( DAL_SUCCESS != pmic_daltf_deinit_lpgprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_lpgprotocol failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_clkbuffprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_clkbuffprotocol failed\n");
    	     return 1;
    	  }
		  /*		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_rgbledprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_rgb failed\n");
    	     return 1;
    	  }
		  
		   if ( DAL_SUCCESS != pmic_daltf_deinit_pwmprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_pwmprotocol failed\n");
    	     return 1;
    	  }
		  */
		   if ( DAL_SUCCESS != pmic_daltf_deinit_rtcprotocol())
    	  {
    	     AsciiPrint(PRINTSIG"pmic_daltf_deinit_rtcprotocol failed\n");
    	     return 1;
    	  }
		
      }
      else
      {
          return TF_RESULT_CODE_BAD_PARAM;
      }
   }
   else
   {
      return TF_RESULT_CODE_BAD_PARAM;
   }

   return TF_RESULT_CODE_SUCCESS;
}

// Pmic deinit test's Parameters
static const TF_ParamDescr PmicDeInitTestParam[] =
{
    {TF_PARAM_STRING, "all", "all - remove all Pmic test"}
};
// PmicDeInitTest's Help Data
static const TF_HelpDescr PmicDeInitTestHelp =
{
    "Sanity De-init",
    sizeof(PmicDeInitTestParam) / sizeof(TF_ParamDescr),
    PmicDeInitTestParam
};

const TF_TestFunction_Descr PmicDeInitTest =
{
   PRINTSIG"PmicDeInitTest",
   PmicDeInitTestFunc,
   &PmicDeInitTestHelp,
   &context1
};


