/** @file ULogTestApp.c
   
  Tests for ULog.

  Copyright (c) 2013-2015 by Qualcomm Technologies Inc.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 10/02/14   vpopuri Merge with ULOGv5
 08/20/14   sj      KW Fixes
 06/03/14   vpopuri Temporarily skipping this for 64bit, until parser is fixed
 05/14/14   sj      64 bit compilation error fix
 05/01/13   rli     Initial revision.
.
=============================================================================*/

#include <Uefi.h>
#include <Library/UefiLib.h>
#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/TestInterface.h>

#include "ULog.h"
#include "npa_log.h"
#include <Library/MemoryAllocationLib.h>


/**
 * <!-- ULT_ReadLogMessage -->
 *
 * @brief A test helper function to read out a log message and format as ASCII. 
 *
 * @param readHandle : The log to read
 * @param outputString : Pointer to a character buffer
 * @param outputStringSize : Size of the outputString buffer 
 */
void ULT_ReadLogMessage(ULogHandle readHandle, char * outputString, uint32 outputStringSize)
{
  uint32 outCount;
  char outMem[1024];

  ULogCore_ReadEx(readHandle, sizeof(outMem), outMem, &outCount, 1);
  if (outCount > 4)
  {
    ULogCore_MsgFormat(readHandle, &outMem[4], outputString, outputStringSize, &outCount);
  }
  else
  {
    outputString[0] = 0;
  }
}

/**
  The user Entry Point for Application. The user code starts with this function
  as the real entry point for the application.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/

#define ULOG_TESTAPP_BYTES_TO_READ 4096

EFI_STATUS
EFIAPI
UefiMain (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS  Status = EFI_NOT_READY;
  ULogHandle h;
  ULogResult ures;
  char *outputData;

  outputData = (char *)AllocateZeroPool(ULOG_TESTAPP_BYTES_TO_READ*sizeof(char));
  if (NULL == outputData)
  {
    return EFI_OUT_OF_RESOURCES;
  }

  TEST_START("ULOG TEST");
  ULogCore_Connect(&h, NPA_DEFAULT_LOG_NAME);
  if (NULL != h)
  {
    ures = ULogCore_Enable(h);
    if( ULOG_ERR_ALREADYENABLED == ures)
    {
            AsciiPrint("ULOG SUCCESS: enabled\n");
            if ((NULL != outputData) && (NULL != h))
            {
              ULT_ReadLogMessage(h, outputData, ULOG_TESTAPP_BYTES_TO_READ*sizeof(char));
              AsciiPrint(outputData);
              Status = EFI_SUCCESS;
            }
    }
    else
    {
            AsciiPrint("ULOG FAILED: %d.\n", ures);
            Status = EFI_NOT_READY;
    }
  }
  else
  {
    Status = EFI_NOT_READY; //Setting an arbitrary value instead of mapping every err code - as test code
  }
  
  FreePool( outputData );
  TestStatus("ULOG TEST", Status);
  TEST_STOP("ULOG TEST");

  return Status;
}
