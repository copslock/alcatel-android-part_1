/** @file DALTFTest.c
   
  This file contains test framework driver entry point.

  Copyright (c) 2015, Qualcomm Technologies Inc. All rights reserved.
  
**/

/*=============================================================================
                              EDIT HISTORY

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 03/18/11   jz     Created.

=============================================================================*/



/*=========================================================================
      Include Files
==========================================================================*/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/TestInterface.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/EFIDALTF.h>


/*=========================================================================
      Functions
==========================================================================*/

/**
  Test DALTF application entry point.

  @param[in] ImageHandle    The firmware allocated handle for the EFI image.  
  @param[in] SystemTable    A pointer to the EFI System Table.
  
  @retval EFI_SUCCESS       The entry point is executed successfully.
  @retval other             Some error occurs when executing this entry point.

**/

EFI_STATUS
EFIAPI
DALTFTestMain (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
  )
{
  EFI_STATUS          eResult;
  EFI_DALTF_PROTOCOL  *DALTFProtocol;
  EFI_LOADED_IMAGE_PROTOCOL *ImageInfo;
  CHAR8* request;

  /* get the loaded image protocol */
  eResult = SystemTable->BootServices->HandleProtocol(
  				ImageHandle,
  				&gEfiLoadedImageProtocolGuid,
  				(VOID **)&ImageInfo);

  ASSERT_EFI_ERROR(eResult);

  eResult = gBS->LocateProtocol (
                  &gEfiDaltfProtocolGuid,
                  NULL,
                  (VOID **) &DALTFProtocol
                  );
  ASSERT_EFI_ERROR(eResult);

  /* process the command line options */
  if ( ImageInfo->LoadOptionsSize == 0 ){
      AsciiPrint("Need to provide command as an argument\n");
      return 1;
  }

  request = (CHAR8*)ImageInfo->LoadOptions;

  TEST_START("CORE");

  // AsciiPrint("\n======================================\n");
  AsciiPrint("%a:\n", request);
  // AsciiPrint("========================================\n");

  eResult = DALTFProtocol->HandleCommand(request);

  if (eResult != EFI_SUCCESS){
    AsciiPrint("WARNING: command %a return error: %d\n", request, eResult);
    AsciiPrint("\n");
  }
  TEST_STOP("CORE");

  return EFI_SUCCESS;
}

