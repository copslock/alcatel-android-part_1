/** @file ClockUEFI.c

  This file implements specialized image specific functions such
  as init image, enable DCVS, etc. In case of UEFI, we have few
  place holders for now.

  Copyright (c) 2010-2015, Qualcomm Technologies, Inc.
                   All rights reserved.
                 QUALCOMM Proprietary/GTDR

**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 05/30/14   sj      Fixed 64 bit compilation errors
 05/08/14   sr      Ported to 8994.
 08/09/12   sr      Ported to 8974.
 12/23/10   vs      Created.

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "DDIClock.h"
#include "Drivers/ClockDxe/ClockDriver.h"
#include "ClockApps.h"
#include "ClockAVS.h"

#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Externals
==========================================================================*/

extern void HAL_clk_MemCoreSave (void);
extern void HAL_clk_MemCoreRestore (void);
extern void HAL_clk_NocDcdEnable (void);
extern void HAL_clk_NocDcdDisable (void);


/*=========================================================================
      Prototypes
==========================================================================*/

void Clock_VDDCXNodeReadyCallback(void *pContext, unsigned int nEventType, void *pNodeName, unsigned int  nNodeNameSize);
void Clock_VDDMXNodeReadyCallback(void *pContext, unsigned int nEventType, void *pNodeName, unsigned int  nNodeNameSize);


/*=========================================================================
      Type Definitions
==========================================================================*/

/**
 * UEFI Clock Driver local context.
 */
typedef struct
{
  boolean        bLowPowerMode;
  boolean        bClusterH1InReset;
  ClockNodeType *ClusterH0Id;
  ClockNodeType *ClusterH1Id;
  ClockNodeType *CbfId;
  ClockNodeType *MDSSMDPClockId;
} ClockLocalCtxtType;


/**
 * UEFI Clock Driver NPA context.
 */
typedef struct
{
  boolean                         bDefineResource;
  npa_remote_resource_definition  Clock_NPARemoteResource;
  npa_callback                    fpCallBack;
  npa_client_handle               hClient;
  uint32                          nLowPowerValue;
  ClockDrvCtxt                   *pDrvCtxt;
} ClockNPACtxtType;


/*=========================================================================
      Data
==========================================================================*/

/*
 * Defined in DalClockFwk.c
 */
extern DALREG_DriverInfo DALClock_DriverInfo;

/*
 * gDALModDriverInfoList needs to be created for WM and UEFI - dynamic
 * loading of DAL drivers. This will not work for AMSS image (static loading
 * via DAL based XML files at compile time) so the following can't go in
 * DalClockFwk.c which is common across images
 *
 */
static DALREG_DriverInfo* DALDriverInfoArr[1] = { &DALClock_DriverInfo};
DALREG_DriverInfoList gDALModDriverInfoList= {1, DALDriverInfoArr};

/*-----------------------------------------------------------------------*/
/* Remote node definitions.                                              */
/*-----------------------------------------------------------------------*/

static ClockNPACtxtType Clock_aNPACtxt[] =
{
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/qdss",
      /* .remote_resource_name  = */ "clk0\x01\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "STATE",
      /* .max                   = */ 2,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL
    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/dcvs.ena",
      /* .remote_resource_name  = */ "clk0\x02\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_binary_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "Enable",
      /* .max                   = */ 1,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL
    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 1,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/pnoc",
      /* .remote_resource_name  = */ "clk1\x00\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 19200,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/snoc",
      /* .remote_resource_name  = */ "clk1\x01\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL
    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 19200,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/cnoc",
      /* .remote_resource_name  = */ "clk1\x02\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 19200,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/bimc",
      /* .remote_resource_name  = */ "clk2\x00\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 37500,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/ipa",
      /* .remote_resource_name  = */ "ipa\x00\x00\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/ce1",
      /* .remote_resource_name  = */ "ce\x00\x00\x00\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "KHz",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/agr0",
      /* .remote_resource_name  = */ "aggr\x00\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_binary_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "Enable",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL
    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/agr1",
      /* .remote_resource_name  = */ "aggr\x01\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_binary_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "Enable",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ TRUE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/clk/agr2",
      /* .remote_resource_name  = */ "aggr\x02\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_binary_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "Enable",
      /* .max                   = */ NPA_MAX_STATE,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ NULL,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ 0,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ FALSE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/pmic/client/rail_cx",
      /* .remote_resource_name  = */ "smpa\x01\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "ModeID",
      /* .max                   = */ CLOCK_VREG_LEVEL_HIGH,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL

    },
    /* .fpCallBack              = */ Clock_VDDCXNodeReadyCallback,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .pDrvCtxt                = */ NULL,
  },
  {
    /* .bDefineResource         = */ FALSE,
    { /* .Clock_NPARemoteResource */
      /* .local_resource_name   = */ "/pmic/client/rail_mx",
      /* .remote_resource_name  = */ "smpa\x02\x00\x00\x00",
      /* .protocol_type         = */ "/protocol/rpm/rpm",
      /* .plugin                = */ &npa_max_plugin,
      /* .driver_fcn            = */ npa_remote_resource_local_aggregation_driver_fcn,
      /* .units                 = */ "ModeID",
      /* .max                   = */ CLOCK_VREG_LEVEL_HIGH,
      /* .attributes            = */ 0,
      /* .data                  = */ NULL,
      /* .handle                = */ NULL
    },
    /* .fpCallBack              = */ Clock_VDDMXNodeReadyCallback,
    /* .hClient                 = */ NULL,
    /* .nLowPowerValue          = */ CLOCK_VREG_LEVEL_LOW,
    /* .pDrvCtxt                = */ NULL,
  },
};

static ClockSourceFreqConfigType HFPLLCfgCPUH0 =
{
  /*                |-.HALConfig------------------------------------------------------------------------------|                             |-.HWVersion----------------------------------------------------------------------------------|              */
  /* .nFreqHz,      {.eSource, .eVCO, .nPreDiv, .nPostDiv, .nL,  .nM, .nN, .nVCOMultiplier, .nAlpha, .nAlphaU } .eVRegLevel,                { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .bLastEntry */
   883200 * 1000UL, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 2,  92, 0, 0, 0, 0x00000000, 0x00 },              CLOCK_VREG_LEVEL_LOW,       { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   0
};

static ClockSourceFreqConfigType HFPLLCfgCPUS0 =
{
  /*                |-.HALConfig------------------------------------------------------------------------------|                             |-.HWVersion----------------------------------------------------------------------------------|              */
  /* .nFreqHz,      {.eSource, .eVCO, .nPreDiv, .nPostDiv, .nL,  .nM, .nN, .nVCOMultiplier, .nAlpha, .nAlphaU } .eVRegLevel,                { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .bLastEntry */
   787200 * 1000UL, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO3, 1, 1,  41, 0, 0, 0, 0x00000000, 0x00 },              CLOCK_VREG_LEVEL_LOW,       { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   0
};

static ClockSourceFreqConfigType HFPLLCfgCPUH1 =
{
  /*                |-.HALConfig------------------------------------------------------------------------------|                             |-.HWVersion----------------------------------------------------------------------------------|              */
  /* .nFreqHz,      {.eSource, .eVCO, .nPreDiv, .nPostDiv, .nL,  .nM, .nN, .nVCOMultiplier, .nAlpha, .nAlphaU } .eVRegLevel,                { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .bLastEntry */
  1017600 * 1000UL, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 2, 106, 0, 0, 0, 0x00000000, 0x00 },              CLOCK_VREG_LEVEL_LOW,       { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   0
};

static ClockSourceFreqConfigType HFPLLCfgCPUS1 =
{
  /*                |-.HALConfig------------------------------------------------------------------------------|                             |-.HWVersion----------------------------------------------------------------------------------|              */
  /* .nFreqHz,      {.eSource, .eVCO, .nPreDiv, .nPostDiv, .nL,  .nM, .nN, .nVCOMultiplier, .nAlpha, .nAlphaU } .eVRegLevel,                { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .bLastEntry */
   921600 * 1000UL, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO3, 1, 1,  48, 0, 0, 0, 0x00000000, 0x00 },              CLOCK_VREG_LEVEL_LOW,       { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   0
};

static ClockSourceFreqConfigType HFPLLCfgCBF =
{
  /*                |-.HALConfig------------------------------------------------------------------------------|                             |-.HWVersion----------------------------------------------------------------------------------|              */
  /* .nFreqHz,      {.eSource, .eVCO, .nPreDiv, .nPostDiv, .nL,  .nM, .nN, .nVCOMultiplier, .nAlpha, .nAlphaU } .eVRegLevel,                { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .bLastEntry */
   960000 * 1000UL, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO1, 1, 2, 100, 0, 0, 0, 0x00000000, 0x00 },              CLOCK_VREG_LEVEL_LOW,       { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   0
};

static ClockMuxConfigType MuxCfgHFCPUH0 =
{
  /*                |-.HALConfig-----------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D      }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  883200 * 1000UL,  { HAL_CLK_SOURCE_APCSPRIPLL0, 2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   &HFPLLCfgCPUH0
};

static ClockMuxConfigType MuxCfgHFCPUS0 =
{
  /*                |-.HALConfig-----------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D      }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  787200 * 1000UL,  { HAL_CLK_SOURCE_APCSSECPLL0, 2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   &HFPLLCfgCPUS0
};

static ClockMuxConfigType MuxCfgHFCPUH1 =
{
  /*                |-.HALConfig-----------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D      }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  1017600 * 1000UL, { HAL_CLK_SOURCE_APCSPRIPLL1, 2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   &HFPLLCfgCPUH1
};

static ClockMuxConfigType MuxCfgHFCPUS1 =
{
  /*                |-.HALConfig-----------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D      }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  921600 * 1000UL,  { HAL_CLK_SOURCE_APCSSECPLL1, 2, 0, 0, 0 }, CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   &HFPLLCfgCPUS1
};

static ClockMuxConfigType MuxCfgHFCBF =
{
  /*                |-.HALConfig-----------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D      }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  960000 * 1000UL,  { HAL_CLK_SOURCE_APCSCBFPLL, 2, 0, 0, 0  }, CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   &HFPLLCfgCBF
};

static ClockMuxConfigType MuxCfgLowPower =
{
  /*                |-.HALConfig--------------------------|                        |-.HWVersion----------------------------------------------------------------------------------|                     */
  /* .nFreqHz,      { .eSource, .nDiv2x, .nM, .nN, .n2D   }  .eVRegLevel,          { { .Min.nMajor, .Min.nMinor }, { .Max.nMajor, .Max.nMinor }, .eChipInfoFamily, .aeChipInfoId }, .pSourceFreqConfig */
  300000 * 1000UL,  { HAL_CLK_SOURCE_GPLL0, 4, 0, 0, 0 },    CLOCK_VREG_LEVEL_LOW, { { 0, 0 }, { 0xFF, 0xFF }, DALCHIPINFO_FAMILY_UNKNOWN, NULL},                                   NULL
};

ClockLocalCtxtType  ClockLocalCtxt;

#define MMNOCAXI_LOW_POWER_FREQUENCY  19200   // KHz
#define MMNOCAXI_DEFAULT_FREQUENCY    300000  // KHz

#define MDSSMDP_LOW_POWER_FREQUENCY   60000   // KHz
#define MDSSMDP_DEFAULT_FREQUENCY     320000  // KHz

/*
 * The voltage levels specified below were chosen to accommodate all parts of
 * every version. These values come from the characterized silicone and are
 * based on the Slow bin parts.
 */
#define CORE_LOWEST_VOLTAGE   900000    // @ 300 MHz - 0.9 V

#define CORE_NOMINAL_VOLTAGE  1000000   // @ varies MHz - 1 V


/*=========================================================================
      Functions
==========================================================================*/

/* =========================================================================
**  Function : Clock_VDDCXNodeReadyCallback
** =========================================================================*/

void Clock_VDDCXNodeReadyCallback(void *pContext, unsigned int nEventType, void *pNodeName, unsigned int nNodeNameSize)
{
  ClockNPACtxtType  *pClockNPACtxtType = (ClockNPACtxtType *)pContext;

  /*-----------------------------------------------------------------------*/
  /* Create NPA client handle for VDD CX                                   */
  /*-----------------------------------------------------------------------*/

  pClockNPACtxtType->hClient = npa_create_sync_client(
    pClockNPACtxtType->Clock_NPARemoteResource.local_resource_name,
    "UEFI_client", NPA_CLIENT_REQUIRED);

} /* END Clock_VDDCXNodeReadyCallback */


/* =========================================================================
**  Function : Clock_VDDMXNodeReadyCallback
** =========================================================================*/

void Clock_VDDMXNodeReadyCallback(void *pContext, unsigned int nEventType, void *pNodeName, unsigned int nNodeNameSize)
{
  ClockNPACtxtType  *pClockNPACtxtType = (ClockNPACtxtType *)pContext;

  /*-----------------------------------------------------------------------*/
  /* Create NPA client handle for VDD MX                                   */
  /*-----------------------------------------------------------------------*/

  pClockNPACtxtType->hClient = npa_create_sync_client(
    pClockNPACtxtType->Clock_NPARemoteResource.local_resource_name,
    "UEFI_client", NPA_CLIENT_REQUIRED);

  /*-----------------------------------------------------------------------*/
  /* Test only.                                                            */
  /*-----------------------------------------------------------------------*/

  #if 0
  {
    DALResult eResult;

    eResult = Clock_EnterLowPowerMode(pClockNPACtxtType->pDrvCtxt);
    eResult = Clock_ExitLowPowerMode(pClockNPACtxtType->pDrvCtxt);
    eResult = eResult;
  }
  #endif

} /* END Clock_VDDMXNodeReadyCallback */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See DalClock.h
*/

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult     eResult;
  uint32        i;

  /*-----------------------------------------------------------------------*/
  /* Initialze local UEFI Clock Driver context.                            */
  /*-----------------------------------------------------------------------*/

  memset(&ClockLocalCtxt, 0, sizeof(ClockLocalCtxtType));

  /*-----------------------------------------------------------------------*/
  /* Initialize the PLLs                                                   */
  /*-----------------------------------------------------------------------*/

  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL0, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL1, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL2, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL3, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL4, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL5, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL8, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_MMPLL9, 0);
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_GPLL4,  0);

  /*-----------------------------------------------------------------------*/
  /* Initialize the CPU PLLs.                                              */
  /*-----------------------------------------------------------------------*/

  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL0, HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL0, HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL1, HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL1, HAL_CLK_CONFIG_PLL_EARLY_OUTPUT_ENABLE);

  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL0, HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL0, HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL1, HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL1, HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSCBFPLL,  HAL_CLK_CONFIG_PLL_MAIN_OUTPUT_ENABLE);

  /*-----------------------------------------------------------------------*/
  /* Get clock IDs for the two clusters and the l3 clock.                 */
  /*-----------------------------------------------------------------------*/
  eResult = Clock_GetClockId(pDrvCtxt, "apcs_cluster0_clk", (ClockIdType *)&ClockLocalCtxt.ClusterH0Id);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  eResult = Clock_GetClockId(pDrvCtxt, "apcs_cluster1_clk", (ClockIdType *)&ClockLocalCtxt.ClusterH1Id);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  eResult = Clock_GetClockId(pDrvCtxt, "apcs_cbf_clk", (ClockIdType *)&ClockLocalCtxt.CbfId);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Ramp up the cluster 0 clock.                                          */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, (ClockIdType)ClockLocalCtxt.ClusterH0Id);

  MuxCfgHFCPUS0.HALConfig.eSource = HAL_CLK_SOURCE_APCSSECPLL0;
  Clock_SetClockConfig(pDrvCtxt, ClockLocalCtxt.ClusterH0Id->pDomain, &MuxCfgHFCPUS0);

  MuxCfgHFCPUH0.HALConfig.eSource = HAL_CLK_SOURCE_APCSPRIPLL0;
  Clock_SetClockConfig(pDrvCtxt, ClockLocalCtxt.ClusterH0Id->pDomain, &MuxCfgHFCPUH0);

  /*-----------------------------------------------------------------------*/
  /* Ramp up the cluster 1 clock. Cluster 1 won't actually be switched to  */
  /* the HF PLL because this code runs on core 0 of cluster 0. The rest of */
  /* the cores will switch to run from the corresponding HF PLLs inside    */
  /* MP_PARK when they are brought out of reset.                           */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, (ClockIdType)ClockLocalCtxt.ClusterH1Id);

  MuxCfgHFCPUS1.HALConfig.eSource = HAL_CLK_SOURCE_APCSSECPLL1;
  Clock_SetClockConfig(pDrvCtxt, ClockLocalCtxt.ClusterH1Id->pDomain, &MuxCfgHFCPUS1);

  MuxCfgHFCPUH1.HALConfig.eSource = HAL_CLK_SOURCE_APCSPRIPLL1;
  Clock_SetClockConfig(pDrvCtxt, ClockLocalCtxt.ClusterH1Id->pDomain, &MuxCfgHFCPUH1);

  /*-----------------------------------------------------------------------*/
  /* Ramp up the CCI clock.                                                */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, (ClockIdType)ClockLocalCtxt.CbfId);
  Clock_SetClockConfig(pDrvCtxt, ClockLocalCtxt.CbfId->pDomain, &MuxCfgHFCBF);

  /*-----------------------------------------------------------------------*/
  /* Enable CPU PLLs FSM mode.                                             */
  /*-----------------------------------------------------------------------*/

  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL0, HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL0, HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSPRIPLL1, HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSSECPLL1, HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE);
  HAL_clk_ConfigSource(HAL_CLK_SOURCE_APCSCBFPLL,  HAL_CLK_CONFIG_PLL_FSM_MODE_ENABLE);

  /*-----------------------------------------------------------------------*/
  /* Get the MDSS MDP clock ID.                                            */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_GetClockId(pDrvCtxt, "mdss_mdp_clk",
    (ClockIdType *)&ClockLocalCtxt.MDSSMDPClockId);
  if (eResult != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the NPA context.                                           */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < SIZEOF_ARRAY(Clock_aNPACtxt); i++)
  {
    if (Clock_aNPACtxt[i].bDefineResource)
    {
      /*-----------------------------------------------------------------------*/
      /* Define the resource and initialize the handle.                        */
      /*-----------------------------------------------------------------------*/
      npa_remote_define_resource(&Clock_aNPACtxt[i].Clock_NPARemoteResource, 0, NULL);
      Clock_aNPACtxt[i].hClient = npa_create_sync_client(
        Clock_aNPACtxt[i].Clock_NPARemoteResource.local_resource_name,
        "UEFI_client", NPA_CLIENT_REQUIRED);
    }
    else
    {
      /*-----------------------------------------------------------------------*/
      /* Create callback events.                                               */
      /*-----------------------------------------------------------------------*/
      npa_resource_available_cb(Clock_aNPACtxt[i].Clock_NPARemoteResource.local_resource_name,
        Clock_aNPACtxt[i].fpCallBack, &Clock_aNPACtxt[i]);
    }
    Clock_aNPACtxt[i].pDrvCtxt = pDrvCtxt;
  }

  pDrvCtxt->pImageCtxt = &ClockLocalCtxt;

  /*-----------------------------------------------------------------------*/
  /* Initialize AVS.                                                       */
  /*-----------------------------------------------------------------------*/
  #if 0
  eResult = Clock_InitAVS(pDrvCtxt, NULL);

  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }
  #endif

  /*-----------------------------------------------------------------------*/
  /* Test only.                                                            */
  /*-----------------------------------------------------------------------*/

  #if 0
  eResult = Clock_EnterLowPowerMode(pDrvCtxt);
  eResult = Clock_ExitLowPowerMode(pDrvCtxt);
  #endif

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_VoltageRequest
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult Clock_VoltageRequest
(
  ClockDrvCtxt          *pDrvCtxt,
  ClockVRegRequestType  *pCurrentRequest,
  ClockVRegRequestType  *pNewRequest
)
{

  /*-----------------------------------------------------------------------*/
  /* Nothing to do for UEFI just yet.                                      */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_VoltageRequest */


/* =========================================================================
**  Function : Clock_InitVoltage
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult Clock_InitVoltage
(
  ClockDrvCtxt *pDrvCtxt
)
{

  /*-----------------------------------------------------------------------*/
  /* Nothing to do for UEFI just yet.                                      */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitVoltage */


/* =========================================================================
**  Function : Clock_EnterLowPowerMode
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_EnterLowPowerMode
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult           eResult, eReturnedResult = DAL_SUCCESS;
  ClockLocalCtxtType  *pClockLocalCtxt = (ClockLocalCtxtType *)pDrvCtxt->pImageCtxt;
  uint32              i;
  uint32              nResultFreq;

  DALCLOCK_LOCK(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Do not allow nested entries of the Low Power Mode.                    */
  /*-----------------------------------------------------------------------*/

  if (pClockLocalCtxt->bLowPowerMode == TRUE)
  {
    DALCLOCK_FREE(pDrvCtxt);
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Lower the MDSS MDP clock's frequency.                                 */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetClockFrequency(pDrvCtxt,
                                    (ClockIdType)ClockLocalCtxt.MDSSMDPClockId,
                                    MDSSMDP_LOW_POWER_FREQUENCY,
                                    CLOCK_FREQUENCY_KHZ_CLOSEST,
                                    &nResultFreq);
  if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
  {
    eReturnedResult = eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Reduce each core's frequency to the lowest possible.                  */
  /*-----------------------------------------------------------------------*/

  Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH0Id->pDomain, &MuxCfgLowPower);
  Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH1Id->pDomain, &MuxCfgLowPower);
  Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->CbfId->pDomain, &MuxCfgLowPower);

  /*-----------------------------------------------------------------------*/
  /* Reduce voltage of both clusters to the lowest possible.               */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVoltageLevel(HAL_AVS_CORE_APC0, CORE_LOWEST_VOLTAGE);
  if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
  {
    eReturnedResult = eResult;
  }
  eResult = Clock_SetVoltageLevel(HAL_AVS_CORE_APC1, CORE_LOWEST_VOLTAGE);
  if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
  {
    /*
     * If the H1 Cluster is not accepting a voltage change, then assume it is
     * being held in reset. Remember this fact.
     */
    pClockLocalCtxt->bClusterH1InReset = TRUE;
  }

  /*-----------------------------------------------------------------------*/
  /* Reduce every bus frequency to the lowest possible.                    */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < SIZEOF_ARRAY(Clock_aNPACtxt); i++)
  {
    if (Clock_aNPACtxt[i].hClient != NULL)
    {
      npa_issue_required_request(Clock_aNPACtxt[i].hClient, Clock_aNPACtxt[i].nLowPowerValue);
    }
  }

  #if 0
  HAL_clk_MemCoreSave();
  HAL_clk_NocDcdEnable();
  #endif

  pClockLocalCtxt->bLowPowerMode = TRUE;

  DALCLOCK_FREE(pDrvCtxt);

  return eReturnedResult;

} /* END Clock_EnterLowPowerMode */


/* =========================================================================
**  Function : Clock_ExitLowPowerMode
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ExitLowPowerMode
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult           eResult = DAL_SUCCESS;
  DALResult           eReturnedResult = DAL_SUCCESS;
  ClockLocalCtxtType  *pClockLocalCtxt = (ClockLocalCtxtType *)pDrvCtxt->pImageCtxt;
  uint32              i;
  uint32              nResultFreq;

  DALCLOCK_LOCK(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Make sure the Low Power Mode is active before exiting it.             */
  /*-----------------------------------------------------------------------*/

  if (pClockLocalCtxt->bLowPowerMode == FALSE)
  {
    DALCLOCK_FREE(pDrvCtxt);
    return DAL_ERROR;
  }

  #if 0
  HAL_clk_NocDcdDisable();
  HAL_clk_MemCoreRestore();
  #endif

  /*-----------------------------------------------------------------------*/
  /* Restore every bus frequency to its nominal.                           */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < SIZEOF_ARRAY(Clock_aNPACtxt); i++)
  {
    if (Clock_aNPACtxt[i].hClient != NULL)
    {
      npa_complete_request(Clock_aNPACtxt[i].hClient);
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Restore voltage of both clusters to the nominal level.                */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVoltageLevel(HAL_AVS_CORE_APC0, CORE_NOMINAL_VOLTAGE);
  if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
  {
    eReturnedResult = eResult;
  }
  eResult = Clock_SetVoltageLevel(HAL_AVS_CORE_APC1, CORE_NOMINAL_VOLTAGE);
  if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
  {
    /*
     * If the H1 Cluster is being held in reset, ignore the voltage setting failure.
     */
    if (pClockLocalCtxt->bClusterH1InReset)
    {
      eResult = DAL_SUCCESS;
    }
    else
    {
      eReturnedResult = eResult;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Restore each core's frequency to the nominal.                         */
  /*-----------------------------------------------------------------------*/

  if (eResult != DAL_ERROR)
  {
    MuxCfgHFCPUS0.HALConfig.eSource = HAL_CLK_SOURCE_APCSSECPLL0;
    Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH0Id->pDomain, &MuxCfgHFCPUS0);

    MuxCfgHFCPUH0.HALConfig.eSource = HAL_CLK_SOURCE_APCSPRIPLL0;
    Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH0Id->pDomain, &MuxCfgHFCPUH0);

    MuxCfgHFCPUS1.HALConfig.eSource = HAL_CLK_SOURCE_APCSSECPLL1;
    Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH1Id->pDomain, &MuxCfgHFCPUS1);

    MuxCfgHFCPUH1.HALConfig.eSource = HAL_CLK_SOURCE_APCSPRIPLL1;
    Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->ClusterH1Id->pDomain, &MuxCfgHFCPUH1);

    Clock_SetClockConfig(pDrvCtxt, pClockLocalCtxt->CbfId->pDomain, &MuxCfgHFCBF);

    /*-----------------------------------------------------------------------*/
    /* Restore the MDSS MDP clock's frequency.                               */
    /*-----------------------------------------------------------------------*/

    eResult = Clock_SetClockFrequency(pDrvCtxt,
                                      (ClockIdType)ClockLocalCtxt.MDSSMDPClockId,
                                      MDSSMDP_DEFAULT_FREQUENCY,
                                      CLOCK_FREQUENCY_KHZ_CLOSEST,
                                      &nResultFreq);
    if (eReturnedResult == DAL_SUCCESS && eResult != DAL_SUCCESS)
    {
      eReturnedResult = eResult;
    }

    pClockLocalCtxt->bLowPowerMode = FALSE;
  }

  DALCLOCK_FREE(pDrvCtxt);

  return eReturnedResult;

} /* END Clock_ExitLowPowerMode */


/*=========================================================================
**  Function : Clock_NormalizeChipInfo
** =========================================================================*/
/*
  See ClockDriver.h
*/

void Clock_NormalizeChipInfo
(
  HAL_clk_ContextType *HALClkCtxt
)
{
}

