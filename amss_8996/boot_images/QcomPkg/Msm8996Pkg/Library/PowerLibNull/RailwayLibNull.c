/** @file RailwayLibNull.c
  
  Stub functions for RailwayLib

  Copyright (c) 2014, 2015, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 02/18/15   kpa     Added railway_init stub.
 09/24/14   ck      Initial revision

=============================================================================*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/



/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

//Function to be called to initialize the railway driver.
void railway_init(void)
{
}
