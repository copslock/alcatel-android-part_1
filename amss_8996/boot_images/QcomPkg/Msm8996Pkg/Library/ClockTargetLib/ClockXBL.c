/** @file ClockXBL.c
  
  Clock functions for ClockLib

  Copyright (c) 2014-2015, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 05/19/15   vph     Init Crypto clocks early for UIE.
 05/15/15   vph     unblock ramdumps to PC in dload mode.
 04/17/14   vph     Add support for V2 HW
 08/27/14   vk      Warning cleanup, remove unused variable
 07/07/14   vph     Initial revision

=============================================================================*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "ClockHWIO.h"
#include "ClockBoot.h"
#include "ClockCommon.h"
#include "ClockBSP.h"
#include "railway.h"

/*=========================================================================
                       MACRO DEFINITIONS
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/

/*
 * Variable to enable Dynamic Clock Divide.
 */
boolean Boot_Clock_DCD = TRUE;


/*=========================================================================
      Function Prototypes
==========================================================================*/

boolean Clock_InitCrypto(void);
boolean Clock_InitRBCPR(void);
boolean Clock_InitIPA(void);
boolean Clock_InitPLLStatic(void);
extern boolean Clock_AGGRE2Init(void);
extern void Clock_ReplaceUSBBootClockEnable(void);
extern void Clock_BIMCConfigFSM( void );
extern void Clock_StoreDDRFreqKHz ( uint32 nFreqKHz );
extern boolean Clock_CopyBIMCPlanToRPM( void );
extern uint32 Clock_GetAPSSCL0SpeedKHz(void);
extern void Clock_InitBIMCPlan( void );

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/*!

  This function enables dynamic clock divider.  HW auto divides clock down
  when bus clock is not in used for power saving.

  @dependencies
  None.

*/
static void Clock_EnableDynClkDivide (void)
{
  HWIO_OUTF(GCC_PNOC_DCD_CONFIG,DCD_ENABLE,1);
  HWIO_OUTF(GCC_CNOC_DCD_CONFIG,DCD_ENABLE,1);

  HWIO_OUT(MMSS_MNOC_DCD_HYSTERESIS_CNT_AXI, 0x00001000); // first count 1 XO cycle; next count 0 XO cycle
  HWIO_OUT(MMSS_MNOC_DCD_CONFIG_AXI        , 0x80004045); // Enabled and div by all (div 2,4,8,16)

  HWIO_OUTF(MMSS_MNOC_DCD_CONFIG_AXI,DCD_ENABLE,1);
  HWIO_OUTF(MMSS_MNOC_DCD_CONFIG_AHB,DCD_ENABLE,1);
  
  return;
}

/* ============================================================================
**  Function : Clock_Init
** ============================================================================
*/
/*!

    This function turns on the required clocks and configures
    Fabric and Krait speeds depending on the System Fabric and
    CPU boot performance level.

    @param eSysPerfLevel   -  [in] Fabric and DDR performance level to initialize.
    @param eCPUPerfLevel   -  [in] Scropion CPU performance level to initialize.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/

boolean Clock_Init( ClockBootPerfLevelType eSysPerfLevel,
                    ClockBootPerfLevelType eCPUPerfLevel)
{  
  if( ! Clock_SetCPUPerfLevel(eCPUPerfLevel)) return FALSE;
  if( ! Clock_SetSysPerfLevel(eSysPerfLevel)) return FALSE;
  if( ! Clock_InitIPA()) return FALSE;

  /* GPLL0 is already in used.  Set its CONFIG_CTL only */
  HWIO_OUT(GCC_GPLL0_CONFIG_CTL, SPARK_CONFIG_CTL);

  /* RPM requires to configure GPLL0 and BIMC GPLLs */
  (void)Clock_ConfigureSource(SRC_GPLL1);
  (void)Clock_ConfigureSource(SRC_GPLL2);
  (void)Clock_ConfigureSource(SRC_GPLL3);
  (void)Clock_ConfigureSource(SRC_GPLL4);

  /* Enable MMSS_NOC_CFG_AHB for program MMSS PLL registers */
  Clock_ToggleClock(HWIO_ADDR(GCC_MMSS_NOC_CFG_AHB_CBCR), CLK_TOGGLE_ENABLE);

  Clock_ReplaceUSBBootClockEnable();
  
  /*
   * Enable HW clock divider
   */
  if (Boot_Clock_DCD == TRUE)
  {
    Clock_EnableDynClkDivide();
  }

  return TRUE;
}


/* ========================================================================
**  Function : Clock_InitVotes
** ======================================================================*/
/*
    Description: This function serves two purposes.  It clears 
    unwanted votes that may be left after watchdog or JTAG reset, which 
    does not actually reset all of GCC.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_InitVotes( void )
{
  uint32 rpm_gpll_votes;
  uint32 apcs_gpll_votes;

  /* 
   * Correct any left over votes from a watchdog or JTAG induced reset.
   *
   * RPM needs GPLL0 and maybe GPLL1 if it was configured in PBL 
   */
  rpm_gpll_votes = HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0);

  if(HWIO_INF(GCC_GPLL1_MODE, PLL_VOTE_FSM_ENA ) > 0) 
  {
    rpm_gpll_votes |= HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL1);
  }

  /* 
   * Keeping the APPS vote for GPLL4 if its enabled.
   * Apps also needs PLL0.
   */
  apcs_gpll_votes = HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0);
  if(HWIO_INF(GCC_GPLL4_MODE, PLL_VOTE_FSM_ENA ) > 0) 
  {
    apcs_gpll_votes |= HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL4);
  }
  HWIO_OUT(GCC_APCS_GPLL_ENA_VOTE, apcs_gpll_votes);
  HWIO_OUT(GCC_RPM_GPLL_ENA_VOTE, rpm_gpll_votes);

  /* Everybody else has not booted yet */
  HWIO_OUT(GCC_APCS_TZ_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_LPASS_DSP_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_SPARE_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_SSC_GPLL_ENA_VOTE, 0);

}


/* ========================================================================
**  Function : Clock_PreDDRInitEx
** ======================================================================*/
/*
    Description: Configure all clocks needed for DDR configuration.  This
    extension API is used for bootup and emergency download mode.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_PreDDRInitEx( uint32 ddr_type )
{
  uint32 mask;
  ClockConfigMuxType clkCfgDDR      = {HWIO_ADDR(GCC_BIMC_DDR_CPLL_CMD_RCGR),   MUX_GCC, SRC_GPLL2,  2, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMC     = {HWIO_ADDR(GCC_BIMC_CMD_RCGR)         ,   MUX_GCC, SRC_GPLL2,  6, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMCHMSS = {HWIO_ADDR(GCC_BIMC_HMSS_AXI_CMD_RCGR),   MUX_GCC, SRC_GPLL2,  4, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMCGFX  = {HWIO_ADDR(GCC_MMSS_BIMC_GFX_CMD_RCGR),   MUX_GCC, SRC_GPLL2,  8, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMCQ6  =  {HWIO_ADDR(GCC_MSS_Q6_BIMC_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL2,  4, 0, 0, 0};

  Clock_StoreDDRFreqKHz(200000);

  /* Clean up from watchdog/JTAG reset */
  Clock_InitVotes();

  /* Set value 0x2 (== termination 5kohm) avoid both DDR hang */
  HWIO_OUTF(GCC_CH0_CSD_RX0_USER_CTL, RESERVE_BITS3_0,0x2);
  HWIO_OUTF(GCC_CH0_CSD_RX1_USER_CTL, RESERVE_BITS3_0,0x2);
  HWIO_OUTF(GCC_CH1_CSD_RX0_USER_CTL, RESERVE_BITS3_0,0x2);
  HWIO_OUTF(GCC_CH1_CSD_RX1_USER_CTL, RESERVE_BITS3_0,0x2);

  /* Enable the necessary voteable HMSS and BIMC related clocks */
  mask = 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_MSTR_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_HMSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_HMSS_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_SLV_AXI_CLK_ENA);
  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);
  
  mask = HWIO_FMSK(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, BIMC_HMSS_AXI_CLK_SLEEP_ENA);
  HWIO_OUTM(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, mask, mask);

  
  /* Revoke APCS IMEM clock enable, since RPM will manage this clock and turn off during XO shutdown */
  HWIO_OUTF( GCC_APCS_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 0 );

  mask =
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, BIMC_HMSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA);
  HWIO_OUTM(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  /* Enable all possible BIMC and DDR clocks */
  Clock_ToggleClock(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  HWIO_OUTF(GCC_BIMC_DDR_CPLL0_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CPLL1_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CH0_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CH1_CBCR, CLK_ENABLE, 1); 
  
  Clock_ToggleClock(HWIO_GCC_BIMC_XO_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_DTTS_XO_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX2, 1); /* BIMC runs from AUX */
	
  /* Perform a BIMC clock switch */
  if( ! Clock_EnableSource( clkCfgBIMC.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&clkCfgBIMC)) return FALSE;

  /* Perform a BIMC_HMSS_AXI clock switch */
  if( ! Clock_EnableSource( clkCfgBIMCHMSS.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&clkCfgBIMCHMSS)) return FALSE;

  /* Perform a MMSS_BIMC_GFX clock switch */
  if( ! Clock_EnableSource( clkCfgBIMCGFX.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&clkCfgBIMCGFX)) return FALSE;

  /* Perform a GCC_MSS_Q6_BIMC_AXI clock switch */
  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) > 1 || HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4 )
  {
    HWIO_OUTF(GCC_MSS_Q6_BIMC_AXI_CBCR, CLK_ENABLE, 1);
    if( ! Clock_EnableSource( clkCfgBIMCQ6.eSource )) return FALSE;
    if( ! Clock_ConfigMux(&clkCfgBIMCQ6)) return FALSE;
  }

  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1); /* BIMC runs from AUX */

  /* This is the first switch in GCC (Legacy) mode */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 1);
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 0);

  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_LEGACY_MODE_EN, 1);
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 1);

  /* Perform a DDR clock switch */
  if( ! Clock_EnableSource( clkCfgDDR.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&clkCfgDDR)) return FALSE;

  /* DDR has switched to CPLL1, force the root on. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 1);

  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, FSM_DIS_DDR_UPDATE))) return FALSE;


  /* Turn off CPLL0 root. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 0);

  /*
   * Trigger an update again so that the JCPLL selection stays in sync with 
   * the FSM state.
   */
  if(! Clock_TriggerUpdate(clkCfgDDR.nCMDCGRAddr, 
                           HWIO_FMSK(GCC_BIMC_DDR_CPLL_CMD_RCGR, UPDATE))) return FALSE;

  /* DDR has switched to CPLL0, force the root on. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 1);
    
  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, FSM_DIS_DDR_UPDATE))) return FALSE;

  /* Turn off CPLL1 root */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 0);

  Clock_I2CInit();

  HWIO_OUTF(GCC_BIMC_GDSCR, RETAIN_FF_ENABLE, 1);

  return TRUE;
}


/* ========================================================================
**  Function : Clock_PreDDRInit
** ======================================================================*/
/*
    Description: Clock_PreDDRInitEx() was added in 8974 to provide a separate API
    for emergency download (EDL) without including Mxdroop work around. Adding wrapper 
    function to avoid compilation erros in 8x26 and 8x10.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_PreDDRInit( uint32 ddr_type )
{
  Clock_RailwayType *pClockRailway;

  /* To improve boot time, speed up the CPU and buses */
  if( ! Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MAX)) return FALSE;
  if( ! Clock_SetSysPerfLevel(CLOCK_BOOT_PERF_MAX)) return FALSE;

  /* Setup railways */
  pClockRailway = Clock_RailwayConfig();
  pClockRailway->nCxRailId = RAIL_NOT_SUPPORTED_BY_RAILWAY;
  pClockRailway->nEBIRailId = RAIL_NOT_SUPPORTED_BY_RAILWAY;

  /* Only vote for railways for normal boot (Not deviceprogrammer) */
  if ( PcdGet32 (PcdBuildType) == 0 || PcdGet32 (PcdBuildType) == 5 )
  {
    /* Initialize CX & VDDA_EBI Rails */
    pClockRailway->nCxRailId = rail_id(pClockRailway->CxRail);
    pClockRailway->nEBIRailId = rail_id(pClockRailway->EBIRail);

    if( (RAIL_NOT_SUPPORTED_BY_RAILWAY == pClockRailway->nCxRailId) ||
        (RAIL_NOT_SUPPORTED_BY_RAILWAY == pClockRailway->nEBIRailId) )
    {
      return FALSE;
    }
    
    pClockRailway->CxVoter = railway_create_voter(pClockRailway->nCxRailId,RAILWAY_CLOCK_DRIVER_VOTER_ID);
    pClockRailway->EBIVoter = railway_create_voter(pClockRailway->nEBIRailId,RAILWAY_CLOCK_DRIVER_VOTER_ID);

    railway_corner_vote(pClockRailway->CxVoter, RAILWAY_NOMINAL);
    railway_corner_vote(pClockRailway->EBIVoter, RAILWAY_TURBO);
    railway_transition_rails( );
  }

  /* Init BIMC clock plan for V1 or V2 */
  Clock_InitBIMCPlan();

  return Clock_PreDDRInitEx( ddr_type );
}


/* ========================================================================
**  Function : Clock_I2CInit
** ======================================================================*/
/*
    Description: Configure all clocks needed for EEPROM to be used Pre DDR.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_I2CInit( void )
{
  /* These clocks can be disabled at Clock_ExitBoot, so keep that function
   * in sync */

  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_SLEEP_CLK_ENA, 1);
  Clock_ToggleClock(HWIO_GCC_BLSP2_QUP5_I2C_APPS_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  // Turn  on the GCC_BLSP1_QUP2_I2C_APPS_CBCR
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_SLEEP_CLK_ENA, 1);
  HWIO_OUTF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_ENABLE, 1);

  return TRUE;
}


/* ============================================================================
**  Function : Clock_InitForDownloadMode
** ============================================================================
*/
/*!
    Configure clocks for download.  Enable every RAM we want to dump.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
void Clock_InitForDownloadMode(void)
{
  uint32 mask;

  /* Enable the necessary voteable KPSS and BIMC related clocks */
  mask = 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_HMSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_HMSS_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_SLV_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BOOT_ROM_AHB_CLK_ENA);

  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 1);
}


/* ============================================================================
**  Function : Clock_ExitBoot
** ============================================================================
*/
/*!

    This function turns off clocks that were used during boot, but are not
    needed after boot.  This should be called at the end of boot.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/
boolean Clock_ExitBoot(void)
{
  /* Clear UFS_BOOT_CLOCK_CTL, so UFS clocks can be managed by HLOS */
  HWIO_OUTF ( GCC_UFS_BOOT_CLOCK_CTL, CLK_ENABLE, 0 );

  /* Disable I2C related clocks */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_AHB_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_SLEEP_CLK_ENA, 0);
  Clock_ToggleClock(HWIO_GCC_BLSP2_QUP4_I2C_APPS_CBCR_ADDR, CLK_TOGGLE_DISABLE);

   // Turn  off the GCC_BLSP1_QUP2_I2C_APPS_CBCR
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_SLEEP_CLK_ENA, 0);
  HWIO_OUTF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_ENABLE, 0);

  //Revert and clean up VMEM clocks
  HWIO_OUTF(MMSS_VMEM_AHB_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF(MMSS_VMEM_MAXI_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF(MMSS_MMSS_MMAGIC_MAXI_CBCR, CLK_ENABLE, 0); 
  HWIO_OUTF(MMSS_MMSS_MMAGIC_AHB_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF(MMSS_MMAGIC_VIDEO_GDSCR, SW_COLLAPSE, 1);

  return TRUE;
}


/* ============================================================================
**  Function : Clock_DebugInit
** ============================================================================
*/
/*!

    This function is called very early in boot.  It is used for work-arounds that
    need to be done before JTAG attaches at the XBL "hold focus button" spin loop.

    @return
    TRUE -- Clean up was successful.
    FALSE -- Clean up failed.

   @dependencies
    You cannot use busywait in this function.

*/
boolean Clock_DebugInit(void)
{
  /* Enable SPMI clocks */
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, SPMI_CNOC_AHB_CLK_ENA, 1);

  /* Enable GPLL0 EARLY for support SVS2 mode */
  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_EARLY, 1);

  /* Enable PIMEM clocks */
  HWIO_OUTF(GCC_PIMEM_AHB_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_SYS_NOC_PIMEM_AXI_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_PIMEM_AXI_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_PIMEM_AXI_CBCR, CLK_ENABLE, 1);

  /* Enable AGGRE NOC clocks*/
  Clock_ToggleClock(HWIO_GCC_AGGRE1_NOC_MPU_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_AGGRE2_NOC_MPU_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  Clock_ToggleClock(HWIO_GCC_SPMI_SER_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SPMI_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Always enable this clock for GCC resets (BCRs) to work */
  Clock_ToggleClock(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  Clock_ToggleClock( HWIO_ADDR(GCC_SNOC_PNOC_AHB_CBCR), CLK_TOGGLE_ENABLE );
  Clock_ToggleClock( HWIO_ADDR(GCC_PERIPH_NOC_AHB_CBCR), CLK_TOGGLE_ENABLE );

  /* Needed for Random Stack Canary */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  /*
   * These clocks are required for the GDS HW controllers to receive the
   * necessary NOC handshakes. Enable them once and leave them on all the time,
   * allowing the ones on collapsible power domains to enable/disable along with
   * the GDS.
   */
  HWIO_OUTF(GCC_MMSS_SYS_NOC_AXI_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_MMSS_NOC_AT_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_AGGRE0_NOC_AT_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_AGGRE1_NOC_AT_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_AGGRE2_NOC_AT_CBCR, CLK_ENABLE, 1);

  if( ! Clock_InitCrypto()) return FALSE;
  if( ! Clock_InitRBCPR()) return FALSE;
  if( ! Clock_InitPLLStatic()) return FALSE;
  if( ! Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MAX)) return FALSE;

  return TRUE;
}

/* ============================================================================
**  Function : Clock_SetSysPerfLevel
** ============================================================================
*/
/**
  Configure NOCs and RPM to a perf level.

  @param eSysPerfLevel [in]  -  NOCs and RPM performance level to configure.

  @return
  TRUE -- NOCs were configured to perf level successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetSysPerfLevel
(
  ClockBootPerfLevelType eSysPerfLevel
)
{
  const ClockConfigMuxType *clkCfg;
  const Clock_ConfigType *cfg = Clock_Config();
  static ClockBootPerfLevelType eCurrLevel = CLOCK_BOOT_PERF_NONE;

  /* Don't switch performance level if it has been set previously */
  if ( eCurrLevel == eSysPerfLevel )
  {
    return TRUE;
  }
  eCurrLevel = eSysPerfLevel;

  if(eSysPerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;

  /* Configure RPM */
  clkCfg = &cfg->RPM_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure SNOC */
  clkCfg = &cfg->SNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure PIMEM_AXI */
  clkCfg = &cfg->PIMEM_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure HS_SNOC */
  clkCfg = &cfg->HS_SNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure CNOC */
  clkCfg = &cfg->CNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure PNOC */
  clkCfg = &cfg->PNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure HMSS AHB clock */
  clkCfg = &cfg->HMSS_AHB_Cfg;
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure BIMC */
  Clock_BIMCConfigFSM();

  return TRUE;
}


/* ============================================================================
**  Function : Clock_InitCrypto
** ============================================================================
*/
/*!
    Configure Crypto clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_InitCrypto(void)
{
  uint32 mask;
  const ClockConfigMuxType *CECfg;
  const Clock_ConfigType *cfg = Clock_Config();
  CECfg = &cfg->CE_Cfg;

  if( ! Clock_EnableSource( CECfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(CECfg)) return FALSE;

  mask = 
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, CE1_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, CE1_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, CE1_AHB_CLK_ENA );
  HWIO_OUTM(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  /* Revoke APCS CE1 clocks which may be enable in PBL, since RPM will manage these clocks */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CE1_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CE1_AXI_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CE1_AHB_CLK_ENA, 0 );
  return TRUE;

} /* END Clock_InitCrypto */


/* ============================================================================
**  Function : Clock_InitRBCPR
** ===========================================================================*/
/*!
    Configure RBCPR clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_InitRBCPR(void)
{

  /* Enable RPCPR clocks */
  Clock_ToggleClock(HWIO_GCC_RBCPR_XPU_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_HMSS_RBCPR_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_RBCPR_CX_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_RBCPR_CX_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_RBCPR_MX_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  return TRUE;

} /* END Clock_InitRBCPR */


/* ============================================================================
**  Function : Clock_InitIPA
** ===========================================================================*/
/*!
    Configure and enable IPA clocks

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_InitIPA(void)
{
  /* A mux config for IPA clock to 150MHz*/
  const ClockConfigMuxType IPACfg = {HWIO_ADDR(GCC_IPA_CMD_RCGR), MUX_GCC, SRC_GPLL0, 8, 0, 0, 0};

  /* IPA is only available in V2 or higher */
  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) > 1 || HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4 )
  {
    /* IPA has dependency on Aggre2 NOC.  Request Aggre2 NOC on */
    Clock_AGGRE2Init();

    /* Enable IPA clocks */
    HWIO_OUTF(GCC_IPA_GDSCR, SW_COLLAPSE, 0);
    while(HWIO_INF(GCC_IPA_GDSCR, PWR_ON) == 0);  // loop while off

    if( ! Clock_ConfigMux(&IPACfg)) return FALSE;

    if( ! Clock_ToggleClock(HWIO_GCC_AGGRE2_NOC_IPA_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
    if( ! Clock_ToggleClock(HWIO_GCC_IPA_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
    if( ! Clock_ToggleClock(HWIO_GCC_IPA_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
    if( ! Clock_ToggleClock(HWIO_GCC_IPA_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  }
  
  return TRUE;

} /* END Clock_InitIPA */


/* ============================================================================
**  Function : Clock_GetClockFrequency
** ============================================================================
*/
/*!
    Get the requested clock frequency in hertz.

    @param
      eBootLogClock [in]  - targeted clock
      pnFrequencyHz [out] - frequency of clock in hertz

    @return
      True iff the requested clock frequency is placed in pnFrequencyHz.

    @dependencies
    None.

    @sa None
*/

boolean Clock_GetClockFrequency(ClockBootLogQueryType eBootLogClock,
                                uint32 *pnFrequencyHz)
{
  boolean ret = TRUE;

  switch(eBootLogClock)
  {
  case CLK_BOOT_LOG_APPS_CLUSTER_0:
    *pnFrequencyHz = (Clock_GetAPSSCL0SpeedKHz() * 1000);
    break;

  case CLK_BOOT_LOG_DDR:
    *pnFrequencyHz = (Clock_DDRSpeed() * 1000);
    break;

  default:
    ret = FALSE;
    break;
  }

  return ret;
}


/* ============================================================================
**  Function : Clock_InitRPM()
** ============================================================================
*/
/**
  Perform any initialization needed just before RPM starts.  SMEM is available
  at this time.

  @param 
  @return
  TRUE -- Initialization succeeded.
  FALSE -- Initialization failed.

  @dependencies
  None.

  @sa
  Clock_Init.
  boot_smem_init
*/
boolean Clock_InitRPM( void )
{
  return Clock_CopyBIMCPlanToRPM();
}


/* ============================================================================
**  Function : Clock_InitPLLStatic()
** ============================================================================
*/
/**
  Perform initial static configurations and workarounds 
  defined in frequency plan documentation,

  @param 
  @return
  TRUE -- Initialization succeeded.
  FALSE -- Initialization failed.

  @dependencies
  None.

*/

boolean Clock_InitPLLStatic( void )
{
  /*Enable MMSS NOC prior to configuring PLLs*/
  HWIO_OUTF(GCC_MMSS_NOC_CFG_AHB_CBCR, CLK_ENABLE, 0x1);
  
  /* Set CONFIG_CTL Non-Huayra PLLs */
  HWIO_OUT(APC0_QLL_ALT_PLL_CONFIG_CTL, SPARK_CONFIG_CTL);
  HWIO_OUT(APC1_QLL_ALT_PLL_CONFIG_CTL, SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL0_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL1_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL2_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL3_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL4_PLL_CONFIG_CTL,  BRAMO_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL5_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL8_PLL_CONFIG_CTL,  SPARK_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL9_PLL_CONFIG_CTL,  BRAMO_CONFIG_CTL);
  HWIO_OUT(MMSS_MMPLL4_PLL_TEST_CTL, (uint32)PLL_TEST_CTL_BRAMO); 
  HWIO_OUT(MMSS_MMPLL9_PLL_TEST_CTL, (uint32)PLL_TEST_CTL_BRAMO);

  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) == 3 || HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4)
  { 
    /* Set HUAYRA_V3 CONFIG_CTL PLLs */
    HWIO_OUT(APCS_CBF_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL_V3 >> 32));
    HWIO_OUT(APCS_CBF_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL_V3);
    HWIO_OUT(APC0_QLL_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL_V3 >> 32));
    HWIO_OUT(APC0_QLL_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL_V3);
    HWIO_OUT(APC1_QLL_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL_V3 >> 32));
    HWIO_OUT(APC1_QLL_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL_V3);
  
    /*Set HUAYRA_V3 PLL_TEST_CTL */
    HWIO_OUT(APC0_QLL_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA_V3 >> 32));
    HWIO_OUT(APC0_QLL_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA_V3);
    HWIO_OUT(APC1_QLL_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA_V3 >> 32));
    HWIO_OUT(APC1_QLL_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA_V3);
    HWIO_OUT(APCS_CBF_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA_V3 >> 32));
    HWIO_OUT(APCS_CBF_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA_V3);
    
    /* Improve CPU to DDR latency with below M4M timer settings.
     * This setting is applicable to MSM8996 only 
     */
    /* set to 0 cycles with L2CPMR[EQSBACG] set to 0 for IstariV3 */
    HWIO_OUTF( APCS_CBF_M4M_CLK_ON_REQ_CNTR, SET_CNTR, 0x0);
    
    /* Set to 255 cycles to cover for cases where CPU is active and DDR is at a lower frequency */
    HWIO_OUTF( APCS_CBF_M4M_CLK_ON_REQ_CNTR, CLR_CNTR, 0xFF);

    /* Disable so that few cycles can be saved inside M4M with minimal power impact */
    HWIO_OUTF( APCS_CBF_M4M_SLEEP_CTRL, ARCH_CLK_GATE_EN, 0);
  }
  else
  {
    /* Set HUAYRA CONFIG_CTL PLLs */
    HWIO_OUT(APCS_CBF_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL >> 32));
    HWIO_OUT(APCS_CBF_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL);
    HWIO_OUT(APC0_QLL_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL >> 32));
    HWIO_OUT(APC0_QLL_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL);
    HWIO_OUT(APC1_QLL_PLL_CONFIG_CTL_HI, (uint32)(HUAYRA_CONFIG_CTL >> 32));
    HWIO_OUT(APC1_QLL_PLL_CONFIG_CTL_LO, (uint32)HUAYRA_CONFIG_CTL);
  
    /*Set HUAYRA PLL_TEST_CTL */
    HWIO_OUT(APC0_QLL_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA >> 32));
    HWIO_OUT(APC0_QLL_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA);
    HWIO_OUT(APC1_QLL_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA >> 32));
    HWIO_OUT(APC1_QLL_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA);
    HWIO_OUT(APCS_CBF_PLL_TEST_CTL_HI,  (uint32)(PLL_TEST_CTL_HUAYRA >> 32));
    HWIO_OUT(APCS_CBF_PLL_TEST_CTL_LO,  (uint32)PLL_TEST_CTL_HUAYRA);
  }
  
  if( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) > 2 || 
    ( HWIO_INF(TCSR_SOC_HW_VERSION, MAJOR_VERSION) == 2 && HWIO_INF(TCSR_SOC_HW_VERSION, MINOR_VERSION) > 0 ) ||
    ( HWIO_INF(TCSR_SOC_HW_VERSION, DEVICE_NUMBER) == 4 )) return TRUE;

  /* Set PLL_TEST_CTL */
  HWIO_OUT(GCC_GPLL0_TEST_CTL,           (uint32)PLL_TEST_CTL_SPARK1);
  HWIO_OUT(GCC_GPLL1_TEST_CTL,           (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(GCC_GPLL2_TEST_CTL,           (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(GCC_GPLL3_TEST_CTL,           (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(GCC_GPLL4_TEST_CTL,           (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(APC0_QLL_ALT_PLL_TEST_CTL_LO, (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(APC1_QLL_ALT_PLL_TEST_CTL_LO, (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(MMSS_MMPLL0_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(MMSS_MMPLL1_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK2);
  HWIO_OUT(MMSS_MMPLL2_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK1); 
  HWIO_OUT(MMSS_MMPLL3_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK1); 
  HWIO_OUT(MMSS_MMPLL5_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK2); 
  HWIO_OUT(MMSS_MMPLL8_PLL_TEST_CTL,     (uint32)PLL_TEST_CTL_SPARK1); 

  return TRUE;
}
