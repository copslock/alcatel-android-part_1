/** @file ClockBSP.c

  Definitions of the support clock perf level

  Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

**/

/*=============================================================================
                              EDIT HISTORY


 when        who     what, where, why
 --------    ---     ---------------------------------------------------------
07/07/14     vph     Created.

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"
#include "ClockCommon.h"
#include "ClockHWIO.h"

/*=========================================================================
      Prototypes
==========================================================================*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_SourceMapToMMSS(ClockSourceType eSource, uint32 *nMuxValue);
boolean Clock_GCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource);
boolean Clock_MMSSMuxMapToSource(uint32 nSource, ClockSourceType* pSource);

/*=========================================================================
      Data
==========================================================================*/
static Clock_RailwayType ClockRailway =
{
  "vddcx",
  0,
  0,
  "vdda_ebi",
  0,
  0
};

static Clock_ConfigType Clock_ConfigData =
{
  /* GPLL0 @ 600 MHz */
  .PLL0_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL0_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0),
    .nVCO          =  2, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  31, 
    .nM            =  0, // unused
    .nN            =  0, // unused
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0x4000000000
  },
 
  /* GPLL1 @ 533 MHz */
  .PLL1_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL1_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL1),
    .nVCO          =  2, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  27, 
    .nM            =  0, // unused
    .nN            =  0, // unused
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0xC2AAAAAAAA
  },

  /* GPLL2 @ 200 MHz (Dedicate to BIMC only.  Will program at run-time) */
  .PLL2_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL2_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL2),
    .nVCO          =  2, 
    .nPreDiv       =  1, 
    .nPostDiv      =  4, 
    .nL            =  41, 
    .nM            =  0, // unused
    .nN            =  0, // unused
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0xAAAAAAAAAA
  },

  /* GPLL3 @ 933 MHz (Dedicate to BIMC only.  Will program at run-time) */
  .PLL3_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL3_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL3),
    .nVCO          =  2, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  0x30,  
    .nM            =  0,
    .nN            =  0,
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0x9800000000
  },

  /* GPLL4 @  1536 MHz  */
  .PLL4_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL4_MODE), 
    .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE), 
    .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL4),
    .nVCO          =  0, 
    .nPreDiv       =  1, 
    .nPostDiv      =  4, 
    .nL            =  0x50, 
    .nM            =  0,
    .nN            =  0,
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0
  },

  /* MMPLL8 @  360 MHz  */
  .MMPLL8_Cfg_V1 =
  {
    .nPLLModeAddr  =  HWIO_ADDR(MMSS_MMPLL8_PLL_MODE), 
    .nVoteAddr     =  0, 
    .nVoteMask     =  0,
    .nVCO          =  3, 
    .nPreDiv       =  1, 
    .nPostDiv      =  1, 
    .nL            =  0x12, 
    .nM            =  0,
    .nN            =  0,
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0xC000000000
  },

  /* MMPLL8 @  500 MHz  */
  .MMPLL8_Cfg_V2 =
  {
    .nPLLModeAddr  =  HWIO_ADDR(MMSS_MMPLL8_PLL_MODE), 
    .nVoteAddr     =  0, 
    .nVoteMask     =  0,
    .nVCO          =  1, 
    .nPreDiv       =  1, 
    .nPostDiv      =  2, 
    .nL            =  0x34, 
    .nM            =  0,
    .nN            =  0,
    .nConfigCtl    =  SPARK_CONFIG_CTL,
    .nAlpha        =  0x1555555555
  },

  .CPU_Cfg_Huayra = 
  {
    /* {ClockConfigPLLType, nFrequency} */
    { {0}, 0 },
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 0, 1, 1,  0, 0, 0, HUAYRA_CONFIG_CTL, 0},  19200},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 2, 92, 0, 0, HUAYRA_CONFIG_CTL, 0}, 883200},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 2, 92, 0, 0, HUAYRA_CONFIG_CTL, 0}, 883200},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 2, 92, 0, 0, HUAYRA_CONFIG_CTL, 0}, 883200},
  },

  /* Spark PLL VCO bands */ 
  /* 0 : 1000 - 2000 MHz */
  /* 1 :  750 - 1500 MHz */
  /* 2 :  500 - 1000 MHz */
  /* 3 :  250 -  500 MHz */
  
  .CPU_Cfg_Huayra_V3 = 
  {
    /* {ClockConfigPLLType, nFrequency} */
    { {0}, 0 },
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 0, 1, 1,  0, 0, 0, HUAYRA_CONFIG_CTL_V3, 0},   19200},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 1, 64, 0, 0, HUAYRA_CONFIG_CTL_V3, 0}, 1228800},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 1, 64, 0, 0, HUAYRA_CONFIG_CTL_V3, 0}, 1228800},
    { {HWIO_ADDR(APC0_QLL_PLL_MODE), 0, 0, 1, 1, 1, 64, 0, 0, HUAYRA_CONFIG_CTL_V3, 0}, 1228800},
  },

  .CBF_Cfg = 
  {
    /* { nSourceHF, nSourceLF, nGFMDiv, nL, nPLLPostDiv, nFrequency } */
    { 0 },
    { CBF_MUXB_XO, CBF_MUXA_MUXB,    1, 0, 1,  19200}, /* Min */
    { CBF_MUXB_XO, CBF_MUXA_PLL_AUX, 1, 0, 1, 600000}, /* Nom */
    { CBF_MUXB_XO, CBF_MUXA_PLL_AUX, 1, 0, 1, 600000}, /* Max */
    { CBF_MUXB_XO, CBF_MUXA_PLL_AUX, 1, 0, 1, 600000}, /* Default */
  },

  .RPM_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                               /* PERF NONE */
    {HWIO_ADDR(GCC_RPM_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0},  /* MIN - 19.2 MHz               */
    {HWIO_ADDR(GCC_RPM_CMD_RCGR), MUX_GCC, SRC_GPLL0,  3, 0, 0, 0},  /* NOM - 400 MHz (SVS)          */
    {HWIO_ADDR(GCC_RPM_CMD_RCGR), MUX_GCC, SRC_GPLL0,  1, 0, 0, 0},  /* MAX - 600 MHz (NOMINAL)      */
    {HWIO_ADDR(GCC_RPM_CMD_RCGR), MUX_GCC, SRC_GPLL0,  1, 0, 0, 0}   /* DEFAULT - 600 MHz (Max Nom)  */
  },

  .SNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                      /* PERF NONE */
    {HWIO_ADDR(GCC_SYSTEM_NOC_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0},  /* MIN - 19.2 MHz               */
    {HWIO_ADDR(GCC_SYSTEM_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0},  /* NOM - 100 MHz (SVS)          */
    {HWIO_ADDR(GCC_SYSTEM_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0},  /* MAX - 200 MHz (NOMINAL)      */
    {HWIO_ADDR(GCC_SYSTEM_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0}   /* DEFAULT - 200 MHz (Max Nom)  */
  },

  .PIMEM_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                      /* PERF NONE */
    {HWIO_ADDR(GCC_PIMEM_AXI_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0},  /* MIN - 19.2 MHz               */
    {HWIO_ADDR(GCC_PIMEM_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0},  /* NOM - 100 MHz (SVS)          */
    {HWIO_ADDR(GCC_PIMEM_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0},  /* MAX - 200 MHz (NOMINAL)      */
    {HWIO_ADDR(GCC_PIMEM_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0}   /* DEFAULT - 200 MHz (Max Nom)  */
  },

  .HS_SNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_SYS_NOC_HS_AXI_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0},  /* MIN - 19.2 MHz               */
    {HWIO_ADDR(GCC_SYS_NOC_HS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0},  /* NOM - 200 MHz (SVS)          */
    {HWIO_ADDR(GCC_SYS_NOC_HS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  3, 0, 0, 0},  /* MAX - 400 MHz (NOMINAL)      */
    {HWIO_ADDR(GCC_SYS_NOC_HS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0}   /* DEFAULT - 400 MHz (Max Nom)  */
  },

  .CNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                      /* PERF NONE                  */
    {HWIO_ADDR(GCC_CONFIG_NOC_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0},  /* MIN - 19.2 MHz             */
    {HWIO_ADDR(GCC_CONFIG_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 32, 0, 0, 0},  /* NOM - 37.5 MHz (SVS)       */
    {HWIO_ADDR(GCC_CONFIG_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0},  /* MAX - 75 MHz    (NOMINAL)  */
    {HWIO_ADDR(GCC_CONFIG_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}   /* DEFAULT - 75 MHz (Max Nom) */
  },

  .PNOC_Cfg = 
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                      /* PERF NONE                  */
    {HWIO_ADDR(GCC_PERIPH_NOC_CMD_RCGR), MUX_GCC, SRC_CXO,  1, 0, 0, 0},    /* MIN - 19.2 MHz             */
    {HWIO_ADDR(GCC_PERIPH_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 0, 0, 0},  /* NOM - 50 MHz (SVS)         */
    {HWIO_ADDR(GCC_PERIPH_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0},  /* MAX - 100 MHz (NOMINAL)    */
    {HWIO_ADDR(GCC_PERIPH_NOC_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0}   /* DEFAULT - 100 MHz(Max Nom) */
  },

  /* SDC configuration : for backwards compatiblity to the old API */
  .SDC_Cfg =
  {
    0,       /* PERF NONE                 */
    400,     /* MIN - 400KHz              */
    25000,   /* NOMINAL - 25MHz           */
    50000,   /* MAX - 50MHz               */
    25000    /* DEFAULT - SAME AS NOMINAL */
  }, /* END SDC config */

  /*  SDC extended configurations */
  .SDC_Ext_Cfg =
  {
    {   400,  {0, MUX_GCC, SRC_CXO, 24, 1, 4, 4}},
    { 25000,  {0, MUX_GCC, SRC_GPLL0, 24, 1, 2, 2}},
    { 50000,  {0, MUX_GCC, SRC_GPLL0, 24, 0, 0, 0}},
    {100000,  {0, MUX_GCC, SRC_GPLL0, 12, 0, 0, 0}},
    {192000,  {0, MUX_GCC, SRC_GPLL4,  4, 0, 0, 0}},
    {384000,  {0, MUX_GCC, SRC_GPLL4,  2, 0, 0, 0}}
  }, /* END SDC_Ext_Cfg */

  /* Crypto configuration CE_Cfg : 171.43 MHz */
  .CE_Cfg = 
  {
    HWIO_ADDR(GCC_CE1_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    7, /* nDiv2x */
    0,0,0 /* M/N:D */
  },

  /* USB configuration USB_Cfg : 120 MHz */
  .USB_Cfg =
  {
    HWIO_ADDR(GCC_USB20_MASTER_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    10, /* nDiv2x */
    0,0,0 /* M/N:D */
  },

  /*UART configurations UART_cfg : 3.6864 MHz for UART1 to UART6 clocks*/
  .UART_Cfg = 
   {
      {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},						   /* PERF-NONE */
      {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 96, 15625, 15625}, /*MIN - 3.6864 MHz */
      {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 96, 15625, 15625}, /* NOMINAL - 3.6864 MHz MHz */
      {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 96, 15625, 15625}, /* MAX - 3.6864 MHz MHz */
      {HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 1, 96, 15625, 15625}, /* DEFAULT - SAME AS NOMINAL */	  
    },

  /* HMSS AHB configuration */
  .HMSS_AHB_Cfg =
  {
    HWIO_ADDR(GCC_HMSS_AHB_CMD_RCGR), MUX_GCC, SRC_CXO, 1, 0, 0, 0 /* 19.2 MHZ */   
  }, /* END HMSS AHB configuration */

  /* I2C */
 .I2C_Cfg = 
   {
      {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                              /* PERF-NONE */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* MIN - 19.2 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 32, 0, 0, 0}, /* NOMINAL - 37.5 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 0, 0, 0}, /* MAX - 50 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* DEFAULT - 19.2 MHz */
    },
    
  /* BLSP*_QUP*_SPI configurations */
  .SPI_Cfg =
  {
    { 19200, {0, MUX_GCC, SRC_CXO,    1, 0, 0, 0}},
    { 50000, {0, MUX_GCC, SRC_GPLL0, 24, 0, 0, 0}},
    {0}
  },

  .QSPISer_Cfg =
  {
    { 75000, {HWIO_ADDR(GCC_QSPI_SER_CMD_RCGR), MUX_GCC, SRC_GPLL0, 16, 0, 0, 0}},
    {150000, {HWIO_ADDR(GCC_QSPI_SER_CMD_RCGR), MUX_GCC, SRC_GPLL0,  8, 0, 0, 0}},
    {266500, {HWIO_ADDR(GCC_QSPI_SER_CMD_RCGR), MUX_GCC, SRC_GPLL1,  4, 0, 0, 0}},
    {300000, {HWIO_ADDR(GCC_QSPI_SER_CMD_RCGR), MUX_GCC, SRC_GPLL0,  4, 0, 0, 0}},
    {0}
  },

  /* UFS */
  .UFS_Cfg = 
  {
    {HWIO_ADDR(GCC_UFS_AXI_CMD_RCGR), MUX_GCC, SRC_CXO,   1,  0, 0, 0}, /* OFF */
    {HWIO_ADDR(GCC_UFS_AXI_CMD_RCGR), MUX_GCC, SRC_CXO,   1,  0, 0, 0}, /* MIN - 19.2 MHz */
    {HWIO_ADDR(GCC_UFS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0, 6,  0, 0, 0}, /* NOMINAL - 200 MHz */
    {HWIO_ADDR(GCC_UFS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0, 6,  0, 0, 0}, /* MAX - 200 MHz */
    {HWIO_ADDR(GCC_UFS_AXI_CMD_RCGR), MUX_GCC, SRC_GPLL0, 6,  0, 0, 0}, /* DEFAULT - SAME AS NOMINAL */
  },
  
  /* UFS ICE CORE*/
  .UFS_Ice_Cfg = 
  {
    {HWIO_ADDR(GCC_UFS_ICE_CORE_CMD_RCGR), MUX_GCC, SRC_CXO,   1,  0, 0, 0}, /* OFF */
    {HWIO_ADDR(GCC_UFS_ICE_CORE_CMD_RCGR), MUX_GCC, SRC_GPLL0, 4,  0, 0, 0}, /* NOMINAL - 300 MHz */
    {HWIO_ADDR(GCC_UFS_ICE_CORE_CMD_RCGR), MUX_GCC, SRC_GPLL0, 4,  0, 0, 0}, /* MAX - 300 MHz */
    {HWIO_ADDR(GCC_UFS_ICE_CORE_CMD_RCGR), MUX_GCC, SRC_GPLL0, 4,  0, 0, 0}, /* DEFAULT - SAME AS NOMINAL */
  },
};


boolean (*Clock_MuxMap[NUM_MUX_TYPES])(ClockSourceType, uint32 *) =
{
  Clock_SourceMapToGCC, 
  Clock_SourceMapToMMSS
};

boolean (*Clock_SourceMap[NUM_MUX_TYPES])(uint32,ClockSourceType *) =
{
  Clock_GCCMuxMapToSource, 
  Clock_MMSSMuxMapToSource
};

/*=========================================================================
      Functions
==========================================================================*/

/* ============================================================================
**  Function : Clock_RailwayCfg
** ============================================================================
*/
/*!
    Return a pointer to the Railway configuration data.

   @param  None

   @retval a pointer to the Railway configuration data

*/
Clock_RailwayType *Clock_RailwayConfig( void )
{
  return &ClockRailway;
}

/* ============================================================================
**  Function : Clock_Config
** ============================================================================
*/
/*!
    Return a pointer to the configuration data.

   @param  None

   @retval a pointer to the configuration data

*/
Clock_ConfigType *Clock_Config( void )
{
  return &Clock_ConfigData;
}

/* ============================================================================
**  Function : Clock_SourceMapToMux
** ============================================================================
*/
boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
)
{
  if( (pConfig == NULL) ||
      (nMuxValue == NULL) ||
      pConfig->eMux >= NUM_MUX_TYPES )
  {
    return FALSE;
  }

  return Clock_MuxMap[pConfig->eMux](pConfig->eSource, nMuxValue);
}


/* ============================================================================
**  Function : Clock_MuxMapToSource
** ============================================================================
*/
boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
)
{
  if( (pConfig == NULL) ||
      pConfig->eMux >= NUM_MUX_TYPES )
  {
    return FALSE;
  }

  return Clock_SourceMap[pConfig->eMux](nSource,&(pConfig->eSource));
}


/* ============================================================================
**  Function : Clock_SourceMapToGCC
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the configuration data

*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
      break;
    case SRC_GPLL1:
      *nMuxValue = 4;
      break;
    case SRC_GPLL2:
      *nMuxValue = 2;
      break;
    case SRC_GPLL3:
      *nMuxValue = 3;
      break;
    case SRC_GPLL4:
      *nMuxValue = 5;
      break;      
    case SRC_GPLL0_DIV2:
      *nMuxValue = 6;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the configuration data

*/
boolean Clock_GCCMuxMapToSource(uint32 nSource, ClockSourceType* pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_GPLL0;
      break;
    case 2:
      *pSource = SRC_GPLL2;
      break;
    case 3:
      *pSource = SRC_GPLL3;
      break;
    case 4:
      *pSource = SRC_GPLL1;
      break;
    case 5:
      *pSource = SRC_GPLL4;
      break;
    case 6:
      *pSource = SRC_GPLL0_DIV2;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}


/*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the configuration data

*/
boolean Clock_MMSSMuxMapToSource(uint32 nSource,ClockSourceType *pSource)
{
  switch( nSource )
  {
    case 0:
      *pSource = SRC_CXO;
      break;
    case 1:
      *pSource = SRC_MMPLL0;
      break;
    case 4:
      *pSource = SRC_MMPLL8;
      break;
    case 5:
      *pSource = SRC_GPLL0;
      break;
    case 6:
      *pSource = SRC_GPLL0_DIV2;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_SourceMapToMMSS
** ============================================================================
*/
/*!
   Map a ClockSourceType into a physical mux setting for the MMSS muxes.

   @param  None

   @retval a pointer to the configuration data

*/
boolean Clock_SourceMapToMMSS(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_MMPLL0:
      *nMuxValue = 1;
      break;
    case SRC_MMPLL8:
      *nMuxValue = 4;
      break;
    case SRC_GPLL0:
      *nMuxValue = 5;
      break;
    case SRC_GPLL0_DIV2:
      *nMuxValue = 6;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_EnableSource
** ============================================================================
*/

boolean Clock_EnableSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;

    case SRC_GPLL0:
    case SRC_GPLL0_DIV2:
      rtrn = Clock_EnablePLL(&Clock_ConfigData.PLL0_Cfg);
      /* Enable AUX2 for HMSS clock use */
      HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX2, 1);
      
      break;

    case SRC_GPLL1:
      rtrn = Clock_EnablePLL(&Clock_ConfigData.PLL1_Cfg);
      break;

    case SRC_GPLL2:
      rtrn = Clock_EnablePLL(&Clock_ConfigData.PLL2_Cfg);
      break;

    case SRC_GPLL3:
      rtrn = Clock_EnablePLL(&Clock_ConfigData.PLL3_Cfg);
      break;

    case SRC_GPLL4:
      rtrn = Clock_EnablePLL(&Clock_ConfigData.PLL4_Cfg);
      break;

    case SRC_MMPLL8:
        rtrn = Clock_EnablePLL(&Clock_ConfigData.MMPLL8_Cfg_V2);
      break;

    default:
      break;
  }
  return rtrn;
}

/* ============================================================================
**  Function : Clock_DisableSource
** ============================================================================
*/
boolean Clock_DisableSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;

    case SRC_GPLL0:
    case SRC_GPLL0_DIV2:
      /* This is so very unlikely, and liable to break everything that
       * I am refusing to implement off for GPLL0 */
      break;

    case SRC_GPLL1:
      rtrn = Clock_DisablePLL(&Clock_ConfigData.PLL1_Cfg);
      break;

    case SRC_GPLL2:
      rtrn = Clock_DisablePLL(&Clock_ConfigData.PLL2_Cfg);
      break;

    case SRC_GPLL3:
      rtrn = Clock_DisablePLL(&Clock_ConfigData.PLL3_Cfg);
      break;

    case SRC_GPLL4:
      rtrn = Clock_DisablePLL(&Clock_ConfigData.PLL4_Cfg);
      break;

    case SRC_MMPLL8:
        rtrn = Clock_DisablePLL(&Clock_ConfigData.MMPLL8_Cfg_V2);
      break;

    default:
      break;
  }
  return rtrn;
}

/* ============================================================================
**  Function : Clock_ConfigureSource
** ============================================================================
*/
boolean Clock_ConfigureSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;
    case SRC_GPLL0:
    case SRC_GPLL0_DIV2:
      rtrn = Clock_ConfigurePLL(&Clock_ConfigData.PLL0_Cfg);
      break;
    case SRC_GPLL1:
      rtrn = Clock_ConfigurePLL(&Clock_ConfigData.PLL1_Cfg);
      break;
    case SRC_GPLL2:
      rtrn = Clock_ConfigurePLL(&Clock_ConfigData.PLL2_Cfg);
      break;
    case SRC_GPLL3:
      rtrn = Clock_ConfigurePLL(&Clock_ConfigData.PLL3_Cfg);
      break;
    case SRC_GPLL4:
        rtrn = Clock_ConfigurePLL(&Clock_ConfigData.PLL4_Cfg);
      break;
    case SRC_MMPLL8:
        rtrn = Clock_ConfigurePLL(&Clock_ConfigData.MMPLL8_Cfg_V2);
      break;

    default:
      break;
  }
  return rtrn;
}
