/*
===========================================================================
  @file ClockBIMC.c

  This file provides clock initialization for the Apps.
===========================================================================

  Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================


  when       who     what, where, why
  --------   ---     ------------------------------------------------------
  08/27/15   vph     Remove V1 frequency plan since nolong support V1 HW.
  04/17/15   vph     Add support for V2 DDR frequencies
  07/07/14   vph     Initial revision

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockBoot.h"
#include "ClockBIMC.h"
#include "ClockHWIO.h"
#include "ClockBSP.h"
#include "ddr_drivers.h"
#include "ddr_common.h"
#include "railway.h"
#include "smem.h"
#include "boot_util.h"
#include "railway.h"

/*=========================================================================
      Macro Definitions
==========================================================================*/
/*
 * HALF_DIVIDER
 *
 * Macro to return the normalized half divider for a given mux structure.
 * NOTE: Expecting (2 * divider) value as input.
 */
#define HALF_DIVIDER(mux)  ((mux)->nDiv2x ? (((mux)->nDiv2x) - 1) : 0)

/*
 * Common root clock command fields/masks (*_CMD_RCGR)
 */
#define HAL_CLK_CMD_CGR_ROOT_OFF_FMSK                                  0x80000000
#define HAL_CLK_CMD_CGR_ROOT_EN_FMSK                                   0x00000002
#define HAL_CLK_CMD_CFG_UPDATE_FMSK                                    0x00000001
#define HAL_CLK_CMD_CFG_UPDATE_SHFT                                    0

/*
 * Common root clock config fields/masks (*_CFG_RCGR)
 */
#define HAL_CLK_CFG_CGR_MODE_FMSK                                      0x00003000
#define HAL_CLK_CFG_CGR_MODE_SHFT                                      0xc
#define HAL_CLK_CFG_CGR_SRC_SEL_FMSK                                   0x00000700
#define HAL_CLK_CFG_CGR_SRC_SEL_SHFT                                   0x8
#define HAL_CLK_CFG_CGR_SRC_DIV_FMSK                                   0x0000001F
#define HAL_CLK_CFG_CGR_SRC_DIV_SHFT                                   0

#define HAL_CLK_CFG_REG_OFFSET                                        0x4

/*
 * Supported PLL pre/post dividers.
 */
#define CLK_PLL_PREDIV_1      1
#define CLK_PLL_PREDIV_2      2
#define CLK_PLL_POSTDIV_1     1
#define CLK_PLL_POSTDIV_2     2
#define CLK_PLL_POSTDIV_4     4

/*
 * 'ClockDDRCPLLConfigType' accessors used for repurposed bit-fields in
 * 'HAL_clk_ClockMuxConfigType'. Provides RPM HAL compatibility.
 */
#define HAL_CLK_BIMC_DIV2X_VAL(ddrcc_mode, ddrcc_idx, ddr_div2x) \
  ((((ddrcc_mode) & 0x1) << 15) | (((ddrcc_idx) & 0x7F) << 8) | ((ddr_div2x) & 0xFF))
#define HAL_CLK_BIMC_DIV2X_DDRCC_MODE_EN(div2x) (((div2x) & 0x8000) >> 15)
#define HAL_CLK_BIMC_DIV2X_DDRCC_IDX(div2x)     (((div2x) & 0x7F00) >> 8)
#define HAL_CLK_BIMC_DIV2X_DDR_DIV2X(div2x)     ((div2x) & 0x00FF)
#define HAL_CLK_BIMC_DIV2X_DDR_DIV2X_MASK       0xFF

#define HAL_CLK_BIMC_M_VAL(gpll23_idx, bimc_div2x, hmss_div2x) \
  ((((gpll23_idx) & 0xF) << 12) | (((bimc_div2x) & 0x3F) << 6) | ((hmss_div2x) & 0x3F))
#define HAL_CLK_BIMC_M_GPLL23_IDX(m)          (((m) & 0xF000) >> 12)
#define HAL_CLK_BIMC_M_BIMC_DIV2X(m)          (((m) & 0x0FC0) >> 6)
#define HAL_CLK_BIMC_M_HMSS_DIV2X(m)          ((m) & 0x003F)

#define HAL_CLK_BIMC_N_VAL(gfx_div2x, mpss_div2x) \
  ((((gfx_div2x) & 0xFF) << 8) | ((mpss_div2x) & 0xFF))
#define HAL_CLK_BIMC_N_GFX_DIV2X(n)           (((n) & 0xFF00) >> 8)
#define HAL_CLK_BIMC_N_MPSS_DIV2X(n)          ((n) & 0x00FF)

#define CLOCK_TABLE(table) {table, sizeof(table), sizeof(table[0]), sizeof(table)/sizeof(table[0]) }

/* This ClockPLLTableType captures the table, and necessary sizeof parameters so that we
 * can swap in tables for V2 versus V1 */
typedef struct 
{
  const RPMClockSourceConfigType *table;  /* Pointer to data */
  const uint32 size;                      /* sizeof(table) */
  const uint32 item_size;                 /* size of one array element */
  const uint32 count;                     /* The sizeof(array) / sizeof(array[0]) */
} ClockPLLTableType;

/* This RPMClockMuxConfigType captures the table, and necessary sizeof parameters so that we
 * can swap in tables for V2 versus V1 */
typedef struct 
{
  RPMClockMuxConfigType *table;  /* Pointer to data */
  const uint32 size;             /* sizeof(table) */
  const uint32 item_size;        /* size of one array element */
  const uint32 count;            /* The sizeof(array) / sizeof(array[0]) */
} ClockRPMTableType;

/*=========================================================================
     Externs
==========================================================================*/

/*=========================================================================
      Function Prototypes
==========================================================================*/

boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue);

/*=========================================================================
      Data
==========================================================================*/
#define MAX_DDR_LEVELS 12

/* Min and Max support DDR Index */
static uint8 nDDRMinIndex = 0;
static uint8 nDDRMaxIndex = MAX_DDR_LEVELS;

/*
 * PLL configurations to be passed to DSF for their internal PLLs.
 */
static const ClockDSFConfigType DSFConfig[] =
{
  {   0, 0,   0,  0,   0, 0,  0,  0 }, // Freq = 100MHz
  {   0, 0,   0,  0,   0, 0,  0,  0 }, // Freq = 150MHz
  {   0, 0,   0,  0,   0, 0,  0,  0 }, // Freq = 200MHz
  {   0, 0,   0,  0,   0, 0,  0,  0 }, // Freq = 300MHz
  {  86, 0, 154,  8,  57, 3, 35, 40 }, // Freq = 412.8MHz 
  { 114, 0, 102, 11,  70, 4, 35, 44 }, // Freq = 547.2MHz
  {  71, 0,  26,  7, 169, 2, 19, 40 }, // Freq = 681.6MHz
  {  80, 0,  0,   8,   0, 3, 19, 40 }, // Freq = 768MHz
  { 106, 0, 154, 10, 249, 3, 19, 44 }, // Freq = 1017.6MHz
  { 135, 0, 128, 13,  16, 5, 19, 47 }, // Freq = 1296MHz
  {  81, 0,  26,  8,   9, 3,  3, 40 }, // Freq = 1555.2MHz
  {  94, 0, 102,  9, 134, 3,  3, 44 }  // Freq = 1804.8MHz
};

const RPMClockSourceConfigType BIMCPLLConfig_V2[] =
{
  /* ==============================================================================================================================================================================
  **  { eSource,          { eSource,           eVCO,                 nPreDiv,         nPostDiv,  nL, nM, nN nVCOMultiplier, nAlpha, nAlphaU}, nConfigMask, nFreqHz, ...eVRegLevel }
  ** =============================================================================================================================================================================*/
  { HAL_CLK_SOURCE_GPLL2, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO_MODE_2, CLK_PLL_PREDIV_1, CLK_PLL_POSTDIV_4, 0x29, 0, 0, 0, 0xAAAAAAAA, 0xAA  }, CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,  200000000}, // Use PostDiv=4 for better DDR PLL duty cycle
  { HAL_CLK_SOURCE_GPLL2, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO_MODE_2, CLK_PLL_PREDIV_1, CLK_PLL_POSTDIV_2, 0x2B, 0, 0, 0, 0x0,        0x0   }, CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,  412800000}, // Use PostDiv=2 for better DDR PLL duty cycle
  { HAL_CLK_SOURCE_GPLL2, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO_MODE_1, CLK_PLL_PREDIV_1, CLK_PLL_POSTDIV_2, 0x39, 0, 0, 0, 0x0,        0x0   }, CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,  547200000}, // Use PostDiv=2 for better DDR PLL duty cycle
  { HAL_CLK_SOURCE_GPLL2, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO_MODE_1, CLK_PLL_PREDIV_1, CLK_PLL_POSTDIV_1, 0x3E, 0, 0, 0, 0x0,        0x80  }, CLOCK_CONFIG_PLL_FSM_MODE_ENABLE, 1200000000},
  { HAL_CLK_SOURCE_GPLL2, { HAL_CLK_SOURCE_XO, HAL_CLK_PLL_VCO_MODE_0, CLK_PLL_PREDIV_1, CLK_PLL_POSTDIV_1, 0x61, 0, 0, 0, 0x0,        0x00  }, CLOCK_CONFIG_PLL_FSM_MODE_ENABLE, 1862400000},
};
const ClockPLLTableType BIMCPLLConfigTbl_V2 = CLOCK_TABLE(BIMCPLLConfig_V2);

ClockPLLTableType const *BIMCPLLConfigTbl = &BIMCPLLConfigTbl_V2; 

/*
 * DDR CPLL Clock configurations.
 *
** =========================================================================
**
** 'HAL_clk_ClockMuxConfigType' repurposing (allows RPM-HAL compatibilty):
**    nDiv2x[15]    - 0 for GCC, and 1 for DDRCC mode
**    nDiv2x[15:8]  - index to DDRCC config (zeroed out for RPM)
**    nDiv2x[7:0]   - DDR nDiv2x (placed here for RPM detect mux config)
**
**    nM[15:12]     - index to GPLL23 config
**    nM[11:6]      - BIMC nDiv2x
**    nM[5:0]       - HMSS nDiv2x
**
**    nN[15:8]      - GFX nDiv2x
**    nN[7:0]       - MPSS nDiv2x
**
**    n2D[15:0]     - VDDA EBI vote
**
** =========================================================================*/
RPMClockMuxConfigType DDRClockConfig_V2[] =
{
  /* =====================================================================================================================================================================
  ** {freq_Hz,     {source,        DDRCCPLL_idx:DDR_div2x       DDRCC_mode:GPLL23_idx:APSS_div2x,     GFX_div2x:MPSS_div2x,         VDDA_EBI_vote},           VDDCX_vote }
  ** ====================================================================================================================================================================*/
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2, HAL_CLK_BIMC_DIV2X_VAL(0,  0,  6), HAL_CLK_BIMC_M_VAL(0, 16, 12), HAL_CLK_BIMC_N_VAL(24, 12), CLOCK_VREG_LEVEL_LOW_MINUS }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  150000000, { HAL_CLK_SOURCE_GPLL0_DIV2, HAL_CLK_BIMC_DIV2X_VAL(0,  1,  4), HAL_CLK_BIMC_M_VAL(0, 12,  8), HAL_CLK_BIMC_N_VAL(16,  8), CLOCK_VREG_LEVEL_LOW_MINUS }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  200000000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(0,  2,  2), HAL_CLK_BIMC_M_VAL(0,  6,  4), HAL_CLK_BIMC_N_VAL( 8,  4), CLOCK_VREG_LEVEL_LOW_MINUS }, CLOCK_VREG_LEVEL_LOW_MINUS },
  {  300000000, { HAL_CLK_SOURCE_GPLL0,      HAL_CLK_BIMC_DIV2X_VAL(0,  3,  4), HAL_CLK_BIMC_M_VAL(0, 12,  8), HAL_CLK_BIMC_N_VAL(16,  8), CLOCK_VREG_LEVEL_LOW_MINUS }, CLOCK_VREG_LEVEL_LOW       },

  // DDRCC mode levels, new  
  {  412800000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  4, 16), HAL_CLK_BIMC_M_VAL(3,  8,  6), HAL_CLK_BIMC_N_VAL(12, 12), CLOCK_VREG_LEVEL_LOW       }, CLOCK_VREG_LEVEL_LOW       },
  {  547200000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  5, 18), HAL_CLK_BIMC_M_VAL(3,  8,  6), HAL_CLK_BIMC_N_VAL(12, 12), CLOCK_VREG_LEVEL_LOW       }, CLOCK_VREG_LEVEL_LOW	    },
  {  681600000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  6, 20), HAL_CLK_BIMC_M_VAL(3,  8,  6), HAL_CLK_BIMC_N_VAL(12, 12), CLOCK_VREG_LEVEL_LOW       }, CLOCK_VREG_LEVEL_LOW       },
  {  768000000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  7, 22), HAL_CLK_BIMC_M_VAL(3,  8,  6), HAL_CLK_BIMC_N_VAL(12,  8), CLOCK_VREG_LEVEL_NOMINAL   }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 1017600000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  8, 24), HAL_CLK_BIMC_M_VAL(3,  4,  3), HAL_CLK_BIMC_N_VAL( 6,  6), CLOCK_VREG_LEVEL_NOMINAL   }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 1296000000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1,  9, 26), HAL_CLK_BIMC_M_VAL(3,  4,  3), HAL_CLK_BIMC_N_VAL( 6,  6), CLOCK_VREG_LEVEL_NOMINAL   }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 1555200000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1, 10, 28), HAL_CLK_BIMC_M_VAL(3,  4,  3), HAL_CLK_BIMC_N_VAL( 6,  6), CLOCK_VREG_LEVEL_NOMINAL   }, CLOCK_VREG_LEVEL_NOMINAL   },
  { 1804800000, { HAL_CLK_SOURCE_GPLL2,      HAL_CLK_BIMC_DIV2X_VAL(1, 11, 30), HAL_CLK_BIMC_M_VAL(4,  5,  4), HAL_CLK_BIMC_N_VAL( 7,  7), CLOCK_VREG_LEVEL_HIGH      }, CLOCK_VREG_LEVEL_HIGH      }, 
  { 0 }
};
const ClockRPMTableType DDRClockConfigTbl_V2 = CLOCK_TABLE(DDRClockConfig_V2);

ClockRPMTableType const *DDRClockConfigTbl = &DDRClockConfigTbl_V2;

/* The current rate of the DRR clock in kHz */
static uint32 ddr_speed_khz = 200000;

/*=========================================================================
      Function Definitions
=========================================================================*/

/* =========================================================
**  Function : Clock_InitBIMCPlan()
** =======================================================*/
/* Choose between the V1 and V2 clock plans based on chip revision.
 *
 * @param None
 * @return None
 */
void Clock_InitBIMCPlan( void )
{
  // Do nothing, since V1 is no longer supported
}


/* =========================================================
**  Function : Clock_SetDDRMinMax()
** =======================================================*/
/* 
 * Function is used for setting minimum and maximum DDR speed.
 *
 * @param 
 *   nDDRMin - Minimum DDR speed index.
 *   nDDRMax - Maximum DDR speed index.
 * @return 
 *   TRUE  - Min and Max are in supporting range
 *   FALSE - Min and Max are NOT in supporting range.  No setting effective.
 */
boolean Clock_SetDDRMinMax( uint8 nDDRMin, uint8 nDDRMax )
{
  /* Check if Min and Max are in supportive range */
  if ( (nDDRMin > nDDRMax) || (nDDRMin > MAX_DDR_LEVELS) )
  {
    return FALSE;
  }

  /* Over-write Max if it is higher than supportive levels */
  if ( nDDRMax > MAX_DDR_LEVELS )
  {
    nDDRMax = MAX_DDR_LEVELS;
  }

  nDDRMinIndex = nDDRMin;
  nDDRMaxIndex = nDDRMax;

  return TRUE;
}


/* =========================================================
**  Function : Clock_GetDDRMinMax()
** =======================================================*/
/* 
 * Function is used for getting minimum and maximum DDR level index.
 *
 * @param 
 *   pDDRMin - Pointer to Minimum DDR speed index.
 *   pDDRMax - Pointer to Maximum DDR speed index.
 * @return 
 *   None
 */
void Clock_GetDDRMinMax( uint8 *pDDRMin, uint8 *pDDRMax )
{
  *pDDRMin = nDDRMinIndex;
  *pDDRMax = nDDRMaxIndex;
}


/* =========================================================
**  Function : Clock_DDRSpeed()
** =======================================================*/
/* 
 * @param None
 * @return The clock rate of DDR in kHz
 */
uint32 Clock_DDRSpeed()
{
  return ddr_speed_khz;
}

void Clock_StoreDDRFreqKHz ( uint32 nFreqKHz )
{
  ddr_speed_khz = nFreqKHz;
}

/* ========================================================================
**  Function : Clock_BIMCConfigFSM
** ======================================================================*/
/*
    Description: Configure BIMC to enable the DDR FSM.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_BIMCConfigFSM( void )
{
  /* Enable the FSM */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 0);

  /* JCPLL clocks should be disabled after the FSM is turned on.
   * This gives the FSM control over these clocks */
  HWIO_OUTF(GCC_BIMC_DDR_CPLL0_CBCR, CLK_ENABLE, 0);
  HWIO_OUTF(GCC_BIMC_DDR_CPLL1_CBCR, CLK_ENABLE, 0);
}


/* ========================================================================
**  Function : Clock_BIMCIsFSMConfigured
** ======================================================================*/
/*
    Description: Find out if BIMC is configured enable the DDR FSM.

    @param None
    @return TRUE if FSM is enabled and FALSE otherwise.

    @dependencies
    None.

    @sa None
*/
boolean Clock_BIMCIsFSMConfigured( void )
{
  uint32 nVal;

  /* Query if the FSM is enabled */
  nVal = HWIO_INF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS);

  if (nVal == 0) 
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

/* ============================================================================
**  Function : Clock_BIMCQuery
** ============================================================================
*/
/*!
 
  This function lets the client query the BIMC frequency plan and the number 
  of supported frequencies.
  @return
  NONE
  @dependencies
  None.

*/
void Clock_BIMCQuery(ClockQueryType nQuery, void* pResource)
{
  uint32 nIdx;
  uint32 eDDRCCModeEn;
  uint32 nPLLIdx;
  uint32 *pnData;
  ClockPlanType *pBimcClkPlan;

  /* Return early if no output buffer is given. */
  if ( pResource == NULL )
  {
    return;
  }

  switch ( nQuery )
  {
    case CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS:
      pnData = (uint32 *)pResource;
      for ( nIdx = 0; (DDRClockConfigTbl->table[nIdx].nFreqHz != 0); nIdx++ );

      *pnData = nIdx;
      break;

    case CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ:
      pBimcClkPlan = (ClockPlanType*)pResource;

      for ( nIdx = 0; (DDRClockConfigTbl->table[nIdx].nFreqHz != 0); nIdx++ )
      {
        eDDRCCModeEn = HAL_CLK_BIMC_DIV2X_DDRCC_MODE_EN(DDRClockConfigTbl->table[nIdx].HALConfig.nDiv2x);

        pBimcClkPlan[nIdx].nFreqKHz   = (DDRClockConfigTbl->table[nIdx].nFreqHz / 1000);
        pBimcClkPlan[nIdx].eVRegLevel = DDRClockConfigTbl->table[nIdx].eVRegLevel;
        pBimcClkPlan[nIdx].eMode      = eDDRCCModeEn;

        nPLLIdx = HAL_CLK_BIMC_DIV2X_DDRCC_IDX(DDRClockConfigTbl->table[nIdx].HALConfig.nDiv2x);

        qmemcpy ( &(pBimcClkPlan[nIdx].sDSFConfig), &(DSFConfig[nPLLIdx]), sizeof(ClockDSFConfigType) );
      }
      break;

    default:
      return;
  }
}

/* ============================================================================
**  Function : Clock_EnableBIMCSource
** ============================================================================
*/
/**
  Configures and enables a BIMC PLL. Handles GPLL2/GPLL3 ping-pong.

  @params
    pNewCfg           [in]  - BIMC config to prepare.
    pOrigCfg          [in]  - Previous BIMC config.
    peSourceToDisable [out] - BIMC source that might require disabling after
                              the clock switch via Clock_DisableBIMCSources().

  @return
    True upon success.

  @dependencies
    None.

  @sa
    None
*/
static boolean Clock_EnableBIMCSource
(
  RPMClockMuxConfigType *pNewCfg,
  RPMClockMuxConfigType *pOrigCfg,
  HAL_clk_SourceType    *peSourceToDisable
)
{
  uint32               nPLLCfgIdx;
  Clock_ConfigType    *pClockCfg;
  ClockConfigPLLType  *pPLLCfg;
  ClockSourceType     ePLL;
  HAL_clk_SourceType  eNewSource = pNewCfg->HALConfig.eSource;
  RPMClockSourceConfigType const *pSource;  /* cache the PLL */

  /* Nothing to do for GPLL0/XO (enabled in PBL). */
  if ( (eNewSource == HAL_CLK_SOURCE_XO) ||
       (eNewSource == HAL_CLK_SOURCE_GPLL0) ||
       (eNewSource == HAL_CLK_SOURCE_GPLL0_DIV2))
  {
    *peSourceToDisable = pOrigCfg->HALConfig.eSource;
    return TRUE;
  }

  /* Else PLL must be GPLL2 or GPLL3. */
  if ( (eNewSource != HAL_CLK_SOURCE_GPLL2) &&
       (eNewSource != HAL_CLK_SOURCE_GPLL3) )
  {
    return FALSE;
  }

  pClockCfg = Clock_Config();

  /* Switch to whichever of GPLL2/GPLL3 is currently inactive. */
  if ( HWIO_INF(GCC_GPLL2_MODE, PLL_ACTIVE_FLAG) == 0 )
  {
    pNewCfg->HALConfig.eSource = HAL_CLK_SOURCE_GPLL2;
    *peSourceToDisable = HAL_CLK_SOURCE_GPLL3;

    pPLLCfg = &pClockCfg->PLL2_Cfg;
    ePLL = SRC_GPLL2;
  }
  else if ( HWIO_INF(GCC_GPLL3_MODE, PLL_ACTIVE_FLAG) == 0 )
  {
    pNewCfg->HALConfig.eSource = HAL_CLK_SOURCE_GPLL3;
    *peSourceToDisable = HAL_CLK_SOURCE_GPLL2;

    pPLLCfg = &pClockCfg->PLL3_Cfg;
    ePLL = SRC_GPLL3;
  }
  else
  {
    /* Both GPLL2 and GPLL3 are active => bug. */
    return FALSE;
  }

  /* Set new configuration for selected PLL. */
  nPLLCfgIdx = HAL_CLK_BIMC_M_GPLL23_IDX(pNewCfg->HALConfig.nM);
  
  pSource = &BIMCPLLConfigTbl->table[nPLLCfgIdx];
  pPLLCfg->nVCO     = pSource->HALConfig.eVCO;
  pPLLCfg->nL       = pSource->HALConfig.nL;
  pPLLCfg->nAlpha   = CLK_64B_ALPHA(pSource->HALConfig.nAlphaU,
                                    pSource->HALConfig.nAlpha);
  pPLLCfg->nPostDiv = pSource->HALConfig.nPostDiv;

  if ( !Clock_ConfigureSource(ePLL) ) return FALSE;

  return Clock_EnableSource( (ClockSourceType)pNewCfg->HALConfig.eSource );
}

/* ============================================================================
**  Function : Clock_DisableBIMCSource
** ============================================================================
*/
/**
  Disables a BIMC PLL if necessary.

  @params
    eSource [in]  - BIMC source to disable.

  @return
    True upon success.

  @dependencies
    None.

  @sa
    None
*/
static boolean Clock_DisableBIMCSource
(
  HAL_clk_SourceType    eSource
)
{
  /* Never disable GPLL0/XO. */
  if ( (eSource == HAL_CLK_SOURCE_XO) ||
       (eSource == HAL_CLK_SOURCE_GPLL0) ||
       (eSource == HAL_CLK_SOURCE_GPLL0_DIV2))
  {
    return TRUE;
  }

  /* Else PLL must be GPLL2 or GPLL3. */
  if ( (eSource != HAL_CLK_SOURCE_GPLL2) &&
       (eSource != HAL_CLK_SOURCE_GPLL3) )
  {
    return FALSE;
  }

  return Clock_DisableSource( (ClockSourceType)eSource );
}

/**
  This function is used for switching all the buses to different 
  voltage level for the ddr driver (LOW and HIGH) for DDR SITE
  training. 

  @param 
    eVoltageLevel - Clock Voltage Level (LOW and HIGH)

  @return
    None

  @dependencies
    None

  @sa
    None
*/
void Clock_SwitchBusVoltage( ClockVRegLevelType eVoltageLevel )
{
  Clock_RailwayType *pClockRailway;

  pClockRailway = Clock_RailwayConfig();

  /* Do nothing if the railway is not supported yet */
  if ( RAIL_NOT_SUPPORTED_BY_RAILWAY == pClockRailway->nCxRailId )
  {
    return;
  }

  if (eVoltageLevel <= CLOCK_VREG_LEVEL_LOW) 
  {
    Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MIN);
    Clock_SetSysPerfLevel(CLOCK_BOOT_PERF_MIN);

    // setup the vote for corner voltage with railway.
    railway_corner_vote(pClockRailway->CxVoter, (railway_corner)eVoltageLevel);

    // Always need to transition the rail after updating the votes for them to be effective.
    railway_transition_rails();
  }
  else if (eVoltageLevel >= CLOCK_VREG_LEVEL_NOMINAL) 
  {
    // setup the vote for corner voltage with railway.
    railway_corner_vote(pClockRailway->CxVoter, (railway_corner)eVoltageLevel);

    // Always need to transition the rail after updating the votes for them to be effective.
    railway_transition_rails();

    Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MAX);
    Clock_SetSysPerfLevel(CLOCK_BOOT_PERF_MAX);
  } 
}

/* =========================================================================
**  Function : HAL_clk_DDRSetConfigMux
** =========================================================================*/
/*!
    Configure BIMC clock domain.

    @param 
      nCmdCGRAddr     - Clock CMD_CGR address
      nSourceIndex    - Source Index
      nDiv2x          - Divider

    @return
      None.

    @dependencies

    @sa None
*/

static void HAL_clk_DDRSetConfigMux
(
  uintnt nCmdCGRAddr,
  uint32 nSourceIndex,
  uint32 nDiv2x
)
{
  uintnt nCmdCGRVal;
  uintnt nCfgCGRAddr = (nCmdCGRAddr + HAL_CLK_CFG_REG_OFFSET);
  uintnt nCfgCGRVal  = inp32(nCfgCGRAddr);

  /*
   * Clear the CFG_CGR fields.
   */
  nCfgCGRVal &= ~(HAL_CLK_CFG_CGR_SRC_SEL_FMSK |
                  HAL_CLK_CFG_CGR_SRC_DIV_FMSK |
                  HAL_CLK_CFG_CGR_MODE_FMSK);

  /* Convert divider to HAL format. */
  if ( nDiv2x > 0 )
    nDiv2x--;

  /* Enforce max half-integer divider of 16 (31 in HAL format). */
  if ( nDiv2x > 31 )
    nDiv2x = 31;

  /*
   * Program the source and divider values.
   */
  nCfgCGRVal |= ((nSourceIndex << HAL_CLK_CFG_CGR_SRC_SEL_SHFT) & HAL_CLK_CFG_CGR_SRC_SEL_FMSK);
  nCfgCGRVal |= ((nDiv2x << HAL_CLK_CFG_CGR_SRC_DIV_SHFT) & HAL_CLK_CFG_CGR_SRC_DIV_FMSK);

  /*
   * Write the final CFG register value.
   */
  outp32(nCfgCGRAddr, nCfgCGRVal);

  /*
   * Trigger the update.
   */
  nCmdCGRVal = inp32(nCmdCGRAddr);
  nCmdCGRVal |= HAL_CLK_CMD_CFG_UPDATE_FMSK;
  outp32(nCmdCGRAddr, nCmdCGRVal);

  /*
   * Wait until update finishes.
   */
  while(inp32(nCmdCGRAddr) & HAL_CLK_CMD_CFG_UPDATE_FMSK);
}

/* =========================================================================
**  Function : HAL_clk_DDRConfigMux
** =========================================================================*/
/*!
    Programs 

    @param
      pmConfig -  [IN] Clock mux config structure

    @return
      None.

    @dependencies
      None.

    @sa
      None.
*/

static boolean HAL_clk_DDRConfigMux
(
  HAL_clk_ClockMuxConfigType *pmConfig
)
{
  uint32 nLegacyModeEnabled;
  uint32 nSourceIndex;

  if ( pmConfig == NULL )
    return FALSE;

  /* Get source index from source enum. */
  if ( !Clock_SourceMapToGCC((ClockSourceType)pmConfig->eSource, &nSourceIndex) )
    return FALSE;

  /* Set GCC(0) or DDRCC(1) mode */
  nLegacyModeEnabled = HAL_CLK_BIMC_DIV2X_DDRCC_MODE_EN(pmConfig->nDiv2x)? 0 : 1;
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_LEGACY_MODE_EN, nLegacyModeEnabled);

  /* Set GCC_BIMC_DDR_CPLL config. */
  HAL_clk_DDRSetConfigMux((uintnt)HWIO_ADDR(GCC_BIMC_DDR_CPLL_CMD_RCGR),
                          nSourceIndex,
                          HAL_CLK_BIMC_DIV2X_DDR_DIV2X(pmConfig->nDiv2x));

  /* Set GCC_BIMC config. */
  HAL_clk_DDRSetConfigMux((uintnt)HWIO_ADDR(GCC_BIMC_CMD_RCGR),
                          nSourceIndex,
                          HAL_CLK_BIMC_M_BIMC_DIV2X(pmConfig->nM));

  /* Set BIMC_HMSS_AXI config. */
  HAL_clk_DDRSetConfigMux((uintnt)HWIO_ADDR(GCC_BIMC_HMSS_AXI_CMD_RCGR),
                          nSourceIndex,
                          HAL_CLK_BIMC_M_HMSS_DIV2X(pmConfig->nM));

  /* Set GCC_MMSS_BIMC_GFX config. */
  HAL_clk_DDRSetConfigMux((uintnt)HWIO_ADDR(GCC_MMSS_BIMC_GFX_CMD_RCGR),
                          nSourceIndex,
                          HAL_CLK_BIMC_N_GFX_DIV2X(pmConfig->nN));

  /* Set GCC_MSS_Q6_BIMC_AXI config. */
  HAL_clk_DDRSetConfigMux((uintnt)HWIO_ADDR(GCC_MSS_Q6_BIMC_AXI_CMD_RCGR),
                          nSourceIndex,
                          HAL_CLK_BIMC_N_MPSS_DIV2X(pmConfig->nN));

  if ( HWIO_INF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS) == 0 )
  {
    HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 0);
    HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 0);
  }

  return TRUE;
} /* HAL_clk_DDRConfigMux */

/* =========================================================================
**  Function : Clock_FindLevel
** =========================================================================*/
/*!
    Find the table line that meets or exceeds the request.

    @param -  nFreqHz [in] Frequency to search for in the table
    @param -  pConfigTable [in] Table to search
    @param -  nTableLimit [in] Length of table 
    @return - index to the table row to be used.

    @dependencies
    None.

    @sa None
*/
uint32 Clock_FindLevel
( 
  uint32 nFreqHz,
  ClockRPMTableType const *pRPMTable
)
{
  uint32 index = 0, nSupCfg = 0;
  RPMClockMuxConfigType const *pConfigTable;
  uint32 nTableLimit;

  if(pRPMTable == NULL) return 0;
  pConfigTable = pRPMTable->table;
  nTableLimit = pRPMTable->count;

  for(index = 0; (index < nTableLimit) && (pConfigTable[index].nFreqHz != 0); index++)
  {
    if( pConfigTable[index].nChipVersion == 0 )
    {
      nSupCfg = index;
    }
    if( (nSupCfg == index) && pConfigTable[index].nFreqHz >= nFreqHz )
    {
      return nSupCfg;
    }
  }

  if(pConfigTable[nSupCfg].nFreqHz == 0) 
  {
    /* End of the table is reached, use the last line, or line 0 if the table is empty */
    nSupCfg = (nSupCfg > 0 ? nSupCfg-1 : 0);
  }
  return nSupCfg;
}

/* =========================================================================
**  Function : Clock_SetBIMCSpeed
** =========================================================================*/
/*!
    Switch DDR and the related BIMC roots to the requested frequency

    @param -  nFreqHz [in] Frequency to search for in the table
    @return - FALSE on fail, TRUE on success

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetBIMCSpeed(uint32 nFreqKHz )
{
  uint32                   nNewLvlIdx, nCurrHz, nCurrKHz, nCurrLvlIdx;
  HAL_clk_SourceType       ePrevSource;
  RPMClockMuxConfigType   *pNewDDRCfg;
  RPMClockMuxConfigType   *pCurrDDRCfg;
  uint32                   nFreqHz;
  Clock_RailwayType       *pClockRailway = Clock_RailwayConfig();

  if ( RAIL_NOT_SUPPORTED_BY_RAILWAY == pClockRailway->nCxRailId )
    return TRUE;

  /* Find current level */
  nCurrKHz = Clock_DDRSpeed();
  nCurrHz = (1000 * nCurrKHz);
  nCurrLvlIdx = Clock_FindLevel ( nCurrHz, DDRClockConfigTbl );

  pCurrDDRCfg = &DDRClockConfigTbl->table[nCurrLvlIdx];
  if (nCurrHz != pCurrDDRCfg->nFreqHz) return FALSE;

  /* Find new level */
  nFreqHz = (nFreqKHz * 1000);
  nNewLvlIdx = Clock_FindLevel ( nFreqHz, DDRClockConfigTbl );
  pNewDDRCfg = &DDRClockConfigTbl->table[nNewLvlIdx];
  nFreqKHz = (pNewDDRCfg->nFreqHz / 1000);

  // Change VDDA_EBI rail if increase
  if ( (RAIL_NOT_SUPPORTED_BY_RAILWAY != pClockRailway->nEBIRailId) &&
       (DDRClockConfigTbl->table[nCurrLvlIdx].HALConfig.n2D < DDRClockConfigTbl->table[nNewLvlIdx].HALConfig.n2D) )
  {
    railway_corner_vote( pClockRailway->EBIVoter,
                         (railway_corner)DDRClockConfigTbl->table[nNewLvlIdx].HALConfig.n2D);
    railway_transition_rails();
  }

  /* 
   * Vote for higher voltage before the switch if the new frequency is more
   * than the current frequency.
   */
  if ( (RAIL_NOT_SUPPORTED_BY_RAILWAY != pClockRailway->nCxRailId) &&
       (nCurrKHz < nFreqKHz) )
  {
    /* CPU and Buses are running at NOMINAL or higher.  Do not scale voltage below NOMINAL */
    if ( pNewDDRCfg->eVRegLevel >= CLOCK_VREG_LEVEL_NOMINAL )
    {
      // Change VDD_CX rail, VDDA_EBI1 will follow
      railway_corner_vote(pClockRailway->CxVoter, (railway_corner)pNewDDRCfg->eVRegLevel);
      railway_transition_rails();
    }
  }

  if (!Clock_BIMCIsFSMConfigured()) 
  {
    /* Configure BIMC */
    Clock_BIMCConfigFSM();
  }

  /* Enable the next source before switch */
  if( !Clock_EnableBIMCSource(pNewDDRCfg,
                              pCurrDDRCfg,
                              &ePrevSource) )
  {
    return FALSE;
  }

  ddr_pre_clock_switch( nCurrKHz, nFreqKHz, SDRAM_INTERFACE_BOTH );
 
  if( !HAL_clk_DDRConfigMux( &pNewDDRCfg->HALConfig ) )
  {
    return FALSE;
  }

  /* Inform BIMC that new clock already switched */
  ddr_post_clock_switch( nCurrKHz, nFreqKHz, SDRAM_INTERFACE_BOTH );

  /* Disable previous DDR-dedicated sources after the switch */
  Clock_DisableBIMCSource(ePrevSource);

  /* 
   * Vote for lower voltage after the switch if the new frequency is less
   * than the current frequency.
   */
  if ( (RAIL_NOT_SUPPORTED_BY_RAILWAY != pClockRailway->nCxRailId) && 
       (nCurrKHz > nFreqKHz) )
  {
    /* CPU and Buses are running at NOMINAL or higher.  Do not scale voltage below NOMINAL */
    if ( pNewDDRCfg->eVRegLevel >= CLOCK_VREG_LEVEL_NOMINAL )
    {
      // Change VDD_CX rail, VDDA_EBI1 will follow
      railway_corner_vote(pClockRailway->CxVoter, (railway_corner)pNewDDRCfg->eVRegLevel);
      railway_transition_rails();
    }
  }

  // Change VDDA_EBI rail if decrease
  if ( (RAIL_NOT_SUPPORTED_BY_RAILWAY != pClockRailway->nEBIRailId) && 
       (DDRClockConfigTbl->table[nCurrLvlIdx].HALConfig.n2D > DDRClockConfigTbl->table[nNewLvlIdx].HALConfig.n2D) )
  {
    railway_corner_vote( pClockRailway->EBIVoter, 
                        (railway_corner)DDRClockConfigTbl->table[nNewLvlIdx].HALConfig.n2D);
    railway_transition_rails();
  }

  Clock_StoreDDRFreqKHz( nFreqKHz );

  return TRUE;
}

/* =========================================================================
**  Function : Clock_CopyBIMCPlanToRPM()
** =========================================================================*/
/*!
    Copy the BIMC clock plan into shared memory for the RPM to use.

    @param -  
    @return - FALSE on fail, TRUE on success

    @dependencies
    None.

    @sa None
*/
boolean Clock_CopyBIMCPlanToRPM()
{
  uint32 smem_size = 
    (sizeof(ClockBIMCConfigInfo) + 
    (sizeof(ClockBIMCTableHeaderType) * CLOCK_BIMC_NUM_TABLES) +
    BIMCPLLConfigTbl->size +
    DDRClockConfigTbl->size);

  uint32 tip;
  ClockBIMCConfigInfo *pBIMCConfigInfo;
  ClockBIMCTableHeaderType *pBIMCTableHeaders;
  void *pSMEM = smem_alloc(SMEM_CLOCK_INFO, smem_size);

  if(pSMEM == NULL) return FALSE;

  pBIMCConfigInfo = pSMEM;
  pBIMCConfigInfo->nVersion = CLOCK_BIMC_SMEM_VERSION;
  pBIMCConfigInfo->nNumConfig = CLOCK_BIMC_NUM_TABLES;
  pBIMCConfigInfo->nHALNumSrc = HAL_CLK_NUM_OF_SOURCES;
  pBIMCConfigInfo->nMinLevel = nDDRMinIndex;
  pBIMCConfigInfo->nMaxLevel = nDDRMaxIndex;
  pBIMCConfigInfo->nConfigData = sizeof(ClockBIMCConfigInfo);

  pBIMCTableHeaders = (pSMEM + sizeof(ClockBIMCConfigInfo));

  /* The dynamic tip of the smem copy sequence */
  tip = (sizeof(ClockBIMCConfigInfo) +
        (sizeof(ClockBIMCTableHeaderType) * CLOCK_BIMC_NUM_TABLES));

  pBIMCTableHeaders[0].nNodeSize = BIMCPLLConfigTbl->item_size;
  pBIMCTableHeaders[0].nNumNodes = BIMCPLLConfigTbl->count;
  pBIMCTableHeaders[0].eBIMCTable = CLOCK_BIMC_PLL;
  pBIMCTableHeaders[0].nNodeOffset = tip;
  qmemscpy((pSMEM + tip), (smem_size - tip), BIMCPLLConfigTbl->table, BIMCPLLConfigTbl->size);
  tip += BIMCPLLConfigTbl->size;

  pBIMCTableHeaders[1].nNodeSize = DDRClockConfigTbl->item_size;
  pBIMCTableHeaders[1].nNumNodes = DDRClockConfigTbl->count;
  pBIMCTableHeaders[1].eBIMCTable = CLOCK_BIMC_DDR;
  pBIMCTableHeaders[1].nNodeOffset = tip;
  qmemscpy((pSMEM + tip), (smem_size - tip), DDRClockConfigTbl->table, DDRClockConfigTbl->size);
  tip += DDRClockConfigTbl->size;

  if(tip == smem_size)
    return TRUE;
  else
    return FALSE;
}

#ifdef CLOCK_BIMC_TEST
boolean Clock_BIMCTest()
{
  uint32 nNumLevel;
  ClockPlanType aClockPlan[11];
  int i;

  /* Call BIMCQuery to get the Num Perf Levels */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);


  /* Call BIMCQuery to get the Clock Plan */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ, aClockPlan);

  for(i=0; i<nNumLevel; i++)
  {
    if(Clock_SetBIMCSpeed( aClockPlan[i].nFreqKHz ) == FALSE) return FALSE;
  }
  return TRUE;
}
#endif
