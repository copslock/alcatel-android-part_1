/** @file ClockSDI.c
  
  Clock functions for support System Debug Image driver.  Since SDI image is required
  to be every small memory footprint, clock drivers only brings in necessary code to
  support it.

  Copyright (c) 2015, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 02/20/15   vph     Initial revision

=============================================================================*/


/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "ClockHWIO.h"

/*=========================================================================
                       MACRO DEFINITIONS
==========================================================================*/
#define CLK_ENABLE_MSK 0x00000001
#define CLK_OFF_MSK    0x80000000

#define SPARK_CONFIG_CTL 0x4001051B

#define HWIO_PLL_MODE_OFFS                 (HWIO_ADDR(GCC_GPLL0_MODE) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_L_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_L_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_M_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_M_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_N_VAL_OFFS                (HWIO_ADDR(GCC_GPLL0_N_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_ALPHA_VAL_OFFS            (HWIO_ADDR(GCC_GPLL0_ALPHA_VAL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_ALPHA_VAL_U_OFFS          (HWIO_ADDR(GCC_GPLL0_ALPHA_VAL_U) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_USER_CTL_OFFS             (HWIO_ADDR(GCC_GPLL0_USER_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_CONFIG_CTL_OFFS           (HWIO_ADDR(GCC_GPLL0_CONFIG_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))
#define HWIO_PLL_TEST_CTL_OFFS             (HWIO_ADDR(GCC_GPLL0_TEST_CTL) - HWIO_ADDR(GCC_GPLL0_MODE))

#define CLOCK_PLL_MODE_ACTIVE  \
  (HWIO_FMSK(GCC_GPLL0_MODE, PLL_OUTCTRL)  | \
   HWIO_FMSK(GCC_GPLL0_MODE, PLL_BYPASSNL) | \
   HWIO_FMSK(GCC_GPLL0_MODE, PLL_RESET_N))
 
/*
 * Definitions for configuring the PLL in FSM Mode
 */
#define CLOCK_PLL_BIAS_COUNT_VAL  (0x6 << HWIO_SHFT(GCC_GPLL0_MODE, PLL_BIAS_COUNT))
#define CLOCK_PLL_LOCK_COUNT_VAL  (0x0 << HWIO_SHFT(GCC_GPLL0_MODE, PLL_LOCK_COUNT))

/*
 * NOT_2D / NOT_N_MINUS_M
 *
 * Macros to return the inverted value of the 2D field or (N - M)
 * in a type 1 mux structure.  Used to prepare the value for writing
 * to the hardware register field.
 */
#define NOT_2D(mux)         (~(mux)->n2D)
#define NOT_N_MINUS_M(mux)  (~((mux)->nN - (mux)->nM))


/*
 * HALF_DIVIDER
 *
 * Macro to return the normalized half divider for a given mux structure.
 * NOTE: Expecting (2 * divider) value as input.
 */
#define HALF_DIVIDER(mux)  ((mux)->nDiv2x ? (((mux)->nDiv2x) - 1) : 0)

/* Definitions for generalizing clock configuration */
#define CLOCK_CMD_CFG_UPDATE_FMSK   HWIO_FMSK(GCC_SDCC2_APPS_CMD_RCGR, UPDATE)

#define CLOCK_CFG_REG_OFFSET        (HWIO_ADDR(GCC_SDCC2_APPS_CFG_RCGR)-HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR))
#define CLOCK_CFG_CGR_SRC_SEL_FMSK  HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, SRC_SEL)
#define CLOCK_CFG_CGR_SRC_SEL_SHFT  HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, SRC_SEL)
#define CLOCK_CFG_CGR_SRC_DIV_FMSK  HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, SRC_DIV)
#define CLOCK_CFG_CGR_SRC_DIV_SHFT  HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, SRC_DIV)

#define CLOCK_CFG_CGR_MODE_FMSK     HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, MODE)
#define CLOCK_CFG_CGR_MODE_SHFT     HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, MODE)
#define CLOCK_CFG_CGR_MODE_DUAL_EDGE_VAL  0x2
#define CLOCK_CFG_CGR_MODE_BYPASS_VAL     0x0

#define CLOCK_M_REG_OFFSET         (HWIO_ADDR(GCC_SDCC2_APPS_M)-HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR))
#define CLOCK_N_REG_OFFSET         (HWIO_ADDR(GCC_SDCC2_APPS_N)-HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR))
#define CLOCK_D_REG_OFFSET         (HWIO_ADDR(GCC_SDCC2_APPS_D)-HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR))

/*
 * ClockConfigPLLType
 *
 * Parameters used for configuring a source that is a PLL.
 *
 *  nPLLModeAddr  - The address of the PLL MODE register.
 *  nVoteAddr     - The address of the PLL voting register. Set to NULL for 
 *                  non-FSM mode.
 *  nVoteMask     - The mask of the voting bit.
 *  eVCO          - The internal VCO to use.
 *  nPreDiv       - The pre-divider value (generally 1 or 2).
 *  nPostDiv      - The pre-divider value (generally 1 or 2).
 *  nL            - The L value for the PLL.  The PLL output frequency is derived
 *                  as out_freq = ((in_freq / nPreDiv) * (L + M/N)) / nPostDiv.
 *  nM            - The M value (see above).
 *  nN            - The N value (see above).
 *  nAlpha        - The Alpha(prog) value. For the 20nm PLLs, the output frequency is
 *                  derived as :
 *                  out_freq = ((in_freq / nPreDiv) * (L + ALPHAfrac)) / nPostDiv.
 *                  ALPHAprog = 2^b x ALPHAfrac where 'b' is 40 for Prius and 40 for Tesla.
 */
typedef struct ClockConfigPLL
{
  uintnt nPLLModeAddr;
  uintnt nVoteAddr;
  uint32 nVoteMask;
  uint32 nVCO;
  uint32 nPreDiv;
  uint32 nPostDiv;
  uint32 nL;
  uint32 nM;
  uint32 nN;
  uint32 nConfigCtl;
  uint64 nAlpha;
} ClockConfigPLLType;

/*
 * ClockConfigMuxType
 *
 * Parameters used for configuring a standard clock multiplexer.
 *
 *  nCMDCGRAddr - The address of the CMD RCGR register.
 *  eMux        - The mux type for mapping eSource to register source values.
 *  eSource     - The source to use.
 *  nDiv2x      - The integer (2 * divider) value to use.
 *  nM          - The M value for any M/N counter, or 0 to bypass.
 *  nN          - The N value for any M/N counter.
 *  n2D         - Twice the D value for any M/N counter.
 */
typedef struct ClockConfigMux
{
  uintnt nCMDCGRAddr;
  uint32 nSource;
  uint32 nDiv2x;
  uint32 nM;
  uint32 nN;
  uint32 n2D;
} ClockConfigMuxType;


/*
 * Clock_CBCRtoggleType
 *
 * A type to choose the the operation on clocks(enable/disable).
 */
typedef enum{
  CLK_TOGGLE_DISABLE,
  CLK_TOGGLE_ENABLE,
  NUM_CLK_TOGGLE_TYPES
} Clock_CBCRtoggleType;


/*=========================================================================
      Data
==========================================================================*/

static ClockConfigPLLType PLL0_Cfg =
{
  .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL0_MODE), 
  .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE), 
  .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0),
  .nVCO          =  2, 
  .nPreDiv       =  1, 
  .nPostDiv      =  1, 
  .nL            =  31, 
  .nM            =  0, // unused
  .nN            =  0, // unused
  .nConfigCtl    =  SPARK_CONFIG_CTL,
  .nAlpha        =  0x4000000000
};

static ClockConfigPLLType PLL2_Cfg =
{
  .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL2_MODE), 
  .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE), 
  .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL2),
  .nVCO          =  2, 
  .nPreDiv       =  1, 
  .nPostDiv      =  4, 
  .nL            =  41, 
  .nM            =  0, // unused
  .nN            =  0, // unused
  .nConfigCtl    =  SPARK_CONFIG_CTL,
  .nAlpha        =  0xAAAAAAAAAA
};

/*=========================================================================
      Function Prototypes
==========================================================================*/


/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 

/* =========================================================================
**  Function : Clock_TriggerUpdate
** =========================================================================*/
/*!
    Clock switch and wait for UPDATE bit to complete

    @param cmdReg - Address of the CBCR register

    TRUE -- CBCR programming successful.
    FALSE -- CBCR programming failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_TriggerUpdate(uintnt cmdReg, uintnt nMask)
{
  uintnt nVal;

  nVal = inp32(cmdReg) | nMask;

  /* Trigger CMD_REG:UPDATE */
  outp32(cmdReg, nVal);

  /* Wait for CMD_REG:UPDATE to clear, showing the clock switch is complete */
  while (inp32(cmdReg) & nMask);

  return TRUE;
}  


/* =========================================================================
**  Function : Clock_ToggleClock
** =========================================================================*/
/*!
    Enable/Disable a Clock and poll for CLK_OFF BIT. 

    @param CBCR_addr - Address of the CBCR register
           enable :-  enable/disable the CBCR 
    TRUE -- CBCR programming successful.
    FALSE -- CBCR programming failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_ToggleClock(uintnt CBCR_addr, Clock_CBCRtoggleType toggle_clk)
{
  uintnt CBCR_value;
  
  if(toggle_clk >= NUM_CLK_TOGGLE_TYPES) return FALSE;

  CBCR_value = inp32(CBCR_addr);
  
  if(toggle_clk == CLK_TOGGLE_ENABLE)  
  {
    CBCR_value = CBCR_value | CLK_ENABLE_MSK;
    outp32(CBCR_addr, CBCR_value);  
    do
    {
      CBCR_value = inp32(CBCR_addr);
    }while((CBCR_value & CLK_OFF_MSK) != 0);
    
  }
  else
  {
    CBCR_value = CBCR_value & ~CLK_ENABLE_MSK;
    outp32(CBCR_addr, CBCR_value);  
  }
  return TRUE;
}  


/* =========================================================================
**  Function : Clock_ConfigMux
** =========================================================================*/
/*!
    Configure a clock mux. 

    @param pConfig -  [IN] Clock mux config structure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_ConfigMux (const ClockConfigMuxType *pConfig)
{
  uintnt nCmdCGRAddr, nCfgCGRAddr;
  uint32 nCmdCGRVal,  nCfgCGRVal;
  uintnt nMAddr, nNAddr, nDAddr;
  uint32 nSource;

  nSource = pConfig->nSource;

  nCmdCGRAddr = pConfig->nCMDCGRAddr;
  nCmdCGRVal  = inp32(nCmdCGRAddr);
  nCfgCGRAddr = pConfig->nCMDCGRAddr + CLOCK_CFG_REG_OFFSET; 
  nCfgCGRVal  = inp32(nCfgCGRAddr);

  /*
   * Clear the fields
   */
  nCfgCGRVal &= ~(CLOCK_CFG_CGR_SRC_SEL_FMSK |
                  CLOCK_CFG_CGR_SRC_DIV_FMSK |
                  CLOCK_CFG_CGR_MODE_FMSK);

  /*
   * Program the source and divider values.
   */
  nCfgCGRVal |= (nSource << CLOCK_CFG_CGR_SRC_SEL_SHFT)
                  & CLOCK_CFG_CGR_SRC_SEL_FMSK;
  nCfgCGRVal |= ((HALF_DIVIDER(pConfig) << CLOCK_CFG_CGR_SRC_DIV_SHFT)
                  & CLOCK_CFG_CGR_SRC_DIV_FMSK);

  /*
   * Set MND counter mode depending on if it is in use
   */
  if (pConfig->nM != 0 && (pConfig->nM < pConfig->nN))
  {
    nMAddr = pConfig->nCMDCGRAddr + CLOCK_M_REG_OFFSET;
    nNAddr = pConfig->nCMDCGRAddr + CLOCK_N_REG_OFFSET;
    nDAddr = pConfig->nCMDCGRAddr + CLOCK_D_REG_OFFSET;

    outp32(nMAddr, pConfig->nM);
    outp32(nNAddr, NOT_N_MINUS_M(pConfig));
    outp32(nDAddr, NOT_2D(pConfig));

    nCfgCGRVal |= ((CLOCK_CFG_CGR_MODE_DUAL_EDGE_VAL << CLOCK_CFG_CGR_MODE_SHFT)
                    & CLOCK_CFG_CGR_MODE_FMSK);
  }

  /*
   * Write the final CFG register value
   */
  outp32(nCfgCGRAddr, nCfgCGRVal);

  /*
   * Trigger the update
   */
  nCmdCGRVal |= CLOCK_CMD_CFG_UPDATE_FMSK;
  outp32(nCmdCGRAddr, nCmdCGRVal);

  /*
   * Wait until update finishes
   */
  while(inp32(nCmdCGRAddr) & CLOCK_CMD_CFG_UPDATE_FMSK);

  return TRUE;

} /* END Clock_ConfigMux */


/* =========================================================================
**  Function : Clock_ConfigurePLL
** =========================================================================*/
/**
  Configures a PLL.

  @param *pConfig [in] -- PLL configuration

*/
static boolean Clock_ConfigurePLL(const ClockConfigPLLType *pConfig)
{
  uintnt nModeAddr;
  uint32 nUserVal, nModeVal;

  nModeAddr = pConfig->nPLLModeAddr;
  nModeVal = inp32(nModeAddr);

  if ( ((nModeVal & CLOCK_PLL_MODE_ACTIVE) == CLOCK_PLL_MODE_ACTIVE) ||
       (nModeVal & HWIO_FMSK(GCC_GPLL0_MODE, PLL_LOCK_DET)) )
  {
    /* This clock has already been configured */
    return FALSE;
  }

  /*
   * Program L, Alpha.
   */
  outp32(nModeAddr + HWIO_OFFS(PLL_L_VAL), pConfig->nL);
  if( pConfig->nAlpha )
  {
    outp32(nModeAddr + HWIO_OFFS(PLL_ALPHA_VAL), (uint32)pConfig->nAlpha);
    outp32(nModeAddr + HWIO_OFFS(PLL_ALPHA_VAL_U), (uint32)(pConfig->nAlpha >> 32));
  }

  /*
   * Get the CONFIG value and clear out fields we will configure.
   */
  nUserVal = inp32(nModeAddr + HWIO_OFFS(PLL_USER_CTL));
  nUserVal &= ~(HWIO_FMSK(GCC_GPLL0_USER_CTL, ALPHA_EN)      |
                HWIO_FMSK(GCC_GPLL0_USER_CTL, VCO_SEL)       |
                HWIO_FMSK(GCC_GPLL0_USER_CTL, PRE_DIV_RATIO) |
                HWIO_FMSK(GCC_GPLL0_USER_CTL, POST_DIV_RATIO));

  /*
   * Program the VCO.
   */
  nUserVal |= (pConfig->nVCO << HWIO_SHFT(GCC_GPLL0_USER_CTL, VCO_SEL));

  /*
   * Program the pre-div value (div-1 to div-8).
   */
  if (pConfig->nPreDiv <= 8 && pConfig->nPreDiv > 0)
  {
    nUserVal |= ((pConfig->nPreDiv - 1) << HWIO_SHFT(GCC_GPLL0_USER_CTL, PRE_DIV_RATIO));
  }

  /*
   * Program the post-div value (div-3 not supported)
   */
  if (pConfig->nPostDiv == 2)
  {
    nUserVal |= (1 << HWIO_SHFT(GCC_GPLL0_USER_CTL, POST_DIV_RATIO));
  }
  else if (pConfig->nPostDiv == 4)
  {
    nUserVal |= (3 << HWIO_SHFT(GCC_GPLL0_USER_CTL, POST_DIV_RATIO));
  }

  /*
   *  Check fractional output or integer output.
   */
  if( pConfig->nAlpha )
  {
    nUserVal |= HWIO_FMSK(GCC_GPLL0_USER_CTL, ALPHA_EN);
  }

  /*
   * Enable MAIN_OUT_ENA bit. 
   */
  nUserVal |= HWIO_FMSK(GCC_GPLL0_USER_CTL, PLLOUT_LV_MAIN);

  /*
   * Finally program the CONFIG register.
   */
  outp32(nModeAddr + HWIO_OFFS(PLL_USER_CTL), nUserVal);

  /*
   * Program the FSM portion of the mode register.
   */
  nModeVal &= ~HWIO_FMSK(GCC_GPLL0_MODE, PLL_BIAS_COUNT);
  nModeVal &= ~HWIO_FMSK(GCC_GPLL0_MODE, PLL_LOCK_COUNT);
  nModeVal |= CLOCK_PLL_BIAS_COUNT_VAL;
  nModeVal |= CLOCK_PLL_LOCK_COUNT_VAL;

  if (pConfig->nVoteAddr != 0)
  {
    nModeVal |= HWIO_FMSK(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA);
  }

  outp32(nModeAddr, nModeVal);
  
  if (pConfig->nConfigCtl)
  {
    /* The CONFIG_CTL resets with an incorrect default value.  Fix it. */
    outp32(nModeAddr + HWIO_OFFS(PLL_CONFIG_CTL), pConfig->nConfigCtl);
  }
  return TRUE;
} /* END Clock_ConfigurePLL */


/* ============================================================================
**  Function : Clock_EnablePLL
** ============================================================================
*/
/*!
    Configure and enable a PLL.  If it is voteable, it will be set for FSM mode
    and voted for using the vote register.

    @param pConfig -  [IN] PLL configuration structure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_EnablePLL(const ClockConfigPLLType *pConfig)
{
  uintnt nModeAddr;
  uint32 nModeVal;

  nModeAddr = pConfig->nPLLModeAddr;
  nModeVal = inp32(nModeAddr);

  if ( ((nModeVal & CLOCK_PLL_MODE_ACTIVE) == CLOCK_PLL_MODE_ACTIVE) ||
       (nModeVal & HWIO_FMSK(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA)) )
  {
    /*
     * The PLL is already running, so vote for the resource but don't configure it.
     */
    if (pConfig->nVoteAddr != 0)
    {
      outp32(pConfig->nVoteAddr, inp32(pConfig->nVoteAddr) | pConfig->nVoteMask);

      /*
       * Wait for the PLL to go active.
       */
      while ((inp32(nModeAddr) & HWIO_FMSK(GCC_GPLL0_MODE, PLL_ACTIVE_FLAG)) == 0);
    }

    return TRUE;
  }

  /*
   * Configure the PLL.
   */
  if(! Clock_ConfigurePLL(pConfig)) return FALSE;
  nModeVal = inp32(nModeAddr);
  
  /*
   * Enable the PLL.
   */
  if (pConfig->nVoteAddr != 0)
  {
    outp32(pConfig->nVoteAddr, inp32(pConfig->nVoteAddr) | pConfig->nVoteMask);

    /*
     * Wait for the PLL to go active.
     */
    while ((inp32(nModeAddr) & HWIO_FMSK(GCC_GPLL0_MODE, PLL_ACTIVE_FLAG)) == 0);
  }
  else
  {
    /* Not support non-FSM PLL yet */
    return FALSE;
  }

  return TRUE;

} /* END Clock_EnablePLL */



/* ========================================================================
**  Function : Clock_ConfigureDDR
** ======================================================================*/
/*
    Description: Configure all clocks needed for DDR configuration.  This
    extension API is used for bootup and emergency download mode.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_ConfigureDDR( void )
{
  uint32 mask;
  ClockConfigMuxType clkCfgDDR      = {HWIO_ADDR(GCC_BIMC_DDR_CPLL_CMD_RCGR), 2, 2, 0, 0, 0}; /* Hard-code to use GPLL2 only */
  ClockConfigMuxType clkCfgBIMC     = {HWIO_ADDR(GCC_BIMC_CMD_RCGR)         , 2, 6, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMCHMSS = {HWIO_ADDR(GCC_BIMC_HMSS_AXI_CMD_RCGR), 2, 4, 0, 0, 0};
  ClockConfigMuxType clkCfgBIMCGFX  = {HWIO_ADDR(GCC_MMSS_BIMC_GFX_CMD_RCGR), 2, 8, 0, 0, 0};
  ClockConfigMuxType clkCfgPIMEM    = {HWIO_ADDR(GCC_PIMEM_AXI_CMD_RCGR),     1, 6, 0, 0, 0};

  /* Config PIMEM */	
  if ( !Clock_EnablePLL(&PLL0_Cfg) ) return FALSE;
  if( ! Clock_ConfigMux(&clkCfgPIMEM)) return FALSE;
   
  Clock_ToggleClock(HWIO_GCC_PIMEM_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SYS_NOC_PIMEM_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_PIMEM_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_PIMEM_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Enable the necessary voteable HMSS and BIMC related clocks */
  mask = 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_MSTR_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_HMSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_HMSS_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, HMSS_SLV_AXI_CLK_ENA);
  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);
  
  mask = HWIO_FMSK(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, BIMC_HMSS_AXI_CLK_SLEEP_ENA);
  HWIO_OUTM(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, mask, mask);
  
  /* Revoke APCS IMEM clock enable, since RPM will manage this clock and turn off during XO shutdown */
  HWIO_OUTF( GCC_APCS_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 0 );
  
  mask =
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, BIMC_HMSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA);
  HWIO_OUTM(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  /* Enable all possible BIMC and DDR clocks */
  Clock_ToggleClock(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  HWIO_OUTF(GCC_BIMC_DDR_CPLL0_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CPLL1_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CH0_CBCR, CLK_ENABLE, 1);
  HWIO_OUTF(GCC_BIMC_DDR_CH1_CBCR, CLK_ENABLE, 1); 
  
  Clock_ToggleClock(HWIO_GCC_BIMC_XO_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX2, 1); /* BIMC runs from AUX */
	
  if ( !Clock_EnablePLL(&PLL2_Cfg) ) return FALSE;

  /* Perform a BIMC clock switch */
  if( ! Clock_ConfigMux(&clkCfgBIMC)) return FALSE;

  /* Perform a BIMC_HMSS_AXI clock switch */
  if( ! Clock_ConfigMux(&clkCfgBIMCHMSS)) return FALSE;

  /* Perform a MMSS_BIMC_GFX clock switch */
  if( ! Clock_ConfigMux(&clkCfgBIMCGFX)) return FALSE;

  /* Perform a MMSS_BIMC_GFX clock switch */
  // if( ! Clock_ConfigMux(&clkCfgBIMCQ6)) return FALSE;

  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1); /* BIMC runs from AUX */

  /* This is the first switch in GCC (Legacy) mode */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 1);
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 0);

  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_LEGACY_MODE_EN, 1);
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 1);

  /* Perform a DDR clock switch */
  if( ! Clock_ConfigMux(&clkCfgDDR)) return FALSE;

  /* DDR has switched to CPLL1, force the root on. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 1);

  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, FSM_DIS_DDR_UPDATE))) return FALSE;


  /* Turn off CPLL0 root. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 0);

  /*
   * Trigger an update again so that the JCPLL selection stays in sync with 
   * the FSM state.
   */
  if(! Clock_TriggerUpdate(clkCfgDDR.nCMDCGRAddr, 
                           HWIO_FMSK(GCC_BIMC_DDR_CPLL_CMD_RCGR, UPDATE))) return FALSE;

  /* DDR has switched to CPLL0, force the root on. */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL0_RCG_EN, 1);
    
  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, FSM_DIS_DDR_UPDATE))) return FALSE;

  /* Turn off CPLL1 root */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_CPLL1_RCG_EN, 0);

  HWIO_OUTF(GCC_BIMC_GDSCR, RETAIN_FF_ENABLE, 1);

  return TRUE;
}
