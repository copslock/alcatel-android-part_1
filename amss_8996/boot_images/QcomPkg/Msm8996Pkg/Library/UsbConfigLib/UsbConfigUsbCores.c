/** @file UsbConfigUsbCores.c

  Function and structure definitions for each USB core supported by this
  platform.

  Copyright (c) 2013-2014, QUALCOMM Technologies Inc. All rights reserved.
**/


/*=============================================================================
                              EDIT HISTORY


 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 04/25/15   sbryksin V2 init sequence updates, update BDS USB options, enable debugger on 2ndary core, NPA updates
 03/25/15   sbryksin USB LPM, HS/SS PHY config changes, USB mode toggle, clamp UsbFn to HS if SS PHY failed
 02/12/15   sbryksin Update PHY params
 02/01/15   sbryksin disable USB clock error, fix CLK reg access offsets
 12/01/14   sbryksin MSM 8996 Virtio enablement for device mode on prim/sec core
 08/13/14   amitg    Clean Up Code, Add Clock initializations in correct sequence
 07/18/14   ar       Fixed possible null pointer dereference issue
 06/30/14   amitg    Updated the QUSB2 Phy parameters and sequence as per the latest HPG
 06/18/14   amitg    Removed the extra reset for the host mode over Synopsys core
 06/13/14   amitg    Add QUSB2 Phy parameters  
 06/09/14   ck       Add QUSB2 Reset Sequence  
 06/05/14   amitg    MSM 8994 Bring Up, SNPS Initization Updates
 06/04/14   amitg    MSM 8994 Bring Up Updates
 05/20/14   amitg    Enabling Vbus for Host Mode, CleanUp
 04/30/14   amitg    Fixes in SSPhy wite sequence
 04/30/14   ar       Cleanup for NDEBUG build  
 04/23/14   amitg    Added NPA Support for HS1 and SS1 Cores
 04/18/14   amitg    Updates in SSInit sequence
 04/14/14   amitg    Added clocks support
 03/14/14   amitg    Updates supporting SNPS Host mode and device mode
 10/29/13   amitg    Initial Revision for MSM 8994
=============================================================================*/


#include "UsbConfigPrivate.h"

#define FREQ_MHZ(f)     (f * 1000 * 1000)
#define FREQ_KHZ(f)     (f * 1000)

/*
 * USB 3.0 Clock definitions
 */

#define USB3_PIPE_CLK_XO_MIN_FREQ_HZ          FREQ_KHZ(19200)
#define USB3_PIPE_CLK_MIN_FREQ_HZ             FREQ_MHZ(125)

#define USB3_MASTER_CLK_MIN_FREQ_HZ           FREQ_MHZ(120)
#define USB3_AXI_CLK_MIN_FREQ_HZ              FREQ_MHZ(120)
#define USB3_PHY_AUX_CLK_MIN_FREQ_HZ          FREQ_KHZ(1200)

// Mock CLK needs to be set at 19.2 Mhz for Gandalf
#define USB3_MOCK_CLK_MIN_FREQ_HZ             FREQ_MHZ(0)

#define MICRO_STALL_UNITS(x)  ((x) * 10)
#define MILLI_STALL_UNITS(x)  ((x) * 10000)

/*******************************************************************************
 * USB core initialization functions for core structures
 ******************************************************************************/


STATIC
EFI_STATUS
SynopsysInitCommon (
  IN  QCOM_USB_CORE         *UsbCore
  );

STATIC
EFI_STATUS
SynopsysInitHostPrimary (
  IN  QCOM_USB_CORE         *UsbCore
  );

STATIC
EFI_STATUS
SynopsysInitDevicePrimary (
  IN  QCOM_USB_CORE         *UsbCore
  );

STATIC
EFI_STATUS
SynopsysReset (
  IN  QCOM_USB_CORE         *UsbCore
  );

STATIC
EFI_STATUS
SynopsysPrimEnterLpm (
  IN  QCOM_USB_CORE         *UsbCore
  );

  STATIC
EFI_STATUS
SynopsysSecEnterLpm (
  IN  QCOM_USB_CORE         *UsbCore
  );
  
STATIC
EFI_STATUS
SynopsysExitLpm (
  IN  QCOM_USB_CORE         *UsbCore
  );
  
/* Power Domain list */
const CHAR8 USB30_PowerDomain[] = "VDD_USB_30";


/*******************************************************************************
 * USB core structures
 ******************************************************************************/

/*
 * USB core enable clock configuration arrays
 */

QCOM_USB_CLOCK SsUsb1EnCoreClocks[] =
{
//{ "clock name",                     frequency,                    divider }
  {"gcc_aggre2_usb3_axi_clk",      USB3_AXI_CLK_MIN_FREQ_HZ,      1 },
  {"gcc_sys_noc_usb3_axi_clk",     USB3_AXI_CLK_MIN_FREQ_HZ,      1 },
  {"gcc_usb30_master_clk",         USB3_MASTER_CLK_MIN_FREQ_HZ,   1 },
  {"gcc_usb30_sleep_clk",          0,                             1 },
  {"gcc_usb30_mock_utmi_clk",      USB3_MOCK_CLK_MIN_FREQ_HZ,     1 },
  {"gcc_usb_phy_cfg_ahb2phy_clk",  0,                             1 },
  {"gcc_usb3_phy_aux_clk",         USB3_PHY_AUX_CLK_MIN_FREQ_HZ,  1 },
  {"gcc_usb3_phy_pipe_clk",        0,                             1 }
};

/*
 * USB core disable clock configuration arrays
 */

QCOM_USB_CLOCK SsUsb1DisCoreClocks[] =
{
  {"gcc_aggre2_usb3_axi_clk",      USB3_AXI_CLK_MIN_FREQ_HZ,      1 },
  {"gcc_usb30_master_clk",         USB3_MASTER_CLK_MIN_FREQ_HZ,   1 },
  {"gcc_sys_noc_usb3_axi_clk",     USB3_AXI_CLK_MIN_FREQ_HZ,      1 },
  {"gcc_usb30_sleep_clk",          0,                             1 },
  {"gcc_usb30_mock_utmi_clk",      USB3_MOCK_CLK_MIN_FREQ_HZ,     1 },
  {"gcc_usb_phy_cfg_ahb2phy_clk",  0,                             1 },
  {"gcc_usb3_phy_aux_clk",         USB3_PHY_AUX_CLK_MIN_FREQ_HZ,  1 }, 
  {"gcc_usb3_phy_pipe_clk",        0,                             1 }
};




QCOM_USB_CLOCK SsUsb2EnCoreClocks[] =
{
//{ "clock name",                     frequency,                    divider }
  {"gcc_periph_noc_usb20_ahb_clk", 0,                             1 },
  {"gcc_usb20_master_clk",         USB3_MASTER_CLK_MIN_FREQ_HZ,   1 },
  {"gcc_usb20_sleep_clk",          0,                             1 },
  {"gcc_usb20_mock_utmi_clk",      USB3_MOCK_CLK_MIN_FREQ_HZ,     1 },
  {"gcc_usb_phy_cfg_ahb2phy_clk",  0,                             1 }
};

/*
 * USB core disable clock configuration arrays
 */

QCOM_USB_CLOCK SsUsb2DisCoreClocks[] =
{
  {"gcc_usb20_master_clk",         USB3_MASTER_CLK_MIN_FREQ_HZ,   1 },
  {"gcc_periph_noc_usb20_ahb_clk", 0,                             1 },
  {"gcc_usb20_sleep_clk",          0,                             1 },
  {"gcc_usb20_mock_utmi_clk",      USB3_MOCK_CLK_MIN_FREQ_HZ,     1 },
  {"gcc_usb_phy_cfg_ahb2phy_clk",  0,                             1 }
};

//vsb: TODO ugly: USB_CORE_MAX_NUM is from Qcom package protocol header so not accurate core #

//structure that holds controller handle and configuration data associated with current core/driver stack
USB_DEV gUsbConfigDevice[USB_CORE_MAX_NUM] = {
   { USB_DEV_SIGNATURE,     //signature
     NULL,                  //client handle
     {                      // SSUSB Core 0 instance
      SynopsysInitCommon,
      SynopsysInitDevicePrimary,
      SynopsysInitHostPrimary,
      SynopsysReset,
      SynopsysPrimEnterLpm,
      SynopsysExitLpm,
      USB_CONFIG_SSUSB1,
      USB30_PRIM_BASE,
      sizeof(SsUsb1EnCoreClocks) / sizeof(QCOM_USB_CLOCK),
      SsUsb1EnCoreClocks,
      sizeof(SsUsb1DisCoreClocks) / sizeof(QCOM_USB_CLOCK),
      SsUsb1DisCoreClocks,
      0,                        //clock ref count
      FALSE                     //isCoreInLPM
     },
     {0}
   },
   { USB_DEV_SIGNATURE,     //signature
     NULL,                  //client handle
     {                      // SSUSB Core 1 instance
      SynopsysInitCommon,
      SynopsysInitDevicePrimary,
      SynopsysInitHostPrimary,
      SynopsysReset,
      SynopsysSecEnterLpm,
      SynopsysExitLpm,
      USB_CONFIG_SSUSB2,
      USB30_SEC_BASE,
      sizeof(SsUsb2EnCoreClocks) / sizeof(QCOM_USB_CLOCK),
      SsUsb2EnCoreClocks,
      sizeof(SsUsb2DisCoreClocks) / sizeof(QCOM_USB_CLOCK),
      SsUsb2DisCoreClocks,
      0,                        //clock ref count
      FALSE                     //isCoreInLPM
     },
     {0}
   },

   { USB_DEV_SIGNATURE, NULL, {0}, {0}}    //NULL core used in case no valid cores are initialized
}; 

/*
 * Static Variables
 */
STATIC EFI_QCOM_PMIC_SMBCHG_PROTOCOL         *PmicSmbChgProtocol  = NULL;

// NPA Client Handle for HS and SS Cores
STATIC npa_client_handle                      PmicNpaClientSS    = NULL;

//NPA client handles for SNOC - required for all cores
STATIC npa_client_handle                      gNpaClientSS1Snoc       = NULL;
STATIC npa_client_handle                      gNpaClientSS2Snoc       = NULL;
//busses required for primary core
STATIC npa_client_handle                      gNpaClientAggre2     = NULL;
//busses required for 2ndary USB core
STATIC npa_client_handle                      gNpaClientAggre1     = NULL;
STATIC npa_client_handle                      gNpaClientPnoc       = NULL;

EFI_CLOCK_PROTOCOL                           *ClockProtocol  = NULL;

extern EFI_GUID                              gQcomTokenSpaceGuid;
extern EFI_PLATFORMINFO_PLATFORM_INFO_TYPE   PlatformInfo;


/*******************************************************************************
 * Core initialization function implementations
 ******************************************************************************/

/*
 * See UsbConfigLibPrivate.h
 */
EFI_STATUS
UsbConfigLibOpenProtocols (
  VOID
  )
{
  EFI_STATUS Status = EFI_SUCCESS;

  FNC_ENTER_MSG();

  Status = gBS->LocateProtocol(&gEfiClockProtocolGuid, NULL, (VOID**) &ClockProtocol);
  ERR_CHK ("failed to locate CLOCK protocol");

  // Find the PMIC Smbb charger protocol
  Status = gBS->LocateProtocol (&gQcomPmicSmbchgProtocolGuid, NULL, (void**)&PmicSmbChgProtocol);
  if (EFI_ERROR (Status)) {
    PmicSmbChgProtocol = NULL;
    ERR_CHK ("failed to locate PMIC SMBCHG Protocol, Status =  (0x%x)\n", Status);
  }
  
  PmicNpaClientSS =  npa_create_sync_client(
                      PMIC_NPA_GROUP_ID_USB_SS1, //PMIC_NPA_GROUP_ID_USB_SS1 - Connect to the usb ss1 resource
                      "usb_ss1",                 //client name "usb_ss1"
                      NPA_CLIENT_REQUIRED);      //NPA_CLIENT_REQUIRED: Request must honor request
  if (NULL == PmicNpaClientSS)
  {
    ERR_CHK("PmicNpaClientSS cannot be created \r\n ");
  }

  //Create NPA nodes for required busses requests
  gNpaClientSS1Snoc = npa_create_sync_client("/clk/snoc", "usb_ss1_snoc", NPA_CLIENT_REQUIRED); 

  if (!gNpaClientSS1Snoc)
  {
    ERR_CHK("gNpaClientSS1Snoc cannot be created \r\n ");
  }
  
  //Create NPA nodes for required busses requests
  gNpaClientSS2Snoc = npa_create_sync_client("/clk/snoc", "usb_ss2_snoc", NPA_CLIENT_REQUIRED); 

  if (!gNpaClientSS2Snoc)
  {
    ERR_CHK("gNpaClientSS2Snoc cannot be created \r\n ");
  }
  
  gNpaClientPnoc = npa_create_sync_client( "/clk/pnoc", "usb_ss2_pnoc", NPA_CLIENT_REQUIRED);

  if (!gNpaClientPnoc)
  {
    ERR_CHK("gNpaClientPnoc cannot be created \r\n ");
  }

  gNpaClientAggre1 = npa_create_sync_client( "/clk/agr1", "usb_ss2_agr1", NPA_CLIENT_REQUIRED);

  if (!gNpaClientAggre1)
  {
    ERR_CHK("gNpaClientAggre1 cannot be created \r\n ");
  }
  
  gNpaClientAggre2 = npa_create_sync_client( "/clk/agr2", "usb_ss1_agr2", NPA_CLIENT_REQUIRED);

  if (!gNpaClientAggre2)
  {
    ERR_CHK("gNpaClientAggre2 cannot be created \r\n ");
  }
  
  Status = EFI_SUCCESS;

ON_EXIT:
  FNC_LEAVE_MSG();
  return Status;
}

/**
  Initializes power domains for USB.

  @retval EFI_SUCCESS            USB clocks initialized successfully.
  @retval EFI_NOT_FOUND          Unable to locate clock protocol.

**/
STATIC
EFI_STATUS
EFIAPI
ConfigPowerDomain(
  IN BOOLEAN       bEnable,
  IN CONST CHAR8   *szDomain
  )
{
  EFI_STATUS   Status = EFI_SUCCESS;
  UINTN        uClockPowerDomainId;
  
  FNC_ENTER_MSG ();

  // Locate clock protocol.
  Status = gBS->LocateProtocol (&gEfiClockProtocolGuid, NULL, (VOID**) &ClockProtocol);
  ERR_CHK ("failed to locate CLOCK protocol");

  if (ClockProtocol)
  {
      Status = ClockProtocol->GetClockPowerDomainID(ClockProtocol, szDomain, &uClockPowerDomainId);
      WRN_CHK ("USBConfigLib:GetClockPowerDomainID failed!");

      if (TRUE == bEnable)
      {
        Status =  ClockProtocol->EnableClockPowerDomain(ClockProtocol, uClockPowerDomainId);
        WRN_CHK ("USBConfigLib:EnableClockPowerDomain failed!");
      }
      else if (FALSE == bEnable)
      {
        Status =  ClockProtocol->DisableClockPowerDomain(ClockProtocol, uClockPowerDomainId);
        WRN_CHK ("USBConfigLib:DisableClockPowerDomain failed!");
      }
  }
  else
  {
      WRN_CHK ("failed to locate Clock Protocol ");
      WRN_CHK ("failed to initialize PowerDomain");
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  Initializes all clocks for a USB core.

  @param [in]  Clocks       Clock array
  @param [in]  ClockCount   Number of clocks in array

  @retval EFI_SUCCESS       USB clocks enabled successfully
  @retval Others            Error enabling at least 1 clock
**/
STATIC
EFI_STATUS
InitUsbClocks (
  IN  UINTN                 ClockCount,
  IN  QCOM_USB_CLOCK        *Clocks
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  UINTN       ClkIdx;

  FNC_ENTER_MSG ();

  // validate parameters
  if ((NULL == Clocks) || (0 == ClockCount)) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  if (NULL == ClockProtocol) {
    DBG(EFI_D_ERROR, "ClockProtocol unavailable");
    Status = EFI_NOT_FOUND;
    goto ON_EXIT;
  }

  // enable each clock
  for (ClkIdx = 0; ClkIdx < ClockCount; ClkIdx++) {
    Status = EnableClock (
                Clocks[ClkIdx].Name,
                Clocks[ClkIdx].Frequency,
                Clocks[ClkIdx].Divider
                );
    if (EFI_ERROR (Status)) {
      DBG(EFI_D_ERROR, "failed to enable clock '%a'", Clocks[ClkIdx].Name);
      goto ON_EXIT;
    }
  }

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
} // end InitUsbClocks



/**
  Disables all clocks for a USB core.

  @param [in]  Clocks       Clock array
  @param [in]  ClockCount   Number of clocks in array

  @retval EFI_SUCCESS       USB clocks disabled successfully
  @retval Others            Error disabling at least 1 clock
**/
STATIC
EFI_STATUS
DisableUsbClocks (
  IN  UINTN                 ClockCount,
  IN  QCOM_USB_CLOCK        *Clocks
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  UINTN       ClkIdx;

  FNC_ENTER_MSG ();

  // validate parameters
  if ((NULL == Clocks) || (0 == ClockCount)) {
    Status = EFI_INVALID_PARAMETER;
    DBG(EFI_D_ERROR, "invalid parameter");
    goto ON_EXIT;
  }

  if (NULL == ClockProtocol) {
    DBG(EFI_D_ERROR, "ClockProtocol unavailable");
    Status = EFI_NOT_FOUND;
    goto ON_EXIT;
  }
  // disable each clock
  for (ClkIdx = 0; ClkIdx < ClockCount; ClkIdx++) {
    Status = DisableClock (Clocks[ClkIdx].Name);
    if (EFI_ERROR (Status)) {
      DBG(EFI_D_ERROR, "failed to disable clock '%a'", Clocks[ClkIdx].Name);
    }
  }
  
ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}



/**
  Enable vbus for primary port (SuperSpeed Port)

  @retval EFI_SUCCESS            Success

**/
STATIC
EFI_STATUS
EFIAPI
EnableVbusSS1 (
  VOID
  )
{
  EFI_STATUS  Status  =  EFI_SUCCESS;

  FNC_ENTER_MSG ();
  
  if (NULL == PmicSmbChgProtocol) {
    DBG(EFI_D_ERROR, "Required Protocols unavailable");
    Status = EFI_NOT_FOUND;
    goto ON_EXIT;
  }

  Status = PmicSmbChgProtocol->EnableOtg(PMIC_SEC_DEVICE_INDEX_8996, TRUE);
  WRN_CHK ("failed to enable Vbus, Status = 0x%x", Status);

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
}


/**
  This API is used for resetting the Link and PHYs using clock control 

  @param None

**/
VOID 
SynopsysGccReset(
IN  QCOM_USB_CORE         *UsbCore
)
{
  if (UsbCore->CoreType == USB_CONFIG_SSUSB1)
  {
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_BCR_OFFS, HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_PRIM_BCR_OFFS, HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3_PHY_BCR_OFFS, HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3PHY_PHY_BCR_OFFS, HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK);
    gBS->Stall (100);

    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3PHY_PHY_BCR_OFFS, ~HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3_PHY_BCR_OFFS, ~HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_PRIM_BCR_OFFS, ~HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_BCR_OFFS, ~HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK);
    gBS->Stall (100);
  }
  else if  (UsbCore->CoreType == USB_CONFIG_SSUSB2)
  {
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_20_BCR_OFFS, HWIO_GCC_USB_20_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_SEC_BCR_OFFS, HWIO_GCC_QUSB2PHY_SEC_BCR_BLK_ARES_BMSK);
    gBS->Stall (100);

    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_SEC_BCR_OFFS, ~HWIO_GCC_QUSB2PHY_SEC_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_20_BCR_OFFS, ~HWIO_GCC_USB_20_BCR_BLK_ARES_BMSK);
    gBS->Stall (100);
  
  }
    return;
}






/**
  Initializes High Speed USB SNPS Pico phy when connected to SNPS Core

  @retval EFI_SUCCESS            High Speed USB phy  initialized
                                 successfully.

**/
STATIC
EFI_STATUS
InitHSUSBPhy (
  IN QCOM_USB_CONFIG_CORE_TYPE  CoreId,
  IN UINTN                      BaseAddr
  )
{
  FNC_ENTER_MSG ();

  if (CoreId == USB_CONFIG_SSUSB1)
  {
    // QUSB2PHY_PORT_POWERDOWN 0x23h     Deassert 'POWER_DOWN' = 1'b1 and sets CLAMP_N_EN to 1'b1 (vdd_1p8 is powered up).
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OFFS ,0x23);
    gBS->Stall(100);

    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE1_OFFS, 0xF8);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_OFFS, 0x83);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE3_OFFS, 0x93);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE4_OFFS, 0xC0);
   
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TUNE_OFFS, 0x30);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL1_OFFS, 0x79);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL2_OFFS, 0x21);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TEST2_OFFS, 0x14);

    // QUSB2PHY_PORT_POWERDOWN 0x22h Deassert 'POWER_DOWN' = 1'b0 and set CLAMP_N_EN to 1'b1 (once vdd_1p8 is powered up).
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OFFS ,0x22);
  }
  else
  {
    // QUSB2PHY_PORT_POWERDOWN 0x23h     Deassert 'POWER_DOWN' = 1'b1 and sets CLAMP_N_EN to 1'b1 (vdd_1p8 is powered up).
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_OFFS ,0x23);
    gBS->Stall(100);
    
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_TUNE1_OFFS, 0xF8);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_TUNE2_OFFS, 0x83);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_TUNE3_OFFS, 0x93);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_TUNE4_OFFS, 0xC0);

    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PLL_TUNE_OFFS, 0x30);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PLL_USER_CTL1_OFFS, 0x79);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PLL_USER_CTL2_OFFS, 0x21);
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_TEST2_OFFS, 0x14);
    
    // QUSB2PHY_PORT_POWERDOWN 0x22h Deassert 'POWER_DOWN' = 1'b0 and set CLAMP_N_EN to 1'b1 (once vdd_1p8 is powered up).
    HAL_HSUSB_WRITE(PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_OFFS ,0x22);
  }
  FNC_LEAVE_MSG ();

  return EFI_SUCCESS;
}



/**
  Initializes super speed USB phy

  @retval EFI_SUCCESS            Super Speed USB phy  initialized
                                 successfully.

**/
STATIC
__attribute__ ((noinline))
EFI_STATUS
InitSSUSBPhy (
  IN QCOM_USB_CONFIG_CORE_TYPE  CoreId,
  IN UINTN                      BaseAddr
  )
{
  UINTN cnt = 0;
  UINTN ReadVal;
  UINT8 MaxSpeedUefiVar;
  UINTN VarSize;

  EFI_STATUS  Status  =  EFI_SUCCESS;
  UINTN       ClockId;

  FNC_ENTER_MSG ();

  // Reset the PHY block
  HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3_PHY_BCR_OFFS, HWIO_GCC_USB3_PHY_BCR_RMSK);
  HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3PHY_PHY_BCR_OFFS, HWIO_GCC_USB3PHY_PHY_BCR_RMSK);
  gBS->Stall (100);
  HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3PHY_PHY_BCR_OFFS, ~HWIO_GCC_USB3PHY_PHY_BCR_RMSK);
  HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3_PHY_BCR_OFFS, ~HWIO_GCC_USB3_PHY_BCR_RMSK);
  gBS->Stall (100);

  //Write 0x11 to the AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG 
  //to enable one-wait-state writes and reads prior to writing or reading the QMP USB3 PHY CSRs.
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_AHB2PHY_TOP_CFG_OFFS, 0x11);

  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_OFFS, 
        HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_DOWN_CONTROL_SW_PWRDN_B_BMSK);
  
  //// Common block settings 19.2 MHz Setup ////
  //QSERDES_COM_SYSCLK_EN_SEL	0x14	SYSCLK source select and enable
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYSCLK_EN_SEL_OFFS, 0x14);
  //QSERDES_COM_BIAS_EN_CLKBUFLR_EN	0x08	Clock Buffer enable Left
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_OFFS, 0x08);
  //QSERDES_COM_CLK_SELECT	0x30	Clock select signals
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CLK_SELECT_OFFS, 0x30);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_CONFIG_OFFS, 0x06);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SVS_MODE_CLK_SEL_OFFS, 0x01);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_HSCLK_SEL_OFFS, 0x01);
  //SS PHY PLL lock changes
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TRIM_OFFS, 0x0F);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_IVCO_OFFS, 0x0F);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SYS_CLK_CTRL_OFFS, 0x04);
  
  //// Res_code settings////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_RESCODE_DIV_NUM_OFFS, 0x15);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CMN_MISC2_OFFS, 0x1F);
  
  ////PLL & Loop filter settings////
  //QSERDES_COM_DEC_START_MODE0	0x82	Start value for the PLL decimal divider
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DEC_START_MODE0_OFFS, 0x82);
  //Starting points for the fractional PLL feedback divider
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START1_MODE0_OFFS, 0x55);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START2_MODE0_OFFS, 0x55);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_DIV_FRAC_START3_MODE0_OFFS, 0x03);
  // QSERDES_COM_CP_CTRL_MODE0	0x0b	PLL charge pump control
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CP_CTRL_MODE0_OFFS, 0x0B);
  // QSERDES_COM_PLL_RCTRL_MODE0	0x16	Control to PLL RC Loop filter component for full rate
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_RCTRL_MODE0_OFFS, 0x16);
  // QSERDES_COM_PLL_CCTRL_MODE0	0x28	Control to second cap of the loop filter for full rate
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_PLL_CCTRL_MODE0_OFFS, 0x28);
  // QSERDES_COM_INTEGLOOP_GAIN0_MODE0	0x80	gain of PLL integral loop
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_OFFS, 0x80);
  
  if (gUsbCoreConfig.MSM8996Version == QCOM_MSM8996_V1)
  {
    //vsb: V1 SSUSB PHY PLL lock sequence changes
    HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_CTRL_OFFS, 0x1C);
    HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE1_MODE0_OFFS, 0x3F);
    HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE2_MODE0_OFFS, 0x01);
  }

  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP1_MODE0_OFFS, 0x15);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP2_MODE0_OFFS, 0x34);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP3_MODE0_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_CORE_CLK_EN_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_LOCK_CMP_CFG_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_VCO_TUNE_MAP_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_BG_TIMER_OFFS, 0x0A);
  
  //////////SSC settings////////////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_EN_CENTER_OFFS, 0x01);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER1_OFFS, 0x31);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_PER2_OFFS, 0x01);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER1_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_ADJ_PER2_OFFS, 0x00);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE1_OFFS, 0xDE);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_COM_SSC_STEP_SIZE2_OFFS, 0x07);
  
  ////////////// RX settings ////////////////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_OFFS, 0x0B);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_UCDR_SO_GAIN_OFFS, 0x04);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_OFFS, 0x02);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_OFFS, 0x4C);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_OFFS, 0xBB);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_EQ_OFFSET_ADAPTOR_CNTRL1_OFFS, 0x77);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_RX_OFFSET_ADAPTOR_CNTRL2_OFFS, 0x80);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_CNTRL_OFFS, 0x03);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_LVL_OFFS, 0x1B);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_OFFS, 0x16);

  ///////////TX settings////////////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OFFS, 0x45);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_RCV_DETECT_LVL_2_OFFS, 0x12);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_QSERDES_TX_LANE_MODE_OFFS, 0x06);
  
  ///////////FLL settings////////////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL2_OFFS, 0x03);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNTRL1_OFFS, 0x02);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_L_OFFS, 0x09);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_CNT_VAL_H_TOL_OFFS, 0x42);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_FLL_MAN_CODE_OFFS, 0x85);
 
  ///////////Lock det settings////////////
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG1_OFFS, 0xD1);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG2_OFFS, 0x1F);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_LOCK_DETECT_CONFIG3_OFFS, 0x47);
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_POWER_STATE_CONFIG2_OFFS, 0x08);

  // Fix for QCTDD02144008: write pcs_start before releasing sw_reset to avoid cm_usb3 fll 
  // to calibrate to incorrect frequency.  
  // USB3_PHY_START_CONTROL	0x03	Enable PHY start & PCS start bits
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_START_CONTROL_OFFS, 0x03);  

  //USB3_PHY_SW_RESET  0x00          Release sw_reset
  HAL_HSUSB_WRITE (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_SW_RESET_OFFS, 0x00);  

  // Phy Status should transition from 1 to 0
  while ((HAL_HSUSB_READ (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_OFFS)) & 
          HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_PHYSTATUS_BMSK) 
  {
    if (cnt >= QMP_PHY_MAX_RW_VERIFY_ATTEMPTS) 
    {
      DBG(EFI_D_ERROR, "Init SSUSBPhy Enable Error");
      break;
    }
    cnt++;
    //stall 100us
    gBS->Stall (MICRO_STALL_UNITS(100));
  }
 
  ReadVal = HAL_HSUSB_READ (PERIPH_SS_BASE_PHYS, HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_OFFS);
  DBG(EFI_D_INFO, "Init SSUSBPhy - HWIO_PERIPH_SS_USB3PHY_PCIE_USB3_PHY_PCS_STATUS = 0x%x", ReadVal);

  //Bail w/ error code on SS PHY init failure
  if ( ReadVal & HWIO_PERIPH_SS_USB3PHY_USB3_PHY_PCS_STATUS_PHYSTATUS_BMSK) 
  {
    DBG(EFI_D_ERROR, "Init SSUSBPhy - HWIO_PERIPH_SS_USB3PHY_PCIE_USB3_PHY_PCS_STATUS = 0x%x", ReadVal);
    MaxSpeedUefiVar = HWIO_DCFG_DEVSPD_HS;
    Status = EFI_DEVICE_ERROR; 
    goto ON_EXIT;
  }

  //On success, set UFN max speed to SS
  MaxSpeedUefiVar = HWIO_DCFG_DEVSPD_SS;

  Status = ClockProtocol->GetClockID (ClockProtocol, "gcc_usb3_phy_pipe_clk", &ClockId);
  WRN_CHK ("failed get clock ID for gcc_usb3_phy_pipe_clk");
  
  if (!EFI_ERROR (Status)) 
  {
    // Select Source as QMP Phy for gcc_usb3_phy_pipe_clk
    Status = ClockProtocol->SelectExternalSource(ClockProtocol, 
                                                 ClockId, 
                                                 USB3_PIPE_CLK_MIN_FREQ_HZ, 
                                                 0x0,
                                                 0,
                                                 0,
                                                 0,
                                                 0);
    WRN_CHK ("failed to set source as QMP Phy for gcc_usb3_phy_pipe_clk");
  }
  gBS->Stall (100);

ON_EXIT:
  //Set the maximum speed supported by controller
  VarSize = sizeof(MaxSpeedUefiVar);
  gRT->SetVariable (L"UsbfnMaxSpeed", &gQcomTokenSpaceGuid,
                             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                             VarSize, &MaxSpeedUefiVar);

  FNC_LEAVE_MSG ();

  return Status;
}


/**
  Performs initialization common to host and device mode on a Synopsys
  super speed USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully initialized
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered initializing USB core
**/
STATIC
EFI_STATUS
SynopsysInitCommon (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status   =  EFI_SUCCESS;
  UINTN       ClockId;
  UINT32      FreqHz;
  BOOLEAN     bSsPhyFailed = FALSE;
  UINT32      cnt = 0;
  UINTN       BaseAddr;

  FNC_ENTER_MSG ();

  if (NULL == UsbCore) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  BaseAddr = UsbCore->BaseAddr;
    
  //Turn on power rails 
  if (UsbCore->CoreType == USB_CONFIG_SSUSB1)
  { 
    if (NULL != PmicNpaClientSS)
    {
      npa_issue_required_request(PmicNpaClientSS, PMIC_NPA_MODE_ID_USB_PERPH_ACTIVE);   //Sends the request to RPM, no return
      DEBUG ((EFI_D_INFO, "SynopsysInitCommon : NPA Request to set PMIC_NPA_MODE_ID_USB_PERPH_ACTIVE \n"));
    }
    else {
      DBG(EFI_D_ERROR, "USB NPA Client unavailable. Aborting");
      Status = EFI_INVALID_PARAMETER;
      goto ON_EXIT;
    }
  }
  //vsb: TODO: need RPM update to have SS2 NPA node
  else if (UsbCore->CoreType == USB_CONFIG_SSUSB2)
  {
    if (NULL != PmicNpaClientSS)
    {
      npa_issue_required_request(PmicNpaClientSS, PMIC_NPA_MODE_ID_USB_PERPH_ACTIVE);   //Sends the request to RPM, no return
      DEBUG ((EFI_D_INFO, "SynopsysInitCommon : NPA Request to set PMIC_NPA_MODE_ID_USB_PERPH_ACTIVE \n"));
    }
    else {
      DBG(EFI_D_ERROR, "USB NPA Client unavailable. Aborting");
      Status = EFI_INVALID_PARAMETER;
      goto ON_EXIT;
    }
  }

  //Enable USB power domain
  ConfigPowerDomain(TRUE, USB30_PowerDomain );

  if (UsbCore->CoreType == USB_CONFIG_SSUSB1)
  {
    //Vote for SNOC @ 200Mhz for both cores
    npa_issue_required_request(gNpaClientSS1Snoc, 200000);

    //Vote for aggr2 for primary @ 200Mhz
    npa_issue_required_request(gNpaClientAggre2, 200000);
  
    Status = ClockProtocol->GetClockID (ClockProtocol, "gcc_usb3_phy_pipe_clk", &ClockId);
    WRN_CHK ("failed get clock ID for gcc_usb3_phy_pipe_clk");
    
    if (!EFI_ERROR (Status)) 
    {
      // Select Source as XO for gcc_usb3_phy_pipe_clk, otherwise we will see erros in enabling clocks
      Status = ClockProtocol->SetClockFreqHz (ClockProtocol, ClockId, USB3_PIPE_CLK_XO_MIN_FREQ_HZ,
               EFI_CLOCK_FREQUENCY_HZ_AT_LEAST, &FreqHz);
      WRN_CHK("failed to set USB30 Pipe clock frequency 19.2 MHz");
      
      DBG (EFI_D_INFO, "USB30 Pipe Clock frequency @(%d)",FreqHz);
    }
  }
  else if (UsbCore->CoreType == USB_CONFIG_SSUSB2)
  {
    //Vote for SNOC @ 200Mhz for both cores
    npa_issue_required_request(gNpaClientSS2Snoc, 200000);
    //Vote for aggr1 for USB2 & PNOC @100Mhz
    npa_issue_required_request(gNpaClientAggre1, 100000);
    npa_issue_required_request(gNpaClientPnoc, 100000);
  }

  // Enable clocks first, without that disable clocks doesnt work
  Status = InitUsbClocks (UsbCore->EnClockCount, UsbCore->EnClocks);
  ERR_CHK ("failed to disable USB clocks");

  // Disable clocks for super speed controller
  Status = DisableUsbClocks (UsbCore->DisClockCount, UsbCore->DisClocks);
  //ERR_CHK ("failed to disable USB clocks");
    
  /*3. 
    To avoid ambiguity in the initial state, put the core in power collapse, then bring the core out of power collapse
    Applies to primary core only
  */
  if (UsbCore->CoreType == USB_CONFIG_SSUSB1)
  {
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_GDSCR_OFFS, HWIO_GCC_USB_30_GDSCR_SW_COLLAPSE_BMSK);
    gBS->Stall (100);
    HAL_HSUSB_AND (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_GDSCR_OFFS, ~HWIO_GCC_USB_30_GDSCR_SW_COLLAPSE_BMSK);
    gBS->Stall (100);
  }
  
  // Hard Reset the USB Link and PHYs using GCC control
  SynopsysGccReset (UsbCore);

  //GCC_USB_SS_PHY_LDO_EN is enabled by default

  // Enable clocks for super speed controller
  Status = InitUsbClocks (UsbCore->EnClockCount, UsbCore->EnClocks);
  ERR_CHK ("failed to initialize USB clocks");
  //Increment reference count
  UsbCore->ClkRefCnt++;
      
  Status = InitHSUSBPhy(UsbCore->CoreType, BaseAddr);
  WRN_CHK("InitHSUSBPhy failed: 0x%08x", Status);

  if (UsbCore->CoreType == USB_CONFIG_SSUSB1)
  {
    Status = InitSSUSBPhy(UsbCore->CoreType, BaseAddr);
    if (Status != EFI_SUCCESS)
    {
      bSsPhyFailed = TRUE;
      DBG( EFI_D_WARN, "InitSSUSBPhy failed: 0x%08x. Falling back to HS PHY only", Status);
      Status = EFI_SUCCESS;
    }
  }

   /*
  10. Assert the core softreset:
  usb30_reg_gctl[CORESOFTRESET] = 0x1
  */
  HAL_HSUSB_OR (BaseAddr, HWIO_GCTL_OFFS, HWIO_GCTL_CORESOFTRESET_BMSK);

  /*
     11. HS only: if operating without the SS PHY, follow this sequence to disable the pipe clock requirement.
    a. usb30_reg_general_cfg[PIPE_UTMI_CLK_DIS] = 0x1
    b. Wait 1 �s.
    c. usb30_reg_general_cfg[PIPE_UTMI_CLK_SEL] = 0x1
    d. usb30_reg_general_cfg [PIPE3_PHYSTATUS_SW] = 0x1
    e. Wait 1 �s.
    f. usb30_reg_general_cfg[PIPE_UTMI_CLK_DIS] = 0x0
    In this case, program the software to skip all SuperSpeed configurations in 
    the link controller and PHY, otherwise the core could get stuck.
  */
  if ((UsbCore->CoreType == USB_CONFIG_SSUSB2) || ( (UsbCore->CoreType == USB_CONFIG_SSUSB1) && bSsPhyFailed))
  {
	DBG( EFI_D_INFO, "Disabling SS PHY");
    HAL_HSUSB_OR (BaseAddr, HWIO_GENERAL_CFG_ADDR(USB30_QSCRATCH_REG_BASE_OFFS), HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK);
    gBS->Stall (MICRO_STALL_UNITS(10));
    HAL_HSUSB_OR (BaseAddr, HWIO_GENERAL_CFG_ADDR(USB30_QSCRATCH_REG_BASE_OFFS), HWIO_GENERAL_CFG_PIPE_UTMI_CLK_SEL_BMSK);
    HAL_HSUSB_OR (BaseAddr, HWIO_GENERAL_CFG_ADDR(USB30_QSCRATCH_REG_BASE_OFFS), HWIO_GENERAL_CFG_PIPE3_PHYSTATUS_SW_BMSK);
    //When PIPE3_PHYSTATUS is high, Setting the PIPE3_SET_PHYSTATUS_SW bit will assert the PIPE_PHYSTATUS controller input 
    //So clear PHY status here
    HAL_HSUSB_AND (BaseAddr, HWIO_GENERAL_CFG_ADDR(USB30_QSCRATCH_REG_BASE_OFFS), ~HWIO_GENERAL_CFG_PIPE3_SET_PHYSTATUS_SW_BMSK);
    gBS->Stall (MICRO_STALL_UNITS(10));
    HAL_HSUSB_AND (BaseAddr, HWIO_GENERAL_CFG_ADDR(USB30_QSCRATCH_REG_BASE_OFFS), ~HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK);
  }
  else
  {
    /*12. SS PHY softreset:
      a. usb30_reg_gusb3pipectl_regs_p_gusb3pipectl[0][PHYSoftRst] = 0x1
      b. Wait 1 ms.
      c. usb30_reg_gusb3pipectl_regs_p_gusb3pipectl[0] [PHYSoftRst] = 0x0
      d. usb30_reg_gusb3pipectl_regs_p_gusb3pipectl[0] [DELAYP1TRANS] = 0x0
      e. usb30_reg_gusb2phycfg_regs_p_gusb2phycfg[0][PHYSOFTRST] = 0x1
      f. Wait 1 ms.
      g. usb30_reg_gusb2phycfg_regs_p_gusb2phycfg[0][PHYSOFTRST] = 0x0
      h. Wait 200 �s.
      i. Wait for the PLL to lock.
    */
    HAL_HSUSB_OR (BaseAddr, HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_OFFS(BaseAddr, 0),HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_PHYSOFTRST_BMSK);
    gBS->Stall (MILLI_STALL_UNITS(1));
    HAL_HSUSB_AND (BaseAddr, HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_OFFS(BaseAddr, 0), ~HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_PHYSOFTRST_BMSK);
    /* Set DELAYP1TRANS to 0 */
    HAL_HSUSB_AND (BaseAddr, HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_OFFS(BaseAddr, 0), ~HWIO_GUSB3PIPECTL_REGS_p_GUSB3PIPECTL_DELAYP1TRANS_BMSK);
              
    HAL_HSUSB_OR (BaseAddr, HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_OFFS(BaseAddr, 0), HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_PHYSOFTRST_BMSK);
    gBS->Stall (MILLI_STALL_UNITS(1));

    HAL_HSUSB_AND (BaseAddr, HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_OFFS(BaseAddr, 0), ~HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_PHYSOFTRST_BMSK);
    gBS->Stall (MICRO_STALL_UNITS(200));
  }

  /*
   13. Deassert the core softreset:
   usb30_reg_gctl[CORESOFTRESET] = 0x0
  */
  HAL_HSUSB_AND (BaseAddr, HWIO_GCTL_OFFS, ~HWIO_GCTL_CORESOFTRESET_BMSK);
  gBS->Stall (MICRO_STALL_UNITS(10));
  
  /*
    14. Reset the device core:
    a. usb30_reg_dctl[CSFTRST] = 0x1
    b. Wait until rusb30_reg_dctl[CSFTRST] returns to 0.
    c. Set the timeout value to 10 ms.
  */    
  HAL_HSUSB_AND (BaseAddr, HWIO_DCTL_OFFS, ~HWIO_DCTL_CSFTRST_BMSK);
  
  while ((HAL_HSUSB_READ (BaseAddr, HWIO_DCTL_OFFS)) & HWIO_DCTL_CSFTRST_BMSK) 
  {
    if (cnt >= 10) 
    {
      DBG(EFI_D_ERROR, "SSUSB link soft reset failed");
      Status = EFI_DEVICE_ERROR;
      goto ON_EXIT;
    }
    cnt++;
    gBS->Stall (MILLI_STALL_UNITS(1));
  }

  /*
    15. To save power, enable the hardware-based clock gating (not relevant for PBL):
    a. usb30_reg_cgctl[DBM_FSM_EN] = 0x1
  */
  HAL_HSUSB_OR (BaseAddr, HWIO_CGCTL_REG_OFFS, HWIO_CGCTL_REG_DBM_FSM_EN_BMSK);
  
  
ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
} // end SynopsysInitCommon




/**
  Performs host mode specific initialization on a Synopsys super speed USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully initialized
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered initializing USB core
**/
STATIC
EFI_STATUS
SynopsysInitHost (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status  =  EFI_SUCCESS;
  UINTN       BaseAddr;

  FNC_ENTER_MSG ();

  if (NULL == UsbCore) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }
  BaseAddr = UsbCore->BaseAddr;
  
  //2. Set usb30_reg_gctl:[PRTCAPDIR] = 0x1 (host), [U2EXIT_LFPS] = 0x1
  HAL_HSUSB_AND(BaseAddr,HWIO_GCTL_OFFS,~HWIO_GCTL_PRTCAPDIR_BMSK);
  HAL_HSUSB_OR (BaseAddr, HWIO_GCTL_OFFS, HWIO_GCTL_U2EXIT_LFPS_BMSK | ( (1 << HWIO_GCTL_PRTCAPDIR_SHFT)) );

  //3. Enable the wake on connect event for each port:
  HAL_HSUSB_OR (BaseAddr, HWIO_PORTSC_REGS_p_PORTSC_ADDR(0,0), HWIO_PORTSC_REGS_p_PORTSC_WCE_BMSK );
  HAL_HSUSB_OR (BaseAddr, HWIO_PORTSC_REGS_p_PORTSC_ADDR(0,1), HWIO_PORTSC_REGS_p_PORTSC_WCE_BMSK );

  //4. Set GRXTHRCFG based on the case 8000615753 values
  HAL_HSUSB_OR (BaseAddr, HWIO_GRXTHRCFG_ADDR(0),  (3 << HWIO_GRXTHRCFG_USBMAXRXBURSTSIZE_SHFT) |
  (3 << HWIO_GRXTHRCFG_USBRXPKTCNT_SHFT) | HWIO_GRXTHRCFG_USBRXPKTCNTSEL_BMSK);

  //5. Set the bus configuration 1K page pipe limit:
  HAL_HSUSB_AND(BaseAddr, HWIO_GSBUSCFG1_ADDR(0), ~HWIO_GSBUSCFG1_DESADRSPC_BMSK | ~HWIO_GSBUSCFG1_DATADRSPC_BMSK );
  HAL_HSUSB_OR(BaseAddr, HWIO_GSBUSCFG1_ADDR(0), (0xE << HWIO_GSBUSCFG1_PIPETRANSLIMIT_SHFT) | HWIO_GSBUSCFG1_EN1KPAGE_BMSK );

  //vsb: TODO:
  //6. Follow xHCI up to the run/stop bit enable.

  //vsb: rest of steps for power mgmt

ON_EXIT:
  FNC_LEAVE_MSG ();
  return Status;
} // end SynopsysInitHost



/**
  Performs host mode specific initialization on the primary Synopsys super
  speed USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully initialized
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered initializing USB core
**/
STATIC
EFI_STATUS
SynopsysInitHostPrimary (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status  =  EFI_SUCCESS;
  FNC_ENTER_MSG();

  if (UsbCore->CoreType != USB_CONFIG_SSUSB1)
  {
    Status  =  EFI_UNSUPPORTED;
    goto ON_EXIT;
  }
  
  Status = EnableVbusSS1();
  ERR_CHK("Failed to initialize VbusSS1");

  Status = SynopsysInitHost(UsbCore);
  
  ON_EXIT:  
  FNC_LEAVE_MSG();
  return Status;
}



/**
  Performs device mode specific initialization on a Synopsys super speed USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully initialized
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered initializing USB core
**/
STATIC
EFI_STATUS
SynopsysInitDevice (
  IN  QCOM_USB_CORE         *UsbCore
  )
{

  EFI_STATUS  Status  =  EFI_SUCCESS;
  UINTN       BaseAddr;
  UINT32      Data1;

  FNC_ENTER_MSG ();

  if (NULL == UsbCore) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }
  (void)Data1; // Make compiler happy

  BaseAddr = UsbCore->BaseAddr;
  USB_ASSERT_GOTO ((BaseAddr != 0), ON_EXIT);
   
  //vsb: TODO
  /* 
  2.Halt the host-only mock_utmi_clock
  */

  /*
    16. Enable the hardware LPM:
    usb30_reg_gusb2phycfg_regs_p_gusb2phycfg[ENBLSLPM] = 0x1
  */
  HAL_HSUSB_OR( BaseAddr, HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_OFFS(BaseAddr, 0), HWIO_GUSB2PHYCFG_REGS_p_GUSB2PHYCFG_ENBLSLPM_BMSK);

ON_EXIT:
FNC_LEAVE_MSG ();
return Status;


}


/**
  Performs device mode specific initialization on the primary Synopsys super
  speed USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully initialized
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered initializing USB core
**/
STATIC
EFI_STATUS
SynopsysInitDevicePrimary (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status  =  EFI_SUCCESS;

  FNC_ENTER_MSG();

  Status = SynopsysInitDevice(UsbCore);

  FNC_LEAVE_MSG();
  return Status;
}


/**
  Performs low power mode for Synopsys Controller/Pico, QMP phy
  Disable USB clocks
 
  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core sucessfully disabled USB SNPS Core clocks
  @retval EFI_UNSUPPORTED   USB core not supported
  @retval Others            Error encountered while disabling USB SNPS Core clocks
**/
STATIC
EFI_STATUS
SynopsysPrimEnterLpm (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  UINTN       ClockId;
  UINT32      FreqHz;

  FNC_ENTER_MSG();

  if (NULL == UsbCore) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  if (!UsbCore->isCoreInLPM)
  {
    // Reference Count 0 implies that we have not enabled clocks
    if (UsbCore->ClkRefCnt == 0)
    {
      // We need to set Phy Pipe Clock to XO frequency since QMP Phy is not yet configured and hence we cannot enable it
      Status = ClockProtocol->GetClockID (ClockProtocol, "gcc_usb3_phy_pipe_clk", &ClockId);
      WRN_CHK ("failed get clock ID for gcc_usb3_phy_pipe_clk");

      if (!EFI_ERROR (Status)) 
      {
        // Select Source as XO for gcc_usb3_phy_pipe_clk, otherwise we will see erros in enabling clocks
        Status = ClockProtocol->SetClockFreqHz (ClockProtocol, ClockId, USB3_PIPE_CLK_XO_MIN_FREQ_HZ,
          EFI_CLOCK_FREQUENCY_HZ_AT_LEAST, &FreqHz);
        WRN_CHK("failed to set USB30 Pipe clock frequency 19.2 MHz");

        DBG (EFI_D_INFO, "USB30 Pipe Clock frequency @(%d)",FreqHz);
      }

      // Enable clocks first, without that disable clocks doesn't work
      Status = InitUsbClocks (UsbCore->EnClockCount, UsbCore->EnClocks);
      WRN_CHK ("failed to enable USB clocks");
      // Increment reference count
      UsbCore->ClkRefCnt++;
    }
    
    //Assert controller & PHYs resets
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_BCR_OFFS, HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_PRIM_BCR_OFFS, HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3_PHY_BCR_OFFS, HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB3PHY_PHY_BCR_OFFS, HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK);

    Status = DisableUsbClocks (UsbCore->DisClockCount, UsbCore->DisClocks);
    WRN_CHK ("failed to disable USB clocks");

    // Disable USB Foot Switch (Power domain)
    ConfigPowerDomain(FALSE, USB30_PowerDomain );

    //  Enable SW PowerCollapse for USB30 Controller
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_30_GDSCR_OFFS, HWIO_GCC_USB_30_GDSCR_SW_COLLAPSE_BMSK);

    if (NULL != PmicNpaClientSS) 
    {
      npa_issue_required_request(PmicNpaClientSS, PMIC_NPA_MODE_ID_USB_PERPH_SUSPEND);   //Sends the request to RPM, no return
      DEBUG ((EFI_D_INFO, "SynopsysInitCommon : NPA Request to set PMIC_NPA_MODE_ID_USB_PERPH_SUSPEND \n"));
    }

    //Remove bus votes
    if (gNpaClientSS1Snoc)
    {
      npa_issue_required_request(gNpaClientSS1Snoc, 0);
    }
    if( gNpaClientAggre2)
    {
      npa_issue_required_request(gNpaClientAggre2, 0);
    }

    // Decrement reference count
    UsbCore->ClkRefCnt--;
    UsbCore->isCoreInLPM = TRUE;

    // Unknown Reference Count, Faulty scenario
    if (UsbCore->ClkRefCnt > 1)
    {
      WRN_CHK ("SynopsysPrimEnterLPM: Wrong clock Reference count= 0x%x", UsbCore->ClkRefCnt);
    }
  }
  else
  {
    WRN_CHK ("SynopsysPrimEnterLPM: Synopsys Core already in LPM");
  }

ON_EXIT:
  FNC_LEAVE_MSG();
  return Status;
}



/**
  Exits low power mode for Synopsys Controller/Pico, QMP phy
 
  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS
  
  @ Comments - Only purpose right now is to bring Pico Phy out of reset so that we can shutdown gracefully
**/
STATIC
EFI_STATUS
SynopsysSecEnterLpm (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;

  FNC_ENTER_MSG();

  if (!UsbCore->isCoreInLPM)
  {
    // Reference Count 0 implies that we have not enabled clocks
    if (UsbCore->ClkRefCnt == 0)
    {
      // Enable clocks first, without that disable clocks doesn't work
      Status = InitUsbClocks (UsbCore->EnClockCount, UsbCore->EnClocks);
      WRN_CHK ("failed to enable USB clocks");
      // Increment reference count
      UsbCore->ClkRefCnt++;
    }

    // QUSB2 Phy power down
    HAL_HSUSB_WRITE(PERIPH_SS_BASE, HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_OFFS ,
      (HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_CLAMP_N_EN_BMSK | 
       HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_FREEZIO_N_BMSK| 
       HWIO_PERIPH_SS_QUSB2PHY_SEC_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK ));

    //Assert controller & PHYs resets
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_USB_20_BCR_OFFS, HWIO_GCC_USB_20_BCR_BLK_ARES_BMSK);
    HAL_HSUSB_OR (GCC_CLK_CTL_REG_REG_BASE, HWIO_GCC_QUSB2PHY_SEC_BCR_OFFS, HWIO_GCC_QUSB2PHY_SEC_BCR_BLK_ARES_BMSK);

    Status = DisableUsbClocks (UsbCore->DisClockCount, UsbCore->DisClocks);
    WRN_CHK ("failed to disable USB clocks");
    // Decrement reference count
    UsbCore->ClkRefCnt--;
    UsbCore->isCoreInLPM = TRUE;

    //Vote off power rails
    if (NULL != PmicNpaClientSS) 
    {
      //vsb:TODO: requires SS2 NPA client. Do not turn off shared resources until then
      //npa_issue_required_request(PmicNpaClientSS, PMIC_NPA_MODE_ID_USB_PERPH_SUSPEND);   //Sends the request to RPM, no return
      //DEBUG ((EFI_D_INFO, "SynopsysInitCommon : NPA Request to set PMIC_NPA_MODE_ID_USB_PERPH_SUSPEND \n"));
    }

    //Remove bus votes
    if (gNpaClientSS2Snoc)
    {
      npa_issue_required_request(gNpaClientSS2Snoc, 0);
    }
    if( gNpaClientAggre2)
    {
      npa_issue_required_request(gNpaClientAggre1, 0);
    }
    if (gNpaClientPnoc)
    {
      npa_issue_required_request(gNpaClientPnoc, 0);
    }

    // Unknown Reference Count, Faulty scenario
    if (UsbCore->ClkRefCnt > 1)
    {
      WRN_CHK ("SynopsysEnterLPM: Wrong clock Reference count= 0x%x", UsbCore->ClkRefCnt);
    }
  }

  FNC_LEAVE_MSG();  
  return EFI_SUCCESS;
}



/**
  Exits low power mode for Synopsys Controller/Pico, QMP phy
 
  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS
  
  @ Comments - Only purpose right now is to bring Pico Phy out of reset so that we can shutdown gracefully
**/
STATIC
EFI_STATUS
SynopsysExitLpm (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  FNC_ENTER_MSG(); 

  // Perform GCC reset for controller and PHYs
  SynopsysGccReset (UsbCore);

  FNC_LEAVE_MSG();
  return EFI_SUCCESS;
}


/**
  Perform a hardware reset on a Synopsys USB core.

  @param [in]  UsbCore      The USB core instance

  @retval EFI_SUCCESS       USB core successfully reset
  @retval Others            Error encountered resetting USB core
**/
STATIC
EFI_STATUS
SynopsysReset (
  IN  QCOM_USB_CORE         *UsbCore
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;

  FNC_ENTER_MSG();

  if (NULL == UsbCore) {
    DBG(EFI_D_ERROR, "invalid parameter");
    Status = EFI_INVALID_PARAMETER;
    goto ON_EXIT;
  }

  // TODO

ON_EXIT:
  FNC_LEAVE_MSG();
  return Status;
}


