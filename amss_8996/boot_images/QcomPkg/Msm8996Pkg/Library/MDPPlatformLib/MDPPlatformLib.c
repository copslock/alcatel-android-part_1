/*=============================================================================

File: MDPPlatformLib.c

Panel Specific configuration file. This file contains configuration for 
Qualcomm specific platforms. This file is not necessary to be modified by
the OEM, however it could serve as sample. The OEMPlatformLib.c file should
be used by the OEM to add their own platform configurations. If the PcdPanelType
is 1 (OEM) then the configuration in OEMPlatformLib.c will be used instead of 
the configuration found here.

     Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
     Qualcomm Technologies Proprietary and Confidential.
=============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/EFIPmicGpio.h>
#include <Protocol/EFITlmm.h>
#include <Protocol/EFIPmicLpg.h>
#include <Protocol/EFIPmicMpp.h>
#include <Protocol/EFIPmicGpio.h>
#include <Protocol/EFIPmicVreg.h>
#include <Protocol/EFIPmicWled.h>
#include <Protocol/EFIPmicIbb.h>
#include <Protocol/EFIPmicLab.h>
#include "DDIChipInfo.h"
#include "MDPTypes.h"
#include "MDPPlatformLib.h"
#include "MDPSystem.h"
#include "OEMPlatformLib.h"
#include "HALDSILib.h"
#include "npa.h"
#include "pmapp_npa.h"


/* -----------------------------------------------------------------------
** Defines
** ----------------------------------------------------------------------- */
#define HDMI_CEC_DATA_GPIO                31
#define HDMI_DDC_CLK_GPIO                 32
#define HDMI_DDC_DATA_GPIO                33
#define HDMI_HPD_GPIO                     34

#define PCD_PANEL_TYPE_OEM                1

#define DSI_READ_ADDRESS_SIZE             2
#define DSI_READ_READBACK_SIZE            8

#define PLATFORM_PANEL_ID_MAX_COMMANDS    3       // maximum panel ID read commands

// PMIC Device Indices
#define PMIC_DEV_INDEX                    0       // PMIC device (Vreg, LDO, ect)
#define PMIC_PMI_DEV_INDEX                1       // PMIC interface device (IBB/LAB, MPP, WLED)


/* -----------------------------------------------------------------------
** Types
** ----------------------------------------------------------------------- */

/* Sample convenience structure to hold the data necessary to do a DSI read
   during DSI auto-detection. See DynamicDSIPanelDetection.
*/

typedef struct {
    uint8      address[DSI_READ_ADDRESS_SIZE];                                        // DCS command for panel ID
    uint8      expectedReadback[DSI_READ_READBACK_SIZE];                              // expected readback
} PlatformPanelIDCommandInfo;


typedef struct {
  uint8                         uCmdType;                                             // data type for panel ID DSI read
  uint32                        uTotalRetry;                                          // number of retry if DSI read fails
  PlatformPanelIDCommandInfo    panelIdCommands[PLATFORM_PANEL_ID_MAX_COMMANDS];      // commands for panel ID and expected readback
  uint32                        uLaneRemapOrder;                                      // Lane remap order
  const int8                   *psPanelCfg;                                           // pointer to the panel configuration
  uint32                        uPanelCfgSize;                                        // size of panel configuration data
  uint32                        uFlags;                                               // flags to set clock config for now, can set other config in future          
} PlatformDSIDetectParams;

typedef struct
{
  npa_client_handle               sNPAClient[MDP_DISPLAY_MAX];                          // NPA client handles for power control of panels.
  MDP_PmicModuleControlType       ePMICSecondaryPower[MDP_DISPLAY_MAX];                 // Configuration for PMIC based secondary power source 
} Panel_PowerCtrlParams;

/* -----------------------------------------------------------------------
** Local functions
** ----------------------------------------------------------------------- */

/* Platform detection 
*/
static MDP_Status ReadPlatformIDAndChipID(EFI_PLATFORMINFO_PLATFORM_INFO_TYPE *pPlatformInfo, EFIChipInfoIdType *pChipSetId, EFIChipInfoFamilyType *pChiSetFamily);
static MDP_Status DynamicDSIPanelDetection(MDPPlatformParams *pPlatformParams, uint32 *puPanelID);

/* 8996 Main Panel CDP/MTP Functions
*/
static MDP_Status Panel_CDP_PowerUp(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams);
static MDP_Status Panel_CDP_PowerDown(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams);
static MDP_Status Panel_CDP_Reset(void);
static MDP_Status Panel_CDP_BacklightLevel(MDP_Display_IDType eDisplayId, BacklightConfigType *pBacklightConfig);
static MDP_Status Panel_CDP_PeripheralPower(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams, bool32 bPowerUp);


/* 8996 HDMI Panel Functions
*/
static MDP_Status HDMI_CDP_PowerUp(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams);
static MDP_Status HDMI_CDP_PowerDown(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams);

/* 8996 Common Power Init function
*/
static MDP_Status Panel_PowerInit(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams);

static void Panel_SetWLEDTest(EFI_QCOM_PMIC_WLED_PROTOCOL  *PmicWledProtocol, bool32 bOLED);
/*===========================================================================
 Local Configuration Definitions
 ===========================================================================*/

/*
 * Qualcomm 8996 CDP Platform
 *
 */
static int8 Sharp_LS062R1SX01_DSI_xmldata[] = 
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
  "<PanelName>LS062R1SX01</PanelName>\n"
  " <PanelDescription>Sharp Dual DSI Panel (1600x2560 24bpp)</PanelDescription>\n"
  " <Group id='EDID Configuration'>\n"
  " <ManufactureID>0xAF0D</ManufactureID>\n"
  " <ProductCode>0x0011</ProductCode>\n"
  " <SerialNumber>0x000000</SerialNumber>\n"
  " <WeekofManufacture>0x09</WeekofManufacture>\n"
  " <YearofManufacture>0x13</YearofManufacture>\n"
  " <EDIDVersion>1</EDIDVersion>\n"
  " <EDIDRevision>3</EDIDRevision>\n"
  " <VideoInputDefinition>0x80</VideoInputDefinition>\n"
  " <HorizontalScreenSize>0x08</HorizontalScreenSize>\n"
  " <VerticalScreenSize>0x0D</VerticalScreenSize>\n"
  " <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>\n"
  " <FeatureSupport>0xA</FeatureSupport>\n"
  " <Red.GreenBits>0xCF</Red.GreenBits>\n"
  " <Blue.WhiteBits>0x45</Blue.WhiteBits>\n"
  " <RedX>0x90</RedX>\n"
  " <RedY>0x59</RedY>\n"
  " <GreenX>0x57</GreenX>\n"
  " <GreenY>0x95</GreenY>\n"
  " <BlueX>0x29</BlueX>\n"
  " <BlueY>0x1f</BlueY>\n"
  " <WhiteX>0x50</WhiteX>\n"
  " <WhiteY>0x54</WhiteY>\n"
  " <EstablishedTimingsI>0x0</EstablishedTimingsI>\n"
  " <EstablishedTimingsII>0x0</EstablishedTimingsII>\n"
  " <ManufacturesTiming>0x0</ManufacturesTiming>\n"
  " <StandardTimings1/>\n"
  " <StandardTimings2/>\n"
  " <StandardTimings3/>\n"
  " <StandardTimings4/>\n"
  " <StandardTimings5/>\n"
  " <StandardTimings6/>\n"
  " <StandardTimings7/>\n"
  " <SignalTimingInterface/>\n"
  "</Group>\n"
  "<Group id='Active Timing'>\n"
  " <HorizontalActive units='Dot Clocks'>1600</HorizontalActive>\n" 
  " <HorizontalFrontPorch units='Dot Clocks'>76</HorizontalFrontPorch>\n"
  " <HorizontalBackPorch units='Dot Clocks'>32</HorizontalBackPorch>\n"
  " <HorizontalSyncPulse units='Dot Clocks'>16</HorizontalSyncPulse>\n"
  " <HorizontalSyncSkew units='Dot Clocks'>0</HorizontalSyncSkew>\n"
  " <HorizontalLeftBorder units='Dot Clocks'>0</HorizontalLeftBorder>\n"
  " <HorizontalRightBorder units='Dot Clocks'>0</HorizontalRightBorder>\n"
  " <VerticalActive units='Dot Clocks'>2560</VerticalActive>\n"
  " <VerticalFrontPorch units='Lines'>4</VerticalFrontPorch>\n"
  " <VerticalBackPorch units='Lines'>2</VerticalBackPorch>\n"
  " <VerticalSyncPulse units='Lines'>2</VerticalSyncPulse>\n"
  " <VerticalSyncSkew units='Lines'>0</VerticalSyncSkew>\n"
  " <VerticalTopBorder units='Lines'>0</VerticalTopBorder>\n"
  " <VerticalBottomBorder units='Lines'>0</VerticalBottomBorder>\n"
  " <InvertDataPolarity>False</InvertDataPolarity>\n"
  " <InvertVsyncPolairty>False</InvertVsyncPolairty>\n"
  " <InvertHsyncPolarity>False</InvertHsyncPolarity>\n"
  " <BorderColor>0x0</BorderColor>\n"
  "</Group>\n"
  "<Group id='Display Interface'>\n"
  " <InterfaceType units='QDI_DisplayConnectType'>8</InterfaceType>\n"
  " <InterfaceColorFormat units='QDI_PixelFormatType'>3</InterfaceColorFormat>\n"
  "</Group>\n"
  "<Group id='DSI Interface'>\n"
  " <DSIChannelId units='DSI_Channel_IDType'>1</DSIChannelId>\n"
  " <DSIVirtualId units='DSI_Display_VCType'>0</DSIVirtualId>\n"
  " <DSIColorFormat units='DSI_ColorFormatType'>36</DSIColorFormat>\n"
  " <DSITrafficMode units='DSI_TrafficModeType'>1</DSITrafficMode>\n"
  " <DSILanes units='integer'>4</DSILanes>\n"
  " <DSIHsaHseAfterVsVe units='Bool'>False</DSIHsaHseAfterVsVe>\n"
  " <DSILowPowerModeInHFP units='Bool'>False</DSILowPowerModeInHFP>\n"
  " <DSILowPowerModeInHBP units='Bool'>False</DSILowPowerModeInHBP>\n"
  " <DSILowPowerModeInHSA units='Bool'>False</DSILowPowerModeInHSA>\n"
  " <DSILowPowerModeInBLLPEOF units='Bool'>True</DSILowPowerModeInBLLPEOF>\n"
  " <DSILowPowerModeInBLLP units='Bool'>True</DSILowPowerModeInBLLP>\n"
  " <DSIRefreshRate units='integer Q16.16'>0x3C0000</DSIRefreshRate>\n"
  " <DSIPhyDCDCMode units='Bool'>True</DSIPhyDCDCMode>\n"
  " <DSIControllerMapping>\n"
  "  00 01\n"
  " </DSIControllerMapping>\n" 
  "</Group>\n"
  "<DSIInitsequence>\n"
  " 05 11 00\n"
  " FF 08 \n"
  " 05 29 00\n"
  "</DSIInitSequence>\n"
  "<DSITermSequence>\n"
  " 05 28 00\n"
  " FF 08 \n"
  " 05 10 00\n"
  "</DSITermSequence>\n"
  "<Group id='Backlight Configuration'>"
  " <BacklightType units='MDP_Panel_BacklightType'>1</BacklightType>\n"
  " <BacklightPmicControlType units='MDP_PmicBacklightControlType'>2</BacklightPmicControlType>\n"  
  "</Group>\n";

/*
* LH450WX2 configuration for smart panel
*/
static int8 lge_LH450WX2_cmd_xmldata[] =
"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<PanelName>LH450WX2</PanelName>"
"<PanelDescription>LGE Command Mode Panel (768x1280 24 bpp)</PanelDescription>"
"<Group id=\"EDID Configuration\">"
" <ManufactureID>0xF330</ManufactureID>"
" <ProductCode>0x01C2</ProductCode>"
" <SerialNumber>0x000000</SerialNumber>"
" <WeekofManufacture>0x09</WeekofManufacture>"
" <YearofManufacture>0x13</YearofManufacture>"
" <EDIDVersion>1</EDIDVersion>"
" <EDIDRevision>3</EDIDRevision>"
" <VideoInputDefinition>0x80</VideoInputDefinition>"
" <HorizontalScreenSize>0x41</HorizontalScreenSize>"
" <VerticalScreenSize>0xcd</VerticalScreenSize>"
" <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>"
" <FeatureSupport>0x0</FeatureSupport>"
" <Red.GreenBits>0x0</Red.GreenBits>"
" <Blue.WhiteBits>0x0</Blue.WhiteBits>"
" <RedX>0x0</RedX>"
" <RedY>0x0</RedY>"
" <GreenX>0x0</GreenX>"
" <GreenY>0x0</GreenY>"
" <BlueX>0x0</BlueX>"
" <BlueY>0x0</BlueY>"
" <WhiteX>0x0</WhiteX>"
" <WhiteY>0x0</WhiteY>"
" <EstablishedTimingsI>0x0</EstablishedTimingsI>"
" <EstablishedTimingsII>0x0</EstablishedTimingsII>"
" <ManufacturesTiming>0x0</ManufacturesTiming>"
" <StandardTimings1/>"
" <StandardTimings2/>"
" <StandardTimings3/>"
" <StandardTimings4/>"
" <StandardTimings5/>"
" <StandardTimings6/>"
" <StandardTimings7/>"
" <SignalTimingInterface/>"
"</Group>"
"<Group id=\"Active Timing\">"
" <ReportedPixelClockFrequency units=\"Hz\">59710000</ReportedPixelClockFrequency>"
" <PixelClockFrequency units=\"Hz\">59710000</PixelClockFrequency>"
" <HorizontalActive units=\"Dot Clocks\">768</HorizontalActive>"
" <HorizontalFrontPorch units=\"Dot Clocks\">26</HorizontalFrontPorch>"
" <HorizontalBackPorch units=\"Dot Clocks\">26</HorizontalBackPorch>"
" <HorizontalSyncPulse units=\"Dot Clocks\">26</HorizontalSyncPulse>"
" <HorizontalSyncSkew units=\"Dot Clocks\">0</HorizontalSyncSkew>"
" <HorizontalLeftBorder units=\"Dot Clocks\">0</HorizontalLeftBorder>"
" <HorizontalRightBorder units=\"Dot Clocks\">0</HorizontalRightBorder>"
" <VerticalActive units=\"Dot Clocks\">1280</VerticalActive>"
" <VerticalFrontPorch units=\"Lines\">2</VerticalFrontPorch>"
" <VerticalBackPorch units=\"Lines\">2</VerticalBackPorch>"
" <VerticalSyncPulse units=\"Lines\">2</VerticalSyncPulse>"
" <VerticalSyncSkew units=\"Lines\">0</VerticalSyncSkew>"
" <VerticalTopBorder units=\"Lines\">0</VerticalTopBorder>"
" <VerticalBottomBorder units=\"Lines\">0</VerticalBottomBorder>"
" <InvertDataPolarity>False</InvertDataPolarity>"
" <InvertVsyncPolairty>False</InvertVsyncPolairty>"
" <InvertHsyncPolarity>False</InvertHsyncPolarity>"
" <BorderColor>0x0</BorderColor>"
"</Group>"
"<Group id=\"Display Interface\">"
" <InterfaceType units=\"QDI_DisplayConnectType\">9</InterfaceType>"
" <InterfaceColorFormat units=\"QDI_PixelFormatType\">3</InterfaceColorFormat>"
"</Group>"
"<Group id=\"DSI Interface\">"
" <DSIChannelId units=\"DSI_Channel_IDType\">2</DSIChannelId>"
" <DSIVirtualId units=\"DSI_Display_VCType\">0</DSIVirtualId>"
" <DSIColorFormat units=\"DSI_ColorFormatType\">36</DSIColorFormat>"
" <DSITrafficMode units=\"DSI_TrafficModeType\">1</DSITrafficMode>"
" <DSILanes units=\"integer\">4</DSILanes>"
" <DSIRefreshRate>0x3C0000</DSIRefreshRate>"
" <DSIBitClockFrequency>419991600</DSIBitClockFrequency>"
" <DSICmdSwapInterface units=\"Bool\">False</DSICmdSwapInterface>"
" <DSICmdUsingTrigger units=\"Bool\">False</DSICmdUsingTrigger>"
" <DSIEnableAutoRefresh units=\"Bool\">True</DSIEnableAutoRefresh>"
" <DSIAutoRefreshFrameNumDiv units=\"integer\">1</DSIAutoRefreshFrameNumDiv>"
" <DSIInitMasterTime units=\"ms\">120</DSIInitMasterTime>"
" <DSITECheckEnable>True</DSITECheckEnable>"
" <DSITEUsingDedicatedTEPin>True</DSITEUsingDedicatedTEPin>"
" <DSITEvSyncStartPos>5</DSITEvSyncStartPos>"
" <DSITEvSyncContinueLines>0x2000</DSITEvSyncContinueLines>"
" <DSITEvSyncStartLineDivisor>320</DSITEvSyncStartLineDivisor>"
" <DSITEPercentVariance>0x000A0000</DSITEPercentVariance>"
" <DSITEvSyncSelect>0</DSITEvSyncSelect>"
"</Group>"
"<DSIInitSequence>"
" 05 11 00\n"
" ff 78\n"
" 15 36 00\n"
" 15 53 2C\n"
" 15 51 32\n"
" 15 55 03\n"
" 15 35 00\n"
" 05 29 00\n"
"</DSIInitSequence>"
"<Group id='Backlight Configuration'>"
" <BacklightType units='MDP_Panel_BacklightType'>1</BacklightType>\n"
" <BacklightPmicControlType units='MDP_PmicBacklightControlType'>1</BacklightPmicControlType>\n"
"</Group>\n";

static int8 truly_cmd_DSI_xmldata[] = 
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
  "<PanelName>TDO-HD0466K40002-0810</PanelName>\n"
  "<PanelDescription>Command mode 720pHD Truly DSI Panel (720x1280 24bpp)</PanelDescription>\n"
  "<Group id='EDID Configuration'>\n"
  " <ManufactureID>0xAF0D</ManufactureID>\n"
  " <ProductCode>0x0011</ProductCode>\n"
  " <SerialNumber>0x000000</SerialNumber>\n"
  " <WeekofManufacture>0x09</WeekofManufacture>\n"
  " <YearofManufacture>0x13</YearofManufacture>\n"
  " <EDIDVersion>1</EDIDVersion>\n"
  " <EDIDRevision>3</EDIDRevision>\n"
  " <VideoInputDefinition>0x80</VideoInputDefinition>\n"
  " <HorizontalScreenSize>0x16</HorizontalScreenSize>\n"
  " <VerticalScreenSize>0x0D</VerticalScreenSize>\n"
  " <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>\n"
  " <FeatureSupport>0xA</FeatureSupport>\n"
  " <Red.GreenBits>0xCF</Red.GreenBits>\n"
  " <Blue.WhiteBits>0x45</Blue.WhiteBits>\n"
  " <RedX>0x90</RedX>\n"
  " <RedY>0x59</RedY>\n"
  " <GreenX>0x57</GreenX>\n"
  " <GreenY>0x95</GreenY>\n"
  " <BlueX>0x29</BlueX>\n"
  " <BlueY>0x1f</BlueY>\n"
  " <WhiteX>0x50</WhiteX>\n"
  " <WhiteY>0x54</WhiteY>\n"
  " <EstablishedTimingsI>0x0</EstablishedTimingsI>\n"
  " <EstablishedTimingsII>0x0</EstablishedTimingsII>\n"
  " <ManufacturesTiming>0x0</ManufacturesTiming>\n"
  " <StandardTimings1/>\n"
  " <StandardTimings2/>\n"
  " <StandardTimings3/>\n"
  " <StandardTimings4/>\n"
  " <StandardTimings5/>\n"
  " <StandardTimings6/>\n"
  " <StandardTimings7/>\n"
  " <SignalTimingInterface/>\n"
  "</Group>\n"
  "<Group id='Active Timing'>\n"
  " <HorizontalActive units='Dot Clocks'>720</HorizontalActive>\n"
  " <HorizontalFrontPorch units='Dot Clocks'>164</HorizontalFrontPorch>\n"
  " <HorizontalBackPorch units='Dot Clocks'>140</HorizontalBackPorch>\n"
  " <HorizontalSyncPulse units='Dot Clocks'>8</HorizontalSyncPulse>\n"
  " <HorizontalSyncSkew units='Dot Clocks'>0</HorizontalSyncSkew>\n"
  " <HorizontalLeftBorder units='Dot Clocks'>0</HorizontalLeftBorder>\n"
  " <HorizontalRightBorder units='Dot Clocks'>0</HorizontalRightBorder>\n"
  " <VerticalActive units='Dot Clocks'>1280</VerticalActive>\n"
  " <VerticalFrontPorch units='Lines'>6</VerticalFrontPorch>\n"
  " <VerticalBackPorch units='Lines'>1</VerticalBackPorch>\n"
  " <VerticalSyncPulse units='Lines'>1</VerticalSyncPulse>\n"
  " <VerticalSyncSkew units='Lines'>0</VerticalSyncSkew>\n"
  " <VerticalTopBorder units='Lines'>0</VerticalTopBorder>\n"
  " <VerticalBottomBorder units='Lines'>0</VerticalBottomBorder>\n"
  " <InvertDataPolarity>False</InvertDataPolarity>\n"
  " <InvertVsyncPolairty>False</InvertVsyncPolairty>\n"
  " <InvertHsyncPolarity>False</InvertHsyncPolarity>\n"
  " <BorderColor>0x0</BorderColor>\n"
  "</Group>\n"
  "<Group id='Display Interface'>\n"
  " <InterfaceType units='QDI_DisplayConnectType'>9</InterfaceType>\n"
  " <InterfaceColorFormat units='QDI_PixelFormatType'>3</InterfaceColorFormat>\n"
  "</Group>\n"
  "<Group id='DSI Interface'>\n"
  " <DSIChannelId units='DSI_Channel_IDType'>2</DSIChannelId>\n"
  " <DSIVirtualId units='DSI_Display_VCType'>0</DSIVirtualId>\n"
  " <DSIColorFormat units='DSI_ColorFormatType'>36</DSIColorFormat>\n"
  " <DSITrafficMode units='DSI_TrafficModeType'>1</DSITrafficMode>\n"
  " <DSILanes units='integer'>4</DSILanes>\n"
  " <DSIHsaHseAfterVsVe units='Bool'>False</DSIHsaHseAfterVsVe>\n"
  " <DSILowPowerModeInHFP units='Bool'>False</DSILowPowerModeInHFP>\n"
  " <DSILowPowerModeInHBP units='Bool'>False</DSILowPowerModeInHBP>\n"
  " <DSILowPowerModeInHSA units='Bool'>False</DSILowPowerModeInHSA>\n"
  " <DSILowPowerModeInBLLPEOF units='Bool'>False</DSILowPowerModeInBLLPEOF>\n"
  " <DSILowPowerModeInBLLP units='Bool'>False</DSILowPowerModeInBLLP>\n"
  " <DSIRefreshRate units='integer Q16.16'>0x3C0000</DSIRefreshRate>\n"
  " <DSIPhyDCDCMode units='Bool'>True</DSIPhyDCDCMode>\n"
  " <DSIEnableAutoRefresh units=\"Bool\">True</DSIEnableAutoRefresh>\n"
  " <DSIAutoRefreshFrameNumDiv units=\"integer\">1</DSIAutoRefreshFrameNumDiv>\n"
  "</Group>\n"
  "<DSIInitSequence>\n"
  " 15 FF EE\n"
  " 15 26 08\n"
  " 15 26 00\n"
  " 15 FF 00\n"
  " FF 0A\n"
  " 15 BA 03\n"
  " 15 C2 08\n"
  " 15 FF 01\n"
  " 15 FB 01\n"
  " 15 00 4A\n"
  " 15 01 33\n"
  " 15 02 53\n"
  " 15 03 55\n"
  " 15 04 55\n"
  " 15 05 33\n"
  " 15 06 22\n"
  " 15 08 56\n"
  " 15 09 8F\n"
  " 15 36 73\n"
  " 15 0B 9F\n"
  " 15 0C 9F\n"
  " 15 0D 2F\n"
  " 15 0E 24\n"
  " 15 11 83\n"
  " 15 12 03\n"
  " 15 71 2C\n"
  " 15 6F 03\n"
  " 15 0F 0A\n"
  " 15 FF 05\n"
  " 15 FB 01\n"
  " 15 01 00\n"
  " 15 02 82\n"
  " 15 03 82\n"
  " 15 04 82\n"
  " 15 05 30\n"
  " 15 06 33\n"
  " 15 07 01\n"
  " 15 08 00\n"
  " 15 09 46\n"
  " 15 0A 46\n"
  " 15 0D 0B\n"
  " 15 0E 1D\n"
  " 15 0F 08\n"
  " 15 10 53\n"
  " 15 11 00\n"
  " 15 12 00\n"
  " 15 14 01\n"
  " 15 15 00\n"
  " 15 16 05\n"
  " 15 17 00\n"
  " 15 19 7F\n"
  " 15 1A FF\n"
  " 15 1B 0F\n"
  " 15 1C 00\n"
  " 15 1D 00\n"
  " 15 1E 00\n"
  " 15 1F 07\n"
  " 15 20 00\n"
  " 15 21 06\n"
  " 15 22 55\n"
  " 15 23 4D\n"
  " 15 2D 02\n"
  " 15 28 01\n"
  " 15 2F 02\n"
  " 15 83 01\n"
  " 15 9E 58\n"
  " 15 9F 6A\n"
  " 15 A0 01\n"
  " 15 A2 10\n"
  " 15 BB 0A\n"
  " 15 BC 0A\n"
  " 15 32 08\n"
  " 15 33 B8\n"
  " 15 36 01\n"
  " 15 37 00\n"
  " 15 43 00\n"
  " 15 4B 21\n"
  " 15 4C 03\n"
  " 15 50 21\n"
  " 15 51 03\n"
  " 15 58 21\n"
  " 15 59 03\n"
  " 15 5D 21\n"
  " 15 5E 03\n"
  " 15 6C 00\n"
  " 15 6D 00\n"
  " 15 FB 01\n"
  " 15 FF 01\n"
  " 15 FB 01\n"
  " 15 75 00\n"
  " 15 76 7D\n"
  " 15 77 00\n"
  " 15 78 8A\n"
  " 15 79 00\n"
  " 15 7A 9C\n"
  " 15 7B 00\n"
  " 15 7C B1\n"
  " 15 7D 00\n"
  " 15 7E BF\n"
  " 15 7F 00\n"
  " 15 80 CF\n"
  " 15 81 00\n"
  " 15 82 DD\n"
  " 15 83 00\n"
  " 15 84 E8\n"
  " 15 85 00\n"
  " 15 86 F2\n"
  " 15 87 01\n"
  " 15 88 1F\n"
  " 15 89 01\n"
  " 15 8A 41\n"
  " 15 8B 01\n"
  " 15 8C 78\n"
  " 15 8D 01\n"
  " 15 8E A5\n"
  " 15 8F 01\n"
  " 15 90 EE\n"
  " 15 91 02\n"
  " 15 92 29\n"
  " 15 93 02\n"
  " 15 94 2A\n"
  " 15 95 02\n"
  " 15 96 5D\n"
  " 15 97 02\n"
  " 15 98 93\n"
  " 15 99 02\n"
  " 15 9A B8\n"
  " 15 9B 02\n"
  " 15 9C E7\n"
  " 15 9D 03\n"
  " 15 9E 07\n"
  " 15 9F 03\n"
  " 15 A0 37\n"
  " 15 A2 03\n"
  " 15 A3 46\n"
  " 15 A4 03\n"
  " 15 A5 56\n"
  " 15 A6 03\n"
  " 15 A7 66\n"
  " 15 A9 03\n"
  " 15 AA 7A\n"
  " 15 AB 03\n"
  " 15 AC 93\n"
  " 15 AD 03\n"
  " 15 AE A3\n"
  " 15 AF 03\n"
  " 15 B0 B4\n"
  " 15 B1 03\n"
  " 15 B2 CB\n"
  " 15 B3 00\n"
  " 15 B4 7D\n"
  " 15 B5 00\n"
  " 15 B6 8A\n"
  " 15 B7 00\n"
  " 15 B8 9C\n"
  " 15 B9 00\n"
  " 15 BA B1\n"
  " 15 BB 00\n"
  " 15 BC BF\n"
  " 15 BD 00\n"
  " 15 BE CF\n"
  " 15 BF 00\n"
  " 15 C0 DD\n"
  " 15 C1 00\n"
  " 15 C2 E8\n"
  " 15 C3 00\n"
  " 15 C4 F2\n"
  " 15 C5 01\n"
  " 15 C6 1F\n"
  " 15 C7 01\n"
  " 15 C8 41\n"
  " 15 C9 01\n"
  " 15 CA 78\n"
  " 15 CB 01\n"
  " 15 CC A5\n"
  " 15 CD 01\n"
  " 15 CE EE\n"
  " 15 CF 02\n"
  " 15 D0 29\n"
  " 15 D1 02\n"
  " 15 D2 2A\n"
  " 15 D3 02\n"
  " 15 D4 5D\n"
  " 15 D5 02\n"
  " 15 D6 93\n"
  " 15 D7 02\n"
  " 15 D8 B8\n"
  " 15 D9 02\n"
  " 15 DA E7\n"
  " 15 DB 03\n"
  " 15 DC 07\n"
  " 15 DD 03\n"
  " 15 DE 37\n"
  " 15 DF 03\n"
  " 15 E0 46\n"
  " 15 E1 03\n"
  " 15 E2 56\n"
  " 15 E3 03\n"
  " 15 E4 66\n"
  " 15 E5 03\n"
  " 15 E6 7A\n"
  " 15 E7 03\n"
  " 15 E8 93\n"
  " 15 E9 03\n"
  " 15 EA A3\n"
  " 15 EB 03\n"
  " 15 EC B4\n"
  " 15 ED 03\n"
  " 15 EE CB\n"
  " 15 EF 00\n"
  " 15 F0 ED\n"
  " 15 F1 00\n"
  " 15 F2 F3\n"
  " 15 F3 00\n"
  " 15 F4 FE\n"
  " 15 F5 01\n"
  " 15 F6 09\n"
  " 15 F7 01\n"
  " 15 F8 13\n"
  " 15 F9 01\n"
  " 15 FA 1D\n"
  " 15 FF 02\n"
  " 15 FB 01\n"
  " 15 00 01\n"
  " 15 01 26\n"
  " 15 02 01\n"
  " 15 03 2F\n"
  " 15 04 01\n"
  " 15 05 37\n"
  " 15 06 01\n"
  " 15 07 56\n"
  " 15 08 01\n"
  " 15 09 70\n"
  " 15 0A 01\n"
  " 15 0B 9D\n"
  " 15 0C 01\n"
  " 15 0D C2\n"
  " 15 0E 01\n"
  " 15 0F FF\n"
  " 15 10 02\n"
  " 15 11 31\n"
  " 15 12 02\n"
  " 15 13 32\n"
  " 15 14 02\n"
  " 15 15 60\n"
  " 15 16 02\n"
  " 15 17 94\n"
  " 15 18 02\n"
  " 15 19 B5\n"
  " 15 1A 02\n"
  " 15 1B E3\n"
  " 15 1C 03\n"
  " 15 1D 03\n"
  " 15 1E 03\n"
  " 15 1F 2D\n"
  " 15 20 03\n"
  " 15 21 3A\n"
  " 15 22 03\n"
  " 15 23 48\n"
  " 15 24 03\n"
  " 15 25 57\n"
  " 15 26 03\n"
  " 15 27 68\n"
  " 15 28 03\n"
  " 15 29 7B\n"
  " 15 2A 03\n"
  " 15 2B 90\n"
  " 15 2D 03\n"
  " 15 2F A0\n"
  " 15 30 03\n"
  " 15 31 CB\n"
  " 15 32 00\n"
  " 15 33 ED\n"
  " 15 34 00\n"
  " 15 35 F3\n"
  " 15 36 00\n"
  " 15 37 FE\n"
  " 15 38 01\n"
  " 15 39 09\n"
  " 15 3A 01\n"
  " 15 3B 13\n"
  " 15 3D 01\n"
  " 15 3F 1D\n"
  " 15 40 01\n"
  " 15 41 26\n"
  " 15 42 01\n"
  " 15 43 2F\n"
  " 15 44 01\n"
  " 15 45 37\n"
  " 15 46 01\n"
  " 15 47 56\n"
  " 15 48 01\n"
  " 15 49 70\n"
  " 15 4A 01\n"
  " 15 4B 9D\n"
  " 15 4C 01\n"
  " 15 4D C2\n"
  " 15 4E 01\n"
  " 15 4F FF\n"
  " 15 50 02\n"
  " 15 51 31\n"
  " 15 52 02\n"
  " 15 53 32\n"
  " 15 54 02\n"
  " 15 55 60\n"
  " 15 56 02\n"
  " 15 58 94\n"
  " 15 59 02\n"
  " 15 5A B5\n"
  " 15 5B 02\n"
  " 15 5C E3\n"
  " 15 5D 03\n"
  " 15 5E 03\n"
  " 15 5F 03\n"
  " 15 60 2D\n"
  " 15 61 03\n"
  " 15 62 3A\n"
  " 15 63 03\n"
  " 15 64 48\n"
  " 15 65 03\n"
  " 15 66 57\n"
  " 15 67 03\n"
  " 15 68 68\n"
  " 15 69 03\n"
  " 15 6A 7B\n"
  " 15 6B 03\n"
  " 15 6C 90\n"
  " 15 6D 03\n"
  " 15 6E A0\n"
  " 15 6F 03\n"
  " 15 70 CB\n"
  " 15 71 00\n"
  " 15 72 19\n"
  " 15 73 00\n"
  " 15 74 36\n"
  " 15 75 00\n"
  " 15 76 55\n"
  " 15 77 00\n"
  " 15 78 70\n"
  " 15 79 00\n"
  " 15 7A 83\n"
  " 15 7B 00\n"
  " 15 7C 99\n"
  " 15 7D 00\n"
  " 15 7E A8\n"
  " 15 7F 00\n"
  " 15 80 B7\n"
  " 15 81 00\n"
  " 15 82 C5\n"
  " 15 83 00\n"
  " 15 84 F7\n"
  " 15 85 01\n"
  " 15 86 1E\n"
  " 15 87 01\n"
  " 15 88 60\n"
  " 15 89 01\n"
  " 15 8A 95\n"
  " 15 8B 01\n"
  " 15 8C E1\n"
  " 15 8D 02\n"
  " 15 8E 20\n"
  " 15 8F 02\n"
  " 15 90 23\n"
  " 15 91 02\n"
  " 15 92 59\n"
  " 15 93 02\n"
  " 15 94 94\n"
  " 15 95 02\n"
  " 15 96 B4\n"
  " 15 97 02\n"
  " 15 98 E1\n"
  " 15 99 03\n"
  " 15 9A 01\n"
  " 15 9B 03\n"
  " 15 9C 28\n"
  " 15 9D 03\n"
  " 15 9E 30\n"
  " 15 9F 03\n"
  " 15 A0 37\n"
  " 15 A2 03\n"
  " 15 A3 3B\n"
  " 15 A4 03\n"
  " 15 A5 40\n"
  " 15 A6 03\n"
  " 15 A7 50\n"
  " 15 A9 03\n"
  " 15 AA 6D\n"
  " 15 AB 03\n"
  " 15 AC 80\n"
  " 15 AD 03\n"
  " 15 AE CB\n"
  " 15 AF 00\n"
  " 15 B0 19\n"
  " 15 B1 00\n"
  " 15 B2 36\n"
  " 15 B3 00\n"
  " 15 B4 55\n"
  " 15 B5 00\n"
  " 15 B6 70\n"
  " 15 B7 00\n"
  " 15 B8 83\n"
  " 15 B9 00\n"
  " 15 BA 99\n"
  " 15 BB 00\n"
  " 15 BC A8\n"
  " 15 BD 00\n"
  " 15 BE B7\n"
  " 15 BF 00\n"
  " 15 C0 C5\n"
  " 15 C1 00\n"
  " 15 C2 F7\n"
  " 15 C3 01\n"
  " 15 C4 1E\n"
  " 15 C5 01\n"
  " 15 C6 60\n"
  " 15 C7 01\n"
  " 15 C8 95\n"
  " 15 C9 01\n"
  " 15 CA E1\n"
  " 15 CB 02\n"
  " 15 CC 20\n"
  " 15 CD 02\n"
  " 15 CE 23\n"
  " 15 CF 02\n"
  " 15 D0 59\n"
  " 15 D1 02\n"
  " 15 D2 94\n"
  " 15 D3 02\n"
  " 15 D4 B4\n"
  " 15 D5 02\n"
  " 15 D6 E1\n"
  " 15 D7 03\n"
  " 15 D8 01\n"
  " 15 D9 03\n"
  " 15 DA 28\n"
  " 15 DB 03\n"
  " 15 DC 30\n"
  " 15 DD 03\n"
  " 15 DE 37\n"
  " 15 DF 03\n"
  " 15 E0 3B\n"
  " 15 E1 03\n"
  " 15 E2 40\n"
  " 15 E3 03\n"
  " 15 E4 50\n"
  " 15 E5 03\n"
  " 15 E6 6D\n"
  " 15 E7 03\n"
  " 15 E8 80\n"
  " 15 E9 03\n"
  " 15 EA CB\n"
  " 15 FF 01\n"
  " 15 FB 01\n"
  " 15 FF 02\n"
  " 15 FB 01\n" 
  " 15 FF 04\n"
  " 15 FB 01\n"
  " 15 FF 00\n"
  " 05 11\n"
  " FF 64\n"
  " 15 FF EE\n"
  " 15 12 50\n"
  " 15 13 02\n"
  " 15 6A 60\n"
  " 15 FF 00\n"
  " FE 00 00 00\n"
  " FF 10\n"
  " 05 29\n"
  "</DSIInitSequence>\n"
  "<DSITermSequence>\n"
  " 05 28 00\n"
  " FF 32\n"
  " 05 10 00\n"
  " FF 78\n"
  "</DSITermSequence>\n"
  "<Group id='Backlight Configuration'>"
  " <BacklightType units='MDP_Panel_BacklightType'>1</BacklightType>\n"
  " <BacklightPmicControlType units='MDP_PmicBacklightControlType'>1</BacklightPmicControlType>\n"  
  "</Group>\n";


/*
* Dummy panel configuration, default fallback mode.
*
*/
static int8 dummy_xmldata[] =
"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<PanelName>Dummy</PanelName>"
"<PanelDescription>Dummy Panel Interface</PanelDescription>"
"<Group id=\"Active Timing\">"
" <HorizontalActive units=\"Dot Clocks\">800</HorizontalActive>"
" <HorizontalFrontPorch units=\"Dot Clocks\">0</HorizontalFrontPorch>"
" <HorizontalBackPorch units=\"Dot Clocks\">0</HorizontalBackPorch>"
" <HorizontalSyncPulse units=\"Dot Clocks\">0</HorizontalSyncPulse>"
" <HorizontalSyncSkew units=\"Dot Clocks\">0</HorizontalSyncSkew>"
" <HorizontalLeftBorder units=\"Dot Clocks\">0</HorizontalLeftBorder>"
" <HorizontalRightBorder units=\"Dot Clocks\">0</HorizontalRightBorder>"
" <VerticalActive units=\"Dot Clocks\">600</VerticalActive>"
" <VerticalFrontPorch units=\"Lines\">0</VerticalFrontPorch>"
" <VerticalBackPorch units=\"Lines\">0</VerticalBackPorch>"
" <VerticalSyncPulse units=\"Lines\">0</VerticalSyncPulse>"
" <VerticalSyncSkew units=\"Lines\">0</VerticalSyncSkew>"
" <VerticalTopBorder units=\"Lines\">0</VerticalTopBorder>"
" <VerticalBottomBorder units=\"Lines\">0</VerticalBottomBorder>"
" <InvertDataPolarity>False</InvertDataPolarity>"
" <InvertVsyncPolairty>False</InvertVsyncPolairty>"
" <InvertHsyncPolarity>False</InvertHsyncPolarity>"
" <BorderColor>0x0</BorderColor>"
"</Group>"
"<Group id=\"Display Interface\">"
" <InterfaceType units=\"QDI_DisplayConnectType\">0</InterfaceType>"
" <InterfaceColorFormat units=\"QDI_PixelFormatType\">3</InterfaceColorFormat>"
"</Group>";

/*
* SS 1080p AMOLED configuration for video mode
*/
static int8 ss_AMS555HB09_xmldata[] =
"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<PanelName>AMS499QP01</PanelName>"
"<PanelDescription>4.99\" AMOLED Video Mode (1080x1920 24 bpp)</PanelDescription>"
"<Group id='EDID Configuration'>\n"
" <ManufactureID>0xAF0D</ManufactureID>\n"
" <ProductCode>0x0011</ProductCode>\n"
" <SerialNumber>0x000000</SerialNumber>\n"
" <WeekofManufacture>0x09</WeekofManufacture>\n"
" <YearofManufacture>0x13</YearofManufacture>\n"
" <EDIDVersion>1</EDIDVersion>\n"
" <EDIDRevision>3</EDIDRevision>\n"
" <VideoInputDefinition>0x80</VideoInputDefinition>\n"
" <HorizontalScreenSize>0x16</HorizontalScreenSize>\n"
" <VerticalScreenSize>0x0D</VerticalScreenSize>\n"
" <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>\n"
" <FeatureSupport>0xA</FeatureSupport>\n"
" <Red.GreenBits>0xCF</Red.GreenBits>\n"
" <Blue.WhiteBits>0x45</Blue.WhiteBits>\n"
" <RedX>0x90</RedX>\n"
" <RedY>0x59</RedY>\n"
" <GreenX>0x57</GreenX>\n"
" <GreenY>0x95</GreenY>\n"
" <BlueX>0x29</BlueX>\n"
" <BlueY>0x1f</BlueY>\n"
" <WhiteX>0x50</WhiteX>\n"
" <WhiteY>0x54</WhiteY>\n"
" <EstablishedTimingsI>0x0</EstablishedTimingsI>\n"
" <EstablishedTimingsII>0x0</EstablishedTimingsII>\n"
" <ManufacturesTiming>0x0</ManufacturesTiming>\n"
" <StandardTimings1/>\n"
" <StandardTimings2/>\n"
" <StandardTimings3/>\n"
" <StandardTimings4/>\n"
" <StandardTimings5/>\n"
" <StandardTimings6/>\n"
" <StandardTimings7/>\n"
" <SignalTimingInterface/>\n"
"</Group>\n"
"<Group id='Active Timing'>\n"
" <HorizontalActive units='Dot Clocks'>1080</HorizontalActive>\n"
" <HorizontalFrontPorch units='Dot Clocks'>32</HorizontalFrontPorch>\n"
" <HorizontalBackPorch units='Dot Clocks'>12</HorizontalBackPorch>\n"
" <HorizontalSyncPulse units='Dot Clocks'>8</HorizontalSyncPulse>\n"
" <HorizontalSyncSkew units='Dot Clocks'>0</HorizontalSyncSkew>\n"
" <HorizontalLeftBorder units='Dot Clocks'>0</HorizontalLeftBorder>\n"
" <HorizontalRightBorder units='Dot Clocks'>0</HorizontalRightBorder>\n"
" <VerticalActive units='Dot Clocks'>1920</VerticalActive>\n"
" <VerticalFrontPorch units='Lines'>12</VerticalFrontPorch>\n"
" <VerticalBackPorch units='Lines'>7</VerticalBackPorch>\n"
" <VerticalSyncPulse units='Lines'>5</VerticalSyncPulse>\n"
" <VerticalSyncSkew units='Lines'>0</VerticalSyncSkew>\n"
" <VerticalTopBorder units='Lines'>0</VerticalTopBorder>\n"
" <VerticalBottomBorder units='Lines'>0</VerticalBottomBorder>\n"
" <InvertDataPolarity>False</InvertDataPolarity>\n"
" <InvertVsyncPolairty>False</InvertVsyncPolairty>\n"
" <InvertHsyncPolarity>False</InvertHsyncPolarity>\n"
" <BorderColor>0x0</BorderColor>\n"
"</Group>\n"
"<Group id='Display Interface'>\n"
" <InterfaceType units='QDI_DisplayConnectType'>8</InterfaceType>\n"
" <InterfaceColorFormat units='QDI_PixelFormatType'>3</InterfaceColorFormat>\n"
"</Group>\n"
"<Group id='DSI Interface'>\n"
" <DSIChannelId units='DSI_Channel_IDType'>1</DSIChannelId>\n"
" <DSIVirtualId units='DSI_Display_VCType'>0</DSIVirtualId>\n"
" <DSIColorFormat units='DSI_ColorFormatType'>36</DSIColorFormat>\n"
" <DSITrafficMode units='DSI_TrafficModeType'>2</DSITrafficMode>\n"
" <DSILanes units='integer'>4</DSILanes>\n"
" <DSIHsaHseAfterVsVe units='Bool'>False</DSIHsaHseAfterVsVe>\n"
" <DSILowPowerModeInHFP units='Bool'>False</DSILowPowerModeInHFP>\n"
" <DSILowPowerModeInHBP units='Bool'>False</DSILowPowerModeInHBP>\n"
" <DSILowPowerModeInHSA units='Bool'>False</DSILowPowerModeInHSA>\n"
" <DSILowPowerModeInBLLPEOF units='Bool'>TRUE</DSILowPowerModeInBLLPEOF>\n"
" <DSILowPowerModeInBLLP units='Bool'>True</DSILowPowerModeInBLLP>\n"
" <DSIRefreshRate units='integer Q16.16'>0x3C0000</DSIRefreshRate>\n"
" <DSIPhyDCDCMode units='Bool'>True</DSIPhyDCDCMode>\n"
"</Group>\n"
"<DSIInitSequence>\n"
" 15 FE 07\n"
" 15 A9 6A\n"
" 15 FE 08\n"
" 15 AA C0\n"
" 15 A4 E0\n"
" 15 92 0F\n"
" 15 9A 07\n"
" 15 9B 0C\n"
" 15 9C 03\n"
" 15 D4 17\n"
" 15 D5 15\n"
" 15 D6 13\n"
" 15 D7 10\n"
" 15 D8 0D\n"
" 15 D9 07\n"
" 15 DA 03\n"
" 15 FE 00\n"
" 15 C2 09\n"
" 15 55 00\n"
" 05 11\n"
" FF 78\n"
" 05 29\n"
" FF 32\n"
" 15 FE 07\n"
" 15 A9 EA\n"
" 15 FE 00\n"
"</DSIInitSequence>\n"
"<Group id='Backlight Configuration'>\n"
" <BacklightType units='MDP_Panel_BacklightType'>0</BacklightType>\n"
" <BacklightPmicControlType units='MDP_PmicBacklightControlType'>0</BacklightPmicControlType>\n"
"</Group>\n";

  /*
  * LPM101A119A configuration for 4K panel
  */
static int8 jdi_LH450WX2_xmldata[] =
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
  "<PanelName>LPM101A119A</PanelName>"
  "<PanelDescription>JDI 4K panel LPM101A119A (3840(H)x2160(V) 24 bpp)</PanelDescription>"
  "<Group id=\"EDID Configuration\">"
  "  <ManufactureID>0xF330</ManufactureID>"
  "  <ProductCode>0x01C2</ProductCode>"
  "  <SerialNumber>0x000000</SerialNumber>"
  "  <WeekofManufacture>0x09</WeekofManufacture>"
  "  <YearofManufacture>0x13</YearofManufacture>"
  "  <EDIDVersion>1</EDIDVersion>"
  "  <EDIDRevision>3</EDIDRevision>"
  "  <VideoInputDefinition>0x80</VideoInputDefinition>"
  "  <HorizontalScreenSize>0x41</HorizontalScreenSize>"
  "  <VerticalScreenSize>0xcd</VerticalScreenSize>"
  "  <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>"
  "  <FeatureSupport>0x0</FeatureSupport>"
  "  <Red.GreenBits>0x0</Red.GreenBits>"
  "  <Blue.WhiteBits>0x0</Blue.WhiteBits>"
  "  <RedX>0x0</RedX>"
  "  <RedY>0x0</RedY>"
  "  <GreenX>0x0</GreenX>"
  "  <GreenY>0x0</GreenY>"
  "  <BlueX>0x0</BlueX>"
  "  <BlueY>0x0</BlueY>"
  "  <WhiteX>0x0</WhiteX>"
  "  <WhiteY>0x0</WhiteY>"
  "  <EstablishedTimingsI>0x0</EstablishedTimingsI>"
  "  <EstablishedTimingsII>0x0</EstablishedTimingsII>"
  "  <ManufacturesTiming>0x0</ManufacturesTiming>"
  "  <StandardTimings1/>"
  "  <StandardTimings2/>"
  "  <StandardTimings3/>"
  "  <StandardTimings4/>"
  "  <StandardTimings5/>"
  "  <StandardTimings6/>"
  "  <StandardTimings7/>"
  "  <SignalTimingInterface/>"
  "</Group>"
  "<Group id=\"Active Timing\">"
  "  <HorizontalActive units=\"Dot Clocks\">3840</HorizontalActive>"
  "  <HorizontalFrontPorch units=\"Dot Clocks\">100</HorizontalFrontPorch>"
  "  <HorizontalBackPorch units=\"Dot Clocks\">80</HorizontalBackPorch>"
  "  <HorizontalSyncPulse units=\"Dot Clocks\">12</HorizontalSyncPulse>"
  "  <HorizontalSyncSkew units=\"Dot Clocks\">0</HorizontalSyncSkew>"
  "  <HorizontalLeftBorder units=\"Dot Clocks\">0</HorizontalLeftBorder>"
  "  <HorizontalRightBorder units=\"Dot Clocks\">0</HorizontalRightBorder>"
  "  <VerticalActive units=\"Dot Clocks\">2160</VerticalActive>"
  "  <VerticalFrontPorch units=\"Lines\">16</VerticalFrontPorch>"
  "  <VerticalBackPorch units=\"Lines\">16</VerticalBackPorch>"
  "  <VerticalSyncPulse units=\"Lines\">4</VerticalSyncPulse>"
  "  <VerticalSyncSkew units=\"Lines\">0</VerticalSyncSkew>"
  "  <VerticalTopBorder units=\"Lines\">0</VerticalTopBorder>"
  "  <VerticalBottomBorder units=\"Lines\">0</VerticalBottomBorder>"
  "  <InvertDataPolarity>False</InvertDataPolarity>"
  "  <InvertVsyncPolairty>False</InvertVsyncPolairty>"
  "  <InvertHsyncPolarity>False</InvertHsyncPolarity>"
  "  <BorderColor>0x0</BorderColor>"
  "</Group>"
  "<Group id='Display Interface'>\n"
  "  <InterfaceType units='QDI_DisplayConnectType'>8</InterfaceType>\n"
  "  <InterfaceColorFormat units='QDI_PixelFormatType'>3</InterfaceColorFormat>\n"
  "</Group>\n"
  "<Group id='DSI Interface'>\n"
  "  <DSIChannelId units='DSI_Channel_IDType'>1</DSIChannelId>\n"
  "  <DSIVirtualId units='DSI_Display_VCType'>0</DSIVirtualId>\n"
  "  <DSIColorFormat units='DSI_ColorFormatType'>36</DSIColorFormat>\n"
  "  <DSITrafficMode units='DSI_TrafficModeType'>1</DSITrafficMode>\n"
  "  <DSILanes units='integer'>4</DSILanes>\n"
  "  <DSIHsaHseAfterVsVe units='Bool'>False</DSIHsaHseAfterVsVe>\n"
  "  <DSILowPowerModeInHFP units='Bool'>False</DSILowPowerModeInHFP>\n"
  "  <DSILowPowerModeInHBP units='Bool'>False</DSILowPowerModeInHBP>\n"
  "  <DSILowPowerModeInHSA units='Bool'>False</DSILowPowerModeInHSA>\n"
  "  <DSILowPowerModeInBLLPEOF units='Bool'>True</DSILowPowerModeInBLLPEOF>\n"
  "  <DSILowPowerModeInBLLP units='Bool'>True</DSILowPowerModeInBLLP>\n"
  "  <DSIRefreshRate units='integer Q16.16'>0x3C0000</DSIRefreshRate>\n"
  "  <DSIPhyDCDCMode units='Bool'>False</DSIPhyDCDCMode>\n"
  "  <DSIControllerMapping>\n"
  "     00 01\n"
  "  </DSIControllerMapping>\n"
  "  <DSIFBCEnable units='Bool'>True</DSIFBCEnable>\n"
  "  <DSIFBCProfileID units='integer'>1</DSIFBCProfileID>\n"
  "  <DSIForceCmdInVideoHS units='Bool'>True</DSIForceCmdInVideoHS>\n"
  "</Group>\n"
  "<DSIInitSequence>"
  "   FD 03 \n"
  "   15 51 FF\n"
  "   15 53 24\n"
  "   05 11 00\n"
  "   ff 78\n"
  "   05 29 00\n"
  "</DSIInitSequence>"
  "<DSITermSequence>\n"
  "   05 28 00\n"
  "   FF 14\n"
  "   05 10 00\n"
  "   FF 32\n"
  "</DSITermSequence>\n"
  "<Group id='Backlight Configuration'>"
  "  <BacklightType units='MDP_Panel_BacklightType'>1</BacklightType>\n"
  "  <BacklightPmicModule units='MDP_PmicModuleControlType'>2</BacklightPmicModule>\n"
  "</Group>\n";


/*
* LS050T1SC01K Sharp 4.95" FHD command mode panel
*/
static int8 sharp_cmd_DSI_xmldata[] = 
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
  "<PanelName>LS050T1SC01K</PanelName>\n"
  "<PanelDescription>Command mode 4.95in  Sharp DSI Panel (1080x1920 24bpp)</PanelDescription>\n"
  "<Group id='EDID Configuration'>\n"
  " <ManufactureID>0xAF0D</ManufactureID>\n"
  " <ProductCode>0x0011</ProductCode>\n"
  " <SerialNumber>0x000000</SerialNumber>\n"
  " <WeekofManufacture>0x09</WeekofManufacture>\n"
  " <YearofManufacture>0x13</YearofManufacture>\n"
  " <EDIDVersion>1</EDIDVersion>\n"
  " <EDIDRevision>3</EDIDRevision>\n"
  " <VideoInputDefinition>0x80</VideoInputDefinition>\n"
  " <HorizontalScreenSize>0x16</HorizontalScreenSize>\n"
  " <VerticalScreenSize>0x0D</VerticalScreenSize>\n"
  " <DisplayTransferCharacteristics>0x78</DisplayTransferCharacteristics>\n"
  " <FeatureSupport>0xA</FeatureSupport>\n"
  " <Red.GreenBits>0xCF</Red.GreenBits>\n"
  " <Blue.WhiteBits>0x45</Blue.WhiteBits>\n"
  " <RedX>0x90</RedX>\n"
  " <RedY>0x59</RedY>\n"
  " <GreenX>0x57</GreenX>\n"
  " <GreenY>0x95</GreenY>\n"
  " <BlueX>0x29</BlueX>\n"
  " <BlueY>0x1f</BlueY>\n"
  " <WhiteX>0x50</WhiteX>\n"
  " <WhiteY>0x54</WhiteY>\n"
  " <EstablishedTimingsI>0x0</EstablishedTimingsI>\n"
  " <EstablishedTimingsII>0x0</EstablishedTimingsII>\n"
  " <ManufacturesTiming>0x0</ManufacturesTiming>\n"
  " <StandardTimings1/>\n"
  " <StandardTimings2/>\n"
  " <StandardTimings3/>\n"
  " <StandardTimings4/>\n"
  " <StandardTimings5/>\n"
  " <StandardTimings6/>\n"
  " <StandardTimings7/>\n"
  " <SignalTimingInterface/>\n"
  "</Group>\n"
  "<Group id='Active Timing'>\n"
  " <HorizontalActive units='Dot Clocks'>1080</HorizontalActive>\n"
  " <HorizontalFrontPorch units='Dot Clocks'>0</HorizontalFrontPorch>\n"
  " <HorizontalBackPorch units='Dot Clocks'>0</HorizontalBackPorch>\n"
  " <HorizontalSyncPulse units='Dot Clocks'>0</HorizontalSyncPulse>\n"
  " <HorizontalSyncSkew units='Dot Clocks'>0</HorizontalSyncSkew>\n"
  " <HorizontalLeftBorder units='Dot Clocks'>0</HorizontalLeftBorder>\n"
  " <HorizontalRightBorder units='Dot Clocks'>0</HorizontalRightBorder>\n"
  " <VerticalActive units='Dot Clocks'>1920</VerticalActive>\n"
  " <VerticalFrontPorch units='Lines'>0</VerticalFrontPorch>\n"
  " <VerticalBackPorch units='Lines'>0</VerticalBackPorch>\n"
  " <VerticalSyncPulse units='Lines'>0</VerticalSyncPulse>\n"
  " <VerticalSyncSkew units='Lines'>0</VerticalSyncSkew>\n"
  " <VerticalTopBorder units='Lines'>0</VerticalTopBorder>\n"
  " <VerticalBottomBorder units='Lines'>0</VerticalBottomBorder>\n"
  " <InvertDataPolarity>False</InvertDataPolarity>\n"
  " <InvertVsyncPolairty>False</InvertVsyncPolairty>\n"
  " <InvertHsyncPolarity>False</InvertHsyncPolarity>\n"
  " <BorderColor>0x0</BorderColor>\n"
  "</Group>\n"
  "<Group id='Display Interface'>\n"
  " <InterfaceType units='QDI_DisplayConnectType'>9</InterfaceType>\n"
  " <InterfaceColorFormat units='QDI_PixelFormatType'>3</InterfaceColorFormat>\n"
  "</Group>\n"
  "<Group id='DSI Interface'>\n"
  " <DSIChannelId units='DSI_Channel_IDType'>2</DSIChannelId>\n"
  " <DSIVirtualId units='DSI_Display_VCType'>0</DSIVirtualId>\n"
  " <DSIColorFormat units='DSI_ColorFormatType'>36</DSIColorFormat>\n"
  " <DSITrafficMode units='DSI_TrafficModeType'>1</DSITrafficMode>\n"
  " <DSILanes units='integer'>4</DSILanes>\n"
  " <DSIHsaHseAfterVsVe units='Bool'>False</DSIHsaHseAfterVsVe>\n"
  " <DSILowPowerModeInHFP units='Bool'>False</DSILowPowerModeInHFP>\n"
  " <DSILowPowerModeInHBP units='Bool'>False</DSILowPowerModeInHBP>\n"
  " <DSILowPowerModeInHSA units='Bool'>False</DSILowPowerModeInHSA>\n"
  " <DSILowPowerModeInBLLPEOF units='Bool'>False</DSILowPowerModeInBLLPEOF>\n"
  " <DSILowPowerModeInBLLP units='Bool'>False</DSILowPowerModeInBLLP>\n"
  " <DSIRefreshRate units='integer Q16.16'>0x3C0000</DSIRefreshRate>\n"
  " <DSIPhyDCDCMode units='Bool'>True</DSIPhyDCDCMode>\n"
  " <DSIEnableAutoRefresh units=\"Bool\">True</DSIEnableAutoRefresh>"
  " <DSIAutoRefreshFrameNumDiv units=\"integer\">1</DSIAutoRefreshFrameNumDiv>"
  "</Group>\n"
  "<DSIInitSequence>\n"
  " 15 BB 10\n"  
  " 15 B0 00\n"  
  " 05 11\n"
  " FF 64\n"
  " 15 51 FF\n"
  " 15 53 24\n"
  " 15 FF 23\n"
  " 15 08 05\n"
  " 15 46 90\n"
  " 15 FF 10\n"
  " 15 FF F0\n"
  " 15 92 01\n"
  " 15 FF 10\n"
  " 15 35 00\n"
  " 05 29\n"
  " FF 28\n"  
  "</DSIInitSequence>\n"
  "<DSITermSequence>\n"
  "</DSITermSequence>\n"
  "<Group id='Backlight Configuration'>"
  " <BacklightType units='MDP_Panel_BacklightType'>0</BacklightType>\n"
  " <BacklightPmicControlType units='MDP_PmicBacklightControlType'>1</BacklightPmicControlType>\n"  
  "</Group>\n";


/*===========================================================================
Function Definitions
===========================================================================*/


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: panel_ConfigGPIO()

** 
** DESCRIPTION:
**   Initialize LCD panel GPIOs and PMICs
**
*//* -------------------------------------------------------------------- */
MDP_Status MDPPlatformConfigure(MDP_Display_IDType eDisplayId, MDPPlatformConfigType eConfig, MDPPlatformParams *pPlatformParams)
{
  MDP_Status  eStatus  = MDP_STATUS_OK;
  uint32      uPanelID = 0;

  /* Static information, initial once during the first call */
  static bool32                 bInitFlag           = FALSE;
  static MDPPlatformInfo        sPlatformInfo;
  static Panel_PowerCtrlParams  sPanelPowerCtrl;

  if (FALSE == bInitFlag)
  {
    MDP_OSAL_MEMZERO(&sPlatformInfo,    sizeof(MDPPlatformInfo));
    MDP_OSAL_MEMZERO(&sPanelPowerCtrl,  sizeof(Panel_PowerCtrlParams));
    bInitFlag = TRUE;
  }

  // If PCD is configured for OEM then defer all calls to OEM function
  if (PCD_PANEL_TYPE_OEM == PcdGet32(PcdPanelType))
  {
    eStatus = OEMPlatformConfigure(eDisplayId, eConfig, pPlatformParams);
  }
  else
  {
    // Read the platform ID once
    if (FALSE == sPlatformInfo.bPlatformDetected)
    {
        if (MDP_STATUS_OK == ReadPlatformIDAndChipID(&sPlatformInfo.sEFIPlatformType, &sPlatformInfo.sEFIChipSetId, &sPlatformInfo.sEFIChipSetFamily))
        {
            sPlatformInfo.bPlatformDetected = TRUE;
        }
    }
    
    switch (eConfig)
    {
    case MDPPLATFORM_CONFIG_SW_RENDERER:

      if ((EFI_PLATFORMINFO_TYPE_UNKNOWN == sPlatformInfo.sEFIPlatformType.platform) ||
          (TRUE == PcdGetBool(PcdDisplayForceSwRenderer)))
      {
        // Treat unknown platforms as a SW model only
        pPlatformParams->sPlatformInfo.bSWRender = TRUE;
        DEBUG ((EFI_D_WARN, "DisplayDxe: SW renderer only\n"));
      }
      else
      {
        pPlatformParams->sPlatformInfo.bSWRender = FALSE;
      }
      break;
    case MDPPLATFORM_CONFIG_GETPANELCONFIG:
        {
            pPlatformParams->sPlatformPanel.uDefaultVendor   = 0;
            pPlatformParams->sPlatformPanel.uDefaultRevision = 0;

            // Retrieve panel configuration (could be dependent on the interface)
            switch (eDisplayId)
            {
            case MDP_DISPLAY_PRIMARY:

                // Report the proper information depending on the display.
                switch (sPlatformInfo.sEFIPlatformType.platform)
                {
                case EFI_PLATFORMINFO_TYPE_SURF:
                    // On this platform we could have a DSI or eDP panel, try to detect DSI first in DynamicDSIPanelDetection
                    if (MDP_STATUS_OK != DynamicDSIPanelDetection(pPlatformParams, &uPanelID))
                    {
                      // Default to Sharp WQXGA panel
                      pPlatformParams->sPlatformPanel.pPanelXMLConfig = (int8*)&Sharp_LS062R1SX01_DSI_xmldata;                 
                      pPlatformParams->sPlatformPanel.uConfigSize     = sizeof(Sharp_LS062R1SX01_DSI_xmldata);
                    }
                    sPlatformInfo.uPrimaryPanelId = uPanelID;
                    break;
                case EFI_PLATFORMINFO_TYPE_MTP_MSM:
                case EFI_PLATFORMINFO_TYPE_FLUID:
                case EFI_PLATFORMINFO_TYPE_MTP_MDM:
                    pPlatformParams->sPlatformPanel.pPanelXMLConfig = (int8*)&Sharp_LS062R1SX01_DSI_xmldata;                 
                    pPlatformParams->sPlatformPanel.uConfigSize     = sizeof(Sharp_LS062R1SX01_DSI_xmldata);
                    break;
                case EFI_PLATFORMINFO_TYPE_LIQUID:
                    pPlatformParams->sPlatformPanel.pPanelXMLConfig = (int8*)&jdi_LH450WX2_xmldata;
                    pPlatformParams->sPlatformPanel.uConfigSize     = sizeof(jdi_LH450WX2_xmldata);
                    break;
                default:          
                    pPlatformParams->sPlatformPanel.pPanelXMLConfig = dummy_xmldata;
                    pPlatformParams->sPlatformPanel.uConfigSize     = sizeof(dummy_xmldata);
                    break;
                }
                break;

            case MDP_DISPLAY_EXTERNAL:
                pPlatformParams->sPlatformPanel.pPanelXMLConfig = dummy_xmldata;
                pPlatformParams->sPlatformPanel.uConfigSize     = sizeof(dummy_xmldata);

                pPlatformParams->sHDMIPinConfig.uCecDataGpio = HDMI_CEC_DATA_GPIO;
                pPlatformParams->sHDMIPinConfig.uDDDClkGpio  = HDMI_DDC_CLK_GPIO;
                pPlatformParams->sHDMIPinConfig.uDDCDataGpio = HDMI_DDC_DATA_GPIO;  
                pPlatformParams->sHDMIPinConfig.uHPDGpio     = HDMI_HPD_GPIO;
                break;

            default:
              break;
            }
        }
        break;
    case MDPPLATFORM_CONFIG_POWERUP:
      {
        // Handle power up
        if((EFI_PLATFORMINFO_TYPE_UNKNOWN != sPlatformInfo.sEFIPlatformType.platform) &&
           (EFI_PLATFORMINFO_TYPE_VIRTIO  != sPlatformInfo.sEFIPlatformType.platform))
        {
            Panel_PowerInit(eDisplayId, &sPanelPowerCtrl);
        }

        switch (eDisplayId)
        {
        case MDP_DISPLAY_PRIMARY:

          // Config based on the platform
          switch (sPlatformInfo.sEFIPlatformType.platform)
          {
          case EFI_PLATFORMINFO_TYPE_QT:
          case EFI_PLATFORMINFO_TYPE_SURF:
          case EFI_PLATFORMINFO_TYPE_MTP_MSM:
          case EFI_PLATFORMINFO_TYPE_FLUID:
          case EFI_PLATFORMINFO_TYPE_MTP_MDM: 

              // Populate the local power configuration (Force to IBB/LAB for LCD)
              //
              // Note: OLED users configure this as MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_OLED
              //          An incorrect configuration can damage the panel.
              // 
              sPanelPowerCtrl.ePMICSecondaryPower[eDisplayId] = MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_LCD;   // LCD Power Configuration (IBB/LAB)
              // sPanelPowerCtrl.ePMICSecondaryPower[eDisplayId] = MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_OLED;   // OLED Power Configuration (IBB/LAB)

              // Primary Power Sequence
              if (MDP_STATUS_OK != (eStatus = Panel_CDP_PowerUp(eDisplayId, &sPanelPowerCtrl)))
              {
                  DEBUG((EFI_D_WARN, "DisplayDxe: Primary Power Up Sequence Failed (%d)\n", eStatus));
              }
              else if (MDP_STATUS_OK != (eStatus = Panel_CDP_PeripheralPower(eDisplayId, &sPanelPowerCtrl, TRUE)))  // Secondary Power Sequence
              {
                  DEBUG((EFI_D_WARN, "DisplayDxe: Secondary Power Up Sequence Failed (%d)\n", eStatus));
              }
              break;
          case EFI_PLATFORMINFO_TYPE_LIQUID:
            eStatus = MDP_STATUS_NOT_SUPPORTED;
            break;
          default:
            break;
          }

          break;

        case MDP_DISPLAY_EXTERNAL:

          // Config based on the platform
          switch (sPlatformInfo.sEFIPlatformType.platform)
          {
          case EFI_PLATFORMINFO_TYPE_QT:
          case EFI_PLATFORMINFO_TYPE_MTP_MSM:
          case EFI_PLATFORMINFO_TYPE_FLUID:
          case EFI_PLATFORMINFO_TYPE_MTP_MDM:
            // HDMI is currently enabled only on CDP and Liquid platform
            break;
          case EFI_PLATFORMINFO_TYPE_SURF:
            eStatus = HDMI_CDP_PowerUp(eDisplayId, &sPanelPowerCtrl);
            break;
          case EFI_PLATFORMINFO_TYPE_LIQUID:
            eStatus = MDP_STATUS_NOT_SUPPORTED;
            break;
          default:
            break;
          }
          break;

        default:
          break;

        }

      }
      break;

    case MDPPLATFORM_CONFIG_POWERDOWN:
      {
        // Handle power down
        switch (eDisplayId)
        {
        case MDP_DISPLAY_PRIMARY:

          // Config based on the platform
          switch (sPlatformInfo.sEFIPlatformType.platform)
          {
          case EFI_PLATFORMINFO_TYPE_QT:
          case EFI_PLATFORMINFO_TYPE_SURF:
          case EFI_PLATFORMINFO_TYPE_MTP_MSM:
          case EFI_PLATFORMINFO_TYPE_FLUID:
          case EFI_PLATFORMINFO_TYPE_MTP_MDM: 

              if (MDP_STATUS_OK != (eStatus = Panel_CDP_PowerDown(eDisplayId, &sPanelPowerCtrl)))
              {
              DEBUG ((EFI_D_WARN, "DisplayDxe: Primary Power Down Sequence Failed (%d)\n", eStatus));
              }
              else if (MDP_STATUS_OK != (eStatus = Panel_CDP_PeripheralPower(eDisplayId, &sPanelPowerCtrl, FALSE)))  // Secondary Power Sequence
              {
              DEBUG ((EFI_D_WARN, "DisplayDxe: Secondary Power Down Sequence Failed (%d)\n", eStatus));
              }
              break;
          case EFI_PLATFORMINFO_TYPE_LIQUID:
            eStatus = MDP_STATUS_NOT_SUPPORTED;
            break;
          default:
            break;
          }

          break;
        case MDP_DISPLAY_EXTERNAL:
          // Config based on the platform
          switch (sPlatformInfo.sEFIPlatformType.platform)
          {
          case EFI_PLATFORMINFO_TYPE_QT:
          case EFI_PLATFORMINFO_TYPE_MTP_MSM:
          case EFI_PLATFORMINFO_TYPE_FLUID:
          case EFI_PLATFORMINFO_TYPE_MTP_MDM: 
            eStatus = MDP_STATUS_NOT_SUPPORTED;
            break;
          case EFI_PLATFORMINFO_TYPE_SURF:
            eStatus = HDMI_CDP_PowerDown(eDisplayId, &sPanelPowerCtrl);
            break;
          case EFI_PLATFORMINFO_TYPE_LIQUID:
            eStatus = MDP_STATUS_NOT_SUPPORTED;
            break;
          default:
            break;
          }
          break;
        default:
          break;
        }
      }
      break;
    case MDPPLATFORM_CONFIG_SETBACKLIGHT:
        {
            // Handle backlight level
            switch (eDisplayId)
            {
            case MDP_DISPLAY_PRIMARY:
              switch (sPlatformInfo.sEFIPlatformType.platform)
              {
              case EFI_PLATFORMINFO_TYPE_QT:
              case EFI_PLATFORMINFO_TYPE_SURF:
              case EFI_PLATFORMINFO_TYPE_MTP_MSM:
              case EFI_PLATFORMINFO_TYPE_FLUID:
              case EFI_PLATFORMINFO_TYPE_MTP_MDM: 

                  eStatus = Panel_CDP_BacklightLevel(eDisplayId, &pPlatformParams->sBacklightConfig);
                  break;

              case EFI_PLATFORMINFO_TYPE_LIQUID:
                eStatus = MDP_STATUS_NOT_SUPPORTED;
                break;

              default:
                break;
              }
              break;
            case MDP_DISPLAY_EXTERNAL:
                eStatus = MDP_STATUS_NOT_SUPPORTED;
                break;
            default:
              break;
            }
        }
        break;
    case MDPPLATFORM_CONFIG_GETPANELID:
        {
        }
        break;
    case MDPPLATFORM_CONFIG_GETPLATFORMINFO:
        {
          //
          // Return platform information
          //
          MDP_OSAL_MEMCPY(&pPlatformParams->sPlatformInfo, &sPlatformInfo, sizeof(MDPPlatformInfo));
        }
        break;
    case MDPPLATFORM_CONFIG_RESETPANEL:
        {
            // Handle power down
            switch (eDisplayId)
            {
            case MDP_DISPLAY_PRIMARY:
              
                // Config based on the platform
                switch (sPlatformInfo.sEFIPlatformType.platform)
                {
                case EFI_PLATFORMINFO_TYPE_QT:
                case EFI_PLATFORMINFO_TYPE_SURF:
                case EFI_PLATFORMINFO_TYPE_MTP_MSM:
                case EFI_PLATFORMINFO_TYPE_FLUID:
                case EFI_PLATFORMINFO_TYPE_MTP_MDM: 
                  eStatus = Panel_CDP_Reset();
                  break; 
                case EFI_PLATFORMINFO_TYPE_LIQUID:
                  eStatus = MDP_STATUS_NOT_SUPPORTED;
                  break;
                default:
                  break;
                }
                
                break;
            case MDP_DISPLAY_EXTERNAL:
                eStatus = MDP_STATUS_NOT_SUPPORTED;
                break;
            default:
              break;
            }
        }
        break;
    default:
        eStatus = MDP_STATUS_BAD_PARAM;
        break;
    }
  }
  
  return eStatus;
}



/*===========================================================================
Private Function Definitions
===========================================================================*/

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: ReadPlatformIDAndChipID()
** 
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
static MDP_Status ReadPlatformIDAndChipID(EFI_PLATFORMINFO_PLATFORM_INFO_TYPE *pPlatformInfo, EFIChipInfoIdType *pChipSetId, EFIChipInfoFamilyType *pChipSetFamily)
{
    MDP_Status                           eStatus                = MDP_STATUS_OK;
    EFI_PLATFORMINFO_PROTOCOL            *hPlatformInfoProtocol; 
    EFI_CHIPINFO_PROTOCOL                *hChipInfoProtocol;

    if(EFI_SUCCESS == gBS->LocateProtocol (&gEfiPlatformInfoProtocolGuid,
                                           NULL,
                                           (VOID **) &hPlatformInfoProtocol))
    {
        if (EFI_SUCCESS != hPlatformInfoProtocol->GetPlatformInfo(hPlatformInfoProtocol, pPlatformInfo))
        {
            DEBUG ((EFI_D_WARN, "DisplayDxe: gEfiPlatformInfoProtocolGuid->GetPlatformInfo() Failed.\n"));

            eStatus = MDP_STATUS_FAILED;
        }
    }
    else
    {
        DEBUG ((EFI_D_WARN, "DisplayDxe: gEfiPlatformInfoProtocolGuid protocol Failed.\n"));        

        eStatus = MDP_STATUS_FAILED;
    }

    if(EFI_PLATFORMINFO_TYPE_UNKNOWN == pPlatformInfo->platform)
    {
        pPlatformInfo->platform = EFI_PLATFORMINFO_TYPE_VIRTIO;
    }

    // Read the chipset ID
    if(EFI_SUCCESS == gBS->LocateProtocol (&gEfiChipInfoProtocolGuid,
                                           NULL,
                                           (VOID **) &hChipInfoProtocol))
    {
        if (EFI_SUCCESS != hChipInfoProtocol->GetChipId(hChipInfoProtocol, pChipSetId))
        {
            DEBUG ((EFI_D_WARN, "DisplayDxe: gEfiChipInfoProtocolGuid->GetChipId() Failed. \n"));

            eStatus = MDP_STATUS_FAILED;
        }
        if (EFI_SUCCESS != hChipInfoProtocol->GetChipFamily(hChipInfoProtocol, pChipSetFamily))
        {
            DEBUG ((EFI_D_WARN, "DisplayDxe: gEfiChipInfoProtocolGuid->GetChipFamily() Failed. \n"));

            eStatus = MDP_STATUS_FAILED;
        }

    }
    else
    {
        DEBUG ((EFI_D_WARN, "DisplayDxe: gEfiChipInfoProtocolGuid protocol Failed.\n"));

        eStatus = MDP_STATUS_FAILED;
    }

    return eStatus;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_PowerInit()
** 
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
static MDP_Status Panel_PowerInit(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams)
{
  MDP_Status    Status = MDP_STATUS_OK;
  typedef struct
  {
    char* cResourceName;
    char* cClientName;
  }NPAClientName;
  
  NPAClientName aNPAClientName[MDP_DISPLAY_MAX] =
  {
    {PMIC_NPA_GROUP_ID_DISP_PRIM,     "DisplayPrim"},
    {PMIC_NPA_GROUP_ID_DISP_SEC,      "DisplaySec"},
    {PMIC_NPA_GROUP_ID_DISP_EXT_HDMI, "DisplayHDMI"},
  };

  if (eDisplayId >= MDP_DISPLAY_MAX )
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Unsupported Display ID for power init.\n"));
    Status =  MDP_STATUS_FAILED;
  }
  else if (NULL == pPowerParams->sNPAClient[eDisplayId])
  {
    pPowerParams->sNPAClient[eDisplayId] = npa_create_sync_client( aNPAClientName[eDisplayId].cResourceName, aNPAClientName[eDisplayId].cClientName, NPA_CLIENT_REQUIRED); 
    
    if (NULL == pPowerParams->sNPAClient[eDisplayId])
    {
      DEBUG ((EFI_D_ERROR, "DisplayDxe: Failed to retrieve NPA Display Handle for Display ID %x.\n", eDisplayId ));
      Status =  MDP_STATUS_FAILED;
    }
  }

  return Status;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_CDP_PowerUp()
** 
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
static MDP_Status Panel_CDP_PowerUp(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams)
{
    MDP_Status                    Status = MDP_STATUS_OK;
    EFI_TLMM_PROTOCOL            *TLMMProtocol = NULL;

    if (NULL == pPowerParams->sNPAClient[eDisplayId])
    {
        DEBUG((EFI_D_ERROR, "DisplayDxe: NULL Handle for Primary display NPA node.\n"));
        Status = MDP_STATUS_NO_RESOURCES;
    }
    else if (EFI_SUCCESS != gBS->LocateProtocol(&gEfiTLMMProtocolGuid, NULL, (void**)&TLMMProtocol))
    {
        DEBUG((EFI_D_ERROR, "DisplayDxe: Locate TLMM protocol failed!\n"));
        Status = MDP_STATUS_NO_RESOURCES;
    }
    else
    {
        /*
        * Voting for Display NPA node to be ON
        */
        npa_issue_required_request(pPowerParams->sNPAClient[eDisplayId], PMIC_NPA_MODE_ID_GENERIC_ACTIVE);

        MDP_OSAL_DELAYUS(2000);   /* delay 2ms to allow power grid to settle */

        if (EFI_SUCCESS != TLMMProtocol->ConfigGpio((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_PULL_UP, GPIO_16MA), TLMM_GPIO_ENABLE))
        {
            DEBUG((EFI_D_WARN, "DisplayDxe: Configure MSM GPIO 8 for reset line Failed!\n"));
        }

        /* Set Reset_N line HIGH */
        if (EFI_SUCCESS != TLMMProtocol->GpioOut((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA), GPIO_HIGH_VALUE))
        {
            DEBUG((EFI_D_WARN, "DisplayDxe: Reset_N line HIGH failed!\n"));
        }

        /* GPIO10 = Display TE pin */
        if (EFI_SUCCESS != TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_NO_PULL, GPIO_2MA), TLMM_GPIO_ENABLE))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config GPIO 10\n"));
        }
    }

  return Status;
}  


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_CDP_PowerDown()
** 
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
static MDP_Status Panel_CDP_PowerDown(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams)
{
  MDP_Status                    Status           = MDP_STATUS_OK;
  EFI_TLMM_PROTOCOL            *TLMMProtocol     = NULL;    
  EFI_QCOM_PMIC_WLED_PROTOCOL  *PmicWledProtocol = NULL;
  EFI_QCOM_PMIC_IBB_PROTOCOL   *PmicIBBProtocol  = NULL;
  EFI_QCOM_PMIC_LAB_PROTOCOL   *PmicLABProtocol  = NULL;
  EFI_QCOM_PMIC_MPP_PROTOCOL   *PmicMppProtocol  = NULL;

  if (NULL == pPowerParams->sNPAClient[eDisplayId])
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: NULL Handle for Primary display NPA node.\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else if (EFI_SUCCESS != gBS->LocateProtocol( &gEfiTLMMProtocolGuid, NULL, (void**)&TLMMProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate TLMM protocol failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  } 
  else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicWledProtocolGuid, NULL, (VOID**) &PmicWledProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate WLED Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicMppProtocolGuid, NULL,(VOID**) &PmicMppProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate MPP Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicIbbProtocolGuid, NULL, (VOID**) &PmicIBBProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate PMIC IBB Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicLabProtocolGuid, NULL, (VOID**) &PmicLABProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate PMIC LAB Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else
  {
    /* 
     * Power Switches 
     */      
  
    if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_1, EFI_PM_MPP__DLOGIC__LVL_VIO_2, EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW )) 
    {
      DEBUG ((EFI_D_ERROR, "DisplayDxe: Error enabling PMI MPP1\n"));
    }

    /* 
     * GPIOs & VRegs
     */      
     
      /* GPIO10 = Display TE pin */
      if (EFI_SUCCESS != TLMMProtocol->ConfigGpio(EFI_GPIO_CFG(10, 1, GPIO_INPUT, GPIO_PULL_DOWN, GPIO_2MA), TLMM_GPIO_DISABLE))
      {
          DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config gpio 10\n"));
      }

      /*
      * Voltage Regulators
      */
      npa_complete_request(pPowerParams->sNPAClient[eDisplayId]);           //Complete the request to power rails
  }

  return Status;
}  


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_CDP_Reset()
** 
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
MDP_Status Panel_CDP_Reset(void)
{
  MDP_Status                    Status           = MDP_STATUS_OK;
  EFI_TLMM_PROTOCOL            *TLMMProtocol     = NULL;    
   
  if (EFI_SUCCESS != gBS->LocateProtocol( &gEfiTLMMProtocolGuid, NULL, (void**)&TLMMProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate TLMM protocol failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else
  {
    /* 
     * Toggle Reset
     */
     
    // GPIO 8 - Display Reset
    if (EFI_SUCCESS != TLMMProtocol->ConfigGpio((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_PULL_UP, GPIO_16MA), TLMM_GPIO_ENABLE))
    {
      DEBUG ((EFI_D_WARN, "DisplayDxe: Configure MSM GPIO 8 for reset line Failed!\n"));
    }
 
    /* Set Reset_N line HIGH */                          
    if(EFI_SUCCESS != TLMMProtocol->GpioOut((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA), GPIO_HIGH_VALUE))
    {
      DEBUG ((EFI_D_WARN, "DisplayDxe: Reset_N line HIGH failed!\n"));
    }
    MDP_OSAL_DELAYUS(10);

    /* Pull Reset_N Low */
    if (EFI_SUCCESS != TLMMProtocol->GpioOut((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA), GPIO_LOW_VALUE))
    {
      DEBUG ((EFI_D_WARN, "DisplayDxe: Pull Reset_N Low Failed!\n"));
    }
 
    // Wait 10us, Reset_N(RESX) Need to be at least 10us to really trigger the (as per the panel spec) 
    MDP_OSAL_DELAYUS(10);
 
    /* Set Reset_N line HIGH */                          
    if(EFI_SUCCESS != TLMMProtocol->GpioOut((UINT32)EFI_GPIO_CFG(8, 0, GPIO_OUTPUT, GPIO_NO_PULL, GPIO_16MA), GPIO_HIGH_VALUE))
    {
      DEBUG ((EFI_D_WARN, "DisplayDxe: Reset_N line HIGH failed!\n"));
    }

    // Wait 120ms, sleep out command should be sent only after 120ms delay after RESX is released (as per the panel spec)
    MDP_OSAL_DELAYUS(120000);
    
  }

  return Status;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_SetWLEDTest()
** 
** DESCRIPTION:
**        Set WLED TEST config 
**
*//* -------------------------------------------------------------------- */
static void Panel_SetWLEDTest(EFI_QCOM_PMIC_WLED_PROTOCOL  *PmicWledProtocol, bool32 bOLED)
{
  EFI_PM_WLED_SHORT_CKT_CONFIG_TYPE sWledShortCktCfg;
  //Enable short protection with discharge resistor
  MDP_OSAL_MEMZERO(&sWledShortCktCfg,    sizeof(EFI_PM_WLED_SHORT_CKT_CONFIG_TYPE));
  sWledShortCktCfg.ShortCktAlertToPbs    = FALSE;
  sWledShortCktCfg.DbncTimeShortCkt      = EFI_PM_WLED_DBNC_SHORT_TIME_4_BY_FSW;
  sWledShortCktCfg.EnDischargeResistance = TRUE;
  sWledShortCktCfg.EnShortCkt            = TRUE;

  if (EFI_SUCCESS != PmicWledProtocol->ConfigShortCktProtect (PMIC_PMI_DEV_INDEX, &sWledShortCktCfg))
  {
    DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShortCktProtect failed\n"));
  }

  if (bOLED)
  {
    //Set TEST3, To avoid OVP interrupt flag, wait for 100us to enable OVP after module enable.
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_ERRAMP_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_ERRAMP_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_ILIM_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_ILIM_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_VREF_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_VREF_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_CS_REF_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_CS_REF_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_SLOPE_COMP_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_SLOPE_COMP_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_OVP_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_T_ERRAMP_OUT, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_RDSON_TM, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed\n"));
    }

    // Set TEST4. In LCD, S/H enabled for preventing OVP. In AMOLED, to prevent in-rush.
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_ENB_IIND_UP, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_SLEEP_CLK_REQUEST, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_DEBOUNCE_BYPASS, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_CLAMP, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_VREF_UP, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed\n"));
    }
  }
  else
  {
    //Set TEST3, 
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_ERRAMP_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_ERRAMP_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_ILIM_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_ILIM_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_VREF_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_VREF_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_CS_REF_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_CS_REF_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_SLOPE_COMP_EN_MASK, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_SLOPE_COMP_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_OVP_EN_MASK, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_OVP_EN_MASK\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_T_ERRAMP_OUT, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_T_ERRAMP_OUT\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigWledMask (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_MASK_RDSON_TM, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigWledMask failed EFI_PM_WLED_MASK_RDSON_TM\n"));
    }

    // Set TEST4. In LCD, S/H enabled for preventing OVP. In AMOLED, to prevent in-rush.
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_ENB_IIND_UP, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed EFI_PM_WLED_SH_CONFIG_ENB_IIND_UP\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_SLEEP_CLK_REQUEST, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed EFI_PM_WLED_SH_CONFIG_EN_SLEEP_CLK_REQUEST\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_DEBOUNCE_BYPASS, FALSE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed EFI_PM_WLED_SH_CONFIG_DEBOUNCE_BYPASS\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_CLAMP, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed EFI_PM_WLED_SH_CONFIG_EN_CLAMP\n"));
    }
    if (EFI_SUCCESS != PmicWledProtocol->ConfigShSoftStart (PMIC_PMI_DEV_INDEX, EFI_PM_WLED_SH_CONFIG_EN_VREF_UP, TRUE))
    {
      DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigShSoftStart failed EFI_PM_WLED_SH_CONFIG_EN_VREF_UP\n"));
    }
  }
}
/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_CDP_PeripheralPower()
** 
** DESCRIPTION:
**        Secondary power sequence for other PMIC modules such as IBB/LAB
**
*//* -------------------------------------------------------------------- */
static MDP_Status Panel_CDP_PeripheralPower(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams, bool32 bPowerUp)
{
  MDP_Status                    Status = MDP_STATUS_OK;


  if ((MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_LCD  != pPowerParams->ePMICSecondaryPower[eDisplayId]) &&
      (MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_OLED != pPowerParams->ePMICSecondaryPower[eDisplayId]))
  {
     // Nothing to do, just return
  }
  else
  {
        EFI_QCOM_PMIC_WLED_PROTOCOL  *PmicWledProtocol = NULL;
        EFI_QCOM_PMIC_IBB_PROTOCOL   *PmicIBBProtocol  = NULL;
        EFI_QCOM_PMIC_LAB_PROTOCOL   *PmicLABProtocol  = NULL;
        EFI_QCOM_PMIC_MPP_PROTOCOL   *PmicMppProtocol  = NULL;
        EFI_QCOM_PMIC_LPG_PROTOCOL   *PmicLpgProtocol  = NULL;

        if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicWledProtocolGuid, NULL, (VOID**)&PmicWledProtocol))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: Locate WLED Protocol Failed!\n"));
            Status = MDP_STATUS_NO_RESOURCES;
        }
        else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicMppProtocolGuid, NULL, (VOID**)&PmicMppProtocol))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: Locate MPP Protocol Failed!\n"));
            Status = MDP_STATUS_NO_RESOURCES;
        }
        else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicIbbProtocolGuid, NULL, (VOID**)&PmicIBBProtocol))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: Locate PMIC IBB Protocol Failed!\n"));
            Status = MDP_STATUS_NO_RESOURCES;
        }
        else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicLabProtocolGuid, NULL, (VOID**)&PmicLABProtocol))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: Locate PMIC LAB Protocol Failed!\n"));
            Status = MDP_STATUS_NO_RESOURCES;
        }
        else if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicLpgProtocolGuid, NULL, (VOID **)&PmicLpgProtocol))
        {
            DEBUG((EFI_D_ERROR, "DisplayDxe: LocateProtocol(LPG) FAILED\n"));
            Status = MDP_STATUS_NO_RESOURCES;
        }
        else if (TRUE == bPowerUp)
        {
            // ********** Brightness Power-up Sequence **********

            // Configure the power grid based on the module type
            switch (pPowerParams->ePMICSecondaryPower[eDisplayId])
            {
            case MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_LCD:
                // Power up

                /************************************************************************/
                /* Turn On IBB(+5.5v) first, wait for 8ms, turn on LAB(-5.5v)           */
                /************************************************************************/
                //Enable LCD mode
                if (EFI_SUCCESS != PmicIBBProtocol->LcdAmoledSel(PMIC_PMI_DEV_INDEX, FALSE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config IBB to LCD mode\n"));
                }
                // Soft start charging register = 320Kohm
                if (EFI_SUCCESS != PmicIBBProtocol->ChgrResistor(PMIC_PMI_DEV_INDEX, 320))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to set start charging registe with ChgrResistor\n"));
                }
                // Finish VDISP Config
                if (EFI_SUCCESS != PmicIBBProtocol->ModuleRdy(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config IBB to Ready\n"));
                }
                //Turn on IBB
                if (EFI_SUCCESS != PmicIBBProtocol->ConfigIbbCtrl(PMIC_PMI_DEV_INDEX, TRUE, FALSE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to enable IBB\n"));
                }

                MDP_OSAL_DELAYUS(8000);   /* delay 8ms */

                //Change default Lab Current Sense setting, ISense_Tap = 1x, ISense_Gain = 1.5x
                if (EFI_SUCCESS != PmicLABProtocol->ConfigCurrentSense(PMIC_PMI_DEV_INDEX, EFI_PM_LAB_ISENSE_1P0, EFI_PM_LAB_ISENSE_1P5))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to config LAB Current Sense\n"));
                }
				//Change default Lab Pulse skip setting, Enabled, 40mA threshold.
                if (EFI_SUCCESS != PmicLABProtocol->ConfigPulseSkipCtrl(PMIC_PMI_DEV_INDEX, 40, 0, 0, TRUE))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to config LAB Pulse skip setting\n"));
                }

                //Enable LCD mode
                if (EFI_SUCCESS != PmicLABProtocol->LcdAmoledSel(PMIC_PMI_DEV_INDEX, FALSE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config LAB to LCD mode\n"));
                }
                // Finish VDISN Config
                if (EFI_SUCCESS != PmicLABProtocol->ModuleRdy(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config LAB to Ready\n"));
                }
                //Turn on LAB for VDISN
                if (EFI_SUCCESS != PmicLABProtocol->IbbRdyEn(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to enable LAB\n"));
                }

                MDP_OSAL_DELAYUS(40000);   /* delay 40ms for IBB module*/

                // PMI8994 MPP04--> EXT_FET_WLED_PWR_EN  
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_0, EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW )) 
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error toggling PMI MPP4 low\n"));
                }

                /************************************************************************/
                /* Backlight control for 720P smart panel using LPG4-->DTEST1-->MPP4    */
                /************************************************************************/
                /*set dtest out*/
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDetstOut(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_1, EFI_PM_MPP__DLOGIC__LVL_VIO_2, EFI_PM_MPP__DLOGIC_OUT__DBUS1))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: failed to route DBUS1 to MPP1 \n"));
                }
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_1, EFI_PM_MPP__DLOGIC__LVL_VIO_2, EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: failed to set MPP1 to low to allow control over DBUS1\n"));
                }
                /*set LPG dtest*/
                if (EFI_SUCCESS != PmicLpgProtocol->SetLpgDtest(PMIC_PMI_DEV_INDEX, EFI_PM_LPG_CHAN_4, EFI_PM_LPG_CHAN_DTEST_1, EFI_PM_LPG_CHAN_LPG_OUT_1))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: failed to route LPG4 to DTEST1(DBUS1)\n"));
                }
                if (EFI_SUCCESS != PmicLpgProtocol->LpgConfig(PMIC_PMI_DEV_INDEX,
                                                              EFI_PM_LPG_CHAN_4,
                                                              0x0080,                          // PWM Value (Used to calculate Duty Cycle) 
                                                              EFI_PM_LPG_PWM_PRE_DIV_1,        // Pre-divider setting 
                                                              EFI_PM_LPG_PWM_FREQ_EXPO_1,      // Pre-divider exponent 
                                                              EFI_PM_LPG_PWM_CLOCK_19_2_MHZ,
                                                              EFI_PM_LPG_PWM_9BIT))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: failed to enable LPG4 as PWM output\n"));
                }

                /************************************************************************
                 *  Enable secondary power source
                 ************************************************************************/

                //0xD84A             MODULATION   0x0C      1.2 MHz modulator clock (default)
                if (EFI_SUCCESS != PmicWledProtocol->SetModClkCtrl(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_MOD_CLK_1P2MHz))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetModClkCtrl failed\n"));
                }

                //0xD84D             WLED_OVP         0x11       For this panel, 17.8V OVP is sufficient
                if (EFI_SUCCESS != PmicWledProtocol->SetOvpThreshold(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_OVP_THR_17P8V))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetOvpThreshold failed\n"));
                }

                //0xD84E             WLED_ILIM         0x02       525 mA current limit setting for fsw = 1.6 MHz
                if (EFI_SUCCESS != PmicWledProtocol->SetBoostIlimit(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_BST_ILIMIT_525mA))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetBoostIlimit failed\n"));
                }

                //0xD84F             EN_CURRENT_SINK         0xE0       Enable all 3 strings
                if (EFI_SUCCESS != PmicWledProtocol->EnableCurrentSink(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableCurrentSink failed\n"));
                }

                //0xD854             SLEW_RATE_CTRL            0x02       At default
                if (EFI_SUCCESS != PmicWledProtocol->SelectSlewRate(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_SLEW_RATE_10nS))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SelectSlewRate failed\n"));
                }

                //0xD84C             SWITCHING_FREQUENCY       0x11      Set boost switching frequency to 800KHz.
                if (EFI_SUCCESS != PmicWledProtocol->SetFswCtrl(PMIC_PMI_DEV_INDEX, 0, 800))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetFswCtrl failed\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->EnableModulator(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableModulator failed\n"));
                }

                // 12bit mode
                if (EFI_SUCCESS != PmicWledProtocol->SelectPwmMode(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_MODE_12b_SDM))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SelectPwmMode failed\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->EnableSync(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableSync failed\n"));
                }

                Panel_SetWLEDTest(PmicWledProtocol, FALSE);

                //0xD846             MODULE_EN      0x80       Enable the entire module
                if (EFI_SUCCESS != PmicWledProtocol->EnableWled(PMIC_PMI_DEV_INDEX, 0, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableWled failed\n"));
                }

                break;


            case MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_OLED:
                // Power up

                /************************************************************************/
                /* Turn On IBB(+5.5v) first, wait for 8ms, turn on LAB(-5.5v)           */
                /************************************************************************/
                //Enable AMOLED mode
                if (EFI_SUCCESS != PmicIBBProtocol->LcdAmoledSel(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config IBB to LCD mode\n"));
                }
                // Soft start charging register = 300Kohm
                if (EFI_SUCCESS != PmicIBBProtocol->ChgrResistor(PMIC_PMI_DEV_INDEX, 300))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to set start charging registe with ChgrResistor\n"));
                }
                // Finish VDISP Config
                if (EFI_SUCCESS != PmicIBBProtocol->ModuleRdy(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config IBB to Ready\n"));
                }
                //Turn on IBB
                if (EFI_SUCCESS != PmicIBBProtocol->ConfigIbbCtrl(PMIC_PMI_DEV_INDEX, FALSE, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to enable IBB\n"));
                }

                //Change default Lab Current Sense setting, ISense_Tap = 1x, ISense_Gain = 1.5x
                if (EFI_SUCCESS != PmicLABProtocol->ConfigCurrentSense(PMIC_PMI_DEV_INDEX, EFI_PM_LAB_ISENSE_1P0, EFI_PM_LAB_ISENSE_1P5))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to config LAB Current Sense\n"));
                }
                //Change default Lab Pulse skip setting, Enabled, 20mA threshold.
                if (EFI_SUCCESS != PmicLABProtocol->ConfigPulseSkipCtrl(PMIC_PMI_DEV_INDEX, 20, 0, 0, TRUE))
                {
                    DEBUG ((EFI_D_ERROR, "DisplayDxe: Error to config LAB Pulse skip setting\n"));
                }
			
                //Enable AMOLED mode
                if (EFI_SUCCESS != PmicLABProtocol->LcdAmoledSel(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config LAB to LCD mode\n"));
                }
                // Finish VDISN Config
                if (EFI_SUCCESS != PmicLABProtocol->ModuleRdy(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to config LAB to Ready\n"));
                }
                //Turn on LAB for VDISN
                if (EFI_SUCCESS != PmicLABProtocol->IbbRdyEn(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to enable LAB\n"));
                }

                /*
                * Enable WLED in AMOLED mode
                */
                if (EFI_SUCCESS != PmicWledProtocol->EnAmoled(PMIC_PMI_DEV_INDEX, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnAmoled failed\n"));
                }

                /*config PSM with default values and then enable PSM*/
                if (EFI_SUCCESS != PmicWledProtocol->ConfigPsm(PMIC_PMI_DEV_INDEX, 0, 500, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol ConfigPsm failed\n"));
                }
				
                /*config Ramp delay time */
                if (EFI_SUCCESS != PmicWledProtocol->SetRampTime(PMIC_PMI_DEV_INDEX, 0, 0))
                {
                    DEBUG ((EFI_D_ERROR, "Wled Protocol ConfigPsm failed\n"));
                }

                /*Gm = 1/24kohm for AMOLED. */
                if (EFI_SUCCESS != PmicWledProtocol->SelectGm(PMIC_PMI_DEV_INDEX, 1))
                {
                    DEBUG ((EFI_D_ERROR, "Wled Protocol SelectGm failed\n"));
                }

                /*Selects the compensation resistor in kilohms */
                if (EFI_SUCCESS != PmicWledProtocol->SelectRz(PMIC_PMI_DEV_INDEX, 320))
                {
                    DEBUG ((EFI_D_ERROR, "Wled Protocol SelectRz failed\n"));
                }
 
                //0xD84E             WLED_ILIM         0x02       525 mA current limit setting for fsw = 1.6 MHz
                if (EFI_SUCCESS != PmicWledProtocol->SetBoostIlimit(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_BST_ILIMIT_980mA))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetBoostIlimit failed\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->SetBoostMaxDuty(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_BST_MAX_DUTY_SUB_104))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetBoostMaxDuty failed\n"));
                }

                //0xD84F             EN_CURRENT_SINK         0xE0       Enable all 3 strings
                if (EFI_SUCCESS != PmicWledProtocol->EnableCurrentSink(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableCurrentSink failed\n"));
                }

                //0xD854             SLEW_RATE_CTRL            0x02       At default
                if (EFI_SUCCESS != PmicWledProtocol->SelectSlewRate(PMIC_PMI_DEV_INDEX, 0, EFI_PM_WLED_SLEW_RATE_10nS))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SelectSlewRate failed\n"));
                }

                Panel_SetWLEDTest(PmicWledProtocol, TRUE);

                //DTEST2 = EXT_FET control. 
                if (EFI_SUCCESS != PmicWledProtocol->SetWledDtest (PMIC_PMI_DEV_INDEX, 9))
                {
                    DEBUG ((EFI_D_ERROR, "Wled Protocol SetWledDtest failed\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->EnableModulator(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableModulator failed\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->EnableSync(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL, TRUE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableSync failed\n"));
                }

                // PMI8994 MPP04--> EXT_FET_WLED_PWR_EN  
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_0, EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW )) 
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error toggling MPP4 low\n"));
                }
                           
                //Route DTEST2 to PMI8994 MPP4
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDetstOut(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_0, EFI_PM_MPP__DLOGIC_OUT__DBUS2 )) 
                {
                  DEBUG ((EFI_D_ERROR, "DisplayDxe: Error ConfigDetstOut failed to route DTEST2 to PMI8994 MPP4\n"));
                }

                break;
            default:
                break;
            }
        }
        else
        {
            // ********** Brightness Power-down Sequence **********

            // Configure the power grid based on the module type
            switch (pPowerParams->ePMICSecondaryPower[eDisplayId])
            {
            case MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_LCD:
            case MDP_PMIC_MODULE_CONTROLTYPE_IBB_LAB_OLED:
                // Power down
                if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_PMI_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_0, EFI_PM_MPP__DLOGIC_OUT__CTRL_HIGH )) 
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error disabling toggling MPP4 high\n"));
                }

                if (EFI_SUCCESS != PmicIBBProtocol->ConfigIbbCtrl(PMIC_PMI_DEV_INDEX, FALSE, FALSE))
                {
                    DEBUG((EFI_D_ERROR, "DisplayDxe: Error to enable IBB\n"));
                }

                if (EFI_SUCCESS != PmicWledProtocol->EnableWled(PMIC_PMI_DEV_INDEX, 0, FALSE))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol EnableWled failed\n"));
                }

                break;
            default:
                break;
            }

        }
    }

    return Status;
}


/* ---------------------------------------------------------------------- */
/**
** FUNCTION: Panel_CDP_BacklightLevel()
**
** DESCRIPTION:
**
*//* -------------------------------------------------------------------- */
static MDP_Status Panel_CDP_BacklightLevel(MDP_Display_IDType eDisplayId, BacklightConfigType *pBacklightConfig)
{
    MDP_Status                    Status = MDP_STATUS_OK;

    if (MDP_PANEL_BACKLIGHTTYPE_PMIC == pBacklightConfig->eBacklightType)
    {
        // Configure the power grid based on the module type
        switch (pBacklightConfig->uBacklightCntrl.eBacklightCtrl)
        {
        case MDP_PMIC_BACKLIGHT_CONTROLTYPE_WLED:
        {
            EFI_QCOM_PMIC_WLED_PROTOCOL  *PmicWledProtocol = NULL;

            if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicWledProtocolGuid, NULL, (VOID**)&PmicWledProtocol))
            {
                DEBUG((EFI_D_ERROR, "DisplayDxe: Locate WLED Protocol Failed!\n"));
                Status = MDP_STATUS_NO_RESOURCES;
            }
            else
            {
                uint32 uWLEDValue = (0xFFF * pBacklightConfig->uLevel) / 100; // Calculate duty cycle based on 12 bit mode

                if (EFI_SUCCESS != PmicWledProtocol->SetLedDutyCycle(PMIC_PMI_DEV_INDEX, EFI_PM_WLED_ALL, uWLEDValue))
                {
                    DEBUG((EFI_D_ERROR, "Wled Protocol SetLedDutyCycle failed\n"));
                }
            }
        }
        break;

        case MDP_PMIC_BACKLIGHT_CONTROLTYPE_LPG:
        {
            EFI_QCOM_PMIC_LPG_PROTOCOL   *PmicLpgProtocol = NULL;

            if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicLpgProtocolGuid, NULL, (VOID **)&PmicLpgProtocol))
            {
                DEBUG((EFI_D_ERROR, "DisplayDxe: Locate LPG Protocol Failed!\n"));
                Status = MDP_STATUS_NO_RESOURCES;
            }
            else
            {
                // Calculate backlight based on 9bit mode
                uint32 uLPGValue = (0x1FF * pBacklightConfig->uLevel) / 100;

                if (EFI_SUCCESS != PmicLpgProtocol->LpgSetPWMValue(PMIC_PMI_DEV_INDEX, EFI_PM_LPG_CHAN_4, uLPGValue))
                {
                    DEBUG((EFI_D_ERROR, "LPG Protocol LpgSetPWMValue failed\n"));
                }
            }
        }
        break;

        default:
            break;
        }
    }
    else
    {
        // Nothing to do for other configurations
    }

    return Status;
}



/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HDMI_CDP_Powerup()
** 
** DESCRIPTION:
**  
**
*//* -------------------------------------------------------------------- */
static MDP_Status HDMI_CDP_PowerUp(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams)
{
  MDP_Status                    Status            = MDP_STATUS_OK;
  EFI_QCOM_PMIC_MPP_PROTOCOL   *PmicMppProtocol   = NULL;


  if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicMppProtocolGuid, NULL,(VOID**) &PmicMppProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate MPP Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else 
  {
    if (NULL == pPowerParams->sNPAClient[eDisplayId])
    {
      DEBUG ((EFI_D_ERROR, "DisplayDxe: NULL Handle for HDMI NPA node.\n"));
      Status = MDP_STATUS_NO_RESOURCES;
    }
    else
    {
      npa_issue_required_request(pPowerParams->sNPAClient[eDisplayId], PMIC_NPA_MODE_ID_GENERIC_ACTIVE );
    }

    // PM8994 MPP04--> HDMI_EN
    if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_2, EFI_PM_MPP__DLOGIC_OUT__CTRL_HIGH))
    {
        DEBUG((EFI_D_ERROR, "DisplayDxe: Error toggling MPP4 high\n"));
    }
  }

  return Status;
}



/* ---------------------------------------------------------------------- */
/**
** FUNCTION: HDMI_CDP_PowerDown()
** 
** DESCRIPTION:
** 
**
*//* -------------------------------------------------------------------- */
static MDP_Status HDMI_CDP_PowerDown(MDP_Display_IDType eDisplayId, Panel_PowerCtrlParams *pPowerParams)
{
  MDP_Status                    Status            = MDP_STATUS_OK;
  EFI_QCOM_PMIC_MPP_PROTOCOL   *PmicMppProtocol   = NULL;

  if (EFI_SUCCESS != gBS->LocateProtocol(&gQcomPmicMppProtocolGuid, NULL,(VOID**) &PmicMppProtocol))
  {
    DEBUG ((EFI_D_ERROR, "DisplayDxe: Locate MPP Protocol Failed!\n"));
    Status = MDP_STATUS_NO_RESOURCES;
  }
  else 
  {
    // PM8994 MPP04--> HDMI_EN
    if (EFI_SUCCESS != PmicMppProtocol->ConfigDigitalOutput(PMIC_DEV_INDEX, EFI_PM_MPP_4, EFI_PM_MPP__DLOGIC__LVL_VIO_2, EFI_PM_MPP__DLOGIC_OUT__CTRL_LOW )) 
    {
      DEBUG((EFI_D_ERROR, "DisplayDxe: Error toggling MPP4 low\n"));
    }

    if (NULL == pPowerParams->sNPAClient[eDisplayId])
    {
      DEBUG ((EFI_D_ERROR, "DisplayDxe: NULL Handle for HDMI NPA node.\n"));
      Status = MDP_STATUS_NO_RESOURCES;
    }
    else
    {
      npa_complete_request(pPowerParams->sNPAClient[eDisplayId]);           //Complete the request to power rails
    }

  }

  return Status;
}

/* ---------------------------------------------------------------------- */
/**
** FUNCTION: DynamicDSIPanelDetection()
** 
** DESCRIPTION:
**  Detect DSI panels by doing a DSI read specific to each panels.
**  This function could be used as sample for OEM to detect DSI panels, 
**  it is not a complete implementation of all possible combinations of read
**  commands that could be needed for this detection.
**   
**  Return success only if a DSI panel was correctly detected and the information 
**  is updated in pPlatformParams->sPlatformPanel
*//* -------------------------------------------------------------------- */
static MDP_Status DynamicDSIPanelDetection(MDPPlatformParams *pPlatformParams, uint32 *puPanelID)
{
  MDP_Status             Status       = MDP_STATUS_FAILED;
  bool32                 bDumpPanelId = FALSE;
  
  PlatformDSIDetectParams panelDetectionList[] = {
    //Sharp_LS062R1SX01_DSI
    { 
      0x06,                                                  //  uCmdType  
      0x05,                                                  //  total number of retry on failures
      {
        {{0xDA, 0x00},                                       //  address 
        {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}     //  expectedReadback
        },
        {{0x04, 0x00},
        {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
        },
        {{0xDC, 0x00},
        {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
        }
      },                          
      0,                                                     // Lane remap order {0, 1, 2, 3}
      Sharp_LS062R1SX01_DSI_xmldata,                         // psPanelCfg                
      sizeof(Sharp_LS062R1SX01_DSI_xmldata),                 // uPanelCfgSize
      0                                                      // uFlags
    },

    //The IDs corresponding to 0xDA and 0xDC have not been filled in these panels.
    //We will use 0xF4, which is the address for unique Novatek ID code.
    {   
      0x06,                                                   // uCmdType
      0x05,                                                   // total number of retry on failures
      {
        {
          {0xF4, 0x00},                                      // address
          {0x90, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   // expected readback
        }, 
        {
          {0x00, 0x00},                                      
          {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   
        },
        {
          {0x00, 0x00},                                      
          {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   
        }
      },
      0,
      truly_cmd_DSI_xmldata,                                 // psPanelCfg (panel configuration)
      sizeof(truly_cmd_DSI_xmldata),                         // uPanelCfgSize
      0                                                      // uFlags
    },

    // LGE LH450WX2
    {  
      0x06,                                                  // uCmdType
      0x05,                                                  // total number of retry on failures
      {
        {{0xDA, 0x00},                                      // address to read ID1
        {0xC1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   // expected readback
        }, 
        {{0xDB, 0x00},                                      // address to read ID2
        {0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   // expected readback
        },
        {{0xDC, 0x00},                                      // address to read ID3
        {0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}   // expected readback
        }
      },
      0,                                                     // Lane remap order {0, 1, 2, 3}
      lge_LH450WX2_cmd_xmldata,                              // psPanelCfg (panel configuration)
      sizeof(lge_LH450WX2_cmd_xmldata),                      // uPanelCfgSize
      0                                                      // uFlags
    },

    // SS AMS555HB09
    {  
      0x06,                                                  // uCmdType
      0x05,                                                  // total number of retry on failures
      {
        {{0xFC, 0x00},                                      // address to read ID1
        {0xA5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        }, 
        {{0xDB, 0x00},                                      // address to read ID2
        {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        },
        {{0xDC, 0x00},                                      // address to read ID3
        {0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        }
      },
      0,                                                    // Lane remap order {0, 1, 2, 3}
      ss_AMS555HB09_xmldata,                                // psPanelCfg (panel configuration)
      sizeof(ss_AMS555HB09_xmldata),                        // uPanelCfgSize
      0                                                     // uFlags
    },

    // Sharp 1080p (Command Mode)
    {  
      0x06,                                                  // uCmdType
      0x05,                                                  // total number of retry on failures
      {
        {{0xF4, 0x00},                                      // address to read ID1
        {0x95, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        }, 
        {{0xF6, 0x00},                                      // address to read ID2
        {0x50, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        },
        {{0xDC, 0x33},                                      // address to read ID3
        {0xBC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}    // expected readback
        }
      },
      0,                                                    // Lane remap order {0, 1, 2, 3}
      sharp_cmd_DSI_xmldata,                                // psPanelCfg (panel configuration)
      sizeof(sharp_cmd_DSI_xmldata),                        // uPanelCfgSize
      0                                                     // uFlags
    },    
    
      
  };
  
  if (MDP_STATUS_OK == DSIDriver_MinimalInit())          // do minimal DSI init
  {
    uint8       panelID[PLATFORM_PANEL_ID_MAX_COMMANDS];
    uint32      uPanelIndex;
    bool32      bMatch         = FALSE;
    uint32      uPrevClkConfig = 0; 


    // go through all possible panels
    for (uPanelIndex = 0; uPanelIndex < (sizeof(panelDetectionList)/sizeof(PlatformDSIDetectParams)); uPanelIndex++)
    {
      uint8     readback[DSI_READ_READBACK_SIZE];
      uint32    readSize      = sizeof(readback);
      int       iCommandIndex = 0;
      uint32    uClkConfig    = (MDPPLATFORM_PANEL_DETECT_FLAG_CLOCK_FORCEHS & 
                                 panelDetectionList[uPanelIndex].uFlags);
	  
      // Check if there is any change in the clock config and set it accordingly
      if (uPrevClkConfig != uClkConfig)
      {
        if (MDP_STATUS_OK != DSIDriver_ConfigClockLane(uClkConfig))
        {
          DEBUG((EFI_D_ERROR, "Display: DSIDriver_ConfigClockLane failed\n"));
        }
        
        uPrevClkConfig = uClkConfig;
      }

      // Reprogram the DSI lane swap based on remap order
      if (MDP_STATUS_OK != DSIDriver_RemapLane(panelDetectionList[uPanelIndex].uLaneRemapOrder))
      {
        DEBUG((EFI_D_WARN, "Display: DSIDriver_RemapLane failed\n"));
      }

      // Allow debug option to scan panel registers (used to help generate a uniquie panel ID for detection)
      if (TRUE == bDumpPanelId)
      {
        DsiPanelDumpRegisters();
        // Dump just once
        bDumpPanelId = FALSE;
      }

      // clear the panel ID
      MDP_OSAL_MEMZERO(panelID, PLATFORM_PANEL_ID_MAX_COMMANDS);

      // for each possible panel ID read
      for(iCommandIndex = 0; iCommandIndex<PLATFORM_PANEL_ID_MAX_COMMANDS; iCommandIndex++)
      {
        uint32         uRetryCount = 0;

        // if read command is 0, then stop reading panel ID
        if ((0 == panelDetectionList[uPanelIndex].panelIdCommands[iCommandIndex].address[0]) &&
            (0 == panelDetectionList[uPanelIndex].panelIdCommands[iCommandIndex].address[1]) )
        {
          break;
        }
        // DSI read
        bMatch = FALSE;

        uRetryCount = 0;
        do
        {
          // clear the readback buffer
          MDP_OSAL_MEMZERO(&readback[0], readSize);
          readSize = sizeof(readback);
          Status = DSIDriver_Read(panelDetectionList[uPanelIndex].uCmdType, 
            panelDetectionList[uPanelIndex].panelIdCommands[iCommandIndex].address, 
            sizeof(panelDetectionList[uPanelIndex].panelIdCommands[iCommandIndex].address), 
            readback, &readSize);
          uRetryCount++;
        } while((uRetryCount < panelDetectionList[uPanelIndex].uTotalRetry) && ((MDP_STATUS_OK != Status) || (0 == readSize)));

        if ((uRetryCount <= panelDetectionList[uPanelIndex].uTotalRetry) &&
            (0 != readSize))
        {
          // Read was successful, now check the data is what we expect
          if (0 == CompareMem(readback, panelDetectionList[uPanelIndex].panelIdCommands[iCommandIndex].expectedReadback, readSize))
          {
            panelID[iCommandIndex] = readback[0];    // store the first byte of readback as panel ID
            bMatch     = TRUE;                       // mark one panel ID matched
          }
        }

        // if any panel ID is not matched, then go to detect next panel in the list
        if(FALSE == bMatch)
        {
          break;
        }
      }

      // if all panel IDs are matched for a specific panel, store settings and stop
      if(TRUE == bMatch)
      {
        // store matched panel configuration xml data
        pPlatformParams->sPlatformPanel.pPanelXMLConfig = (int8*) panelDetectionList[uPanelIndex].psPanelCfg;
        pPlatformParams->sPlatformPanel.uConfigSize     = panelDetectionList[uPanelIndex].uPanelCfgSize;

        // return the final combined panel ID
        *puPanelID = (panelID[0]<<16) | (panelID[1]<<8) | (panelID[2]);

        DEBUG((EFI_D_WARN, "Display: Detected Panel ID = 0x%08X\n", *puPanelID));

        Status = MDP_STATUS_OK;
        break;
      }
      else
      {
        Status = MDP_STATUS_FAILED;
      }
    }
  }

  // If anything fails, fallback to default panel ID which is generated by using default vendor and revision for this platform
  if(MDP_STATUS_OK != Status)
  {
      *puPanelID = (pPlatformParams->sPlatformPanel.uDefaultVendor<<16) | (pPlatformParams->sPlatformPanel.uDefaultRevision<<8);
       DEBUG((EFI_D_ERROR, "Display: Panel dynamic detection failed.\n"));      
  }

  DSIDriver_Close();

  return Status;
}


#ifdef __cplusplus
}
#endif
