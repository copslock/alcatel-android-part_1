/**********************************************************************
* ufs_bsp_8996.c
*
* Board support file for MSM8996
*
* Copyright (c) 2013-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
* 
*
**********************************************************************/

/*=======================================================================
                        Edit History

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/UfsTargetLib/ufs_bsp_boot_8996.c#7 $
$DateTime: 2015/12/28 19:52:42 $

YYYY-MM-DD   who     what, where, why
---------------------------------------------------------------------- 
2015-08-01   kpa     use dcache_flush_region instead of mmu_flush_cache
2015-06-15   rh      Disable the PHY from decoding the LCC sequence
2015-02-12   rh      8996 V1 PHY workaround
2014-09-17   rh      Adapted for 8996 
2014-07-16   rh      Adding cache operation
2014-06-02   rh      PHY init adjustment
2014-03-24   rh      Adapt the BSP file for 8994
2013-10-30   rh      Clock regime function cleanup
2013-10-30   rh      More accurate delay added
2013-06-19   rh      Initial creation
===========================================================================*/

#include <Uefi.h>
#include <HALhwio.h>
#include <Library/PcdLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/CacheMaintenanceLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/SynchronizationLib.h>
#include <Library/TimerLib.h>
#include <Protocol/EFIHWIO.h>

#include <Library/PcdLib.h>
#include <Library/ArmLib.h>

#include "ufs_osal.h"
#include "ufs_bsp.h"
#include "ufs_phy_hwio.h"
#include "hwio_clkgcc_v2.h"
#include "ClockBoot.h"
#include "boot_cache.h"

#include "smd_type.h"


#define CLK_CTL_BASE                         0x00300000
#define MPM2_MPM_BASE                        0x004A0000

/* Clock regime clock offset mapping */
#define HWIO_GCC_UFS_TX_SYMBOL_0_CBCR_OUT(v)       \
        out_dword(HWIO_GCC_UFS_TX_SYMBOL_0_CBCR_ADDR,v)
#define HWIO_GCC_UFS_TX_SYMBOL_1_CBCR_OUT(v)       \
        out_dword(HWIO_GCC_UFS_TX_SYMBOL_1_CBCR_ADDR,v)
#define HWIO_GCC_UFS_RX_SYMBOL_0_CBCR_OUT(v)       \
        out_dword(HWIO_GCC_UFS_RX_SYMBOL_0_CBCR_ADDR,v)
#define HWIO_GCC_UFS_RX_SYMBOL_1_CBCR_OUT(v)       \
        out_dword(HWIO_GCC_UFS_RX_SYMBOL_1_CBCR_ADDR,v)

#define HWIO_GCC_UFS_RX_CFG_CBCR_OUT(v)         \
        out_dword(HWIO_GCC_UFS_RX_CFG_CBCR_ADDR,v)
#define HWIO_GCC_UFS_TX_CFG_CBCR_OUT(v)         \
        out_dword(HWIO_GCC_UFS_TX_CFG_CBCR_ADDR,v)

#define HWIO_GCC_UFS_AXI_CMD_RCGR_OUT(v)        \
        out_dword(HWIO_GCC_UFS_AXI_CMD_RCGR_ADDR,v)
#define HWIO_GCC_UFS_AXI_CFG_RCGR_OUT(v)        \
        out_dword(HWIO_GCC_UFS_AXI_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SYS_NOC_UFS_AXI_CBCR_OUT(v)        \
        out_dword(HWIO_GCC_SYS_NOC_UFS_AXI_CBCR_ADDR,v)
#define HWIO_GCC_UFS_AXI_CBCR_OUT(v)        \
        out_dword(HWIO_GCC_UFS_AXI_CBCR_ADDR,v)
#define HWIO_GCC_UFS_AHB_CBCR_OUT(v)        \
        out_dword(HWIO_GCC_UFS_AHB_CBCR_ADDR,v)


#define RCGR_DIVBYPASS                      0
#define RCGR_DIV2                           3
#define RCGR_DIV3                           5
#define RCGR_DIV4                           7

#define RCGR_MODE_BYPASS                    0
#define RCGR_MODE_DUAL_EDGE                 2
#define RCGR_MODE_SINGLE_EDGE               3

#define UFS_PHY_PCS_READY_TIMEOUT           1000
#define REQUEST_LIST_BUFFER_SIZE            4096  
//__attribute__((section(".bss.BOOT_OCIMEM_PAGE_TABLE_ZONE"))) char ALIGN(4*1024) transfer_request_list_buf[REQUEST_LIST_BUFFER_SIZE];      
char transfer_request_list_buf[REQUEST_LIST_BUFFER_SIZE];      

void ufs_bsp_clk_set (uint8_t hostid, uint32_t mode)
{
   (void) hostid;
   if (mode == UFS_CLOCK_MODE_OFF) {
      (void) Clock_UFSInit (CLOCK_BOOT_PERF_NONE);
   }
   else if (mode == UFS_CLOCK_MODE_ON) {
      (void) Clock_UFSInit (CLOCK_BOOT_PERF_NOMINAL);
   }
}

void ufs_bsp_enable_symbol_clk (void)
{
}

/* A utility function to get the sleep timetick */
static uint32 Clock_GetSclkTimetick()
{
  uint32_t curr_count;
  uint32_t last_count;

  /*Grab current time count*/
  curr_count = HWIO_IN(MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL);

  /*Keep grabbing the time until a stable count is given*/
  do 
  {
    last_count = curr_count;
    curr_count = HWIO_IN(MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL);
  } while (curr_count != last_count);
  
  return curr_count;
}

// Maximum delay is 134s
void ufs_bsp_busy_wait(uint32_t us)
{
   uint32_t pause_cycle = us * 32 / 1000 + us * 768 / 1000000;
   uint32_t now;

   // Need to check if there is a case of wrapping around
   now = Clock_GetSclkTimetick();
   while( Clock_GetSclkTimetick() - now <= pause_cycle);
}

// Constant table for UFS-PHY initialization
static struct ufs_mphy_init_item ufs_bsp_mphy_init_table_v2[] = {
     { HWIO_UFS_UFS_PHY_POWER_DOWN_CONTROL_ADDR,                    0x01},
     { HWIO_UFS_QSERDES_COM_CMN_CONFIG_ADDR,                        0x0e},
     { HWIO_UFS_QSERDES_COM_SYSCLK_EN_SEL_ADDR,                     0x14},
     { HWIO_UFS_QSERDES_COM_CLK_SELECT_ADDR,                        0x30},
     { HWIO_UFS_QSERDES_COM_SYS_CLK_CTRL_ADDR,                      0x02},
     { HWIO_UFS_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,               0x08},
     { HWIO_UFS_QSERDES_COM_BG_TIMER_ADDR,                          0x0a},
     { HWIO_UFS_QSERDES_COM_HSCLK_SEL_ADDR,                         0x05},
     { HWIO_UFS_QSERDES_COM_CORECLK_DIV_ADDR,                       0x0a},
     { HWIO_UFS_QSERDES_COM_CORECLK_DIV_MODE1_ADDR,                 0x0a},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP_EN_ADDR,                       0x01},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_CTRL_ADDR,                     0x10},
     { HWIO_UFS_QSERDES_COM_RESETSM_CNTRL_ADDR,                     0x20},
     { HWIO_UFS_QSERDES_COM_CORE_CLK_EN_ADDR,                       0x00},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP_CFG_ADDR,                      0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_TIMER1_ADDR,                   0xff},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_TIMER2_ADDR,                   0x3f},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      0x14},       // Adjust for series A
     { HWIO_UFS_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                  0x05},
     { HWIO_UFS_QSERDES_COM_DEC_START_MODE0_ADDR,                   0x82},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_CP_CTRL_MODE0_ADDR,                     0x0b},
     { HWIO_UFS_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                   0x16},
     { HWIO_UFS_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,             0x80},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN1_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE1_MODE0_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE2_MODE0_ADDR,                   0x02},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                   0xff},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                   0x0c},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_COM_DEC_START_MODE1_ADDR,                   0x98},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START1_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START2_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START3_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_CP_CTRL_MODE1_ADDR,                     0x0b},
     { HWIO_UFS_QSERDES_COM_PLL_RCTRL_MODE1_ADDR,                   0x16},
     { HWIO_UFS_QSERDES_COM_PLL_CCTRL_MODE1_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN0_MODE1_ADDR,             0x80},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN1_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE1_MODE1_ADDR,                   0xd6},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE2_MODE1_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP1_MODE1_ADDR,                   0x32},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP2_MODE1_ADDR,                   0x0f},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP3_MODE1_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,   0x45},
     { HWIO_UFS_QSERDES_TX_LANE_MODE_ADDR,                          0x06},
     { HWIO_UFS_QSERDES_RX_SIGDET_LVL_ADDR,                         0x24},
     { HWIO_UFS_QSERDES_RX_SIGDET_CNTRL_ADDR,                       0x0F},
     { HWIO_UFS_QSERDES_RX_RX_INTERFACE_MODE_ADDR,                  0x40},
     { HWIO_UFS_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,              0x1E},
     { HWIO_UFS_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,              0x0B},
     { HWIO_UFS_QSERDES_RX_RX_TERM_BW_ADDR,                         0x5B},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN1_LSB_ADDR,                    0xFF},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN1_MSB_ADDR,                    0x3F},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN2_LSB_ADDR,                    0xFF},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN2_MSB_ADDR,                    0x3F},
     { HWIO_UFS_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,              0x0D},
     { HWIO_UFS_QSERDES_COM_PLL_IVCO_ADDR,                          0x0f},
     { HWIO_UFS_QSERDES_COM_BG_TRIM_ADDR,                           0x0f},
     { HWIO_UFS_UFS_PHY_RX_PWM_GEAR_BAND_ADDR,                      0x15},
     { HWIO_UFS_QSERDES_COM_RESCODE_DIV_NUM_ADDR,                   0x15},
     { HWIO_UFS_QSERDES_COM_CMN_MISC2_ADDR,                         0x1f},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_HALF_ADDR,              0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_QUARTER_ADDR,           0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_ADDR,                   0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SO_SATURATION_AND_ENABLE_ADDR,      0x4B},
     { HWIO_UFS_UFS_PHY_RX_SIGDET_CTRL2_ADDR,                       0x6c},
     { HWIO_UFS_UFS_PHY_TX_LARGE_AMP_DRV_LVL_ADDR,                  0x12},
     { HWIO_UFS_UFS_PHY_TX_SMALL_AMP_DRV_LVL_ADDR,                  0x06},
     { HWIO_UFS_UFS_PHY_LINECFG_DISABLE_ADDR,                       0x00},
     { HWIO_UFS_UFS_PHY_RX_SYM_RESYNC_CTRL_ADDR,                    0x3},
     {0,0}};

// Constant table for UFS-PHY initialization
static struct ufs_mphy_init_item ufs_bsp_mphy_init_table_v3[] = {
     { HWIO_UFS_UFS_PHY_POWER_DOWN_CONTROL_ADDR,                    0x01},
     { HWIO_UFS_QSERDES_COM_CMN_CONFIG_ADDR,                        0x0e},
     { HWIO_UFS_QSERDES_COM_SYSCLK_EN_SEL_ADDR,                     0x14},
     { HWIO_UFS_QSERDES_COM_CLK_SELECT_ADDR,                        0x30},
     { HWIO_UFS_QSERDES_COM_SYS_CLK_CTRL_ADDR,                      0x02},
     { HWIO_UFS_QSERDES_COM_BIAS_EN_CLKBUFLR_EN_ADDR,               0x08},
     { HWIO_UFS_QSERDES_COM_BG_TIMER_ADDR,                          0x0a},
     { HWIO_UFS_QSERDES_COM_HSCLK_SEL_ADDR,                         0x00},
     { HWIO_UFS_QSERDES_COM_CORECLK_DIV_ADDR,                       0x0a},
     { HWIO_UFS_QSERDES_COM_CORECLK_DIV_MODE1_ADDR,                 0x0a},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP_EN_ADDR,                       0x01},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_CTRL_ADDR,                     0x00},
     { HWIO_UFS_QSERDES_COM_RESETSM_CNTRL_ADDR,                     0x20},
     { HWIO_UFS_QSERDES_COM_CORE_CLK_EN_ADDR,                       0x00},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP_CFG_ADDR,                      0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_TIMER1_ADDR,                   0xff},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_TIMER2_ADDR,                   0x3f},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      0x04},       // Adjust for series A
     { HWIO_UFS_QSERDES_COM_SVS_MODE_CLK_SEL_ADDR,                  0x05},
     { HWIO_UFS_QSERDES_COM_DEC_START_MODE0_ADDR,                   0x82},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START1_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START2_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START3_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_CP_CTRL_MODE0_ADDR,                     0x0b},
     { HWIO_UFS_QSERDES_COM_PLL_RCTRL_MODE0_ADDR,                   0x16},
     { HWIO_UFS_QSERDES_COM_PLL_CCTRL_MODE0_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN0_MODE0_ADDR,             0x80},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN1_MODE0_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE1_MODE0_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE2_MODE0_ADDR,                   0x02},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP1_MODE0_ADDR,                   0xff},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP2_MODE0_ADDR,                   0x0c},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP3_MODE0_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_COM_DEC_START_MODE1_ADDR,                   0x98},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START1_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START2_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_DIV_FRAC_START3_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_CP_CTRL_MODE1_ADDR,                     0x0b},
     { HWIO_UFS_QSERDES_COM_PLL_RCTRL_MODE1_ADDR,                   0x16},
     { HWIO_UFS_QSERDES_COM_PLL_CCTRL_MODE1_ADDR,                   0x28},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN0_MODE1_ADDR,             0x80},
     { HWIO_UFS_QSERDES_COM_INTEGLOOP_GAIN1_MODE1_ADDR,             0x00},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE1_MODE1_ADDR,                   0xd6},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE2_MODE1_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP1_MODE1_ADDR,                   0x32},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP2_MODE1_ADDR,                   0x0f},
     { HWIO_UFS_QSERDES_COM_LOCK_CMP3_MODE1_ADDR,                   0x00},
     { HWIO_UFS_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_ADDR,   0x45},
     { HWIO_UFS_QSERDES_TX_LANE_MODE_ADDR,                          0x06},
     { HWIO_UFS_QSERDES_RX_SIGDET_LVL_ADDR,                         0x24},
     { HWIO_UFS_QSERDES_RX_SIGDET_CNTRL_ADDR,                       0x0F},
     { HWIO_UFS_QSERDES_RX_RX_INTERFACE_MODE_ADDR,                  0x40},
     { HWIO_UFS_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_ADDR,              0x1E},
     { HWIO_UFS_QSERDES_RX_UCDR_FASTLOCK_FO_GAIN_ADDR,              0x0B},
     { HWIO_UFS_QSERDES_RX_RX_TERM_BW_ADDR,                         0x5B},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN1_LSB_ADDR,                    0xFF},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN1_MSB_ADDR,                    0x3F},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN2_LSB_ADDR,                    0xFF},
     { HWIO_UFS_QSERDES_RX_RX_EQ_GAIN2_MSB_ADDR,                    0x3F},
     { HWIO_UFS_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_ADDR,              0x0D},
     { HWIO_UFS_QSERDES_COM_PLL_IVCO_ADDR,                          0x0f},
     { HWIO_UFS_QSERDES_COM_BG_TRIM_ADDR,                           0x0f},
     { HWIO_UFS_UFS_PHY_RX_PWM_GEAR_BAND_ADDR,                      0x15},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_HALF_ADDR,              0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_QUARTER_ADDR,           0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SVS_SO_GAIN_ADDR,                   0x04},
     { HWIO_UFS_QSERDES_RX_UCDR_SO_SATURATION_AND_ENABLE_ADDR,      0x4B},
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_INITVAL1_ADDR,                 0xFF}, 
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_INITVAL2_ADDR,                 0x00},
     { HWIO_UFS_UFS_PHY_RX_SIGDET_CTRL2_ADDR,                       0x6c},
     { HWIO_UFS_UFS_PHY_TX_LARGE_AMP_DRV_LVL_ADDR,                  0x0a},
     { HWIO_UFS_UFS_PHY_TX_SMALL_AMP_DRV_LVL_ADDR,                  0x02},
     { HWIO_UFS_UFS_PHY_LINECFG_DISABLE_ADDR,                       0x00},
     { HWIO_UFS_UFS_PHY_RX_SYM_RESYNC_CTRL_ADDR,                    0x3},
     {0,0}};

static struct ufs_mphy_init_item ufs_bsp_mphy_rate_b_init_table[] = {
     { HWIO_UFS_QSERDES_COM_VCO_TUNE_MAP_ADDR,                      0x54},
     {0,0}};

uint32_t ufs_bsp_get_mphy_init_item_table (struct ufs_mphy_init_item **itm)
{
   uint32_t f_val;
   f_val = in_dword (0x7a8000);
   if (f_val == 0x30000100) {
      // V1 no longer supported, use V2 PHY init code as baseline
      *itm = ufs_bsp_mphy_init_table_v2;
      return sizeof(ufs_bsp_mphy_init_table_v2)/sizeof(struct ufs_mphy_init_item);
   } if ((f_val & 0xffffff00) == 0x30000200) {
      *itm = ufs_bsp_mphy_init_table_v2;
      return sizeof(ufs_bsp_mphy_init_table_v2)/sizeof(struct ufs_mphy_init_item);
   }
   else {
      *itm = ufs_bsp_mphy_init_table_v3;
      return sizeof(ufs_bsp_mphy_init_table_v3)/sizeof(struct ufs_mphy_init_item);
   }
}

uint32_t ufs_bsp_get_mphy_init_rate_b_item_table (struct ufs_mphy_init_item **itm)
{
   *itm = ufs_bsp_mphy_rate_b_init_table;
   return sizeof(ufs_bsp_mphy_rate_b_init_table)/sizeof(struct ufs_mphy_init_item);
}

// Start the PHY and wait for the PHY to be ready
uint32_t ufs_bsp_mphy_start (uint8_t hostid)
{
   uintptr_t base_addr;
   int32_t tout = UFS_PHY_PCS_READY_TIMEOUT;

   base_addr = ufs_bsp_get_reg_baseaddress (hostid);

   HWIO_REG_OUTM (base_addr, UFS_UFS_PHY_PHY_START_ADDR, 1, 1);
   ufs_bsp_busy_wait (10);
   
   while (!(HWIO_REG_IN(base_addr, UFS_UFS_PHY_PCS_READY_STATUS_ADDR) & 0x01)) {
      if (tout-- == 0) {
         return FAIL;
      }
      ufs_bsp_busy_wait (1);
   }
   return SUCCESS;
}

void ufs_bsp_post_link_init (uint8_t hostid)
{
   uintptr_t base_addr;

   base_addr = ufs_bsp_get_reg_baseaddress (hostid);
   out_dword(HWIO_UFS_UFS_PHY_LINECFG_DISABLE_ADDR + base_addr, 0x02);   
}

void ufs_bsp_cache_op (void *addr, uint32_t len, uint32_t op) 
{
   (void) op;
   dcache_flush_region(addr, len);
}

void ufs_bsp_memory_barrier (void)
{
   ArmDataMemoryBarrier();
}

void *ufs_bsp_allocate_norm_buf (size_t size)
{
   (void) size;
   void *mem = transfer_request_list_buf;
   return mem;
}

void  ufs_bsp_free_norm_buf (void *pmem)
{
   (void) pmem;
}

void *ufs_bsp_allocate_xfer_buf (size_t size)
{
   (void) size;
   void *mem = transfer_request_list_buf;
   return mem;
}

void  ufs_bsp_free_xfer_buf (void *pmem)
{
   (void) pmem;
}

uintptr_t ufs_bsp_get_reg_baseaddress (uint8_t hostid)
{
   (void) hostid;
   return (uintptr_t) 0x620000;
}


