/* Copyright (C) 2016 Tcl Corporation Limited */
/****************************************************************************
*
*     Copyright (c) 2010 Broadcom Corporation
*           All Rights Reserved
*
*     No portions of this material may be reproduced in any form without the
*     written permission of:
*
*           Broadcom Corporation
*           16215 Alton Parkway
*           P.O. Box 57013
*           Irvine, California 92619-7013
*
*     All information contained in this document is Broadcom Corporation
*     company private, proprietary, and trade secret.
*
****************************************************************************/

#ifndef TYPEDEF_H

#define TYPEDEF_H

#include "com_dtypes.h"

/* #include "types.h"   */

//typedef unsigned char byte;
//typedef unsigned char uint8;
typedef unsigned char UInt8;
//typedef char int8;
//typedef unsigned short uint16;
//typedef short int16;
//typedef unsigned int uint32;
typedef unsigned int UInt32;
//typedef int int32;


#define  READ_REG32(reg)                  ( *((volatile UInt32 *) (reg)) )
#define  WRITE_REG32(reg, value)          ( *((volatile UInt32 *) (reg)) = (UInt32) (value) )
#define  MASK_REG32(reg, mask)          (WRITE_REG32((reg), (READ_REG32((reg)) & (~(mask)))))
#define  UNMASK_REG32(reg, unMask)        (WRITE_REG32((reg), (READ_REG32((reg)) | (unMask))))
#define  SET_BITS32                       UNMASK_REG32
#define  CLEAR_BITS32                   MASK_REG32


#endif
