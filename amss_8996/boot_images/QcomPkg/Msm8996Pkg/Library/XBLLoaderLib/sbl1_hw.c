
/*=============================================================================

                         SBL1 Hardware Initialization

GENERAL DESCRIPTION
  This file does basic hardware initialization at power up.

Copyright 2014-2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/

/*=============================================================================

                            EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who       what, where, why
--------   ---       ----------------------------------------------------------
11/23/15   kpa       Added boot_pm_app_config_download_mode api. 
11/10/15   kpa       Clear GCC_RESET_DEBUG_ENABLE.
09/25/15   kpa       Initialize pmic init in dload mode.
09/24/15   kpa       remove redundant api sbl1_wait_for_ddr_training
09/15/15   kpa       Put High Speed QUSB2 PHY in non-drive mode.
09/10/15   tw        updated logic to not call ddr set param when in 2nd pass sdi
08/31/15   dp        Update boot-log messages for ddr-init, ddr-training etc.
08/23/15   kpa       Update Pimem region attributes.
08/08/15   kpa       Updated sbl1_hw_pre_ddr_init() to support download mode with out loading pmic.elf
08/01/15   sng       DDRDebugImage Call Added
07/28/15   rp        Changes to support conditional loading of DDR training
07/26/15   kpa       Map Pimem memory region just before pimem init.
07/23/15   sds       Call boot_ICB_Config_PostInit() after training.
07/20/15   tw        enable one time training for ddr 
07/11/15   rp        Enable LED during DDR training
07/08/15   kpa       Clear GCC_RESET_FSM_CTRL's FIRST_PASS_COMPLETE.
06/30/15   aab       Placed pm_sbl_chg_init() call right after pm_driver_init()
06/22/15   kpa       Skip pmic init in dload mode.
06/10/15   rp        Added bl_shared_data function parameter for sbl1_ddr_init and sbl1_save_ddr_training_data
06/08/15   rp        Added support for one time training enablement for ddr
05/29/15   kpa       Support UIE, update 'reset_required' flag
05/31/15   aab       Replace pm_oem_init() with pm_sbl_chg_init()
05/19/15   kpa       Clear GCC_RESET_DEBUG_ENABLE.
04/23/15   plc       Added support for boot profiler, and calls to profile ddr
04/20/15   aks       Added support for Voltage Sensors
03/27/15   kpa       Added boot_pm_device_programmer_init
03/04/15   kpa       Reduce DDR freq to 200Mhz for Deviceprogrammer
03/04/15   kpa       Do not initialize pImem during dload mode boot, reset GCC_RESET_STATUS
02/20/15   kpa       Update boot logs for pmic driver
02/18/15   kpa       Rename sbl1_hw_pmic_init to sbl1_hw_pre_ddr_init. Add railway driver.
02/11/15   kpa       Enable Pimem.
02/03/15   wek       Skip ddr training for device programmer.
01/27/15   kpa       Added sbl1_ddr_set_default_params
01/27/15   jjo       Enable tsens init.
01/05/15   kpa       Temporarily stub out pimem and tsens init.
10/14/14   kpa       Move pm_device_init into sbl1_hw_pmic_init() for pmic
                     image loading support
05/07/14   kpa       Branch for 8996 and include HALbootHWIO.h instead of msmhwioreg.h
03/21/14   ck        Added logic to save reset_status_register to imem cookies before clearing
03/18/14   ck        Updated boot_hw_reset calls as they now take a reset type parameter
03/14/14   ck        Moved boot_busywait_init to sbl1_main_ctl as it needs to be done before boot logger init
03/03/14   ck        Removing SpmiInit and SpmiCfg from sbl1_hw_init per SPMI team as PMIC driver does this now
02/18/14   ck        Added logic to sbl1_hw_init to clear PMIC_ABNORMAL_RESIN_STATUS
10/17/13   ck        Temp zeroing of ddr training memory location until ddr team delivers
08/07/13   sr        Removed boot_ddr_debug call .
07/26/13   rj        Calling i2C init to fix eeprom init issue
07/11/13   rj        Calling boot_pre_ddr_clock_init after CDT configuration is
                     done to dynamically detect ddr and setup the clock plan
06/18/13   sl        Call boot_ddr_debug()
06/06/13   dh        Backup gcc reset status then clear it
06/14/13   dh        Only load ddr training data if sbl is not in dload mode
05/21/13   yp        Turn on vibrator when device power up in normal mode.
04/04/13   dh        Move boot_DALSYS_InitMod to early sbl1_main_ctl
04/02/13   dh        Use boot_dload_is_dload_mode_set instead of uefi dump cookie
                     to determin if device is in dload mode
04/11/13   sl        Relocate boot_ddr_test() call
04/03/13   sl        Call Clock API to get DDR speed
03/21/13   dh        Add memory barrier in boot_ddr_post_init
03/07/12   jz        Cleanup logging
12/13/12   jz        Change clock frequency back to MAX after DDR training is done
12/04/12   dh        Move boot_Tsens_Init to sbl1_hw_init
11/26/12   dh        Skip DDR training if UEFI dload cookie is set
11/12/12   sl        Enable DDR Test Framework
11/05/12   dh        Move thermal management code to common file
10/09/12   dh        Add boot_ICB_Segments_Init right after ddr init to configure
                     bimc slave address range
                     Add boot_SpmiCfg_Init before boot_SpmiBus_Init
10/09/12   dh        Only enable fast debug feature if FEATURE_BOOT_FAST_DEBUG
                     is defined
09/26/12   jz        Compile out boot_debug_mode_enter for Virtio testing
09/25/12   dh        Added pm_driver_init to allow PMIC API usage and pm_oem_init
	  	  	         to allow customer to call PMIC API in boot for
	  	             their desirable PMIC configurations.
08/30/12   dh        Add boot log for clock and pmic functions
08/16/12   AJC       Added boot_debug_mode_enter for FASTDEBUG feature
08/01/12   dh        Add sbl1_hw_deinit
07/23/12   dh        Add sbl1_set_ddr_training_data and sbl1_wait_for_ddr_training
07/16/12   dh        Move spmi and pmic init to sbl1_hw_init
06/18/12   dh        Switch to boot external driver api for tsensor and bus API
06/11/12   dh        Add sbl1_check_device_temp, check temp at the beginning of
                     sbl1_hw_init
06/08/12   dh        Add ICB_Config_Init in sbl1_hw_init
05/29/12   dh        Move boot_clock_init to sbl1_hw_init_secondary
                     Add boot_busywait_init in sbl1_hw_init
                     Remove gpio init from sbl1_hw_init_secondary since it must be
                     called after smem is available.
                     Rename sbl1_clk_regime_ram_dump_init to sbl1_hw_dload_init
05/08/12   dh        Add clock init api
10/18/11   dh        Initial revision
=============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_comdef.h"
#include "boot_sbl_if.h"
#include "sbl1_hw.h"
#include "sbl1_mc.h"
#include "boothw_target.h"
#include "boot_dload.h"
#include "boot_logger.h"
#include "boot_config_data.h"
#include "boot_util.h"
#include "boot_thermal_management.h"
#include "boot_extern_clk_interface.h"
#include "boot_extern_ddr_interface.h"
#include "boot_extern_ddi_api_interface.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_dal_interface.h"
#include "boot_extern_tlmm_interface.h"
#include "boot_extern_bus_interface.h"
#include "boot_extern_tsensor_interface.h"
#include "boot_extern_pimem_interface.h"
#include "boot_extern_power_interface.h"
#include "boot_extern_qusb_edl_interface.h"
#include "boot_extern_vsense_interface.h"
#include "boot_shared_imem_cookie.h"
#include "string.h"
#include "boot_cache_mmu.h"
#include "HALbootHWIO.h"
#include "boot_profiler.h"
#include "boot_page_table_armv8.h"
#include "ddr_drivers.h"
#include "icb_sdi.h"
#include "boot_visual_indication.h"

#include "DDITlmm.h"
#include "pm_pon.h"
#include "boot_flash_trans_if.h"
#include "boot_flash_dev_if.h"
#include "boot_shared_functions.h"
#include "boot_sdcc.h"
#include <stdio.h>
#include "boot_extern_busywait_interface.h"
/*Task: 1989872, Smart Log*/
#include "tct.h"

#define RESTART_REASON_ADDR_OFFSET 0x65C
#define NPI_GPIO                   38    /*38 is GPIO for idol4 pro cn*/
#define EMMC_MODE                  0x776655CC
extern uint8 traceability_partition_id[];
/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/

/* Writing a 1 clears corresponding bit field */
#define GCC_RESET_STATUS_CLEAR_VAL 0xF

/* backed up GCC_RESET_STATUS for ramdump */
static uint32 reset_status_register = 0;

static boot_boolean need_reset_device_by_qsee = FALSE;

/*=============================================================================

                              FUNCTION DEFINITIONS

=============================================================================*/

static void sbl1_save_ddr_training_data(bl_shared_data_type *bl_shared_data)
{
  uint32 ddr_params_training_data_size = 0;
  void* ddr_training_data_ptr = NULL;
  
  memory_barrier();
        
  ddr_training_data_ptr = 
    boot_ddr_params_get_training_data(&ddr_params_training_data_size);
        
  BL_VERIFY((ddr_training_data_ptr != NULL) &&
                   (ddr_params_training_data_size != 0),
                   BL_ERR_NULL_PTR);
        
  /*Save the updated training data to storage device */
  sbl1_save_ddr_training_data_to_partition(ddr_training_data_ptr, 
                                           ddr_params_training_data_size, 
                                           0);
                                                
  /* Do a reset after training data is saved */
  bl_shared_data->sbl_qsee_interface.reset_required = (uint32) RESET_DEVICE_BY_QSEE;
  need_reset_device_by_qsee = TRUE;

}

/*===========================================================================

**  Function :  sbl1_ddr_init

** ==========================================================================
*/
/*!
*
* @brief
*   Initialize DDR device.
* *
* @par Dependencies
*  Following Api's need to be called first:
*    sbl1_hw_init : To have ddr clocks setup.
*    sbl1_ddr_set_params : To have DDR config parameters setup.
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_ddr_init(bl_shared_data_type *bl_shared_data)
{
  uintnt page_table_address;
  uintnt page_table_entry;
  boot_boolean status = FALSE;

  #if !defined(FEATURE_DDI_IMAGE) && !defined(FEATURE_DEVICEPROGRAMMER_IMAGE) 
    boolean ddr_save_training_required = FALSE;
  #endif

  ddr_info sbl1_ddr_info;
  struct mem_block pimem_mem_block;
  boot_boolean result = FALSE;
  
  /* Initialize DDR */
  boot_log_message("ddr_initialize_device, Start");
  boot_ddr_initialize_device(boot_clock_get_ddr_speed());
  boot_log_message("ddr_initialize_device, Delta");
  
  if(!boot_dload_is_dload_mode_set())
  {
    /* Apply any workarounds needed post init */
    boot_ddr_post_init();
  }
  /* Configure DDR slave segments address range */
  sbl1_ddr_info = boot_ddr_get_info();
  boot_ICB_Segments_Init( "/dev/icbcfg/boot", &sbl1_ddr_info);
  
  boot_ddr_remapper();
  icb_sdi_save();
  
  boot_ddr_test(boot_clock_get_ddr_speed());
  /* perform sanity ddr test */
  boot_log_message("Basic DDR tests done");
  
  #ifdef FEATURE_DEVICEPROGRAMMER_IMAGE  
    /* Skip DDR training for Device Programmer, and lower clock speed */
    boot_Clock_SetBIMCSpeed(200000);
  #else	
   #ifdef FEATURE_DDI_IMAGE
	//This does nothing during normal DDI boot and never returns if training is called from DDI
	// set MMU for DDI
	sbl1_devprog_mmu_config();
    
    retrain_from_ddi();
    

    boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_CODE_BASE, 
                                               SCL_DDR_TRAINING_CODE_SIZE);
    boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_DATA_BASE, 
                                               SCL_DDR_TRAINING_DATA_SIZE);
    boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_DATA_ZI_BASE, 
                                               SCL_DDR_TRAINING_DATA_ZI_SIZE);
   #else
   if(!boot_dload_is_dload_mode_set())
   {
    // boot_enable_led(DDR_TRAINING_LED, TRUE);/* [BUFFIX]-Removed- by TCTSH.XQJ, PR-2156961, 2016/05/18,disalbe led flash in phone power on*/
     if (boot_ddr_do_ddr_training())
     {
	   /* Do DDR training */
       boot_log_message("do_ddr_training, Start");
       boot_log_start_timer();
       ddr_training_entry();
       ddr_save_training_required = boot_ddr_post_training();
       boot_log_stop_timer("do_ddr_training, Delta"); 
     }
    // boot_enable_led(DDR_TRAINING_LED, FALSE);/* [BUFFIX]-Removed- by TCTSH.XQJ, PR-2156961, 2016/05/18,disalbe led flash in phone power on*/
   
   
     boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_CODE_BASE, 
                                               SCL_DDR_TRAINING_CODE_SIZE);
     boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_DATA_BASE, 
                                               SCL_DDR_TRAINING_DATA_SIZE);
     boot_clobber_remove_global_protection_region ((void *)SCL_DDR_TRAINING_DATA_ZI_BASE, 
                                               SCL_DDR_TRAINING_DATA_ZI_SIZE);
   }
           
   if(ddr_save_training_required)
   {
     /* Training is done, save to ddr training partition */
     /* Save ddr training data to storage device if it is updated */
     sbl1_save_ddr_training_data(bl_shared_data);  
   }
   #endif

#endif

  /*ICB post config api will configure any settings done after DDR/memmap is finalized. */
  BL_VERIFY(boot_ICB_Config_PostInit("/dev/icbcfg/boot") == ICBCFG_SUCCESS,BL_ERR_SBL);

  status = boot_mmu_get_page_table_entry_addr( 
    (uintnt*)mmu_get_page_table_base(), 
    SCL_pIMEM_BASE,
    (uintnt*)&page_table_address );

  BL_VERIFY(status, BL_ERR_SBL);

  /* Configure Pimem memory region */
  pimem_mem_block.p_base = SCL_pIMEM_BASE;
  pimem_mem_block.v_base = SCL_pIMEM_BASE;
  pimem_mem_block.size_in_kbytes = SCL_pIMEM_SIZE >> 10;
  pimem_mem_block.memory_mapping = MMU_L2_SECTION_MAPPING;
  pimem_mem_block.access = MMU_PAGETABLE_MEM_READ_WRITE;
  pimem_mem_block.executable = MMU_PAGETABLE_NON_EXECUTABLE_REGION;  
  
  page_table_entry =    (pimem_mem_block.p_base)          |
                        (pimem_mem_block.access)          |
                        (MMU_PAGETABLE_INNER_WT_OUTER_WT) |
                        (pimem_mem_block.executable)      |
                         MMU_PAGETABLE_BLOCK_DESCRIPTOR;
                         
  if(pimem_mem_block.memory_mapping == MMU_L2_NS_SECTION_MAPPING)
  {
    page_table_entry |= MMU_PAGETABLE_BLOCK_NS;
  }
  
  /*Initialize pImem only during cold boot */ 
  if(!boot_dload_is_dload_mode_set())
  {
    boot_pimem_init(page_table_address, page_table_entry);
  }
  else
  {
    sbl1_disable_serror();
    /* initialize for download mode */
    boot_pimem_debug_init(page_table_address, page_table_entry);
    sbl1_enable_serror();
  }

  /* update cache attribute to WB */
  pimem_mem_block.cachetype = MMU_PAGETABLE_MEM_WRITE_BACK_CACHE;
  
  result =
   boot_mmu_page_table_map_single_mem_block(
                               (uint64*)mmu_get_page_table_base(),
                               &pimem_mem_block);
  BL_VERIFY(result, BL_ERR_SBL);

  mmu_invalidate_tlb_el3();
  
} /* sbl1_ddr_init() */

#ifndef BOOT_PRE_SILICON
#ifdef  FEATURE_BOOT_FAST_DEBUG
/*===========================================================================

**  Function :  sbl1_enter_fastdebug

** ==========================================================================
*/
/*!
*
* @brief
*   Enters debug mode if PMIC GPIO key is pressed while booting up
*
* @param[in] bl_shared_data Pointer to shared data
*
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
static volatile boolean enter_fastdebug = FALSE;
static void  boot_debug_mode_enter (void)
{

	uint8 data;
	uint32 addr, readlen;

	// On 8974, GPIO4 is used for the focus key
	// Use a pull up value of 30uA and a voltage source of VIN0

    data = 0x2;
	addr = 0xC341;
	BL_VERIFY((SPMI_BUS_SUCCESS == boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1)), BL_ERR_SBL);

  	data = 0x0;
    addr = 0xC340;
	BL_VERIFY((SPMI_BUS_SUCCESS == boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1)), BL_ERR_SBL);


	// Slave & Priority  = 0
	data = 0x0;
	addr = 0xC342;
	BL_VERIFY((SPMI_BUS_SUCCESS == boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1)), BL_ERR_SBL);

	// Then enable the GPIO
	data = 0x80;
	addr = 0xC346;
	BL_VERIFY((SPMI_BUS_SUCCESS == boot_SpmiBus_WriteLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1)), BL_ERR_SBL);

	// Now poll the value
	addr = 0xC308;
	if (SPMI_BUS_SUCCESS == boot_SpmiBus_ReadLong(0, (SpmiBus_AccessPriorityType)0, addr, &data, 1, &readlen))
	{
		// If GPIO is pressed, bit 0 will be 0 (active low)
		if (!(data & 0x1))
		{
			enter_fastdebug = TRUE;
			while(enter_fastdebug);
		}
	}
}
#endif
#endif


/*===========================================================================

**  Function :  sbl1_hw_init

** ==========================================================================
*/
/*!
*
* @brief
*   This function performs hardware initialization.
*
*   Only common hardware that is needed for flash
*   initialization is initialized here.
*
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_hw_init()
{
#ifndef BOOT_PRE_SILICON
#ifdef FEATURE_BOOT_FAST_DEBUG
  /* Check if we are going to enter debug mode */
  boot_debug_mode_enter();
#endif
#endif

  /* Initialize temperature sensor */
  BL_VERIFY(boot_Tsens_Init() == TSENS_SUCCESS,BL_ERR_SBL);

  /* Check the temperature */
  boot_check_device_temp();

  /* Put High Speed QUSB2 PHY in non-drive mode. */
  boot_qusb_hs_phy_nondrive_mode_set();

#if (!defined(FEATURE_RUMI_BOOT))
  /* Calling here to ensure eeprom init goes fine for CDT read */
  boot_pre_i2c_clock_init();

#endif /*FEATURE_RUMI_BOOT*/

} /* sbl1_hw_init() */


/*===========================================================================

**  Function :  sbl1_hw_deinit

** ==========================================================================
*/
/*!
*
* @brief
*   This function deinit the hardware that's not needed beyond sbl1
*
* @param[in] bl_shared_data Pointer to shared data
*
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_hw_deinit()
{

  /* Call clock exit boot to disable unneeded clock*/
  BL_VERIFY(boot_clock_exit_boot() == TRUE,BL_ERR_SBL);

} /* sbl1_hw_deinit() */


/*===========================================================================

**  Function :  sbl1_hw_dload_init

** ==========================================================================
*/
/*!
*
* @brief
*   This function initializes the necessary clocks and pmic configs for dload
*   We only do so when auth is disabled
*
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_hw_dload_init(bl_shared_data_type *bl_shared_data)
{
  boot_dload_clock_init();
  
  /* Call into pmic to configure MPP2 so that hard reset on 
    8996 does not reset MDM on fusion platform.
  */
  boot_pm_app_config_download_mode();
  
}


/*===========================================================================

**  Function :  sbl1_hw_init_secondary

** ==========================================================================
*/
/*!
*
* @brief
*   This function performs secondary hardware initialization.
*
*   DDR is initialized here. This needs to be done after trustzone is loaded.
*
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_hw_init_secondary( bl_shared_data_type *bl_shared_data )
{
  /*compile out pmic and clock api for rumi */
#if (!defined(FEATURE_RUMI_BOOT))

  /* First save gcc reset status to shared IMEM, then clear it */
  reset_status_register = HWIO_IN(GCC_RESET_STATUS);

  if (boot_shared_imem_cookie_ptr)
  {
    boot_shared_imem_cookie_ptr->reset_status_register = reset_status_register;
  }

  HWIO_OUT(GCC_RESET_STATUS,GCC_RESET_STATUS_CLEAR_VAL);

  /* Clear GCC Reset Enable so DDR is not in wdog self refresh after
    warm reset. The device will not enter SDI with the reg cleared.
    however if there is warm reset after gcc reset debug enable is cleared, 
    pimem contents will be lost.
  */
  HWIO_OUT(GCC_RESET_DEBUG_ENABLE,0x0);

  
  /* note for future, this needs to happen post ddr set param and set default param
   otherwise we will not be able to detect if we are in 2nd pass or not
   */
  HWIO_OUTF(GCC_RESET_FSM_CTRL, FIRST_PASS_COMPLETE, 0x0);

  boot_log_message("clock_init, Start");
  boot_log_start_timer();

  BL_VERIFY((boot_clock_init() == TRUE), BL_ERR_SBL);

  boot_log_stop_timer("clock_init, Delta");

#endif /*FEATURE_RUMI_BOOT*/

} /* sbl1_hw_init_secondary() */

/*Task: 1989872, Smart Log
  restart_reason   0xXXXX YYYY
  XXXX =1122   smart log always on
  XXXX =1212   smart log on for one time
  XXX0,XXX1    which buffer to store log, this value will submit to HOLS
  YYYY =3456   smart log enable
  YYYY =456    temp disable, xbl may crash if always on
*/
#if defined(FEATURE_TCT_SMART_LOG)
#define PERMANENT_MARK         0x1122
#define ONETIME_MARK           0x1212
#define AFTERSALE_MARK         0x3456
#define TEMP_DISABLE_AFTERSALE 0x456
#endif

//[PLATFORM]-Add-BEGIN by TCTNB.(LWS), FR-1162933 , 2015/12/15
/*===========================================================================

**  Function :  sbl1_cable_dload_check

** ==========================================================================
*/

// emergency cable download
#define TRACEABILITY_PARTITION_OFFSET 0x175
void sbl1_cable_dload_check(void)
{
    uint32 pwr_mode = 0;
    DalDeviceHandle *htlmm;

    DALGpioSignalType gpio_cfg = DAL_GPIO_CFG(NPI_GPIO, 0, DAL_GPIO_INPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA);

    uint32 *boot_restart_reason_ptr = (uint32*)(SHARED_IMEM_BOOT_BASE + RESTART_REASON_ADDR_OFFSET);
    uint8 is_usb_charge;
    DALGpioValueType value;
    char log_message[50];
    uint32 clear = 0x00000000; /*clear power on mode sign*/
    uint32 reason = 0x776655cc;

    /*Task: 1989872, Smart Log*/
#if defined(FEATURE_TCT_SMART_LOG)
    if(( (*boot_restart_reason_ptr) & 0xffff )== TEMP_DISABLE_AFTERSALE) {
       *boot_restart_reason_ptr = (*boot_restart_reason_ptr) | AFTERSALE_MARK;
    }
#endif

    boot_log_message("cable_dload_check, Start");
    boot_log_start_timer();

    /* get status of usb */
    boot_busywait(5*100*1000);/*wait 0.5s*/
    pm_comm_read_byte(2, 0x1310, &is_usb_charge, 0);

    //snprintf(log_message,50,"is_usb_charge 0x%x",is_usb_charge);
    //boot_log_message(log_message);
    is_usb_charge = is_usb_charge >> 2 ;

    /*get boot mode*/
    boot_read_flash_partition(traceability_partition_id,
		                      (void*)&pwr_mode,
		                      TRACEABILITY_PARTITION_OFFSET,
		                      sizeof(uint32));
    //snprintf(log_message,50,"traceability pwr_mode is 0x%x",pwr_mode);
    //boot_log_message(log_message);

    /* USB power on */
    if (is_usb_charge ){

         // Create a TLMM handle
         DAL_DeviceAttach(DALDEVICEID_TLMM, &htlmm);

         DalDevice_Open(htlmm, DAL_OPEN_SHARED);

         // Enable GPIO
         DalTlmm_ConfigGpio(htlmm, gpio_cfg, DAL_TLMM_GPIO_ENABLE);

        /*delay 100ms*/
        DALSYS_BusyWait(100000);

        /* GPIO operation */
        DalTlmm_GpioIn(htlmm, gpio_cfg, &value);

         // Disable GPIO
         DalTlmm_ConfigGpio(htlmm, gpio_cfg, DAL_TLMM_GPIO_DISABLE);

         // Remove the handle
         DalDevice_Close(htlmm);
         DAL_DeviceDetach(htlmm);

       /* check NPI_DOWNLOAD AND sahara or mprg reboot  */
        if(( DAL_GPIO_LOW_VALUE == value )&&(pwr_mode != 0x776655cc)) {
	      boot_write_flash_partition(traceability_partition_id,
			                       &reason,
			                       TRACEABILITY_PARTITION_OFFSET,
			                       sizeof(uint32));
          boot_dload_transition_pbl_forced_dload();
        }

    }

	if(pwr_mode == 0x776655cc){

	    snprintf(log_message,50,"pwr_mode is 0x%x clear",pwr_mode);
            boot_log_message(log_message);
            *boot_restart_reason_ptr = 0x776655cc;
            if (need_reset_device_by_qsee) {
                boot_log_message("device need reset by qsee,don't clear dload flag!");
            } else {
		boot_write_flash_partition(traceability_partition_id,
	                               &clear,
			                       TRACEABILITY_PARTITION_OFFSET,
			                       sizeof(uint32));
            }
	}
	boot_log_message("cable_dload_check, End");

}

void boot_write_flash_partition
(
  uint8 * partition_id,           /*partition id define in boot_gpt_partition_id.c*/
  void* buf,                      /*data buffer*/
  uint32 byte_offset,             /*offset in partition*/
  uint32 size                     /*size to read*/
)
{
  boot_flash_trans_if_type *trans_if = NULL;
  /*boot_boolean success = FALSE;*/


  /* Write DDR training data to storage device */
  boot_flash_configure_target_image(partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  BL_VERIFY( trans_if != NULL, BL_ERR_SBL );

  /*success = */dev_sdcc_write_bytes(buf,
                                 byte_offset,
                                 size,
                                 GEN_IMG);

  /*BL_VERIFY(success, BL_ERR_SBL); not report error*/

  /* close parition*/
  boot_flash_dev_close_image(&trans_if);
}


void boot_read_flash_partition
(
  uint8 * partition_id,           /*partition id define in boot_gpt_partition_id.c*/
  void* buf,                      /*data buffer*/
  uint32 byte_offset,             /*offset in partition*/
  uint32 size                     /*size to read*/
)
{
  boot_flash_trans_if_type *trans_if = NULL;
  /*boot_boolean success = FALSE; not report error*/

  boot_flash_configure_target_image(partition_id);

  trans_if = boot_flash_dev_open_image(GEN_IMG);

  /*BL_VERIFY( trans_if != NULL, BL_ERR_NULL_PTR );*/

  boot_clobber_add_local_hole( boot_flash_trans_get_clobber_tbl_ptr( trans_if ),
                               buf, size );

  /*success = */boot_flash_trans_read( trans_if,
                                   buf,
                                   byte_offset,
                                   size);
  /*BL_VERIFY( success, BL_ERR_ELF_FLASH );*/

  /* close parition*/
  boot_flash_dev_close_image(&trans_if);
}
//[PLATFORM]-Add-END by TCTNB.(LWS), FR-1162933 , 2015/12/15

/*===========================================================================

**  Function :  sbl1_hw_pre_ddr_init

** ==========================================================================
*/
/*!
*
* @brief
*  Initialize PMIC and railway driver. API used for hw initialization that
*  must occur before DDR init.
*
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*  	This Api needs to be called :
*  Before: sbl1_ddr_init
*
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_hw_pre_ddr_init( bl_shared_data_type *bl_shared_data )
{
  if ((bl_shared_data->build_type == SBL_BUILD_SBL) ||
      (bl_shared_data->build_type == SBL_BUILD_DDRDEBUGIMAGE))
  {
    boot_log_message("pm_device_init, Start");
    boot_log_start_timer();
    
    if (bl_shared_data->build_type == SBL_BUILD_DDRDEBUGIMAGE)
    {
		boot_pm_set_psi_address((uint64)SCL_PMIC_CONFIG_BUF_BASE_DDI);
	}


    /* Initialize the pmic */
    BL_VERIFY((boot_pm_device_init() == PM_ERR_FLAG__SUCCESS), BL_ERR_SBL);
    boot_log_stop_timer("pm_device_init, Delta");
  
    boot_log_message("pm_driver_init, Start");
    boot_log_start_timer();    
    BL_VERIFY((boot_pm_driver_init() == PM_ERR_FLAG__SUCCESS), BL_ERR_SBL);
    boot_log_stop_timer("pm_driver_init, Delta");
    
    boot_log_message("pm_sbl_chg_init, Start");
    boot_log_start_timer();    
    BL_VERIFY((boot_pm_sbl_chg_init() == PM_ERR_FLAG__SUCCESS), BL_ERR_SBL);
    boot_log_stop_timer("pm_sbl_chg_init, Delta");
	
    boot_log_message("vsense_init, Start");
    boot_log_start_timer();

    /* calibrate the voltage sensors */
    BL_VERIFY((boot_vsense_init() == TRUE), BL_ERR_SBL);

    boot_log_stop_timer("vsense_init, Delta"); 
  
    /*Initialize railway driver */
    boot_railway_init();
    
    /* Call CPR init to settle the voltages to the recommended levels*/
    boot_cpr_init();

  }
  else
  {
    boot_log_message("pm_device_programmer_init, Start");
    boot_log_start_timer();
  
    /* Initialize the pmic */
    BL_VERIFY((boot_pm_device_programmer_init() == PM_ERR_FLAG__SUCCESS), BL_ERR_SBL);  
  
    boot_log_stop_timer("pm_device_programmer_init, Delta");
  }
  
}

/*===========================================================================

**  Function :  sbl1_hw_get_reset_status

** ==========================================================================
*/
/*!
*
* @brief
*   This function returns the value of reset status register saved from this boot
*
* @par Dependencies
*   None
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
uint32 sbl1_hw_get_reset_status( void )
{
   return reset_status_register;
}  /* sbl1_hw_get_reset_status */

/*===========================================================================

**  Function :  sbl1_ddr_set_params

** ==========================================================================
*/
/*!
*
* @brief
*   Set DDR configuration parameters
*
* @param[in] bl_shared_data Pointer to shared data
*
* @par Dependencies
*  	This Api needs to be called :
*  Before: sbl1_ddr_init
*  After: boot_config_data_table_init
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_ddr_set_params(  bl_shared_data_type *bl_shared_data  )
{
  uint32 ddr_cdt_params_size = 0;
  struct cdt_info *config_data_table_info;
  uint8 *ddr_cdt_params_ptr;
  ddr_info ddr_type_info;

  /*Initialize param only during cold boot, for download mode preserve the content 
    also check if we are in sdi 2nd pass but no cookie is set
  */ 
  if(!boot_dload_is_dload_mode_set() && HWIO_INF(GCC_RESET_FSM_CTRL,FIRST_PASS_COMPLETE) == 0)
  {
    /* If a DDR CDB was found in flashed CDT, overwrite the previously shared
       default compiled DDR CDT configurations with DDR driver*/  
    if (boot_cdt_contains_ddr_cdb())
    {
      boot_log_message("DDR-CDB set params, Start");
      boot_log_start_timer();

      /* Extract ddr data block from configuration data table(CDT) */
      config_data_table_info =
                        bl_shared_data->sbl_shared_data->config_data_table_info;

      ddr_cdt_params_ptr =
                        boot_get_config_data_block(config_data_table_info->cdt_ptr,
                                                 CONFIG_DATA_BLOCK_INDEX_V1_DDR,
                                                 &ddr_cdt_params_size);

      /* Make sure cdt param size is valid */
      BL_VERIFY((ddr_cdt_params_size <= CONFIG_DATA_TABLE_MAX_SIZE) &&
                (ddr_cdt_params_ptr != NULL),  BL_ERR_SBL );
    
      /* Copy CDT DDR paramters to shared IMEM */
      qmemcpy((void*)SHARED_IMEM_BOOT_CDT_BASE,
              ddr_cdt_params_ptr,
              ddr_cdt_params_size);


      /* Pass cdt parameter to DDR driver */
      BL_VERIFY(boot_ddr_set_params((void*)SHARED_IMEM_BOOT_CDT_BASE, ddr_cdt_params_size) == TRUE, BL_ERR_SBL);

      boot_log_stop_timer("DDR-CDB set params, Delta");
    }

    /* Pass ddr training data to DDR driver */
    if (PcdGet32 (PcdBuildType) != SBL_BUILD_DEVICEPROGRAMMER_DDR && PcdGet32 (PcdBuildType) != SBL_BUILD_DDRDEBUGIMAGE)
    {
      sbl1_load_ddr_training_data();
    }
  }
  
  /* Get the ddr type by reading the cdt */
  ddr_type_info = boot_ddr_get_info();

  boot_log_message("Pre_DDR_clock_init, Start");
  boot_log_start_timer();

  /* Initialize the pre ddr clock */
  BL_VERIFY((boot_pre_ddr_clock_init(ddr_type_info.ddr_type) == TRUE), BL_ERR_SBL);

  boot_log_stop_timer("Pre_DDR_clock_init, Delta");
  
  /*ICB config api will configure address range for NOCs */
  BL_VERIFY(boot_ICB_Config_Init("/dev/icbcfg/boot") == ICBCFG_SUCCESS,BL_ERR_SBL);

} /* sbl1_ddr_set_params() */

/*===========================================================================

**  Function :  sbl1_ddr_set_default_params

** ==========================================================================
*/
/*!
* 
* @brief
*   Set DDR configuration parameters
* 
* @param[in] bl_shared_data Pointer to shared data
* 
* @par Dependencies
*  	This Api needs to be called :
*  Before: sbl1_ddr_init, boot_config_data_table_init
*  After: 
* 
* @retval
*   None
* 
* @par Side Effects
*   None
* 
*/
void sbl1_ddr_set_default_params(  bl_shared_data_type *bl_shared_data  )
{
  uint32 ddr_cdt_params_size = 0;
  struct cdt_info *config_data_table_info;
  uint8 *ddr_cdt_params_ptr;
  
  boot_log_message("sbl1_ddr_set_default_params, Start");
  boot_log_start_timer();    
  
  /*Initialize param only during cold boot, for download mode preserve the content 
    also check if we are in sdi 2nd pass but no cookie is set
  */   
  if(!boot_dload_is_dload_mode_set() && HWIO_INF(GCC_RESET_FSM_CTRL,FIRST_PASS_COMPLETE) == 0)
  {
    /* Extract ddr data block from configuration data table(CDT) */
    config_data_table_info = 
                      bl_shared_data->sbl_shared_data->config_data_table_info;
                      
    ddr_cdt_params_ptr = 
                      boot_get_config_data_block(config_data_table_info->cdt_ptr,
                                                 CONFIG_DATA_BLOCK_INDEX_V1_DDR,
                                                 &ddr_cdt_params_size);
    
    /* Make sure cdt param size is valid */
    BL_VERIFY((ddr_cdt_params_size <= CONFIG_DATA_TABLE_MAX_SIZE) &&
              (ddr_cdt_params_ptr != NULL),  BL_ERR_SBL );

      /* Copy CDT DDR parameters to shared IMEM */
    qmemscpy( (void*)SHARED_IMEM_BOOT_CDT_BASE, 
        SHARED_IMEM_BOOT_CDT_SIZE, 
        ddr_cdt_params_ptr, 
        ddr_cdt_params_size );
    
    /* Pass cdt parameter to DDR driver */
    BL_VERIFY(boot_ddr_set_params((void*)SHARED_IMEM_BOOT_CDT_BASE, ddr_cdt_params_size) == TRUE, BL_ERR_SBL);
  }
  boot_log_stop_timer("sbl1_ddr_set_default_params, Delta");
  
} /* sbl1_ddr_set_default_params() */


/*===========================================================================

**  Function :  sbl1_load_ddr_training_data

** ==========================================================================
*/
/*!
*
* @brief
*   If ddr training is required, read the ddr training data from partition
*   and pass it to ddr driver.
*
* @param[in]
*   None
*
* @par Dependencies
*   Must be called before sbl1_ddr_init
*
* @retval
*   None
*
* @par Side Effects
*   None
*
*/
void sbl1_load_ddr_training_data()
{

  uint32 ddr_params_training_data_size = 0;
  boot_boolean success = FALSE;
  uint8 *ddr_training_data_ptr = (uint8 *)SCL_DDR_TRAINING_DATA_BUF_BASE;
  
  /*Read the ddr partition if ddr training is required and not in dload mode */
  if((boot_ddr_params_is_training_required() == TRUE) &&
     (!boot_dload_is_dload_mode_set()))
  {
    /* Get the size of training data */
    boot_ddr_params_get_training_data(&ddr_params_training_data_size);

   /* Read the ddr training data out, we use
     SCL_DDR_TRAINING_DATA_BUF_BASE which is tz base as a temporary buffer */
    sbl1_load_ddr_training_data_from_partition(ddr_training_data_ptr,
                                               ddr_params_training_data_size);

    BL_VERIFY(ddr_training_data_ptr != NULL, BL_ERR_NULL_PTR);

    /* Pass the training data to DDR driver */
    success = boot_ddr_params_set_training_data((void *)ddr_training_data_ptr,
                                               ddr_params_training_data_size);

    BL_VERIFY(success, BL_ERR_SBL);
  }
}

