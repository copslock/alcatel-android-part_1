#ifndef __HAL_HWIO_TSENS_H__
#define __HAL_HWIO_TSENS_H__
/*============================================================================
  @file HALhwioTsens.h

  Implementation of the TSENS HAL - HWIO info was auto-generated

                Copyright (c) 2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "msmhwiobase.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS
 *--------------------------------------------------------------------------*/
#define HWIO_TSENS_HW_VER_ADDR(x)                                                     ((x) + 0x00000000)
#define HWIO_TSENS_HW_VER_RMSK                                                        0xffffffff
#define HWIO_TSENS_HW_VER_IN(x)      \
        in_dword_masked(HWIO_TSENS_HW_VER_ADDR(x), HWIO_TSENS_HW_VER_RMSK)
#define HWIO_TSENS_HW_VER_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_HW_VER_ADDR(x), m)
#define HWIO_TSENS_HW_VER_MAJOR_BMSK                                                  0xf0000000
#define HWIO_TSENS_HW_VER_MAJOR_SHFT                                                        0x1c
#define HWIO_TSENS_HW_VER_MINOR_BMSK                                                   0xfff0000
#define HWIO_TSENS_HW_VER_MINOR_SHFT                                                        0x10
#define HWIO_TSENS_HW_VER_STEP_BMSK                                                       0xffff
#define HWIO_TSENS_HW_VER_STEP_SHFT                                                          0x0

#define HWIO_TSENS_CTRL_ADDR(x)                                                       ((x) + 0x00000004)
#define HWIO_TSENS_CTRL_RMSK                                                          0x3fffffff
#define HWIO_TSENS_CTRL_IN(x)      \
        in_dword_masked(HWIO_TSENS_CTRL_ADDR(x), HWIO_TSENS_CTRL_RMSK)
#define HWIO_TSENS_CTRL_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_CTRL_ADDR(x), m)
#define HWIO_TSENS_CTRL_OUT(x, v)      \
        out_dword(HWIO_TSENS_CTRL_ADDR(x),v)
#define HWIO_TSENS_CTRL_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_CTRL_ADDR(x),m,v,HWIO_TSENS_CTRL_IN(x))
#define HWIO_TSENS_CTRL_MAX_TEMP_PWM_EN_BMSK                                          0x20000000
#define HWIO_TSENS_CTRL_MAX_TEMP_PWM_EN_SHFT                                                0x1d
#define HWIO_TSENS_CTRL_MAX_TEMP_PWM_EN_DISABLED_FVAL                                        0x0
#define HWIO_TSENS_CTRL_MAX_TEMP_PWM_EN_ENABLED_FVAL                                         0x1
#define HWIO_TSENS_CTRL_VALID_DELAY_BMSK                                              0x1e000000
#define HWIO_TSENS_CTRL_VALID_DELAY_SHFT                                                    0x19
#define HWIO_TSENS_CTRL_PSHOLD_ARES_EN_BMSK                                            0x1000000
#define HWIO_TSENS_CTRL_PSHOLD_ARES_EN_SHFT                                                 0x18
#define HWIO_TSENS_CTRL_PSHOLD_ARES_EN_DISABLED_FVAL                                         0x0
#define HWIO_TSENS_CTRL_PSHOLD_ARES_EN_ENABLED_FVAL                                          0x1
#define HWIO_TSENS_CTRL_TEMP_BROADCAST_EN_BMSK                                          0x800000
#define HWIO_TSENS_CTRL_TEMP_BROADCAST_EN_SHFT                                              0x17
#define HWIO_TSENS_CTRL_TEMP_BROADCAST_EN_DISABLED_FVAL                                      0x0
#define HWIO_TSENS_CTRL_TEMP_BROADCAST_EN_ENABLED_FVAL                                       0x1
#define HWIO_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_BMSK                                      0x400000
#define HWIO_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_SHFT                                          0x16
#define HWIO_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_DISABLED_FVAL                                  0x0
#define HWIO_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_ENABLED_FVAL                                   0x1
#define HWIO_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_BMSK                                 0x200000
#define HWIO_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_SHFT                                     0x15
#define HWIO_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_ADC_CODE_FVAL                             0x0
#define HWIO_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_REAL_TEMPERATURE_FVAL                     0x1
#define HWIO_TSENS_CTRL_TSENS_CLAMP_BMSK                                                0x100000
#define HWIO_TSENS_CTRL_TSENS_CLAMP_SHFT                                                    0x14
#define HWIO_TSENS_CTRL_TSENS_CLAMP_UNCLAMPED_FVAL                                           0x0
#define HWIO_TSENS_CTRL_TSENS_CLAMP_CLAMPED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_TSENS_BYPASS_EN_BMSK                                             0x80000
#define HWIO_TSENS_CTRL_TSENS_BYPASS_EN_SHFT                                                0x13
#define HWIO_TSENS_CTRL_TSENS_BYPASS_EN_DISABLED_FVAL                                        0x0
#define HWIO_TSENS_CTRL_TSENS_BYPASS_EN_ENABLED_FVAL                                         0x1
#define HWIO_TSENS_CTRL_SENSOR15_EN_BMSK                                                 0x40000
#define HWIO_TSENS_CTRL_SENSOR15_EN_SHFT                                                    0x12
#define HWIO_TSENS_CTRL_SENSOR15_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR15_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR14_EN_BMSK                                                 0x20000
#define HWIO_TSENS_CTRL_SENSOR14_EN_SHFT                                                    0x11
#define HWIO_TSENS_CTRL_SENSOR14_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR14_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR13_EN_BMSK                                                 0x10000
#define HWIO_TSENS_CTRL_SENSOR13_EN_SHFT                                                    0x10
#define HWIO_TSENS_CTRL_SENSOR13_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR13_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR12_EN_BMSK                                                  0x8000
#define HWIO_TSENS_CTRL_SENSOR12_EN_SHFT                                                     0xf
#define HWIO_TSENS_CTRL_SENSOR12_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR12_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR11_EN_BMSK                                                  0x4000
#define HWIO_TSENS_CTRL_SENSOR11_EN_SHFT                                                     0xe
#define HWIO_TSENS_CTRL_SENSOR11_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR11_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR10_EN_BMSK                                                  0x2000
#define HWIO_TSENS_CTRL_SENSOR10_EN_SHFT                                                     0xd
#define HWIO_TSENS_CTRL_SENSOR10_EN_DISABLED_FVAL                                            0x0
#define HWIO_TSENS_CTRL_SENSOR10_EN_ENABLED_FVAL                                             0x1
#define HWIO_TSENS_CTRL_SENSOR9_EN_BMSK                                                   0x1000
#define HWIO_TSENS_CTRL_SENSOR9_EN_SHFT                                                      0xc
#define HWIO_TSENS_CTRL_SENSOR9_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR9_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR8_EN_BMSK                                                    0x800
#define HWIO_TSENS_CTRL_SENSOR8_EN_SHFT                                                      0xb
#define HWIO_TSENS_CTRL_SENSOR8_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR8_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR7_EN_BMSK                                                    0x400
#define HWIO_TSENS_CTRL_SENSOR7_EN_SHFT                                                      0xa
#define HWIO_TSENS_CTRL_SENSOR7_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR7_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR6_EN_BMSK                                                    0x200
#define HWIO_TSENS_CTRL_SENSOR6_EN_SHFT                                                      0x9
#define HWIO_TSENS_CTRL_SENSOR6_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR6_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR5_EN_BMSK                                                    0x100
#define HWIO_TSENS_CTRL_SENSOR5_EN_SHFT                                                      0x8
#define HWIO_TSENS_CTRL_SENSOR5_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR5_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR4_EN_BMSK                                                     0x80
#define HWIO_TSENS_CTRL_SENSOR4_EN_SHFT                                                      0x7
#define HWIO_TSENS_CTRL_SENSOR4_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR4_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR3_EN_BMSK                                                     0x40
#define HWIO_TSENS_CTRL_SENSOR3_EN_SHFT                                                      0x6
#define HWIO_TSENS_CTRL_SENSOR3_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR3_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR2_EN_BMSK                                                     0x20
#define HWIO_TSENS_CTRL_SENSOR2_EN_SHFT                                                      0x5
#define HWIO_TSENS_CTRL_SENSOR2_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR2_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR1_EN_BMSK                                                     0x10
#define HWIO_TSENS_CTRL_SENSOR1_EN_SHFT                                                      0x4
#define HWIO_TSENS_CTRL_SENSOR1_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR1_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_SENSOR0_EN_BMSK                                                      0x8
#define HWIO_TSENS_CTRL_SENSOR0_EN_SHFT                                                      0x3
#define HWIO_TSENS_CTRL_SENSOR0_EN_DISABLED_FVAL                                             0x0
#define HWIO_TSENS_CTRL_SENSOR0_EN_ENABLED_FVAL                                              0x1
#define HWIO_TSENS_CTRL_TSENS_ADC_CLK_SEL_BMSK                                               0x4
#define HWIO_TSENS_CTRL_TSENS_ADC_CLK_SEL_SHFT                                               0x2
#define HWIO_TSENS_CTRL_TSENS_ADC_CLK_SEL_INTERNAL_OSCILLATOR_FVAL                           0x0
#define HWIO_TSENS_CTRL_TSENS_ADC_CLK_SEL_EXTERNAL_CLOCK_SOURCE_FVAL                         0x1
#define HWIO_TSENS_CTRL_TSENS_SW_RST_BMSK                                                    0x2
#define HWIO_TSENS_CTRL_TSENS_SW_RST_SHFT                                                    0x1
#define HWIO_TSENS_CTRL_TSENS_SW_RST_RESET_DEASSERTED_FVAL                                   0x0
#define HWIO_TSENS_CTRL_TSENS_SW_RST_RESET_ASSERTED_FVAL                                     0x1
#define HWIO_TSENS_CTRL_TSENS_EN_BMSK                                                        0x1
#define HWIO_TSENS_CTRL_TSENS_EN_SHFT                                                        0x0
#define HWIO_TSENS_CTRL_TSENS_EN_DISABLED_FVAL                                               0x0
#define HWIO_TSENS_CTRL_TSENS_EN_ENABLED_FVAL                                                0x1

#define HWIO_TSENS_MEASURE_PERIOD_ADDR(x)                                             ((x) + 0x00000008)
#define HWIO_TSENS_MEASURE_PERIOD_RMSK                                                    0xffff
#define HWIO_TSENS_MEASURE_PERIOD_IN(x)      \
        in_dword_masked(HWIO_TSENS_MEASURE_PERIOD_ADDR(x), HWIO_TSENS_MEASURE_PERIOD_RMSK)
#define HWIO_TSENS_MEASURE_PERIOD_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_MEASURE_PERIOD_ADDR(x), m)
#define HWIO_TSENS_MEASURE_PERIOD_OUT(x, v)      \
        out_dword(HWIO_TSENS_MEASURE_PERIOD_ADDR(x),v)
#define HWIO_TSENS_MEASURE_PERIOD_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_MEASURE_PERIOD_ADDR(x),m,v,HWIO_TSENS_MEASURE_PERIOD_IN(x))
#define HWIO_TSENS_MEASURE_PERIOD_POWERDOWN_MEASURE_PERIOD_BMSK                           0xff00
#define HWIO_TSENS_MEASURE_PERIOD_POWERDOWN_MEASURE_PERIOD_SHFT                              0x8
#define HWIO_TSENS_MEASURE_PERIOD_MAIN_MEASURE_PERIOD_BMSK                                  0xff
#define HWIO_TSENS_MEASURE_PERIOD_MAIN_MEASURE_PERIOD_SHFT                                   0x0

#define HWIO_TSENS_TEST_CTRL_ADDR(x)                                                  ((x) + 0x0000000c)
#define HWIO_TSENS_TEST_CTRL_RMSK                                                           0x1f
#define HWIO_TSENS_TEST_CTRL_IN(x)      \
        in_dword_masked(HWIO_TSENS_TEST_CTRL_ADDR(x), HWIO_TSENS_TEST_CTRL_RMSK)
#define HWIO_TSENS_TEST_CTRL_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_TEST_CTRL_ADDR(x), m)
#define HWIO_TSENS_TEST_CTRL_OUT(x, v)      \
        out_dword(HWIO_TSENS_TEST_CTRL_ADDR(x),v)
#define HWIO_TSENS_TEST_CTRL_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_TEST_CTRL_ADDR(x),m,v,HWIO_TSENS_TEST_CTRL_IN(x))
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_SEL_BMSK                                            0x1e
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_SEL_SHFT                                             0x1
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_EN_BMSK                                              0x1
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_EN_SHFT                                              0x0
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_DISABLED_FVAL                                0x0
#define HWIO_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_ENABLED_FVAL                                 0x1

#define HWIO_TSENS_MAX_MIN_INT_STATUS_ADDR(x)                                         ((x) + 0x00000010)
#define HWIO_TSENS_MAX_MIN_INT_STATUS_RMSK                                            0xffffffff
#define HWIO_TSENS_MAX_MIN_INT_STATUS_IN(x)      \
        in_dword_masked(HWIO_TSENS_MAX_MIN_INT_STATUS_ADDR(x), HWIO_TSENS_MAX_MIN_INT_STATUS_RMSK)
#define HWIO_TSENS_MAX_MIN_INT_STATUS_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_MAX_MIN_INT_STATUS_ADDR(x), m)
#define HWIO_TSENS_MAX_MIN_INT_STATUS_MAX_INT_STATUS_BMSK                             0xffff0000
#define HWIO_TSENS_MAX_MIN_INT_STATUS_MAX_INT_STATUS_SHFT                                   0x10
#define HWIO_TSENS_MAX_MIN_INT_STATUS_MIN_INT_STATUS_BMSK                                 0xffff
#define HWIO_TSENS_MAX_MIN_INT_STATUS_MIN_INT_STATUS_SHFT                                    0x0

#define HWIO_TSENS_MAX_MIN_INT_CLEAR_ADDR(x)                                          ((x) + 0x00000014)
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_RMSK                                             0xffffffff
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_IN(x)      \
        in_dword_masked(HWIO_TSENS_MAX_MIN_INT_CLEAR_ADDR(x), HWIO_TSENS_MAX_MIN_INT_CLEAR_RMSK)
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_MAX_MIN_INT_CLEAR_ADDR(x), m)
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_OUT(x, v)      \
        out_dword(HWIO_TSENS_MAX_MIN_INT_CLEAR_ADDR(x),v)
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_MAX_MIN_INT_CLEAR_ADDR(x),m,v,HWIO_TSENS_MAX_MIN_INT_CLEAR_IN(x))
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_MAX_INT_CLEAR_BMSK                               0xffff0000
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_MAX_INT_CLEAR_SHFT                                     0x10
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_MIN_INT_CLEAR_BMSK                                   0xffff
#define HWIO_TSENS_MAX_MIN_INT_CLEAR_MIN_INT_CLEAR_SHFT                                      0x0

#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(base,n)                                ((base) + 0x00000020 + 0x4 * (n))
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_RMSK                                         0x3ffffff
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAXn                                                15
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(base,n), HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_RMSK)
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(base,n),val)
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(base,n),mask,val,HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_INI(base,n))
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_BMSK                         0x2000000
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_SHFT                              0x19
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_NORMAL_OPERATION_FVAL              0x0
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_MASK_OFF_MAX_STATUS_FVAL           0x1
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_BMSK                         0x1000000
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_SHFT                              0x18
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_NORMAL_OPERATION_FVAL              0x0
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_MASK_OFF_MIN_STATUS_FVAL           0x1
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_THRESHOLD_BMSK                            0xfff000
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_THRESHOLD_SHFT                                 0xc
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_THRESHOLD_BMSK                               0xfff
#define HWIO_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_THRESHOLD_SHFT                                 0x0

#define HWIO_TSENS_Sn_CONVERSION_ADDR(base,n)                                         ((base) + 0x00000060 + 0x4 * (n))
#define HWIO_TSENS_Sn_CONVERSION_RMSK                                                  0x1ffffff
#define HWIO_TSENS_Sn_CONVERSION_MAXn                                                         15
#define HWIO_TSENS_Sn_CONVERSION_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_CONVERSION_ADDR(base,n), HWIO_TSENS_Sn_CONVERSION_RMSK)
#define HWIO_TSENS_Sn_CONVERSION_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_CONVERSION_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_CONVERSION_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_Sn_CONVERSION_ADDR(base,n),val)
#define HWIO_TSENS_Sn_CONVERSION_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_Sn_CONVERSION_ADDR(base,n),mask,val,HWIO_TSENS_Sn_CONVERSION_INI(base,n))
#define HWIO_TSENS_Sn_CONVERSION_SHIFT_BMSK                                            0x1800000
#define HWIO_TSENS_Sn_CONVERSION_SHIFT_SHFT                                                 0x17
#define HWIO_TSENS_Sn_CONVERSION_SLOPE_BMSK                                             0x7ffc00
#define HWIO_TSENS_Sn_CONVERSION_SLOPE_SHFT                                                  0xa
#define HWIO_TSENS_Sn_CONVERSION_CZERO_BMSK                                                0x3ff
#define HWIO_TSENS_Sn_CONVERSION_CZERO_SHFT                                                  0x0

#define HWIO_TSENS_Sn_ID_ASSIGNMENT_ADDR(base,n)                                      ((base) + 0x000000a0 + 0x4 * (n))
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_RMSK                                                     0xf
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_MAXn                                                      15
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_ID_ASSIGNMENT_ADDR(base,n), HWIO_TSENS_Sn_ID_ASSIGNMENT_RMSK)
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_ID_ASSIGNMENT_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_Sn_ID_ASSIGNMENT_ADDR(base,n),val)
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_Sn_ID_ASSIGNMENT_ADDR(base,n),mask,val,HWIO_TSENS_Sn_ID_ASSIGNMENT_INI(base,n))
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_SENSOR_ID_BMSK                                           0xf
#define HWIO_TSENS_Sn_ID_ASSIGNMENT_SENSOR_ID_SHFT                                           0x0

#define HWIO_TS_CONTROL_ADDR(x)                                                       ((x) + 0x000000e0)
#define HWIO_TS_CONTROL_RMSK                                                           0x1ffffff
#define HWIO_TS_CONTROL_IN(x)      \
        in_dword_masked(HWIO_TS_CONTROL_ADDR(x), HWIO_TS_CONTROL_RMSK)
#define HWIO_TS_CONTROL_INM(x, m)      \
        in_dword_masked(HWIO_TS_CONTROL_ADDR(x), m)
#define HWIO_TS_CONTROL_OUT(x, v)      \
        out_dword(HWIO_TS_CONTROL_ADDR(x),v)
#define HWIO_TS_CONTROL_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TS_CONTROL_ADDR(x),m,v,HWIO_TS_CONTROL_IN(x))
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_BMSK                         0x1000000
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_SHFT                              0x18
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_DISABLED_FVAL                      0x0
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_ENABLED_FVAL                       0x1
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_BMSK                             0x800000
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_SHFT                                 0x17
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_DISABLED_FVAL                         0x0
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_ENABLED_FVAL                          0x1
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_FREQ_BMSK                                   0x400000
#define HWIO_TS_CONTROL_REF_CURR_MIRROR_DEM_FREQ_SHFT                                       0x16
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_BMSK                                   0x200000
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_SHFT                                       0x15
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_DISABLED_FVAL                               0x0
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_ENABLED_FVAL                                0x1
#define HWIO_TS_CONTROL_REF_BJT_DEM_BMSK                                                0x180000
#define HWIO_TS_CONTROL_REF_BJT_DEM_SHFT                                                    0x13
#define HWIO_TS_CONTROL_SEND_QUANTIZER_OUTPUT_BMSK                                       0x40000
#define HWIO_TS_CONTROL_SEND_QUANTIZER_OUTPUT_SHFT                                          0x12
#define HWIO_TS_CONTROL_SEND_QUANTIZER_OUTPUT_DISABLED_FVAL                                  0x0
#define HWIO_TS_CONTROL_SEND_QUANTIZER_OUTPUT_ENABLED_FVAL                                   0x1
#define HWIO_TS_CONTROL_REF_OPAMP_CHOPPING_BMSK                                          0x30000
#define HWIO_TS_CONTROL_REF_OPAMP_CHOPPING_SHFT                                             0x10
#define HWIO_TS_CONTROL_NOT_USED_0_BMSK                                                   0x8000
#define HWIO_TS_CONTROL_NOT_USED_0_SHFT                                                      0xf
#define HWIO_TS_CONTROL_VBE_R_SENSE_CURRENT_DEM_BMSK                                      0x6000
#define HWIO_TS_CONTROL_VBE_R_SENSE_CURRENT_DEM_SHFT                                         0xd
#define HWIO_TS_CONTROL_BANDGAP_CORE_VREF_RES_TRIM_BMSK                                   0x1c00
#define HWIO_TS_CONTROL_BANDGAP_CORE_VREF_RES_TRIM_SHFT                                      0xa
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_BMSK                                     0x200
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_SHFT                                       0x9
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_VR1_FVAL                                   0x0
#define HWIO_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_VR2_FVAL                                   0x1
#define HWIO_TS_CONTROL_VBE_R_SENSE_OPAMP_CHOPPING_BMSK                                    0x180
#define HWIO_TS_CONTROL_VBE_R_SENSE_OPAMP_CHOPPING_SHFT                                      0x7
#define HWIO_TS_CONTROL_RO_CLK_TO_PIN_BMSK                                                  0x40
#define HWIO_TS_CONTROL_RO_CLK_TO_PIN_SHFT                                                   0x6
#define HWIO_TS_CONTROL_RO_CLK_TO_PIN_DISABLED_FVAL                                          0x0
#define HWIO_TS_CONTROL_RO_CLK_TO_PIN_ENABLED_FVAL                                           0x1
#define HWIO_TS_CONTROL_BANDGAP_CORE_CTAT_RES_TRIM_BMSK                                     0x3c
#define HWIO_TS_CONTROL_BANDGAP_CORE_CTAT_RES_TRIM_SHFT                                      0x2
#define HWIO_TS_CONTROL_SENSE_BJT_DEM_BMSK                                                   0x3
#define HWIO_TS_CONTROL_SENSE_BJT_DEM_SHFT                                                   0x0

#define HWIO_TS_CONFIG_ADDR(x)                                                        ((x) + 0x000000e4)
#define HWIO_TS_CONFIG_RMSK                                                                 0xff
#define HWIO_TS_CONFIG_IN(x)      \
        in_dword_masked(HWIO_TS_CONFIG_ADDR(x), HWIO_TS_CONFIG_RMSK)
#define HWIO_TS_CONFIG_INM(x, m)      \
        in_dword_masked(HWIO_TS_CONFIG_ADDR(x), m)
#define HWIO_TS_CONFIG_OUT(x, v)      \
        out_dword(HWIO_TS_CONFIG_ADDR(x),v)
#define HWIO_TS_CONFIG_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TS_CONFIG_ADDR(x),m,v,HWIO_TS_CONFIG_IN(x))
#define HWIO_TS_CONFIG_CLOCK_FREQ_BMSK                                                      0xc0
#define HWIO_TS_CONFIG_CLOCK_FREQ_SHFT                                                       0x6
#define HWIO_TS_CONFIG_NOT_USED_11_BMSK                                                     0x30
#define HWIO_TS_CONFIG_NOT_USED_11_SHFT                                                      0x4
#define HWIO_TS_CONFIG_ISENSE_MODE_FOR_BASE_RES_CAL_BMSK                                     0x8
#define HWIO_TS_CONFIG_ISENSE_MODE_FOR_BASE_RES_CAL_SHFT                                     0x3
#define HWIO_TS_CONFIG_VBE_R_SENSE_RES_TRIM_BMSK                                             0x7
#define HWIO_TS_CONFIG_VBE_R_SENSE_RES_TRIM_SHFT                                             0x0

#define HWIO_TSENS_SIDEBAND_EN_ADDR(x)                                                ((x) + 0x000000e8)
#define HWIO_TSENS_SIDEBAND_EN_RMSK                                                       0xffff
#define HWIO_TSENS_SIDEBAND_EN_IN(x)      \
        in_dword_masked(HWIO_TSENS_SIDEBAND_EN_ADDR(x), HWIO_TSENS_SIDEBAND_EN_RMSK)
#define HWIO_TSENS_SIDEBAND_EN_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_SIDEBAND_EN_ADDR(x), m)
#define HWIO_TSENS_SIDEBAND_EN_OUT(x, v)      \
        out_dword(HWIO_TSENS_SIDEBAND_EN_ADDR(x),v)
#define HWIO_TSENS_SIDEBAND_EN_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_SIDEBAND_EN_ADDR(x),m,v,HWIO_TSENS_SIDEBAND_EN_IN(x))
#define HWIO_TSENS_SIDEBAND_EN_SENSOR_EN_BMSK                                             0xffff
#define HWIO_TSENS_SIDEBAND_EN_SENSOR_EN_SHFT                                                0x0

#define HWIO_TSENS_TBCB_CONTROL_ADDR(x)                                               ((x) + 0x000000ec)
#define HWIO_TSENS_TBCB_CONTROL_RMSK                                                  0xffffffff
#define HWIO_TSENS_TBCB_CONTROL_IN(x)      \
        in_dword_masked(HWIO_TSENS_TBCB_CONTROL_ADDR(x), HWIO_TSENS_TBCB_CONTROL_RMSK)
#define HWIO_TSENS_TBCB_CONTROL_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_TBCB_CONTROL_ADDR(x), m)
#define HWIO_TSENS_TBCB_CONTROL_OUT(x, v)      \
        out_dword(HWIO_TSENS_TBCB_CONTROL_ADDR(x),v)
#define HWIO_TSENS_TBCB_CONTROL_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_TBCB_CONTROL_ADDR(x),m,v,HWIO_TSENS_TBCB_CONTROL_IN(x))
#define HWIO_TSENS_TBCB_CONTROL_TBCB_ACK_DELAY_BMSK                                   0xff000000
#define HWIO_TSENS_TBCB_CONTROL_TBCB_ACK_DELAY_SHFT                                         0x18
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLK_DIV_BMSK                                       0xff0000
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLK_DIV_SHFT                                           0x10
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_BMSK                                       0xffff
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_SHFT                                          0x0
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_DISABLED_FVAL                                 0x0
#define HWIO_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_ENABLED_FVAL                                  0x1

#define HWIO_TSENS_TBCB_CLIENT_n_REQ_ADDR(base,n)                                     ((base) + 0x000000f0 + 0x4 * (n))
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_RMSK                                                 0xffff
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_MAXn                                                     15
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_TBCB_CLIENT_n_REQ_ADDR(base,n), HWIO_TSENS_TBCB_CLIENT_n_REQ_RMSK)
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_TBCB_CLIENT_n_REQ_ADDR(base,n), mask)
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_TBCB_CLIENT_n_REQ_ADDR(base,n),val)
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_TBCB_CLIENT_n_REQ_ADDR(base,n),mask,val,HWIO_TSENS_TBCB_CLIENT_n_REQ_INI(base,n))
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_TCBC_CLIENT_REQ_SENSOR_BMSK                          0xffff
#define HWIO_TSENS_TBCB_CLIENT_n_REQ_TCBC_CLIENT_REQ_SENSOR_SHFT                             0x0

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS_TM
 *--------------------------------------------------------------------------*/
#define HWIO_TSENS_CONTROLLER_ID_ADDR(x)                                                ((x) + 0x00000000)
#define HWIO_TSENS_CONTROLLER_ID_RMSK                                                          0xf
#define HWIO_TSENS_CONTROLLER_ID_IN(x)      \
        in_dword_masked(HWIO_TSENS_CONTROLLER_ID_ADDR(x), HWIO_TSENS_CONTROLLER_ID_RMSK)
#define HWIO_TSENS_CONTROLLER_ID_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_CONTROLLER_ID_ADDR(x), m)
#define HWIO_TSENS_CONTROLLER_ID_CONTROLLER_ID_BMSK                                            0xf
#define HWIO_TSENS_CONTROLLER_ID_CONTROLLER_ID_SHFT                                            0x0

#define HWIO_TSENS_TM_INT_EN_ADDR(x)                                                    ((x) + 0x00000004)
#define HWIO_TSENS_TM_INT_EN_RMSK                                                              0x7
#define HWIO_TSENS_TM_INT_EN_IN(x)      \
        in_dword_masked(HWIO_TSENS_TM_INT_EN_ADDR(x), HWIO_TSENS_TM_INT_EN_RMSK)
#define HWIO_TSENS_TM_INT_EN_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_TM_INT_EN_ADDR(x), m)
#define HWIO_TSENS_TM_INT_EN_OUT(x, v)      \
        out_dword(HWIO_TSENS_TM_INT_EN_ADDR(x),v)
#define HWIO_TSENS_TM_INT_EN_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_TM_INT_EN_ADDR(x),m,v,HWIO_TSENS_TM_INT_EN_IN(x))
#define HWIO_TSENS_TM_INT_EN_CRITICAL_INT_EN_BMSK                                              0x4
#define HWIO_TSENS_TM_INT_EN_CRITICAL_INT_EN_SHFT                                              0x2
#define HWIO_TSENS_TM_INT_EN_CRITICAL_INT_EN_DISABLED_FVAL                                     0x0
#define HWIO_TSENS_TM_INT_EN_CRITICAL_INT_EN_ENABLED_FVAL                                      0x1
#define HWIO_TSENS_TM_INT_EN_UPPER_INT_EN_BMSK                                                 0x2
#define HWIO_TSENS_TM_INT_EN_UPPER_INT_EN_SHFT                                                 0x1
#define HWIO_TSENS_TM_INT_EN_UPPER_INT_EN_DISABLED_FVAL                                        0x0
#define HWIO_TSENS_TM_INT_EN_UPPER_INT_EN_ENABLED_FVAL                                         0x1
#define HWIO_TSENS_TM_INT_EN_LOWER_INT_EN_BMSK                                                 0x1
#define HWIO_TSENS_TM_INT_EN_LOWER_INT_EN_SHFT                                                 0x0
#define HWIO_TSENS_TM_INT_EN_LOWER_INT_EN_DISABLED_FVAL                                        0x0
#define HWIO_TSENS_TM_INT_EN_LOWER_INT_EN_ENABLED_FVAL                                         0x1

#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_ADDR(x)                                       ((x) + 0x00000008)
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_RMSK                                          0xffffffff
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_IN(x)      \
        in_dword_masked(HWIO_TSENS_UPPER_LOWER_INT_STATUS_ADDR(x), HWIO_TSENS_UPPER_LOWER_INT_STATUS_RMSK)
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_UPPER_LOWER_INT_STATUS_ADDR(x), m)
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_UPPER_INT_STATUS_BMSK                         0xffff0000
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_UPPER_INT_STATUS_SHFT                               0x10
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_LOWER_INT_STATUS_BMSK                             0xffff
#define HWIO_TSENS_UPPER_LOWER_INT_STATUS_LOWER_INT_STATUS_SHFT                                0x0

#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_ADDR(x)                                        ((x) + 0x0000000c)
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_RMSK                                           0xffffffff
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_OUT(x, v)      \
        out_dword(HWIO_TSENS_UPPER_LOWER_INT_CLEAR_ADDR(x),v)
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_UPPER_INT_CLEAR_BMSK                           0xffff0000
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_UPPER_INT_CLEAR_SHFT                                 0x10
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_LOWER_INT_CLEAR_BMSK                               0xffff
#define HWIO_TSENS_UPPER_LOWER_INT_CLEAR_LOWER_INT_CLEAR_SHFT                                  0x0

#define HWIO_TSENS_UPPER_LOWER_INT_MASK_ADDR(x)                                         ((x) + 0x00000010)
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_RMSK                                            0xffffffff
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_IN(x)      \
        in_dword_masked(HWIO_TSENS_UPPER_LOWER_INT_MASK_ADDR(x), HWIO_TSENS_UPPER_LOWER_INT_MASK_RMSK)
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_UPPER_LOWER_INT_MASK_ADDR(x), m)
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_OUT(x, v)      \
        out_dword(HWIO_TSENS_UPPER_LOWER_INT_MASK_ADDR(x),v)
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_UPPER_LOWER_INT_MASK_ADDR(x),m,v,HWIO_TSENS_UPPER_LOWER_INT_MASK_IN(x))
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_UPPER_INT_MASK_BMSK                             0xffff0000
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_UPPER_INT_MASK_SHFT                                   0x10
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_LOWER_INT_MASK_BMSK                                 0xffff
#define HWIO_TSENS_UPPER_LOWER_INT_MASK_LOWER_INT_MASK_SHFT                                    0x0

#define HWIO_TSENS_CRITICAL_INT_STATUS_ADDR(x)                                          ((x) + 0x00000014)
#define HWIO_TSENS_CRITICAL_INT_STATUS_RMSK                                                 0xffff
#define HWIO_TSENS_CRITICAL_INT_STATUS_IN(x)      \
        in_dword_masked(HWIO_TSENS_CRITICAL_INT_STATUS_ADDR(x), HWIO_TSENS_CRITICAL_INT_STATUS_RMSK)
#define HWIO_TSENS_CRITICAL_INT_STATUS_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_CRITICAL_INT_STATUS_ADDR(x), m)
#define HWIO_TSENS_CRITICAL_INT_STATUS_CRITICAL_INT_STATUS_BMSK                             0xffff
#define HWIO_TSENS_CRITICAL_INT_STATUS_CRITICAL_INT_STATUS_SHFT                                0x0

#define HWIO_TSENS_CRITICAL_INT_CLEAR_ADDR(x)                                           ((x) + 0x00000018)
#define HWIO_TSENS_CRITICAL_INT_CLEAR_RMSK                                                  0xffff
#define HWIO_TSENS_CRITICAL_INT_CLEAR_OUT(x, v)      \
        out_dword(HWIO_TSENS_CRITICAL_INT_CLEAR_ADDR(x),v)
#define HWIO_TSENS_CRITICAL_INT_CLEAR_CRITICAL_INT_CLEAR_BMSK                               0xffff
#define HWIO_TSENS_CRITICAL_INT_CLEAR_CRITICAL_INT_CLEAR_SHFT                                  0x0

#define HWIO_TSENS_CRITICAL_INT_MASK_ADDR(x)                                            ((x) + 0x0000001c)
#define HWIO_TSENS_CRITICAL_INT_MASK_RMSK                                                   0xffff
#define HWIO_TSENS_CRITICAL_INT_MASK_IN(x)      \
        in_dword_masked(HWIO_TSENS_CRITICAL_INT_MASK_ADDR(x), HWIO_TSENS_CRITICAL_INT_MASK_RMSK)
#define HWIO_TSENS_CRITICAL_INT_MASK_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_CRITICAL_INT_MASK_ADDR(x), m)
#define HWIO_TSENS_CRITICAL_INT_MASK_OUT(x, v)      \
        out_dword(HWIO_TSENS_CRITICAL_INT_MASK_ADDR(x),v)
#define HWIO_TSENS_CRITICAL_INT_MASK_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_CRITICAL_INT_MASK_ADDR(x),m,v,HWIO_TSENS_CRITICAL_INT_MASK_IN(x))
#define HWIO_TSENS_CRITICAL_INT_MASK_CRITICAL_INT_MASK_BMSK                                 0xffff
#define HWIO_TSENS_CRITICAL_INT_MASK_CRITICAL_INT_MASK_SHFT                                    0x0

#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(base,n)                                ((base) + 0x00000020 + 0x4 * (n))
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_RMSK                                          0xffffff
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_MAXn                                                15
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(base,n), HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_RMSK)
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(base,n),val)
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(base,n),mask,val,HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_INI(base,n))
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_UPPER_THRESHOLD_BMSK                          0xfff000
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_UPPER_THRESHOLD_SHFT                               0xc
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_LOWER_THRESHOLD_BMSK                             0xfff
#define HWIO_TSENS_Sn_UPPER_LOWER_THRESHOLD_LOWER_THRESHOLD_SHFT                               0x0

#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(base,n)                                   ((base) + 0x00000060 + 0x4 * (n))
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_RMSK                                                0xfff
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_MAXn                                                   15
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(base,n), HWIO_TSENS_Sn_CRITICAL_THRESHOLD_RMSK)
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_OUTI(base,n,val)    \
        out_dword(HWIO_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(base,n),val)
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_OUTMI(base,n,mask,val) \
        out_dword_masked_ns(HWIO_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(base,n),mask,val,HWIO_TSENS_Sn_CRITICAL_THRESHOLD_INI(base,n))
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_CRITICAL_THRESHOLD_BMSK                             0xfff
#define HWIO_TSENS_Sn_CRITICAL_THRESHOLD_CRITICAL_THRESHOLD_SHFT                               0x0

#define HWIO_TSENS_Sn_STATUS_ADDR(base,n)                                               ((base) + 0x000000a0 + 0x4 * (n))
#define HWIO_TSENS_Sn_STATUS_RMSK                                                         0x3fffff
#define HWIO_TSENS_Sn_STATUS_MAXn                                                               15
#define HWIO_TSENS_Sn_STATUS_INI(base,n)        \
        in_dword_masked(HWIO_TSENS_Sn_STATUS_ADDR(base,n), HWIO_TSENS_Sn_STATUS_RMSK)
#define HWIO_TSENS_Sn_STATUS_INMI(base,n,mask)    \
        in_dword_masked(HWIO_TSENS_Sn_STATUS_ADDR(base,n), mask)
#define HWIO_TSENS_Sn_STATUS_VALID_BMSK                                                   0x200000
#define HWIO_TSENS_Sn_STATUS_VALID_SHFT                                                       0x15
#define HWIO_TSENS_Sn_STATUS_MAX_STATUS_BMSK                                              0x100000
#define HWIO_TSENS_Sn_STATUS_MAX_STATUS_SHFT                                                  0x14
#define HWIO_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_NOT_VIOLATED_FVAL                        0x0
#define HWIO_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_VIOLATED_FVAL                            0x1
#define HWIO_TSENS_Sn_STATUS_CRITICAL_STATUS_BMSK                                          0x80000
#define HWIO_TSENS_Sn_STATUS_CRITICAL_STATUS_SHFT                                             0x13
#define HWIO_TSENS_Sn_STATUS_CRITICAL_STATUS_CRITICAL_THRESHOLD_NOT_VIOLATED_FVAL              0x0
#define HWIO_TSENS_Sn_STATUS_CRITICAL_STATUS_CRITICAL_THRESHOLD_VIOLATED_FVAL                  0x1
#define HWIO_TSENS_Sn_STATUS_UPPER_STATUS_BMSK                                             0x40000
#define HWIO_TSENS_Sn_STATUS_UPPER_STATUS_SHFT                                                0x12
#define HWIO_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_NOT_VIOLATED_FVAL                    0x0
#define HWIO_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_VIOLATED_FVAL                        0x1
#define HWIO_TSENS_Sn_STATUS_LOWER_STATUS_BMSK                                             0x20000
#define HWIO_TSENS_Sn_STATUS_LOWER_STATUS_SHFT                                                0x11
#define HWIO_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_NOT_VIOLATED_FVAL                    0x0
#define HWIO_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_VIOLATED_FVAL                        0x1
#define HWIO_TSENS_Sn_STATUS_MIN_STATUS_BMSK                                               0x10000
#define HWIO_TSENS_Sn_STATUS_MIN_STATUS_SHFT                                                  0x10
#define HWIO_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_NOT_VIOLATED_FVAL                        0x0
#define HWIO_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_VIOLATED_FVAL                            0x1
#define HWIO_TSENS_Sn_STATUS_SENSOR_ID_BMSK                                                 0xf000
#define HWIO_TSENS_Sn_STATUS_SENSOR_ID_SHFT                                                    0xc
#define HWIO_TSENS_Sn_STATUS_LAST_TEMP_BMSK                                                  0xfff
#define HWIO_TSENS_Sn_STATUS_LAST_TEMP_SHFT                                                    0x0

#define HWIO_TSENS_MAX_TEMP_ADDR(x)                                                     ((x) + 0x000000e0)
#define HWIO_TSENS_MAX_TEMP_RMSK                                                            0xffff
#define HWIO_TSENS_MAX_TEMP_IN(x)      \
        in_dword_masked(HWIO_TSENS_MAX_TEMP_ADDR(x), HWIO_TSENS_MAX_TEMP_RMSK)
#define HWIO_TSENS_MAX_TEMP_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_MAX_TEMP_ADDR(x), m)
#define HWIO_TSENS_MAX_TEMP_MAX_TEMP_SENSOR_ID_BMSK                                         0xf000
#define HWIO_TSENS_MAX_TEMP_MAX_TEMP_SENSOR_ID_SHFT                                            0xc
#define HWIO_TSENS_MAX_TEMP_MAX_TEMP_BMSK                                                    0xfff
#define HWIO_TSENS_MAX_TEMP_MAX_TEMP_SHFT                                                      0x0

#define HWIO_TSENS_TRDY_ADDR(x)                                                         ((x) + 0x000000e4)
#define HWIO_TSENS_TRDY_RMSK                                                                   0x7
#define HWIO_TSENS_TRDY_IN(x)      \
        in_dword_masked(HWIO_TSENS_TRDY_ADDR(x), HWIO_TSENS_TRDY_RMSK)
#define HWIO_TSENS_TRDY_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_TRDY_ADDR(x), m)
#define HWIO_TSENS_TRDY_OSC_CLK_OFF_BMSK                                                       0x4
#define HWIO_TSENS_TRDY_OSC_CLK_OFF_SHFT                                                       0x2
#define HWIO_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_ON_FVAL                                             0x0
#define HWIO_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_OFF_FVAL                                            0x1
#define HWIO_TSENS_TRDY_SLP_CLK_OFF_BMSK                                                       0x2
#define HWIO_TSENS_TRDY_SLP_CLK_OFF_SHFT                                                       0x1
#define HWIO_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_ON_FVAL                                             0x0
#define HWIO_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_OFF_FVAL                                            0x1
#define HWIO_TSENS_TRDY_TRDY_BMSK                                                              0x1
#define HWIO_TSENS_TRDY_TRDY_SHFT                                                              0x0
#define HWIO_TSENS_TRDY_TRDY_TEMPERATURE_MEASUREMENT_IN_PROGRESS_FVAL                          0x0
#define HWIO_TSENS_TRDY_TRDY_TEMPERATURE_READING_IS_READY_FVAL                                 0x1

#define HWIO_TSENS_DEBUG_CONTROL_ADDR(x)                                                ((x) + 0x00000130)
#define HWIO_TSENS_DEBUG_CONTROL_RMSK                                                     0x7fffff
#define HWIO_TSENS_DEBUG_CONTROL_IN(x)      \
        in_dword_masked(HWIO_TSENS_DEBUG_CONTROL_ADDR(x), HWIO_TSENS_DEBUG_CONTROL_RMSK)
#define HWIO_TSENS_DEBUG_CONTROL_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_DEBUG_CONTROL_ADDR(x), m)
#define HWIO_TSENS_DEBUG_CONTROL_OUT(x, v)      \
        out_dword(HWIO_TSENS_DEBUG_CONTROL_ADDR(x),v)
#define HWIO_TSENS_DEBUG_CONTROL_OUTM(x,m,v) \
        out_dword_masked_ns(HWIO_TSENS_DEBUG_CONTROL_ADDR(x),m,v,HWIO_TSENS_DEBUG_CONTROL_IN(x))
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_ID_BMSK                                  0x7fff80
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_ID_SHFT                                       0x7
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_BMSK                             0x40
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_SHFT                              0x6
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_DISABLED_FVAL                     0x0
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_ENABLED_FVAL                      0x1
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_BMSK                            0x20
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_SHFT                             0x5
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_DISABLED_FVAL                    0x0
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_ENABLED_FVAL                     0x1
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_SEL_BMSK                                     0x1e
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_SEL_SHFT                                      0x1
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_BMSK                                       0x1
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_SHFT                                       0x0
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_DISABLED_FVAL                              0x0
#define HWIO_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_ENABLED_FVAL                               0x1

#define HWIO_TSENS_DEBUG_READ_ADDR(x)                                                   ((x) + 0x00000134)
#define HWIO_TSENS_DEBUG_READ_RMSK                                                      0xffffffff
#define HWIO_TSENS_DEBUG_READ_IN(x)      \
        in_dword_masked(HWIO_TSENS_DEBUG_READ_ADDR(x), HWIO_TSENS_DEBUG_READ_RMSK)
#define HWIO_TSENS_DEBUG_READ_INM(x, m)      \
        in_dword_masked(HWIO_TSENS_DEBUG_READ_ADDR(x), m)
#define HWIO_TSENS_DEBUG_READ_DEBUG_DATA_READ_BMSK                                      0xffffffff
#define HWIO_TSENS_DEBUG_READ_DEBUG_DATA_READ_SHFT                                             0x0

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/
#define SECURITY_CONTROL_CORE_REG_BASE                                                        (SECURITY_CONTROL_BASE      + 0x00000000)

#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004238)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_BMSK                                0xfc000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE1_5_0_SHFT                                      0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE0_BMSK                                     0x3ff0000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS0_BASE0_SHFT                                          0x10
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS_CAL_SEL_BMSK                                       0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_TSENS_CAL_SEL_SHFT                                          0xd
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CLK_B6_BMSK                                              0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_CLK_B6_SHFT                                                 0xb
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_VREF_B6_BMSK                                              0x600
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_VREF_B6_SHFT                                                0x9
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M3_BMSK                                              0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M3_SHFT                                                0x6
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M2_BMSK                                               0x38
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M2_SHFT                                                0x3
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M1_BMSK                                                0x7
#define HWIO_QFPROM_CORR_CALIB_ROW10_LSB_PH_B6M1_SHFT                                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000423c)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW10_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW10_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE1_BMSK                                      0xffc000
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE1_SHFT                                           0xe
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE0_BMSK                                        0x3ff0
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS1_BASE0_SHFT                                           0x4
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_BMSK                                       0xf
#define HWIO_QFPROM_CORR_CALIB_ROW10_MSB_TSENS0_BASE1_9_6_SHFT                                       0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004240)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS9_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS9_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS8_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS8_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS7_OFFSET_BMSK                                     0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS7_OFFSET_SHFT                                         0x14
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS6_OFFSET_BMSK                                      0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS6_OFFSET_SHFT                                         0x10
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS5_OFFSET_BMSK                                       0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS5_OFFSET_SHFT                                          0xc
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS4_OFFSET_BMSK                                        0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS4_OFFSET_SHFT                                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS3_OFFSET_BMSK                                         0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS3_OFFSET_SHFT                                          0x4
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS2_OFFSET_BMSK                                          0xf
#define HWIO_QFPROM_CORR_CALIB_ROW11_LSB_TSENS2_OFFSET_SHFT                                          0x0

#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004244)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW11_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW11_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS17_OFFSET_BMSK                                  0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS17_OFFSET_SHFT                                        0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS16_OFFSET_BMSK                                   0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS16_OFFSET_SHFT                                        0x18
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS15_OFFSET_BMSK                                    0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS15_OFFSET_SHFT                                        0x14
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS14_OFFSET_BMSK                                     0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS14_OFFSET_SHFT                                        0x10
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS13_OFFSET_BMSK                                      0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS13_OFFSET_SHFT                                         0xc
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS12_OFFSET_BMSK                                       0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS12_OFFSET_SHFT                                         0x8
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS11_OFFSET_BMSK                                        0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS11_OFFSET_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS10_OFFSET_BMSK                                         0xf
#define HWIO_QFPROM_CORR_CALIB_ROW11_MSB_TSENS10_OFFSET_SHFT                                         0x0

#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004248)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW12_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_BMSK                              0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_SHFT                                    0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_BMSK                                  0x40000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_SHFT                                        0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_BMSK                               0x20000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_SHFT                                     0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_BMSK                               0x10000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_SHFT                                     0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_BMSK                                       0xff00000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_SHFT                                            0x14
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_BMSK                                         0xff000
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_SHFT                                             0xc
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS20_OFFSET_BMSK                                       0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS20_OFFSET_SHFT                                         0x8
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS19_OFFSET_BMSK                                        0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS19_OFFSET_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS18_OFFSET_BMSK                                         0xf
#define HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TSENS18_OFFSET_SHFT                                         0x0

#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000424c)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_BMSK                                0xe0000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_SW_CAL_REDUN_SEL_SHFT                                      0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RSVD0_BMSK                                           0x1e000000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_RSVD0_SHFT                                                 0x19
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_HSTX_TRIM_LSB_BMSK                                    0x1e00000
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_HSTX_TRIM_LSB_SHFT                                         0x15
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_BMSK                              0x1fff80
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_VOLTAGE_SENSOR_CALIB_SHFT                                   0x7
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VCM_BMSK                                          0x60
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VCM_SHFT                                           0x5
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_LDO_BMSK                                          0x18
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_LDO_SHFT                                           0x3
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VREF_BMSK                                          0x6
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_GNSS_ADC_VREF_SHFT                                          0x1
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_BMSK                                     0x1
#define HWIO_QFPROM_CORR_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004250)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW13_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_BMSK                               0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_LSB_SW_CAL_REDUN_31_0_SHFT                                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004254)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW13_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW13_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_BMSK                              0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW13_MSB_SW_CAL_REDUN_63_32_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004258)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW14_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_BMSK                              0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_LSB_SW_CAL_REDUN_95_64_SHFT                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000425c)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW14_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW14_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_BMSK                             0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW14_MSB_SW_CAL_REDUN_127_96_SHFT                                    0x0

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/

#endif /* #ifndef __HAL_HWIO_TSENS_H__ */

