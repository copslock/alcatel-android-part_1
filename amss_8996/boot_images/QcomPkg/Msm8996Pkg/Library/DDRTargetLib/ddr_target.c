/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/09/16   jh      Flush cache before warm reboot
01/26/16   tw      added force pwm mode in target init
07/28/15   rp      Added ddr_post_training function to support conditional loading of ddr training
07/20/15   tw      enable one time training
07/01/15   tw      added bimc remapper api call
06/20/14   tw      added ddr_pre_init api to capture any target specific 
                   workarounds that needs to be applied prior to ddr init
05/28/14   tw      cleaned up sbl <-> ddr driver dependencies around ddr training
                   implementation of cx\mx\cpr hash to force retraining
03/12/14   sr      Initial version.
================================================================================
                   Copyright 2014-2016 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_target.h"
#include "HAL_SNS_DDR.h"
#include "ddr_config.h"
#include "ClockBoot.h"
#include "icbcfg.h"
#include <stddef.h>
#include "ddr_external.h"
#include "pm_ldo.h"
#include "CoreVerify.h"
#include "cpr_api.h"
#include "crc.h"
#include "HALbootHWIO.h"
#include "boot_extern_platforminfo_interface.h"
#include "boothw_target.h"
#include "railway.h"
#include "busywait.h"
#include "ddi_tool.h"
#include "boot_cache_mmu.h"


#define ONE_TIME_TRAINING TRUE
#define PERIODIC_TRAINING TRUE

/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF

/*==============================================================================
                                  DATA
==============================================================================*/
/* DDR interface status, keeps track of active interfaces and their status */
extern ddr_interface_state ddr_status;

extern uint32 ddr_bus_width;

/* DDR Partition information */
ddr_size_partition ddr_size_partition_info;

uint32 msm_revision = DDR_TLMM_HW_REV_ID_V10;

extern ddr_info ddr_physical_size;
extern ddr_size_info ddr_system_size;

 uint32 ddr_post_init_freq = 1017000;
 
 static railway_voter_t ebi_tubo_pin = NULL;
 
/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
/* ============================================================================
**  Function : ddr_target_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called at the end of ddr init to initialize any
*   target specific configurations
*
*   @details
*   Target specific configurations such as override of timing registers and
*   calling target specific init functions will be captured in this api
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_target_init()
{
  uint32 nNumLevel;
  uint8 i;
  uint32 chip_plat_ver_info;
  ClockPlanType   *pClockPlan;

  /* dal variables to grab platform type */
  DalDeviceHandle *phPlatform;
  DALResult eResult;
  uint32 key_info = 0;
  uint32 err;
  
  if (PcdGet32 (PcdBuildType) == 0)
  {
    int rail = rail_id("vdda_ebi");
    CORE_VERIFY(rail!=RAIL_NOT_SUPPORTED_BY_RAILWAY);
      
    ebi_tubo_pin = railway_create_voter(rail, RAILWAY_DDR_TRAINING_VOTER_ID);
    railway_corner_vote(ebi_tubo_pin, RAILWAY_SUPER_TURBO);
    railway_transition_rails();
    
    err = pm_smps_sw_mode(0, PM_SMPS_8, PM_SW_MODE_NPM); // VDDA_EBI SMPS8 to PWM
    CORE_VERIFY(err == PM_ERR_FLAG__SUCCESS);

    err = pm_smps_sw_mode(0, PM_SMPS_1, PM_SW_MODE_NPM); // CX SMPS1 to PWM
    CORE_VERIFY(err == PM_ERR_FLAG__SUCCESS);
  }
     
  /* 8994 ddr is 32bit wide */
  ddr_bus_width = 32;

  eResult = DAL_DeviceAttachEx(NULL,
                                  DALDEVICEID_PLATFORMINFO,
                                  DALPLATFORMINFO_INTERFACE_VERSION,
                                  &phPlatform);

  if (eResult == DAL_SUCCESS)
  {
    if(DalPlatformInfo_GetKeyValue(phPlatform, DALPLATFORMINFO_KEY_DDR_FREQ, &key_info) != DAL_SUCCESS)
    {
      key_info = 0;
    }
  }

  chip_plat_ver_info =  HWIO_IN(TCSR_SOC_HW_VERSION);

  /* extract upper 16 bit and store it in platform field of ddr_misc */
  ddrsns_share_data->misc.platform_id  = chip_plat_ver_info >> 16 /*& 0xFFFF0000*/ ;
  /* extract lower 16 bits and store it in version field of ddr_misc */
  ddrsns_share_data->misc.chip_version  = chip_plat_ver_info & 0x0000FFFF ;

  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);
  CORE_VERIFY((DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan) == DAL_SUCCESS));

  seq_wait(1, US);

  if ( pClockPlan == NULL )
  {
    return;
  }
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);
  
  if(ddrsns_share_data->misc.platform_id == MSM_SOC_HW_VER_8996_PRO || HWIO_IN(TCSR_TCSR_RESET_DEBUG_SW_ENTRY) == MSM_SOC_HW_VER_8996_PRO)
  {
    // Detected 8996 Pro
    Clock_SetDDRMinMax( 0, 11 );
    nNumLevel = 12;
    boot_log_message("8996 Pro v1.x detected, Max frequency = 1.8 GHz");
  }
  /* get platform/speed bin */
  else if ( HWIO_INM(QFPROM_CORR_PTE_ROW0_LSB, 0xE0000000 ) == 0x20000000 )
  {
  	Clock_SetDDRMinMax (0, 9 );
    nNumLevel = 10;
    boot_log_message("Max frequency = 1.3 GHz");
  } 
  else if (key_info)
  {
    /* key found, override default max frequency */
    Clock_SetDDRMinMax( 0, 11 );
    nNumLevel = 12;
    boot_log_message("Compound socket detected, Max frequency = 1.8 GHz");
  }
  else if ( ddrsns_share_data->misc.chip_version == 0x200 )
  {
	  Clock_SetDDRMinMax( 0, 10 );
	  nNumLevel = 12;
    boot_log_message("8996 v2.0 detected, Max frequency = 1.5 GHz");
  }
  else if ( ddrsns_share_data->misc.chip_version == 0x201 )
  {
  	Clock_SetDDRMinMax (0, 11 );
	  nNumLevel = 12;
    boot_log_message("8996 v2.1 detected, Max frequency = 1.8 GHz");
  }
  else if ( ddrsns_share_data->misc.chip_version >= 0x300 )
  {
	  Clock_SetDDRMinMax( 0, 11 );
	  nNumLevel = 12;
    boot_log_message("8996 v3.x detected, Max frequency = 1.8 GHz");
  }
  else
  {
    Clock_SetDDRMinMax( 0, 11 );
	  nNumLevel = 12;
    boot_log_message("Unknown chip version detected, Max frequency = 1.8 GHz");
  }

  ddrsns_share_data->misc.ddr_num_clock_levels = nNumLevel;
  for(i=0;i<nNumLevel;i++)
   {
     ddrsns_share_data->misc.clock_plan[i].clk_freq_in_khz  = pClockPlan[i].nFreqKHz;
     ddrsns_share_data->misc.clock_plan[i].clk_mode         = pClockPlan[i].eMode;
     ddrsns_share_data->misc.clock_plan[i].pll_dec_start      = pClockPlan[i].sDSFConfig.pll_dec_start;
     ddrsns_share_data->misc.clock_plan[i].pll_div_frac_start3= pClockPlan[i].sDSFConfig.pll_div_frac_start3 ;
     ddrsns_share_data->misc.clock_plan[i].pll_plllock_cmp1   = pClockPlan[i].sDSFConfig.pll_plllock_cmp1;
     ddrsns_share_data->misc.clock_plan[i].pll_plllock_cmp2	  = pClockPlan[i].sDSFConfig.pll_plllock_cmp2;
     ddrsns_share_data->misc.clock_plan[i].pll_vco_count1	  = pClockPlan[i].sDSFConfig.pll_vco_count1;
     ddrsns_share_data->misc.clock_plan[i].pll_vco_count2     = pClockPlan[i].sDSFConfig.pll_vco_count2;
     ddrsns_share_data->misc.clock_plan[i].pll_pll_lpf2_postdiv= pClockPlan[i].sDSFConfig.pll_pll_lpf2_postdiv;
     ddrsns_share_data->misc.clock_plan[i].pll_kvco_code  	  = pClockPlan[i].sDSFConfig.pll_kvco_code;
  }

  ddrsns_share_data->misc.misc_cfg[0] = 1;

  DALSYS_Free(pClockPlan);
}

/* ============================================================================
**  Function : ddr_pre_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called before ddr is initialized. It will take care of any
*   pre initialization workarounds.
*
*   @details
*   This function is called before ddr is initialized. It will take care of any
*   pre initialization workarounds.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
boolean ddr_pre_init()
{
  return TRUE;
}


/* ============================================================================
**  Function : ddr_post_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @details
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
boolean ddr_post_init()
{
  char ddr_info[60];
  
  snprintf (ddr_info, 60, "DDR ID, Rank 0, Rank 1, 0x%x, 0x%x, 0x%x", 
            ddrsns_share_data->cdt_params[0].common.manufacture_name,
            ddrsns_share_data->ddr_size_info.ddr0_cs0_mb,
            ddrsns_share_data->ddr_size_info.ddr0_cs1_mb);
  boot_log_message(ddr_info);  
  return TRUE;
}

/* ============================================================================
**  Function : ddr_pre_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before clock switching occures.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be stalled and new timing parameter is loaded into shadow.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk   -   [IN] the current clock speed
*   @param new_clk    -  [IN] the clock speed we are switching to
*   @param new_clk    -  [IN] interface to switch clock for
*
*   @par Dependencies
*
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_pre_clock_switch(uint32 curr_clk, uint32 new_clk , SDRAM_INTERFACE interface_name)
{
     HAL_DDR_Pre_Clock_Switch(ddrsns_share_data, (DDR_CHANNEL)interface_name, curr_clk, new_clk);
} /* ddr_pre_clock_switch */

/* ============================================================================
**  Function : ddr_post_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after clock switching occurs.
*   The function will configure the ddr such that no data loss will occur
*
*   @details
*   DDR will be unstalled.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*
*   @param curr_clk          -  [IN] the current clock speed
*   @param new_clk           -  [IN] the clock speed we are switching to
*   @param interface_name    -  [IN] interface to switch clock for
*
*   @par Dependencies
*   This code has to be on IRAM because ddr is unavailable during clock switching
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

void ddr_post_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
     HAL_DDR_Post_Clock_Switch(ddrsns_share_data, (DDR_CHANNEL)interface_name , curr_clk, new_clk);
} /* ddr_post_clock_switch */

/* ============================================================================
**  Function : ddr_pre_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_pre_vddmx_switch(uint32 vddmx_microvolts)
{
  /* Stepping Down in VDDCX voltage */
  ddr_status.vddmx_voltage = vddmx_microvolts;
} /* ddr_pre_vddmx_switch */

/* ============================================================================
**  Function : ddr_post_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddmx_switch(uint32 vddmx_microvolts)
{

} /* ddr_post_vddmx_switch */

/* ============================================================================
**  Function : ddr_pre_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before vddcx switch.
*
*   @param settings - contains the VDDCX voltage level we just switched to
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/
void ddr_pre_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_pre_vddcx_switch */

/* ============================================================================
**  Function : ddr_post_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after VDDCX is switched
*
*   @param none
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_post_vddcx_switch */

/* ============================================================================
**  Function : ddr_pre_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right before XO shutdown. Puts DDR into self refresh mode and
*   disables CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_post_xo_shutdown
*/

void ddr_pre_xo_shutdown(uint32 clk_speed)
{
  ddr_enter_self_refresh_all(clk_speed);
} /* ddr_pre_xo_shutdown */

/* ============================================================================
**  Function : ddr_post_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right after XO wakeup. Takes DDR out of self refresh mode and enables
*   CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_pre_xo_shutdown
*/

void ddr_post_xo_shutdown(uint32 clk_speed)
{
  ddr_exit_self_refresh_all(clk_speed);

} /* ddr_post_xo_shutdown */

/* ============================================================================
**  Function : ddr_check_partition
** ============================================================================
*/
/*!
*   @brief
*   A helper function for ddr_dmm_partition to check the given interface and partition
*   whether we can put into dpd or full\partial self refresh
*
*   @details
*   Given the retention and active state, put the corresponding ddr interface
*   into self refresh, or deep power down when possible
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param interface_name   -  [IN] the interface to check for
*   @param chip_sel         -  [IN] the chip select on the interface to check for
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*   @param num_partitions   -  [IN] the number of partitions on this interface
*
*   @par Dependencies
*   None
*
*   @par Side Effects
*   None
*
*   @retval
*   None
*
*   @sa None
*/
void ddr_check_partition(DDR_CHANNEL interface_name, SDRAM_CHIPSELECT chip_sel, uint32 retention_state, uint32 active_state, uint8 num_partitions)
{

} /* ddr_check_partition */

/* ============================================================================
**  Function : ddr_dmm_partition
** ============================================================================
*/
/*!
*   @brief
*   Given the retention and active state, put the corresponding ddr interface
*   into self refresh, or deep power down when possible
*
*   @details
*   Given the retention and active state, put the corresponding ddr interface
*   into self refresh, or deep power down when possible
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*
*   @par Dependencies
*   Caller of this API has to take care not to put ddr interface that is in use
*   into self refresh or deep power down.
*
*   @par Side Effects
*   None
*
*   @retval
*  None
*
*   @sa None
*/

void ddr_dmm_partition(uint32 retention_state, uint32 active_state)
{
}
/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
   return TRUE;
//   return FALSE;
} /* ddr_params_is_training_required */


/* ============================================================================
**  Function : ddr_is_training_required
** ============================================================================
*/
/*!
*   @brief
*   This function will parse the crc hash and determine if training is required
*   based on serial number, mx\cx\rbcpr hash
*
*   @details
*   This function will parse the crc hash and determine if training is required
*   based on serial number, mx\cx\rbcpr hash
*
*   @param
*   None
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  boolean - Training required/Not required
*
*   @sa None
*/
boolean ddr_is_training_required(void)
{
  uint32 serial_number;

  /* cx/mx/vdda hash variables */
  uint32 combined_checksum = cpr_cx_mx_settings_checksum();

  /* compare checksum for training data in our partition with DDR_STRUCT to see if training is required */
  serial_number = BOOT_HWIO_IN(QFPROM_RAW_PTE_ROW1_LSB , 0);

  ddr_printf ( DDR_NORMAL, "DDR: The serial number is %d", serial_number);

  /* combine serial number with voltage checksum for a new seed */
  combined_checksum = serial_number ^ combined_checksum;

  ddr_printf (DDR_NORMAL, "DDR: Checksum on flash is %d", ddrsns_share_data->flash_params.checksum);
  ddr_printf (DDR_NORMAL, "DDR: Recomputed checksum is %d", ddr_get_training_checksum(combined_checksum));


  if (ddrsns_share_data->flash_params.checksum  != ddr_get_training_checksum(combined_checksum))
  {
    ddr_printf (DDR_NORMAL, "DDR: Training is required");

    return TRUE;
  }

  ddr_printf (DDR_NORMAL, "DDR: Training is not required");
  return FALSE;
} /* ddr_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid.
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
  return FALSE;
} /* ddr_params_is_training_on_rpm_required */


/* =============================================================================
**  Function :  ddr_do_phy_training_DDI
** =============================================================================
*/
/**
*   @brief
*   Indicates that PHY training needs to be done in SBL1 from DDI.
*
*   @param  None
*
*   @retval  
*   @retval 
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/


void ddr_do_phy_training_ddi( void )
{

    if (ddrsns_share_data->ddi_buf[0] == 0xE12A3456)
    {
        HAL_DDR_Boot_Training_Restore(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH);
        //ddrsns_share_data->ddi_buf[0] = 0x0;

    }
    if (ddrsns_share_data->ddi_buf[0] == 0xDD3DEB06 && ddrsns_share_data->ddi_buf[1] == 0xb5b5b5b5)
    {
        
    
        applyMRVal();
    
		if( ddrsns_share_data->ddi_buf[2] != 0 || ddrsns_share_data->ddi_buf[14] != 0 ){
              update_soc_drive_strength();
        }
         InitDDITransport();
         clearFlashParams();
         
         HAL_DDR_Boot_Training_Init(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH );
         clearDDIBuff();
         initMRStruct();

         // flush the region containing the share data sctructure
         dcache_flush_region((void *)ddrsns_share_data, sizeof(*ddrsns_share_data));

         sendTrainingRsp();
		 busywait(10000000);
		 deInitTransport();		 
         boot_hw_reset(BOOT_WARM_RESET_TYPE);
         
    }
}
/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   Indicates that PHY training needs to be done in SBL1.
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_do_phy_training( void )
{

   railway_corner_vote(ebi_tubo_pin, RAILWAY_NO_REQUEST);
  #if ONE_TIME_TRAINING
    /* Boot training */
    if((ddr_is_training_required() == TRUE)||
          (HAL_DDR_Boot_Training_Restore(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH) == FALSE))
  #endif
    {
       /* training data corrupted, memclr training data to force a retraining */
      memset(&ddrsns_share_data->flash_params, 0x0, sizeof(struct ddr_params_partition));

      return TRUE;

    }
  #if ONE_TIME_TRAINING
    else if(HAL_DDR_Boot_Training_Restore(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH)== TRUE){
      ddr_printf (DDR_NORMAL, "DDR: DDR training mode check failed");
      Clock_SetBIMCSpeed(ddr_post_init_freq);
      
      /* Periodic training */ 
      #if PERIODIC_TRAINING
        ddr_printf (DDR_NORMAL, "DDR: Start of HAL DDR Periodic Training");
        HAL_DDR_Periodic_Training(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH, DDR_TRAINING_MODE_INIT);
        ddr_printf (DDR_NORMAL, "DDR: End of HAL DDR Periodic Training");
      #endif

    return FALSE;
    }

    else {
       return FALSE;
    }
  #endif
  
} /* ddr_do_phy_training */

/* =============================================================================
**  Function : ddr_post_training
** =============================================================================
*/
/**
*   @brief
*   This function is called after ddr training. It will take care of all post
*   training activities such as computing checksum over training data.
*
*   @param  None
*
*   @retval  TRUE If one time training is enabled
*   @retval  FALSE If one time training is disabled
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_post_training(void)
{
  uint32 serial_number;
  uint32 combined_checksum = cpr_cx_mx_settings_checksum();
  
  Clock_SetBIMCSpeed(ddr_post_init_freq);
  
  /* Periodic training */ 
	#if PERIODIC_TRAINING
		ddr_printf (DDR_NORMAL, "DDR: Start of HAL DDR Periodic Training");
		HAL_DDR_Periodic_Training(ddrsns_share_data, DDR_CH_BOTH, DDR_CS_BOTH, DDR_TRAINING_MODE_INIT);
		ddr_printf (DDR_NORMAL, "DDR: End of HAL DDR Periodic Training");
	#endif

  serial_number = BOOT_HWIO_IN(QFPROM_RAW_PTE_ROW1_LSB , 0);

  /* combine serial number with voltage checksum for a new seed */
  combined_checksum = serial_number ^ combined_checksum;

  /* update training data checksum */
  ddr_set_training_checksum(combined_checksum);

  /* update training data and log size */
  ddrsns_share_data->flash_params.training_data_size = sizeof(ddrsns_share_data->flash_params.training_data);
  ddrsns_share_data->flash_params.training_log_size = ddr_external_get_training_log_size();

  #if ONE_TIME_TRAINING
    return TRUE;
  #else
    return FALSE;
  #endif
}


/* =============================================================================
**  Function : ddr_remapper
** =============================================================================
*/
/**
*   @brief
*   configures ddr to be remapped to lower 32bit addresss
*
*   @param void
*
*   @retval  void
*
*   @dependencies
*   ddr initialization has already finished
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_remapper(void)
{
  icbcfg_remap_info_type remap_info;

  uint64 remap_start_addr = 0;
  uint64 remap_size = ddr_system_size.sdram0_cs0 +
                      ddr_system_size.sdram0_cs1 +
                      ddr_system_size.sdram1_cs0 +
                      ddr_system_size.sdram1_cs1;

  uint64 remap_addr = 0;
  /*
    remapping configurations
    
                      SCMO CS0	SCMO CS1	BIMC Remapper CS0	BIMC Remapper CS1
    3GB Single Rank	    0x0	      N/A           0x80000000	    N/A
    3GB Dual Rank	    0x0	      0x80000000	0x100000000	    N/A
    4GB Dual Rank	    0x0	      0x100000000	0x80000000	    N/A
    4GB Single Rank 	0x0	      N/A	        0x80000000	    N/A
    6GB Dual Rank 	    0x0	      0x100000000	0x40000000	    N/A
  */
  
  if(remap_size == 3072)
  {
    /* remap CS0 to 0x80000000 only if single rank
       if dual rank map it to 0x100000000 for performance
    */
    if(ddr_system_size.sdram0_cs1 != 0)
    {
      remap_size /= 2;
      remap_addr = 0x100000000;
    }
    else
    {
      remap_addr = 0x80000000;
    }
  }  
  else if(remap_size == 4096)
  {
    /* remap CS0 to 0x80000000 */
    if(ddr_system_size.sdram0_cs1 != 0)
    {
      remap_size /= 2;
    }
    remap_addr = 0x80000000;
  }    
  else // 6GB configuration
  {
    if(ddr_system_size.sdram0_cs1 != 0)
    {
      remap_size /= 2;
    }
    remap_addr = 0x40000000;    
  }
  
  /* ram size is in byte */
  remap_size *= 1024 * 1024;
    
  /* CS0 starts at 0x0 always */
  remap_start_addr = 0x0;
  remap_info.src_addr = remap_start_addr;
  remap_info.size = remap_size;
  remap_info.dest_addr = remap_addr; /* start of 512MB address */
  remap_info.interleaved = ICBCFG_REMAP_INTERLEAVE_DEFAULT;
  remap_info.deinterleave = FALSE;

  /* update internal variable to track new mapping */
  ddr_system_size.sdram0_cs0_addr = remap_info.dest_addr;
  ddr_system_size.sdram1_cs0_addr = remap_info.dest_addr;

  ICB_Remap("/dev/icbcfg/boot", &ddr_physical_size, &remap_info);    

  /* update ddr struct remapping address */
  ddrsns_share_data->ddr_size_info.ddr0_cs0_remapped_addr = remap_info.dest_addr;
  ddrsns_share_data->ddr_size_info.ddr1_cs0_remapped_addr = remap_info.dest_addr;
  ddrsns_share_data->ddr_size_info.ddr0_cs1_remapped_addr = ddrsns_share_data->ddr_size_info.ddr0_cs1_addr;
  ddrsns_share_data->ddr_size_info.ddr1_cs1_remapped_addr = ddrsns_share_data->ddr_size_info.ddr1_cs1_addr;
  
} /* ddr_configure_lpae */

