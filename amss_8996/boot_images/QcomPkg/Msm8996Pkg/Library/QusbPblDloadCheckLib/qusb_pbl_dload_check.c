/*=======================================================================*//**
 * @file        qusb_al_pbl_dload_check.c
 * @author:     kameya
 * @date        13-Feb-2015
 *
 * @brief       QUSB implementation to check forced PBL DLOAD mode 
 *              when D+ is grounded.
 *
 * @details     This file contains the implementation of the APIs to be used by
 *              the boot code to detect PBL DLOAD entry when D+ is grounded.
 *
 * @note        
 *
 *              Copyright 2015 QUALCOMM Technologies Incorporated.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 * 
*//*========================================================================*/

// ===========================================================================
// 
//                            EDIT HISTORY FOR FILE
//   This section contains comments describing changes made to the module.
//   Notice that changes are listed in reverse chronological order.
// 
// 
// when          who     what, where, why
// ----------   -----    ----------------------------------------------------------
// 2013-06-19    shreyasr Added changes for MDM9x35
// 2012-04-14    tnk     Added flat file changes 
// 2010-04-14    yli     MSM8960 branch
// 2008-09-03   amirs    First Draft
// 
// ===========================================================================

//----------------------------------------------------------------------------
// Include Files
//----------------------------------------------------------------------------
#include "qusb_pbl_dload_check.h"
#include "qusb_pbl_dload_dependency_8996.h"
#include "busywait.h"  // busywait APIs
#include "ClockBoot.h"
#include TARGET_DEF_H             // For SHARED_IMEM_BASE
#include "com_dtypes.h"           // common defines - basic types as uint32 etc
#include "HALhwio.h"              // For OUTF and INF definitions
#include "pmic.h"
#include "pm_smbchg_usb_chgpth.h"
#include "pm_smbchg_misc.h"        //charger detection api#include "pm_smbchg_usb_chgpth.h"  //vbus detection api
#include "qusb_common.h"
#include "qusb_dci_api.h"

#include "qusb_cookie.h"
#include "fs_hotplug.h"
#include "CoreString.h"
#include "boot_logger.h"

//----------------------------------------------------------------------------
// Preprocessor Definitions and Constants
//----------------------------------------------------------------------------
#define QUSB_DLOAD_LOG_SIZE                       (8)
#define QUSB_DLOAD_UART_INT_STR_SIZE              (32)    //MAX int digits can appear on UART log

//defines for enabling the 1.5k pull up 
#define QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_EN     (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN   (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_BMSK)
#define QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS   (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_SHFT)
#define QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE          (0x0 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE_SHFT)
#define QUSB2PHY_PORT_UTMI_CTRL1_OP_NON_MODE      (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE_SHFT)

#define QUSB_LINESTATE_CHECK_DELAY                (0x5)
#define QUSB_LINESTATE_CHECK_RETRY_CNT            (10000)  //50msec is the total wait time to deal with a bad cable

// Assume a 300 ohm SMB charger resistor on D+/D- which corresponds to HSTX_TRIM offset of value 2
// Subtract QUSB_HSTX_TRIM_OFFSET to make amplitude overshoot by 400mV to account for longer cables and hubs.

#define QUSB_HSTX_TRIM_OFFSET                     (2)
#define PMI_8994_INDEX                            (1)
#define QUSB_CHARGER_DETECT_RETRY_CNT             (100)

// This needs to match the same definition in qusb_dci_[target].c
#define QUSB_DLOAD_INFO_ADDR_IN_IMEM              (SHARED_IMEM_USB_BASE)

// Hotplug API status for success
#define QUSB_FS_HOTPLUG_SUCCESS                   (0)

// Definition for UFS array
// [0] - Length (including header), [1] - Type, [2] - unicode string
#define QUSB_UFS_SER_STR_LEN_INDEX                (0)
#define QUSB_UFS_SER_STR_TYPE_INDEX               (1)
#define QUSB_UFS_SER_STR_UNICODE_INDEX            (2)

// 
#define QUSB_DLOAD_HS_PHY_PLL_BMSK                (0x1)
#define QUSB_DLOAD_HS_PHY_PLL_MAX_CNT             (20000)


//----------------------------------------------------------------------------
// Type Declarations
//----------------------------------------------------------------------------
typedef enum
{
  QUSB_DLOAD_PHY_REG_ARRAY_PROCESS__FAIL_LOG,
  QUSB_DLOAD_PHY_REG_ARRAY_PROCESS__START_LOG,
  QUSB_DLOAD_PHY_REG_ARRAY_PROCESS____END_LOG,
  QUSB_DLOAD_SELECT_UTMI_CLK_LOG,
  QUSB_DLOAD_SKIP_PBL_DLOAD_LOG,
  QUSB_DLOAD_ENTER_PBL_DLOAD_LOG,
  QUSB_DLOAD_PMIC_API_FAILURE_LOG,

  // Skip PBL dload due to error condition log
  QUSB_DLOAD_SKIP__CHGR_PORT_DET_FAIL_LOG,
  QUSB_DLOAD_SKIP__CHGR_PORT_NOT_SDP_LOG,
  QUSB_DLOAD_SKIP__CHGR_PORT_VBUS_LOW_LOG,
} qusb_dload_log_enum;

typedef struct 
{
  uint16 idx;
  qusb_dload_log_enum id_0;
  uint32 param;
} qusb_dload_log_entry;

typedef struct
{
  uint16 seq_num; 
  uint16 dload_buffer_index; 
  qusb_dload_log_entry dload_log[QUSB_DLOAD_LOG_SIZE];
} qusb_dload_log_type;

//----------------------------------------------------------------------------
// Global Data Definitions
//----------------------------------------------------------------------------
#ifdef QUSB_ENABLE_LOGGING
qusb_dload_log_type qusb_dload_log_buffer;
uint8 qusb_dload_log_buf_size    =   QUSB_DLOAD_LOG_SIZE;
#endif


//----------------------------------------------------------------------------
// Static Variable Definitions
//----------------------------------------------------------------------------
static boolean qusb_forced_download_feature_supported = TRUE;

//============================================================================
// QUSB High-Speed PHY Configuration Array
//============================================================================

#define QUSB_PBL_DLOAD_HS_PHY_CFG_ARRAY_ENTRY_CNT      (12)

// It is recommended to set the CSR bit "r_power_down = 1'b1", program any 
// hardware tuning (such as mentioned in #1 above)/config bits/etc., then 
// release "r_power_down = 1'b0" once again, in the QUSB2PHY_POWER_DOWN 
// register.

static const uint32 qusb_pbl_dload_hs_phy_cfg_address[QUSB_PBL_DLOAD_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE1_ADDR,      // 0  : (0xF8)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_ADDR,      // 1  : (0xB3)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE3_ADDR,      // 2  : (0x83)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE4_ADDR,      // 3  : (0xC0)
  //------------------------------------------------------------------------------------------
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TUNE_ADDR,        // 4  : (0x30)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL1_ADDR,   // 5  : (0x79)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL2_ADDR,   // 6  : (0x21)
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TEST2_ADDR,      // 7  : (0x14)
  //------------------------------------------------------------------------------------------  
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_PWR_CTL_ADDR,     // 8  : (0x00) 
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,// 9  : (0x9F)
  QUSB_HWIO_ADDR_EMPTY,                                       // 10 : (0x00)
  QUSB_HWIO_ADDR_EMPTY,                                       // 11 : (0x00)
};

static const uint8 qusb_pbl_dload_hs_phy_cfg_value[QUSB_PBL_DLOAD_HS_PHY_CFG_ARRAY_ENTRY_CNT] = 
{
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE1_ADDR,      0  */ (0xF8),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_ADDR,      1  */ (0xB3),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE3_ADDR,      2  */ (0x83),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE4_ADDR,      3  */ (0xC0),
  //------------------------------------------------------------------------------------------
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TUNE_ADDR,        4  */ (0x30),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL1_ADDR,   5  */ (0x79),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_USER_CTL2_ADDR,   6  */ (0x21),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TEST2_ADDR,      7  */ (0x14),
  //------------------------------------------------------------------------------------------  
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_PWR_CTL_ADDR,     8  */ (0x00),
  /* HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_AUTOPGM_CTL1_ADDR,9  */ (0x9F),
  /* QUSB_HWIO_ADDR_EMPTY,                                       10 */ (0x00),
  /* QUSB_HWIO_ADDR_EMPTY,                                       11 */ (0x00), 
};

static const uint32  qusb_crc32_table[256] = {
  0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F,
  0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
  0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2,
  0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
  0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
  0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
  0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C,
  0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
  0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423,
  0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
  0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x01DB7106,
  0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
  0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D,
  0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
  0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
  0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
  0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7,
  0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
  0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA,
  0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
  0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81,
  0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
  0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84,
  0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
  0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
  0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
  0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E,
  0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
  0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55,
  0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
  0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28,
  0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
  0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F,
  0x72076785, 0x05005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
  0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
  0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
  0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69,
  0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
  0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC,
  0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
  0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693,
  0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
  0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};


//----------------------------------------------------------------------------
// Static Function Declarations and Definitions
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Externalized Function Definitions
//----------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Core Initialization APIs
// ------------------------------------------------------------------------------------------------

#ifdef QUSB_UART_LOG

// UART logging
static void qusb_dload_uart_log(char *message)
{
  boot_log_message(message);
}

// UART logging with optional value parameter
void qusb_dload_uart_w_param_log(char *message, uint32 value)
{ 
  int len;
  char uart_int_log_buffer[QUSB_DLOAD_UART_INT_STR_SIZE];
  len = snprintf(uart_int_log_buffer, QUSB_DLOAD_UART_INT_STR_SIZE, "0x%x", value);
  if((len < 0) || (len >= QUSB_DLOAD_UART_INT_STR_SIZE))
  {
    boot_log_message_optional_data(message, "str_overflow");
  }
  else
  {
    boot_log_message_optional_data(message, uart_int_log_buffer);
  }
}


// UART logging with optional string parameter
static void qusb_dload_uart_w_str_log(char *message, char *opt_message)
{
  boot_log_message_optional_data(message, opt_message);
}

#else

#define qusb_dload_uart_log(msg)
#define qusb_dload_uart_w_param_log(msg, val)
#define qusb_dload_uart_w_str_log(msg, val)


#endif

// Minimum logging function to track errors
static void qusb_dload_log(qusb_dload_log_enum id_0, uint8 offset, uint32 param)
{
#ifdef QUSB_ENABLE_LOGGING
  qusb_dload_log_buffer.dload_log[qusb_dload_log_buffer.dload_buffer_index].idx =  qusb_dload_log_buffer.seq_num;
  qusb_dload_log_buffer.seq_num = (qusb_dload_log_buffer.seq_num + 1);
  qusb_dload_log_buffer.dload_log[qusb_dload_log_buffer.dload_buffer_index].id_0 = (qusb_dload_log_enum)(id_0 + offset);
  qusb_dload_log_buffer.dload_log[qusb_dload_log_buffer.dload_buffer_index].param = param;
  qusb_dload_log_buffer.dload_buffer_index = (qusb_dload_log_buffer.dload_buffer_index + 1) % QUSB_DLOAD_LOG_SIZE;
#endif
  return;
}

//============================================================================
/**
* @function  qusb_pbl_dload_delay_ms
*
* @brief Perform delay in miliseconds.
*
* @Note : The USB Timers can not be used before the core is initialized.
*
* @param microseconds
*
* @return none
*
*/
//============================================================================
static void qusb_pbl_dload_delay_ms(uint32 msecs)
{
  int counter;
  for (counter = 0; counter < msecs; counter++)
  {
    busywait(1000); // 1 ms
  }
}

//============================================================================
/**
* @function  qusb_pbl_dload_delay_us
*
* @brief Perform delay in microseconds.
*
* @Note : The USB Timers can not be used before the core is initialized.
*
* @param microseconds
*
* @return none
*
*/
//============================================================================
static void qusb_pbl_dload_delay_us(uint32 usecs)
{
  busywait(usecs);
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_phy_reg_array_process
 * 
 * @brief   This function reads from array which define list of hwio writes for
 *          USB PHY
 * 
 * @param   address_array   - array holding address of HW register
 *          value_array     - array holding values to be written to HW register
 *          start_index     - starting index for array processing
 *          array_entry_cnt - number of entries in the array
 * 
 * @return  None
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_phy_reg_array_process
(
  const uint32 *address_array, 
  const uint8  *value_array, 
  uint32        start_index, 
  uint32        array_entry_cnt
)
{
  uint32 index = start_index;

  if ( (NULL == address_array)
      || (NULL == value_array)
      || (0 == array_entry_cnt) )
  {
    qusb_dload_log(QUSB_DLOAD_PHY_REG_ARRAY_PROCESS__FAIL_LOG, 0, (uint32)address_array);
  }
  else
  {
    for (; index < array_entry_cnt; index++)
    {
      if (QUSB_HWIO_ADDR_END == address_array[index])
      {
        break;
      }

      if (QUSB_HWIO_ADDR_EMPTY == address_array[index])
      {
        continue;
      }

      out_dword(address_array[index], value_array[index]);
    }
  }
  qusb_dload_log(QUSB_DLOAD_PHY_REG_ARRAY_PROCESS__START_LOG, 0, start_index);
  qusb_dload_log(QUSB_DLOAD_PHY_REG_ARRAY_PROCESS____END_LOG, 0, index);
}

// ===========================================================================
/**
 * @function    qusb_hs_phy_refclk_enable
 * 
 * @brief   This function will be used to enable / disable HS PHY reference clock.
 * 
 * @param  TRUE or FALSE depending on enable or disable.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
void qusb_dload_hs_phy_refclk_enable(boolean enable)
{
  HWIO_GCC_RX1_USB2_CLKREF_EN_OUTM(HWIO_GCC_RX1_USB2_CLKREF_EN_RX1_USB2_ENABLE_BMSK,
    enable << HWIO_GCC_RX1_USB2_CLKREF_EN_RX1_USB2_ENABLE_SHFT);

  HWIO_GCC_RX1_USB2_CLKREF_EN_OUTM(HWIO_GCC_RX1_USB2_CLKREF_EN_SW_RXTAP1_EN_BMSK,
    enable << HWIO_GCC_RX1_USB2_CLKREF_EN_SW_RXTAP1_EN_SHFT);
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_enable_vbus_valid
 * 
 * @brief       API used to enable VBUS using s/w control
 * 
 * @param   qusb_max_speed_required_t - Maximum Speed required to be configured
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_enable_vbus_valid(qusb_max_speed_required_t speed_required)
{

  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x1 << HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
  HWIO_HS_PHY_CTRL_OUTM(HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x1 << HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));

  /* If we want to enable SUPER SPEED then also set LANE PWR PRESENT bit */
  if(speed_required == QUSB_MAX_SPEED_SUPER)
  {
    HWIO_SS_PHY_CTRL_OUTM(HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_BMSK, (0x1 << HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_SHFT));
  }

  return;
}


// ===========================================================================
/**
 * @function    qusb_pbl_dload_hs_phy_update_hstx_trim
 * 
 * @brief   This function will update TUNE2 HSTX_TRIM register bits if feature is enabled.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_hs_phy_update_hstx_trim(void)
{
  uint8 hstx_trim_val = 
    (HWIO_QFPROM_CORR_CALIB_ROW12_MSB_INM(HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_BMSK) 
    >> HWIO_QFPROM_CORR_CALIB_ROW12_MSB_PORT0_HSTX_TRIM_LSB_SHFT);

  if(hstx_trim_val)
  {
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_BMSK,
        hstx_trim_val << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_TUNE2_UTM_HSTX_TRIM_SHFT);
  }
}


// ===========================================================================
/**
 * @function    qusb_pbl_dload_hs_phy_init
 * 
 * @brief   API used to initialize the High Speed PHY.
 * 
 * @param   None.
 * 
 * @return  TRUE if PHY initializes successfully, 
 *          FALSE if it fails to initialize.
 * 
 */
// ===========================================================================
static boolean qusb_pbl_dload_hs_phy_init(void)
{
  uint8 pll_status = 0;
  uint32 pll_status_cnt = 0;

    /* Keep the PHY in power down mode */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

    qusb_pbl_dload_phy_reg_array_process(
          qusb_pbl_dload_hs_phy_cfg_address,
          qusb_pbl_dload_hs_phy_cfg_value,
          0,
          QUSB_PBL_DLOAD_HS_PHY_CFG_ARRAY_ENTRY_CNT);

    // Overwrite HSTX TRIM calibration value
    qusb_pbl_dload_hs_phy_update_hstx_trim();

    /* Bring Out  the PHY from power down mode */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x0 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

    /* The delay is required for PHY PLL capacitor to get charged / ramp up */
    qusb_pbl_dload_delay_us(150);

    /* Enable/Select sourcing single ended reference clock to PHY */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_TEST_OUT(0x80);

    /* Wait maximum of ~1ms for PLL to lock */
    for(pll_status_cnt = 0; (FALSE == pll_status) && (pll_status_cnt < QUSB_DLOAD_HS_PHY_PLL_MAX_CNT); pll_status_cnt++)
    {
      qusb_pbl_dload_delay_us(10);
      
      pll_status = (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_INM
                    (HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_PLL_STATUS_BMSK)
                    >> HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PLL_STATUS_PLL_STATUS_SHFT);
    
      pll_status &= QUSB_DLOAD_HS_PHY_PLL_BMSK;
    }
    
    if(FALSE == pll_status)
    {
      qusb_dload_uart_w_param_log("usb: PLL lock failed", pll_status_cnt);
      return FALSE;
    }
    qusb_dload_uart_w_param_log("usb: PLL lock success", pll_status_cnt);
    return TRUE;
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_select_utmi_clk
 * 
 * @brief   This is used for configuring the core to UTMI clock instead of pipe
 *          clock.  This needs to be called when there is no SS USB PHY.
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_select_utmi_clk(void)
{
  qusb_dload_log(QUSB_DLOAD_SELECT_UTMI_CLK_LOG, 0, 0);
  
  // If operating without SS PHY, follow this sequence to disable 
  // pipe clock requirement
  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
                      0x1 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);

  qusb_pbl_dload_delay_us(100);

  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_SEL_BMSK,
                      0x1 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_SEL_SHFT);

  qusb_pbl_dload_delay_us(100);
  
  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE3_PHYSTATUS_SW_BMSK,
    0x1 << HWIO_GENERAL_CFG_PIPE3_PHYSTATUS_SW_SHFT);

  qusb_pbl_dload_delay_us(100);

  HWIO_GENERAL_CFG_OUTM(HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_BMSK,
                      0x0 << HWIO_GENERAL_CFG_PIPE_UTMI_CLK_DIS_SHFT);
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_hs_phy_gcc_reset
 * 
 * @brief   API used for resetting High Speed QUSB2 PHY using GCC control
 *  
 * @details This API is used for resetting High Speed QUSB2 PHY using GCC control
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_hs_phy_gcc_reset(void)
{
  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,
    (0x1 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));

  qusb_pbl_dload_delay_us(100);

  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,
    (0x0 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));
}

// ===========================================================================
/**
 * @function    qusb_pbl_dload_usb30_gcc_reset
 * 
 * @brief   API used for resetting the Link and PHYs using GCC control
 *  
 * @details This API is used for resetting the Link and PHYs using clock control 
 * 
 * @param   None.
 * 
 * @return  None.
 * 
 */
// ===========================================================================
static void qusb_pbl_dload_usb30_gcc_reset(void)
{
  // Reset SNPS Link controller 
  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  qusb_pbl_dload_delay_us(100);

  HWIO_GCC_USB_30_BCR_OUTM(HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT));

  // Reset QUSB (USB 2.0) and QMP (USB 3.0) PHYs 
  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x1 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x1 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));

  qusb_pbl_dload_delay_us(100);

  HWIO_GCC_USB3PHY_PHY_BCR_OUTM(HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK, (0x0 << HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_USB3_PHY_BCR_OUTM(HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT));
  HWIO_GCC_QUSB2PHY_PRIM_BCR_OUTM(HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_BMSK,(0x0 << HWIO_GCC_QUSB2PHY_PRIM_BCR_BLK_ARES_SHFT));

  return; 
}


// ===========================================================================
/**
 * @function    qusb_pbl_dload_vbus_detect
 * 
 * @brief   This function will check Vbus is present using PMIC API.
 * 
 * @param   None
 * 
 * @return  TRUE: Vbus is detected.
 *          FALSE: Vbus is not detected.
 * 
 */
// ===========================================================================

boolean qusb_pbl_dload_vbus_detect(void)
{
  boolean vbus_state = 0;
  pm_err_flag_type pm_status;

  // Read Vbus state using PMIC CHGPTH API.
  pm_status = pm_smbchg_usb_chgpth_irq_status(PMI_8994_INDEX, PM_SMBCHG_USB_CHGPTH_USBIN_SRC_DET, 
              PM_IRQ_STATUS_RT, &vbus_state);
    
  if(PM_ERR_FLAG__SUCCESS == pm_status)
  {
    return vbus_state;
  }
  else
  {
    qusb_dload_log(QUSB_DLOAD_PMIC_API_FAILURE_LOG, 0, pm_status);
    return FALSE;
  }
}

// ===========================================================================
/**
 * @function    qusb_dload_get_chgr_str
 * 
 * @brief   This function converts charger type to string.
 * 
 * @param  PMIC charger type
 * 
 * @return  Charger type string 
 * 
 */
// ===========================================================================
char* qusb_dload_get_chgr_str(pm_smbchg_misc_src_detect_type usb_charger_type)
{
  switch (usb_charger_type)
  {
    case PM_SMBCHG_MISC_SRC_DETECT_CDP:                 return "CDP";
    case PM_SMBCHG_MISC_SRC_DETECT_DCP:                 return "DCP";
    case PM_SMBCHG_MISC_SRC_DETECT_OTHER_CHARGING_PORT: return "OTHER_CHARGING_PORT";
    case PM_SMBCHG_MISC_SRC_DETECT_SDP:                 return "SDP";
    case PM_SMBCHG_MISC_SRC_DETECT_FMB_UART_ON:         return "FMB_UART_ON";
    case PM_SMBCHG_MISC_SRC_DETECT_FMB_UART_OFF:        return "FMB_UART_OFF";
    case PM_SMBCHG_MISC_SRC_DETECT_FMB_USB_ON:          return "FMB_USB_ON";
    case PM_SMBCHG_MISC_SRC_DETECT_FMB_USB_OFF:         return "FMB_USB_OFF";
    case PM_SMBCHG_MISC_SRC_DETECT_INVALID:             return "INVALID";
    default:                                            return "UNKNOWN";
  }
}

// ===========================================================================
/**
 * @function    qusb_dload_is_chgr_sdp
 * 
 * @brief   This function check if vbus is present and will read charger type with timeout.
 * 
 * @param   None
 * 
 * @return  TRUE: if charger type is SDP.
 *              FALSE: if charger type is not SDP or PMIC charger detection fails or 500ms timeout occurs.
 * 
 */
// ===========================================================================
boolean qusb_dload_is_chgr_sdp(void)
{
  uint32 charger_det_retry_cnt = QUSB_CHARGER_DETECT_RETRY_CNT;
  pm_smbchg_misc_src_detect_type usb_charger_type = PM_SMBCHG_MISC_SRC_DETECT_INVALID;
  pm_err_flag_type pm_err;
  boolean is_vbus_low = TRUE;

  // Check charger type with timeout 
  while (--charger_det_retry_cnt)
  {
    // Read Vbus state using PMIC CHGPTH API.
    pm_err = pm_smbchg_usb_chgpth_irq_status(PMI_8994_INDEX, 
                                             PM_SMBCHG_USB_CHGPTH_USBIN_UV, 
                                             PM_IRQ_STATUS_RT, &is_vbus_low);
    if (PM_ERR_FLAG__SUCCESS != pm_err)
    {
      qusb_dload_uart_log("usb: fedl, vbus_det_err");
      qusb_dload_log(QUSB_DLOAD_SKIP__CHGR_PORT_DET_FAIL_LOG, 0, pm_err);
      return FALSE;
    }
    
    if (is_vbus_low)
    {
      qusb_dload_uart_log("usb: fedl, vbus_low");
      qusb_dload_log(QUSB_DLOAD_SKIP__CHGR_PORT_VBUS_LOW_LOG, 0, is_vbus_low);
      return FALSE;
    }

    pm_err = pm_smbchg_misc_chgr_port_detected(PMI_8994_INDEX, &usb_charger_type);
    if (PM_ERR_FLAG__SUCCESS != pm_err)
    {
      qusb_dload_uart_log("usb: fedl, chgr_type_det_err");
      qusb_dload_log(QUSB_DLOAD_SKIP__CHGR_PORT_DET_FAIL_LOG, 0, pm_err);
      return FALSE;
    }

    if (PM_SMBCHG_MISC_SRC_DETECT_INVALID != usb_charger_type)
    {
      break;
    }

    qusb_pbl_dload_delay_ms(5);
  }

  qusb_dload_uart_w_str_log("usb: chgr", qusb_dload_get_chgr_str(usb_charger_type));

  if (PM_SMBCHG_MISC_SRC_DETECT_INVALID == usb_charger_type)
  {
    qusb_dload_uart_log("usb: fedl, chgr_det_timeout");
    qusb_dload_log(QUSB_DLOAD_SKIP__CHGR_PORT_DET_FAIL_LOG, 0, 0);
    return FALSE;
  }

  if (PM_SMBCHG_MISC_SRC_DETECT_SDP != usb_charger_type)
  {
    qusb_dload_log(QUSB_DLOAD_SKIP__CHGR_PORT_NOT_SDP_LOG, 0, usb_charger_type);
    return FALSE;
  }

  return TRUE;
}


// ===========================================================================
/* @function  qusb_calc_crc32
 *
 * @ brief    Calculates CRC32 (Cyclic redundancy check) over the number 
 *            of bytes specified.
 *
 * @param data   Buffer to use to calculate crc32
 * @param nbytes Calculate CRC32 over this number of bytes.
 * @param seed   CRC32 seed. Used to chain CRC32 calculations.
 *
 * @return CRC32 over the buffer
 */
// ===========================================================================
static uint32
qusb_calc_crc32 (const uint8 *data, uint32 nbytes, uint32 seed)
{
  uint32  crc = 0;
  uint32  pos;
  const uint8   *pbyte;

  if ((data != NULL) && (nbytes != 0))
  {
    crc = seed;
    for (pos = 0, pbyte = data; pos < nbytes; pos++, pbyte++)
    {
      crc = (crc >> 8) ^ qusb_crc32_table[(uint8) crc ^ *pbyte];
    }
  }
  return  crc;
}


// ===========================================================================
/**
 * @function    qusb_dload_update_usb_serial_string_and_cookie
 * 
 * @brief   This function will update dload cookie to UFS/EMMC serial number 
 *          if dload cookie is not set.
 * 
 * @param   None
 * 
 * @return  None 
 * 
 */
// ===========================================================================
static void qusb_dload_update_usb_serial_string_and_cookie(void)
{
  int     char_written_length;
  uint32  serial_num            = 0;
  uint16  length                = 0;
  boolean is_ufs_available      = TRUE;

  // Read the serial string from UFS or EMMC
  struct hotplug_device *hdev;
  struct hotplug_device_info dev_info;
  struct hotplug_guid sbl1_partition_id = 
        /*{DEA0BA2C-CBDD-4805-B4F9-F428251C3E98}*/
        { 0xDEA0BA2C, 0xCBDD, 0x4805, { 0xB4, 0xF9, 0xF4, 0x28, 0x25, 0x1C, 0x3E, 0x98 } };

  qusb_dload_info_type *dload_info_ptr = 
    (qusb_dload_info_type*)QUSB_DLOAD_INFO_ADDR_IN_IMEM;
  
  /* We verify that the data is valid according to the product_id validity */
  if (SERIAL_NUMBER_MAGIC_NUMBER == dload_info_ptr->pid_serial_num_cookie.magic_2)
  {
    // Since serial string is updated correctly already, we can skip the logic
    // Suppress UART logging for normal boot
    // qusb_dload_uart_log("usb: ser_str_cookie_valid");
    return;
  }

  //======================== 
  // Try UFS first 
  //========================

  hdev = hotplug_open_device_by_gpt_partition_type(HOTPLUG_TYPE_UFS, HOTPLUG_ITER_EMBEDDED_DEVICES_ONLY, &sbl1_partition_id);
  if (NULL != hdev)
  {
    if (QUSB_FS_HOTPLUG_SUCCESS != hotplug_dev_get_device_info(hdev, &dev_info))
    {
      qusb_dload_uart_log("usb: ufs_info_fail");
      return;
    }
    
    if (2 >= dev_info.product_name[QUSB_UFS_SER_STR_LEN_INDEX])
    {
      qusb_dload_uart_log("usb: ufs_ser_strlen <= 2");
      return;      
    }

    // Execute proprietary algorithm to convert UFS Product Serial (UNICODE) 
    // to uint32 hex using customer crc32
    length = (dev_info.product_name[QUSB_UFS_SER_STR_LEN_INDEX] - 2) / 2;

    serial_num = qusb_calc_crc32(
      &dev_info.product_name[QUSB_UFS_SER_STR_UNICODE_INDEX], 
      length, 
      0xFFFFFFFF);
  }
  else 
  {
    is_ufs_available = FALSE;

    //========================
    // Try EMMC next 
    //========================

    hdev = hotplug_open_device_by_gpt_partition_type(HOTPLUG_TYPE_MMC, HOTPLUG_ITER_EMBEDDED_DEVICES_ONLY, &sbl1_partition_id);
    if (NULL == hdev)
    {
      qusb_dload_uart_log("usb: emmc_open_fail");
      return;
    }

    if(QUSB_FS_HOTPLUG_SUCCESS != hotplug_dev_get_device_info(hdev, &dev_info))
    {
      qusb_dload_uart_log("usb: emmc_info_fail");
      return;
    }

    serial_num = dev_info.prod_serial_num;
  }

  if (serial_num == 0)
  {
    qusb_dload_uart_log("usb: serial_num_0");
    return;
  }
  
  char_written_length = core_snprintf(dload_info_ptr->serial_number, QUSB_SERIAL_NUMBER_LEN, "%x", serial_num);
  
  if ((char_written_length < 0) || (char_written_length >= QUSB_SERIAL_NUMBER_LEN))
  {
    qusb_dload_uart_log("usb: ser_str_conv_fail");
    return;
  }

  if (is_ufs_available)
  {
    qusb_dload_uart_w_str_log("usb: UFS Serial", dload_info_ptr->serial_number);
  }
  else
  {
    qusb_dload_uart_w_str_log("usb: EMMC Serial", dload_info_ptr->serial_number);
  }

  // Updated the USB Serial cookie
  dload_info_ptr->pid_serial_num_cookie.magic_2 = SERIAL_NUMBER_MAGIC_NUMBER;
}


// ===========================================================================
/**
 * @function    qusb_pbl_dload_check
 * 
 * @brief   This function will check if D+ is grounded. And if it is connected
 *          to ground, then we go into EDL.
 * 
 * @param   None
 * 
 * @return  TRUE: if it is connected to GND
 *          FALSE: if it is not connected to GND. 
 * 
 */
// ===========================================================================
boolean qusb_pbl_dload_check(void)
{
  uint32 linestate = 0x0; 
  int32 retries = 0x0;
  uint32 enable_fs_xvr = 0x0;

  // In case the serial string is not updated to IMEM, update to serial string
  qusb_dload_update_usb_serial_string_and_cookie();

  
  if(FALSE == qusb_forced_download_feature_supported)
  {
    return FALSE; 
  }

  // Continue only if Vbus is present and charger type is SDP
  if(FALSE == qusb_dload_is_chgr_sdp())
  {
    return FALSE; 
  }

  // It is not guaranteed that the SNPS core is always power collapsed **/ 
  // Put it to power collapse and then bring out of power collapse
  Clock_Usb30EnableSWCollapse(TRUE); // Put it to power collapse explicitly 

  /** Bring the controller out of Power Collapse**/
  Clock_Usb30EnableSWCollapse(FALSE);

  //Disable the clocks 
  Clock_DisableUSB(); 

  // Disable PHY reference clock to ensure QUSB2PHY PLL lock.
  qusb_dload_hs_phy_refclk_enable(FALSE);

  //hard reset the USB Link and PHY using the GCC reset
  qusb_pbl_dload_usb30_gcc_reset();

  //enable the clocks now 
  Clock_InitUSB();

  // Write 0x11 to AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG so that 
  // writes and reads to/from the PHY use one wait state.
  // This is essential to operate at nominal frequency with lower CX rail voltages.
  HWIO_PERIPH_SS_AHB2PHY_TOP_CFG_OUT(0x11);

  //Enable HS Only clock
  qusb_pbl_dload_select_utmi_clk();

  //Initialize the HS_PHY
  if (FALSE == qusb_pbl_dload_hs_phy_init())
  {    
    /* Keep the PHY in power down mode */
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
        HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
        (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

    return FALSE;
  }

  //Disable the D+ Pull down 
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_BMSK, 
    (0x0 <<HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_SHFT ));

  //Enable the D- Pull down 
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_BMSK, 
    (0x1 <<HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_SHFT ));

  //Select the FS transceiver and enable the 1.5k pull-up. 
  enable_fs_xvr = (QUSB2PHY_PORT_UTMI_CTRL1_SUSPEND_N_EN | QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN | 
                   QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS | QUSB2PHY_PORT_UTMI_CTRL1_OP_MODE); 

  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OUT(enable_fs_xvr);

  // Disable CLAMP to get valid linestate output
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_CLAMP_N_EN_BMSK,
    (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_CLAMP_N_EN_SHFT));
  
  // Enable UTMI_TEST_MUX_SEL
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
    0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);

  do
  {
    //wait for 5 microseconds 
    qusb_pbl_dload_delay_us(QUSB_LINESTATE_CHECK_DELAY); 
    
    //read the linestate
    linestate = HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_STATUS_INM(
                    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_STATUS_LINESTATE_BMSK);
    
    //if D+ is still high, then the device should not enter the EDL Mode 
    if(linestate != 0x0)
    {
      break; 
    }
    
    retries++; 
   
  }while (retries < QUSB_LINESTATE_CHECK_RETRY_CNT);

  //disable the 1.5K Pull up resistor
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OUT(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_BMSK);

  // Disable UTMI_TEST_MUX_SEL
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
    0x0 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);

  qusb_pbl_dload_delay_us(10);

  /* Keep the PHY in power down mode */
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
      HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
      (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

  if(linestate != 0x0)
  {
    qusb_dload_log(QUSB_DLOAD_SKIP_PBL_DLOAD_LOG, 0, linestate);
    return FALSE;
  }

  //D+ is connected to the GND
  qusb_dload_log(QUSB_DLOAD_ENTER_PBL_DLOAD_LOG, 0, linestate);
  return TRUE;
}

void qusb_hs_phy_nondrive_mode_set(void)
{
  uint32 enable_fs_xvr = 0x0;

  qusb_dload_uart_log("usb: hs_phy_nondrive_start");
 
  qusb_dload_hs_phy_refclk_enable(FALSE);
 
  qusb_pbl_dload_hs_phy_gcc_reset();

  Clock_InitUSB();

  // Write 0x11 to AHB2PHY bridge CSR PERIPH_SS_AHB2PHY_TOP_CFG so that 
  // writes and reads to/from the PHY use one wait state.
  // This is essential to operate at nominal frequency with lower CX rail voltages.
  HWIO_PERIPH_SS_AHB2PHY_TOP_CFG_OUT(0x11);

  qusb_pbl_dload_delay_us(10);

  if (qusb_pbl_dload_hs_phy_init())
  {
    // Disable the D+ Pull down 
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
      HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_BMSK,
      (0x0 <<HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DPPULLDOWN_SHFT));

    // Disable the D- Pull down 
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
      HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_BMSK,
      (0x0 <<HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_DMPULLDOWN_SHFT));

    // Select the FS transceiver and put into non-drive mode
    // Putting phy to suspend as SUSPEND_N = 0 (power on default)
    enable_fs_xvr = (QUSB2PHY_PORT_UTMI_CTRL1_TERM_SELECT_EN
                     | QUSB2PHY_PORT_UTMI_CTRL1_XCVR_SELECT_FS
                     | QUSB2PHY_PORT_UTMI_CTRL1_OP_NON_MODE);

    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL1_OUT(enable_fs_xvr);
   
    // Enable UTMI_TEST_MUX_SEL
    HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_OUTM(
      HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_BMSK,
      0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_UTMI_CTRL2_UTMI_TEST_MUX_SEL_SHFT);
  }
  
  qusb_pbl_dload_delay_us(10);

  /* Keep the PHY in power down mode */
  HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_OUTM(
      HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_BMSK,
      (0x1 << HWIO_PERIPH_SS_QUSB2PHY_PRIM_QUSB2PHY_PORT_POWERDOWN_POWER_DOWN_SHFT));

  qusb_pbl_dload_delay_us(10);

  Clock_DisableUSB();

  qusb_dload_uart_log("usb: hs_phy_nondrive_finish");
}
