// @file
//  This implements Page Parking Algorithm.
//  MpParkMailbox is relocated at runtime, so everything must remain relative.
//
//  Copyright (c) 2014, Qualcomm TechnologiesInc. All rights reserved.
//
//=============================================================================
//                              EDIT HISTORY
//
//
// when       who     what, where, why
// --------   ---     -----------------------------------------------------------
// 07/08/14   rli     Initial version
//
//=============================================================================

// startup parameter block
.set OFST_PROCID_LO_DWORD,  0x0000
.set OFST_PROCID_HI_DWORD,  0x0004
.set OFST_JUMP_LO_DWORD,    0x0008


ASM_PFX(MpParkMailbox):

        // save entry point address to r10
        ADR     x10, .                   // get current value of PC
        LSR     x10, x10, #8             // strip off lower 12 bits
        LSL     x10, x10, #8             // entry point

        // save page boundary to r4
        ADR     x4, .                    // get current value of PC
        LSR     x4, x4, #12              // strip off lower 12 bits
        LSL     x4, x4, #12              // page boundary

        // isolate processor id
        LDR     x0, [x4, #OFST_PROCID_LO_DWORD]
        AND     x0, x0, #0xFF             // isolate proc ID

        MRS     x2, MPIDR_EL1
        AND     x3, x2, #0xFF00 
        LSR     x3, x3,  #0x6
        AND     x2, x2, #0xFF 
        ADD     x2, x2, x3                //absolute cpu id
        CMP     x0, x2
        BNE     _wait                     // if not our CPU id, goto WFI
            
        // check for Zero Jump
        LDR     x0, [x4, #OFST_JUMP_LO_DWORD]
        CMP     x0, #0
        BEQ     _wait                     // if Jump address == 0, goto WFI

        // copy jump address to r8-r9
        MOV     x8, x0
        
        // store mailbox address in R0
        MOV     x0, x4

        // acknowledge
        MOV     x1, #0
        STR     x1, [x4, #OFST_JUMP_LO_DWORD]

        // jump to specified address
        BR      x8

_wait:
        DSB      sy                       // data syncronization barrier
        WFI                               // wait for interrupt

        BR       x10

VectorSpin:
        B        VectorSpin

ASM_PFX(MpParkMailbox_END):
        GCC_ASM_EXPORT(MpParkMailbox)
        GCC_ASM_EXPORT(MpParkMailbox_END)
