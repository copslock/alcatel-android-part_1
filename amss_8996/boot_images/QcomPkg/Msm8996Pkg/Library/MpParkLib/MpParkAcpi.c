/** @file
  Communicates number of cores via AcpiPlatform Protocol

  Copyright (c) 2014, Qualcomm Technologies,Inc. All rights reserved.
   
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
09/22/14   rli     Added reading of ARM_BOOT_ARCH value from ACPI FADT table
                    and updating PSCI ACPI global
07/08/14   rli     Updated to support AARCH64
06/13/14   rli     Initial version.

=============================================================================*/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiLib.h>
#include <Library/PcdLib.h>
#include <Library/QcomBaseLib.h>
#include <Library/UefiCfgLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/EFIAcpiPlatform.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/QcomLib.h>
#include <Include/IndustryStandard/Acpi.h>

extern EFI_GUID gEfiPlatPartitionTypeGuid;
#define PLATNAME            L"ACPI\\madt.acp"
#define PLATNAME_OCTA       L"ACPI\\madtocta.acp"
#define PLATNAME_QUAD       L"ACPI\\madtquad.acp"

extern UINT32  NumCpus;
extern EFI_GUID gQcomTokenSpaceGuid;
extern BOOLEAN  PsciCompliant;

typedef struct
{
  UINT8   AddressSpaceID;
  UINT8   RegisterBitWidth;
  UINT8   RegisterBitOffset;
  UINT8   AccessSize;
  UINT64  Address;
} __attribute__ ((packed))  ACPI_GAS;

typedef struct {
  EFI_ACPI_DESCRIPTION_HEADER   Header;
  UINT32       FIRMWARE_CTRL;
  UINT32       DSDT;
  UINT8        Reserved0;
  UINT8        Preferred_PM_Profile;
  UINT16       SCI_INT;
  UINT32       SMI_CMD;
  UINT8        ACPI_ENABLE;
  UINT8        ACPI_DISABLE;
  UINT8        S4BIOS_REQ;
  UINT8        PSTATE_CNT;
  UINT32       PM1a_EVT_BLK;
  UINT32       PM1b_EVT_BLK;
  UINT32       PM1a_CNT_Blk;
  UINT32       PM1b_CNT_Blk;
  UINT32       PM2_CNT_BLk;
  UINT32       PM_TMR_BLK;
  UINT32       GPE0_BLK;
  UINT32       GPE1_BLK;
  UINT8        PM1_EVT_LEN;
  UINT8        PM1_CNT_LEN;
  UINT8        PM2_CNT_LEN;
  UINT8        PM_TMR_LEN;
  UINT8        GPE0_BLK_LEN;
  UINT8        GPE1_BLK_LEN;
  UINT8        GPE1_BASE;
  UINT8        CST_CNT;
  UINT16       P_LVL2_LAT;
  UINT16       P_LVL3_LAT;
  UINT16       FLUSH_SIZE;
  UINT16       FLUSH_STRIDE;
  UINT8        DUTY_OFFSET;
  UINT8        DUTY_WIDTH;
  UINT8        DAY_ALRM;
  UINT8        MON_ALRM;
  UINT8        CENTURY;
  UINT16       IAPC_BOOT_ARCH;
  UINT8        Reserved1;
  UINT32       Flags;
  ACPI_GAS     RESET_REG;
  UINT8        RESET_VALUE;
  UINT8        Reserved2[3];
  UINT64       X_FIRMWARE_CTRL;
  UINT64       X_DSDT;
  ACPI_GAS     X_PM1a_EVT_BLK;
  ACPI_GAS     X_PM1b_EVT_BLK;
  ACPI_GAS     X_PM1a_CNT_BLK;
  ACPI_GAS     X_PM1b_CNT_BLK;
  ACPI_GAS     X_PM2_CNT_BLK;
  ACPI_GAS     X_PM_TMR_BLK;
  ACPI_GAS     X_GPE0_BLK;
  ACPI_GAS     X_GPE1_BLK;
}  __attribute__ ((packed))  ACPI_FACP_TABLE;

STATIC UINT8    ArmBootArch = 0;


/**
 FacpTableSetCallback is called by AcpiPlatformDxe to update the Facp
 table. This function initializes the Facp control area, updates
 the Facp table to point to the control area, and returns
 the updated Facp table in the AcpiTable parameter.
 
 @param[in, out] AcpiTable: The Facp table to be updated.
 @param[in, out] AcpiTableSize:  The size of the Facp table.

 @retval EFI_SUCCESS:      The function executed successfully.
 @retval EFI_INVALID_PARAMETER: AcpiTable is NULL or AcpiTableSize is 
                          NULL or AcpiTableSize is less than the size of 
                          ACPI_Facp_TABLE
*/
static EFI_STATUS EFIAPI FacpTableSetCallback (
  IN OUT VOID         **AcpiTable,
  IN OUT UINTN         *AcpiTableSize
)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (AcpiTable == NULL || *AcpiTable == NULL 
    || AcpiTableSize == NULL || *AcpiTableSize < sizeof(ACPI_FACP_TABLE))
  {
    DEBUG((EFI_D_ERROR, " FADT table was not found.\r\n"));
    return EFI_INVALID_PARAMETER;
  }

  ArmBootArch = ((ACPI_FACP_TABLE*) *AcpiTable)->Reserved2[0];
  PsciCompliant = ArmBootArch & 0x1;
  DEBUG((EFI_D_INFO, "PSCI_COMPLIANT = %d (ARM_BOOT_ARCH = 0x%x)\r\n", PsciCompliant, ArmBootArch));
  
  return Status;
}

/**
 InitEnableOctacoreVariable
 This function checks for madocta/madtquad ACPI files at PLAT partition
 and initializes EnableOctacore UEFI variable accordingly. 
 
 @retval EFI_SUCCESS:          The function executed successfully.
 @retval EFI_COMPROMISED_DATA: Unknown layout
*/
EFI_STATUS
InitEnableOctacoreVariable()
{
  EFI_STATUS Status;
  UINT32 FileSizeMadt, FileSizeMadtOctaQuad;
  BOOLEAN Flag = FALSE;
  UINTN   VarSize;
  
  Status = GetFileSize(PLATNAME, NULL, &gEfiPlatPartitionTypeGuid, TRUE, NULL, &FileSizeMadt);
  if (EFI_ERROR(Status))
  {
    DEBUG(( EFI_D_ERROR, "Failure to read %s!\n", PLATNAME));
  }
  else
  {
    Status = GetFileSize(PLATNAME_OCTA, NULL, &gEfiPlatPartitionTypeGuid, TRUE, NULL, &FileSizeMadtOctaQuad);
    if (EFI_ERROR(Status))
    {
       Flag = TRUE;
       Status = GetFileSize(PLATNAME_QUAD, NULL, &gEfiPlatPartitionTypeGuid, TRUE, NULL, &FileSizeMadtOctaQuad);
    }

    if (EFI_ERROR(Status) || FileSizeMadtOctaQuad == FileSizeMadt)
    {
       Status = EFI_COMPROMISED_DATA;
    }
  }
  
  if (!EFI_ERROR(Status))
  {
     VarSize = sizeof(Flag);
     Status = gRT->SetVariable (L"EnableOctacore", &gQcomTokenSpaceGuid,
                             EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE,
                             VarSize, &Flag);
  }
  return Status;
}

/**
 MpParkAmlSetNcpu is called by AcpiPlatformDxe to update the NCPU
 dsdt variable.
 
 @param[in, out] AmlVariableBuffer:     The pointer to NCPU variable to be updated.
 @param[in, out] AmlVariableBufferSize: The size of the NCPU variable buffer.

 @retval EFI_SUCCESS:              The function executed successfully.
 @retval EFI_INVALID_PARAMETER:    AmlVariableBuffer is NULL
*/
static EFI_STATUS EFIAPI MpParkAmlSetNcpu (
  IN OUT VOID         **AmlVariableBuffer,
  IN OUT UINTN          AmlVariableBufferSize
)
{
  BOOLEAN       Flag = FALSE;
  UINTN         VarSize;
  EFI_STATUS    Status = EFI_INVALID_PARAMETER;

  if(AmlVariableBuffer == NULL)
  {
    DEBUG((EFI_D_WARN,"AML: NCPU Buffer is empty\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  
  Status = GetConfigValue ("NumCpus", &NumCpus);
  if ((Status != EFI_SUCCESS) || (NumCpus == 0))
  {
    DEBUG ((EFI_D_WARN, "NumCpus not found in uefiplat.cfg. Defaulting to 1.\r\n"));
    // Default to 1
    NumCpus = 1;
  }
  VarSize = sizeof(Flag);
  Status = gRT->GetVariable (L"EnableOctacore", &gQcomTokenSpaceGuid,
                             NULL, &VarSize, &Flag);
  if ((Status == EFI_SUCCESS) && Flag )
  {
    NumCpus = 8;
    DEBUG ((EFI_D_WARN, "Octacore mode enabled by BDS menu option. NumCpus = %d.\r\n", NumCpus));
  }
  if(AmlVariableBufferSize)
  {
  ((UINT8*)(*AmlVariableBuffer))[AmlVariableBufferSize-2] = '0' + NumCpus;
  }
  return EFI_SUCCESS;
}

/**
 MpParkAmlSetPsci is called by AcpiPlatformDxe to update the NCPU
 dsdt variable.
 
 @param[in, out] AmlVariableBuffer:     The pointer to PSCI  variable to be updated.
 @param[in, out] AmlVariableBufferSize: The size of the PSCI variable buffer.

 @retval EFI_SUCCESS:            The function executed successfully.
 @retval EFI_INVALID_PARAMETER:  AmlVariableBuffer is NULL or AmlVariableBufferSize  <= 4
*/
static EFI_STATUS EFIAPI MpParkAmlSetPsci (
  IN OUT VOID         **AmlVariableBuffer,
  IN OUT UINTN          AmlVariableBufferSize
)
{
  if(AmlVariableBuffer == NULL)
  {
    DEBUG((EFI_D_WARN,"AML: PSCI Buffer is empty\r\n"));
    return EFI_INVALID_PARAMETER;
  }
  if(AmlVariableBufferSize  > 4)
  {
    //UINT8 to decimal ascii: 
    ((UINT8*)(*AmlVariableBuffer))[AmlVariableBufferSize-4] = '0' + (ArmBootArch/100);
    ((UINT8*)(*AmlVariableBuffer))[AmlVariableBufferSize-3] = '0' + (ArmBootArch%100)/10;
    ((UINT8*)(*AmlVariableBuffer))[AmlVariableBufferSize-2] = '0' + (ArmBootArch%10);
  }
  else
  {
    DEBUG((EFI_D_WARN,"AML: PSCI invalid BufferSize = %d, expected ('000'-'255' +'\\0')\r\n", AmlVariableBufferSize));
    return EFI_INVALID_PARAMETER;
  }
  return EFI_SUCCESS;
}

/**
 AcpiAmlRegister
 This function registers callbacks with AcpiDxe. 
 @retval EFI_SUCCESS:      The function executed successfully.
 @retval EFI_STATUS from AcpiPlatformDxe if it's not EFI_SUCCESS
*/
EFI_STATUS
AcpiAmlRegister()
{
  EFI_QCOM_ACPIPLATFORM_PROTOCOL *pEfiAcpiPlatformProtocol = NULL;
  EFI_STATUS Status;
  CHAR8 AmlVariableNcpu[AML_NAMESTRING_LENGTH] =  {'N','C','P','U'};
  CHAR8 AmlVariablePsci[AML_NAMESTRING_LENGTH] =  {'P','S','C','I'};

  // Init EnableOctacore Variable
  InitEnableOctacoreVariable();

  Status = gBS->LocateProtocol(&gQcomAcpiPlatformProtocolGuid,
                                NULL, (VOID**) &pEfiAcpiPlatformProtocol);
  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_WARN, "AML: Locate ACPI Protocol failed: %r\r\n", Status));
    return Status;
  }
    
  // Read FADT
  Status = pEfiAcpiPlatformProtocol->AcpiTableRegister(
            pEfiAcpiPlatformProtocol, EFI_ACPI_1_0_FIXED_ACPI_DESCRIPTION_TABLE_SIGNATURE,
            GETMODE_PREFIX, (ACPITableGetCallbackPtr) NULL, FacpTableSetCallback);
  
  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_WARN, " ACPI Table Register failed: %r\r\n", Status));
  }

  // Update NCPU
  Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol, AmlVariableNcpu, 
      GETMODE_POSTFIX, NULL, MpParkAmlSetNcpu);
  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_WARN, " ACPI NCPU Variable Register failed: %r\r\n", Status));
  }

  // Update PSCI
  Status = pEfiAcpiPlatformProtocol->AmlVariableRegister( pEfiAcpiPlatformProtocol, AmlVariablePsci, 
      GETMODE_POSTFIX, NULL, MpParkAmlSetPsci);
  if (EFI_ERROR(Status))
  {
    DEBUG((EFI_D_WARN, " ACPI PSCI Variablee Register failed: %r\r\n", Status));
  }

  return Status;
}
