/** @file MpParkLib.c
   
  This file implements specialized chip specific code for MpPark 
  protocol 

  Copyright (c) 2015, Qualcomm Technologies, Inc. All rights reserved.
  
**/

/*=============================================================================
                              EDIT HISTORY


 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 02/27/15   rli      Updated InitAPC1_L2 per HPG
 01/12/15   vpopuri  Branched from 8994 UEFI 3.1 and updated for 8996
=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include <Uefi.h>

#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MpParkLib.h>
#include <Library/IoLib.h>
#include <Protocol/EFIPmicVreg.h>
#include <Protocol/EFIChipInfo.h>

#define CHIPVERSION_V2_BASE                    0x00020000

EFIChipInfoVersionType ChipVersion;

extern BOOLEAN  PsciCompliant;

/*=========================================================================
      Macros
==========================================================================*/

/*  APC0_CPU1 Registers  */
#define APCS_APC0_CPU1_PWR_CTL                 0x09991000
#define APCS_APC0_CPU1_MAS_STS                 0x09991040           
#define APCS_APC0_CPU1_PGS_STS                 0x09991038

/*  APC1_CPU0 Registers */
#define APCS_APC1_CPU0_PWR_CTL                 0x099B1000
#define APCS_APC1_CPU0_MAS_STS                 0x099B1040
#define APCS_APC1_CPU0_PGS_STS                 0x099B1038

/*  APC1_CPU1 Registers */
#define APCS_APC1_CPU1_PWR_CTL                 0x099C1000
#define APCS_APC1_CPU1_MAS_STS                 0x099C1040
#define APCS_APC1_CPU1_PGS_STS                 0x099C1038

/*  APC1_L2 Registers   */
#define APCS_APC1_L2_PWR_CTL                   0x099D1000
#define APCS_APC1_L2_MAS_STS                   0x099D1038
#define APCS_APC1_L2_PGS_STS                   0x099D1030

EFI_STATUS    AcpiAmlRegister(void);
 
STATIC EFI_QCOM_PMIC_VREG_PROTOCOL *PMICVregp = NULL;

/*=========================================================================
      Functions
==========================================================================*/
void CPU_Start(UINTN cpu){

  UINT32 apc_cpu_pwr_ctrl_addr;
  UINT32 apc_cpu_mas_sts_addr;
  UINT32 apc_cpu_pgs_sts_addr;

  switch(cpu)
  {
  case 1:
      apc_cpu_pwr_ctrl_addr = APCS_APC0_CPU1_PWR_CTL;
      apc_cpu_mas_sts_addr = APCS_APC0_CPU1_MAS_STS;
      apc_cpu_pgs_sts_addr = APCS_APC0_CPU1_PGS_STS;
    break;
  case 2:
      apc_cpu_pwr_ctrl_addr = APCS_APC1_CPU0_PWR_CTL;
      apc_cpu_mas_sts_addr = APCS_APC1_CPU0_MAS_STS;
      apc_cpu_pgs_sts_addr = APCS_APC1_CPU0_PGS_STS;
    break;
  case 3:
      apc_cpu_pwr_ctrl_addr = APCS_APC1_CPU1_PWR_CTL;
      apc_cpu_mas_sts_addr = APCS_APC1_CPU1_MAS_STS;
      apc_cpu_pgs_sts_addr = APCS_APC1_CPU1_PGS_STS;
    break;
  default:
    return;
  }

    /* 
     * NOTE: This is for HMSS v1.0.0. Some steps relevant to HMSS 1.1.0 need
     * to be added.
     */

  /* Assert POR Reset, Clamp, Close CPU APM HS */
  MmioWrite32(apc_cpu_pwr_ctrl_addr, 0x00000055);

  /* Poll STEADY_ACTIVE bit. Wait till STS bit 14 is high */
  MmioWrite32(apc_cpu_mas_sts_addr, 0x00004000);

  /* Close CPU logic BHS */
  MmioWrite32(apc_cpu_pwr_ctrl_addr, 0x00000045);
  
  /* Poll STEADY_ACTIVE bit. Wait until STS bit 14 is high */
  MmioWrite32(apc_cpu_pgs_sts_addr, 0x00004000);
  /* wait 2 micro sec */
  gBS->Stall(2);

  /* De-assert Clamp; wait 8 XO cycles */
  MmioWrite32(apc_cpu_pwr_ctrl_addr, 0x00000004);
  gBS->Stall(1); // HPG: 0.416 us
  
  /* De-assert POR Reset */
  MmioWrite32(apc_cpu_pwr_ctrl_addr, 0x00000000);

  /* Assert PWRDUP */
  MmioWrite32(apc_cpu_pwr_ctrl_addr, 0x00000100);
}

void InitAPC1_L2(void)
{
  MmioWrite32(APCS_APC1_L2_PWR_CTL, 0x00000055);      // Assert POR Reset, Clamp, Close L2 APM HS
  MmioWrite32(APCS_APC1_L2_MAS_STS, 0x00004000);      // Poll STEADY_ACTIVE bit. Wait until STS bit 14 is high
  MmioWrite32(APCS_APC1_L2_PWR_CTL, 0x00000045);      // Close L2 logic BHS
  MmioWrite32(APCS_APC1_L2_PGS_STS, 0x00004000);      // Poll STEADY_ACTIVE bit. Wait until STS bit 14 is high
  gBS->Stall(2);  // Reset L2 - 32 XO cycle reset pulse
  MmioWrite32(APCS_APC1_L2_PWR_CTL, 0x00000004);      // De-assert Clamp, Wait 8 XO cycles
  gBS->Stall(1);  // wait for 8 XO cycles
  MmioWrite32(APCS_APC1_L2_PWR_CTL, 0x00000000);      // De-assert Reset
}

EFIChipInfoVersionType GetChipVersion()
{
    EFI_STATUS                Status;
    EFI_CHIPINFO_PROTOCOL     *pChipInfoProtocol = NULL;
    EFIChipInfoVersionType    Version = 0;

  /* Get Protocol handles */
  Status = gBS->LocateProtocol (&gEfiChipInfoProtocolGuid, NULL,
                               (VOID **) &pChipInfoProtocol);
  if (EFI_ERROR(Status))
  {
     DEBUG(( EFI_D_WARN, "LocateProtocol for ChipInfo failed, Status = (0x%x)\r\n", Status));
  }
  else
  {
      Status = pChipInfoProtocol->GetChipVersion(pChipInfoProtocol, &Version);
      if (EFI_ERROR(Status))
      {
         DEBUG(( EFI_D_WARN, " ChipInfoProtocol->GetChipVersion failed, Status = (0x%x)\r\n", Status));
      }
  }
  return Version;
}

EFI_STATUS
PowerupCPUCore (UINTN  CoreNum)
{
  EFI_STATUS Status = EFI_SUCCESS;

  if (CoreNum == 0)
  {
     /* Read Chip Version*/
     ChipVersion = GetChipVersion();

     /* Report Num of Cores to ACPI driver*/
      Status = AcpiAmlRegister();
  } 
  else
  if (CoreNum == 2)
  {
     if (PMICVregp == NULL)
     {
        Status = gBS->LocateProtocol (&gQcomPmicVregProtocolGuid,
                                  NULL, (VOID **) &PMICVregp);

        if (Status != EFI_SUCCESS)
        {
             DEBUG(( EFI_D_ERROR, "LocateProtocol for PmicVreg failed, Status = (0x%x)\r\n", Status));
        }
     }
     if (Status == EFI_SUCCESS)
     {
        if( !PsciCompliant ) //If PSCI is enabled don't init APC1, since it will be initialized by HLOS
        {
            InitAPC1_L2();
        }
      }
  } 

  return Status;
}
