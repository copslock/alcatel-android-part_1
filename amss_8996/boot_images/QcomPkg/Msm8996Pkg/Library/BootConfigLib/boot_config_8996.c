/** @file boot_config_8996.c

  BootConfigLib is used to get boot configuration information

  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential

**/

/*=============================================================================
                              EDIT HISTORY

when         who     what, where, why
----------   ---     -----------------------------------------------------------
2015-09-15   rm      Remove secboot_hwio.h
2015-04-30   as      Add SPI NOR functionality.
2015-03-31   rm      Initial version

=============================================================================*/

#include <Uefi.h>
#include "HALhwio.h"
#include "BootConfig.h"

#define HWIO_BOOT_CONFIG_ADDR                                         0x00076044
#define HWIO_BOOT_CONFIG_RMSK                                              0x7ff
#define HWIO_BOOT_CONFIG_IN          \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, HWIO_BOOT_CONFIG_RMSK)
#define HWIO_BOOT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, m)
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_BMSK                            0x400
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_SHFT                              0xa
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_BMSK                          0x300
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_SHFT                            0x8
#define HWIO_BOOT_CONFIG_MODEM_BOOT_FROM_ROM_BMSK                           0x80
#define HWIO_BOOT_CONFIG_MODEM_BOOT_FROM_ROM_SHFT                            0x7
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_BMSK                            0x40
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_SHFT                             0x6
#define HWIO_BOOT_CONFIG_FAST_BOOT_BMSK                                     0x3e
#define HWIO_BOOT_CONFIG_FAST_BOOT_SHFT                                      0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_BMSK                                        0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_SHFT                                        0x0
        
#define DEFAULT_BOOT_DEVICE         0x0
#define EMMC_SDC1_USB2_0            0x2
#define UFS_USB2_0                  0x4
#define SPI_NOR_QUP1                0x6
#define SPI_NOR_QUP2                0x7

/******************************************************************************
* FUNCTION      boot_from_ufs
*
* DESCRIPTION   This function returns if device boots from UFS
*
* PARAMETERS    NONE
*
* RETURN VALUE  TRUE if device boots from UFS
*
******************************************************************************/
BOOLEAN boot_from_ufs (void)
{
   UINT32 val = HWIO_INF(BOOT_CONFIG, FAST_BOOT);
   if (val == UFS_USB2_0)
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}  

/******************************************************************************
* FUNCTION      boot_from_emmc
*
* DESCRIPTION   This function returns if device boots from eMMC
*
* PARAMETERS    NONE
*
* RETURN VALUE  TRUE if device boots from eMMC
*
******************************************************************************/
BOOLEAN boot_from_emmc (VOID)
{
   UINT32 val = HWIO_INF(BOOT_CONFIG, FAST_BOOT);

   /* eMMC is default boot device*/ 
   if ((val == DEFAULT_BOOT_DEVICE) || (val == EMMC_SDC1_USB2_0))
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }       
}

/******************************************************************************
* FUNCTION      boot_from_spi_nor
*
* DESCRIPTION   This function returns if device boots from SPI NOR
*
* PARAMETERS    NONE
*
* RETURN VALUE  TRUE if device boots from SPI NOR
*
******************************************************************************/
BOOLEAN boot_from_spi_nor (VOID)
{
   UINT32 val = HWIO_INF(BOOT_CONFIG, FAST_BOOT);

   if ((val == SPI_NOR_QUP1) || (val == SPI_NOR_QUP2))
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }       
}

