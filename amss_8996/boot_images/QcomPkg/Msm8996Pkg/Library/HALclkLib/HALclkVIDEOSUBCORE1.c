/*
==============================================================================

FILE:         HALclkVIDEOSUBCORE1.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   VIDEOSUBCORE1 clocks.

   List of clock domains:
     - HAL_clk_mMMSSVIDEOSUBCORE1ClkDomain


   List of power domains:
     - HAL_clk_mMMSSVIDEOSUBCORE1PowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_3;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_3;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mVIDEOSUBCORE1ClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mVIDEOSUBCORE1ClkDomainClks[] =
{
  {
    /* .szClockName      = */ "video_subcore1_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VIDEO_SUBCORE1_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VIDEO_SUBCORE1_CLK
  },
};


/*
 * HAL_clk_mMMSSVIDEOSUBCORE1ClkDomain
 *
 * VIDEOSUBCORE1 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSVIDEOSUBCORE1ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_VIDEO_SUBCORE1_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mVIDEOSUBCORE1ClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mVIDEOSUBCORE1ClkDomainClks)/sizeof(HAL_clk_mVIDEOSUBCORE1ClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_3,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSVIDEOSUBCORE1PowerDomain
 *
 * VIDEO_SUBCORE1 power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSVIDEOSUBCORE1PowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_VIDEO_SUBCORE1",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_VIDEO_SUBCORE1_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

