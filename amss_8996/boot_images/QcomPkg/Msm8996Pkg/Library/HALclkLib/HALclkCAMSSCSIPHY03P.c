/*
==============================================================================

FILE:         HALclkCAMSSCSIPHY03P.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   CAMSSCSIPHY03P clocks.

   List of clock domains:
     - HAL_clk_mMMSSCSIPHY03PClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_1;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_1;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCSIPHY03PClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCSIPHY03PClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_csiphy0_3p_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSIPHY0_3P_CBCR), HWIO_OFFS(MMSS_CAMSS_CSIPHY0_3P_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSIPHY0_3P_CLK
  },
};


/*
 * HAL_clk_mMMSSCSIPHY03PClkDomain
 *
 * CSIPHY03P clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSIPHY03PClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_CSIPHY0_3P_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mCSIPHY03PClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCSIPHY03PClkDomainClks)/sizeof(HAL_clk_mCSIPHY03PClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};

