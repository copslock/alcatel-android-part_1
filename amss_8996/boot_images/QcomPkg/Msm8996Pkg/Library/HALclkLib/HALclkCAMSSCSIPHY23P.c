/*
==============================================================================

FILE:         HALclkCAMSSCSIPHY23P.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   CAMSSCSIPHY23P clocks.

   List of clock domains:
     - HAL_clk_mMMSSCSIPHY23PClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_1;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_1;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCSIPHY23PClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCSIPHY23PClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_csiphy2_3p_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSIPHY2_3P_CBCR), HWIO_OFFS(MMSS_CAMSS_CSIPHY2_3P_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSIPHY2_3P_CLK
  },
};


/*
 * HAL_clk_mMMSSCSIPHY23PClkDomain
 *
 * CSIPHY23P clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSIPHY23PClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_CSIPHY2_3P_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mCSIPHY23PClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCSIPHY23PClkDomainClks)/sizeof(HAL_clk_mCSIPHY23PClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_1,
  /* .pmNextClockDomain    = */ NULL
};

