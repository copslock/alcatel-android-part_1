/*
==============================================================================

FILE:         HALclkMMAGICBIMC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICBIMC clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mMMSSMMAGICBIMCPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mMMSSMMAGICBIMCPowerDomain
 *
 * MMAGIC_BIMC power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICBIMCPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_MMAGIC_BIMC",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_MMAGIC_BIMC_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

