/*
==============================================================================

FILE:         HALclkMain.c

DESCRIPTION:
  This file contains the main platform initialization code for the clock
  HAL on the apcs processor.


==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include "HALclkInternal.h"
#include "HALclkGeneric.h"
#include "Library/HALclkSharedLib/HALclkGenericPLL.h"
#include "HALhwio.h"
#include "HALclkHWIO.h"


/* ============================================================================
**    Prototypes
** ==========================================================================*/

void HAL_clk_PlatformInitSources(void);


/* ============================================================================
**    Externs
** ==========================================================================*/


extern void HAL_clk_PlatformInitGCCMain(void);
extern void HAL_clk_PlatformInitMMSSMain(void);
extern void HAL_clk_PlatformInitAPCSMain(void);


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_aInitFuncs
 *
 * Declare array of module initialization functions.
 */
static HAL_clk_InitFuncType HAL_clk_afInitFuncs[] =
{
  /*
   * Sources
   */
  HAL_clk_PlatformInitSources,

  /*
   * GCC
   */
  HAL_clk_PlatformInitGCCMain,

  /*
   * MMSS
   */
  HAL_clk_PlatformInitMMSSMain,

  /*
   * APCS
   */
  HAL_clk_PlatformInitAPCSMain,

  NULL
};



size_t HAL_clk_nHWIOBaseGCC;
size_t HAL_clk_nHWIOBaseMMSS;
size_t HAL_clk_nHWIOBaseHMSSQLL;
size_t HAL_clk_nHWIOBaseHMSS;
size_t HAL_clk_nHWIOBaseSecCtrl;
size_t HAL_clk_nHWIOBaseCoreTopCsr;


/*
 * HAL_clk_aHWIOBases
 *
 * Declare array of HWIO bases in use on this platform.
 */
static HAL_clk_HWIOBaseType HAL_clk_aHWIOBases[] =
{

  { CLK_CTL_BASE_PHYS,          CLK_CTL_BASE_SIZE,          &HAL_clk_nHWIOBaseGCC        },
  { MMSS_BASE_PHYS,             MMSS_BASE_SIZE,             &HAL_clk_nHWIOBaseMMSS       },
  { HMSS_QLL_BASE_PHYS,         HMSS_QLL_BASE_SIZE,         &HAL_clk_nHWIOBaseHMSSQLL    },
  { HMSS_BASE_PHYS,             HMSS_BASE_SIZE,             &HAL_clk_nHWIOBaseHMSS       },
  { SECURITY_CONTROL_BASE_PHYS, SECURITY_CONTROL_BASE_SIZE, &HAL_clk_nHWIOBaseSecCtrl    },
  { CORE_TOP_CSR_BASE_PHYS,     CORE_TOP_CSR_BASE_SIZE,     &HAL_clk_nHWIOBaseCoreTopCsr },

  { 0, 0, NULL }
};


/*
 * HAL_clk_Platform;
 * Platform data.
 */
HAL_clk_PlatformType HAL_clk_Platform =
{
  HAL_clk_afInitFuncs,
  HAL_clk_aHWIOBases
};


/*
 * GCC PLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextGCCPLL[] =
{

  {
    HWIO_OFFS(GCC_GPLL0_MODE),
    HAL_CLK_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0),
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(GCC_GPLL4_MODE),
    HAL_CLK_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL4),
    HAL_CLK_PLL_SPARK
  },

};


/*
 * MMSS PLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextMMSSPLL[] =
{

  {
    HWIO_OFFS(MMSS_MMPLL0_PLL_MODE),
    HAL_CLK_FMSK(MMSS_MMSS_PLL_VOTE_APCS, ENABLE_MMPLL0),
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL1_PLL_MODE),
    HAL_CLK_FMSK(MMSS_MMSS_PLL_VOTE_APCS, ENABLE_MMPLL1),
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL2_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL3_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL4_PLL_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },

  {
    HWIO_OFFS(MMSS_MMPLL5_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL8_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(MMSS_MMPLL9_PLL_MODE),
    {0},
    HAL_CLK_PLL_BRAMMO
  },

};


/*
 * HMSS PLL contexts
 */
static HAL_clk_PLLContextType HAL_clk_aPLLContextAPCSPLL[] =
{

  {
    HWIO_OFFS(APC0_QLL_PLL_MODE),
    {0},
    HAL_CLK_PLL_HUAYRA
  },

  {
    HWIO_OFFS(APC0_QLL_ALT_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(APC1_QLL_PLL_MODE),
    {0},
    HAL_CLK_PLL_HUAYRA
  },

  {
    HWIO_OFFS(APC1_QLL_ALT_PLL_MODE),
    {0},
    HAL_CLK_PLL_SPARK
  },

  {
    HWIO_OFFS(APCS_CBF_PLL_MODE),
    {0},
    HAL_CLK_PLL_HUAYRA_CBF
  },

};


/* ============================================================================
**    Functions
** ==========================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitSources
**
** ======================================================================== */

void HAL_clk_PlatformInitSources (void)
{
  /*
   * Install PLL handlers.
   */

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL0, &HAL_clk_aPLLContextGCCPLL[0], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_GPLL4, &HAL_clk_aPLLContextGCCPLL[1], CLK_CTL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL0, &HAL_clk_aPLLContextMMSSPLL[0], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL1, &HAL_clk_aPLLContextMMSSPLL[1], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL2, &HAL_clk_aPLLContextMMSSPLL[2], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL3, &HAL_clk_aPLLContextMMSSPLL[3], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL4, &HAL_clk_aPLLContextMMSSPLL[4], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL5, &HAL_clk_aPLLContextMMSSPLL[5], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL8, &HAL_clk_aPLLContextMMSSPLL[6], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_MMPLL9, &HAL_clk_aPLLContextMMSSPLL[7], MMSS_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_APCSPRIPLL0, &HAL_clk_aPLLContextAPCSPLL[0], HMSS_QLL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_APCSSECPLL0, &HAL_clk_aPLLContextAPCSPLL[1], HMSS_QLL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_APCSPRIPLL1, &HAL_clk_aPLLContextAPCSPLL[2], HMSS_QLL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_APCSSECPLL1, &HAL_clk_aPLLContextAPCSPLL[3], HMSS_QLL_BASE);

  HAL_clk_InstallPLL(
    HAL_CLK_SOURCE_APCSCBFPLL, &HAL_clk_aPLLContextAPCSPLL[4], HMSS_BASE);
} /* END HAL_clk_PlatformInitSources */



/* ===========================================================================
**  HAL_clk_Save
**
** ======================================================================== */

void HAL_clk_Save (void)
{
  /*
   * Nothing to save.
   */

} /* END HAL_clk_Save */


/* ===========================================================================
**  HAL_clk_Restore
**
** ======================================================================== */

void HAL_clk_Restore (void)
{
  /*
   * Nothing to restore.
   */

} /* END HAL_clk_Restore */


