/*
==============================================================================

FILE:         HALclkGPU.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   GPU clocks.

   List of clock domains:


   List of power domains:
     - HAL_clk_mMMSSGPUPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mMMSSGPUPowerDomain
 *
 * GPU power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSGPUPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_GPU",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_GPU_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

