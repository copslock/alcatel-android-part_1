/*
==============================================================================

FILE:         HALclkMMSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMSS clocks.

   List of clock domains:
     - HAL_clk_mGCCMMSSBIMCGFXClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl_2;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO_2;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mMMSSBIMCGFXClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMMSSBIMCGFXClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_bimc_gfx_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_BIMC_GFX_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_BIMC_GFX_CLK
  },
  {
    /* .szClockName      = */ "gcc_mmss_bimc_gfx_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MMSS_BIMC_GFX_CBCR), HWIO_OFFS(GCC_MMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MMSS_BIMC_GFX_CLK
  },
};


/*
 * HAL_clk_mGCCMMSSBIMCGFXClkDomain
 *
 * MMSSBIMCGFX clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCMMSSBIMCGFXClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_MMSS_BIMC_GFX_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMMSSBIMCGFXClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMMSSBIMCGFXClkDomainClks)/sizeof(HAL_clk_mMMSSBIMCGFXClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControlRO_2,
  /* .pmNextClockDomain    = */ NULL
};

