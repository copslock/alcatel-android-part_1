/*
==============================================================================

FILE:         HALclkGPUGX.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   GPUGX clocks.

   List of clock domains:
     - HAL_clk_mMMSSGFX3DClkDomain
     - HAL_clk_mMMSSRBBMTIMERClkDomain


   List of power domains:
     - HAL_clk_mMMSSGPUGXPowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl_4;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO_4;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mGFX3DClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mGFX3DClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gpu_gx_gfx3d_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_GPU_GX_GFX3D_CBCR), HWIO_OFFS(MMSS_GPU_GX_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_GPU_GX_GFX3D_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_debug_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_DEBUG_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ 0
  },
  {
    /* .szClockName      = */ "mmss_spdm_gfx3d_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_GFX3D_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_GFX3D_CLK
  },
};


/*
 * HAL_clk_mMMSSGFX3DClkDomain
 *
 * GFX3D clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSGFX3DClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_GFX3D_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mGFX3DClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mGFX3DClkDomainClks)/sizeof(HAL_clk_mGFX3DClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl_4,
  /* .pmNextClockDomain    = */ NULL
};


/*                           
 *  HAL_clk_mRBBMTIMERClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBBMTIMERClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gpu_gx_rbbmtimer_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_GPU_GX_RBBMTIMER_CBCR), HWIO_OFFS(MMSS_GPU_GX_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_GPU_GX_RBBMTIMER_CLK
  },
};


/*
 * HAL_clk_mMMSSRBBMTIMERClkDomain
 *
 * RBBMTIMER clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSRBBMTIMERClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_RBBMTIMER_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mRBBMTIMERClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBBMTIMERClkDomainClks)/sizeof(HAL_clk_mRBBMTIMERClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mMMSSGPUGXPowerDomain
 *
 * GPU_GX power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mMMSSGPUGXPowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_GPU_GX",
  /* .nGDSCRAddr              = */ HWIO_OFFS(MMSS_GPU_GX_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

