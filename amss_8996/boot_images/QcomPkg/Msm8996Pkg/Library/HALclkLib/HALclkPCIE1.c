/*
==============================================================================

FILE:         HALclkPCIE1.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the
   PCIE1 clocks.

   List of clock domains:
     - HAL_clk_mGCCPCIE1PIPEClkDomain


   List of power domains:
     - HAL_clk_mGCCPCIE1PowerDomain



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 *  HAL_clk_mPCIE1PIPEClkDomainClks
 *
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mPCIE1PIPEClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_pcie_1_pipe_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_PCIE_1_PIPE_CBCR), HWIO_OFFS(GCC_PCIE_1_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_PCIE_1_PIPE_CLK
  },
};


/*
 * HAL_clk_mGCCPCIE1PIPEClkDomain
 *
 * PCIE1PIPE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCPCIE1PIPEClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mPCIE1PIPEClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mPCIE1PIPEClkDomainClks)/sizeof(HAL_clk_mPCIE1PIPEClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*
 * HAL_clk_mGCCPCIE1PowerDomain
 *
 * PCIE_1 power domain.
 */
HAL_clk_PowerDomainDescType HAL_clk_mGCCPCIE1PowerDomain =
{
  /* .szPowerDomainName       = */ "VDD_PCIE_1",
  /* .nGDSCRAddr              = */ HWIO_OFFS(GCC_PCIE_1_GDSCR),
  /* .pmControl               = */ &HAL_clk_mGenericPowerDomainControl,
  /* .pmNextPowerDomain       = */ NULL
};

