/*
==============================================================================

FILE:         HALclkMMAGICAHB.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICAHB clocks.

   List of clock domains:
     - HAL_clk_mMMSSAHBClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mAHBClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAHBClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_AHB_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_cci_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CCI_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CCI_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CCI_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_cpp_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CPP_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CPP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CPP_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_cpp_vbif_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CPP_VBIF_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CPP_TOP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CPP_VBIF_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_csi0_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSI0_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CSI0_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSI0_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_csi1_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSI1_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CSI1_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSI1_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_csi2_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSI2_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CSI2_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSI2_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_csi3_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CSI3_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_CSI3_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CSI3_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_ispif_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_ISPIF_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_ISPIF_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_ISPIF_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_jpeg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_JPEG_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_JPEG_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_micro_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_MICRO_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_MICRO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_MICRO_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_top_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_TOP_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_TOP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_TOP_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_vfe0_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_VFE0_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_VFE0_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_VFE0_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_vfe1_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_VFE1_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_VFE1_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_VFE1_AHB_CLK
  },
  {
    /* .szClockName      = */ "camss_vfe_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_VFE_AHB_CBCR), HWIO_OFFS(MMSS_CAMSS_VFE_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_VFE_AHB_CLK
  },
  {
    /* .szClockName      = */ "fd_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_FD_AHB_CBCR), HWIO_OFFS(MMSS_FD_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_FD_AHB_CLK
  },
  {
    /* .szClockName      = */ "gpu_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_GPU_AHB_CBCR), HWIO_OFFS(MMSS_GPU_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_GPU_AHB_CLK
  },
  {
    /* .szClockName      = */ "mdss_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_AHB_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_AHB_CLK
  },
  {
    /* .szClockName      = */ "mdss_hdmi_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_HDMI_AHB_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_HDMI_AHB_CLK
  },
#if 0 /* not present on apcs */

  {
    /* .szClockName      = */ "mmss_bto_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_BTO_AHB_CBCR), HWIO_OFFS(MMSS_BTO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_BTO_AHB_CLK
  },
#endif

  {
    /* .szClockName      = */ "mmss_misc_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MISC_AHB_CBCR), HWIO_OFFS(MMSS_MISC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MISC_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmss_mmagic_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MMAGIC_AHB_CBCR), HWIO_OFFS(MMSS_MMAGICAHB_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MMAGIC_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmss_mmagic_cfg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MMAGIC_CFG_AHB_CBCR), HWIO_OFFS(MMSS_MMAGIC_CFG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MMAGIC_CFG_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmss_rbcpr_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_RBCPR_AHB_CBCR), HWIO_OFFS(MMSS_MMSS_RBCPR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_RBCPR_AHB_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_AHB_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_cpp_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_CPP_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_CPP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_CPP_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_jpeg_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_JPEG_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_JPEG_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_mdp_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_MDP_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_MDP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_MDP_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_rot_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_ROT_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_ROT_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_ROT_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_vfe_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_VFE_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_VFE_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_VFE_AHB_CLK
  },
  {
    /* .szClockName      = */ "smmu_video_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_VIDEO_AHB_CBCR), HWIO_OFFS(MMSS_SMMU_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_VIDEO_AHB_CLK
  },
  {
    /* .szClockName      = */ "throttle_camss_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_CAMSS_AHB_CBCR), HWIO_OFFS(MMSS_THROTTLE_CAMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_CAMSS_AHB_CLK
  },
  {
    /* .szClockName      = */ "throttle_mdss_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_MDSS_AHB_CBCR), HWIO_OFFS(MMSS_THROTTLE_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_MDSS_AHB_CLK
  },
  {
    /* .szClockName      = */ "throttle_video_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_VIDEO_AHB_CBCR), HWIO_OFFS(MMSS_THROTTLE_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_VIDEO_AHB_CLK
  },
  {
    /* .szClockName      = */ "video_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VIDEO_AHB_CBCR), HWIO_OFFS(MMSS_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VIDEO_AHB_CLK
  },
  {
    /* .szClockName      = */ "vmem_ahb_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VMEM_AHB_CBCR), HWIO_OFFS(MMSS_VMEM_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VMEM_AHB_CLK
  },
};


/*
 * HAL_clk_mMMSSAHBClkDomain
 *
 * AHB clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSAHBClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_AHB_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mAHBClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mAHBClkDomainClks)/sizeof(HAL_clk_mAHBClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

