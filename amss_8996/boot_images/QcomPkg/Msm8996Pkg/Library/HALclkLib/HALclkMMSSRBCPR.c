/*
==============================================================================

FILE:         HALclkMMSSRBCPR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMSSRBCPR clocks.

   List of clock domains:
     - HAL_clk_mMMSSRBCPRClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mRBCPRClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBCPRClkDomainClks[] =
{
  {
    /* .szClockName      = */ "mmss_rbcpr_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_RBCPR_CBCR), HWIO_OFFS(MMSS_MMSS_RBCPR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_RBCPR_CLK
  },
};


/*
 * HAL_clk_mMMSSRBCPRClkDomain
 *
 * RBCPR clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSRBCPRClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_RBCPR_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mRBCPRClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBCPRClkDomainClks)/sizeof(HAL_clk_mRBCPRClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

