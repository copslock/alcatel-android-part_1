/*
==============================================================================

FILE:         HALclkMMSSMain.c

DESCRIPTION:
   The main auto-generated file for MMSS.


==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/



/*
 * Clock domains
 */
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSAHBClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSBYTE0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSBYTE1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCAMSSGP0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCAMSSGP1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCCIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCOREPISLEEPCLKClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCPPClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI0PHYTIMERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI1PHYTIMERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI2ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI2PHYTIMERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSI3ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSIPHY03PClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSIPHY13PClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSCSIPHY23PClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSDSACOREClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPAUXClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPGTCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPLINKClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSEDPPIXELClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSESC0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSESC1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSEXTPCLKClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSFDCOREClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSGCCMMSSNOCCFGAHBCLKClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSGFX3DClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSHDMIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSISENSEClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEG0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEG2ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSJPEGDMAClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMAXIClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMCLK0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMCLK1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMCLK2ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMCLK3ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSMDPClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSPCLK0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSPCLK1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSRBBMTIMERClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSRBCPRClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVFE0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVFE1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVIDEOCOREClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVIDEOSUBCORE0ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVIDEOSUBCORE1ClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSVSYNCClkDomain;
extern HAL_clk_ClockDomainDescType HAL_clk_mMMSSXOClkDomain;

/*
 * Power domains
 */
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSCPPPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSJPEGPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSTOPPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSVFE0PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSCAMSSVFE1PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSFDPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSGPUPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSGPUGXPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMDSSPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICBIMCPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICCAMSSPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICMDSSPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSMMAGICVIDEOPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSVIDEOPowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSVIDEOSUBCORE0PowerDomain;
extern HAL_clk_PowerDomainDescType HAL_clk_mMMSSVIDEOSUBCORE1PowerDomain;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aMMSSSourceMap
 *
 * MMSS HW source mapping
 *
 */
static HAL_clk_SourceMapType aMMSSSourceMap[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_MMPLL0,             1 },
  { HAL_CLK_SOURCE_MMPLL1,             2 },
  { HAL_CLK_SOURCE_MMPLL4,             3 },
  { HAL_CLK_SOURCE_SLEEPCLK,           4 },
  { HAL_CLK_SOURCE_GPLL0,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap
};


/*
 * HAL_clk_mMMSSClockDomainControlRO
 *
 * Read-only functions for MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControlRO =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap
};


/*
 * aMMSSSourceMap_1
 *
 * MMSS HW source mapping
 * 
 */
static HAL_clk_SourceMapType aMMSSSourceMap_1[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_MMPLL0,             1 },
  { HAL_CLK_SOURCE_MMPLL1,             2 },
  { HAL_CLK_SOURCE_MMPLL4,             3 },
  { HAL_CLK_SOURCE_MMPLL3,             4 },
  { HAL_CLK_SOURCE_GPLL0,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl_1
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl_1 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_1
};


/*
 * HAL_clk_mMMSSClockDomainControlRO_1
 *
 * Read-only functions for MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControlRO_1 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_1
};


/*
 * aMMSSSourceMap_2
 *
 * MMSS HW source mapping
 * 
 */
static HAL_clk_SourceMapType aMMSSSourceMap_2[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_EXTERNAL1,          1 },
  { HAL_CLK_SOURCE_EXTERNAL2,          2 },
  { HAL_CLK_SOURCE_EXTERNAL3,          3 },
  { HAL_CLK_SOURCE_EXTERNAL4,          4 },
  { HAL_CLK_SOURCE_EXTERNAL5,          5 },
  { HAL_CLK_SOURCE_GROUND,             6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl_2
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl_2 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_2
};


/*
 * HAL_clk_mMMSSClockDomainControlRO_2
 *
 * Read-only functions for MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControlRO_2 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_2
};


/*
 * aMMSSSourceMap_3
 *
 * MMSS HW source mapping
 * 
 */
static HAL_clk_SourceMapType aMMSSSourceMap_3[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_MMPLL0,             1 },
  { HAL_CLK_SOURCE_MMPLL5,             2 },
  { HAL_CLK_SOURCE_MMPLL3,             3 },
  { HAL_CLK_SOURCE_GROUND,             4 },
  { HAL_CLK_SOURCE_GPLL0,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl_3
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl_3 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_3
};


/*
 * HAL_clk_mMMSSClockDomainControlRO_3
 *
 * Read-only functions for MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControlRO_3 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_3
};


/*
 * aMMSSSourceMap_4
 *
 * MMSS HW source mapping
 * 
 */
static HAL_clk_SourceMapType aMMSSSourceMap_4[] =
{
  { HAL_CLK_SOURCE_XO,                 0 },
  { HAL_CLK_SOURCE_MMPLL0,             1 },
  { HAL_CLK_SOURCE_MMPLL9,             2 },
  { HAL_CLK_SOURCE_MMPLL2,             3 },
  { HAL_CLK_SOURCE_MMPLL8,             4 },
  { HAL_CLK_SOURCE_GPLL0,              5 },
  { HAL_CLK_SOURCE_GPLL0_DIV2,         6 },
  { HAL_CLK_SOURCE_PLLTEST,            7 },
  { HAL_CLK_SOURCE_NULL,               HAL_CLK_SOURCE_INDEX_INVALID }
};


/*
 * HAL_clk_mMMSSClockDomainControl_4
 *
 * Functions for controlling MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControl_4 =
{
   /* .ConfigMux          = */ HAL_clk_GenericConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_4
};


/*
 * HAL_clk_mMMSSClockDomainControlRO_4
 *
 * Read-only functions for MMSS clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mMMSSClockDomainControlRO_4 =
{
   /* .ConfigMux          = */ NULL,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aMMSSSourceMap_4
};


/*
 * HAL_clk_aMMSSClockDomainDesc
 *
 * List of MMSS clock domains
*/
static HAL_clk_ClockDomainDescType * HAL_clk_aMMSSClockDomainDesc [] =
{
  &HAL_clk_mMMSSAHBClkDomain,
  &HAL_clk_mMMSSAXIClkDomain,
  &HAL_clk_mMMSSBYTE0ClkDomain,
  &HAL_clk_mMMSSBYTE1ClkDomain,
  &HAL_clk_mMMSSCAMSSGP0ClkDomain,
  &HAL_clk_mMMSSCAMSSGP1ClkDomain,
  &HAL_clk_mMMSSCCIClkDomain,
  &HAL_clk_mMMSSCOREPISLEEPCLKClkDomain,
  &HAL_clk_mMMSSCPPClkDomain,
  &HAL_clk_mMMSSCSI0ClkDomain,
  &HAL_clk_mMMSSCSI0PHYTIMERClkDomain,
  &HAL_clk_mMMSSCSI1ClkDomain,
  &HAL_clk_mMMSSCSI1PHYTIMERClkDomain,
  &HAL_clk_mMMSSCSI2ClkDomain,
  &HAL_clk_mMMSSCSI2PHYTIMERClkDomain,
  &HAL_clk_mMMSSCSI3ClkDomain,
  &HAL_clk_mMMSSCSIPHY03PClkDomain,
  &HAL_clk_mMMSSCSIPHY13PClkDomain,
  &HAL_clk_mMMSSCSIPHY23PClkDomain,
  &HAL_clk_mMMSSDSACOREClkDomain,
  &HAL_clk_mMMSSEDPAUXClkDomain,
  &HAL_clk_mMMSSEDPGTCClkDomain,
  &HAL_clk_mMMSSEDPLINKClkDomain,
  &HAL_clk_mMMSSEDPPIXELClkDomain,
  &HAL_clk_mMMSSESC0ClkDomain,
  &HAL_clk_mMMSSESC1ClkDomain,
  &HAL_clk_mMMSSEXTPCLKClkDomain,
  &HAL_clk_mMMSSFDCOREClkDomain,
  &HAL_clk_mMMSSGCCMMSSNOCCFGAHBCLKClkDomain,
  &HAL_clk_mMMSSGFX3DClkDomain,
  &HAL_clk_mMMSSHDMIClkDomain,
  &HAL_clk_mMMSSISENSEClkDomain,
  &HAL_clk_mMMSSJPEG0ClkDomain,
  &HAL_clk_mMMSSJPEG2ClkDomain,
  &HAL_clk_mMMSSJPEGDMAClkDomain,
  &HAL_clk_mMMSSMAXIClkDomain,
  &HAL_clk_mMMSSMCLK0ClkDomain,
  &HAL_clk_mMMSSMCLK1ClkDomain,
  &HAL_clk_mMMSSMCLK2ClkDomain,
  &HAL_clk_mMMSSMCLK3ClkDomain,
  &HAL_clk_mMMSSMDPClkDomain,
  &HAL_clk_mMMSSPCLK0ClkDomain,
  &HAL_clk_mMMSSPCLK1ClkDomain,
  &HAL_clk_mMMSSRBBMTIMERClkDomain,
  &HAL_clk_mMMSSRBCPRClkDomain,
  &HAL_clk_mMMSSVFE0ClkDomain,
  &HAL_clk_mMMSSVFE1ClkDomain,
  &HAL_clk_mMMSSVIDEOCOREClkDomain,
  &HAL_clk_mMMSSVIDEOSUBCORE0ClkDomain,
  &HAL_clk_mMMSSVIDEOSUBCORE1ClkDomain,
  &HAL_clk_mMMSSVSYNCClkDomain,
  &HAL_clk_mMMSSXOClkDomain,
  NULL
};


/*
 * HAL_clk_aMMSSPowerDomainDesc
 *
 * List of MMSS power domains
 */
static HAL_clk_PowerDomainDescType * HAL_clk_aMMSSPowerDomainDesc [] =
{
  &HAL_clk_mMMSSCAMSSCPPPowerDomain,
  &HAL_clk_mMMSSCAMSSJPEGPowerDomain,
  &HAL_clk_mMMSSCAMSSTOPPowerDomain,
  &HAL_clk_mMMSSCAMSSVFE0PowerDomain,
  &HAL_clk_mMMSSCAMSSVFE1PowerDomain,
  &HAL_clk_mMMSSFDPowerDomain,
  &HAL_clk_mMMSSGPUPowerDomain,
  &HAL_clk_mMMSSGPUGXPowerDomain,
  &HAL_clk_mMMSSMDSSPowerDomain,
  &HAL_clk_mMMSSMMAGICBIMCPowerDomain,
  &HAL_clk_mMMSSMMAGICCAMSSPowerDomain,
  &HAL_clk_mMMSSMMAGICMDSSPowerDomain,
  &HAL_clk_mMMSSMMAGICVIDEOPowerDomain,
  &HAL_clk_mMMSSVIDEOPowerDomain,
  &HAL_clk_mMMSSVIDEOSUBCORE0PowerDomain,
  &HAL_clk_mMMSSVIDEOSUBCORE1PowerDomain,
  NULL
};



/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_PlatformInitMMSSMain
**
** ======================================================================== */

void HAL_clk_PlatformInitMMSSMain (void)
{

  /*
   * Install all clock domains
   */
  HAL_clk_InstallClockDomains(HAL_clk_aMMSSClockDomainDesc, MMSS_BASE);

  /*
   * Install all power domains
   */
  HAL_clk_InstallPowerDomains(HAL_clk_aMMSSPowerDomainDesc, MMSS_BASE);

} /* END HAL_clk_PlatformInitMMSSMain */

