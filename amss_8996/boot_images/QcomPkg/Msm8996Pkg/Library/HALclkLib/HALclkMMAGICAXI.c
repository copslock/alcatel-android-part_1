/*
==============================================================================

FILE:         HALclkMMAGICAXI.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MMAGICAXI clocks.

   List of clock domains:
     - HAL_clk_mMMSSAXIClkDomain


   List of power domains:



==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "camss_cpp_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_CPP_AXI_CBCR), HWIO_OFFS(MMSS_CAMSS_CPP_TOP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_CPP_AXI_CLK
  },
  {
    /* .szClockName      = */ "camss_jpeg_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_JPEG_AXI_CBCR), HWIO_OFFS(MMSS_CAMSS_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_JPEG_AXI_CLK
  },
  {
    /* .szClockName      = */ "camss_vfe_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_CAMSS_VFE_AXI_CBCR), HWIO_OFFS(MMSS_CAMSS_VFE_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_CAMSS_VFE_AXI_CLK
  },
  {
    /* .szClockName      = */ "mdss_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MDSS_AXI_CBCR), HWIO_OFFS(MMSS_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MDSS_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmagic_bimc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_BIMC_AXI_CBCR), HWIO_OFFS(MMSS_MMAGIC_BIMC_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_BIMC_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmagic_camss_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_CAMSS_AXI_CBCR), HWIO_OFFS(MMSS_MMAGIC_CAMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_CAMSS_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmagic_mdss_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_MDSS_AXI_CBCR), HWIO_OFFS(MMSS_MMAGIC_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_MDSS_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmagic_video_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMAGIC_VIDEO_AXI_CBCR), HWIO_OFFS(MMSS_MMAGIC_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMAGIC_VIDEO_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmss_mmagic_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_MMAGIC_AXI_CBCR), HWIO_OFFS(MMSS_MMAGICAXI_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_MMAGIC_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmss_s0_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_S0_AXI_CBCR), HWIO_OFFS(MMSS_MMAGICAXI_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_S0_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_AXI_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_AXI_CLK
  },
  {
    /* .szClockName      = */ "mmss_spdm_rm_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_MMSS_SPDM_RM_AXI_CBCR), HWIO_OFFS(MMSS_MMSS_SPDM_RM_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_MMSS_SPDM_RM_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_cpp_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_CPP_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_CPP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_CPP_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_jpeg_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_JPEG_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_JPEG_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_JPEG_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_mdp_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_MDP_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_MDP_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_MDP_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_rot_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_ROT_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_ROT_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_ROT_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_vfe_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_VFE_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_VFE_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_VFE_AXI_CLK
  },
  {
    /* .szClockName      = */ "smmu_video_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_SMMU_VIDEO_AXI_CBCR), HWIO_OFFS(MMSS_SMMU_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_SMMU_VIDEO_AXI_CLK
  },
  {
    /* .szClockName      = */ "throttle_camss_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_CAMSS_AXI_CBCR), HWIO_OFFS(MMSS_THROTTLE_CAMSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_CAMSS_AXI_CLK
  },
  {
    /* .szClockName      = */ "throttle_mdss_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_MDSS_AXI_CBCR), HWIO_OFFS(MMSS_THROTTLE_MDSS_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_MDSS_AXI_CLK
  },
  {
    /* .szClockName      = */ "throttle_video_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_THROTTLE_VIDEO_AXI_CBCR), HWIO_OFFS(MMSS_THROTTLE_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_THROTTLE_VIDEO_AXI_CLK
  },
  {
    /* .szClockName      = */ "video_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(MMSS_VIDEO_AXI_CBCR), HWIO_OFFS(MMSS_VIDEO_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MMSS_TEST_VIDEO_AXI_CLK
  },
};


/*
 * HAL_clk_mMMSSAXIClkDomain
 *
 * AXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMMSSAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MMSS_AXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mAXIClkDomainClks)/sizeof(HAL_clk_mAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

